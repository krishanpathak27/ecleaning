<?php
include("includes/config.inc.php");
include("includes/function.php");
$page_name="WhiteSpot Building";
$_objAdmin = new Admin();
$cleaner_id = $_REQUEST['id'];
$fromdate = date("y-m-d",strtotime("-30 day")); 
$todate = date("y-m-d",strtotime("+30 day")); 
if(isset($cleaner_id ) && $cleaner_id != '')
{
	$cleaner_details = $_objAdmin->_getselectList2('table_cleaner','cleaner_name','','cleaner_id='.$cleaner_id);
	$_objAdmin = new Admin();
	$tb = "`table_booking_allotment` as A LEFT JOIN `table_booking_details` as B ON A.booking_detail_id = B.booking_detail_id"
	." LEFT JOIN `table_booking_register` as C ON B.booking_id = C.booking_id"
	." LEFT JOIN `table_customer_address` as D ON C.customer_address_id = D.customer_address_id"
	." LEFT JOIN `table_buildings` as E ON D.building_id = E.building_id";
	$colmns = "cleaning_time_start,cleaning_time_end,C.customer_address_id,E.building_name";
	$whr ="A.cleaner_id = '".$cleaner_id."' AND B.cleaning_date BETWEEN '".$fromdate."' AND '".$todate."'";
	$time_booked = $_objAdmin->_getselectList2($tb,$colmns,'',$whr); 
	$booked_times =array();
	$events = '';
	foreach($time_booked as $time_book)
	{
		if($time_book->cleaning_time_start == NULL || $time_book->cleaning_time_start == null) continue;
		$booked_times[] = array('from'=> $time_book->cleaning_time_start,'to'=> $time_book->cleaning_time_end); 
		$desc = date('h:i A',strtotime($time_book->cleaning_time_start)).' - '.date('h:i A',strtotime($time_book->cleaning_time_end)).'\n'.$time_book->building_name;
		$events .= "{  title: '".$time_book->building_name."',
                        start: '".$time_book->cleaning_time_start."',
                        description: '".$desc."',
                        end: '".$time_book->cleaning_time_end."',
                        className: 'm-fc-event--accent slot-alloted'
                    },";			
					
		
	}

	
}
?>
<?php include("header.inc.php") ?>
<div class="m-grid__item m-grid__item--fluid m-wrapper">
					<!-- BEGIN: Subheader -->
					<div class="m-subheader ">
						<div class="d-flex align-items-center">
							<div class="mr-auto">
								<h3 class="m-subheader__title m-subheader__title--separator">
									
								</h3>
								<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
									<li class="m-nav__item m-nav__item--home">
										<a href="#" class="m-nav__link m-nav__link--icon">
											<i class="m-nav__link-icon la la-home"></i>
										</a>
									</li>
									<li class="m-nav__separator">
										-
									</li>
									<li class="m-nav__item">
										<a href="" class="m-nav__link">
											<span class="m-nav__link-text">
												Whitespot
											</span>
										</a>
									</li>
									<li class="m-nav__separator">
										-
									</li>
									<li class="m-nav__item">
										<a href="" class="m-nav__link">
											<span class="m-nav__link-text">
												Schedule Calander
											</span>
										</a>
									</li>
								</ul>
							</div>
							<div>
								<div class="m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push" data-dropdown-toggle="hover" aria-expanded="true">
									
									<div class="m-dropdown__wrapper">
										<span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>
										<div class="m-dropdown__inner">
											<div class="m-dropdown__body">
												<div class="m-dropdown__content">
													<ul class="m-nav">
														<li class="m-nav__section m-nav__section--first m--hide">
															<span class="m-nav__section-text">
																Quick Actions
															</span>
														</li>
														<li class="m-nav__item">
															<a href="" class="m-nav__link">
																<i class="m-nav__link-icon flaticon-share"></i>
																<span class="m-nav__link-text">
																	Activity
																</span>
															</a>
														</li>
														<li class="m-nav__item">
															<a href="" class="m-nav__link">
																<i class="m-nav__link-icon flaticon-chat-1"></i>
																<span class="m-nav__link-text">
																	Messages
																</span>
															</a>
														</li>
														<li class="m-nav__item">
															<a href="" class="m-nav__link">
																<i class="m-nav__link-icon flaticon-info"></i>
																<span class="m-nav__link-text">
																	FAQ
																</span>
															</a>
														</li>
														<li class="m-nav__item">
															<a href="" class="m-nav__link">
																<i class="m-nav__link-icon flaticon-lifebuoy"></i>
																<span class="m-nav__link-text">
																	Support
																</span>
															</a>
														</li>
														<li class="m-nav__separator m-nav__separator--fit"></li>
														<li class="m-nav__item">
															<a href="#" class="btn btn-outline-danger m-btn m-btn--pill m-btn--wide btn-sm">
																Submit
															</a>
														</li>
													</ul>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- END: Subheader -->
					<div class="m-content">
						<div class="row">
							<div class="col-lg-12">
								<!--begin::Portlet-->
								<div class="m-portlet" id="m_portlet">
									<div class="m-portlet__head">
										<div class="m-portlet__head-caption">
											<div class="m-portlet__head-title">
												<span class="m-portlet__head-icon">
													<i class="flaticon-map-location"></i>
												</span>
												<h3 class="m-portlet__head-text text-center">
                                                  <?php if($cleaner_id =='') { echo 'Schedule Calendar';} else { echo $cleaner_details[0]->cleaner_name; } ?>
													
												</h3>
											</div>
										</div>
										<div class="m-portlet__head-tools">
											<ul class="m-portlet__nav">
												<li class="m-portlet__nav-item">
													
												</li>
											</ul>
										</div>
									</div>
									<div class="m-portlet__body">
										<div id="m2_calendar"></div>
									</div>
								</div>
								<!--end::Portlet-->
							</div>
						</div>
					</div>
				</div>
</div>
</div>


<div class="modal fade" id="m_modal_2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">
                    Ready to Delete?
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">
                        &times;
                    </span>
                </button>
            </div>
            <div class="modal-body">
                <p>
                    Are you sure,You want to Delete.
                </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">
                    Close
                </button>
                <a id="deteleA" href="" class="btn btn-primary">
                    Delete
                </a>
            </div>
        </div>
    </div>
</div>
<!-- start footer -->         
<?php include("footer.php") ?>
<script type="text/javascript">
var CalendarBasic = function() {

    return {
        //main function to initiate the module
        init: function() {
            var todayDate = moment().startOf('day');
			
            var YM = todayDate.format('YYYY-MM');
            var YESTERDAY = todayDate.clone().subtract(1, 'day').format('YYYY-MM-DD');
            var TODAY = todayDate.format('YYYY-MM-DD');
            var TOMORROW = todayDate.clone().add(1, 'day').format('YYYY-MM-DD');

            $('#m2_calendar').fullCalendar({
                header: {
                    left: 'prev,next today',
                    center: 'title',
                    right: 'month,listWeek,listDay'
                },
				views: {
				listDay: { buttonText: 'List Day' },
				listWeek: { buttonText: 'List Week' }
				},

				allDaySlot: false,
        		axisFormat: 'h:mm A',
        		timeFormat: 'h:mm T',
        		minTime: '10:00:00',
       			maxTime: '21:00:00',
                editable: false,
                eventLimit: true, // allow "more" link when too many events
                navLinks: true,
				timeFormat: 'hh(:mm) A',
                events: [
                   
                   <?php echo $events; ?>
                   
                ],
                dayClick: function(date, jsEvent, view) {
						if(view.name != 'month')
						return;
                         $('#m2_calendar').fullCalendar('changeView', 'listDay')
						 $('#m2_calendar').fullCalendar('gotoDate', date);
					
				},
                eventRender: function(event, element) {
                    if (element.hasClass('fc-day-grid-event')) {
                        element.data('content', event.description);
                        element.data('placement', 'top');
                        mApp.initPopover(element); 
                    } else if (element.hasClass('fc-time-grid-event')) {
                        element.find('.fc-title').append('<div class="fc-description">' + event.description + '</div>'); 
                    } else if (element.find('.fc-list-item-title').lenght !== 0) {
                        element.find('.fc-list-item-title').append('<div class="fc-description">' + event.description + '</div>'); 
                    }
                }
            });
        }
    };
}();

jQuery(document).ready(function() {
    CalendarBasic.init();
});
</script>

