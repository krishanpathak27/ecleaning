<?php
include("includes/config.inc.php");
include("includes/function.php");
$page_name="Bookings";
$_objAdmin = new Admin();



if(isset($_POST['showBooking']) && $_POST['showBooking'] == 'yes'){ 
    if($_POST['from']!=""){
        $_SESSION['FromBookingList']=date('Y-m-d',strtotime($_POST['from']));  
    }

    if($_POST['to']!=""){
        $_SESSION['ToBookingList']=date('Y-m-d',strtotime($_POST['to']));
    }
    if($_POST['bookingStatus']!=""){
        $_SESSION['bookingStatus']=$_POST['bookingStatus'];
    }
    if($_POST['building_filter']!=""){
       
        $_SESSION['buildingFil']=$_POST['building_filter'];
    }
    if($_POST['customer_filter']!=""){
       
        $_SESSION['customerFil']=$_POST['customer_filter'];
    }
}

if($_POST['from']==''){
  $_SESSION['FromBookingList']= '';
}
//echo $_SESSION['FromBookingList'];die;

if($_POST['to']==''){
  $_SESSION['ToBookingList']= '';
}
if($_POST['building_filter']==''){
  $_SESSION['buildingFil']= '';
}
if($_POST['bookingStatus']==''){
  $_SESSION['bookingStatus']= '';
  //echo "hiii";die;
}
if($_POST['customer_filter']==''){
  $_SESSION['customerFil']= '';
}

if(isset($_POST['export']) && $_POST['export']!=""){


    header("Location:export.inc.php?export_bookings");
    exit;
}
if(isset($_REQUEST['reset']) && $_REQUEST['reset'] == 'yes'){ 

  $_SESSION['FromBookingList']= '';
  $_SESSION['ToBookingList']='';
  $_SESSION['bookingStatus']='';
   $_SESSION['customerFil']='';
   $_SESSION['buildingFil']='';
  header("Location: booking.php");
}
?>
<?php include("header.inc.php"); ?>
<div class="m-grid__item m-grid__item--fluid m-wrapper" style="background-color:">
    <!-- BEGIN: Subheader -->
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
            <h3 class="m-subheader__title m-subheader__title--separator">
                   
                </h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="#" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__separator">
                        -
                    </li>
                    <li class="m-nav__item">
                        <a href="#" class="m-nav__link">
                            <span class="m-nav__link-text">
                                Resource Scheduling
                            </span>
                        </a>
                    </li>
                    <li class="m-nav__separator">
                        -
                    </li>
                    <li class="m-nav__item">
                        <a href="" class="m-nav__link">
                            <span class="m-nav__link-text">
                                Cleaning Scheduling
                            </span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <!-- END: Subheader -->
    <div class="m-content">
        <?php 
            if(isset($_REQUEST['suc']) && $_REQUEST['suc'] != "") { ?>
            <div role="alert" style="background: #d7fbdc;" class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-dismissible m--margin-bottom-30">
                <div class="m-alert__icon" >
                        <i class="flaticon-exclamation m--font-brand"></i>
                </div>
                <div class="m-alert__text">
                       <?php echo  $_REQUEST['suc']; ?> 
                </div>
            </div>
            <?php } ?>
        <div class="m-portlet m-portlet--mobile">
            
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            Booking List
                        </h3>
                    </div>
                </div>
            </div>
            <div class="m-portlet__body">

                     <form class="m-form m-form--fit m-form--label-align-right" name="frmPre" id="frmPre" method="post" action="" enctype="multipart/form-data">
                    <div class="form-group m-form__group row">
                      
                        <div class="col-lg-4">
                         <label>
                                Booking Status:
                            </label>
                            <div class="input-group m-input-group m-input-group--square">
                         
                                            
                                <select name='bookingStatus' class="form-control m-input m-input--square">
                                   
                                   <option value="UA" <?php if($_SESSION['bookingStatus']=='UA') echo "selected";  ?>>Not Alloted Booking</option>
                                   <option value="AL" <?php if($_SESSION['bookingStatus']=='AL') echo "selected";  ?>>Allotted Booking</option>
                                     
                                   
                                </select>

                                </div>
                                </div>
                           
                        <div class="col-lg-4">
                                  <label>
                                        Buildings:
                                    </label>
                                <div class="input-group m-input-group m-input-group--square">
                                <select name='building_filter' class="form-control m-input m-input--square">
                                    <option value="">All</option>

                                    <?php  
                                            $auRec = $_objAdmin->_getSelectList2('table_buildings', 'building_id,building_name', '', " status = 'A' ORDER BY building_name");
                                            if(!empty($auRec)){ foreach ($auRec as $value): ?>


                                                <option value="<?php echo $value->building_id;?>"<?php if($_SESSION['buildingFil']==$value->building_id) echo "selected";  ?>><?php echo $value->building_name?>
                                                    
                                                </option>
                                               
                                            <?php endforeach;}?>
                                   </select>
                       
                               
                                </div>
                    
                            </div>
                            
                        <div class="col-lg-4" style="padding-bottom: 10px">
                                  <label>
                                        Date Picker
                                    </label> 
                                    <div class="m-input-icon m-input-icon--right">
                                        <div class="input-daterange input-group" id="m_datepicker_5">
                                            <input type="text" class="form-control m-input" name="from" id="from" placeholder="From"value="<?php if(!empty($_SESSION['FromBookingList'])){echo date('m/d/Y',strtotime($_SESSION['FromBookingList']));}?>">
                                            <span class="input-group-addon">
                                                <i class="la la-ellipsis-h"></i>
                                            </span>
                                            <input type="text" class="form-control" name="to" placeholder="To" id="to" value="<?php if(!empty($_SESSION['ToBookingList'])){echo date('m/d/Y',strtotime($_SESSION['ToBookingList']));}?>">
                                        </div>

                                    </div>
                                </div>    
                        </div>
                        


                   
                
                <div></div>



                  
                            <div class="form-group m-form__group row">
                               
                                
                                <div class="col-lg-4 "> 
                                    <div class="m-input-icon m-input-icon--right">
                                        <button type="submit" class="btn btn-success">
                                            Submit
                                        </button>
                                       
                                         <button type="reset" class="btn btn-secondary" onclick="location.href='booking.php?reset=yes';">
                                            Reset
                                        </button>
                                    </div> 
                                </div> 
                            </div>
                            <input name="showBooking" type="hidden" value="yes"/>
                        </form>

                <!--begin: Search Form -->
                <!--
                <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
                    <div class="row align-items-center">
                        <div class="col-xl-8 order-2 order-xl-1">
                            <div class="form-group m-form__group row align-items-center">
                                <div class="col-md-4">
                                    <div class="m-input-icon m-input-icon--left">
                                        <input type="text" class="form-control m-input" placeholder="Search..." id="generalSearch">
                                        <span class="m-input-icon__icon m-input-icon__icon--left">
                                            <span>
                                                <i class="la la-search"></i>
                                            </span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
                -->
                <!--end: Search Form -->
            <!--begin: Datatable -->
                <div class="cleaning_schedule" id="booking_listId"></div>
                <!--end: Datatable -->
                <!-- Modal -->
                <div id="form_modal4" class="modal fade" aria-hidden="true" style="display: none;">
                   <div class="modal-dialog">
                      <div class="modal-content">
                        <div class="modal-body">
                            <form action="#" class="form-horizontal">
                              <div class="form-group m-form__group row">
                                    <div class="col-lg-6">
                                        <label class="control-label col-md-6">Start Time</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control cleaning_time_start timepicker timepicker-24">
                                            <span class="input-group-btn">
                                                <button class="btn default" type="button">
                                                    <i class="fa fa-clock-o"></i>
                                                </button>
                                            </span>
                                        </div>
                                    </div>
                                    
                                    <div class="col-lg-6">
                                        <label class="control-label col-md-6">Finish Time</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control cleaning_time_start timepicker timepicker-24">
                                            <span class="input-group-btn">
                                                <button class="btn default" type="button">
                                                    <i class="fa fa-clock-o"></i>
                                                </button>
                                            </span>
                                        </div>
                                    </div>
                               
                            </form>
                       </div>
                                                    <div class="modal-footer">
                                                        <button class="btn dark btn-outline" data-dismiss="modal" aria-hidden="true">Close</button>
                                                        <button class="btn green btn-primary" data-dismiss="modal">Save changes</button>
                                                    </div>
                      </div>
                   </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
<!-- start footer -->         
<?php include("footer.php") ?>
<script type="text/javascript" >
    var DefaultDatatableDemo = function () {
        //== Private functions

        // basic demo
        var demo = function () {
            var datatable = $('.cleaning_schedule').mDatatable({
                data: {
                    type: 'remote',
                    source: {
                        read: {
                            url: '<?php echo $ajaxUrl . '?pageType=CleaningSchedule' ?>'
                        }
                    },
                    pageSize: 20,
                    serverPaging: true,
                    serverFiltering: true,
                    serverSorting: true
                },
                layout: {
                    theme: 'default',
                    class: '',
                    scroll: true,
                    footer: false
                },
                sortable: true,
                filterable: false,
                pagination: true,
                search: {
                    input: $('#generalSearch')
                },
                columns: [{
                        field: "booking_id",
                        title: "Booking Id",
                        width: 70,
                        sortable: 'asc'
                    },
					{
                        field: "building_name",
                        title: "Building Name",
                        responsive: {visible: 'lg'}
                    }, 
					{
                        field: "unit_name",
                        title: "Unit Type",
                        sortable: 'asc',
                        responsive: {visible: 'lg'}
                    }, {
                        field: "cleaning_type",
                        title: "Cleaning Type",
                        sortable: 'asc',
                        responsive: {visible: 'lg'}
                    }, {
                        field: "room_detail",
                        title: "Room Detail",
                        responsive: {visible: 'lg'}
                    },
					{
                        field: "time_required",
                        title: "Time Required",
                        responsive: {visible: 'lg'}
                    },
					   
					 {
                        field: "Actions",
                        title: "Actions",
                        sortable: false,
                        locked: {right: 'xl'},
                        overflow: 'visible',
                        template: function (row, index, datatable) {
                            var dropup = (datatable.getPageSize() - index) <= 4 ? 'dropup' : '';
                             return '\<a href="booking_details.php?id='+row.booking_id+'" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only edit m-btn--pill" title="View Booking Details">\
                                                        <i class="la la-eye"></i>\
                                                </a>\<a href="booking_customer_details.php?id='+row.booking_id+'" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only edit m-btn--pill" title="View Customer Details">\
                                                        <i class="la la-info"></i>\
                                                </a>\<a href="#form_modal4" data-toggle="modal" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only edit m-btn--pill" title="Schedule">\
                                                        <i class="la la-clock-o"></i>\
                                                </a>';
                        }
                    }]
            });
        };

        return {
            // public functions
            init: function () {
                demo();
            }
        };
    }();

    jQuery(document).ready(function () {
        DefaultDatatableDemo.init();
    });

    jQuery(document).ready(function(){

        $("#booking_listId").on('dblclick','.m-datatable__row',function(){
            var bookingId=($('td:eq(0)',$(this)).find('span').html());
            window.location.href = "booking_details.php?id="+bookingId;
        });
    });


</script>


