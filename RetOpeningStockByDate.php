<?php
include("includes/config.inc.php");
include("includes/function.php");
$_objAdmin = new Admin();

if(isset($_REQUEST['show']) && $_REQUEST['show']=="yes")
{
	$_objAdmin->showRetailerStockByDate();
	die;
}
if(isset($_POST['DispatchOS']) && $_POST['DispatchOS'] == 'yes')
{	
	if($_POST['ret']!="") 
	{
	$_SESSION['RetailerID']=$_POST['ret'];	
	}
	if($_POST['from']!="") 
	{
	$_SESSION['FromOS']=$_objAdmin->_changeDate($_POST['from']);	
	}
	if($_POST['to']!="") 
	{
	$_SESSION['ToOS']=$_objAdmin->_changeDate($_POST['to']);	
	}
	
	if($_POST['ret']=="all") 
	{
	  unset($_SESSION['RetailerID']);	
	}
	
   if(isset($_POST['export']) && $_POST['export'])
   {
   header("Location:export.inc.php?export_retailer_opening_stock");
   exit;
   }	
	
	header("Location: RetOpeningStockByDate.php");
}
if(isset($_REQUEST['reset']) && $_REQUEST['reset'] == 'yes')
{
	unset($_SESSION['RetailerID']);	
	$_SESSION['FromOS']= $_objAdmin->_changeDate(date('Y-m-d'));
	$_SESSION['ToOS']= $_objAdmin->_changeDate(date('Y-m-d'));
	header("Location: RetOpeningStockByDate.php");
}
	if( $_SESSION['userLoginType']==3){
	//$disLogCond="o.distributor_id='".$_SESSION['userLoginType']."' and ";
	$retArray=array();
				$salArray=array();
				$getRetailerList=$_objAdmin->_getSelectList('table_retailer as r','r.retailer_id',''," r.distributor_id='".$_SESSION['distributorId']."'");			
				foreach($getRetailerList as $key=>$value){					
							$retArray[]=$value->retailer_id;					
					}
				if(sizeof($retArray)>0){ 
					
						$retList=implode(',',$retArray);
						
			$retCond=" and s.retailer_id IN(".$retList.")";
		}
	}

	if(isset($_SESSION['userLoginType']) && in_array($_SESSION['userLoginType'],array(6,7,8,9)))
	{
			
		$salesman = $_objAdmin->getSalesMenID();

		$RID=$_objAdmin->_getSelectList('table_order_os AS s',
		   		'DISTINCT(retailer_id) ',
		   		'',
		   		" 1 ".$salesman." AND retailer_id!='' AND s.ostype = 'R'"); 
		if(!empty($RID)){	
		function reducearray($obj){ return $obj->retailer_id; }
		
		$filtereddID = array_map('reducearray', $RID);
				
		//$filtereddID = array_map(function($obj){ return $obj->retailer_id;}, $RID);
		$RetID = implode(',',$filtereddID);		
		print_r($filtereddID);
		 $Retcondition = " AND retailer_id IN (".$RetID.")";
		} else {
		
		 $Retcondition = " AND retailer_id IN (0)";
		}
		
	} else {
		if( $_SESSION['userLoginType']==3){		
			$Retcondition = " retailer_id IN (".$retList.")";
		}
		else { $Retcondition = ""; }
	}


?>

<?php include("header.inc.php") ?>
<script src="javascripts/dateNextPrev.js" type="text/javascript"></script>

<!-- start content-outer -->
<input name="pagename" type="hidden"  id="pagename" value="RetOpeningStockByDate.php" />
<div id="content-outer">
<!-- start content -->
<div id="content">
<div id="page-heading"><h1><span style="color: #d74343; font-family: Tahoma; font-weight: bold;">Dealer Opening Stock</span></h1></div>
<table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">
<!--<tr>
	<th rowspan="3" class="sized"><img src="images/side_shadowleft.jpg" width="20" height="300" alt="" /></th>
	<th class="topleft"></th>
	<td id="tbl-border-top">&nbsp;</td>
	<th class="topright"></th>
	<th rowspan="3" class="sized"><img src="images/side_shadowright.jpg" width="20" height="300" alt="" /></th>
</tr>
-->
<tr>
	<!--<td id="tbl-border-left"></td>-->
	<td>
	<!--  start content-table-inner -->
	<div id="content-table-inner">
		
	<div id="page-heading" align="left" >
	<form name="frmPre" id="frmPre" method="post" action="" enctype="multipart/form-data" >
	<table border="0" width="85%" cellpadding="0" cellspacing="0">
	
	<tr>
	
	
	<td id="distributorMenu">
			<h3>Dealers: </h3><h6>
			<select name="ret" id="ret" class="" style="border: solid 1px #BDBDBD; color: #393939;font-family: Arial;font-size: 12px;border-radius:5px ;-moz-border-radius:5px;-o-border-radius:5px;-ms-border-radius:5px;-webkit-border-radius:5px;width: 150px;height: 30px;" >
			<option value="all">All</option>
			<?php $aSal=$_objAdmin->_getSelectList('table_retailer','retailer_id, retailer_name',''," $Retcondition ORDER BY retailer_name"); 
			if(is_array($aSal)){
			for($i=0;$i<count($aSal);$i++){?>
			<option value="<?php echo $aSal[$i]->retailer_id;?>" <?php if ($aSal[$i]->retailer_id==$_SESSION['RetailerID']){ ?> selected <?php } ?>><?php echo $aSal[$i]->retailer_name;?></option>
			<?php } }?>
			</select></h6>
		</td>
	
	
	
	
	<?php //echo "<pre>"; print_r($_SESSION);?>
	
	
	
		<td><h3>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;From Date:</h3><h6><img src="css/images/prev.png" height="18" width="18" onclick="dateFromPrev();"> <input type="text" id="from" name="from" class="date" style="width:150px" value="<?php if($_SESSION['FromOS']!='') { echo $_SESSION['FromOS']; } else { echo $_objAdmin->_changeDate(date('Y-m-d')); }?>"  readonly /> <img src="css/images/next.png" height="18" width="18" onclick="dateFromNext();"></h6></td>
		<td><h3>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;To Date: </h3><h6><img src="css/images/prev.png" height="18" width="18" onclick="dateToPrev();"> <input type="text" id="to" name="to" class="date" style="width:150px" value="<?php if($_SESSION['ToOS']!='') { echo $_SESSION['ToOS']; } else { echo $_objAdmin->_changeDate(date('Y-m-d')); }?>"  readonly /><img src="css/images/next.png" height="18" width="18" onclick="dateToNext();"></h6></td>
		<td><h3></h3><input name="DispatchOS" type="hidden" value="yes" />
		
		<input name="submit" class="result-submit" type="submit" id="submit" value="View Details" />
		<input type="button" value="Reset!" class="form-reset" onclick="location.href='RetOpeningStockByDate.php?reset=yes';" /></td></tr>
		<tr><td><input name="export" class="result-submit" type="submit" id="export" value="Export to Excel" >
		<!-- <a href='RetOpeningStockByDate_graph.php?y=<?php echo checkFromdate($_SESSION['FromOS'])."&m=".date('F',strtotime($_SESSION['FromOS']))."&retID=".$_SESSION['RetailerID']; ?>' target="_blank"><input type="button" value=" Show Graph" class="result-submit" /></a> -->
		
		</td>
	</tr>
	
	</table>
	</form>
	
	</div>
	
	
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
	<tr valign="top">
	<td>
		<!-- start id-form -->
		<table  border="0" cellspacing="0" cellpadding="0"  align="center" width="100%">
				<tr>
				<td align="lift" valign="top"  >
				<table id="flex1" style="display:none"></table>
				<script type="text/javascript">showRetailerStockByDate();</script>
				</td>
				</tr>
				</table>
	<!-- end id-form  -->

	</td>
<td>
 <!-- right bar-->
 <?php //include("rightbar/dispatch_bar.php") ?>
 </td>
</tr>
<tr>
<td><img src="images/blank.gif" width="695" height="1" alt="blank" /></td>
<td></td>
</tr>
</table>
<div class="clear"></div>
</div>
<!--  end content-table-inner  -->
</td>
<td id="tbl-border-right"></td>
</tr>
<!--<tr>
	<th class="sized bottomleft"></th>
	<td id="tbl-border-bottom">&nbsp;</td>
	<th class="sized bottomright"></th>
</tr>-->
</table>
<div class="clear">&nbsp;</div>
</div>
<!--  end content -->
<div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->
<div class="clear">&nbsp;</div> 
<!-- start footer -->         
<?php include("footer.php") ?>
<!-- end footer -->
<script src="javascripts/jquery-1.8.2.js" type="text/javascript"></script>
<script src="javascripts/jquery-ui.js"></script>
<script type="text/javascript" src="javascripts/validate.js"></script>
<script type="text/javascript">
// Popup window code
function newPopup(url) {
	popupWindow = window.open(
		url,'popUpWindow','height=500,width=600,left=200,top=100,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no,status=yes')
}
</script>
<script>
    $(function() {
        $( "#from" ).datepicker({
			dateFormat: "d M yy",
            defaultDate: "w",
            changeMonth: true,
            numberOfMonths: 1,
            onSelect: function( selectedDate ) {
                $( "#to" ).datepicker( "option", "minDate", selectedDate );
            }
        });
        $( "#to" ).datepicker({
			dateFormat: "d M yy",
            defaultDate: "-w",
            changeMonth: true,
            numberOfMonths: 1,
            onSelect: function( selectedDate ) {
                $( "#from" ).datepicker( "option", "maxDate", selectedDate );
            }
        });
    });
</script>
</body>
</html>