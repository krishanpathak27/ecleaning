<?php
include("includes/config.inc.php");
include("includes/function.php");
$page_name="WhiteSpot Building";
$_objAdmin = new Admin();
$fromdate = date("y-m-d",strtotime("-30 day")); 
$todate = date("y-m-d",strtotime("+30 day")); 
$cleanersLists=$_objAdmin->_getSelectList('table_cleaner',"cleaner_id,cleaner_name",''," status='A'");
$cleaners_names = '';
$events = '';
for($i=0;$i<count($cleanersLists);$i++){
	$cleaners_names.="{ id: '".$cleanersLists[$i]->cleaner_id."', title: '".$cleanersLists[$i]->cleaner_name."' },";
	
	$_objAdmin = new Admin();
	$tb = "`table_booking_allotment` as A LEFT JOIN `table_booking_details` as B ON A.booking_detail_id = B.booking_detail_id"
	." LEFT JOIN `table_booking_register` as C ON B.booking_id = C.booking_id"
	." LEFT JOIN `table_customer_address` as D ON C.customer_address_id = D.customer_address_id"
	." LEFT JOIN `table_buildings` as E ON D.building_id = E.building_id";
	$colmns = "cleaning_time_start,cleaning_time_end,C.customer_address_id,E.building_name,A.booking_allotment_id";
	$whr ="A.cleaner_id = '".$cleanersLists[$i]->cleaner_id."' AND B.cleaning_date BETWEEN '".$fromdate."' AND '".$todate."'";
	$time_booked = $_objAdmin->_getselectList2($tb,$colmns,'',$whr); 
	$booked_times =array();
	foreach($time_booked as $time_book)
	{
		
		$desc = date('h:i A',strtotime($time_book->cleaning_time_start)).' - '.date('h:i A',strtotime($time_book->cleaning_time_end));
		$events .= "{ id: '".$time_book->booking_allotment_id."', resourceId: '".$cleanersLists[$i]->cleaner_id."', start:'".$time_book->cleaning_time_start."', end: '".$time_book->cleaning_time_end."', title: '".$desc."',className: ['event', 'slot-alloted'],description: '".$time_book->building_name."'},";
		

	}
}
		
?>
<?php include("header.inc.php") ?>
<div class="m-grid__item m-grid__item--fluid m-wrapper">
					<!-- BEGIN: Subheader -->
					<div class="m-subheader ">
						<div class="d-flex align-items-center">
							<div class="mr-auto">
								<h3 class="m-subheader__title m-subheader__title--separator">
									
								</h3>
								<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
									<li class="m-nav__item m-nav__item--home">
										<a href="#" class="m-nav__link m-nav__link--icon">
											<i class="m-nav__link-icon la la-home"></i>
										</a>
									</li>
									<li class="m-nav__separator">
										-
									</li>
									<li class="m-nav__item">
										<a href="" class="m-nav__link">
											<span class="m-nav__link-text">
												Whitespot
											</span>
										</a>
									</li>
									<li class="m-nav__separator">
										-
									</li>
									<li class="m-nav__item">
										<a href="" class="m-nav__link">
											<span class="m-nav__link-text">
												Schedule Calander
											</span>
										</a>
									</li>
								</ul>
							</div>
							<div>
								
							</div>
						</div>
					</div>
					<!-- END: Subheader -->
					<div class="m-content">
						<div class="row">
							<div class="col-lg-12">
								<!--begin::Portlet-->
								<div class="m-portlet" id="m_portlet">
									<div class="m-portlet__head">
										<div class="m-portlet__head-caption">
											<div class="m-portlet__head-title">
												<span class="m-portlet__head-icon">
													<i class="flaticon-map-location"></i>
												</span>
												<h3 class="m-portlet__head-text text-center">
                                                 Schedule Calendar
													
												</h3>
											</div>
										</div>
										<div class="m-portlet__head-tools">
											<ul class="m-portlet__nav">
												<li class="m-portlet__nav-item">
													
												</li>
											</ul>
										</div>
									</div>
									<div class="m-portlet__body">
										<div id="calendar_full"></div>
									</div>
								</div>
								<!--end::Portlet-->
							</div>
						</div>
					</div>
				</div>
</div>
</div>

<!-- start footer -->         
<?php include("footer.php") ?>
<script>
var CalendarFull = function() { // dom ready
  return {
        //main function to initiate the module
  init: function() {
  var todayDate = moment().startOf('day');
  var YESTERDAY = todayDate.clone().subtract(1, 'day').format('YYYY-MM-DD');
  var TODAY = todayDate.format('YYYY-MM-DD');
  var TOMORROW = todayDate.clone().add(1, 'day').format('YYYY-MM-DD');

  $('#calendar_full').fullCalendar({
    resourceAreaWidth: 250,
    editable: false,
    scrollTime: '00:00',
    header: {
      left: 'today prev,next',
      center: 'title',
      right: 'month,agendaWeek,agendaDay,timelineDay'
    },
    defaultView: 'timelineDay',
    allDaySlot: false,
    axisFormat: 'h:mm A',
    timeFormat: 'h:mm T',
    minTime: '10:00:00',
    maxTime: '21:00:00',
	eventLimit: true, // allow "more" link when too many events
    navLinks: true,
    resourceLabelText: 'Cleaners',
	height: 'auto',
    resources: [
      <?php echo $cleaners_names; ?>
    ],
    events: [
      <?php echo $events; ?>
    ],
	
  });
	}
  };
}();

// readjust sizing after font load
jQuery(document).ready(function() {
    CalendarFull.init();
	$('.fc-prev-button span').click(function(){
  	 var view = $('#calendar_full').fullCalendar('getView');
	 alert("The view's title is " + view.title);
});

$('.fc-next-button span').click(function(){
     var view = $('#calendar_full').fullCalendar('getView');
	 alert("The view's title is " + view.title);
});
});

</script>

