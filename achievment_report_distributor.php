<?php
include("includes/config.inc.php");
include("includes/function.php");
include("includes/globalarraylist.php");


$page_name="Distributor Scheme Achievement Report";

$sale_Cond = "";

  if($_REQUEST['distributor_id']!='' && $_REQUEST['distributor_id'] > 0)
	{
		$sale_Cond = " o.distributor_id ='".$_REQUEST['distributor_id']."' ";


		$sql_discount = "select td.*,group_concat(distinct dp.state_id) as state_id,group_concat(distinct dp.city_id) as city_id,group_concat(distinct tddm.distributor_id) as distributor_id,tdi.category_id,tdi.item_id,tdi.brand_id from table_discount as td 

		left join table_discount_party AS dp on td.discount_id = dp.discount_id and td.party_type IN(2,3)

		left join table_discount_distributor_mapping AS tddm on td.discount_id = tddm.discount_id and td.party_type = 5 

		left join table_discount_item AS tdi on td.discount_id = tdi.discount_id and td.item_type IN(2,3,4)

		 where td.slabwise_scheme='1' and td.party_type != '4' and td.is_dis_discount = '1' and td.start_date <= CURDATE() and td.end_date >= CURDATE() and td.status = 'A' group by td.discount_id";
		$result_sql_discount = mysql_query($sql_discount);
		if(mysql_num_rows($result_sql_discount)>0)
		{
			$array_sql_discount =array();
			$i=0;
			while($fetch=mysql_fetch_assoc($result_sql_discount))
			{
				$array_sql_discount[$i] = $fetch;
				$i++;
			}

		}		
		$array_sql_quantity = array();
		$schemes_dt_array = array();		

		/******get all distributors with there applied schems on items wise with quantity**********/
		foreach ($array_sql_discount as $key => $value)
		{	
			$stat_Cond = " ";
			$city_Cond = " ";
			$Dist_Cond = " ";
			$cate_Cond = " ";
			$item_Cond = " ";
			$brnd_Cond = " ";
			$groupByCondition = " ";

			//check party type of the schemes
			if($value['party_type'] == 1){
				$stat_Cond = " ";
				$city_Cond = " ";
				$Dist_Cond = " ";								

			}elseif($value['party_type'] == 2 && $value['state_id'] != '' && $value['state_id'] != 0){  //for party type state

				$stat_Cond = " and d.state IN(".$value['state_id'].") ";
			}elseif($value['party_type'] == 3 && $value['city_id'] != '' && $value['city_id'] != 0){   //for party type city

				$city_Cond = " and d.city IN(".$value['city_id'].") ";
			}elseif($value['party_type'] == 5 && $value['distributor_id'] != '' && $value['distributor_id'] != 0){  //for party type distributor

				$Dist_Cond = " and d.distributor_id IN(".$value['distributor_id'].") ";
			}

			//check item type of the schemes
			if($value['item_type'] == 1){				
				$cate_Cond = " ";
				$item_Cond = " ";
				$brnd_Cond = " ";	
				$groupByCondition = " ";			

			}elseif($value['item_type'] == 2 && $value['category_id'] > 0){  //for type category

				$cate_Cond = " and i.category_id IN(".$value['category_id'].") ";
				$groupByCondition = " , i.category_id";

			}elseif($value['item_type'] == 3 && $value['item_id'] > 0){   //for type item

				$item_Cond = " and i.item_id IN(".$value['item_id'].") ";
				$groupByCondition = " , i.item_id";

			}elseif($value['item_type'] == 4 && $value['brand_id'] > 0){  //for type brand

				$brnd_Cond = " and i.brand_id IN(".$value['brand_id'].") ";
				$groupByCondition = " , i.brand_id";
			}


												

			 $sql_order_quantity = "select o.distributor_id,d.distributor_name,od.item_id,group_concat(distinct(od.order_id)) as order_id,SUM(odd.quantity) as quantity,SUM(odd.acc_total) as acc_total,od.discount_id,i.item_name  
			    from table_order as o

			   	inner join table_order_detail as od on od.order_id = o.order_id and od.type = '2' and od.discount_id = '".$value['discount_id']."' 

				inner join table_order_detail as odd on odd.order_id = od.order_id and odd.type = '1' and od.item_id = odd.item_id

              	left join table_distributors as d on o.distributor_id = d.distributor_id
              	left join table_item as i on i.item_id = odd.item_id 

			  	where $sale_Cond  and o.date_of_order between '".$value['start_date']."' and '".$value['end_date']."' 
			  	$stat_Cond $city_Cond $Dist_Cond $cate_Cond $item_Cond $brnd_Cond 
			  	GROUP BY o.distributor_id $groupByCondition 
			  	ORDER BY `od`.`order_id` DESC";	  		  	

			  	
			$result_sql_quantity = mysql_query($sql_order_quantity);
			if(mysql_num_rows($result_sql_quantity)>0)
			{				
				
				while($fetch_quantity = mysql_fetch_object($result_sql_quantity))
				{
					// echo "<pre>";
					// print_r($fetch_quantity);


					/*if(array_key_exists($fetch_quantity->item_id, $array_sql_quantity[$fetch_quantity->distributor_id])){					

						$array_sql_quantity[$fetch_quantity->distributor_id][$fetch_quantity->item_id]->distributor_id = $fetch_quantity->distributor_id;

						$array_sql_quantity[$fetch_quantity->distributor_id][$fetch_quantity->item_id]->distributor_name = $fetch_quantity->distributor_name;

						$array_sql_quantity[$fetch_quantity->distributor_id][$fetch_quantity->item_id]->item_id = $fetch_quantity->item_id;

						$array_sql_quantity[$fetch_quantity->distributor_id][$fetch_quantity->item_id]->order_id .=  ",".$fetch_quantity->order_id;	

						$array_sql_quantity[$fetch_quantity->distributor_id][$fetch_quantity->item_id]->quantity += $fetch_quantity->quantity;

						$array_sql_quantity[$fetch_quantity->distributor_id][$fetch_quantity->item_id]->acc_total += $fetch_quantity->acc_total;

						$array_sql_quantity[$fetch_quantity->distributor_id][$fetch_quantity->item_id]->discount_id .=  ",".$fetch_quantity->discount_id;

						$array_sql_quantity[$fetch_quantity->distributor_id][$fetch_quantity->item_id]->item_name = $fetch_quantity->item_name;



					}else{*/
						$fetch_quantity->item_type = $value['item_type'];
						$array_sql_quantity[$fetch_quantity->distributor_id][$fetch_quantity->discount_id][] = $fetch_quantity;
					//}	
				}
			}

			/******get all schemes details ********************************************************/
			$scheme_query = "select dd.*,i.item_name as free_itme_name,focd.free_qty,d.start_date,d.end_date,d.item_type,tc.category_id as scm_catId,tc.category_name as scm_catName,ti.item_id as scm_itmId,ti.item_name as scm_itmName,tb.brand_id as scm_brnId,tb.brand_name as scm_brnName 
			from table_discount_detail as dd 

			 left join table_discount as d on d.discount_id = dd.discount_id 

			 left join table_discount_item AS tdi on d.discount_id = tdi.discount_id and d.item_type IN(2,3,4) 

			 left join table_category as tc on tc.category_id = tdi.category_id 
			 left join table_item as ti on ti.item_id = tdi.item_id 
			 left join table_brands as tb on tb.brand_id = tdi.brand_id

			 left join table_foc_detail as focd on dd.foc_id = focd.foc_id and dd.foc_id > 0 

			 left join table_item as i on focd.free_item_id = i.item_id 

			  where dd.discount_id ='".$value['discount_id']."'  order by dd.discount_type";
			$sql_scheme_resultset = mysql_query($scheme_query);
			if(mysql_num_rows($sql_scheme_resultset)>0)
			{
				while($fetch_records = mysql_fetch_object($sql_scheme_resultset))
				{					
					$schemes_dt_array[$fetch_records->discount_id][] = $fetch_records;
					
				}
			}	
		}

		// echo "<pre>";
		// print_r($array_sql_quantity);	
		// exit;


		// echo "<pre>";
		// print_r($array_sql_quantity);

		// echo "<pre>";
		// print_r($schemes_dt_array);
		// exit;

		
		/******get all schemes details ********************************************************/

		$fullArray = array();
		foreach ($array_sql_quantity as $Dis_id => $Dis_data)
		{		
			foreach ($Dis_data as $discount_id => $discountkeyArray) {

				foreach ($discountkeyArray as $key => $item_with_discount_detail) {
				

						  		foreach ($schemes_dt_array[$discount_id] as $key2 => $discount_slab_detail) {	

						  			$newArr = array_merge((array) $item_with_discount_detail,(array) $discount_slab_detail);

				    				$fullArray[$Dis_id][] = $newArr;
						  			
						  		}
				}
			}
		}
	}

	// echo "<pre>";
 //     print_r($fullArray);
 //    exit;					

	

?>

<?php include("header.inc.php") ?>
<script type="text/javascript">
	function showloader()
	{
		$('#Report').hide();
		$('#loader').show();
	}
</script>
<script>
$(document).ready(function(){
	$('#loader').hide();
	$('#loader').html('<div id="loader" align="center"><img src="images/ajax-loader.gif" /><br/>Please Wait...</div>');
	$('#Report').show();
	
});
</script>
<script type="text/javascript">

    function PrintElem(elem)
    {
        Popup($(elem).html());
    }

    function Popup(data) 
    {
		
        var mywindow = window.open('', 'Report');
		
        mywindow.document.write('<html><head><title>Activity Report</title>');
		mywindow.document.write('<table><tr><td><b>From Date:</b> <?php echo $from_date; ?></td><td><b>To Date:</b> <?php echo $to_date; ?></td></tr></table>');
        /*optional stylesheet*/ //mywindow.document.write('<link rel="stylesheet" href="main.css" type="text/css" />');
        mywindow.document.write('</head><body >');
        mywindow.document.write(data);
        mywindow.document.write('</body></html>');

        mywindow.print();
        mywindow.close();
        return true;
    }

    function loadDistributorAchievmentbySalesmanID (sale_id) { 

		if(parseInt(sale_id)>0)
			window.location.href = "achievment_report_distributor.php?distributor_id="+sale_id;
		else {
			alert('Please select a distributor!');
			window.location.href = "achievment_report_distributor.php";
		}
	}

$(document).ready(function()
{
<?php if($_POST['submit']=='Export to Excel'){ ?>
//tableToExcel('report_export', 'Activity Report', 'Activity Report.xls');	
<?php } ?>
});	

</script>

<script src="javascripts/dateNextPrev.js" type="text/javascript"></script>
<!-- start content-outer -->
<input name="pagename" type="hidden"  id="pagename" value="admin_order_list.php" />
<div id="content-outer">

<!-- start content -->
<div id="content">
<div id="loader" style="position:absolute; margin-left:40%; margin-top:10%;"></div>
<div id="page-heading"><h1><span style="color: #d74343; font-family: Tahoma; font-weight: bold;">Distributor Schemes Achievement Report</span></h1></div>
<table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">

<tr>
	<td>
	<!--  start content-table-inner -->
	<div id="content-table-inner">
	
	<div id="page-heading" align="left" >
	<form name="frmPre" id="frmPre" method="post" action="" enctype="multipart/form-data" >
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
	
	<tr>
		<td><h3>Select Distributor:</h3><h6> 
		
		 <!-- <select name="sal" id="sal" class="styledselect_form_5 required" style="" 
		 onchange="loadDistributorAchievmentbySalesmanID(this.value);" >
			<?php echo $rtndata = $_objArrayList->GetSalesmenMenu($salsList, $_REQUEST['salesman_id']);?>
		</select> -->

		<select name="sal" id="sal" class="styledselect_form_5 required" style="" 
		 onchange="loadDistributorAchievmentbySalesmanID(this.value);" >
			<?php $dquery = mysql_query("SELECT distributor_id,distributor_name FROM table_distributors WHERE status ='A'");?>


			<option value="">Select</option>
			<?php while ($data = mysql_fetch_object($dquery)) { ?>

					<option value="<?php echo $data->distributor_id; ?>" <?php if($data->distributor_id == $_REQUEST['distributor_id']){?> selected <?php } ?>><?php echo $data->distributor_name; ?></option>
				
			<?php }

			?>
		</select>
		</h6></td>	
		
		<!-- <td><h3></h3>
		<input name="submit" class="result-submit" type="submit" id="submit" value="View Details" onclick="showloader()";/>		 		<input type="button" value="Reset!" class="form-reset" onclick="location.href='achievment_report_distributor.php?reset=yes';" />
		</td>
		<td colspan="2"></td>
		</tr>
		<tr>
		<td colspan="6"><input name="showReport" type="hidden" value="yes" />
		
		<input type="button" value="Print" class="result-submit" onclick="PrintElem('#Report')" />
		<a id="dlink"  style="display:none;"></a>
		<input type="submit" name="submit" value="Export to Excel" class="result-submit"  ></td> -->
	</tr>
	</table>
	</form>
	</div>
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
	<tr valign="top">
		<td>
		<div id="Report">
		<table  border="0" width="100%" cellpadding="0" cellspacing="0" id="report_export" name="report_export" style="overflow: scroll;" >
			<tr  bgcolor="#6E6E6E" style="color: #fff;font-weight: bold;" >							
				<!-- <td style="padding:10px;" width="5%">Distributor Name</td> -->
				<td style="padding:10px;" width="20%">Scheme Description(Min. off take)</td>				
				<td style="padding:10px;" width="15%">Valid upto</td>
				<td style="padding:10px;" width="15%">Scheme On</td>
				<td style="padding:10px;" width="15%">Scheme to be applied</td>	
				<td style="padding:10px;" width="15%">Scheme</td>			
							
			</tr>
			<?php if(is_array($fullArray) && sizeof($fullArray) > 0){
			 		$achieved = array();
				foreach($fullArray as $dataset){
						for($i=0;$i<count($dataset);$i++){

						if($dataset[$i]['quantity'] < $dataset[$i]['minimum_quantity']){ 
							$achieved[] = $i; ?>
					<tr  style="border-bottom:2px solid #6E6E6E;" <?php 
					//if($dataset[$i]['quantity'] >= $dataset[$i]['minimum_quantity'] && $dataset[$i]['quantity'] <= $dataset[$i]['maximum_quantity']){ echo "bgcolor='green;'"; }elseif($dataset[$i]['quantity'] > $dataset[$i]['maximum_quantity']){ echo "bgcolor='green;'"; } ?> >
						<!-- <td style="padding:10px;" width="5%"><?php echo $dataset[$i]['distributor_name'] ?></td> -->
						<td style="padding:10px;word-wrap: inherit;line-height: 20px;">
						<?php echo $dataset[$i]['discount_desc'];?> (<?php echo $dataset[$i]['minimum_quantity'];?> Nos. To <?php echo $dataset[$i]['maximum_quantity'];?> Nos.)</td>						
						<td style="padding:10px;" width="1%"><?php echo $dataset[$i]['end_date'] ?></td>						
						<?php 
						$schemOn = '';
						if($dataset[$i]['item_type']== 1){
							$schemOn = "All Items";
							$achievedDesc = " of any item ";
						}elseif($dataset[$i]['item_type']== 2){
							//$schemOn = "Any Item From ".$dataset[$i]['scm_catName'];
							$schemOn = "<b>Category</b> (".$dataset[$i]['scm_catName'].")";
							$achievedDesc = " of any item from ".$dataset[$i]['scm_catName']." category ";
						}elseif($dataset[$i]['item_type']== 3){
							//$schemOn = "Buy ".$dataset[$i]['scm_itmName'];
							$schemOn = "<b>Item</b> (".$dataset[$i]['scm_itmName'].")";
							$achievedDesc = " of ".$dataset[$i]['scm_itmName']."";
						}elseif($dataset[$i]['item_type']== 4){
							//$schemOn = "Buy Any Item From ".$dataset[$i]['scm_brnName'];
							$schemOn = "Brand (".$dataset[$i]['scm_brnName'].")";
							$achievedDesc = " of any item from ".$dataset[$i]['scm_brnName']." brand ";
						} ?>
						<td style="padding:10px;word-wrap: inherit;line-height: 26px;" width="5%"><?php //echo $dataset[$i]['item_id']; ?><?php echo $schemOn; ?></td>

						<?php 
						 $less_quantity = $dataset[$i]['minimum_quantity']-$dataset[$i]['quantity'];

						 $less_quantity = "Buy ".$less_quantity." more quantity $achievedDesc to get this scheme";
						 ?>
						<td style="padding:10px;word-wrap: inherit;line-height: 26px;" width="5%"><?php echo $less_quantity; ?></td>

						<?php 
						if($dataset[$i]['discount_type'] == 1){
						 	$scheme = "You will get ".$dataset[$i]['discount_percentage']."% Off"; 
						}
						elseif($dataset[$i]['discount_type'] == 2){
							$scheme = "You will get ".$dataset[$i]['discount_amount']." Rs. Off";
						}
						elseif($dataset[$i]['discount_type'] == 3){
						 	$scheme = "You will get free ".$dataset[$i]['free_qty']." Quantity of ".$dataset[$i]['free_itme_name'];
						}
						elseif($dataset[$i]['discount_type'] == 4){
						 	$scheme = "You will get Free Trip of ".$dataset[$i]['discount_trip_pkg'];
						}
						?>


						<td style="padding:10px;word-wrap: inherit;line-height: 26px;" width="5%"><?php echo $scheme; ?></td>				
												
					</tr>

			<?php } } }
				if(count($achieved) == 0){ ?>
					<tr  style="border-bottom:2px solid #6E6E6E;" align="center" >
						<td style="padding:10px;word-wrap: inherit;line-height: 30px;" colspan="10">All Schemes has been Achieved by the distributor</td>
					</tr>
				<?php }
			}elseif(empty($_REQUEST['distributor_id'])){ ?>
				
			<tr  style="border-bottom:2px solid #6E6E6E;" align="center" >
				<td style="padding:10px;word-wrap: inherit;line-height: 30px;" colspan="10">Select Distributor</td>
			</tr>

			<?php }else{?>

				<tr  style="border-bottom:2px solid #6E6E6E;" align="center" >
				<td style="padding:10px;word-wrap: inherit;line-height: 30px;" colspan="10">Report Not Available</td>
			</tr>

			<?php }?>

			
		</table>
		</div>
		</td>
	</tr>
	<tr>
		<td><img src="images/blank.gif" width="695" height="1" alt="blank" /></td>
		<td></td>
	</tr>
	</table>
<div class="clear"></div>
</div>
<!--  end content-table-inner  -->
</td>
<td id="tbl-border-right"></td>
</tr>
<!--<tr>
	<th class="sized bottomleft"></th>
	<td id="tbl-border-bottom">&nbsp;</td>
	<th class="sized bottomright"></th>
</tr>-->
</table>
<div class="clear">&nbsp;</div>
</div>
<!--  end content -->
<div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->
<div class="clear">&nbsp;</div> 
<!-- start footer -->         
<?php include("footer.php") ?>
<?php unset($_SESSION['SalAttList']); ?>
<!-- end footer -->
<script src="javascripts/jquery-1.8.2.js" type="text/javascript"></script>
<script src="javascripts/jquery-ui.js"></script>
<script type="text/javascript" src="javascripts/validate.js"></script>
<script type="text/javascript">
// Popup window code
function newPopup(url) {
	popupWindow = window.open(
		url,'popUpWindow','height=500,width=600,left=200,top=100,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no,status=yes')
}
</script>
</body>
</html>