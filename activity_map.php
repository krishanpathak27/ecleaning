<?php
include("includes/config.inc.php");
include("includes/function.php");
$_objAdmin = new Admin();

if($_REQUEST['id']!=""){
$auMarker=$_objAdmin->_getSelectList2('table_activity',"lat,lng,network_mode,accuracy_level",''," activity_id='".base64_decode($_REQUEST['id'])."' ");
}
?>
<html>
<head>
<meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=9" >
<meta http-equiv="X-UA-Compatible" content="IE=8" />
<meta http-equiv="X-UA-Compatible" content="IE=7" />
<meta http-equiv="X-UA-Compatible" content="IE=6" />
<style type="text/css">
  html { height: 100% }
  body { height: 100%; margin: 0; padding: 0 }
  #map_canvas { height: 100% }
</style>
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
<script type="text/javascript">
function initialize() 
{
    var myOptions = 
	{  center: new google.maps.LatLng(<?php echo $auMarker[0]->lat.",".$auMarker[0]->lng; ?>), zoom: 18, mapTypeId: google.maps.MapTypeId.ROADMAP };
	var map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);

				var myLatlng = new google.maps.LatLng(<?php echo $auMarker[0]->lat.",".$auMarker[0]->lng; ?>)
				var marker = new google.maps.Marker
				({ position: myLatlng,title:"<?php echo "Location mode: ".$auMarker[0]->network_mode.", Accuracy: ".$auMarker[0]->accuracy_level ?>"});//, animation: google.maps.Animation.DROP});
				marker.setMap(map);
	
}
</script>
 </head>
 <body onLoad="initialize();">
    <div id="map_canvas" ></div>
  </body>
</html>