<?php


include("includes/config.inc.php");
include("includes/function.php");
include("includes/globalarraylist.php");


$page_name="Activity Report";
if(isset($_POST['showReport']) && $_POST['showReport'] == 'yes')
{	
	if($_REQUEST['sal']!="") 
	{
	$sal_list=$_REQUEST['sal'];
	$salesman=" AND s.salesman_id='".$sal_list."' ";
	}
	if($_POST['from']!="") 
	{
	$from_date=$_objAdmin->_changeDate($_POST['from']);	
	}
	if($_POST['to']!="") 
	{
	$to_date=$_objAdmin->_changeDate($_POST['to']);	
	}

} else {
$from_date= $_objAdmin->_changeDate(date("Y-m-d"));
$to_date= $_objAdmin->_changeDate(date("Y-m-d"));
unset($_SESSION['SalAct']);		
}
if(isset($_REQUEST['reset']) && $_REQUEST['reset'] == 'yes')
{
	header("Location: activity_report.php");
}
if($_REQUEST['sal']!=''){
$SalName=$_objAdmin->_getSelectList('table_salesman','salesman_name',''," salesman_id='".$_REQUEST['sal']."'"); 
$sal_name=$SalName[0]->salesman_name;
} else {
$sal_name="All Salesman";
}

$order_by_sal="asc";
$List= "ORDER BY s.salesman_name asc, a.activity_date";
//$List= "ORDER BY o.date_of_order,s.salesman_name asc ";

if(isset($_POST['order_date']) && $_POST['order_date'] == 'yes')
{
	$List= "ORDER BY a.activity_date asc,s.salesman_name asc ";
	$order_by_date="asc";
	$order_by_sal="desc";
	if($_REQUEST['sal']!="") 
	{
		if($_REQUEST['order_by_dt']=='asc')
		{
		$List= "ORDER BY a.activity_date,s.salesman_name asc ";
		$order_by_date="desc";
		} else {
		$List= "ORDER BY a.activity_date desc,s.salesman_name asc ";
		$order_by_date="asc";
		}
	}
}

if(isset($_POST['order_sal']) && $_POST['order_sal'] == 'yes')
{
	$List= "ORDER BY s.salesman_name asc, a.activity_date";	
	$order_by_sal="asc";
	$order_by_date="desc";
}
?>

<?php include("header.inc.php") ?>
<script type="text/javascript">
	function showloader()
	{
		$('#Report').hide();
		$('#loader').show();
	}
</script>
<script>
$(document).ready(function(){
	$('#loader').hide();
	$('#loader').html('<div id="loader" align="center"><img src="images/ajax-loader.gif" /><br/>Please Wait...</div>');
	$('#Report').show();
});
</script>
<script type="text/javascript">

    function PrintElem(elem)
    {
        Popup($(elem).html());
    }

    function Popup(data) 
    {
		
        var mywindow = window.open('', 'Report');
		
        mywindow.document.write('<html><head><title>Activity Report</title>');
		mywindow.document.write('<table><tr><td><b>From Date:</b> <?php echo $from_date; ?></td><td><b>To Date:</b> <?php echo $to_date; ?></td></tr></table>');
        /*optional stylesheet*/ //mywindow.document.write('<link rel="stylesheet" href="main.css" type="text/css" />');
        mywindow.document.write('</head><body >');
        mywindow.document.write(data);
        mywindow.document.write('</body></html>');

        mywindow.print();
        mywindow.close();
        return true;
    }
$(document).ready(function()
{
<?php if($_POST['submit']=='Export to Excel'){ ?>
tableToExcel('report_export', 'Activity Report', 'Activity Report.xls');	
<?php } ?>
});	

</script>

<script src="javascripts/dateNextPrev.js" type="text/javascript"></script>
<!-- start content-outer -->
<input name="pagename" type="hidden"  id="pagename" value="admin_order_list.php" />
<div id="content-outer">

<!-- start content -->
<div id="content">
<div id="loader" style="position:absolute; margin-left:40%; margin-top:10%;"></div>
<div id="page-heading"><h1><span style="color: #d74343; font-family: Tahoma; font-weight: bold;">Activity Report</span></h1></div>
<table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">

<tr>
	<td>
	<!--  start content-table-inner -->
	<div id="content-table-inner">
	
	<div id="page-heading" align="left" >
	<form name="frmPre" id="frmPre" method="post" action="" enctype="multipart/form-data" >
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
	
	<tr>
		<td ><h3>Salesman Name:</h3><h6> 
		
		 <select name="sal" id="sal" class="styledselect_form_5" style="" >
			<?php echo $rtndata = $_objArrayList->GetSalesmenMenu($salsList, $_REQUEST['sal']);?>
		</select>
		</h6></td>

		
		<td><h3>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;From Date: </h3><h6><img src="css/images/prev.png" height="18" width="18" onclick="dateFromPrev();"> <input type="text" id="from" name="from" class="date" style="width:150px" value="<?php  echo $from_date;?>"  readonly /> <img src="css/images/next.png" height="18" width="18" onclick="dateFromNext();"></h6></td>
		<td><h3>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;To Date: </h3> <h6><img src="css/images/prev.png" height="18" width="18" onclick="dateToPrev();"> <input type="text" id="to" name="to" class="date" style="width:150px" value="<?php echo $to_date; ?>"  readonly /><img src="css/images/next.png" height="18" width="18" onclick="dateToNext();"></h6></td>
		<td><h3></h3>
		<input name="submit" class="result-submit" type="submit" id="submit" value="View Details" onclick="showloader()";/>		 		<input type="button" value="Reset!" class="form-reset" onclick="location.href='activity_report.php?reset=yes';" />
		</td>
		<td colspan="2"></td>
		</tr>
		<tr>
		<td colspan="6"><input name="showReport" type="hidden" value="yes" />
		
		<input type="button" value="Print" class="result-submit" onclick="PrintElem('#Report')" />
		<a id="dlink"  style="display:none;"></a>
		<input type="submit" name="submit" value="Export to Excel" class="result-submit"  ></td>
	</tr>
	</table>
	</form>
	</div>
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
	<tr valign="top">
		<td>
		<div id="Report">
		<table  border="0" width="100%" cellpadding="0" cellspacing="0" id="report_export" name="report_export" >
			<tr  bgcolor="#6E6E6E" style="color: #fff;font-weight: bold;" >
				<!--<td style="padding:10px;" width="10%">Salesman Name</td>-->
				<form name="submitAmt" method="post" action="">
				<input name="showReport" type="hidden" value="yes" />
				<input type="hidden" name="sal" value="<?php echo $sal_list;?>">
				<input type="hidden" name="from" value="<?php echo $from_date;?>">
				<input type="hidden" name="to" value="<?php echo $to_date;?>">
				<input type="hidden" name="order_sal" value="yes">
				<td style="padding:10px;" width="10%"><div style="width: 120px;" align="center">Salesman Name &nbsp;&nbsp;<a href="javascript:document.submitAmt.submit()">
				<?php if($order_by_sal=='asc'){ ?>
				<img src="images/arrow-up.png" width="13" height="13" />
				<?php } else { ?>
				<img src="images/arrow-down.png" width="13" height="13" />
				<?php } ?>
				</a></div></td>
				<td style="padding:10px;" width="5%">Salesman Code</td>
				</form>
				<!--<td style="padding:10px;" width="5%">Date</td>-->
				<form name="submitQty" method="POST" action="">
				<input type="hidden" name="order_date" value="yes">
				<input name="showReport" type="hidden" value="yes" />
				<input type="hidden" name="order_by_dt" value="<?php echo $order_by_date; ?>">
				<input type="hidden" name="sal" value="<?php echo $sal_list;?>">
				<input type="hidden" name="from" value="<?php echo $from_date;?>">
				<input type="hidden" name="to" value="<?php echo $to_date;?>">
				<td style="padding:10px;" width="10%"><div style="width: 90px;" align="center">Date &nbsp;&nbsp;<a href="javascript:document.submitQty.submit()">
				<?php if($order_by_date=='asc'){ ?>
				<img src="images/arrow-up.png" width="13" height="13" />
				<?php } else { ?>
				<img src="images/arrow-down.png" width="13" height="13" />
				<?php } ?>
				</a></div></td>
				</form>
				<td style="padding:10px;" width="5%">Day</td>
				<td style="padding:10px;" width="20%">Market</td>
				<td style="padding:10px;" align="center" width="5%">Total Calls</td>
				<td style="padding:10px;" align="center" width="15%">Total Productive Calls</td>
				<td style="padding:10px;" align="center" width="15%">Total No. of New Dealer</td>
				<td style="padding:10px;" align="center" width="15%">Total No. of New Distributor</td>
				<td style="padding:10px;" width="15%">Total Amount of Order</td>
				<td style="padding:10px;" width="15%">Total Quantity</td>
				<td style="padding:10px;" width="15%">Collection Amount</td>
				<td style="padding:10px;" width="15%">Activity</td>
				<td style="padding:10px;" width="15%">Order Type</td>
				<td style="padding:10px;" width="15%">Reason</td>
			</tr>
			<?php
			/*$auRet=$_objAdmin->_getSelectList2('table_activity as a

				left join table_salesman as s on a.salesman_id=s.salesman_id

				left join table_retailer as r on a.ref_id=r.retailer_id and a.activity_type="5"

				left join table_distributors as d on a.ref_id=d.distributor_id and a.activity_type="24"

			    left join table_order as o on a.ref_id=o.order_id and a.salesman_id=o.salesman_id and a.activity_type="3"

			    left join table_retailer as r1 on o.retailer_id=r1.retailer_id AND a.ref_id=o.order_id and a.activity_type="3"

			    left join table_order as o1 on a.ref_id=o1.order_id and a.salesman_id=o1.salesman_id and a.activity_type="3" and o1.order_type != "No"
			    
			    ',"a.activity_type,s.salesman_name,s.salesman_code,a.activity_date,a.salesman_id,count(o.order_id) as total_call, sum(o.total_invoice_amount) as total_amt,count(o1.order_id) as productive_call,count(r.retailer_id) as total_retailer,count(d.distributor_id) as total_distributor,GROUP_CONCAT(DISTINCT(r1.retailer_location)) as retailer_location",''," (a.activity_date BETWEEN '".date('Y-m-d', strtotime($from_date))."' AND '".date('Y-m-d', strtotime($to_date))."') $salesman and activity_type IN (3,5,24) Group by a.activity_date,a.salesman_id $List");*/

			$auRet=$_objAdmin->_getSelectList2('table_activity as a

				left join table_salesman as s on a.salesman_id=s.salesman_id

				left join table_retailer as r on a.ref_id=r.retailer_id and a.activity_type="5"

				left join table_distributors as d on a.ref_id=d.distributor_id and a.activity_type="24"

			    left join table_order as o on a.ref_id=o.order_id and a.salesman_id=o.salesman_id and a.activity_type="3"

			    left join table_retailer as r1 on o.retailer_id=r1.retailer_id AND a.ref_id=o.order_id and a.activity_type="3"

			    left join table_order as o1 on a.ref_id=o1.order_id and a.salesman_id=o1.salesman_id and a.activity_type="3" and o1.order_type = "No"
			    
			    left join table_order_detail as od on od.order_id= o.order_id

			    left join table_transaction_details as td on a.ref_id=td.transaction_id and a.salesman_id=td.salesman_id and a.activity_type="35"

			    ',"a.activity_type, a.activity, s.salesman_name,s.salesman_code,a.activity_date,a.salesman_id,count(o.order_id) as total_call, sum(o.total_invoice_amount) as total_amt, sum(od.quantity) AS total_qty, count(o1.order_id) as productive_call,count(r.retailer_id) as total_retailer,count(d.distributor_id) as total_distributor,GROUP_CONCAT(DISTINCT(r1.retailer_location)) as retailer_location, sum(td.total_sale_amount) as total_sale_amount, o.order_type, a.activity, o.tag_description",''," (a.activity_date BETWEEN '".date('Y-m-d', strtotime($from_date))."' AND '".date('Y-m-d', strtotime($to_date))."') $salesman and activity_type IN (3,5,24,35) Group by a.activity_date,a.salesman_id $List");


			//$from_date
			//$to_date
			//$auRet=$_objAdmin->_getSelectList('table_order as o left join table_salesman as s on s.salesman_id=o.salesman_id',"o.date_of_order,o.salesman_id,s.salesman_name",''," (o.date_of_order BETWEEN '".date('Y-m-d', strtotime($from_date))."' AND '".date('Y-m-d', strtotime($to_date))."') $salesman Group by o.date_of_order,o.salesman_id $List ");
			
			if(is_array($auRet)){
				for($i=0;$i<count($auRet);$i++)
				{
				 /*$td1=$_objAdmin->_getSelectList('table_order as o left join table_retailer as r on o.retailer_id=r.retailer_id','count(order_id) as total_call, sum( o.total_invoice_amount ) as total_amt ',''," o.date_of_order='".$auRet[$i]->activity_date."' and r.new='' and o.salesman_id='".$auRet[$i]->salesman_id."'");*/ 

				/*$td2=$_objAdmin->_getSelectList('table_order as o left join table_retailer as r on o.retailer_id=r.retailer_id','count(order_id) as productive_call',''," o.date_of_order='".$auRet[$i]->activity_date."' and o.order_type != 'No' and r.new='' and o.salesman_id='".$auRet[$i]->salesman_id."'");
				*/
				//$td3=$_objAdmin->_getSelectList('table_retailer','count(retailer_id) as total_retailer',''," start_date='".$auRet[$i]->date_of_order."' and salesman_id='".$auRet[$i]->salesman_id."'");
				/*$td3=$_objAdmin->_getSelectList2('table_activity','count(activity_id) as total_retailer',''," activity_date='".$auRet[$i]->activity_date."' and salesman_id='".$auRet[$i]->salesman_id."' and activity_type='5'");*/

				/****
				* Modified On : 22 feb 2016
				* Abhishek Jaiswal
				* Desc : Add new field "Total no of new distributor."
				*****/

				/*$td5=$_objAdmin->_getSelectList2('table_activity','count(activity_id) as total_distributor',''," activity_date='".$auRet[$i]->activity_date."' and salesman_id='".$auRet[$i]->salesman_id."' and activity_type='24'");*/
				
				/*$td4=$_objAdmin->_getSelectList('table_retailer','DISTINCT(retailer_location)',''," retailer_id in(Select DISTINCT(retailer_id) from table_order where salesman_id='".$auRet[$i]->salesman_id."' and date_of_order='".$auRet[$i]->activity_date."') Order by retailer_location asc");*/
				/*for($a=0;$a<count($td4);$a++)
				{
				$market.=$comma.$td4[$a]->retailer_location;
				$comma=", ";
				}*/
				?>
				<tr  style="border-bottom:2px solid #6E6E6E;" >
					<td style="padding:10px;" width="10%"><?php echo $auRet[$i]->salesman_name;?></td>
					<td style="padding:10px;" width="10%"><?php echo $auRet[$i]->salesman_code;?></td>
					<td style="padding:10px;" width="10%"><?php echo $_objAdmin->_changeDate($auRet[$i]->activity_date);?></td>
					<td style="padding:10px;" width="5%"><?php echo date("l",strtotime($auRet[$i]->activity_date));?></td>
					<td style="padding:10px;" width="20%"><?php echo strtoupper($auRet[$i]->retailer_location); ?></td>
					<td style="padding:10px;" align="center" width="10%"><?php echo $auRet[$i]->total_call; ?></td>
					<td style="padding:10px;" align="center" width="15%"><?php echo $auRet[$i]->productive_call; ?></td>
					<td style="padding:10px;" align="center" width="15%"><?php echo $auRet[$i]->total_retailer; ?></td>
					<td style="padding:10px;" align="center" width="15%"><?php echo $auRet[$i]->total_distributor; ?></td>
					<td style="padding:10px; padding-right:50px;" align="right" width="15%"><?php echo $auRet[$i]->total_amt; ?></td>
					<td style="padding:10px; padding-right:50px;" align="right" width="15%"><?php echo $auRet[$i]->total_qty; ?></td>
					<td style="padding:10px; padding-right:50px;" align="right" width="15%"><?php echo $auRet[$i]->total_sale_amount; ?></td>
					<td style="padding:10px; padding-right:50px;" align="right" width="15%"><?php echo $auRet[$i]->activity; ?></td>
					<td style="padding:10px; padding-right:50px;" align="right" width="15%"><?php echo $auRet[$i]->order_type; ?></td>
					<td style="padding:10px; padding-right:50px;" align="right" width="15%"><?php echo $auRet[$i]->tag_description; ?></td>

				</tr>
				<?php
				//unset($market);
				//unset($comma);
				}
			} else {
			?>
			<tr  style="border-bottom:2px solid #6E6E6E;" align="center" >
				<td style="padding:10px;" colspan="10">Report Not Available</td>
			</tr>
			<?php
			}
			?>
		</table>
		</div>
		</td>
	</tr>
	<tr>
		<td><img src="images/blank.gif" width="695" height="1" alt="blank" /></td>
		<td></td>
	</tr>
	</table>
<div class="clear"></div>
</div>
<!--  end content-table-inner  -->
</td>
<td id="tbl-border-right"></td>
</tr>
<!--<tr>
	<th class="sized bottomleft"></th>
	<td id="tbl-border-bottom">&nbsp;</td>
	<th class="sized bottomright"></th>
</tr>-->
</table>
<div class="clear">&nbsp;</div>
</div>
<!--  end content -->
<div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->
<div class="clear">&nbsp;</div> 
<!-- start footer -->         
<?php include("footer.php") ?>
<?php unset($_SESSION['SalAttList']); ?>
<!-- end footer -->
<script src="javascripts/jquery-1.8.2.js" type="text/javascript"></script>
<script src="javascripts/jquery-ui.js"></script>
<script type="text/javascript" src="javascripts/validate.js"></script>
<script type="text/javascript">
// Popup window code
function newPopup(url) {
	popupWindow = window.open(
		url,'popUpWindow','height=500,width=600,left=200,top=100,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no,status=yes')
}
</script>
<script>
    $(function() {
        $( "#from" ).datepicker({
			dateFormat: "d M yy",
            defaultDate: "w",
            changeMonth: true,
            numberOfMonths: 1,
            onSelect: function( selectedDate ) {
                $( "#to" ).datepicker( "option", "minDate", selectedDate );
            }
        });
        $( "#to" ).datepicker({
			dateFormat: "d M yy",
            defaultDate: "-w",
            changeMonth: true,
            numberOfMonths: 1,
            onSelect: function( selectedDate ) {
                $( "#from" ).datepicker( "option", "maxDate", selectedDate );
            }
        });
    });
</script>
<script type='text/javascript'>//<![CDATA[ 
var tableToExcel = (function () {
        var uri = 'data:application/vnd.ms-excel;base64,'
        , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table><tr><td><b>From Date:</b> <?php echo $from_date; ?></td><td><b>To Date:</b> <?php echo $to_date; ?></td></tr></table><table>{table}</table></body></html>'
        , base64 = function (s) { return window.btoa(unescape(encodeURIComponent(s))) }
        , format = function (s, c) { return s.replace(/{(\w+)}/g, function (m, p) { return c[p]; }) }
        return function (table, name, filename) {
            if (!table.nodeType) table = document.getElementById(table)
            var ctx = { worksheet: name || 'Worksheet', table: table.innerHTML }

            document.getElementById("dlink").href = uri + base64(format(template, ctx));
            document.getElementById("dlink").download = filename;
            document.getElementById("dlink").click();

        }
    })()

//]]>  
</script>
</body>
</html>