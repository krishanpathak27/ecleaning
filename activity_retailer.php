<?php
include("includes/config.inc.php");
include("includes/function.php");
$_objAdmin = new Admin();

$auRec=$_objAdmin->_getSelectList('table_retailer as r left join table_salesman as sal on sal.salesman_id=r.salesman_id left join city as c on c.city_id=r.city left join state as s on s.state_id=r.state left join table_markets as m on m.market_id=r.market_id',"r.*,sal.salesman_name,c.city_name,s.state_name,m.market_name",''," r.retailer_id='".base64_decode($_REQUEST['retId'])."' ");

?>

<?php include("header.inc.php") ?>
<style>
div.img
  {
  margin:2px;
  border:1px solid  #43A643;
  height:auto;
  width:auto;
  float:left;
  text-align:center;
  }
div.img img
  {
  display:inline;
  margin:3px;
  border:1px solid #fbfcfc;
  }
div.img a:hover img
  {
  border:1px solid #fbfcfc;
  }
div.desc
  {
  text-align:center;
  font-weight:normal;
  width:120px;
  margin:2px;
  }
</style>
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
<script type="text/javascript">
function initialize() 
{
	$('#map_canvas').show();
    var myOptions = 
	{  center: new google.maps.LatLng(<?php echo $auRec[0]->lat.",".$auRec[0]->lng; ?>), zoom: 15, mapTypeId: google.maps.MapTypeId.ROADMAP };
	var map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);

				var myLatlng = new google.maps.LatLng(<?php echo $auRec[0]->lat.",".$auRec[0]->lng; ?>)
				var marker = new google.maps.Marker
				({ position: myLatlng,title:"<?php echo "Accuracy:" .$auRec[0]->lat_lng_capcure_accuracy ?>"});//, animation: google.maps.Animation.DROP});
				marker.setMap(map);
	
}
</script>

<body onLoad="initialize();">
<!-- start content-outer -->
<div id="content-outer">
<!-- start content -->

<div id="content">
<?php if($sus!=''){?>
	<!--  start message-green -->
	<div id="message-green">
		<table border="0" width="100%" cellpadding="0" cellspacing="0">
		<tr>
			<td class="green-left"><?php echo $sus; ?></td>
			<td class="green-right"><a class="close-green"><img src="images/icon_close_green.gif"   alt="" /></a></td>
		</tr>
		</table>
	</div>
	<?php } ?>
<div id="page-heading"><h1><span style="color: #d74343; font-family: Tahoma; font-weight: bold;">New Retailer</span></h1></div>
<table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">
<!--<tr>
	<th rowspan="3" class="sized"><img src="images/side_shadowleft.jpg" width="20" height="300" alt="" /></th>
	<th class="topleft"></th>
	<td id="tbl-border-top">&nbsp;</td>
	<th class="topright"></th>
	<th rowspan="3" class="sized"><img src="images/side_shadowright.jpg" width="20" height="300" alt="" /></th>
</tr>
-->
<tr>
	<!--<td id="tbl-border-left"></td>-->
	<td>
	<!--  start content-table-inner -->
	<div id="content-table-inner">
	
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
	<tr valign="top">
	<td>
		<!-- start id-form -->
		<table border="0" cellpadding="0" cellspacing="0"  >
			<tr>
				<th valign="top" style="width: 150px;line-height: 28px;"  align="left">Retailer name:</th>
				<td align="left" style="min-width: 150px;"><?php echo $auRec[0]->retailer_name; ?></td>
				<td rowspan="10" align="right" valign="top"><div id="map_canvas"  style="display:none; border:1px solid; width:600px;  height: 300px;"></div></td>
			</tr>
			<tr>
				<th valign="top" style="width: 150px;line-height: 28px;"  align="left">Added By:</th>
				<td align="left" style="min-width: 150px;"><?php echo $auRec[0]->salesman_name; ?></td>
			</tr>
			<tr>
				<th valign="top" style="width: 150px;line-height: 28px;"  align="left">Added Date:</th>
				<td align="left" style="min-width: 150px;"><?php echo $auRec[0]->lat_lng_capcure_date; ?></td>
			</tr>
			<tr>
				<th valign="top" style="width: 150px;line-height: 28px;"  align="left">Phone Number:</th>
				<td align="left" valign="top"><?php echo $auRec[0]->retailer_phone_no; ?></td>
			</tr>
			<tr>
				<th valign="top" style="width: 150px;line-height: 28px;"  align="left">Contact Person:</th>
				<td align="left" valign="top"><?php echo $auRec[0]->contact_person; ?></td>
			</tr>
			<tr>
				<th valign="top" style="width: 150px;line-height: 28px;"  align="left">Contact Mobile No:</th>
				<td align="left" valign="top"><?php echo $auRec[0]->contact_number; ?></td>
			</tr>
			<tr>
				<th valign="top" style="width: 150px;line-height: 28px;"  align="left">Retailer Address:</th>
				<td align="left"><?php echo $auRec[0]->retailer_address; ?></td>
			</tr>
			<tr>
				<th valign="top" style="width: 150px;line-height: 28px;"  align="left">Retailer Market:</th>
				<td align="left"><?php echo $auRec[0]->market_name; ?></td>
			</tr>
			<tr>
				<th valign="top" style="width: 150px;line-height: 28px;"  align="left">State:</th>
				<td align="left"><?php echo $auRec[0]->state_name; ?></td>
			</tr>
			<tr>
				<th valign="top" style="width: 150px;line-height: 28px;"  align="left">City:</th>
				<td align="left" valign="top"><?php echo $auRec[0]->city_name; ?></td>
			</tr>
			<tr>
				<td colspan="2" align="center">
				<input type="button" value="Close" class="form-cen" onclick="javascript:window.close();" />
				</td>
			</tr>
		</table>
	<!-- end id-form  -->

	</td>
	<td>

	<!-- right bar-->
	<?php //include("rightbar/category_bar.php") ?>
</td>
</tr>
<tr>
<td><img src="images/blank.gif" width="695" height="1" alt="blank" /></td>
<td></td>
</tr>
</table>
<div class="clear"></div>
</div>
<!--  end content-table-inner  -->
</td>
<td id="tbl-border-right"></td>
</tr>
<!--<tr>
	<th class="sized bottomleft"></th>
	<td id="tbl-border-bottom">&nbsp;</td>
	<th class="sized bottomright"></th>
</tr>-->
</table>
<div class="clear">&nbsp;</div>
</div>
<!--  end content -->
<div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->
<div class="clear">&nbsp;</div> 
<!-- start footer -->         
<?php include("footer.php") ?>
<!-- end footer -->
 
</body>
</html>