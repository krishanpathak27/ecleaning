
<?php
include("includes/config.inc.php");
include("includes/function.php");
$page_name = "Buildings";
$_objAdmin = new Admin();
$objArrayList = new ArrayList();

if (isset($_POST['add']) && $_POST['add'] == 'yes') {
    if ($_POST['building_id'] != "") {
        $condi = " building_name='" . $_POST['building_name'] . "'  and building_id != '" . $_POST['building_id'] . "'";
    } else {
        $condi = " building_name=" .'"'. $_POST['building_name'] .'"';
    }
    $auRec = $_objAdmin->_getSelectList('table_buildings', "*", '', $condi);
    if (is_array($auRec)) {
        $err = "Building already exists in the system.";
        $auRec[0] = (object) $_POST;
    } else {
        if ($_POST['building_id'] != "") {
            $cid = $_objAdmin->updateBuilding($_POST['building_id']);
            $sus = "Building has been updated successfully.";
        } else {
            $cid = $_objAdmin->addBuilding();
            $sus = "Building has been added successfully.";
        }
	?>
        <script type="text/javascript">
            window.location = "buildings.php?suc=" + '<?php echo $sus; ?>';
        </script>
        <?php    
    }
}


if (isset($_REQUEST['id']) && $_REQUEST['id'] != "") {
    $auRec = $_objAdmin->_getSelectList('table_buildings', "*", '', " building_id=" . $_REQUEST['id']);
    if (count($auRec) <= 0)
        header("Location: buildings.php");
}
?>
<?php
include("header.inc.php");
//$pageAccess=1;
//$check=$objArrayList->checkAccess($pageAccess, basename($_SERVER['PHP_SELF']));
//if($check == false){
//header('Location: ' . $_SERVER['HTTP_REFERER']);
//}
?>
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <!-- BEGIN: Subheader -->
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">
                    Building
                </h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="#" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__separator">
                        -
                    </li>
                    <li class="m-nav__item">
                        <a href="#" class="m-nav__link">
                            <span class="m-nav__link-text">
                                Master
                            </span>
                        </a>
                    </li>
                    <li class="m-nav__separator">
                        -
                    </li>
                    <li class="m-nav__item">
                        <a href="" class="m-nav__link">
                            <span class="m-nav__link-text">
                                Buildings
                            </span>
                        </a>
                    </li>  
                </ul>
            </div> 
        </div>
    </div>
    <!-- END: Subheader -->
    <div class="m-content">
        <div class="row">
            <div class="col-lg-12">
                <!--begin::Portlet--> 
                <div class="m-portlet">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <span class="m-portlet__head-icon m--hide">
                                    <i class="la la-gear"></i>
                                </span>
                                <h3 class="m-portlet__head-text">
                                    Building Form
                                </h3>
                            </div>
                        </div>
                    </div>
                    <!--begin::Form-->
                    <form class="m-form m-form--fit m-form--label-align-right" id="m_form_1"  method="post" action="addBuildings.php" enctype="multipart/form-data">

                        <div class="m-portlet__body">
                            <div class="m-form__content">
                                <div class="m-alert m-alert--icon alert alert-danger m--hide" role="alert" id="m_form_1_msg">
                                    <div class="m-alert__icon">
                                        <i class="la la-warning"></i>
                                    </div>
                                    <div class="m-alert__text">
                                        Oh snap! Change a few things up and try submitting again.
                                    </div>
                                    <div class="m-alert__close">
                                        <button type="button" class="close" data-close="alert" aria-label="Close"></button>
                                    </div>
                                </div>
                            </div>
<?php if (isset($err) && !empty($err)) { ?>
                                <div class="m-form__content">
                                    <div class="m-alert m-alert--icon alert alert-danger" role="alert" id="m_form_1_msg1">
                                        <div class="m-alert__icon">
                                            <i class="la la-warning"></i>
                                        </div>
                                        <div class="m-alert__text">
    <?php echo $err; ?>
                                        </div>
                                        <div class="m-alert__close">
                                            <button type="button" class="close" data-close="alert" aria-label="Close"></button>
                                        </div>
                                    </div>
                                </div>
<?php } ?>
                            <div class="form-group m-form__group row">
                                <div class="col-lg-4">
                                    <label>
                                        Building Name: 
                                    </label>
                                    <input type="text" class="form-control m-input" placeholder="" id="building_name" name="building_name" value="<?php echo $auRec[0]->building_name; ?>">
                                </div>
                                <div class="col-lg-4">
                                    <label>
                                    Location :
                                       
                                    </label>
                                    <div class="input-group m-input-group m-input-group--square"> 
                                        <select class="form-control m-input m-input--square" id="area_id" name="area_id" required="">
                                            <option value="">Select Location</option>
                                            <?php
                                            $unitType = $_objAdmin->_getSelectList2('table_area', 'area_id,area_name', '', " status = 'A' ORDER BY area_name");
                                            if (is_array($unitType)) {
                                                foreach ($unitType as $value):
                                                    ?>
                                                <option value="<?php echo $value->area_id; ?>" <?php if ($value->area_id == $auRec[0]->area_id) echo "selected"; ?> ><?php echo $value->area_name; ?></option>
                                            <?php endforeach;
                                        } ?>
                                    </select>
                                </div> 
                            </div>
                            <div class="col-lg-4">
                                <label>
                                     Unit Type:
                                </label>
                                <div class="input-group m-input-group m-input-group--square"> 
                                    <select class="form-control m-input m-input--square" id="unit_type_id" name="unit_type_id" required="">
                                    <option value="">Select Location</option>
                                        <?php
                                        $unitType = $_objAdmin->_getSelectList2('table_unit_type', 'unit_type_id,unit_name', '', " status = 'A' ORDER BY unit_name");
                                        if (is_array($unitType)) {
                                            foreach ($unitType as $value):
                                                ?>
                                            <option value="<?php echo $value->unit_type_id; ?>" <?php if ($value->unit_type_id == $auRec[0]->unit_type_id) echo "selected"; ?> ><?php echo $value->unit_name; ?></option>
                                        <?php endforeach;
                                    } ?>
                                </select>
                            </div> 
                        </div>

                    </div>
                            <div class="form-group m-form__group row">
                                <div class="col-lg-12">
                                    <label class="">
                                        Location:
                                    </label>
                                <div id="m_gmap_8" style="height:300px;"></div>
                                </div>
                            </div>
                            <div class="form-group m-form__group row"> 
                                <div class="col-lg-4">
                                    <label class="">
                                        Linen Change:
                                    </label>
                                    <div class="m-radio-inline">
                                        <label class="m-radio m-radio--solid">
                                            <input <?php if ($auRec[0]->linen_change == '1') echo "checked"; ?> type="radio" name="linen_change"  value="1" >
                                            Yes
                                            <span></span>
                                        </label>
                                        <label class="m-radio m-radio--solid">
                                            <input type="radio" name="linen_change" value="2" <?php if ($auRec[0]->linen_change == '2') echo "checked"; ?>>
                                            No
                                            <span></span>
                                        </label>
                                    </div> 
                                </div>
                                <div class="col-lg-3">
                                    <label class="">
                                        Balcony:
                                    </label>
                                    <div class="m-radio-inline">
                                        <label class="m-radio m-radio--solid">
                                            <input <?php if ($auRec[0]->balcony == '1') echo "checked"; ?> type="radio" name="balcony"  value="1" >
                                            Yes
                                            <span></span>
                                        </label>
                                        <label class="m-radio m-radio--solid">
                                            <input type="radio" name="balcony" value="2" <?php if ($auRec[0]->balcony == '2') echo "checked"; ?>>
                                            No
                                            <span></span>
                                        </label>
                                    </div> 
                                </div>
                                <!-- <div class="col-lg-5">
                                    <label class="">
                                        Travelling time for cleaning (in minutes):
                                    </label>
                                    <input type="text" class="form-control m-input" placeholder="Time in minutes" id="travelling_time_for_cleaning" name="travelling_time_for_cleaning" value="<?php echo $auRec[0]->travelling_time_for_cleaning; ?>">
                                </div> -->
                            </div>
                            <div class="form-group m-form__group row"> 
                                <div class="col-lg-4">
                                    <label class="">
                                        Status:
                                    </label>
                                    <div class="m-radio-inline">
                                        <label class="m-radio m-radio--solid">
                                            <input <?php if ($auRec[0]->status == 'A') echo "checked"; ?> type="radio" name="status"  value="A" >
                                            Active
                                            <span></span>
                                        </label>
                                        <label class="m-radio m-radio--solid">
                                            <input type="radio" name="status" value="I" <?php if ($auRec[0]->status == 'I') echo "checked"; ?>>
                                            Inactive
                                            <span></span>
                                        </label>
                                    </div> 
                                </div>
                            </div>
                        </div>
                        <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                            <div class="m-form__actions m-form__actions--solid">
                                <div class="row">
                                    <div class="col-lg-4"></div>
                                    <div class="col-lg-8">
                                        <button type="submit" class="btn btn-success">
                                            Submit
                                        </button>
                                        <button type="reset" class="btn btn-secondary">
                                            Reset
                                        </button>
                                        <a href="buildings.php" class="btn btn-secondary">
                                            back
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                         <input name="account_id" type="hidden" value="<?php echo $_SESSION['accountId']; ?>" />
                         <input name="building_id" type="hidden" value="<?php echo $auRec[0]->building_id; ?>" />
                         <input type="hidden" id="latFld" name="latitude" value="<?php echo $auRec[0]->lat; ?>">
                         <input type="hidden" id="lngFld" name="longitude" value="<?php echo $auRec[0]->lng; ?>">
                        <input type="hidden" name="add" value="yes">

                    </form>
                    <!--end::Form-->
                </div>
                <!--end::Portlet-->
            </div>
        </div>
    </div>
</div>
</div>
</div>
<!-- start footer -->         
<?php include("footer.php") ?>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBs3PXRGjSu0tH4_DpHCpZm8TgrE-kKJgw" type="text/javascript"></script>
<script src="javascripts/gmaps.js" type="text/javascript"></script>
<script type="text/javascript">
    var FormControls = function () {
        //== Private functions

        var demo1 = function () {
            $("#m_form_1").validate({
                // define validation rules
                rules: {
                    building_name: {
                        required: true
                    },
                    unit_type_id: {
                        required: true,
                    },
                    status: {
                        required: true,
                    },
                    linen_change: {
                        required: true,
                    },
                    balcony: {
                        required: true,
                    }
                },
                //display error alert on form submit  
                invalidHandler: function (event, validator) {
                    var alert = $('#m_form_1_msg');
                    alert.removeClass('m--hide').show();
                    mApp.scrollTo(alert, -200);
                },
                submitHandler: function (form) {
                    form.submit();
                }
            });
        }

        return {
            // public functions
            init: function () {
                demo1();
            }
        };
    }();
    //== Class definition
    var GoogleMapsDemo = function() {

    //== Private functions

    var demo8 = function() {
        var map = new GMaps({
            div: '#m_gmap_8',
            lat: '<?php echo $auRec[0]->lat; ?>',
            lng: '<?php echo $auRec[0]->lng; ?>'
        });
        map.addMarker({
            lat: '<?php echo $auRec[0]->lat; ?>',
            lng:  '<?php echo $auRec[0]->lng; ?>',
            draggable: true,
            dragend: function(e) {
                var lat = e.latLng.lat();
                var lng = e.latLng.lng();
                $('#latFld').val(lat);
                $('#lngFld').val(lng);
            }
        });

        var handleAction = function() {
            var text = '';
            var building = $.trim($('#building_name').val());
            text = building;
                
            
            GMaps.geocode({
                address: text,
                callback: function(results, status) {
                    if (status == 'OK') {
                        var latlng = results[0].geometry.location;
                        map.setCenter(latlng.lat(), latlng.lng());
                        map.addMarker({
                            lat: latlng.lat(),
                            lng: latlng.lng(),
                            draggable: true,
                            dragend: function(e) {
                                var lat = e.latLng.lat();
                                var lng = e.latLng.lng();
                                $('#latFld').val(lat);
                                $('#lngFld').val(lng);
                            }
                        });
                        $('#latFld').val(latlng.lat());
                        $('#lngFld').val(latlng.lng());
                        mApp.scrollTo($('#m_gmap_8'));
                    }
                }
            });
        }

        $('#building_name').blur(function(e) {
            e.preventDefault();
            handleAction();
        });
	map.setZoom(10);
    }

    return {
        // public functions
        init: function() {
            // default charts
            demo8();
        }
    };
}();

jQuery(document).ready(function() {
    FormControls.init();
    GoogleMapsDemo.init();
});

</script>

