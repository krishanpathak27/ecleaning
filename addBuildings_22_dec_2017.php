<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBs3PXRGjSu0tH4_DpHCpZm8TgrE-kKJgw&callback=initmap"
type="text/javascript"></script>


<?php
include("includes/config.inc.php");
include("includes/function.php");
$page_name = "Buildings";
$_objAdmin = new Admin();
$objArrayList = new ArrayList();

if (isset($_POST['add']) && $_POST['add'] == 'yes') {
    if ($_REQUEST['id'] != "") {
        $condi = " building_name='" . $_POST['building_name'] . "'  and building_id<>'" . $_REQUEST['id'] . "'";
    } else {
        $condi = " building_name='" . $_POST['building_name'] . "' ";
    }
    $auRec = $_objAdmin->_getSelectList('table_buildings', "*", '', $condi);
    if (is_array($auRec)) {
        $err = "Building already exists in the system.";
        $auRec[0] = (object) $_POST;
    } else {
        if ($_REQUEST['id'] != "") {
            $cid = $_objAdmin->updateBuilding($_REQUEST['id']);
            $sus = "Building has been updated successfully.";
        } else {
            $cid = $_objAdmin->addBuilding();
            $sus = "Building has been added successfully.";
        }
    }
}


if (isset($_REQUEST['id']) && $_REQUEST['id'] != "") {
    $auRec = $_objAdmin->_getSelectList('table_buildings', "*", '', " building_id=" . $_REQUEST['id']);
    if (count($auRec) <= 0)
        header("Location: buildings.php");
}
?>
<script>
function showstage_city(str)
{
document.getElementById("address2").options.length = 0;
	if (str=="")
	{
		document.getElementById("address2").innerHTML="";
		return;
	}	 
	if (window.XMLHttpRequest)
	{// code for IE7+, Firefox, Chrome, Opera, Safari
	//alert("in 1");
		xmlhttp=new XMLHttpRequest();
		xmlhttp.open("POST","change_state.php?q="+str,true);
		xmlhttp.send();
		//alert("xml send");
		xmlhttp.onreadystatechange=function()
		{
		     //alert("in onready state");
			if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				var x=document.getElementById("address2");
				var xmlDoc=xmlhttp.responseXML;
				i=xmlDoc.getElementsByTagName("option");
				var lenx=i.length;
				for(var k=0;k<lenx;k++)
				{
					try
					{
						var option=window.document.createElement("option");
						option.value=i[k].getAttribute('value');
						option.text=i[k].getAttribute('description');
						x.add(option);
					}
					catch(err)
					{
					alert(err.message);
					}
				}
			}
		}
	}
	else 
	{// code for IE6, IE5
 		xmlhttp= new ActiveXObject("Microsoft.XMLHTTP");
		xmlhttp.open("POST","change_state.php?q="+str,true);
		xmlhttp.send();
		xmlhttp.onreadystatechange=function()
		{
			if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				var i;
				var x=document.getElementById("address2");
				var xmlDoc=xmlhttp.responseXML;
				i=xmlDoc.getElementsByTagName("option");
				var len=i.length;
				for(var j=0;j<len;j++)
				{
					var option=document.createElement("option");
					option.value=xmlDoc.documentElement.childNodes[j].attributes[0].nodeValue;
				    option.text=xmlDoc.documentElement.childNodes[j].attributes[1].nodeValue;
					try
					{
						x.add(option,x.options[null]);
					}
					catch (e)
					{
						x.add(option,null);
					}
				}
			}
		}
	}
}
</script>
<script>
    var location1;
    var location2;

    var address1;
    var address2;

    var latlng;
    var geocoder;
    var map;

    var distance;

    var map;
    var markersArray = [];

    // finds the coordinates for the two locations and calls the showMap() function
    function initialize()
    {
        var area = $("#area_id option:selected").text();
        $("#address1").val(area);
        geocoder = new google.maps.Geocoder(); // creating a new geocode object

        // getting the two address values
        address1 = document.getElementById("address1").value;
        //address2 = document.getElementById("address2").value;

        // finding out the coordinates
        if (geocoder)
        {
            geocoder.geocode({'address': address1}, function (results, status)
            {
                if (status == google.maps.GeocoderStatus.OK)
                {
                    //location of first address (latitude + longitude)
                    location1 = results[0].geometry.location;
                } else
                {
                    alert("Geocode was not successful for the following reason: " + status);
                }
            });
            geocoder.geocode({'address': address1}, function (results, status)
            {
                if (status == google.maps.GeocoderStatus.OK)
                {
                    //location of second address (latitude + longitude)
                    location2 = results[0].geometry.location;
                    // calling the showMap() function to create and show the map 
                    showMap();
                } else
                {
                    alert("Geocode was not successful for the following reason: " + status);
                }
            });
        }

        $(this).hide();
    }

    // creates and shows the map
    function showMap()
    {
        // center of the map (compute the mean value between the two locations)
        latlng = new google.maps.LatLng((location1.lat() + location2.lat()) / 2, (location1.lng() + location2.lng()) / 2);
        // set map options
        // set zoom level
        // set center
        // map type
        var mapOptions =
                {
                    zoom: 13,
                    center: latlng,
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                };
        // create a new map object
        // set the div id where it will be shown
        // set the map options
        map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);
        // add a click event handler to the map object
        google.maps.event.addListener(map, "click", function (event)
        {
            // place a marker
            placeMarker(event.latLng);
            // display the lat/lng in your form's lat/lng fields
            document.getElementById("latFld").value = event.latLng.lat();
            document.getElementById("lngFld").value = event.latLng.lng();
        });
<?php if ($auRec[0]->lat != "") { ?>
            var image = 'images/location.png';
            var myLatLng = new google.maps.LatLng(<?php echo $auRec[0]->lat; ?>,<?php echo $auRec[0]->lng; ?>);
            var beachMarker = new google.maps.Marker({
                position: myLatLng,
                map: map,
                icon: image
            });
<?php } ?>
    }
    function placeMarker(location) {
        // first remove all markers if there are any
        deleteOverlays();

        var marker = new google.maps.Marker({
            position: location,
            map: map
        });

        // add marker in markers array
        markersArray.push(marker);

        //map.setCenter(location);
    }

    // Deletes all markers in the array by removing references to them
    function deleteOverlays() {
        if (markersArray) {
            for (i in markersArray) {
                markersArray[i].setMap(null);
            }
            markersArray.length = 0;
        }
    }

</script> 
<?php
include("header.inc.php");
//$pageAccess=1;
//$check=$objArrayList->checkAccess($pageAccess, basename($_SERVER['PHP_SELF']));
//if($check == false){
//header('Location: ' . $_SERVER['HTTP_REFERER']);
//}
?>
<script type="text/javascript" src="javascripts/validate.js"></script>
<div class="content-wrapper">
    <div class="container-fluid">
        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="#">Home</a>
            </li>
            <li class="breadcrumb-item active">Admin</li>
        </ol>
        <!-- Icon Cards--> 
        <div class="row">
            <div class="accordian">
                <div class="panel-group">
                    <div class="panel panel-default card_bg mrgn-lg"><!--1 card-->

                        <div class="panel-heading">
                            <h4 class="panel-title">Buildings</h4>
                            <hr/>
                        </div> 
<?php if ($sus != '') { ?>
                            <!--  start message-green -->

                            <div id="message-green" >
                                <div class="bg-success pdng-md mrgn-btm-lg">
                                    <div class="pull-left bold"><?php echo $sus; ?></div>
                                    <div class="pull-right"><a class="close-green"><i class="fa fa-close"></i></a></div>
                                    <div class="clear"></div>
                                </div>
                            </div>

<?php } ?>

                        <?php if ($err != '') { ?>
                            <div id="message-red">
                                <div class="bg-warning pdng-md mrgn-btm-lg">
                                    <div class="pull-left bold">Error. <?php echo $err; ?></div>
                                    <div class="pull-right"><a class="close-red"><i class="fa fa-close"></i></a></div>
                                    <div class="clear"></div>
                                </div>
                            </div>

<?php } ?>

                        <div class="panel-body">   
                            <form name="frmPre" id="frmPre" method="post" action="addBuildings.php" enctype="multipart/form-data" >         
                                <div class="row">
                                    <div class="col-lg-3 col-md-3 col-xs-12 bold">
                                        Building Name:
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-xs-12">
                                        <div class="form-group">
                                            <input type="text" name="building_name" id="building_name" class="required minlength form-control" value="<?php echo $auRec[0]->building_name; ?>" maxlength="50" />

                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-3 col-md-3 col-xs-12 bold">
                                        Select Unit Type:
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-xs-12">
                                        <div class="form-group">
                                            <select name="unit_type_id" id="unit_type_id" class="required styledselect_form_3 form-control unit_type_id">
                                                <option value="">Select Unit Type</option>
<?php
$unitType = $_objAdmin->_getSelectList2('table_unit_type', 'unit_type_id,unit_name', '', " status = 'A' ORDER BY unit_name");
if (is_array($unitType)) {
    foreach ($unitType as $value):
        ?>
                                                        <option value="<?php echo $value->unit_type_id; ?>" <?php if ($value->unit_type_id == $auRec[0]->unit_type_id) echo "selected"; ?> ><?php echo $value->unit_name; ?></option>
    <?php endforeach;
} ?>
                                            </select>

                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-3 col-md-3 col-xs-12 bold">
                                        Select Area:
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-xs-12">
                                        <div class="form-group">
                                            <select name="area_id" id="area_id" class="required styledselect_form_3 form-control area_id">
                                                <option value="">Select Area</option>
<?php
$area = $_objAdmin->_getSelectList2('table_area', 'area_id,area_name', '', " status = 'A' ORDER BY area_name");
if (is_array($area)) {
    foreach ($area as $value):
        ?>
                                                        <option value="<?php echo $value->area_id; ?>" <?php if ($value->area_id == $auRec[0]->area_id) echo "selected"; ?> ><?php echo $value->area_name; ?></option>
    <?php endforeach;
} ?>
                                            </select>

                                        </div>
                                    </div>
                                </div>  


                                <div class="row">
                                    <div class="col-lg-3 col-md-3 col-xs-12 bold">
                                        Status:
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-xs-12">
                                        <div class="form-group">
                                            <select name="status" id="status" class="styledselect_form_3 form-control">
                                                <option value="A" <?php if ($auRec[0]->status == 'A') echo "selected"; ?> >Active</option>
                                                <option value="I" <?php if ($auRec[0]->status == 'I') echo "selected"; ?> >Inactive</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <table border="0" cellpadding="0" cellspacing="0"  id="id-form">
                                        <tr>
                                            <th valign="top">Point on Map:</th>
                                            <td><input type="button" class="form-submit" value="Show on Map" onClick="initialize();"/></td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="3"  align="center" ><p><div id="map_canvas" style="width:800px; height:400px"></div>
                                        </tr>
                                        <th>&nbsp;</th>
                                    </table>
                                </div>

                                <div class="row mrgn-top-sm">
                                    <div class="col-md-12 col-sm-12 col-xs-12 text-right">
                                        <input name="account_id" type="hidden" value="<?php echo $_SESSION['accountId']; ?>" />
                                        <input name="id" type="hidden" value="<?php echo $auRec[0]->building_id; ?>" />
                                        <input type="hidden" id="latFld" name="latitude" value="<?php echo $auRec[0]->lat; ?>">
                                        <input type="hidden" id="lngFld" name="longitude" value="<?php echo $auRec[0]->lng; ?>">
                                        <input name="address1" id="address1" type="hidden" value="<?php echo $auRec[0]->area_name; ?>"/>
                                        <input name="add" type="hidden" value="yes" />
                                        <button type="reset" class="btn bg-info"><i class="fa fa-refresh mrgn-rgt-sm" aria-hidden="true"></i>Reset</button>

                                        <button type="button" class="btn bg-info" onclick="location.href = 'buildings.php';" ><i class="fa fa-chevron-left mrgn-rgt-sm" aria-hidden="true"></i>Back</button>
                                        <input name="submit" class="form-submit btn bg-lgtgreen" type="submit" id="submit" value="Save" />
                                    </div>
                                </div>
                            </form>
                        </div> 
                    </div> 
                </div>
            </div>
        </div>
    </div>
    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->
<?php include("footer.php"); ?>

</div>

