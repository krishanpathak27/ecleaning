<?php
include("includes/config.inc.php");
include("includes/function.php");
$page_name="Category Type";
$_objAdmin = new Admin();
$objArrayList= new ArrayList();

if(isset($_POST['add']) && $_POST['add'] == 'yes')
{
	if($_REQUEST['id']!="") 
	{
		$condi=	" room_category='".$_POST['room_category']."' and u.unit_type_id ='".$_POST['unit_type_id']."' and c.cleaning_type_id='".$_POST['cleaning_type_id']."' and CT.room_category_id<>'".$_REQUEST['id']."'";
	}
	else
	{
		$condi=	" room_category='".$_POST['room_category']."' and u.unit_type_id ='".$_POST['unit_type_id']."' and c.cleaning_type_id='".$_POST['cleaning_type_id']."' ";
	}
	$auRec=$_objAdmin->_getSelectList('table_category_type as CT left join table_unit_type as u on u.unit_type_id = CT.unit_type_id left join table_cleaning_type as c on c.cleaning_type_id=CT.cleaning_type_id',"CT.*",'',$condi);

	if(is_array($auRec) && sizeof($auRec)>0)
	{
		$err="Category Type already exists in the system.";	
		$auRec[0]=(object)$_POST;						
	}
	else
	{
		if($_REQUEST['id']!="") 
		{
			$cid=$_objAdmin->updateCategoryType($_REQUEST['id']);
			$sus="Category Type has been updated successfully."; 
		}
		else 
		{
			$cid=$_objAdmin->addCategoryType();
			$sus="Category Type has been added successfully.";
		}
	}
}


if(isset($_REQUEST['id']) && $_REQUEST['id']!=""){
	$auRec=$_objAdmin->_getSelectList('table_category_type as CT left join table_unit_type as u on u.unit_type_id = CT.unit_type_id left join table_cleaning_type as c on c.cleaning_type_id=CT.cleaning_type_id',"CT.*,u.unit_name,u.unit_type_id,c.cleaning_type_id,c.cleaning_type",''," CT.room_category_id=".$_REQUEST['id']);

	if(count($auRec)<=0) header("Location: addCategoryType.php");
}


?>
<?php include("header.inc.php");
//$pageAccess=1;
//$check=$objArrayList->checkAccess($pageAccess, basename($_SERVER['PHP_SELF']));
//if($check == false){
//header('Location: ' . $_SERVER['HTTP_REFERER']);
//}

 ?>
<script type="text/javascript" src="javascripts/validate.js"></script>
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="index.php">Home</a>
        </li>
        <?php $breadCumArray=(getBreadCrum('profile_setting.php',-1));if(!empty($breadCumArray)){foreach($breadCumArray as $breadCum){?>
    <li class="breadcrumb-item active"><?php  echo $breadCum;?></li>
    <?php }}else{?>
      <li class="breadcrumb-item active">Admin</li>
    <?php }?>
      </ol>
      <!-- Icon Cards--> 
       <div class="row">
      <div class="accordian">
        <div class="panel-group">
          <div class="panel panel-default card_bg mrgn-lg"><!--1 card-->

            <div class="panel-heading">
              <h4 class="panel-title">Area</h4>
              <hr/>
            </div> 
  			<?php if($sus!=''){?>
            <!--  start message-green -->

            <div id="message-green" >
              <div class="bg-success pdng-md mrgn-btm-lg">
                  <div class="pull-left bold"><?php echo $sus; ?></div>
                  <div class="pull-right"><a class="close-green"><i class="fa fa-close"></i></a></div>
                 <div class="clear"></div>
              </div>
            </div>

           <?php } ?>

            <?php if($err!=''){?>
	          <div id="message-red">
	            <div class="bg-warning pdng-md mrgn-btm-lg">
	              <div class="pull-left bold">Error. <?php echo $err; ?></div>
	              <div class="pull-right"><a class="close-red"><i class="fa fa-close"></i></a></div>
	              <div class="clear"></div>
	            </div>
	          </div>

        	<?php } ?>

            <div class="panel-body">   
             	<form name="frmPre" id="frmPre" method="post" action="addCategoryType.php" enctype="multipart/form-data" >         
                	<div class="row">
	                  <div class="col-lg-3 col-md-3 col-xs-12 bold">
	                    Room Category:
	                  </div>
	                  <div class="col-lg-3 col-md-3 col-xs-12">
	                    <div class="form-group">
	                    <input type="text" name="room_category" id="room_category" class="required form-control" value="<?php echo $auRec[0]->room_category; ?>"  />

	                    
	                    </div>
	                  </div>
                  	</div>
                   
                   	<div class="row">
	                  	<div class="col-lg-3 col-md-3 col-xs-12 bold">
	                    	Select Unit Type:
	                  	</div>

	                  	<div class="col-lg-3 col-md-3 col-xs-12">
	                    	<div class="form-group">
	                    	<select name="unit_type_id" id="unit_type_id" class="required styledselect_form_3 unit_type_id form-control" onchange="showCleanerDropDown();">
									<option value="">Select Unit Type</option>
									<?php $unitType = $_objAdmin->_getSelectList2('table_unit_type','unit_type_id,unit_name',''," status = 'A' ORDER BY unit_name"); 
										if(is_array($unitType)){
										 foreach($unitType as $value):?>
										 	<option value="<?php echo $value->unit_type_id;?>" <?php if($value->unit_type_id==$auRec[0]->unit_type_id) echo "selected";?> ><?php echo $value->unit_name;?></option>
									<?php endforeach; }?>
								</select>	
	                     		
	                    
	                    	</div>
	                  	</div>
                	</div>

            		<div class="row">
	                  	<div class="col-lg-3 col-md-3 col-xs-12 bold">
	                    	Select Cleaning Type:
	                  	</div>

	                  	<div class="col-lg-3 col-md-3 col-xs-12">
	                    	<div class="form-group" id="cleaningTypedrop">

	                    		
	                    
	                    	</div>
	                  	</div>
            		</div>

                	<div class="row">
	                  <div class="col-lg-3 col-md-3 col-xs-12 bold">
	                    Minimum Count:
	                  </div>
	                  <div class="col-lg-3 col-md-3 col-xs-12">
	                    <div class="form-group">

	                    <input type="text" name="minimum" id="minimum" class="required form-control" value="<?php echo $auRec[0]->minimum; ?>"  />
	                   
	                    </div>
	                  </div>
                  	</div>  
                     <div class="row">
	                  <div class="col-lg-3 col-md-3 col-xs-12 bold">
	                    Maximum Count:
	                  </div>
	                  <div class="col-lg-3 col-md-3 col-xs-12">
	                    <div class="form-group">
	                    <input type="text" name="maximum" id="maximum" class="required form-control" value="<?php echo $auRec[0]->maximum; ?>"  />
	                   
	                   
	                    </div>
	                  </div>
                  	</div>

                  	<div class="row">
	                  <div class="col-lg-3 col-md-3 col-xs-12 bold">
	                    Status:
	                  </div>
	                  <div class="col-lg-3 col-md-3 col-xs-12">
	                    <div class="form-group">
	                    <select name="status" id="status" class="styledselect_form_3 form-control">
							<option value="A" <?php if($auRec[0]->status=='A') echo "selected";?> >Active</option>
							<option value="I" <?php if($auRec[0]->status=='I') echo "selected";?> >Inactive</option>
						</select>
	                   
	                   
	                    </div>
	                  </div>
                  	</div>
                 	

	                <div class="row mrgn-top-sm">
	                  <div class="col-md-12 col-sm-12 col-xs-12 text-right">
	                  	<input name="account_id" type="hidden" value="<?php echo $_SESSION['accountId']; ?>" />
						<input name="id" type="hidden" value="<?php echo $auRec[0]->room_category_id; ?>" />
      					<input name="add" type="hidden" value="yes" />
						<button type="button" class="btn bg-info"><i class="fa fa-refresh mrgn-rgt-sm" aria-hidden="true"></i>Reset</button>

	                  	<button type="button" class="btn bg-info" onclick="location.href='room_category_type.php';"><i class="fa fa-chevron-left mrgn-rgt-sm" aria-hidden="true"></i>Back</button>
	                  	<input name="submit" class="form-submit btn bg-lgtgreen" type="submit" id="submit" value="Save" />
	                   </div>
	                </div>
                </form>
              </div> 
          </div> 
        </div>
      </div>
    </div>
    </div>
    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->
    <?php include("footer.php"); ?>
    
  </div>
<script>

	function showCleanerDropDown(){
		var unitTypeId = document.getElementById("unit_type_id").value;
		$.ajax({
			'type': 'POST',
			'url': 'cleaner_type_dropdown.php',
			'data': 'unit_type_id='+unitTypeId,
			'success' : function(mystring) {
				document.getElementById("cleaningTypedrop").innerHTML = mystring;
			
			}
		});
	}


</script>