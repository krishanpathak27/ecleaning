<?php
include("includes/config.inc.php");
include("includes/function.php");
$_objAdmin = new Admin();
if (isset($_POST['add']) && $_POST['add'] == 'yes') {
    $err = '';
    if ($_POST['cleaner_id'] != "") {
        $condi = " cleaner_id=" . $_POST['cleaner_id'] . "  and leave_type=" . $_POST['leave_type'] . " and leave_from_date='" . $_POST['leave_from_date'] . "' and leave_to_date='".$_POST['leave_to_date']."'";
    }
       
    $auRec = $_objAdmin->_getSelectList2('table_cleaner_leaves', "*", '', $condi);
    if (is_array($auRec)) {
        $err = "This leave already exist.";
    }
    if($err == ''){
        if ($_POST['leave_request_id'] != "" && $_POST['leave_request_id'] != 0) {
            $save = $_objAdmin->updateCleaninerLeave($_POST["leave_request_id"]);
            $sus = "Updated Successfully";
        } else {
            $save = $_objAdmin->addCleanerLeave();
            $sus = "Added Successfully";
        }
        ?>
        <script type="text/javascript">
            window.location = "cleaners_leaves.php?suc=" + '<?php echo $sus; ?>';
        </script>
        <?php
    }
}
if (isset($_REQUEST['id']) && $_REQUEST['id'] != "") {
    //$auRec=$_objAdmin->_getSelectList('table_category_type as CT left join table_unit_type as u on u.unit_type_id = CT.unit_type_id left join table_cleaning_type as c on c.cleaning_type_id=CT.cleaning_type_id',"CT.*,u.unit_name,u.unit_type_id,c.cleaning_type_id,c.cleaning_type",''," CT.room_category_id=".$_REQUEST['id']);

    $auRec=$_objAdmin->_getSelectList('table_cleaner_leaves  as tcum ',"tcum.*",''," tcum.leave_id=".$_REQUEST['id']);
    //print_r($auRec);die;
    if (count($auRec) <= 0)
        header("Location: cleaners_leaves.php");
}
include("header.inc.php");

?>
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <!-- BEGIN: Subheader -->
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
               
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="#" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__separator">
                        -
                    </li>
                    <li class="m-nav__item">
                        <a href="#" class="m-nav__link">
                            <span class="m-nav__link-text">
                                Master
                            </span>
                        </a>
                    </li>
                    <li class="m-nav__separator">
                        -
                    </li>
                    <li class="m-nav__item">
                        <a href="" class="m-nav__link">
                            <span class="m-nav__link-text">
                               Cleaners Leaves
                            </span>
                        </a>
                    </li>  
                </ul>
            </div> 
        </div>
    </div>
    <!-- END: Subheader -->
    <div class="m-content">
        <div class="row">
            <div class="col-lg-12">
                <!--begin::Portlet--> 
                <div class="m-portlet">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <span class="m-portlet__head-icon m--hide">
                                    <i class="la la-gear"></i>
                                </span>
                                <h3 class="m-portlet__head-text">
                                    Cleaner Leave Form
                                </h3>
                            </div>
                        </div>
                    </div>
                    <!--begin::Form-->
                    <form class="m-form m-form--fit m-form--label-align-right" id="m_form_1"  method="post" action="addCleanerLeave.php" enctype="multipart/form-data">

                        <div class="m-portlet__body">
                            <div class="m-form__content">
                                <div class="m-alert m-alert--icon alert alert-danger m--hide" role="alert" id="m_form_1_msg">
                                    <div class="m-alert__icon">
                                        <i class="la la-warning"></i>
                                    </div>
                                    <div class="m-alert__text">
                                        Oh snap! Change a few things up and try submitting again.
                                    </div>
                                    <div class="m-alert__close">
                                        <button type="button" class="close" data-close="alert" aria-label="Close"></button>
                                    </div>
                                </div>
                            </div>
                            <?php if (isset($err) && !empty($err)) { ?>
                                <div class="m-form__content">
                                    <div class="m-alert m-alert--icon alert alert-danger" role="alert" id="m_form_1_msg1">
                                        <div class="m-alert__icon">
                                            <i class="la la-warning"></i>
                                        </div>
                                        <div class="m-alert__text">
                                            <?php echo $err; ?>
                                        </div>
                                        <div class="m-alert__close">
                                            <button type="button" class="close" data-close="alert" aria-label="Close"></button>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                            <div class="form-group m-form__group row">
                                <div class="col-lg-4">
                                    <label>
                                        Cleaner:
                                    </label>
                                      <div class="input-group m-input-group m-input-group--square"> 
                                        <select class="form-control m-input m-input--square" id="cleaner_id" name="cleaner_id">
                                           <option value="">Select Cleaner</option>
                                           <?php $condi = "";
                                                $branchList=$_objAdmin->_getSelectList('table_cleaner',"cleaner_id,cleaner_name",''," status='A'");
                                            
                                                for($i=0;$i<count($branchList);$i++){
                  
                                                    if($branchList[$i]->cleaner_id==$auRec[0]->cleaner_id){$select="selected";} else {$select="";}
                    
                                                ?>
                                                  <option value="<?php echo $branchList[$i]->cleaner_id; ?>" <?php echo $select ?>><?php echo $branchList[$i]->cleaner_name; ?></option>
                 
                                              <?php } ?>
                                          </select>
                                    </div>
                                   
                                </div>
                                <div class="col-lg-4">
                                    <label>
                                        Leave Type:
                                    </label>
                                      <div class="input-group m-input-group m-input-group--square"> 
                                        <select class="form-control m-input m-input--square" id="leave_type" name="leave_type">
                                           <option value="">Select Leave Type</option>
                                           <?php $condi = "";
										        $_objAdmin = new Admin();
                                                $branchList=$_objAdmin->_getSelectList2('table_leave_type',"leave_type_id,leave_type_name",''," status='A'");
                                                var_dump($branchList);
                                                for($i=0;$i<count($branchList);$i++){
                  
                                                    if($branchList[$i]->leave_type_id==$auRec[0]->leave_type){$select="selected";} else {$select="";}
                    
                                                ?>
                                                  <option value="<?php echo $branchList[$i]->leave_type_id; ?>" <?php echo $select ?>><?php echo $branchList[$i]->leave_type_name; ?></option>
                 
                                              <?php } ?>
                                          </select>
                                    </div>
                                   
                                </div>
                                <div class="col-lg-2">
                                    <label class="">
                                        From:
                                    </label>
                                    <div class="input-group"> 
                                        <input type="text" name="leave_from_date" class="form-control cleaning_time_start" id="datetimepicker1" value="<?php echo $auRec[0]->leave_from_date; ?>" />
                                    </div> 
                               </div>
                                <div class="col-lg-2">
                                    <label class="">
                                        From:
                                    </label>
                                    <div class="input-group"> 
                                        <input type="text" name="leave_to_date" class="form-control cleaning_time_start" id="datetimepicker2" value="<?php echo $auRec[0]->leave_to_date; ?>" />
                                    </div> 
                               </div>
                            </div>
                            </div>
                            
                        </div>
                        <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                            <div class="m-form__actions m-form__actions--solid">
                                <div class="row">
                                    <div class="col-lg-4"></div>
                                    <div class="col-lg-8">
                                        <button type="submit" class="btn btn-success">
                                            Submit
                                        </button>
                                        <button type="reset" class="btn btn-secondary">
                                            Reset
                                        </button>
                                        <a href="cleaning_mapping_list.php" class="btn btn-secondary">
                                                back
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                         <input name="status" type="hidden" value="A" />
                        <input name="leave_request_id" type="hidden" value="<?php echo $auRec[0]->leave_id; ?>" />
                        <input name="account_id" type="hidden" value="<?php echo $_SESSION['accountId']; ?>" />
                        <input type="hidden" name="add" value="yes">

                    </form>
                    <!--end::Form-->
                </div>
                <!--end::Portlet-->
            </div>
        </div>
    </div>
</div>
</div>
</div>
<!-- start footer -->         
<?php include("footer.php") ?>

<script type="text/javascript">
    var FormControls = function () {
        //== Private functions

        var demo1 = function () {
            $("#m_form_1").validate({
                // define validation rules
                rules: {
                    cleaner_id: {
                        required: true
                    },
                    leave_type: {
                        required: true
                    },
					leave_from_date:{
                        required: true
                    },
					leave_to_date:{
                        required: true
                    }
                    
                    
                },
                
                //display error alert on form submit  
                invalidHandler: function (event, validator) {
                    var alert = $('#m_form_1_msg');
                    alert.removeClass('m--hide').show();
                    mApp.scrollTo(alert, -200);
                },
                submitHandler: function (form) {
                    form.submit();
                }
            });
        }

        return {
            // public functions
            init: function () {
                demo1();
            }
        };
    }();

    jQuery(document).ready(function () {
        FormControls.init();
		$('#datetimepicker1').datetimepicker();
		$('#datetimepicker2').datetimepicker();
        
    });

</script>

