<?php
include("includes/config.inc.php");
include("includes/function.php");
$_objAdmin = new Admin();
if (isset($_POST['add']) && $_POST['add'] == 'yes') {
    $err = '';
    if ($_POST['cleaning_time_id'] != "") {
        $condi = " cleaning_time_id='" . $_POST['cleaning_time_id'] . "' and unit_type='" . $_POST['unit_type'] . "' and time_required='" . $_POST['time_required'] . "'";
    }
	
    $auRec = $_objAdmin->_getSelectList('table_cleaning_time', "*", '', $condi);
    if (is_array($auRec)) {
        $err = "Cleaning Time already exists in the system.";
    }
    if ($err == '') {
        if ($_POST['cleaning_time_id'] != "" && $_POST['cleaning_time_id'] != 0) {
            $save = $_objAdmin->updateCleaningTime($_POST["cleaning_time_id"]);
            $sus = "Updated Successfully";
        } else {
            $save = $_objAdmin->addCleaningTime();
            $sus = "Added Successfully";
        }
        ?>
        <script type="text/javascript">
            window.location = "cleaningTime.php?suc=" + '<?php echo $sus; ?>';
        </script>
        <?php
    }
}
if (isset($_REQUEST['id']) && $_REQUEST['id'] != "") {
    //$auRec=$_objAdmin->_getSelectList('table_category_type as CT left join table_unit_type as u on u.unit_type_id = CT.unit_type_id left join table_cleaning_type as c on c.cleaning_type_id=CT.cleaning_type_id',"CT.*,u.unit_name,u.unit_type_id,c.cleaning_type_id,c.cleaning_type",''," CT.room_category_id=".$_REQUEST['id']);

    $auRec=$_objAdmin->_getSelectList('table_cleaning_time as c',"c.*",''," c.cleaning_time_id=".$_REQUEST['id']);
    //print_r($auRec);die;
    if (count($auRec) <= 0)
        header("Location: cleaningTime.php");
}
include("header.inc.php");
?>
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <!-- BEGIN: Subheader -->
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">
                    Cleaning Type
                </h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="#" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__separator">
                        -
                    </li>
                    <li class="m-nav__item">
                        <a href="#" class="m-nav__link">
                            <span class="m-nav__link-text">
                                Master
                            </span>
                        </a>
                    </li>
                    <li class="m-nav__separator">
                        -
                    </li>
                    <li class="m-nav__item">
                        <a href="" class="m-nav__link">
                            <span class="m-nav__link-text">
                                Cleaning Time Management
                            </span>
                        </a>
                    </li>  
                </ul>
            </div> 
        </div>
    </div>
    <!-- END: Subheader -->
    <div class="m-content">
        <div class="row">
            <div class="col-lg-12">
                <!--begin::Portlet--> 
                <div class="m-portlet">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <span class="m-portlet__head-icon m--hide">
                                    <i class="la la-gear"></i>
                                </span>
                                <h3 class="m-portlet__head-text">
                                    Cleaning Type Form
                                </h3>
                            </div>
                        </div>
                    </div>
                    <!--begin::Form-->
                    <form class="m-form m-form--fit m-form--label-align-right" id="m_form_1"  method="post" action="addCleaningTime.php" enctype="multipart/form-data">

                        <div class="m-portlet__body">
                            <div class="m-form__content">
                                <div class="m-alert m-alert--icon alert alert-danger m--hide" role="alert" id="m_form_1_msg">
                                    <div class="m-alert__icon">
                                        <i class="la la-warning"></i>
                                    </div>
                                    <div class="m-alert__text">
                                        Oh snap! Change a few things up and try submitting again.
                                    </div>
                                    <div class="m-alert__close">
                                        <button type="button" class="close" data-close="alert" aria-label="Close"></button>
                                    </div>
                                </div>
                            </div>
                            <?php if (isset($err) && !empty($err)) { ?>
                                <div class="m-form__content">
                                    <div class="m-alert m-alert--icon alert alert-danger" role="alert" id="m_form_1_msg1">
                                        <div class="m-alert__icon">
                                            <i class="la la-warning"></i>
                                        </div>
                                        <div class="m-alert__text">
                                            <?php echo $err; ?>
                                        </div>
                                        <div class="m-alert__close">
                                            <button type="button" class="close" data-close="alert" aria-label="Close"></button>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                            <div class="form-group m-form__group row">
                                <div class="col-lg-4">
                                    <label>
                                        Cleaning Type:
                                    </label>
                                    <select class="form-control m-input m-input--square" id="cleaning_type_id" name="cleaning_type_id">
                                            <option value="">Select Cleaning Type</option>
                                            <?php
                                            $condi = "";

                                                $cleaningList = $_objAdmin->_getSelectList('table_cleaning_type as c', "c.cleaning_type_id,c.cleaning_type", '', " c.status='A'");
                                           

                                            for ($i = 0; $i < count($cleaningList); $i++) {

                                                if ($cleaningList[$i]->cleaning_type_id == $auRec[0]->cleaning_type_id) {
                                                    $select = "selected";
                                                } else {
                                                    $select = "";
                                                }
                                                ?>
                                                <option value="<?php echo $cleaningList[$i]->cleaning_type_id; ?>" <?php echo $select ?>><?php echo $cleaningList[$i]->cleaning_type; ?></option>

                                            <?php } ?>
                                           
                                        </select>
                                </div>
                                <div class="col-lg-4">
                                    <label class="">
                                        Unit Type
                                    </label>
                                    <select  class="form-control m-input" id="unit_type" name="unit_type">
                                    	<option value="Studio">Studio</option>
                                        <option value="1 Bedroom">1 Bedroom</option>
                                        <option value="2 Bedroom">2 Bedroom</option>
                                        <option value="3 Bedroom">3 Bedroom</option>
                                    </select> 
                                    </div>
                                <div class="col-lg-4">
                                    <label class="">
                                       Time Required for Cleaning:
                                    </label>
                                    <input type="text" class="form-control m-input" placeholder="Ex: 01:45 means 1 hour 45 min" id="time_required" name="time_required" value="<?php echo $auRec[0]->time_required; ?>"> 
                                    </div>    
                                </div>
                            
                            </div>
                            
                        </div>
                        <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                            <div class="m-form__actions m-form__actions--solid">
                                <div class="row">
                                    <div class="col-lg-4"></div>
                                    <div class="col-lg-8">
                                        <button type="submit" class="btn btn-success">
                                            Submit
                                        </button>
                                        <button type="reset" class="btn btn-secondary">
                                            Reset
                                        </button>
                                        <a href="cleaningType.php" class="btn btn-secondary">
                                                back
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <input name="cleaning_time_id" type="hidden" value="<?php echo $auRec[0]->cleaning_time_id; ?>" />
                        <input name="account_id" type="hidden" value="<?php echo $_SESSION['accountId']; ?>" />
                         <input name="status" type="hidden" value="A" />
                        <input type="hidden" name="add" value="yes">

                    </form>
                    <!--end::Form-->
                </div>
                <!--end::Portlet-->
            </div>
        </div>
    </div>
</div>
</div>
</div>
<!-- start footer -->         
<?php include("footer.php") ?>

<script type="text/javascript">
    var FormControls = function () {
        //== Private functions

        var demo1 = function () {
            $("#m_form_1").validate({
                // define validation rules
                rules: {
                    cleaning_type: {
                        required: true
                    },
                    unit_type_id: {
                        required: true
                    },
                    
                    status: {
                        required: true,
                    },
                    
                    is_long_term: {
                        required: true,
                    }
                },
                
                //display error alert on form submit  
                invalidHandler: function (event, validator) {
                    var alert = $('#m_form_1_msg');
                    alert.removeClass('m--hide').show();
                    mApp.scrollTo(alert, -200);
                },
                submitHandler: function (form) {
                    form.submit();
                }
            });
        }

        return {
            // public functions
            init: function () {
                demo1();
            }
        };
    }();

    jQuery(document).ready(function () {
        FormControls.init();
        
    });

</script>

