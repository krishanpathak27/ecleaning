<?php
include("includes/config.inc.php");
include("includes/function.php");
$page_name="Item";
$_objAdmin = new Admin();
$_objItem = new Item();
$objArrayList= new ArrayList();


if(isset($_POST['add']) && $_POST['add'] == 'yes')
{
	
	
	if($_REQUEST['id']!="") 
	{
		
		$condi=	" item_name='".$_POST['item_name']."' and item_code='".$_POST['item_code']."'  and item_id<>'".$_REQUEST['id']."'";
	
	}
	else
	{
		$condi=	" item_name='".$_POST['item_name']."' and item_code='".$_POST['item_code']."' ";
		
	}

	$auRec=$_objAdmin->_getSelectList('table_item',"*",'',$condi);
	
	
	if((is_array($auRec)) &&(count($auRec)>0))
	{
		$err="Item already exists in the system.";	
		$auRec[0]=(object)$_POST;		
		//$auRec_price[0]=(object)$_POST;						
	}
	else
	{
		
		if($_REQUEST['id']!="") 
		{
			
			$cid=$_objAdmin->updateItem($_REQUEST['id']);
			
			//$_objAdmin->mysql_query("delete from table_item_case_relationship where item_id='".$_REQUEST['id']."'");
			//$color_id=$_objAdmin->addItemCases($_REQUEST['id']);
			$sus="Item has been updated successfully.";
		}
		else 
		{

			$cid=$_objAdmin->addItem();
			if($cid){
					 $sus= "Item has been added successfully.";
					}
			if(isset($_FILES['fileUpload']['tmp_name']) && $_FILES['fileUpload']['tmp_name']!=""){			 
				$ret = $_objItem->uploadPRTFile();
					if($ret=='') {																	
					$sus= "Item has been added successfully.";								
					}elseif($ret=='no') {
					$error= "Empty file";
					}else {
					$error= "Following record has not been inserted.<br>".$ret;														
					}
					if($sus!=''){
					$sus=$sus;
					}else{
					$err=$error;
					}		
				}
			//$sus="Item has been added successfully.";
		}
	}
}


if(isset($_REQUEST['id']) && $_REQUEST['id']!="")
{
	//print_R($_REQUEST['id']);
	//$auRec=$_objAdmin->_getSelectList('table_item as i left join table_item_case_relationship as c on c.item_id=i.item_id left join table_cases as ca on ca.case_id=c.case_id ',"*",''," i.item_id=".$_REQUEST['id']);
	
//$auRec=$_objAdmin->_getSelectList('table_item as i left join table_item_case_relationship as c on c.item_id=i.item_id left join table_cases as ca on ca.case_id=c.case_id',"*",''," i.item_id=".$_REQUEST['id']);
	$auRec_price=$_objAdmin->_getSelectList('table_price',"*",'',"item_id=".$_REQUEST['id']);
	$auRec=$_objAdmin->_getSelectList('table_item',"*",'',"item_id=".$_REQUEST['id']);
	if(count($auRec)<=0 ) header("Location: item.php");
	//echo '<pre>';
	//print_r($auRec);
}


/************************************************************
* DESC: Get Item/Model Claim Slabs
* Author: AJAY
* Created: 2017-03-22
*
*
***********************************************************/


if(isset($_GET['requestType']) && $_GET['requestType'] == 'getClaimDetail' && isset($_GET['item_id']) && !empty($_GET['item_id'])) {

	$claimSlabDetail = $_objAdmin->_getSelectList('table_item AS I LEFT JOIN table_item_claim_slabs AS ICS ON ICS.item_id = I.item_id',"ICS.* ",''," I.item_id ='".$_GET['item_id']."' AND ICS.item_id>0");
	// echo "<pre>";
	// print_r($claimSlabDetail);
	

	if(sizeof($claimSlabDetail)>0) {

		echo json_encode(array('status'=>'success', 'result'=>$claimSlabDetail));
		
	} else {

		echo json_encode(array('status'=>'failed', 'result'=>''));
	}

	exit;
}
?>

<?php include("header.inc.php");
$pageAccess=1;
$check=$objArrayList->checkAccess($pageAccess, basename($_SERVER['PHP_SELF']));
if($check == false){
header('Location: ' . $_SERVER['HTTP_REFERER']);
}

 ?>

<script src="javascripts/jquery-1.8.2.js" type="text/javascript"></script>
<script src="javascripts/jquery-ui.js"></script>
<script type="text/javascript" src="javascripts/validate.js"></script>
		<script type="text/javascript">

			$(document).ready(function() {


				// $( "#item_prodata, #item_warranty" ).load(function() {
				//   // Handler for .load() called.
				//   				alert('Hello');
				// });

				// While change the value of Warranty/Prorata

				$('#showClaimslabs').hide();

				$('#item_prodata, #item_warranty').bind("focusout", function() {

					//alert('Hello');

					var warrantyPeriod = parseInt($('#item_warranty').val());
					var warrantyProRata = parseInt($('#item_prodata').val());

					//console.log(warrantyPeriod);
					//console.log(warrantyProRata);

					if(warrantyPeriod>0 && warrantyProRata>0) {

						//console.log(Math.ceil(warrantyProRata/5));

					var loopRepeatLimit = Math.ceil(warrantyProRata/5);

					var claimSlabStart = warrantyPeriod;

					//var claimSlabArray = [];
					var claimSlabArray = new Array(); 
					claimSlabArray.push(claimSlabStart);
					var slabHtmlFirstString ='<tr><th valign="top">Claim Month From:</th><th valign="top">Claim Month To:</th><th valign="top">Claim Discount(%):</th>	</tr>';
					$('#showSlabs').html(slabHtmlFirstString);
					for (var i=1; i<=loopRepeatLimit; i++) {

						//if(i==0)
						//console.log(claimSlabArray[i]);

						 var startInterval = claimSlabArray[i-1]+1;
						 var endInterval   = startInterval+5;
					
						 console.log('Claim Month:'+startInterval+'-'+endInterval);
						 claimSlabArray.push(endInterval);

							
						var slabHtmlOtherString ='<tr><td><input type="text"  class="text"  readonly="" name="startSlab[]" value="'+startInterval+'"></td><td><input type="text"  class="text"  readonly="" name="endSlab[]" value="'+endInterval+'"></td><td><input type="text"  class="text"  name="slabDiscount[]"  id="'+(startInterval+endInterval)+'" max="99"></td></tr>';
						$('#showSlabs').append(slabHtmlOtherString);

					}
						getClaimSlabDetails(); // get Slab Details
						$('#showClaimslabs').show();

					} else {
						$('#showClaimslabs').hide();
					}




				});


				// While load a page

				$(window).bind("load", function() {

					//alert('Hello');

					var warrantyPeriod = parseInt($('#item_warranty').val());
					var warrantyProRata = parseInt($('#item_prodata').val());

					//console.log(warrantyPeriod);
					//console.log(warrantyProRata);

					if(warrantyPeriod>0 && warrantyProRata>0) {

						//console.log(Math.ceil(warrantyProRata/5));

					var loopRepeatLimit = Math.ceil(warrantyProRata/5);

					var claimSlabStart = warrantyPeriod;

					//var claimSlabArray = [];
					var claimSlabArray = new Array(); 
					claimSlabArray.push(claimSlabStart);
					var slabHtmlFirstString ='<tr><th valign="top">Claim Month From:</th><th valign="top">Claim Month To:</th><th valign="top">Claim Discount(%):</th>	</tr>';
					$('#showSlabs').html(slabHtmlFirstString);
					for (var i=1; i<=loopRepeatLimit; i++) {

						//if(i==0)
						//console.log(claimSlabArray[i]);

						 var startInterval = claimSlabArray[i-1]+1;
						 var endInterval   = startInterval+5;
					
						 console.log('Claim Month:'+startInterval+'-'+endInterval);
						 claimSlabArray.push(endInterval);

							
						var slabHtmlOtherString ='<tr><td><input type="text"  class="text"  readonly="" name="startSlab[]" value="'+startInterval+'"></td><td><input type="text"  class="text"  readonly="" name="endSlab[]" value="'+endInterval+'"></td><td><input type="text"  class="text"  name="slabDiscount[]" id="'+(startInterval+endInterval)+'" max="99"></td></tr>';
						$('#showSlabs').append(slabHtmlOtherString);

					}
						getClaimSlabDetails(); // get Slab Details
						$('#showClaimslabs').show();

					} else {
						$('#showClaimslabs').hide();
					}


				});
		});



			function getClaimSlabDetails () {

				var itemId = parseInt($('#item_id').val());

				if(itemId>0) {
					$.ajax({
						url: 'addItem.php',
						data: {'requestType':'getClaimDetail', 'item_id': itemId},
						method: 'POST',
						success: function (result){
							var data = $.parseJSON(result);
				  		
								if(data.status == 'success') {

									$.each( data.result, function( key, value ) {
									  //alert( key + ": " + value.slab_start_month );
									  var createClaimSlabID = parseInt(value.slab_start_month) + parseInt(value.slab_end_month) ;
									  $('#'+createClaimSlabID).val(value.slab_discount);
									});

								}

							}

					});

				}

			}

			</script>
<script>
    $(function() {
        $( "#from" ).datepicker({
			dateFormat: 'd M yy',
			changeMonth: true, 
			changeYear: true, 
			yearRange: '1930:2050'
        });
        $( "#to" ).datepicker({
			dateFormat: 'd M yy',
			changeMonth: true, 
			changeYear: true, 
			yearRange: '1930:2050'
        });
    });
</script>

<div class="clear"></div>
<!-- start content-outer -->
<div id="content-outer">
<!-- start content -->
	<div id="content">
	<div id="page-heading"><h1><span style="color: #d74343; font-family: Tahoma; font-weight: bold;">Item</span></h1></div>
		<table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">
		<tr>
			<th rowspan="3" class="sized"><img src="images/side_shadowleft.jpg" width="20" height="300" alt="" /></th>
			<th class="topleft"></th>
			<td id="tbl-border-top">&nbsp;</td>
			<th class="topright"></th>
			<th rowspan="3" class="sized"><img src="images/side_shadowright.jpg" width="20" height="300" alt="" /></th>
		</tr>
		<tr>
			<!--<td id="tbl-border-left"></td>-->
			<td>
			<!--  start content-table-inner -->
			<div id="content-table-inner">
				<table border="0" width="100%" cellpadding="0" cellspacing="0">
				<tr valign="top">
					<td>
					<!--  start message-red -->
					<?php if($err!=''){?>
					<div id="message-red">
						<table border="0" width="100%" cellpadding="0" cellspacing="0">
						<tr>
							<td class="red-left"> <?php echo $err; ?></td>
							<td class="red-right"><a class="close-red"><img src="images/icon_close_red.gif"   alt="" /></a></td>
						</tr>
						</table>
					</div>
					<?php } ?>
					<!--  end message-red -->
					<?php if($sus!=''){?>
					<!--  start message-green -->
					<div id="message-green">
						<table border="0" width="100%" cellpadding="0" cellspacing="0">
						<tr>
							<td class="green-left"><?php echo $sus; ?></td>
							<td class="green-right"><a class="close-green"><img src="images/icon_close_green.gif"   alt="" /></a></td>
						</tr>
						</table>
					</div>
					<?php } ?>
					<!--  end message-green -->
					
			<!-- start id-form -->
		<form name="frmPre" id="frmPre" method="post" action="addItem.php" enctype="multipart/form-data" >
			<table border="0" cellpadding="0" cellspacing="0"  id="id-form">

			<tr>
				<th valign="top">Item Code:</th>
				<td><input type="text" name="item_code" id="item_code" class="required" value="<?php echo $auRec[0]->item_code; ?>" maxlength="50" /></td>
				<td></td>
			</tr>



			<tr>
				<th valign="top">Item Name:</th>
				<td><input type="text" name="item_name" id="item_name" class="required" value="<?php echo $auRec[0]->item_name; ?>" maxlength="100" /></td>
				<td></td>
			</tr>


			<tr>
				<th valign="top"><input type="radio" class="checkbox" name="choose_bsn" value="new" checked> New Product Code:</th>
				<td><input type="text" name="item_erp_code" id="item_erp_code" class="text" value="<?php echo $auRec[0]->item_erp_code; ?>" maxlength="50" /></td>
				<td></td>
			</tr>

			<tr>
				<th valign="top"><input type="radio" class="checkbox" name="choose_bsn" value="old"> Old Product Code:</th>
				<td><input type="text" name="item_erp_code_old" id="item_erp_code_old" class="text" value="<?php echo $auRec[0]->item_erp_code_old; ?>" maxlength="50" /></td>
				<td></td>
			</tr>

			<tr>
				<th valign="top"> Model Code:</th>
				<td><input type="text" name="model_code" id="model_code" class="text required" value="<?php echo $auRec[0]->model_code; ?>" /></td>
				<td></td>
			</tr>	
						
			
			
			<!-----Changes Done-------->


			<tr>
				<th valign="top">Item Division:</th>
				<td>
				<select name="add_divisions" id="add_divisions" class="styledselect_form_3">
				<option value="">--Select Division--</option>
					<?php $divis=$_objAdmin->_getSelectList('table_division','division_id,division_name',''," status='A' ORDER BY division_name");
					if(is_array($divis)){
					for($i=0;$i<count($divis);$i++){?>
					<option value="<?php echo $divis[$i]->division_id;?>" <?php if($divis[$i]->division_id==$auRec[0]->item_division) echo "selected";?> ><?php echo $divis[$i]->division_name;?></option>
					
					<?php }}?>
					</select>
				</td>
				<td></td>
			</tr>


			<tr>
				<th valign="top">Segment:</th>
				<td>
				<select name="item_segment" id="item_segment" class="styledselect_form_3">
				<option value="">--Select Segment--</option>
					<?php $segmt=$_objAdmin->_getSelectList('table_segment','segment_id,segment_name',''," status='A' ORDER BY segment_name");
					if(is_array($segmt)){
					for($i=0;$i<count($segmt);$i++){?>
					<option value="<?php echo $segmt[$i]->segment_id;?>" <?php if($segmt[$i]->segment_id==$auRec[0]->item_segment) echo "selected";?> ><?php echo $segmt[$i]->segment_name;?></option>
					
					<?php }}?>
					</select>
				</td>
				<td></td>
			</tr>

			<tr>
				<th valign="top">Brands:</th>
				<td>
				<select name="brand_id" id="brand_id" class="styledselect_form_3">
				<option value="">--Select Brand--</option>
					<?php $aBrands=$_objAdmin->_getSelectList('table_brands','brand_id,brand_name',''," status='A' ORDER BY brand_name");
					if(is_array($aBrands)){
					for($i=0;$i<count($aBrands);$i++){?>
					<option value="<?php echo $aBrands[$i]->brand_id;?>" <?php if($aBrands[$i]->brand_id==$auRec[0]->brand_id) echo "selected";?> ><?php echo $aBrands[$i]->brand_name;?></option>
					
					<?php }}?>
					</select>
				</td>
				<td></td>
			</tr>


			<tr>
				<th valign="top"> Category:</th>
				<td>
				<select name="category_id" id="category_id" class="styledselect_form_3">
					<option value="">--Select Category--</option>
					<?php $aCategory=$_objAdmin->_getSelectList('table_category','category_id,category_name',''," status='A' AND cat_type = 'C' ORDER BY category_name");
					if(is_array($aCategory)){
					for($i=0;$i<count($aCategory);$i++){?>
					<option value="<?php echo $aCategory[$i]->category_id;?>" <?php if($aCategory[$i]->category_id==$auRec[0]->category_id) echo "selected";?> ><?php echo $aCategory[$i]->category_name;?></option>
					
					<?php }}?>
					</select>
				</td>
				<td></td>
			</tr>

			<tr>
				<th valign="top">Color:</th>
				<td>
				<select name="color_id" id="color_id" class="styledselect_form_3">
				<option value="">--Select Color--</option>
					<?php $color=$_objAdmin->_getSelectList('table_color','color_id,color_desc',''," status='A' ORDER BY color_desc");

					if(is_array($color)){
					for($i=0;$i<count($color);$i++){?>

					<option value="<?php echo $color[$i]->color_id;?>" <?php if($color[$i]->color_id==$auRec[0]->item_color) echo "selected";?> ><?php echo $color[$i]->color_desc;?></option>
					
					<?php }}?>
					</select>
				</td>
				<td></td>
			</tr>
			<!-- <tr>
				<th valign="top">Varient:</th>
				<td>
				<select name="variant_id" id="variant_id" class="styledselect_form_5">
					<option value="">--Select Variant--</option>
					<?php $aVarint=$_objAdmin->_getSelectList('table_variant','variant_id,variant_name',''," status='A' ORDER BY variant_name");
					if(is_array($aVarint)){
					for($i=0;$i<count($aVarint);$i++){?>
					<option value="<?php echo $aVarint[$i]->variant_id;?>" <?php if($aVarint[$i]->variant_id==$auRec[0]->variant_id) echo "selected";?> ><?php echo $aVarint[$i]->variant_name; ?></option>
					
					<?php }}?>
					</select>
				</td>
				<td></td>
			</tr> -->

			<!-- <tr>
				<th valign="top">SKU Name:</th>
				<td>
				<select name="sku_id" id="sku_id" class="styledselect_form_5">
					<option value="">--Select Sku--</option>
					<?php $aSku=$_objAdmin->_getSelectList('table_sku','sku_id,sku_name',''," status='A' ORDER BY sku_name");
					if(is_array($aSku)){
					for($i=0;$i<count($aSku);$i++){?>
					<option value="<?php echo $aSku[$i]->sku_id;?>" <?php if($aSku[$i]->sku_id==$auRec[0]->sku_id) echo "selected";?> ><?php echo $aSku[$i]->sku_name; ?></option>
					<?php }}?>
					</select>
				</td>
				<td></td>
			</tr> -->

			<tr>
				<th valign="top">Weight:</th>
				<td><input type="text" name="item_size" id="item_size" class="text" value="<?php echo $auRec[0]->item_size; ?>" maxlength="100" /></td>
				<td></td>
			</tr>

			<tr>
				<th valign="top">Dimensions</th>
				<td><input type="text" name="item_diameter" id="item_diameter" class="text" value="<?php echo $auRec[0]->item_diameter; ?>" maxlength="100" placeholder="Diameter"/> &nbsp;&nbsp;&nbsp;X&nbsp;&nbsp;&nbsp; </td>

				<td><input type="text" name="item_length" id="item_length" class="text" value="<?php echo $auRec[0]->item_length; ?>" maxlength="100" placeholder="Length"/></td>
				<td></td>
			</tr>



	<!-- <tr>
		<th valign="top">No of pcs/case:</th>
		<?php if($_REQUEST['id']==""){ ?>
		<td><input type="text" name="case_id[]" id="case_id" class="text" onkeypress="return IsNumeric(event);" value="<?php echo $auRec[0]->case_size; ?>" maxlength="50"   ondrop="return false;" onpaste="return false;"/><span id="error" style="color: red; display: none">* Input digits (0 - 9)</span></td>
		<?php } else { ?>
		<td><?php echo $auRec[0]->case_size; ?></td>
		<?php } ?>
		<td></td>
	</tr> -->



			<tr>
				<th valign="top">Offer Type:</th>
				<td>
				<select name="offer_id" id="offer_id" class="styledselect_form_3">
				<option value="">--Select Offer--</option>
					<?php $offers=$_objAdmin->_getSelectList('table_offer','offer_id,offer_name',''," status='A' ORDER BY offer_name");
					if(is_array($offers)){
					for($i=0;$i<count($offers);$i++){?>
					<option value="<?php echo $offers[$i]->offer_id;?>" <?php if($offers[$i]->offer_id==$auRec[0]->offer_id) echo "selected";?> ><?php echo $offers[$i]->offer_name;?></option>
					
					<?php }}?>
					</select>
				</td>
				<td></td>
			</tr>
			<tr>
				<th valign="top">Capacity:</th>
				<td><input type="text" name="item_capacity" id="item_capacity" value="<?php echo $auRec[0]->item_capacity; ?>" class="text" maxlength="10" /></td>
				<td></td>
			</tr>
			<tr>
				<th valign="top">Volt:</th>
				<td><input type="text" name="item_volt" id="item_volt"  value="<?php echo $auRec[0]->item_volt; ?>" class="text" maxlength="10" /></td>
				<td></td>
			</tr>
			<tr>
				<th valign="top">CCA:</th>
				<td><input type="text" name="item_cca" id="item_cca"  value="<?php echo $auRec[0]->item_cca; ?>" class="text" maxlength="10" /></td>
				<td></td>
			</tr>
			<tr>
				<th valign="top">Item Availability:</th>
				<td>
				<select name="item_availability" id="item_availability" class="styledselect_form_3">
				<option value="1" <?php if($auRec[0]->item_availability=='1') echo "selected";?> >Available</option>
				<option value="0" <?php if($auRec[0]->item_availability=='0') echo "selected";?> >Not Available</option>
				</td>
				<td></td>
			</tr>

			<tr>
				<th valign="top">Warranty (in months):</th>
				<td><input type="text" name="item_warranty"  value="<?php echo $auRec[0]->item_warranty; ?>" id="item_warranty" class="text" maxlength="10" /></td>
				<td></td>
			</tr>
			<tr>
				<th valign="top">ProData (in months):</th>
				<td><input type="text" name="item_prodata"  value="<?php echo $auRec[0]->item_prodata; ?>"  id="item_prodata" class="text" maxlength="10" /></td>
				<td></td>
			</tr>
			<tr id="showClaimslabs">
				<th valign="top">Claim Slabs:</th>
				<td colspan="3">
					<table id="showSlabs"></table>
				</td>
			</tr>
			<tr>
				<th valign="top">Grace (in months):</th>
				<td><input type="text" name="item_grace" value="<?php echo $auRec[0]->item_grace; ?>" id="item_grace" class="text" maxlength="10" /></td>
				<td></td>
			</tr>
			<tr>
				<th valign="top">Item MRP:</th>
				<td><input type="text" name="item_mrp" id="item_mrp" class="required"  value="<?php echo $auRec_price[0]->item_mrp; ?>"maxlength="10" /></td>
				<td><span style="font-size: 12px;margin-left: 10px;font-weight: bolder;">MRP is in: <b id="currency_sym_mrp" style="font-size: 14px;"> INR</b></span></td>
			</tr>
			<tr>
				<th valign="top">Item DP:</th>
				<td><input type="text" name="item_dp" id="item_dp" class="required"  value="<?php echo $auRec_price[0]->item_dp; ?>" maxlength="10"/></td>
				<td><span style="font-size: 12px;margin-left: 10px;font-weight: bolder;">DP is in: <b id="currency_sym_dp" style="font-size: 14px;"> INR</b></span></td>
			</tr>
			<tr>
				<th valign="top">Price Start Date:</th>
				<td><input type="text" name="from" id="from" class="date required " readonly value="<?php echo $_objAdmin->_changeDate($auRec_price[0]->start_date); ?>"/></td>
				<td></td>
			</tr>
			<tr>
				<th valign="top">Price End Date:</th>
				<td><input type="text" id="to" name="to" class="date required" readonly value="<?php echo $_objAdmin->_changeDate($auRec_price[0]->end_date); ?>"/></td>
				<td></td>
			</tr>


			




			<?php /************** 
					* Desc : code for showing sku chain list 
					* created on : 05 Jan 2015
					* Author : AKV	
					*/
			?>
			<?php /*<tr>
				<th valign="top">Item Chain:</th>
				<td>
					<div style="width:200px; height:200px; overflow:auto;" >
					<table border="0" cellpadding="0" cellspacing="0"  id="id-form">
					<tr>
						<th valign="top"></th>
					</tr>
					<?php
					$auCol=$_objAdmin->_getSelectList('table_chain',"chain_id, chain_name",''," status='A'");
					for($i=0;$i<count($auCol);$i++){
					?>
					<tr>
						<td><input type="checkbox" name="chain_id[]" <?php 
							if($auRec[0]->item_id!=''){
							$auchk=$_objAdmin->_getSelectList2('table_item_chain_relationship',"*",''," chain_id=".$auCol[$i]->chain_id." and item_id=".$auRec[0]->item_id);
							if(is_array($auchk)){
							echo "checked";
						}
					}
					?> value="<?php echo $auCol[$i]->chain_id;?>" > <?php echo $auCol[$i]->chain_name;?>
					
					</td>
					</tr>
					<?php } ?>
					</table>
					</div>
				</td>
				<td></td>
			</tr> */
			
			?>

			
			

			<!-- <tr>
				<th valign="top">Cases Sizes:</th>
				<td>
					<div style="width:200px; height:100px;overflow:auto;" >
					<table border="0" cellpadding="0" cellspacing="0"  id="id-form">
					<tr>
						<th valign="top"></th>
					</tr>
					<?php

					$auCol=$_objAdmin->_getSelectList('table_cases',"*",''," status='A'");
					for($i=0;$i<count($auCol);$i++){
					?>
					<tr>
						<td><input type="checkbox" name="case_id[]" <?php 
							if($auRec[0]->item_id!=''){
							$auchk=$_objAdmin->_getSelectList2('table_item_case_relationship',"*",''," case_id=".$auCol[$i]->case_id." and item_id=".$auRec[0]->item_id);
							if(is_array($auchk)){
							echo "checked";
						}
					}
					?> value="<?php echo $auCol[$i]->case_id;?>" > <?php echo $auCol[$i]->case_size;?>
					
					</td>
					</tr>
					<?php } ?>
					</table>
					</div>
				</td>
				<td></td>
			</tr> -->




	 <script type="text/javascript">
        var specialKeys = new Array();
        specialKeys.push(8); //Backspace
        function IsNumeric(e) {
            var keyCode = e.which ? e.which : e.keyCode
            var ret = ((keyCode >= 48 && keyCode <= 57) || specialKeys.indexOf(keyCode) != -1);
            document.getElementById("error").style.display = ret ? "none" : "inline";
            return ret;
        }
    </script>
	







			<?php if($_REQUEST['id']=="")
			{
			?>
			
			<!-- <tr>
				<th valign="top">Item PTR <small>(Import)</small> :</th>
				<td><input type="file" name="fileUpload" id="file" class="text" ></td>
				<td><input name="submit" class="form-submit" type="button" id="submit" value="Downlaod PTR CSV Sheet" onclick="location.href='import.inc.php?items_prt_csv=yes';"/></td>
			</tr>-->

			
			<!-- <tr>
				<th valign="top">Price Start Date:</th>
				<td><input type="text" name="from" id="from" class="date required" readonly /></td>
				<td></td>
			</tr>
			<tr>
				<th valign="top">Price End Date:</th>
				<td><input type="text" id="to" name="to" class="date required" readonly /></td>
				<td></td>
			</tr> -->
			<?php } ?>

			<tr>
				<th valign="top">Status:</th>
				<td>
				<select name="status" id="status" class="styledselect_form_3">
				<option value="A" <?php if($auRec[0]->status=='A') echo "selected";?> >Active</option>
				<option value="I" <?php if($auRec[0]->status=='I') echo "selected";?> >Inactive</option>
				</td>
				<td></td>
			</tr>

			<tr>
				<th>&nbsp;</th>
				<td valign="top">
					<input name="account_id" type="hidden" value="<?php echo $_SESSION['accountId']; ?>" />
					<input name="id" type="hidden" id="item_id" value="<?php echo $auRec[0]->item_id; ?>" />
					<input name="case_ids" type="hidden" value="<?php echo $auRec[0]->case_id; ?>">
					<input name="add" type="hidden" value="yes" />
					<input type="reset" value="Reset!" class="form-reset">
					<input type="button" value="Back" class="form-reset" onclick="location.href='item.php';" />
					<input name="submit" class="form-submit" type="submit" id="submit" value="Save" />
				</td>
			</tr>
			</table>
		</form>
			<!-- end id-form  -->
			</td>
			<td>
			<!-- right bar-->
			<?php include("rightbar/item_bar.php") ?>
			<div class="clear"></div>
		</div>
<!-- end related-act-bottom -->
</div>
<!-- end related-activities -->
</td>
</tr>
<tr>
<td><img src="images/blank.gif" width="695" height="1" alt="blank" /></td>
<td></td>
</tr>
</table>
<div class="clear"></div>
</div>
<!--  end content-table-inner  -->
</td>
<td id="tbl-border-right"></td>
</tr>
<!--<tr>
	<th class="sized bottomleft"></th>
	<td id="tbl-border-bottom">&nbsp;</td>
	<th class="sized bottomright"></th>
</tr>-->
</table>
<div class="clear">&nbsp;</div>

</div>
<!--  end content -->
<div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->
<div class="clear">&nbsp;</div>
    
<!-- start footer --> 
<?php include("footer.php") ?>  
<!-- end footer -->
</body>
</html>


  <link rel="stylesheet" href="docsupport/prism.css">
  <link rel="stylesheet" href="docsupport/chosen.css">
  <style type="text/css" media="all">
    /* fix rtl for demo */
    .chosen-rtl .chosen-drop { left: -9000px; }
	
	#smsform label.error {
	margin-left: 10px;
	width: auto;
	display: inline;
	}
	.error {
		color: #FF0000;
		font:normal 12px tahoma;
	}

		input, textarea {
		width:auto;
		}

  </style>

		  <script src="docsupport/chosen.jquery.js" type="text/javascript"></script>

		  <script type="text/javascript">
			var config = {
			  '.chosen-select'           : {},
			  '.chosen-select-deselect'  : {allow_single_deselect:true},
			  '.chosen-select-no-single' : {disable_search_threshold:10},
			  '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
			  '.chosen-select-width'     : {width:"100%"}
			}
			for (var selector in config) {
			  $(selector).chosen(config[selector]);
			}
			</script>

			<script>
				$("#add_divisions").change(function(){
				    var selected_division = $("#add_divisions").val();
				    if(selected_division=='4')
				    {
				    	$("#currency_sym_mrp").text("US Dollars");
				    	$("#currency_sym_dp").text("US Dollars");
				    }
				    else
				    {
				    	$("#currency_sym_mrp").text("INR");
				    	$("#currency_sym_dp").text("INR");
				    }
				});
			</script>
			<script type="text/javascript">
				$(document).ready(function(){
					$("#item_erp_code_old").prop("disabled","disabled");
					$("#item_erp_code").prop("required","required");
				});
				$("input[name=choose_bsn]").change(function(){
					var new_old = $("input[name=choose_bsn]:checked").val();
					if(new_old=='old')
					{
						$("#item_erp_code_old").prop("required","required");
						$("#item_erp_code_old").prop("disabled",false);
						$("#item_erp_code").prop("required",false);
						$("#item_erp_code").prop("disabled","disabled");
					}
					else
					{
						$("#item_erp_code").prop("required","required");
						$("#item_erp_code").prop("disabled",false);
						$("#item_erp_code_old").prop("required",false);
						$("#item_erp_code_old").prop("disabled","disabled");
					}
				});
			</script>
