<?php
include("includes/config.inc.php");
include("includes/function.php");
$page_name = "Room Service";
$_objAdmin = new Admin();
$objArrayList = new ArrayList();

if (isset($_POST['add']) && $_POST['add'] == 'yes') {
    if ($_POST['service_id'] != "") {
        $condi = " service_name='" . $_POST['service_name'] . "' and discount_percentage = '" . $_POST['discount_percentage'] . "' and service_id<>'" . $_POST['service_id'] . "'";
    } else {
        $condi = " service_name='" . $_POST['service_name'] . "' and service_id ='" . $_POST['service_id'] . "'";
    }
    $auRec = $_objAdmin->_getSelectList('table_room_services', "*", '', $condi);

    if (is_array($auRec) && sizeof($auRec) > 0) {
        $err = "Room Service already exists in the system.";
    } else {
        if ($_POST['service_id'] != "") {
            $cid = $_objAdmin->updateRoomService($_POST['service_id']);
            $sus = "Room Service has been updated successfully.";
        } else {
            $cid = $_objAdmin->addRoomService();
            $sus = "Room Service has been added successfully.";
        }
        ?>
        <script type="text/javascript">
            window.location = "room_services.php?suc=" + '<?php echo $sus; ?>';
        </script>
        <?php
    }
}


if (isset($_REQUEST['id']) && $_REQUEST['id'] != "") {
    $auRec = $_objAdmin->_getSelectList('table_room_services', "*", '', " service_id=" . $_REQUEST['id']);
    if (count($auRec) <= 0)
        header("Location: room_services.php");
}
?>
<?php
include("header.inc.php");
//$pageAccess=1;
//$check=$objArrayList->checkAccess($pageAccess, basename($_SERVER['PHP_SELF']));
//if($check == false){
//header('Location: ' . $_SERVER['HTTP_REFERER']);
//}
?>
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <!-- BEGIN: Subheader -->
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">
                    Room Services
                </h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="#" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__separator">
                        -
                    </li>
                    <li class="m-nav__item">
                        <a href="#" class="m-nav__link">
                            <span class="m-nav__link-text">
                                Master
                            </span>
                        </a>
                    </li>
                    <li class="m-nav__separator">
                        -
                    </li>
                    <li class="m-nav__item">
                        <a href="" class="m-nav__link">
                            <span class="m-nav__link-text">
                                Room Services
                            </span>
                        </a>
                    </li>  
                </ul>
            </div> 
        </div>
    </div>
    <!-- END: Subheader -->
    <div class="m-content">
        <div class="row">
            <div class="col-lg-12">
                <!--begin::Portlet--> 
                <div class="m-portlet">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <span class="m-portlet__head-icon m--hide">
                                    <i class="la la-gear"></i>
                                </span>
                                <h3 class="m-portlet__head-text">
                                    Room Services Form
                                </h3>
                            </div>
                        </div>
                    </div>
                    <!--begin::Form-->
                    <form class="m-form m-form--fit m-form--label-align-right" id="m_form_1"  method="post" action="addRoomService.php" enctype="multipart/form-data">

                        <div class="m-portlet__body">
                            <div class="m-form__content">
                                <div class="m-alert m-alert--icon alert alert-danger m--hide" role="alert" id="m_form_1_msg">
                                    <div class="m-alert__icon">
                                        <i class="la la-warning"></i>
                                    </div>
                                    <div class="m-alert__text">
                                        Oh snap! Change a few things up and try submitting again.
                                    </div>
                                    <div class="m-alert__close">
                                        <button type="button" class="close" data-close="alert" aria-label="Close"></button>
                                    </div>
                                </div>
                            </div>
<?php if (isset($err) && !empty($err)) { ?>
                                <div class="m-form__content">
                                    <div class="m-alert m-alert--icon alert alert-danger" role="alert" id="m_form_1_msg1">
                                        <div class="m-alert__icon">
                                            <i class="la la-warning"></i>
                                        </div>
                                        <div class="m-alert__text">
    <?php echo $err; ?>
                                        </div>
                                        <div class="m-alert__close">
                                            <button type="button" class="close" data-close="alert" aria-label="Close"></button>
                                        </div>
                                    </div>
                                </div>
<?php } ?>
                            <div class="form-group m-form__group row">
                                <div class="col-lg-4">
                                    <label>
                                        Service Name:
                                    </label>
                                    <input type="text" class="form-control m-input" placeholder="Service Name" id="service_name" name="service_name" value="<?php echo $auRec[0]->service_name; ?>">
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <div class="col-lg-4">
                                    <label>
                                        No. of Services:
                                    </label>
                                    <input type="text" class="form-control m-input" placeholder="" id="number_of_services" name="number_of_services" value="<?php echo $auRec[0]->number_of_services; ?>">
                                </div>
                                <div class="col-lg-4">
                                    <label>
                                        Discount %age: 
                                    </label>
                                    <input type="text" class="form-control m-input" placeholder="" id="discount_percentage" name="discount_percentage" value="<?php echo $auRec[0]->discount_percentage; ?>">
                                </div>
                            </div>
                            <div class="form-group m-form__group row"> 
                                <div class="col-lg-4">
                                    <label class="">
                                        Status:
                                    </label>
                                    <div class="m-radio-inline">
                                        <label class="m-radio m-radio--solid">
                                            <input <?php if ($auRec[0]->status == 'A') echo "checked"; ?> type="radio" name="status"  value="A" >
                                            Active
                                            <span></span>
                                        </label>
                                        <label class="m-radio m-radio--solid">
                                            <input type="radio" name="status" value="I" <?php if ($auRec[0]->status == 'I') echo "checked"; ?>>
                                            Inactive
                                            <span></span>
                                        </label>
                                    </div> 
                                </div>
                            </div>
                        </div>
                        <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                            <div class="m-form__actions m-form__actions--solid">
                                <div class="row">
                                    <div class="col-lg-4"></div>
                                    <div class="col-lg-8">
                                        <button type="submit" class="btn btn-success">
                                            Submit
                                        </button>
                                        <button type="reset" class="btn btn-secondary">
                                            Reset
                                        </button>
                                        <a href="room_services.php" class="btn btn-secondary">
                                            back
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <input name="service_id" type="hidden" value="<?php echo $auRec[0]->service_id; ?>" />
                        <input name="account_id" type="hidden" value="<?php echo $_SESSION['accountId']; ?>" />
                        <input type="hidden" name="add" value="yes">

                    </form>
                    <!--end::Form-->
                </div>
                <!--end::Portlet-->
            </div>
        </div>
    </div>
</div>
</div>
</div>
<!-- start footer -->         
<?php include("footer.php") ?>

<script type="text/javascript">
    var FormControls = function () {
        //== Private functions

        var demo1 = function () {
            $("#m_form_1").validate({
                // define validation rules
                rules: {
                    service_name: {
                        required: true
                    },
                    number_of_services: {
                        required: true,
                        number: true
                    },
                    discount_percentage: {
                        required: true,
                        number: true
                    },
                    status: {
                        required: true,
                    }
                },
                //display error alert on form submit  
                invalidHandler: function (event, validator) {
                    var alert = $('#m_form_1_msg');
                    alert.removeClass('m--hide').show();
                    mApp.scrollTo(alert, -200);
                },
                submitHandler: function (form) {
                    form.submit();
                }
            });
        }

        return {
            // public functions
            init: function () {
                demo1();
            }
        };
    }();

    jQuery(document).ready(function () {
        var id = '<?php echo $_REQUEST['id']; ?>';
        FormControls.init();
    });

</script>
