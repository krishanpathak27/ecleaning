<?php
include("includes/config.inc.php");
include("includes/function.php");
$page_name = "Add Room Service Packages";
$_objAdmin = new Admin();
$objArrayList = new ArrayList();

if(isset($_POST['add']) && $_POST['add'] == 'yes'){
    if($_POST['room_package_id'] != ""){
        $condi = " category_id = '" . $_POST['category_id'] . "' and cleaning_type_id = '" . $_POST['cleaning_type_id'] . "' and room_package_id<>'" . $_POST['room_package_id'] . "'";
    }else{
        $condi = " room_package_id ='" . $_POST['id'] . "'";
    }
    $auRec = $_objAdmin->_getSelectList('table_room_packages', "*", '', $condi);

    if (is_array($auRec) && sizeof($auRec) > 0) {
        $err = "Room Service Package already exists in the system.";
        $auRec[0] = (object) $_POST;
    } else {
        if ($_POST['room_package_id'] != "") {
            $cid = $_objAdmin->updateRoomServicePackage($_POST['room_package_id']);
            $sus = "Room Service Package has been updated successfully.";
        } else {
            $cid = $_objAdmin->addRoomServicePackage();
            $sus = "Room Service Package has been added successfully.";
        }
        ?>
        <script type="text/javascript">
            window.location = "room_service_packages.php?suc=" + '<?php echo $sus; ?>';
        </script>
        <?php
    }
}


if (isset($_REQUEST['id']) && $_REQUEST['id'] != "") {
    $auRec = $_objAdmin->_getSelectList('table_room_packages', "*", '', " room_package_id=" . $_REQUEST['id']);
    if (count($auRec) <= 0)
        header("Location: room_service_packages.php");
}
?>
<?php
include("header.inc.php");
//$pageAccess=1;
//$check=$objArrayList->checkAccess($pageAccess, basename($_SERVER['PHP_SELF']));
//if($check == false){
//header('Location: ' . $_SERVER['HTTP_REFERER']);
//}
?>
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <!-- BEGIN: Subheader -->
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">
                     Add Packages
                </h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="#" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__separator">
                        -
                    </li>
                    <li class="m-nav__item">
                        <a href="#" class="m-nav__link">
                            <span class="m-nav__link-text">
                                Master
                            </span>
                        </a>
                    </li>
                    <li class="m-nav__separator">
                        -
                    </li>
                    <li class="m-nav__item">
                        <a href="" class="m-nav__link">
                            <span class="m-nav__link-text">
                                Packages
                            </span>
                        </a>
                    </li>  
                </ul>
            </div> 
        </div>
    </div>
    <!-- END: Subheader -->
    <div class="m-content">
        <div class="row">
            <div class="col-lg-12">
                <!--begin::Portlet--> 
                <div class="m-portlet">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <span class="m-portlet__head-icon m--hide">
                                    <i class="la la-gear"></i>
                                </span>
                                <h3 class="m-portlet__head-text">
                                    Packages Form
                                </h3>
                            </div>
                        </div>
                    </div>
                    <!--begin::Form-->
                    <form class="m-form m-form--fit m-form--label-align-right" id="m_form_1"  method="post" action="addRoomServicePackages.php" enctype="multipart/form-data">

                        <div class="m-portlet__body">
                            <div class="m-form__content">
                                <div class="m-alert m-alert--icon alert alert-danger m--hide" role="alert" id="m_form_1_msg">
                                    <div class="m-alert__icon">
                                        <i class="la la-warning"></i>
                                    </div>
                                    <div class="m-alert__text">
                                        Oh snap! Change a few things up and try submitting again.
                                    </div>
                                    <div class="m-alert__close">
                                        <button type="button" class="close" data-close="alert" aria-label="Close"></button>
                                    </div>
                                </div>
                            </div>
<?php if (isset($err) && !empty($err)) { ?>
                                <div class="m-form__content">
                                    <div class="m-alert m-alert--icon alert alert-danger" role="alert" id="m_form_1_msg1">
                                        <div class="m-alert__icon">
                                            <i class="la la-warning"></i>
                                        </div>
                                        <div class="m-alert__text">
    <?php echo $err; ?>
                                        </div>
                                        <div class="m-alert__close">
                                            <button type="button" class="close" data-close="alert" aria-label="Close"></button>
                                        </div>
                                    </div>
                                </div>
<?php } ?>
                            <div class="form-group m-form__group row">
                                <div class="col-lg-4">
                                    <label>
                                        Unit Type:
                                    </label>
                                    <div class="input-group m-input-group m-input-group--square"> 
                                        <select name="unit_type_id" id="unit_type_id" class="required styledselect_form_3 unit_type_id form-control" onchange="showCleanerDropDown();">
                                            <option value="">Select Unit Type</option>
<?php
$unitType = $_objAdmin->_getSelectList2('table_unit_type', 'unit_type_id,unit_name', '', " status = 'A' ORDER BY unit_name");
if (is_array($unitType)) {
    foreach ($unitType as $value):
        ?>
                                                    <option value="<?php echo $value->unit_type_id; ?>" <?php if ($value->unit_type_id == $auRec[0]->unit_type_id) echo "selected"; ?> ><?php echo $value->unit_name; ?></option>
    <?php endforeach;
}
?>
                                        </select>
                                    </div> 
                                </div>
                                <div class="col-lg-4">
                                    <label>
                                        Cleaning Type:
                                    </label>
                                    <div class="input-group m-input-group m-input-group--square"  id="cleaningTypedrop"> 

                                    </div> 
                                </div>

                               
                                <div class="col-lg-4">
                                    <label>
                                        Category:
                                    </label>
                                   <div class="input-group m-input-group m-input-group--square"  id="categorydropdown"> 

                                    </div> 
                                   
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                              
                                
                                <div class="col-lg-4">
                                    <label>
                                        No Of Category: 
                                    </label>
                                    <input type="text" class="form-control m-input" placeholder="" id="counter" name="counter" value="<?php echo $auRec[0]->counter; ?>">
                                </div>
                                 <div class="col-lg-4">
                                    <label>
                                        Package Rate: 
                                    </label>
                                    <input type="text" class="form-control m-input" placeholder="" id="package_rate" name="package_rate" value="<?php echo $auRec[0]->package_rate; ?>">
                                </div>
                                <div class="col-lg-4">
                                    <label>
                                        Linen Change per Room: 
                                    </label>
                                    <input type="text" class="form-control m-input" placeholder="" id="linen_change_per_room" name="linen_change_per_room" value="<?php echo $auRec[0]->linen_change_per_room; ?>">
                                </div>
                              
                                </div>

                                <div class="form-group m-form__group row"> 
                                    <div class="col-lg-4">
                                        <label>
                                            Balcony Cleaning per Room: 
                                        </label>
                                        <input type="text" class="form-control m-input" placeholder="" id="balcony_price" name="balcony_price" value="<?php echo $auRec[0]->balcony_price; ?>">
                                    </div>
                                </div>
                                <div class="form-group m-form__group row"> 
                                    <div class="col-lg-4">
                                        <label class="">
                                            Status:
                                        </label>
                                        <div class="m-radio-inline">
                                            <label class="m-radio m-radio--solid">
                                                <input <?php if ($auRec[0]->status == 'A') echo "checked"; ?> type="radio" name="status"  value="A" >
                                                Active
                                                <span></span>
                                            </label>
                                            <label class="m-radio m-radio--solid">
                                                <input type="radio" name="status" value="I" <?php if ($auRec[0]->status == 'I') echo "checked"; ?>>
                                                Inactive
                                                <span></span>
                                            </label>
                                        </div> 
                                    </div>
                                </div>
                            </div>
                            <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                                <div class="m-form__actions m-form__actions--solid">
                                    <div class="row">
                                        <div class="col-lg-4"></div>
                                        <div class="col-lg-8">
                                            <button type="submit" class="btn btn-success">
                                                Submit
                                            </button>
                                            <button type="reset" class="btn btn-secondary">
                                                Reset
                                            </button>
                                            <a href="room_service_packages.php" class="btn btn-secondary">
                                                back
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <input name="room_package_id" type="hidden" value="<?php echo $auRec[0]->room_package_id; ?>" />
                        <input name="account_id" type="hidden" value="<?php echo $_SESSION['accountId']; ?>" />
                        <input type="hidden" name="add" value="yes">

                    </form>
                    <!--end::Form-->
                </div>
                <!--end::Portlet-->
            </div>
        </div>
    </div>
</div>
</div>
</div>
<!-- start footer -->         
<?php include("footer.php") ?>

<script type="text/javascript">
    var FormControls = function () {
        //== Private functions

        var demo1 = function () {
            $("#m_form_1").validate({
                // define validation rules
                rules: {
                    service_id: {
                        required: true
                    },
                    service_type: {
                        required: true
                    },
                    category_id: {
                        required: true,
                    },
                    cleaning_type_id: {
                        required: true,
                    },
                    unit_type_id: {
                        required: true,
                    },
                    package_rate: {
                        required: true,
                    },
                    counter: {
                        required: true,
                    },
                    status: {
                        required: true,
                    }
                },
                //display error alert on form submit  
                invalidHandler: function (event,validator){
                    var alert = $('#m_form_1_msg');
                    alert.removeClass('m--hide').show();
                    mApp.scrollTo(alert, -200);
                },
                submitHandler: function(form){
                    form.submit();
                }
            });
        }

        return {
            // public functions
            init: function () {
                demo1();
            }
        };
    }();

    // jQuery(document).ready(function(){
    //     var id = '<?php echo $_REQUEST['id']; ?>';
    //     FormControls.init();
    // });
     jQuery(document).ready(function(){
        var id = '<?php echo $_REQUEST['id']; ?>';
        showCleanerDropDown();
        showCategoryDropdown();
        if(id!=''){
            <?php if($auRec[0]->service_type == 'o'){ ?>
                $('#service_id').attr("disabled","disabled");
            <?php } else{ ?>
                $('#service_id').removeAttr('disabled');
            <?php } ?> 
        }else{
            $('#service_id').attr("disabled","disabled");   
        }
        
        FormControls.init();
        
    });

    function showCleanerDropDown(){
        var unitTypeId = document.getElementById("unit_type_id").value;
        <?php if($auRec[0]->cleaning_type_id != ''){ ?>
            var cleaningTypeId = '<?php echo $auRec[0]->cleaning_type_id; ?>';
        <?php }else{ ?>
            var cleaningTypeId = '';
        <?php } ?>
        $.ajax({
            'type': 'POST',
            'url': 'cleaner_type_dropdown.php',
            'data': 'unit_type_id=' + unitTypeId + '&cleaningTypeId=' + cleaningTypeId,
            'success': function(mystring){
                document.getElementById("cleaningTypedrop").innerHTML = mystring;

            }
        });
    }

    function showCategoryDropdown(){
        var unitTypeId = document.getElementById("unit_type_id").value;

        
       
        if(document.getElementById("cleaning_type_id")==null){
        <?php if($auRec[0]->cleaning_type_id != ''){ ?>
            var cleaningTypeId = '<?php echo $auRec[0]->cleaning_type_id; ?>';
        <?php }else{?>
             var cleaningTypeId = '';
        <?php }?>
        }else{
             var cleaningTypeId = document.getElementById("cleaning_type_id").value;
        }
         <?php if($auRec[0]->category_id != ''){ ?>
            var categoryId = '<?php echo $auRec[0]->category_id; ?>';
        <?php }else{?>
             var categoryId = '';
        <?php }?>


        $.ajax({
            'type': 'POST',
            'url': 'category_dropdown.php',
            'data': 'unit_type_id=' + unitTypeId + '&cleaning_type_id='+cleaningTypeId+'&category_id=' + categoryId,
            'success': function(mystring){
                document.getElementById("categorydropdown").innerHTML = mystring;

            }
        });
    }


    function showLongTermService(){

        var selectedService = $("#service_type option:selected").val();
        if(selectedService=="o")
            $('#service_id').attr("disabled","disabled");
        else
            $('#service_id').removeAttr('disabled');
    };



</script>

