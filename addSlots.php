
<?php
include("includes/config.inc.php");
include("includes/function.php");
$_objAdmin = new Admin();
if (isset($_POST['add']) && $_POST['add'] == 'yes'){
    $err = '';
    if ($_POST['slot_id'] != "") {
        $condi = " slot_start_time='" . date("H:i",strtotime($_POST['slot_start_time'])) . "' and slot_end_time='" . date("H:i",strtotime($_POST['slot_end_time'])) . "' and slot_id<>" . $_POST['slot_id'] . "";
    }else{
        $condi = " slot_start_time='" . date("H:i:",strtotime($_POST['slot_start_time'])) . "' and slot_end_time='" . date("H:i",strtotime($_POST['slot_end_time'])) . "' ";
    }
       
    $auRec = $_objAdmin->_getSelectList('table_calender_slots', "*", '', $condi);
    if(is_array($auRec)){
        $err = "Slot already exist.";
    }
    if($err == ''){
        if ($_POST['slot_id'] != "" && $_POST['slot_id'] != 0) {
            $save = $_objAdmin->updateCalenderSlot($_POST["slot_id"]);
            $sus = "Updated Successfully";
        } else {
            $save = $_objAdmin->addCalenderSlot();
            $sus = "Added Successfully";
        }
        ?>
        <script type="text/javascript">
            window.location = "slots_listing.php?suc=" + '<?php echo $sus; ?>';
        </script>
        <?php
    }
}
if (isset($_REQUEST['id']) && $_REQUEST['id'] != "") {
    //$auRec=$_objAdmin->_getSelectList('table_category_type as CT left join table_unit_type as u on u.unit_type_id = CT.unit_type_id left join table_cleaning_type as c on c.cleaning_type_id=CT.cleaning_type_id',"CT.*,u.unit_name,u.unit_type_id,c.cleaning_type_id,c.cleaning_type",''," CT.room_category_id=".$_REQUEST['id']);

    $auRec=$_objAdmin->_getSelectList('table_calender_slots as tcs',"tcs.*",''," tcs.slot_id=".$_REQUEST['id']);
    //echo date("h:i A",strtotime($auRec[0]->slot_start_time)); die;
    if (count($auRec) <= 0)
        header("Location: slots_listing.php");
}

include("header.inc.php");
?>

<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <!-- BEGIN: Subheader -->
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">
                    Day Slot
                </h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="#" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__separator">
                        -
                    </li>
                    <li class="m-nav__item">
                        <a href="#" class="m-nav__link">
                            <span class="m-nav__link-text">
                                Master
                            </span>
                        </a>
                    </li>
                    <li class="m-nav__separator">
                        -
                    </li>
                    <li class="m-nav__item">
                        <a href="" class="m-nav__link">
                            <span class="m-nav__link-text">
                                Day Slot
                            </span>
                        </a>
                    </li>  
                </ul>
            </div> 
        </div>
    </div>
    <!-- END: Subheader -->
    <div class="m-content">
        <div class="row">
            <div class="col-lg-12">
                <!--begin::Portlet--> 
                <div class="m-portlet">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <span class="m-portlet__head-icon m--hide">
                                    <i class="la la-gear"></i>
                                </span>
                                <h3 class="m-portlet__head-text">
                                    Day Slot Form
                                </h3>
                            </div>
                        </div>
                    </div>
                    <!--begin::Form-->
                    <form class="m-form m-form--fit m-form--label-align-right" id="m_form_1"  method="post" action="addSlots.php" enctype="multipart/form-data">

                        <div class="m-portlet__body">
                            <div class="m-form__content">
                                <div class="m-alert m-alert--icon alert alert-danger m--hide" role="alert" id="m_form_1_msg">
                                    <div class="m-alert__icon">
                                        <i class="la la-warning"></i>
                                    </div>
                                    <div class="m-alert__text">
                                        Oh snap! Change a few things up and try submitting again.
                                    </div>
                                    <div class="m-alert__close">
                                        <button type="button" class="close" data-close="alert" aria-label="Close"></button>
                                    </div>
                                </div>
                            </div>
                            <?php if (isset($err) && !empty($err)) { ?>
                                <div class="m-form__content">
                                    <div class="m-alert m-alert--icon alert alert-danger" role="alert" id="m_form_1_msg1">
                                        <div class="m-alert__icon">
                                            <i class="la la-warning"></i>
                                        </div>
                                        <div class="m-alert__text">
                                            <?php echo $err; ?>
                                        </div>
                                        <div class="m-alert__close">
                                            <button type="button" class="close" data-close="alert" aria-label="Close"></button>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                           <!--  <div class="form-group m-form__group row">
                                <div class="col-lg-4">
                                    <label>
                                        Week Day:
                                    </label>
                                      <div class="input-group m-input-group m-input-group--square"> 
                                        <select class="form-control m-input m-input--square" id="week_day_id" name="week_day_id">
                                            <option value="">Select Week Day</option>
                                            <?php
                                            $condi = "";

                                                $weekDayList = $_objAdmin->_getSelectList('table_week_days as twd', "twd.week_day_id,twd.week_day", '', " twd.status='A'");
                                           

                                            for($i = 0;$i < count($weekDayList); $i++){

                                                if($weekDayList[$i]->week_day_id == $auRec[0]->week_day_id){
                                                    $select = "selected";
                                                } else {
                                                    $select = "";
                                                }
                                                ?>
                                                <option value="<?php echo $weekDayList[$i]->week_day_id; ?>" <?php echo $select ?>><?php echo $weekDayList[$i]->week_day; ?></option>

                                            <?php } ?>
                                           
                                        </select>
                                    </div>
                                   
                                </div>
                            </div> -->
                            
                            <div class="form-group m-form__group row">
                                <div class="input-daterange input-group">
                                    <div class="col-lg-4">
                                        <label class="">
                                            Slot Start Time: 
                                        </label>
                                        <div class="m-input-icon m-input-icon--right">
                                            <input type="text" class="form-control m-input" id="slot_start_time" name="slot_start_time" style="width:100%" value="<?php if(isset($auRec[0]->slot_start_time) && !empty($auRec[0]->slot_start_time)) { echo date("h:i A",strtotime($auRec[0]->slot_start_time));  }   ?>"> 
                                        </div> 
                                    </div>
                                    <div class="col-lg-4">
                                        <label class="">
                                            Slot End Time: 
                                        </label>
                                        <div class="m-input-icon m-input-icon--right">
                                            <input type="text" class="form-control m-input" id="slot_end_time" name="slot_end_time" style="width:100%" value="<?php if(isset($auRec[0]->slot_end_time) && !empty($auRec[0]->slot_end_time)) { echo date("h:i A",strtotime($auRec[0]->slot_end_time));}?>"> 
                                        </div> 
                                    </div>
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <div class="col-lg-4">
                                    <label class="">
                                        Status:
                                    </label>
                                    <div class="m-radio-inline">
                                        <label class="m-radio m-radio--solid">
                                            <input <?php if ($auRec[0]->status == 'A') echo "checked"; ?> type="radio" name="status"  value="A" >
                                            Active
                                            <span></span>
                                        </label>
                                        <label class="m-radio m-radio--solid">
                                            <input type="radio" name="status" value="I" <?php if ($auRec[0]->status == 'I') echo "checked"; ?>>
                                            Inactive
                                            <span></span>
                                        </label>
                                    </div> 
                                </div>
                            </div>   
                        </div>
                            
                    </div>
                    <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                        <div class="m-form__actions m-form__actions--solid">
                            <div class="row">
                                <div class="col-lg-4"></div>
                                <div class="col-lg-8">
                                    <button type="submit" class="btn btn-success">
                                        Submit
                                    </button>
                                    <button type="reset" class="btn btn-secondary">
                                        Reset
                                    </button>
                                    <a href="slots_listing.php" class="btn btn-secondary">
                                        back
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                        <input name="slot_id" type="hidden" value="<?php echo $auRec[0]->slot_id; ?>" />
                        <input name="account_id" type="hidden" value="<?php echo $_SESSION['accountId']; ?>" />
                        <input type="hidden" name="add" value="yes">

                    </form>
                    <!--end::Form-->
                </div>
                <!--end::Portlet-->
            </div>
        </div>
    </div>
</div>
</div>
</div>
<!-- start footer -->         
<?php include("footer.php") ?>

<script type="text/javascript">
    var FormControls = function () {
        //== Private functions

        var demo1 = function () {
            $("#m_form_1").validate({
                // define validation rules
                rules: {
                    week_day_id: {
                        required: true
                    },
                    slot_start_time: {
                        required: true
                    },
                    slot_end_time: {
                        required: true
                    },
                    
                    status: {
                        required: true,
                    }
                },
                
                //display error alert on form submit  
                invalidHandler: function (event, validator) {
                    var alert = $('#m_form_1_msg');
                    alert.removeClass('m--hide').show();
                    mApp.scrollTo(alert, -200);
                },
                submitHandler: function (form) {
                    form.submit();
                }
            });
        }

        return {
            // public functions
            init: function () {
                demo1();
            }
        };
    }();

    jQuery(document).ready(function () {
        FormControls.init();
        
    });
    
    var BootstrapTimepicker = function () {
    
    //== Private functions
    var demos = function () {
        // minimum setup
        $('#slot_start_time, #slot_end_time').timepicker();
    }

    return {
        // public functions
        init: function() {
            demos(); 
        }
    };
}();

jQuery(document).ready(function() {    
    BootstrapTimepicker.init();
});

</script>
