<?php
include("includes/config.inc.php");
include("includes/function.php");
$_objAdmin = new Admin();
if (isset($_POST['add']) && $_POST['add'] == 'yes') {
    $err = '';
    if ($_POST['unit_type_id'] != "") {
        $condi = " unit_name='" . $_POST['unit_name'] . "'  and unit_type_id<>'" . $_POST['unit_type_id'] . "'";
    } else {
        $condi = " unit_name='" . $_POST['unit_name'] . "' ";
    }
    $auRec = $_objAdmin->_getSelectList('table_unit_type', "*", '', $condi);
    if (is_array($auRec)) {
        $err = "Unit Type already exists in the system.";
    }
    if ($_FILES["unit_image"]['size'] != 0) {
        $check1 = getimagesize($_FILES["unit_image"]["tmp_name"]);
        if ($check1 == false) {
            $err = "unit image 1 is not an image.";
        }
    }
    if ($_FILES["unit_image2"]['size'] != 0) {
        $check2 = getimagesize($_FILES["unit_image2"]["tmp_name"]);
        if ($check2 == false) {
            $err = "unit image 2 is not an image.";
        }
    }
    if ($err == '') {

        if ($_POST['unit_type_id'] != "" && $_POST['unit_type_id'] != 0) {
            $save = $_objAdmin->updateUnitType($_POST["unit_type_id"]);
            $sus = "Updated Successfully";
        } else {
            $save = $_objAdmin->addUnitType();
            $sus = "Added Successfully";
        }
        ?>
        <script type="text/javascript">
            window.location = "unitType.php?suc=" + '<?php echo $sus; ?>';
        </script>
        <?php
    }
}
if(isset($_REQUEST['id']) && $_REQUEST['id']!="")
{
    $auRec=$_objAdmin->_getSelectList('table_unit_type',"*",''," unit_type_id=".$_REQUEST['id']); 
    
    if(count($auRec)<=0) header("Location: cleaningType.php");
}
include("header.inc.php");
?>
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <!-- BEGIN: Subheader -->
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">
                    Unit Type
                </h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="#" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__separator">
                        -
                    </li>
                    <li class="m-nav__item">
                        <a href="#" class="m-nav__link">
                            <span class="m-nav__link-text">
                                Master
                            </span>
                        </a>
                    </li>
                    <li class="m-nav__separator">
                        -
                    </li>
                    <li class="m-nav__item">
                        <a href="" class="m-nav__link">
                            <span class="m-nav__link-text">
                                Unit Type
                            </span>
                        </a>
                    </li>  
                </ul>
            </div> 
        </div>
    </div>
    <!-- END: Subheader -->
    <div class="m-content">
        <div class="row">
            <div class="col-lg-12">
                <!--begin::Portlet--> 
                <div class="m-portlet">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <span class="m-portlet__head-icon m--hide">
                                    <i class="la la-gear"></i>
                                </span>
                                <h3 class="m-portlet__head-text">
                                    Unit Type Form
                                </h3>
                            </div>
                        </div>
                    </div>
                    <!--begin::Form-->
                    <form class="m-form m-form--fit m-form--label-align-right" id="m_form_1"  method="post" action="addUnitType.php" enctype="multipart/form-data">

                        <div class="m-portlet__body">
                            <div class="m-form__content">
                                <div class="m-alert m-alert--icon alert alert-danger m--hide" role="alert" id="m_form_1_msg">
                                    <div class="m-alert__icon">
                                        <i class="la la-warning"></i>
                                    </div>
                                    <div class="m-alert__text">
                                        Oh snap! Change a few things up and try submitting again.
                                    </div>
                                    <div class="m-alert__close">
                                        <button type="button" class="close" data-close="alert" aria-label="Close"></button>
                                    </div>
                                </div>
                            </div>
                            <?php if (isset($err) && !empty($err)) { ?>
                                <div class="m-form__content">
                                    <div class="m-alert m-alert--icon alert alert-danger" role="alert" id="m_form_1_msg1">
                                        <div class="m-alert__icon">
                                            <i class="la la-warning"></i>
                                        </div>
                                        <div class="m-alert__text">
                                            <?php echo $err; ?>
                                        </div>
                                        <div class="m-alert__close">
                                            <button type="button" class="close" data-close="alert" aria-label="Close"></button>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                            <div class="form-group m-form__group row">
                                <div class="col-lg-4">
                                    <label>
                                        Unit Type:
                                    </label>
                                    <input type="text" class="form-control m-input" placeholder="Unit Type" id="unit_name" name="unit_name" value="<?php echo $auRec[0]->unit_name; ?>">
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                    <div class="col-lg-4">
                                    <label>
                                        Unit Image 1:
                                    </label>
    <span style="margin: 10px;display: inline-block;"> 
    <?php 
    if($auRec[0]->unit_image)         
        echo '<img id="image_upload_preview" src="images/unitType/'.$auRec[0]->unit_image.'" height="50" width="50">';   
    else      
       echo '<img id="image_upload_preview" src="images/unitType/noimageavailable.png" height="50" width="50">';    
    ?> 
    </span>


                                    <input type="file" id="inputFile" class="form-control m-input" placeholder="Unit Type" id="unit_image" name="unit_image" >
                                </div>
                                <div class="col-lg-4">
                                    <label>
                                        Unit Image 2:
                                    </label>
    <span style="margin: 10px;display: inline-block;"> 
    <?php 
    if($auRec[0]->unit_image2)   
        echo '<img id="image_upload_preview2" src="images/unitType/'.$auRec[0]->unit_image2.'" height="50" width="50">';
     else
        echo '<img id="image_upload_preview2" src="images/unitType/noimageavailable.png" height="50" width="50">';   
    ?> 
    </span>
                                    <input type="file" id="inputFile2" class="form-control m-input" placeholder="Unit Type" id="unit_image2" name="unit_image2">
                                </div>
                            </div>
                            <div class="form-group m-form__group row"> 
                                <div class="col-lg-4">
                                    <label class="">
                                        Status:
                                    </label>
                                    <div class="m-radio-inline">
                                        <label class="m-radio m-radio--solid">
                                            <input <?php if ($auRec[0]->status == 'A') echo "checked"; ?> type="radio" name="status"  value="A" >
                                            Active
                                            <span></span>
                                        </label>
                                        <label class="m-radio m-radio--solid">
                                            <input type="radio" name="status" value="I" <?php if ($auRec[0]->status == 'I') echo "checked"; ?>>
                                            Inactive
                                            <span></span>
                                        </label>
                                    </div> 
                                </div>
                            </div>
                        </div>
                        

                        <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                            <div class="m-form__actions m-form__actions--solid">
                                <div class="row">
                                    <div class="col-lg-4"></div>
                                    <div class="col-lg-8">
                                        <button type="submit" class="btn btn-success">
                                            Submit
                                        </button>
                                        <button type="reset" class="btn btn-secondary">
                                            Reset
                                        </button>
                                        <a href="unitType.php" class="btn btn-secondary">
                                                back
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <input name="unit_type_id" type="hidden" value="<?php echo $auRec[0]->unit_type_id; ?>" />
                        <input name="account_id" type="hidden" value="<?php echo $_SESSION['accountId']; ?>" />
                        <input type="hidden" name="add" value="yes">

                    </form>
                    <!--end::Form-->
                </div>
                <!--end::Portlet-->
            </div>
        </div>
    </div>
</div>
</div>
</div>
<!-- start footer -->         
<?php include("footer.php") ?>

<script type="text/javascript">
    var FormControls = function () {
        //== Private functions

        var demo1 = function () {
            $("#m_form_1").validate({
                // define validation rules
                rules: {
                    unit_name: {
                        required: true
                    },
                    unit_image: {
                        accept:"jpg,png,jpeg,gif"
                    },
                    unit_image2: {
                        accept:"jpg,png,jpeg,gif"
                    },
                    status: {
                        required: true,
                    }
                },
                messages: {

                unit_image:{
                        accept: "Only image type jpg/png/jpeg/gif is allowed"
                    } ,
                    unit_image2:{
                        accept: "Only image type jpg/png/jpeg/gif is allowed"
                    }

                 }  , 
                //display error alert on form submit  
                invalidHandler: function (event, validator) {
                    var alert = $('#m_form_1_msg');
                    alert.removeClass('m--hide').show();
                    mApp.scrollTo(alert, -200);
                },
                submitHandler: function (form) {
                    form.submit();
                }
            });
        }

        return {
            // public functions
            init: function () {
                demo1();
            }
        };
    }();

    jQuery(document).ready(function () {
        var id = '<?php echo $_REQUEST['id']; ?>';
        FormControls.init();
        if (id) {
            $('#unit_image').rules('add', {
                accept:"jpg,png,jpeg,gif"
            });
            $('#unit_image2').rules('add', {
                accept:"jpg,png,jpeg,gif"
            });
        }
    });

</script>
<script type="text/javascript">
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#image_upload_preview').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }
    $("#inputFile").change(function () {
        readURL(this);
    });

    function readURL2(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#image_upload_preview2').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }
    $("#inputFile2").change(function () {
        readURL2(this);
    });

</script>


