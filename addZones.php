
<?php
include("includes/config.inc.php");
include("includes/function.php");
$page_name="Add/Edit Zones";
$_objAdmin = new Admin();

if(isset($_POST['add']) && $_POST['add'] == 'yes')
{
	if($_REQUEST['id']!="") 
	{

		$condi=	"zone_name='".$_POST['zone_name']."' and  zone_id ='".$_REQUEST['id']."' and status!='D'";
	}
	else
	{
		$condi=	" zone_name='".$_POST['zone_name']."' and status!='D'";
	}
	$auRec=$_objAdmin->_getSelectList2('table_zone',"*",'',$condi);
	if(is_array($auRec))
	{
		$err="Zone already exists in the system.";	
		$auRec[0]=(object)$_POST;						
	}
	else
	{
		if($_REQUEST['id']!="") 
		{
			$cid=$_objAdmin->updateZones($_REQUEST['id']);
			$sus="Zone has been updated successfully.";
		}
		else 
		{   
			$cid = $_objAdmin->addZone();
			$sus="Zone has been added successfully.";
		}
	}
}
if(isset($_REQUEST['id']) && $_REQUEST['id']!="")
{
	$condi = " zone_id='".$_REQUEST['id']."'";
	$auRec=$_objAdmin->_getSelectList2('table_zone as z left join table_region r on z.region_id=r.region_id left join country as c on c.country_id = r.country_id',"z.*, z.region_id,r.country_id",'',$condi);

			//$auRec2=$_objAdmin->_getSelectList('table_salesman_allwns_slab_category_city_relation AS CR',"CR.*",''," CR.status = 'A' AND  CR.city_id=".$_REQUEST['id']);
		
		
		
	if(count($auRec)<=0) header("Location: zones.php");
}

?>

<?php include("header.inc.php") ?>

<script src="javascripts/jquery-1.8.2.js" type="text/javascript"></script>
<script src="javascripts/jquery-ui.js"></script>
<script type="text/javascript" src="javascripts/validate.js"></script>
<script type="text/javascript" src="javascripts/select_dropdowns.js"></script>
<script type="text/javascript">
$(document).ready(function() {
	var v = $("#frmPre").validate({
			rules: {
				fileToUpload: {
				  required: true,
				  accept: "csv"
				}
			  },
			submitHandler: function(form) {
				document.frmPre.submit();				
			}
		});
});

</script>

<!-- start content-outer -->
<div id="content-outer">
<!-- start content -->
	<div id="content">
	<div id="page-heading"><h1><span style="color: #d74343; font-family: Tahoma; font-weight: bold;">Add Zone</span></h1></div>
	<table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">
		<tr>
			<td>
			<!--  start content-table-inner -->
			<div id="content-table-inner">
				<table border="0" width="100%" cellpadding="0" cellspacing="0">
				<tr valign="top">
					<td>
					<!--  start message-red -->
					<?php if($err!=''){?>
					<div id="message-red">
						<table border="0" width="100%" cellpadding="0" cellspacing="0">
						<tr>
							<td class="red-left"> <?php echo $err; ?></td>
							<td class="red-right"><a class="close-red"><img src="images/icon_close_red.gif"   alt="" /></a></td>
						</tr>
						</table>
					</div>
					<?php } ?>
					<!--  end message-red -->
					<?php if($sus!=''){?>
					<!--  start message-green -->
					<div id="message-green">
						<table border="0" width="100%" cellpadding="0" cellspacing="0">
						<tr>
							<td class="green-left"><?php echo $sus; ?></td>
							<td class="green-right"><a class="close-green"><img src="images/icon_close_green.gif"   alt="" /></a></td>
						</tr>
						</table>
					</div>
					<?php } ?>
					<!--  end message-green -->
				
			<!-- start id-form -->
			<?php 
				// echo "<pre>";
				// print_r($auRec);
			?>
		<form name="frmPre" id="frmPre" method="post" action="addZones.php" enctype="multipart/form-data" >
			<table border="0" cellpadding="0" cellspacing="0"  id="id-form">
			<tr>
				<th valign="top">Select Country:</th>
				<td>
				<select name="country_id" id="country_id" class="required styledselect_form_3 country_id">
				<option value="">Please Select</option>
				<?php $country = $_objAdmin->_getSelectList2('country','country_id,country_name',''," status = 'A' ORDER BY country_name"); 
					if(is_array($country)){
					 foreach($country as $value):?>
					 	<option value="<?php echo $value->country_id;?>" <?php if($value->country_id==$auRec[0]->country_id) echo "selected";?> ><?php echo $value->country_name;?></option>
				<?php endforeach; }?>
				</select>
				</td>
				<td></td>
			</tr>
			<tr>
				<th valign="top">Select Region:</th>
				<td>
				<select name="region_id" id="region_id" class="required styledselect_form_3 region_id">
					<option value="">Please Select</option>
					<?php if(!empty($auRec[0]->region_id) && $auRec[0]->region_id > 0){?>
					<?php $region = $_objAdmin->_getSelectList2('table_region','region_id,region_name',''," status = 'A' and country_id='".$auRec[0]->country_id."' ORDER BY region_name"); 
						if(is_array($region)){
						 foreach($region as $value):?>
						 	<option value="<?php echo $value->region_id;?>" <?php if($value->region_id==$auRec[0]->region_id) echo "selected";?> ><?php echo $value->region_name;?></option>
					<?php endforeach; }?>

					<?php }?>	
				</select>
				</td>
				<td></td>
			</tr>
			<tr>
				<th valign="top">Zone:</th>
				<td><input type="text" name="zone_name" id="zone_name" class="required" value="<?php echo $auRec[0]->zone_name; ?>" maxlength="100" /></td>
				<td></td>
			</tr>			
			<tr>
				<th valign="top">Status:</th>
				<td>
				<select name="status" id="status" class="styledselect_form_3">
				<option value="A" <?php if($auRec[0]->status=='A') echo "selected";?> >Active</option>
				<option value="I" <?php if($auRec[0]->status=='I') echo "selected";?> >Inactive</option>
				</select>
				</td>
				<td></td>
			</tr>
			<tr>
				<th>&nbsp;</th>
				<td valign="top">
					<input name="id" type="hidden" value="<?php echo $auRec[0]->zone_id; ?>" />
					<input name="account_id" type="hidden" value="<?php echo $_SESSION['accountId'] ?>" />
					<input name="add" type="hidden" value="yes" />
					<input type="reset" value="Reset!" class="form-reset">
					<input type="button" value="Back" class="form-reset" onclick="location.href='addZones.php';" />
					<input name="submit" class="form-submit" type="submit" id="submit" value="Save" />
				</td>
			</tr>
			</table>
		</form>
			<!-- end id-form  -->
			</td>
				</tr>
				</table>
			</div>
			</td>
		</tr>
	</table>
	<div class="clear"></div>
	</div>
<!-- end related-act-bottom -->
<div class="clear"></div>
</div>
<div class="clear"></div>
<!-- start footer --> 
<?php include("footer.php") ?>  
<!-- end footer -->
</body>
</html>
