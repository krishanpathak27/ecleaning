<?php
include("includes/config.inc.php");
include("includes/function.php");
$page_name="Booking";
$_objAdmin = new Admin();
$objArrayList= new ArrayList();

if(isset($_POST['add']) && $_POST['add'] == 'yes') {
    if($_POST['booking_status'] == 'AL' && $_POST['cleaner_id'] == "") {
        $err =  "To allot a booking cleaner is require";
        $auRec[0]=(object)$_POST;
    } else {
        $cid=$_objAdmin->updateBooking($_POST['id']);
        $sus="Booking has been updated successfully."; 
    }
} else {
    if(isset($_REQUEST['cleaner_id']) && $_REQUEST['cleaner_id']!="")
    {
        $_objAdmin->showClenerServiceCount($_REQUEST['cleaner_id']);
        die;
    }
    if(isset($_REQUEST['id']) && $_REQUEST['id']!="")
    {
        $select = "table_booking_details AS bd "
                   ."LEFT JOIN table_booking_allotment AS ba ON bd.booking_detail_id = ba.booking_detail_id "
                   ."LEFT JOIN table_cleaner AS c ON c.cleaner_id = bd.cleaner_id ";
        $fields = " bd.booking_detail_id,c.cleaner_name,ba.booking_status,bd.cleaner_id";
        $auRec=$_objAdmin->_getSelectList($select,$fields,''," bd.booking_detail_id=".$_REQUEST['id']);
        if(count($auRec)<=0) header("Location: booking.php");
    } else {
        header("Location: booking.php");
    }
}




?>
<?php include("header.inc.php");
//$pageAccess=1;
//$check=$objArrayList->checkAccess($pageAccess, basename($_SERVER['PHP_SELF']));
//if($check == false){
//header('Location: ' . $_SERVER['HTTP_REFERER']);
//}

 ?>
<script type="text/javascript" src="javascripts/validate.js"></script>
<div class="clear"></div>
<!-- start content-outer -->
<div id="content-outer">
<!-- start content -->
	<div id="content">
	<div id="page-heading"><h1><span style="color: #d74343; font-family: Tahoma; font-weight: bold;">Edit Booking</span></h1></div>
		<table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">
		<tr>
			<!--<td id="tbl-border-left"></td>-->
			<td>
			<!--  start content-table-inner -->
			<div id="content-table-inner">
				<table border="0" width="100%" cellpadding="0" cellspacing="0">
				<tr valign="top">
					<td>
					<!--  start message-red -->
					<?php if($err!=''){?>
					<div id="message-red">
						<table border="0" width="100%" cellpadding="0" cellspacing="0">
						<tr>
							<td class="red-left"> <?php echo $err; ?></td>
							<td class="red-right"><a class="close-red"><img src="images/icon_close_red.gif"   alt="" /></a></td>
						</tr>
						</table>
					</div>
					<?php } ?>
					<!--  end message-red -->
					<?php if($sus!=''){?>
					<!--  start message-green -->
					<div id="message-green">
						<table border="0" width="100%" cellpadding="0" cellspacing="0">
						<tr>
							<td class="green-left"><?php echo $sus; ?></td>
							<td class="green-right"><a class="close-green"><img src="images/icon_close_green.gif"   alt="" /></a></td>
						</tr>
						</table>
					</div>
					<?php } ?>
					<!--  end message-green -->
					
			<!-- start id-form -->
                        <form name="frmPre" id="frmPre" method="post" action="add_booking.php" enctype="multipart/form-data" >
			<table border="0" cellpadding="0" cellspacing="0"  id="id-form">			
			<tr>
			<th valign="top">Cleaner:</th>
			<td>
                            <select name="cleaner_id" id="cleaner_id" class="styledselect_form_4" style="border:1px solid  !important" >
					<option value="">Select Cleaner</option>
					<?php 
					$condi = "";
					
					
					$branchList=$_objAdmin->_getSelectList('table_cleaner',"cleaner_id,cleaner_name",''," status='A'");
                                      
						for($i=0;$i<count($branchList);$i++){
						
						if($branchList[$i]->cleaner_id==$auRec[0]->cleaner_id){$select="selected";} else {$select="";}
							
					 ?>
					 <option value="<?php echo $branchList[$i]->cleaner_id; ?>" <?php echo $select ?>><?php echo $branchList[$i]->cleaner_name; ?></option>
					 
					 <?php } ?>
				</select>
				
			</td>
			<td></td>
		</tr>
			<tr>
				<th valign="top">Booking Status:</th>
				<td>
                                <select name="booking_status" id="booking_status" class="styledselect_form_3">
				<option value="UA" <?php if($auRec[0]->booking_status=='UA') echo "selected";?> >Not Alloted</option>
				<option value="AL" <?php if($auRec[0]->booking_status=='AL') echo "selected";?> >Alloted</option>
                                <option value="AC" <?php if($auRec[0]->booking_status=='AC') echo "selected";?> >Accepted</option>
                                <option value="RJ" <?php if($auRec[0]->booking_status=='RJ') echo "selected";?> >Rejected</option>
                                <option value="RD" <?php if($auRec[0]->booking_status=='RD') echo "selected";?> >Reached</option>
                                <option value="SC" <?php if($auRec[0]->booking_status=='SC') echo "selected";?> >Start Cleaning</option>
                                <option value="CC" <?php if($auRec[0]->booking_status=='CC') echo "selected";?> >Complete Cleaning</option>
                                <option value="RP" <?php if($auRec[0]->booking_status=='RP') echo "selected";?> >Payment Received</option>
                                <option value="CB" <?php if($auRec[0]->booking_status=='CB') echo "selected";?> >Complete Booking</option>
                                <option value="CL" <?php if($auRec[0]->booking_status=='CL') echo "selected";?> >Cancel Booking</option>
                                
				</td>
				<td></td>
                                </tr>
			
			<tr>
				<th>&nbsp;</th>
				<td valign="top">
					<input name="account_id" type="hidden" value="<?php echo $_SESSION['accountId']; ?>" />
					<input name="id" type="hidden" value="<?php echo $auRec[0]->booking_detail_id; ?>" />
					<input name="add" type="hidden" value="yes" />
					<input type="reset" value="Reset!" class="form-reset">
					<input type="button" value="Back" class="form-reset" onclick="location.href='booking.php';" />
					<input name="submit" class="form-submit" type="submit" id="submit" value="Save" />
					
				</td>
			</tr>
			</table>
		</form>
			<!-- end id-form  -->
			</td>
			<td>
			<!-- right bar-->
			<?php include("rightbar/division_bar.php") ?>
			<div class="clear"></div>
		</div>
<!-- end related-act-bottom -->
</div>
<!-- end related-activities -->
</td>
</tr>
<tr>
<td><img src="images/blank.gif" width="695" height="1" alt="blank" /></td>
<td></td>
</tr>
</table>
<div class="clear"></div>
</div>
<!--  end content-table-inner  -->
</td>
<td id="tbl-border-right"></td>
</tr>
</table>
<div class="clear">&nbsp;</div>

</div>
<!--  end content -->
<div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->
<div class="clear">&nbsp;</div>

<script type="text/javascript">

$(document).ready(function() {
    $('#cleaner_id').change(function() {
        var cleaner_id = $('#cleaner_id').val()
        if(cleaner_id != '') {
            $.ajax({
                method: "GET",
                url: "add_booking.php?cleaner_id="+cleaner_id,
                data: {},
                success: function(data) {
                    if(data != 0) {
                        var r = confirm(data + " booking is alreadry assign to this cleaner.Do you want to continue?" );
                        if (r == false) {
                            $('#cleaner_id').val('');
                        }
                    }
                }
            })
        }
    })
})


</script>

<!-- start footer --> 
<?php include("footer.php") ?>  
<!-- end footer -->

</body>
</html>

