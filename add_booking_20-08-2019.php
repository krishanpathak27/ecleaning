
 <?php
include("includes/config.inc.php");
include("includes/function.php");
include("classes/FCM.php");
// ini_set("display_errors", "1");
//     error_reporting(E_ALL);
$page_name="Booking";
$_objAdmin = new Admin();
$objArrayList= new ArrayList();
$fcm = new FCM();
$time_required = $_REQUEST['time_required'];
$customer_name = $_REQUEST['cus_name'];
$time_required_no_sec = substr($time_required,0,-3);
$sus = '';
$sus = $_REQUEST['sus'];
if(isset($_POST['add']) && $_POST['add'] == 'yes'){
    if($_POST['booking_status'] == 'AL' && $_POST['cleaner_id'] == "") {
        $err =  "To allot a booking cleaner is require";
        $auRec[0]=(object)$_POST;
    } else {
        $customer_name = $_POST['customer_name'];
		$cid=$_objAdmin->updateBooking($_POST['id']);

        /************** Code for Sending Push Notification ********************************************/
            if($_POST['id']>0)
            {

                $bookingDetails = $_objAdmin->_getSelectList2('table_booking_register as TBR left join table_booking_details as TBD on TBD.booking_id = TBR.booking_id','*','',' TBD.booking_detail_id="'.$_POST['id'].'"');    
                
            }
            
            if($_POST['booking_status'] == 'AL')
            {
                $fcm->send('AL',$bookingDetails[0]->booking_id , $bookingDetails[0]->customer_id,$bookingDetails[0]->booking_id);
            }   
            else if($_POST['booking_status'] == 'AC')
            {
                $fcm->send('AC',$bookingDetails[0]->booking_id , $bookingDetails[0]->customer_id,$bookingDetails[0]->booking_id);
            }
            else if($_POST['booking_status'] == 'RJ')
            {
                $fcm->send('RJ',$bookingDetails[0]->booking_id , $bookingDetails[0]->customer_id,$bookingDetails[0]->booking_id);
            }
            else if($_POST['booking_status'] == 'SC')
            {
                $fcm->send('SC',$bookingDetails[0]->booking_id , $bookingDetails[0]->customer_id,$bookingDetails[0]->booking_id);
            }
            else if($_POST['booking_status'] == 'CC')
            {
                $fcm->send('CC',$bookingDetails[0]->booking_id , $bookingDetails[0]->customer_id,$bookingDetails[0]->booking_id);
            }
        /************** Code for Sending Push Notification ********************************************/
		header("Location: add_booking.php?id=$_POST[id]&time_required=$time_required&cus_name=$customer_name&sus=Booking has been updated successfully");
        //$sus="Booking has been updated successfully.$cid"; 
    }
} else {
    if(isset($_REQUEST['cleaner_id']) && $_REQUEST['cleaner_id']!="")
    {
        $_objAdmin->showClenerServiceCount($_REQUEST['cleaner_id']);
        die;
    }
	
    if(isset($_REQUEST['id']) && $_REQUEST['id']!="")
    {
        $select = "table_booking_details AS bd "
                   ."LEFT JOIN table_booking_allotment AS ba ON bd.booking_detail_id = ba.booking_detail_id "
                   ."LEFT JOIN table_cleaner AS c ON c.cleaner_id = bd.cleaner_id "
				   ."LEFT JOIN table_calender_slots AS tcs ON tcs.slot_id = bd.slot_id "
				   ."LEFT JOIN table_booking_register AS br ON bd.booking_id = br.booking_id "
				   ."LEFT JOIN table_customer_address AS ca ON br.customer_address_id = ca.customer_address_id "
				   ."LEFT JOIN table_buildings AS b ON b.building_id = ca.building_id ";
        $fields = " bd.booking_detail_id,c.cleaner_name,ba.booking_status,bd.cleaner_id,bd.cleaning_date,tcs.slot_start_time,tcs.slot_end_time,ba.cleaning_time_end,ba.cleaning_time_start,b.building_name";
        $auRec=$_objAdmin->_getSelectList($select,$fields,''," bd.booking_detail_id=".$_REQUEST['id']);
        if(count($auRec)<=0) header("Location: booking.php");
    } else {
        header("Location: booking.php");
    }
}
?>
<?php include("header.inc.php");?>
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <!-- BEGIN: Subheader -->
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">
                    Cleaner Allotment
                </h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="index.php" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__separator">
                      
                    </li>
                    <li class="m-nav__item">
                        <a href="" class="m-nav__link">
                            <span class="m-nav__link-text">
                                
                            </span>
                        </a>
                    </li>
                    <li class="m-nav__separator">
                       
                    </li>
                    <li class="m-nav__item">
                        <a href="" class="m-nav__link">
                            <span class="m-nav__link-text">
                                
                            </span>
                        </a>
                    </li>  
                </ul>
            </div> 
        </div>
    </div>
    <!-- END: Subheader -->
    <div class="m-content">
        <div class="row">
            <div class="col-lg-12">
                <!--begin::Portlet--> 
                <div class="m-portlet">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <span class="m-portlet__head-icon m--hide">
                                    <i class="la la-gear"></i>
                                </span>
                                <h3 class="m-portlet__head-text">
                                    Cleaner Allotment Form
                                </h3>
                            </div>
                        </div>
                    </div>
                    <!--begin::Form-->
                    <form class="m-form m-form--fit m-form--label-align-right" id="m_form_1"  method="post" action="add_booking.php" enctype="multipart/form-data">

                        <div class="m-portlet__body">
                            <div class="m-form__content">
                                <div class="m-alert m-alert--icon alert alert-danger m--hide" role="alert" id="m_form_1_msg">
                                    <div class="m-alert__icon">
                                        <i class="la la-warning"></i>
                                    </div>
                                    <div class="m-alert__text">
                                        Oh snap! Change a few things up and try submitting again.
                                    </div>
                                    <div class="m-alert__close">
                                        <button type="button" class="close" data-close="alert" aria-label="Close"></button>
                                    </div>
                                </div>
                            </div>
                             <?php if(isset($sus) && $sus != ""){ ?>
                                <div role="alert" style="background: #d7fbdc;" class="m-alert   m-alert--icon m-alert--air m-alert--square alert alert-dismissible m--margin-bottom-30">
                                  <div class="m-alert__icon" >
                                          <i class="flaticon-exclamation m--font-brand"></i>
                                  </div>
                                  <div class="m-alert__text">
                                         <?php echo $sus; ?> 
                                  </div>
                                </div>
                            <?php } ?>
                            <?php if(isset($err) && !empty($err)){ ?>
                                <div class="m-form__content">
                                    <div class="m-alert m-alert--icon alert alert-danger" role="alert" id="m_form_1_msg1">
                                        <div class="m-alert__icon">
                                            <i class="la la-warning"></i>
                                        </div>
                                        <div class="m-alert__text">
                                           Error. <?php echo $err; ?>
                                        </div>
                                        <div class="m-alert__close">
                                            <button type="button" class="close" data-close="alert" aria-label="Close"></button>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                             <div class="form-group m-form__group row">
                               <div class="col-lg-4">
                             	 <label>Cleanig Date:</label>
                                <b> <?php echo  $auRec[0]->cleaning_date; ?></b>
                                </div> 
                                <div class="col-lg-4">
                             	 <label>Building Name:</label>
                                <b> <?php echo  $auRec[0]->building_name; ?></b>
                                </div> 
                                <div class="col-lg-4">
                                <?php if($auRec[0]->cleaning_time_start !='') { ?>
                             	 <label>Assigned Cleaner : </label>
                                <b> <?php echo  $auRec[0]->cleaner_name; ?></b>
                                <?php } 

                                else { ?>
                             	<!--  <label>Time Required for Cleaning</label>
                                <b> <?php echo  $time_required; ?></b> -->
                                <?php } ?>

                                </div>
                                 <div class="col-lg-4">
                             	 <label>Customer Name:</label>
                                <b> <?php echo  $customer_name; ?></b>
                                </div> 
                                <div class="col-lg-4">
                             	 <label>Cleanig Time Slot:</label>
                                <b> <?php echo  $auRec[0]->slot_start_time.' - '. $auRec[0]->slot_end_time; ?></b>
                                </div> 
                                <!-- <div class="col-lg-4">
                             	 <label>Alloted Time for Cleaning</label>
                                <b> <?php if($auRec[0]->cleaning_time_start !='') { echo date("H:i:s",strtotime($auRec[0]->cleaning_time_start)).' - '.date("H:i:s",strtotime($auRec[0]->cleaning_time_end)); } else { echo "NOT ALLOTED"; } ?></b>
                                </div> -->
                             </div>
                             
                            <div class="form-group m-form__group row">
                            
                                <div class="col-lg-4">
                                    <label>
                                        Cleaner:
                                    </label>
                                    <div class="input-group m-input-group m-input-group--square"> 
                                        <select class="form-control m-input m-input--square" id="cleaner_id" name="cleaner_id">
                                           <option value="">Select Cleaner</option>
                                           <?php $condi = "";
                                                $branchList=$_objAdmin->_getSelectList('table_cleaner',"cleaner_id,cleaner_name",''," status='A'");
                                            
                                                for($i=0;$i<count($branchList);$i++){
                  
                                                    if($branchList[$i]->cleaner_id==$auRec[0]->cleaner_id){$select="selected";} else {$select="";}
                    
                                                ?>
                                                  <option value="<?php echo $branchList[$i]->cleaner_id; ?>" <?php echo $select ?>><?php echo $branchList[$i]->cleaner_name; ?></option>
                 
                                              <?php } ?>
                                          </select>
                                    </div>

                                </div>
                                <div class="col-lg-4">
                                    <label class="">
                                        Booking Status:
                                    </label>
                                    <div class="input-group m-input-group m-input-group--square"> 
                                        <select class="form-control m-input m-input--square" id="booking_status" name="booking_status">
                                          <option value="UA" <?php if($auRec[0]->booking_status=='UA') echo "selected";?> >Not Alloted</option>
                                          <option value="AL" <?php if($auRec[0]->booking_status=='AL') echo "selected";?> >Alloted</option>
                                          <option value="AC" <?php if($auRec[0]->booking_status=='AC') echo "selected";?> >Accepted</option>
                                          <option value="RJ" <?php if($auRec[0]->booking_status=='RJ') echo "selected";?> >Rejected</option>
                                          <option value="RD" <?php if($auRec[0]->booking_status=='RD') echo "selected";?> >Reached</option>
                                          <option value="SC" <?php if($auRec[0]->booking_status=='SC') echo "selected";?> >Start Cleaning</option>
                                          <option value="CC" <?php if($auRec[0]->booking_status=='CC') echo "selected";?> >Complete Cleaning</option>
                                          <option value="RP" <?php if($auRec[0]->booking_status=='RP') echo "selected";?> >Payment Received</option>
                                          <option value="CB" <?php if($auRec[0]->booking_status=='CB') echo "selected";?> >Complete Booking</option>
                                          <option value="CL" <?php if($auRec[0]->booking_status=='CL') echo "selected";?> >Cancel Booking</option>
                                        </select>
                                    </div>
                                </div>

                                <input type="hidden" name="cleaning_time_start" value="<?php if($auRec[0]->cleaning_time_start !='') { echo date('H:i',strtotime($auRec[0]->cleaning_time_start)); } else { echo $auRec[0]->slot_start_time; } ?>">

                                <input type="hidden" name="cleaning_time_end" value="<?php if($auRec[0]->cleaning_time_end !='') { echo date('H:i',strtotime($auRec[0]->cleaning_time_end)); } else { echo $auRec[0]->slot_end_time;} ?>">
                                
                               <!--  <div class="col-lg-2">
                                        <label class="control-label col-md-6">Start Time</label>
                                        <div class="input-group">
                                            <input type="text" name="cleaning_time_start" class="form-control cleaning_time_start timepicker timepicker-24" value="<?php if($auRec[0]->cleaning_time_start !='') { echo date('H:i',strtotime($auRec[0]->cleaning_time_start)); } else { echo $auRec[0]->slot_start_time; } ?>">
                                            <span class="input-group-btn">
                                                <button class="btn default" type="button">
                                                    <i class="fa fa-clock-o"></i>
                                                </button>
                                            </span>
                                        </div>
                                    </div>
                                    
                                <div class="col-lg-2">
                                        <label class="control-label col-md-6">Finish Time</label>
                                        <div class="input-group">
                                            <input type="text" name="cleaning_time_end" class="form-control cleaning_time_end timepicker timepicker-24" value="<?php if($auRec[0]->cleaning_time_end !='') { echo date('H:i',strtotime($auRec[0]->cleaning_time_end)); } else { echo $auRec[0]->slot_end_time;} ?>">
                                            <span class="input-group-btn">
                                                <button class="btn default" type="button">
                                                    <i class="fa fa-clock-o"></i>
                                                </button>
                                            </span>
                                        </div>
                                    </div> -->
                                
                            </div>
                           
                            <div class="form-group m-form__group row">
                            	<div id="booked_slotes_details"></div>                              
                               <input type="hidden" name="cleaning_date" id="cleaning_date" value="<?php echo $auRec[0]->cleaning_date; ?>">
                               <input type="hidden" name="cleaning_slot_start" id="cleaning_slot_start" value="<?php echo $auRec[0]->slot_start_time; ?>">
                               <input type="hidden" name="cleaning_slot_end" id="cleaning_slot_end"  value="<?php echo $auRec[0]->slot_end_time; ?>">
                                
                            </div>
                        </div>
                        <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                            <div class="m-form__actions m-form__actions--solid">
                                <div class="row">
                                    <div class="col-lg-4"></div>
                                    <div class="col-lg-8">
                                        <button type="submit" id="cleaning_alot_submit" class="btn btn-success">
                                            Submit
                                        </button>
                                        <button type="reset" class="btn btn-secondary">
                                            Reset
                                        </button>
                                        <a href="supervisor.php">
                                        <!--<button class="btn btn-secondary" onclick="location.href='booking.php';">
                                            Back
                                        </button>-->
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <input name="account_id" type="hidden" value="<?php echo $_SESSION['accountId']; ?>" />
                         <input name="customer_name" type="hidden" value="<?php echo $customer_name; ?>" />
                       <input name="id" type="hidden" value="<?php echo $auRec[0]->booking_detail_id; ?>" />
                       <input name="add" type="hidden" value="yes" />
                    </form>
                    <!--end::Form-->
                </div>
                <!--end::Portlet-->
            </div>
        </div>
    </div>
</div>
</div>
</div>
<!-- start footer -->         
<?php include("footer.php") ?>

<script type="text/javascript">
    var FormControls = function () {
        //== Private functions

        var demo1 = function () {
            $("#m_form_1").validate({
                // define validation rules
                rules: {
                    cleaner_id: {
                        required: true
                    },
                    booking_status: {
                        required: true
                        
                    }
                },
                //display error alert on form submit  
                invalidHandler: function (event, validator) {
                    var alert = $('#m_form_1_msg');
                    alert.removeClass('m--hide').show();
                    mApp.scrollTo(alert, -200);
                },
                submitHandler: function (form) {
                    form.submit();
                }
            });
        }

        return {
            // public functions
            init: function () {
                demo1();
            }
        };
    }();

    jQuery(document).ready(function () {
        var id = '<?php echo $_REQUEST['id']; ?>';
        FormControls.init();
        if(id) {
            $('#password').rules('add', {
            required: false,
            minlength: 8
            });
        }
	   /* Showing current booked slots of each cleaner when a user selects from the list  */	
	   $("#cleaner_id").trigger('change');
	   $('#cleaner_id').on('change', function() {
		    $('#booked_slotes_details').html('Checking availability....');
		    var cleaner_id = $(this).val();
			console.log(cleaner_id);
			if(cleaner_id =='') return false;
			var cleaning_date = $('#cleaning_date').val();
			var cleaning_slot_start = $('#cleaning_slot_start').val();
			var cleaning_slot_end = $('#cleaning_slot_end').val();
			$.ajax({
			  method: "POST",
			  url: "test.php",
			  data: { cleaner_id: cleaner_id, cleaning_date: cleaning_date,cleaning_slot_start: cleaning_slot_start,cleaning_slot_end: cleaning_slot_end}
			})
		  .done(function( msg ) {
			$('#booked_slotes_details').html('');
			$('#booked_slotes_details').html(msg);
			if(document.getElementById('booked').value ==1)
			   $('#cleaning_alot_submit').attr('disabled', 'disabled');
			else
			   $('#cleaning_alot_submit').removeAttr('disabled');   
		  });
	   }) 
	   /* Calculating the Finishing Time Based on the start time selected */
	   $('.cleaning_time_start').on('change', function() {
		   from_time = $(this).val();
		   time_needed = '<?php echo $time_required_no_sec; ?>'; 
		   var m = (from_time.substring(0,from_time.indexOf(':'))-0) * 60 +
        	(from_time.substring(from_time.indexOf(':')+1,from_time.length)-0) +
        	(time_needed.substring(0,time_needed.indexOf(':'))-0) * 60 +
       		(time_needed.substring(time_needed.indexOf(':')+1,time_needed.length)-0);
		    var h = Math.floor(m / 60);
			minss = m - (h * 60);
			if(minss < 10) mins = '0'+ minss; else mins = minss;
			finish_time = h + ':' + mins;
			console.log(finish_time);
		    $('.cleaning_time_end').val(finish_time);
	   });
	  
    });

</script>

