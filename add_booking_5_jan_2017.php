
 <?php
include("includes/config.inc.php");
include("includes/function.php");
$page_name="Booking";
$_objAdmin = new Admin();
$objArrayList= new ArrayList();

if(isset($_POST['add']) && $_POST['add'] == 'yes') {
    if($_POST['booking_status'] == 'AL' && $_POST['cleaner_id'] == "") {
        $err =  "To allot a booking cleaner is require";
        $auRec[0]=(object)$_POST;
    } else {
        $cid=$_objAdmin->updateBooking($_POST['id']);
        $sus="Booking has been updated successfully."; 
    }
} else {
    if(isset($_REQUEST['cleaner_id']) && $_REQUEST['cleaner_id']!="")
    {
        $_objAdmin->showClenerServiceCount($_REQUEST['cleaner_id']);
        die;
    }
    if(isset($_REQUEST['id']) && $_REQUEST['id']!="")
    {
        $select = "table_booking_details AS bd "
                   ."LEFT JOIN table_booking_allotment AS ba ON bd.booking_detail_id = ba.booking_detail_id "
                   ."LEFT JOIN table_cleaner AS c ON c.cleaner_id = bd.cleaner_id ";
        $fields = " bd.booking_detail_id,c.cleaner_name,ba.booking_status,bd.cleaner_id";
        $auRec=$_objAdmin->_getSelectList($select,$fields,''," bd.booking_detail_id=".$_REQUEST['id']);
        if(count($auRec)<=0) header("Location: booking.php");
    } else {
        header("Location: booking.php");
    }
}




?>
<?php include("header.inc.php");
//$pageAccess=1;
//$check=$objArrayList->checkAccess($pageAccess, basename($_SERVER['PHP_SELF']));
//if($check == false){
//header('Location: ' . $_SERVER['HTTP_REFERER']);
//}

 ?>
 <script type="text/javascript" src="javascripts/validate.js"></script>
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="index.php">Home</a>
        </li>
        <?php $breadCumArray=(getBreadCrum('profile_setting.php',-1));if(!empty($breadCumArray)){foreach($breadCumArray as $breadCum){?>
    <li class="breadcrumb-item active"><?php  echo $breadCum;?></li>
    <?php }}else{?>
      <li class="breadcrumb-item active">Admin</li>
    <?php }?>
      </ol>
      <!-- Icon Cards--> 
       <div class="row">
      <div class="accordian">
        <div class="panel-group">
          <div class="panel panel-default card_bg mrgn-lg"><!--1 card-->

            <div class="panel-heading">
              <h4 class="panel-title">Edit Booking</h4>
              <hr/>
            </div> 
  			<?php if($sus!=''){?>
            <!--  start message-green -->

            <div id="message-green" >
              <div class="bg-success pdng-md mrgn-btm-lg">
                  <div class="pull-left bold"><?php echo $sus; ?></div>
                  <div class="pull-right"><a class="close-green"><i class="fa fa-close"></i></a></div>
                 <div class="clear"></div>
              </div>
            </div>

           <?php } ?>

            <?php if($err!=''){?>
	          <div id="message-red">
	            <div class="bg-warning pdng-md mrgn-btm-lg">
	              <div class="pull-left bold">Error. <?php echo $err; ?></div>
	              <div class="pull-right"><a class="close-red"><i class="fa fa-close"></i></a></div>
	              <div class="clear"></div>
	            </div>
	          </div>

        	<?php } ?>

            <div class="panel-body">   
             	<form name="frmPre" id="frmPre" method="post" action="add_booking.php" enctype="multipart/form-data" >         
                	<div class="row">
	                  <div class="col-lg-3 col-md-3 col-xs-12 bold">
	                    Cleaner:
	                  </div>
	                  <div class="col-lg-3 col-md-3 col-xs-12">
	                    <div class="form-group">

                            <select name="cleaner_id" id="cleaner_id" class="styledselect_form_4 form-control" style="border:1px solid  !important" >
								<option value="">Select Cleaner</option>
								<?php 
								$condi = "";
								
								
								$branchList=$_objAdmin->_getSelectList('table_cleaner',"cleaner_id,cleaner_name",''," status='A'");
			                                      
									for($i=0;$i<count($branchList);$i++){
									
									if($branchList[$i]->cleaner_id==$auRec[0]->cleaner_id){$select="selected";} else {$select="";}
										
								 ?>
								 <option value="<?php echo $branchList[$i]->cleaner_id; ?>" <?php echo $select ?>><?php echo $branchList[$i]->cleaner_name; ?></option>
								 
								 <?php } ?>
							</select>
	                    </div>
	                  </div>
                  	</div>
                   
                   	<div class="row">
	                  	<div class="col-lg-3 col-md-3 col-xs-12 bold">
	                    	Booking Status:
	                  	</div>

	                  	<div class="col-lg-3 col-md-3 col-xs-12">
	                    	<div class="form-group">
	                     		<select name="booking_status" id="booking_status" class="styledselect_form_3 form-control">
									<option value="UA" <?php if($auRec[0]->booking_status=='UA') echo "selected";?> >Not Alloted</option>
									<option value="AL" <?php if($auRec[0]->booking_status=='AL') echo "selected";?> >Alloted</option>
	                                <option value="AC" <?php if($auRec[0]->booking_status=='AC') echo "selected";?> >Accepted</option>
	                                <option value="RJ" <?php if($auRec[0]->booking_status=='RJ') echo "selected";?> >Rejected</option>
	                                <option value="RD" <?php if($auRec[0]->booking_status=='RD') echo "selected";?> >Reached</option>
	                                <option value="SC" <?php if($auRec[0]->booking_status=='SC') echo "selected";?> >Start Cleaning</option>
	                                <option value="CC" <?php if($auRec[0]->booking_status=='CC') echo "selected";?> >Complete Cleaning</option>
	                                <option value="RP" <?php if($auRec[0]->booking_status=='RP') echo "selected";?> >Payment Received</option>
	                                <option value="CB" <?php if($auRec[0]->booking_status=='CB') echo "selected";?> >Complete Booking</option>
	                                <option value="CL" <?php if($auRec[0]->booking_status=='CL') echo "selected";?> >Cancel Booking</option>
                            	</select>
	                    	</div>
	                  	</div>
                	</div>  
                     
                 	
	                <div class="row mrgn-top-sm">
	                  	<div class="col-md-12 col-sm-12 col-xs-12 text-right">
	                  		<input name="account_id" type="hidden" value="<?php echo $_SESSION['accountId']; ?>" />
							<input name="id" type="hidden" value="<?php echo $auRec[0]->booking_detail_id; ?>" />
							<input name="add" type="hidden" value="yes" />
							<button type="reset" class="btn bg-info"><i class="fa fa-refresh mrgn-rgt-sm" aria-hidden="true"></i>Reset</button>

	                  		<button type="button" class="btn bg-info" onclick="location.href='booking.php';"><i class="fa fa-chevron-left mrgn-rgt-sm" aria-hidden="true"></i>Back</button>
	                  		<input name="submit" class="form-submit btn bg-lgtgreen" type="submit" id="submit" value="Save" />
	                  		
	                   </div>
	                </div>
                </form>
              </div> 
          </div> 
        </div>
      </div>
    </div>
    </div>
    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->
    <?php include("footer.php"); ?>
    
  </div>

