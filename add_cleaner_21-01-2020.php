<?php
include("includes/config.inc.php");
include("includes/function.php");
$_objAdmin = new Admin();
if (isset($_POST['add']) && $_POST['add'] == 'yes') {
    $err = '';
    if ($_POST['supervisor_id'] != "") {
        $condi = " (cleaner_name='" . mysql_escape_string($_POST['cleaner_name']) . "') and cleaner_phone_no='" . mysql_escape_string($_POST['cleaner_phone_no']) . "' and status != 'D'  and cleaner_id<>'" . $_POST['cleaner_id'] . "'";
    } else {
        $condi = " (cleaner_name='" . mysql_escape_string($_POST['cleaner_name']) . "') and cleaner_phone_no='" . mysql_escape_string($_POST['cleaner_phone_no']) . "'  and status != 'D'";
    }
    $auRec = $_objAdmin->_getSelectList('table_cleaner', "*", '', $condi);
    if (is_array($auRec) && $_POST['cleaner_id'] == "" ) {
        $err = "Cleaner Already exits";
    }
    if ($_POST['cleaner_id'] != "") {
        $condi = " username='" . $_POST['username'] . "' and web_user_id<>'" . $_POST['web_id'] . "'";
    } else {
        $condi = " username='" . $_POST['username'] . "'";
    }
    $auRec = $_objAdmin->_getSelectList('table_web_users', "*", '', $condi);
   // print_r($auRec);
    if (is_array($auRec) && $_POST['cleaner_id'] == "" ) {
        $err = "Username Already exits";
    }
    if ($err == '') {
        if ($_POST['cleaner_id'] != "" && $_POST['cleaner_id'] != 0) {
            $save = $_objAdmin->UpdateCleaner($_POST["cleaner_id"]);
            $sus = "Updated Successfully";
        } else {
            $save = $_objAdmin->addCleaner();
            $sus = "Added Successfully";
        }
        ?>
        <script type="text/javascript">
            window.location = "cleaner.php?suc="+'<?php echo $sus; ?>';
        </script>
        <?php
    }
}
if (isset($_REQUEST['id']) && $_REQUEST['id'] != "") {
    $auRec=$_objAdmin->_getSelectList('table_cleaner as d left join table_web_users as w on w.cleaner_id=d.cleaner_id',"d.*,w.username,w.email_id,w.web_user_id",''," d.cleaner_id=".$_REQUEST['id']);
    // echo "<pre>";print_r($auRec);die();

	if (count($auRec) <= 0)
        header("Location: cleaner.php");
}
include("header.inc.php");
?>
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <!-- BEGIN: Subheader -->
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">
                    Cleaner
                </h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="#" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__separator">
                        -
                    </li>
                    <li class="m-nav__item">
                        <a href="#" class="m-nav__link">
                            <span class="m-nav__link-text">
                                Master
                            </span>
                        </a>
                    </li>
                    <li class="m-nav__separator">
                        -
                    </li>
                    <li class="m-nav__item">
                        <a href="" class="m-nav__link">
                            <span class="m-nav__link-text">
                                Cleaner
                            </span>
                        </a>
                    </li>  
                </ul>
            </div> 
        </div>
    </div>
    <!-- END: Subheader -->
    <div class="m-content">
        <div class="row">
            <div class="col-lg-12">
                <!--begin::Portlet--> 
                <div class="m-portlet">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <span class="m-portlet__head-icon m--hide">
                                    <i class="la la-gear"></i>
                                </span>
                                <h3 class="m-portlet__head-text">
                                    Cleaner Form
                                </h3>
                            </div>
                        </div>
                    </div>
                    <!--begin::Form-->
                    <form class="m-form m-form--fit m-form--label-align-right" id="m_form_1"  method="post" action="add_cleaner.php" enctype="multipart/form-data">

                        <div class="m-portlet__body">
                            <div class="m-form__content">
                                <div class="m-alert m-alert--icon alert alert-danger m--hide" role="alert" id="m_form_1_msg">
                                    <div class="m-alert__icon">
                                        <i class="la la-warning"></i>
                                    </div>
                                    <div class="m-alert__text">
                                        Oh snap! Change a few things up and try submitting again.
                                    </div>
                                    <div class="m-alert__close">
                                        <button type="button" class="close" data-close="alert" aria-label="Close"></button>
                                    </div>
                                </div>
                            </div>
                            <?php if (isset($err) && !empty($err)) { ?>
                                <div class="m-form__content">
                                    <div class="m-alert m-alert--icon alert alert-danger" role="alert" id="m_form_1_msg1">
                                        <div class="m-alert__icon">
                                            <i class="la la-warning"></i>
                                        </div>
                                        <div class="m-alert__text">
                                            <?php echo $err; ?>
                                        </div>
                                        <div class="m-alert__close">
                                            <button type="button" class="close" data-close="alert" aria-label="Close"></button>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                            <div class="form-group m-form__group row">
                                <div class="col-lg-4">
                                    <label>
                                        Cleaner Name:
                                    </label>
                                    <input type="text" class="form-control m-input" placeholder="Enter full name" id="cleaner_name" name="cleaner_name" value="<?php echo $auRec[0]->cleaner_name; ?>">

                                </div>
                                <div class="col-lg-4">
                                    <label class="">
                                        Cleaner Address:
                                    </label>
                                    <input type="text" class="form-control m-input" placeholder="Enter Address" id="cleaner_address" name="cleaner_address" value="<?php echo $auRec[0]->cleaner_address; ?>"> 
                                </div>
                                <div class="col-lg-4">
                                    <label class="">
                                       Mobile Number Country Code:
                                    </label>
                                    <input type="text" class="form-control m-input" placeholder="Country Code" name="mobile_number_country_code" id="mobile_number_country_code" value="<?php echo $auRec[0]->mobile_number_country_code; ?>" > 
                                </div>
                                <div class="col-lg-4">
                                    <label class="">
                                        Cleaner Phone No:
                                    </label>
                                    <input type="text" class="form-control m-input" placeholder="Phone No" name="cleaner_phone_no" id="cleaner_phone_no" value="<?php echo $auRec[0]->cleaner_phone_no; ?>"> 
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <div class="col-lg-4">
                                    <label class="">
                                        Cleaner Code:
                                    </label>
                                    <input type="text" class="form-control m-input" placeholder="Code" id="cleaner_code" name="cleaner_code" value="<?php echo $auRec[0]->cleaner_code; ?>"> 
                                </div>
                                <div class="col-lg-4">
                                    <label class="">
                                        Cleaner Email:
                                    </label>
                                    <div class="m-input-icon m-input-icon--right">
                                        <input type="text" class="form-control m-input" placeholder="Email" name="cleaner_email" id="cleaner_email" value="<?php echo $auRec[0]->cleaner_email; ?>"> 
                                    </div> 
                                </div>
                                
                                <div class="col-lg-4">
                                    <label>
                                        Supervisor:
                                    </label>
                                    <div class="input-group m-input-group m-input-group--square"> 
                                        <select class="form-control m-input m-input--square" id="supervisor_id" name="supervisor_id">
                                            <option value="">Select Supervisor</option>
					<?php 
					$condi = "";
					
					
					$branchList=$_objAdmin->_getSelectList('table_supervisor',"supervisor_id,supervisor_name",''," status='A'");
						for($i=0;$i<count($branchList);$i++){
						
						if($branchList[$i]->supervisor_id==$auRec[0]->supervisor_id){$select="selected";} else {$select="";}
							
					 ?>
					 <option value="<?php echo $branchList[$i]->supervisor_id; ?>" <?php echo $select ?>><?php echo $branchList[$i]->supervisor_name; ?></option>
					 
					 <?php } ?>
                                        </select>
                                    </div> 
                                </div>
                                
                            </div>
                            <div class="form-group m-form__group row">
                                <div class="col-lg-4">
                                    <label class="">
                                        User Name:
                                    </label>
                                    <div class="m-input-icon m-input-icon--right">
                                        <input type="text" class="form-control m-input" placeholder="User Name" id="username" name="username" value="<?php echo $auRec[0]->username; ?>"> 
                                    </div> 
                                </div>
                                <div class="col-lg-4">
                                    <label class="">
                                        Password:
                                    </label>
                                    <div class="m-input-icon m-input-icon--right">
                                        <input type="password" class="form-control m-input" placeholder="password" id="password" name="password"> 
                                    </div> 
                                </div>
                                <div class="col-lg-4">
                                    <label class="">
                                        Status:
                                    </label>
                                    <div class="m-radio-inline">
                                        <label class="m-radio m-radio--solid">
                                            <input <?php if($auRec[0]->status=='A') echo "checked";?> type="radio" name="status"  value="A" >
                                            Active
                                            <span></span>
                                        </label>
                                        <label class="m-radio m-radio--solid">
                                            <input type="radio" name="status" value="I" <?php if($auRec[0]->status=='I') echo "checked";?>>
                                            Inactive
                                            <span></span>
                                        </label>
                                    </div> 
                                </div>
                            </div>
                            <div class="form-group m-form__group row"> 
                                <div class="col-lg-6">
                                    <label class="">
                                      Working Hours From:
                                    </label>
                                     <div class="input-group m-input-group m-input-group--square"> 
                                        <select class="form-control m-input m-input--square" id="duty_start_time" name="duty_start_time">
                                        	<?php $times = array('08:00','09:00','10:00','11:00','12:00','13:00','14:00','15:00','16:00','17:00','18:00','19:00','20:00','21:00','22:00');
											foreach($times as $hour) { 
											 if($auRec[0]->duty_start_time == $hour)  $selected = 'selected'; else $selected = '';
											 ?>
                                            <option value="<?php echo $hour; ?>" <?php echo $selected; ?> ><?php echo $hour; ?></option>
                                            <?php } ?>
                                        </select>
                                     </div>    
                                </div>
                                <div class="col-lg-6">
                                    <label class="">
                                      Working Hours To:
                                    </label>
                                     <div class="input-group m-input-group m-input-group--square"> 
                                        <select class="form-control m-input m-input--square" id="duty_end_time" name="duty_end_time">
                                        	<?php $times = array('08:00','09:00','10:00','11:00','12:00','13:00','14:00','15:00','16:00','17:00','18:00','19:00','20:00','21:00','22:00');
											foreach($times as $hour) { 
											 if($auRec[0]->duty_end_time == $hour) $selected = 'selected'; else $selected = '';
											 ?>
                                            <option value="<?php echo $hour; ?>" <?php echo $selected; ?> ><?php echo $hour; ?></option>
                                            <?php } ?>
                                        </select>
                                     </div>    
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <div class="col-lg-4">
                                    <label class="">
                                        Profile Pic:
                                    </label>
                                    <div class="m-input-icon m-input-icon--right">
                                        <img src="<?php if($auRec[0]->image!=""){ echo 'http://13.127.138.63/ecleaning/dev/images/cleaner/'.$auRec[0]->image; } ?>" style="width: 75px;" alt="Profile Picture"> 
                                    </div> 
                                    <div class="m-input-icon m-input-icon--right">
                                        <input type="file" class="form-control m-input" placeholder="User Name" id="image" name="image"> 
                                    </div> 
                                </div> 
                                <div class="col-lg-4">
                                    <label class="">
                                      ID Document:
                                    </label>
                                    <div class="m-input-icon m-input-icon--right">
                                        <img src="<?php if($auRec[0]->cleaner_id_document!=""){ echo 'http://13.127.138.63/ecleaning/dev/images/cleaner_id_document/'.$auRec[0]->cleaner_id_document; } ?>" style="width: 75px;" alt="Profile Picture"> 
                                    </div>
                                    <div class="m-input-icon m-input-icon--right">
                                        <input type="file" class="form-control m-input" id="id_document" name="id_document"> 
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                            <div class="m-form__actions m-form__actions--solid">
                                <div class="row">
                                    <div class="col-lg-4"></div>
                                    <div class="col-lg-8">
                                        <button type="submit" class="btn btn-success">
                                            Submit
                                        </button>
                                        <button type="reset" class="btn btn-secondary">
                                            Reset
                                        </button>
                                        <a href="supervisor.php">
                                        <button class="btn btn-secondary">
                                            back
                                        </button>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <input name="cleaner_id" type="hidden" value="<?php echo $auRec[0]->cleaner_id; ?>" />
                        <input name="web_id" type="hidden" value="<?php echo $auRec[0]->web_user_id; ?>" />
                        <input name="account_id" type="hidden" value="<?php echo $_SESSION['accountId']; ?>" />
                        <input type="hidden" name="add" value="yes">

                    </form>
                    <!--end::Form-->
                </div>
                <!--end::Portlet-->
            </div>
        </div>
    </div>
</div>
</div>
</div>
<!-- start footer -->         
<?php include("footer.php") ?>

<script type="text/javascript">
    var FormControls = function () {
        //== Private functions

        var demo1 = function () {
            $("#m_form_1").validate({
                // define validation rules
                rules: {
                    cleaner_name: {
                        required: true
                    },
                    cleaner_phone_no: {
                        required: true,
                        digits: true
                    },
                     mobile_number_country_code: {
                        required:!0,number:!0
                    },
                    cleaner_address: {
                        required: true
                    },
                    cleaner_code: {
                        required: true
                    },
                    cleaner_email: {
                        required: true,
                        email: true,
                        minlength: 10
                    },
                    supervisor_id: {
                        required: true,
                    },
					
                    image: {
                        // required: true,
                        accept:"jpg,png,jpeg,gif"
                    },
                    
                    username: {
                        required: true
                    },
                    password: {
                        required: true,
                        minlength: 8
                    },
					duty_start_time: {
						required:true
					},
					duty_end_time: {
						required:true
					},
                    status: {
                        required: true,
                    }
                },
                messages: {
                    image:{
                        accept: "Only image type jpg/png/jpeg/gif is allowed"
                    },
                    mobile_number_country_code:{
                        required:"Enter mobile number country code",
                        number:"Enter Only Digits"
                    }

                 } ,
                //display error alert on form submit  
                invalidHandler: function (event, validator) {
                    var alert = $('#m_form_1_msg');
                    alert.removeClass('m--hide').show();
                    mApp.scrollTo(alert, -200);
                },
                submitHandler: function (form) {
                    form.submit();
                }
            });
        }

        return {
            // public functions
            init: function () {
                demo1();
            }
        };
    }();

    jQuery(document).ready(function () {
        var id = '<?php echo $_REQUEST['id']; ?>';
        FormControls.init();
        if(id) {
            $('#password').rules('add', {
                required: false,
                minlength: 8
            });
            $('#image').rules('add', {
                required: false,
                accept:"jpg,png,jpeg,gif"
            });
        }
        var userType = '<?php echo $_SESSION['userLoginType'] ?>'
        var id = '<?php echo $_SESSION['supervisor_id'] ?>'
        if(userType == '5' && id != null) {
            $('#supervisor_id').val(id);
            $('#supervisor_id').attr('disabled',true);
        }
    });

</script>

