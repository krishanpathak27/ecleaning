<?php
include("includes/config.inc.php");
include("includes/function.php");
$page_name="Add Company";
$_objAdmin = new Admin();
$_objItem = new Item();

if(isset($_POST['add']) && $_POST['add'] == 'yes')
{
	//print_r($_REQUEST);
	$company_exits=$_objAdmin->_getSelectList2('table_account','*','',"company_name='".trim($_POST['company_name'])."' and company_phone_no='".trim($_POST['company_phone_no'])."' and state='".$_POST['state']."'");
		if(count($company_exits)>0)
		{
		$_SESSION['company_exits']="Company Already Exits.";	
		}
		else
		{
		$incen_id=$_objAdmin->addCompanyAccount();
		//echo $duration_id;
			if($incen_id!=''){			
				$sus="Account has been Added successfully.";
				$_SESSION['CompId']=$incen_id;
				header("Location: company_login_account.php?id=".$incen_id);
			}
		}
}
/*if($sus!=''){
		
		//header ("Loaction: ");
}
*/




?>

<?php include("header.inc.php") ?>
<link rel="stylesheet" href="./base/jquery.ui.all.css">
<script src="./Autocomplete/jquery-1.5.1.js"></script>
<script src="./Autocomplete/jquery.ui.core.js"></script>
<script src="./Autocomplete/jquery.ui.widget.js"></script>
<script src="./Autocomplete/jquery.ui.button.js"></script>
<script src="./Autocomplete/jquery.ui.position.js"></script>
<script src="./Autocomplete/jquery.ui.menu.js"></script>
<script src="./Autocomplete/jquery.ui.autocomplete.js"></script>
<script src="./Autocomplete/jquery.ui.tooltip.js"></script>
<script src="./Autocomplete/jquery.ui.datepicker.js"></script>
<link rel="stylesheet" href="./base/demos.css">
<script type="text/javascript" src="./javascripts/validate.js"></script>


<script>
    $(function() {
        $( "#from" ).datepicker({
			dateFormat: "d M yy",
            defaultDate: "+1w",
            changeMonth: true,
            numberOfMonths: 3,
            onSelect: function( selectedDate ) {
                $( "#to" ).datepicker( "option", "minDate", selectedDate );
            }
        });
        $( "#to" ).datepicker({
			dateFormat: "d M yy",
            defaultDate: "+1w",
            changeMonth: true,
            numberOfMonths: 3,
            onSelect: function( selectedDate ) {
                $( "#from" ).datepicker( "option", "maxDate", selectedDate );
            }
        });
    });
	
	
	
	function showStateCity(str,id)
		{
		/*alert(str);
		alert(id);*/
			$.ajax({
				'type': 'POST',
				'url': 'dropdown_city.php',
				'data': 's='+str+'&c='+id,
				'success' : function(mystring) {
				document.getElementById("outputcity").innerHTML = mystring;
				$('#selectCity').show();
				}
			});
		}
</script>

	


<div class="clear"></div>
<!-- start content-outer -->
<div id="content-outer">
<!-- start content -->
	<div id="content">
	<div id="page-heading"><h1><span style="color: #d74343; font-weight: bold;">Add Company Account</span></h1></div>
		<table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">
		<tr>
			<td>
			<!--  start content-table-inner -->
			<div id="content-table-inner">
				<table border="0" width="100%" cellpadding="0" cellspacing="0">
				<tr valign="top">
					<td>
					<!--  start message-red -->
					<?php if($err!=''){?>
					<div id="message-red">
						<table border="0" width="100%" cellpadding="0" cellspacing="0">
						<tr>
							<td class="red-left"> <?php echo $err; ?></td>
							<td class="red-right"><a class="close-red"><img src="images/icon_close_red.gif"   alt="" /></a></td>
						</tr>
						</table>
					</div>
					<?php } ?>
					 <?php if($_SESSION['company_exits']!=''){?>
					<div id="message-red">
						<table border="0" width="100%" cellpadding="0" cellspacing="0">
						<tr>
							<td class="red-left"> <?php echo $_SESSION['company_exits'];  unset($_SESSION['company_exits']); ?></td>
							<td class="red-right"><a class="close-red"><img src="images/icon_close_red.gif"   alt="" /></a></td>
						</tr>
						</table>
					</div>
					<?php } ?>
					<!--  end message-red -->
					<?php if($login_sus!=''){?>

					 <!-- start message-green -->
					<div id="message-green">
						<table border="0" width="100%" cellpadding="0" cellspacing="0">
						<tr>
							<td class="green-left"><?php echo $login_sus; ?></td>
							<td class="green-right"><a class="close-green"><img src="images/icon_close_green.gif"   alt="" /></a></td>
						</tr>
						</table>
					</div>
					<?php } ?>
					<!--  end message-green -->
					<div id="step-holder">
					<div class="step-no">1</div>
					<div class="step-dark-left">Company Form</div>
					<div class="step-dark-right">&nbsp;</div>
					<div class="step-no-off">2</div>
					<div class="step-light-left">Feature</div>
					<div class="step-light-right">&nbsp;</div>
                    <div class="step-no-off">3</div>
					<div class="step-light-left">Login Form</div>
					<div class="step-light-right">&nbsp;</div>
					<div class="clear"></div>
					
                    
					<div class="clear"></div>
					</div>
			<!-- start id-form -->
		<form name="frmPre" id="frmPre" method="post" action="<?php $_SERVER['PHP_SELF'] ?>" enctype="multipart/form-data" >
			<table border="0" cellpadding="0" cellspacing="0" width="800" id="id-form">
			<tr>
				<th valign="top" >Company Name:</th>
				<td width="200px"><input type="text" name="company_name" id="company_name"  class="required" value="<?php echo $auRec[0]->company_name; ?>" maxlength="255" placeholder="Company Name"/></td>
				<td width="500px"></td>
			</tr>
			<tr>
				<th valign="top">Company Address:</th>
				<td colspan="2">
			<input type="text" style="width:590px;" name="company_address" id="company_address" class="text" value="<?php echo $auRec[0]->company_address; ?>" placeholder="Address Line"/>
			</td>
			</tr>
			<tr>
				<th valign="top" >Company Phone No.</th>
				<td >
					<input type="text" name="company_phone_no" id="company_phone_no" class="required number" maxlength="15" value="<?php echo $auRec[0]->company_phone_no; ?>" placeholder="Phone Number"/>
				</td>
				<td ></td>
			</tr>
			<tr>
				<th valign="top" >No. Of Users</th>
				<td >
					<input type="text" name="no_of_employees" id="no_of_employees" class="required number" maxlength="5" value="<?php echo $auRec[0]->no_of_employees; ?>" placeholder="No. Of Users"/>
				</td>
				<td ></td>
			</tr>
	
		<tr>
			<th valign="top">Sales Hierarchy :</th>
			<td><input type="radio" name="hierarchy_enable" value="1" <?php if($auRec[0]->hierarchy_enable=='1'){ ?> checked <?php } ?> > Yes</td>
			<td><input type="radio" name="hierarchy_enable" checked="checked" value="0" <?php if($auRec[0]->hierarchy_enable=='0'){ ?> checked <?php } ?> > No</td>
		</tr>
		<tr>
			<th valign="top">Access Management</th>
			<td><input type="radio" name="access_enable" value="1" <?php if($auRec[0]->shop_desc=='1'){ ?> checked <?php } ?> > Yes</td>
			<td><input type="radio" name="access_enable" checked="checked" value="0" <?php if($auRec[0]->shop_desc=='0'){ ?> checked <?php } ?> > No</td>
		</tr>
			<tr>
			<th valign="top">Country:</th>
			<td>
			<select name="country" id="country" class="styledselect_form_3">
			<option value="india">India</option>
			</select>
			</td>
			<td></td>
			<td></td>
		</tr>
		<tr>
		
			<th valign="top">Select State:</th>
			<td>
			<select name="state" id="<?php echo $auRec[0]->city; ?>" class="styledselect_form_3" onChange="showStateCity(this.value,id)" >
			<option value="" selected>Please Select</option>
			<?php 
			$auUsr=$_objAdmin->_getSelectList2('state','state_id,state_name',''," ORDER BY state_name"); 
			if(is_array($auUsr)){ ?>
			<?php
			for($i=0;$i<count($auUsr);$i++){?>
			<option value="<?php echo $auUsr[$i]->state_id;?>" <?php if ($auUsr[$i]->state_id==$auRec[0]->state){ ?> selected <?php } ?> ><?php echo $auUsr[$i]->state_name;?></option>
			<?php }}?>
			</select>
			</td>
		</tr>
		
		<tr id="selectCity" style="display:none;">
			<th valign="top">Select City:</th>
			<td>
			<div id="outputcity"><div>
			</td>
		</tr>
			<tr>
				<th valign="top">Start Date:</th>
				<td><input type="text" name="from" id="from" class="date required" readonly /></td>
				<td></td>
			</tr>
			<tr>
				<th valign="top">End Date:</th>
				<td><input type="text" id="to" name="to" class="date required" readonly /></td>
				<td></td>
			</tr>
			<tr>
				<th valign="top">Status:</th>
				<td>
				<select name="status" id="status" class="styledselect_form_3">
				<option value="A" <?php if($auRec[0]->status=='A') echo "selected";?> >Active</option>
				<option value="I" <?php if($auRec[0]->status=='I') echo "selected";?> >Inactive</option>
				</select>
				</td>
				<td></td>
			</tr>
			
				
			<tr>
				<th>&nbsp;</th>
				<td valign="top">
					<input name="add" type="hidden" value="yes" />
					<input type="reset" value="Reset!" class="form-reset">
					<input type="button" value="Back" class="form-reset" onclick="location.href='manage_account.php';" />
					<input name="submit" class="form-submit" type="submit" id="submit" value="Save"  />
				</td>
			</tr>
			</table>
		</form>
			<!-- end id-form  -->
			</td>
			<td>
			<!-- right bar-->
			<?php //include("rightbar/item_bar.php") ?>
			<div class="clear"></div>
		</div>
<!-- end related-act-bottom -->
</div>
<!-- end related-activities -->
</td>
</tr>
<tr>
<td><img src="images/blank.gif" width="695" height="1" alt="blank" /></td>
<td></td>
</tr>
</table>
<div class="clear"></div>
</div>
<!--  end content-table-inner  -->
</td>
<td id="tbl-border-right"></td>
</tr>
<!--<tr>
	<th class="sized bottomleft"></th>
	<td id="tbl-border-bottom">&nbsp;</td>
	<th class="sized bottomright"></th>
</tr>-->
</table>
<div class="clear">&nbsp;</div>

</div>
<!--  end content -->
<div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->
<div class="clear">&nbsp;</div>
    
<!-- start footer --> 
<?php include("footer.php") ?>  
<!-- end footer -->
</body>
</html>