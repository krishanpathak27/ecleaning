<?php

include("includes/config.inc.php");
include("includes/function.php");

$_objAdmin = new Admin();
$objArrayList= new ArrayList();
if(!empty($_REQUEST['id']))
{
	$condi = " operator_id='".$_REQUEST['id']."'";
	$auRec=$_objAdmin->_getSelectList('table_account_admin as admin left join table_category as cat on cat.category_id = admin.category_id',"admin.*,cat.category_name",'',$condi);
	$auRec1=$_objAdmin->_getSelectList('table_web_users',"*",'',$condi);
	$max_permissible=$_objAdmin->_getSelectList2('table_company_goodwill_approving_limit',"*",'',$condi);
	// print_R($max_permissible);
}




?>

<?php include("header.inc.php");
include("company.inc.php");
$pageAccess=1;
$check=$objArrayList->checkAccess($pageAccess, basename($_SERVER['PHP_SELF']));
// echo $check;
 if($check == false){
 header('Location: ' . $_SERVER['HTTP_REFERER']);
 }

 ?>

 <style>
.sortable		{ padding:0; }
.sortable li	{ padding:4px 8px; color:#000; cursor:move; list-style:none; width:350px; background:#ddd; margin:10px 0; border:1px solid #999; }
#message-box		{ background:#fffea1; border:2px solid #fc0; padding:4px 8px; margin:0 0 14px 0; width:350px; }
</style>
<div class="clear"></div>
<!-- start content-outer -->
<div id="content-outer">
<!-- start content -->
	<div id="content">
	<div id="page-heading"><h1><span style="color: #d74343; font-family: Tahoma; font-weight: bold;"><?php if(isset($_GET['id'])){?>  Company <?php } else {?> Company <?php }?> </span></h1></div>
		<table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">
		<tr>
			<td id="tbl-border-left"></td>
			<td>
			<!--  start content-table-inner -->
			<div id="content-table-inner">
				<table border="0" width="100%" cellpadding="0" cellspacing="0" style="margin-left:10px;">
				<tr valign="top">
					<td>
					<!--  start message-red -->
					<?php if($err!=''){?>
					<div id="message-red">
						<table border="0" width="100%" cellpadding="0" cellspacing="0">
						<tr>
							<td class="red-left"> <?php echo $err; ?></td>
							<td class="red-right"><a class="close-red"><img src="images/icon_close_red.gif"   alt="" /></a></td>
						</tr>
						</table>
					</div>
					<?php }?>
					<!--  end message-red -->
					<?php if($sus!=''){?>
					<!--  start message-green -->
					<div id="message-green">
						<table border="0" width="100%" cellpadding="0" cellspacing="0">
						<tr>
							<td class="green-left"><?php echo $sus; ?></td>
							<td class="green-right"><a class="close-green"><img src="images/icon_close_green.gif"   alt="" /></a></td>
						</tr>
						</table>
					</div>
					<?php } ?>
					<!--  end message-green -->
					<div id="step-holder">
<div class="step-no">1</div>
<div class="step-dark-left">Login Form</div>
<div class="step-dark-right">&nbsp;</div>

<div class="clear"></div>
</div>
			<!-- start id-form -->
			<form name="frmPre" id="frmPre" method="post" action="<?php $_SERVER['PHP_SELF'];?>" enctype="multipart/form-data" >
			
				<table border="0" cellpadding="0" cellspacing="0"  id="id-form">
      <tr>
			<th valign="top">Select level:</th>
				<td>
				<select name="hierarchy_id" id="hierarchy_id" class="styledselect_form_4 required" onchange="loadParentUsers(this.value, 'srtord', 'loadDiv1');">
					<option value="">Please Select</option>
				<?php $usertype=$_objAdmin->_getSelectList('table_company_hierarchy',"*",''," status='A'"); 
				
                         
							if(is_array($usertype)){
							foreach($usertype as $value){
								?>
							<option value="<?php echo $value->hierarchy_id;?>" id="<?php echo $value->hierarchy_id;?>" 
						
						    <?php if($value->hierarchy_id==$auRec[0]->hierarchy_id){?> selected="selected" <? }?>>
						    
							<?php echo $value->user_label;?>
							</option>
							<?php } }?>
							</select>
				</td>
			
		</tr>







		<tr>
			<th valign="top"> Name:</th>
			<td><input type="text" name="company_user" id="company_user" class="required" value="<?php echo $auRec[0]->operator_name; ?>" /></td>
			<td></td>
		</tr>
		<tr id="select-category">
			<th valign="top"> Category:</th>
			<td>
			<select name="category_id" id="category_id" class="styledselect_form_3">
				<option value="">--Select Category--</option>
				<?php $aCategory=$_objAdmin->_getSelectList('table_category','category_id,category_name',''," status='A' AND cat_type = 'C' ORDER BY category_name");
				if(is_array($aCategory)){
				for($i=0;$i<count($aCategory);$i++){?>
				<option value="<?php echo $aCategory[$i]->category_id;?>" <?php if($aCategory[$i]->category_id==$auRec[0]->category_id) echo "selected";?> ><?php echo $aCategory[$i]->category_name;?></option>
				
				<?php }}?>
				</select>
			</td>
			<td></td>
		</tr>
		
		<tr id="max_permissible_section">

			<td colspan="4">
				<h3 style="color:#000;">Fill in the maximum number of batteries in each brand for Goodwill Permission</h3>
				<div style="margin-top: 10px;margin-bottom: 20px;overflow: scroll;max-height: 200px;">
					<table>
						<?php $aBrand=$_objAdmin->_getSelectList('table_brands','brand_id,brand_name',''," status='A' ORDER BY brand_name");
						if(is_array($aBrand)){
						for($i=0;$i<count($aBrand);$i++){?>
						<tr>
							<th valign="top">Brand <?php echo $aBrand[$i]->brand_name;?></th>
							<td><input type="text" name="max_brand[<?php echo $aBrand[$i]->brand_id;?>]" id="max_brand[<?php echo $aBrand[$i]->brand_id;?>]" value="<?php echo $max_permissible[$i]->max_permissible_gw;?>" placeholder="<?php echo $aBrand[$i]->brand_name;?> Max Permissible"/></td>
						</tr>
						<?php }}?>
						
					</table>
				</div>
			</td>
		</tr>

		</div>
		<!-- <tr>
			<th valign="top">Code:</th>
			<td><input type="text" name="company_code" id="company_code" class="required" value="<?php echo $auRec[0]->company_code; ?>" maxlength="255" /></td>
			<td></td>
			<td></td>
		</tr> -->

            <tr>
			<th valign="top"> Phone No:</th>
			<td><input type="text" name="phone_number" id="phone_number" class="required number minlength" maxlength="15" minlength="10" value="<?php echo $auRec[0]->operator_phone_number; ?>"/></td>
			<td></td>
		</tr>

        

                    
         <tr>
			<th valign="top">User name:</th>
			<td><input type="text" name="username" id="username" class="required" value="<?php echo $auRec1[0]->username; ?>"/>
			
			</td>
			<td></td>
		</tr>
		<tr>
			<th valign="top">Password:</th>
			<td><input type="password" name="password" id="password" <?php if($auRec1[0]->web_user_id!=''){ ?>class="text"<?php } else { ?>class="required minlength" <?php } ?>  minlength="8" /></td>
			<td>
			</td>
		</tr>
		<tr>
			<th valign="top">Confirm-Password:</th>
			<td><input type="password" name="conf_pass" id="conf_pass" equalto="#password" <?php if($auRec1[0]->web_user_id!=''){ ?>class="text"<?php } else { ?>class="required" <?php } ?>/></td>
			<td>
			</td>
		</tr>
		<tr>
			<th valign="top">Email ID:</th>
			<td><input type="text" name="email" id="email" class="required email" value="<?php echo $auRec1[0]->email_id; ?>"/></td>
			<td>
			</td>
		</tr>
		<th>&nbsp;</th>				
					
			<tr>
						<th valign="top">Status:</th>
						<td>
						<select name="status" id="status" class="styledselect_form_3">
						<option value="A" <?php if($auRec[0]->status=='A') echo "selected";?> >Active</option>
						<option value="I" <?php if($auRec[0]->status=='I') echo "selected";?> >Inactive</option>
						</select>
						</td>
						<td></td>
					</tr>
					
					
					<tr>
						<th>&nbsp;</th>
						<td valign="top">
			              <input name="company_add" type="hidden" value="yes" />
			              <input name="comp_id" type="hidden" value="<?php echo $_REQUEST['id']; ?>" />
			              <input name="account_id" type="hidden" value="<?php echo $_SESSION['accountId']; ?>" />
			              <input name="start_date" type="hidden" value="<?php echo $date; ?>" />
			             <input name="end_date" type="hidden" value="<?php echo $_SESSION['EndDate']; ?>" />
                           
                           <input type="hidden" name="sal_id" value="<?php echo $_SESSION['salId']; ?>" />
			               <input type="hidden" name="web_id" value="<?php echo $auRec1[0]->web_user_id; ?>"/>
			               <input type="hidden" name="user_type" value="2" />
                         <input type="button" value="Back" class="form-reset" onclick="location.href='company_user.php';" />
			             <input type="reset" value="Reset!" class="form-reset">
			           
			             <input name="save" class="form-submit" type="submit" id="save" value="Save" />
			           
		</td>
		</tr>
					</tr>
					</table>
				</form>
			<!-- end id-form  -->
			</td>
			
			<td><?php /*function create_pyramid($limit) {
					for($row = 1; $row < $limit; $row ++) {
						$stars = str_repeat('*', ($row - 1) * 2 + 1);
						$space = str_repeat(' ', $limit - $row);
						echo $space . $stars . '<br/>';
					}
				}
					echo "<pre>" ;
				create_pyramid(10);*/?>
				
			<!--</div>-->
				
				
				</td>
			</tr>
			</table>
			<!-- right bar-->
			
			<div class="clear"></div>
			</div>
			</td>
		</tr>
	</table>
<div class="clear"></div>
</div>
<!--  end content-table-inner  -->
<div class="clear">&nbsp;</div>

</div>
<!--  end content-outer -->
<div class="clear">&nbsp;</div>
    
<!-- start footer --> 
<?php include("footer.php") ?>  
<!-- end footer -->
</body>
</html>
<script>
// $(document).ready(function(){
// 	$("#max_permissible_section_label").hide();
// 	    	$("#max_permissible_section").hide();
// });
	$("#hierarchy_id").change(function(){
	    var selected_hierarchy = $("#hierarchy_id").val();
	    if(selected_hierarchy=='6')					//Product Category Head
	    {
	    	
	    	$("#max_permissible_section_label").show();
	    	$("#max_permissible_section").show();
	    	$("#select-category").show();
	    }
	    else if(selected_hierarchy =='8')			//Call Center
	    {
	    	$("#max_permissible_section_label").hide();
	    	$("#max_permissible_section").hide();
	    	$("#select-category").hide();
	    }
	    else if(selected_hierarchy =='7')			//GM Services
	    {
	    	
	    	$("#max_permissible_section_label").hide();
	    	$("#max_permissible_section").hide();
	    	$("#select-category").hide();
	    }
	    else
	    {
	    	$("#max_permissible_section_label").hide();
	    	$("#max_permissible_section").hide();
	    	$("#select-category").hide();
	    }
	});
</script>