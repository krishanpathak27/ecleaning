<script src="javascripts/jquery-1.8.2.js" type="text/javascript"></script>
<script src="javascripts/jquery-ui.js"></script>
<script type="text/javascript" src="javascripts/validate.js"></script>
<script type="text/javascript" src="javascripts/select_dropdowns.js"></script>
 <script>
  $(function() {
    $( "#datepicker" ).datepicker({dateFormat: 'd M yy',changeMonth: true, changeYear: true, yearRange: '1930:2050'});
	
  });

  <?php if($auRec[0]->taluka_id!=''){?>
	  $(document).ready(function(){
		 // alert("Heloo");
			showTalukaCity(<?php echo $auRec[0]->city.",". $auRec[0]->taluka_id; ?>);
			showTalukaMarket(<?php echo $auRec[0]->taluka_id.",". $auRec[0]->market_id; ?>);
			
			 
		  });

	 <?php }  ?>
  </script>
  
<!--  start message-blue -->
	<?php if($auRec[0]->distributor_id=="" && $dis_name_err==''){ ?>
	<div id="message-blue">
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
	<tr>
		<!--<td class="blue-left">Import Distributors. <a href="distributors.php?import">Your import file to ensure you have the file perfect for the import.</a> </td>-->
		<td class="blue-left">Import Distributors. <a href="#">Your import file to ensure you have the file perfect for the import.</a> </td>
		<td class="blue-right"><a class="close-blue"><img src="images/icon_close_blue.gif"   alt="" /></a></td>
	</tr>
	</table>
	</div>
	<?php } ?>
<!--  end message-blue -->
<!--  start message-red -->
	<?php if($dis_name_err!=''){?>
	<div id="message-red">
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
	<tr>
		<td class="red-left">Error. <?php echo $dis_name_err; ?></td>
		<td class="red-right"><a class="close-red"><img src="images/icon_close_red.gif"   alt="" /></a></td>
	</tr>
	</table>
	</div>
	<?php } ?>
<!--  end message-red -->
<!--  start step-holder -->
<div id="step-holder">
<div class="step-no">1</div>
<div class="step-dark-left">Distributor Form</div>
<div class="step-dark-right">&nbsp;</div>
<div class="step-no-off">2</div>
<div class="step-light-left">Login Form</div>
<div class="step-light-right">&nbsp;</div>
<div class="step-no-off">3</div>
<div class="step-light-left">Map</div>
<div class="step-light-round">&nbsp;</div>
<div class="clear"></div>
</div>
	
	<!--  end step-holder -->
	<form name="frmPre" id="frmPre" method="post" action="distributors.php" enctype="multipart/form-data" >
		<table border="0" cellpadding="0" cellspacing="0"  id="id-form">
		<tr>
			<th valign="top">Distributor Name:</th>
			<td><input type="text" name="distributors_name" id="distributors_name" class="required" value="<?php echo $auRec[0]->distributor_name; ?>" maxlength="255" /></td>
			<td></td>
			<td></td>
		</tr>
		
        <tr>
			<th valign="top">Distributor Class:</th>
			<td>
			<select name="relationship_id" id="relationship_id" class="styledselect_form_3">
			<option value="">Please Select</option>
			<?php 
			//$condi=" and relationship_code!=''";
			$condi=" and relationship_code!='' AND relationship_type IN (3) AND status='A'";
			$aDis=$_objAdmin->_getSelectList('table_relationship','*','',"$condi"); 
			if(is_array($aDis)){
			for($i=0;$i<count($aDis);$i++){?>
			<option value="<?php echo $aDis[$i]->relationship_id;?>" <?php if($auRec[0]->relationship_id==$aDis[$i]->relationship_id){ echo 'selected';} ?> ><?php echo $aDis[$i]->relationship_code;?></option>
			<?php }} ?>
			</select>
			</td>
		</tr>


		<tr>
			<th valign="top">Division:</th>
			<td>
				<select name="division_id" id="division_id" class="styledselect_form_4 required">
					<option value="">Select Division</option>
					<?php 
					$condi = "";
					if(!empty($divisionIdString)) {
						$condi = " AND division_id IN (".$divisionIdString.") ";	
					}
					
					$branchList=$_objAdmin->_getSelectList('table_division',"division_id,division_name",''," status='A' $condi ");
						for($i=0;$i<count($branchList);$i++){
						
						if($branchList[$i]->division_id==$auRec[0]->division_id){$select="selected";} else {$select="";}
							
					 ?>
					 <option value="<?php echo $branchList[$i]->division_id; ?>" <?php echo $select ?>><?php echo $branchList[$i]->division_name; ?></option>
					 <?php } ?>
				</select>
			</td>
			<td></td>
		</tr>




		<tr>
		<th valign="top">Brands:</th>
			<td>
			<select name="brand_id" id="brand_id" class="styledselect_form_4 required">
			<option>Please Select</option>
			<?php $brands = $_objAdmin->_getSelectList('table_brands','*','',""); 

			if(is_array($brands)){
			for($i=0;$i<count($brands);$i++){?>
			<option value="<?php echo $brands[$i]->brand_id;?>" brand_code="<?php echo $brands[$i]->brnd_cd;?>" <?php if($auRec[0]->brand_id == $brands[$i]->brand_id){ echo 'selected';} ?> ><?php echo ucwords(strtolower(trim($brands[$i]->brand_name)));?></option>
			<?php }} ?>
			</select>
			</td>
		</tr>


		
		

		<tr>
	    <th valign="top">Interested:</th>
		<td colspan="2">
		<table width="20%">
		<tr><td><input type="radio" name="display_outlet" value="hot" <?php if($auRec[0]->display_outlet=='hot'){ ?> checked <?php } ?>>Hot</td>
		<td><input type="radio" name="display_outlet" value="cold" <?php if($auRec[0]->display_outlet=='cold'){ ?> checked <?php } ?>>Cold</td>
		</tr>
		</table>
		</td>
		</tr>
		
		<!-- <tr>
			<th valign="top">Distributor Category:</th>
			<td>
				<div style="width:200px; height:100px;overflow:auto;" >
				<table border="0" cellpadding="0" cellspacing="0"  id="id-form">
				<tr>
					<th valign="top"></th>
				</tr>
				<input type="checkbox" name="all" id="all" onclick="checkall(this);"> Check All
<?php $auCol=$_objAdmin->_getSelectList('table_category',"category_id,category_name",''," status='A' AND cat_type = 'C'");
				for($i=0;$i<count($auCol);$i++){
				?>
				<tr>
					<td><input type="checkbox" class="getCheckBoxAll" name="category_id[]" <?php 
						if($auRec[0]->distributor_id!=''){
						$auchk=$_objAdmin->_getSelectList2('table_distributors_category',"*",''," category_id=".$auCol[$i]->category_id." and distributor_id=".$auRec[0]->distributor_id);
						if(is_array($auchk)){
						echo "checked";
					}
				}
				?> value="<?php echo $auCol[$i]->category_id;?>" > <?php echo $auCol[$i]->category_name;?>
					</td>
				</tr>
				<?php } ?>
				</table>
				</div>
			</td>
			<td></td>
			<td></td>
		</tr> -->
		
		<tr>
			<th valign="top">Distributor Phone No:</th>
			<td><input type="text" name="distributor_phone_no" id="distributor_phone_no" class="required number" maxlength="15" value="<?php echo $auRec[0]->distributor_phone_no; ?>" placeholder="Phone Number 1"/></td>
			<td><input type="text" name="distributor_phone_no2" id="distributor_phone_no2" class="text number" maxlength="15" value="<?php echo $auRec[0]->distributor_phone_no2; ?>" placeholder="Phone Number 2"/></td>
			<td><input type="text" name="distributor_phone_no3" id="distributor_phone_no3" class="text number" maxlength="15" value="<?php echo $auRec[0]->distributor_phone_no3; ?>" placeholder="Phone Number 3"/></td>
		</tr>
		<tr>
			<th valign="top">Distributor Landline No:</th>
			<td><input type="text" name="distributor_leadline_no" id="distributor_leadline_no" class="text number minlength" maxlength="15" minlength="6" value="<?php echo $auRec[0]->distributor_leadline_no; ?>"/></td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<th valign="top">Aadhaar No:</th>
			<td><input type="text" name="aadhar_no" id="aadhar_no" class="text"  maxlength="15" minlength="6" value="<?php echo $auRec[0]->aadhar_no; ?>"/></td>
			<td></td>
			<td></td>
		</tr>

		<tr>
			<th valign="top">Pan No:</th>
			<td><input type="text" name="pan_no" id="pan_no" class="text"  maxlength="15" minlength="6" value="<?php echo $auRec[0]->pan_no; ?>"/></td>
			<td></td>
			<td></td>
		</tr>
		
		<tr>
			<th valign="top">Send SMS Mobile No:</th>
			<td><input type="text" name="sms_number" id="sms_number" class="required number minlength" maxlength="10" minlength="10" value="<?php echo $auRec[0]->sms_number; ?>"/></td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<th valign="top">Distributor Email-ID:</th>
			<td><input type="text" name="distributor_email" id="distributor_email" class="required email minlength" maxlength="100" value="<?php echo $auRec[0]->distributor_email; ?>" placeholder="Email-ID 1"/></td>
			<td><input type="text" name="distributor_email2" id="distributor_email2" class="text email minlength" maxlength="100" value="<?php echo $auRec[0]->distributor_email2; ?>" placeholder="Email-ID 2"/></td>
			<td><input type="text" name="distributor_email3" id="distributor_email3" class="text email minlength" maxlength="100" value="<?php echo $auRec[0]->distributor_email3; ?>" placeholder="Email-ID 3"/></td>
		</tr>
		<tr>
			<th valign="top">Distributor Address:</th>
			<td colspan="3">
			<input type="text" style="width:590px;" name="distributor_address" id="distributor_address" class="text" value="<?php echo $auRec[0]->distributor_address; ?>" placeholder="Address Line 1"/>
			</td>
		</tr>
		<tr>
			<th valign="top"></th>
			<td colspan="3">
			<input type="text" style="width:590px;" name="distributor_address2" id="distributor_address2" class="text" value="<?php echo $auRec[0]->distributor_address2; ?>" placeholder="Address Line 2"/>
			</td>
		</tr>
		<!-- <tr>
			<th valign="top">Distributor City:</th>
			<td><input type="text" name="address1" id="address1" class="required" value="<?php echo $auRec[0]->distributor_location; ?>"/></td>
			<td></td>
			<td></td>
		</tr> -->

		<tr>
			<th valign="top">Select Country:</th>
			<td>
			<select name="country_id" id="country_id" class="required styledselect_form_3 country_id">
				<option value="">Please Select</option>
				<?php $country = $_objAdmin->_getSelectList2('country','country_id,country_name',''," status = 'A' ORDER BY country_name"); 
					if(is_array($country)){
					 foreach($country as $value):?>
					 	<option value="<?php echo $value->country_id;?>" <?php if($value->country_id==$auRec[0]->country) echo "selected";?> ><?php echo $value->country_name;?></option>
				<?php endforeach; }?>
			</select>
			</td>
			<td></td>
		</tr>
		<tr>
			<th valign="top">Select Region:</th>
			<td>
			<select name="region_id" id="region_id" class="required styledselect_form_3 region_id">
				<option value="">Please Select</option>
				<?php if(!empty($auRec[0]->region_id) && $auRec[0]->region_id > 0){?>
				<?php $region = $_objAdmin->_getSelectList2('table_region','region_id,region_name',''," status = 'A' and country_id='".$auRec[0]->country."' ORDER BY region_name"); 
					if(is_array($region)){
					 foreach($region as $value):?>
					 	<option value="<?php echo $value->region_id;?>" <?php if($value->region_id==$auRec[0]->region_id) echo "selected";?> ><?php echo $value->region_name;?></option>
				<?php endforeach; }?>

				<?php }?>	
			</select>
			</td>
			<td></td>
		</tr>
		<tr>
			<th valign="top">Select Zone:</th>
			<td>
			<select name="zone_id" id="zone_id" class="required styledselect_form_3 zone_id">
				<option value="">Please Select</option>
				<?php if(!empty($auRec[0]->zone) && $auRec[0]->zone > 0){?>
				<?php $zone = $_objAdmin->_getSelectList2('table_zone','zone_id,zone_name',''," status = 'A' and region_id='".$auRec[0]->region."' ORDER BY zone_name"); 
					if(is_array($zone)){
					 foreach($zone as $value):?>
					 	<option value="<?php echo $value->zone_id;?>" <?php if($value->zone_id==$auRec[0]->zone) echo "selected";?> ><?php echo $value->zone_name;?></option>
				<?php endforeach; }?>
				<?php }?>
			</select>
			</td>
			<td></td>
		</tr>

		<tr>
			<th valign="top">Select State:</th>
			<td>
			<select name="state_id" id="state_id" class="required styledselect_form_3 state_id">
				<option value="">Please Select</option>
				<?php if(!empty($auRec[0]->state) && $auRec[0]->state > 0){?>
				<?php $state = $_objAdmin->_getSelectList2('state','state_id,state_name,state_code',''," status = 'A' and zone_id='".$auRec[0]->zone."' ORDER BY state_name"); 
					if(is_array($state)){
					 foreach($state as $value):?>
					 	<option value="<?php echo $value->state_id;?>" state_code="<?php echo $value->state_code;?>" <?php if($value->state_id==$auRec[0]->state) echo "selected";?> state_code=""><?php echo $value->state_name;?></option>
				<?php endforeach; }?>
				<?php }?>
			</select>
			</td>
			<td></td>
		</tr>
		<tr>
			<th valign="top">Select City:</th>
			<td>
			<select name="city_id" id="city_id" class="required styledselect_form_3 city_id">
				<option value="">Please Select</option>
				<?php if(!empty($auRec[0]->city) && $auRec[0]->city > 0){?>
				<?php $city = $_objAdmin->_getSelectList2('city','city_id,city_name',''," status = 'A' and state_id='".$auRec[0]->state."' ORDER BY city_name"); 
					if(is_array($city)){
					 foreach($city as $value):?>
					 	<option value="<?php echo $value->city_id;?>" <?php if($value->city_id==$auRec[0]->city) echo "selected";?> ><?php echo $value->city_name;?></option>
				<?php endforeach; }?>
				<?php }?>
			</select>
			</td>
			<td></td>
		</tr>
		
		<tr>
			<th valign="top">Zipcode:</th>
			<td><input type="text" name="zipcode" id="zipcode" class="text number minlength" maxlength="6" minlength="6" value="<?php echo $auRec[0]->zipcode; ?>"/></td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<th valign="top">Contact Person:</th>
			<td><input type="text" name="contact_person" id="contact_person" class="required" value="<?php echo $auRec[0]->contact_person; ?>" placeholder="Contact Person 1"/></td>
			<td><input type="text" name="contact_person2" id="contact_person2" class="text" value="<?php echo $auRec[0]->contact_person2; ?>" placeholder="Contact Person 2"/></td>
			<td><input type="text" name="contact_person3" id="contact_person3" class="text" value="<?php echo $auRec[0]->contact_person3; ?>" placeholder="Contact Person 3"/></td>
		</tr>
		<tr>
			<th valign="top">Contact Phone No:</th>
			<td><input type="text" name="contact_number" id="contact_number" class="required number minlength" maxlength="15" minlength="10" value="<?php echo $auRec[0]->contact_number; ?>" placeholder="Contact Phone 1"/></td>
			<td><input type="text" name="contact_number2" id="contact_number2" class="text number minlength" maxlength="15" minlength="10" value="<?php echo $auRec[0]->contact_number2; ?>" placeholder="Contact Phone 2"/></td>
			<td><input type="text" name="contact_number3" id="contact_number3" class="text number minlength" maxlength="15" minlength="10" value="<?php echo $auRec[0]->contact_number3; ?>" placeholder="Contact Phone 3"/></td>
		</tr>
		<tr>
			<th valign="top">Distributor DOB:</th>
			<td><input type="text" id="datepicker" name="distributor_dob" class="date" value="<?php echo $_objAdmin->_changeDate($auRec[0]->distributor_dob); ?>"  readonly /></td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<th valign="top">Distributor Code:</th>
			<td><input type="text" name="distributor_code" id="distributor_code" class="required" value="<?php echo $auRec[0]->distributor_code; ?>" maxlength="255" readonly/><p style="padding-top: 2px;">To be Generated Automatically</p></td>
			<td></td>
			<td></td>
		</tr>
		
		<tr>
			<th valign="top">Status:</th>
			<td>
			<select name="status" id="status" class="styledselect_form_3">
			<option value="A" <?php if($auRec[0]->status=='A') echo "selected";?> >Active</option>
			<option value="I" <?php if($auRec[0]->status=='I') echo "selected";?> >Inactive</option>
			</td>
			<td></td>
		</tr>
		<tr>
		<th>&nbsp;</th>
		<td valign="top" colspan="3">
			<input name="distributor_add" type="hidden" value="yes" />
			<input name="dis_id" type="hidden" value="<?php echo $auRec[0]->distributor_id; ?>" />
			<input name="account_id" type="hidden" value="<?php echo $_SESSION['accountId']; ?>" />
			<input name="start_date" type="hidden" value="<?php echo $date; ?>" />
			<input name="end_date" type="hidden" value="<?php echo $_SESSION['EndDate']; ?>" />

			<?php 
			if($auRec[0]->new!=''){
			$_SESSION['new_ret']="New";
			?>
			<input type="button" value="Back" class="form-reset" onclick="location.href='new_distributors.php'" />
			<input name="submit" class="form-submit" type="submit" id="submit" value="Approve & Continue" />
			<input name="submit" class="form-submit" type="submit" id="submit" value="Approve & Exit" />
			<?php } else { ?>

			<!--<input type="reset" value="Reset!" class="form-reset">-->
			<input type="button" value="Back" class="form-reset" onclick="location.href='distributors.php';" />
			<input name="save" class="form-submit" type="submit" id="save" value="Save" />
			<input name="submit" class="form-submit" type="submit" id="submit" value="Save & Continue" />
			<?php } ?>
		</td>
		</tr>
		</table>

	</form>
<script>
	$('#contact_number').blur(function(){
		var division = $('#division_id').val();
		var state_code = $('#state_id option:selected').attr('state_code');
		var brand_code = $('#brand_id option:selected').attr('brand_code');
   		$.ajax({
	    	url: "generate_distributor_code.php",
	    	type: "post",
	    	data: {division:division,state_code:state_code,brand_code:brand_code},
	    	DataType: 'text',
	     	success: function(result){
	     		$('[id$=distributor_code]').val(result);
	    }});
	});
</script>