<?php
include("includes/config.inc.php");
include("includes/function.php");
$page_name="Add Master Category";

$_objAdmin = new Admin();
$objArrayList= new ArrayList();

if(isset($_POST['add']) && $_POST['add'] == 'yes')
{
	if($_REQUEST['id']!="") 
	{
		$condi=	" category_name='".$_POST['category_name']."'  and category_id<>'".$_REQUEST['id']."'";
	}
	else
	{
		$condi=	" category_name='".$_POST['category_name']."' ";
	}
	$auRec=$_objAdmin->_getSelectList('table_category',"*",'',$condi);
	if(is_array($auRec))
	{
		$err="Category already exists in the system.";	
		$auRec[0]=(object)$_POST;						
	}
	else
	{
		if($_REQUEST['id']!="") 
		{
			$cid=$_objAdmin->updateMasterCategory($_REQUEST['id']);
			$sus="Category has been updated successfully.";
		}
		else 
		{
			$cid=$_objAdmin->addMasterCategory();
			//$_objAdmin->addMasterCategoryRelation($cid);
			$sus="Category has been added successfully.";
		}
	}
}


if(isset($_REQUEST['id']) && $_REQUEST['id']!="")
{
	$auRec=$_objAdmin->_getSelectList('table_category',"*",''," cat_type = 'S' AND category_id=".$_REQUEST['id']);
	if(count($auRec)<=0) header("Location: master_category.php");
}


?>
<?php include("header.inc.php");

$pageAccess=1;
$check=$objArrayList->checkAccess($pageAccess, basename($_SERVER['PHP_SELF']));

if($check == false){
header('Location: ' . $_SERVER['HTTP_REFERER']);
}

 ?>
<script type="text/javascript" src="javascripts/validate.js"></script>
<div class="clear"></div>
<!-- start content-outer -->
<div id="content-outer">
<!-- start content -->
<div id="content">
<div id="page-heading"><h1><span style="color: #d74343; font-family: Tahoma; font-weight: bold;">Master Category</span></h1></div>
		<table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">
		<tr>
			<!--<td id="tbl-border-left"></td>-->
			<td>
			<!--  start content-table-inner -->
			<div id="content-table-inner">
			<table border="0" width="100%" cellpadding="0" cellspacing="0">
				<tr valign="top">
					<td>
					<!--  start message-red -->
					<?php if($err!=''){?>
					<div id="message-red">
						<table border="0" width="100%" cellpadding="0" cellspacing="0">
						<tr>
							<td class="red-left"> <?php echo $err; ?></td>
							<td class="red-right"><a class="close-red"><img src="images/icon_close_red.gif"   alt="" /></a></td>
						</tr>
						</table>
					</div>
					<?php } ?>
					<!--  end message-red -->
					<?php if($sus!=''){?>
					<!--  start message-green -->
					<div id="message-green">
						<table border="0" width="100%" cellpadding="0" cellspacing="0">
						<tr>
							<td class="green-left"><?php echo $sus; ?></td>
							<td class="green-right"><a class="close-green"><img src="images/icon_close_green.gif"   alt="" /></a></td>
						</tr>
						</table>
					</div>
					<?php } ?>
					<!--  end message-green -->
					
					<!-- start id-form -->
						<form name="frmPre" id="frmPre" method="post" action="add_master_category.php" enctype="multipart/form-data" >
							<table border="0" cellpadding="0" cellspacing="0"  id="id-form">
							<tr>
								<th valign="top">Category name:</th>
								<td><input type="text" name="category_name" id="category_name" class="required" value="<?php echo $auRec[0]->category_name; ?>" maxlength="50" /></td>
								<td></td>
							</tr>
							<tr>
								<th valign="top">Category Code:</th>
								<td><input type="text" name="category_code" id="category_code" class="text" value="<?php echo $auRec[0]->category_code; ?>" maxlength="50" /></td>
								<td></td>
							</tr>
							<tr>
								<th valign="top">Status:</th>
								<td>
								<select name="status" id="status" class="styledselect_form_3">
								<option value="A" <?php if($auRec[0]->status=='A') echo "selected";?> >Active</option>
								<option value="I" <?php if($auRec[0]->status=='I') echo "selected";?> >Inactive</option>
								</select>
								</td>
								<td></td>
							</tr>
							<tr>
								<th>&nbsp;</th>
								<td valign="top">
									<input name="account_id" type="hidden" value="<?php echo $_SESSION['accountId']; ?>" />
									<input name="id" type="hidden" value="<?php echo $auRec[0]->category_id; ?>" />
									<input name="add" type="hidden" value="yes" />
									<!--<input type="reset" value="Reset!" class="form-reset">-->
									<input type="button" value="Back" class="form-reset" onclick="location.href='master_category.php';" />
									<input name="submit" class="form-submit" type="submit" id="submit" value="Save" />
									
								</td>
							</tr>
							</table>
						</form>
					<!-- end id-form  -->
					</td>
				</tr>
			</table>
		<div class="clear"></div>
		</div>
	<!--  end content-table-inner  -->
	</td>
	<td id="tbl-border-right"></td>
	</tr>
</table>
<div class="clear">&nbsp;</div>
</div>
<!--  end content -->
<div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->
<div class="clear">&nbsp;</div>
    
<!-- start footer --> 
<?php include("footer.php") ?>  
<!-- end footer -->
</body>
</html>