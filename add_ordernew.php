<?php
include("includes/config.inc.php");
include("includes/function.php");
include("includes/globalarraylist.php");
	  $_insertValues = new Admin();
//echo '<pre>';
//echo print_r($_SESSION);
$page_name="Add New Order";
if(isset($_POST['proceed']) && $_POST['proceed']!='')
{	    
		 $total=0;
		 foreach($_POST['qty'] as $key=>$value)
		 {
		 $total=$total+$value;
		 }
		
        
        $data['account_id']=$_SESSION['accountId'];
		$data['salesman_id']=$_SESSION['salesmanId'];
		$data['distributor_id']=$_SESSION['distributorId'];
		$data['retailer_id']= '';
		$data['date_of_order']=date('Y-m-d');
		$data['time_of_order']=date("H:i", time());
		$data['lat']= 0;
		$data['lng']= 0;
		$data['accuracy_level']= 0;
		$data['comments']= 'by web panel';
		$data['order_type']= 'yes';
		$data['location_provider']= 'web';
		$data['tag_id']= 0;
		$data['tag_description']= 'web Panel order';
		$data['total_invoice_amount']=$total;
		$data['acc_total_invoice_amount']=$total;
		$data['acc_free_item_id']= 0;
		$data['free_item_qty']= 0;
		$data['discount_id']=0;
		$data['acc_discount_type']=0;
		$data['acc_discount_amount']=0;
		$data['acc_dicount_percentage']=0;
		$data['acc_free_item_id']=0;
		$data['acc_free_item_qty']=0;
		$data['acc_discount_id']= 0;
		$data['last_update_date']= date('Y-m-d');
		$data['last_update_status']= 'New';
		$data['order_status']= 'A';
		$order_id=$_insertValues->_dbInsert($data,'table_order');
		
	// for table_order_detail
	if($order_id!="")
	 {
	 $totalval=0;	
	 foreach($_POST['qty'] as $key=>$value)
	 {
	 
	 $data_order_detail['order_id']=$order_id;
	 $data_order_detail['item_id']=$key;
	 $data_order_detail['quantity']=$value;
	 $data_order_detail['price']=$value;
	 $data_order_detail['total']=$total;
	 $data_order_detail['acc_quantity']=$value;
	 $data_order_detail['acc_total']=$total;
	 $data_order_detail['discount_desc']=0;
	 $data_order_detail['color_id']=0;
	 $data_order_detail['type']=1;
	 $data_order_detail['color_type']=1;
	 $data_order_detail['price_type']=1;
	 $data_order_detail['free_item_id']=0;
	 $data_order_detail['discount_id']=0;
	 $data_order_detail['discount_type']=0;
	 $data_order_detail['discount_amount']=0;
	 $data_order_detail['discount_percentage']=0;
	 $data_order_detail['total_free_quantity']=0;
	 $data_order_detail['acc_discount_type']=0;
	 $data_order_detail['acc_discount_amount']=0;
	 $data_order_detail['acc_dicount_percentage']=0;
	 $data_order_detail['acc_free_item_id']=0;
	 $data_order_detail['acc_free_item_qty']=0;
	 $data_order_detail['acc_discount_id']=0;
	 $data_order_detail['last_update_date']=date('Y-m-d');
	 $data_order_detail['last_update_status']='New';
	 $data_order_detail['order_detail_status']=1;
	 $order_detail_id=$_insertValues->_dbInsert($data_order_detail,'table_order_detail');
	 }
	 header('Location:success.php?ord='.$order_id);
	 }
		

} 
?>

<?php include("header.inc.php") ?>
<script type="text/javascript">
	function showloader()
	{
		$('#Report').hide();
		$('#loader').show();
	}
</script>
<script>
function getProductList(id){
	if(typeof id!='undefined' && id!='') {
	$.ajax({
	'type': 'GET', 
	'url': 'productList.php', 
	'data': 'id='+id, 
	'success' : function(data) {
		//alert(data);
		console.log(data);
		$("#showContainerId").html(data); 
		} 
	     });
	}
}







</script>
<script type="text/javascript">

    function PrintElem(elem)
    {
        Popup($(elem).html());
    }

    function Popup(data) 
    {
		
        var mywindow = window.open('', 'Report');
		
        mywindow.document.write('<html><head><title>Distance Traveled</title>');
		mywindow.document.write('<table><tr><td><b>From Date:</b> <?php echo $from_date; ?></td><td><b>To Date:</b> <?php echo $to_date; ?></td></tr></table>');
        /*optional stylesheet*/ //mywindow.document.write('<link rel="stylesheet" href="main.css" type="text/css" />');
        mywindow.document.write('</head><body >');
        mywindow.document.write(data);
        mywindow.document.write('</body></html>');

        mywindow.print();
        mywindow.close();
        return true;
    }
$(document).ready(function()
{
<?php if($_POST['submit']=='Export to Excel'){ ?>
tableToExcel('report_export', 'Distance Traveled', 'Distance Traveled.xls');	
<?php } ?>
});	

</script>

<script src="javascripts/dateNextPrev.js" type="text/javascript"></script>
<!-- start content-outer -->
<input name="pagename" type="hidden"  id="pagename" value="admin_order_list.php" />
<div id="content-outer">
<!-- start content -->
<div id="content">
<div id="loader" style="position:absolute; margin-left:40%; margin-top:10%;"></div>
<div id="page-heading"><h1><span style="color: #d74343; font-family: Tahoma; font-weight: bold;">Take Order</span></h1></div>
<table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">

<tr>
	<td>
	<!--  start content-table-inner -->
	<div id="content-table-inner">
	<div id="page-heading" align="left" >
	<form name="frmPre" id="frmPre" method="post" action="" enctype="multipart/form-data" >
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
	
	<tr>
			<td width="50%" align="right"><h3>Category:</h3></td>
			<td width="50%" align="left">
			<select name="category_id" id="category_id" class="styledselect_form_3" onchange="getProductList(this.value);">
				<option value="">Please select</option>
				<?php $aCategory=$_objAdmin->_getSelectList('table_category','*',''," ORDER BY category_name"); 
						if(is_array($aCategory)){
						for($i=0;$i<count($aCategory);$i++){?>
						<option value="<?php echo $aCategory[$i]->category_id;?>" <?php if($aCategory[$i]->category_id==$_SESSION['categoryID']) echo "selected";?> ><?php echo $aCategory[$i]->category_name;?></option><?php }}?>
			</select>
			<?php if(isset($error['category']) && $error['category']!=""){ echo '<span  style="color:red;">'.$error['category'].'</span>'; } ?>
			<span id="catname" style="color:red;"></span>
			</td>
			</tr>
		
	</table>
	</form>
	</div>
	<tbody id="" ></tbody>
	<table border="0" width="100%" cellpadding="0" cellspacing="0" id="showContainerId">
	
	</table>
	</div>
		</td>
	</tr>
	<tr>
		<td><img src="images/blank.gif" width="695" height="1" alt="blank" /></td>
		<td></td>
	</tr>
	</table>
<div class="clear"></div>
</div>
<!--  end content-table-inner  -->
</td>
<td id="tbl-border-right"></td>
</tr>
<!--<tr>
	<th class="sized bottomleft"></th>
	<td id="tbl-border-bottom">&nbsp;</td>
	<th class="sized bottomright"></th>
</tr>-->
</table>
<div class="clear">&nbsp;</div>
</div>
<!--  end content -->
<div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->
<div class="clear">&nbsp;</div> 
<!-- start footer -->         
<?php include("footer.php") ?>
<?php unset($_SESSION['SalAttList']); ?>
<!-- end footer -->
<script src="javascripts/jquery-1.8.2.js" type="text/javascript"></script>
<script src="javascripts/jquery-ui.js"></script>
<script type="text/javascript" src="javascripts/validate.js"></script>
<script type="text/javascript">
// Popup window code
function newPopup(url) {
	popupWindow = window.open(
		url,'popUpWindow','height=500,width=600,left=200,top=100,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no,status=yes')
}
</script>
<script>
    $(function() {
        $( "#from" ).datepicker({
			dateFormat: "d M yy",
            defaultDate: "w",
            changeMonth: true,
            numberOfMonths: 1,
            onSelect: function( selectedDate ) {
                $( "#to" ).datepicker( "option", "minDate", selectedDate );
            }
        });
        $( "#to" ).datepicker({
			dateFormat: "d M yy",
            defaultDate: "-w",
            changeMonth: true,
            numberOfMonths: 1,
            onSelect: function( selectedDate ) {
                $( "#from" ).datepicker( "option", "maxDate", selectedDate );
            }
        });
    });
</script>
<script type='text/javascript'>//<![CDATA[ 
var tableToExcel = (function () {
        var uri = 'data:application/vnd.ms-excel;base64,'
        , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table><tr><td><b>From Date:</b> <?php echo $from_date; ?></td><td><b>To Date:</b> <?php echo $to_date; ?></td></tr></table><table>{table}</table></body></html>'
        , base64 = function (s) { return window.btoa(unescape(encodeURIComponent(s))) }
        , format = function (s, c) { return s.replace(/{(\w+)}/g, function (m, p) { return c[p]; }) }
        return function (table, name, filename) {
            if (!table.nodeType) table = document.getElementById(table)
            var ctx = { worksheet: name || 'Worksheet', table: table.innerHTML }

            document.getElementById("dlink").href = uri + base64(format(template, ctx));
            document.getElementById("dlink").download = filename;
            document.getElementById("dlink").click();

        }
    })()

//]]>  
</script>
</body>
</html>