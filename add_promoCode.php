<?php
include("includes/config.inc.php");
include("includes/function.php");
$page_name = "WhiteSpot Promo Code";
$_objAdmin = new Admin();
$objArrayList = new ArrayList();

if (isset($_POST['add']) && $_POST['add'] == 'yes') {
    if ($_POST['id'] != "") {
        $condi = " promo_code='" . $_POST['promo_code'] . "'  and promo_code_id<>'" . $_POST['promo_code_id'] . "'";
    } else {
        $condi = " promo_code='" . $_POST['promo_code'] . "' ";
    }
    $auRec = $_objAdmin->_getSelectList('table_promo_code', "*", '', $condi);
    if (is_array($auRec)) {
        $err = "Promo Code already exists in the system.";
    } else {
        $cid = $_objAdmin->addPromoCode();
        $sus = "Promo Code has been added successfully.";
        ?>
        <script type="text/javascript">
            window.location = "promoCode.php?suc="+'<?php echo $sus; ?>';
        </script>
        <?php
    }
    
}
?>


<?php
include("header.inc.php");
//$pageAccess=1;
//$check=$objArrayList->checkAccess($pageAccess, basename($_SERVER['PHP_SELF']));
//if($check == false){
//header('Location: ' . $_SERVER['HTTP_REFERER']);
//}
?>


<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <!-- BEGIN: Subheader -->
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">
                    Promo Code
                </h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="#" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__separator">
                        -
                    </li>
                    <li class="m-nav__item">
                        <a href="#" class="m-nav__link">
                            <span class="m-nav__link-text">
                                Master
                            </span>
                        </a>
                    </li>
                    <li class="m-nav__separator">
                        -
                    </li>
                    <li class="m-nav__item">
                        <a href="" class="m-nav__link">
                            <span class="m-nav__link-text">
                                Promo Code
                            </span>
                        </a>
                    </li>  
                </ul>
            </div> 
        </div>
    </div>
    <!-- END: Subheader -->
    <div class="m-content">
        <div class="row">
            <div class="col-lg-12">
                <!--begin::Portlet--> 
                <div class="m-portlet">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <span class="m-portlet__head-icon m--hide">
                                    <i class="la la-gear"></i>
                                </span>
                                <h3 class="m-portlet__head-text">
                                    Promo Code Form
                                </h3>
                            </div>
                        </div>
                    </div>
                    <!--begin::Form-->
                    <form class="m-form m-form--fit m-form--label-align-right" id="m_form_1"  method="post" action="add_promoCode.php" enctype="multipart/form-data">

                        <div class="m-portlet__body">
                            <div class="m-form__content">
                                <div class="m-alert m-alert--icon alert alert-danger m--hide" role="alert" id="m_form_1_msg">
                                    <div class="m-alert__icon">
                                        <i class="la la-warning"></i>
                                    </div>
                                    <div class="m-alert__text">
                                        Oh snap! Change a few things up and try submitting again.
                                    </div>
                                    <div class="m-alert__close">
                                        <button type="button" class="close" data-close="alert" aria-label="Close"></button>
                                    </div>
                                </div>
                            </div>
                            <?php if (isset($err) && !empty($err)) { ?>
                                <div class="m-form__content">
                                    <div class="m-alert m-alert--icon alert alert-danger" role="alert" id="m_form_1_msg1">
                                        <div class="m-alert__icon">
                                            <i class="la la-warning"></i>
                                        </div>
                                        <div class="m-alert__text">
                                            <?php echo $err; ?>
                                        </div>
                                        <div class="m-alert__close">
                                            <button type="button" class="close" data-close="alert" aria-label="Close"></button>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                            <div class="form-group m-form__group row">
                                <div class="col-lg-4">
                                    <label>
                                        Promo Code:
                                    </label>
                                    <input type="text" class="form-control m-input" placeholder="Enter Promo Code" id="promo_code" name="promo_code" value="<?php echo $auRec[0]->promo_code; ?>">

                                </div>
                                <div class="col-lg-4">
                                    <label class="">

                                        Description: 
                                    </label>
                                    <input type="text" class="form-control m-input" placeholder="Description" id="discription" name="discription" value="<?php echo $auRec[0]->discription; ?>"> 
                                </div>
                                <div class="col-lg-4">
                                    <label class="">
                                        Multiplier: 
                                    </label>
                                    <input type="text" class="form-control m-input" placeholder="" name="promo_code_discount" id="promo_code_discount" onkeypress="return isNumberKey(event)" min="1" max="100" value="<?php echo $auRec[0]->promo_code_discount; ?>"> 
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <div class="col-lg-4">
                                    <label class="">
                                        Number of Uses: 
                                    </label>
                                    <input type="text" class="form-control m-input" placeholder="" id="max_limit_use" name="max_limit_use" value="<?php echo $auRec[0]->max_limit_use; ?>" onkeypress="return isNumberKey(event)"> 
                                </div>

                                <div class="col-lg-4">
                                    <label>
                                        Building:
                                    </label>
                                    <div class="input-group m-input-group m-input-group--square"> 
                                        <select class="form-control m-input m-input--square" id="building_id" name="building_id">
                                            <option value="">Select Building</option>
                                            <?php
                                            $condi = "";


                                            $branchList = $_objAdmin->_getSelectList2('table_buildings', "building_id,building_name", '', " status='A'");

                                            for ($i = 0; $i < count($branchList); $i++) {

                                                if ($branchList[$i]->building_id == $auRec[0]->building_id) {
                                                    $select = "selected";
                                                } else {
                                                    $select = "";
                                                }
                                                ?>
                                                <option value="<?php echo $branchList[$i]->building_id; ?>" <?php echo $select ?>><?php echo $branchList[$i]->building_name; ?></option>

                                            <?php } ?>
                                        </select>
                                    </div> 
                                </div>
                                <div class="col-lg-4">
                                    <label>
                                        Cleaning Type:
                                    </label>
                                    <div class="input-group m-input-group m-input-group--square"> 
                                        <select class="form-control m-input m-input--square" id="cleaning_type_id" name="cleaning_type_id">
                                            <option value="">Select Cleaning Type </option>
                                            <?php
                                            $condi = "";


                                            $branchList = $_objAdmin->_getSelectList2('table_cleaning_type', "cleaning_type_id,cleaning_type", '', " status='A'");

                                            for ($i = 0; $i < count($branchList); $i++) {

                                                if ($branchList[$i]->cleaning_type_id == $auRec[0]->cleaning_type_id) {
                                                    $select = "selected";
                                                } else {
                                                    $select = "";
                                                }
                                                ?>
                                                <option value="<?php echo $branchList[$i]->cleaning_type_id; ?>" <?php echo $select ?>><?php echo $branchList[$i]->cleaning_type; ?></option>

                                            <?php } ?>
                                        </select>
                                    </div> 
                                </div>

                            </div>
                            <div class="form-group m-form__group row">
                                <div class="input-daterange input-group" id="m_datepicker_5">
                                    <div class="col-lg-4">
                                        <label class="">
                                            Start Date: 
                                        </label>
                                        <div class="m-input-icon m-input-icon--right">
                                            <input type="text" class="form-control m-input" id="start_date" name="start_date" style="width:100%"> 
                                        </div> 
                                    </div>
                                    <div class="col-lg-4">
                                        <label class="">
                                            End Date: 
                                        </label>
                                        <div class="m-input-icon m-input-icon--right">
                                            <input type="text" class="form-control m-input" id="end_date" name="end_date" style="width:100%"> 
                                        </div> 
                                    </div>
                                </div>
                            </div>
                            <div class="form-group m-form__group row"> 
                                <div class="col-lg-4">
                                    <label class="">
                                        Status:
                                    </label>
                                    <div class="m-radio-inline">
                                        <label class="m-radio m-radio--solid">
                                            <input <?php if ($auRec[0]->status == 'A') echo "checked"; ?> type="radio" name="status"  value="A" >
                                            Active
                                            <span></span>
                                        </label>
                                        <label class="m-radio m-radio--solid">
                                            <input type="radio" name="status" value="I" <?php if ($auRec[0]->status == 'I') echo "checked"; ?>>
                                            Inactive
                                            <span></span>
                                        </label>
                                    </div> 
                                </div>
                            </div>
                        </div>
                        <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                            <div class="m-form__actions m-form__actions--solid">
                                <div class="row">
                                    <div class="col-lg-4"></div>
                                    <div class="col-lg-8">
                                        <button type="submit" class="btn btn-success">
                                            Submit
                                        </button>
                                        <button type="reset" class="btn btn-secondary">
                                            Reset
                                        </button>
                                        <a href="promoCode.php" class="btn btn-secondary">
                                                back
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <input name="promo_code_id" type="hidden" value="<?php echo $auRec[0]->promo_code_id; ?>" />
                        <input name="account_id" type="hidden" value="<?php echo $_SESSION['accountId']; ?>" />
                        <input type="hidden" name="add" value="yes">

                    </form>
                    <!--end::Form-->
                </div>
                <!--end::Portlet-->
            </div>
        </div>
    </div>
</div>
</div>
</div>
<!-- start footer -->         
<?php include("footer.php") ?>

<script type="text/javascript">
    var FormControls = function () {
        //== Private functions

        var demo1 = function () {
            $("#m_form_1").validate({
                // define validation rules
                rules: {
                    promo_code: {
                        required: true
                    },
                    promo_code_discount: {
                        required: true,
                        digits: true
                    },
                    max_limit_use: {
                        required: true,
                        digits: true
                    },
                    start_date: {
                        required: true
                    },
                    end_date: {
                        required: true,
                    },
                    status: {
                        required: true,
                    }
                },
                //display error alert on form submit  
                invalidHandler: function (event, validator) {
                    var alert = $('#m_form_1_msg');
                    alert.removeClass('m--hide').show();
                    mApp.scrollTo(alert, -200);
                },
                submitHandler: function (form) {
                    form.submit();
                }
            });
        }

        return {
            // public functions
            init: function () {
                demo1();
            }
        };
    }();

    jQuery(document).ready(function () {
        
        var id = '<?php echo $_REQUEST['id']; ?>';
        FormControls.init();

        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1; //January is 0!

        var yyyy = today.getFullYear();
        if (dd < 10) {
            dd = '0' + dd;
        }
        if (mm < 10) {
            mm = '0' + mm;
        }
        var today = mm + '/' + dd + '/' + yyyy;
        console.log(today);
        $('#m_datepicker_5').datepicker({
            todayHighlight: true,
            templates: {
                leftArrow: '<i class="la la-angle-left"></i>',
                rightArrow: '<i class="la la-angle-right"></i>'
            }
        });
    });

</script>
<script type="text/javascript">
  function isNumberKey(evt){

    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
      return false;
    return true;
  }
</script>

