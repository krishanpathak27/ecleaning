<?php
include("includes/config.inc.php");
include("includes/function.php");
$_objAdmin = new Admin();
$_objItem = new Item();

if(isset($_POST['add']) && $_POST['add'] == 'yes')
{
	if($_POST['id']!="") 
	{
		$condi=	" start_date<='".$_POST['from']."' and end_date>='".$_POST['from']."' and start_date>='".$_POST['to']."' and end_date<='".$_POST['to']."' and item_id ='".$_POST['item_id']."' and ptr_id<>'".$_POST['id']."'";
	}
	else
	{
		$condi=	" start_date<='".$_POST['from']."' and end_date>='".$_POST['from']."' and start_date<='".$_POST['to']."' and end_date<='".$_POST['to']."' and item_id ='".$_POST['item_id']."'";
		//$condi=	" (start_date BETWEEN '".$_POST['to']."' AND '".$_POST['from']."') and (end_date BETWEEN '".$_POST['to']."' AND '".$_POST['from']."') and item_id ='".$_POST['item_id']."'";
	}
	$auRec=$_objAdmin->_getSelectList('table_ptr',"*",'',$condi);
	if(is_array($auRec))
	{
		$err="PTR Price Date already exists in the system.";	
		$auRec[0]=(object)$_POST;						
	}
	else
	{
		if($_POST['id']!="") 
		{
			$cid=$_objAdmin->updatePTRPrice($_POST['id']);
			$sus="Item PTR price has been updated successfully.";
		}
		else 
		{
			if(isset($_FILES['fileToUpload']['tmp_name']) && $_FILES['fileToUpload']['tmp_name']!=""){			 
				$ret = $_objItem->uploadItemPRTFile();
					if($ret=='') {																	
					$sus= "Item has been added successfully.";								
					}elseif($ret=='no') {
					$error= "Empty file";
					}else {
					$error= "Following record has not been inserted.<br>".$ret;														
					}
					if($sus!=''){
					$sus=$sus;
					}else{
					$err=$error;
					}		
				}
			
			//$sus="Item price has been added successfully.";
		}
	}
}


if(isset($_REQUEST['id']) && $_REQUEST['id']!="")
{
	$auRec=$_objAdmin->_getSelectList('table_ptr as p left join table_item as i on p.item_id = i.item_id left join table_category as c on i.category_id = c.category_id ',"p.*, i.item_name, i.item_code, c.category_name",''," p.ptr_id =".$_REQUEST['id']);
	if(count($auRec)<=0) header("Location: item_ptr.php");
}
else
{
	$auRec=$_objAdmin->_getSelectList('table_item as i left join table_category as c on i.category_id = c.category_id ',"i.*, c.category_name",''," i.item_id =".$_SESSION['item_price_id']);
	if(count($auRec)<=0) header("Location: item_ptr.php");
}

?>

<?php include("header.inc.php") ?>

<script src="javascripts/jquery-1.8.2.js" type="text/javascript"></script>
<script src="javascripts/jquery-ui.js"></script>
<script type="text/javascript" src="javascripts/validate.js"></script>
<script>
    $(function() {
        $( "#from" ).datepicker({
			dateFormat: "yy-mm-dd",
            defaultDate: "+1w",
            changeMonth: true,
            numberOfMonths: 3,
            onSelect: function( selectedDate ) {
                $( "#to" ).datepicker( "option", "minDate", selectedDate );
            }
        });
        $( "#to" ).datepicker({
			dateFormat: "yy-mm-dd",
            defaultDate: "+1w",
            changeMonth: true,
            numberOfMonths: 3,
            onSelect: function( selectedDate ) {
                $( "#from" ).datepicker( "option", "maxDate", selectedDate );
            }
        });
    });
</script>

<div class="clear"></div>
<!-- start content-outer -->
<div id="content-outer">
<!-- start content -->
	<div id="content">
	<div id="page-heading"><h1><span style="color: #d74343; font-family: Tahoma; font-weight: bold;">Item PTR Price</span></h1></div>
		<table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">
		<tr>
			<th rowspan="3" class="sized"><img src="images/side_shadowleft.jpg" width="20" height="300" alt="" /></th>
			<th class="topleft"></th>
			<td id="tbl-border-top">&nbsp;</td>
			<th class="topright"></th>
			<th rowspan="3" class="sized"><img src="images/side_shadowright.jpg" width="20" height="300" alt="" /></th>
		</tr>
		<tr>
			<!--<td id="tbl-border-left"></td>-->
			<td>
			<!--  start content-table-inner -->
			<div id="content-table-inner">
				<table border="0" width="100%" cellpadding="0" cellspacing="0">
				<tr valign="top">
					<td>
					<!--  start message-red -->
					<?php if($err!=''){?>
					<div id="message-red">
						<table border="0" width="100%" cellpadding="0" cellspacing="0">
						<tr>
							<td class="red-left"> <?php echo $err; ?></td>
							<td class="red-right"><a class="close-red"><img src="images/icon_close_red.gif"   alt="" /></a></td>
						</tr>
						</table>
					</div>
					<?php } ?>
					<!--  end message-red -->
					<?php if($sus!=''){?>
					<!--  start message-green -->
					<div id="message-green">
						<table border="0" width="100%" cellpadding="0" cellspacing="0">
						<tr>
							<td class="green-left"><?php echo $sus; ?></td>
							<td class="green-right"><a class="close-green"><img src="images/icon_close_green.gif"   alt="" /></a></td>
						</tr>
						</table>
					</div>
					<?php } ?>
					<!--  end message-green -->
					
			<!-- start id-form -->
		<form name="frmPre" id="frmPre" method="post" action="add_ptr.php" enctype="multipart/form-data" >
			<table border="0" cellpadding="0" cellspacing="0"  id="id-form">
			<?php if($_REQUEST['id']!=''){ ?>
			<tr>
				<th valign="top">Category:</th>
				<td><input type="text" class="text" value="<?php echo $auRec[0]->category_name; ?>" readonly />	</td>
				<td></td>
			</tr>
			<tr>
				<th valign="top">Item Name:</th>
				<td><input type="text" class="text" name="item_name" id="item_name" value="<?php echo addslashes($auRec[0]->item_name); ?>" readonly />
				<input type="hidden" class="text" name="item_id" id="item_id" value="<?php echo $_SESSION['item_price_id']; ?>" /></td>
				<td></td>
			</tr>
			<tr>
				<th valign="top">Item Code:</th>
				<td><input type="text" name="item_code" id="item_code" class="text" value="<?php echo $auRec[0]->item_code; ?>" readonly /></td>
				<td></td>
			</tr>
			<tr>
				<th valign="top">Item PTR:</th>
				<td><input type="text" name="item_price" id="item_price" class="required" value="<?php echo $auRec[0]->item_price; ?>" /></td>
				<td></td>
			</tr>
			<tr>
				<th valign="top">Start Date:</th>
				<td><input type="text" name="from" id="from" class="date required" value="<?php echo $auRec[0]->start_date; ?>" readonly /></td>
				<td></td>
			</tr>
			<tr>
				<th valign="top">End Date:</th>
				<td><input type="text" id="to" name="to" class="date required" value="<?php echo $auRec[0]->end_date; ?>" readonly /></td>
				<td></td>
			</tr>
			<tr>
				<th valign="top">Status:</th>
				<td>
				<select name="status" id="status" class="styledselect_form_3">
				<option value="A" <?php if($auRec[0]->status=='A') echo "selected";?> >Active</option>
				<option value="I" <?php if($auRec[0]->status=='I') echo "selected";?> >Inactive</option>
				</td>
				<td></td>
			</tr>
			<?php } else { ?>
			<tr>
				<th valign="top">Item PTR <small>(Import)</small> :</th>
				<td><input type="file" name="fileToUpload" id="file_1" class="text" ></td>
				<td><input name="submit" class="form-submit" type="button" id="submit" value="Downlaod Item PTR CSV Sheet" onclick="location.href='import.inc.php?item_prt_csv=yes';"/></td>
			</tr>
			<?php } ?>
			<tr>
				<th>&nbsp;</th>
				<td valign="top">
					<input name="account_id" type="hidden" value="<?php echo $_SESSION['accountId']; ?>" />
					<input name="id" type="hidden" value="<?php echo $auRec[0]->prt_id; ?>" />
					<input name="add" type="hidden" value="yes" />
					<input type="reset" value="Reset!" class="form-reset">
					<input type="button" value="Back" class="form-reset" onclick="location.href='item_ptr.php';" />
					<input name="submit" class="form-submit" type="submit" id="submit" value="Save" />
				</td>
			</tr>
			</table>
		</form>
			<!-- end id-form  -->
			</td>
			<td>
			<!-- right bar-->
			<?php include("rightbar/item_bar.php") ?>
			<div class="clear"></div>
		</div>
<!-- end related-act-bottom -->
</div>
<!-- end related-activities -->
</td>
</tr>
<tr>
<td><img src="images/blank.gif" width="695" height="1" alt="blank" /></td>
<td></td>
</tr>
</table>
<div class="clear"></div>
</div>
<!--  end content-table-inner  -->
</td>
<td id="tbl-border-right"></td>
</tr>
<!--<tr>
	<th class="sized bottomleft"></th>
	<td id="tbl-border-bottom">&nbsp;</td>
	<th class="sized bottomright"></th>
</tr>-->
</table>
<div class="clear">&nbsp;</div>

</div>
<!--  end content -->
<div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->
<div class="clear">&nbsp;</div>
    
<!-- start footer --> 
<?php include("footer.php") ?>  
<!-- end footer -->
</body>
</html>