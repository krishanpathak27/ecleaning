<?php 
include("includes/config.inc.php");
include("includes/globalarraylist.php");	

if($_SESSION['userLoginType'] != 5 ) { $salesman = ""; }


$currentYear = date('Y');
$currentMonth = date('m');

$currentYearMonthConcat = $currentYear."-".$currentMonth;
$currentMonthYearString = strtotime(date($currentYearMonthConcat));


/*------------------------------------------------
  Description: Edit Route assignmnet details.
  By : Nizam
-------------------------------------------------*/
if(isset($_GET['id']) && $_GET['id']!="")
{     
      $route_schedule_id=$_GET['id'];
      $condi= " route_schedule_id='".mysql_escape_string($route_schedule_id)."' ";
      $auRec=$_objAdmin->_getSelectList('table_route_scheduled',"*",'',$condi);

   $requestedMonth = $auRec[0]->month;
   $requestedYear = $auRec[0]->year;
   $requestedYearMonthConcat = $requestedYear."-".$requestedMonth;
   $requestedMonthYearString = strtotime(date($requestedYearMonthConcat));



}

if(isset($_POST['save']) && $_POST['save']=='Save') {


  // echo "<pre>";
  // print_r($_POST);
         
  if(isset($_POST['month']) && $_POST['month'] > 0  && isset($_POST['year']) && $_POST['year'] > 0 && isset($_GET['id'])) {

   $requestedMonth = $_POST['month'];
   $requestedYear = $_POST['year'];
   $requestedYearMonthConcat = $requestedYear."-".$requestedMonth;
   $requestedMonthYearString = strtotime(date($requestedYearMonthConcat));
 
    if($requestedMonthYearString >= $currentMonthYearString) {


    $route_schedule_id=$_GET['id'];
    $routeName=$_POST['town'];       
    $wrongRouteName=array(); // Array of wrong route name input by user



		
	/****************************** Get Valid Route Name Only (Gaurav 25 Sep 2015) ******************/
		
		//$getRouteName=$_objAdmin->_getSelectList('table_route',"route_name",'',"status='A'");
      $getRouteName=$_objAdmin->_getSelectList('table_route AS R LEFT JOIN table_user_relationships AS s ON s.route_id = R.route_id ',"route_name",''," R.status='A' $salesman ");
		$getRouteList=array();
		foreach($getRouteName as $value)
		{
			$getRouteList[]=$value->route_name;
		}
		
		$result=array_intersect($routeName, $getRouteList);   
		               
	/****************************** Get Valid Route Name Only (Gaurav 25 Sep 2015)******************/
	

  /****************************************************************
  * DESC: Delete route assigment in these cases
          CASE 1: requested month/year > current month/year then delete all the records of this month
          CASE 2: requested month/year < current month/year Skip & exit
          CASE 3: requested month/year = current month/year then delete only those records assign day >= current day of this month/year
  * Author : AJAY
  * Updated On: 2016-01-13
  *
  *
  **/

      // echo "yes its";
      // echo sizeof($result);
      // exit;
      

     if(sizeof($result)>0){

      if($requestedMonthYearString == $currentMonthYearString) { 

      $assign_days = " AND assign_day>='".date(j)."' ";
      // echo "days";
      // exit;
      $_objAdmin->mysql_query("delete from table_route_schedule_details where route_schedule_id='".$route_schedule_id."' $assign_days");

      } else if($requestedMonthYearString > $currentMonthYearString){
      // echo "month";
      // exit;
     $_objAdmin->mysql_query("delete from table_route_schedule_details where route_schedule_id='".$route_schedule_id."'"); 

      }

     }

              
      $days[]=array(); // array for assigned days



         
 /********************** Check and update the route assignement (Gaurav 25 Sep 2015) ***************************/
		
	  foreach($routeName as $key=>$value) // Changed, Use key insted of for loop
	  {
		 $days=$_POST['daynumber'.$key]; // Changed, Use key insted of for loop
		if(in_array($value,$result)) // Check weather route exist in the system or not
			{
				$condition=" route_name='".mysql_escape_string($value)."' ";
				$townData=$_objAdmin->_getSelectList('table_route',"*",'',$condition);
				$route_ids=$townData[0]->route_id;        
			
				for($i=0;$i<=sizeof($days)-1;$i++) 
					{					
					   $route_data['route_schedule_id']  =$route_schedule_id;
					   $route_data['route_id']           =$route_ids;
					   $route_data['assign_day']         =$days[$i];
					   $route_data['status']             ='A';
					   $_objAdmin->_dbInsert($route_data, 'table_route_schedule_details');                
					}              
		  }
		  else 
		  {  
				$wrongRouteName[]=$value;		// Array of wrong Route name			  
		  }
	  }
/********************** Check and update the route assignement (Gaurav 25 Sep 2015) ***************************/

		if($route_ids!="")
		{ 
			
			if(sizeof($wrongRouteName)>0)
			{
				$wrongRouteList=implode(', ',$wrongRouteName);
				$WrongRouteMsg=" Invalid route name: ".$wrongRouteList; // List Of Wrong Route Name
			}
			header('Location:add_route_schedule_edit.php?id='.$route_schedule_id);
			$_SESSION['msg']= "Route has been assigned successfully.".$WrongRouteMsg;
		 
		 }


    } else {

      header('Location:add_route_schedule_edit.php?id='.$route_schedule_id);
      $_SESSION['msg']= "Sorry you can't delete past route assignments";

    } // check if month+year > current month+year


  } else {

      header('Location:add_route_schedule_edit.php?id='.$route_schedule_id);
      $_SESSION['msg']= "Invalid month/year!";

  }  // check valid paramanters


} // End of post request







/********************** add export for route assignement (Gaurav 25 Sep 2015) ***************************/
	
if(isset($_POST['export']) && $_POST['export']!="")
{
header("Location:export.inc.php?export_salesman_planned_route&id=".$_GET['id']);
exit;
}	
?>

<?php include("header.inc.php") ?>
<script src="javascripts/jquery.min.js"></script>
<script src="javascripts/jquery-ui-1.10.3.custom.js" ></script>
<script type="text/javascript">
$(document).ready(function() { 
$('#Spec_all').on('click', function() {
  $('.checknumber:enabled').prop('checked', this.checked);
});
});


$(document).ready(function() {
	
	$('input[name=checkall]').ready(function() {
		
	});
	
	
});




function getchecked(checkedStatus, id)
{
	//alert(checkedStatus);
	//alert(id);
	
	var getClass="check"+id;
	
	var checkClass=getClass+":enabled";
	//alert(checkClass);
	
	 var getLength = $('input[name="daynumber'+id+'[]"]:checked').length;
	
	if (checkedStatus==true) {
	$('.'+checkClass).prop('checked',true);
}
	else {
		$('.'+checkClass).prop('checked', false); 
	}
	
}

</script>

    <link rel="stylesheet" href="css/bootstrap.css"/>
    <link rel="stylesheet" href="css/bower_components/Font-Awesome/css/font-awesome.css"/>
    <link rel="stylesheet" href="css/build.css"/>

    <style type="text/css">
    label {
      color: #000;
    }
    </style>

<!-- start content-outer -->
<!-- <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script> -->
<div id="content-outer">
<!-- start content -->
<div id="content">

<?php if(isset($_SESSION['msg']) && $_SESSION['msg']!=""){  ?>
  <div id="message-red">
  <table border="0" width="100%" cellpadding="0" cellspacing="0">
  <tr>
    <td class="red-left"><?php  echo $_SESSION['msg']; unset($_SESSION['msg']);?></td>
    <td class="red-right"><a class="close-red"><img src="images/icon_close_red.gif"   alt="" /></a></td>
  </tr>
  </table>
  </div>
  <?php } ?>


<table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">

<tr>
	<!--<td id="tbl-border-left"></td>-->
	<td>
	<!--  start content-table-inner -->
	<div id="content-table-inner">
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
	<tr valign="top">
	<td>

  
  <!--  end step-holder -->
  <form name="frmPre" id="frmPre" method="post" action="" enctype="multipart/form-data" >
    <table border="0" cellpadding="0" cellspacing="0"  id="id-form" width="100%">
    <tr>
      <!--<td><h3>State:</h3></td>-->
      <td><h3>Salesman:</h3></td>
       <td><h3>Year:</h3></td>
       <td><h3>Month:</h3></td>
    </tr>
  <tr>
   

    <td>
      <input type="hidden" name="salesman_id" value="<?php echo $auRec[0]->salesman_id;?>">
      <select name="salesman_id" id="salesman_id" required class="styledselect_form_5 required" disabled>
          <?php echo $rtndata = $_objArrayList->GetSalesmenMenu($salsList, $auRec[0]->salesman_id, 'root');?>
      </select>
    </td>
    
    <td>
       <input type="hidden" name="year" value="<?php echo $auRec[0]->year;?>">
        <select name="year" id="year" class="required styledselect_form_5 required" disabled>
         <?php if(isset($auRec[0]->year) & !empty($auRec[0]->year)) { $selectedYear = $auRec[0]->year; } else { $selectedYear = date('Y'); }
         echo $_objArrayList->getYearListRoute(2010, date('Y', strtotime('+1 years')), $selectedYear, 'default');?>
        </select>
    </td> 
     
    <td>
       <input type="hidden" name="month" value="<?php echo $auRec[0]->month;?>" >
        <select name="month" id="month" class="styledselect_form_3 required" disabled  onchange="route_load(this.value,'days','<?php echo $auRec[0]->year; ?>','<?php echo $auRec[0]->salesman_id; ?>','<?php echo $auRec[0]->route_schedule_id; ?>'),days_values(this.value,'days2','<?php echo $_POST['month']; ?>');">
          <?php echo $_objArrayList->Months($auRec[0]->month); ?>
        </select>
    </td>
  </tr>

<table  border="0" cellspacing="5" cellpadding="5">

   <tr>
    <td colspan="2">
		<table  border="0" cellspacing="5" cellpadding="5" id="items" style="display:none;" style="padding:20px">
          <div>
                <span>Check All</span>
                <input id="Spec_all" type="checkbox" />
          </div>
           <td align="lift" valign="top">
              <table id="items" style="display:none;">
                <div id="days" ></div>
                <div id="days2"></div>
         </table>
			  <?php //echo $requestedMonthYearString."=".$currentMonthYearString; 
         if($requestedMonthYearString >= $currentMonthYearString) {?> <input type="button" id="addmore" class="form-submit" value="Add More Route" style="display:none;">   <?php }?>
          </td>
        </tr>
      </table>
    </td>
  </tr>
    <th>&nbsp;</th>
   <tr>
   <td>
      <?php if($requestedMonthYearString >= $currentMonthYearString) {?>  <input type="button" id="addmore" value="Add More" class="form-submit" style="display:none;">  <?php }?>
   </td>
   </tr>

      <tr>
      <td valign="top" colspan="4">
      <input name="route_add" type="hidden" value="yes" />
      <input name="com_id" type="hidden" value="<?php echo $com_id; ?>" />
      <!--<input type="reset" value="Reset!" class="form-reset">-->
      <input type="button" value="Back" class="form-reset" onclick="location.href='route_schedule.php';" />
       <input name="export" class="result-submit" type="submit" id="export" value="Export to Excel" onclick="return exportexcel();"  >
      <?php if($requestedMonthYearString >= $currentMonthYearString) {?> <input name="save" class="form-submit" type="submit" id="save" value="Save"/> <?php }?>
     </td>
    </tr>
    </table>
  </form>


<script type="text/javascript">
 $(document).ready(function(){ 
 
    $("#year").on('change', function(){ 
     route_load($("#month").val(),'days','<?php echo $auRec[0]->year; ?>','<?php echo $auRec[0]->salesman_id; ?>','<?php echo $auRec[0]->route_schedule_id; ?>');
     days_values($("#month").val(),'days2','<?php echo $_POST['month']; ?>');
   });
});


$("#addmore").on('click', function () {
   
    //var month     =  $('#monthday').val();

   
    var d         =  new Date();
    var day       = d.getDate();
    var curmonth  =  d.getMonth()+1;
    var curyear  =  d.getFullYear();
    var selectedmonth  =$('#month').val();
    var selectedYear   = $('#year').val();
    

    function daysInMonth(selectedmonth,selectedYear) {
    return new Date(selectedYear, selectedmonth, 0).getDate(); 
    }
    var month =daysInMonth(selectedmonth,selectedYear); //31
	//alert(month);
    //alert(curmonth); 
    //alert(month);
    //alert(day);
    var rowCount = $('#days tr').length+1;
    var inputId="input"+rowCount;
    var checkBoxId="checkBox"+rowCount;
     //alert(rowCount);
    var container='<tr id="'+inputId+'" class="addedRuntime"><td valign="top" style="padding:5px 0px 5px 0px"><div><span>Check All</span><input id="'+rowCount+'" type="checkbox"  onclick="getchecked(this.checked, this.id)" /></div><input class="input3 item ui-autocomplete-input" name="town['+rowCount+']" type="text" value="" autocomplete="off" required><span role="status" aria-live="polite" class="ui-helper-hidden-accessible"></span></td>';
    var a1='<td><div id="days" style="border: 1px solid #ededed; border-radius: 10px; height: 60px; padding: 10px 5px 5px 10px;">';

    var a2='';
    for (var idx = 1; idx <= month; idx++) 
    {
      /*if(selectedmonth<curmonth){  var disabled='disabled';}	
      else if(selectedmonth>curmonth){ var disabled='';} 
      else{
      if(idx<day && curmonth==selectedmonth) {  var disabled='disabled'; } else{ var disabled=''; }
       }*/

       if(selectedYear > curyear){
              var disabled=''; 
          }else{
            if(selectedmonth < curmonth){
               var disabled='disabled';
             }
             else if(selectedmonth > curmonth){
               var disabled=''; 
             }
             else{
               if(idx<day && curmonth==selectedmonth) {
                var disabled='disabled'; 
               } else{ 
                var disabled=''; 
                }
             }
          }


       var runTimeStyle = "";
       if(idx == 1) {
        var runTimeStyle = 'style="margin-top:-5px"';
       }

       a2 =a2+'<div id="'+checkBoxId+'" class="checkbox checkbox-primary" '+runTimeStyle+'><input type="checkbox" class=" check'+rowCount+' checknumber styled" name="daynumber' + rowCount + '[]" value="' + idx + '"'+disabled+'><label>' + idx + '</label></div>';
        //if(cnt = cnt + 1)    
    }
	
    var a3='<input type="button" class="form-submit" name="delete" onclick="deleteRow('+rowCount+')" value="delete"/></div></td></tr>';
    //alert(finaldata);
    var row = document.createElement("TR");
    //var someText = finaldata;
    // var newDiv = $("<tr>").append(container,a1,a2,a3);

    var newDiv = container+a1+a2+a3;
		    


     $('#days').append(newDiv);
       $(".item" ).autocomplete({
      source: availableTags,
      minLength: 1,
    });
  }
);

function deleteRow(idCount){
	//alert(idCount);
			$("#input"+idCount).remove();
			$("#checkBoxId"+idCount).remove();
	}
// get autocomplete tags
    var state_id = $('#state').val();
    //alert(state_id);
    //alert(state_id);
    var availableTags = [<?php 
    //$auCol=$_objAdmin->_getSelectList2('table_route',"route_name",''," status='A' ORDER by route_name");
            $auCol=$_objAdmin->_getSelectList('table_route AS R LEFT JOIN table_user_relationships AS s ON s.route_id = R.route_id ',"route_name",''," R.status='A' $salesman ");
            for($i=0;$i<count($auCol);$i++){ 
                if(count($auCol)-1!=$i){ 
                  echo '"'.trim($auCol[$i]->route_name).'",'; 
                } else { echo '"'.trim($auCol[$i]->route_name).'"'; }
            }
            ?>];

    $(".item").autocomplete({
      source: availableTags,
      minLength: 1,
    });

</script>

  <style type="text/css'">
  .routestyle {
    padding:5px 0px 5px 0px;
  }
  label {
  color: #000000;
  font-family: Arial, Helvetica, sans-serif;
  font-size: 12px;
  font-weight: normal;
  float: left;
  }
  .error{

    float: left;
  }
  </style>



  </td>
	<td>

	<!-- right bar-->
	<?php //include("rightbar/message_bar.php") ?>
</td>
</tr>
<tr>
<td><img src="images/blank.gif" width="695" height="1" alt="blank" /></td>
<td></td>
</tr>
</table>
<div class="clear"></div>
</div>
<!--  end content-table-inner  -->
</td>
<td id="tbl-border-right"></td>
</tr>
<!--<tr>
	<th class="sized bottomleft"></th>
	<td id="tbl-border-bottom">&nbsp;</td>
	<th class="sized bottomright"></th>
</tr>-->
</table>
<div class="clear">&nbsp;</div>
</div>
<!--  end content -->
<div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->
<div class="clear">&nbsp;</div> 
<!-- start footer -->         
<?php include("footer.php") ?>
<!-- end footer -->
 
</body>
</html>

