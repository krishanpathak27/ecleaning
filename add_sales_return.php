<?php

include("includes/config.inc.php");
include("includes/function.php");
		if($_SESSION['userLoginType']==1)
		{
			header("Location: sales_return.php");
		}
		$_objAdmin = new Admin();
		$objArrayList= new ArrayList();
		if(!empty($_REQUEST['id']))
		{
			$condi = " sales_order_id='".$_REQUEST['id']."'";
			$auRec=$_objAdmin->_getSelectList('table_sales_return',"*",'',$condi);
			
		}


?>

<?php include("header.inc.php");
include("company.inc.php");
// $pageAccess=1;
// $check=$objArrayList->checkAccess($pageAccess, basename($_SERVER['PHP_SELF']));
// //echo $check;
//  if($check == false){
//  header('Location: ' . $_SERVER['HTTP_REFERER']);
//  }

 ?>
<script src="javascripts/jquery-1.8.2.js" type="text/javascript"></script>
<script src="javascripts/jquery-ui.js"></script>
<script type="text/javascript" src="javascripts/validate.js"></script> 

 <style>
.sortable		{ padding:0; }
.sortable li	{ padding:4px 8px; color:#000; cursor:move; list-style:none; width:350px; background:#ddd; margin:10px 0; border:1px solid #999; }
#message-box		{ background:#fffea1; border:2px solid #fc0; padding:4px 8px; margin:0 0 14px 0; width:350px; }
</style>
<div class="clear"></div>
<!-- start content-outer -->
<div id="content-outer">
<!-- start content -->
	<div id="content">
	<div id="page-heading"><h1><span style="color: #d74343; font-family: Tahoma; font-weight: bold;"><?php if(isset($_GET['id'])){?>   <?php } else {?>  <?php }?> </span></h1></div>
		<table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">
		<tr>
			<td id="tbl-border-left"></td>
			<td>
			<!--  start content-table-inner -->
			<div id="content-table-inner">
				<table border="0" width="100%" cellpadding="0" cellspacing="0" style="margin-left:10px;">
				<tr valign="top">
					<td>
					<!--  start message-red -->
					<?php if($err!=''){?>
					<div id="message-red">
						<table border="0" width="100%" cellpadding="0" cellspacing="0">
						<tr>
							<td class="red-left"> <?php echo $err; ?></td>
							<td class="red-right"><a class="close-red"><img src="images/icon_close_red.gif"   alt="" /></a></td>
						</tr>
						</table>
					</div>
					<?php }?>
					<!--  end message-red -->
					<?php if($sus!=''){?>
					<!--  start message-green -->
					<div id="message-green">
						<table border="0" width="100%" cellpadding="0" cellspacing="0">
						<tr>
							<td class="green-left"><?php echo $sus; ?></td>
							<td class="green-right"><a class="close-green"><img src="images/icon_close_green.gif"   alt="" /></a></td>
						</tr>
						</table>
					</div>
					<?php } ?>
					<!--  end message-green -->
					<div id="step-holder">
<div class="step-no">1</div>
<div class="step-dark-left">Sales Return Form</div>
<div class="step-dark-right">&nbsp;</div>

<div class="clear"></div>
</div>
			<!-- start id-form -->
	<form name="frmPre" id="frmPre" method="post" action="<?php $_SERVER['PHP_SELF'];?>" enctype="multipart/form-data" >
			
		<table border="0" cellpadding="0" cellspacing="0"  id="id-form">

			<tr>
				<th valign="top"> State:</th>
					<td>
						<select name="stateID" id="stateid" class="required styledselect_form_3 country_id">
									<option value="all">All</option>
									<?php $state = $_objAdmin->_getSelectList2('table_warehouse as w left join state as s on s.state_id=w.state_id','w.state_id,s.state_name',''," group BY w.state_id"); 


									if(is_array($state)){

										foreach($state as $value):?>
										<option value="<?php echo $value->state_id;?>" <?php if($value->state_id==$_SESSION['stateID']) echo "selected";?> ><?php echo $value->state_name;?></option>
									<?php endforeach; }?>
						</select>
					</td>
				<td></td>
			</tr>
		

	        <tr>
				<th valign="top"> Warehouse:</th>
					<td>
						<select name="warehouse_id" id="warehouse_id" class="required styledselect_form_3 region_id">
									<option value="all">All</option>
									
									<?php $wh_name = $_objAdmin->_getSelectList2('table_warehouse','warehouse_name,warehouse_id',''," state_id='".$_SESSION['stateID']."' ORDER BY state_id"); 
									if(is_array($wh_name)){
										foreach($wh_name as $value):?>
										<option value="<?php echo $value->warehouse_id;?>" <?php if($value->warehouse_id==$_SESSION['warehId']) echo "selected";?> ><?php echo $value->warehouse_name;?></option>
									<?php endforeach; }?>

									
						</select>
					</td>
				<td></td>
			</tr>

	        <tr>
				<th valign="top">BSN:</th>
				<td class="input_fields_wrap">
					<input type="text" name="barcode[]" id="barcode" value="" class="required" >  
	                  <button class="add_field_button" style="border: 0;background: transparent;" >&nbsp;&nbsp;
	                  <img src="images/add.png"/></button>
				
				</td>
				<td></td>
			</tr>



       <!--   <tr>
			<th valign="top">Select State:</th>
			<td>
			<select name="state" id="stateid" class="required styledselect_form_3 country_id">
				<option value="">Please Select</option>
				<?php $state = $_objAdmin->_getSelectList2('table_warehouse as w left join state as s on s.state_id=w.state_id','w.state_id,s.state_name',''," group BY w.state_id"); 
			
                     
					if(is_array($state)){

					 foreach($state as $value):?>
					 	<option value="<?php echo $value->state_id;?>" <?php if($value->state_id==$auRec[0]->state_id) echo "selected";?> ><?php echo $value->state_name;?></option>
				<?php endforeach; }?>
			</select>
			</td>
			<td></td>
		</tr>  -->

		<!--  <tr>
			<th valign="top">Warehouse Location:</th>
			<td>
			<select name="warehouse_id" id="warehouse_id" class="required styledselect_form_3 region_id">
				<option value="">Please Select</option>
				<?php if(!empty($auRec[0]->warehouse_id) && $auRec[0]->warehouse_id > 0){?>
				<?php $wh_name = $_objAdmin->_getSelectList2('table_warehouse','warehouse_name,warehouse_id',''," state_id='".$auRec[0]->state."' ORDER BY state_id"); 
					if(is_array($wh_name)){
					 foreach($wh_name as $value):?>
					 	<option value="<?php echo $value->warehouse_id;?>" <?php if($value->region_id==$auRec[0]->warehouse_id) echo "selected";?> ><?php echo $value->warehouse_name;?></option>
				<?php endforeach; }?>

				<?php }?>	
			</select>
			</td>
			<td></td>
		</tr>  -->


			<tr>
				<th valign="top">Comment:</th>
				<td>
				<textarea id="comments" minlength="8" cols="25" rows="5" name="comments" class="required" value="<?php echo $auRec[0]->comments; ?> "></textarea></td>
				<td>
				</td>
			</tr>
		
		
			<tr>
				<th>&nbsp;</th>
				<td valign="top">
					<input name="sales_return" type="hidden" value="sales_return" />
					<input name="complaint_id" type="hidden" value="<?php echo $_REQUEST['id']; ?>" />
					<input name="account_id" type="hidden" value="<?php echo $_SESSION['accountId']; ?>" />

					<input type="hidden" name="distributor_id" value="<?php echo $_SESSION['distributorId']; ?>" />
					<input type="button" value="Back" class="form-reset" onclick="location.href='sales_return.php';" />
					<input type="reset" value="Reset!" class="form-reset">

					<input name="save" class="form-submit" type="submit" id="save" value="Save" />

				</td>
			</tr>
		
		</tr>
		</table>
	</form>
			<!-- end id-form  -->
			</td>
			
			<td><?php
			?>
				
			<!--</div>-->
				
				
			</td>
			</tr>
			</table>
			<!-- right bar-->
			
			<div class="clear"></div>
			</div>
			</td>
		</tr>
	</table>
<div class="clear"></div>
</div>
<!--  end content-table-inner  -->
<div class="clear">&nbsp;</div>

</div>
<!--  end content-outer -->
<div class="clear">&nbsp;</div>
    
<!-- start footer --> 
<?php include("footer.php") ?>  
<!-- end footer -->
</body>
</html>
<script type="text/javascript" src="javascripts/select_dropdowns.js"></script>

<script>


$(function() {

    $( "#datepicker_id" ).datepicker({dateFormat: 'd M yy',changeMonth: true, changeYear: true, yearRange: '1930:2050'});
	
  });
</script>


<script>
 $(document).ready(function() {
    var max_fields      = 10; //maximum input boxes allowed
    var wrapper         = $(".input_fields_wrap"); //Fields wrapper
    var add_button      = $(".add_field_button"); //Add button ID
   
    var x = 1; //initlal text box count
    $(add_button).click(function(e){ //on add input button click
        e.preventDefault();
        if(x < max_fields){ //max input box allowed
            x++; //text box increment
            $(wrapper).append('<div><input type="text" name="barcode[]" class="" style="height:20px; width:187px;" /><a href="#" class="remove_field ">&nbsp;&nbsp;<img src="images/delete.png"/></a></div>'); //add input box
        }
    });
   
    $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
        e.preventDefault(); $(this).parent('div').remove(); x--;
    })
});

 </script>