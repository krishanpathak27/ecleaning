<?php  $stateRec=$_objAdmin->_getSelectList2('state',"state_name,state_id",'','status="A"'); //echo "<pre>"; print_r($auRec);?>

<!--  start step-holder -->
<div id="step-holder">
<div class="step-no">1</div>
<div class="step-dark-left">Serial Number Scan </div>
</div>
	<!--  end step-holder -->
	<form name="frmPre" id="frmPre" method="post" action="serial_number_scan.php" enctype="multipart/form-data" >
		<table border="0" cellpadding="0" cellspacing="0"  id="id-form">
		<tr>
			<th valign="top">State:</th>
			<td> 

			<select name="state" id="state" class="styledselect_form_3">
                                <option value="all" <?php echo ($_SESSION['stateCust'] =="all")?'selected':''?>>All</option>
                                <?php foreach ($stateRec as $key => $value) {?>
                                    <option value="<?php echo $value->state_id?>" <?php echo ($_SESSION['stateCust'] ==$value->state_id)?'selected':''?>><?php echo $value->state_name?></option>
                                <?php }?>
                            </select>
			</td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<th valign="top">City:</th>
			<td> 

		 <select name="district" id="district" class="styledselect_form_3">
                                <option value="all">All</option>
                            </select>
			</td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<th valign="top">Taluka:</th>
			<td> 

		 <select name="tehsil" id="tehsil" class="styledselect_form_3">
                                <option value="all">All</option>
                            </select>
			</td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<th valign="top">Shakti Partner Name:</th>
			<td>
			 <select name="partner_name" id="partner_name" class="styledselect_form_3">
                                <option value="all">All</option>
                            </select>
                          <!--   <input type="text" name="partner_name" id="partner_name" class="required"  value="<?php echo $auRec[0]->shakti_partner_name; ?>" /></td> -->
			<td></td>
			<td></td>
		</tr>
		<tr>
			<th valign="top">Shakti Partner Code:</th>
			<td><input type="text" name="partner_code" id="partner_code" class="text number minlength"  value="" maxlength="255" /></td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<th valign="top">Employee ID:</th>
			<td><input type="text" name="employee_id" id="employee_id" class="required"  value="<?php echo $auRec[0]->employee_name; ?>" /></td>
			<td></td>
		</tr>
		<!-- <tr>
			<th valign="top">Employee Code:</th>
			<td><input type="text" name="employee_code" id="employee_code" class="required"  value="<?php echo $auRec[0]->employee_code; ?>" maxlength="255" /></td>
			<td></td>
			<td></td>
		</tr> -->
		<tr>
			<th valign="top">Customer Name:</th>
			<td><input type="text" name="customer_name" id="customer_name" class="required" value="<?php echo $auRec[0]->customer_name; ?>" /></td>
			<td></td>
		</tr>
		<tr>
			<th valign="top">Customer Phone No:</th>
			<td><input type="text" name="customer_mobile_number" id="customer_mobile_number" class="required number minlength" maxlength="15" minlength="10" value="<?php echo $auRec[0]->customer_mobile_number; ?>"/></td>
			<td></td>
		</tr>
		<tr>
			<th valign="top">Serial No:</th>
			<td><input type="text" name="serial_number" id="serial_number" class="required" value="<?php echo $auRec[0]->serial_number; ?>"/></td>
			<td></td>
		</tr>
		
	
		<tr>
			<th valign="top">Status:</th>
			<td> 
			<select name="serial_number_status" id="serial_number_status" class="styledselect_form_3 required">
			<option value="">Please Select</option>
			<option value="Approved">Approved</option>
			<option value="Pending">Pending</option>
			</select>
			</td>
			<td></td>
		</tr>
		
		
		<tr>
		<th>&nbsp;</th>
		<td valign="top">
			<input name="serial_add" type="hidden" value="yes" />
			<input name="serial_id" type="hidden" value="<?php //echo $auRec[0]->serial_id; ?>" />
			<input name="sal_id" type="hidden" value="<?php //echo $auRec[0]->salesman_id; ?>" />
			<input type="reset" value="Reset!" class="form-reset">
			<input type="button" value="Back" class="form-reset" onclick="location.href='serial_no_scan_report.php';" />
			<input name="save" class="form-submit" type="submit" id="save" value="Save" />
	
		</td>
		</tr>
		</table>
	</form>
	<script type="text/javascript">
$(function(){
    if($("#state").val()!="all"){
        state($("#state").val());
        var dist="<?php echo $_SESSION['districtCust'];?>";
        if(dist!="all"){
            district(dist);
        }
    }
   /* if($("#customer").val()!="all"){
        customerClass($("#customer").val());
    }*/
   /* $("#customer").change(function(){
        var customer = $(this).val();
        customerClass(customer);
    });*/
    $("#state").change(function(){
        var stateID = $(this).val();
        state(stateID);
    });
    $("#district").change(function(){
        var cityID = $(this).val();
        district(cityID);
    });
     $("#tehsil").change(function(){
        var tehsilID = $(this).val();
        tehsil(tehsilID);
    });
     $("#partner_name").change(function(){
        var partnerID = $(this).val();
        partner(partnerID);
    });
});

function state(stateID){
    if(stateID!='all'){
        $.ajax({
            method:"GET",
            url: "ajax_serial.php",
            data: { stateID: stateID },
            success: function(result){
                $("#district").html(result);
            }
        });
    }else{
        $("#district").html("<option value='all'>All</option>");
    }
}
function district(cityID){
    if(cityID!='all'){
        $.ajax({
            method:"GET",
            url: "ajax_serial.php",
            data: { cityID: cityID },
            success: function(result){
                $("#tehsil").html(result);
            }
        });
    } else {
        $("#tehsil").html("<option value='all'>All</option>");
    }
}

function tehsil(tehsilID){
    if(tehsilID!='all'){
        $.ajax({
            method:"GET",
            url: "ajax_serial.php",
            data: { tehsilID: tehsilID },
            success: function(result){
                $("#partner_name").html(result);
            }
        });
    } else {
        $("#partner_name").html("<option value='all'>All</option>");
    }
}
function partner(partnerID){
    if(partnerID!='all'){
        $.ajax({
            method:"GET",
            url: "ajax_serial.php",
            dataType: 'text',
            data: { partnerID: partnerID },
            success: function(result){
            	
                //$("#partner_code").html(result);

                 $('#partner_code').val(result);

            }
        });
    } else {
        $("#partner_code").val('');
    }
}
</script>