<?php 
	include("includes/config.inc.php");
	include("includes/function.php");
	$page_name="Add City";
	$_objAdmin = new Admin();
	if($_POST['type']=='country')
	{
		$region = $_objAdmin->_getSelectList2('table_region','region_id,region_name',''," status = 'A' and country_id = '".$_POST['selected_country']."' ORDER BY region_name"); 
		echo '<option value="">Please Select</option>';
		if(is_array($region))
		{
			foreach($region as $value):?>
			 	<option value="<?php echo $value->region_id;?>" <?php if($value->region_id==$auRec[0]->region_id) echo "selected";?> >
			 	<?php echo $value->region_name;?></option>
						<?php endforeach; }
	}
	else if($_POST['type']=='region')
	{
		$zone = $_objAdmin->_getSelectList2('table_zone','zone_id,zone_name',''," status = 'A' and region_id = '".$_POST['selected_region']."' ORDER BY zone_name"); 
		echo '<option value="">Please Select</option>';
		if(is_array($zone))
		{
			foreach($zone as $value):?>
			 	<option value="<?php echo $value->zone_id;?>" <?php if($value->zone_id==$auRec[0]->zone_id) echo "selected";?>>
			 	<?php echo $value->zone_name;?></option>
						<?php endforeach; }
	}
	else if($_POST['type']=='zone')
	{
		$state = $_objAdmin->_getSelectList2('state','state_id,state_name,state_code',''," status = 'A' and zone_id = '".$_POST['selected_zone']."' ORDER BY state_name"); 
		echo '<option value="">Please Select</option>';
		if(is_array($state))
		{
			foreach($state as $value):?>
			 	<option value="<?php echo $value->state_id;?>" <?php if($value->state_id==$auRec[0]->state_id) echo "selected";?> state_code="<?php echo $value->state_code;?>">
			 	<?php echo $value->state_name;?></option>
						<?php endforeach; }
	}
	else if($_POST['type']=='state')
	{
		$city = $_objAdmin->_getSelectList2('city','city_id,city_name',''," status = 'A' and state_id = '".$_POST['selected_state']."' ORDER BY city_name"); 
		echo '<option value="">Please Select</option>';
		if(is_array($city))
		{
			foreach($city as $value):?>
			 	<option value="<?php echo $value->city_id;?>" <?php if($value->city_id==$auRec[0]->city_id) echo "selected";?>>
			 	<?php echo $value->city_name;?></option>
						<?php endforeach; }
	}
	else if($_POST['type']=='city')
	{

		$area = $_objAdmin->_getSelectList2('table_markets','market_id,market_name',''," status = 'A' and city_id = '".$_POST['selected_city']."' ORDER BY market_name"); 
		echo '<option value="">Please Select</option>';
		if(is_array($area))
		{
			foreach($area as $value):?>
			 	<option value="<?php echo $value->market_id;?>" <?php if($value->market_id==$auRec[0]->market_id) echo "selected";?>>
			 	<?php echo $value->market_name;?></option>
						<?php endforeach; 
	    }
	}
	else if($_POST['type']=='getDistributors')
	{

		$getDistributorsList = $_objAdmin->_getSelectList2('table_distributors','distributor_id,distributor_name',''," status = 'A' and city = '".$_POST['selected_city']."' and state = '".$_POST['selected_state']."' ORDER BY distributor_name"); 
		echo '<option value="">Please Select</option>';
		if(is_array($getDistributorsList))
		{
			foreach($getDistributorsList as $value):?>
			 	<option value="<?php echo $value->distributor_id;?>" <?php if($value->distributor_id==$auRec[0]->distributor_id) echo "selected";?>>
			 	<?php echo $value->distributor_name;?></option>
						<?php endforeach; 
	    }
	}
	else if($_POST['type']=='getAllArea') //AJAY@2017-05-04 (Show all the market/area/districts/location and mapped with service distributor)
	{

		$area = $_objAdmin->_getSelectList2('city','city_id,city_name',''," status = 'A' and state_id = '".$_POST['selected_state']."' ORDER BY city_name"); 
		if(is_array($area))
		{
			foreach($area as $value):?>

				<tr><td><input type="checkbox" class="getCheckBoxAll" name="market_id[]"  value="<?php echo $value->city_id;?>" > <?php echo $value->city_name;?></td></tr>
			<?php endforeach; 
	    }
	}

	else if($_POST['type']=='getServiceDistributor') // Show service distributor based on area/districts/location/market AJAY@2017-05-03
	{

		$area = $_objAdmin->_getSelectList2('table_service_distributor AS SD LEFT JOIN table_service_distributor_area_mapping AS SDAM ON SDAM.service_distributor_id = SD.service_distributor_id left join table_markets as MARKET on MARKET.market_id = SDAM.market_id','SD.service_distributor_id,SD.service_distributor_name',''," SD.status = 'A' AND MARKET.city_id = '".$_POST['selected_city']."'  ORDER BY SD.service_distributor_name"); 
		echo '<option value="">Please Select</option>';
		if(is_array($area))
		{
			foreach($area as $value):?>
			 	<option value="<?php echo $value->service_distributor_id;?>" <?php if($value->service_distributor_id==$auRec[0]->service_distributor_id) echo "selected";?>>
			 	<?php echo $value->service_distributor_name;?></option>
						<?php endforeach; 
	    }
	}

	else if($_POST['type']=='service_distributor') // Show service distributor based on area/districts/location/market AJAY@2017-05-03
	{

		$SerDis = $_objAdmin->_getSelectList2('table_service_distributor','service_distributor_email,contact_number',''," service_distributor_id = '".$_POST['selected_distributor']."'"); 
		echo $SerDis[0]->service_distributor_email ."andname=".$SerDis[0]->contact_number;
	}

	else if($_POST['type']=='invoice_change')
	{

      $invoice_date = $_objAdmin->_getSelectList2('table_order_invoice_assign_wh','tally_inv_master_id,warhouse_id,assign_date','',"  tally_inv_master_id = '".$_POST['selected_invoice']."' ORDER BY tally_inv_master_id"); 
		echo '';
		if(is_array($invoice_date))
		{
			foreach($invoice_date as $value):?>
			 	<option value="<?php echo $value->assign_date;?>" <?php if($value->assign_date==$auRec[0]->assign_date) echo "selected";?> >
			 	<?php echo $value->assign_date;?></option>
						<?php endforeach; 
	    }


	}


else if($_POST['type']=='bsn')
	{

      $bsn_date = $_objAdmin->_getSelectList2('table_item_bsn','date_of_sale','',"  bsn = '".$_POST['selected_bsn']."' ORDER BY bsn"); 

	//	echo '<option value="">Please Select</option>';
		if(is_array($bsn_date))
		{
			 
			foreach($bsn_date as $value):?>
		  <?php echo date('d M Y',strtotime($value->date_of_sale));?>

							<?php endforeach; 
	    }

}


else if($_POST['type']=='bsnDetail'){

	// $search_bsn = $_objAdmin->_getSelectList2('table_item_bsn','*','',' bsn="'.$_POST['selected_bsn'].'"');
	// if(count($search_bsn)<=0)
	// {
	// 	$data = array();	
	// 	$bsn = $_POST['selected_bsn'];
	// 	$item_code = substr($bsn, 0, 2);
	// 	$month = substr($bsn, 2, 2);
	// 	$year = substr($bsn, 4, 1);
	// 	$manufacture_date = "201".$year."-".$month."-01";				
	// 	$data['account_id']=$_objAdmin->clean($_SESSION['accountId']);	
	// 	$item_details = $_objAdmin->_getSelectList2('table_item','','',' item_erp_code="'.$item_code.'"');	
	// 	$data['item_id']    			= $item_details[0]->item_id;
	// 	$data['manufacture_date']		= $manufacture_date;
	// 	$data['date_of_sale']			= date('Y-m-d',strtotime($_POST['purchase_date']));
	// 	$warranty_period 				= $item_details[0]->item_warranty;
	// 	$warranty_prorata 				= $item_details[0]->item_prodata;
	// 	$warranty_grace_period			= $item_details[0]->item_grace;
	// 	$category_id 					= $item_details[0]->category_id;

	// 	if($warranty_period>0)
	// 	$data['warranty_period']=$warranty_period;

	// 	if($warranty_prorata>0)
	// 		$data['warranty_prorata']=$warranty_prorata;

	// 	if($warranty_grace_period>0)
	// 		$data['warranty_grace_period']=$warranty_grace_period;

	// 	if($category_id>0)
	// 		$data['category_id']=$category_id;

	// 	$data['bsn']       				= $bsn;
	// 	$data['stock_value']       		= 1;
	// 	$data['last_updated_date']		= date('Y-m-d');
	// 	$data['last_update_datetime']	= date('Y-m-d H:i:s');
	// 	$data['created_datetime']		= date('Y-m-d H:i:s');
	// 	$data['user_type'] 		        = (isset($_SESSION['userLoginType']))?$_SESSION['userLoginType']:0;	
	// 	$data['web_user_id'] 		    = (isset($_SESSION['userLoginType']))?$_SESSION['PepUpSalesUserId']:0;
	// 	$data['status']					= 'C';	//BSN is in Complaint
	// 	$bsn_id = $_objAdmin->_dbInsert($data,'table_item_bsn');
	// }
	// sleep(5);
	$bsnDetail = $_objAdmin->_getSelectList2('table_item_bsn AS BSN 
		LEFT JOIN table_item AS I ON I.item_id = BSN.item_id  
		LEFT JOIN table_segment AS SEG ON SEG.segment_id = I.item_segment 
		LEFT JOIN table_distributors AS D ON D.distributor_id = BSN.sale_distributor_id
		LEFT JOIN table_retailer AS R ON R.retailer_id = BSN.sale_retailer_id
		LEFT JOIN table_customer AS C ON C.customer_id = BSN.customer_id
		LEFT JOIN table_brands AS BRND ON BRND.brand_id = I.brand_id
		LEFT JOIN table_category as CAT ON CAT.category_id = I.category_id
		LEFT JOIN table_order as ORDER1 ON BSN.order_id = ORDER1.order_id
		LEFT JOIN table_complaint as COMPLAINT on COMPLAINT.bsn_id = BSN.bsn_id
		LEFT JOIN table_order_invoice_bsn_tracking as TOIBT on TOIBT.bsn_id = BSN.bsn_id
		LEFT JOIN table_customer as CUSTOMER on CUSTOMER.customer_id = BSN.customer_id
		LEFT JOIN country as CNTRY on CNTRY.country_id = CUSTOMER.country
		LEFT JOIN table_region as REG on REG.region_id = CUSTOMER.region_id
		LEFT JOIN table_zone as ZN on ZN.zone_id = CUSTOMER.zone
		LEFT JOIN state as ST on ST.state_id = CUSTOMER.state
		LEFT JOIN city as CT on CT.city_id = CUSTOMER.city
		LEFT JOIN table_wh_mrn_detail as MRNDtl on MRNDtl.bsn_id = BSN.bsn_id 
		LEFT JOIN table_wh_mrn as MRN on MRN.mrn_id = MRNDtl.mrn_id
		','BSN.date_of_sale, BSN.item_id, I.item_name, I.item_code, I.item_prodata, I.item_warranty,BSN.bsn_id, BSN.warranty_period, BSN.warranty_prorata,BSN.batch_no,BSN.manufacture_date,BSN.warranty_end_date,BSN.date_of_sale, SEG.segment_name, SEG.segment_id, BSN.sale_distributor_id, BSN.sale_retailer_id, D.distributor_name, D.distributor_code, R.retailer_name, R.retailer_code, C.customer_name, C.customer_phone_no, C.customer_address, C.zipcode, C.customer_email,BRND.brand_id,BRND.brand_name,CAT.category_name,ORDER1.bill_no,ORDER1.bill_date,COMPLAINT.created_date,CUSTOMER.*,CNTRY.country_name,REG.region_name,ZN.zone_name,ST.state_name,CT.city_name,TOIBT.challan_no,TOIBT.challan_date,MRN.mrn_no,MRN.mrn_date,MRN.mrn_id,MRNDtl.mrn_detail_id,CAT.category_id,COMPLAINT.complaint_id,COMPLAINT.created_date as complaint_date,COMPLAINT.remark as complaint_remarks,MRNDtl.bsn_reg','',"  LOWER(BSN.bsn) = '".strtolower(trim($_POST['selected_bsn']))."' ORDER BY BSN.bsn"); 
	if(sizeof($bsnDetail)>0){


			$bsn = array();
			$bsn['segment_name'] = "";
			$bsn['segment_id']   = "";
			$bsn['brand_name'] = "";
			$bsn['brand_id']   = "";
			$bsn['item_name']    = "";
			$bsn['item_code']    = "";
			$bsn['item_id']      = "";
			$bsn['category_id']  = "";
			$bsn['retailer_name'] = "";
			$bsn['distributor_name'] = "";
			$bsn['datepicker_id'] = "";
			$bsn['purchased_from'] = "";
			$bsn['purchased_from_type'] = "";
			$bsn['item_warranty'] = "";
			$bsn['item_prodata'] = "";
			$bsn['batch_no'] = "";
			$bsn['bsn_id'] = "";
			$bsn['manufacture_date'] = "";
			$bsn['warranty_end_date'] = "";
			$bsn['date_of_sale'] = "";
			$bsn['EAPL_Invoice_no'] = "";
			$bsn['EAPL_Invoice_date'] ="";
			$bsn['complaint_date'] ="";
			$bsn['complaint_id'] ="";
			$bsn['customer_complaint_remarks'] ="";
			$bsn['challan_no']="";
			$bsn['challan_date']="";
			$bsn['customer_name'] ="";
			$bsn['customer_phone_no'] ="";
			$bsn['customer_address'] ="";
			$bsn['customer_address2'] ="";
			$bsn['customer_address3'] ="";
			$bsn['customer_mobile_no'] ="";
			$bsn['customer_email_id'] ="";
			$bsn['country_id'] ="";
			$bsn['region_id'] ="";
			$bsn['zone_id'] ="";
			$bsn['state_id'] ="";
			$bsn['city_id'] ="";
			$bsn['mrn_no']="";
			$bsn['mrn_date']="";
			$bsn['mrn_id']="";
			$bsn['mrn_detail_id']="";
			$bsn['balance_item_warranty'] = 0;
			$bsn['balance_item_prodata'] =  0;
			$bsn['balanceTotalWarranty'] = 0;


		foreach($bsnDetail as $value){

			$bsn['segment_name'] = addslashes($value->segment_name);
			$bsn['segment_id'] = addslashes($value->segment_id);
			$bsn['brand_name'] = addslashes($value->brand_name);
			$bsn['brand_id'] = addslashes($value->brand_id);
			$bsn['category_name'] = addslashes($value->category_name);
			$bsn['category_id'] = addslashes($value->category_id);
			$bsn['item_name'] = addslashes($value->item_name);
			$bsn['item_code'] = addslashes($value->item_code);
			$bsn['item_id'] = addslashes($value->item_id);
			$bsn['retailer_name'] = addslashes($value->retailer_name);
			$bsn['distributor_name'] = addslashes($value->distributor_name);
			$bsn['datepicker_id'] = date('d M Y',strtotime($value->date_of_sale));
			$bsn['item_warranty'] = $value->warranty_period;
			$bsn['item_prodata'] =  $value->warranty_prorata;
			$bsn['batch_no'] =  $value->batch_no;
			$bsn['bsn_id'] =  $value->bsn_id;
			$bsn['manufacture_date'] =  $value->manufacture_date;
			$_objStock = new StockClass();
			if($value->bsn_reg=='N')
			{
				$bsn['date_of_sale'] =  "";
				$bsn['warranty_end_date'] =  "";
			}
			else
			{
				if($value->date_of_sale =='1970-01-01')
				{
					$bsn['date_of_sale'] = "";	
				}
				else
				{
					$bsn['date_of_sale'] =  $value->date_of_sale;
				}
				$bsn['warranty_end_date'] =  $_objStock->getBSNWarrantyEndDate($value->date_of_sale, $value->item_warranty, $value->item_prodata);	
			}
			
			$bsn['EAPL_Invoice_no'] =  $value->bill_no;
			$bsn['EAPL_Invoice_date'] =  $value->bill_date;
			if(date('Y-m-d',strtotime($value->complaint_date))=='1970-01-01')
			{
				$bsn['complaint_date']='-';
			}
			else
			{
				$bsn['complaint_date'] =  date('Y-m-d',strtotime($value->complaint_date));	
			}
			
			$bsn['complaint_id'] =  $value->complaint_id;
			$bsn['customer_complaint_remarks'] = $value->complaint_remarks;
			$bsn['challan_no'] = $value->challan_no;
			$bsn['challan_date'] = $value->challan_date;
			$bsn['customer_name'] =  $value->customer_name;
			$bsn['customer_phone_no'] =  $value->customer_phone_no;
			$bsn['customer_address'] =  $value->customer_address;
			$bsn['customer_address2'] =  $value->customer_address2;
			$bsn['customer_address3'] =  $value->customer_address3;
			$bsn['customer_mobile_no'] =  $value->customer_mobile_no;
			$bsn['customer_email_id'] =  $value->customer_email;

			if($value->country == '')
			{
				$bsn['country_id'] = 1;
			}
			else
			{
				$bsn['country_id'] = $value->country."Name=".$value->country_name;	
			}			
			$bsn['region_id'] = $value->region_id."Name=".$value->region_name;
			$bsn['zone_id'] = $value->zone."Name=".$value->zone_name;
			$bsn['state_id'] = $value->state."Name=".$value->state_name;
			$bsn['city_id'] = $value->city."Name=".$value->city_name;
			$bsn['mrn_no'] = $value->mrn_no;
			$bsn['mrn_date'] = $value->mrn_date;
			$bsn['mrn_id'] = $value->mrn_id;
			$bsn['mrn_detail_id'] = $value->mrn_detail_id;
			$bsn['balance_item_warranty'] = 0;
			$bsn['balance_item_prodata'] =  0;
			$bsn['balanceTotalWarranty'] = 0;

			if($value->sale_retailer_id>0) {
				$bsn['purchased_from'] = addslashes($value->retailer_name);
				$bsn['purchased_from_type'] = "R";
			} else if($value->sale_distributor_id>0) {
				$bsn['sale_distributor_id'] = addslashes($value->sale_distributor_id);
				$bsn['purchased_from'] = addslashes($value->retailer_name);
				$bsn['purchased_from_type'] = "D";
			} else {
				$bsn['purchased_from'] = '';
				$bsn['purchased_from_type'] = "";
			}

			// Calculate Balance Warranty Date(FOC/Prorata) AJAY@2017-05-03

			$actualTotalWarranty = 0;

			if($value->date_of_sale!='' && $value->date_of_sale!='1970-01-01' && $value->date_of_sale!='0000-00-00' ) {
			

				//$usedBatteryInMonths = strtotime(date('Y-m-d'))-strtotime($auRec[$i]->date_of_sale) / (60*60*24);	

				$datetime1 = date_create(date('Y-m-d'));

				$datetime2 = date_create($value->date_of_sale);

				$interval = date_diff($datetime1, $datetime2);

				//$usedBatteryInMonths = $interval->format('%m month');
				$usedBatteryInDays = $interval->format('%a');
				$usedBatteryInMonths = ceil($usedBatteryInDays  / 30);



				$actualTotalWarranty = $value->warranty_period + $value->warranty_prorata;
				$balanceTotalWarranty = $actualTotalWarranty - $usedBatteryInMonths;

				$bsn['balanceTotalWarranty'] = $balanceTotalWarranty;

				if($balanceTotalWarranty>0 && $balanceTotalWarranty<$actualTotalWarranty) {
					if($balanceTotalWarranty < $value->warranty_period) {
						$bsn['balance_item_warranty'] = $value->warranty_period - $balanceTotalWarranty;
						$bsn['balance_item_prodata'] =  $value->warranty_prorata;
					} else if($balanceTotalWarranty > $value->warranty_period) {
						$bsn['balance_item_warranty'] = 0;
						$bsn['balance_item_prodata'] =  $actualTotalWarranty - $balanceTotalWarranty;
					}
					
					
				}

			} 
			// Calculate Balance Warranty Date(FOC/Prorata) AJAY@2017-05-03

		}

		echo json_encode(array('status'=>'success', 'data'=>$bsn, 'msg'=>'BSN Found'));
	} else {



			$bsn = array();
			$bsn['segment_name'] = "";
			$bsn['segment_id']   = "";
			$bsn['item_name']    = "";
			$bsn['item_id']      = "";
			$bsn['retailer_name'] = "";
			$bsn['distributor_name'] = "";
			$bsn['datepicker_id'] = "";
			$bsn['purchased_from'] = "";
			$bsn['purchased_from_type'] = "";
			$bsn['item_warranty'] = "";
			$bsn['item_prodata'] = "";
			$bsn['balance_item_warranty'] = 0;
			$bsn['balance_item_prodata'] =  0;
			$bsn['balanceTotalWarranty'] = 0;

		echo json_encode(array('status'=>'failed', 'data'=>$bsn, 'msg'=>'BSN Not Found'));
	}
	

}






else if($_POST['type']=='w_name')
	{

      $w_name = $_objAdmin->_getSelectList2('table_warehouse','warehouse_name,warehouse_id','',"  state_id = '".$_POST['wearhouse_name']."' ORDER BY state_id"); 

		echo '<option value="">Please Select</option>';
		if(is_array($w_name))
		{
			 
			foreach($w_name as $value):?>
		  <option value="<?php echo $value->warehouse_id;?>" <?php if($value->assign_date==$auRec[0]->warehouse_id) echo "selected";?> >
			 	<?php echo $value->warehouse_name;?></option>

							<?php endforeach; 
	    }

}

else if($_POST['type']=='warranty_balance')
{
	$bsn_details=$_objAdmin->_getSelectList2('table_item_bsn','*','',' bsn="'.$_POST['selected_bsn'].'"');
	
	if($bsn_details[0]->date_of_sale!='' && $bsn_details[0]->date_of_sale!='1970-01-01' && $bsn_details[0]->date_of_sale!='0000-00-00' )
	{
		$datetime1 = date_create(date('Y-m-d'));

		$datetime2 = date_create(date('Y-m-d',strtotime($_POST['selected_date'])));

		$interval = date_diff($datetime1, $datetime2);
		$usedBatteryInDays = $interval->format('%a');
		$usedBatteryInMonths = ceil($usedBatteryInDays  / 30);
		$actualTotalWarranty = $bsn_details[0]->warranty_period + $bsn_details[0]->warranty_prorata;
		$balanceTotalWarranty = $actualTotalWarranty - $usedBatteryInMonths;
		if($balanceTotalWarranty>0 && $balanceTotalWarranty<$actualTotalWarranty) {
			if($balanceTotalWarranty < $bsn_details[0]->warranty_period) {
				$biw = $bsn_details[0]->warranty_period - $balanceTotalWarranty;
				$bip = $bsn_details[0]->warranty_prorata;
				
			} else if($balanceTotalWarranty > $bsn_details[0]->warranty_period) {
				$biw = 0;
				$bip =  $actualTotalWarranty - $balanceTotalWarranty;
				
			}
		}
		else
		{

			$biw = 0;
			$bip = 0;
		}

	} 
	echo $biw."-()-".$bip;
	
}

else if($_POST['type']=='dealer_name')
	{

      $w_name = $_objAdmin->_getSelectList2('table_retailer','retailer_name,retailer_id','',"  city = '".$_POST['dealer_name']."' ORDER BY city"); 

		echo '<option value="">Please Select</option>';
		if(is_array($w_name))
		{
			 
			foreach($w_name as $value):?>
		  <option value="<?php echo $value->retailer_id;?>" <?php if($value->assign_date==$auRec[0]->retailer_id) echo "selected";?> >
			 	<?php echo $value->retailer_name;?></option>

							<?php endforeach; 
	    }

}


else if($_POST['type']=='id_state')
	{
		$city = $_objAdmin->_getSelectList2('city','city_id,city_name',''," status = 'A' and state_id = '".$_POST['selected_state']."' ORDER BY city_name"); 
		echo '<option value="">Please Select</option>';
		if(is_array($city))
		{
			foreach($city as $value):?>
			 	<option value="<?php echo $value->city_id;?>" <?php if($value->city_id==$auRec[0]->city_id) echo "selected";?> >
			 	<?php echo $value->city_name;?></option>
						<?php endforeach; 

					}
	}
else if($_POST['requestType']=='calculate_time')
	{

		$start_datetime = date('Y-m-d',strtotime($_POST['start_date']))." ".date("G:i:s", strtotime(str_replace(' ', '', $_POST['start_time'])));
		$removal_datetime = date('Y-m-d',strtotime($_POST['removal_date']))." ".date("G:i:s", strtotime(str_replace(' ','',$_POST['removal_time'])));
		$total_hours = round((strtotime($removal_datetime) - strtotime($start_datetime))/(60*60));
		echo $total_days = floor($total_hours/24)."hours=".round($total_hours % 24);
	}


	

	
?>