<?php 
	include("includes/config.inc.php");
	include("includes/function.php");
	$page_name="Add City";
	$_objAdmin = new Admin();
	if($_POST['type']=='country')
	{
		$region = $_objAdmin->_getSelectList2('table_region','region_id,region_name',''," status = 'A' and country_id = '".$_POST['selected_country']."' ORDER BY region_name"); 
		echo '<option value="">Please Select</option>';
		if(is_array($region))
		{
			foreach($region as $value):?>
			 	<option value="<?php echo $value->region_id;?>" <?php if($value->region_id==$auRec[0]->region_id) echo "selected";?> >
			 	<?php echo $value->region_name;?></option>
						<?php endforeach; }
	}
	else if($_POST['type']=='region')
	{
		$zone = $_objAdmin->_getSelectList2('table_zone','zone_id,zone_name',''," status = 'A' and region_id = '".$_POST['selected_region']."' ORDER BY zone_name"); 
		echo '<option value="">Please Select</option>';
		if(is_array($zone))
		{
			foreach($zone as $value):?>
			 	<option value="<?php echo $value->zone_id;?>" <?php if($value->zone_id==$auRec[0]->zone_id) echo "selected";?>>
			 	<?php echo $value->zone_name;?></option>
						<?php endforeach; }
	}
	else if($_POST['type']=='zone')
	{
		$state = $_objAdmin->_getSelectList2('state','state_id,state_name,state_code',''," status = 'A' and zone_id = '".$_POST['selected_zone']."' ORDER BY state_name"); 
		echo '<option value="">Please Select</option>';
		if(is_array($state))
		{
			foreach($state as $value):?>
			 	<option value="<?php echo $value->state_id;?>" <?php if($value->state_id==$auRec[0]->state_id) echo "selected";?> state_code="<?php echo $value->state_code;?>">
			 	<?php echo $value->state_name;?></option>
						<?php endforeach; }
	}
	else if($_POST['type']=='state')
	{
		$city = $_objAdmin->_getSelectList2('city as CT','city_id,city_name',''," CT.status = 'A' and CT.state_id = '".$_POST['selected_state']."' ORDER BY CT.city_name"); 
		echo '<option value="">Please Select</option>';
		if(is_array($city))
		{
			foreach($city as $value):?>
			 	<option value="<?php echo $value->city_id;?>" <?php if($value->city_id==$auRec[0]->city_id) echo "selected";?>>
			 	<?php echo $value->city_name;?></option>
						<?php endforeach; }
	}
	else if($_POST['type']=='city')
	{

		$area = $_objAdmin->_getSelectList2('table_markets','market_id,market_name',''," status = 'A' and city_id = '".$_POST['selected_city']."' ORDER BY market_name"); 
		echo '<option value="">Please Select</option>';
		if(is_array($area))
		{
			foreach($area as $value):?>
			 	<option value="<?php echo $value->market_id;?>" <?php if($value->market_id==$auRec[0]->market_id) echo "selected";?>>
			 	<?php echo $value->market_name;?></option>
						<?php endforeach; 
	    }
	}
	else if($_POST['type']=='getAllArea') //AJAY@2017-05-04 (Show all the market/area/districts/location and mapped with service distributor)
	{

		$area = $_objAdmin->_getSelectList2('table_markets','market_id,market_name',''," status = 'A' and city_id = '".$_POST['selected_city']."' ORDER BY market_name"); 
		if(is_array($area))
		{
			foreach($area as $value):?>

				<tr><td><input type="checkbox" class="getCheckBoxAll" name="market_id[]"  value="<?php echo $value->market_id;?>" > <?php echo $value->market_name;?></td></tr>
			<?php endforeach; 
	    }
	}

	else if($_POST['type']=='getServiceDistributor') // Show service distributor based on area/districts/location/market AJAY@2017-05-03
	{

		$area = $_objAdmin->_getSelectList2('table_service_distributor_area_mapping as SDAM left join city as CT on CT.city_id = SDAM.market_id left join table_service_distributor as SD on SD.service_distributor_id = SDAM.service_distributor_id','SD.service_distributor_name,SD.service_distributor_id',''," SD.status = 'A' AND SDAM.market_id = '".$_POST['selected_district']."'  ORDER BY SD.service_distributor_name"); 
		echo '<option value="">Please Select</option>';
		if(is_array($area))
		{
			foreach($area as $value):?>
			 	<option value="<?php echo $value->service_distributor_id;?>" <?php if($value->service_distributor_id==$auRec[0]->service_distributor_id) echo "selected";?>>
			 	<?php echo $value->service_distributor_name;?></option>
						<?php endforeach; 
	    }
	}

	else if($_POST['type']=='service_distributor') // Show service distributor based on area/districts/location/market AJAY@2017-05-03
	{

		$SerDis = $_objAdmin->_getSelectList2('table_service_distributor','service_distributor_email,contact_number',''," service_distributor_id = '".$_POST['selected_distributor']."'"); 
		echo $SerDis[0]->service_distributor_email ."andname=".$SerDis[0]->contact_number;
	}

	else if($_POST['type']=='invoice_change')
	{

      $invoice_date = $_objAdmin->_getSelectList2('table_order_invoice_assign_wh','tally_inv_master_id,warhouse_id,assign_date','',"  tally_inv_master_id = '".$_POST['selected_invoice']."' ORDER BY tally_inv_master_id"); 
		echo '<option value="">Please Select</option>';
		if(is_array($invoice_date))
		{
			foreach($invoice_date as $value):?>
			 	<option value="<?php echo $value->assign_date;?>" <?php if($value->assign_date==$auRec[0]->assign_date) echo "selected";?> >
			 	<?php echo $value->assign_date;?></option>
						<?php endforeach; 
	    }


	}


else if($_POST['type']=='bsn')
	{

      $bsn_date = $_objAdmin->_getSelectList2('table_item_bsn','date_of_sale','',"  bsn = '".$_POST['selected_bsn']."' ORDER BY bsn"); 

	//	echo '<option value="">Please Select</option>';
		if(is_array($bsn_date))
		{
			 
			foreach($bsn_date as $value):?>
		  <?php echo date('d M Y',strtotime($value->date_of_sale));?>

							<?php endforeach; 
	    }

}


else if($_POST['type']=='bsnDetail'){
	$search_bsn = $_objAdmin->_getSelectList2('table_item_bsn','*','',' bsn="'.$_POST['selected_bsn'].'"');
	if(count($search_bsn)<=0 && $_POST['new_old']=='old')
	{
		$data = array();	
		$bsn = $_POST['selected_bsn'];
		$item_code = substr($bsn, 0, 2);
		$month = substr($bsn, 2, 2);
		$year = substr($bsn, 4, 1);
		$manufacture_date = "201".$year."-".$month."-01";				
		$data['account_id']        		= 1;
		$item_details = $_objAdmin->_getSelectList2('table_item','','',' item_erp_code_old="'.$item_code.'"');	
		if(count($item_details)>0)
		{
			$data['item_id']    			= $item_details[0]->item_id;
			$data['manufacture_date']		= $manufacture_date;
			$data['date_of_sale']			= date('Y-m-d',strtotime($_POST['purchase_date']));
			$warranty_period 				= $item_details[0]->item_warranty;
			$warranty_prorata 				= $item_details[0]->item_prodata;
			$warranty_grace_period			= $item_details[0]->item_grace;
			$category_id 					= $item_details[0]->category_id;

			if($warranty_period>0)
			$data['warranty_period']=$warranty_period;

			if($warranty_prorata>0)
				$data['warranty_prorata']=$warranty_prorata;

			if($warranty_grace_period>0)
				$data['warranty_grace_period']=$warranty_grace_period;

			if($category_id>0)
				$data['category_id']=$category_id;

			$data['bsn']       				= $bsn;
			$data['stock_value']       		= 1;
			$data['account_id']        		= 1;
			$data['last_updated_date']		= date('Y-m-d');
			$data['last_update_datetime']	= date('Y-m-d H:i:s');
			$data['created_datetime']		= date('Y-m-d H:i:s');
			$data['user_type'] 		        = (isset($_SESSION['userLoginType']))?$_SESSION['userLoginType']:0;	
			$data['web_user_id'] 		    = (isset($_SESSION['userLoginType']))?$_SESSION['PepUpSalesUserId']:0;
			$data['status']					= 'C';	//BSN is in Complaint
			$bsn_id = $_objAdmin->_dbInsert($data,'table_item_bsn');

		}
	}
	else if(count($search_bsn)<=0 && $_POST['new_old']=='new')
	{
		$data = array();	
		$bsn = $_POST['selected_bsn'];
		$item_code = substr($bsn, 0, 3);
		$day = substr($bsn,3,1);
		$month = substr($bsn,4,1);
		$year = substr($bsn,5,1);
		$charging_site = substr($bsn,6,1);
		$division = substr($bsn,7,1);
		$model_code = substr($bsn,8,4);
		$battery_serial = substr($bsn,12,5);
		$day_value = $_objAdmin->_getSelectList2('table_bsn_logic_day','*','',' character_value="'.$day.'"');				

		$month_value = $_objAdmin->_getSelectList2('table_bsn_logic_month','*','',' character_value="'.$month.'"');
		$year_value = $_objAdmin->_getSelectList2('table_bsn_logic_year','*','',' character_value="'.$year.'"');
		$division_value = $_objAdmin->_getSelectList2('table_division','*','',' division_bsn_code="'.$division.'"');
		$manufacture_date = $year_value[0]->year."-".date('m',strtotime($month_value[0]->month))."-".str_pad($day_value[0]->day,2,'0',STR_PAD_LEFT);
		$item_details = $_objAdmin->_getSelectList2('table_item','','',' item_erp_code="'.$item_code.'" and model_code="'.$model_code.'"');
		$data['account_id'] = 1;
		if(count($item_details)>0 && count($month_value)>0 && count($day_value)>0 && count($year_value)>0 && count($division_value)>0)
		{
			$data['item_id']    			= $item_details[0]->item_id;
			$data['manufacture_date']		= $manufacture_date;
			$data['date_of_sale']			= date('Y-m-d',strtotime($_POST['purchase_date']));
			$warranty_period 				= $item_details[0]->item_warranty;
			$warranty_prorata 				= $item_details[0]->item_prodata;
			$warranty_grace_period			= $item_details[0]->item_grace;
			$category_id 					= $item_details[0]->category_id;

			if($warranty_period>0)
			$data['warranty_period']=$warranty_period;

			if($warranty_prorata>0)
				$data['warranty_prorata']=$warranty_prorata;

			if($warranty_grace_period>0)
				$data['warranty_grace_period']=$warranty_grace_period;

			if($category_id>0)
				$data['category_id']=$category_id;

			$data['bsn']       				= $bsn;
			$data['stock_value']       		= 1;
			$data['account_id']        		= 1;
			$data['last_updated_date']		= date('Y-m-d');
			$data['last_update_datetime']	= date('Y-m-d H:i:s');
			$data['created_datetime']		= date('Y-m-d H:i:s');
			$data['user_type'] 		        = (isset($_SESSION['userLoginType']))?$_SESSION['userLoginType']:0;	
			$data['web_user_id'] 		    = (isset($_SESSION['userLoginType']))?$_SESSION['PepUpSalesUserId']:0;
			$data['status']					= 'C';	//BSN is in Complaint
			$bsn_id = $_objAdmin->_dbInsert($data,'table_item_bsn');

		}
	}
	sleep(5);
	$bsnDetail = $_objAdmin->_getSelectList2('table_item_bsn AS BSN 
		LEFT JOIN table_item AS I ON I.item_id = BSN.item_id  
		LEFT JOIN table_segment AS SEG ON SEG.segment_id = I.item_segment 
		LEFT JOIN table_distributors AS D ON D.distributor_id = BSN.sale_distributor_id
		LEFT JOIN table_retailer AS R ON R.retailer_id = BSN.sale_retailer_id
		LEFT JOIN table_customer AS C ON C.customer_id = BSN.customer_id
		LEFT JOIN table_brands AS BRND ON BRND.brand_id = I.brand_id
		','BSN.date_of_sale, BSN.item_id, I.item_name, I.item_code, I.item_prodata, I.item_warranty, BSN.warranty_period, BSN.warranty_prorata, SEG.segment_name, SEG.segment_id, BSN.sale_distributor_id, BSN.sale_retailer_id, D.distributor_name, D.distributor_code, R.retailer_name, R.retailer_code, C.customer_name, C.customer_phone_no, C.customer_address, C.zipcode, C.customer_email,BRND.brand_id,BRND.brand_name','',"  LOWER(BSN.bsn) = '".strtolower(trim($_POST['selected_bsn']))."' ORDER BY BSN.bsn"); 
	
	if(sizeof($bsnDetail)>0){


			$bsn = array();
			$bsn['segment_name'] = "";
			$bsn['segment_id']   = "";
			$bsn['brand_name'] = "";
			$bsn['brand_id']   = "";
			$bsn['item_name']    = "";
			$bsn['item_code']    = "";
			$bsn['item_id']      = "";
			$bsn['retailer_name'] = "";
			$bsn['distributor_name'] = "";
			$bsn['datepicker_id'] = "";
			$bsn['purchased_from'] = "";
			$bsn['purchased_from_type'] = "";
			$bsn['item_warranty'] = "";
			$bsn['item_prodata'] = "";
			$bsn['balance_item_warranty'] = 0;
			$bsn['balance_item_prodata'] =  0;
			$bsn['balanceTotalWarranty'] = 0;


		foreach($bsnDetail as $value){

			$bsn['segment_name'] = addslashes($value->segment_name);
			$bsn['segment_id'] = addslashes($value->segment_id);
			$bsn['brand_name'] = addslashes($value->brand_name);
			$bsn['brand_id'] = addslashes($value->brand_id);
			$bsn['item_name'] = addslashes($value->item_name);
			$bsn['item_code'] = addslashes($value->item_code);
			$bsn['item_id'] = addslashes($value->item_id);
			$bsn['retailer_name'] = addslashes($value->retailer_name);
			$bsn['distributor_name'] = addslashes($value->distributor_name);
			$bsn['datepicker_id'] = date('d M Y',strtotime($value->date_of_sale));
			$bsn['item_warranty'] = $value->warranty_period;
			$bsn['item_prodata'] =  $value->warranty_prorata;
			$bsn['balance_item_warranty'] = 0;
			$bsn['balance_item_prodata'] =  0;
			$bsn['balanceTotalWarranty'] = 0;

			if($value->sale_retailer_id>0) {
				$bsn['purchased_from'] = addslashes($value->retailer_name);
				$bsn['purchased_from_type'] = "R";
			} else if($value->sale_distributor_id>0) {
				$bsn['purchased_from'] = addslashes($value->retailer_name);
				$bsn['purchased_from_type'] = "D";
			} else {
				$bsn['purchased_from'] = '';
				$bsn['purchased_from_type'] = "";
			}

			// Calculate Balance Warranty Date(FOC/Prorata) AJAY@2017-05-03

			$actualTotalWarranty = 0;

			if($value->date_of_sale!='' && $value->date_of_sale!='1970-01-01' && $value->date_of_sale!='0000-00-00' ) {
			

				//$usedBatteryInMonths = strtotime(date('Y-m-d'))-strtotime($auRec[$i]->date_of_sale) / (60*60*24);	

				$datetime1 = date_create(date('Y-m-d'));

				$datetime2 = date_create($value->date_of_sale);

				$interval = date_diff($datetime1, $datetime2);

				//$usedBatteryInMonths = $interval->format('%m month');
				$usedBatteryInDays = $interval->format('%a');
				$usedBatteryInMonths = ceil($usedBatteryInDays  / 30);



				$actualTotalWarranty = $value->warranty_period + $value->warranty_prorata;
				//$balanceTotalWarranty = $actualTotalWarranty - $usedBatteryInMonths;

				$bsn['balanceTotalWarranty'] = $balanceTotalWarranty;

				//if($balanceTotalWarranty>0 && $balanceTotalWarranty<$actualTotalWarranty) {
					if($balanceTotalWarranty < $value->warranty_period) {
						// $bsn['balance_item_warranty'] = $value->warranty_period - $balanceTotalWarranty;
						// $bsn['balance_item_prodata'] =  $value->warranty_prorata;

 						$bsn['balance_item_warranty'] = $value->warranty_period - $usedBatteryInMonths;
            			$bsn['balance_item_prodata'] =   $value->warranty_prorata;

					} else if($balanceTotalWarranty > $value->warranty_period) {
						// $bsn['balance_item_warranty'] = 0;
						// $bsn['balance_item_prodata'] =  $actualTotalWarranty - $balanceTotalWarranty;

			            $bsn['balance_item_warranty'] = 0;
			            $bsn['balance_item_prodata'] =  $actualTotalWarranty - $usedBatteryInMonths;

					}
					
					
				//}

			} 
			// Calculate Balance Warranty Date(FOC/Prorata) AJAY@2017-05-03

		}

		echo json_encode(array('status'=>'success', 'data'=>$bsn, 'msg'=>'BSN Found'));
	} else {



			$bsn = array();
			$bsn['segment_name'] = "";
			$bsn['segment_id']   = "";
			$bsn['item_name']    = "";
			$bsn['item_id']      = "";
			$bsn['retailer_name'] = "";
			$bsn['distributor_name'] = "";
			$bsn['datepicker_id'] = "";
			$bsn['purchased_from'] = "";
			$bsn['purchased_from_type'] = "";
			$bsn['item_warranty'] = "";
			$bsn['item_prodata'] = "";
			$bsn['balance_item_warranty'] = 0;
			$bsn['balance_item_prodata'] =  0;
			$bsn['balanceTotalWarranty'] = 0;
		echo json_encode(array('status'=>'failed', 'data'=>$bsn, 'msg'=>'BSN Not Found'));
	}
	

}






else if($_POST['type']=='w_name')
	{

      $w_name = $_objAdmin->_getSelectList2('table_warehouse','warehouse_name,warehouse_id','',"  state_id = '".$_POST['wearhouse_name']."' ORDER BY state_id"); 

		echo '<option value="">Please Select</option>';
		if(is_array($w_name))
		{
			 
			foreach($w_name as $value):?>
		  <option value="<?php echo $value->warehouse_id;?>" <?php if($value->assign_date==$auRec[0]->warehouse_id) echo "selected";?> >
			 	<?php echo $value->warehouse_name;?></option>

							<?php endforeach; 
	    }

}

else if($_POST['type']=='warranty_balance')
{
	$bsn_details=$_objAdmin->_getSelectList2('table_item_bsn','*','',' bsn="'.$_POST['selected_bsn'].'"');
	
	if($bsn_details[0]->date_of_sale!='' && $bsn_details[0]->date_of_sale!='1970-01-01' && $bsn_details[0]->date_of_sale!='0000-00-00' )
	{
		$datetime1 = date_create(date('Y-m-d'));

		$datetime2 = date_create(date('Y-m-d',strtotime($_POST['selected_date'])));

		$interval = date_diff($datetime1, $datetime2);
		$usedBatteryInDays = $interval->format('%a');
		$usedBatteryInMonths = ceil($usedBatteryInDays  / 30);
		$actualTotalWarranty = $bsn_details[0]->warranty_period + $bsn_details[0]->warranty_prorata;
		$balanceTotalWarranty = $actualTotalWarranty - $usedBatteryInMonths;
		if($balanceTotalWarranty>0 && $balanceTotalWarranty<$actualTotalWarranty) {
			if($balanceTotalWarranty < $bsn_details[0]->warranty_period) {
				$biw = $bsn_details[0]->warranty_period - $balanceTotalWarranty;
				$bip = $bsn_details[0]->warranty_prorata;
				
			} else if($balanceTotalWarranty > $bsn_details[0]->warranty_period) {
				$biw = 0;
				$bip =  $actualTotalWarranty - $balanceTotalWarranty;
				
			}
		}
		else
		{

			$biw = 0;
			$bip = 0;
		}

	} 
	echo $biw."-()-".$bip;
	
}





	

	
?>