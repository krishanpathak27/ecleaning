<?php
include("includes/config.inc.php");
include("includes/function.php");
$_objAdmin = new Admin();
if (isset($_POST['add']) && $_POST['add'] == 'yes') {
    $err = '';
    if ($_POST['supervisor_id'] != "") {
        $condi = " (supervisor_name='" . mysql_escape_string($_POST['supervisor_name']) . "') and supervisor_phone_no='" . mysql_escape_string($_POST['supervisor_phone_no']) . "' and status != 'D'  and supervisor_id<>'" . $_POST['supervisor_id'] . "'";
    } else {
        $condi = " (supervisor_name='" . mysql_escape_string($_POST['supervisor_name']) . "') and supervisor_phone_no='" . mysql_escape_string($_POST['supervisor_phone_no']) . "'  and status != 'D'";
    }
    $auRec = $_objAdmin->_getSelectList('table_supervisor', "*", '', $condi);
    if (is_array($auRec)) {
        $err = "Supervisor Already exits";
    }
    if ($_POST['supervisor_id'] != "") {
        $condi = " username='" . $_POST['username'] . "' and web_user_id<>'" . $_POST['web_id'] . "'";
    } else {
        $condi = " username='" . $_POST['username'] . "'";
    }
    $auRec = $_objAdmin->_getSelectList('table_web_users', "*", '', $condi);

    if (is_array($auRec)) {
        $err = "Username Already exits";
    }
    if ($err == '') {
        if ($_POST['supervisor_id'] != "" && $_POST['supervisor_id'] != 0) {
            $save = $_objAdmin->UpdateSupervisor($_POST["supervisor_id"]);
            $sus = "Updated Successfully";
        } else {
            $save = $_objAdmin->addSupervisor();
            $sus = "Added Successfully";
        }
        ?>
        <script type="text/javascript">
            window.location = "supervisor.php?suc="+'<?php echo $sus; ?>';
        </script>
        <?php
    }
}
if (isset($_REQUEST['id']) && $_REQUEST['id'] != "") {
    $auRec = $_objAdmin->_getSelectList('table_supervisor as d left join table_web_users as w on w.supervisor_id=d.supervisor_id', "d.*,w.username,w.email_id,w.web_user_id", '', " d.supervisor_id=" . $_REQUEST['id']);
    
    if (count($auRec) <= 0)
        header("Location: supervisor.php");
}
include("header.inc.php");
?>
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <!-- BEGIN: Subheader -->
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">
                    Supervisor
                </h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="#" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__separator">
                        -
                    </li>
                    <li class="m-nav__item">
                        <a href="#" class="m-nav__link">
                            <span class="m-nav__link-text">
                                Master
                            </span>
                        </a>
                    </li>
                    <li class="m-nav__separator">
                        -
                    </li>
                    <li class="m-nav__item">
                        <a href="" class="m-nav__link">
                            <span class="m-nav__link-text">
                                Supervisor
                            </span>
                        </a>
                    </li>  
                </ul>
            </div> 
        </div>
    </div>
    <!-- END: Subheader -->
    <div class="m-content">
        <div class="row">
            <div class="col-lg-12">
                <!--begin::Portlet--> 
                <div class="m-portlet">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <span class="m-portlet__head-icon m--hide">
                                    <i class="la la-gear"></i>
                                </span>
                                <h3 class="m-portlet__head-text">
                                    Supervisor Form
                                </h3>
                            </div>
                        </div>
                    </div>
                    <!--begin::Form-->
                    <form class="m-form m-form--fit m-form--label-align-right" id="m_form_1"  method="post" action="add_supervisor.php" enctype="multipart/form-data">

                        <div class="m-portlet__body">
                            <div class="m-form__content">
                                <div class="m-alert m-alert--icon alert alert-danger m--hide" role="alert" id="m_form_1_msg">
                                    <div class="m-alert__icon">
                                        <i class="la la-warning"></i>
                                    </div>
                                    <div class="m-alert__text">
                                        Oh snap! Change a few things up and try submitting again.
                                    </div>
                                    <div class="m-alert__close">
                                        <button type="button" class="close" data-close="alert" aria-label="Close"></button>
                                    </div>
                                </div>
                            </div>
                            <?php if (isset($err) && !empty($err)) { ?>
                                <div class="m-form__content">
                                    <div class="m-alert m-alert--icon alert alert-danger" role="alert" id="m_form_1_msg1">
                                        <div class="m-alert__icon">
                                            <i class="la la-warning"></i>
                                        </div>
                                        <div class="m-alert__text">
                                            <?php echo $err; ?>
                                        </div>
                                        <div class="m-alert__close">
                                            <button type="button" class="close" data-close="alert" aria-label="Close"></button>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                            <div class="form-group m-form__group row">
                                <div class="col-lg-4">
                                    <label>
                                        Supervisor Name:
                                    </label>
                                    <input type="text" class="form-control m-input" placeholder="Enter full name" id="supervisor_name" name="supervisor_name" value="<?php echo $auRec[0]->supervisor_name; ?>">

                                </div>
                                <div class="col-lg-4">
                                    <label class="">
                                        Supervisor Phone No:
                                    </label>
                                    <input type="text" class="form-control m-input" placeholder="Phone No" name="supervisor_phone_no" id="supervisor_phone_no" value="<?php echo $auRec[0]->supervisor_phone_no; ?>"> 
                                </div>
                                <div class="col-lg-4">
                                    <label>
                                        Building:
                                    </label>
                                    <div class="input-group m-input-group m-input-group--square"> 
                                        <select class="form-control m-input m-input--square" id="building_id" name="building_id">
                                            <option value="">Select Building</option>
                                            <?php
                                            $condi = "";

                                            if (isset($auRec[0]->building_id) && !empty($auRec[0]->building_id)) {
                                                $branchList = $_objAdmin->_getSelectList('table_buildings as b left join table_supervisor as s on s.building_id = b.building_id', "b.building_id,b.building_name", '', " b.status='A' and s.supervisor_name IS NULL");
                                                $brnchlist1 = $_objAdmin->_getSelectList('table_buildings as b ', "b.building_id,b.building_name", '', " b.building_id='" . $auRec[0]->building_id . "'");
                                            } else {
                                                $branchList = $_objAdmin->_getSelectList('table_buildings as b left join table_supervisor as s on s.building_id = b.building_id', "b.building_id,b.building_name", '', " b.status='A' and s.supervisor_name IS NULL");
                                            }

                                            for ($i = 0; $i < count($branchList); $i++) {

                                                if ($branchList[$i]->building_id == $auRec[0]->building_id) {
                                                    $select = "selected";
                                                } else {
                                                    $select = "";
                                                }
                                                ?>
                                                <option value="<?php echo $branchList[$i]->building_id; ?>" <?php echo $select ?>><?php echo $branchList[$i]->building_name; ?></option>

                                            <?php } ?>
                                            <?php if (isset($brnchlist1) && !empty($brnchlist1)) { ?>
                                                <option value="<?php echo $brnchlist1[0]->building_id; ?>" selected><?php echo $brnchlist1[0]->building_name; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div> 
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <div class="col-lg-4">
                                    <label class="">
                                        Supervisor Address:
                                    </label>
                                    <input type="text" class="form-control m-input" placeholder="Enter Address" id="supervisor_address" name="supervisor_address" value="<?php echo $auRec[0]->supervisor_address; ?>"> 
                                </div>
                                <div class="col-lg-4">
                                    <label class="">
                                        Supervisor Email:
                                    </label>
                                    <div class="m-input-icon m-input-icon--right">
                                        <input type="text" class="form-control m-input" placeholder="Email" name="supervisor_email" id="supervisor_email" value="<?php echo $auRec[0]->supervisor_email; ?>"> 
                                    </div> 
                                </div>
                                <div class="col-lg-4">
                                    <label class="">
                                        User Name:
                                    </label>
                                    <div class="m-input-icon m-input-icon--right">
                                        <input type="text" class="form-control m-input" placeholder="User Name" id="username" name="username" value="<?php echo $auRec[0]->username; ?>"> 
                                    </div> 
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <div class="col-lg-4">
                                    <label class="">
                                        Password:
                                    </label>
                                    <div class="m-input-icon m-input-icon--right">
                                        <input type="password" class="form-control m-input" placeholder="password" id="password" name="password"> 
                                    </div> 
                                </div>
                            </div>
                            <div class="form-group m-form__group row"> 
                                <div class="col-lg-4">
                                    <label class="">
                                        Status:
                                    </label>
                                    <div class="m-radio-inline">
                                        <label class="m-radio m-radio--solid">
                                            <input <?php if($auRec[0]->status=='A') echo "checked";?> type="radio" name="status"  value="A" >
                                            Active
                                            <span></span>
                                        </label>
                                        <label class="m-radio m-radio--solid">
                                            <input type="radio" name="status" value="I" <?php if($auRec[0]->status=='I') echo "checked";?>>
                                            Inactive
                                            <span></span>
                                        </label>
                                    </div> 
                                </div>
                            </div>
                        </div>
                        <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                            <div class="m-form__actions m-form__actions--solid">
                                <div class="row">
                                    <div class="col-lg-4"></div>
                                    <div class="col-lg-8">
                                        <button type="submit" class="btn btn-success">
                                            Submit
                                        </button>
                                        <button type="reset" class="btn btn-secondary">
                                            Reset
                                        </button>
                                        <a href="supervisor.php" class="btn btn-secondary">
                                            back
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <input name="supervisor_id" type="hidden" value="<?php echo $auRec[0]->supervisor_id; ?>" />
                        <input name="web_id" type="hidden" value="<?php echo $auRec[0]->web_user_id; ?>" />
                        <input name="account_id" type="hidden" value="<?php echo $_SESSION['accountId']; ?>" />
                        <input type="hidden" name="add" value="yes">

                    </form>
                    <!--end::Form-->
                </div>
                <!--end::Portlet-->
            </div>
        </div>
    </div>
</div>
</div>
</div>
<!-- start footer -->         
<?php include("footer.php") ?>

<script type="text/javascript">
    var FormControls = function () {
        //== Private functions

        var demo1 = function () {
            $("#m_form_1").validate({
                // define validation rules
                rules: {
                    supervisor_name: {
                        required: true
                    },
                    supervisor_phone_no: {
                        required: true,
                        digits: true
                    },
                    building_id: {
                        required: true
                    },
                    supervisor_email: {
                        required: true,
                        email: true,
                        minlength: 10
                    },
                    username: {
                        required: true
                    },
                    password: {
                        required: true,
                        minlength: 8
                    },
                    status: {
                        required: true,
                    }
                },
                //display error alert on form submit  
                invalidHandler: function (event, validator) {
                    var alert = $('#m_form_1_msg');
                    alert.removeClass('m--hide').show();
                    mApp.scrollTo(alert, -200);
                },
                submitHandler: function (form) {
                    form.submit();
                }
            });
        }

        return {
            // public functions
            init: function () {
                demo1();
            }
        };
    }();

    jQuery(document).ready(function () {
        var id = '<?php echo $_REQUEST['id']; ?>';
        FormControls.init();
        if(id) {
            $('#password').rules('add', {
            required: false,
            minlength: 8
        });
        }
    });

</script>

