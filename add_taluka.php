<?php
include("includes/config.inc.php");
include("includes/function.php");
$page_name="Add/Edit Taluka";
$_objAdmin = new Admin();

if(isset($_POST['add']) && $_POST['add'] == 'yes')
{
	if($_REQUEST['id']!="") {
		$condi=	" taluka_code ='".$_POST['taluka_code']."' and taluka_id <>'".$_REQUEST['id']."' and status!='D'";
	} else {
		$condi=	" taluka_code='".$_POST['taluka_code']."' and status!='D'";
	}

	$auRec=$_objAdmin->_getSelectList('table_taluka',"*",'',$condi);

	if(is_array($auRec)) {
		$err="Taluka already exists in the system.";	
		$auRec[0]=(object)$_POST;						
	} else {

		if($_REQUEST['id']!="")  {
			$cid=$_objAdmin->updateTaluka($_REQUEST['id']);
			$sus="Taluka has been updated successfully.";
		} else  {
			$cid = $_objAdmin->addTaluka();
			$sus="Taluka has been added successfully.";
		}
	}
}


if(isset($_REQUEST['id']) && $_REQUEST['id']!="")
{
	$auRec=$_objAdmin->_getSelectList('table_taluka',"*",''," taluka_id=".$_REQUEST['id']);
	if(count($auRec)<=0) header("Location: taluka.php");
}

?>

<?php include("header.inc.php") ?>

<script src="javascripts/jquery-1.8.2.js" type="text/javascript"></script>
<script src="javascripts/jquery-ui.js"></script>
<script type="text/javascript" src="javascripts/validate.js"></script>
<script type="text/javascript">
$(document).ready(function() {
	var v = $("#frmPre").validate({
			rules: {
				fileToUpload: {
				  required: true,
				  accept: "csv"
				}
			  },
			submitHandler: function(form) {
				document.frmPre.submit();				
			}
		});
});

</script>

<!-- start content-outer -->
<div id="content-outer">
<!-- start content -->
	<div id="content">
	<div id="page-heading"><h1><span style="color: #d74343; font-family: Tahoma; font-weight: bold;">Add Taluka</span></h1></div>
	<table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">
		<tr>
			<td>
			<!--  start content-table-inner -->
			<div id="content-table-inner">
				<table border="0" width="100%" cellpadding="0" cellspacing="0">
				<tr valign="top">
					<td>
					<!--  start message-red -->
					<?php if($err!=''){?>
					<div id="message-red">
						<table border="0" width="100%" cellpadding="0" cellspacing="0">
						<tr>
							<td class="red-left"> <?php echo $err; ?></td>
							<td class="red-right"><a class="close-red"><img src="images/icon_close_red.gif"   alt="" /></a></td>
						</tr>
						</table>
					</div>
					<?php } ?>
					<!--  end message-red -->
					<?php if($sus!=''){?>
					<!--  start message-green -->
					<div id="message-green">
						<table border="0" width="100%" cellpadding="0" cellspacing="0">
						<tr>
							<td class="green-left"><?php echo $sus; ?></td>
							<td class="green-right"><a class="close-green"><img src="images/icon_close_green.gif"   alt="" /></a></td>
						</tr>
						</table>
					</div>
					<?php } ?>
					<!--  end message-green -->
					
			<!-- start id-form -->
		<form name="frmPre" id="frmPre" method="post" action="add_taluka.php" enctype="multipart/form-data" >
			<table border="0" cellpadding="0" cellspacing="0"  id="id-form">
			<tr>
				<th valign="top">Select District:</th>
				<td>
				<select name="city_id" class="required styledselect_form_3">
					<option value="">Please Select</option>
					<?php $cityList = $_objAdmin->_getSelectList2('city','city_id,city_name',''," city_name!='' AND status!='D' ORDER BY city_name"); 
						if(is_array($cityList)){
						 foreach($cityList as $value):?>
						 	<option value="<?php echo $value->city_id;?>" <?php if($value->city_id==$auRec[0]->city_id) echo "selected";?> ><?php echo $value->city_name;?></option>
					<?php endforeach; }?>
				</select>
				</td>
				<td></td>
			</tr>
			<tr>
				<th valign="top">Taluka:</th>
				<td><input type="text" name="taluka_name" id="taluka_name" class="required" value="<?php echo $auRec[0]->taluka_name; ?>" maxlength="100" /></td>
				<td></td>
			</tr>
			<tr>
				<th valign="top">Taluka Code:</th>
				<td><input type="text" name="taluka_code" id="taluka_code" class="required" value="<?php echo $auRec[0]->taluka_code; ?>" maxlength="100" /></td>
				<td></td>
			</tr>
			
			<tr>
				<th valign="top">Status:</th>
				<td>
				<select name="status" id="status" class="styledselect_form_3">
				<option value="A" <?php if($auRec[0]->status=='A') echo "selected";?> >Active</option>
				<option value="I" <?php if($auRec[0]->status=='I') echo "selected";?> >Inactive</option>
				</select>
				</td>
				<td></td>
			</tr>
			<tr>
				<th>&nbsp;</th>
				<td valign="top">
					<input name="id" type="hidden" value="<?php echo $auRec[0]->taluka_id; ?>" />
					<input name="account_id" type="hidden" value="<?php echo $_SESSION['accountId'] ?>" />
					<input name="add" type="hidden" value="yes" />
					<input type="reset" value="Reset!" class="form-reset">
					<input type="button" value="Back" class="form-reset" onclick="location.href='taluka.php';" />
					<input name="submit" class="form-submit" type="submit" id="submit" value="Save" />
				</td>
			</tr>
			</table>
		</form>
			<!-- end id-form  -->
			</td>
				</tr>
				</table>
			</div>
			</td>
		</tr>
	</table>
	<div class="clear"></div>
	</div>
<!-- end related-act-bottom -->
<div class="clear"></div>
</div>
<div class="clear"></div>
<!-- start footer --> 
<?php include("footer.php") ?>  
<!-- end footer -->
</body>
</html>
