<?php
include("includes/config.inc.php");
include("includes/function.php");
include("includes/globalarraylist.php");

$page_name="Add Target";


if(isset($_POST['add']) && $_POST['add'] == 'yes')
{
	//print_r($_REQUEST);
		$incen_id=$_objAdmin->addIncentive();
		//echo $duration_id;
			if($incen_id!=''){
				if($_POST['reward']==2){
					$slab_id=$_objAdmin->addIncentiveSlab($incen_id);	
				}
				if($_POST['party_type']==2){
					$st_id=$_objAdmin->addIncentiveState($incen_id);	
				}
				if($_POST['party_type']==3){
					$cy_id=$_objAdmin->addIncentiveCity($incen_id);
				}	
				if($_POST['type']==1){
					$cat_id=$_objAdmin->addIncentiveCat($incen_id);
				}
				if($_POST['type']==14){
					$seg_id=$_objAdmin->addIncentiveSeg($incen_id);
				}	
				if($_POST['type']==2 || $_POST['type']==9){
					$item_id=$_objAdmin->addIncentiveItem($incen_id);
				}			
				if($_POST['duration']==1 || $_POST['duration']==2 || $_POST['duration']==4 || $_POST['duration']==5|| $_POST['duration']==6 || $_POST['duration']==7){
					$duration_id=$_objAdmin->addIncentiveDuration($incen_id);
				}
				$aQua=$_objAdmin->_getSelectList2('table_temp_incentive',"id",''," guid='".$_SESSION['Guid']."'");
					if(is_array($aQua)){
						$qualifire_id=$_objAdmin->addIncentiveQualifire($incen_id);
					}
				/*if($duration_id!=''){
					$sal_id=$_objAdmin->addIncentiveSalesman($duration_id);
				}*/
				
				$sus="Target has been added successfully.";
			}
		
}

$_objAdmin->mysql_query("delete from table_temp_incentive where guid='".$_SESSION['Guid']."'");
$_objAdmin->mysql_query("delete from table_temp_incentive_type where guid='".$_SESSION['Guid']."'");
function NewGuid() { 
    $s = strtoupper(md5(uniqid(rand(),true))); 
    $guidText = 
        substr($s,0,8) .
        substr($s,8,4) . 
        substr($s,12,4). 
        substr($s,16,4). 
        substr($s,20); 
    return $guidText;
}
$Guid = NewGuid();
$_SESSION['Guid']=$Guid;	
?>

<?php include("header.inc.php") ?>
<link rel="stylesheet" href="./base/jquery.ui.all.css">
<script src="./Autocomplete/jquery-1.5.1.js"></script>
<script src="./Autocomplete/jquery.ui.core.js"></script>
<script src="./Autocomplete/jquery.ui.widget.js"></script>
<script src="./Autocomplete/jquery.ui.button.js"></script>
<script src="./Autocomplete/jquery.ui.position.js"></script>
<script src="./Autocomplete/jquery.ui.menu.js"></script>
<script src="./Autocomplete/jquery.ui.autocomplete.js"></script>
<script src="./Autocomplete/jquery.ui.tooltip.js"></script>
<script src="./Autocomplete/jquery.ui.datepicker.js"></script>
<link rel="stylesheet" href="./base/demos.css">
<script type="text/javascript" src="./javascripts/validate.js"></script>
<script src="./javascripts/incentive.js"></script>
<script type="text/javascript">
function durationcheck(fieldname) {
var from = $('#from').val();
if(! from){
		alert('Please Select Start Date');
		return false;
	}
var duration = $('#duration').val();
var day = $('#day_list').val();
var week = $('#week_list').val();
var month = $('#month_list').val();
var qty = $('#quarterly_list').val();
var half = $('#halfyearly_list').val();
var year = $('#yearly_list').val();
//alert(duration);
	$.ajax({
		'type': 'POST',
		'url': 'date_calculator.php',
		'data': 'date='+from+'&duration='+duration+'&day='+day+'&week='+week+'&month='+month+'&qty='+qty+'&half='+half+'&year='+year,
		'success' : function(mystring) {
		//document.getElementById("output").innerHTML = mystring;
		var w =  mystring;
		document.getElementById("end_date").value = w;
		//alert(mystring);
		}
	});
}
</script>
<script type="text/javascript">
// Popup window code
function newPopup(fieldname) {
	var type = $('#type').val();
	//alert(type);
	popupWindow = window.open(
		'qualifire.php?id='+type,'popUpWindow','height=300,width=700,left=250,top=225,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no,status=yes')
}
</script>
 <script type="text/javascript">
function checkall(el){
	var ip = document.getElementsByTagName('input'), i = ip.length - 1;
	for (i; i > -1; --i){
		if(ip[i].type && ip[i].type.toLowerCase() === 'checkbox'){			 			
				ip[i].checked = el.checked;	
			
		}
	}
}
</script>
<script>
    $(function() {
        $( "#from" ).datepicker({
			dateFormat: "d M yy",
            defaultDate: "+1w",
            changeMonth: true,
            numberOfMonths: 3,
            onSelect: function( selectedDate ) {
                $( "#to" ).datepicker( "option", "minDate", selectedDate );
            }
        });
        $( "#to" ).datepicker({
			dateFormat: "d M yy",
            defaultDate: "+1w",
            changeMonth: true,
            numberOfMonths: 3,
            onSelect: function( selectedDate ) {
                $( "#from" ).datepicker( "option", "maxDate", selectedDate );
            }
        });
    });

</script>
	<style>
	.ui-button {
		margin-left: -1px;
	}
	.ui-button-icon-only .ui-button-text {
		padding: 0.35em;
	} 
	.ui-autocomplete-input {
		margin: 0;
		padding: 0.4em 0 0.4em 0.45em;
	}
	</style>
<?php include("incentive_list.php") ?>
<script type="text/javascript">
$(document).ready(function() {
	$("#category_list").attr('disabled', 'disabled');
	$("#segment_list").attr('disabled', 'disabled');
	$("#item_list").attr('disabled', 'disabled');
	$("#item_list1").attr('disabled', 'disabled');
	$("#focus_item_list").attr('disabled', 'disabled');
	$("#state_list").attr('disabled', 'disabled');
	$("#city_list").attr('disabled', 'disabled');
	$("#day_list").attr('disabled', 'disabled');
	$("#week_list").attr('disabled', 'disabled');
	$("#month_list").attr('disabled', 'disabled');
	$("#quarterly_list").attr('disabled', 'disabled');
	$("#halfyearly_list").attr('disabled', 'disabled');
	$("#yearly_list").attr('disabled', 'disabled');
	$("#retailer_placed").attr('disabled', 'disabled');
	$("#average_amount").attr('disabled', 'disabled');
	$("#slab_description").attr('disabled', 'disabled');
	$("#percentage_of_criteria").attr('disabled', 'disabled');
	$("#percentage_of_reward_amount").attr('disabled', 'disabled');
});

</script>
<script type="text/javascript">
window.setInterval(function(){
showQualifire()
{
//alert(str);
//alert(id);
	$.ajax({
		'type': 'POST',
		'url': 'qualifire_list.php',
		'success' : function(mystring) {
			// alert(mystring);
		document.getElementById("QuaList").innerHTML = mystring;
		}
	});
}
}, 1000);
//window.onload = showQualifire();
function showQualifire()
{
//alert(str);
//alert(id);
	$.ajax({
		'type': 'POST',
		'url': 'qualifire_list.php',
		'success' : function(mystring) {
			//alert(mystring);
		document.getElementById("QuaList").innerHTML = mystring;
		}
	});
} 
</script>
<script type="text/javascript">
function deleteQualifire(id)
{
//alert(str);
//alert(id);
var result = confirm("Are you sure, you want to delete selected Qualifier?");
	if (result==true) {
		$.ajax({
			'type': 'POST',
			'url': 'qualifire_list.php',
			'data': 'id='+id,
			'success' : function(mystring) {
				//alert(mystring);
			//document.getElementById("disList").innerHTML = mystring;
			}
		});
	}
}
</script>
<script language="javascript">

        function addRow(tableID,flag,value,qty,dqty,i,id) {
 			//alert(id);
			
            var table = document.getElementById(tableID);
			if (i==1 && flag==2)
 				table.deleteRow(0);
            var rowCount = table.rows.length;
            var row = table.insertRow(rowCount);
 			row.id = rowCount + 1;
            var colCount = table.rows[0].cells.length;
 
            for(var i=0; i<colCount; i++) {
 				
                var newcell = row.insertCell(i);
 				
                newcell.innerHTML = table.rows[0].cells[i].innerHTML;
				//alert(newcell.childNodes);
                switch(newcell.childNodes[0].type) {
					case "label":
							newcell.childNodes[0].innerHTML = "4";
                            break;
					case "text":
					//alert(i);
							if (flag == 2)
							{
								if (i == 1)
									newcell.childNodes[0].value = qty;
								else if (i ==2)
									newcell.childNodes[0].value = dqty;
								else if (i ==3)
									newcell.childNodes[0].value = id;
							}
							else
							{ 	newcell.childNodes[0].value = ""; }
                            break;
                    case "checkbox":
                            newcell.childNodes[0].checked = false;
                            break;
                    case "select-one":
							if (flag == 2)
								newcell.childNodes[0].value = value;
							else
                            	newcell.childNodes[0].selectedIndex = 0;
                            break;
					case "button":
							
                            newcell.onclick = function() {
								table.deleteRow(row); }
                            break;
					
                }
            }
        }
 
        function deleteRow(tableID) {
			//alert(tableID);
            try {
            var table = document.getElementById(tableID);
            var rowCount = table.rows.length;
 
            for(var i=0; i<rowCount; i++) {
                var row = table.rows[i];
                var chkbox = row.cells[0].childNodes[0];
                if(null != chkbox && true == chkbox.checked) {
                    if(rowCount <= 1) {
                        alert("Cannot delete all the rows.");
                        break;
                    }
                    table.deleteRow(i);
                    rowCount--;
                    i--;
                }
 
            }
            }catch(e) {
                alert(e);
            }
        }
 
</script>
<script>
$(document).ready(function() {
$("#showRewordAmount").show();


	var $rows = $('.srchSal tr');
	$('#srch_sal').live('input',function() {
	    var val = $.trim($(this).val()).replace(/ +/g, ' ').toLowerCase();

	    $rows.show().filter(function() {
	        var text = $(this).text().replace(/\s+/g, ' ').toLowerCase();
	        return !~text.indexOf(val);
	    }).hide();
	});

});
function rewardtype(value)
{
	if(value==1)
	{
		$("#showRewordAmount").show();
		$("#showRewordSlab").hide();
		$("#showAvgAmount").hide();
		$("#average_amount").attr('disabled', 'disabled');
		$("#slab_description").attr('disabled', 'disabled');
		$("#percentage_of_criteria").attr('disabled', 'disabled');
		$("#percentage_of_reward_amount").attr('disabled', 'disabled');
	}
	else 
	{
		$("#showRewordAmount").hide();
		$("#showRewordSlab").show();
		$("#showAvgAmount").show();
		$("#average_amount").removeAttr('disabled', 'disabled');
		$("#slab_description").removeAttr('disabled', 'disabled');
		$("#percentage_of_criteria").removeAttr('disabled', 'disabled');
		$("#percentage_of_reward_amount").removeAttr('disabled', 'disabled');
	}
}

function getMonthThisYear(selectedYear,currYear,currMonth){
 //alert("selected Year ="+selectedYear+" Current Year ="+currYear+" Current Month ="+currMonth);
 	var months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
 	var html = '<option value="">Select Month</option>';

 		if(selectedYear==currYear){
 			for(var i=currMonth;i<=months.length;i++){ 
 				html += '<option value="'+i+'">'+months[i-1]+'</option>';
 			}

 		}else{
 			for(var i=1;i<=months.length;i++){ 
 				html += '<option value="'+i+'">'+months[i-1]+'</option>';
 			}
 		} 		
 		//console.log(html);

 		$("#month").html(html);
 		
}
</script>
<div class="clear"></div>
<!-- start content-outer -->
<div id="content-outer">
<!-- start content -->
	<div id="content">
	<div id="page-heading"><h1><span style="color: #d74343; font-family: Tahoma; font-weight: bold;">Add Target</span></h1></div>
		<table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">
		<tr>
			<th rowspan="3" class="sized"><img src="images/side_shadowleft.jpg" width="20" height="300" alt="" /></th>
			<th class="topleft"></th>
			<td id="tbl-border-top">&nbsp;</td>
			<th class="topright"></th>
			<th rowspan="3" class="sized"><img src="images/side_shadowright.jpg" width="20" height="300" alt="" /></th>
		</tr>
		<tr>
			<!--<td id="tbl-border-left"></td>-->
			<td>
			<!--  start content-table-inner -->
			<div id="content-table-inner">
				<table border="0" width="100%" cellpadding="0" cellspacing="0">
				<tr valign="top">
					<td>
					<!--  start message-red -->
					<?php if($err!=''){?>
					<div id="message-red">
						<table border="0" width="100%" cellpadding="0" cellspacing="0">
						<tr>
							<td class="red-left"> <?php echo $err; ?></td>
							<td class="red-right"><a class="close-red"><img src="images/icon_close_red.gif"   alt="" /></a></td>
						</tr>
						</table>
					</div>
					<?php } ?>
					<!--  end message-red -->
					<?php if($sus!=''){?>
					<!--  start message-green -->
					<div id="message-green">
						<table border="0" width="100%" cellpadding="0" cellspacing="0">
						<tr>
							<td class="green-left"><?php echo $sus; ?></td>
							<td class="green-right"><a class="close-green"><img src="images/icon_close_green.gif"   alt="" /></a></td>
						</tr>
						</table>
					</div>
					<?php } ?>
					<!--  end message-green -->
					
			<!-- start id-form -->
		<form name="frmPre" id="frmPre" method="post" action="<?php $_SERVER['PHP_SELF'] ?>" enctype="multipart/form-data" >
			<table border="0" cellpadding="0" cellspacing="0" width="800" id="id-form">
			<tr>
				<th valign="top" >Target Description:</th>
				<td width="200px"><input type="text" name="description" id="description"  class="required" value="" maxlength="255"/></td>
				<td width="500px"></td>
			</tr>
			<!-- <tr>
				<th valign="top">Target Start Date:</th>
				<td><input type="text" name="from" id="from" class="date required"  onclick="removenumber()" readonly  /></td>
				<td></td>
			</tr>
			<tr>
				<th valign="top" >Target Duration:</th>
				<td >
					<select name="duration" id="duration" class="required styledselect_form_4" onchange="shownumber(this.value);" >
						<option value="">Select</option>
						<?php 
					$auDur=$_objAdmin->_getSelectList2('table_duration',"dur_id,dur_description",''," status='A' ORDER BY dur_id");
					for($i=0;$i<count($auDur);$i++){
				?>
					<option value="<?php echo $auDur[$i]->dur_id;?>"><?php echo $auDur[$i]->dur_description;?></option>
				<?php } ?>
					</select>
				</td>
				<td ></td>
			</tr> -->
				<tr>
					<th valign="top">Target Year:</th>
					<td>
				        <select name="year" id="year" class="required styledselect_form_5 required" onchange="getMonthThisYear(this.value,<?php echo date('Y');?>,<?php echo date('n');?>);">
				         <option value="">Select Year:</option>
				         <?php 
				           for($i=0;$i<2;$i++){ 
				           	$year = date('Y', strtotime('+'.$i.' years'));
				           	?>
				           	<option value="<?php echo $year;?>"><?php echo $year;?></option>
				         <?php  }
				          ?>
				        </select>
				    </td>
			    </tr> 
     
				<tr> 
					<th valign="top">Target Month:</th>
					<td>
					<select name="month" id="month" class="styledselect_form_3 required">
					     <option value="">Select Month</option>
					</select>
					</td>
				</tr>
			<tr id="dayshow" style="display:none;">
				<th valign="top" >No of Days to Run:</th>
				<td><input type="text" name="day_list" id="day_list"  class="required numberDE" value="" onkeyup="durationcheck()" maxlength="3"/></td>
				<td></td>
			</tr>
			<tr id="weekshow" style="display:none;">
				<th valign="top" >No of Weeks to Run:</th>
				<td><input type="text" name="week_list" id="week_list"  class="required numberDE" value="" onkeyup="durationcheck()" maxlength="3"/></td>
				<td></td>
			</tr>
			<tr id="monthshow" style="display:none;">
				<th valign="top" >No of Months to Run:</th>
				<td><input type="text" name="month_list" id="month_list"  class="required numberDE" value="" onkeyup="durationcheck()" maxlength="3"/></td>
				<td></td>
			</tr>
			<tr id="quarterlyshow" style="display:none;">
				<th valign="top" >No of Quarter to Run:</th>
				<td><input type="text" name="quarterly_list" id="quarterly_list"  class="required numberDE" value="" onkeyup="durationcheck()" maxlength="3"/></td>
				<td></td>
			</tr>
			<tr id="halfyearlyshow" style="display:none;">
				<th valign="top" >No of Half Year to Run:</th>
				<td><input type="text" name="halfyearly_list" id="halfyearly_list"  class="required numberDE" value="" onkeyup="durationcheck()" maxlength="2"/></td>
				<td></td>
			</tr>
			<tr id="yearlyshow" style="display:none;">
				<th valign="top" >No of Year to Run:</th>
				<td><input type="text" name="yearly_list" id="yearly_list"  class="required numberDE" value="" onkeyup="durationcheck()" maxlength="2"/></td>
				<td></td>
			</tr>
			<!-- <tr>
				<th valign="top">Target End Date:</th>
				<td><input type="text" name="end_date" id="end_date"  class="text" readonly /></td>
				<td></td>
			</tr> -->
			
			<tr>
				<th valign="top">Salesman:</th>
				<td>
					<div style="width:200px; height:200px;overflow:auto;" >
					<table class="srchSal" border="0" cellpadding="0" cellspacing="0"  id="id-form">

					<input class="text srch_sal" type="text" style="width: 179px; margin-bottom: 10px;" id="srch_sal" placeholder="Search Salesman">				
					
					<tr>
						<th valign="top"></th>
					</tr>
					
					<input type="checkbox" name="" onclick="checkall(this);" /> All Salesman
					<?php
					$auCol=$_objAdmin->_getSelectList('table_salesman AS s',"salesman_id,salesman_name",''," status='A' $salesman ORDER BY  salesman_name");
					for($i=0;$i<count($auCol);$i++){
					?>
					<tr>
						<td><input type="checkbox" name="salesman_id[]" value="<?php echo $auCol[$i]->salesman_id;?>" /> <?php echo $auCol[$i]->salesman_name;?>
						</td>
					</tr>
					<?php } ?>
					</table>
					</div>
				</td>
				<td></td>
			</tr>
			
			<tr>
				<th valign="top">Target Type:</th>
				<td>
				<select name="type" id="type" class="required styledselect_form_4" onchange="inctype(this.value);">
					<option value="">Select</option>
					<?php 
					$auInc=$_objAdmin->_getSelectList2('table_type',"type_id,type_description",''," status='A' ORDER BY type_id");
					for($i=0;$i<count($auInc);$i++){
				?>
					<option value="<?php echo $auInc[$i]->type_id;?>"><?php echo $auInc[$i]->type_description;?></option>
				<?php } ?>
				</select>	
				</td>
				<td></td>
			</tr>
			<tr id="showcat" style="display:none;">
				<th valign="top">Category Code:</th>
				<td>
				<input id="category_list" style="width: 500px; height: 15px;" class="ui-autocomplete-input required" autocomplete="off" role="textbox" aria-autocomplete="list" aria-haspopup="true" name="category_list" placeholder="Search Category Code" />
				</td>
				<td></td>
			</tr>

			<!-- <tr id="showsegment" style="display:none;">
				<th valign="top">Segment Code:</th>
				<td>
				<input id="segment_list" style="width: 500px; height: 15px;" class="ui-autocomplete-input required" autocomplete="off" role="textbox" aria-autocomplete="list" aria-haspopup="true" name="segment_list" placeholder="Search Segment Name" />
				</td>
				<td></td>
			</tr> -->

			<tr>
				<th valign="top">Segment:</th>
				<td>
				<select name="segment_list" id="segment_list" class="required styledselect_form_4">
					<option value="">Select</option>
					<?php 
					$auInc=$_objAdmin->_getSelectList2('table_segment',"segment_id,segment_name",''," status='A' ORDER BY segment_name");
					for($i=0;$i<count($auInc);$i++){
				?>
					<option value="<?php echo $auInc[$i]->segment_name;?>"><?php echo $auInc[$i]->segment_name;?></option>
				<?php } ?>
				</select>	
				</td>
				<td></td>
			</tr>


			<tr id="showItem" style="display:none;">
				<th valign="top">Item Code:</th>
				<td>
				<input id="item_list" style="width: 500px; height: 15px;" class="ui-autocomplete-input required" autocomplete="off" role="textbox" aria-autocomplete="list" aria-haspopup="true" name="item_list" placeholder="Search Item Code" />
				</td>
				<td></td>
			</tr>
			<tr id="showItem1" style="display:none;">
				<th valign="top">Item Code:</th>
				<td>
				<input id="item_list1" style="width: 500px; height: 15px;" class="ui-autocomplete-input required" autocomplete="off" role="textbox" aria-autocomplete="list" aria-haspopup="true" name="item_list1" placeholder="Search Item Code" />
				</td>
				<td></td>
			</tr>
			<tr id="showFocusItem" style="display:none;">
				<th valign="top">Item Code:</th>
				<td>
				<input id="focus_item_list" style="width: 500px; height: 15px;" class="ui-autocomplete-input required" autocomplete="off" role="textbox" aria-autocomplete="list" aria-haspopup="true" name="focus_item_list" placeholder="Search Focus Item Code" />
				</td>
				<td></td>
			</tr>
			<tr id="showValue" style="display:none;">
				<th id="showCategoryNumber" style="display:none;" valign="top" >Minimum Quantity:</th>
				<th id="showSegmentHeading" style="display:none;" valign="top" >Minimum Quantity:</th>
				<th id="showItemDet1" style="display:none;" valign="top" >Minimum Quantity:</th>
				<th id="showItemDet2" style="display:none;" valign="top" >Number OF Item:</th>
				<th id="showItemNumber" style="display:none;" valign="top">Minimum Items:</th>
				<th valign="top" id="showOrderNumber" style="display:none;">Minimum Orders:</th>
				<th valign="top" id="showOrderValue" style="display:none;">Minimum Order Value:</th>
				<th id="showSchemeNumber" style="display:none;" valign="top">Minimum Schemes:</th>
				<th id="showRetailerNumber" style="display:none;"  valign="top">Minimum Retailers:</th>					
				<th id="showRetailerAdded" style="display:none;"  valign="top">Retailers To Be Added:</th>
				<th id="showFocusRetailerItemNumber" style="display:none;" valign="top">Minimum Quantity:</th>
				<th id="showTotalCall" style="display:none;" valign="top">Minimum Total Call:</th>
				<th id="showTotalAmount" style="display:none;" valign="top">Minimum Amount:</th>
				<td id="showPrimaryVal" style="display:none;"><input type="text" name="value" id="value"  value="" class="required numberDE"/></td>
				<td></td>
			</tr>
			<tr id="showSecondaryValuerow" style="display:none;"> 
				<th id="showSecondaryValueRetailer" valign="top" style="display:none;">Minimum Retailers:</th>
				<th id="showSecondaryValueOrder" valign="top" style="display:none;" >Minimum Order:</th>
				<th id="showSecondaryValueProductive" valign="top" style="display:none;" >% of Productive Calls:</th>
				<td>
				<input type="text" id="secondary_value" class="required numberDE" name="secondary_value" />
				</td>
				<td></td>
			</tr>
			<tr id="showRewordType" style="display:none;">
				<th valign="top" ></th>
				<td colspan="2">
				<input type="radio" name="reward" value="1" checked="checked" onchange="rewardtype(this.value);"> Reward In Amount 
				<input type="radio" name="reward" value="2" onchange="rewardtype(this.value);"> Reward In Slab
				</td>
			</tr>
			<!-- <tr id="showRewordAmount" style="display:none;">
				<th valign="top" >Reward Amount:</th>
				<td><input type="text" name="reward_amount" id="reward_amount" class="text numberDE" value="" /></td>
				<td></td>
			</tr> -->
			<tr id="showAvgAmount" style="display:none;">
				<th valign="top" >Average Amount:</th>
				<td><input type="text" name="average_amount" id="average_amount" class="required numberDE" value="" /></td>
				<td></td>
			</tr>
			<tr id="showRewordSlab" style="display:none;">
				<th valign="top" >Reward Slab:</th>
				<td colspan="2">
					<table   border="0"  >
						<tr >
							<td><div style="font-weight:bold; width:210px" >Description</div></td>
							<td><div style="font-weight:bold; width:120px" >Criteria</div></td>
							<td><div style="font-weight:bold;">Incentive Amount on Turn Over</div></td>
							
						</tr>
						<tr >
							<td colspan="6">
								<table id="dataTableQuestion" cellspacing="10" cellpadding="10" border="0">
									<tr >
										<td><div style="font-weight:bold; width:235px" ><input name="slab_description[]" class="required" type="text" id="slab_description" value="" maxlength="250" style="width:200px"/></div></td>
										<td><div style="font-weight:bold; width:145px" ><input name="percentage_of_criteria[]" class="required" type="text" id="percentage_of_criteria" value="" maxlength="3" style="width:25px"/><b> &#37; &amp; Above</b></div></td>
										<td ><div style="font-weight:bold; width:220px"  ><input name="percentage_of_reward_amount[]" class="required" type="text" id="percentage_of_reward_amount" value="" maxlength="5" style="width:25px"/><span style="padding-bottom:5px; border:1px;"><b> &#37;</b></span></div></td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td colspan="3"><a href="#" onclick="addRow('dataTableQuestion','','')"><input name="add_next" class="form-submit" type="button" id="add_next" value="Add More" /></a></td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<th valign="top">Party Type:</th>
				<td valign="top">
				<select name="party_type" id="party_type" class="styledselect_form_3" onchange="partype(this.value);">
					<?php 
					$auPartyType=$_objAdmin->_getSelectList2('table_party_type',"party_type_id,party_description",''," status='A' ORDER BY party_type_id");
					for($i=0;$i<count($auPartyType);$i++){
				?>
					<option value="<?php echo $auPartyType[$i]->party_type_id;?>"><?php echo $auPartyType[$i]->party_description;?></option>
				<?php } ?>
				</select>	
				</td>
				<td></td>
			</tr>
			<tr id="satshow" style="display:none;" >
				<th valign="top">State Name:</th>
				<td>
				<input id="state_list" style="width: 500px; height: 15px;" class="ui-autocomplete-input required" autocomplete="off" role="textbox" aria-autocomplete="list" aria-haspopup="true" name="state_list" placeholder="Search State Name" />
				</td>
				<td></td>
			</tr>
			<tr id="cityshow" style="display:none;" >
				<th valign="top">City Name:</th>
				<td>
				<input id="city_list" style="width: 500px; height: 15px;" class="ui-autocomplete-input required" autocomplete="off" role="textbox" aria-autocomplete="list" aria-haspopup="true" name="city_list" placeholder="Search City Name" />
				</td>
				<td></td>
			</tr>
			<tr>
				<th valign="top">Target Status:</th>
				<td>
				<select name="status" id="status" class="styledselect_form_3">
					<option value="A">Active</option>
					<option value="I">Inactive</option>
				</select>
				</td>
				<td></td>
			</tr>
			<tr>
			<td id="QuaList" colspan="3" >
			</td>
			<!--<div id="disList" ></div>-->
			</tr>
			<tr id="addquali" >
				<th>&nbsp;</th>
				<td><input type="button" onclick="return newPopup()" value="Add Qualifier" class="form-reset"  /> <!--<input type="button" onclick="showQualifire()" value="test" class="form-reset"  />--></td>
			</tr>
				
			<tr>
				<th>&nbsp;</th>
				<td valign="top">
					<input type="hidden" name="target_incentive_type" value="1"  />
					<input type="hidden" name="qualifiers_type" value="1"  />
					<input type="hidden" name="duration" value="2"  />
					<input name="add" type="hidden" value="yes" />
					<input type="reset" value="Reset!" class="form-reset">
					<input type="button" value="Back" class="form-reset" onclick="location.href='target.php';" />
					<input name="submit" class="form-submit" type="submit" id="submit" value="Save"  />
				</td>
			</tr>
			</table>
		</form>
			<!-- end id-form  -->
			</td>
			<td>
			<!-- right bar-->
			<?php //include("rightbar/item_bar.php") ?>
			<div class="clear"></div>
		</div>
<!-- end related-act-bottom -->
</div>
<!-- end related-activities -->
</td>
</tr>
<tr>
<td><img src="images/blank.gif" width="695" height="1" alt="blank" /></td>
<td></td>
</tr>
</table>
<div class="clear"></div>
</div>
<!--  end content-table-inner  -->
</td>
<td id="tbl-border-right"></td>
</tr>
<!--<tr>
	<th class="sized bottomleft"></th>
	<td id="tbl-border-bottom">&nbsp;</td>
	<th class="sized bottomright"></th>
</tr>-->
</table>
<div class="clear">&nbsp;</div>

</div>
<!--  end content -->
<div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->
<div class="clear">&nbsp;</div>
    
<!-- start footer --> 
<?php include("footer.php") ?>  
<!-- end footer -->
</body>
</html>