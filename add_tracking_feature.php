<?php
include("includes/config.inc.php");
include("includes/function.php");
//echo $_SESSION['navId'];
$page_name="Manage Navigation";
$_objAdmin = new Admin();
$_objItem = new Item();
$_objArrayList = new ArrayList();

if($_GET['id']=='')
{
header('location:manage_account.php');
}
//Feature access list.
if(isset($_POST['submit']) && $_POST['submit']!='')
{
$condition="account_id='".$_POST['nav_preID']."' and status='A'";
$tablefeature=$_objAdmin->_getSelectList2('table_location_feature','*','',"$condition");
if(count($tablefeature)>0)
{
//Update Location Tracking.
if($_POST['tracking_type_location']=='')
{
$condition_location="account_id='".$_POST['nav_preID']."' and status='A' and tracking_type='L'"; 
$_objAdmin->mysql_query("delete from table_location_feature where $condition_location");	
}
if($_POST['tracking_type']=='')
{
$condition_distance="account_id='".$_POST['nav_preID']."' and status='A' and tracking_type='D'"; 
$_objAdmin->mysql_query("delete from table_location_feature where $condition_distance");	
}
$update_id=$_objAdmin->updateFeatureTracking($_POST['nav_preID']);

if($update_id!="")
{

header('location:add_tracking_feature.php?id='.$update_id);
$_SESSION['addsuss']="Update location tracking successfully";
exit;
}

}
else
{

$insert_id=$_objAdmin->addfeatureTracking();
if($insert_id!=""){ 

header('location:add_tracking_feature.php?id='.$insert_id);
$_SESSION['addsuss']="Add location tracking successfully";
}	
//header('location:add_tracking_feature.php?id='.$_REQUEST['id']);
}
	
}

// For next steps
if(isset($_POST['next']) && $_POST['next']!="")
{
header('location:account_navigation.php?id='.$_POST['nav_preID']);
exit;
}
// Check feature Enable or disable.

$getLocation=$_objAdmin->_getSelectList2('table_location_feature',"*",''," account_id='".$_REQUEST['id']."'");
//print_r($getLocation);
$value_of_location=count($getLocation);
$accountID=$getLocation[0]->account_id;
for($i=0;$i<count($getLocation);$i++)
{

if($getLocation[$i]->tracking_type=='L')
{
$location_is_checked=$getLocation[$i]->tracking_type;
// Tracking Based on
$location_based_on_tracking=$getLocation[$i]->tracking_based_on;
$getHour = explode(":",$getLocation[$i]->start_time);
$getHourLast=explode(":",$getLocation[$i]->end_time);
$location_interval=$getLocation[$i]->interval_val;
}

// For distance calculation.
if($getLocation[$i]->tracking_type=='D')
{
$distance_is_checked=$getLocation[$i]->tracking_type;
// Tracking based on.
$distance_based_on_tracking=$getLocation[$i]->tracking_based_on;

$dis_getHour = explode(":",$getLocation[$i]->start_time);
$dis_getHourLast=explode(":",$getLocation[$i]->end_time);
$distance_interval=$getLocation[$i]->interval_val;
}

}

include("header.inc.php");
?>

<style>
.parentfeature{
	height:20px;
 	background: #999999; 
	padding:5px;
	border:1px solid #000; 
	border-radius:5px; 
	-moz-border-radius:5px;
	-o-border-radius:5px;
	-ms-border-radius:5px;
}
.childfeature{
	height:20px;
 	background: #c3c3c3; 
	padding:5px;
	border:1px solid #000; 
	border-radius:5px; 
	-moz-border-radius:5px;
	-o-border-radius:5px;
	-ms-border-radius:5px;
 }
</style>
<script>



function validation()
  {

  var check=$('#location').is(':checked');
  var checkdis=$('#distance_calc').is(':checked');
  if(check==false && checkdis==false)
  {
  //alert('aaaa');
  document.getElementById('locat').innerHTML="Please Select Checkbox";
  return false;
  }
  else
  {
  //alert('bbbb');
  document.getElementById('locat').innerHTML='';
  }
  
  
 if(check==true)
   {
   var location = $('input:radio[name=trackingbasedon]:checked').val();
   
//var location=$('input[type="radio"]:checked').val();
  
 if(location=='Time')
 {
var start_hour=$('#start_hour').val();
var start_minutes=$('#start_minutes').val();
var end_hour=$('#end_hour').val();
var end_minuets=$('#end_minuets').val();
if(start_hour=="")
 {
$('#start_hour').css('border-color', 'red');
return false;
 }
else
{
$('#start_hour').css('border-color', 'green');
}
 if(start_minutes=="")
 {
$('#start_minutes').css('border-color','red'); 
return false;
 }
 else
 {
 $('#start_minutes').css('border-color','green'); 
 }
 if(end_hour=="")
 {
 $('#end_hour').css('border-color','red');
 return false;
 }
 else
 {
 $('#end_hour').css('border-color','green');
 }
 if(end_minuets=="")
 {
 $('#end_minuets').css('border-color','red');
 return false;
 }
 else
 {
 $('#end_minuets').css('border-color','green');
 }
 
// alert(start_hour);
 //alert(end_hour);
 //alert(start_minutes);
 //alert(end_minuets);
 if(start_hour>=end_hour ||  start_hour>=end_hour && start_minutes>end_minuets)
 {
 document.getElementById('time_greater').innerHTML="Ends time should be greater than  start time.";
 return false;
 }
 else
 {
 document.getElementById('time_greater').innerHTML="";
 }
 }
 
 // ends time.
 
var intervall=$('#intervall').val();
 if(intervall=="")
 {
 $('#intervall').css('border-color','red');
 return false;
 } 
 else
 {
 $('#intervall').css('border-color','green');
 }
 
 }
 
//if distance calculation is checked and time is checked then following activity has been done.

 if(checkdis==true)
 {
var dis = $('input:radio[name=tracking_based_on]:checked').val();


 if(dis=='Time')
 {
var start_hour=$('#dis_start_hour').val();
var start_minutes=$('#dis_start_minutes').val();
var end_hour=$('#dis_end_hour_distance').val();
var end_minuets=$('#dis_end_minuets').val();
if(start_hour=="")
 {
$('#dis_start_hour').css('border-color', 'red');
 }
else
{
$('#dis_start_hour').css('border-color', 'green');
}
 if(start_minutes=="")
 {
$('#dis_start_minutes').css('border-color','red'); 
 }
 else
 {
 $('#dis_start_minutes').css('border-color','green'); 
 }
 if(end_hour=="")
 {
 $('#dis_end_hour_distance').css('border-color','red');
 }
 else
 {
 $('#dis_end_hour_distance').css('border-color','green');
 }
 if(end_minuets=="")
 {
 $('#dis_end_minuets').css('border-color','red');
 }
 else
 {
 $('#dis_end_minuets').css('border-color','green');
 }
 
 //alert(start_minutes);
 //alert(end_minuets);
 if(start_hour>=end_hour || start_hour>=end_hour && start_minutes>end_minuets)
 {
 document.getElementById('dis_time_greater').innerHTML="Ends time should be greater than  start time.";
 return false;
 }
 else
 {
 document.getElementById('dis_time_greater').innerHTML="";
 }
 }
 
var dis_interval=$('#dis_interval').val();
 if(dis_interval=="")
 {
 $('#dis_interval').css('border-color','red');
 return false;
 } 
 else
 {
 $('#dis_interval').css('border-color','green');
 }
 
 
 
 }

 
 
 
return true;   
  }





$(document).ready(function(){
   var location =$('#location').is(':checked');
   var timeel=$('#timeloc').is(':checked');
   
   if(location==true)
     {
     $('#distance').show();
     }
   /*-----------for attendence show----------------*/
   if(timeel==true)
     {
	 $('#timeeloc').show();
	 }	
	 
    $('#location').change(function(){
	  
        if(this.checked)
		$('#distance').show();
		
		else
        $('#distance').hide();

    });
	
	

	
	$('#attendloc').change(function()
	 {
	if(this.checked)
	{
	$('#attenloc').show();
	$('#timeeloc').hide();
	}
	else
	{
	$('#attenloc').hide();
	}
	});
	$('#timeloc').change(function()
	{
	if(this.checked)
	{
	$('#timeeloc').show();
	$('#attenloc').hide();
	}
	else
	{
	$('#timeeloc').hide();
	}
	});
	

	 
	
});

$(document).ready(function(){
var dis_location=$('#distance_calc').is(':checked');
var dis_time_attendence=$('#dis_timeloc').is(':checked');  // tr id is 'dis_timeloc' and show tr id is  'dis_timeeloc' one 'e' diffrence is.
  if(dis_location==true)
    {
	$('#dis_calc').show();
	}
  // for time and distance.
  
 if(dis_time_attendence==true)
   {
   $('#dis_timeeloc').show();
   }
 
  
 $('#distance_calc').change(function(){
	   if(this.checked)
		$('#dis_calc').show();
		else
        $('#dis_calc').hide();

    });
	
	
$('#dis_attendloc').change(function()
	 {
	if(this.checked)
	{
	$('#dis_attendloc').show();
	$('#dis_timeeloc').hide();
	}
	else
	{
	$('#dis_attendloc').hide();
	}
	});
	$('#dis_timeloc').change(function()
	{
	if(this.checked)
	{
	$('#dis_timeeloc').show();
	
	}
	else
	{
	$('#dis_timeeloc').hide();
	}
	});	
	
	 
});


function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
}	  
	  		
			
	function backto()
	    {
			window.location.assign("add_company_feature.php?id=<?php echo $_GET['id']; ?>")
        }		
			
		</script>
		
<style type="text/css">
.timelist {
border: solid 1px #BDBDBD;
color: #393939;
font-family: Arial;
font-size: 12px;
border-radius: 5px;
-moz-border-radius: 5px;
-o-border-radius: 5px;
-ms-border-radius: 5px;
-webkit-border-radius: 5px;
width: 70px;
height: 30px;
}
.interval{
border: solid 1px #BDBDBD;
height: 20px;
padding: 6px 6px 0 6px;
width:55px;
border-radius: 5px;
-moz-border-radius: 5px;
-o-border-radius: 5px;
-ms-border-radius: 5px;
-webkit-border-radius: 5px;

}

</style>		

<input name="pagename" type="hidden"  id="pagename" value="editdiscount.php" />
<div id="content-outer">
<!-- start content -->
<div id="content">
<div id="page-heading"><h1><span style="color: #d74343; font-family: Tahoma; font-weight: bold;">Feature Access Management</span></h1></div>
<table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">

<tr>
	<!--<td id="tbl-border-left"></td>-->
	<td>
	<div id="content-table-inner">
	<?php if($_SESSION['addsuss']!=''){?>
	<div id="message-green">
		<table border="0" width="100%" cellpadding="0" cellspacing="0">
		<tr>
			<td class="green-left"><?php echo $_SESSION['addsuss']; unset($_SESSION['addsuss']);  ?></td>
			<td class="green-right"><a class="close-green"><img src="images/icon_close_green.gif"   alt="" /></a></td>
		</tr>
		</table>
	</div>
	<?php } ?>
	
	
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
	<tr valign="top">
	<td>
		<!-- start id-form -->
		<table  border="0" cellspacing="0" cellpadding="0"  align="center" width="100%">
        <tr>
        <td><?php 
		if(isset($message)!=""){ echo $message;}
		?></td>
        </tr>
			<tr>
				<td align="lift" valign="top"  >
				<table id="flex1" style="display:none"></table>
                <div id="step-holder">
					<div class="step-no-off">1</div>
					<div class="step-light-left">Company Features</div>
					<div class="step-light-right">&nbsp;</div>
					<div class="step-no-off">2</div>
					<div class="step-light-left">Page Access</div>
					<div class="step-light-right">&nbsp;</div>
					<div class="step-no-off">3</div>
					<div class="step-light-left">Photo & Feature</div>
					<div class="step-light-right">&nbsp;</div>
                    <div class="step-no">4</div>
					<div class="step-dark-left">Tracking Feature</div>
					<div class="step-dark-right">&nbsp;</div>
					<div class="clear"></div>
					
                    
					<div class="clear"></div>
					</div>
	
	<form name="frmPre" id="frmPre" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>" enctype="multipart/form-data" >
		<table border="0" width="90%" cellpadding="0" cellspacing="0"  id="id-form">
		        <tr>
			  	<td>
				<table border="0" width="100%" cellpadding="0" cellspacing="1">
                <tr>
                 <td  style="font-size:15px; font-weight:bold;">Location Tracking</td>
                </tr>
                <tr>
                <td>Location Tracking &nbsp;
<input type="checkbox" name="tracking_type_location" value="L"  id="location" <?php if($location_is_checked=='L'){ ?> checked="checked" <?php } ?>/>
				<span id="locat" style="color:red;"></span>
				</td>
				</tr>
				<tr><td><table id="distance" style="display:none;">
				<tr><td>Attendence &nbsp;<input type="radio" name="trackingbasedon" value="Attendence" id="attendloc" <?php if($location_based_on_tracking=='Attendence'){ ?> checked="checked" <?php } else{ ?> checked="checked"<?php } ?>  class="attend"  /></td>
				<td>Time &nbsp;<input type="radio" name="trackingbasedon" value="Time"  id="timeloc" <?php if($location_based_on_tracking=='Time'){ ?> checked="checked"<?php } ?> /></td></tr>
				
               
		<tr id="timeeloc" style="display:none;">
		<td>Start time
		<select name="start_hour_loc" id="start_hour" class="timelist">
		<option value="">--Select Hour--</option>
		<?php for($i=0;$i<=23;$i++){ ?>
		<option value="<?php if($i<=9){ echo '0'.$i;} else{ echo $i;} ?>" <?php if($i<=9){ $val='0'.$i;} else{ $val=$i;} if($val==$getHour[0]  ){ echo 'selected';}?>><?php if($i<=9){ echo '0'.$i;} else{ echo $i;} ?></option>
		<?php } ?>
		</select>
		<select name="start_minutes_loc" id="start_minutes" class="timelist">
		<option value="">--Select Minutes--</option>
		<?php for($i=0;$i<=59;$i++){ ?>
		<option value="<?php if($i<=9){ echo '0'.$i;} else{ echo $i;} ?>" <?php if($i<=9){ $val='0'.$i;} else{$val=$i;} if($val==$getHour[1] ){ echo 'selected';} ?> ><?php if($i<=9){ echo '0'.$i;} else{ echo $i;} ?></option>
		<?php } ?>
		</select>
		</td><td>End Time
		<select name="end_hour_loc" id="end_hour" class="timelist">
		<option value="">--Select Hour--</option>
		<?php for($i=0;$i<=23;$i++){ ?>
		<option value="<?php if($i<=9){ echo '0'.$i;} else{ echo $i;} ?>" <?php if($i<=9){ $val='0'.$i;} else{ $val=$i;} if($val==$getHourLast[0] ){ echo 'selected';} ?>><?php if($i<=9){ echo '0'.$i;} else{ echo $i;} ?></option>
		<?php } ?>
		</select>
		<select name="end_minuets_loc" id="end_minuets" class="timelist">
		<option value="">--Select Minutes--</option>
		<?php for($i=0;$i<=59;$i++){ ?>
		<option value="<?php if($i<=9){ echo '0'.$i;} else{ echo $i;} ?>" <?php if($i<=9){$val='0'.$i;} else{$val=$i;} if($val==$getHourLast[1] ){ echo 'selected';} ?>><?php if($i<=9){ echo '0'.$i;} else{ echo $i;} ?></option>
		<?php } ?>
		</select>
		</td>
		<td id="time_greater" style="color:red;"></td>
		</tr>
			   
				<tr>
				<td>Interval &nbsp;<input type="text" name="interval_vall"   onkeypress="return isNumberKey(event);" id="intervall" value="<?php if($location_interval!=""){ echo $location_interval;} else{echo '';} ?>" size="6"  class="interval"/>(In Minutes)</td>
				</tr>
				
					
				</table>
				</td></tr>
				<tr>
				<td>Distance Calculation &nbsp;<input type="checkbox" value="D"  name="tracking_type" id="distance_calc"<?php if($distance_is_checked=='D'){?> checked="checked" <?php } ?> /></td>
				</tr>
	   			<tr><td><table id="dis_calc" style="display:none;">
				<tr><td>Attendence &nbsp;<input type="radio" name="tracking_based_on" value="Attendence" id="dis_attendloc" <?php if($distance_based_on_tracking=='Attendence'){ ?> checked="checked" <?php } else{ ?> checked="checked"<?php }?>  class="attend"  /></td>
				<td>Time &nbsp;<input type="radio" name="tracking_based_on" value="Time"  id="dis_timeloc"  class="attend" <?php if($distance_based_on_tracking=='Time'){ ?> checked="checked"<?php } ?> /></td></tr>
				
               
		<tr id="dis_timeeloc" style="display:none;">
		<td>Start time
		<select name="start_hour_distance" id="dis_start_hour" class="timelist">
		<option value="">--Select Hour--</option>
		<?php for($i=0;$i<=23;$i++){ ?>
		<option value="<?php if($i<=9){ echo '0'.$i;} else{ echo $i;} ?>" <?php if($i<=9){ $val='0'.$i;} else{ $val=$i;} if($val==$dis_getHour[0] ){ echo 'selected';}?>><?php if($i<=9){ echo '0'.$i;} else{ echo $i;} ?></option>
		<?php } ?>
		</select>
		<select name="start_minutes_distance" id="dis_start_minutes" class="timelist">
		<option value="">--Select Minutes--</option>
		<?php for($i=0;$i<=59;$i++){ ?>
		<option value="<?php if($i<=9){ echo '0'.$i;} else{ echo $i;} ?>" <?php if($i<=9){ $val='0'.$i;} else{$val=$i;} if($val==$dis_getHour[1]){ echo 'selected';} ?> ><?php if($i<=9){ echo '0'.$i;} else{ echo $i;} ?></option>
		<?php } ?>
		</select>
		</td><td>End Time
		<select name="end_hour_distance" id="dis_end_hour_distance" class="timelist">
		<option value="">--Select Hour--</option>
		<?php for($i=0;$i<=23;$i++){ ?>
		<option value="<?php if($i<=9){ echo '0'.$i;} else{ echo $i;} ?>" <?php if($i<=9){ $val='0'.$i;} else{ $val=$i;} if($val==$dis_getHourLast[0] ){ echo 'selected';} ?>><?php if($i<=9){ echo '0'.$i;} else{ echo $i;} ?></option>
		<?php } ?>
		</select>
		<select name="end_minuets_distance" id="dis_end_minuets" class="timelist">
		<option value="">--Select Minutes--</option>
		<?php for($i=0;$i<=59;$i++){ ?>
		<option value="<?php if($i<=9){ echo '0'.$i;} else{ echo $i;} ?>" <?php if($i<=9){$val='0'.$i;} else{$val=$i;} if($val==$dis_getHourLast[1] ){ echo 'selected';} ?>><?php if($i<=9){ echo '0'.$i;} else{ echo $i;} ?></option>
		<?php } ?>
		</select>
		</td>
		<td id="dis_time_greater" style="color:red;"></td>
		</tr>
			   <tr>
				<td>Interval &nbsp;<input type="text" name="interval_val"   onkeypress="return isNumberKey(event);" id="dis_interval" value="<?php if($distance_interval!=""){ echo $distance_interval;} else{echo '';} ?>" size="6"  class="interval"/>(In Minutes)</td>
				</tr>
				</table>
				</td>
				</tr>
				
			</table>
			</td>
				
			</tr>
            
            
			<tr>
				<td><input name="page_add" type="hidden" value="yes" />
				<input name="nav_preID" type="hidden" value="<?php echo $_GET['id']; ?>" />
				<input name="account_id" type="hidden" value="<?php echo $pageGroup[0]->account_id; ?>" />
				<input name="user_type" type="hidden" value="<?php echo $pageGroup[0]->user_type; ?>" />
				<input name="hierarchy_id" type="hidden" value="<?php echo $pageGroup[0]->hierarchy_id; ?>" />
				<input type="button" value="Back" class="form-reset" onclick="backto();" />
		<?php /*?><input type="submit" class="form-submit" name="submit" value="Save" <?php if($value_of_location<=0){ ?> onclick="return validation();" <?php } ?> /><?php */?>
				<input type="submit" class="form-submit" name="submit" value="Save"  onclick="return validation();"  />
				<input type="submit" class="form-submit" name="next" value="Next" />
				
				</td>
				
			</tr>
		</table>
	</form>

</td>
			</tr>
		</table>
	<!-- end id-form  -->

	</td>
		<td>
		<!-- right bar-->
		<?php //include("rightbar/item_bar.php") ?>
		</td>
	</tr>
	</table>
	<div class="clear"></div>
	</div>
	<!--  end content-table-inner  -->
	</td>
	<td id="tbl-border-right"></td>
	</tr>
<!--<tr>
	<th class="sized bottomleft"></th>
	<td id="tbl-border-bottom">&nbsp;</td>
	<th class="sized bottomright"></th>
</tr>-->
</table>
<div class="clear">&nbsp;</div>
</div>
<!--  end content -->
<div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->
<div class="clear">&nbsp;</div> 
<!-- start footer -->         
<?php include("footer.php") ?>
<!-- end footer -->
 
</body>
</html>