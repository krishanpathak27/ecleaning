<?php
include("includes/config.inc.php");
include("includes/function.php");
$page_name="Add Scheme";
$_objAdmin = new Admin();
$_objItem = new Item();
$objArrayList= new ArrayList();


if(isset($_POST['add']) && $_POST['add'] == 'yes')
{
	// echo "<pre>";
	// print_r($_POST);
	// exit;


	if($_POST['combo']==2 && $_POST['item_list']!=''){
		$item_list=mysql_escape_string($_POST['item_list']);
		$items = explode(",", $item_list);
		$data="";
		for($i=0;$i<count($items);$i++){
			if($items[$i]!=''){
			if($i>=1){
				$data.=",";
				}
			$data.="'";
			$data.=$items[$i];
			$data.="'";
			}
		}

		$auItemRec=$_objAdmin->_getSelectList('table_item as i inner join table_discount_item as di on di.item_id=i.item_id inner join table_discount as d on d.discount_id=di.discount_id left join table_discount_detail as ds on ds.discount_id=d.discount_id',"di.discount_item_id,ds.discount_desc,i.item_code",''," i.item_code in ($data) and d.mode=2 and d.last_update_status!='Delete'");
		
		if(is_array($auItemRec)){
			$err="";
			for($a=0;$a<count($auItemRec);$a++){
				$err.= "Item Code: ".$auItemRec[$a]->item_code." already exists in Scheme: ".$auItemRec[$a]->discount_desc."</br>";
			}
		} else {

			$dis_id=$_objAdmin->addDiscount();

			if($dis_id!=''){

				// AJAY@2017-01-17

				if(isset($_POST['slabwise_scheme']) && $_POST['slabwise_scheme'] == 1){	
					$dis_det_id=$_objAdmin->addDiscountDetailSlabs($dis_id);
				} else {
					$dis_det_id=$_objAdmin->addDiscountDetail($dis_id);
				}

				// AJAY@2017-01-17

				if($_POST['combo']==2 && $_POST['item_list']!=''){
					$id=$_objAdmin->addDiscountCombo($dis_id);	
				}
				
				if($_POST['item_type']==2){
					$id=$_objAdmin->addDiscountCat($dis_id);	
				}
				
				if($_POST['item_type']==3){
					$id=$_objAdmin->addDiscountItem($dis_id);	
				}

				if($_POST['item_type']==4){
					$id=$_objAdmin->addDiscountBrand($dis_id);	
				}



				
				if($_POST['party_type']==2){
					$id=$_objAdmin->addDiscountState($dis_id);	
				}

				if($_POST['party_type']==3){
					$id=$_objAdmin->addDiscountCity($dis_id);	
				}
				
				if($_POST['party_type']==4){
					$id=$_objAdmin->addDiscountRetailer($dis_id);	
				}

				if($_POST['party_type']==5){
					$id=$_objAdmin->addDiscountDistributor($dis_id);	
				}



				
				if($_POST['Dis_type']==3){
					$id=$_objAdmin->addDiscountFOC($dis_id);	
				}
				
				$sus="Scheme has been Added successfully.";
			}
		}

		//echo $data;
		unset($data);

	} else {

		// echo "<pre>";
		// print_r($_POST);
		// exit;

		$dis_id=$_objAdmin->addDiscount();

		if($dis_id!=''){
			
			$openId=$_objAdmin->addOpenDiscount($dis_id);	
			//$disSchemeID=$_objAdmin->addDisDiscount($dis_id);
			
			// AJAY@2017-01-17

			if(isset($_POST['slabwise_scheme']) && $_POST['slabwise_scheme'] == 1){	
				$dis_det_id=$_objAdmin->addDiscountDetailSlabs($dis_id);
			} else {
				$dis_det_id=$_objAdmin->addDiscountDetail($dis_id);
			}

			// AJAY@2017-01-17

			if($_POST['item_type']==2){
				$id=$_objAdmin->addDiscountCat($dis_id);	
			}
			
			if($_POST['item_type']==3){
				$id=$_objAdmin->addDiscountItem($dis_id);	
			}

			
			if($_POST['item_type']==4){
				$id=$_objAdmin->addDiscountBrand($dis_id);	
			}


			
			if($_POST['party_type']==2){
				$id=$_objAdmin->addDiscountState($dis_id);	
			}
			
			if($_POST['party_type']==3){
				$id=$_objAdmin->addDiscountCity($dis_id);	
			}
			
			if($_POST['party_type']==4){
				$id=$_objAdmin->addDiscountRetailer($dis_id);	
			}


				if($_POST['party_type']==5){
					$id=$_objAdmin->addDiscountDistributor($dis_id);	
				}



			
			if($_POST['Dis_type']==3){
				$id=$_objAdmin->addDiscountFOC($dis_id);	
			}
			$sus="Scheme has been Added successfully.";
			
		}
	}
}




?>

<?php include("header.inc.php");
$pageAccess=1;
$check=$objArrayList->checkAccess($pageAccess, basename($_SERVER['PHP_SELF']));
if($check == false){
header('Location: ' . $_SERVER['HTTP_REFERER']);}
 ?>
<!--<script src="javascripts/jquery-ui.js"></script>
<script src="javascripts/discount.js"></script>
<script src="javascripts/addrow.js"></script>-->

<link rel="stylesheet" href="./base/jquery.ui.all.css">
<script src="./Autocomplete/jquery-1.5.1.js"></script>
<script src="./Autocomplete/jquery.ui.core.js"></script>
<script src="./Autocomplete/jquery.ui.widget.js"></script>
<script src="./Autocomplete/jquery.ui.button.js"></script>
<script src="./Autocomplete/jquery.ui.position.js"></script>
<script src="./Autocomplete/jquery.ui.menu.js"></script>
<script src="./Autocomplete/jquery.ui.autocomplete.js"></script>
<script src="./Autocomplete/jquery.ui.tooltip.js"></script>
<script src="./Autocomplete/jquery.ui.datepicker.js"></script>
<link rel="stylesheet" href="./base/demos.css">
<script type="text/javascript" src="javascripts/validate.js"></script>
<script src="javascripts/discount.js"></script>
<script src="javascripts/addrow.js"></script>

<!--  end content-outer -->
<div class="clear">&nbsp;</div>
<script>

  $(function() {
        $( "#from" ).datepicker({
			dateFormat: "d M yy",
            defaultDate: "w",
            changeMonth: true,
            numberOfMonths: 1,
            onSelect: function( selectedDate ) {
                $( "#to" ).datepicker( "option", "minDate", selectedDate );
            }
        });
        $( "#to" ).datepicker({
			dateFormat: "d M yy",
            defaultDate: "-w",
            changeMonth: true,
            numberOfMonths: 1,
            onSelect: function( selectedDate ) {
                $( "#from" ).datepicker( "option", "maxDate", selectedDate );
            }
        });
    });

     
</script>
	<style>
	.ui-button {
		margin-left: -1px;
	}
	.ui-button-icon-only .ui-button-text {
		padding: 0.35em;
	} 
	.ui-autocomplete-input {
		margin: 0;
		padding: 0.4em 0 0.4em 0.45em;
	}


.ui-widget .ui-widget {
    font-size: 12px;
}




	</style>
<?php include("discount_list.php") ?>
<script type="text/javascript">


function blockSpecialChar(e) {
	        //console.log(e);
            var k = e.charCode;
            //alert(k);
            return ((k > 64 && k < 91) || k == 8  || k == 0  || k == 190   || (k >= 48 && k <= 57));
        }


$(document).ready(function() {
	$("#min_qty").attr('disabled', 'disabled');
	$("#min_amt").attr('disabled', 'disabled');
	$("#cat").attr('disabled', 'disabled');
	$("#item").attr('disabled', 'disabled');
	$("#state_list").attr('disabled', 'disabled');
	$("#city_list").attr('disabled', 'disabled');
	$("#ret_list").attr('disabled', 'disabled');
	$("#dis_list").attr('disabled', 'disabled');
	$("#dis_per").attr('disabled', 'disabled');
	$("#dis_amt").attr('disabled', 'disabled');
	$("#item_list").attr('disabled', 'disabled');
		$("#brand").attr('disabled', 'disabled');
		$(".disshow").hide();
	var v = $("#frmPre").validate({
			rules: {
				fileToUpload: {
				  required: true,
				  accept: "csv"
				}
			  },
			submitHandler: function(form) {
				document.frmPre.submit();				
			}
		});
});

</script>
<script type="text/javascript">
$(document).ready(function(){
	$('#loader').show();
	$('#loader').html('<div id="loader" align="center"><img src="images/ajax-loader.gif" /><br/>Please Wait...</div>');
    $.ajax({
		'type': 'POST',
		'url': 'discount_date.php',
		'data': '',
		'success' : function(mystring) {
			//alert(mystring);
		//document.getElementById("div1").innerHTML = mystring;
		$('#div1').html(mystring);
		$('#loader').hide();
		}
	});
	
});




function loadSchemePageWithDivisionId (division_id) {

	if(parseInt(division_id)>0)
		window.location.href = "adddiscount.php?division_id="+division_id;
	else {
		alert('Please select at least a division!');
		window.location.href = "adddiscount.php";
	}


}


</script>
<!-- Update For New Scheme (Gaurav)------->
<style type="text/css">
#StateDiv, #CityDiv, #ClassDiv, #StateDivNew {
    clear: none;
    height: 170px;
    margin: 0;
    overflow: scroll;
    padding: 0;
    width: 160px;
}

#MarketDiv, #RetailerDiv, #DistributorDiv{
    clear: none;
    height: 170px;
    margin: 0;
    overflow: scroll;
    padding: 0;
    width: 200px;
}

label {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	font-weight: normal;
}

.optlegend {
    background: none repeat scroll 0 0 #6E6E6E;
    border-radius: 0.2em 0.2em 0.2em 0.2em;
    color: #FFFFFF;
    font-family: arial;
    font-size: 17px;
    font-weight: normal;
    padding: 3px;

}
</style>
<script type="text/javascript">
	
	function stateDisplay(){
					var favorite=[];							
					
					<?php $data = $_objAdmin->_getSelectList('table_relationship',"relationship_id, relationship_code",''," relationship_code!='' and status='A'"); 
					
					foreach($data as $value){
						?>
						var relID="<?php echo $value->relationship_id; ?>";
						var checked=$('#rel'+relID).is(':checked'); 
							//alert(checked);
							var cName="relation"+<?php echo $value->relationship_id; ?>; 
							//alert(cName);
							if(checked==false){								
										$("input."+cName).attr('disabled','disabled');
										$("input."+cName).removeAttr('checked');	
										$("."+cName).hide();							
								}
							else if(checked==true){
									$("input."+cName).removeAttr('disabled','');
									$("."+cName).show();
								}
						<?php
						
						
						}
					
					?>
						
			}
	
	$(document).ready(function(){
	
	
	// Get list of city with selected state
	
		$('.newcheck input').change(function() {						   
		var final = $(this).val();
		var checkID = $('#state'+final).length;
		
		if(final!="" && checkID == 0){
			Getlistofdata('stateID','CityDiv', final,'');
			//RetailerCheckAll('RetailerDiv', 'retailercheck', 'selectAllRetailDist');
		} else {
			$('#state'+final).remove();
			$('.mstate'+final).remove();
			//RetailerCheckAll('RetailerDiv', 'retailercheck', 'selectAllRetailDist');
		}
	});

	
	// Get list of Markets with selected Cities
	
		$('.citycheck input:checkbox').live('change', '.citycheck', function(event) {	
			//alert("Hello");
			
			var favorite = [];
				$.each($("input[name='relationship[]']:checked"), function(key, value){ 
					//alert($(this).val());           
                favorite.push($(this).val());
            });
			
			/*var arrayLength = favorite.length;
			for (var i = 0; i < arrayLength; i++) {
				var cName="relation"+favorite[i]; 
						$("."+cName).show();
				//alert(cName);
				//Do something
			}*/
			
			var final = $(this).val();
			var stateID = $(this).attr('alt');
			var classID = favorite.join(", ");
			var checkID = $('#city'+final).length;
			//alert(classID);
			if(final!="" && checkID == 0){
				Getlistofdata('cityID','DistributorDiv', final,stateID,classID);
				
				//RetailerCheckAll('RetailerDiv', 'retailercheck', 'selectAllRetailDist');
			} else {
				$('#city'+final).remove();
				$('.city'+final).remove();
				//RetailerCheckAll('RetailerDiv', 'retailercheck', 'selectAllRetailDist');
			}
	});
	
// Get list of Retailers with selected Market
	
	$('.selectAll').live('change', function(event) {
		//alert("Hello");
		
	   var btn_request = $(this).val();
	   var status = $(this).prop('checked');
	   
	   var cityID = $(this).attr('title');
	   var stateID = $(this).attr('alt');

	  	if (status==true){ 	
		   $('.city'+cityID).remove();
		   $('#'+btn_request).parent().find('input[type="checkbox"]').not("[disabled]").prop('checked', true);
		   loadretaildis('All', btn_request, cityID, stateID)
	   } else {
		   $('#'+btn_request).parent().find('input[type="checkbox"]').prop('checked', false);
		   $('.city'+cityID).remove();
		   //RetailerCheckAll('RetailerDiv', 'retailercheck', 'selectAllRetailDist');
	   }
	   return false;
	});	
		

		
	$('.selectAllRetailDist').live('change', function(event) {
		
	   var btn_request = $(this).val();
	   var status = $(this).prop('checked');

	  	if (status==true){ 	
		   $('#'+btn_request).parent().find('input[type="checkbox"]').prop('checked', true);
	   } else {
		   $('#'+btn_request).parent().find('input[type="checkbox"]').prop('checked', false);
	   }
	   return false;
	});	

	});
	
	function GetlistofdataRD(name, divID, final, cityID, stateID){
	$('#loader').show();
	$('#loader').html('<div id="loader" align="center"><img src="images/ajax-loader.gif" /></div>');
	//alert('Ajax');
		$.ajax({
			type: "GET",
			url: '<?php echo HOST_NAME;?>/disajaxcitylist.php',
			data: name+'='+final+'&cid='+cityID+'&sid='+stateID,
			success: function(status){
			//alert(status);
				$('#loader').hide();
				$('.selectAllRetailDist').attr('checked', false);
				$('#'+divID).append(status);
				//RetailerCheckAll('RetailerDiv', 'retailercheck', 'selectAllRetailDist');
				}
			});	
	}
	
	function Getlistofdata(name, divID, final, stateID,classID){
		$('#11'+divID).show();
		$('#11'+divID).html('<div id="#11"'+divID+' align="center"><img src="images/ajax-loader.gif" /></div>');
	
		var rID = $('#rID').val();
		
		if(typeof stateID!='undefined'){ } else { stateID = ''; }
		if(typeof classID!='undefined'){ } else { classID = ''; }
		$.ajax({
			type: "GET",
			url: '<?php echo HOST_NAME;?>/disajaxcitylist.php',
			data: name+"="+final+"&stID="+stateID+"&clID="+classID,
			success: function(status){
				$('#11'+divID).hide();
				$('#'+divID).append(status);
				var favorite = [];
				$.each($("input[name='relationship[]']:checked"), function(key, value){ 
					//alert($(this).val());           
                favorite.push($(this).val());
            });
			
			var arrayLength = favorite.length;
			for (var i = 0; i < arrayLength; i++) {
				var cName="relation"+favorite[i]; 
						$("."+cName).show();
				//alert(cName);
				//Do something
			}
				
				}
			});	
	}
	
	
	
	function GetlistofdataMarkert(name, divID, final, stateID){
			$('#Mloader').show();
			$('#Mloader').html('<div id="Mloader" align="center"><img src="images/ajax-loader.gif" /></div>');
		
			var rID = $('#rID').val();
			
			if(typeof stateID!='undefined'){ } else { stateID = ''; }
			$.ajax({
				type: "GET",
				url: '<?php echo HOST_NAME;?>/disajaxcitylist.php',
				data: name+"="+final+"&stID="+stateID,
				success: function(status){
					$('#Mloader').hide();
					$('#'+divID).append(status);
					}
				});	
		}
	
	
	
</script>

<script type="text/javascript">
	
		$(document).ready(function(){
			RetailerCheckAll('RetailerDiv', 'retailercheck', 'selectAllRetailDist');
			
			
			
			$('.retailercheck').live('change', function(event) {
				//RetailerCheckAll('RetailerDiv', 'retailercheck', 'selectAllRetailDist');
				
			});
			
			$('.distributorcheck').live('change', function(event) {
				//RetailerCheckAll('RetailerDiv', 'retailercheck', 'selectAllRetailDist');
				RetailerCheckAll('DistributorDiv', 'distributorcheck', 'selectAllDistDist');
			});

			
			
			
		});
	
		function RetailerCheckAll(DivID, ClassName, SelectAllID){

				var checkboxCount;
				checkboxCount = $("#"+DivID+" input[type='checkbox']").length;
				//alert(checkboxCount);
				
				
					var CountAllCheckBoxes = 0;
					$('.'+ClassName+' input:checked').each(function(){        
						CountAllCheckBoxes = CountAllCheckBoxes+1;
					});
				//alert(CountAllCheckBoxes);
				
				if(checkboxCount == CountAllCheckBoxes && checkboxCount!=0){
					$('#'+SelectAllID).attr('checked', true);
				} else {
					$('#'+SelectAllID).attr('checked', false);
				}
				
		}

	
	function checkAllSelectedCheckBOX(DivID, ClassName, SelectAllID)
	{
		//alert(SelectAllID);
			var checkboxCount;
			checkboxCount = $("#"+DivID).children().find('input[type="checkbox"]').length;
			//alert(checkboxCount);
			
			
				var CountAllCheckBoxes = 0;
				$("#"+DivID).children().find('input:checked').each(function(){        
					CountAllCheckBoxes = CountAllCheckBoxes+1;
				});
			//alert(CountAllCheckBoxes);
			
			if(checkboxCount == CountAllCheckBoxes && checkboxCount!=0){
				$('#'+SelectAllID).attr('checked', true);
			} else {
				$('#'+SelectAllID).attr('checked', false);
			}
			
	}
</script>
<!-- Update For New Scheme (Gaurav)------->


<div class="clear"></div>
<!-- start content-outer -->
<div id="content-outer">
<!-- start content -->
	<div id="content">
	<div id="loader" style="position:absolute; margin-left:40%; margin-top:10%;"></div>
	<div id="page-heading"><h1><span style="color: #d74343; font-family: Tahoma; font-weight: bold;">Add Scheme</span></h1></div>
		<table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">
		<tr>
			<th rowspan="3" class="sized"><img src="images/side_shadowleft.jpg" width="20" height="300" alt="" /></th>
			<th class="topleft"></th>
			<td id="tbl-border-top">&nbsp;</td>
			<th class="topright"></th>
			<th rowspan="3" class="sized"><img src="images/side_shadowright.jpg" width="20" height="300" alt="" /></th>
		</tr>
		<tr>
			<!--<td id="tbl-border-left"></td>-->
			<td>
			<!--  start content-table-inner -->
			<div id="content-table-inner">
				<table border="0" width="100%" cellpadding="0" cellspacing="0">
				<tr valign="top">
					<td>
					<!--  start message-red -->
					<?php if($err!=''){?>
					<div id="message-red">
						<table border="0" width="100%" cellpadding="0" cellspacing="0">
						<tr>
							<td class="red-left"> <?php echo $err; ?></td>
							<td class="red-right"><a class="close-red"><img src="images/icon_close_red.gif"   alt="" /></a></td>
						</tr>
						</table>
					</div>
					<?php } ?>
					<!--  end message-red -->
					<?php if($sus!=''){?>
					<!--  start message-green -->
					<div id="message-green">
						<table border="0" width="100%" cellpadding="0" cellspacing="0">
						<tr>
							<td class="green-left"><?php echo $sus; ?></td>
							<td class="green-right"><a class="close-green"><img src="images/icon_close_green.gif"   alt="" /></a></td>
						</tr>
						</table>
					</div>
					<?php } ?>
					<!--  end message-green -->
					
			<!-- start id-form -->
		<form name="frmPre" id="frmPre" method="post" action="#" enctype="multipart/form-data" >
			<table border="0" cellpadding="0" cellspacing="0" width="800" id="id-form">


			<tr>
						<th valign="top">Division:</th>
						<td>
							<select name="division_id" id="division_id" class="styledselect_form_4 required" onchange="loadSchemePageWithDivisionId(this.value)">
								<option value="">Select Division</option>
								<?php 
								$condi = "";
								if(!empty($divisionIdString)) {
									$condi = " AND division_id IN (".$divisionIdString.") ";	
								}
								
								$branchList=$_objAdmin->_getSelectList('table_division',"division_id,division_name",''," status='A' $condi ");
									for($i=0;$i<count($branchList);$i++){
									
									if($branchList[$i]->division_id==$_GET['division_id']){$select="selected";} else {$select="";}
										
								 ?>
								 <option value="<?php echo $branchList[$i]->division_id; ?>" <?php echo $select ?>><?php echo $branchList[$i]->division_name; ?></option>
								 <?php } ?>
							</select>
						</td>
						<td></td>
					</tr>





			<tr>
				<th valign="top" width="100px">Scheme Description:</th>
				<td width="200px"><input type="text" name="dis_desc" id="dis_desc"  class="required" value="" maxlength="255"/></td>
				<td width="500px"></td>
			</tr>
			<tr>
				<th valign="top">Scheme:</th>
				<td>
				<input type="radio" name="discount" value="1" checked="checked"> Exclusive &nbsp;
				<input type="radio" name="discount" value="2" > Non Exclusive &nbsp;
				</td>
				<td></td>
			</tr>
			<tr id="" style="">
				<th valign="top">Primary Scheme</th>
				<td>
				<input type="radio" name="dis_discount" value="1" onchange="hidePartyType(this.value);" id="dis_discount"> Yes &nbsp;
				<input type="radio" name="dis_discount" value="2" onchange="hidePartyType(this.value);" id="dis_discount" checked="checked"> NO &nbsp;
				</td>
				<td></td>
			</tr>







		   <?php if(isset($_GET['division_id']) && !empty($_GET['division_id']) && $_GET['division_id']>0) {?>

			<tr>
				<th valign="top">Scheme Mode:</th>
				<td>
				<select name="dis_mode" id="dis_mode" class="required styledselect_form_4" onchange="distype(this.value);">
					<option value="">Select</option>
					<option value="1">Quantity</option>
					<!-- <option value="2">Amount</option> -->
				</select>	
				</td>
				<td></td>
			</tr>


			
			<tr id="itemtypeshow" style="display:none;">
				<th valign="top">Item Type:</th>
				<td valign="top">
				<select name="item_type" id="item_type" class="styledselect_form_3" onchange="itemtype(this.value);">
					<!-- <option value="">Select</option> -->
					<option value="1">All</option>
					<option value="2">Category</option>
					<option value="3">Items</option>
					<option value="4">Brands</option>
				</select>	
				</td>
			</tr>


			<?php }?>



			<tr id="itmshow" style="display:none;">
				<th valign="top">Select Item:</th>
				<td>
				<div class="ui-widget">
					<select id="item" name="item" style="display: none;">
						<option value="">Search</option>
						<?php
						$auCol=$_objAdmin->_getSelectList('table_item AS I',"I.item_id,I.item_name,I.item_code",''," I.status='A' and I.item_division='".$_GET['division_id']."' AND I.account_id=".$_SESSION['accountId']);
						for($i=0;$i<count($auCol);$i++){
						?>
							<option value="<?php echo $auCol[$i]->item_id;?>"><?php echo $auCol[$i]->item_name;?> <?php if($auCol[$i]->item_code !=''){ echo "(".$auCol[$i]->item_code.")"; }?></option>
						<?php } ?>
					</select>
				</div>
				</td>
				<td></td>
			</tr>
			<tr id="catshow" style="display:none;">
				<th valign="top">Select Category:</th>
				<td>
				<div class="ui-widget">
					<select id="cat" name="cat" class="required" style="display: none;">
						<option value="">Search</option>
						<?php
						$auCol=$_objAdmin->_getSelectList('table_category',"category_id,category_name,category_code",''," status='A' and account_id=".$_SESSION['accountId']);
						for($i=0;$i<count($auCol);$i++){
						?>
							<option value="<?php echo $auCol[$i]->category_id;?>"><?php echo $auCol[$i]->category_name;?> <?php if($auCol[$i]->category_code !=''){ echo "(".$auCol[$i]->category_code.")"; }?></option>
						<?php } ?>
					</select>
				</div>
				</td>
				<td></td>
			</tr>



			<tr id="brandshow" style="display:none;">
				<th valign="top">Select Brand:</th>
				<td>
				<div class="ui-widget">
					<select id="brand" name="brand" class="required">
						<option value="">Search</option>
						<?php
						$auCol=$_objAdmin->_getSelectList('table_brands',"brand_id,brand_name",''," status='A' and account_id=".$_SESSION['accountId']);
						for($i=0;$i<count($auCol);$i++){
						?>
							<option value="<?php echo $auCol[$i]->brand_id;?>"><?php echo $auCol[$i]->brand_name;?></option>
						<?php } ?>
					</select>
				</div>
				</td>
				<td></td>
			</tr>




			
			<!--
			DESC: Create Slabwise options
			Author: AJAY@2017-01-13
			-->

			<tr class="SlabWiseSchemeEnteryPoint" style="display:none;">
				<th valign="top">Slab Scheme:</th>
				<td>
				<input type="radio" name="slabwise_scheme"  value="1" onclick="showSlabOptions(this.value)"> Yes &nbsp;
				<input type="radio" name="slabwise_scheme" value="0" checked="checked" onclick="showSlabOptions(this.value)"> No &nbsp;
				</td>
				<td></td>
			</tr>


			<?php require('adddiscount_slabs.php');?>

			




			<tr  class="withOutSlabWiseGroup" style="display:none;">
			<th>Scheme Detail:</th>
				<td colspan="4"><table border="0">
				<tr>

					<th id="minqtyshow" style="display:none;" valign="top">Minimum Quantity:<br><input type="text" name="min_qty" id="min_qty" class="required numberDE" onkeypress="return blockSpecialChar(event)" maxlength="10"/></th>
					
					<th  id="minamtshow" style="display:none;" valign="top">Minimum Amount:<br><input type="text" name="min_amt" id="min_amt" class="required numberDE" onkeypress="return blockSpecialChar(event)" maxlength="10"/></th>



					<th valign="top">Scheme Type:<br>
						<select name="Dis_type" id="Dis_type" class="required styledselect_form_4" onchange="dtype(this.value);">
							<option value="0">Select</option>
							<option value="1">Percentage</option>
							<option value="2">Amount</option>
							<option value="3">FOC</option>
							<option value="4">Trip</option>
						</select>
					</th>	


					<th id="pershow" style="display:none;" valign="top">Scheme Percentage:<br><input type="text" name="dis_per" id="dis_per" class="required numberDE" value="" maxlength="3" max="99"/></th>

					<th id="amtshow" style="display:none;" valign="top">Scheme Amount:<br><input type="text" name="dis_amt" id="dis_amt" class="required numberDE" value="" maxlength="10"/></th>

					<th  id="focshow" style="display:none;" valign="top" >Free Item:<br>
									<div class="ui-widget">
									<select class="free_item" name="free_item" style="display: none;">
									<option value="">Search</option>
									<?php $auCol=$_objAdmin->_getSelectList('table_item AS I',"I.item_id,I.item_name,I.item_code",''," I.status='A' and I.item_division='".$_GET['division_id']."' AND I.account_id=".$_SESSION['accountId']);
									for($i=0;$i<count($auCol);$i++){?>
										<option value="<?php echo $auCol[$i]->item_id;?>"><?php echo $auCol[$i]->item_name;?> <?php if($auCol[$i]->item_code !=''){ echo "(".$auCol[$i]->item_code.")"; }?></option>
									<?php } ?>
									</select>
									</div>
									<span class="style1">Free Quantity:</span><br>
									<input name="disquanity" class="text" type="text numberDE" id="disquanity" value="" maxlength="10" style="width:70px" />
									 <input name="discountlistid" class="text" type="text" id="discountlistid" value="" style="display:none;visibility:hidden" />
									 <span id="txtHint"></span>
					 </th>

					 <th id="tripDescShow" style="display:none;" valign="top">Trip Description:<br><input type="text" name="trip_desc" id="trip_desc" class="required" /></th>

					 </tr>
					</table></td>
				</th>
			</tr>



			<tr id="comboshow" style="display:none;">
				<th valign="top"></th>
				<td>
				<input type="radio" name="combo" value="1" id="combo" checked="checked" onchange="combotype(this.value);"> Invoice &nbsp;
				<input type="radio" name="combo" value="2" id="combo" onchange="combotype(this.value);" > Item Combo &nbsp;
				</td>
				<td></td>
			</tr>
			
			<!-- <tr id="comboshow1" style="display:none;">
				<th valign="top">Open Discount</th>
				<td>
					<input type="radio" name="open_discount" value="1" id="open_discount" > Yes &nbsp;
					<input type="radio" name="open_discount" value="2" id="open_discount" checked="checked"> No &nbsp;
				</td>
				<td></td>
			</tr> -->

			<tr id="showItem" style="display:none;">
				<th valign="top">Item Code:</th>
				<td>
				<input id="item_list" style="width: 500px; height: 15px;" class="ui-autocomplete-input required" autocomplete="off" role="textbox" aria-autocomplete="list" aria-haspopup="true" name="item_list" placeholder="Search Item Code">
				</td>
				<td></td>
			</tr>



 		<?php if(isset($_GET['division_id']) && !empty($_GET['division_id']) && $_GET['division_id']>0) {?>
			
			<tr id="disParty">
				<th valign="top">Party Type:</th>
				<td valign="top">
				<select name="party_type" id="party_type" class="styledselect_form_3" onchange="partype(this.value);">
					<option value="1">All</option>
					<option value="2">State</option>
					<option value="3">City</option>
					<option value="4" class="retshow">Retailer</option>
					<option value="5" class="disshow">Distributor</option>
				</select>	
				</td>
				
				<td></td>
		</tr>

		<?php }?>













	<!--      Add New Party Type -->
	<tr id="dispartyType" style="display:none"><th>Party Type:</th>
	<td>
	<table border="0" cellpadding="0" cellspacing="0" style="width:300px;">
	<tr>		
		<th>Select State
			<div id="StateDiv" class="input select" style="display:block">
			<?php 
			$stateID=array();
			$getListOfState=$_objAdmin->_getSelectList('table_distributors',"DISTINCT(state)",'','');
			foreach($getListOfState as $value){$stateID[]=$value->state;}			
			$stateIdList=implode(',',$stateID);
			
			$data = $_objAdmin->_getSelectList2('state',"state_id, state_name",''," state_id IN (".$stateIdList.") and state_name!=''");

				foreach($data as $value):?>
				<div class="newcheck">
				<input type="checkbox" name="state[]" value="<?php echo $value->state_id;?>"
					<?php if(in_array($value->state_id, $stateArr)){?> checked="checked"<?php }?> />
				<label for="MultistatsID"><?php echo $value->state_name;?></label>
				</div>	
				<?php endforeach;?>								
			</div>
			<!--<div id="StateDivNew" class="input select" style="display:block">
			</div>-->
		</th>
		<th>Select City
			<div id="CityDiv" class="input select">		
				<div id="11CityDiv"></div>
			</div>
		</th>
		<th>Distributor Class
			<div id="ClassDiv" class="input select">
			<?php 
			
			$data = $_objAdmin->_getSelectList('table_division',"division_id, division_name",''," division_name!='' and status='A'");

				foreach($data as $value):?>
				<div class="newcheck1">
				<input id="rel<?php echo $value->division_id; ?>" type="checkbox" name="relationship[]" value="<?php echo $value->division_id;?>"
					<?php if(in_array($value->state_id, $stateArr)){?> checked="checked"<?php }?> onclick="stateDisplay()"/>
				<label for="MulticlassID"><?php echo $value->division_name;?></label>
				</div>	
				<?php endforeach;?>								
			</div>
		</th>	
		
		<th>  Select Distributors
			<div id="DistributorDiv" class="input select">
				<div id="11DistributorDiv"></div>						
			</div>
		</th>		
	</tr>
	</table>
	</td>
	</tr>
<!------   End  Add New Party Type ---------------------------------------->
	
			<tr id="satshow" style="display:none;" >
				<th valign="top">State Name:</th>
				<td>
				<input id="state_list" style="width: 500px; height: 15px;" class="ui-autocomplete-input required" autocomplete="off" role="textbox" aria-autocomplete="list" aria-haspopup="true" name="state_list" placeholder="Search State Name">
				</td>
				<td></td>
			</tr>
			<tr id="cityshow" style="display:none;" >
				<th valign="top">City Name:</th>
				<td>
				<input id="city_list" style="width: 500px; height: 15px;" class="ui-autocomplete-input required" autocomplete="off" role="textbox" aria-autocomplete="list" aria-haspopup="true" name="city_list" placeholder="Search City Name">
				</td>
				<td></td>
			</tr>
			<tr id="retshow" style="display:none;" >
				<th valign="top">Retailer Name:</th>
				<td>
				<input id="ret_list" style="width: 500px; height: 15px;" class="ui-autocomplete-input required" autocomplete="off" role="textbox" aria-autocomplete="list" aria-haspopup="true" name="ret_list" placeholder="Search Retailer Name">
				</td>
				<td></td>
			</tr>


			<tr id="disshow" style="display:none;" >
				<th valign="top">Distributor Name:</th>
				<td>
				<input id="dis_list" style="width: 500px; height: 15px;" class="ui-autocomplete-input required" autocomplete="off" role="textbox" aria-autocomplete="list" aria-haspopup="true" name="dis_list" placeholder="Search Distributor Name">
				</td>
				<td></td>
			</tr>




			<!-- <tr id="disshow" style="display:none;" >
				<th valign="top">Distributor Name:</th>
				<td colspan="2">
				<table border="0">
				<tr>
				<td>
				<select name="dis_list[]" id="dis_list"  data-placeholder="Search Distributor Name" style="width:450px;"  class="chosen-select" multiple>
				<?php $auRut=$_objAdmin->_getSelectList('table_division',"division_id, division_name",''," status = 'A' ORDER by division_name");	
					  for($i=0;$i<count($auRut);$i++){?>
					  <optgroup class="routeList" label="<?php echo ucwords(strtolower(trim($auRut[$i]->division_name)));?>">
					  <?php if($auRut[$i]->division_id!=''){
					  	$auchk=$_objAdmin->_getSelectList('table_distributors',"distributor_code, distributor_name",''," division_id=".$auRut[$i]->division_id." AND status ='A'"); }
					  	foreach ($auchk as $key => $value) {?>
					  		<option value="<?php echo $value->division_id;?>">
					  		<?php echo ucwords(strtolower(trim($value->distributor_name)));?>
					  		</option>
					  <?php }}?>
				</select>
				</td>
				</tr>
				</table>
				</td>
			</tr> -->



			
			
			<tr >
				<!--<th valign="top"></th>-->
				<td colspan="3">
					<div id="div1"></div>
				</td>
			</tr>
			<tr>
				<th valign="top">Scheme Status:</th>
				<td>
				<select name="status" id="status" class="styledselect_form_3">
				<option value="A">Active</option>
				<option value="I">Inactive</option>
				</select>
				</td>
				<td></td>
			</tr>
			<tr>
			</tr>
			<tr>
				<th>&nbsp;</th>
				<td valign="top">
					<input name="account_id" type="hidden" value="<?php echo $_SESSION['accountId']; ?>" />
					<input name="add" type="hidden" value="yes" />
					<input type="reset" value="Reset!" class="form-reset">
					<input type="button" value="Back" class="form-reset" onclick="location.href='discount.php';" />
					<input name="submit" class="form-submit" type="submit" id="submit" value="Save" />
				</td>
			</tr>
			</table>
		</form>
			<!-- end id-form  -->
			</td>
			<td>
			<!-- right bar-->
			<?php //include("rightbar/item_bar.php") ?>
			<div class="clear"></div>
		</div>
<!-- end related-act-bottom -->
</div>
<!-- end related-activities -->
</td>
</tr>
<tr>
<td><img src="images/blank.gif" width="695" height="1" alt="blank" /></td>
<td></td>
</tr>
</table>
<div class="clear"></div>
</div>
<!--  end content-table-inner  -->
</td>
<td id="tbl-border-right"></td>
</tr>
<!--<tr>
	<th class="sized bottomleft"></th>
	<td id="tbl-border-bottom">&nbsp;</td>
	<th class="sized bottomright"></th>
</tr>-->
</table>





<div class="clear">&nbsp;</div>

</div>
<!--  end content -->
<div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->
<div class="clear">&nbsp;</div>
      <link rel="stylesheet" href="docsupport/prism.css">
  <link rel="stylesheet" href="docsupport/chosen.css">
  <style type="text/css" media="all">
    /* fix rtl for demo */
    .chosen-rtl .chosen-drop { left: -9000px; }
	
	#smsform label.error {
	margin-left: 10px;
	width: auto;
	display: inline;
	}
	.error {
		color: #FF0000;
		font:normal 12px tahoma;
	}

		input, textarea {
		width:auto;
		}

  </style>

		  <script src="docsupport/chosen.jquery.js" type="text/javascript"></script>

		  <script type="text/javascript">
			var config = {
			  '.chosen-select'           : {},
			  '.chosen-select-deselect'  : {allow_single_deselect:true},
			  '.chosen-select-no-single' : {disable_search_threshold:10},
			  '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
			  '.chosen-select-width'     : {width:"100%"}
			}
			for (var selector in config) {
			  $(selector).chosen(config[selector]);
			}

			function ChangeDropdowns(){
			if ($("#valnostatus").is(":checked")) {
			    $("#validation_reason").removeAttr("disabled");
			}
			if ($("#valyesstatus").is(":checked")) {
			    $("#validation_reason").attr("disabled","disabled");
			}

			}
			</script>

<!-- start footer --> 
<?php include("footer.php") ?>  
<!-- end footer -->
</body>
</html>