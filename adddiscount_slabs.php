<!--
			DESC: Create Slabwise options
			Author: AJAY@2017-01-13
			-->

			<tr class="withSlabWiseGroup" style="display:none;">
			<th>Slab 1: </th>
			<td colspan="4">
			<table border="0">
			<tr>

				<th valign="top">Minimum Quantity:<br><input type="text" name="min_qty1[1]" id="min_qty_1" class="required numberDE" onkeypress="return blockSpecialChar(event)" maxlength="10"/></th>

				<th valign="top">Max Quantity:<br><input type="text" name="max_qty1[1]" id="max_qty_1" class="required numberDE" onkeypress="return blockSpecialChar(event)" maxlength="10"/></th>



				<!-- Slab Offer 1 -->

				<th valign="top">Offer 1:<br>
					<select name="Dis_type1[1]" id="Dis_type1" class="required styledselect_form_4" onchange="dtypeslab(this.value, 1);">
						<option value="0">Select</option>
						<option value="1">Percentage</option>
						<option value="2">Amount</option>
						<option value="3">FOC</option>
						<option value="4">Trip</option>
					</select>
				</th>	

				<th id="pershow1" style="display:none;" valign="top">Scheme Percentage:<br><input type="text" name="dis_per1[1]" id="dis_per1" class="required numberDE" value="" maxlength="3"/></th>

				<th id="amtshow1" style="display:none;" valign="top">Scheme Amount:<br><input type="text" name="dis_amt1[1]" id="dis_amt1" class="required numberDE" value="" maxlength="10"/></th>


				<th  id="focshow1" style="display:none;" valign="top" >Free Item:<br>
				<div class="ui-widget">
				<select class="free_item" name="free_item1[1]" style="display: none;">
				<option value="">Search</option>
				<?php $auCol=$_objAdmin->_getSelectList('table_item AS I',"I.item_id,I.item_name,I.item_code",''," I.status='A' and I.item_division='".$_GET['division_id']."' AND I.account_id=".$_SESSION['accountId']);
				for($i=0;$i<count($auCol);$i++){?>
				<option value="<?php echo $auCol[$i]->item_id;?>"><?php echo $auCol[$i]->item_name;?> <?php if($auCol[$i]->item_code !=''){ echo "(".$auCol[$i]->item_code.")"; }?></option>
				<?php } ?>
				</select>
				</div>
				<span class="style1">Free Quantity:</span><br>
				<input name="disquanity1[1]" class="text" type="text numberDE" id="disquanity1" value="" maxlength="10" style="width:70px" />
				<input name="discountlistid1[1]" class="text" type="text" id="discountlistid1" value="" style="display:none;visibility:hidden" />
				<span id="txtHint"></span>
				</th>

				<th id="tripDescShow1" style="display:none;" valign="top">Trip Description:<br><input type="text" name="trip_desc1[1]" id="trip_desc1" class="required" /></th>



				<!-- End Slab Offer 1 -->


				


				</tr>


				<tr>
				<!-- Slab Offer 2 -->

				<th valign="top">Offer 2:<br>
					<select name="Dis_type1[2]" id="Dis_type2" class="styledselect_form_3" onchange="dtypeslab(this.value, 2);">
						<option value="0">Select</option>
						<option value="1">Percentage</option>
						<option value="2">Amount</option>
						<option value="3">FOC</option>
						<option value="4">Trip</option>
					</select>
				</th>	

				<th id="pershow2" style="display:none;" valign="top">Scheme Percentage:<br><input type="text" name="dis_per1[2]" id="dis_per2" class=" numberDE" value="" maxlength="3"/></th>

				<th id="amtshow2" style="display:none;" valign="top">Scheme Amount:<br><input type="text" name="dis_amt1[2]" id="dis_amt2" class=" numberDE" value="" maxlength="10"/></th>


				<th  id="focshow2" style="display:none;" valign="top" >Free Item:<br>
				<div class="ui-widget">
				<select class="free_item" name="free_item1[2]" style="display: none;">
				<option value="">Search</option>
				<?php $auCol=$_objAdmin->_getSelectList('table_item AS I',"I.item_id,I.item_name,I.item_code",''," I.status='A' and I.item_division='".$_GET['division_id']."' AND I.account_id=".$_SESSION['accountId']);
				for($i=0;$i<count($auCol);$i++){?>
				<option value="<?php echo $auCol[$i]->item_id;?>"><?php echo $auCol[$i]->item_name;?> <?php if($auCol[$i]->item_code !=''){ echo "(".$auCol[$i]->item_code.")"; }?></option>
				<?php } ?>
				</select>
				</div>
				<span class="style">Free Quantity:</span><br>
				<input name="disquanity1[2]" class="text" type="text numberDE" id="disquanity2" value="" maxlength="10" style="width:70px" />
				<input name="discountlistid1[2]" class="text" type="text" id="discountlistid2" value="" style="display:none;visibility:hidden" />
				<span id="txtHint"></span>
				</th>

				<th id="tripDescShow2" style="display:none;" valign="top">Trip Description:<br><input type="text" name="trip_desc1[2]" id="trip_desc2" class="" /></th>

				<!-- End Slab Offer 2 -->

				</tr>

				</table>
				</td>
				</th>
			</tr>




































			<tr class="withSlabWiseGroup" style="display:none;">
			<th>Slab 2: </th>
			<td colspan="4">
			<table border="0">
			<tr>

				<th valign="top">Minimum Quantity:<br><input type="text" name="min_qty1[2]" id="min_qty_2" class="numberDE" onkeypress="return blockSpecialChar(event)" maxlength="10"/></th>

				<th valign="top">Max Quantity:<br><input type="text" name="max_qty1[2]" id="max_qty_2" class="numberDE" onkeypress="return blockSpecialChar(event)" maxlength="10"/></th>



				<!-- Slab Offer 1 -->

				<th valign="top">Offer 1:<br>
					<select name="Dis_type2[1]" id="Dis_type3" class="styledselect_form_4" onchange="dtypeslab(this.value, 3);">
						<option value="0">Select</option>
						<option value="1">Percentage</option>
						<option value="2">Amount</option>
						<option value="3">FOC</option>
						<option value="4">Trip</option>
					</select>
				</th>	

				<th id="pershow3" style="display:none;" valign="top">Scheme Percentage:<br><input type="text" name="dis_per2[1]" id="dis_per3" class="numberDE" value="" maxlength="3"/></th>

				<th id="amtshow3" style="display:none;" valign="top">Scheme Amount:<br><input type="text" name="dis_amt2[1]" id="dis_amt3" class="numberDE" value="" maxlength="10"/></th>


				<th  id="focshow3" style="display:none;" valign="top" >Free Item:<br>
				<div class="ui-widget">
				<select class="free_item" name="free_item2[1]" style="display: none;">
				<option value="">Search</option>
				<?php $auCol=$_objAdmin->_getSelectList('table_item AS I',"I.item_id,I.item_name,I.item_code",''," I.status='A' and I.item_division='".$_GET['division_id']."' AND I.account_id=".$_SESSION['accountId']);
				for($i=0;$i<count($auCol);$i++){?>
				<option value="<?php echo $auCol[$i]->item_id;?>"><?php echo $auCol[$i]->item_name;?> <?php if($auCol[$i]->item_code !=''){ echo "(".$auCol[$i]->item_code.")"; }?></option>
				<?php } ?>
				</select>
				</div>
				<span class="style3">Free Quantity:</span><br>
				<input name="disquanity2[1]" class="text" type="text numberDE" id="disquanity3" value="" maxlength="10" style="width:70px" />
				<input name="discountlistid2[1]" class="text" type="text" id="discountlistid3" value="" style="display:none;visibility:hidden" />
				<span id="txtHint"></span>
				</th>

				<th id="tripDescShow3" style="display:none;" valign="top">Trip Description:<br><input type="text" name="trip_desc2[1]" id="trip_desc3" class="" /></th>



				<!-- End Slab Offer 1 -->


				


				</tr>


				<tr>
				<!-- Slab Offer 2 -->

				<th valign="top">Offer 2:<br>
					<select name="Dis_type2[2]" id="Dis_type4" class="styledselect_form_3" onchange="dtypeslab(this.value, 4);">
						<option value="0">Select</option>
						<option value="1">Percentage</option>
						<option value="2">Amount</option>
						<option value="3">FOC</option>
						<option value="4">Trip</option>
					</select>
				</th>	

				<th id="pershow4" style="display:none;" valign="top">Scheme Percentage:<br><input type="text" name="dis_per2[2]" id="dis_per4" class=" numberDE" value="" maxlength="3"/></th>

				<th id="amtshow4" style="display:none;" valign="top">Scheme Amount:<br><input type="text" name="dis_amt2[2]" id="dis_amt4" class=" numberDE" value="" maxlength="10"/></th>


				<th  id="focshow4" style="display:none;" valign="top" >Free Item:<br>
				<div class="ui-widget">
				<select class="free_item" name="free_item2[2]" style="display: none;">
				<option value="">Search</option>
				<?php $auCol=$_objAdmin->_getSelectList('table_item AS I',"I.item_id,I.item_name,I.item_code",''," I.status='A' and I.item_division='".$_GET['division_id']."' AND I.account_id=".$_SESSION['accountId']);
				for($i=0;$i<count($auCol);$i++){?>
				<option value="<?php echo $auCol[$i]->item_id;?>"><?php echo $auCol[$i]->item_name;?> <?php if($auCol[$i]->item_code !=''){ echo "(".$auCol[$i]->item_code.")"; }?></option>
				<?php } ?>
				</select>
				</div>
				<span class="style">Free Quantity:</span><br>
				<input name="disquanity2[2]" class="text" type="text numberDE" id="disquanity4" value="" maxlength="10" style="width:70px" />
				<input name="discountlistid2[2]" class="text" type="text" id="discountlistid4" value="" style="display:none;visibility:hidden" />
				<span id="txtHint"></span>
				</th>

				<th id="tripDescShow4" style="display:none;" valign="top">Trip Description:<br><input type="text" name="trip_desc2[2]" id="trip_desc4" class="" /></th>

				<!-- End Slab Offer 2 -->

				</tr>

				</table>
				</td>
				</th>
			</tr>













			<tr class="withSlabWiseGroup" style="display:none;">
			<th>Slab 3: </th>
			<td colspan="4">
			<table border="0">
			<tr>

				<th valign="top">Minimum Quantity:<br><input type="text" name="min_qty1[3]" id="min_qty_3" class="numberDE" onkeypress="return blockSpecialChar(event)" maxlength="10"/></th>

				<th valign="top">Max Quantity:<br><input type="text" name="max_qty1[3]" id="max_qty_3" class="numberDE" onkeypress="return blockSpecialChar(event)" maxlength="10"/></th>



				<!-- Slab Offer 1 -->

				<th valign="top">Offer 1:<br>
					<select name="Dis_type3[1]" id="Dis_type5" class="styledselect_form_4" onchange="dtypeslab(this.value, 5);">
						<option value="0">Select</option>
						<option value="1">Percentage</option>
						<option value="2">Amount</option>
						<option value="3">FOC</option>
						<option value="4">Trip</option>
					</select>
				</th>	

				<th id="pershow5" style="display:none;" valign="top">Scheme Percentage:<br><input type="text" name="dis_per3[1]" id="dis_per5" class="numberDE" value="" maxlength="3"/></th>

				<th id="amtshow5" style="display:none;" valign="top">Scheme Amount:<br><input type="text" name="dis_amt3[1]" id="dis_amt5" class="numberDE" value="" maxlength="10"/></th>


				<th  id="focshow5" style="display:none;" valign="top" >Free Item:<br>
				<div class="ui-widget">
				<select class="free_item" name="free_item3[1]" style="display: none;">
				<option value="">Search</option>
				<?php $auCol=$_objAdmin->_getSelectList('table_item AS I',"I.item_id,I.item_name,I.item_code",''," I.status='A' and I.item_division='".$_GET['division_id']."' AND I.account_id=".$_SESSION['accountId']);
				for($i=0;$i<count($auCol);$i++){?>
				<option value="<?php echo $auCol[$i]->item_id;?>"><?php echo $auCol[$i]->item_name;?> <?php if($auCol[$i]->item_code !=''){ echo "(".$auCol[$i]->item_code.")"; }?></option>
				<?php } ?>
				</select>
				</div>
				<span class="style5">Free Quantity:</span><br>
				<input name="disquanity3[1]" class="text" type="text numberDE" id="disquanity5" value="" maxlength="10" style="width:70px" />
				<input name="discountlistid3[1]" class="text" type="text" id="discountlistid5" value="" style="display:none;visibility:hidden" />
				<span id="txtHint"></span>
				</th>

				<th id="tripDescShow5" style="display:none;" valign="top">Trip Description:<br><input type="text" name="trip_desc3[1]" id="trip_desc5" class="" /></th>



				<!-- End Slab Offer 1 -->


				


				</tr>


				<tr>
				<!-- Slab Offer 2 -->

				<th valign="top">Offer 2:<br>
					<select name="Dis_type3[2]" id="Dis_type6" class="styledselect_form_3" onchange="dtypeslab(this.value, 6);">
						<option value="0">Select</option>
						<option value="1">Percentage</option>
						<option value="2">Amount</option>
						<option value="3">FOC</option>
						<option value="4">Trip</option>
					</select>
				</th>	

				<th id="pershow6" style="display:none;" valign="top">Scheme Percentage:<br><input type="text" name="dis_per3[2]" id="dis_per6" class=" numberDE" value="" maxlength="3"/></th>

				<th id="amtshow6" style="display:none;" valign="top">Scheme Amount:<br><input type="text" name="dis_amt3[2]" id="dis_amt6" class=" numberDE" value="" maxlength="10"/></th>


				<th  id="focshow6" style="display:none;" valign="top" >Free Item:<br>
				<div class="ui-widget">
				<select class="free_item" name="free_item3[2]" style="display: none;">
				<option value="">Search</option>
				<?php $auCol=$_objAdmin->_getSelectList('table_item AS I',"I.item_id,I.item_name,I.item_code",''," I.status='A' and I.item_division='".$_GET['division_id']."' AND I.account_id=".$_SESSION['accountId']);
				for($i=0;$i<count($auCol);$i++){?>
				<option value="<?php echo $auCol[$i]->item_id;?>"><?php echo $auCol[$i]->item_name;?> <?php if($auCol[$i]->item_code !=''){ echo "(".$auCol[$i]->item_code.")"; }?></option>
				<?php } ?>
				</select>
				</div>
				<span class="style">Free Quantity:</span><br>
				<input name="disquanity3[2]" class="text" type="text numberDE" id="disquanity6" value="" maxlength="10" style="width:70px" />
				<input name="discountlistid3[2]" class="text" type="text" id="discountlistid6" value="" style="display:none;visibility:hidden" />
				<span id="txtHint"></span>
				</th>

				<th id="tripDescShow6" style="display:none;" valign="top">Trip Description:<br><input type="text" name="trip_desc3[2]" id="trip_desc6" class="" /></th>

				<!-- End Slab Offer 2 -->

				</tr>

				</table>
				</td>
				</th>
			</tr>



































