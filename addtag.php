<?php
include("includes/config.inc.php");
include("includes/function.php");
$_objAdmin = new Admin();


if(isset($_POST['add']) && $_POST['add'] == 'yes')
{
	if($_REQUEST['id']!="") 
	{
		$condi=	" tag_description='".$_POST['tag_description']."' and tag_type='".$_POST['tag_type']."' and status='A'  and tag_id<>'".$_REQUEST['id']."'";
	}
	else
	{
		$condi=	" tag_description='".$_POST['tag_description']."' and tag_type='".$_POST['tag_type']."' and status='A' ";
	}
	$auRec=$_objAdmin->_getSelectList('table_tags',"*",'',$condi);
	if(is_array($auRec))
	{
		$err="Tag already exists in the system.";	
		$auRec[0]=(object)$_POST;						
	}
	else
	{
		if($_REQUEST['id']!="") 
		{
			$cid=$_objAdmin->updateTag($_REQUEST['id']);
			$sus="Tag has been updated successfully.";
		}
		else 
		{
			$cid=$_objAdmin->addTag();
			$sus="Tag has been added successfully.";
		}
	}
}


if(isset($_REQUEST['id']) && $_REQUEST['id']!="")
{
	$auRec=$_objAdmin->_getSelectList('table_tags',"*",''," tag_id=".$_REQUEST['id']);
	if(count($auRec)<=0) header("Location: tag_list.php");
}


?>
<?php include("header.inc.php") ?>
<script type="text/javascript" src="javascripts/validate.js"></script>
<div class="clear"></div>
<!-- start content-outer -->
<div id="content-outer">
<!-- start content -->
	<div id="content">
<div id="page-heading"><h1><span style="color: #d74343; font-family: Tahoma; font-weight: bold;">No Order Reason</span></h1></div>
		<table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">		
			<tr>			
				<td>			
			<!--  start content-table-inner -->
					<div id="content-table-inner">			
				<table border="0" width="100%" cellpadding="0" cellspacing="0">
				<tr valign="top">
					<td>
					<!--  start message-red -->
					<?php if($err!=''){?>
					<div id="message-red">
						<table border="0" width="100%" cellpadding="0" cellspacing="0">
						<tr>
							<td class="red-left"> <?php echo $err; ?></td>
							<td class="red-right"><a class="close-red"><img src="images/icon_close_red.gif"   alt="" /></a></td>
						</tr>
						</table>
					</div>
					<?php } ?>
					<!--  end message-red -->
					<?php if($sus!=''){?>
					<!--  start message-green -->
					<div id="message-green">
						<table border="0" width="100%" cellpadding="0" cellspacing="0">
						<tr>
							<td class="green-left"><?php echo $sus; ?></td>
							<td class="green-right"><a class="close-green"><img src="images/icon_close_green.gif"   alt="" /></a></td>
						</tr>
						</table>
					</div>
					<?php } ?>
					<!--  end message-green -->
					
			<!-- start id-form -->
				<form name="frmPre" id="frmPre" method="post" action="addtag.php" enctype="multipart/form-data" >
			<table border="0" cellpadding="0" cellspacing="0"  id="id-form">
				<tr>
					<th valign="top">Description:</th>
					<td><input type="text" name="tag_description" id="tag_description" class="required" value="<?php echo $auRec[0]->tag_description; ?>"  maxlength="250" /></td>
					<td></td>
				</tr>
				<tr>
					<th valign="top">Status:</th>
					<td>
					<select name="status" id="status" class="styledselect_form_3">
					<option value="A" <?php if($auRec[0]->status=='A') echo "selected";?> >Active</option>
					<option value="I" <?php if($auRec[0]->status=='I') echo "selected";?> >Inactive</option>
					</select>
					</td>
					<td></td>
				</tr>
				<tr>
					<th>&nbsp;</th>
					<td valign="top">
						<input name="account_id" type="hidden" value="<?php echo $_SESSION['accountId']; ?>" />
						<input name="id" type="hidden" value="<?php echo $auRec[0]->tag_id; ?>" />
						<input name="tag_type" type="hidden" value="3" />
						<input name="add" type="hidden" value="yes" />
						<input type="reset" value="Reset!" class="form-reset">
						<input type="button" value="Back" class="form-reset" onclick="location.href='tag_list.php';" />
						<input name="submit" class="form-submit" type="submit" id="submit" value="Save" />
						
					</td>
				</tr>
			</table>
		</form>
			<!-- end id-form  -->
				</td>
				</tr>
		</table>		
			<!-- right bar-->
			</div>
			<!--  end content-table-inner  -->
				</td>
				<td id="tbl-border-right"></td>
			</tr>
		</table>
<div class="clear">&nbsp;</div>

</div>
<!--  end content -->
<div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->
<div class="clear">&nbsp;</div>
    
<!-- start footer --> 
<?php include("footer.php") ?>  
<!-- end footer -->
</body>
</html>