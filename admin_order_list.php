<?php
include("includes/config.inc.php");
include("includes/function.php");

	
$_objAdmin = new Admin();
$page_name="Bookings Report";

if(isset($_POST['export']) && $_POST['export']) {
   header("Location:export.inc.php?export_bookings");
   exit;
}
if(isset($_POST['showOrderlist']) && $_POST['showOrderlist'] == 'yes'){
    
    if($_POST['cleaner_id']!="") {
        $_SESSION['cleaner_id']=$_POST['cleaner_id'];
    }
    if($_POST['booking_type']!="0") {
        $_SESSION['booking_type']=$_POST['booking_type'];
    }
    if($_POST['booking_status']!="") {
        $_SESSION['booking_status']=$_POST['booking_status'];
    }
    if($_POST['from']!="") {
        $_SESSION['from']=$_POST['from'];
    }
    if($_POST['to']!="") {
        $_SESSION['to']=$_POST['to'];
    }
    if($_POST['city_id']!="") {
        $_SESSION['city_id']=$_POST['city_id'];
    }
    if($_POST['area_id']!="") {
        $_SESSION['area_id']=$_POST['area_id'];
    }
    if($_POST['cleaning_type_id']!="") {
        $_SESSION['cleaning_type_id']=$_POST['cleaning_type_id'];
    }
    header("Location: admin_order_list.php?show=yes");
    
}
if(isset($_REQUEST['reset']) && $_REQUEST['reset']=="yes")
{
    $_SESSION['cleaner_id']="";
    $_SESSION['booking_type']="";
    $_SESSION['booking_status']="";
    $_SESSION['from']="";
    $_SESSION['to']="";
    $_SESSION['city_id']="";
    $_SESSION['area_id']="";
    $_SESSION['cleaning_type_id']="";
}
if(isset($_REQUEST['show']) && $_REQUEST['show']=="yes")
{
    
    $_objAdmin->showBookingList();
    die;
}


?>




<?php include("header.inc.php") ?>
<script src="javascripts/dateNextPrev.js" type="text/javascript"></script>

<!-- start content-outer -->
<input name="pagename" type="hidden"  id="pagename" value="admin_order_list.php" />
<div id="content-outer">
<!-- start content -->
<div id="content">


<div id="dialog" title="Push To Tally" style="display:none;">
  <p><center><img src="images/ajax-loader.gif" /></center></p>
</div>


<div id="dialog2" title="Pull From Tally" style="display:none;">
  <p><center><img src="images/ajax-loader.gif" /></center></p>
</div>

<div id="page-heading"><h1><span style="color: #d74343; font-family: Tahoma; font-weight: bold;">Bookings Report</span></h1></div>
<table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">

<tr>
	<td>
	<!--  start content-table-inner -->
	<div id="content-table-inner">
	
	<div id="page-heading" align="left" >
	
	<form name="submitOrderby" method="post" action="#" enctype="multipart/form-data" >
	<table border="0" width="72%" cellpadding="0" cellspacing="0">
		<tr  align="left" style="padding-button:10px;">
			<td >
			<h4><input type="radio" name="orderby" value="1" checked="checked" onchange="javascript:document.submitOrderby.submit()"> &nbsp;Booking Through App&nbsp;&nbsp;</h4>
			</td>
			<td >
			<h4><input type="radio" name="orderby" value="2" <?php if($_SESSION['OrderBy']==2){ ?>checked="checked" <?php } ?> onchange="javascript:document.submitOrderby.submit()"  > &nbsp;Booking Through Web&nbsp;&nbsp;</h4></td>
			<td></td>
			<td></td>
			
		</tr>
	</table>
	</form>
	</br>
	<form name="frmPre1" id="frmPre1" method="post" action="admin_order_list.php" enctype="multipart/form-data" >
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
	<tr>
		<td>
			<h3>&nbsp;&nbsp;Cleaner:</h3>
			<h6>
		  		<select name="cleaner_id" id="cleaner_id" class="styledselect_form_5" style="width:150px;" >
					<option value="">Please Select</option>
					<?php $cleaner = $_objAdmin->_getSelectList2('table_cleaner','cleaner_id,cleaner_name',''," status = 'A' order by cleaner_name"); 
						if(is_array($cleaner)){
						 foreach($cleaner as $value):?>
						 	<option value="<?php echo $value->cleaner_id;?>" <?php if($value->cleaner_id==$auRec[0]->cleaner_id) echo "selected";?> ><?php echo $value->cleaner_name;?></option>
					<?php endforeach; }?>
				</select>
			</h6>
		</td>
		<td>
			<h3>&nbsp;&nbsp;Booking Type:</h3>
			<h6>
				<select name="booking_type" id="booking_type" class="styledselect_form_5" style="width:150px;" >
					<option value="0">All</option>
					<option value="1">Contract</option>
					<option value="2">One Time</option>
				</select>
			</h6>
		</td>
		<!-- <td>
			<h3>&nbsp;&nbsp;Customer Class:</h3>
			<h6>
				<select name="customer_class" id="customer_class" class="menulist">
					<option value="all">All</option>
				</select>
			</h6>
		</td> -->
		<td>
			<h3>&nbsp;&nbsp;Booking Status:</h3>
			<h6>
		  		<select name="booking_status" class="styledselect_form_5" style="width:150px;" >
		  			<option value="">All</option>
		  			<option value="UA" >Not Alloted</option>
				<option value="AL"  >Alloted</option>
                                <option value="AC" >Accepted</option>
                                <option value="RJ" >Rejected</option>
                                <option value="RD">Reached</option>
                                <option value="SC" >Start Cleaning</option>
                                <option value="CC" >Complete Cleaning</option>
                                <option value="RP"  >Payment Received</option>
                                <option value="CB" >Complete Booking</option>
                                <option value="CL" >Cancel Booking</option>
                                
		  		</select>
			</h6>
		</td>
		<td>
			<h3>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;From Date:</h3>
			<h6>
				<img src="css/images/prev.png" height="18" width="18" onclick="dateFromPrev();"> 
				<input type="text" id="from" name="from" class="date" style="width:100px" value="<?php if($_SESSION['booking_from']!='') { echo $_objAdmin->_changeDate($_SESSION['booking_from']); } else { echo $_objAdmin->_changeDate(date('Y-m-d')); }?>"  readonly /> 
				<img src="css/images/next.png" height="18" width="18" onclick="dateFromNext();">
			</h6>
		</td>
		<td>
			<h3>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;To Date:</h3>
			<h6>
				<img src="css/images/prev.png" height="18" width="18" onclick="dateToPrev();"> 
				<input type="text" id="to" name="to" class="date" style="width:100px" value="<?php if($_SESSION['booking_to']!='') { echo $_objAdmin->_changeDate($_SESSION['booking_to']); } else { echo $_objAdmin->_changeDate(date('Y-m-d')); }?>"  readonly /> 
				<img src="css/images/next.png" height="18" width="18" onclick="dateToNext();">
			</h6>
		</td>
		<td>
			<h3></h3>
				
		</td>
	   <!-- <td colspan="2"></td> -->
	</tr>
	<tr>
		<td>
			<h3>City: </h3>
			<h6>
				<h6>
				 <select name="city_id" id="city_id" class="menulist">
                    <option value="all">All</option>
                    <?php $cityss = $_objAdmin->_getSelectList2('city','city_id,city_name',''," order by city_name"); 

						if(is_array($cityss)){
						 foreach($cityss as $value):?>
						 	<option value="<?php echo $value->city_id;?>" <?php if($value->city_id==$auRec[0]->city_id) echo "selected";?> ><?php echo $value->city_name;?></option>
					<?php endforeach; }?>
                </select>
			</h6>
			</h6>
		</td>
		<td>
			<h3>Area: </h3>
			<h6>
				 <select name="area_id" id="area_id" class="menulist">
                    <option value="all">All</option>
                    <?php $area = $_objAdmin->_getSelectList2('table_area','area_id,area_name',''," order by area_name"); 

						if(is_array($area)){
						 foreach($area as $value):?>
						 	<option value="<?php echo $value->area_id;?>" <?php if($value->area_id==$auRec[0]->area_id) echo "selected";?> ><?php echo $value->area_name;?></option>
					<?php endforeach; }?>
                </select>
			</h6>
		</td>

		<td>
			<h3>Type of Cleaning: </h3>
			<h6>
				<select name="cleaning_type_id" id="cleaning_type_id" class="menulist">
					<option value="all">All</option>
					<?php $cleaningType = $_objAdmin->_getSelectList2('table_cleaning_type','cleaning_type_id,cleaning_type',''," order by cleaning_type"); 

						if(is_array($cleaningType)){
						 foreach($cleaningType as $value):?>
						 	<option value="<?php echo $value->cleaning_type_id;?>" <?php if($value->cleaning_type_id==$auRec[0]->cleaning_type_id) echo "selected";?> ><?php echo $value->cleaning_type;?></option>
					<?php endforeach; }?>
				</select>
			</h6>
		</td>
	</tr>
	
	<tr>
		<td colspan="6" >
		<input name="showOrderlist" type="hidden" value="yes" />
			<input name="export" class="result-submit" type="submit" value="Export to Excel" id="export" onclick=""  >
			<input name="submit" class="result-submit" type="submit" id="submit" value="View Details" />
		  	<input type="button" value="Reset!" class="form-reset" onclick="location.href='admin_order_list.php?reset=yes';" />

		<!-- <a href='admin_order_list_year_graph.php?y=
		</td>
		 <!-- <td colspan="5"></td> -->
	</tr>
	
	</table>
	</form>
	</div>
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
	<tr valign="top">
	<td>
		<!-- start id-form -->
		<table  border="0" cellspacing="0" cellpadding="0"  align="center" width="100%">
				<tr>
				<td align="lift" valign="top"  >
				<table id="flex1" style="display:none"></table>
				<script type="text/javascript">showBookingList();</script>
				</td>
				</tr>
				</table>
	<!-- end id-form  -->

	</td>

</tr>
<tr>
<td><img src="images/blank.gif" width="695" height="1" alt="blank" /></td>
<td></td>
</tr>
</table>
<div class="clear"></div>
</div>
<!--  end content-table-inner  -->
</td>
<td id="tbl-border-right"></td>
</tr>
<!--<tr>
	<th class="sized bottomleft"></th>
	<td id="tbl-border-bottom">&nbsp;</td>
	<th class="sized bottomright"></th>
</tr>-->
</table>
<div class="clear">&nbsp;</div>


	
	
	
	
</div>
<!--  end content -->
<div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->
<div class="clear">&nbsp;</div> 
<!-- start footer -->         
<?php include("footer.php") ?>
<!-- end footer -->
<script src="javascripts/jquery-1.8.2.js" type="text/javascript"></script>
<script src="javascripts/jquery-ui.js"></script>
<script type="text/javascript" src="javascripts/validate.js"></script>
<script type="text/javascript"  language="javascript" src="javascripts/jquery.printarea.js"></script>
<script type="text/javascript">
function exportexcel() {
     window.location.href="export.inc.php?=yes"; 
}

</script>
</body>
</html>