<?php
include("includes/config.inc.php");
include("includes/function.php");
include("includes/globalarraylist.php");


$d1=0; $d2=0; $d3=0; $d4=0; $d5=0; $d6=0; $d7=0; $d8=0; $d9=0; $d10=0;
$d11=0; $d12=0; $d13=0; $d14=0; $d15=0; $d16=0; $d17=0; $d18=0; $d19=0; $d20=0;
$d21=0; $d22=0; $d23=0; $d24=0; $d25=0; $d26=0; $d27=0; $d28=0; $d29=0; $d30=0; $d31=0;

if($_REQUEST['salID']!=''){
	$salesman=" and s.salesman_id='".$_REQUEST['salID']."'";
} 


if($_SESSION['FromOrderList']!=''){
	$fromdate=" and monthname(o.date_of_order)='$_REQUEST[m]'";
}
else
{
	$fromdate=" and monthname(o.date_of_order)='$_REQUEST[m]'";
}
if($_SESSION['ToOrderList']!=''){
	$todate=" and EXTRACT(YEAR FROM o.date_of_order)='$_REQUEST[y]'";
}
else
{
	$todate=" and EXTRACT(YEAR FROM o.date_of_order)='$_REQUEST[y]'";
}
if($_SESSION['OrderBy']!=''){
	if($_SESSION['OrderBy']==1){
	$orderby="";
	}
	if($_SESSION['OrderBy']==2){
	$orderby=" and o.order_type='Adhoc' ";
	}
	if($_SESSION['OrderBy']==3){
	$orderby=" and o.order_type!='Adhoc' ";
	}
}
else
{
	$orderby="";
}
//$where = " 1=1 ";
$where = " r.new='' $salesman $fromdate $todate $orderby group by day(o.date_of_order)";
$graph=$_objAdmin->_getSelectList('table_order as o left join table_retailer as r on o.retailer_id=r.retailer_id left join table_salesman as s on o.salesman_id=s.salesman_id left join table_distributors as d on o.distributor_id=d.distributor_id',"sum(o.acc_total_invoice_amount) as amt, s.salesman_name, day(o.date_of_order) as day",$rp,$where.$sort,'');
//print_r($graph);
for($i=0; $i<count($graph); $i++)
{
	if($graph[$i]->day=='1') { $d1 = $graph[$i]->amt; }
	if($graph[$i]->day=='2') { $d2 = $graph[$i]->amt; }
	if($graph[$i]->day=='3') { $d3 = $graph[$i]->amt; }
	if($graph[$i]->day=='4') { $d4 = $graph[$i]->amt; }
	if($graph[$i]->day=='5') { $d5 = $graph[$i]->amt; }
	if($graph[$i]->day=='6') { $d6 = $graph[$i]->amt; }
	if($graph[$i]->day=='7') { $d7 = $graph[$i]->amt; }
	if($graph[$i]->day=='8') { $d8 = $graph[$i]->amt; }
	if($graph[$i]->day=='9') { $d9 = $graph[$i]->amt; }
	if($graph[$i]->day=='10') { $d10 = $graph[$i]->amt; }
	if($graph[$i]->day=='11') { $d11 = $graph[$i]->amt; }
	if($graph[$i]->day=='12') { $d12 = $graph[$i]->amt; }
	if($graph[$i]->day=='13') { $d13 = $graph[$i]->amt; }
	if($graph[$i]->day=='14') { $d14 = $graph[$i]->amt; }
	if($graph[$i]->day=='15') { $d15 = $graph[$i]->amt; }
	if($graph[$i]->day=='16') { $d16 = $graph[$i]->amt; }
	if($graph[$i]->day=='17') { $d17 = $graph[$i]->amt; }
	if($graph[$i]->day=='18') { $d18 = $graph[$i]->amt; }
	if($graph[$i]->day=='19') { $d19 = $graph[$i]->amt; }
	if($graph[$i]->day=='20') { $d20 = $graph[$i]->amt; }
	if($graph[$i]->day=='21') { $d21 = $graph[$i]->amt; }
	if($graph[$i]->day=='22') { $d22 = $graph[$i]->amt; }
	if($graph[$i]->day=='23') { $d23 = $graph[$i]->amt; }
	if($graph[$i]->day=='24') { $d24 = $graph[$i]->amt; }
	if($graph[$i]->day=='25') { $d25 = $graph[$i]->amt; }
	if($graph[$i]->day=='26') { $d26 = $graph[$i]->amt; }
	if($graph[$i]->day=='27') { $d27 = $graph[$i]->amt; }
	if($graph[$i]->day=='28') { $d28 = $graph[$i]->amt; }
	if($graph[$i]->day=='29') { $d29 = $graph[$i]->amt; }
	if($graph[$i]->day=='30') { $d30 = $graph[$i]->amt; }
	if($graph[$i]->day=='31') { $d31 = $graph[$i]->amt; }
}


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
	<title>Quytech PepUpSales - Daily Order List Report Graph</title>
	<?php include_once('graph/header-files.php');?>

    <script type="text/javascript">
      google.load('visualization', '1', {packages: ['corechart']});
    </script>
    <script type="text/javascript">
      function drawVisualization() {
        // Some raw data (not necessarily accurate)
        var data = google.visualization.arrayToDataTable([
          ['day', 'Total invoice amount'],
          ['1',  <?php echo $d1; ?> ],
          ['2',  <?php echo $d2; ?> ],
          ['3',  <?php echo $d3; ?> ],
          ['4',  <?php echo $d4; ?> ],
		  ['5',  <?php echo $d5; ?> ],
		  ['6',  <?php echo $d6; ?> ],
		  ['7',  <?php echo $d7; ?> ],
		  ['8',  <?php echo $d8; ?> ],
		  ['9',  <?php echo $d9; ?> ],
		  ['10',  <?php echo $d10; ?> ],
		  ['11',  <?php echo $d11; ?> ],
		  ['12',  <?php echo $d12; ?> ],
		  ['13',  <?php echo $d13; ?> ],
		  ['14',  <?php echo $d14; ?> ],
		  ['15',  <?php echo $d15; ?> ],
		  ['16',  <?php echo $d16; ?> ],
		  ['17',  <?php echo $d17; ?> ],
		  ['18',  <?php echo $d18; ?> ],
		  ['19',  <?php echo $d19; ?> ],
          ['20',  <?php echo $d20; ?> ],
		  ['21',  <?php echo $d21; ?> ],
		  ['22',  <?php echo $d22; ?> ],
		  ['23',  <?php echo $d23; ?> ],
		  ['24',  <?php echo $d24; ?> ],
		  ['25',  <?php echo $d25; ?> ],
		  ['26',  <?php echo $d26; ?> ],
		  ['27',  <?php echo $d27; ?> ],
		  ['28',  <?php echo $d28; ?> ],
		  ['29',  <?php echo $d29; ?> ],
          ['30',  <?php echo $d30; ?> ],
          ['31',  <?php echo $d31; ?> ]
        ]);

        var options = {
          title : 'Daily Order List',
          vAxis: {title: "Order" , gridlines: { count: 8  }, minValue:0},
          hAxis: {title: "<?php echo $ARR_MONTHS[09]; ?>"},
          seriesType: "bars",
          series: {5: {type: "line"}}
        };

        var chart = new google.visualization.ComboChart(document.getElementById('chart_div'));
        chart.draw(data, options);
      }
      google.setOnLoadCallback(drawVisualization);
    </script>
  </head>
  <body>
<!-- Start: page-top-outer -->
<?php include_once('graph/header.php');?>
<!-- End: page-top-outer -->

<div id="content-outer">
<div id="content">
<div id="page-heading"><h1>Daily Order List Report Graph of <?php echo $_REQUEST['m'];?> <?php echo $_REQUEST['y'];?></h1></div>
<div id="container">
<form name="frmPre" id="frmPre" method="post" action="<?php echo $_SERVER['PHP_SELF']?>" enctype="multipart/form-data">
<table width="100%">
 <tr>
 
	<td width="17%" valign="bottom" align="right"><h3>Salesman :</h3></td>
    <td width="12%" align="left">
	<select name="salID" id="salID" class="styledselect_form_5" >
		<?php echo $rtndata = $_objArrayList->GetSalesmenMenu($salsList, $_REQUEST['salID']);?>
	</select>
	</td>
 
 
  	<td width="8%" valign="bottom" align="right"><h3> Month :</h3></td>
    <td width="14%" align="left">
	<select name="m" id="m" onchange="newMonth(this.value)" class="styledselect_form_5" >
			<?php echo $months = $_objArrayList->getMonthList2($_REQUEST['m']);?>
		</select>
	</td>

	<td width="6%" valign="bottom" align="right"><h3>Year :</h3></td>
    <td width="15%" align="left">
		<select name="y" id="y"  class="styledselect_form_5" >
			<?php echo $year = $_objArrayList->getYearList2($_REQUEST['y']);?>
		</select>
	</td>


	<td width="49%" valign="bottom"><input name="submit" class="result-submit" type="submit" id="submit" value="Show graph" /></td>
	</tr>
  <tr>
    <td colspan="7"><div id="chart_div" style="width: 100%; height: 450px;"></div></td>
  </tr>
</table>
</form>
</div>
	
<div class="clear">&nbsp;</div>
	
</div>
</div>
	<!-- Graph code ends here -->
	<?php include("footer.php") ?>
  </body>
</html>