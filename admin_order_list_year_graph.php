<?php
include("includes/config.inc.php");
include("includes/function.php");
include("includes/globalarraylist.php");

?>
<!-- Graph code starts here -->
<?php
if($_REQUEST['y']!=''){
	$fromdate=" and o.date_of_order >= '".$_REQUEST['y']."-1-1"."'";
	$todate=" and o.date_of_order <= '".$_REQUEST['y']."-12-31"."'";
}

if($_SESSION['OrderBy']!=''){
	if($_SESSION['OrderBy']==1){
	$orderby="";
	}
	if($_SESSION['OrderBy']==2){
	$orderby=" and o.order_type='Adhoc' ";
	}
	if($_SESSION['OrderBy']==3){
	$orderby=" and o.order_type!='Adhoc' ";
	}
}
else
{
	$orderby="";
}

if($_REQUEST['salID']!=''){

    $salesman = " AND s.salesman_id = ".$_REQUEST['salID'];

}


$where = " r.new='' $salesman $fromdate $todate $orderby group by MONTH(o.date_of_order)";
$graph=$_objAdmin->_getSelectList('table_order as o left join table_retailer as r on o.retailer_id=r.retailer_id left join table_salesman as s on o.salesman_id=s.salesman_id left join table_distributors as d on o.distributor_id=d.distributor_id',"sum(o.acc_total_invoice_amount) as amt, s.salesman_name, monthname(o.date_of_order) as month",$rp,$where.$sort,'');


//echo count($graph);
$jan = 0; $feb = 0; $march = 0; $april = 0; $may = 0; $june = 0; $july = 0; $aug = 0; $sep = 0; $oct = 0; $nov = 0; $dec = 0;
for($i=0; $i<count($graph); $i++)
{
	if($graph[$i]->month=='January') { $jan = $graph[$i]->amt; }
	if($graph[$i]->month=='February') { $feb = $graph[$i]->amt; }
	if($graph[$i]->month=='March') { $march = $graph[$i]->amt; }
	if($graph[$i]->month=='April') { $april = $graph[$i]->amt; }
	if($graph[$i]->month=='May') { $may = $graph[$i]->amt; }
	if($graph[$i]->month=='June') { $june = $graph[$i]->amt; }
	if($graph[$i]->month=='July') { $july = $graph[$i]->amt; }
	if($graph[$i]->month=='August') { $aug = $graph[$i]->amt; }
	if($graph[$i]->month=='September') { $sep = $graph[$i]->amt; }
	if($graph[$i]->month=='October') { $oct = $graph[$i]->amt; }
	if($graph[$i]->month=='November') { $nov = $graph[$i]->amt; }
	if($graph[$i]->month=='December') { $dec = $graph[$i]->amt; }
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta charset="utf-8">
<title>Quytech PepUpSales - Yearly Order List Report Graph</title>
<?php include_once('graph/header-files.php');?>
    <script type="text/javascript">
      google.load('visualization', '1', {packages: ['corechart']});
	 
    </script>
    <script type="text/javascript">
      function drawVisualization() {
        // Some raw data (not necessarily accurate)
        var data = google.visualization.arrayToDataTable([
          ['Month', 'Total invoice amount'],
          ['January',  <?php echo $jan; ?>],
          ['February',  <?php echo $feb; ?>],
          ['March',  <?php echo $march; ?>],
          ['April',  <?php echo $april; ?>],
		  ['May',  <?php echo $may; ?>],
          ['June',  <?php echo $june; ?>],
          ['July',  <?php echo $july; ?>],
          ['August',  <?php echo $aug; ?>],
		  ['September',  <?php echo $sep; ?>],
		  ['October',  <?php echo $oct; ?>],
		  ['November',  <?php echo $nov; ?>],
		  ['December',  <?php echo $dec; ?>]
        ]);

        var options = {
          title : 'Monthly Order List',
          vAxis: {title: "Order" , gridlines: { count: 8  }, minValue:0},
          hAxis: {title: "Month"},
          seriesType: "bars",
          series: {1: {type: "line"}}
        };

        var chart = new google.visualization.ComboChart(document.getElementById('chart_div'));
		/* click handler ends */
		 function selectHandler() {
          var selectedItem = chart.getSelection()[0];
          if (selectedItem) {
            var topping = data.getValue(selectedItem.row, 0);
            var url = 'admin_order_list_month_graph.php?m='+topping+'&salID=<?php echo $_REQUEST['salID'];?>&y='+<?php echo $_REQUEST['y']; ?>;
			OpenInNewTab(url);
          }
        }

        google.visualization.events.addListener(chart, 'click', selectHandler);   
		
		/* click handler ends */
        chart.draw(data, options);
      }
      google.setOnLoadCallback(drawVisualization);
    </script>
	
	</head>
<body> 
<!-- Start: page-top-outer -->

<?php include_once('graph/header.php');?>
<!-- End: page-top-outer -->
	
<div id="content-outer">
<div id="content">
<div id="page-heading"><h1>Yearly Order List Report Graph</h1></div>
<div id="container">
<form name="frmPre" id="frmPre" method="post" action="<?php echo $_SERVER['PHP_SELF']?>" enctype="multipart/form-data">
<table width="100%" border="0">
  <tr>
  
  
	<td width="15%" valign="bottom" align="right"><h3>Salesman :</h3></td>
    <td width="12%" align="left">
	<select name="salID" id="salID" class="styledselect_form_5" >
		<?php echo $rtndata = $_objArrayList->GetSalesmenMenu($salsList, $_REQUEST['salID']);?>
	</select>
	</td>
  
  
  
  	<td width="6%" valign="bottom" align="right"><h3>Year :</h3></td>
    <td width="15%" align="left">
		<select name="y" id="y"  class="styledselect_form_5" >
			<?php echo $year = $_objArrayList->getYearList2($_REQUEST['y']);?>
		</select>
	</td>
	
	<td width="52%" valign="bottom"><input name="submit" class="result-submit" type="submit" id="submit" value="Show graph" /></td>
	
  </tr>
  
  
  
  
  <tr>
    <td colspan="5"><div id="chart_div" style="width: 100%; height: 450px;"></div></td>
  </tr>
</table>
</form>
</div>
	
<div class="clear">&nbsp;</div>
	
</div>
</div>
	<!-- Graph code ends here -->
	<?php include("footer.php") ?>
	<script type="text/javascript">
		function OpenInNewTab(url )
		{
		  var win=window.open(url, '_blank');
		  win.focus();
		}
		function newYear(val)
		{
			window.location = 'admin_order_list_year_graph.php?y='+val;
		}
	</script>

	  </body>
</html>