<?php
include("includes/config.inc.php");
include("includes/function.php");
$_objAdmin = new Admin();
$page_name="Order List";
if(isset($_REQUEST['id']) && $_REQUEST['id']!="" ){
	$_SESSION['orderdetails']=$_REQUEST['id'];	
	header("Location: order_list.php?searchParam_1=".$_REQUEST['searchParam_1']."&searchParam_2=".$_REQUEST['searchParam_2']."&searchParam_3=".$_REQUEST['searchParam_3']);
}

if(isset($_POST['showOrderlist']) && $_POST['showOrderlist'] == 'yes')
{	
	if($_POST['sal']!="A") 
	{
	$_SESSION['SalOrderList']=$_POST['sal'];	
	}
	if($_POST['from']!="") 
	{
	$_SESSION['FromOrderList']=$_POST['from'];	
	}
	if($_POST['to']!="") 
	{
		$_SESSION['ToOrderList']=$_POST['to'];	
	}
	
	if($_POST['sal']=="A") 
	{
	  unset($_SESSION['SalOrderList']);	
	}
	
	header("Location: admin_order_list.php");
}
if(isset($_POST['showOrderBy']) && $_POST['showOrderBy'] == 'yes')
{	
	
	$_SESSION['OrderBy']=$_POST['orderby'];
	header("Location: admin_order_list.php");
}
if(isset($_REQUEST['salid']) && $_REQUEST['salid']!="" ){
	$auSal=$_objAdmin->_getSelectList('table_order',"salesman_id",''," order_id='".$_REQUEST['salid']."'");
	$_SESSION['SalOrderList']=$auSal[0]->salesman_id;
		 header("Location: admin_order_list.php");
} 

if(isset($_REQUEST['resetsal']) && $_REQUEST['resetsal'] == 'yes' ){
	 unset($_SESSION['SalOrderList']);	
	// header("Location: admin_order_list.php");
}

if(isset($_REQUEST['reset']) && $_REQUEST['reset'] == 'yes')
{
	 unset($_SESSION['SalOrderList']);	
	  unset($_SESSION['OrderBy']);	
	/*unset($_SESSION['FromOrderList']);	
	unset($_SESSION['ToOrderList']); */
	$_SESSION['FromOrderList']= date('Y-m-d');
	$_SESSION['ToOrderList']= date('Y-m-d');
	header("Location: admin_order_list.php");
}
	
if(isset($_REQUEST['show']) && $_REQUEST['show']=="yes")
{
	$_objAdmin->showOrderList();
	die;
}


?>

<?php include("header.inc.php") ?>


<script type="text/javascript">

function ShowDailyReport(ldata)
{
	if(ldata!=''){$("#dayWiseReport").show();}
	else{$("#dayWiseReport").hide();}

}
function HideDailyReport(ldata)
{
	if(ldata!=''){$("#dayWiseReport").hide();}
	//else{$("#dayWiseReport").hide();}

}


$(document).keydown(function(e) {
	if(e.keyCode==113){
	//alert('Hello');
	 eventHandlerOrderList('Retailer Report (F2)');
	}	
});
</script>
<script type="text/javascript">
$(document).keydown(function(e) {
	if(e.keyCode==115){
	//alert('Hello');
	 eventHandlerOrderList('Salesman Report (F4)');
	}	
});
</script>
<script type="text/javascript">
$(document).keydown(function(e){
   var code = e.keyCode ? e.keyCode : e.which;
   if(code==27){
   //alert(code);
	window.location.href = "admin_order_list.php?resetsal=yes";
    return false;
   }
});
</script>
<script src="javascripts/dateNextPrev.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function(){
	    $("#sal").change(function(){
		 document.report.submit();
		})
	});
</script>
<script type="text/javascript">
// Popup window code
function newPopup(url) {
	popupWindow = window.open(
		url,'popUpWindow','height=500,width=600,left=200,top=100,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no,status=yes')
}
</script>
<!-- start content-outer -->
<input name="pagename" type="hidden"  id="pagename" value="admin_order_list.php" />
<div id="content-outer">
<!-- start content -->
<div id="content">
<div id="page-heading"><h1><span style="color: #d74343; font-family: Tahoma; font-weight: bold;">Order List</span></h1></div>
<table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">
<!--<tr>
	<th rowspan="3" class="sized"><img src="images/side_shadowleft.jpg" width="20" height="300" alt="" /></th>
	<th class="topleft"></th>
	<td id="tbl-border-top">&nbsp;</td>
	<th class="topright"></th>
	<th rowspan="3" class="sized"><img src="images/side_shadowright.jpg" width="20" height="300" alt="" /></th>
</tr>
-->
<tr>
	<!--<td id="tbl-border-left"></td>-->
	<td>
	<!--  start content-table-inner -->
	<div id="content-table-inner">
	
	<div id="page-heading" align="left" >
	
	<form name="submitOrderby" method="post" action="" enctype="multipart/form-data" >
	<table border="0" width="42%" cellpadding="0" cellspacing="0">
		<tr  align="left" style="padding-button:10px;">
			<td >
			<h4><input type="radio" name="orderby" value="1" checked="checked" onchange="javascript:document.submitOrderby.submit()"> &nbsp;With Telephonic Order&nbsp;&nbsp;</h4>
			</td>
			<td >
			<h4><input type="radio" name="orderby" value="2" <?php if($_SESSION['OrderBy']==2){ ?>checked="checked" <?php } ?> onchange="javascript:document.submitOrderby.submit()"  > &nbsp;Only Telephonic Order&nbsp;&nbsp;</h4></td>
			<td >
			<h4><input type="radio" name="orderby" value="3" <?php if($_SESSION['OrderBy']==3){ ?>checked="checked" <?php } ?> onchange="javascript:document.submitOrderby.submit()"> &nbsp;Without Telephonic Order&nbsp;&nbsp;</h4>
			<input name="showOrderBy" type="hidden" value="yes" />
			</td>
		</tr>
	</table>
	</form>
	</br>
	<form name="frmPre" id="frmPre" method="post" action="" enctype="multipart/form-data" >
	<table border="0" width="85%" cellpadding="0" cellspacing="0">
	<tr  align="right">
	  <td></td>
	  <td><table border="0" width="70%" ><tr><td> <!--<img src="css/images/prev.png" onclick="dateFromPrev();">--> </td> <td align="right"> <!--<img src="css/images/next.png" onclick="dateFromNext();">--> </td><td>&nbsp;&nbsp; </td> </tr></table></td>
	  <td><table border="0" width="75%"><tr><td> <!--<img src="css/images/prev.png" onclick="dateToPrev();">--> </td> <td align="right"> <!--<img src="css/images/next.png" onclick="dateToNext();">--> </td><td>&nbsp;&nbsp; </td> </tr></table></td>
	  <td></td>
	  <td></td>
	  <td><a href='admin_order_list_year_graph.php?y=<?php echo date('Y'); ?>' target="_blank"><input type="button" value=" Show Graph" class="result-submit" /></a></td>
	 
	<tr>
		<td ><h3>Salesman Name: 
		<select name="sal" id="sal" class="" style="border: solid 1px #BDBDBD; color: #393939;font-family: Arial;font-size: 12px;border-radius:5px ;-moz-border-radius:5px;-o-border-radius:5px;-ms-border-radius:5px;-webkit-border-radius:5px;width: 150px;height: 30px;" >
		<option value="A" >All</option>
		<?php $aSal=$_objAdmin->_getSelectList('table_salesman','*',''," account_id='".$_SESSION['accountId']."'ORDER BY salesman_name"); 
		if(is_array($aSal)){
		for($i=0;$i<count($aSal);$i++){?>
		<option value="<?php echo $aSal[$i]->salesman_id;?>" <?php if ($aSal[$i]->salesman_id==$_SESSION['SalOrderList']){ ?> selected <?php } ?>><?php echo $aSal[$i]->salesman_name;?></option>
		<?php } }?>
		</select></h3></td>
		<td><h3>From Date: <img src="css/images/prev.png" onclick="dateFromPrev();"><img src="css/images/next.png" onclick="dateFromNext();"><input type="text" id="from" name="from" class="date" style="width:150px" value="<?php if($_SESSION['FromOrderList']!='') { echo $_objAdmin->_changeDate($_SESSION['FromOrderList']); } else { echo $_objAdmin->_changeDate(date('Y-m-d')); }?>"  readonly /></h3></td>
		<td><h3>To Date:<img src="css/images/prev.png" onclick="dateToPrev();"><img src="css/images/next.png" onclick="dateToNext();"> <input type="text" id="to" name="to" class="date" style="width:150px" value="<?php if($_SESSION['ToOrderList']!='') { echo $_objAdmin->_changeDate($_SESSION['ToOrderList']); } else { echo $_objAdmin->_changeDate(date('Y-m-d')); }?>"  readonly /></h3></td>
		<td><input name="showOrderlist" type="hidden" value="yes" />
		<input name="submit" class="result-submit" type="submit" id="submit" value="Show Order List" /></td>	
		<td style=" padding-right:5px; padding-bottom:3px; padding-top:3px;"><input style="padding:5px; float:right;" type="button" id="OpenReport" value="Daily Report" class="result-submit" onclick="ShowDailyReport(this.id)" /></td>	
		<td><input type="button" value="Reset!" class="form-reset" onclick="location.href='admin_order_list.php?reset=yes';" /></td>
		
	</tr>
	<tr>
		<td colspan="5" >
		  <table border="0" width="100%">
		  <tr  align="center">
		  <?php 
		   if($_SESSION['SalOrderList']!=''){
			$salesman=" and s.salesman_id='".$_SESSION['SalOrderList']."'";
		   }
			   if($_SESSION['OrderBy']!=''){
					if($_SESSION['OrderBy']==1){
					$orderby="";
					}
					if($_SESSION['OrderBy']==2){
					$orderby=" and o.order_type='Adhoc' ";
					}
					if($_SESSION['OrderBy']==3){
					$orderby=" and o.order_type!='Adhoc' ";
					}
				}
				else
				{
					$orderby="";
				}
		   if($_SESSION['FromOrderList']!='') { $frmNew = $_SESSION['FromOrderList']; } else { $frmNew = date('Y-m-d'); }
		   if($_SESSION['ToOrderList']!='') { $toNew = $_SESSION['ToOrderList']; } else { $toNew = date('Y-m-d'); }
		   $td1=$_objAdmin->_getSelectList('table_order as o left join table_retailer as r on o.retailer_id=r.retailer_id left join table_salesman as s on o.salesman_id=s.salesman_id left join table_distributors as d on o.distributor_id=d.distributor_id','count(*) as total_call, sum( o.total_invoice_amount ) as total_amt ',''," o.date_of_order >= '".date('Y-m-d', strtotime(mysql_escape_string($frmNew)))."' and o.date_of_order <= '".date('Y-m-d', strtotime(mysql_escape_string($toNew)))."' and r.new='' $salesman $orderby"); 
		   $td2=$_objAdmin->_getSelectList('table_order as o left join table_retailer as r on o.retailer_id=r.retailer_id left join table_salesman as s on o.salesman_id=s.salesman_id left join table_distributors as d on o.distributor_id=d.distributor_id','count(*) as productive_call',''," o.date_of_order >= '".date('Y-m-d', strtotime(mysql_escape_string($frmNew)))."' and o.date_of_order <= '".date('Y-m-d', strtotime(mysql_escape_string($toNew)))."' and o.order_type != 'No' and r.new='' $salesman $orderby");   
		   $td4=$_objAdmin->_getSelectList('table_order as o left join table_retailer as r on o.retailer_id=r.retailer_id left join table_salesman as s on o.salesman_id=s.salesman_id left join table_distributors as d on o.distributor_id=d.distributor_id','count(distinct(o.retailer_id)) as total_retailer ',''," o.date_of_order >= '".date('Y-m-d', strtotime(mysql_escape_string($frmNew)))."' and o.date_of_order <= '".date('Y-m-d', strtotime(mysql_escape_string($toNew)))."' and r.new='' $salesman $orderby");
		   
		  ?>
		   <td><b>Total calls : </b><?php echo $td1[0]->total_call; ?></td>
		   <td><b>Total productive calls : </b><?php echo $td2[0]->productive_call; ?></td>
		   <td><b>Total amount of order : </b><?php echo $td1[0]->total_amt; ?></td>
		   <td><b>Total number of retailers : </b><?php echo $td4[0]->total_retailer; ?></td>
		   <!--<td><b>Total number of No calls : </b><?php //echo $td5[0]->no_call; ?></td>-->
		  </tr>
		  </table>
		</td>
  </tr>
	</table>
	</form>
	<!--<h3>Salesman Name: 
		<select name="sal" id="sal" class="" style="width:150px" >
		<option value="" >All</option>
		<?php $aSal=$_objAdmin->_getSelectList('table_salesman','*',''," account_id='".$_SESSION['accountId']."'ORDER BY salesman_name"); 
		if(is_array($aSal)){
		for($i=0;$i<count($aSal);$i++){?>
		<option value="<?php echo $aSal[$i]->salesman_id;?>" <?php if ($aSal[$i]->salesman_id==$_SESSION['SalOrderList']){ ?> selected <?php } ?>><?php echo $aSal[$i]->salesman_name;?></option>
		<?php } }?>
		</select></span> 
		From Date: <input type="text" id="from" name="from" class="date" style="width:150px" value="<?php echo $_SESSION['FromOrderList'];?>"  readonly />
		To Date: <input type="text" id="to" name="to" class="date" style="width:150px" value="<?php echo $_SESSION['ToOrderList'];?>"  readonly />
		<input name="showOrderlist" type="hidden" value="yes" />
		<input name="submit" class="result-submit" type="submit" id="submit" value="Show Order List" />
		<input type="button" value="Reset!" class="form-reset" onclick="location.href='admin_order_list.php?reset=yes';" />
	</h3>-->
	</div>
	<div id="dayWiseReport" style="display:none; padding-top:5px; padding-bottom:5px; overflow:scroll; height:250px; width:1000px;">
	<table  border="0"  cellpadding="0" cellspacing="0" id="lists1">
				<tr>
					<td style=" padding-right:5px; padding-bottom:3px; padding-top:3px;"><input style="padding:5px; float:right;" type="button" id="CloseReport" value="Close Report" class="form-reset" onclick="HideDailyReport(this.id)" /></td>
				</tr>
			<tr>
			<td>
			<table  border="1"  cellpadding="0" cellspacing="0" id="lists">
				<tr  bgcolor="#6E6E6E" style="color: #fff;font-weight: bold;" id="candidateNameTD">
					<td style="padding:10px;"><div style="width: 100px;">Date</div></td>
					<td style="padding:10px;"><div style="width: 100px;">Day</div></td>
					<td style="padding:10px;" ><div style="width: 100px;">Total calls</div></td>
					<td style="padding:10px;" ><div style="width: 175px;">Total productive calls</div></td>
					<td style="padding:10px;"><div style="width: 200px;">Total amount of order</div></td>
					<td style="padding:10px;"><div style="width: 200px;">Total number of retailers</div></td>
				</tr>
				<?php  
				function DateCheck($start_date,$end_date){
				$start = strtotime($start_date);
				$end = strtotime($end_date);
				$new_date=array($start_date);
				$date_arr=array();
				$days_count = ceil(abs($end - $start) / 86400)+1;
				for($i=0;$i<$days_count;$i++)
					{
						$date = strtotime ('+1 days', strtotime($start_date)) ;
						$start_date = date ('Y-m-d' , $date);
						$date_arr[$i] = $start_date;
						$week_last_date=$start_date;
						array_push($new_date,$week_last_date);
						$get_newdate[]=array($new_date[$i],$new_date[$i+1]);
					}
					return $get_newdate;
			}	
		$d1="".$_SESSION['FromOrderList']."";
		$d2="".$_SESSION['ToOrderList']."";
		$duration_dates = DateCheck($d1,$d2);
		foreach($duration_dates as $value)
		{
			$duration_start_date=$value[0];
			 $tdr1=$_objAdmin->_getSelectList('table_order as o left join table_retailer as r on o.retailer_id=r.retailer_id left join table_salesman as s on o.salesman_id=s.salesman_id left join table_distributors as d on o.distributor_id=d.distributor_id','count(*) as total_call, sum( o.total_invoice_amount ) as total_amt ',''," o.date_of_order >= '".date('Y-m-d', strtotime(mysql_escape_string($duration_start_date)))."' and o.date_of_order <= '".date('Y-m-d', strtotime(mysql_escape_string($duration_start_date)))."' and r.new='' $salesman $orderby"); 
		   $tdr2=$_objAdmin->_getSelectList('table_order as o left join table_retailer as r on o.retailer_id=r.retailer_id left join table_salesman as s on o.salesman_id=s.salesman_id left join table_distributors as d on o.distributor_id=d.distributor_id','count(*) as productive_call',''," o.date_of_order >= '".date('Y-m-d', strtotime(mysql_escape_string($duration_start_date)))."' and o.date_of_order <= '".date('Y-m-d', strtotime(mysql_escape_string($duration_start_date)))."' and o.order_type != 'No' and r.new='' $salesman $orderby");   
		   $tdr4=$_objAdmin->_getSelectList('table_order as o left join table_retailer as r on o.retailer_id=r.retailer_id left join table_salesman as s on o.salesman_id=s.salesman_id left join table_distributors as d on o.distributor_id=d.distributor_id','count(distinct(o.retailer_id)) as total_retailer ',''," o.date_of_order >= '".date('Y-m-d', strtotime(mysql_escape_string($duration_start_date)))."' and o.date_of_order <= '".date('Y-m-d', strtotime(mysql_escape_string($duration_start_date)))."' and r.new='' $salesman $orderby");
			
			
			
			//echo $duration_start_date."<br/>";
			
	
				?>
				<tr>
				<td style="padding:10px; " ><?php  echo $_objAdmin->_changeDate($duration_start_date); ?></td>
				<td style="padding:10px; " ><?php  echo date('l', strtotime($duration_start_date)); ?></td>
				<td style="padding:10px; " ><?php  if($tdr1[0]->total_call==0 || $tdr1[0]->total_call=='' ){echo "-";} else {echo $tdr1[0]->total_call;} ?></td>
		<td style="padding:10px; " ><?php  if($tdr2[0]->productive_call==0 || $tdr2[0]->productive_call==''){echo "-";} else {echo $tdr2[0]->productive_call;} ?></td>
				<td style="padding:10px; " ><?php  if($tdr1[0]->total_amt==0 || $tdr1[0]->total_amt==''){echo "-";} else { echo $tdr1[0]->total_amt;} ?></td>
				<td style="padding:10px; " ><?php  if($tdr4[0]->total_retailer==0 || $tdr4[0]->total_retailer==''){echo "-";} else {echo $tdr4[0]->total_retailer;} ?></td>
				
				</tr>
				<?php }?>
			</table>
			</td>
			</tr>
			</table>
	</div>
	
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
	<tr valign="top">
	<td>
		<!-- start id-form -->
		<table  border="0" cellspacing="0" cellpadding="0"  align="center" width="100%">
				<tr>
				<td align="lift" valign="top"  >
				<table id="flex1" style="display:none"></table>
				<script type="text/javascript">showOrderList();</script>
				</td>
				</tr>
				</table>
	<!-- end id-form  -->

	</td>

</tr>
<tr>
<td><img src="images/blank.gif" width="695" height="1" alt="blank" /></td>
<td></td>
</tr>
</table>
<div class="clear"></div>
</div>
<!--  end content-table-inner  -->
</td>
<td id="tbl-border-right"></td>
</tr>
<!--<tr>
	<th class="sized bottomleft"></th>
	<td id="tbl-border-bottom">&nbsp;</td>
	<th class="sized bottomright"></th>
</tr>-->
</table>
<div class="clear">&nbsp;</div>


	
	
	
	
</div>
<!--  end content -->
<div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->
<div class="clear">&nbsp;</div> 
<!-- start footer -->         
<?php include("footer.php") ?>
<!-- end footer -->
<script src="javascripts/jquery-1.8.2.js" type="text/javascript"></script>
<script src="javascripts/jquery-ui.js"></script>
<script type="text/javascript" src="javascripts/validate.js"></script>
<script type="text/javascript">
// Popup window code
function newPopup(url) {
	popupWindow = window.open(
		url,'popUpWindow','height=500,width=600,left=200,top=100,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no,status=yes')
}
</script>
<script>
    $(function() {
        $( "#from" ).datepicker({
			dateFormat: "d M yy",
            defaultDate: "w",
            changeMonth: true,
            numberOfMonths: 1,
            onSelect: function( selectedDate ) {
                $( "#to" ).datepicker( "option", "minDate", selectedDate );
            }
        });
        $( "#to" ).datepicker({
			dateFormat: "d M yy",
            defaultDate: "-w",
            changeMonth: true,
            numberOfMonths: 1,
            onSelect: function( selectedDate ) {
                $( "#from" ).datepicker( "option", "maxDate", selectedDate );
            }
        });
    });
</script>
</body>
</html>