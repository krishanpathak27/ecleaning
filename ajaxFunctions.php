<?php

include("includes/config.inc.php");
$_objAdmin = new Admin();

if ($_REQUEST['pageType'] == 'supervisorList') {
    $_objAdmin = new Admin();
    $page = $_REQUEST['datatable']['pagination']['page'];
    $perPage = $_REQUEST['datatable']['pagination']['perpage'];
    $sortname = $_REQUEST['datatable']['sort']['field'];
    $sortorder = $_REQUEST['datatable']['sort']['sort'];
    $search = $_REQUEST['datatable']['query']['generalSearch'];

    if (!$sortname)
        $sortname = 'supervisor_name';
    if (!$sortorder)
        $sortorder = 'asc';

    $sort = " ORDER BY $sortname $sortorder";

    if (!$page)
        $page = 1;
    if (!$perPage)
        $perPage = 50;

    $start = (($page - 1) * $perPage);
    if(isset($_SESSION['order']) && $_SESSION['order'] == 'updated') {
        $sort = " ORDER BY table_supervisor.last_update_date desc";
        unset($_SESSION['order']);
    }
    $limit = "$start, $perPage";
    $query = $_REQUEST['query'];
    $where = "(table_supervisor.status != 'D')";
    $table = 'table_supervisor LEFT JOIN table_buildings on table_buildings.building_id=table_supervisor.building_id';
    $clms = 'table_supervisor.supervisor_id,table_supervisor.supervisor_name,table_supervisor.building_id,table_supervisor.supervisor_address,'
            . 'table_supervisor.supervisor_phone_no,table_supervisor.status,table_supervisor.supervisor_email,table_supervisor.status,table_buildings.building_name';

    if (isset($search) && !empty($search)) {
        $where .= " and (table_supervisor.supervisor_name like '%" . $search . "%' or table_supervisor.supervisor_address like '%" . $search . "%'"
                . "or table_supervisor.supervisor_email like '%" . $search . "%' or table_supervisor.supervisor_phone_no like '%" . $search . "%')";
    }
    $auRec = $_objAdmin->_getSelectList($table, $clms, $limit, $where . ' ' . $sort, '', '');
    $auRecCount = $_objAdmin->_getSelectList('table_supervisor', 'count(*) as total', '', $where);
    $total = $auRecCount[0]->total;

    $dataReturn = array();
    $dataReturn['meta'] = array(
        'page' => $page,
        'pages' => ceil($total / $perPage),
        'total' => $total,
        'perpage' => $perPage,
        'sort' => $sortorder,
        'field' => $sortname
    );
    $dataReturn['data'] = array();
    foreach ($auRec as $auRecTemp) {
        $temp['supervisor_id'] = $auRecTemp->supervisor_id;
        $temp['supervisor_name'] = $auRecTemp->supervisor_name;
        $temp['supervisor_address'] = $auRecTemp->supervisor_address;
        $temp['supervisor_phone_no'] = $auRecTemp->supervisor_phone_no;
        $temp['supervisor_email'] = $auRecTemp->supervisor_email;
        $temp['status'] = $auRecTemp->status;
        $temp['building_name'] = $auRecTemp->building_name;
        if ($auRecTemp->status == 'A') {
            $temp['Status'] = 1;
        } else if ($auRecTemp->status == 'I') {
            $temp['Status'] = 2;
        } else {
            $temp['Status'] = 2;
        }
        $dataReturn['data'][] = $temp;
    }
    echo json_encode($dataReturn, true);
    die;
}


if ($_REQUEST['pageType'] == 'unitType') {
    $_objAdmin = new Admin();
    $page = $_REQUEST['datatable']['pagination']['page'];
    $perPage = $_REQUEST['datatable']['pagination']['perpage'];
    $sortname = $_REQUEST['datatable']['sort']['field'];
    $sortorder = $_REQUEST['datatable']['sort']['sort'];
    $search = $_REQUEST['datatable']['query']['generalSearch'];

    if (!$sortname)
        $sortname = 'unit_name';
    if (!$sortorder)
        $sortorder = 'asc';

    $sort = " ORDER BY $sortname $sortorder";

    if (!$page)
        $page = 1;
    if (!$perPage)
        $perPage = 50;

    $start = (($page - 1) * $perPage);
    if(isset($_SESSION['order']) && $_SESSION['order'] == 'updated') {
        $sort = " ORDER BY table_unit_type.last_update_date desc";
        unset($_SESSION['order']);
    }
    $limit = "$start, $perPage";
    $query = $_REQUEST['query'];
    $where = "(table_unit_type.status != 'D')";
    $table = 'table_unit_type';
    $clms = 'table_unit_type.*';
    
    if (isset($search) && !empty($search)) {
        $where .= " and (table_unit_type.unit_name like '%" . $search . "%')";
    }
    $auRec = $_objAdmin->_getSelectList($table, $clms, $limit, $where . ' ' . $sort, '', '');
    $auRecCount = $_objAdmin->_getSelectList('table_unit_type', 'count(*) as total', '', $where);
    $total = $auRecCount[0]->total;

    $dataReturn = array();
    $dataReturn['meta'] = array(
        'page' => $page,
        'pages' => ceil($total / $perPage),
        'total' => $total,
        'perpage' => $perPage,
        'sort' => $sortorder,
        'field' => $sortname
    );
    $dataReturn['data'] = array();
    foreach ($auRec as $auRecTemp) {
        $temp['unit_type_id'] = $auRecTemp->unit_type_id;
        $temp['unit_name'] = $auRecTemp->unit_name;
        if ($auRecTemp->status == 'A') {
            $temp['Status'] = 1;
        } else if ($auRecTemp->status == 'I') {
            $temp['Status'] = 2;
        } else {
            $temp['Status'] = 2;
        }
        $dataReturn['data'][] = $temp;
    }
    echo json_encode($dataReturn, true);
    die;
}

if ($_REQUEST['pageType'] == 'cleanerList') {
    $_objAdmin = new Admin();
    $page = $_REQUEST['datatable']['pagination']['page'];
    $perPage = $_REQUEST['datatable']['pagination']['perpage'];
    //$sortname = $_REQUEST['datatable']['sort']['field'];
    $sortorder = $_REQUEST['datatable']['sort']['sort'];
    $search = $_REQUEST['datatable']['query']['generalSearch'];
//print_r($sortname);die;
    if (!$sortname)
        $sortname = 'cleaner_name';
    if (!$sortorder)
        $sortorder = 'asc';

    $sort = " ORDER BY $sortname $sortorder";

    if (!$page)
        $page = 1;
    if (!$perPage)
        $perPage = 50;

    $start = (($page - 1) * $perPage);
    if(isset($_SESSION['order']) && $_SESSION['order'] == 'updated') {
        $sort = " ORDER BY table_cleaner.last_update_date desc";
        unset($_SESSION['order']);
    }
    $limit = "$start, $perPage";
    $query = $_REQUEST['query'];
    $where = "(table_cleaner.status != 'D')";
    $table = 'table_cleaner LEFT JOIN table_supervisor on table_supervisor.supervisor_id=table_cleaner.supervisor_id';
    $clms = 'table_cleaner.cleaner_id,table_cleaner.supervisor_id,table_cleaner.cleaner_name,table_cleaner.cleaner_code,table_supervisor.supervisor_name,table_cleaner.cleaner_address'
            . ',table_cleaner.cleaner_phone_no,table_cleaner.cleaner_email,table_cleaner.status';
    if($_SESSION['userLoginType'] == 5 && isset($_SESSION['supervisor_id']) && !empty($_SESSION['supervisor_id'])) {
        $where .= " and table_cleaner.supervisor_id = '".$_SESSION['supervisor_id']."'";
    }
    if (isset($search) && !empty($search)) {
        $where .= " and (table_cleaner.cleaner_name like '%" . $search . "%' or table_cleaner.cleaner_code like '%" . $search . "%'"
                . "or table_cleaner.cleaner_address like '%" . $search . "%'"
                . "or table_cleaner.cleaner_phone_no like '%" . $search . "%' or table_cleaner.cleaner_email like '%" . $search . "%')";
    }
    $auRec = $_objAdmin->_getSelectList($table, $clms, $limit, $where . ' ' . $sort, '', '');
    $auRecCount = $_objAdmin->_getSelectList('table_cleaner', 'count(*) as total', '', $where);
    $total = $auRecCount[0]->total;

    $dataReturn = array();
    $dataReturn['meta'] = array(
        'page' => $page,
        'pages' => ceil($total / $perPage),
        'total' => $total,
        'perpage' => $perPage,
        'sort' => $sortorder,
        'field' => $sortname
    );
    $dataReturn['data'] = array();
    foreach ($auRec as $auRecTemp) {
        $temp['cleaner_id'] = $auRecTemp->cleaner_id;
        $temp['cleaner_name'] = $auRecTemp->cleaner_name;
        $temp['supervisor_name'] = $auRecTemp->supervisor_name;
        $temp['cleaner_code'] = $auRecTemp->cleaner_code;
        $temp['cleaner_address'] = $auRecTemp->cleaner_address;
        $temp['cleaner_phone_no'] = $auRecTemp->cleaner_phone_no;
        $temp['cleaner_email'] = $auRecTemp->cleaner_email;
        $temp['status'] = $auRecTemp->status;
        if ($auRecTemp->status == 'A') {
            $temp['Status'] = 1;
        } else if ($auRecTemp->status == 'I') {
            $temp['Status'] = 2;
        } else {
            $temp['Status'] = 2;
        }
        $dataReturn['data'][] = $temp;
    }
    echo json_encode($dataReturn, true);
    die;
}

if ($_REQUEST['pageType'] == 'cleanerWorkingHours') {
	
    $_objAdmin = new Admin();
    $page = $_REQUEST['datatable']['pagination']['page'];
    $perPage = $_REQUEST['datatable']['pagination']['perpage'];
    //$sortname = $_REQUEST['datatable']['sort']['field'];
    $sortorder = $_REQUEST['datatable']['sort']['sort'];
    $search = $_REQUEST['datatable']['query']['generalSearch'];
//print_r($sortname);die;
    if (!$sortname)
        $sortname = 'cleaner_name';
    if (!$sortorder)
        $sortorder = 'asc';

    $sort = " ORDER BY $sortname $sortorder";

    if (!$page)
        $page = 1;
    if (!$perPage)
        $perPage = 50;

    $start = (($page - 1) * $perPage);
    if(isset($_SESSION['order']) && $_SESSION['order'] == 'updated') {
        $sort = " ORDER BY table_cleaner.last_update_date desc";
        unset($_SESSION['order']);
    }
    $limit = "$start, $perPage";
    $query = $_REQUEST['query'];
    $where = "(table_cleaner.status != 'D')";
    $table = 'table_cleaner LEFT JOIN table_supervisor on table_supervisor.supervisor_id=table_cleaner.supervisor_id';
    $clms = 'table_cleaner.cleaner_id,table_cleaner.cleaner_name,table_cleaner.cleaner_code,table_cleaner.duty_start_time,table_cleaner.duty_end_time,table_cleaner.status';
    if($_SESSION['userLoginType'] == 5 && isset($_SESSION['supervisor_id']) && !empty($_SESSION['supervisor_id'])) {
        $where .= " and table_cleaner.supervisor_id = '".$_SESSION['supervisor_id']."'";
    }
    if (isset($search) && !empty($search)) {
        $where .= " and (table_cleaner.cleaner_name like '%" . $search . "%' or table_cleaner.cleaner_code like '%" . $search . "%'"
                . "or table_cleaner.cleaner_address like '%" . $search . "%'"
                . "or table_cleaner.cleaner_phone_no like '%" . $search . "%' or table_cleaner.cleaner_email like '%" . $search . "%')";
    }
    $auRec = $_objAdmin->_getSelectList($table, $clms, $limit, $where . ' ' . $sort, '', '');
    $auRecCount = $_objAdmin->_getSelectList('table_cleaner', 'count(*) as total', '', $where);
    $total = $auRecCount[0]->total;

    $dataReturn = array();
    $dataReturn['meta'] = array(
        'page' => $page,
        'pages' => ceil($total / $perPage),
        'total' => $total,
        'perpage' => $perPage,
        'sort' => $sortorder,
        'field' => $sortname
    );
    $dataReturn['data'] = array();
    foreach ($auRec as $auRecTemp) {
        $temp['cleaner_id'] = $auRecTemp->cleaner_id;
        $temp['cleaner_name'] = $auRecTemp->cleaner_name;
		$temp['duty_start_time'] = $auRecTemp->duty_start_time;
		$temp['duty_end_time'] = $auRecTemp->duty_end_time;
       
        $temp['status'] = $auRecTemp->status;
        if ($auRecTemp->status == 'A') {
            $temp['Status'] = 1;
        } else if ($auRecTemp->status == 'I') {
            $temp['Status'] = 2;
        } else {
            $temp['Status'] = 2;
        }
        $dataReturn['data'][] = $temp;
    }
	
    echo json_encode($dataReturn, true);
    die;
}

if ($_REQUEST['pageType'] == 'cleanerLoginStatusList') {
    $_objAdmin = new Admin();
    $page = $_REQUEST['datatable']['pagination']['page'];
    $perPage = $_REQUEST['datatable']['pagination']['perpage'];
    //$sortname = $_REQUEST['datatable']['sort']['field'];
    $sortorder = $_REQUEST['datatable']['sort']['sort'];
    $search = $_REQUEST['datatable']['query']['generalSearch'];
    if(!$sortname)
        $sortname='cleaner_name';
    if(!$sortorder)
        $sortorder='desc';

    $sort = " ORDER BY $sortname $sortorder";

    if (!$page)
        $page = 1;
    if (!$perPage)
        $perPage = 50;

    $start = (($page - 1) * $perPage);

    $limit = "$start, $perPage";
    $query = $_REQUEST['query'];
    $where = " table_cleaner.status='A' ";
    $table = 'table_cleaner LEFT JOIN table_account on table_account.account_id=table_cleaner.account_id left join table_web_users on table_web_users.cleaner_id=table_cleaner.cleaner_id';

    $clms = 'table_cleaner.*,table_web_users.username,table_web_users.email_id,table_web_users.web_user_id,table_web_users.status';

    if (isset($search) && !empty($search)) {
        $where .= " and (table_cleaner.cleaner_name like '%" . $search . "%' or table_web_users.username like '%" . $search . "%'". "or table_web_users.email_id like '%" . $search . "%' or table_cleaner.cleaner_phone_no like '%" . $search . "%')";
    }
    $auRec = $_objAdmin->_getSelectList($table, $clms, $limit, $where . ' ' . $sort, '', '');
    $auRecCount = $_objAdmin->_getSelectList('table_cleaner left join table_account  on table_cleaner.account_id=table_account.account_id left join table_web_users on table_web_users.cleaner_id=table_cleaner.cleaner_id', 'count(*) as total', '', $where);
    $total = $auRecCount[0]->total;

    $dataReturn = array();
    $dataReturn['meta'] = array(
        'page' => $page,
        'pages' => ceil($total / $perPage),
        'total' => $total,
        'perpage' => $perPage,
        'sort' => $sortorder,
        'field' => $sortname
    );
    $dataReturn['data'] = array();
    foreach($auRec as $auRecTemp){
        $temp['cleaner_id'] = $auRecTemp->cleaner_id;
        $temp['cleaner_name'] = $auRecTemp->cleaner_name;
        $temp['username'] = $auRecTemp->username;
        $temp['email_id'] = $auRecTemp->email_id;
        $temp['cleaner_phone_no'] = $auRecTemp->cleaner_phone_no;
        
        if($auRecTemp->session_id!=''){
            $sts="Logged In"; 
           
            } else {
            $sts="Logged Out";
            
            }

        $temp['Login_status'] =$sts;
        
        
        $dataReturn['data'][] = $temp;
    }
    echo json_encode($dataReturn, true);
    die;
}
if($_REQUEST['pageType'] == 'bookingList'){
    $_objAdmin = new Admin();
    $page = $_REQUEST['datatable']['pagination']['page'];
    $perPage = $_REQUEST['datatable']['pagination']['perpage'];
    $sortname = $_REQUEST['datatable']['sort']['field'];
    $sortorder = $_REQUEST['datatable']['sort']['sort'];
    $search = $_REQUEST['datatable']['query']['generalSearch'];
   // print_r($search);die;

    if (!$sortname)
        $sortname = 'booking_id';
    if (!$sortorder)
        $sortorder = 'desc';

    $sort = " ORDER BY $sortname $sortorder";

    if (!$page)
        $page = 1;
    if (!$perPage)
        $perPage = 50;

    $start = (($page - 1) * $perPage);

    $limit = "$start, $perPage";
    $query = $_REQUEST['query'];
      $where='';
   
   if($_SESSION['bookingStatus']!=NULL){

        if($_SESSION['bookingStatus']=='U'){//UNpaid check
            //$bookingStatus=" AND ba.booking_status!='CB' AND tph.payment_mode='cash' ";


            $allBookings=$_objAdmin->_getSelectList2('table_booking_register AS outb RIGHT JOIN ( SELECT ba.booking_allotment_id AS booking_allotment_id, group_concat(if(ba.booking_status = "CB" , "NA", ba.booking_status)) AS aggr_status, group_concat(ba.booking_status) AS booking_status, bd.booking_detail_id AS booking_detail_id, br.booking_id FROM table_booking_details AS bd LEFT JOIN table_booking_register AS br ON br.booking_id = bd.booking_id LEFT JOIN table_booking_allotment AS ba ON ba.booking_detail_id = bd.booking_detail_id group by br.booking_id )inb ON inb.booking_id = outb.booking_id left join table_payment_history as tph on outb.payment_id= tph.payment_id',"outb.booking_id",''," inb.aggr_status NOT LIKE '%NA%' AND tph.payment_mode='cash'");
            if(!empty($allBookings)){
                $bookingIds='';
                foreach($allBookings as $key=>$book){

                    $bookingIds=$bookingIds.','.$book->booking_id;

                }
                $bookingIds=ltrim($bookingIds,',');
                $where.="br.booking_id IN(".$bookingIds.")";
            }else{
                $where.="br.booking_id=''";
            }

        }else if($_SESSION['bookingStatus']=='P'){//Paid Check
            $bookingStatus=" AND ba.booking_status='CB' OR tph.payment_mode!='cash' ";

            $allBookings=$_objAdmin->_getSelectList2('table_booking_register AS outb RIGHT JOIN ( SELECT ba.booking_allotment_id AS booking_allotment_id, group_concat(if(ba.booking_status != "CB" , "NA", ba.booking_status)) AS aggr_status, group_concat(ba.booking_status) AS booking_status, bd.booking_detail_id AS booking_detail_id, br.booking_id FROM table_booking_details AS bd LEFT JOIN table_booking_register AS br ON br.booking_id = bd.booking_id LEFT JOIN table_booking_allotment AS ba ON ba.booking_detail_id = bd.booking_detail_id group by br.booking_id )inb ON inb.booking_id = outb.booking_id left join table_payment_history as tph on outb.payment_id= tph.payment_id',"outb.booking_id",''," inb.aggr_status NOT LIKE '%NA%' OR tph.payment_mode!='cash'");
            if(!empty($allBookings)){
                $bookingIds='';
                foreach($allBookings as $key=>$book){

                    $bookingIds=$bookingIds.','.$book->booking_id;

                }
                $bookingIds=ltrim($bookingIds,',');
                $where.="br.booking_id IN(".$bookingIds.")";
            }else{
                $where.="br.booking_id=''";
            }
        }else if($_SESSION['bookingStatus']=='CB'){//Complete BOoking Check

            $allBookings=$_objAdmin->_getSelectList2('table_booking_register AS outb RIGHT JOIN ( SELECT ba.booking_allotment_id AS booking_allotment_id, group_concat(if(ba.booking_status != "CB" , "NA", ba.booking_status)) AS aggr_status, group_concat(ba.booking_status) AS booking_status, bd.booking_detail_id AS booking_detail_id, br.booking_id FROM table_booking_details AS bd LEFT JOIN table_booking_register AS br ON br.booking_id = bd.booking_id LEFT JOIN table_booking_allotment AS ba ON ba.booking_detail_id = bd.booking_detail_id group by br.booking_id )inb ON inb.booking_id = outb.booking_id ',"outb.booking_id",''," inb.aggr_status NOT LIKE '%NA%'");
            
            if(!empty($allBookings)){
                $bookingIds='';
                foreach($allBookings as $key=>$book){

                    $bookingIds=$bookingIds.','.$book->booking_id;

                }
                $bookingIds=ltrim($bookingIds,',');
                $where.="br.booking_id IN(".$bookingIds.")";
            }else{
                $where.="br.booking_id=''";
            }
           
        }else if($_SESSION['bookingStatus']=='CL'){//Cancelled Check
           // $bookingStatus=" AND ba.booking_status='CL'";

            $allBookings=$_objAdmin->_getSelectList2('table_booking_register AS outb RIGHT JOIN ( SELECT ba.booking_allotment_id AS booking_allotment_id, group_concat(if(ba.booking_status != "CL" , "NA", ba.booking_status)) AS aggr_status, group_concat(ba.booking_status) AS booking_status, bd.booking_detail_id AS booking_detail_id, br.booking_id FROM table_booking_details AS bd LEFT JOIN table_booking_register AS br ON br.booking_id = bd.booking_id LEFT JOIN table_booking_allotment AS ba ON ba.booking_detail_id = bd.booking_detail_id group by br.booking_id )inb ON inb.booking_id = outb.booking_id ',"outb.booking_id",''," inb.aggr_status NOT LIKE '%NA%'");

            if(!empty($allBookings)){
                $bookingIds='';
                foreach($allBookings as $key=>$book){

                    $bookingIds=$bookingIds.','.$book->booking_id;

                }
                $bookingIds=ltrim($bookingIds,',');
                $where.="br.booking_id IN(".$bookingIds.")";
            }else{
                $where.="br.booking_id=''";
            }
      


        }else if($_SESSION['bookingStatus']=='AL'){//Cancelled Check
            //$bookingStatus=" AND ba.booking_status='AL'";

             $allBookings=$_objAdmin->_getSelectList2('table_booking_register AS outb RIGHT JOIN ( SELECT ba.booking_allotment_id AS booking_allotment_id, group_concat(if(ba.booking_status != "AL" , "NA", ba.booking_status)) AS aggr_status, group_concat(ba.booking_status) AS booking_status, bd.booking_detail_id AS booking_detail_id, br.booking_id FROM table_booking_details AS bd LEFT JOIN table_booking_register AS br ON br.booking_id = bd.booking_id LEFT JOIN table_booking_allotment AS ba ON ba.booking_detail_id = bd.booking_detail_id group by br.booking_id )inb ON inb.booking_id = outb.booking_id ',"outb.booking_id",''," inb.aggr_status NOT LIKE '%NA%'");

            if(!empty($allBookings)){
                $bookingIds='';
                foreach($allBookings as $key=>$book){

                    $bookingIds=$bookingIds.','.$book->booking_id;

                }
                $bookingIds=ltrim($bookingIds,',');
                $where.="br.booking_id IN(".$bookingIds.")";
            }else{
                $where.="br.booking_id=''";
            }

        }else if($_SESSION['bookingStatus']=='UA'){//Cancelled Check
            $bookingStatus=" AND ba.booking_status='UA'";

            $allBookings=$_objAdmin->_getSelectList2('table_booking_register AS outb RIGHT JOIN ( SELECT ba.booking_allotment_id AS booking_allotment_id, group_concat(if(ba.booking_status != "UA" , "NA", ba.booking_status)) AS aggr_status, group_concat(ba.booking_status) AS booking_status, bd.booking_detail_id AS booking_detail_id, br.booking_id FROM table_booking_details AS bd LEFT JOIN table_booking_register AS br ON br.booking_id = bd.booking_id LEFT JOIN table_booking_allotment AS ba ON ba.booking_detail_id = bd.booking_detail_id group by br.booking_id )inb ON inb.booking_id = outb.booking_id ',"outb.booking_id",''," inb.aggr_status NOT LIKE '%NA%'");

            if(!empty($allBookings)){
                $bookingIds='';
                foreach($allBookings as $key=>$book){

                    $bookingIds=$bookingIds.','.$book->booking_id;

                }
                $bookingIds=ltrim($bookingIds,',');
                $where.="br.booking_id IN(".$bookingIds.")";
            }else{
                $where.="br.booking_id=''";
            }
        }
    }else{
        $bookingStatus='';
    }
    if($_SESSION['buildingFil']!=NULL || $_SESSION['buildingFil']!=''){
        $where .=" AND b.building_id=".$_SESSION['buildingFil'];
    }
    if($_SESSION['customerFil']!=NULL || $_SESSION['customerFil']!=''){
        $where .=" AND cus.customer_id=".$_SESSION['customerFil'];
    }

    if(isset($bookingStatus) && $bookingStatus != ""){
        $where .= "$bookingStatus"; 
    }
   
    

   
    if($_SESSION['FromBookingList']!=''){
        $fromdate=" AND  br.packg_start_date >= '".date('Y-m-d', strtotime($_SESSION['FromBookingList']))."'";
        $where.="$fromdate ";
    }
    if($_SESSION['ToBookingList']!=''){
        $todate=" and br.packg_start_date <= '".date('Y-m-d', strtotime($_SESSION['ToBookingList']))."'";
        $where.="$todate ";
    }
    if(isset($search) && !empty($search)){
       // print_r($search);die;
        $where .= " and (br.booking_id like '%" . $search . "%' or cus.customer_name like '%" . $search . "%' or cus.customer_number like '%" . $search . "%' or b.building_name like '%" . $search . "%')";
    }


    if($query) $where .= " AND $qtype LIKE '%$query%'";
   // if(isset($bookingStatus) && $bookingStatus == ""){
        $where .=" Group by br.booking_id";
    //}
    if($_SESSION['userLoginType'] == 5 && isset($_SESSION['supervisor_id']) && !empty($_SESSION['supervisor_id'])){
        $auRec=$this->_getSelectList2('table_supervisor',"*",'',"supervisor_id = '".$_SESSION['supervisor_id']."'",'');
        $where .= " and b.building_id = '".$auRec[0]->building_id."'";
    }
   // print_r($where);die;
    $table = "table_booking_register AS br LEFT JOIN table_booking_details AS bd ON br.booking_id = bd.booking_id "."JOIN (SELECT GROUP_CONCAT(CONCAT(brt.counter,' ',tblC.category_name)) AS room_detail,brt.booking_id FROM table_booking_room_type_mapping AS brt "
                ."LEFT JOIN table_category AS tblC ON tblC.category_id = brt.room_category_id "
                ."GROUP BY brt.booking_id ) xx ON bd.booking_id = xx.booking_id "
                ."LEFT JOIN table_booking_allotment AS ba ON bd.booking_detail_id = ba.booking_detail_id "
                ."LEFT JOIN table_customer AS cus ON cus.customer_id = br.customer_id "
                ."LEFT JOIN table_cleaning_type AS ct ON ct.cleaning_type_id = br.cleaning_type_id "
                ."LEFT JOIN table_unit_type AS ut ON ut.unit_type_id = br.unit_type_id "
                ."LEFT JOIN table_customer_address AS ca ON ca.customer_address_id = br.customer_address_id "
                 ."LEFT JOIN table_buildings AS b ON b.building_id = ca.building_id "
                ."LEFT JOIN table_calender_slots AS cst ON cst.slot_id = bd.slot_id "
        ."LEFT JOIN table_payment_history AS tph ON tph.payment_id=br.payment_id";
    $clms = "sum(IF(ba.booking_status = 'CB',1,0)) AS totcb,count(ba.booking_allotment_id) AS cnt_auto_id,br.booking_id, bd.booking_detail_id,cus.customer_name,cus.customer_number,br.cleaning_actual_cost,br.total_cost_paid,br.packg_start_date,br.packg_end_date,ca.customer_address,"."b.building_name,ct.cleaning_type,ut.unit_name,ba.booking_status,br.status,tph.payment_mode,"
               ."xx.room_detail";
    $auRec = $_objAdmin->_getSelectList($table,$clms, $limit,$where.$sort,'');
    $auRecCount = $_objAdmin->_getSelectList($table,'count(*) as total', '', $where);
    $total=count($auRecCount);
    $dataReturn = array();
    $dataReturn['meta'] = array(
        'page' => $page,
        'pages' => ceil($total/$perPage),
        'total' => $total,
        'perpage' =>$perPage,
        'sort' => $sortorder,
        'field' => $sortname
    );
    $dataReturn['data'] = array();
    foreach($auRec as $auRecTemp){
        $temp['booking_id'] = $auRecTemp->booking_id;
       //$temp['bookingId'] = $auRecTemp->booking_id;
        $temp['customer_name'] = $auRecTemp->customer_name;
        $temp['unit_name']     = $auRecTemp->unit_name;
        $temp['cleaning_type'] = $auRecTemp->cleaning_type;
        $temp['building_name'] = $auRecTemp->building_name;
        $temp['total_cost_paid'] = $auRecTemp->total_cost_paid;
        $temp['room_detail'] = $auRecTemp->room_detail;
        //$temp['customer_address'] = $auRecTemp->customer_address;
        $temp['packg_start_date'] = $auRecTemp->packg_start_date;
        $temp['packg_end_date'] = $auRecTemp->packg_end_date;
        $temp['payment_mode'] = $auRecTemp->payment_mode;
        if($auRecTemp->payment_mode!='cash'){
           $temp['payment_status'] = 'Paid'; 
        }else{
            if($auRecTemp->totcb<$auRecTemp->cnt_auto_id){
                $temp['payment_status'] = 'Unpaid'; 
            }else{
                 $temp['payment_status'] = 'Paid'; 
            }
        }
        $dataReturn['data'][] = $temp;
    }
    echo json_encode($dataReturn, true);
    die;
}
/* Added by Azeez*/
if($_REQUEST['pageType'] == 'CleaningSchedule'){
    $_objAdmin = new Admin();
    $page = $_REQUEST['datatable']['pagination']['page'];
    $perPage = $_REQUEST['datatable']['pagination']['perpage'];
    $sortname = $_REQUEST['datatable']['sort']['field'];
    $sortorder = $_REQUEST['datatable']['sort']['sort'];
    $search = $_REQUEST['datatable']['query']['generalSearch'];
   // print_r($search);die;

    if (!$sortname)
        $sortname = 'booking_id';
    if (!$sortorder)
        $sortorder = 'desc';

    $sort = " ORDER BY $sortname $sortorder";

    if (!$page)
        $page = 1;
    if (!$perPage)
        $perPage = 50;

    $start = (($page - 1) * $perPage);

    $limit = "$start, $perPage";
    $query = $_REQUEST['query'];
      $where='';
   
   if($_SESSION['bookingStatus']!=NULL){

        if($_SESSION['bookingStatus']=='U'){//UNpaid check
            //$bookingStatus=" AND ba.booking_status!='CB' AND tph.payment_mode='cash' ";


            $allBookings=$_objAdmin->_getSelectList2('table_booking_register AS outb RIGHT JOIN ( SELECT ba.booking_allotment_id AS booking_allotment_id, group_concat(if(ba.booking_status = "CB" , "NA", ba.booking_status)) AS aggr_status, group_concat(ba.booking_status) AS booking_status, bd.booking_detail_id AS booking_detail_id, br.booking_id FROM table_booking_details AS bd LEFT JOIN table_booking_register AS br ON br.booking_id = bd.booking_id LEFT JOIN table_booking_allotment AS ba ON ba.booking_detail_id = bd.booking_detail_id group by br.booking_id )inb ON inb.booking_id = outb.booking_id left join table_payment_history as tph on outb.payment_id= tph.payment_id',"outb.booking_id",''," inb.aggr_status NOT LIKE '%NA%' AND tph.payment_mode='cash'");
            if(!empty($allBookings)){
                $bookingIds='';
                foreach($allBookings as $key=>$book){

                    $bookingIds=$bookingIds.','.$book->booking_id;

                }
                $bookingIds=ltrim($bookingIds,',');
                $where.="br.booking_id IN(".$bookingIds.")";
            }else{
                $where.="br.booking_id=''";
            }

        }else if($_SESSION['bookingStatus']=='P'){//Paid Check
            $bookingStatus=" AND ba.booking_status='CB' OR tph.payment_mode!='cash' ";

            $allBookings=$_objAdmin->_getSelectList2('table_booking_register AS outb RIGHT JOIN ( SELECT ba.booking_allotment_id AS booking_allotment_id, group_concat(if(ba.booking_status != "CB" , "NA", ba.booking_status)) AS aggr_status, group_concat(ba.booking_status) AS booking_status, bd.booking_detail_id AS booking_detail_id, br.booking_id FROM table_booking_details AS bd LEFT JOIN table_booking_register AS br ON br.booking_id = bd.booking_id LEFT JOIN table_booking_allotment AS ba ON ba.booking_detail_id = bd.booking_detail_id group by br.booking_id )inb ON inb.booking_id = outb.booking_id left join table_payment_history as tph on outb.payment_id= tph.payment_id',"outb.booking_id",''," inb.aggr_status NOT LIKE '%NA%' OR tph.payment_mode!='cash'");
            if(!empty($allBookings)){
                $bookingIds='';
                foreach($allBookings as $key=>$book){

                    $bookingIds=$bookingIds.','.$book->booking_id;

                }
                $bookingIds=ltrim($bookingIds,',');
                $where.="br.booking_id IN(".$bookingIds.")";
            }else{
                $where.="br.booking_id=''";
            }
        }else if($_SESSION['bookingStatus']=='CB'){//Complete BOoking Check

            $allBookings=$_objAdmin->_getSelectList2('table_booking_register AS outb RIGHT JOIN ( SELECT ba.booking_allotment_id AS booking_allotment_id, group_concat(if(ba.booking_status != "CB" , "NA", ba.booking_status)) AS aggr_status, group_concat(ba.booking_status) AS booking_status, bd.booking_detail_id AS booking_detail_id, br.booking_id FROM table_booking_details AS bd LEFT JOIN table_booking_register AS br ON br.booking_id = bd.booking_id LEFT JOIN table_booking_allotment AS ba ON ba.booking_detail_id = bd.booking_detail_id group by br.booking_id )inb ON inb.booking_id = outb.booking_id ',"outb.booking_id",''," inb.aggr_status NOT LIKE '%NA%'");
            
            if(!empty($allBookings)){
                $bookingIds='';
                foreach($allBookings as $key=>$book){

                    $bookingIds=$bookingIds.','.$book->booking_id;

                }
                $bookingIds=ltrim($bookingIds,',');
                $where.="br.booking_id IN(".$bookingIds.")";
            }else{
                $where.="br.booking_id=''";
            }
           
        }else if($_SESSION['bookingStatus']=='CL'){//Cancelled Check
           // $bookingStatus=" AND ba.booking_status='CL'";

            $allBookings=$_objAdmin->_getSelectList2('table_booking_register AS outb RIGHT JOIN ( SELECT ba.booking_allotment_id AS booking_allotment_id, group_concat(if(ba.booking_status != "CL" , "NA", ba.booking_status)) AS aggr_status, group_concat(ba.booking_status) AS booking_status, bd.booking_detail_id AS booking_detail_id, br.booking_id FROM table_booking_details AS bd LEFT JOIN table_booking_register AS br ON br.booking_id = bd.booking_id LEFT JOIN table_booking_allotment AS ba ON ba.booking_detail_id = bd.booking_detail_id group by br.booking_id )inb ON inb.booking_id = outb.booking_id ',"outb.booking_id",''," inb.aggr_status NOT LIKE '%NA%'");

            if(!empty($allBookings)){
                $bookingIds='';
                foreach($allBookings as $key=>$book){

                    $bookingIds=$bookingIds.','.$book->booking_id;

                }
                $bookingIds=ltrim($bookingIds,',');
                $where.="br.booking_id IN(".$bookingIds.")";
            }else{
                $where.="br.booking_id=''";
            }
      


        }else if($_SESSION['bookingStatus']=='AL'){//Cancelled Check
            //$bookingStatus=" AND ba.booking_status='AL'";

             $allBookings=$_objAdmin->_getSelectList2('table_booking_register AS outb RIGHT JOIN ( SELECT ba.booking_allotment_id AS booking_allotment_id, group_concat(if(ba.booking_status != "AL" , "NA", ba.booking_status)) AS aggr_status, group_concat(ba.booking_status) AS booking_status, bd.booking_detail_id AS booking_detail_id, br.booking_id FROM table_booking_details AS bd LEFT JOIN table_booking_register AS br ON br.booking_id = bd.booking_id LEFT JOIN table_booking_allotment AS ba ON ba.booking_detail_id = bd.booking_detail_id group by br.booking_id )inb ON inb.booking_id = outb.booking_id ',"outb.booking_id",''," inb.aggr_status NOT LIKE '%NA%'");

            if(!empty($allBookings)){
                $bookingIds='';
                foreach($allBookings as $key=>$book){

                    $bookingIds=$bookingIds.','.$book->booking_id;

                }
                $bookingIds=ltrim($bookingIds,',');
                $where.="br.booking_id IN(".$bookingIds.")";
            }else{
                $where.="br.booking_id=''";
            }

        }else if($_SESSION['bookingStatus']=='UA'){//Cancelled Check
            $bookingStatus=" AND ba.booking_status='UA'";

            $allBookings=$_objAdmin->_getSelectList2('table_booking_register AS outb RIGHT JOIN ( SELECT ba.booking_allotment_id AS booking_allotment_id, group_concat(if(ba.booking_status != "UA" , "NA", ba.booking_status)) AS aggr_status, group_concat(ba.booking_status) AS booking_status, bd.booking_detail_id AS booking_detail_id, br.booking_id FROM table_booking_details AS bd LEFT JOIN table_booking_register AS br ON br.booking_id = bd.booking_id LEFT JOIN table_booking_allotment AS ba ON ba.booking_detail_id = bd.booking_detail_id group by br.booking_id )inb ON inb.booking_id = outb.booking_id ',"outb.booking_id",''," inb.aggr_status NOT LIKE '%NA%'");

            if(!empty($allBookings)){
                $bookingIds='';
                foreach($allBookings as $key=>$book){

                    $bookingIds=$bookingIds.','.$book->booking_id;

                }
                $bookingIds=ltrim($bookingIds,',');

                $where.="br.booking_id IN(".$bookingIds.")";
            }else{
                $where.="br.booking_id=''";
            }
        }
    }else{
        $bookingStatus='ba.booking_status="UA"';
    }
	
    if($_SESSION['buildingFil']!=NULL || $_SESSION['buildingFil']!=''){
        $where .=" AND b.building_id=".$_SESSION['buildingFil'];
    }
    if($_SESSION['customerFil']!=NULL || $_SESSION['customerFil']!=''){
        $where .=" AND cus.customer_id=".$_SESSION['customerFil'];
    }

    if(isset($bookingStatus) && $bookingStatus != ""){
        $where .= "$bookingStatus"; 
    }
   
    

   
    if($_SESSION['FromBookingList']!=''){
        $fromdate=" AND  br.packg_start_date >= '".date('Y-m-d', strtotime($_SESSION['FromBookingList']))."'";
        $where.="$fromdate ";
    }
    if($_SESSION['ToBookingList']!=''){
        $todate=" and br.packg_start_date <= '".date('Y-m-d', strtotime($_SESSION['ToBookingList']))."'";
        $where.="$todate ";
    }
    if(isset($search) && !empty($search)){
       // print_r($search);die;
        $where .= " and (br.booking_id like '%" . $search . "%' or cus.customer_name like '%" . $search . "%' or cus.customer_number like '%" . $search . "%' or b.building_name like '%" . $search . "%')";
    }


    if($query) $where .= " AND $qtype LIKE '%$query%'";
   // if(isset($bookingStatus) && $bookingStatus == ""){
        $where .=" Group by br.booking_id";
    //}
    if($_SESSION['userLoginType'] == 5 && isset($_SESSION['supervisor_id']) && !empty($_SESSION['supervisor_id'])){
        $auRec=$this->_getSelectList2('table_supervisor',"*",'',"supervisor_id = '".$_SESSION['supervisor_id']."'",'');
        $where .= " and b.building_id = '".$auRec[0]->building_id."'";
    }
   // print_r($where);die;
    $table = "table_booking_register AS br LEFT JOIN table_booking_details AS bd ON br.booking_id = bd.booking_id "."JOIN (SELECT GROUP_CONCAT(CONCAT(brt.counter,' ',tblC.category_name)) AS room_detail,brt.booking_id FROM table_booking_room_type_mapping AS brt "
                ."LEFT JOIN table_category AS tblC ON tblC.category_id = brt.room_category_id "
                ."GROUP BY brt.booking_id ) xx ON bd.booking_id = xx.booking_id "
                ."LEFT JOIN table_booking_allotment AS ba ON bd.booking_detail_id = ba.booking_detail_id "
                ."LEFT JOIN table_customer AS cus ON cus.customer_id = br.customer_id "
                ."LEFT JOIN table_cleaning_type AS ct ON ct.cleaning_type_id = br.cleaning_type_id "
                ."LEFT JOIN table_unit_type AS ut ON ut.unit_type_id = br.unit_type_id "
                ."LEFT JOIN table_customer_address AS ca ON ca.customer_address_id = br.customer_address_id "
                 ."LEFT JOIN table_buildings AS b ON b.building_id = ca.building_id "
                ."LEFT JOIN table_calender_slots AS cst ON cst.slot_id = bd.slot_id "
        ."LEFT JOIN table_payment_history AS tph ON tph.payment_id=br.payment_id";
    $clms = "sum(IF(ba.booking_status = 'CB',1,0)) AS totcb,count(ba.booking_allotment_id) AS cnt_auto_id,br.booking_id, bd.booking_detail_id,cus.customer_name,cus.customer_number,br.cleaning_actual_cost,br.total_cost_paid,br.packg_start_date,br.packg_end_date,ca.customer_address,"."b.building_name,b.travelling_time_for_cleaning,ct.cleaning_type,ut.unit_name,ba.booking_status,br.status,tph.payment_mode,"
               ."xx.room_detail";
    $auRec = $_objAdmin->_getSelectList($table,$clms, $limit,$where.$sort,'');
    $auRecCount = $_objAdmin->_getSelectList($table,'count(*) as total', '', $where);
    $total=count($auRecCount);
    $dataReturn = array();
    $dataReturn['meta'] = array(
        'page' => $page,
        'pages' => ceil($total/$perPage),
        'total' => $total,
        'perpage' =>$perPage,
        'sort' => $sortorder,
        'field' => $sortname
    );
    $dataReturn['data'] = array();
    foreach($auRec as $auRecTemp){
		$_objAdmin = new Admin();
		$room_detail = $auRecTemp->room_detail;
		$tb = "`table_cleaning_type` AS CTYPE LEFT JOIN table_cleaning_time as CTIME ON CTYPE.cleaning_type_id = CTIME.cleaning_type_id";
		$colmns = "cleaning_type,time_required";
		$whr ="unit_type = '".$room_detail."'";

		$time_r = $_objAdmin->_getselectList2($tb,$colmns,'',$whr);
        $temp['booking_id'] = $auRecTemp->booking_id;
		$temp['booking_detail_id'] = $auRecTemp->booking_detail_id;
		$temp['building_name'] = $auRecTemp->building_name;
        $temp['unit_name']     = $auRecTemp->unit_name;
        $temp['cleaning_type'] = $auRecTemp->cleaning_type;
        $temp['room_detail'] = $auRecTemp->room_detail;
		//$total_time = date('H:i:s',((strtotime($time_r[0]->time_required)+(strtotime($auRecTemp->travelling_time_for_cleaning)))));
		$total_time =strtotime($time_r[0]->time_required)+strtotime($auRecTemp->travelling_time_for_cleaning)- strtotime('00:00:00');  
		$temp['time_required'] = date('H:i:s',$total_time);
        $dataReturn['data'][] = $temp;
    }
    echo json_encode($dataReturn, true);
    die;
}


if($_REQUEST['pageType'] == 'bookingDetailList'){
    $_objAdmin = new Admin();
    $page = $_REQUEST['datatable']['pagination']['page'];
    $perPage = $_REQUEST['datatable']['pagination']['perpage'];
    $sortname = $_REQUEST['datatable']['sort']['field'];
    $sortorder = $_REQUEST['datatable']['sort']['sort'];
    $search = $_REQUEST['datatable']['query']['generalSearch'];
    if(!$sortname)
        $sortname='cleaning_date';
    if(!$sortorder)
        $sortorder='asc';

    $sort = " ORDER BY $sortname $sortorder";

    if(!$page)
        $page = 1;
    if(!$perPage)
        $perPage = 50;

    $start = (($page - 1) * $perPage);

    $limit = "$start, $perPage";
    $query = $_REQUEST['query'];

   $where = " br.booking_id=".$_REQUEST['id'];
    if(isset($search) && !empty($search)){
       
        $where .= " and (br.booking_id like '%" . $search . "%' or cus.customer_name like '%" . $search . "%'". "or bd.cleaning_date like '%" . $search . "%' or cst.slot_start_time like '%" . $search . "%'". "or cst.slot_end_time like '%" . $search . "%' or c.cleaner_name like '%" . $search . "%' or s.supervisor_name like '%" . $search . "%')";
    }
    $table = "table_booking_register AS br "
                ."LEFT JOIN table_booking_details AS bd ON br.booking_id = bd.booking_id "
                ."JOIN "
                ."("
                ."SELECT GROUP_CONCAT( CONCAT(brt.counter,' ',tblC.category_name)) AS room_detail,brt.booking_id "
                ."FROM   table_booking_room_type_mapping AS brt "
                ."LEFT JOIN table_category AS tblC ON tblC.category_id = brt.room_category_id "
           
                ."GROUP BY brt.booking_id ) xx ON bd.booking_id = xx.booking_id "
                ."LEFT JOIN table_booking_allotment AS ba ON bd.booking_detail_id = ba.booking_detail_id "
                ."LEFT JOIN  table_calender_slots AS cs ON cs.slot_id = bd.slot_id "
               
                ."LEFT JOIN table_cleaner AS c ON c.cleaner_id = bd.cleaner_id "
                ."LEFT JOIN table_supervisor AS s ON s.supervisor_id = c.supervisor_id "
                ."LEFT JOIN table_customer AS cus ON cus.customer_id = br.customer_id "
                ."LEFT JOIN table_cleaning_type AS ct ON ct.cleaning_type_id = br.cleaning_type_id "
                ."LEFT JOIN table_customer_address AS ca ON ca.customer_address_id = br.customer_address_id "
                 ."LEFT JOIN table_buildings AS b ON b.building_id = ca.building_id "
                ."LEFT JOIN table_calender_slots AS cst ON cst.slot_id = bd.slot_id "
                ."LEFT JOIN table_reject_reason AS rs ON  rs.reject_reason_id = ba.reject_reason_id "
    ."LEFT JOIN table_payment_history AS tph ON  tph.payment_id=br.payment_id ";

    $clms =  " br.booking_id, bd.booking_detail_id,cus.customer_name,br.cleaning_actual_cost,br.total_cost_paid,br.packg_start_date,br.packg_end_date,ca.customer_address,"
               ."b.building_name,b.travelling_time_for_cleaning,ct.cleaning_type,cst.slot_end_time,cst.slot_start_time,bd.cleaning_date,bd.amount,c.cleaner_name,"
               ."s.supervisor_name,ba.booking_alloted,ba.booking_alloted_date,ba.not_alloted_reason,ba.booking_accepted,"
               ."ba.booking_accepted_date,ba.other_rejection_reason,rs.reject_reason,ba.booking_status,br.status,"
               ."xx.room_detail";
    
    $auRec = $_objAdmin->_getSelectList($table,$clms,$limit,$where. ' ' .$sort, '', '');
    $auRecCount = $_objAdmin->_getSelectList($table, 'count(*) as total', '', $where);
    $total = $auRecCount[0]->total;

    $dataReturn = array();
    $dataReturn['meta'] = array(
        'page' => $page,
        'pages' => ceil($total / $perPage),
        'total' => $total,
        'perpage' => $perPage,
        'sort' => $sortorder,
        'field' => $sortname
    );
    $dataReturn['data'] = array();
    foreach($auRec as $auRecTemp){
		$_objAdmin = new Admin();
		$room_detail = $auRecTemp->room_detail;
		$tb = "`table_cleaning_type` AS CTYPE LEFT JOIN table_cleaning_time as CTIME ON CTYPE.cleaning_type_id = CTIME.cleaning_type_id";
		$colmns = "cleaning_type,time_required";
		$whr ="unit_type = '".$room_detail."'";
		$time_r = $_objAdmin->_getselectList2($tb,$colmns,'',$whr);
        $temp['booking_detail_id'] = $auRecTemp->booking_detail_id;
        //$temp['booking_id'] = $auRecTemp->booking_id;
        $temp['customer_name'] = $auRecTemp->customer_name;
        $temp['amount'] = $auRecTemp->amount;
        $temp['cleaning_date'] = preg_replace('/[^A-Za-z0-9\. -]/', '',$auRecTemp->cleaning_date);
        $temp['slot_start_time'] = preg_replace('/[^A-Za-z0-9\. :-]/', '',$auRecTemp->slot_start_time);
        $temp['slot_end_time'] = preg_replace('/[^A-Za-z0-9\. :-]/', '', $auRecTemp->slot_end_time);
        $temp['cleaner_name'] = $auRecTemp->cleaner_name;
        $temp['supervisor_name'] = $auRecTemp->supervisor_name;
		$oneway_travel = strtotime($auRecTemp->travelling_time_for_cleaning)-strtotime("00:00:00");
		$twoway_travel = date("H:i:s",strtotime($auRecTemp->travelling_time_for_cleaning)+$oneway_travel);
		$total_time =strtotime($time_r[0]->time_required)+strtotime($twoway_travel)- strtotime('00:00:00');  
		$temp['time_required'] = date('H:i:s',$total_time);
        switch($auRecTemp->booking_status){
                case ("UA"):
                    $bookingStatus = "Not Alloted";
                    break;
                case ("AL"):
                    $bookingStatus = "Alloted";
                    break;
                case ("AC"):
                    $bookingStatus = "Accepted";
                    break;
                case ("RJ"):
                    $bookingStatus = "Rejected";
                    break;
                case ("RD"):
                    $bookingStatus = "Reached";
                    break;
                case ("SC"):
                    $bookingStatus = "Start Cleaning";
                    break;
                case ("CC"):
                    $bookingStatus = "Completed Cleaning";
                    break;
                case ("RP"):
                    $bookingStatus = "Payment Received";
                    break;
                case ("CB"):
                    $bookingStatus = "Complete Booking";
                    break;
                case ("CL"):
                    $bookingStatus = "Cancel";
                    break;
                default:
                    $bookingStatus = "Not Alloted";
                    break;
            }
        $temp['booking_status'] = $bookingStatus;
        $dataReturn['data'][] = $temp;
    }
    echo json_encode($dataReturn, true);
    die;
}

if ($_REQUEST['pageType'] == 'cleaningType') {
    $_objAdmin = new Admin();
    $page = $_REQUEST['datatable']['pagination']['page'];
    $perPage = $_REQUEST['datatable']['pagination']['perpage'];
    $sortname = $_REQUEST['datatable']['sort']['field'];
    $sortorder = $_REQUEST['datatable']['sort']['sort'];
    $search = $_REQUEST['datatable']['query']['generalSearch'];

    if (!$sortname)
        $sortname = 'cleaning_type';
    if (!$sortorder)
        $sortorder = 'asc';

    $sort = " ORDER BY $sortname $sortorder";

    if (!$page)
        $page = 1;
    if (!$perPage)
        $perPage = 50;

    $start = (($page - 1) * $perPage);

    $limit = "$start, $perPage";
    $query = $_REQUEST['query'];
    $where = "(c.status != 'D')";
    $table = 'table_cleaning_type as c';
    $clms = 'c.*';
    if(isset($_SESSION['order']) && $_SESSION['order'] == 'updated') {
        $sort = " ORDER BY c.last_update_date desc";
        unset($_SESSION['order']);
    }
    if (isset($search) && !empty($search)) {
        $where .= " and (c.cleaning_type like '%" . $search . "%')";
    }
    $auRec = $_objAdmin->_getSelectList($table, $clms, $limit, $where . ' ' . $sort, '', '');
    $auRecCount = $_objAdmin->_getSelectList($table, 'count(*) as total', '', $where);
    $total = $auRecCount[0]->total;

    $dataReturn = array();
    $dataReturn['meta'] = array(
        'page' => $page,
        'pages' => ceil($total / $perPage),
        'total' => $total,
        'perpage' => $perPage,
        'sort' => $sortorder,
        'field' => $sortname
    );
    $dataReturn['data'] = array();
    foreach ($auRec as $auRecTemp) {
        $temp['cleaning_type_id'] = $auRecTemp->cleaning_type_id;
        $temp['cleaning_type'] = $auRecTemp->cleaning_type;
        
        if ($auRecTemp->status == 'A') {
            $temp['Status'] = 1;
        } else if ($auRecTemp->status == 'I') {
            $temp['Status'] = 2;
        } else {
            $temp['Status'] = 2;
        }
        $dataReturn['data'][] = $temp;
    }
    echo json_encode($dataReturn, true);
    die;
}

if ($_REQUEST['pageType'] == 'cleaningTime') {
    $_objAdmin = new Admin();
    $page = $_REQUEST['datatable']['pagination']['page'];
    $perPage = $_REQUEST['datatable']['pagination']['perpage'];
    $sortname = $_REQUEST['datatable']['sort']['field'];
    $sortorder = $_REQUEST['datatable']['sort']['sort'];
    $search = $_REQUEST['datatable']['query']['generalSearch'];

    if (!$sortname)
        $sortname = 'cleaning_type_id';
    if (!$sortorder)
        $sortorder = 'asc';

    $sort = " ORDER BY $sortname $sortorder";

    if (!$page)
        $page = 1;
    if (!$perPage)
        $perPage = 50;

    $start = (($page - 1) * $perPage);

    $limit = "$start, $perPage";
    $query = $_REQUEST['query'];
    $where = "(c.status != 'D')";
    $table = 'table_cleaning_time as c left join table_cleaning_type as b ON c.cleaning_type_id = b.cleaning_type_id';
    $clms = 'c.*,b.cleaning_type';
    if(isset($_SESSION['order']) && $_SESSION['order'] == 'updated') {
        $sort = " ORDER BY c.last_update_date desc";
        unset($_SESSION['order']);
    }
    if (isset($search) && !empty($search)) {
        $where .= " and (c.cleaning_type_id like '%" . $search . "%')";
    }
    $auRec = $_objAdmin->_getSelectList($table, $clms, $limit, $where . ' ' . $sort, '', '');
    $auRecCount = $_objAdmin->_getSelectList($table, 'count(*) as total', '', $where);
    $total = $auRecCount[0]->total;

    $dataReturn = array();
    $dataReturn['meta'] = array(
        'page' => $page,
        'pages' => ceil($total / $perPage),
        'total' => $total,
        'perpage' => $perPage,
        'sort' => $sortorder,
        'field' => $sortname
    );
    $dataReturn['data'] = array();
    foreach ($auRec as $auRecTemp) {
        $temp['cleaning_time_id'] = $auRecTemp->cleaning_time_id;
        $temp['cleaning_type_id'] = $auRecTemp->cleaning_type;
    	$temp['unit_type'] = $auRecTemp->unit_type;
	    $temp['time_required'] = $auRecTemp->time_required;
        
        if ($auRecTemp->status == 'A') {
            $temp['Status'] = 1;
        } else if ($auRecTemp->status == 'I') {
            $temp['Status'] = 2;
        } else {
            $temp['Status'] = 2;
        }
        $dataReturn['data'][] = $temp;
    }
    echo json_encode($dataReturn, true);
    die;
}

if($_REQUEST['pageType'] == 'cleaning_type_mapping') {
    $_objAdmin = new Admin();
    $page = $_REQUEST['datatable']['pagination']['page'];
    $perPage = $_REQUEST['datatable']['pagination']['perpage'];
    $sortname = $_REQUEST['datatable']['sort']['field'];
    $sortorder = $_REQUEST['datatable']['sort']['sort'];
    $search = $_REQUEST['datatable']['query']['generalSearch'];

    if (!$sortname)
        $sortname = 'cleaning_unit_mapping_id';
    if (!$sortorder)
        $sortorder = 'desc';

    $sort = " ORDER BY $sortname $sortorder";
    
    if (!$page)
        $page = 1;
    if (!$perPage)
        $perPage = 50;

    $start = (($page - 1) * $perPage);

    $limit = "$start, $perPage";
    $query = $_REQUEST['query'];
    $where = "(tcum.status != 'D' and tcum.status!='')";
    
    $table = 'table_cleaning_unit_mapping  as tcum left join table_cleaning_type as c on  tcum.cleaning_type_id = c.cleaning_type_id left join table_unit_type as u on u.unit_type_id = tcum.unit_type_id';
   
    $clms = 'tcum.*,c.cleaning_type,u.unit_name';
    
    if(isset($search) && !empty($search)){
        $where .= " and (c.cleaning_type like '%" . $search . "%')";
    }
    $auRec = $_objAdmin->_getSelectList($table, $clms, $limit, $where . ' ' . $sort, '', '');
    $auRecCount = $_objAdmin->_getSelectList($table, 'count(*) as total', '', $where);
    $total = $auRecCount[0]->total;

    $dataReturn = array();
    $dataReturn['meta'] = array(
        'page' => $page,
        'pages' => ceil($total / $perPage),
        'total' => $total,
        'perpage' => $perPage,
        'sort' => $sortorder,
        'field' => $sortname
    );
    $dataReturn['data'] = array();
    foreach($auRec as $auRecTemp){
        $temp['cleaning_unit_mapping_id'] = $auRecTemp->cleaning_unit_mapping_id;
        $temp['cleaning_type'] = $auRecTemp->cleaning_type;
        $temp['unit_name'] = $auRecTemp->unit_name;
        if($auRecTemp->status == 'A'){
            $temp['Status'] = 1;
        } else if($auRecTemp->status == 'I'){
            $temp['Status'] = 2;
        } else {
            $temp['Status'] = 2;
        }
        $dataReturn['data'][] = $temp;
    }
    echo json_encode($dataReturn, true);
    die;
}

if ($_REQUEST['pageType'] == 'customerList') {
    $_objAdmin = new Admin();
    $page = $_REQUEST['datatable']['pagination']['page'];
    $perPage = $_REQUEST['datatable']['pagination']['perpage'];
    $sortname = $_REQUEST['datatable']['sort']['field'];
    $sortorder = $_REQUEST['datatable']['sort']['sort'];
    $search = $_REQUEST['datatable']['query']['generalSearch'];

    if (!$sortname)
        $sortname = 'customer_name';
    if (!$sortorder)
        $sortorder = 'asc';

    $sort = " ORDER BY $sortname $sortorder";

    if (!$page)
        $page = 1;
    if (!$perPage)
        $perPage = 50;

    $start = (($page - 1) * $perPage);
    if(isset($_SESSION['order']) && $_SESSION['order'] == 'updated') {
        $sort = " ORDER BY table_customer.last_update_date desc";
        unset($_SESSION['order']);
    }
    $limit = "$start, $perPage";
    $query = $_REQUEST['query'];
    $where = "(table_customer.status != 'D')";
    $table = 'table_customer';
    $clms = 'table_customer.customer_id,table_customer.customer_name,table_customer.customer_email,table_customer.customer_number,table_customer.status';

    if (isset($search) && !empty($search)) {
        $where .= " and (table_customer.customer_name like '%" . $search . "%' or table_customer.customer_email like '%" . $search . "%'"
                . " or table_customer.customer_number like '%" . $search . "%')";
    }
    $auRec = $_objAdmin->_getSelectList($table, $clms, $limit, $where . ' ' . $sort, '', '');
    $auRecCount = $_objAdmin->_getSelectList('table_customer', 'count(*) as total', '', $where);
    $total = $auRecCount[0]->total;

    $dataReturn = array();
    $dataReturn['meta'] = array(
        'page' => $page,
        'pages' => ceil($total / $perPage),
        'total' => $total,
        'perpage' => $perPage,
        'sort' => $sortorder,
        'field' => $sortname
    );
    $dataReturn['data'] = array();
    foreach ($auRec as $auRecTemp) {
        $temp['customer_id'] = $auRecTemp->customer_id;
        $temp['customer_name'] = $auRecTemp->customer_name;
        $temp['customer_number'] = $auRecTemp->customer_number;
        $temp['customer_email'] = $auRecTemp->customer_email;
        $temp['status'] = $auRecTemp->status;
        if ($auRecTemp->status == 'A') {
            $temp['Status'] = 1;
        } else if ($auRecTemp->status == 'I') {
            $temp['Status'] = 2;
        } else {
            $temp['Status'] = 2;
        }
        $dataReturn['data'][] = $temp;
    }
    echo json_encode($dataReturn, true);
    die;
}

if ($_REQUEST['pageType'] == 'cityList') {
    $_objAdmin = new Admin();
    $page = $_REQUEST['datatable']['pagination']['page'];
    $perPage = $_REQUEST['datatable']['pagination']['perpage'];
    $sortname = $_REQUEST['datatable']['sort']['field'];
    $sortorder = $_REQUEST['datatable']['sort']['sort'];
    $search = $_REQUEST['datatable']['query']['generalSearch'];

    if (!$sortname)
        $sortname = 'city_name';
    if (!$sortorder)
        $sortorder = 'asc';

    $sort = " ORDER BY $sortname $sortorder";

    if (!$page)
        $page = 1;
    if (!$perPage)
        $perPage = 50;

    $start = (($page - 1) * $perPage);

    $limit = "$start, $perPage";
    $query = $_REQUEST['query'];
    $where = "(city.status != 'D')";
    $table = 'city';
    $clms = 'city.*';
    
    if (isset($search) && !empty($search)) {
        $where .= " and (city.city_name like '%" . $search . "%')";
    }
    if(isset($_SESSION['order']) && $_SESSION['order'] == 'updated') {
        $sort = " ORDER BY city.last_update_date desc";
        unset($_SESSION['order']);
    }
    $auRec = $_objAdmin->_getSelectList2($table, $clms, $limit, $where . ' ' . $sort, '', '');
    $auRecCount = $_objAdmin->_getSelectList2('city', 'count(*) as total', '', $where);
    $total = $auRecCount[0]->total;

    $dataReturn = array();
    $dataReturn['meta'] = array(
        'page' => $page,
        'pages' => ceil($total / $perPage),
        'total' => $total,
        'perpage' => $perPage,
        'sort' => $sortorder,
        'field' => $sortname
    );
    $dataReturn['data'] = array();
    foreach ($auRec as $auRecTemp) {
        $temp['city_id'] = $auRecTemp->city_id;
        $temp['city_name'] = $auRecTemp->city_name;
        if ($auRecTemp->status == 'A') {
            $temp['Status'] = 1;
        } else if ($auRecTemp->status == 'I') {
            $temp['Status'] = 2;
        } else {
            $temp['Status'] = 2;
        }
        $dataReturn['data'][] = $temp;
    }
    echo json_encode($dataReturn, true);
    die;
}

if ($_REQUEST['pageType'] == 'stateList') {
    $_objAdmin = new Admin();
    $page = $_REQUEST['datatable']['pagination']['page'];
    $perPage = $_REQUEST['datatable']['pagination']['perpage'];
    $sortname = $_REQUEST['datatable']['sort']['field'];
    $sortorder = $_REQUEST['datatable']['sort']['sort'];
    $search = $_REQUEST['datatable']['query']['generalSearch'];

    if (!$sortname)
        $sortname = 'state_name';
    if (!$sortorder)
        $sortorder = 'asc';

    $sort = " ORDER BY $sortname $sortorder";

    if (!$page)
        $page = 1;
    if (!$perPage)
        $perPage = 50;

    $start = (($page - 1) * $perPage);

    $limit = "$start, $perPage";
    $query = $_REQUEST['query'];
    $where = "(state.status != 'D')";
    $table = 'state';
    $clms = 'state.*';
    
    if (isset($search) && !empty($search)) {
        $where .= " and (state.state_name like '%" . $search . "%')";
    }
    if(isset($_SESSION['order']) && $_SESSION['order'] == 'updated') {
        $sort = " ORDER BY state.last_update_date desc";
        unset($_SESSION['order']);
    }
    $auRec = $_objAdmin->_getSelectList2($table, $clms, $limit, $where . ' ' . $sort, '', '');
    $auRecCount = $_objAdmin->_getSelectList2('state', 'count(*) as total', '', $where);
    $total = $auRecCount[0]->total;
    $dataReturn = array();
    $dataReturn['meta'] = array(
        'page' => $page,
        'pages' => ceil($total / $perPage),
        'total' => $total,
        'perpage' => $perPage,
        'sort' => $sortorder,
        'field' => $sortname
    );
    $dataReturn['data'] = array();
    foreach ($auRec as $auRecTemp) {
        $temp['state_id'] = $auRecTemp->state_id;
        $temp['state_name'] = $auRecTemp->state_name;
        if ($auRecTemp->status == 'A') {
            $temp['Status'] = 1;
        } else if ($auRecTemp->status == 'I') {
            $temp['Status'] = 2;
        } else {
            $temp['Status'] = 2;
        }
        $dataReturn['data'][] = $temp;
    }
    echo json_encode($dataReturn, true);
    die;
}

if ($_REQUEST['pageType'] == 'areaList') {
    $_objAdmin = new Admin();
    $page = $_REQUEST['datatable']['pagination']['page'];
    $perPage = $_REQUEST['datatable']['pagination']['perpage'];
    $sortname = $_REQUEST['datatable']['sort']['field'];
    $sortorder = $_REQUEST['datatable']['sort']['sort'];
    $search = $_REQUEST['datatable']['query']['generalSearch'];

    if (!$sortname)
        $sortname = 'area_name';
    if (!$sortorder)
        $sortorder = 'asc';

    $sort = " ORDER BY $sortname $sortorder";

    if (!$page)
        $page = 1;
    if (!$perPage)
        $perPage = 50;

    $start = (($page - 1) * $perPage);

    $limit = "$start, $perPage";
    $query = $_REQUEST['query'];
    $where = "(a.status != 'D')";
    $table = 'table_area as a left join city as c on c.city_id = a.city_id';
    $clms = 'a.*,c.city_name';
        
    if (isset($search) && !empty($search)) {
        $where .= " and (c.city_name like '%" . $search . "%' or a.area_name like '%" . $search . "%')";
    }
    if(isset($_SESSION['order']) && $_SESSION['order'] == 'updated') {
        $sort = " ORDER BY a.last_update_date desc";
        unset($_SESSION['order']);
    }
    $auRec = $_objAdmin->_getSelectList2($table, $clms, $limit, $where . ' ' . $sort, '', '');
    $auRecCount = $_objAdmin->_getSelectList2($table, 'count(*) as total', '', $where);
    $total = $auRecCount[0]->total;

    $dataReturn = array();
    $dataReturn['meta'] = array(
        'page' => $page,
        'pages' => ceil($total / $perPage),
        'total' => $total,
        'perpage' => $perPage,
        'sort' => $sortorder,
        'field' => $sortname
    );
    $dataReturn['data'] = array();
    foreach ($auRec as $auRecTemp) {
        $temp['area_id'] = $auRecTemp->area_id;
        $temp['area_name'] = $auRecTemp->area_name;
        $temp['city_name'] = $auRecTemp->city_name;
        if ($auRecTemp->status == 'A') {
            $temp['Status'] = 1;
        } else if ($auRecTemp->status == 'I') {
            $temp['Status'] = 2;
        } else {
            $temp['Status'] = 2;
        }
        $dataReturn['data'][] = $temp;
    }
    echo json_encode($dataReturn, true);
    die;
}


if ($_REQUEST['pageType'] == 'catagoryList'){
    $_objAdmin = new Admin();
    $page = $_REQUEST['datatable']['pagination']['page'];
    $perPage = $_REQUEST['datatable']['pagination']['perpage'];
    $sortname = $_REQUEST['datatable']['sort']['field'];
    $sortorder = $_REQUEST['datatable']['sort']['sort'];
    $search = $_REQUEST['datatable']['query']['generalSearch'];

    if (!$sortname)
        $sortname = 'category_name';
    if (!$sortorder)
        $sortorder = 'asc';

    $sort = " ORDER BY $sortname $sortorder";

    if (!$page)
        $page = 1;
    if (!$perPage)
        $perPage = 50;

    $start = (($page - 1) * $perPage);

    $limit = "$start, $perPage";
    $query = $_REQUEST['query'];
    $where = "(C.status != 'D')";
    if(isset($_SESSION['order']) && $_SESSION['order'] == 'updated') {
        $sort = " ORDER BY C.last_update_date desc";
        unset($_SESSION['order']);
    }
    $table = 'table_category as C ';
    $clms = 'C.*';
    
    if(isset($search) && !empty($search)){
        $where .= " and (C.category_name like '%" . $search . "%')";
    }
    $auRec = $_objAdmin->_getSelectList($table,$clms,$limit,$where . ' ' .$sort, '', '');
    $auRecCount = $_objAdmin->_getSelectList($table, 'count(*) as total', '', $where);
    $total = $auRecCount[0]->total;

    $dataReturn = array();
    $dataReturn['meta'] = array(
        'page' => $page,
        'pages' => ceil($total / $perPage),
        'total' => $total,
        'perpage' => $perPage,
        'sort' => $sortorder,
        'field' => $sortname
    );
    $dataReturn['data'] = array();
    foreach($auRec as $auRecTemp){
        $temp['category_id'] = $auRecTemp->category_id;
        $temp['category_name'] = $auRecTemp->category_name;
        $temp['created_date'] = $auRecTemp->created_date;
        if ($auRecTemp->status == 'A') {
            $temp['Status'] = 1;
        } else if ($auRecTemp->status == 'I') {
            $temp['Status'] = 2;
        } else {
            $temp['Status'] = 2;
        }
        $dataReturn['data'][] = $temp;
    }
    echo json_encode($dataReturn, true);
    die;
}
if($_REQUEST['pageType'] == 'catagoryType'){
    $_objAdmin = new Admin();
    $page = $_REQUEST['datatable']['pagination']['page'];
    $perPage = $_REQUEST['datatable']['pagination']['perpage'];
    $sortname = $_REQUEST['datatable']['sort']['field'];
    $sortorder = $_REQUEST['datatable']['sort']['sort'];
    $search = $_REQUEST['datatable']['query']['generalSearch'];

    if (!$sortname)
        $sortname = 'category_name';
    if (!$sortorder)
        $sortorder = 'asc';

    $sort = " ORDER BY $sortname $sortorder";

    if (!$page)
        $page = 1;
    if (!$perPage)
        $perPage = 50;

    $start = (($page - 1) * $perPage);

    $limit = "$start, $perPage";
    $query = $_REQUEST['query'];
    $where = "(CT.status != 'D')";
    if(isset($_SESSION['order']) && $_SESSION['order'] == 'updated') {
        $sort = " ORDER BY CT.last_update_date desc";
        unset($_SESSION['order']);
    }
    $table = 'table_category_type as CT left join table_unit_type as UNIT on UNIT.unit_type_id = CT.unit_type_id left join '
            . 'table_cleaning_type as tct on tct.cleaning_type_id=CT.cleaning_type_id left join table_category as C on C.category_id=CT.category_id';
    $clms = 'CT.*,UNIT.unit_name,tct.cleaning_type,C.category_name';
    
    if (isset($search) && !empty($search)) {
        $where .= " and (UNIT.unit_name like '%" . $search . "%' or tct.cleaning_type like '%" . $search . "%'"
                . " or C.category_name like '%" . $search . "%')";
    }
    $auRec = $_objAdmin->_getSelectList($table, $clms, $limit, $where . ' ' . $sort, '', '');
    $auRecCount = $_objAdmin->_getSelectList($table, 'count(*) as total', '', $where);
    $total = $auRecCount[0]->total;

    $dataReturn = array();
    $dataReturn['meta'] = array(
        'page' => $page,
        'pages' => ceil($total / $perPage),
        'total' => $total,
        'perpage' => $perPage,
        'sort' => $sortorder,
        'field' => $sortname
    );
    $dataReturn['data'] = array();
    foreach($auRec as $auRecTemp){
        $temp['room_category_id'] = $auRecTemp->room_category_id;
        $temp['unit_name'] = $auRecTemp->unit_name;
        $temp['cleaning_type'] = $auRecTemp->cleaning_type;
         $temp['category_name'] = $auRecTemp->category_name;
        $temp['minimum'] = $auRecTemp->minimum;
        $temp['maximum'] = $auRecTemp->maximum;
        if($auRecTemp->status == 'A'){
            $temp['Status'] = 1;
        }else if($auRecTemp->status == 'I'){
            $temp['Status'] = 2;
        } else {
            $temp['Status'] = 2;
        }
        $dataReturn['data'][] = $temp;
    }
    echo json_encode($dataReturn, true);
    die;
}

if ($_REQUEST['pageType'] == 'serviceType') {
    $_objAdmin = new Admin();
    $page = $_REQUEST['datatable']['pagination']['page'];
    $perPage = $_REQUEST['datatable']['pagination']['perpage'];
    $sortname = $_REQUEST['datatable']['sort']['field'];
    $sortorder = $_REQUEST['datatable']['sort']['sort'];
    $search = $_REQUEST['datatable']['query']['generalSearch'];

    if (!$sortname)
        $sortname = 'service_name';
    if (!$sortorder)
        $sortorder = 'asc';

    $sort = " ORDER BY $sortname $sortorder";

    if (!$page)
        $page = 1;
    if (!$perPage)
        $perPage = 50;

    $start = (($page - 1) * $perPage);

    $limit = "$start, $perPage";
    $query = $_REQUEST['query'];
    $table = 'table_room_services';
    if(isset($_SESSION['order']) && $_SESSION['order'] == 'updated') {
        $sort = " ORDER BY last_update_date desc";
        unset($_SESSION['order']);
    }
    $clms = '*';
    $where = "(status != 'D')";
    if (isset($search) && !empty($search)) {
        $where .= " and (service_name like '%" . $search . "%')";
    }
    $auRec = $_objAdmin->_getSelectList($table, $clms, $limit, $where . ' ' . $sort, '', '');
    $auRecCount = $_objAdmin->_getSelectList($table, 'count(*) as total', '', $where);
    $total = $auRecCount[0]->total;

    $dataReturn = array();
    $dataReturn['meta'] = array(
        'page' => $page,
        'pages' => ceil($total / $perPage),
        'total' => $total,
        'perpage' => $perPage,
        'sort' => $sortorder,
        'field' => $sortname
    );
    $dataReturn['data'] = array();
    foreach ($auRec as $auRecTemp) {
        $temp['service_id'] = $auRecTemp->service_id;
        $temp['service_name'] = $auRecTemp->service_name;
        $temp['number_of_services'] = $auRecTemp->number_of_services;
        $temp['discount_percentage'] = $auRecTemp->discount_percentage;
        if ($auRecTemp->status == 'A') {
            $temp['Status'] = 1;
        } else if ($auRecTemp->status == 'I') {
            $temp['Status'] = 2;
        } else {
            $temp['Status'] = 2;
        }
        $dataReturn['data'][] = $temp;
    }
    echo json_encode($dataReturn, true);
    die;
}


if ($_REQUEST['pageType'] == 'serviceTypePckg') {
    $_objAdmin = new Admin();
    $page = $_REQUEST['datatable']['pagination']['page'];
    $perPage = $_REQUEST['datatable']['pagination']['perpage'];
    $sortname = $_REQUEST['datatable']['sort']['field'];
    $sortorder = $_REQUEST['datatable']['sort']['sort'];
    $search = $_REQUEST['datatable']['query']['generalSearch'];

    if (!$sortname)
        $sortname = 'category_name';
    if (!$sortorder)
        $sortorder = 'asc';

    $sort = " ORDER BY $sortname $sortorder";

    if (!$page)
        $page = 1;
    if (!$perPage)
        $perPage = 50;

    $start = (($page - 1) * $perPage);

    $limit = "$start, $perPage";
    $query = $_REQUEST['query'];
    $where = "(PCKG.status != 'D')";
    if(isset($_SESSION['order']) && $_SESSION['order'] == 'updated') {
        $sort = " ORDER BY PCKG.last_update_date desc";
        unset($_SESSION['order']);
    }
    $table = 'table_room_packages as PCKG left join table_category as CatType on CatType.category_id = PCKG.category_id left '
            . 'join table_cleaning_type as TYPE on TYPE.cleaning_type_id = PCKG.cleaning_type_id left join table_unit_type as tut on tut.unit_type_id=PCKG.unit_type_id left join table_room_services as trs on trs.service_id=PCKG.service_id cross join table_config as tc';
    $clms = 'PCKG.*,CatType.category_name,TYPE.cleaning_type,tut.unit_name,tc.vat,trs.service_name';
    
    if (isset($search) && !empty($search)) {
        $where .= " and (CatType.category_name like '%" . $search . "%'"
                . " or TYPE.cleaning_type like '%" . $search . "%' )";
    }
    $auRec = $_objAdmin->_getSelectList($table, $clms, $limit, $where . ' ' . $sort, '', '');
    $auRecCount = $_objAdmin->_getSelectList($table, 'count(*) as total', '', $where);
    $total = $auRecCount[0]->total;

    $dataReturn = array();
    $dataReturn['meta'] = array(
        'page' => $page,
        'pages' => ceil($total / $perPage),
        'total' => $total,
        'perpage' => $perPage,
        'sort' => $sortorder,
        'field' => $sortname
    );
    $dataReturn['data'] = array();
    foreach ($auRec as $auRecTemp) {
        $temp['room_package_id'] = $auRecTemp->room_package_id;
        $temp['category_name'] = $auRecTemp->category_name;
        $temp['unit_name'] = $auRecTemp->unit_name;
        $temp['cleaning_type'] = $auRecTemp->cleaning_type;
        
        if($auRecTemp->service_type=='o'){
            $service_type="OneTime";
        }else{
            $service_type="LongTerm";
        }
        $temp['service_type'] = $service_type;
        $temp['service_name'] = $auRecTemp->service_name;
        $temp['package_rate'] = $auRecTemp->package_rate;
        $temp['counter'] = $auRecTemp->counter;
        $temp['linen_change_per_room'] = $auRecTemp->linen_change_per_room;
        $temp['balcony_price'] = $auRecTemp->balcony_price;
        $temp['vat'] = "Included ".$auRecTemp->vat."% VAT";
        if ($auRecTemp->status == 'A') {
            $temp['Status'] = 1;
        } else if ($auRecTemp->status == 'I') {
            $temp['Status'] = 2;
        } else {
            $temp['Status'] = 2;
        }
        $dataReturn['data'][] = $temp;
    }
    echo json_encode($dataReturn, true);
    die;
}

if ($_REQUEST['pageType'] == 'buildings') {
    $_objAdmin = new Admin();
    $page = $_REQUEST['datatable']['pagination']['page'];
    $perPage = $_REQUEST['datatable']['pagination']['perpage'];
    $sortname = $_REQUEST['datatable']['sort']['field'];
    $sortorder = $_REQUEST['datatable']['sort']['sort'];
    $search = $_REQUEST['datatable']['query']['generalSearch'];

    if (!$sortname)
        $sortname = 'building_name';
    if (!$sortorder)
        $sortorder = 'asc';

    $sort = " ORDER BY $sortname $sortorder";

    if (!$page)
        $page = 1;
    if (!$perPage)
        $perPage = 50;

    $start = (($page - 1) * $perPage);

    $limit = "$start, $perPage";
    $query = $_REQUEST['query'];
    $where = "(BUILD.status != 'D')";
    if(isset($_SESSION['order']) && $_SESSION['order'] == 'updated') {
        $sort = " ORDER BY BUILD.last_update_date desc";
        unset($_SESSION['order']);
    }
    $table = 'table_buildings as BUILD left join table_unit_type as UNIT on UNIT.unit_type_id = BUILD.unit_type_id ';
    $clms = 'BUILD.*,UNIT.unit_name';
    
    if (isset($search) && !empty($search)) {
        $where .= " and (BUILD.building_name like '%" . $search . "%' or UNIT.unit_name like '%" . $search . "%' )";
    }
    $auRec = $_objAdmin->_getSelectList($table, $clms, $limit, $where . ' ' . $sort, '', '');
    $auRecCount = $_objAdmin->_getSelectList($table, 'count(*) as total', '', $where);
    $total = $auRecCount[0]->total;

    $dataReturn = array();
    $dataReturn['meta'] = array(
        'page' => $page,
        'pages' => ceil($total / $perPage),
        'total' => $total,
        'perpage' => $perPage,
        'sort' => $sortorder,
        'field' => $sortname
    );
    $dataReturn['data'] = array();
    foreach ($auRec as $auRecTemp) {
        $temp['building_id'] = $auRecTemp->building_id;
        $temp['building_name'] = $auRecTemp->building_name;
        $temp['unit_name'] = $auRecTemp->unit_name;
        $temp['lat'] = $auRecTemp->lat;
        $temp['lng'] = $auRecTemp->lng;
        if ($auRecTemp->status == 'A') {
            $temp['Status'] = 1;
        } else if ($auRecTemp->status == 'I') {
            $temp['Status'] = 2;
        } else {
            $temp['Status'] = 2;
        }
        $dataReturn['data'][] = $temp;
    }
    echo json_encode($dataReturn, true);
    die;
}
if ($_REQUEST['pageType'] == 'TravellingTime') {
    $_objAdmin = new Admin();
    $page = $_REQUEST['datatable']['pagination']['page'];
    $perPage = $_REQUEST['datatable']['pagination']['perpage'];
    $sortname = $_REQUEST['datatable']['sort']['field'];
    $sortorder = $_REQUEST['datatable']['sort']['sort'];
    $search = $_REQUEST['datatable']['query']['generalSearch'];

    if (!$sortname)
        $sortname = 'building_name';
    if (!$sortorder)
        $sortorder = 'asc';

    $sort = " ORDER BY $sortname $sortorder";

    if (!$page)
        $page = 1;
    if (!$perPage)
        $perPage = 50;

    $start = (($page - 1) * $perPage);

    $limit = "$start, $perPage";
    $query = $_REQUEST['query'];
    $where = "(BUILD.status != 'D')";
    if(isset($_SESSION['order']) && $_SESSION['order'] == 'updated') {
        $sort = " ORDER BY BUILD.last_update_date desc";
        unset($_SESSION['order']);
    }
    $table = 'table_buildings as BUILD left join table_area as AREA on AREA.area_id = BUILD.area_id ';
    $clms = 'BUILD.*,AREA.area_name';
    
    if (isset($search) && !empty($search)) {
        $where .= " and (BUILD.building_name like '%" . $search . "%' or AREA.area_name like '%" . $search . "%' )";
    }
    $auRec = $_objAdmin->_getSelectList($table, $clms, $limit, $where . ' ' . $sort, '', '');
    $auRecCount = $_objAdmin->_getSelectList($table, 'count(*) as total', '', $where);
    $total = $auRecCount[0]->total;

    $dataReturn = array();
    $dataReturn['meta'] = array(
        'page' => $page,
        'pages' => ceil($total / $perPage),
        'total' => $total,
        'perpage' => $perPage,
        'sort' => $sortorder,
        'field' => $sortname
    );
    $dataReturn['data'] = array();
    foreach ($auRec as $auRecTemp) {
        $temp['building_id'] = $auRecTemp->building_id;
        $temp['building_name'] = $auRecTemp->building_name;
        $temp['area_id'] = $auRecTemp->area_name;
        $temp['travel_from'] = 'Damac Maison Upper Crest';  
		$temp['travelling_time_for_cleaning'] = $auRecTemp->travelling_time_for_cleaning;
        if ($auRecTemp->status == 'A') {
            $temp['Status'] = 1;
        } else if ($auRecTemp->status == 'I') {
            $temp['Status'] = 2;
        } else {
            $temp['Status'] = 2;
        }
        $dataReturn['data'][] = $temp;
    }
    echo json_encode($dataReturn, true);
    die;
}

if ($_REQUEST['pageType'] == 'queryType') {
    $_objAdmin = new Admin();
    $page = $_REQUEST['datatable']['pagination']['page'];
    $perPage = $_REQUEST['datatable']['pagination']['perpage'];
    $sortname = $_REQUEST['datatable']['sort']['field'];
    $sortorder = $_REQUEST['datatable']['sort']['sort'];
    $search = $_REQUEST['datatable']['query']['generalSearch'];

    if (!$sortname)
        $sortname = 'query_type_text';
    if (!$sortorder)
        $sortorder = 'asc';

    $sort = " ORDER BY $sortname $sortorder";

    if (!$page)
        $page = 1;
    if (!$perPage)
        $perPage = 50;

    $start = (($page - 1) * $perPage);

    $limit = "$start, $perPage";
    $query = $_REQUEST['query'];
    $where = "(status != 'D')";
    $table = 'table_contact_type_master';
    $clms = '*';
    if(isset($_SESSION['order']) && $_SESSION['order'] == 'updated') {
        $sort = " ORDER BY last_update_date desc";
        unset($_SESSION['order']);
    }
    if (isset($search) && !empty($search)) {
        $where .= " and (query_type_text like '%" . $search . "%')";
    }
    $auRec = $_objAdmin->_getSelectList($table, $clms, $limit, $where . ' ' . $sort, '', '');
    $auRecCount = $_objAdmin->_getSelectList($table, 'count(*) as total', '', $where);
    $total = $auRecCount[0]->total;

    $dataReturn = array();
    $dataReturn['meta'] = array(
        'page' => $page,
        'pages' => ceil($total / $perPage),
        'total' => $total,
        'perpage' => $perPage,
        'sort' => $sortorder,
        'field' => $sortname
    );
    $dataReturn['data'] = array();
    foreach ($auRec as $auRecTemp) {
        $temp['query_id'] = $auRecTemp->query_id;
        $temp['query_type_text'] = $auRecTemp->query_type_text;
        $temp['created_date'] = date('d M, Y',strtotime($auRecTemp->created_date));
        if ($auRecTemp->status == 'A') {
            $temp['Status'] = 1;
        } else if ($auRecTemp->status == 'I') {
            $temp['Status'] = 2;
        } else {
            $temp['Status'] = 2;
        }
        $dataReturn['data'][] = $temp;
    }
    echo json_encode($dataReturn, true);
    die;
}

if ($_REQUEST['pageType'] == 'promoCode') {
    $_objAdmin = new Admin();
    $page = $_REQUEST['datatable']['pagination']['page'];
    $perPage = $_REQUEST['datatable']['pagination']['perpage'];
    $sortname = $_REQUEST['datatable']['sort']['field'];
    $sortorder = $_REQUEST['datatable']['sort']['sort'];
    $search = $_REQUEST['datatable']['query']['generalSearch'];

    if (!$sortname)
        $sortname = 'query_type_text';
    if (!$sortorder)
        $sortorder = 'asc';

    $sort = " ORDER BY $sortname $sortorder";

    if (!$page)
        $page = 1;
    if (!$perPage)
        $perPage = 50;

    $start = (($page - 1) * $perPage);

    $limit = "$start, $perPage";
    $query = $_REQUEST['query'];
    $table = 'table_promo_code as p left join table_buildings as b on b.building_id = p.building_id left join '
            . 'table_cleaning_type as c on c.cleaning_type_id = p.cleaning_type_id';
    $clms = 'p.*,b.building_name,c.cleaning_type';
    if(isset($_SESSION['order']) && $_SESSION['order'] == 'updated') {
        $sort = " ORDER BY p.last_updated_date desc";
        unset($_SESSION['order']);
    }
    if (isset($search) && !empty($search)) {
        $where .= " and (b.building_name like '%" . $search . "%' or c.cleaning_type like '%" . $search . "%' or p.promo_code like '%" . $search . "%')";
    }
    $auRec = $_objAdmin->_getSelectList($table, $clms, $limit, $where . ' ' . $sort, '', '');
    $auRecCount = $_objAdmin->_getSelectList($table, 'count(*) as total', '', $where);
    $total = $auRecCount[0]->total;

    $dataReturn = array();
    $dataReturn['meta'] = array(
        'page' => $page,
        'pages' => ceil($total / $perPage),
        'total' => $total,
        'perpage' => $perPage,
        'sort' => $sortorder,
        'field' => $sortname
    );
    $dataReturn['data'] = array();
    foreach ($auRec as $auRecTemp) {
        $temp['promo_code_id'] = $auRecTemp->promo_code_id;
        $temp['promo_code'] = $auRecTemp->promo_code;
        $temp['building_name'] = $auRecTemp->building_name;
        $temp['cleaning_type'] = $auRecTemp->cleaning_type;
        $temp['discription'] = $auRecTemp->discription;
        $temp['max_limit_use'] = $auRecTemp->max_limit_use;
        $temp['promo_code_discount'] = ($auRecTemp->promo_code_discount) * 100;
        $temp['start_date'] = date('d M, Y',strtotime($auRecTemp->start_date));
        $temp['end_date'] = date('d M, Y',strtotime($auRecTemp->end_date));
        if ($auRecTemp->status == 'A') {
            $temp['Status'] = 1;
        } else if ($auRecTemp->status == 'I') {
            $temp['Status'] = 2;
        } else {
            $temp['Status'] = 2;
        }
        $dataReturn['data'][] = $temp;
    }
    echo json_encode($dataReturn, true);
    die;
}
if ($_REQUEST['pageType'] == 'cleanerFeedback'){
    $_objAdmin = new Admin();
    $page = $_REQUEST['datatable']['pagination']['page'];
    $perPage = $_REQUEST['datatable']['pagination']['perpage'];
    //$sortname = $_REQUEST['datatable']['sort']['field'];
    $sortorder = $_REQUEST['datatable']['sort']['sort'];
    $search = $_REQUEST['datatable']['query']['generalSearch'];

    if (!$sortname)
        $sortname = 'feedback_id';
    if (!$sortorder)
        $sortorder = 'desc';

    $sort = " ORDER BY $sortname $sortorder";

    if (!$page)
        $page = 1;
    if (!$perPage)
        $perPage = 50;

    $start = (($page - 1) * $perPage);

    $limit = "$start, $perPage";
    $query = $_REQUEST['query'];
    if($_SESSION['FromCleanerFeedback']!=''){
        $fromdate=" tsf.created_date >= '".date('Y-m-d', strtotime($_SESSION['FromCleanerFeedback']))."'";
        $where="$fromdate ";
    }
    if($_SESSION['ToCleanerFeedback']!=''){
        $todate=" and  tsf.created_date <= '".date('Y-m-d', strtotime($_SESSION['ToCleanerFeedback']))."'";
        $where.="$todate AND ";
    }

   
    $where .= "tsf.feedback_type='cleaner' AND tsf.cleaner_id!=0";
    $table = 'table_service_feedback as tsf left join table_booking_register as tbr on tbr.booking_id=tsf.booking_id left join table_cleaner as tc on tc.cleaner_id=tsf.cleaner_id';
    $clms = 'tc.cleaner_id,tc.cleaner_code,tc.cleaner_name,tc.cleaner_phone_no,tc.cleaner_email,tsf.*,tbr.booking_id';

    if(isset($search) && !empty($search)){
        $where .= " and (tsf.comments like '%" . $search . "%' or tbr.booking_id like '%" . $search . "%'". "or tc.cleaner_code like '%" . $search . "%' or tc.cleaner_name like '%" . $search . "%'"
                . "or tc.cleaner_phone_no like '%" . $search . "%' or tc.cleaner_email like '%" . $search . "%')";
    }
    if($_SESSION['userLoginType'] == 5 && isset($_SESSION['supervisor_id']) && !empty($_SESSION['supervisor_id'])){
        $where .= " and tc.supervisor_id = '".$_SESSION['supervisor_id']."'";
    }
    $auRec = $_objAdmin->_getSelectList($table, $clms, $limit, $where . ' ' . $sort, '', '');
    $auRecCount = $_objAdmin->_getSelectList($table,'count(*) as total', '', $where);
    $total = $auRecCount[0]->total;

    $dataReturn = array();
    $dataReturn['meta'] = array(
        'page' => $page,
        'pages' => ceil($total / $perPage),
        'total' => $total,
        'perpage' => $perPage,
        'sort' => $sortorder,
        'field' => $sortname
    );
    $dataReturn['data'] = array();
    foreach ($auRec as $auRecTemp) {
        $temp['feedback_id'] = $auRecTemp->feedback_id;
        $temp['comments'] = $auRecTemp->comments;
        $temp['booking_id'] = $auRecTemp->booking_id;
        $temp['cleaner_code'] = $auRecTemp->cleaner_code;
        $temp['cleaner_name'] = $auRecTemp->cleaner_name;
        $temp['cleaner_phone_no'] = $auRecTemp->cleaner_phone_no;
        $temp['cleaner_email'] = preg_replace('/[^A-Za-z0-9\. @:-]/', '', $auRecTemp->cleaner_email);
        $temp['created_date'] = $auRecTemp->created_date;
        $dataReturn['data'][] = $temp;
    }
    echo json_encode($dataReturn, true);
    die;
}
/*****Customer Feedback Report****/
if($_REQUEST['pageType'] == 'customerFeedback'){
    $_objAdmin = new Admin();
    $page = $_REQUEST['datatable']['pagination']['page'];
    $perPage = $_REQUEST['datatable']['pagination']['perpage'];
    $sortname = $_REQUEST['datatable']['sort']['field'];
    $sortorder = $_REQUEST['datatable']['sort']['sort'];
    $search = $_REQUEST['datatable']['query']['generalSearch'];

    if(!$sortname)
        $sortname = 'feedback_id';
    if(!$sortorder)
        $sortorder = 'desc';

    $sort = " ORDER BY $sortname $sortorder";
    if(!$page)
        $page = 1;
    if(!$perPage)
        $perPage = 50;

    $start = (($page - 1) * $perPage);

    $limit = "$start, $perPage";
    $query = $_REQUEST['query'];
      if($_SESSION['FromCustomerFeedback']!=''){
        $fromdate=" tsf.created_date >= '".date('Y-m-d', strtotime($_SESSION['FromCustomerFeedback']))."'";
        $where ="$fromdate ";
    }
    if($_SESSION['ToCustomerFeedback']!=''){
        $todate=" and  tsf.created_date <= '".date('Y-m-d', strtotime($_SESSION['ToCustomerFeedback']))."'";
        $where .="$todate AND ";
    }
    $where .= "tsf.feedback_type='customer' AND tsf.customer_id!=0";
    $table = 'table_service_feedback as tsf left join table_booking_register as tbr on tbr.booking_id=tsf.booking_id left join table_customer as tc on tc.customer_id=tsf.customer_id';
    $clms = 'tc.customer_id,tc.customer_name,tc.customer_number,tc.customer_email,tsf.*,tbr.booking_id';

    if(isset($search) && !empty($search)){
        $where .= " and (tsf.comments like '%" . $search . "%' or tbr.booking_id like '%" . $search . "%'". "or tc.customer_number like '%" . $search . "%' or tc.customer_email like '%" . $search . "%'or tc.customer_name like '%" . $search . "%')";
    }
    $auRec = $_objAdmin->_getSelectList($table, $clms, $limit, $where . ' ' . $sort, '', '');
    $auRecCount = $_objAdmin->_getSelectList($table, 'count(*) as total', '', $where);
    $total = $auRecCount[0]->total;

    $dataReturn = array();
    $dataReturn['meta'] = array(
        'page' => $page,
        'pages' => ceil($total / $perPage),
        'total' => $total,
        'perpage' => $perPage,
        'sort' => $sortorder,
        'field' => $sortname
    );
    $dataReturn['data'] = array();
    foreach($auRec as $auRecTemp){
        $temp['feedback_id'] = $auRecTemp->feedback_id;
        $temp['comments'] = mb_strimwidth(str_replace("'", '',$auRecTemp->comments), 0, 10, "...");
        $temp['booking_id'] = $auRecTemp->booking_id;
        $temp['customer_name'] = $auRecTemp->customer_name;
        $temp['customer_number'] = $auRecTemp->customer_number;
        $temp['customer_email'] = $auRecTemp->customer_email;
        $temp['star_rating'] = $auRecTemp->star_rating;
        $temp['created_date'] = $auRecTemp->created_date;
        $dataReturn['data'][] = $temp;
    }
    echo json_encode($dataReturn, true);
    die;
}
/*****END Customer Feedback Report****/
/*****Customer Enquiry Report****/
if($_REQUEST['pageType'] == 'enquiryList'){
    $_objAdmin = new Admin();
    $page = $_REQUEST['datatable']['pagination']['page'];
    $perPage = $_REQUEST['datatable']['pagination']['perpage'];
    $sortname = $_REQUEST['datatable']['sort']['field'];
    $sortorder = $_REQUEST['datatable']['sort']['sort'];
    $search = $_REQUEST['datatable']['query']['generalSearch'];

    if(!$sortname)
        $sortname = 'contact_query_id';
    if(!$sortorder)
        $sortorder = 'desc';

    $sort = " ORDER BY $sortname $sortorder";
    if(!$page)
        $page = 1;
    if(!$perPage)
        $perPage = 50;

    $start = (($page - 1) * $perPage);

    $limit = "$start, $perPage";
    $query = $_REQUEST['query'];
   $where='';
    if($_SESSION['FromEnquiryList']!=''){
        $fromdate=" QUERY.created_date >= '".date('Y-m-d', strtotime($_SESSION['FromEnquiryList']))."'";
        $where .="$fromdate ";
    }
    if($_SESSION['ToEnquiryList']!=''){
        $todate=" and QUERY.created_date <= '".date('Y-m-d', strtotime($_SESSION['ToEnquiryList']))."'";
        $where .="$todate AND ";
    }
    $where .= "QUERY.status !='D'";
    $table = 'table_contact_query as QUERY left join table_contact_type_master as MASTER on MASTER.query_id = QUERY.query_id left join table_customer as CUST on CUST.customer_id = QUERY.customer_id';
    $clms = 'QUERY.*,MASTER.query_type_text,CUST.customer_name,CUST.customer_email,CUST.customer_email,CUST.customer_number';

    if(isset($search) && !empty($search)){
        $where .= " and (MASTER.query_type_text like '%" . $search . "%' or CUST.customer_name like '%" . $search . "%'". "or CUST.customer_number like '%" . $search . "%' or CUST.customer_email like '%" . $search . "%')";
    }
    $auRec = $_objAdmin->_getSelectList($table, $clms, $limit, $where . ' ' . $sort, '', '');
    $auRecCount = $_objAdmin->_getSelectList($table, 'count(*) as total', '', $where);
    $total = $auRecCount[0]->total;

    $dataReturn = array();
    $dataReturn['meta'] = array(
        'page' => $page,
        'pages' => ceil($total / $perPage),
        'total' => $total,
        'perpage' => $perPage,
        'sort' => $sortorder,
        'field' => $sortname
    );
    $dataReturn['data'] = array();
    foreach($auRec as $auRecTemp){

        $temp['contact_query_id'] = $auRecTemp->contact_query_id;
        $temp['message'] = mb_strimwidth(str_replace("'", '',$auRecTemp->message), 0, 50, "...");
        $temp['query_type_text'] = $auRecTemp->query_type_text;
        $temp['customer_name'] = $auRecTemp->customer_name;
        $temp['customer_number'] = $auRecTemp->customer_number;
        $temp['customer_email'] = $auRecTemp->customer_email;
        $temp['created_date'] = $auRecTemp->created_date;
        $dataReturn['data'][] = $temp;
    }
    echo json_encode($dataReturn, true);
    die;
}
/*****END Customer Enquiry Report****/
/*****Customer Payment Report****/
if($_REQUEST['pageType'] == 'paymentList'){
    $_objAdmin = new Admin();
    $page = $_REQUEST['datatable']['pagination']['page'];
    $perPage = $_REQUEST['datatable']['pagination']['perpage'];
    $sortname = $_REQUEST['datatable']['sort']['field'];
    $sortorder = $_REQUEST['datatable']['sort']['sort'];
    $search = $_REQUEST['datatable']['query']['generalSearch'];

    if(!$sortname)
        $sortname = 'payment_id';
    if(!$sortorder)
        $sortorder = 'desc';

    $sort = " ORDER BY $sortname $sortorder";
    if(!$page)
        $page = 1;
    if(!$perPage)
        $perPage = 50;

    $start = (($page - 1) * $perPage);

    $limit = "$start, $perPage";
    $query = $_REQUEST['query'];
    $where='';
    if($_SESSION['FromPaymentHistory']!=''){
        $fromdate=" tph.created_date >= '".date('Y-m-d', strtotime($_SESSION['FromPaymentHistory']))."'";
        $where.="$fromdate ";
    }
    if($_SESSION['ToPaymentHistory']!=''){
        $todate=" and  tph.created_date <= '".date('Y-m-d', strtotime($_SESSION['ToPaymentHistory']))."'";
        $where.="$todate AND ";
    }
    $where .= "tph.status='A'";
    $table = 'table_payment_history as tph left join table_customer as CUST on CUST.customer_id = tph.customer_id left join table_booking_register as REGISTER on REGISTER.payment_id = tph.payment_id';
    $clms = 'tph.payment_id,tph.customer_id,tph.payment_mode,tph.created_date,tph.payment_mode,tph. transaction_status,tph.amount,CUST.customer_name,CUST.customer_number,CUST.customer_email,REGISTER.booking_id';

    if(isset($search) && !empty($search)){
        $where .= " and (tph.payment_mode like '%" . $search . "%' or REGISTER.booking_id like '%" . $search . "%'". "or CUST.customer_number like '%" . $search . "%' or CUST.customer_number like '%" . $search . "%'  or CUST.customer_email like '%" . $search . "%')";
    }
    $auRec = $_objAdmin->_getSelectList($table,$clms,$limit,$where . ' ' .$sort, '', '');
    $auRecCount = $_objAdmin->_getSelectList($table, 'count(*) as total', '', $where);
    $total = $auRecCount[0]->total;

    $dataReturn = array();
    $dataReturn['meta'] = array(
        'page' => $page,
        'pages' => ceil($total / $perPage),
        'total' => $total,
        'perpage' => $perPage,
        'sort' => $sortorder,
        'field' => $sortname
    );
    $dataReturn['data'] = array();
    foreach($auRec as $auRecTemp){

        $temp['payment_id'] = $auRecTemp->payment_id;
        //$temp['payment_mode'] = mb_strimwidth(str_replace("'", '',$auRecTemp->payment_mode), 0, 50, "...");
        $temp['booking_id'] = $auRecTemp->booking_id;
        $temp['customer_name'] = $auRecTemp->customer_name;
        $temp['customer_number'] = $auRecTemp->customer_number;
        $temp['customer_email'] = $auRecTemp->customer_email;
        $temp['amount'] = $auRecTemp->amount;
        $temp['created_date'] = $auRecTemp->created_date;
        if(strtolower($auRecTemp->payment_mode)=='cash'){
            $status="cash";
        }else{
            $status="paid";
        }
        $temp['payment_mode'] = $status;
        $dataReturn['data'][] = $temp;
    }
    echo json_encode($dataReturn, true);
    die;
}
/*****END Customer Payment Report****/
/*****User privilages****/
if ($_REQUEST['pageType'] == 'userNavigationList') {
    $_objAdmin = new Admin();
    $page = $_REQUEST['datatable']['pagination']['page'];
    $perPage = $_REQUEST['datatable']['pagination']['perpage'];
    $sortname = $_REQUEST['datatable']['sort']['field'];
    $sortorder = $_REQUEST['datatable']['sort']['sort'];
    $search = $_REQUEST['datatable']['query']['generalSearch'];
    if(!$sortname)
        $sortname='nav_privilege_id';
    if(!$sortorder)
        $sortorder='asc';

    $sort = " ORDER BY $sortname $sortorder";

    if (!$page)
        $page = 1;
    if (!$perPage)
        $perPage = 50;

    $start = (($page - 1) * $perPage);

    $limit = "$start, $perPage";
    $query = $_REQUEST['query'];
    $where = " tnp.user_type!='1' and tnp.hierarchy_id=tup.hierarchy_id ";
    $table = 'table_navigation_privilege as tnp left join table_account as ta on tnp.account_id=ta.account_id left join table_user_type as tup on tup.parentType=tnp.user_type left join table_web_users as twu on twu.web_user_id=tnp.user_id';

    $clms = 'tnp.*,ta.company_name,tup.type,twu.username';

    if(isset($search) && !empty($search)){

        $where .= " and (ta.company_name like '%" . $search . "%' or tup.type like '%" . $search . "%'". "or twu.username like '%" . $search . "%')";
    }
    $auRec = $_objAdmin->_getSelectList($table, $clms, $limit, $where . ' ' . $sort, '', '');
    $auRecCount = $_objAdmin->_getSelectList($table, 'count(*) as total', '', $where);
    $total = $auRecCount[0]->total;

    $dataReturn = array();
    $dataReturn['meta'] = array(
        'page' => $page,
        'pages' => ceil($total / $perPage),
        'total' => $total,
        'perpage' => $perPage,
        'sort' => $sortorder,
        'field' => $sortname
    );
    $dataReturn['data'] = array();
    foreach($auRec as $auRecTemp){

        $pageID = $auRecTemp->page_privileges;
        if($pageID!=''){
            $checkPageID="id IN ($pageID) AND";
        }
        $auRecName=$_objAdmin->_getSelectList2('table_cms','page_alias_name',''," $checkPageID status = 'A'");
        $my_array=$_objAdmin->returnObjectArrayToIndexArray($auRecName,'page_alias_name');
        $comma_string=implode(",",$my_array);

        $temp['nav_privilege_id'] = $auRecTemp->nav_privilege_id;
        $temp['company_name'] = ucwords($auRecTemp->company_name);
        $temp['type'] = ucwords($auRecTemp->type);
        $temp['username'] = ucwords($auRecTemp->username);
        $temp['Previlages'] = ucwords($comma_string);
        $dataReturn['data'][] = $temp;
    }
    echo json_encode($dataReturn, true);
    die;
}
/*****End User privilages****/
/******SlotList*****/
if($_REQUEST['pageType'] == 'slotList'){
    $_objAdmin = new Admin();
    $page = $_REQUEST['datatable']['pagination']['page'];
    $perPage = $_REQUEST['datatable']['pagination']['perpage'];
    $sortname = $_REQUEST['datatable']['sort']['field'];
    $sortorder = $_REQUEST['datatable']['sort']['sort'];
    $search = $_REQUEST['datatable']['query']['generalSearch'];

    if (!$sortname)
        $sortname = 'tcs.slot_start_time';
    if (!$sortorder)
        $sortorder = 'asc';

    $sort = " ORDER BY $sortname $sortorder";

    if (!$page)
        $page = 1;
    if (!$perPage)
        $perPage = 50;

    $start = (($page - 1) * $perPage);

    $limit = "$start, $perPage";
    $query = $_REQUEST['query'];
    $where = " tcs.status!='D'";
    $table = 'table_calender_slots as tcs';
    $clms = 'tcs.*';
    if(isset($_SESSION['order']) && $_SESSION['order'] == 'updated'){
        $sort = " ORDER BY tcs.last_update_date desc";
        unset($_SESSION['order']);
    }
    // if(isset($search) && !empty($search)){
    //     $where .= " and (twd.week_day like '%" . $search . "%')";
    // }
    $auRec = $_objAdmin->_getSelectList($table, $clms, $limit, $where . ' ' . $sort, '', '');
    $auRecCount = $_objAdmin->_getSelectList($table, 'count(*) as total', '', $where);
    $total = $auRecCount[0]->total;

    $dataReturn = array();
    $dataReturn['meta'] = array(
        'page' => $page,
        'pages' => ceil($total/$perPage),
        'total' => $total,
        'perpage' => $perPage,
        'sort' => $sortorder,
        'field' => $sortname
    );
    $dataReturn['data'] = array();
    foreach ($auRec as $auRecTemp){
        $temp['slot_id'] = $auRecTemp->slot_id;
        //$temp['week_day'] = $auRecTemp->week_day;
        $temp['slot_start_time'] = date('h:i A',strtotime($auRecTemp->slot_start_time));
        $temp['slot_end_time'] = date('h:i A',strtotime($auRecTemp->slot_end_time));
        if ($auRecTemp->status == 'A') {
            $temp['Status'] = 1;
        } else if ($auRecTemp->status == 'I') {
            $temp['Status'] = 2;
        } else {
            $temp['Status'] = 2;
        }
        $dataReturn['data'][] = $temp;
    }
    echo json_encode($dataReturn, true);
    die;
}

if ($_REQUEST['pageType'] == 'hierarchyList') {
    $_objAdmin = new Admin();
    $page = $_REQUEST['datatable']['pagination']['page'];
    $perPage = $_REQUEST['datatable']['pagination']['perpage'];
    $sortname = $_REQUEST['datatable']['sort']['field'];
    $sortorder = $_REQUEST['datatable']['sort']['sort'];
    $search = $_REQUEST['datatable']['query']['generalSearch'];

    if (!$sortname)
        $sortname = 'description';
    if (!$sortorder)
        $sortorder = 'asc';

    //$sort = " ORDER BY $sortname $sortorder";

    if (!$page)
        $page = 1;
    if (!$perPage)
        $perPage = 50;

    $start = (($page - 1) * $perPage);
    
    $limit = "$start, $perPage";
    $query = $_REQUEST['query'];
    $where = "(a.status != 'D')";
    $table = 'table_user_hierarchy AS a';
    $clms = 'a.hierarchy_id AS id, a.created AS created, a.description AS hierarchy, a.status AS status';

    if (isset($search) && !empty($search)) {
        $where .= " and (a.hierarchy like '%" . $search . "%')";
    }
    $auRec = $_objAdmin->_getSelectList($table, $clms, $limit, $where . ' ' . $sort, '', '');
    $auRecCount = $_objAdmin->_getSelectList('table_user_hierarchy as a', 'count(*) as total', '', $where);
    $total = $auRecCount[0]->total;

    $dataReturn = array();
    $dataReturn['meta'] = array(
        'page' => $page,
        'pages' => ceil($total / $perPage),
        'total' => $total,
        'perpage' => $perPage,
        'sort' => $sortorder,
        'field' => $sortname
    );
    $dataReturn['data'] = array();
    foreach ($auRec as $auRecTemp) {
        $temp['id'] = $auRecTemp->id;
        $temp['hierarchy'] = $auRecTemp->hierarchy;
        $temp['created'] = $auRecTemp->created;
        if ($auRecTemp->status == 'A') {
            $temp['Status'] = 1;
        } else if ($auRecTemp->status == 'I') {
            $temp['Status'] = 2;
        } else {
            $temp['Status'] = 2;
        }
        $dataReturn['data'][] = $temp;
    }
    echo json_encode($dataReturn, true);
    die;
}

if ($_REQUEST['pageType'] == 'userList') {
    $_objAdmin = new Admin();
    $page = $_REQUEST['datatable']['pagination']['page'];
    $perPage = $_REQUEST['datatable']['pagination']['perpage'];
    $sortname = $_REQUEST['datatable']['sort']['field'];
    $sortorder = $_REQUEST['datatable']['sort']['sort'];
    $search = $_REQUEST['datatable']['query']['generalSearch'];

    if (!$sortname)
        $sortname = 'user_name';
    if (!$sortorder)
        $sortorder = 'asc';

    //$sort = " ORDER BY $sortname $sortorder";

    if (!$page)
        $page = 1;
    if (!$perPage)
        $perPage = 50;

    $start = (($page - 1) * $perPage);
    if(isset($_SESSION['order']) && $_SESSION['order'] == 'updated') {
        $sort = " ORDER BY table_supervisor.last_update_date desc";
        unset($_SESSION['order']);
    }
    $limit = "$start, $perPage";
    $query = $_REQUEST['query'];
    $where = "(s.status != 'D')";
    $table = 'table_user AS s '
		.'LEFT JOIN table_web_users AS w ON w.user_id = s.user_id '
		.'LEFT JOIN table_user_hierarchy_relationship AS SH ON SH.user_id = s.user_id '
		.'LEFT JOIN table_user_hierarchy AS H ON H.hierarchy_id = SH.hierarchy_id '
		.'LEFT JOIN table_user_hierarchy AS H2 ON H2.hierarchy_id = SH.rpt_hierarchy_id '
                .'LEFT JOIN table_user AS S2 ON S2.user_id = SH.rpt_user_id';
    
    $clms = 's.*,w.username,w.email_id,w.web_user_id,w.status as loginStatus, H.description AS des1, H2.description AS des2, S2.user_name AS rptPerson';

    if (isset($search) && !empty($search)) {
        $where .= " and (table_supervisor.supervisor_name like '%" . $search . "%' or table_supervisor.supervisor_address like '%" . $search . "%'"
                . "or table_supervisor.supervisor_email like '%" . $search . "%' or table_supervisor.supervisor_phone_no like '%" . $search . "%')";
    }
    $auRec = $_objAdmin->_getSelectList($table, $clms, $limit, $where . ' ' . $sort, '', '');
    $auRecCount= $_objAdmin->_getSelectList('table_user as s left join table_account as a on a.account_id=s.account_id left join table_web_users as w on w.user_id=s.user_id','count(*) as total','',$where);
    $total = $auRecCount[0]->total;
    
    $dataReturn = array();
    $dataReturn['meta'] = array(
        'page' => $page,
        'pages' => ceil($total / $perPage),
        'total' => $total,
        'perpage' => $perPage,
        'sort' => $sortorder,
        'field' => $sortname
    );
    $dataReturn['data'] = array();
    foreach ($auRec as $auRecTemp) {
        $temp['user_id'] = $auRecTemp->user_id;
        $temp['user_name'] = $auRecTemp->user_name;
        $temp['des1'] = $auRecTemp->des1;
        $temp['des2'] = $auRecTemp->des2;
        $temp['user_email'] = $auRecTemp->user_email;
        $temp['status'] = $auRecTemp->status;
        if ($auRecTemp->status == 'A') {
            $temp['Status'] = 1;
        } else if ($auRecTemp->status == 'I') {
            $temp['Status'] = 2;
        } else {
            $temp['Status'] = 2;
        }
        $dataReturn['data'][] = $temp;
    }
    echo json_encode($dataReturn, true);
    die;
}
/********End Slot List*********/

if ($_REQUEST['pageType'] == 'buildingsNew') {
    $_objAdmin = new Admin();
    $page = $_REQUEST['datatable']['pagination']['page'];
    $perPage = $_REQUEST['datatable']['pagination']['perpage'];
    $sortname = $_REQUEST['datatable']['sort']['field'];
    $sortorder = $_REQUEST['datatable']['sort']['sort'];
    $search = $_REQUEST['datatable']['query']['generalSearch'];

    if (!$sortname)
        $sortname = 'building_name';
    if (!$sortorder)
        $sortorder = 'asc';

    $sort = " ORDER BY $sortname $sortorder";

    if (!$page)
        $page = 1;
    if (!$perPage)
        $perPage = 50;

    $start = (($page - 1) * $perPage);

    $limit = "$start, $perPage";
    $query = $_REQUEST['query'];
    $where = "(BUILD.status != 'D' and BUILD.is_approved='0')";
    if(isset($_SESSION['order']) && $_SESSION['order'] == 'updated') {
        $sort = " ORDER BY BUILD.last_update_date desc";
        unset($_SESSION['order']);
    }
    $table = 'table_buildings as BUILD left join table_unit_type as UNIT on UNIT.unit_type_id = BUILD.unit_type_id ';
    $clms = 'BUILD.*,UNIT.unit_name';
    
    if (isset($search) && !empty($search)) {
        $where .= " and (BUILD.building_name like '%" . $search . "%' or UNIT.unit_name like '%" . $search . "%' )";
    }
    $auRec = $_objAdmin->_getSelectList($table, $clms, $limit, $where . ' ' . $sort, '', '');
    $auRecCount = $_objAdmin->_getSelectList($table, 'count(*) as total', '', $where);
    $total = $auRecCount[0]->total;

    $dataReturn = array();
    $dataReturn['meta'] = array(
        'page' => $page,
        'pages' => ceil($total / $perPage),
        'total' => $total,
        'perpage' => $perPage,
        'sort' => $sortorder,
        'field' => $sortname
    );
    $dataReturn['data'] = array();
    foreach ($auRec as $auRecTemp) {
        $temp['building_id'] = $auRecTemp->building_id;
        $temp['building_name'] = $auRecTemp->building_name;
        $temp['unit_name'] = $auRecTemp->unit_name;
        $temp['lat'] = $auRecTemp->lat;
        $temp['lng'] = $auRecTemp->lng;
        if ($auRecTemp->status == 'A') {
            $temp['Status'] = 1;
        } else if ($auRecTemp->status == 'I') {
            $temp['Status'] = 2;
        } else {
            $temp['Status'] = 2;
        }
        $dataReturn['data'][] = $temp;
    }
    echo json_encode($dataReturn, true);
    die;
}
/* Azeez */
if ($_REQUEST['pageType'] == 'cleaning_leaves') {
    $_objAdmin = new Admin();
    $page = $_REQUEST['datatable']['pagination']['page'];
    $perPage = $_REQUEST['datatable']['pagination']['perpage'];
    $sortname = $_REQUEST['datatable']['sort']['field'];
    $sortorder = $_REQUEST['datatable']['sort']['sort'];
    $search = $_REQUEST['datatable']['query']['generalSearch'];

    if (!$sortname)
        $sortname = 'leave_id';
    if (!$sortorder)
        $sortorder = 'desc';

    $sort = " ORDER BY $sortname $sortorder";

    if (!$page)
        $page = 1;
    if (!$perPage)
        $perPage = 50;

    $start = (($page - 1) * $perPage);
    /*if(isset($_SESSION['order']) && $_SESSION['order'] == 'updated') {
        $sort = " ORDER BY table_cleaner_leaves.last_update_date desc";
        unset($_SESSION['order']);
    }*/
    $limit = "$start, $perPage";
    $query = $_REQUEST['query'];
    $where = "(table_cleaner_leaves.status != 'D')";
    $table = 'table_cleaner_leaves LEFT JOIN table_leave_type ON table_cleaner_leaves.leave_type=table_leave_type.leave_type_id'.
	          ' LEFT JOIN table_cleaner ON table_cleaner_leaves.cleaner_id=table_cleaner.cleaner_id';     ;
    $clms = 'table_cleaner_leaves.*,table_leave_type.leave_type_name,table_cleaner.cleaner_name';
    
    if (isset($search) && !empty($search)) {
        $where .= " and (table_cleaner_leaves.cleaner_id like '%" . $search . "%')";
    }
    $auRec = $_objAdmin->_getSelectList($table, $clms, $limit, $where . ' ' . $sort, '', '');
    $auRecCount = $_objAdmin->_getSelectList('table_cleaner_leaves', 'count(*) as total', '', $where);
    $total = $auRecCount[0]->total;

    $dataReturn = array();
    $dataReturn['meta'] = array(
        'page' => $page,
        'pages' => ceil($total / $perPage),
        'total' => $total,
        'perpage' => $perPage,
        'sort' => $sortorder,
        'field' => $sortname
    );
    $dataReturn['data'] = array();
    foreach ($auRec as $auRecTemp) {
        $temp['leave_id'] = $auRecTemp->leave_id;
		$temp['cleaner_id'] = $auRecTemp->cleaner_name;
        $temp['leave_type'] = $auRecTemp->leave_type_name;
		$temp['leave_from_date'] = date('d-M-Y',strtotime($auRecTemp->leave_from_date));
		$temp['leave_to_date'] = date('d-M-Y',strtotime($auRecTemp->leave_to_date));
        $dataReturn['data'][] = $temp;
    }
    echo json_encode($dataReturn, true);
    die;
}
if($_REQUEST['pageType'] = 'bookingStatusDetails')
{
	
}

