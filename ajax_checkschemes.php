<?php include("includes/config.inc.php");
	  $_objAdmin = new Admin();
	  $_objSchemeClass = new SchemeClass();


	  	/***********************************************************
	  	* Desc : Check Schemes for this current reuqest with these parameters (Item ID, Retailer ID, Item Quantity)
	  	* Author : AJAY
	  	* Created : 7th June 2015
	  	* Base Logic :  
	  		- Firstly find all the schemes are active
	  		- Thereafter check the scheme type like All, Category, Items
	  		   - Into second step we check party type All, state, city, retailer
	  		- Saved all the active and party based schemes into a dataset
	  	* 
	  	*/


	if(isset($_REQUEST["item_id"]) && isset($_REQUEST["retailer_id"]) && isset($_REQUEST["quantity"]) && is_int((int)$_REQUEST["item_id"]) && is_int((int)$_REQUEST['retailer_id']) && is_int((int)$_REQUEST["quantity"]) ) {

	$item_id = trim($_REQUEST['item_id']);
	$retailer_id = trim($_REQUEST['retailer_id']);
	$quantity = trim($_REQUEST['quantity']);

	$schemeArraySet = array();
	$category_id = 0;

		$itemSchemeResultSet = $_objAdmin->_getSelectList('table_discount AS D  
			LEFT JOIN table_discount_detail AS DD ON DD.discount_id = D.discount_id',
			"D.discount_id, D.discount, D.party_type, D.item_type, D.mode, D.status, D.start_date, D.end_date, DD.*",
			''," D.start_date<='".$date."' AND D.end_date>='".$date."' AND D.status = 'A' AND DD.minimum_quantity<=".$quantity);


			if(is_array($itemSchemeResultSet) && sizeof($itemSchemeResultSet)>0) {

				foreach ($itemSchemeResultSet as $key => $value) {


				 if($value->discount_type = 1 	&& 	$quantity >= $value->minimum_quantity  ) {

					switch ($value->item_type) {

						case '1': // Check for scheme type all
							# code...
								
								$party_type_result = $_objSchemeClass->checkPartyType($retailer_id, $value->discount_id, $value->party_type);

								if($party_type_result) {

									$schemeArraySet[] = array('discount_id'=>$value->discount_id, 'discount_desc'=>$value->discount_desc);

								}

								

							break;


						case '2': // Check for scheme type is category
							# code...
								
								$party_type_result = $_objSchemeClass->checkPartyType($retailer_id, $value->discount_id, $value->party_type);

								if($party_type_result) {


									$getItemDetails = $_objAdmin->_getSelectList('table_item AS I',"I.category_id",''," I.item_id = '".$item_id."'");

									//print_r($getItemDetails);

									if(sizeof($getItemDetails)>0)  {

										$category_id = $getItemDetails[0]->category_id;

										$itemTypeCategoryResult = $_objAdmin->_getSelectList2('table_discount_item AS DI',"DI.category_id",''," DI.discount_id ='".$value->discount_id."' AND DI.category_id = '".$category_id."'");

											if(sizeof($itemTypeCategoryResult) > 0 ) {

									  			$schemeArraySet[] = array('discount_id'=>$value->discount_id, 'discount_desc'=>$value->discount_desc);
									  		}

									}

								}

								

							break;

							case '3': // Check for scheme type is item
							# code...
								
								$party_type_result = $_objSchemeClass->checkPartyType($retailer_id, $value->discount_id, $value->party_type);
								//echo "Hello";
								

								if($party_type_result) {

										$itemTypeItemResult = $_objAdmin->_getSelectList2('table_discount_item AS DI',"DI.item_id",''," DI.discount_id ='".$value->discount_id."' AND DI.item_id = '".$item_id."'");

											//echo $party_type_result;
											//print_r($itemTypeItemResult);
											//exit;

											if(sizeof($itemTypeItemResult) > 0 ) {

									  			$schemeArraySet[] = array('discount_id'=>$value->discount_id, 'discount_desc'=>$value->discount_desc);
									  		}

									}

							break;
						
						default:
							# code...
							break;


					} 	// End of check item types swicth case


				} 	// End of check quantity


			} 	// check any discount schemes for loop

		}	// Check if any schemes exists


		//print_r($schemeArraySet);
		//exit;

		if( sizeof($schemeArraySet)>0 ) {
			foreach ($schemeArraySet as $key=>$value) {?>
				<option value="<?php echo $value['discount_id'];?>"><?php echo $value['discount_desc'];?></option>
			<?php } } else { ?>
				<option value="">No Scheme</option>
		<?php }?>




	<?php }  // final tag closed ?>






