<?php
	include("includes/config.inc.php");
	include("includes/globalarraylist.php");
	$_objAdmin = new Admin();

	if(isset($_GET['stateID'])){
		$stateID = $_GET['stateID'];
		$data = $_objAdmin->_getSelectList2('city LEFT JOIN state ON state.state_id = city.state_id',"city_id, state.state_id, city_name, state_name",'',' city.status = "A" and city.state_id IN ('.$stateID.') order by city_name');
		if(!empty($data)){
			if($_SESSION['districtCust'] =='all'){
				$selected = "selected";
			} else {
				$selected = "";
			}
			$result = "<option value='all'".$selected.">All</option>";
			foreach($data as $value){ 
			if($_SESSION['districtCust'] == $value->city_id){
				$selected = "selected";
			} else {
				$selected = "";
			}
				$result .= "<option value='".$value->city_id."' ".$selected.">".$value->city_name."</option>";
			}
		} else {
			$result = "<option value='all'>All</option>";
		}
		echo $result;
	} else if(isset($_GET['cityID'])){
		$cityID = $_GET['cityID'];
		$data = $_objAdmin->_getSelectList2('table_taluka AS T LEFT JOIN city AS C ON C.city_id = T.city_id',"T.taluka_name, T.taluka_id, C.city_name, C.city_id, C.state_id",'',' T.city_id = "'.$cityID.'" AND T.status = "A" order by T.taluka_name');
		if(!empty($data)){
			if($_SESSION['tehsilCust'] =='all'){
				$selected = "selected";
			} else {
				$selected = "";
			}
			$result = "<option value='all'".$selected.">All</option>";
			foreach($data as $value){ 
			if($_SESSION['tehsilCust'] == $value->taluka_id){
				$selected = "selected";
			} else {
				$selected = "";
			}
				$result .= "<option value='".$value->taluka_id."' ".$selected.">".$value->taluka_name."</option>";
			}
		} else {
			$result = "<option value='all'>All</option>";
		}
		echo $result;
	} else if(isset($_GET['customer'])){

		if($_GET['customer']=="retailer" || $_GET['customer'] =="R"){
			$customer = "4";
		}else if($_GET['customer']=="distributor" || $_GET['customer']=="D"){
			$customer = "3";
		}else if($_GET['customer']=="customer" || $_GET['customer']=="C"){
			$customer = "0";
		}
		
		$data = $_objAdmin->_getSelectList2('table_relationship AS R',"R.relationship_id, R.relationship_desc",'',' R.relationship_type = "'.$customer.'" AND R.status = "A" order by R.relationship_desc');
		if(!empty($data)){
			if($_SESSION['customerClassRec'] =='all' || $_SESSION['customerClass'] =='all'){
				$selected = "selected";
			} else {
				$selected = "";
			}
			$result = "<option value='all'".$selected.">All</option>";
			foreach($data as $value)
			{ 
				if($_SESSION['customerClassRec'] == $value->relationship_id || $_SESSION['customerClass'] == $value->relationship_id){
					$selected = "selected";
				} else {
					$selected = "";
				}
				$result .= "<option value='".$value->relationship_id."' ".$selected.">".$value->relationship_desc."</option>";
			}
		} else {
			$result = "<option value='all'>All</option>";
		}
		echo $result;
	}else if(isset($_GET['distId'])){

		$where='';
		if($_GET['distId'] =='all'){
			$where='';
		}else{
			$querAccount= mysql_query("SELECT division_id FROM table_distributors WHERE distributor_id=".$_GET['distId']."");
				$stockRec = mysql_fetch_array($querAccount);
				
				if($stockRec['division_id']>0){
					$divisionCondition = " AND IDR.division_id = '".$stockRec['division_id']."' ";
				}
				else {
					$divisionCondition = " AND IDR.division_id = -1 ";
				}

				$where .= " $divisionCondition ";	
		}
		
		$data = $items=$_objAdmin->_getSelectList('table_item as i 
			LEFT JOIN table_item_division_relationship AS IDR ON IDR.item_id = i.item_id 
			','i.item_id,i.item_name,i.item_code',''," i.account_id='".$_SESSION['accountId']."' $where ORDER BY item_name");
		if(!empty($data)){
			
			$result = "<option value='all' title='All'>All</option>";
			foreach($data as $value)
			{ 
				if($_SESSION['itemDisId'] == $value->item_id){
					$selected = "selected";
				} else {
					$selected = "";
				}

				$result .= "<option value='".$value->item_id."' title='".$value->item_code."' ".$selected.">".$value->item_name."</option>";
			}
		} else {
			$result = "<option value='all'>All</option>";
		}
		echo $result;
	}
?>