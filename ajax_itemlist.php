<?php include("includes/config.inc.php");
	  $_objAdmin = new Admin();
	  $_objStock = new StockClass();

	if(isset($_REQUEST["type"]) && !empty($_REQUEST["type"]) && $_REQUEST['type'] == 'items') {

	//echo $row_num = $_REQUEST['row_num'];

		if(isset($_REQUEST["last_used_items"]) && !empty($_REQUEST["last_used_items"])) {

			//echo "<pre>";
			//print_r($_REQUEST);
			//exit;
			$lastUsedItems = array();
			$last_used_item_ids = 0;
			$condition = "";

			$lastUsedItems = json_decode($_REQUEST["last_used_items"]);

			if(sizeof(array_filter($lastUsedItems))>0) {

				$last_used_item_ids = implode(",", array_unique(array_filter($lastUsedItems)));

				$condition = " AND I.item_id NOT IN (".$last_used_item_ids.")";

			}

		}

	//file_put_contents("services/Log/auto.log",print_r($_REQUEST["last_used_items"],true)."\r\n", FILE_APPEND);
	//file_put_contents("services/Log/auto.log",print_r($lastUsedItems,true)."\r\n", FILE_APPEND);
	//file_put_contents("services/Log/auto.log",print_r($last_used_item_ids,true)."\r\n", FILE_APPEND);
	//file_put_contents("services/Log/auto.log",print_r($condition,true)."\r\n", FILE_APPEND);
	//file_put_contents("services/Log/auto.log",print_r('SELECT item_id, item_name, item_code FROM table_item WHERE item_code LIKE "'.$item_code."%'",true)."\r\n", FILE_APPEND);


	$item_code = strtoupper(trim($_REQUEST['name_startsWith']));
	//$result  = $_objAdmin->_getSelectList('table_item',"item_id, item_name, item_code ",'',"  item_code LIKE '".$item_code."%'".$condition);
	$result  = $_objAdmin->_getSelectList('table_item AS I LEFT JOIN table_price AS TP ON TP.item_id = I.item_id AND TP.start_date <= "'.$date.'" AND TP.end_date >= "'.$date.'"',"I.item_id, I.item_name, I.item_code, TP.item_mrp, TP.item_dp",'',"  TP.item_mrp IS NOT NULL AND I.item_code LIKE '".$item_code."%'".$condition);

	//print_r($result);
	if(!empty($result)) {?>
	<?php
		$data = array();
		foreach($result as $key=>$row) {
			$name = $row->item_code.'|'.$row->item_name.'|'.$row->item_id.'|'.$row->item_mrp.'|'.$row_num;
			array_push($data, $name);	
		}	
		//echo json_decode($_REQUEST["last_used_items"]);
		echo json_encode($data);
	}
}














	if(isset($_REQUEST["type"]) && !empty($_REQUEST["type"]) && $_REQUEST['type'] == 'bsn') {

	//echo $row_num = $_REQUEST['row_num'];

		if(isset($_REQUEST["last_used_items"]) && !empty($_REQUEST["last_used_items"])) {

			//echo "<pre>";
			//print_r($_REQUEST);
			//exit;
			$lastUsedItems = array();
			$last_used_item_ids = 0;
			$condition = "";

			$lastUsedItems = json_decode($_REQUEST["last_used_items"]);

			if(sizeof(array_filter($lastUsedItems))>0) {

				$last_used_item_ids = implode(",", array_unique(array_filter($lastUsedItems)));

				$condition = " AND BSN.bsn_id NOT IN (".$last_used_item_ids.")";

			}

		}

	//file_put_contents("services/Log/auto.log",print_r($_REQUEST["last_used_items"],true)."\r\n", FILE_APPEND);
	//file_put_contents("services/Log/auto.log",print_r($lastUsedItems,true)."\r\n", FILE_APPEND);
	//file_put_contents("services/Log/auto.log",print_r($last_used_item_ids,true)."\r\n", FILE_APPEND);
	//file_put_contents("services/Log/auto.log",print_r($condition,true)."\r\n", FILE_APPEND);
	//file_put_contents("services/Log/auto.log",print_r('SELECT item_id, item_name, item_code FROM table_item WHERE item_code LIKE "'.$item_code."%'",true)."\r\n", FILE_APPEND);


	$item_code = strtoupper(trim($_REQUEST['name_startsWith']));
	//$result  = $_objAdmin->_getSelectList('table_item',"item_id, item_name, item_code ",'',"  item_code LIKE '".$item_code."%'".$condition);
	//$result  = $_objAdmin->_getSelectList('table_warehouse_stock AS WS INNER JOIN table_item_bsn AS BSN ON BSN.bsn_id = WS.bsn_id LEFT JOIN table_item AS I ON I.item_id = BSN.item_id',"BSN.*, I.item_name, I.item_code",''," BSN.bsn LIKE '".$item_code."%'".$condition." AND WS.status='A'");
	$result  = $_objAdmin->_getSelectList('table_item_bsn AS BSN LEFT JOIN table_item AS I ON I.item_id = BSN.item_id LEFT JOIN table_wh_mrn_detail AS MRND ON MRND.bsn_id = BSN.bsn_id AND MRND.status!= "C" LEFT JOIN table_segment as SEG on SEG.segment_id = I.item_segment LEFT JOIN table_division as DIVI on DIVI.division_id = I.item_division',"BSN.*, I.item_name, I.item_code, DIVI.division_name,SEG.segment_name, MRND.mrn_detail_id",''," BSN.bsn LIKE '".$item_code."'".$condition."");


	// print_r($result);
	if(!empty($result)) {?>
	<?php



		$data = array();
		foreach($result as $key=>$row) {

			// Check Warranty Date
			$warrantyEndDate = $_objStock->getBSNWarrantyEndDate($row->date_of_sale, $row->warranty_period, $row->warranty_prorata);
			if($warrantyEndDate == "-") {
				$warrantyEndDate = "";
			}
			if($row->date_of_sale=='0000-00-00')
			{
				$date_of_sale1 = '';
				$warranty_end_date1 = '';
			}
			else
			{
				$date_of_sale1 = date('Y-m-d',strtotime($row->date_of_sale));
				$warranty_end_date1 = date('d-M-y',strtotime($warrantyEndDate));
			}
			$name = $row->bsn.'|'.$row->bsn_id.'|'.$row->item_name.'|'.date('d-M-y',strtotime($row->manufacture_date)).'|'.date('d-M-y',strtotime($warranty_end_date1)).'|'.$row->item_id.'|'.$row->mrn_detail_id.'|'.$date_of_sale1.'|'.$row->segment_name.'|'.$row->division_name;
			array_push($data, $name);	
		}	
		//echo json_decode($_REQUEST["last_used_items"]);
		echo json_encode($data);
	}
}






	if(isset($_REQUEST["requestType"]) && $_REQUEST['requestType'] == 'checkBsnComplaint' && isset($_REQUEST['runtimeBSN'])) {

		if(!empty(trim($_REQUEST['runtimeBSN']))) {

			$bsnNo = strtolower(trim($_REQUEST['runtimeBSN']));

		// Get the BSN Complaint ID
		$complaintID = "";
		$Cresult  = $_objAdmin->_getSelectList('table_complaint AS C',"C.complaint_id",''," LOWER(C.product_serial_no)='".$bsnNo."' AND C.complain_status IN ('P','I')");

		// echo "<pre>";
		// print_r($Cresult);
		// exit;

		if(isset($Cresult[0]->complaint_id)) {
			$complaintID = trim($Cresult[0]->complaint_id);
			echo json_encode(array('status'=>'success', 'id'=>$complaintID));
		} else {
			echo json_encode(array('status'=>'failed', 'id'=>0));
		}
			

		} else {
			echo json_encode(array('status'=>'failed', 'id'=>0));
		}

	}

	if(isset($_REQUEST["type"]) && !empty($_REQUEST["type"]) && $_REQUEST['type'] == 'addBSN')
	{
		$search_bsn = $_objAdmin->_getSelectList2('table_item_bsn','*','',' bsn="'.$_POST['bsn'].'"');
		if(count($search_bsn)<=0 && strlen($_POST['bsn'])==17)
		{
			$data = array();	
			$bsn = $_POST['bsn'];
			$item_code = substr($bsn, 0, 3);
			$day = substr($bsn,3,1);
			$month = substr($bsn,4,1);
			$year = substr($bsn,5,1);
			$charging_site = substr($bsn,6,1);
			$division = substr($bsn,7,1);
			$model_code = substr($bsn,8,4);
			$battery_serial = substr($bsn,12,5);
			$day_value = $_objAdmin->_getSelectList2('table_bsn_logic_day','*','',' character_value="'.$day.'"');				

			$month_value = $_objAdmin->_getSelectList2('table_bsn_logic_month','*','',' character_value="'.$month.'"');
			$year_value = $_objAdmin->_getSelectList2('table_bsn_logic_year','*','',' character_value="'.$year.'"');
			$division_value = $_objAdmin->_getSelectList2('table_division','*','',' division_bsn_code="'.$division.'"');
			$manufacture_date = $year_value[0]->year."-".date('m',strtotime($month_value[0]->month))."-".str_pad($day_value[0]->day,2,'0',STR_PAD_LEFT);
			$item_details = $_objAdmin->_getSelectList2('table_item','','',' item_erp_code="'.$item_code.'" and model_code="'.$model_code.'"');
			$data['account_id'] = 1;
			if(count($item_details)>0 && count($month_value)>0 && count($day_value)>0 && count($year_value)>0 && count($division_value)>0)
			{
				$data['item_id']    			= $item_details[0]->item_id;
				$data['manufacture_date']		= $manufacture_date;
				// $data['date_of_sale']			= date('Y-m-d',strtotime($_POST['purchase_date']));
				$warranty_period 				= $item_details[0]->item_warranty;
				$warranty_prorata 				= $item_details[0]->item_prodata;
				$warranty_grace_period			= $item_details[0]->item_grace;
				$category_id 					= $item_details[0]->category_id;

				if($warranty_period>0)
				$data['warranty_period']=$warranty_period;

				if($warranty_prorata>0)
					$data['warranty_prorata']=$warranty_prorata;

				if($warranty_grace_period>0)
					$data['warranty_grace_period']=$warranty_grace_period;

				if($category_id>0)
					$data['category_id']=$category_id;

				$data['bsn']       				= $bsn;
				$data['stock_value']       		= 1;
				$data['account_id']        		= 1;
				$data['last_updated_date']		= date('Y-m-d');
				$data['last_update_datetime']	= date('Y-m-d H:i:s');
				$data['created_datetime']		= date('Y-m-d H:i:s');
				$data['user_type'] 		        = (isset($_SESSION['userLoginType']))?$_SESSION['userLoginType']:0;	
				$data['web_user_id'] 		    = (isset($_SESSION['userLoginType']))?$_SESSION['PepUpSalesUserId']:0;
				$data['status']					= 'S';	//BSN is in Complaint
				$bsn_id = $_objAdmin->_dbInsert($data,'table_item_bsn');

			}
		}
		else if(count($search_bsn)<=0 && strlen($_POST['bsn'])==10)
		{
			$data = array();	
			$bsn = $_POST['bsn'];
			$item_code = substr($bsn, 0, 2);
			$month = substr($bsn, 2, 2);
			$year = substr($bsn, 4, 1);
			$manufacture_date = "201".$year."-".$month."-01";				
			$data['account_id']        		= $_SESSION['accountId'];
			$item_details = $_objAdmin->_getSelectList2('table_item','','',' item_erp_code_old="'.$item_code.'"');	
			if(count($item_details)>0)
			{
				$data['item_id']    			= $item_details[0]->item_id;
				$data['manufacture_date']		= $manufacture_date;
				// $data['date_of_sale']			= date('Y-m-d',strtotime($_POST['purchase_date']));
				$warranty_period 				= $item_details[0]->item_warranty;
				$warranty_prorata 				= $item_details[0]->item_prodata;
				$warranty_grace_period			= $item_details[0]->item_grace;
				$category_id 					= $item_details[0]->category_id;

				if($warranty_period>0)
				$data['warranty_period']=$warranty_period;

				if($warranty_prorata>0)
					$data['warranty_prorata']=$warranty_prorata;

				if($warranty_grace_period>0)
					$data['warranty_grace_period']=$warranty_grace_period;

				if($category_id>0)
					$data['category_id']=$category_id;

				$data['bsn']       				= $bsn;
				$data['stock_value']       		= 1;
				$data['account_id']        		= $_SESSION['accountId'];
				$data['last_updated_date']		= date('Y-m-d');
				$data['last_update_datetime']	= date('Y-m-d H:i:s');
				$data['created_datetime']		= date('Y-m-d H:i:s');
				$data['user_type'] 		        = (isset($_SESSION['userLoginType']))?$_SESSION['userLoginType']:0;	
				$data['web_user_id'] 		    = (isset($_SESSION['userLoginType']))?$_SESSION['PepUpSalesUserId']:0;
				$data['status']					= 'S';	//BSN was Sold to the customer
				$bsn_id = $_objAdmin->_dbInsert($data,'table_item_bsn');

			}
		}

		$result  = $_objAdmin->_getSelectList('table_item_bsn AS BSN LEFT JOIN table_item AS I ON I.item_id = BSN.item_id LEFT JOIN table_wh_mrn_detail AS MRND ON MRND.bsn_id = BSN.bsn_id AND MRND.status = "P" LEFT JOIN table_segment as SEG on SEG.segment_id = I.item_segment LEFT JOIN table_division as DIVI on DIVI.division_id = I.item_division',"BSN.*, I.item_name, I.item_code, DIVI.division_name,SEG.segment_name, MRND.mrn_detail_id",''," BSN.bsn = '".$_POST['bsn']."'");
		if(!empty($result))
		{
			$data = array();
			foreach($result as $key=>$row) {

				// Check Warranty Date
				$warrantyEndDate = $_objStock->getBSNWarrantyEndDate($row->date_of_sale, $row->warranty_period, $row->warranty_prorata);
				if($warrantyEndDate == "-") {
					$warrantyEndDate = "";
				}

				$name = $row->bsn.'|'.$row->bsn_id.'|'.$row->item_name.'|'.date('d-M-y',strtotime($row->manufacture_date)).'|'.date('d/M/y',strtotime($warrantyEndDate)).'|'.$row->item_id.'|'.$row->mrn_detail_id.'|'.$row->segment_name.'|'.$row->division_name;
				array_push($data, $name);	
			}
			 echo json_encode($data);
		}
		

	}


	if(isset($_REQUEST['type']) && $_REQUEST['type']=='servicePersonnel')
	{
		
		$serPerList = $_objAdmin->_getSelectList2('table_service_personnel','*','',' warehouse_id = "'.$_SESSION['warehouseId'].'"');	
		if(count($serPerList)>0)
		{
			for($i=0;$i<count($serPerList);$i++)
			{
				echo "<option value='".$serPerList[$i]->service_personnel_id."'>".$serPerList[$i]->sp_name."</option>";
			}
		}
		else
		{
			echo "<option value=''>Select</option>";
		}
	}



?>