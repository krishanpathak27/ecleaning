<?php
	include("includes/config.inc.php");
	include("includes/globalarraylist.php");
	$_objAdmin = new Admin();

	if(isset($_GET['stateID'])){
		$stateID = $_GET['stateID'];
		$data = $_objAdmin->_getSelectList2('city LEFT JOIN state ON state.state_id = city.state_id',"city_id, state.state_id, city_name, state_name",'',' city.status = "A" and city.state_id IN ('.$stateID.') order by city_name');
		if(!empty($data)){
			if($_SESSION['districtCust'] =='all'){
				$selected = "selected";
			} else {
				$selected = "";
			}
			$result = "<option value='all'".$selected.">All</option>";
			foreach($data as $value){ 
			if($_SESSION['districtCust'] == $value->city_id){
				$selected = "selected";
			} else {
				$selected = "";
			}
				$result .= "<option value='".$value->city_id."' ".$selected.">".$value->city_name."</option>";
			}
		} else {
			$result = "<option value='all'>All</option>";
		}
		echo $result;
	} else if(isset($_GET['cityID'])){
		$cityID = $_GET['cityID'];
		$data = $_objAdmin->_getSelectList2('table_taluka AS T LEFT JOIN city AS C ON C.city_id = T.city_id',"T.taluka_name, T.taluka_id, C.city_name, C.city_id, C.state_id",'',' T.city_id = "'.$cityID.'" AND T.status = "A" order by T.taluka_name');
		if(!empty($data)){
			if($_SESSION['tehsilCust'] =='all'){
				$selected = "selected";
			} else {
				$selected = "";
			}
			$result = "<option value='all'".$selected.">All</option>";
			foreach($data as $value){ 
			if($_SESSION['tehsilCust'] == $value->taluka_id){
				$selected = "selected";
			} else {
				$selected = "";
			}
				$result .= "<option value='".$value->taluka_id."' ".$selected.">".$value->taluka_name."</option>";
			}
		} else {
			$result = "<option value='all'>All</option>";
		}
		echo $result;
	} 
	else if(isset($_GET['tehsilID'])){


		$tehsilID = $_GET['tehsilID'];
		$data = $_objAdmin->_getSelectList2('table_customer AS C LEFT JOIN table_taluka AS T ON C.taluka_id = T.taluka_id',"C.customer_name, C.customer_id, C.customer_code",'',' C.taluka_id = "'.$tehsilID.'" AND C.status = "A" AND C.customer_type = "C" order by C.customer_name');
		if(!empty($data)){
			if($_SESSION['partner_name'] =='all'){
				$selected = "selected";
			} else {
				$selected = "";
			}
			$result = "<option value='all'".$selected.">All</option>";
			foreach($data as $value){ 
			if($_SESSION['partner_name'] == $value->customer_id){
				$selected = "selected";
			} else {
				$selected = "";
			}
				$result .= "<option value='".$value->customer_id."' ".$selected.">".$value->customer_name."</option>";
			}
		} else {
			$result = "<option value='all'>All</option>";
		}
		echo $result;
	}
	else if(isset($_GET['partnerID'])){


		$partnerID = $_GET['partnerID'];
		$data = $_objAdmin->_getSelectList2('table_customer AS C ',"C.customer_code",'',' C.customer_id = "'.$partnerID.'" AND C.status = "A" AND C.customer_type = "C"');
		
		
		if(!empty($data)){

foreach($data as $value){ 
			/*if($_SESSION['partner_code'] =='all'){*/
			/*	$selected = "selected";
			} else {
				$selected = "";
			}*/
			//$result = "<option value='all'".$selected.">All</option>";
			//foreach($data as $value){ 
			/*if($_SESSION['partner_code'] == $value->customer_id){
				$selected = "selected";
			} else {
				$selected = "";
			}*/
				$result .= $value->customer_code;}
			}
			//}
		/*}*/ else {
			$result = "";
		}
		echo $result;
	}

?>