<?php
include("includes/config.inc.php");
include("includes/function.php");
$_objAdmin = new Admin();
include("header.inc.php");
if(isset($_POST['add']) && $_POST['add'] == 'yes')
{
$_SESSION['planMapdate']=$_POST['to'];
} else {
$_SESSION['planMapdate']=date("Y-m-d",strtotime("-0 day"));
}

include("all_salesman_tracking_map.php");
 ?>
 
<script>
    $(function() {
        $( "#from" ).datepicker({
			dateFormat: "d M yy",
            defaultDate: "1w",
            changeMonth: true,
            numberOfMonths: 3,
            onSelect: function( selectedDate ) {
                $( "#to" ).datepicker( "option", "minDate", selectedDate );
            }
        });
        $( "#to" ).datepicker({
			dateFormat: "d M yy",
            defaultDate: "-1w",
            changeMonth: true,
            numberOfMonths: 1,
            onSelect: function( selectedDate ) {
                $( "#from" ).datepicker( "option", "maxDate", selectedDate );
            }
        });
    });
	$(document).ready(function(){
	    $("#sal").change(function(){
		 document.report.submit();
		})
	});
</script>
<script src="javascripts/jquery-1.8.2.js" type="text/javascript"></script>
<script src="javascripts/jquery-ui.js"></script>
<script type="text/javascript" src="javascripts/validate.js"></script>


.<!-- start content-outer -->
<div id="content-outer">
<!-- start content -->
<div id="content">
<div id="page-heading"><h1><span style="color: #d74343; font-family: Tahoma; font-weight: bold;">All Salesman Tracking Report</span></h1> </div>

<table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">

<tr>
	
	<td>
	<!--  start content-table-inner -->
	<div id="content-table-inner">
	
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
	
	<tr valign="top">
	<td>
	
	<form name="frmPre" id="frmPre" method="post" action="#" enctype="multipart/form-data" >
	<div id="page-heading" align="center" ><h3> Report Date: <input type="text" id="to" name="to" class="date" value="<?php echo $_objAdmin->_changeDate($_SESSION['planMapdate']);?>"  readonly /><input name="add" type="hidden" value="yes" /><input name="submit" class="result-submit" type="submit" id="submit" value="Show Result" /></h3><!--<td><input type="button" value="Reset!" class="form-reset" onclick="location.href='admin_retailer_order_history.php?reset=yes';" /></td>--></div>
	
	</form>
	</td>
	
	<tr valign="top">
	<td>
		<!-- start id-form -->
		<div style="height:500px;overflow:auto;" >
		<table border="0" width="100%" cellpadding="0" cellspacing="0" id="id-form">
			<?php  
			if($roww<=0){
			?>
			<tr bgcolor="#A52A2A" style="color: #fff;font-weight: bold;">
				<td align="center" style="padding:10px;"><?php echo 'Salesman NOt Working During This time Period'; ?></td>
			</tr>
			<?php } else { ?>
			<tr>	
				<div id="map_canvas"></div>
			</tr>
			<?php } ?>
		</table>
		</div>
		<!-- end id-form  -->
	</td>
	
	<td>
	<!-- right bar-->
	<?php //include("rightbar/salesmanMapList_bar.php") ?>
	</td>
	</tr>
<tr>
<td><img src="images/blank.gif" width="695" height="1" alt="blank" /></td>
<td></td>
</tr>
</table>

<!--  end content-table-inner  -->
</td>
<td id="tbl-border-right"></td>
</tr>
<!--<tr>
	<th class="sized bottomleft"></th>
	<td id="tbl-border-bottom">&nbsp;</td>
	<th class="sized bottomright"></th>
</tr>-->
</table>
<div class="clear">&nbsp;</div>
</div>
<!--  end content -->
<div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->
<div class="clear">&nbsp;</div> 
<!-- start footer -->         
<?php include("footer.php");?>
<!-- end footer -->
 
</body>
</html>