<?php

include("includes/config.inc.php");

include("classes/functions.php");

include("includes/function.php");

$page_name="Mobile/Tablet App";

$_objAdmin = new Admin();



if(isset($_POST['add']) && $_POST['add'] == 'yes')

{

	if(isset($_FILES['fupload'])) {

		$filename = $_FILES['fupload']['name'];

		$source = $_FILES['fupload']['tmp_name'];

		$type = $_FILES['fupload']['type'];

		$name = explode('.', $filename); 

		$target = 'apps/' . $name[0] . '-' . time() . '/'; 

		$unlink='apps/' . $name[0] . '-' . time(); 

		$accepted_types = array('application/zip', 'application/x-zip-compressed', 'multipart/x-zip', 'application/x-compressed', 'application/vnd.android.package-archive', 'application/octet-stream');

		foreach($accepted_types as $mime_type) {

			if($mime_type == $type) {

				$okay = true;

				break;

			} 

		}

		$okay = strtolower($name[1]) == 'zip' ? true : false;

		if(!$okay) {

			$err="Please choose a zip file";

			//die;

		}

		mkdir($target);

		$saved_file_location = $target . $filename;

		if(move_uploaded_file($source, $target . $filename)) {

			openZip($saved_file_location);

		} else {

			$err="There was a problem";

			//die;

		}

		$scan = scandir($target . $name[0]);

		$ext = substr($filename, 0,strrpos($filename,'.'));

		$path=$target . $ext."/apkdetails.xml";

		$link=$target . $ext;

		for($i = 0; $i<count($scan); $i++) {

			if (strlen($scan[$i]) >= 3) { 

				$check_for_apk = strpos($scan[$i], '.apk');

				if ($check_for_apk === false ){

				} else {

					$apk=$scan[$i] ;

				}

			}	

		}

		$ext = substr($filename, 0,strrpos($filename,'.'));

		$path=$target . $ext."/apkdetails.xml";

		$link=$target . $ext;

		if(!$xml=simplexml_load_file($path)){

		trigger_error('Error reading XML file',E_USER_ERROR);

		}

		foreach ($xml as $ApkDetail) {

		$mandatory = $ApkDetail->mandatory;  
		$version = $ApkDetail->version;     
		}
		$auRec=$_objAdmin->_getSelectList('mobile_app',"*",''," version='".$version."'");
		if(is_array($auRec)){
			$err= "App version:".$version." already exists in the system.";
			} else {
				$sql="INSERT INTO mobile_app (version, link, mandatory, apkname) VALUES ('$version','$link','$mandatory', '$apk')";
				if (!mysql_query($sql)) {
					die('Error: ' . mysql_error());
				}
				$sus="Apk Upload Successfully";
			}
		//echo "Records added";
	} 

}
?>
<?php include("header.inc.php"); ?>
<style>
div.img
  {
  margin:5px;
  padding:5px;
  border:1px solid  #848484;
  height:auto;
  width:auto;
  float:left;
  text-align:center;
  }
div.img img
  {
  display:inline;
  margin:5px;
  border:1px solid #fbfcfc;
  }
div.img a:hover img
  {
  border:1px solid #fbfcfc;
  }
div.desc
  {
  text-align:center;
  font-weight:normal;
  width:120px;
  margin:2px;
  }
</style>
<script type="text/javascript" src="javascripts/validate.js"></script>



<!-- start content-outer -->

<div id="content-outer">

<!-- start content -->

	<div id="content">

	<div id="page-heading"><h1><span style="color: #d74343; font-family: Tahoma; font-weight: bold;">Mobile/Tablet App</span></h1></div>

		<table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">

		<!--<tr>

			<th rowspan="3" class="sized"><img src="images/side_shadowleft.jpg" width="20" height="300" alt="" /></th>

			<th class="topleft"></th>

			<td id="tbl-border-top">&nbsp;</td>

			<th class="topright"></th>

			<th rowspan="3" class="sized"><img src="images/side_shadowright.jpg" width="20" height="300" alt="" /></th>

		</tr>-->

		<tr>

			<!--<td id="tbl-border-left"></td>-->

			<td>

			<!--  start content-table-inner -->

			<div id="content-table-inner">

				<table border="0" width="100%" cellpadding="0" cellspacing="0">

				<tr valign="top">

					<td>

					<!--  start message-red -->

					<?php if($err!=''){?>

					<div id="message-red">

						<table border="0" width="100%" cellpadding="0" cellspacing="0">

						<tr>

							<td class="red-left"> <?php echo $err; ?></td>

							<td class="red-right"><a class="close-red"><img src="images/icon_close_red.gif"   alt="" /></a></td>

						</tr>

						</table>

					</div>

					<?php } ?>

					<!--  end message-red -->

					<?php if($sus!=''){?>

					<!--  start message-green -->
					<div id="message-green">
						<table border="0" width="100%" cellpadding="0" cellspacing="0">
						<tr>
							<td class="green-left"><?php echo $sus; ?></td>
							<td class="green-right"><a class="close-green"><img src="images/icon_close_green.gif"   alt="" /></a></td>
						</tr>
						</table>
					</div>
					<?php } ?>
					<!--  end message-green -->
					
			<!-- start id-form -->
		<form name="frmPre" id="frmPre" method="post" action="" enctype="multipart/form-data" >
			<table border="0" cellpadding="0" cellspacing="0"  id="id-form">
			<tr>
				<th valign="top">Upload App:</th>
				<td><input type="file" name="fupload" id="file_1" class="field required" ></td>
			</tr>
			<tr>
				<th>&nbsp;</th>
				<td valign="top">
					<input name="add" type="hidden" value="yes" />
					<input name="submit" class="form-submit" type="submit" id="submit" value="Upload Zip File" />
					
				</td>
			</tr>
			<tr>
				<th>&nbsp;</th>
				<td valign="top" >
					<?php
							$auRec=$_objAdmin->_getSelectList(' mobile_app',"*",'',"");	
							for($i=0;$i<count($auRec);$i++){
							?>
							<div class="img">
								<img src="images/ic_launcher.png" alt="" width="60" height="40">
								<div ><b>Version: <?php echo $auRec[$i]->version;?></b></div>
							</div>
							<?php } ?>
					
				</td>
			</tr>
			</table>
		</form>
			<!-- end id-form  -->
			</td>
			<td>
			<!-- right bar-->
			<div class="clear"></div>
		</div>
<!-- end related-act-bottom -->
</div>
<!-- end related-activities -->
</td>
</tr>
<tr>
<td><img src="images/blank.gif" width="695" height="1" alt="blank" /></td>
<td></td>
</tr>
</table>
<div class="clear"></div>
</div>
<!--  end content-table-inner  -->
</td>
<td id="tbl-border-right"></td>
</tr>
<!--<tr>
	<th class="sized bottomleft"></th>
	<td id="tbl-border-bottom">&nbsp;</td>
	<th class="sized bottomright"></th>
</tr>-->

</table>

<div class="clear">&nbsp;</div>



</div>

<!--  end content -->

<div class="clear">&nbsp;</div>

</div>

<!--  end content-outer -->

<div class="clear">&nbsp;</div>

    

<!-- start footer --> 

<?php include("footer.php") ?>  

<!-- end footer -->

</body>

</html>