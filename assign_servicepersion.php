<?php

include("includes/config.inc.php");
include("includes/function.php");

$_objAdmin = new Admin();
$_objStock = new StockClass();
$objArrayList= new ArrayList();


?>

<?php include("header.inc.php");
include("company.inc.php");
// $pageAccess=1;
// $check=$objArrayList->checkAccess($pageAccess, basename($_SERVER['PHP_SELF']));
// echo $check;
//  if($check == false){
//  header('Location: ' . $_SERVER['HTTP_REFERER']);
 //}

 ?>

 <style>
.sortable		{ padding:0; }
.sortable li	{ padding:4px 8px; color:#000; cursor:move; list-style:none; width:350px; background:#ddd; margin:10px 0; border:1px solid #999; }
#message-box		{ background:#fffea1; border:2px solid #fc0; padding:4px 8px; margin:0 0 14px 0; width:350px; }
</style>
<div class="clear"></div>
<!-- start content-outer -->
<div id="content-outer">
<!-- start content -->
	<div id="content">
	<div id="page-heading"><h1><span style="color: #d74343; font-family: Tahoma; font-weight: bold;">Complaint / MRN Assignment</span></h1></div>
		<table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">
		<tr>
			<td id="tbl-border-left"></td>
			<td>
			<!--  start content-table-inner -->
			<div id="content-table-inner">
				<table border="0" width="100%" cellpadding="0" cellspacing="0" style="margin-left:10px;">
				<tr valign="top">
					<td>
					<!--  start message-red -->
					<?php if($err!=''){?>
					<div id="message-red">
						<table border="0" width="100%" cellpadding="0" cellspacing="0">
						<tr>
							<td class="red-left"> <?php echo $err; ?></td>
							<td class="red-right"><a class="close-red"><img src="images/icon_close_red.gif"   alt="" /></a></td>
						</tr>
						</table>
					</div>
					<?php }?>
					<!--  end message-red -->
					<?php if($sus!=''){?>
					<!--  start message-green -->
					<div id="message-green">
						<table border="0" width="100%" cellpadding="0" cellspacing="0">
						<tr>
							<td class="green-left"><?php echo $sus; ?></td>
							<td class="green-right"><a class="close-green"><img src="images/icon_close_green.gif"   alt="" /></a></td>
						</tr>
						</table>
					</div>
					<?php } ?>
					<!--  end message-green -->
					<div id="step-holder">



<div class="clear"></div>
</div>


<!-- start id-form -->
<form name="frmPre" id="frmPre" method="post" action="assign_servicepersion.php?id=<?php echo $_REQUEST['id'];?>" enctype="multipart/form-data" >
<table border="0" cellpadding="0" cellspacing="0"  id="id-form">
<tr><th valign="top">BSN Number</th><td><?php echo $_REQUEST['bsn_no'];?></tr>
<tr><th valign="top">Sale Date</th><td><?php echo $_REQUEST['sale_Date'];?></tr>


<!-- AJAY@2017-04-28 -->


<?php if(isset($_GET['service_distributor_id']) && $_SESSION['serviceDistributorId']>0) {?>


<tr>
		<th valign="top">Service Distributor SP:</th>
			<td>
			<select name="service_distributor_sp_id" id="service_distributor_sp_id" class="styledselect_form_4 required">
			<option value="">Please Select</option>
			<?php $serviceDistributorSPDetail = $_objAdmin->_getSelectList('table_service_distributor_sp','service_distributor_sp_id, service_distributor_sp_name, service_distributor_sp_code','',"service_distributor_id =".$_GET['service_distributor_id']);
			if(is_array($serviceDistributorSPDetail)){
			for($i=0;$i<count($serviceDistributorSPDetail);$i++){?>
			<option value="<?php echo $serviceDistributorSPDetail[$i]->service_distributor_sp_id;?>"><?php echo ucwords(strtolower(trim($serviceDistributorSPDetail[$i]->service_distributor_sp_name)));?></option>
			<?php }} ?>
			</select>
			</td>
</tr>



<?php }else if(isset($_GET['warehouse_id']) && isset($_GET['mrn_id']) && $_REQUEST['warehouse_id']>0) {

$Sp_Choosen = $_objAdmin->_getSelectList2('table_wh_mrn_detail','*','',' bsn="'.$_REQUEST['bsn_no'].'" and mrn_id="'.$_REQUEST['mrn_id'].'"');

?>
<tr>
		<th valign="top">Service Engineer:</th>
			<td>
			<select name="service_personnel_id" id="service_personnel_id" class="styledselect_form_4 required">
			<option value="">Please Select</option>
			<?php $servicePersonnelSPDetail = $_objAdmin->_getSelectList('table_service_personnel','service_personnel_id, sp_name, sp_code','',"warehouse_id =".$_GET['warehouse_id']);
			if(is_array($servicePersonnelSPDetail)){
			for($i=0;$i<count($servicePersonnelSPDetail);$i++){?>
			<option value="<?php echo $servicePersonnelSPDetail[$i]->service_personnel_id;?>" <?php if($servicePersonnelSPDetail[$i]->service_personnel_id==$Sp_Choosen[0]->service_personnel_id) echo 'selected';?> required><?php echo ucwords(strtolower(trim($servicePersonnelSPDetail[$i]->sp_name)));?></option>
			<?php }} ?>
			</select>
			</td>
</tr>
<tr>
		
			<td><input type="hidden" name="assign_mrn" id="assign_mrn" value="yes"></td>
			<td><input type="hidden" name="mrn_id" id="mrn_id" value="<?php echo $_REQUEST['mrn_id'];?>"></td>
			<td><input type="hidden" name="bsn" id="bsn" value="<?php echo $_REQUEST['bsn_no'];?>"></td>
			<td><input type="hidden" name="mrn_detail_id" id="mrn_detail_id" value="<?php echo $_REQUEST['mrn_detail_id'];?>"></td>
</tr>


<?php } else {?>

<tr>
		<th valign="top">Service Engineer:</th>
			<td>
			<select name="brand_id" id="brand_id" class="styledselect_form_4 required">
			<option>Please Select</option>
			<?php if($_SESSION['userLoginType']==7){
				$brands = $_objAdmin->_getSelectList('table_service_personnel','*','',"warehouse_id=".$_SESSION['warehouseId']); }
				else{
					//$brands = $_objAdmin->_getSelectList('table_service_personnel','*','',""); 
						$brands = $_objAdmin->_getSelectList('table_service_personnel','*','',''); 
				}

			if(is_array($brands)){
			for($i=0;$i<count($brands);$i++){?>
			<option value="<?php echo $brands[$i]->service_personnel_id;?>" brand_code="<?php echo $brands[$i]->service_personnel_id;?>"  ><?php echo ucwords(strtolower(trim($brands[$i]->sp_name)));?></option>
			<?php }} ?>
			</select>
			</td>
		</tr>

<?php }?>

<!-- AJAY@2017-04-28 -->





					<tr>
						<th>&nbsp;</th>
						<td valign="top">
						<?php if(!(isset($_GET['warehouse_id']) && isset($_GET['mrn_id']) && $_REQUEST['warehouse_id']>0)){?>
			              <input name="service_persion" type="hidden" value="yes" />			              
			              <input name="comp_id" type="hidden" value="<?php echo $_REQUEST['id']; ?>" />
			              <?php }?>
			              <input name="account_id" type="hidden" value="<?php echo $_SESSION['accountId']; ?>" />
			                <input type="button" value="Back" class="form-reset" onclick="location.href='<?php if(isset($_REQUEST['mrn_detail_id']) && $_REQUEST['mrn_detail_id']!='') echo 'warehouse_mrn.php?action=view&id='.$_REQUEST['mrn_id']; else echo 'distributor_wearhouse_comp.php'?>'" />
			             <input name="save" class="form-submit" type="submit" id="save" value="Save" />
			           
			           
		</td>
		</tr>
					</tr>
					</table>
				</form>
			<!-- end id-form  -->
			</td>
			
			<td><?php /*function create_pyramid($limit) {
					for($row = 1; $row < $limit; $row ++) {
						$stars = str_repeat('*', ($row - 1) * 2 + 1);
						$space = str_repeat(' ', $limit - $row);
						echo $space . $stars . '<br/>';
					}
				}
					echo "<pre>" ;
				create_pyramid(10);*/?>
				
			<!--</div>-->
				
				
				</td>
			</tr>
			</table>
			<!-- right bar-->
			
			<div class="clear"></div>
			</div>
			</td>
		</tr>
	</table>
<div class="clear"></div>
</div>
<!--  end content-table-inner  -->
<div class="clear">&nbsp;</div>

</div>
<!--  end content-outer -->
<div class="clear">&nbsp;</div>
    
<!-- start footer --> 
<?php include("footer.php") ?>  
<!-- end footer -->
</body>
</html>