<?php
include("includes/config.inc.php");
include("includes/function.php");
include("includes/globalarraylist.php");
$page_name="Attendance Report";
$_objModuleClass = new ModuleClass();

$feature_array = $_objModuleClass->getFeatureModulel();
//print_r($feature_array);

//print_r($feature_array);
if(in_array(24,$feature_array))
{
$flag=1;
// for  show.
}
else
{
$flag=0;
// for not show.	
}
//echo $flag;

if(isset($_POST['showReport']) && $_POST['showReport'] == 'yes')
{	
	if($_POST['sal']!="") 
	{
	$_SESSION['SalAttList']=$_POST['sal'];	
	}
	if($_POST['from']!="") 
	{
	$_SESSION['FromAttList']=$_objAdmin->_changeDate($_POST['from']);	
	}
	if($_POST['to']!="") 
	{
	$_SESSION['ToAttList']=$_objAdmin->_changeDate($_POST['to']);	
	}
	if($_POST['division_id']!="") 
	{
	$_SESSION['DivisionAttList']=$_POST['division_id'];	
	}
	if($_POST['sal']=="") 
	{
	  unset($_SESSION['SalAttList']);	
	}
	
	
} else {
$_SESSION['FromAttList']= $_objAdmin->_changeDate(date("Y-m-d"));
$_SESSION['ToAttList']= $_objAdmin->_changeDate(date("Y-m-d"));	
}
if(isset($_REQUEST['reset']) && $_REQUEST['reset'] == 'yes')
{
	 unset($_SESSION['SalAttList']);	
	/*unset($_SESSION['FromAttList']);	
	unset($_SESSION['ToAttList']); */
	 unset($_SESSION['DivisionAttList']);
	header("Location: attendance_report.php");
}

if($_SESSION['SalAttList']!=''){
$SalName=$_objAdmin->_getSelectList('table_salesman','salesman_name',''," salesman_id='".$_SESSION['SalAttList']."'"); 
$sal_name=$SalName[0]->salesman_name;
} else {
$sal_name="All Salesman";
}
if($_SESSION['DivisionAttList']!=''){ $divisionCond=" and s.division_id='".$_SESSION['DivisionAttList']."'";}

?>

<?php include("header.inc.php") ?>
<script src="javascripts/dateNextPrev.js" type="text/javascript"></script>
<script src="http://code.jquery.com/jquery-1.7.2.js"></script>
<script type="text/javascript">

    function PrintElem(elem)
    {
        Popup($(elem).html());
    }

    function Popup(data) 
    {
		
        var mywindow = window.open('', 'Report');
		
        mywindow.document.write('<html><head><title>Attendance Report</title>');
		mywindow.document.write('<table><tr><td><b>Salesman Name:</b> <?php echo $sal_name; ?></td><td><b>From Date:</b> <?php echo $_SESSION['FromAttList']; ?></td><td><b>To Date:</b> <?php echo $_SESSION['ToAttList']; ?></td></tr></table>');
        /*optional stylesheet*/ //mywindow.document.write('<link rel="stylesheet" href="main.css" type="text/css" />');
        mywindow.document.write('</head><body >');
        mywindow.document.write(data);
        mywindow.document.write('</body></html>');

        mywindow.print();
        mywindow.close();
        return true;
    }

 function showSalesmanDivision(str)
{
	//alert(str);
	var bracID=$('#branch_id').val();
	//alert(divID);
	$.ajax({
		'type': 'POST',
		'url': 'salesman_dropdown.php',
		'data': 'divisionID='+str,
		'success' : function(mystring) {
			//alert(mystring);
		document.getElementById("sal").innerHTML = mystring;
		$('#cityListID').hide();		
		}
	});
}   

$(document).ready(function(){
<?php if($_POST['submit']=='Export to Excel'){ ?>
tableToExcel('report_export', 'Attendance Report', 'Attendance Report.xls');
<?php } ?>
});

</script>
<script type="text/javascript">
// Popup window code
function photoPopup(url) {
	popupWindow = window.open(
		url,'popUpWindow','height=500,width=800,left=100,top=100,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no,status=yes')
}
</script>
<!-- start content-outer -->
<input name="pagename" type="hidden"  id="pagename" value="admin_order_list.php" />
<div id="content-outer">
<!-- start content -->
<div id="content">
<div id="page-heading"><h1><span style="color: #d74343; font-family: Tahoma; font-weight: bold;">Attendance Report</span></h1></div>
<table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">
<tr>
	<td>
	<!--  start content-table-inner -->
	<div id="content-table-inner">
	
	<div id="page-heading" align="left" >
	<form name="frmPre" id="frmPre" method="post" action="" enctype="multipart/form-data" >
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
	<!--<tr  align="right">
	  <td></td>
	  <td><table border="0" width="70%" ><tr><td> <img src="css/images/prev.png" onclick="dateFromPrev();"> </td> <td align="right"> <img src="css/images/next.png" onclick="dateFromNext();"> </td><td>&nbsp;&nbsp; </td> </tr></table></td>
	  <td><table border="0" width="75%"><tr><td> <img src="css/images/prev.png" onclick="dateToPrev();"> </td> <td align="right"> <img src="css/images/next.png" onclick="dateToNext();"> </td><td>&nbsp;&nbsp; </td> </tr></table></td>
	  <td></td>
	  <td></td>-->
	<tr>
		<td><h3>Division: </h3><h6>
		<select name="division_id" id="division_id" class="menulist" onChange="showSalesmanDivision(this.value)" style="" >
			<option value="">Select Division</option>
			<?php $divisionList=$_objAdmin->_getSelectList('table_division',"division_id,division_name",''," order by division_name asc ");
			//print_r($branchList); 
			foreach($divisionList as $divisionVal){		
				
				if($_SESSION['DivisionAttList'] == $divisionVal->division_id){ $select="selected"; }else {$select="";}		
			?>
			<option value="<?php echo $divisionVal->division_id; ?>" <?php echo $select; ?>><?php echo $divisionVal->division_name; ?></option>
			<?php	
			}
			?>
		</select>
		</h6>
		</td>

		<td><h3>Salesman Name: </h3><h6>
		<select name="sal" id="sal" class="styledselect_form_5" style="" >
			<?php echo $rtndata = $_objArrayList->GetSalesmenMenu($salsList, $_SESSION['SalAttList']);?>
		</select>
		</h6>
		</td>
		
		
		<td><h3>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;From Date: </h3><h6><img src="css/images/prev.png" height="18" width="18" onclick="dateFromPrev();"> <input type="text" id="from" name="from" class="date" style="width:150px" value="<?php  echo $_SESSION['FromAttList'];?>"  readonly /> <img src="css/images/next.png" height="18" width="18" onclick="dateFromNext();"></h6></td>
		<td><h3>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;To Date: </h3><h6><img src="css/images/prev.png" height="18" width="18" onclick="dateToPrev();"> <input type="text" id="to" name="to" class="date" style="width:150px" value="<?php echo $_SESSION['ToAttList']; ?>"  readonly /> <img src="css/images/next.png" height="18" width="18" onclick="dateToNext();"></h6></td>
		<td><h3></h3><input name="submit" class="result-submit" type="submit" id="submit" value="View Details" />
		<input type="button" value="Reset!" class="form-reset" onclick="location.href='attendance_report.php?reset=yes';" />
		</td>
		<td colspan="3"></td>
		</tr>
		<tr>
		<td colspan="4"><input name="showReport" type="hidden" value="yes" />
		<input type="button" value="Print" class="result-submit" onclick="PrintElem('#Report')" />
		<a id="dlink"  style="display:none;"></a>
		<input type="submit" name="submit" value="Export to Excel" class="result-submit"  >
		<!-- <a href="attendance_report_year_graph.php?y=<?php echo checkFromdate($_SESSION['FromAttList']); ?>&salID=<?php echo $_SESSION['SalAttList'];?>" target="_blank"><input type="button" value="Show Graph" class="result-submit" /></a> -->
		</td>
		<td></td>
	</tr>
	</table>
	</form>
	</div>
	<div id="Report">
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
	<tr valign="top">
	<?php
	if($_SESSION['SalAttList']==""){
		if($salsList!=''){
			for($a=0;$a<count($salsList);$a++){
				if(count($salsList)==$a+1){
					$sal_in.=$salsList[$a];
				} else {
					$sal_in.=$salsList[$a].",";
				}
			}
		}
		
		if($sal_in!= "")
		$salesmanList = "and i.salesman_id in (".$sal_in.")";
		
	$auAttSt=$_objAdmin->_getSelectList2('sal_id as i left join act_in as n on i.salesman_id=n.salesman_id and i.activity_date=n.activity_date left join act_out as o on i.salesman_id=o.salesman_id and i.activity_date=o.activity_date left join table_salesman as s on s.salesman_id=i.salesman_id left join table_division as d on d.division_id=s.division_id',"s.salesman_name,s.salesman_id,d.division_name,n.attendance_start,o.attendance_end,i.activity_date,n.in_address,o.out_address,n.start_let,o.end_let,n.activity_id as in_activity,o.activity_id as out_activity",''," (i.activity_date BETWEEN '".date('Y-m-d', strtotime($_SESSION['FromAttList']))."' AND '".date('Y-m-d', strtotime($_SESSION['ToAttList']))."') ".$salesmanList." $stateCond $divisionCond order by s.salesman_name asc, i.activity_date asc");
	} else {

	$auAttSt=$_objAdmin->_getSelectList2('sal_id as i left join act_in as n on i.salesman_id=n.salesman_id and i.activity_date=n.activity_date left join act_out as o on i.salesman_id=o.salesman_id and i.activity_date=o.activity_date left join table_salesman as s on s.salesman_id=i.salesman_id left join table_division as d on d.division_id=s.division_id',"s.salesman_name,s.salesman_id,d.division_name,n.attendance_start,o.attendance_end,i.activity_date,n.in_address,o.out_address,n.start_let,o.end_let,n.activity_id as in_activity,o.activity_id as out_activity",''," (i.activity_date BETWEEN '".date('Y-m-d', strtotime($_SESSION['FromAttList']))."' AND '".date('Y-m-d', strtotime($_SESSION['ToAttList']))."') and i.salesman_id='".$_SESSION['SalAttList']."' $stateCond order by s.salesman_name asc, i.activity_date asc");
	}
	?>
		<td>
		<table  border="0" width="100%" cellpadding="0" cellspacing="0" id="report_export" name="report_export">
				<div id="Report">
			<tr  bgcolor="#6E6E6E" style="color: #fff;font-weight: bold;" >
				<?php if($_SESSION['SalAttList']==''){ ?>
				<td style="padding:10px;" width="20%">Salesman Name</td>
				<?php } ?>
				<td style="padding:10px;" width="10%">Date</td>
				<td style="padding:10px;" width="10%">Day</td>
				<?php if ($opRec[0]->view_activity_report=="Yes"){ ?>
				<td style="padding:10px;" width="10%">Timeline</td>
				<?php } ?>
				
				<td style="padding:10px;" width="15%">In Time</td>
				<td style="padding:10px;" width="10%">Division</td>
				<td style="padding:10px;" width="15%">In Address</td>		
				<td style="padding:10px;" width="15%">Out Time</td>
				<td style="padding:10px;" width="15%">Out Address</td>
				<td style="padding:10px;" width="20%">Working Hours(-20 minutes)</td>
			</tr>
			<?php
			$daywork = array();
			if(is_array($auAttSt)){
				for($i=0;$i<count($auAttSt);$i++)
				{
				$daywork[] = $auAttSt[$i]->activity_date;
				if($auAttSt[$i]->attendance_start!='' && $auAttSt[$i]->attendance_end!='') {	
				$time1 = date('H:i:s', strtotime('+20 minutes', strtotime($auAttSt[$i]->attendance_start))); // start time + 20min
				$time2 = $auAttSt[$i]->attendance_end; //End Time
				list($hours, $minutes, $sec) = explode(':', $time1);
				$startTimestamp = mktime($hours, $minutes, $sec);
				list($hours, $minutes, $sec) = explode(':', $time2);
				$endTimestamp = mktime($hours, $minutes, $sec);
				$diff = $endTimestamp - $startTimestamp;
				if($diff > 0){
				$minutes = ($diff / 60) % 60;
				$hours = floor($diff / 3600);
				$seconds = $diff  % 60;
				} else {
				$minutes = ($diff / 60) % 60;
				$hours = ceil($diff / 3600);
				$seconds = abs($diff  % 60);
				}
				if($seconds<=9){
				$seconds="0".$seconds;
				}
				 
				if (strstr($hours,'-')) 
					{
						if($hours=='0'){  $hours = '00'; }
						else
						{
					            $hours = '-'.date('H', strtotime($hours.':00:00'));
						}
					} 
				else{
					 $minutes =  date('i', strtotime('00:'.$minutes.':00'));
					
					$hours = 	date('H', strtotime($hours.':00:00'));
					}
				if (strstr($minutes,'-')){
						if($minutes<0 && $minutes>='-9' )
							{
								$minutes = '-0'.abs($minutes);
							}
					}
				if (strstr($hours,'-') || strstr($minutes,'-')){
					
					            $hours = '-'.date('H', strtotime($hours.':00:00'));
							    $minutes = date('i', strtotime('00:'.abs($minutes).':00'));
					
					} 
				  $work_hours=$hours.":".$minutes.":".$seconds; 
				///$time1 = date('H:i:s', strtotime('+20 minutes', strtotime($auAttSt[$i]->start_time))); // start time + 20min
				} else {
				$work_hours="-";
				}
				?>
				<tr  style="border-bottom:2px solid #6E6E6E;" >
				<?php if($_SESSION['SalAttList']==''){ ?>
				<td style="padding:10px;" width="20%"><?php echo $auAttSt[$i]->salesman_name;?> </td>
				<?php } ?>
				<td style="padding:10px;" width="10%"><?php echo $_objAdmin->_changeDate($auAttSt[$i]->activity_date);?> </td>
				<td style="padding:10px;" width="10%"><?php echo date("l",strtotime($auAttSt[$i]->activity_date));?></td>
				<?php if ($opRec[0]->view_activity_report=="Yes"){ ?>
				<td style="padding:10px;" width="10%"><a href="salesman_report.php?sal=<?php echo base64_encode($auAttSt[$i]->salesman_id);?>&date=<?php echo base64_encode($auAttSt[$i]->activity_date); ?>&act=<?php echo base64_encode($auAttSt[$i]->in_activity) ?>&act1=<?php echo base64_encode($auAttSt[$i]->out_activity) ?>#<?php echo base64_encode($auAttSt[$i]->in_activity) ?>" target="_blank">View</a></td>
				<?php } ?>
				<td style="padding:10px;" width="10%">
				<?php 
				if($auAttSt[$i]->attendance_start!=''){
				echo $auAttSt[$i]->attendance_start;
				if($auAttSt[$i]->start_let!=''){ ?>
					<a href="JavaScript:newPopup('activity_map.php?id=<?php echo base64_encode($auAttSt[$i]->in_activity);?>');">&nbsp;&nbsp;&nbsp;Map</a>
				<?php } 
				    if($flag==1)
					{
					$aPho=$_objAdmin->_getSelectList('table_image','image_id',''," ref_id='".$auAttSt[$i]->in_activity."' and image_type=6 "); 
					if(is_array($aPho)){
					?>
					<a href="JavaScript:photoPopup('activity_photo.php?id=<?php echo base64_encode($aPho[0]->image_id) ;?>');">&nbsp;&nbsp;&nbsp;Image</a>
					<?php 
					}
					}
				} else {
				echo "In Time Not Available"; 
				}
				?>
				</td>
				<td style="padding:10px;" width="15%"><?php echo $auAttSt[$i]->division_name;?></td>
				<td style="padding:10px;" width="15%"><?php echo $auAttSt[$i]->in_address;?></td>
				<td style="padding:10px;" width="10%">
				<?php 
				if($auAttSt[$i]->attendance_end!=''){
				echo $auAttSt[$i]->attendance_end;
				if($auAttSt[$i]->end_let!=''){ ?>
					<a href="JavaScript:newPopup('activity_map.php?id=<?php echo base64_encode($auAttSt[$i]->out_activity);?>');">&nbsp;&nbsp;&nbsp;Map</a>
				<?php }
				    if($flag==1)
					{
					$aPho=$_objAdmin->_getSelectList('table_image','image_id',''," ref_id='".$auAttSt[$i]->out_activity."' and image_type=6 "); 
					if(is_array($aPho)){
					?>
					<a href="JavaScript:photoPopup('activity_photo.php?id=<?php echo base64_encode($aPho[0]->image_id) ;?>');">&nbsp;&nbsp;&nbsp;Image</a>
					<?php 
					}}
				} else {
				echo "Out Time Not Available"; 
				}
				?>
				</td>
				<td style="padding:10px;" width="15%"><?php echo $auAttSt[$i]->out_address;?></td>
				<td style="padding:10px;" width="10%"><?php echo $work_hours;?></td>
				<?php } ?>
				<?php if($_SESSION['SalAttList']!=''){ ?>
				<tr   >
				<td style="padding:10px;" colspan="6"><b>Total Worked Days:</b> <?php echo count($daywork) ?></td>
				</tr>
				<?php } ?>
			<?php } else { ?>
			<tr  style="border-bottom:2px solid #6E6E6E;" align="center">
				<td style="padding:10px;" colspan="10">Report Not Available</td>
			</tr>
			<?php } ?>
			</div>
		</table>
		</td>
	</tr>
	<tr>
		<td><img src="images/blank.gif" width="695" height="1" alt="blank" /></td>
		<td></td>
	</tr>
	</table>
	</div>
<div class="clear"></div>
</div>
<!--  end content-table-inner  -->
</td>
<td id="tbl-border-right"></td>
</tr>
<!--<tr>
	<th class="sized bottomleft"></th>
	<td id="tbl-border-bottom">&nbsp;</td>
	<th class="sized bottomright"></th>
</tr>-->
</table>
<div class="clear">&nbsp;</div>
</div>
<!--  end content -->
<div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->
<div class="clear">&nbsp;</div> 
<!-- start footer -->         
<?php include("footer.php") ?>
<?php unset($_SESSION['SalAttList']);	?>
<!-- end footer -->
<script src="javascripts/jquery-1.8.2.js" type="text/javascript"></script>
<script src="javascripts/jquery-ui.js"></script>
<script type="text/javascript" src="javascripts/validate.js"></script>
<script type="text/javascript">
// Popup window code
function newPopup(url) {
	popupWindow = window.open(
		url,'popUpWindow','height=500,width=600,left=200,top=100,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no,status=yes')
}
</script>
<script>
    $(function() {
        $( "#from" ).datepicker({
			dateFormat: "d M yy",
            defaultDate: "w",
            changeMonth: true,
            numberOfMonths: 1,
            onSelect: function( selectedDate ) {
                $( "#to" ).datepicker( "option", "minDate", selectedDate );
            }
        });
        $( "#to" ).datepicker({
			dateFormat: "d M yy",
            defaultDate: "-w",
            changeMonth: true,
            numberOfMonths: 1,
            onSelect: function( selectedDate ) {
                $( "#from" ).datepicker( "option", "maxDate", selectedDate );
            }
        });
    });
</script>
<script type='text/javascript'>//<![CDATA[ 
var tableToExcel = (function () {
        var uri = 'data:application/vnd.ms-excel;base64,'
        , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table><tr><td><b>Salesman Name:</b> <?php echo $sal_name; ?></td><td><b>From Date:</b> <?php echo $_SESSION['FromAttList']; ?></td><td><b>To Date:</b> <?php echo $_SESSION['ToAttList']; ?></td></tr></table><table>{table}</table></body></html>'
        , base64 = function (s) { return window.btoa(unescape(encodeURIComponent(s))) }
        , format = function (s, c) { return s.replace(/{(\w+)}/g, function (m, p) { return c[p]; }) }
        return function (table, name, filename) {
            if (!table.nodeType) table = document.getElementById(table)
            var ctx = { worksheet: name || 'Worksheet', table: table.innerHTML }

            document.getElementById("dlink").href = uri + base64(format(template, ctx));
            document.getElementById("dlink").download = filename;
            document.getElementById("dlink").click();

        }
    })()

//]]>  
</script>


</body>
</html>
