<?php
include("includes/config.inc.php");
include("includes/function.php");
include("includes/globalarraylist.php");

$_objAdmin = new Admin();
$page_name="Attendance Report";

if( $_SESSION['userLoginType']==3){
		 
				$list=$_objAdmin->getDepotWiseSalesmanName($_SESSION['distributorId']);
				foreach($list as $value){
						$salValue[]=$value->salesman_id;
					}
					
				if(sizeof($salValue)>0){
					$salValue=implode(',',$salValue);
				//print_r($salValue);
				$depotSalCondition=" salesman_id IN (".$salValue.") and";
			}
			else {
					$depotSalCondition="";
				}
		 }	


if(isset($_POST['showReport']) && $_POST['showReport'] == 'yes')
{	
	if($_POST['sal']!="")  {
		$_SESSION['SalAttList']=$_POST['sal'];	
		$SalesmanVal=" and salesman_id=".$_SESSION['SalAttList'];
	} else {
		unset($_SESSION['SalAttList']);	
	}


	if($_POST['from']!="")  {
	
	 $_SESSION['FromAttList']=$_objAdmin->_changeDate($_POST['from']);
	 $fromDate=date('Y-m-d',strtotime($_SESSION['FromAttList']));

	}

	if($_POST['to']!="")  {
	     $_SESSION['ToAttList']=$_objAdmin->_changeDate($_POST['to']);
		 $toDate=date('Y-m-d',strtotime($_SESSION['ToAttList']));	
	}

	if($_POST['division_id']!="") 
	{
	$_SESSION['DivisionAttList']=$_POST['division_id'];	
	}


   if(isset($_POST['attendance'])){
   	if($_POST['attendance']=='all'){ 
   		$_SESSION['AttendanceStatus'] = ""; 
   	} else if($_POST['attendance']>0) { 
   		$_SESSION['AttendanceStatus']=$_POST['attendance'];
   	}
   }	
	
} else {
	$_SESSION['FromAttList']= $_objAdmin->_changeDate(date("Y-m-d"));
	$_SESSION['ToAttList']= $_objAdmin->_changeDate(date("Y-m-d"));	
}



if(isset($_REQUEST['reset']) && $_REQUEST['reset'] == 'yes')
{
	unset($_SESSION['SalAttList']);	
	unset($_SESSION['FromAttList']);	
	unset($_SESSION['ToAttList']);
	unset($_SESSION['AttendanceStatus']);
	unset($_SESSION['DivisionAttList']);
	header("Location: attendance_report.php");
}


$depotSalCondition="";
 $attendSalCondition="";  
 $notattendSalCondition="";

 if(isset($_SESSION['AttendanceStatus']) && $_SESSION['AttendanceStatus']==1){
     
     $attendSalesman = $_objAdmin->_getSelectList2('table_activity','salesman_id,activity_type,activity_date',''," activity_type in(11,12) and activity_date between '".$fromDate."' and '".$toDate."' $SalesmanVal GROUP BY salesman_id ORDER BY activity_id ");
     $during_salesman=array();

     foreach ($attendSalesman as $key => $value) {
     	$during_salesman[]=$value->salesman_id;
     }


    if(sizeof($during_salesman)>0){
     	$salValue=implode(',',$during_salesman);
		$attendSalCondition=" salesman_id IN (".$salValue.") and";
	} else{
     	$attendSalCondition="";
     }
     // ends for marked.
  } else if(isset($_SESSION['AttendanceStatus']) && $_SESSION['AttendanceStatus']==2){
  		$attend=0;
  	 $attendSalesman = $_objAdmin->_getSelectList2('table_activity','salesman_id,activity_type,activity_date',''," activity_type in(11,12) and activity_date between '".$fromDate."' and '".$toDate."' $SalesmanVal GROUP BY salesman_id ORDER BY activity_id ");
     $during_salesman=array();

     foreach ($attendSalesman as $key => $value) {
     	$during_salesman[]=$value->salesman_id;

     }  
      if($_SESSION['SalAttList']==""){
      	$duringSal=array_values(array_diff($salsList,$during_salesman)); 
      	if(sizeof($duringSal)>0)
      		{
      	$salValue=implode(',',$duringSal);
		$notattendSalCondition=" salesman_id IN (".$salValue.") and";
			}	
      	
        } else{

         $notattendSalCondition=" salesman_id IN (0) and";	

      }
      
      
  

   } 
 // for salesman division 
  if(isset($_SESSION['DivisionAttList']) && $_SESSION['DivisionAttList']!="")
  {
  	$divi=" s.division_id='".$_SESSION['DivisionAttList']."' and " ;
  }  



$salArr = $_objAdmin->_getSelectList('table_salesman as s left join table_division as d on s.division_id=d.division_id ','s.salesman_id,s.salesman_name,d.division_name',''," $depotSalCondition $attendSalCondition  $notattendSalCondition $divi s.status='A' ORDER BY salesman_name"); 
	if(is_array($salArr)){
		foreach($salArr as $value): 
			$salesmanList[$value->salesman_id] = array('salesman_id'=>$value->salesman_id, 'salesman_name'=>$value->salesman_name,'division_name'=>$value->division_name);
		endforeach;
	}
/*echo '<pre>';
print_r($salArr);*/

if($_SESSION['SalAttList']!=''){ 

	$sal_name = $salesmanList[$_SESSION['SalAttList']]['salesman_name']; 
	$salesmanCondition = " AND s.salesman_id=".$_SESSION['SalAttList']; 
	$salesmanList2 = array();
	$salesmanList2 = $salesmanList[$_SESSION['SalAttList']];
	$salesmanList  = array();
	$salesmanList[0] = $salesmanList2;
	//echo "<pre>"; print_r($salesmanList);
} else { $sal_name="All Salesman"; }

if($_SESSION['DivisionAttList']!=''){ 

    $divisionCond=" and s.division_id='".$_SESSION['DivisionAttList']."'";
}


?>

<?php



		/********************************************************************************
		* Description : Get the list of dates between selected date by user
		* Author : AJAY
		* Updated : 23 March 2015 (No changes made this function)
		*
		*
		**/


		function DateCheck($start_date,$end_date) {
			$start = strtotime($start_date);
			$end = strtotime($end_date);
			$new_date=array(date('Y-m-d', strtotime($start_date)));

			$date_arr=array();
			$days_count = ceil(abs($end - $start) / 86400)+1;
				for($i=0;$i<$days_count;$i++){
					
					$date = strtotime ('+1 days', strtotime($start_date)) ;
					$start_date = date ('Y-m-d' , $date);
					$date_arr[$i] = $start_date;
					$week_last_date=$start_date;
					array_push($new_date,$week_last_date);
					$get_newdate[]=array($new_date[$i],$new_date[$i+1]);
				}

			return $get_newdate;
		}	

		/********************************************************************************
		* Description : calculating working hours with the help of salesman start/end activity time(-20 minutes of lunch break)
		* Author : AJAY
		* Updated : 23 March 2015 (No changes made this function)
		*
		*
		**/

		function calculateWorkingHours ($attendance_start, $attendance_end) {

			if($attendance_start!='' && $attendance_end!='') {	
				$time1 = date('H:i:s', strtotime('+20 minutes', strtotime($attendance_start))); // start time + 20min
				$time2 = $attendance_end; //End Time
				list($hours, $minutes, $sec) = explode(':', $time1);
				$startTimestamp = mktime($hours, $minutes, $sec);
				list($hours, $minutes, $sec) = explode(':', $time2);
				$endTimestamp = mktime($hours, $minutes, $sec);
				$diff = $endTimestamp - $startTimestamp;

					if($diff > 0){
						$minutes = ($diff / 60) % 60;
						$hours = floor($diff / 3600);
						$seconds = $diff  % 60;
					} else {
						$minutes = ($diff / 60) % 60;
						$hours = ceil($diff / 3600);
						$seconds = abs($diff  % 60);
					}


					if($seconds<=9){ $seconds="0".$seconds; }
						$work_hours=$hours.":".abs($minutes).":".$seconds; 
					} else {
						$work_hours="-";
					}


				return $work_hours;

		}
?>
<?php include("header.inc.php") ?>
<script src="javascripts/dateNextPrev.js" type="text/javascript"></script>
<script type="text/javascript">

    function PrintElem(elem)
    {
        Popup($(elem).html());
    }

    function Popup(data) 
    {
		
        var mywindow = window.open('', 'Report');
		
        mywindow.document.write('<html><head><title>Attendance Report</title>');
		mywindow.document.write('<table><tr><td><b>Salesman Name:</b> <?php echo $sal_name; ?></td><td><b>From Date:</b> <?php echo $_SESSION['FromAttList']; ?></td><td><b>To Date:</b> <?php echo $_SESSION['ToAttList']; ?></td></tr></table>');
        /*optional stylesheet*/ //mywindow.document.write('<link rel="stylesheet" href="main.css" type="text/css" />');
        mywindow.document.write('</head><body >');
        mywindow.document.write(data);
        mywindow.document.write('</body></html>');

        mywindow.print();
        mywindow.close();
        return true;
    }


function showSalesmanDivision(str)
{
	//alert(str);
	var bracID=$('#branch_id').val();
	//alert(divID);
	$.ajax({
		'type': 'POST',
		'url': 'salesman_dropdown.php',
		'data': 'divisionID='+str,
		'success' : function(mystring) {
			//alert(mystring);
		document.getElementById("sal").innerHTML = mystring;
		$('#cityListID').hide();		
		}
	});
}   

</script>
<script type="text/javascript">
// Popup window code
function photoPopup(url) {
	popupWindow = window.open(
		url,'popUpWindow','height=500,width=800,left=100,top=100,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no,status=yes')
}
</script>

<style type="text/css">

.row0 {

	background-color: green;
	color: #FFF;
}

</style>

<!-- start content-outer -->
<input name="pagename" type="hidden"  id="pagename" value="admin_order_list.php" />
<div id="content-outer">
<!-- start content -->
<div id="content">
<div id="page-heading"><h1><span style="color: #d74343; font-family: Tahoma; font-weight: bold;">Attendance Report</span></h1></div>
<table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">
<tr>
	<td>
	<!--  start content-table-inner -->
	<div id="content-table-inner">
	
	<div id="page-heading" align="left" >
	<form name="frmPre" id="frmPre" method="post" action="" enctype="multipart/form-data" >
	<table border="0" width="100%" cellpadding="0" cellspacing="0">

	<tr>

		<td><h3>Division: </h3><h6>
		<select name="division_id" id="division_id" class="menulist" onChange="showSalesmanDivision(this.value)" style="" >
			<option value="">All</option>
			<?php $divisionList=$_objAdmin->_getSelectList('table_division',"division_id,division_name",''," order by division_name asc ");
			//print_r($branchList); 
			foreach($divisionList as $divisionVal){		
				
				if($_SESSION['DivisionAttList'] == $divisionVal->division_id){ $select="selected"; }else {$select="";}		
			?>
			<option value="<?php echo $divisionVal->division_id; ?>" <?php echo $select; ?>><?php echo $divisionVal->division_name; ?></option>
			<?php	
			}
			?>
		</select>
		</h6>
		</td>
		<td><h3>Salesman Name: </h3><h6>
		<select name="sal" id="sal" class="styledselect_form_5" style="" >
			<?php echo $rtndata = $_objArrayList->GetSalesmenMenu($salsList, $_SESSION['SalAttList']);?>
		</select>
		</h6>
		</td>

		<td><h3>Attendance: </h3><h6>
		<select name="attendance" id="attendance" class="styledselect_form_5" style="" >
			<option value="all">All</option>
			<option value="1" <?php if($_POST['attendance']=='1'){ echo 'selected';} ?>>Marked</option>
			<option value="2" <?php if($_POST['attendance']=='2'){ echo 'selected';} ?>>Not Marked</option>
		</select>
		</h6>
		</td>

		<td><h3>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;From Date: </h3><h6><img src="css/images/prev.png" height="18" width="18" onclick="dateFromPrev();"> <input type="text" id="from" name="from" class="date" style="width:150px" value="<?php  echo $_SESSION['FromAttList'];?>"  readonly /> <img src="css/images/next.png" height="18" width="18" onclick="dateFromNext();"></h6></td>


		<td><h3>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;To Date: </h3><h6><img src="css/images/prev.png" height="18" width="18" onclick="dateToPrev();"> <input type="text" id="to" name="to" class="date" style="width:150px" value="<?php echo $_SESSION['ToAttList']; ?>"  readonly /> <img src="css/images/next.png" height="18" width="18" onclick="dateToNext();"></h6></td>
        </tr>
        <tr>
		<td><h3></h3><input name="submit" class="result-submit" type="submit" id="submit" value="View Details" />
		<input type="button" value="Reset!" class="form-reset" onclick="location.href='attendance_report.php?reset=yes';" />
		</td>

		<td colspan="4"><input name="showReport" type="hidden" value="yes" />
		<input type="button" value="Print" class="result-submit" onclick="PrintElem('#Report')" />
		<a id="dlink"  style="display:none;"></a>
		<input input type="button" value="Export to Excel" class="result-submit"  onclick="tableToExcel('report_export', 'Attendance Report', 'Attendance Report.xls')">
		<!--<a href="attendance_report_year_graph.php?y=<?php echo checkFromdate($_SESSION['FromAttList']); ?>&salID=<?php echo $_SESSION['SalAttList'];?>" target="_blank"><input type="button" value="Show Graph" class="result-submit" /></a>-->
		</td>
		<td></td>
		</tr>
	</table>
	</form>
	</div>
	<div id="Report">
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
	<tr valign="top">
		<td>
		<table  border="0" width="100%" cellpadding="0" cellspacing="0" id="report_export" name="report_export">
				<div id="Report">
			<tr  bgcolor="#6E6E6E" style="color: #fff;font-weight: bold;" >
				<?php if($_SESSION['SalAttList']==''){ ?>
				<td style="padding:10px;" width="20%">Salesman Name</td>
				<?php } ?>
				<td style="padding:10px;" width="10%">Date</td>
				<td style="padding:10px;" width="10%">Day</td>
				<td style="padding:10px;" width="10%">Division</td> 
				<td style="padding:10px;" width="15%">In Address</td>
				<td style="padding:10px;" width="15%">In Time</td>
				<td style="padding:10px;" width="15%">Out Time</td>
				<td style="padding:10px;" width="15%">Out Address</td>
				<td style="padding:10px;" width="20%">Working Hours(-20 minutes)</td>
			</tr>



	<?php
			$auAttSt = array();
			$attendance_start  = "";
			$attendance_end = "";
			$activity_date = "";
			$start_activity_id = "";
			$end_activity_id = "";
			$start_latitude = "";
			$end_latitude = "";

		/*************************************************************************
		* Description : get the salesman IN,OUT Activity details with time gaps
		* Author : AJAY
		* Created : 20 March 2015
		*
		**/

		
		$auAttSt=$_objAdmin->_getSelectList2('table_activity AS A LEFT JOIN table_salesman AS s ON s.salesman_id = A.salesman_id left join table_division as d on d.division_id=s.division_id',"A.activity_id,A.activity_date,d.division_name, CASE WHEN A.activity_type=11 THEN A.start_time END AS start_time, CASE WHEN A.activity_type = 12 THEN A.start_time END AS end_time,CASE WHEN A.activity_type=11 THEN A.acc_address END AS in_address, CASE WHEN A.activity_type=12 THEN A.acc_address END AS out_address, A.lat,s.salesman_id, s.salesman_name, A.activity_type,A.acc_address",''," activity_type IN (11,12) ".$salesmanCondition." ".$divisionCond." AND (activity_date BETWEEN '".date('Y-m-d', strtotime($_SESSION['FromAttList']))."' AND '".date('Y-m-d', strtotime($_SESSION['ToAttList']))."')  AND s.account_id='".$_SESSION['accountId']."' GROUP BY salesman_id, activity_date, activity_type");



		foreach ($auAttSt as $key=>$value) :

			

			//$attendanceDataSet[$value->activity_date][$value->salesman_id]['salesman_id'] = $value->salesman_id;
			//$attendanceDataSet[$value->activity_date][$value->salesman_id]['salesman_name'] = $value->salesman_name;
			$attendanceDataSet[$value->activity_date][$value->salesman_id]['division_name'] = $value->division_name;

			if($value->activity_type == 11) {
				$attendanceDataSet[$value->activity_date][$value->salesman_id]['start_activity_id'] = $value->activity_id;
				$attendanceDataSet[$value->activity_date][$value->salesman_id]['attendance_start'] = $value->start_time;
				$attendanceDataSet[$value->activity_date][$value->salesman_id]['start_latitude'] = $value->lat;
				$attendanceDataSet[$value->activity_date][$value->salesman_id]['in_address'] = $value->in_address;


			} else if($value->activity_type == 12) {
				$attendanceDataSet[$value->activity_date][$value->salesman_id]['end_activity_id'] = $value->activity_id;
				$attendanceDataSet[$value->activity_date][$value->salesman_id]['attendance_end'] = $value->end_time;
				$attendanceDataSet[$value->activity_date][$value->salesman_id]['end_latitude'] = $value->lat;
				$attendanceDataSet[$value->activity_date][$value->salesman_id]['out_address'] = $value->out_address;
			}

			

		endforeach;

		//echo "<pre>";
		//print_r($attendanceDataSet);

		/*************************************************************************
		* Description : get the photolist of salesman ( IN,OUT/ Start,End) activity 
		* Author : AJAY
		* Created : 23 March 2015
		*
		**/


		$salesmanAttendanceActivityPhotos=$_objAdmin->_getSelectList('table_image AS s','image_id, ref_id, image_url',''," (date_app BETWEEN '".date('Y-m-d', strtotime($_SESSION['FromAttList']))."' AND '".date('Y-m-d', strtotime($_SESSION['ToAttList']))."') ".$salesmanCondition." AND image_type=6 "); 
		if(is_array($salesmanAttendanceActivityPhotos)){

			foreach($salesmanAttendanceActivityPhotos as $pkey=>$pValue) :

				$photoList[$pValue->ref_id][] = $pValue;

			endforeach;

		}
		
		
		$duration_dates = DateCheck($_SESSION['FromAttList'],$_SESSION['ToAttList']);
         
		/*echo "<pre>";
		print_r($duration_dates);*/
		
		//print_r($salesmanList);

		foreach($duration_dates as $value) :

			$activity_date = $value[0];

			foreach ($salesmanList as $sKey=>$sValue ) :
			
			$salesman_name = $sValue['salesman_name'];
			$salesman_id = $sValue['salesman_id'];

			if(is_array($attendanceDataSet) && isset($attendanceDataSet[$activity_date][$salesman_id])){

			$start_activity_id 	= $attendanceDataSet[$activity_date][$salesman_id]['start_activity_id'];	
			$attendance_start 	= $attendanceDataSet[$activity_date][$salesman_id]['attendance_start'];
			$start_latitude		= $attendanceDataSet[$activity_date][$salesman_id]['start_latitude'];
			$in_address			= $attendanceDataSet[$activity_date][$salesman_id]['in_address'];

			$end_activity_id	= $attendanceDataSet[$activity_date][$salesman_id]['end_activity_id'];
			$attendance_end		= $attendanceDataSet[$activity_date][$salesman_id]['attendance_end'];
			$end_latitude		= $attendanceDataSet[$activity_date][$salesman_id]['end_latitude'];
			$out_address		= $attendanceDataSet[$activity_date][$salesman_id]['out_address'];

			$division_name		= $attendanceDataSet[$activity_date][$salesman_id]['division_name'];

			

			

			?>

			<tr style="border-bottom:2px solid #6E6E6E;" class="row1">
			<?php if($_SESSION['SalAttList']==''){ ?> <td style="padding:10px;" width="20%"><?php echo $salesman_name;?> </td><?php }?>
			<td style="padding:10px;" width="10%"><?php echo $_objAdmin->_changeDate($activity_date);?> </td>
			<td style="padding:10px;" width="10%"><?php echo date("l",strtotime($activity_date));?></td>
			<td style="padding:10px;" width="10%"><?php echo  $division_name;?></td>
			<td style="padding:10px;" width="10%"><?php echo $in_address; ?></td>
			<?php if ($opRec[0]->view_activity_report=="Yes"){ ?>
			<td style="padding:10px;" width="10%">
				<a href="salesman_report.php?sal=<?php echo base64_encode($salesman_id);?>&date=<?php echo base64_encode($activity_date); ?>&act=<?php echo base64_encode($start_activity_id) ?>&act1=<?php echo base64_encode($end_activity_id) ?>#<?php echo base64_encode($start_activity_id) ?>" target="_blank">View</a>
			</td>
			<?php } ?>


				
			<td style="padding:10px;" width="10%">
			<?php  if($attendance_start!=''){ echo $attendance_start;
					if($start_latitude!=''){ ?>
						<a href="JavaScript:newPopup('activity_map.php?id=<?php echo base64_encode($start_activity_id);?>');">&nbsp;&nbsp;&nbsp;Map</a>
					<?php }?>

					<?php if(is_array($photoList[$start_activity_id]) && sizeof($photoList[$start_activity_id])>0){ ?>
						<a href="JavaScript:photoPopup('activity_photo.php?id=<?php echo base64_encode($photoList[$start_activity_id][0]->image_id);?>');">&nbsp;&nbsp;&nbsp;Image</a>
					<?php } 
				} else { echo "In Time Not Available"; }?>
			</td>



			<td style="padding:10px;" width="10%">
			<?php if($attendance_end!=''){ echo $attendance_end;
					if($end_latitude!=''){ ?>
						<a href="JavaScript:newPopup('activity_map.php?id=<?php echo base64_encode($end_activity_id);?>');">&nbsp;&nbsp;&nbsp;Map</a>
					<?php }?>

					<?php  if(is_array($photoList[$end_activity_id])){?>
						<a href="JavaScript:photoPopup('activity_photo.php?id=<?php echo base64_encode($photoList[$end_activity_id][0]->image_id) ;?>');">&nbsp;&nbsp;&nbsp;Image</a>
				<?php } } else { echo "Out Time Not Available"; } ?>
			</td>
			<td style="padding:10px;" width="10%"><?php echo $out_address; ?></td>

			<td style="padding:10px;" width="10%"><?php echo $work_hours = calculateWorkingHours($attendance_start, $attendance_end);?></td>
		</tr>

		<?php } else {

			if($_SESSION['AttendanceStatus']!="1"){ ?>
			<tr  style="border-bottom:2px solid #6E6E6E;">			
				<?php if($_SESSION['SalAttList']==''){ ?> <td style="padding:10px;" width="20%"><?php echo $salesman_name;?> </td> <?php } ?>
					<td style="padding:10px;" width="20%"><?php echo $_objAdmin->_changeDate($activity_date)?> </td>
					<td style="padding:10px;" width="20%"><?php echo date("l",strtotime($activity_date)); ?> </td>
					
					<td style="padding:10px;" width="20%"><?php echo $sValue['division_name']; ?></td>
					<td style="padding:10px;" width="20%">-</td>
					<td style="padding:10px;" width="20%">In Time Not Available</td>
					<td style="padding:10px;" width="20%">Out Time Not Available</td>
					<td style="padding:10px;" width="20%">-</td>
					<td style="padding:10px;" width="20%">-</td>
			</tr>
			<?php } ?>
			


		<?php } ?>
		<?php endforeach;?> <!-- salesman endforeach -->
	<?php endforeach;?> <!-- Duration endforeach -->
</div>
</table>
</td>
</tr>
<tr><td><img src="images/blank.gif" width="695" height="1" alt="blank" /></td><td></td></tr>
</table>
</div>
<div class="clear"></div>
</div>
<!--  end content-table-inner  -->
</td>
<td id="tbl-border-right"></td>
</tr>
<!--<tr>
	<th class="sized bottomleft"></th>
	<td id="tbl-border-bottom">&nbsp;</td>
	<th class="sized bottomright"></th>
</tr>-->
</table>
<div class="clear">&nbsp;</div>
</div>
<!--  end content -->
<div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->
<div class="clear">&nbsp;</div> 
<!-- start footer -->         
<?php include("footer.php") ?>
<?php //unset($_SESSION['SalAttList']);	?>
<!-- end footer -->
<script src="javascripts/jquery-1.8.2.js" type="text/javascript"></script>
<script src="javascripts/jquery-ui.js"></script>
<script type="text/javascript" src="javascripts/validate.js"></script>
<script type="text/javascript">
// Popup window code
function newPopup(url) {
	popupWindow = window.open(
		url,'popUpWindow','height=500,width=600,left=200,top=100,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no,status=yes')
}
</script>
<script>
    $(function() {
        $( "#from" ).datepicker({
			dateFormat: "d M yy",
            defaultDate: "w",
            changeMonth: true,
            numberOfMonths: 1,
            onSelect: function( selectedDate ) {
                $( "#to" ).datepicker( "option", "minDate", selectedDate );
            }
        });
        $( "#to" ).datepicker({
			dateFormat: "d M yy",
            defaultDate: "-w",
            changeMonth: true,
            numberOfMonths: 1,
            onSelect: function( selectedDate ) {
                $( "#from" ).datepicker( "option", "maxDate", selectedDate );
            }
        });
    });
</script>
<script type='text/javascript'>//<![CDATA[ 
var tableToExcel = (function () {
        var uri = 'data:application/vnd.ms-excel;base64,'
        , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table><tr><td><b>Salesman Name:</b> <?php echo $sal_name; ?></td><td><b>From Date:</b> <?php echo $_SESSION['FromAttList']; ?></td><td><b>To Date:</b> <?php echo $_SESSION['ToAttList']; ?></td></tr></table><table>{table}</table></body></html>'
        , base64 = function (s) { return window.btoa(unescape(encodeURIComponent(s))) }
        , format = function (s, c) { return s.replace(/{(\w+)}/g, function (m, p) { return c[p]; }) }
        return function (table, name, filename) {
            if (!table.nodeType) table = document.getElementById(table)
            var ctx = { worksheet: name || 'Worksheet', table: table.innerHTML }

            document.getElementById("dlink").href = uri + base64(format(template, ctx));
            document.getElementById("dlink").download = filename;
            document.getElementById("dlink").click();

        }
    })()

//]]>  
</script>
</body>
</html>
