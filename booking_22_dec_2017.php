<?php
include("includes/config.inc.php");
include("includes/function.php");
$page_name="Bookings";
$_objAdmin = new Admin();




//Made session for Booking status
if(isset($_POST['showBookingStatus']) && $_POST['showBookingStatus'] == 'yes'){	

	$_SESSION['bookingStatus']=$_POST['bookingStatus'];
	//header("Location: booking.php");
}
/***End booking status**/	
if(isset($_REQUEST['show']) && $_REQUEST['show']=="yes"){
    $_objAdmin->showBookings();
    die;
}


?>

<?php include("header.inc.php"); ?>

<div class="content-wrapper">
    <div class="container-fluid"> 
		<ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="#">Home</a>
        </li>
        <li class="breadcrumb-item active">Admin</li>
      </ol>
        
        
        
	  	<div class="row pdng-top-lg pdng-btm-lg">
                    
                    <div class="accordian">
                        <div class="panel-group">
	        <div class="col-lg-12 col-md-12 col-sm-12"> 
	          	<div id="content-table-inner" class="panel panel-default card_bg"> 
<div class="panel-heading">
              <h4 class="panel-title">Bookings</h4>
              <hr>
            </div>
<div class="panel-body">

<form name="submitStatusby" method="post" action="#" enctype="multipart/form-data" >
		<div class="row ">
	        <div class="col-lg-4 col-md-4 col-sm-12"> 
		<input type="radio" name="bookingStatus" value="" checked="checked" onchange="javascript:document.submitStatusby.submit()"> &nbsp;All&nbsp;&nbsp;
		</div>

	        <div class="col-lg-4 col-md-4 col-sm-12"> 
					<input type="radio" name="bookingStatus" value="AL" <?php if($_SESSION['bookingStatus']=='AL'){ ?>checked="checked" <?php } ?>  onchange="javascript:document.submitStatusby.submit()"> &nbsp;Alloted Booking&nbsp;&nbsp; 
		</div>

	        <div class="col-lg-4 col-md-4 col-sm-12"> 

					<input type="radio" name="bookingStatus" value="UA" <?php if($_SESSION['bookingStatus']=='UA'){ ?>checked="checked" <?php } ?> onchange="javascript:document.submitStatusby.submit()"  > &nbsp;Not Alloted Booking&nbsp;&nbsp; 
		</div>
 <div class="col-lg-12 col-md-12 col-sm-12"> 
 &nbsp;&nbsp;&nbsp; <input name="showBookingStatus" type="hidden" value="yes" />
</div>

		</div>
	<div class="row">
			<div class="col-lg-4 col-md-4 col-sm-12"> 

					<input type="radio" name="bookingStatus" value="CC" <?php if($_SESSION['bookingStatus']=='CC'){ ?>checked="checked" <?php } ?> onchange="javascript:document.submitStatusby.submit()"  > &nbsp;Completed Cleaning&nbsp;&nbsp; 
			</div>

	        <div class="col-lg-4 col-md-4 col-sm-12"> 
					<input type="radio" name="bookingStatus" value="CL" <?php if($_SESSION['bookingStatus']=='CL'){ ?>checked="checked" <?php } ?>  onchange="javascript:document.submitStatusby.submit()"> &nbsp;Canceled Cleaning&nbsp;&nbsp; 
			</div>

	        <div class="col-lg-4 col-md-4 col-sm-12"> 

					<input type="radio" name="bookingStatus" value="P" <?php if($_SESSION['bookingStatus']=='P'){ ?>checked="checked" <?php } ?> onchange="javascript:document.submitStatusby.submit()"  > &nbsp;Paid&nbsp;&nbsp; 
			</div>
			
			 <div class="col-lg-12 col-md-12 col-sm-12"> 
			 &nbsp;&nbsp;&nbsp;
			</div>
		 	
		</div>
		<div class="row">
			<div class="col-lg-4 col-md-4 col-sm-12"> 

					<input type="radio" name="bookingStatus" value="U" <?php if($_SESSION['bookingStatus']=='U'){ ?>checked="checked" <?php } ?> onchange="javascript:document.submitStatusby.submit()"  > &nbsp;Unpaid&nbsp;&nbsp; 
			</div>
		</div>
	</form>	
				
					 <div class="table-responsive table table-bordred table-striped">
								<!-- start id-form -->
								<table  border="0" cellspacing="0" cellpadding="0"  align="center" width="100%">
									<tr>
										<td align="lift" valign="top"  >
											<table id="flex1" style="display:none"></table>
											<script type="text/javascript">showBookings();</script>
										</td>
									</tr>
								</table>
							<!-- end id-form  -->
 					</div>

</div>
				</div>
			</div>
		</div>
                    </div>
                </div>
	</div>
</div>
	<div class="clear"></div> 
	<!-- start footer -->         
	<?php include("footer.php") ?>
	<!-- end footer -->
<!-- start content-outer -->
