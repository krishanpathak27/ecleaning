<?php
include("includes/config.inc.php");
include("includes/function.php");
$page_name="Bookings";
$_objAdmin = new Admin();




//Made session for Booking status
// if(isset($_POST['showBookingStatus']) && $_POST['showBookingStatus'] == 'yes'){	

// 	$_SESSION['bookingStatus']=$_POST['bookingStatus'];
// 	//header("Location: booking.php");
// }
/***End booking status**/	
if(isset($_POST['showBookingStatus']) && $_POST['showBookingStatus'] == 'yes'){  

 $_SESSION['bookingStatus']=$_POST['bookingStatus'];
 
}

if(isset($bookingStatus) && $bookingStatus != ""){
     $where .= " AND $bookingStatus"; 
}

if(isset($_POST['showBooking']) && $_POST['showBooking'] == 'yes'){	
	//echo "dd";die;
	
//print_r($_POST['from']);die;
	if($_POST['from']!=""){
   	 	$_SESSION['FromBookingList']=date('Y-m-d',strtotime($_POST['from']));  
  	}

  	if($_POST['to']!=""){
    	$_SESSION['ToBookingList']=date('Y-m-d',strtotime($_POST['to']));
  	}

}

if($_SESSION['FromBookingList']==''){
  $_SESSION['FromBookingList']= date('Y-m-d');
}
//echo $_SESSION['FromBookingList'];die;

if($_SESSION['ToBookingList']==''){
  $_SESSION['ToBookingList']= date('Y-m-d');
}

if(isset($_REQUEST['show']) && $_REQUEST['show']=="yes"){
    $_objAdmin->showBookings();
    die;
}
if(isset($_REQUEST['reset']) && $_REQUEST['reset'] == 'yes'){ 

  $_SESSION['FromBookingList']= date('Y-m-d');
  $_SESSION['ToBookingList']= date('Y-m-d');
  $_SESSION['bookingStatus']='';
  header("Location: booking.php");
}
?>

<?php include("header.inc.php"); ?>
<div class="m-grid__item m-grid__item--fluid m-wrapper" style="background-color:">
    <!-- BEGIN: Subheader -->
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="#" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__separator">
                        
                    </li>
                    <li class="m-nav__item">
                        <a href="" class="m-nav__link">
                            <span class="m-nav__link-text">
                               
                            </span>
                        </a>
                    </li>
                    <li class="m-nav__separator">
                        
                    </li>
                    <li class="m-nav__item">
                        <a href="" class="m-nav__link">
                            <span class="m-nav__link-text">
                                
                            </span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <!-- END: Subheader -->
    <div class="m-content">
        <?php 
            if (isset($_REQUEST['suc']) && $_REQUEST['suc'] != "") { ?>
            <div role="alert" style="background: #d7fbdc;" class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-dismissible m--margin-bottom-30">
                <div class="m-alert__icon" >
                        <i class="flaticon-exclamation m--font-brand"></i>
                </div>
                <div class="m-alert__text">
                       <?php echo  $_REQUEST['suc']; ?> 
                </div>
            </div>
            <?php } ?>
        <div class="m-portlet m-portlet--mobile">
            
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            Booking List
                        </h3>
                    </div>
                </div>
            </div>
            <div class="m-portlet__body">
							<div class="m-form__group form-group">	
            	<form class="m-form m-form--fit m-form--label-align-right" id="m_form_1"  method="post"  enctype="multipart/form-data" name="submitStatusby">							
								<div class="m-radio-inline">
									<label class="m-radio">
										<input type="radio" value="" name="bookingStatus" checked="checked" onchange="javascript:document.submitStatusby.submit()">
										All
										<span></span>
									</label>
									<label class="m-radio">
										<input type="radio" value="AL" name="bookingStatus" <?php if($_SESSION['bookingStatus']=='AL'){ ?>checked="checked" <?php } ?> onchange="javascript:document.submitStatusby.submit()">
										Alloted Booking
										<span></span>
									</label>
									<label class="m-radio">
										<input type="radio" value="UA" name="bookingStatus" <?php if($_SESSION['bookingStatus']=='UA'){ ?>checked="checked" <?php } ?> onchange="javascript:document.submitStatusby.submit()">
										Not Alloted Booking
										<span></span>
									</label>
									<label class="m-radio">
										<input type="radio" value="CC" name="bookingStatus" <?php if($_SESSION['bookingStatus']=='CC'){ ?>checked="checked" <?php } ?> onchange="javascript:document.submitStatusby.submit()">
										Completed Booking
										<span></span>
									</label>
									<label class="m-radio">
										<input type="radio" value="CL" name="bookingStatus" <?php if($_SESSION['bookingStatus']=='CL'){ ?>checked="checked" <?php } ?> onchange="javascript:document.submitStatusby.submit()">
										Canceled Booking
										<span></span>
									</label>
									<label class="m-radio">
										<input type="radio" value="P" name="bookingStatus" <?php if($_SESSION['bookingStatus']=='P'){ ?>checked="checked" <?php } ?> onchange="javascript:document.submitStatusby.submit()">
										Paid
										<span></span>
									</label>
									<label class="m-radio">
										<input type="radio" value="U" name="bookingStatus" <?php if($_SESSION['bookingStatus']=='U'){ ?>checked="checked" <?php } ?> onchange="javascript:document.submitStatusby.submit()">
										Unpaid
										<span></span>
									</label>

									<input name="showBookingStatus" type="hidden" value="yes" />
								</div>
								
                        </form>
								
							</div>


                        <form class="m-form m-form--fit m-form--label-align-right" name="frmPre" id="frmPre" method="post" action="" enctype="multipart/form-data">
                        	<div class="form-group m-form__group row">
		                        <div class="col-lg-2">													 
		                            <label>
		                                Date Picker
		                            </label> 													 
		                        </div>
		                        <div class="col-lg-6">
		                            <div class="m-input-icon m-input-icon--right">
		                                <div class="input-daterange input-group" id="m_datepicker_5">
		                                    <input type="text" class="form-control m-input" name="from" id="from" value="<?php echo date('m/d/Y',strtotime($_SESSION['FromBookingList']));?>">
		                                    <span class="input-group-addon">
		                                        <i class="la la-ellipsis-h"></i>
		                                    </span>
		                                    <input type="text" class="form-control" name="to" id="to" value="<?php echo date('m/d/Y',strtotime($_SESSION['ToBookingList']));?>">
		                                </div>

		                            </div>
		                        </div>
		                        <div class="col-lg-4 text-right"> 
		                            <div class="m-input-icon m-input-icon--right">
		                                <button type="submit" class="btn btn-success">
		                                    Submit
		                                </button>
		                                <button type="reset" class="btn btn-secondary">
		                                    Reset
		                                </button>
		                            </div> 
		                        </div> 
                    		</div>
                    		<input name="showBooking" type="hidden" value="yes"/>
                    	</form>

                <!--begin: Search Form -->
                <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
                    <div class="row align-items-center">
                        <div class="col-xl-8 order-2 order-xl-1">
                            <div class="form-group m-form__group row align-items-center">
                                <div class="col-md-4">
                                    <div class="m-input-icon m-input-icon--left">
                                        <input type="text" class="form-control m-input" placeholder="Search..." id="generalSearch">
                                        <span class="m-input-icon__icon m-input-icon__icon--left">
                                            <span>
                                                <i class="la la-search"></i>
                                            </span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
                <!--end: Search Form -->
                <!--begin: Datatable -->
                <div class="m_datatable1" id="booking_list"></div>
                <!--end: Datatable -->
            </div>
        </div>
    </div>
</div>
</div>
</div>
<!-- start footer -->         
<?php include("footer.php") ?>
<script type="text/javascript" >
    var DefaultDatatableDemo = function () {
        //== Private functions

        // basic demo
        var demo = function () {
            var datatable = $('.m_datatable1').mDatatable({
                data: {
                    type: 'remote',
                    source: {
                        read: {
                            url: '<?php echo $ajaxUrl . '?pageType=bookingList' ?>'
                        }
                    },
                    pageSize: 10,
                    serverPaging: true,
                    serverFiltering: true,
                    serverSorting: true
                },
                layout: {
                    theme: 'default',
                    class: '',
                    scroll: true,
                    //height: 550,
                    footer: false
                },
                sortable: true,
                filterable: false,
                pagination: true,
                search: {
                    input: $('#generalSearch')
                },
                columns: [{
                        field: "booking_id",
                        title: "#",
                        sortable: false,
                        width: 40,
                        selector: {class: 'm-checkbox--solid m-checkbox--brand'}
                    }, {
                        field: "bookingId",
                        title: "Booking Id",
                        sortable: false,
                    }, {
                        field: "customer_name",
                        title: "Customer Name",
                        sortable: 'asc',
                    }, {
                        field: "customer_number",
                        title: "Customer Number",
                        responsive: {visible: 'lg'}
                        
                    }, {
                        field: "total_cost_paid",
                        title: "Total Cost Paid",
                        responsive: {visible: 'lg'}
                        
                    }, {
                        field: "building_name",
                        title: "Building Name",
                        responsive: {visible: 'lg'}
                        
                    }, {
                        field: "customer_address",
                        title: "Customer Address",
                        responsive: {visible: 'lg'}
                      
                    }, {
                        field: "packg_start_date",
                        title: "Package Start Date",
                        responsive: {visible: 'lg'}
                       
                    }, {
                        field: "packg_end_date",
                        title: "Package End Date",
                        responsive: {visible: 'lg'}
                        
                    }, {
                        field: "room_detail",
                        title: "Room Detail",
                        responsive: {visible: 'lg'}
                       
                    }, {
                        field: "Actions",
                        width: 110,
                        title: "Actions",
                        sortable: false,
                        locked: {right: 'xl'},
                        overflow: 'visible',
                        template: function (row, index, datatable) {
                            var dropup = (datatable.getPageSize() - index) <= 4 ? 'dropup' : '';
                            return '\<a href="booking_details.php?id='+row.booking_id+'" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only edit m-btn--pill" title="Edit Details">\
                                                        <i class="la la-edit"></i>\
                                                </a>';
                        }
                    }]
            });
        };

        return {
            // public functions
            init: function () {
                demo();
            }
        };
    }();

    jQuery(document).ready(function () {
        DefaultDatatableDemo.init();
    });



</script>


