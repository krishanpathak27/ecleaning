<?php
include("includes/config.inc.php");
include("includes/function.php");
$_objAdmin = new Admin();

if (isset($_REQUEST['id']) && $_REQUEST['id'] != "") {
    $where = "br.booking_id='".$_REQUEST['id']."'";
    $table = "table_booking_register AS br  "
                ."LEFT JOIN table_customer AS cus ON cus.customer_id = br.customer_id "
                ."LEFT JOIN table_customer_address AS ca ON ca.customer_address_id = br.customer_address_id "
                 ."LEFT JOIN table_buildings AS b ON b.building_id = ca.building_id "
                 ."LEFT JOIN table_area as a on a.area_id=ca.area_id";
    $clms = "cus.*,b.building_name,ca.customer_address as address,a.area_name";
    $auRec = $_objAdmin->_getSelectList($table,$clms, '',$where,'');

    // echo "<pre>";
    // print_r($auRec);
    // exit;
    
    if (count($auRec) <= 0)
        header("Location: cleaner_feedback_details.php");
}
include("header.inc.php");
?>
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <!-- BEGIN: Subheader -->
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">
                    Customer Detail
                </h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="#" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__separator">
                        -
                    </li>
                    <li class="m-nav__item">
                        <a href="" class="m-nav__link">
                            <span class="m-nav__link-text">
                                Master
                            </span>
                        </a>
                    </li>
                    <li class="m-nav__separator">
                        -
                    </li>
                    <li class="m-nav__item">
                        <a href="" class="m-nav__link">
                            <span class="m-nav__link-text">
                                Customers
                            </span>
                        </a>
                    </li>  
                </ul>
            </div> 
        </div>
    </div>
    <!-- END: Subheader -->
    <div class="m-content">
        <div class="row">
            <div class="col-lg-12">
                <!--begin::Portlet--> 
                <div class="m-portlet">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <span class="m-portlet__head-icon m--hide">
                                    <i class="la la-gear"></i>
                                </span>
                                <h3 class="m-portlet__head-text">
                                    Customer Detail
                                </h3>
                            </div>
                        </div>
                    </div>
                    <!--begin::Form-->
                    <form class="m-form m-form--fit m-form--label-align-right">

                        <div class="m-portlet__body">
                            <div class="m-form__content">
                                <div class="m-alert m-alert--icon alert alert-danger m--hide" role="alert" id="m_form_1_msg">
                                    <div class="m-alert__icon">
                                        <i class="la la-warning"></i>
                                    </div>
                                    <div class="m-alert__text">
                                        Oh snap! Change a few things up and try submitting again.
                                    </div>
                                    <div class="m-alert__close">
                                        <button type="button" class="close" data-close="alert" aria-label="Close"></button>
                                    </div>
                                </div>
                            </div>
                            <?php if (isset($err) && !empty($err)) { ?>
                                <div class="m-form__content">
                                    <div class="m-alert m-alert--icon alert alert-danger" role="alert" id="m_form_1_msg1">
                                        <div class="m-alert__icon">
                                            <i class="la la-warning"></i>
                                        </div>
                                        <div class="m-alert__text">
                                            <?php echo $err; ?>
                                        </div>
                                        <div class="m-alert__close">
                                            <button type="button" class="close" data-close="alert" aria-label="Close"></button>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                            <div class="form-group m-form__group row">
                                <div class="col-lg-4">
                                    <label>
                                        Customer Name :
                                    </label>
                                    <div><?php echo $auRec[0]->customer_name;?></div>

                                </div>
                                <div class="col-lg-4">
                                    <label>
                                        Email:
                                    </label>
                                    <div><?php echo $auRec[0]->customer_email;?></div>

                                </div>
                                 <div class="col-lg-4">
                                    <label>
                                        Number:
                                    </label>
                                    <div><?php echo $auRec[0]->customer_number;?></div>

                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <div class="col-lg-4">
                                    <label>
                                        Address :
                                    </label>
                                    <div><?php echo $auRec[0]->address;?></div>

                                </div>
                                <div class="col-lg-4">
                                    <label>
                                        Building :
                                    </label>
                                    <div><?php echo $auRec[0]->building_name;?></div>

                                </div>
                                <div class="col-lg-4">
                                    <label>
                                        Area :
                                    </label>
                                    <div><?php echo $auRec[0]->area_name;?></div>

                                </div>
                            </div>
                        </div>
                        <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                            <div class="m-form__actions m-form__actions--solid">
                                <div class="row">
                                    <div class="col-lg-4"></div>
                                    <div class="col-lg-8">
                                        <a href="booking.php"  class="btn btn-secondary">
                                            Back
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </form>
                    <!--end::Form-->
                </div>
                <!--end::Portlet-->
            </div>
        </div>
    </div>
</div>
</div>
</div>
<!-- start footer -->         
<?php include("footer.php") ?>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBs3PXRGjSu0tH4_DpHCpZm8TgrE-kKJgw" type="text/javascript"></script>
<script src="javascripts/gmaps.js" type="text/javascript"></script>
<script type="text/javascript">
    
var GoogleMapsDemo = function() {

    //== Private functions
    var address = '<?php echo $address; ?>';
    address = JSON.parse(address);
    var lng = ""
    var lat = ""
    if(address) {
        if(address[0].lat) {
            var lat = address[0].lat;
        }
        if(address[0].lng) {
            var lng = address[0].lng;
        }
    }
    var demo3 = function() {
        var map = new GMaps({
            div: '#m_gmap_3',
            lat: lat,
            lng: lng,
        });
        
        
        jQuery.each(address, function() {
            if(this.building_name) {
                var building = this.building_name;
            } else {
                var building = "";
            }
            map.addMarker({
                lat: this.lat,
                lng: this.lng,
                title: this.customer_address+' '+building,
            
            });
        });
        map.setZoom(4);
    }


    return {
        // public functions
        init: function() {
            // default charts
            demo3();
        }
    };
}();

jQuery(document).ready(function() {
    GoogleMapsDemo.init();
});


</script>

