<?php
include("includes/config.inc.php");
include("includes/function.php");
$page_name = "Booking Details";
$_objAdmin = new Admin();
if(isset($_REQUEST['stid'])){
    
   //print_r($_REQUEST['cleaner_id']);die;
    $cid = $_objAdmin->_dbUpdate(array("booking_status" => $_REQUEST['booking_status'], "cleaner_id" => $_REQUEST['cleaner_id'],"last_update_date" => date('Y-m-d H:i:s')), 'table_booking_allotment', "booking_detail_id in(" . $_REQUEST['stid'] . ")");
    $_SESSION['order'] = 'updated';
    header("Location: booking_details.php?id=".$_REQUEST['id']."&suc=Status has been updated successfully");
}
include("header.inc.php");
?>

<div class="m-grid__item m-grid__item--fluid m-wrapper" style="background-color:">
    <!-- BEGIN: Subheader -->
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">
                    Booking Details
                </h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="#" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__separator">
                        -
                    </li>
                    <li class="m-nav__item">
                        <a href="#" class="m-nav__link">
                            <span class="m-nav__link-text">
                                Master
                            </span>
                        </a>
                    </li>
                    <li class="m-nav__separator">
                        -
                    </li>
                    <li class="m-nav__item">
                        <a href="" class="m-nav__link">
                            <span class="m-nav__link-text">
                                Booking Detail
                            </span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <!-- END: Subheader -->
    <div class="m-content">
       <?php 
            if(isset($_REQUEST['suc']) && $_REQUEST['suc'] != ""){ ?>
            <div role="alert" style="background: #d7fbdc;" class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-dismissible m--margin-bottom-30">
                <div class="m-alert__icon" >
                        <i class="flaticon-exclamation m--font-brand"></i>
                </div>
                <div class="m-alert__text">
                       <?php echo  $_REQUEST['suc']; ?> 
                </div>
            </div>
        <?php } ?>
        <div class="m-portlet m-portlet--mobile">

            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            Booking Details
                        </h3>
                    </div>
                </div>
            </div>
            <div class="m-portlet__body">
                <!--begin: Search Form -->
                <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
                    <div class="row align-items-center">
                        <div class="col-xl-8 order-2 order-xl-1">
                            <div class="form-group m-form__group row align-items-center">
                                <div class="col-md-4">
                                    <div class="m-input-icon m-input-icon--left">
                                        <input type="text" class="form-control m-input" placeholder="Search..." id="generalSearch">
                                        <span class="m-input-icon__icon m-input-icon__icon--left">
                                            <span>
                                                <i class="la la-search"></i>
                                            </span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
                <!--end: Search Form -->
                <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30 collapse" id="m_datatable_group_action_form">
                    <div class="row align-items-center">
                        <div class="col-xl-12">
                            <div class="m-form__group m-form__group--inline">
                                <div class="m-form__label m-form__label-no-wrap">
                                    <label class="m--font-bold m--font-danger-">
                                        Selected
                                        <span id="m_datatable_selected_number"></span>
                                        records:
                                    </label>
                                </div>
                                <div class="m-form__control">
                                    <div class="btn-toolbar">
                                    <input type="hidden" name="bookingId" id="bookingId" value="<?php echo $_REQUEST['id'];?>">
                                        <div class="input-group m-input-group m-input-group--square"> 
                                            <select class="form-control m-input m-input--square" id="cleaner_id" name="cleaner_id">
                                               <option value="">Select Cleaner</option>
                                               <?php $condi = "";
                                                    $branchList=$_objAdmin->_getSelectList('table_cleaner',"cleaner_id,cleaner_name",''," status='A'");
                                                
                                                    for($i=0;$i<count($branchList);$i++){
                      
                                                        if($branchList[$i]->cleaner_id==$auRec[0]->cleaner_id){$select="selected";} else {$select="";}
                        
                                                    ?>
                                                      <option value="<?php echo $branchList[$i]->cleaner_id; ?>" <?php echo $select ?>><?php echo $branchList[$i]->cleaner_name; ?></option>
                     
                                                  <?php } ?>
                                              </select>
                                        </div>
                                        &nbsp;&nbsp;&nbsp;
                                       <div class="input-group m-input-group m-input-group--square"> 
                                        <select class="form-control m-input m-input--square" id="booking_status" name="booking_status">
                                          <option value="UA" >Not Alloted</option>
                                          <option value="AL">Alloted</option>
                                          <option value="AC">Accepted</option>
                                          <option value="RJ">Rejected</option>
                                          <option value="RD">Reached</option>
                                          <option value="SC">Start Cleaning</option>
                                          <option value="CC">Complete Cleaning</option>
                                          <option value="RP">Payment Received</option>
                                          <option value="CB">Complete Booking</option>
                                          <option value="CL">Cancel Booking</option>
                                        </select>
                                    </div>
                                        &nbsp;&nbsp;&nbsp;
                                         
                                        <a  class="btn btn-sm btn-danger"  href="#"  id="m_update">
                                            Update
                                        </a>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--begin: Datatable -->
                <div class="bookingDt" id="bookingDetails"></div>
                <!--end: Datatable -->
            </div>
        </div>
    </div>
</div>
</div>
</div>


<!-- start footer -->         
<?php include("footer.php") ?>
<script type="text/javascript" >
    var DefaultDatatableDemo = function () {
        //== Private functions

        // basic demo
        var options = {
            data: {
                type: 'remote',
                source: {
                    read: {
                         url: '<?php echo $ajaxUrl . '?pageType=bookingDetailList&id='.$_REQUEST['id'] ?>'
                    }
                },
                pageSize: 20,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true
            },
            layout: {
                theme: 'default',
                class: '',
                scroll: true,
                height: 550,
                footer: false
            },
            sortable: true,
            filterable: false,
            pagination: true,
            search: {
                input: $('#generalSearch')
            },
            columns: [{
                    field: "booking_detail_id",
                    title: "#",
                    sortable: false,
                    width: 40,
                    selector: {class: 'm-checkbox--solid m-checkbox--brand'}
                },{
                        field: "customer_name",
                        title: "Customer Name",
                        width:80,
                        sortable: 'asc'
                        
                },{
                        field: "amount",
                        title: "Amount",
                        width: 80,
                        sortable: false,
                        responsive: {visible: 'lg'}
                },{
                        field: "cleaning_date",
                        title: "Cleaning Date",
                        width: 80,
                        responsive: {visible: 'lg'}
                }, {
                        field: "slot_start_time",
                        title: "Start Time",
                        width: 80,
                        responsive: {visible: 'lg'}
                    },{
                        field: "slot_end_time",
                        title: "End Time",
                        width: 80,
                        responsive: {visible: 'lg'}
                    },{
                        field: "cleaner_name",
                        title: "Cleaner Name",
                        //width: 150,
                        responsive: {visible: 'lg'}
                    },{
                        field: "supervisor_name",
                        title: "Supervisior Name",
                        width: 100,
                        responsive: {visible: 'lg'}
                    }, {
                        field: "booking_status",
                        title: "Booking Status",
                        width: 80,
                        responsive: {visible: 'lg'}
                    },
					{
                        field: "time_required",
                        title: "Time Required",
                        width: 80,
                        responsive: {visible: 'lg'}
                    },
					
					{
                        field: "Actions",
                        title: "Actions",
                        sortable: false,
                        locked: {right: 'xl'},
                        overflow: 'visible',
                        template: function (row, index, datatable) {
                            var dropup = (datatable.getPageSize() - index) <= 4 ? 'dropup' : '';
                             return '\<a href="add_booking.php?id='+row.booking_detail_id+'&time_required='+row.time_required+'&cus_name='+row.customer_name+'" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only edit m-btn--pill" title="Cleaner Allotment">\
                                                        <i class="la la-eye"></i>\
                                                </a>';
                        }
                    }]
        }

        var localSelectorDemo = function () {

            options.search = {
                input: $('#generalSearch'),
            };

            var datatable = $('#bookingDetails').mDatatable(options);

            // $('#m_form_status').on('change', function () {
            //     datatable.search($(this).val().toLowerCase(), 'Status');
            // });

            // $('#m_form_type').on('change', function () {
            //     datatable.search($(this).val().toLowerCase(), 'Type');
            // });

            $('#m_form_status,#m_form_type').selectpicker();

            datatable.on('m-datatable--on-check m-datatable--on-uncheck m-datatable--on-layout-updated', function(e){
                var checkedNodes = datatable.rows('.m-datatable__row--active').nodes();
                var count = checkedNodes.length;
                $('#m_datatable_selected_number').html(count / 2);
                if(count>0){
                    $('#m_datatable_group_action_form').collapse('show');
                } else {
                    $('#m_datatable_group_action_form').collapse('hide');
                }
            });

            $('#m_update').on('click',function(e){
                
                var id = '';
                var ids = datatable.rows('.m-datatable__row--active').
                        nodes().
                        find('.m-checkbox--single > [type="checkbox"]').
                        map(function (i, chk) {
                            return $(chk).val();
                        });
                for(var i=0;i < ids.length;i++){
                    if(i == 0){
                        id += ids[i];
                    } else {
                        id += ',' + ids[i];
                    }
                }
               var booking_id=$("#bookingId").val();
               
               var cleanerId= $("#cleaner_id option:selected").val();
               var bookingStatus= $("#booking_status option:selected").val();
               
               
                if(cleanerId==''){
                    alert('Please select cleaner');
                }
                else if(bookingStatus==''){
                    alert('Please select Booking Status');
                }else{
                    window.location.assign("booking_details.php?stid=" + id + "&id="+booking_id+ "&cleaner_id="+ cleanerId +"&booking_status="+bookingStatus);
                }
                

            })

        };
        return {
            // public functions
            init: function () {
                localSelectorDemo();
            }
        };
    }();

    jQuery(document).ready(function(){
        DefaultDatatableDemo.init();
    });


</script>


