<?php
include("includes/config.inc.php");
include("includes/function.php");
//include("includes/globalarraylist.php");
$page_name = "Booking Details";
include("header.inc.php");
?>
<div class="m-grid__item m-grid__item--fluid m-wrapper" style="background-color:">
    <!-- BEGIN: Subheader -->
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
            <h3 class="m-subheader__title m-subheader__title--separator">
                    Booking Details
                </h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="#" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__separator">
                        
                    </li>
                    <li class="m-nav__item">
                        <a href="" class="m-nav__link">
                            <span class="m-nav__link-text">
                               
                            </span>
                        </a>
                    </li>
                    <li class="m-nav__separator">
                        
                    </li>
                    <li class="m-nav__item">
                        <a href="" class="m-nav__link">
                            <span class="m-nav__link-text">
                              
                            </span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <!-- END: Subheader -->
    <div class="m-content">
        <?php 
            if(isset($_REQUEST['suc']) && $_REQUEST['suc'] != ""){ ?>
            <div role="alert" style="background: #d7fbdc;" class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-dismissible m--margin-bottom-30">
                <div class="m-alert__icon" >
                        <i class="flaticon-exclamation m--font-brand"></i>
                </div>
                <div class="m-alert__text">
                       <?php echo  $_REQUEST['suc']; ?> 
                </div>
            </div>
            <?php } ?>
        <div class="m-portlet m-portlet--mobile">
            
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            Booking Details
                        </h3>
                    </div>
                </div>
            </div>
            <div class="m-portlet__body">
                <!--begin: Search Form -->
                <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
                    <div class="row align-items-center">
                        <div class="col-xl-8 order-2 order-xl-1">
                            <div class="form-group m-form__group row align-items-center">
                                <div class="col-md-4">
                                    <div class="m-input-icon m-input-icon--left">
                                        <input type="text" class="form-control m-input" placeholder="Search..." id="generalSearch">
                                        <span class="m-input-icon__icon m-input-icon__icon--left">
                                            <span>
                                                <i class="la la-search"></i>
                                            </span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--end: Search Form -->
                <!--begin: Datatable -->
                <div class="m_datatable" id="booking_detail"></div>
                <!--end: Datatable -->
            </div>
        </div>
    </div>
</div>
</div>
</div>
<!-- start footer -->         
<?php include("footer.php") ?>
<script type="text/javascript" >
    var DefaultDatatableDemo = function () {
        //== Private functions

        // basic demo
        var demo = function () {
            var datatable = $('.m_datatable').mDatatable({
                data: {
                    type: 'remote',
                    source: {
                        read: {
                            url: '<?php echo $ajaxUrl . '?pageType=bookingDetailList&id='.$_REQUEST['id'] ?>'
                        }
                    },
                    pageSize: 10,
                    serverPaging: true,
                    serverFiltering: true,
                    serverSorting: true
                },
                layout:{
                    theme: 'default',
                    class: '',
                    scroll: true,
                    //height: 550,
                    footer: false
                },
                sortable: true,
                filterable: false,
                pagination: true,
                search: {
                    input: $('#generalSearch')
                },
                columns: [ {
                        field: "customer_name",
                        title: "Customer Name",
                        width:80,
                        sortable: 'asc'
                        
                    }, {
                        field: "amount",
                        title: "Amount",
                        width: 80,
                        sortable: false,
                        responsive: {visible: 'lg'}
                    }, {
                        field: "cleaning_date",
                        title: "Cleaning Date",
                        width: 80,
                        responsive: {visible: 'lg'}
                    }, {
                        field: "slot_start_time",
                        title: "Start Time",
                        width: 80,
                        responsive: {visible: 'lg'}
                    }, {
                        field: "slot_end_time",
                        title: "End Time",
                        width: 80,
                        responsive: {visible: 'lg'}
                    }, {
                        field: "cleaner_name",
                        title: "Cleaner Name",
                        //width: 150,
                        responsive: {visible: 'lg'}
                    }, {
                        field: "supervisor_name",
                        title: "Supervisior Name",
                        width: 100,
                        responsive: {visible: 'lg'}
                    }, {
                        field: "booking_status",
                        title: "Booking Status",
                        width: 80,
                        responsive: {visible: 'lg'}
                    },{
                        field: "Actions",
                        title: "Actions",
                        sortable: false,
                        locked: {right: 'xl'},
                        overflow: 'visible',
                        template: function (row, index, datatable) {
                            var dropup = (datatable.getPageSize() - index) <= 4 ? 'dropup' : '';
                             return '\<a href="add_booking.php?id='+row.booking_detail_id+'" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only edit m-btn--pill" title="Cleaner Allotment">\
                                                        <i class="la la-eye"></i>\
                                                </a>';
                        }
                    }]
            });
        };

        return {
            // public functions
            init: function () {
                demo();
            }
        };
    }();

    jQuery(document).ready(function () {
        DefaultDatatableDemo.init();
    });



</script>


