<?php
include("includes/config.inc.php");
include("includes/function.php");

$_objAdmin = new Admin();
$page_name="Booking Details";

// if(isset($_POST['export']) && $_POST['export']!=""){
//   header("Location:export.inc.php?export_salesman_attendance_report");
//   exit;
// }
$where=" br.booking_id=".$_REQUEST['id'];
// if(isset($_POST['showBookingStatus']) && $_POST['showBookingStatus'] == 'yes'){  

//  $_SESSION['bookingStatus']=$_POST['bookingStatus'];
 
// }
// if($_SESSION['bookingStatus']!=''){
//   if($_SESSION['bookingStatus']=='U'){//UNpaid check
//         $bookingStatus="ba.booking_status!='CB' AND tph.payment_mode='cash' ";
//   }else if($_SESSION['bookingStatus']=='P'){//Paid Check
//         $bookingStatus="ba.booking_status='CB' OR tph.payment_mode!='cash' ";
//   }else{
//    $bookingStatus="ba.booking_status='".$_SESSION['bookingStatus']."' ";
//  }
// //$bookingStatus="ba.booking_status='".$_SESSION['bookingStatus']."' ";
// }else{
//   $bookingStatus='';
// }
// if(isset($bookingStatus) && $bookingStatus != ""){
//      $where .= " AND $bookingStatus"; 
// }

$sort=" order by cleaning_date asc";
$select="table_booking_register AS br "
                ."LEFT JOIN table_booking_details AS bd ON br.booking_id = bd.booking_id "
                ."JOIN "
                ."("
                ."SELECT GROUP_CONCAT( CONCAT(brt.counter,' ',tct.room_category)) AS room_detail,brt.booking_id,ut.`unit_name` "
                ."FROM   table_booking_room_type_mapping AS brt "
                ."LEFT JOIN table_category_type AS tct ON tct.room_category_id=brt.room_category_id "
                ."LEFT JOIN table_unit_type AS ut ON ut.unit_type_id = tct.unit_type_id "
                ."GROUP BY brt.booking_id ) xx ON bd.booking_id = xx.booking_id "
                ."LEFT JOIN table_booking_allotment AS ba ON bd.booking_detail_id = ba.booking_detail_id "
                ."LEFT JOIN  table_calender_slots AS cs ON cs.slot_id = bd.slot_id "
               
                ."LEFT JOIN table_cleaner AS c ON c.cleaner_id = bd.cleaner_id "
                ."LEFT JOIN table_supervisor AS s ON s.supervisor_id = c.supervisor_id "
                ."LEFT JOIN table_customer AS cus ON cus.customer_id = br.customer_id "
                ."LEFT JOIN table_cleaning_type AS ct ON ct.cleaning_type_id = br.cleaning_type_id "
                ."LEFT JOIN table_customer_address AS ca ON ca.customer_address_id = br.customer_address_id "
                 ."LEFT JOIN table_buildings AS b ON b.building_id = ca.building_id "
                ."LEFT JOIN table_calender_slots AS cst ON cst.slot_id = bd.slot_id "
                ."LEFT JOIN table_reject_reason AS rs ON  rs.reject_reason_id = ba.reject_reason_id "
    ."LEFT JOIN table_payment_history AS tph ON  tph.payment_id=br.payment_id ";
    $fields = " br.booking_id, bd.booking_detail_id,cus.customer_name,br.cleaning_actual_cost,br.total_cost_paid,br.packg_start_date,br.packg_end_date,ca.customer_address,"
               ."b.building_name,ct.cleaning_type,cst.slot_end_time,cst.slot_start_time,bd.cleaning_date,bd.amount,c.cleaner_name,"
               ."s.supervisor_name,ba.booking_alloted,ba.booking_alloted_date,ba.not_alloted_reason,ba.booking_accepted,"
               ."ba.booking_accepted_date,ba.other_rejection_reason,rs.reject_reason,ba.booking_status,br.status,"
               ."xx.room_detail,xx.unit_name";
    $auRec=$_objAdmin->_getSelectList($select,$fields,$rp,$where,'');
//print_r($auRec);die;

?>

<?php include("header.inc.php"); ?>

 <div class="content-wrapper">
    <div class="container-fluid">
    <div class="pg_header">
       <div class="container">Booking Details</div>
    </div>
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="index.php">Home</a> </li>
     <?php $breadCumArray=(array_reverse(getBreadCrum('booking.php',-1)));if(!empty($breadCumArray)){foreach($breadCumArray as $breadCum){?>
      <li class="breadcrumb-item active"><?php  echo $breadCum;?></li>
     <?php }}else{?>
      <li class="breadcrumb-item active">Admin</li>
    <?php }?>
  </ol>
  <div class="row pdng-top-lg pdng-btm-lg">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <div id="content-table-inner" class="panel panel-default card_bg">
        <div id="page-heading" align="left" >
         <!--  <form name="submitStatusby" method="post" action="#" enctype="multipart/form-data" >
            <div class="row ">
                <div class="col-lg-4 col-md-4 col-sm-12"> 
                  <input type="radio" name="bookingStatus" value="" checked="checked" onchange="javascript:document.submitStatusby.submit()"> &nbsp;All&nbsp;&nbsp;
                </div>

                <div class="col-lg-4 col-md-4 col-sm-12"> 
                  <input type="radio" name="bookingStatus" value="AL" <?php if($_SESSION['bookingStatus']=='AL'){ ?>checked="checked" <?php } ?>  onchange="javascript:document.submitStatusby.submit()"> &nbsp;Alloted Booking&nbsp;&nbsp; 
                </div>

                <div class="col-lg-4 col-md-4 col-sm-12"> 

                  <input type="radio" name="bookingStatus" value="UA" <?php if($_SESSION['bookingStatus']=='UA'){ ?>checked="checked" <?php } ?> onchange="javascript:document.submitStatusby.submit()"  > &nbsp;Not Alloted Booking&nbsp;&nbsp; 
                </div>

      
               <div class="col-lg-12 col-md-12 col-sm-12"> 
               &nbsp;&nbsp;&nbsp; <input name="showBookingStatus" type="hidden" value="yes" />
              </div>

            </div> 

            <div class="row">
              <div class="col-lg-4 col-md-4 col-sm-12"> 

                  <input type="radio" name="bookingStatus" value="CC" <?php if($_SESSION['bookingStatus']=='CC'){ ?>checked="checked" <?php } ?> onchange="javascript:document.submitStatusby.submit()"  > &nbsp;Completed Booking&nbsp;&nbsp; 
              </div>

                  <div class="col-lg-4 col-md-4 col-sm-12"> 
                  <input type="radio" name="bookingStatus" value="CL" <?php if($_SESSION['bookingStatus']=='CL'){ ?>checked="checked" <?php } ?>  onchange="javascript:document.submitStatusby.submit()"> &nbsp;Canceled Booking&nbsp;&nbsp; 
              </div>

                  <div class="col-lg-4 col-md-4 col-sm-12"> 

                  <input type="radio" name="bookingStatus" value="P" <?php if($_SESSION['bookingStatus']=='P'){ ?>checked="checked" <?php } ?> onchange="javascript:document.submitStatusby.submit()"  > &nbsp;Paid&nbsp;&nbsp; 
              </div>
              
               <div class="col-lg-12 col-md-12 col-sm-12"> 
               &nbsp;&nbsp;&nbsp;
              </div>
              
            </div>
            <div class="row mrgn-btm-sm">
              <div class="col-lg-4 col-md-4 col-sm-12"> 

                  <input type="radio" name="bookingStatus" value="U" <?php if($_SESSION['bookingStatus']=='U'){ ?>checked="checked" <?php } ?> onchange="javascript:document.submitStatusby.submit()"  > &nbsp;Unpaid&nbsp;&nbsp; 
              </div>
              <div class="col-lg-8 col-md-8 col-sm-12"> 
                <input type="button" value="Back" class="form-reset btn bg-info float-right" onclick="location.href='booking.php';" />
              </div>
            </div>
          
          
  </form>  -->
          <div class="row mrgn-btm-sm">
              <div class="col-lg-4 col-md-4 col-sm-12"> 
              </div>
              <div class="col-lg-8 col-md-8 col-sm-12"> 
                <input type="button" value="Back" class="form-reset btn bg-info float-right" onclick="location.href='booking.php';" />
              </div>
          </div>
        </div>
      
        <div class="panel-body" id="Report">   
        <table  width="100%" id="report_export" name="report_export" class="table table-bordred table-striped">
           
          <thead>
            <tr style="font-weight:500 !important;">

              <th>Booking No</th>
              <th>Customer Name</th>
              <!--<th>Cleaning Actual Cost</th>-->
              <th>Amount</th>
              <th>Cleaning Date</th>
              <th>Slot Start Time</th>
              <th>Cleaner Name</th>
              <th>Supervisior Name</th>
              <th>Booking Status</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>

            <?php  if(is_array($auRec)){for($i=0;$i<count($auRec);$i++){?>
            <?php switch($auRec[$i]->booking_status){
                case ("UA"):
                    $bookingStatus = "Not Alloted";
                    break;
                case ("AL"):
                    $bookingStatus = "Alloted";
                    break;
                case ("AC"):
                    $bookingStatus = "Accepted";
                    break;
                case ("RJ"):
                    $bookingStatus = "Rejected";
                    break;
                case ("RD"):
                    $bookingStatus = "Reached";
                    break;
                case ("SC"):
                    $bookingStatus = "Start Cleaning";
                    break;
                case ("CC"):
                    $bookingStatus = "Completed Cleaning";
                    break;
                case ("RP"):
                    $bookingStatus = "Payment Received";
                    break;
                case ("CB"):
                    $bookingStatus = "Complete Booking";
                    break;
                case ("CL"):
                    $bookingStatus = "Cancel";
                    break;
                default:
                    $bookingStatus = "Not Alloted";
                    break;
                
            }?>

              <tr>
                <td><?php echo $auRec[$i]->booking_id;?> </td>
                <td><?php echo $auRec[$i]->customer_name;?> </td>
                <!--<td><?php echo $auRec[$i]->cleaning_actual_cost;?> </td>-->
                <td><?php echo $auRec[$i]->amount;?> </td>
                <td><?php echo $auRec[$i]->slot_start_time;?> </td>
                <td><?php echo $auRec[$i]->slot_end_time;?> </td>
                <td><?php echo $auRec[$i]->cleaner_name;?> </td>
                <td><?php echo $auRec[$i]->supervisor_name;?> </td>
                <td><?php echo $bookingStatus;?> </td>
                <?php if($auRec[$i]->booking_detail_id!=''){?>
                <td><a href="add_booking.php?id=<?php echo $auRec[$i]->booking_detail_id;?>">EDIT</a></td>
                <?php }else{?>
                 <td><a href="javascript:void();">EDIT</a></td>
                <?php }?>
              </tr>

            <?php }}else{?>
            <tr>

              <td colspan="10" align="center">No Booking Available</td>
            </tr>
            <?php }?>
          </tbody>
        </tbody> 
    </table>
 
        </div>
       
      </div>
    </div>
  </div>
       
<?php include("footer.php") ?>
</div>
