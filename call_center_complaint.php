<?php
include("includes/config.inc.php");
include("includes/function.php");
$_objAdmin = new Admin();




if(isset($_REQUEST['exportBCF']) && $_REQUEST['exportBCF']=='Export BCF Dump')
{
	header("Location:export.inc.php?export_call_center_complaints_bcf_dump");
	exit;
}

if(isset($_REQUEST['submit']) && $_REQUEST['submit']!="")
 {

if($_POST['from']!="") 
	{
	$_SESSION['FromDate']=$_objAdmin->_changeDate($_POST['from']);	
	//print_R($_SESSION['FromDate']);

	}
	if($_POST['to']!="") 
	{
	$_SESSION['ToDate']=$_objAdmin->_changeDate($_POST['to']);	
	//print_R($_SESSION['ToDate']);
	}




}else {
	
	//$_SESSION['FromDate']= $_objAdmin->_changeDate(date("Y-m-d"));
	//$_SESSION['ToDate']= $_objAdmin->_changeDate(date("Y-m-d"));
	

}

if(isset($_REQUEST['reset']) && $_REQUEST['reset'] == 'yes')
{
	unset($_SESSION['FromDate']);	
	unset($_SESSION['ToDate']);
	header("Location: call_center_complaint.php");
}

// print_r($_SESSION);
// if(isset($_REQUEST['id']) && $_REQUEST['value']!="")
// {
// 	if($_REQUEST['value']=="Active")
// 	{	
// 		$status='I';
// 	}
// 	else
// 	{
// 		$status='A';
// 	}
// 	$cid=$_objAdmin->_dbUpdate(array("status"=>$status),'table_company_hierarchy', " hierarchy_id='".$_REQUEST['id']."'");
// 	//$Typeid=$_objAdmin->_dbUpdate(array("status"=>$status),'table_user_type', " hierarchy_id='".$_REQUEST['id']."'");
// 	header("Location: company_hierarchy.php");
// }	




// if($_REQUEST['id']!="" && $_REQUEST['delete']="yes" )
// {

//         mysql_query("DELETE FROM table_company_hierarchy WHERE hierarchy_id='".$_REQUEST['id']."'");
// 		mysql_query("DELETE FROM table_user_type WHERE hierarchy_id='".$_REQUEST['id']."'AND parentType ='2'");
//            header("Location: company_hierarchy.php");
// }







if(isset($_REQUEST['show']) && $_REQUEST['show']=="yes")
{
	$_objAdmin->callCenterComplaint();
	die;
}	

?>

<?php include("header.inc.php") ?>
 
 <style>
.sortable		{ padding:0; }
.sortable li	{ padding:4px 8px; color:#000; cursor:move; list-style:none; width:350px; background:#ddd; margin:10px 0; border:1px solid #999; }
#message-box		{ background:#fffea1; border:2px solid #fc0; padding:4px 8px; margin:0 0 14px 0; width:350px; }

h3
{
font-family:Arial,Helvetica,sans-serif !important;
font-size: 14px !important;
font-weight:normal !important;
}
</style>

<script type="text/javascript" src="javascripts/select_dropdowns.js"></script>
<!-- start content-outer -->
<div id="content-outer">
<!-- start content -->
<div id="content">
<div id="page-heading"><h1><span style="color: #d74343; font-family: Tahoma; font-weight: bold;"> Complaint List</span></h1></div>
<table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">
<!--<tr>
	<th rowspan="3" class="sized"><img src="images/side_shadowleft.jpg" width="20" height="300" alt="" /></th>
	<th class="topleft"></th>
	<td id="tbl-border-top">&nbsp;</td>
	<th class="topright"></th>
	<th rowspan="3" class="sized"><img src="images/side_shadowright.jpg" width="20" height="300" alt="" /></th>
</tr>-->
<tr>
	<td id="tbl-border-left"></td>
	<td>
	<!--  start content-table-inner -->
	<div id="content-table-inner">
	
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
	<tr valign="top">
	<td>
		<?php 

			if($_SESSION['userLoginType']==1)
			{
				$total_complaints = $_objAdmin->_getSelectList2('table_complaint','count(*) as total_complaints','',' call_center_id > 0');
				$pending_complaints = $_objAdmin->_getSelectList2('table_complaint','count(*) as pending','',' call_center_id > 0 and complain_status="P"');
				$inprocess_complaints = $_objAdmin->_getSelectList2('table_complaint','count(*) as inprocess','',' call_center_id > 0 and complain_status="I"');
				$completed_complaints = $_objAdmin->_getSelectList2('table_complaint','count(*) as completed','',' call_center_id > 0 and complain_status="C"');
				$rejected_complaints = $_objAdmin->_getSelectList2('table_complaint','count(*) as rejected','',' call_center_id > 0 and complain_status="R"');
				$tat1_complaints = $_objAdmin->_getSelectList2('table_complaint','SUM(cctat1) as sum_tat1','',' call_center_id > 0 and complain_status="I"');
				$tat2_complaints = $_objAdmin->_getSelectList2('table_complaint','SUM(cctat2) as sum_tat2','',' call_center_id > 0 and complain_status="I"');
		?>
			<table border="0" width="100%" cellpadding="0" cellspacing="0"> 
	      		<tr>      
	        		<td><h3>Total Complaints : <?php echo $total_complaints[0]->total_complaints;?></h3></td>
	        		<td><h3>Pending Complaints : <?php echo $pending_complaints[0]->pending;?></h3></td>
	        		<td><h3>In-Process Complaints : <?php echo $inprocess_complaints[0]->inprocess;?></h3></td>
	      		</tr>
	      		<tr>
	      			<td><h3>Completed Complaints : <?php echo $completed_complaints[0]->completed;?></h3></td>
	      			<td><h3>Rejected Complaints : <?php echo $rejected_complaints[0]->rejected;?></h3></td>
	      		</tr>
	      		<tr>
	      			<td colspan="3"><h3>Avg. TAT1 (time from Raising of Complaint to Reaching of SP) : <?php echo $tat1_complaints[0]->sum_tat1/$total_complaints[0]->total_complaints;?></h3></td>
	      		</tr>
	      		<tr>
	      			<td colspan="3"><h3>Avg. TAT2 (time from Raising of Complaint to Submitting BCF) : <?php echo $tat2_complaints[0]->sum_tat2/$total_complaints[0]->total_complaints;?></h3></td>
	      		</tr>
	      		<form name="myform" id="myform" method="post" action="" enctype="multipart/form-data" >
		<table border="0" width="85%" cellpadding="0" cellspacing="0">	
			<tr>
			<!-- <td><h3>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;From Date:</h3><h6><img src="css/images/prev.png" height="18" width="18" onclick="dateFromPrev();"> <input type="text" id="from" name="from" class="date" style="width:150px" value="<?php if($_SESSION['FromDate']!='') { echo $_objAdmin->_changeDate($_SESSION['FromDate']); } else { echo $_objAdmin->_changeDate(date('Y-m-d')); }?>"  readonly /> <img src="css/images/next.png" height="18" width="18" onclick="dateFromNext();"></h6></td>
	  <td><h3>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;To Date:</h3><h6><img src="css/images/prev.png" height="18" width="18" onclick="dateToPrev();"> <input type="text" id="to" name="to" class="date" style="width:150px" value="<?php if($_SESSION['ToDate']!='') { echo $_objAdmin->_changeDate($_SESSION['ToDate']); } else { echo $_objAdmin->_changeDate(date('Y-m-d')); }?>"  readonly /> <img src="css/images/next.png" height="18" width="18" onclick="dateToNext();"></h6></td>
	<td>  <input type="button" value="Reset!" class="form-reset" onclick="location.href='call_center_complaint.php?reset=yes';" /></td>
            <td><input name="submit" class="result-submit" type="submit" id="submit" value="View Details" /></td> -->
            <td><input name="exportBCF" class="result-submit" type="submit" id="exportBCF" value="Export BCF Dump" /></td>
		</tr>		
		</table>
	</form>
	    	</table>
	    <?php
	    	}
	    ?>
	    <?php 
			if($_SESSION['userLoginType']==2)
			{
				// $total_complaints = $_objAdmin->_getSelectList2('table_complaint','count(*) as total_complaints','',' call_center_id ="'.$_SESSION['operatorId'].'"');
				// $pending_complaints = $_objAdmin->_getSelectList2('table_complaint','count(*) as pending','',' call_center_id ="'.$_SESSION['operatorId'].'" and complain_status="P"');
				// $inprocess_complaints = $_objAdmin->_getSelectList2('table_complaint','count(*) as inprocess','',' call_center_id ="'.$_SESSION['operatorId'].'" and complain_status="I"');
				// $completed_complaints = $_objAdmin->_getSelectList2('table_complaint','count(*) as completed','',' call_center_id ="'.$_SESSION['operatorId'].'" and complain_status="C"');
				// $rejected_complaints = $_objAdmin->_getSelectList2('table_complaint','count(*) as rejected','',' call_center_id ="'.$_SESSION['operatorId'].'" and complain_status="R"');
				// $tat1_complaints = $_objAdmin->_getSelectList2('table_complaint','SUM(cctat1) as sum_tat1','',' call_center_id ="'.$_SESSION['operatorId'].'" and complain_status="I"');
				// $tat2_complaints = $_objAdmin->_getSelectList2('table_complaint','SUM(cctat2) as sum_tat2','',' call_center_id ="'.$_SESSION['operatorId'].'" and complain_status="I"');		// In case if wants to show only that call center complaints


				$total_complaints = $_objAdmin->_getSelectList2('table_complaint','count(*) as total_complaints','',' call_center_id > 0');
				$unapproved_complaints = $_objAdmin->_getSelectList2('table_complaint as COMPLAINT left join table_service_distributor_sp_bcf as BCF on COMPLAINT.complaint_id = BCF.complaint_id','count(*) as unapproved','',' call_center_id > 0 and BCF.under_process ="U" and BCF.status="R" and COMPLAINT.complain_status="R"');
				$approved_complaints = $_objAdmin->_getSelectList2('table_complaint as COMPLAINT left join table_service_distributor_sp_bcf as BCF on COMPLAINT.complaint_id = BCF.complaint_id','count(*) as approved','',' call_center_id > 0 and BCF.under_process="T" and BCF.status="A"');
				$Completed_by_SE = $_objAdmin->_getSelectList2('table_complaint as COMPLAINT left join table_service_distributor_sp_bcf as BCF on COMPLAINT.complaint_id = BCF.complaint_id','count(*) as completed','',' call_center_id > 0 and BCF.under_process="C" and BCF.status="C"');
				$initial_inspection = $_objAdmin->_getSelectList2('table_complaint as COMPLAINT left join table_service_distributor_sp_bcf as BCF on COMPLAINT.complaint_id = BCF.complaint_id','count(*) as initiated','',' call_center_id > 0 and BCF.under_process="I" and BCF.status="P"');
				$final_inspection = $_objAdmin->_getSelectList2('table_complaint as COMPLAINT left join table_service_distributor_sp_bcf as BCF on COMPLAINT.complaint_id = BCF.complaint_id','count(*) as finalised','',' call_center_id > 0 and BCF.under_process="F" and BCF.status="P"');
				// $pending_at_SE = $_objAdmin->_getSelectList2('table_complaint as COMPLAINT left join table_service_distributor_sp_bcf as BCF on COMPLAINT.complaint_id = BCF.complaint_id','count(*) as pending,COMPLAINT.assigned_sp_time,BCF.service_distributor_sp_bcf_id','',' call_center_id > 0 and COMPLAINT.assigned_sp_time !="0000-00-00 00:00:00" and BCF.service_distributor_sp_bcf_id=NULL and COMPLAINT.complain_status="P"');
				$new_complaints = $_objAdmin->_getSelectList2('table_complaint as COMPLAINT','count(*) as new','',' call_center_id > 0 and COMPLAINT.service_distributor_sp_id <=0 and COMPLAINT.complain_status="P"');
				$tat1_complaints = $_objAdmin->_getSelectList2('table_complaint','SUM(cctat1) as sum_tat1','',' call_center_id > 0 and complain_status="I"');
				$tat2_complaints = $_objAdmin->_getSelectList2('table_complaint','SUM(cctat2) as sum_tat2','',' call_center_id > 0 and complain_status="I"');
		?>
			<table border="0" width="100%" cellpadding="0" cellspacing="0"> 
	      		<tr>      
	        		<td><h3>Total Complaints : <?php echo $total_complaints[0]->total_complaints;?></h3></td>
	        		<td><h3>Unapproved Complaints : <?php echo $unapproved_complaints[0]->unapproved;?></h3></td>
	        		<td><h3>Approved Complaints : <?php echo $approved_complaints[0]->approved;?></h3></td>
	      		</tr>
	      		<tr>
	      			<td><h3>Completed Complaints SE : <?php echo $Completed_by_SE[0]->completed;?></h3></td>
	      			<td><h3>Initial Inspected : <?php echo $initial_inspection[0]->initiated;?></h3></td>
	      			<td><h3>Final Inspected : <?php echo $final_inspection[0]->finalised;?></h3></td>
	      		</tr>
	      		<tr>
	      			<!-- <td><h3>Pending at SE : <?php echo $pending_at_SE[0]->pending;?></h3></td> -->
	      			<td><h3>New Complaints : <?php echo $new_complaints[0]->new;?></h3></td>
	      		</tr>

	      		<tr>
	      			<td colspan="3"><h3>Avg. TAT1 (time from Raising of Complaint to Reaching of SP) : <?php echo $tat1_complaints[0]->sum_tat1/$total_complaints[0]->total_complaints;?></h3></td>
	      		</tr>
	      		<tr>
	      			<td colspan="3"><h3>Avg. TAT2 (time from Raising of Complaint to Submitting BCF) : <?php echo $tat2_complaints[0]->sum_tat2/$total_complaints[0]->total_complaints;?></h3></td>
	      		</tr>


                <!-- <form name="myform" id="myform" method="post" action="" enctype="multipart/form-data" >
		<table border="0" width="85%" cellpadding="0" cellspacing="0">	
			<tr><td><h3>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;From Date:</h3><h6><img src="css/images/prev.png" height="18" width="18" onclick="dateFromPrev();"> <input type="text" id="from" name="from" class="date" style="width:150px" value="<?php if($_SESSION['FromDate']!='') { echo $_objAdmin->_changeDate($_SESSION['FromDate']); } else { echo $_objAdmin->_changeDate(date('Y-m-d')); }?>"  readonly /> <img src="css/images/next.png" height="18" width="18" onclick="dateFromNext();"></h6></td>
	  <td><h3>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;To Date:</h3><h6><img src="css/images/prev.png" height="18" width="18" onclick="dateToPrev();"> <input type="text" id="to" name="to" class="date" style="width:150px" value="<?php if($_SESSION['ToDate']!='') { echo $_objAdmin->_changeDate($_SESSION['ToDate']); } else { echo $_objAdmin->_changeDate(date('Y-m-d')); }?>"  readonly /> <img src="css/images/next.png" height="18" width="18" onclick="dateToNext();"></h6></td>
	<td>  <input type="button" value="Reset!" class="form-reset" onclick="location.href='call_center_complaint.php?reset=yes';" /></td>
            <td><input name="submit" class="result-submit" type="submit" id="submit" value="View Details" /></td>

		</tr>		
		</table>
	</form> -->









	    	</table>
	    <?php
	    	}
	    ?>
		<!-- start id-form -->
		<table  border="0" cellspacing="0" cellpadding="0"  align="center" width="100%">
				<tr>
				<td align="lift" valign="top"  >
				<table id="flex1" style="display:none"></table>
					<script type="text/javascript">callCenterComplaint();</script>
				</td>
				</tr>
				</table>
	<!-- end id-form  -->

	</td>
	<td>

	<!-- right bar-->
	<?php
		// if($_SESSION['userLoginType']==1) 
			include("rightbar/call_center_complaint_bar.php");
	?>
</td>
</tr>
<tr>
<td></td>
<td></td>
</tr>
</table>
<div class="clear"></div>
</div>
<!--  end content-table-inner  -->
</td>
<td id="tbl-border-right"></td>
</tr>
<tr>
	<th class="sized bottomleft"></th>
	<td id="tbl-border-bottom">&nbsp;</td>
	<th class="sized bottomright"></th>
</tr>
</table>
<div class="clear">&nbsp;</div>
</div>
<!--  end content -->
<div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->
<div class="clear">&nbsp;</div> 
<!-- start footer -->         
<?php include("footer.php") ?>
<!-- end footer -->
 
</body>
</html>
<script src="javascripts/jquery-1.8.2.js" type="text/javascript"></script>
<script src="javascripts/jquery-ui.js"></script>
<script type="text/javascript" src="javascripts/validate.js"></script>
<script>
    $(function() {
        $( "#from" ).datepicker({
			dateFormat: "d M yy",
            defaultDate: "w",
            changeMonth: true,
            numberOfMonths: 1,
            onSelect: function( selectedDate ) {
                $( "#to" ).datepicker( "option", "minDate", selectedDate );
            }
        });
        $( "#to" ).datepicker({
			dateFormat: "d M yy",
            defaultDate: "-w",
            changeMonth: true,
            numberOfMonths: 1,
            onSelect: function( selectedDate ) {
                $( "#from" ).datepicker( "option", "maxDate", selectedDate );
            }
        });
    });
</script>
