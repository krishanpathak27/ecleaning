<?php
include("includes/config.inc.php");
include("includes/function.php");
error_reporting(E_ALL & ~E_DEPRECATED & ~E_NOTICE & ~ E_WARNING);
$_objAdmin = new Admin();

//print_R($_REQUEST['id']);
//exit;
?>
<table class="table table-bordered">
   <thead>
      <tr>
         <th colspan="3">
            <center>Delivery Challan</center>
         </th>
      </tr>
   </thead>
   <thead>
      <tr>
         <td style=" width:50%;"><b>Consigner<br>Eastman Auto & Power Limited</b><br>By Pass Mor,<br>Opposite Irrigation Building, <br>Anisabad,Patna(Bihar)-800002</td>
         <td style="padding: 0px; border-left: 0px none;  width:50%;">
            <table class="table">
               <tr>
                  <td style="border-top: 0px none; border-left:0px;  width:50%;">EAPL Invoice:<br> <b>
                     <?php echo $_REQUEST['einvoice'];?></b></td>
                  <td style="border-top: 0px none; border-right:0px;  width:50%;">EAPL Invoice Date : <br> <b><?php echo $_REQUEST['invoicedate'];?></b></td>
               </tr>
               <tr>
                  <td style=" border-left:0px;  width:50%;">Vehicle No. <br><b><?php echo $_REQUEST['vichalno'];?></b></td>
                  <td style="border-right:0px;  width:50%;">Challan Date: <br><b><?php echo $_REQUEST['cdate'];?></b> </td>
               </tr>
               <tr>
                  <td style=" border-left:0px;  width:50%;">Transporter Mode:<br><b><?php echo $_REQUEST['Transmode'];?></b></td>
                  <td style="border-right:0px;"  width:50%;>Delivery Challan Transporter<br><b><?php echo $_REQUEST['DChallan'];?></b></td>
               </tr>
               <tr>
                  <td style=" border-left:0px;  width:50%;">G.R.No:<br><b><?php echo $_REQUEST['Grn'];?></b></td>
                  <td style="border-right:0px;"  width:50%;>Road Permit No.:<br> <b><?php echo $_REQUEST['RoadPermit'];?></b></td>
               </tr>
            </table>

         </td>
      </tr>
      
      
   </thead>
    <tr>
      
      <td style="border: 0px none;"><u>Declaration</u><br> <span style="font-weight:normal;">We declare that this invoice shows the acutal price of the goods described and that all particulars are true and correct.</span></td>
      <th style="padding: 0px;">
      <table class="table"><tr valign="top"><td  align="right" style="border: 0px none;"><b>for </b></td></tr><tr> <td align="right" style="border:0px; font-weight:normal; ">Authorised Signatory</td></tr></table>
      
      </th>

      </tr>
</table>


<style type="text/css">
  
.table-bordered td, .table-bordered th {
    border: 1px solid #000;
}


.table thead th {
    vertical-align: bottom;
    border-bottom: 1px solid #000;
}

.table-bordered thead td, .table-bordered thead th {
    border-bottom-width: 0px;
}

.table-bordered td, .table-bordered th {
    border-top: 1px solid rgb(0, 0, 0);
    border-bottom: 1px solid rgb(0, 0, 0);
    border-left: 1px solid rgb(0, 0, 0);
    border-right: 1px solid rgb(0, 0, 0);
}


.table {
    width: 100%;
    max-width: 100%;
    margin-bottom: 0rem;
    font-size: 12px;
}

/*.table td, .table th {
    padding: 0.75rem;
    vertical-align: top;
    border-top: 1px solid #ECEEEF;
}*/

.table-bordered {
    border: 1px solid #000;
}


.table {
  width: 100%;
  max-width: 100%;
  margin-bottom: 0rem;
}

.table th,
.table td {
  padding: 0.75rem;
  vertical-align: top;
  border-top: 1px solid #eceeef;
}

.table thead th {
  vertical-align: bottom;
  border-bottom: 2px solid #eceeef;
}

.table tbody + tbody {
  border-top: 2px solid #eceeef;
}

.table .table {
  background-color: #fff;
}

.table-sm th,
.table-sm td {
  padding: 0.3rem;
}

.table-bordered {
  border: 1px solid #eceeef;
}

.table-bordered th,
.table-bordered td {
  border: 1px solid #eceeef;
}

.table-bordered thead th,
.table-bordered thead td {
  border-bottom-width: 2px;
}

.table-striped tbody tr:nth-of-type(odd) {
  background-color: rgba(0, 0, 0, 0.05);
}

.table-hover tbody tr:hover {
  background-color: rgba(0, 0, 0, 0.075);
}

.table-active,
.table-active > th,
.table-active > td {
  background-color: rgba(0, 0, 0, 0.075);
}

.table-hover .table-active:hover {
  background-color: rgba(0, 0, 0, 0.075);
}

.table-hover .table-active:hover > td,
.table-hover .table-active:hover > th {
  background-color: rgba(0, 0, 0, 0.075);
}

.table-success,
.table-success > th,
.table-success > td {
  background-color: #dff0d8;
}

.table-hover .table-success:hover {
  background-color: #d0e9c6;
}

.table-hover .table-success:hover > td,
.table-hover .table-success:hover > th {
  background-color: #d0e9c6;
}

.table-info,
.table-info > th,
.table-info > td {
  background-color: #d9edf7;
}

.table-hover .table-info:hover {
  background-color: #c4e3f3;
}

.table-hover .table-info:hover > td,
.table-hover .table-info:hover > th {
  background-color: #c4e3f3;
}

.table-warning,
.table-warning > th,
.table-warning > td {
  background-color: #fcf8e3;
}

.table-hover .table-warning:hover {
  background-color: #faf2cc;
}

.table-hover .table-warning:hover > td,
.table-hover .table-warning:hover > th {
  background-color: #faf2cc;
}

.table-danger,
.table-danger > th,
.table-danger > td {
  background-color: #f2dede;
}

.table-hover .table-danger:hover {
  background-color: #ebcccc;
}

.table-hover .table-danger:hover > td,
.table-hover .table-danger:hover > th {
  background-color: #ebcccc;
}

.thead-inverse th {
  color: #fff;
  background-color: #292b2c;
}

.thead-default th {
  color: #464a4c;
  background-color: #eceeef;
}

.table-inverse {
  color: #fff;
  background-color: #292b2c;
}

.table-inverse th,
.table-inverse td,
.table-inverse thead th {
  border-color: #fff;
}

.table-inverse.table-bordered {
  border: 0;
}

.table-responsive {
  display: block;
  width: 100%;
  overflow-x: auto;
  -ms-overflow-style: -ms-autohiding-scrollbar;
}

.table-responsive.table-bordered {
  border: 0;
}
.ui-widget-header{
background: red !important;
   

}

</style>


<script type="text/javascript"  language="javascript" src="javascripts/jquery.printarea.js"></script>