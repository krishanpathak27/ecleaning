<?php
include("includes/config.inc.php");
include("includes/function.php");
$page_name="Claim Approval";
$_objAdmin = new Admin();
$objArrayList= new ArrayList();
$objStock = new StockClass();


// if(isset($_REQUEST['statusId']) && isset($_REQUEST['com']) && $_REQUEST['statusId']!="")
// {
// 	if(isset($_REQUEST['value']) && $_REQUEST['value']=='U')
// 	{
// 		$data['last_updated_date'] = date('Y-m-d');
// 		$data['complain_status']	= 'R'; 	//Complaint is to be rejected if disapprove by call center

// 		$cid=$_objAdmin->_dbUpdate2($data,'table_complaint', " complaint_id=".$_REQUEST['com']);	//Table Complaint Updated

// 		$data1['account_id'] = $_SESSION['accountId'];
// 		$data1['under_process'] = 'U'; //U for Unapprove(Rejected for Approval)
// 		$data1['last_updated_date'] = date('Y-m-d');
// 		$data1['status']	= 'R';	//table_service_distributor_sp_bcf status updated
		
// 		$id = $_objAdmin->_dbUpdate($data1,'table_service_distributor_sp_bcf', " service_distributor_sp_bcf_id=".$_REQUEST['statusId']);	//table_service_distributor_sp_bcf Updated
// 	}
// 	else if(isset($_REQUEST['value']) && isset($_REQUEST['com']) && $_REQUEST['value']=='A')
// 	{
// 		$token = $_objAdmin->getEAPLToken(15);
// 		$data['last_updated_date'] = date('Y-m-d');
// 		$data['auth_token']	= $token;

// 		$cid=$_objAdmin->_dbUpdate2($data,'table_complaint', " complaint_id=".$_REQUEST['com']);	//Table Complaint Updated

// 		$data1['eapi_auth_code']	= $token;
// 		$data1['under_process']		= 'T';// T for Auth Token Generated 
// 		$data1['account_id'] = $_SESSION['accountId'];
// 		$data1['last_updated_date'] = date('Y-m-d');
		
// 		$data1['status']	= 'A';	//table_service_distributor_sp_bcf status updated

// 		$id = $_objAdmin->_dbUpdate($data1,'table_service_distributor_sp_bcf', " complaint_id=".$_REQUEST['com']);	//table_service_distributor_sp_bcf Updated
// 	}
// 	$ret_id = $_objAdmin->addTAT2_call_center($_REQUEST['com']);		// Adding TAT-2 for call Center
// }

if(isset($_REQUEST['id']) && $_REQUEST['id']!="")
{
	$auRec=$_objAdmin->_getSelectList('table_service_distributor_sp_bcf as BCF left join table_complaint as COMPLAINT on COMPLAINT.complaint_id = BCF.complaint_id left join table_customer as CUST on CUST.customer_id = COMPLAINT.customer_id left join table_item_bsn as BSN on BSN.bsn_id = COMPLAINT.bsn_id left join table_item as ITEM on ITEM.item_id = BSN.item_id left join table_segment as SEGMENT on ITEM.item_segment = SEGMENT.segment_id left join table_category as CATEGORY on CATEGORY.category_id = ITEM.category_id left join table_brands as BRANDS on BRANDS.brand_id = ITEM.brand_id left join table_distributors as DISTRIBUTOR on DISTRIBUTOR.distributor_id = BSN.sale_distributor_id left join table_service_distributor_sp as SERDIS_sp on SERDIS_sp.service_distributor_sp_id = BCF.service_distributor_sp_id left join table_service_distributor as SERDIS on SERDIS.service_distributor_id = SERDIS_sp.service_distributor_id left join table_failure_mode as FAILURE on FAILURE.failure_id = BCF.failure_mode','COMPLAINT.*,CUST.*,BSN.*,SEGMENT.segment_name,SEGMENT.segment_id,CATEGORY.category_name,CATEGORY.category_id,BRANDS.brand_id,BRANDS.brand_name,ITEM.item_name,ITEM.item_id, DISTRIBUTOR.distributor_name,DISTRIBUTOR.distributor_id, SERDIS_sp.service_distributor_sp_name,SERDIS_sp.service_distributor_sp_id, SERDIS.service_distributor_id, SERDIS.service_distributor_name, BCF.last_updated_date as date_of_inspection,BCF.*,BCF.status as final_status, FAILURE.failure_code,BCF.approver_comments','',' BCF.complaint_id = "'.$_REQUEST['id'].'"');


		// echo "<pre>";
		// print_r($auRec);




	$actualTotalWarranty = 0;

	if($auRec[0]->date_of_sale!='' && $auRec[0]->date_of_sale!='1970-01-01' && $auRec[0]->date_of_sale!='0000-00-00' ) {
	

		//$usedBatteryInMonths = strtotime(date('Y-m-d'))-strtotime($auRec[$i]->date_of_sale) / (60*60*24);	

		$datetime1 = date_create(date('Y-m-d'));

		$datetime2 = date_create($auRec[0]->date_of_sale);

		$interval = date_diff($datetime1, $datetime2);

		//$usedBatteryInMonths = $interval->format('%m month');
		$usedBatteryInDays = $interval->format('%a');
		$usedBatteryInMonths = ceil($usedBatteryInDays  / 30);



		$actualTotalWarranty = $auRec[0]->warranty_period + $auRec[0]->warranty_prorata;
		// $balanceTotalWarranty = $actualTotalWarranty - $usedBatteryInMonths;

		$bsn['balanceTotalWarranty'] = $balanceTotalWarranty;

		//if($balanceTotalWarranty>0 && $balanceTotalWarranty<$actualTotalWarranty) {
			if($balanceTotalWarranty < $auRec[0]->warranty_period) {
				// $bsn['balance_item_warranty'] = $value->warranty_period - $balanceTotalWarranty;
				// $bsn['balance_item_prodata'] =  $value->warranty_prorata;

					$auRec[0]->balance_item_warranty = $auRec[0]->warranty_period - $usedBatteryInMonths;
    			$auRec[0]->balance_item_prodata =   $auRec[0]->warranty_prorata;

			} else if($balanceTotalWarranty > $auRec[0]->warranty_period) {
				// $bsn['balance_item_warranty'] = 0;
				// $bsn['balance_item_prodata'] =  $actualTotalWarranty - $balanceTotalWarranty;

	            $auRec[0]->balance_item_warranty = 0;
	            $auRec[0]->balance_item_prodata =  $actualTotalWarranty - $usedBatteryInMonths;

			}
			
			
		//}

	}
	// if($auRec[0]->date_of_sale!='' && $auRec[0]->date_of_sale!='1970-01-01' && $auRec[0]->date_of_sale!='0000-00-00' )
	// {

	// 	$datetime1 = date_create(date('Y-m-d'));
	// 	$datetime2 = date_create($auRec[0]->date_of_sale);
	// 	$interval = date_diff($datetime1, $datetime2);
	// 	$usedBatteryInDays = $interval->format('%a');
	// 	$usedBatteryInMonths = ceil($usedBatteryInDays  / 30);	
	// 	$actualTotalWarranty = $auRec[0]->warranty_period + $auRec[0]->warranty_prorata;
	// 	// $balanceTotalWarranty = $actualTotalWarranty - $usedBatteryInMonths;

	// 	if($balanceTotalWarranty>0 && $balanceTotalWarranty<$actualTotalWarranty)
	// 	{
	// 		if($balanceTotalWarranty < $auRec[0]->warranty_period)
	// 		{
	// 			$auRec[0]->balance_item_warranty = $auRec[0]->warranty_period - $balanceTotalWarranty;
	// 			$auRec[0]->balance_item_prodata =  $auRec[0]->warranty_prorata;
	// 		} 
	// 		else if($balanceTotalWarranty > $auRec[0]->warranty_period)
	// 		{
	// 			$auRec[0]->balance_item_warranty = 0;
	// 			$auRec[0]->balance_item_prodata =  $actualTotalWarranty - $balanceTotalWarranty;
	// 		}
	// 	}
	// }
	if(count($auRec)<=0) header("Location: completed_complaints_by_approver.php");
} 
else
{
	header("Location: completed_complaints_by_approver.php");
}

if(($_POST['change_status']!='' )	&& ($_POST['change_status']=='yes'))
{
	$cid=$_objAdmin->updateApprovedComplaint($_POST['bcf_id']);
	$sus="BCF form successfully updated."; 
}
?>



<?php include("header.inc.php");

 ?>
 <style>
	.custom
	{
		color: #423b3b;
    	font-size: 12px;
    	line-height: 12px;
    	font-weight: bold;
    	padding-left: 10px;
	}


.formStyle {
    border: 1px solid #ccc;
    color: #393939;
    height: 20px;
    padding: 6px 6px 0px;
    width: 187px;
    border-radius: 5px;
}

.alert-success {
    color: #3C763D;
    background-color: #DFF0D8;
    border-color: #D6E9C6;
}
.alert {
    padding: 15px;
    margin-bottom: 20px;
    border: 1px solid transparent;
    border-radius: 4px;
}

</style>
<script type="text/javascript" src="javascripts/validate.js"></script>
<script src="docsupport/chosen.jquery.js" type="text/javascript"></script>

<script type="text/javascript">
var config = {
  '.chosen-select'           : {},
  '.chosen-select-deselect'  : {allow_single_deselect:true},
  '.chosen-select-no-single' : {disable_search_threshold:10},
  '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
  '.chosen-select-width'     : {width:"100%"}
}
for (var selector in config) {
  $(selector).chosen(config[selector]);
}
</script>
<script src="javascripts/jquery-1.8.2.js" type="text/javascript"></script>
<script src="javascripts/jquery-ui.js"></script>
<script type="text/javascript" src="javascripts/validate.js"></script>

<style>
	.custom
	{
		color: #423b3b;
    	font-size: 12px;
    	line-height: 12px;
    	font-weight: bold;
    	padding-left: 10px;
	}


.formStyle {
    border: 1px solid #ccc;
    color: #393939;
    height: 20px;
    padding: 6px 6px 0px;
    width: 187px;
    border-radius: 5px;
}


</style>

<div class="clear"></div>
<!-- start content-outer -->
<div id="content-outer">
<!-- start content -->
	<div id="content">
	<div id="page-heading"><h1><span style="color: #d74343; font-family: Tahoma; font-weight: bold;">Claim Approval </span></h1></div>
		<table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">
		<tr>
			<!--<td id="tbl-border-left"></td>-->
			<td>
			<!--  start content-table-inner -->
			<div id="content-table-inner">
				<table border="0" width="100%" cellpadding="0" cellspacing="0">
				<tr valign="top">
					<td>
					<!--  start message-red -->
					<?php if($err!=''){?>
					<div id="message-red">
						<table border="0" width="100%" cellpadding="0" cellspacing="0">
						<tr>
							<td class="red-left"> <?php echo $err; ?></td>
							<td class="red-right"><a class="close-red"><img src="images/icon_close_red.gif"   alt="" /></a></td>
						</tr>
						</table>
					</div>
					<?php } ?>
					<!--  end message-red -->
					<?php if($sus!=''){?>
					<!--  start message-green -->
					<div id="message-green">
						<table border="0" width="100%" cellpadding="0" cellspacing="0">
						<tr>
							<td class="green-left"><?php echo $sus; ?></td>
							<td class="green-right"><a class="close-green"><img src="images/icon_close_green.gif"   alt="" /></a></td>
						</tr>
						</table>
					</div>
					<?php } ?>
					<!--  end message-green -->
					
			<!-- start id-form -->
		<form name="frmPre" id="frmPre" method="post" action="change_status_of_approved_complaint.php"  enctype="multipart/form-data" >
			<div class="row" style="margin:0px;">
	        <div class="col-sm">
	          <div class="panel panel-default">
	            <div class="panel-heading">
	              <h3 class="panel-title">Complaint Details</h3>
	            </div>
	            <div class="panel-body">
					<div class="form-group">
			            <div class="row">
		            		<div class="col-sm-6" >
			                <div class="input-group">
			                   <span class="input-group-addon" id="basic-addon1">Complaint No.</span>
			                    <input type="text" class="form-control required"  id="complaint_no" name="complaint_no" readonly value="<?php echo $auRec[0]->complaint_no; ?>" placeholder="Complaint No." readonly>
			                </div>
			              </div>
			              <div class="col-sm-6" >
			                <div class="input-group">
			                   <span class="input-group-addon" id="basic-addon1">Complaint Date</span>
			                    <input type="text" class="form-control required datepicker"  id="complaint_date" name="complaint_date" readonly placeholder="Complaint Date" value="<?php echo date('Y-m-d',strtotime($auRec[0]->created_date)); ?>">
			                </div>
			              </div>	              
			            </div>
		            </div>
		        </div>
		     </div>
		    </div>
		 </div>
		 
		 <div class="row" style="margin:0px;">
	        <div class="col-sm">
	          <div class="panel panel-default">
	            <div class="panel-heading">
	              <h3 class="panel-title">Customer Details</h3>
	            </div>
	            <div class="panel-body">
					<div class="form-group">
			            <div class="row">
		            		<div class="col-sm-3" >
			                <div class="input-group">
			                   <span class="input-group-addon" id="basic-addon1">Customer Name</span>
			                    <input type="text" class="form-control <?php echo $classRequire;?>"  id="customer_name" name="customer_name" readonly placeholder="Customer Name" value="<?php echo $auRec[0]->customer_name; ?>">
			                </div>
			              </div>
			              <div class="col-sm-3" >
			                <div class="input-group">
			                   <span class="input-group-addon" id="basic-addon1">Mobile No.</span>
			                    <input type="text" class="form-control <?php echo $classRequire;?>"  id="customer_phone_no" name="customer_phone_no" readonly placeholder="Mobile No." value="<?php echo $auRec[0]->customer_phone_no; ?>">
			                </div>
			              </div>
		              <div class="col-sm-3" >
		            	<div class="input-group">
		                   <span class="input-group-addon" id="basic-addon1">Alternate No.</span>
		                    <input type="text" class="form-control required"  id="customer_phone_no2" name="customer_phone_no2" readonly placeholder="Alternate No. " value="<?php echo $auRec[0]->customer_phone_no2; ?>" readonly>
		                </div>
		                </div>
		        		<div class="col-sm-3" >
			                <div class="input-group">
			                   <span class="input-group-addon" id="basic-addon1">Email Id</span>
			                    <input type="text" class="form-control <?php echo $classRequire;?>"  id="customer_email" name="customer_email" readonly placeholder="Email ID" value="<?php echo $auRec[0]->customer_email; ?>">
			                </div>
			              </div>
			              
			              </div>              
			            </div>
			            <div class="form-group">
			            <div class="row">
		            		<div class="col-sm-12" >
			                <div class="input-group">
			                   <span class="input-group-addon" id="basic-addon1">Customer Address</span>
			                    <input type="text" class="form-control <?php echo $classRequire;?>"  id="customer_address" name="customer_address" readonly placeholder="Customer Address" value="<?php echo $auRec[0]->customer_address; ?>">
			                </div>
			              </div>
			              </div>              
			            </div>
		            </div>
		        </div>
		     </div>
		    </div>


		    <div class="row" style="margin:0px;">
	        <div class="col-sm">
	          <div class="panel panel-default">
	            <div class="panel-heading">
	              <h3 class="panel-title">Battery Details</h3>
	            </div>
	            <div class="panel-body">
					<div class="form-group">
			            <div class="row">
			              <div class="col-sm-3" >
			                <div class="input-group">
			                   <span class="input-group-addon" id="basic-addon1">Segment</span>
			                    <input type="text" class="form-control"  id="segment_id" name="segment_id"  placeholder="Segment" value="<?php echo $auRec[0]->segment_name; ?>" readonly>
			                </div>
			              </div>
		        		<div class="col-sm-3" >
			                <div class="input-group">
			                   <span class="input-group-addon" id="basic-addon1">Category</span>
			                    <input type="text" class="form-control "  id="category_id" name="category_id"  placeholder="Category" value="<?php echo $auRec[0]->category_name; ?>" readonly>
			                </div>
			              </div>
			              <div class="col-sm-3" >
			                <div class="input-group">
			                   <span class="input-group-addon" id="basic-addon1">Brand</span>
			                    <input type="text" class="form-control "  id="brand_id" name="brand_id"  placeholder="Brand" value="<?php echo $auRec[0]->brand_name; ?>" readonly>
			                </div>
			              </div>
			              <div class="col-sm-3" >
			                <div class="input-group">
			                   <span class="input-group-addon" id="basic-addon1">Item Name</span>
			                    <input type="text" class="form-control"  id="item_id" name="item_id"  placeholder="Item Name" value="<?php echo $auRec[0]->item_name; ?>" readonly>
			                </div>
			              </div>
			              </div>              
			            </div>
			            <div class="form-group">
			            <div class="row">
			              <div class="col-sm-3" >
			                <div class="input-group">
			                   <span class="input-group-addon" id="basic-addon1">BSN</span>
			                    <input type="text" class="form-control "  id="bsn" name="bsn"  placeholder="BSN" value="<?php echo $auRec[0]->bsn; ?>" readonly>
			                </div>
			              </div>
		        		<div class="col-sm-3" >
			                <div class="input-group">
			                   <span class="input-group-addon" id="basic-addon1">Purchased On</span>
			                    <input type="text" class="form-control"  id="date_of_sale" name="date_of_sale"  placeholder="Purchased On" value="<?php echo $auRec[0]->date_of_sale; ?>" readonly>
			                </div>
			              </div>
			              <div class="col-sm-6" >
			                <div class="input-group">
			                   <span class="input-group-addon" id="basic-addon1">Purchased From</span>
			                    <input type="text" class="form-control "  id="distributor_id" name="distributor_id"  placeholder="Purchased From" value="<?php echo $auRec[0]->distributor_name; ?>" readonly>
			                </div>
			              </div>
			              </div>              
			            </div>
		            </div>
		        </div>
		     </div>
		    </div>
		    <div class="row" style="margin:0px;">
	        <div class="col-sm">
	          <div class="panel panel-default">
	            <div class="panel-heading">
	              <h3 class="panel-title">Warranty Details</h3>
	            </div>
	            <div class="panel-body">
					<div class="form-group">
			            <div class="row">
			              <div class="col-sm-6" >
			                <div class="input-group">
			                   <span class="input-group-addon" id="basic-addon1">Total Warranty FOC</span>
			                    <input type="text" class="form-control "  id="warranty_period" name="warranty_period"  placeholder="Warranty FOC" value="<?php echo $auRec[0]->warranty_period; ?>" readonly>
			                </div>
			              </div>
		        		<div class="col-sm-6" >
			                <div class="input-group">
			                   <span class="input-group-addon" id="basic-addon1">Total Warranty Prorata</span>
			                    <input type="text" class="form-control "  id="warranty_prorata" name="warranty_prorata"  placeholder="Warranty Prorata" value="<?php echo $auRec[0]->warranty_prorata; ?>" readonly>
			                </div>
			              </div>
			              </div>              
			            </div>
			            <div class="form-group">
			            <div class="row">
			              <div class="col-sm-6" >
			                <div class="input-group">
			                   <span class="input-group-addon" id="basic-addon1">Balance Warranty FOC</span>
			                    <input type="text" class="form-control "  id="balance_item_warranty" name="balance_item_warranty"  placeholder="Balance Warranty FOC" value="<?php echo $auRec[0]->balance_item_warranty; ?>" readonly>
			                </div>
			              </div>
		        		<div class="col-sm-6" >
			                <div class="input-group">
			                   <span class="input-group-addon" id="basic-addon1">Balance Warranty Prorata</span>
			                    <input type="text" class="form-control "  id="balance_item_prodata" name="balance_item_prodata"  placeholder="Warranty Prorata" value="<?php echo $auRec[0]->balance_item_prodata; ?>" readonly>
			                </div>
			              </div>
			              </div>              
			            </div>
			            
		            </div>
		        </div>
		     </div>
		    </div>
		    <div class="row" style="margin:0px;">
	        <div class="col-sm">
	          <div class="panel panel-default">
	            <div class="panel-heading">
	              <h3 class="panel-title">Service Distributor</h3>
	            </div>
	            <div class="panel-body">
					<div class="form-group">
			            <div class="row">
		            		
			              <div class="col-sm-4" >
			                <div class="input-group">
			                   <span class="input-group-addon" id="basic-addon1">Service Distributor</span>
			                    <input type="text" class="form-control"  value="<?php echo $auRec[0]->service_distributor_name; ?>" id="service_distributor_id" name="service_distributor_id"  placeholder="Distributor" readonly>
			                </div>
			              </div>
			              <div class="col-sm-4" >
			                <div class="input-group">
			                   <span class="input-group-addon" id="basic-addon1">Service Personnel</span>
			                    <input type="text" class="form-control"  value="<?php echo $auRec[0]->service_distributor_sp_name; ?>" id="service_personnel_id" name="service_personnel_id"  placeholder="Service Personnel" readonly>
			                </div>
			              </div>
		        		<div class="col-sm-4" >
			                <div class="input-group">
			                   <span class="input-group-addon" id="basic-addon1">Reaching Date</span>
			                    <input type="text" class="form-control "  value="<?php echo $auRec[0]->initial_reading_save_time; ?>" id="initial_reading_save_time" name="initial_reading_save_time"  placeholder="Date of Inspection" readonly>
			                </div>
			              </div>
			            	
			              </div> 
			            </div>
		            </div>
		        </div>
		     </div>
		    </div>
		    <div class="row" style="margin:0px;">
	        <div class="col-sm">
	          <div class="panel panel-default">
	            <div class="panel-heading">
	              <h3 class="panel-title">Initial Readings</h3>
	            </div>
	            <div class="panel-body">
	            	<div class="form-group">
			            <div class="row">
			              <div class="col-sm-6" >
			                <div class="input-group">
			                   <span class="input-group-addon" id="basic-addon1">Actual Problem</span>
			                    <input type="text" class="form-control"  id="actual_problem" name="actual_problem"  placeholder="Actual Problem" value="<?php echo $auRec[0]->actual_problem; ?>">
			                </div>
			              </div>
			              <div class="col-sm-6" >
			                <div class="input-group">
			                   <span class="input-group-addon" id="basic-addon1">Visual Observation</span>
			                    <input type="text" class="form-control"  id="visual_observation" name="visual_observation"  placeholder="Visual Observation" value="<?php echo $auRec[0]->visual_observation; ?>">
			                </div>
			              </div>
			              </div>
			            </div>
			            <div class="form-group">
			            <div class="row">
			              <div class="col-sm-4" >
			                <div class="input-group">
			                   <span class="input-group-addon" id="basic-addon1">Document Checked</span>
			                    <select class="form-control required"  id="documents_checked" name="documents_checked" placeholder="Documents Checked" >
			                    	<option value="">--Documents Checked--</option>
									<option value="Yes" <?php if($auRec[0]->documents_checked=='Yes') echo "selected";?>>Yes</option>
									<option value="No" <?php if($auRec[0]->documents_checked=='No') echo "selected";?>>No</option>
								</select>
			                </div>
			              </div>
			              <?php
			              		if($auRec[0]->warranty_card_pic !='')
			              		{
			              ?>
			              <div class="col-sm-4" >
			                <div class="input-group">
			                   <span class="input-group-addon" id="basic-addon1">Warranty Card</span>
			                   <a href="images/warranty_card/<?php echo $auRec[0]->warranty_card_pic; ?>" class="form-control"  target="_blank">Warranty Card</a>
			                    
			                </div>
			              </div>
			              <?php 
			              		}
			              ?>
			              <?php 
			              		if($auRec[0]->purchase_bill_pic !='')
			              		{
			              ?>
			              <div class="col-sm-4" >
			                <div class="input-group">
			                   <span class="input-group-addon" id="basic-addon1">Purchase Bill</span>
			                   <a href="images/purchase_bill/<?php echo $auRec[0]->purchase_bill_pic; ?>" class="form-control" target="_blank">Purchase Bill</a>
			                    
			                </div>
			              </div>
			              <?php 
			              		}
			              ?>
			              </div>
			            </div>
	            	<div class="form-group">
			            	<div class="row">
			              
			              </div>
			            </div>
					<div class="form-group">
			            	<div class="row">
			            		<label class="col-sm-12" style="text-align: center;">Specific Gravity (Starting from +ve)</label>
			            	</div>
			            	<div class="row">
			              		<label class="col-sm-2" style="text-align: center;color:#423b3b;">OCV</label>
			              		<label class="col-sm-2" style="text-align: center;color:#423b3b;">C1</label>
			              		<label class="col-sm-2" style="text-align: center;color:#423b3b;">C2</label>
			              		<label class="col-sm-1" style="text-align: center;color:#423b3b;">C3</label>
			              		<label class="col-sm-2" style="text-align: center;color:#423b3b;">C4</label>
			              		<label class="col-sm-2" style="text-align: center;color:#423b3b;">C5</label>
			              		<label class="col-sm-1" style="text-align: center;color:#423b3b;">C6</label>
			              	</div>
			            	<div class="row">
			            	<div class="col-sm-2" >
			                <div class="input-group">
			                   
			                    <input type="text" class="form-control"  id="ocv" name="ocv"  placeholder="OCV" value="<?php echo $auRec[0]->ocv; ?>" >
			                </div>
			              </div>
		            		<div class="col-sm-2" >
			                <div class="input-group">
			                   
			                    <input type="text" class="form-control "  id="inital_reading_c1" name="inital_reading_c1"  placeholder="C1" value="<?php echo $auRec[0]->inital_reading_c1; ?>" >
			                </div>
			              </div>
			              <div class="col-sm-2" >
			                <div class="input-group">
			                   
			                    <input type="text" class="form-control"  id="inital_reading_c2" name="inital_reading_c2"  placeholder="C2" value="<?php echo $auRec[0]->inital_reading_c2; ?>" >
			                </div>
			              </div>
		        		<div class="col-sm-1" >
			                <div class="input-group">
			                   
			                    <input type="text" class="form-control"  id="inital_reading_c3" name="inital_reading_c3"  placeholder="C3" value="<?php echo $auRec[0]->inital_reading_c3; ?>" >
			                </div>
			              </div>
			              <div class="col-sm-2" >
			                <div class="input-group">
			                   
			                    <input type="text" class="form-control"  id="inital_reading_c4" name="inital_reading_c4"  placeholder="C4" value="<?php echo $auRec[0]->inital_reading_c4; ?>" >
			                </div>
			              </div>
			              <div class="col-sm-2" >
			                <div class="input-group">
			                   
			                    <input type="text" class="form-control "  id="inital_reading_c5" name="inital_reading_c5" placeholder="C5" value="<?php echo $auRec[0]->inital_reading_c5; ?>" >
			                </div>
			              </div>
			              <div class="col-sm-1" >
			                <div class="input-group">
			                   
			                    <input type="text" class="form-control "  id="inital_reading_c6" name="inital_reading_c6"  placeholder="C6" value="<?php echo $auRec[0]->inital_reading_c6; ?>" >
			                </div>
			              </div>
			              </div>
			            </div>
			            
		            </div>
		        </div>
		     </div>
		    </div>
		    <div class="row" style="margin:0px;">
	        <div class="col-sm">
	          <div class="panel panel-default">
	            <div class="panel-heading">
	              <h3 class="panel-title">Readings after Recharging</h3>
	            </div>
	            <div class="panel-body">
	            	<div class="form-group">
			            	<div class="row">
			              
			              </div>
			            </div>
			            <div class="form-group">
			            	<div class="row">
			            		<label class="col-sm-12" style="text-align: center;">Specific Gravity (Starting from +ve)</label>
			            	</div>
			            	<div class="row">
			              		<label class="col-sm-2" style="text-align: center;color:#423b3b;">TOC</label>
			              		<label class="col-sm-2" style="text-align: center;color:#423b3b;">C1</label>
			              		<label class="col-sm-2" style="text-align: center;color:#423b3b;">C2</label>
			              		<label class="col-sm-1" style="text-align: center;color:#423b3b;">C3</label>
			              		<label class="col-sm-2" style="text-align: center;color:#423b3b;">C4</label>
			              		<label class="col-sm-2" style="text-align: center;color:#423b3b;">C5</label>
			              		<label class="col-sm-1" style="text-align: center;color:#423b3b;">C6</label>
			              	</div>
			            	<div class="row">
			            	<div class="col-sm-2" >
			                <div class="input-group">
			                   
			                    <input type="text" class="form-control"  id="toc" name="toc"  placeholder="TOC" value="<?php echo $auRec[0]->toc; ?>" >
			                </div>
			              </div>
		            		<div class="col-sm-2" >
			                <div class="input-group">
			                   
			                    <input type="text" class="form-control"  id="final_reading_c1" name="final_reading_c1"  placeholder="C1" value="<?php echo $auRec[0]->final_reading_c1; ?>" >
			                </div>
			              </div>
			              <div class="col-sm-2" >
			                <div class="input-group">
			                   
			                    <input type="text" class="form-control" value="<?php echo $auRec[0]->final_reading_c2; ?>"  id="final_reading_c2" name="final_reading_c2"  placeholder="C2" >
			                </div>
			              </div>
		        		<div class="col-sm-1" >
			                <div class="input-group">
			                   
			                    <input type="text" class="form-control"  id="final_reading_c3" name="final_reading_c3"  placeholder="C3" value="<?php echo $auRec[0]->final_reading_c3; ?>" >
			                </div>
			              </div>
			              <div class="col-sm-2" >
			                <div class="input-group">
			                   
			                    <input type="text" class="form-control"  id="final_reading_c4" name="final_reading_c4" value="<?php echo $auRec[0]->final_reading_c4; ?>"  placeholder="C4" >
			                </div>
			              </div>
			              <div class="col-sm-2" >
			                <div class="input-group">
			                   
			                    <input type="text" class="form-control" value="<?php echo $auRec[0]->final_reading_c5; ?>"  id="final_reading_c5" name="final_reading_c5"  placeholder="C5" >
			                </div>
			              </div>
			              <div class="col-sm-1" >
			                <div class="input-group">
			                   
			                    <input type="text" class="form-control" value="<?php echo $auRec[0]->final_reading_c6; ?>" id="final_reading_c6" name="final_reading_c6"  placeholder="C6" >
			                </div>
			              </div>
			              </div>
			            </div>
			            <div class="form-group">
			            <div class="row">
		        		<div class="col-sm-4" >
			                <div class="input-group">
			                   <span class="input-group-addon" id="basic-addon1">Total Charging Hours</span>
			                    <input type="text" class="form-control" value="<?php echo $auRec[0]->charging_time_in_hrs; ?>" id="charging_time_in_hrs" name="charging_time_in_hrs"  placeholder="Total Hours" >
			                </div>
			              </div>			            	
			              </div> 
			            </div>	            
		            </div>
		        </div>
		     </div>
		    </div>
		    <div class="row" style="margin:0px;">
	        <div class="col-sm">
	          <div class="panel panel-default">
	            <div class="panel-heading">
	              <h3 class="panel-title">Back-Up Status</h3>
	            </div>
	            <div class="panel-body">
	            	
			            
			            <div class="form-group">
			            <div class="row">
		        		<div class="col-sm-4" >
			                <div class="input-group">
			                   <span class="input-group-addon" id="basic-addon1">Back-Up Load</span>
			                    <input type="text" class="form-control" value="<?php echo $auRec[0]->backup_load; ?>" id="backup_load" name="backup_load"  placeholder="Back-Up Load" >
			                </div>
			              </div>
		        		<div class="col-sm-4" >
			                <div class="input-group">
			                   <span class="input-group-addon" id="basic-addon1">Back-Up Time</span>
			                    <input type="text" class="form-control" value="<?php echo $auRec[0]->min_backup; ?>" id="min_backup" name="min_backup"  placeholder="Back-Up Time" >
			                </div>
			              </div>
			              
			              <div class="col-sm-4" >
			                <div class="input-group">
			                   <span class="input-group-addon" id="basic-addon1">Load Used</span>
			                    <input type="text" class="form-control" value="<?php echo $auRec[0]->load_used; ?>" id="load_used" name="load_used"  placeholder="Load Used" >
			                </div>
			              </div>			            	
			              </div> 
			            </div>		            
		            </div>
		        </div>
		     </div>
		    </div>
		    <div class="row" style="margin:0px;">
	        <div class="col-sm">
	          <div class="panel panel-default">
	            <div class="panel-heading">
	              <h3 class="panel-title">Failure Status</h3>
	            </div>
	            <div class="panel-body">
			            <div class="form-group">
			            <div class="row">
		        		<div class="col-sm-6" >
			                <div class="input-group">
			                   <span class="input-group-addon" id="basic-addon1">Failure Mode</span>
			                    <select class="form-control required"  id="failure_id" name="failure_id"  placeholder="Failure Mode" >
			                    	<option value="">--Select Failure Mode--</option>
									<?php $fMode=$_objAdmin->_getSelectList('table_failure_mode','failure_id,failure_code,failure_desc',''," status='A' ORDER BY failure_code");
									if(is_array($fMode)){
									for($i=0;$i<count($fMode);$i++){?>
									<option value="<?php echo $fMode[$i]->failure_id;?>" <?php if($fMode[$i]->failure_id==$auRec[0]->failure_mode) echo "selected";?>><?php echo $fMode[$i]->failure_desc;?></option>
									
									<?php }}?>
								</select>
			                </div>
			              </div>
		        		<div class="col-sm-6" >
			                <div class="input-group">
			                   <span class="input-group-addon" id="basic-addon1">Remarks</span>
			                   <textarea class="form-control" value="<?php echo $auRec[0]->remarks; ?>" id="remarks" name="remarks"  placeholder="Remarks" ><?php echo $auRec[0]->remarks; ?></textarea>
			                </div>
			              </div>

			              </div> 
			            </div>	
			            <div class="form-group">
			            <div class="row">
		        		<div class="col-sm-6" >
			                <div class="input-group">
			                   <span class="input-group-addon" id="basic-addon1">Test Report Action</span>
			                    <select class="form-control required"  id="action_taken" name="action_taken" placeholder="Action Taken" required>
			                    	<option value="">--Choose Action Taken--</option>
									<option value="Replace free" <?php if($auRec[0]->action_taken=='Replace free') echo "selected";?>>Replace free</option>
									<option value="Replace pro-rata" <?php if($auRec[0]->action_taken=='Replace pro-rata') echo "selected";?>>Replace pro-rata</option>
									<option value="Reject" <?php if($auRec[0]->action_taken=='Reject') echo "selected";?>>Reject</option>
									<option value="Recharge" <?php if($auRec[0]->action_taken=='Recharge') echo "selected";?>>Recharge</option>
									<option value="Tested Ok" <?php if($auRec[0]->action_taken=='Tested Ok') echo "selected";?>>Tested Ok</option>
								</select>
			                </div>
			              </div>
			              			            	
			              </div> 
			            </div>		            
		            </div>
		        </div>
		     </div>
		    </div>
		    <div class="row" style="margin:0px;">
	        <div class="col-sm">
	          <div class="panel panel-default">
	            <div class="panel-heading">
	              <h3 class="panel-title">Approve / Disapprove</h3>
	            </div>
	            <div class="panel-body">
					<div class="form-group">
			            <div class="row">
			            	<div class="col-sm-6" >
			                <div class="input-group">
			                   <span class="input-group-addon" id="basic-addon1">Approver Comments</span>
			                    <textarea class="form-control" value="<?php echo $auRec[0]->approver_comments; ?>" id="approver_comments" name="approver_comments"  placeholder="approver_comments" required><?php echo $auRec[0]->approver_comments;?></textarea>
			                </div>
			              </div>
		            		<div class="col-sm-6" >
			                <div class="input-group">
			                   <span class="input-group-addon" id="basic-addon1">Status</span>
			                    <select class="form-control required"  id="final_status" name="final_status"  placeholder="Status">
										<option value="P" <?php if($auRec[0]->final_status=='R') echo "selected";?> >Dis-Approve</option>
										<option value="A" <?php if($auRec[0]->final_status=='A') echo "selected";?> >Approve</option>
								</select>

			                </div>
			              </div>
			              
			             </div>
			            </div>
			            <div class="form-group">
			            <div class="row">
			            	<div class="col-sm-8" >
			                <div class="input-group">
			                	&nbsp;
			                </div>
			                </div>
		            		<div class="col-sm-8" >
			                <div class="input-group">
			                	<input name="account_id" type="hidden" value="<?php echo $_SESSION['accountId']; ?>" />
								<input name="id" type="hidden" value="<?php echo $_REQUEST['id']; ?>" />
								<input name="complaint_id" type="hidden" value="<?php echo $auRec[0]->complaint_id; ?>" />
								<input name="bcf_id" type="hidden" value="<?php echo $auRec[0]->service_distributor_sp_bcf_id; ?>" />
								<input name="change_status" type="hidden" value="yes" />
								<input type="button" value="Back" class="form-reset btn btn-default" onclick="location.href='completed_complaints_by_approver.php'" style="padding-bottom: 25px; padding-left: 25px; padding-right: 25px;background: #d9534f;background-image: linear-gradient(to bottom, #d9534f 0px, #c12e2a 100%);color:#fff;"/>
								
								<input name="submit" class="form-submit btn btn-default" type="submit" id="submit" value="Save" style="padding-bottom: 25px; padding-left: 25px; padding-right: 25px;background-image: linear-gradient(to bottom, #5cb85c 0px, #419641 100%);background-color: #5cb85c;color:#fff;"/>
								
								
			                </div>
			              </div>
			              
			             </div>
			            </div>		            
		            </div>
		        </div>
		     </div>
		    </div>
		 </div>
		</form>
		
			<!-- end id-form  -->
			</td>
			<td>
			<!-- right bar-->
			
			<div class="clear"></div>
		</div>
<!-- end related-act-bottom -->
</div>
<!-- end related-activities -->
</td>
</tr>
<tr>
<td><img src="images/blank.gif" width="695" height="1" alt="blank" /></td>
<td></td>
</tr>
</table>
<div class="clear"></div>
</div>
<!--  end content-table-inner  -->
</td>
<td id="tbl-border-right"></td>
</tr>
</table>
<div class="clear">&nbsp;</div>

</div>
<!--  end content -->
<div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->
<div class="clear">&nbsp;</div>
    
<!-- start footer --> 
<?php include("footer.php") ?>  
<!-- end footer -->
</body>
</html>

<link href="bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<link href="bootstrap/dist/css/bootstrap-theme.min.css" rel="stylesheet">
<style>
	.form-control
	{
		color:black;
	}
</style>
<script src="javascripts/jquery-1.12.4.js"></script>
<script src="javascripts/jquery-ui-1.12.1.js"></script>

<script type="text/javascript" src="javascripts/validate.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script type="text/javascript">
  

  $(function() {
    $( ".datepicker" ).datepicker({
     // showOn: "button",
     // buttonImage: "images/calendar.gif",
      //buttonImageOnly: true,
      //buttonText: "Select date",
      dateFormat: "d M yy",
      defaultDate: "w",
      changeMonth: true,
      numberOfMonths: 1
    });
  });


  
</script>