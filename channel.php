<?php
include("includes/config.inc.php");
include("includes/function.php");


$_objAdmin = new Admin();

if(isset($_REQUEST['show']) && $_REQUEST['show']=="yes"){
	$_objAdmin->showRetailerChannel();
	die;
}

/*********************************************** 
 |  Discription: Add retailer channel
 |  Date : 20 May 2015
 |	By   : Nizam 	  
************************************************/
if(isset($_POST['submit']) && $_POST['submit']=='Save')
 {
 	/* Chainel Already exits condtions */
 	if(isset($_POST['channel_name']) && $_POST['channel_name']!="")
 	 {
 		$channel_exits=$_objAdmin->retailerChannelExits();
 		if($channel_exits==1){ $sus="Retailer channel name Already exits in the system.";}
 		else
 		{   /* Update Retailer Chainel */
 			if(isset($_POST['channel_id']) && $_POST['channel_id']!="")
 			 {
 			 	$update=$_objAdmin->updateRetailerChannel($_POST['channel_id']);
 			 	if($update>0){ $sus="Retailer channel updated successfully.";}
 			 }
 			 else
 			 {
 			$channel=$_objAdmin->addRetailerChannel();
     		if($channel>0){ $sus="Retailer channel successfully added";}
 	         }	
 			
 		}	

 	}
     
 }

/*********************************************** 
 |  Discription: Edit retailer channel
 |  Date : 20 May 2015
 |	By   : Nizam 	  
************************************************/
 if(isset($_REQUEST['action']) && $_REQUEST['action']=='edit')
       { 
         $channelEdit=$_objAdmin->retailerChannelEdit($_REQUEST['id']);
       	 /*echo '<pre>';
       	 print_r($channelEdit);*/
       	 
       } 



if(isset($_REQUEST['statusId']) && $_REQUEST['value']!="")
{
	if($_REQUEST['value']=="Active"){	
		$status='I';
	} else {
		$status='A';
	}
	$cid=$_objAdmin->_dbUpdate(array("status"=>$status),'table_retailer_channel_master', " channel_id='".$_REQUEST['statusId']."'");
	header("Location: channel.php");
}	

include("header.inc.php");



?>
<!-- start content-outer -->
<div id="content-outer">
<!-- start content -->
<div id="content">
<?php if(isset($sus)){?>
	<!--  start message-green -->
	<div id="message-green">
		<table border="0" width="100%" cellpadding="0" cellspacing="0">
		<tr>
			<td class="green-left"><?php echo $sus; ?></td>
			<td class="green-right"><a class="close-green"><img src="images/icon_close_green.gif"   alt="" /></a></td>
		</tr>
		</table>
	</div>
	<?php } ?>
<div id="page-heading"><h1><span style="color: #d74343; font-family: Tahoma; font-weight: bold;"><?php if($_REQUEST['action']=='add'){ echo 'Add Retailer Channel';} else if($_REQUEST['action']=='edit'){ echo 'Edit Retailer Channel';} else if($action=='import'){ echo 'City Import';} else {?>Add Channel<?php } ?></span></h1></div>
<table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">
<tr>
<td>
	<!--  start content-table-inner -->
	<div id="content-table-inner">

	<table border="0" width="100%" cellpadding="0" cellspacing="0">
	
	<tr valign="top">
	<td>
		<!-- start id-form -->
		<?php  //echo $_REQUEST['action'];
		      switch ($_REQUEST['action']) {
			  case 'add':
				include('channel/retailerchannel/action/add.php');
				break;
			  case 'edit':
				include('channel/retailerchannel/action/add.php');
				break;
				case 'delete':
				include("salesman/edit_salesman.php");
				break;
			  case 'import':
				include("city/import.php");
				break;
			  case 'export':
				include("salesman/export_salesman.php");
				break;
			  /*---------------------------------------------
			     Login Form Case:
			   ----------------------------------------------*/
			 case 'index':
			 include("city/view.php");
			 break;		
			 		
			 default:
			 include('channel/retailerchannel/action/index.php');
			 break;
				
		} ?>
		<!-- end id-form  -->
	</td>
	<td>
	<!-- right bar-->
	
	</td>
	</tr>
<tr>
<td><img src="images/blank.gif" width="695" height="1" alt="blank" /></td>
<td></td>
</tr>
</table>
<div class="clear"></div>
</div>
<!--  end content-table-inner  -->
</td>
<td id="tbl-border-right"></td>
</tr>
</table>
<div class="clear">&nbsp;</div>
</div>
<!--  end content -->
<div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->
<div class="clear">&nbsp;</div> 
<!-- start footer --> 
   
<?php include("footer.php");?>
<!-- end footer -->
</body>
</html>
