<?php
/*
* Created Date 21 jan 2016
* By Abhishek Jaiswal
*/
include("includes/config.inc.php");
include("includes/function.php");
include("includes/globalarraylist.php");


if(isset($_POST['export']) && $_POST['export']!="")
{
header("Location:export.inc.php?export_sal_checkin_checkout");
exit;
}   
?>
<?php
include("header.inc.php");
include("customer_map_check_inout.php"); ?>
<script src="javascripts/dateNextPrev.js" type="text/javascript"></script>
<script src="javascripts/jquery-1.8.2.js" type="text/javascript"></script>
<script src="javascripts/jquery-ui.js"></script>
<script type="text/javascript" src="javascripts/validate.js"></script>


<script>
    $(function() {
        $( "#from" ).datepicker({
            dateFormat: "d M yy",
            defaultDate: "w",
            changeMonth: true,
            numberOfMonths: 1,
            onSelect: function( selectedDate ) {
                $( "#to" ).datepicker( "option", "minDate", selectedDate );
            }
        });
        $( "#to" ).datepicker({
            dateFormat: "d M yy",
            defaultDate: "-w",
            changeMonth: true,
            numberOfMonths: 1,
            onSelect: function( selectedDate ) {
                $( "#from" ).datepicker( "option", "maxDate", selectedDate );
            }
        });
    });
</script>


<!-- start content-outer -->
<div id="content-outer">
<!-- start content -->
<div id="content">
    <div id="page-heading">
        <h1>
            <span style="color: #d74343; font-weight: bold;">Salesman Check-In Check-Out </span>
        </h1>
    </div>
    <div class="map_wrap">
        <div class="siderbarmap" style="padding-bottom:20px;">
        <form name="frmPre" id="frmPre" method="post" action="" enctype="multipart/form-data" >
            <table border="0" width="100%" cellpadding="0" cellspacing="0">
                <tr>
                    <!-- <td>
                        <h3>Customer: </h3>
                        <h6>
                            <select name="customer" id="customer" class="menulist">
                                <option value="all" <?php echo ($_SESSION['findCust'] =="all")?'selected':''?>>All</option>
                                <option value="retailer" <?php echo ($_SESSION['findCust'] =="retailer")?'selected':''?>>Retailer</option>
                                <option value="distributor" <?php echo ($_SESSION['findCust'] =="distributor")?'selected':''?>>Distributor</option>
                                <option value="customer" <?php echo ($_SESSION['findCust'] =="customer")?'selected':''?>>Shakti-Partner</option>
                            </select>
                        </h6>
                    </td> -->

                    
                    <!-- AJAY@2016-04-06 -->
                    <td>
                        <h3>Salesman: </h3>
                        <h6>
                        <select name="sal" id="sal" class="menulist" >
                            <option value="" >Select</option>
                            <?php $aSal=$_objAdmin->_getSelectList('table_salesman AS s','*',''," status='A' $salesman ORDER BY salesman_name"); 
                            if(is_array($aSal)){
                            for($i=0;$i<count($aSal);$i++){?>
                            <option value="<?php echo $aSal[$i]->salesman_id;?>" <?php if ($aSal[$i]->salesman_id == $_SESSION['salesName']){ ?> selected <?php } ?>><?php echo $aSal[$i]->salesman_name;?></option>
                            <?php } }?>
                        </select>
                        </h6>
                    </td>
                    <td colspan="2">
                        <h3>From Date: </h3>
                        <h6>
                            <img src="css/images/prev.png" onclick="dateFromPrev();"> 
                            <input type="text" id="from" name="from" class="date" value="<?php  echo $_SESSION['fromDate'];?>"  readonly /> 
                            <img src="css/images/next.png" onclick="dateFromNext();">
                        </h6>
                    </td>
                    <td colspan="2">
                        <h3>To Date: </h3> 
                        <h6>
                            <img src="css/images/prev.png" onclick="dateToPrev();"> 
                            <input type="text" id="to" name="to" class="date" value="<?php echo $_SESSION['toDate']; ?>"  readonly />
                            <img src="css/images/next.png" onclick="dateToNext();">
                        </h6>
                    </td>
                    </tr>
                    <tr>
                    <td>
                        <h3>Customer: </h3>
                        <h6>
                        <select name="customer" id="customer" class="menulist">
                        <option value="all" <?php echo ($_SESSION['findCust'] =="all")?'selected':''?>>All</option>
                        <?php 
                        $customer_type = $_objAdmin->_getSelectList('`table_customer_type`','`cus_type_id`, `type_code`, `type_name`',''," `status` = 'A'"); 
                        if(is_array($customer_type)){
                        for($i=0;$i<count($customer_type);$i++){?>
                        <option value="<?php echo $customer_type[$i]->type_code;?>" <?php if($_SESSION['findCust']==$customer_type[$i]->type_code){ echo 'selected';} ?> ><?php echo $customer_type[$i]->type_name;?></option>
                        <?php }} ?>
                        </select>
                        </h6>
                    </td>
                    <!-- AJAY@2016-04-06 -->


                    
                    <td>
                        <h3>Active/Inactive: </h3>
                        <h6>
                            <select name="active" id="active" class="menulist">
                                <option value="all" <?php echo ($_SESSION['activeCust'] =="all")?'selected':''?>>All</option>
                                <option value="A" <?php echo ($_SESSION['activeCust'] =="A")?'selected':''?>>Active</option>
                                <option value="I" <?php echo ($_SESSION['activeCust'] =="I")?'selected':''?>>Inactive</option>
                            </select>
                        </h6>
                    </td>
                    
                    <td>
                        <h3>Division: </h3>
                        <h6>
                            <select name="division" id="division" class="menulist">
                                <option value="all"<?php echo ($_SESSION['findDivision'] =="all")?'selected':''?>>All</option>
                                <?php 
                                    foreach ($divisionRec as $key => $value) {?>
                                        <option value="<?php echo $value->division_id?>" <?php echo ($_SESSION['findDivision'] ==$value->division_id)?'selected':''?>><?php echo $value->division ?></option>
                                    <?php }
                                ?>
                            </select>
                        </h6>
                    </td>
                    <td>
                        <h3>Count: </h3>
                        <h3 id="count"><?php echo sizeof($retRec)+sizeof($distRec)+sizeof($custRec)?> </h3>
                    </td>
                </tr>
                <tr>
                    <td>
                        <h3>State: </h3>
                        <h6>
                            <select name="state" id="state" class="menulist">
                                <option value="all" <?php echo ($_SESSION['stateCust'] =="all")?'selected':''?>>All</option>
                                <?php foreach ($stateRec as $key => $value) {?>
                                    <option value="<?php echo $value->state_id?>" <?php echo ($_SESSION['stateCust'] ==$value->state_id)?'selected':''?>><?php echo $value->state_name?></option>
                                <?php }?>
                            </select>
                        </h6>
                    </td>
                    <td>
                        <h3>City: </h3>
                        <h6>
                            <select name="district" id="district" class="menulist">
                                <option value="all">All</option>
                            </select>
                        </h6>
                    </td>
                    
                    <!-- <td>
                        <h3>Search: </h3>
                        <h6>
                            <input type="text" name="find" id="find" placeholder="Type Here" class="menulist" value="<-?php echo $_POST['find'];?>"></input>
                        </h6>
                    </td> -->
                    
                    <td colspan="4">
                        <input name="showReport" class="result-submit" type="submit" id="submit" value="View" />
                        <input type="button" value="Reset!" class="form-reset" onclick="location.href='customer_geo_tagging.php?reset=yes';" />
                        <a id="dlink"  style="display:none;"></a> 
                       <input name="export" class="result-submit" type="submit" id="export" value="Export to Excel">
                    </td>
                </tr>
            </table>
        <form>
            <!-- <ul>
                <b>Retailer (<?php echo sizeof($retRec);?>)</b>&nbsp;&emsp;&nbsp;
                <input id="retailerCheckbox" type="checkbox" onclick="toggleGroup('retailer'); toggleGroup1('retailer')" checked="checked" />&nbsp;&emsp;&nbsp; <b>Distributor (<?php echo sizeof($distRec);?>) </b>&nbsp;&emsp;&nbsp;
                <input id="distributorCheckbox" type="checkbox" onclick="toggleGroup('distributor')" checked="checked" />&nbsp;&emsp;&nbsp; <b>Shakti-Partner (<?php echo sizeof($custRec);?>)</b>&nbsp;&emsp;&nbsp;
                <input id="customerCheckbox" type="checkbox" onclick="toggleGroup('customer')" checked="checked" />&nbsp;&emsp;&nbsp;
                Hot (<?php echo sizeof($custRec);?>)</b>&nbsp;&emsp;&nbsp;
                <input id="customerCheckbox" type="checkbox" onclick="toggleGroup('hot')" checked="checked" />
                Cold (<?php echo sizeof($custRec);?>)</b>&nbsp;&emsp;&nbsp;
                <input id="customerCheckbox" type="checkbox" onclick="toggleGroup('cold')" checked="checked" />
            </ul> -->
        </div>
        <table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">
            <tr>
                <td>
                    <div id="map_canvas"></div>
                </td>
            </tr>
        </table>
    </div>
    <!--  end content-table-inner  -->
    <div class="clear">&nbsp;</div>
</div>
<!--  end content -->
<div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->
<div class="clear">&nbsp;</div>        
<!-- start footer -->         
<?php include("footer.php");?>
<!-- end footer -->
<script type="text/javascript">
$(function(){
    if($("#state").val()!="all"){
        state($("#state").val());
        var dist="<?php echo $_SESSION['districtCust'];?>";
        if(dist!="all"){
            district(dist);
        }
    }
    if($("#customer").val()!="all"){
        customerClass($("#customer").val());
    }
    $("#customer").change(function(){
        var customer = $(this).val();
        customerClass(customer);
    });
    $("#state").change(function(){
        var stateID = $(this).val();
        state(stateID);
    });
    $("#district").change(function(){
        var cityID = $(this).val();
        district(cityID);
    });
});

function state(stateID){
    if(stateID!='all'){
        $.ajax({
            method:"GET",
            url: "ajax_customer_geo_tag.php",
            data: { stateID: stateID },
            success: function(result){
                $("#district").html(result);
            }
        });
    }else{
        $("#district").html("<option value='all'>All</option>");
    }
}
function district(cityID){
    if(cityID!='all'){
        $.ajax({
            method:"GET",
            url: "ajax_customer_geo_tag.php",
            data: { cityID: cityID },
            success: function(result){
                $("#tehsil").html(result);
            }
        });
    } else {
        $("#tehsil").html("<option value='all'>All</option>");
    }
}

function customerClass(customer){
    if(customer!='all'){
        $.ajax({
            method:"GET",
            url: "ajax_customer_geo_tag.php",
            data: { customer: customer },
            success: function(result){
                $("#customerclass").html(result);
            }
        });
    } else {
        $("#customerclass").html("<option value='all'>All</option>");
    }
}
</script>


</body>
</html>