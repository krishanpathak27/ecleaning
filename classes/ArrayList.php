<?php 
class ArrayList extends Db_Action {

	//public static $salArrayList = array();
	
	var $value;
	var $access;
	var $salesman;
	var $sortOrder;
	var $cityList;
	var $cond;
    public $IDarray;
	public $IDlist;
	public $distributorSalesmen;
	public $fetaurearray;
	public $ARR_MONTHS;
	public $thisYear;
	public $month_names;

	
	
	
	public function __construct() {
	
		//$this = new Db_Action();
		
		parent::__construct();
		$this->fetaurearray = array();
		$this->IDarray = array();
		$this->IDlist = array();
		$this->aSal = array();
		$this->distributorSalesmen = array();
		$this->thisYear = range(date('Y'), 2013);
		$this->ARR_MONTHS = array('01'=>'Jan','02'=>'Feb','03'=>'Mar','04'=>'Apr','05'=>'May','06'=>'Jun','07'=>'Jul','08'=>'Aug','09'=>'Sep','10'=>'Oct','11'=>'Nov','12'=>'Dec');
		$this->month_names = array("January","February","March","April","May","June","July","August","September","October","November","December");
	
	}
	
	
	public static function checkUserType() {
	
		if(isset($_SESSION['userLoginType']) && $_SESSION['userLoginType']!='') 
		
			return $_SESSION['userLoginType']; else  return false;
	
	}
	
	
	
	public  function getListofRoutes() {
	
		/*$userType = $this->checkUserType();
		
		if(isset($userType) && $userType>1) {
		
			if(isset($_SESSION['WEBUSERID']))
				$cond = " AND ur.web_user_id ='".$_SESSION['WEBUSERID']."' ";
			else 
				$cond = " AND ur.web_user_id IS NULL ";
			
		}*/
		
		
		$auRut= $this->_getSelectList('table_route AS r LEFT JOIN table_user_relationships AS ur ON ur.route_id = r.route_id ',"r.route_id, r.route_name ",''," r.status='A' $cond ");
		
		return $auRut;
		
	
	}
	
	
	public function getFilterCondForRouteSchemeIncentiveTargets($salesmanID){
	
	
	$userType = $this->checkUserType();
		
		if(isset($userType) && $userType>1) {
		
				switch($userType) {
				
					case 5 : 
						array_push($salesmanID, $_SESSION['salesmanId']);
						$IDlist = implode(',', array_filter(array_unique($salesmanID)));
		   				$this->cond =" AND s.salesman_id IN ($IDlist)";
						break;
					case 4 :
						$this->cond = " AND ur.web_user_id ='".$_SESSION['PepUpSalesUserId']."' ";
						break;
					case 3 : 
						$this->cond = " AND ur.web_user_id ='".$_SESSION['PepUpSalesUserId']."' ";
						break;	
			}
			
			return $this->cond;
		} 
	
	}
	
	
	/*public function getSalesbottomhierarchy($salID, $sortOrder) {
		//echo "<pre>";
		//print_r($salID);
		
		
		if($salID[0]!='') 
		{
				
				$Ids = implode(',', $salID);
				
				if(isset($sortOrder) && $sortOrder!=0 && $sortOrder!=''){  $sort = "AND H.sort_order >= ".$sortOrder; }
				

				$aSal=$this->_getSelectList('table_salesman AS s  LEFT JOIN table_salesman_hierarchy_relationship AS SH ON SH.salesman_id = s.salesman_id LEFT JOIN table_salesman_hierarchy AS H ON H.hierarchy_id = SH.hierarchy_id ', 's.salesman_id','',
						" SH.rpt_user_id IN ($Ids) $sort ORDER BY s.salesman_name");
						
					//print_r($aSal);
					if(is_object($aSal) || is_array($aSal)) 
					{
						//$IDlist = array_map(function($obj){  return $IDlist[] = $obj->salesman_id; }, $aSal);
						
						$IDlist = $this->returnObjectArrayToIndexArray($aSal, 'salesman_id');
						
						//print_r($IDlist);
						//$list = array(100);
						//$this->IDarray = array_merge($this->IDarray, $IDlist);
						//print_r($this->IDarray);
						$this->getSalesbottomhierarchy($IDlist, $sortOrder);
						
					}  
			} 
				
			return 	$this->IDarray = array_merge($this->IDarray, $salID);		
		}*/
	
	
	public function getSalesbottomhierarchy($salID, $sortOrder) {
		 /* echo "<pre>";
		  print_r($salID);
		  echo $sortOrder;
		  echo "-------------------------------------------<br>";*/
  
		  if($salID[0]!='') 
		  {
			
		   $Ids = implode(',', $salID);
		   
			
		   //if(isset($sortOrder) && $sortOrder!=0 && $sortOrder!=''){  $sort = "AND H.sort_order >= ".$sortOrder; }
		   
		   $aSal=$this->_getSelectList('table_salesman AS s  
		   LEFT JOIN table_salesman_hierarchy_relationship AS SH ON SH.salesman_id = s.salesman_id
		   LEFT JOIN table_salesman_hierarchy AS H ON H.hierarchy_id = SH.hierarchy_id
		   LEFT JOIN table_salesman_hierarchy AS H2 ON H2.hierarchy_id = SH.rpt_hierarchy_id AND H2.sort_order < H.sort_order 
		   LEFT JOIN table_salesman AS S2 ON S2.salesman_id = SH.rpt_user_id  AND H2.sort_order < H.sort_order ', 's.salesman_id, H.sort_order ',''," SH.rpt_user_id IN ($Ids) $sort ORDER BY s.salesman_name");
			  
		  
		  // print_r($aSal);
			 if(is_object($aSal) || is_array($aSal)) 
			 {
			 
			  foreach ($aSal as $key=>$val) :
				$IDlist = array ();
			   if($val->sort_order > $sortOrder)  {
			  
			   $IDlist[] = $val->salesman_id;
			   /*print_r($IDlist);
			   exit;*/
		
				$this->getSalesbottomhierarchy($IDlist, $val->sort_order);					
			   }			  
			  endforeach ;
		
			 }  
		   } 
    
  		 return  $this->IDarray = array_merge($this->IDarray, $salID);
		}
	
	
	public function getsaleteamID() {
		
		if(isset($_SESSION['salesmanId']) && $_SESSION['salesmanId']!=''  && $_SESSION['salesmanId']!=0) 
		
			return array($_SESSION['salesmanId']); else  return false;
	}

	//created by Maninder on 08th March 2017
	public function getServiceManID() {
		
		if(isset($_SESSION['servicePersonnelId']) && $_SESSION['servicePersonnelId']!=''  && $_SESSION['servicePersonnelId']!=0) 
		
			return array($_SESSION['servicePersonnelId']); else  return false;
	}
	
	
	public function getdistributorteamID() {
		
		if(isset($_SESSION['distributorId']) && $_SESSION['distributorId']!='' && $_SESSION['distributorId']!=0) 
		
			return array($_SESSION['distributorId']); else  return false;
	}

	
	public function getdealerteamID() {
		
		if(isset($_SESSION['retailerId']) && $_SESSION['retailerId']!='' && $_SESSION['retailerId']!=0) 
		
			return array($_SESSION['retailerId']); else  return false;
	}
	
	
	
	public function getSortOrderofsalesTeam($SalID) {
	
		$getSortOrder = $this->_getSelectList('table_salesman AS s  
		LEFT JOIN table_salesman_hierarchy_relationship AS SH ON SH.salesman_id = s.salesman_id 
		LEFT JOIN table_salesman_hierarchy AS H ON H.hierarchy_id = SH.hierarchy_id ', 
		'H.sort_order',''," s.salesman_id = ".$SalID." ORDER BY s.salesman_name");
		
		if(is_array($getSortOrder) && isset($getSortOrder[0]->sort_order)) 
			return $getSortOrder[0]->sort_order; else return $sort_order = false;
							
	}


	//created by Maninder on 08th March 2017
	public function getSortOrderofserviceTeam($SalID) {
	
		$getSortOrder = $this->_getSelectList('table_service_personnel AS s  
		LEFT JOIN table_service_personnel_hierarchy_relationship AS SH ON SH.service_personnel_id = s.service_personnel_id 
		LEFT JOIN table_service_personnel_hierarchy AS H ON H.hierarchy_id = SH.hierarchy_id ', 
		'H.sort_order',''," s.service_personnel_id = ".$SalID." ORDER BY s.salesman_name");
		
		if(is_array($getSortOrder) && isset($getSortOrder[0]->sort_order)) 
			return $getSortOrder[0]->sort_order; else return $sort_order = false;
							
	}
	
	
	

	public function SalesmanArrayList() {
	
		
		//$this = new Db_Action();
		$value =  $this->checkUserType();
		
		switch ($value) {
		
				// If User Type Admin
			case 0 : 
				
				$salArrayList=$this->_getSelectList('table_salesman AS s', 'salesman_id,salesman_name','',
					" s.status!='D' ORDER BY s.salesman_name");
				break;
				// If User Type Account
			case 1 : 
			
				$aSal = $this->_getSelectList('table_salesman AS s', 'salesman_id','',
					" s.status!='D' ORDER BY s.salesman_name");
				
				//$list = array_map(function($obj){  return $IDlist[] = $obj->salesman_id; }, $aSal);
				
				$list = $this->returnObjectArrayToIndexArray($aSal, 'salesman_id');
				
				return $list;
					
				break;
				// If User Type Company User
			case 2 : 
				$aSal=$this->_getSelectList('table_salesman AS s', 'salesman_id,salesman_name','',
					" s.status!='D' ORDER BY s.salesman_name");
				$list = $this->returnObjectArrayToIndexArray($aSal, 'salesman_id');
				return $list;
				break;
				// If User Type Distributor
			case 3 : 
			
				$res = $this->getdistributorteamID();
				//print_r($res);
				if($res) {
				
				$aSal = $this->_getSelectList('table_order AS O 
					LEFT JOIN table_salesman AS S ON S.salesman_id = O.salesman_id', 'DISTINCT(S.salesman_id)','',
					" S.status!='D'and O.distributor_id = ".$res[0]." ORDER BY S.salesman_name");
				
				
				
				$list = $this->returnObjectArrayToIndexArray($aSal, 'salesman_id');
				
				//$list = array_map(function($obj){  return $IDlist[] = $obj->salesman_id; }, $aSal);
				
				}

				
				//return array($list,$distributorSalesmen);
				return $list;
				break;
				// If User Type Retailer
			case 4 : 


                         
               $res = $this->getdealerteamID();
				//print_r($res);
				if($res) {
				
				$retailer_user = $this->_getSelectList('table_order AS O 
					LEFT JOIN table_salesman AS S ON S.salesman_id = O.salesman_id', 'DISTINCT(S.salesman_id)','',
					" S.status!='D'and O.retailer_id = ".$res[0]." ORDER BY S.salesman_name");
				
				
				
				$list = $this->returnObjectArrayToIndexArray($retailer_user, 'salesman_id');
                    }
                 	return $list;








			//	$salArrayList=$this->_getSelectList('table_salesman AS s', 'salesman_id,salesman_name','',
				//	" s.status!='D' ORDER BY s.salesman_name");
				break;
				// If User Type Salesman
			case 5 : 
			
				$res = $this->getsaleteamID();
				
				if($res) 
				{	
				// Call the ModuleClass method   
				$_objModuleClass = new ModuleClass();
				$fetaurearray = $_objModuleClass->getEnableModuleList();
				//print_r($fetaurearray);
				//Check hierarchy modules enabled or not
				if(in_array(1,$fetaurearray)){
					
					//print_r($res);
						
						$sort_order = $this->getSortOrderofsalesTeam($res[0]);
							
						$list = $this->getSalesbottomhierarchy($res, $sort_order);
						// If a salesman Login
						if(empty($list)) { $list = array($_SESSION['salesmanId']);  } 
						return $list;
				} else {
				
						// If a salesman Login
						if(empty($list)) { $list = array($_SESSION['salesmanId']);  } 
						return $list;
						
						
					} 
				} 

				break;
				// If User Type QA
			case 6 : 
					$salArrayList=$this->_getSelectList('table_salesman AS s', 'salesman_id,salesman_name','',
					" s.status!='D' ORDER BY s.salesman_name");
				
				break;
			case 7:	//for Warehouse user

                    $warehouse_user=$this->_getSelectList('table_salesman AS s', 'salesman_id,salesman_name','',
					" s.status!='D' ORDER BY s.salesman_name");
				$list = $this->returnObjectArrayToIndexArray($warehouse_user, 'salesman_id');
				return $list;


				break;



			case 8:	//for Service Personnel User
				break;


				
			case 9:	//for Service Distributor
				break;


				
			case 10:	//for Service Distributor Service Personnel
				break;


			 default:
       			echo "Sorry no user type exists!";
				break;
		} // End of Switch case
		
		
		 
		// Check array isn't empty
		if(is_array($salArrayList)){
		
			// Return salesmen arraylist
			return  $salArrayList;
			
		} else {
			
			return  $salArrayList;
		
		}
	
	}
	
	
	
	public function GetSalesmenMenu($salArray, $sals_ses_ID, $flexiLoad) {
	
	if(is_array($salArray) && !empty($salArray))
	{
	
		$Ids = implode(',', array_unique($salArray));
		
		$res = $this->getsaleteamID();
		
		// Call the ModuleClass method   
		$_objModuleClass = new ModuleClass();
		$fetaurearray = $_objModuleClass->getEnableModuleList();
		
		//Check hierarchy modules enabled
		if(in_array(1,$fetaurearray) && $this->getdistributorteamID() === false)
		{
			//  Get Salesmen Team ID	
			
			if(!empty($res) && is_array($res) && $res[0]!=0) {
			
				$auRec = $this->_getSelectList('table_salesman as sl  
						LEFT JOIN table_salesman_hierarchy_relationship AS SH ON SH.salesman_id = sl.salesman_id ',
							"SH.hierarchy_id",''," sl.salesman_id=".$_SESSION['salesmanId']." and sl.status='A'");
				
				if(isset($auRec[0]->hierarchy_id) && $auRec[0]->hierarchy_id!=0 && $auRec[0]->hierarchy_id!='') {
				
					$usertype = $this->_getSelectList('table_salesman_hierarchy','hierarchy_id,description','',"  
					sort_order >= (SELECT sort_order FROM table_salesman_hierarchy WHERE hierarchy_id = ".$auRec[0]->hierarchy_id.") 					AND status = 'A' ORDER BY sort_order ASC");
				} 
					
			}  else {
			
				$usertype = $this->_getSelectList('table_salesman_hierarchy','hierarchy_id,description','',"
				 status = 'A' ORDER BY sort_order ASC");
				 
			 $usertype[] = (object) array('hierarchy_id'=>'NULL', 'description'=>'Default Level(Salesmen)'); 
			}
		
				
		if(is_array($usertype))
		{
			if($flexiLoad == 'flex')
				$data.= '<option value="All" >All</option>';		
			else if($flexiLoad == 'root')
				$data.= '<option value="">Please Select</option>';	
			else 
				$data.= '<option value="">All</option>';
				
			foreach($usertype as $uval):
			
				if($uval->hierarchy_id == 'NULL') {
					
					$condition = "AND SH.hierarchy_id IS NULL";
				
				} else {
					$condition = "AND SH.hierarchy_id = ".$uval->hierarchy_id."";
				}
					
				$data.= "<optgroup label='".$uval->description."'>";
					
				$aSal= $this->_getSelectList('table_salesman AS s  LEFT JOIN table_salesman_hierarchy_relationship AS SH ON SH.salesman_id = s.salesman_id ', 's.salesman_id, salesman_name','',
												" s.salesman_id IN ($Ids) ".$condition."
													and s.status='A' ORDER BY s.salesman_name");
				if(is_array($aSal))
				{
					foreach($aSal as $key=>$value):
							
						if($value->salesman_id == $sals_ses_ID){ $selected = 'selected="selected"'; } else{ $selected = ""; }
							
						$data.= "<option value=".$value->salesman_id." ".$selected." >$value->salesman_name</option>";
							
					endforeach;
				}
						
				$data.= "</optgroup>";
					
			endforeach;
		}  else {
			
			$aSal= $this->_getSelectList('table_salesman AS s', 's.salesman_id, salesman_name',''," s.status!='D' and s.salesman_id=".$_SESSION['salesmanId']);
				if(is_array($aSal))
				{
					foreach($aSal as $key=>$value):
							
						if($value->salesman_id == $sals_ses_ID){ $selected = 'selected="selected"'; } else{ $selected = ""; }
							
						$data.= "<option value=".$value->salesman_id." ".$selected." >$value->salesman_name</option>";
							
					endforeach;
				}
		}
		
		} else if(!empty($res) && is_array($res) && $res[0]!=0) { // If a salesman logged IN
		
			$aSal= $this->_getSelectList('table_salesman AS s', 's.salesman_id, salesman_name',''," s.status!='D' and  s.salesman_id=".$_SESSION['salesmanId']);
				if(is_array($aSal))
				{
					foreach($aSal as $key=>$value):
							
						if($value->salesman_id == $sals_ses_ID){ $selected = 'selected="selected"'; } else{ $selected = ""; }
							
						$data.= "<option value=".$value->salesman_id." ".$selected." >$value->salesman_name</option>";
							
					endforeach;
				}
		
		
		}  else {  // End of check salesmen List array
		
			if($flexiLoad == 'flex')
				$data.= '<option value="All" >All</option>';		
			else if($flexiLoad == 'root')
				$data.= '<option value="">Please Select</option>';	
			else 
				$data.= '<option value="">All</option>';
				
			$aSal= $this->_getSelectList('table_salesman AS s', 's.salesman_id, salesman_name',''," s.status!='D' and   s.salesman_id IN ($Ids)  ORDER BY s.salesman_name");
				if(is_array($aSal))
				{
					foreach($aSal as $key=>$value):
							
						if($value->salesman_id == $sals_ses_ID){ $selected = 'selected="selected"'; } else{ $selected = ""; }
							
						$data.= "<option value=".$value->salesman_id." ".$selected." >$value->salesman_name</option>";
							
					endforeach;
				}
		
		
		}
		} else {
		
			$data.= '<option value="" >No salesman</option>';
		
		}
		
		return $data;
		
	} // Method End brace


		// start service person code   //
	
	public function GetServicemenMenu($salArray, $sals_ses_ID, $flexiLoad) {
	
	if(is_array($salArray) && !empty($salArray))
	{
	
		$Ids = implode(',', array_unique($salArray));
		
		$res = $this->getsaleteamID();
		
		// Call the ModuleClass method   
		$_objModuleClass = new ModuleClass();
		$fetaurearray = $_objModuleClass->getEnableModuleList();
		
		//Check hierarchy modules enabled
		if(in_array(1,$fetaurearray) && $this->getdistributorteamID() === false)
		{
			//  Get Salesmen Team ID	
			
			if(!empty($res) && is_array($res) && $res[0]!=0) {
			
				$auRec = $this->_getSelectList('table_service_personnel as sl  
						LEFT JOIN table_service_personnel_hierarchy_relationship AS SH ON SH.service_personnel_id= sl.service_personnel_id',
							"SH.hierarchy_id",''," sl.service_personnel_id=".$_SESSION['servicePersonnelId']." and sl.status='A'");
				
				if(isset($auRec[0]->hierarchy_id) && $auRec[0]->hierarchy_id!=0 && $auRec[0]->hierarchy_id!='') {
				
					$usertype = $this->_getSelectList('table_service_personnel_hierarchy','hierarchy_id,description','',"  
					sort_order >= (SELECT sort_order FROM table_service_personnel_hierarchy WHERE hierarchy_id = ".$auRec[0]->hierarchy_id.") AND status = 'A' ORDER BY sort_order ASC");
				} 
					
			}  else {
			
				$usertype = $this->_getSelectList('table_service_personnel_hierarchy','hierarchy_id,description','',"
				 status = 'A' ORDER BY sort_order ASC");
				 
			 $usertype[] = (object) array('hierarchy_id'=>'NULL', 'description'=>'Default Level(Service person)'); 
			}
		
				
		if(is_array($usertype))
		{
			if($flexiLoad == 'flex')
				$data.= '<option value="All" >All</option>';		
			else if($flexiLoad == 'root')
				$data.= '<option value="">Please Select</option>';	
			else 
				$data.= '<option value="">All</option>';
				
			foreach($usertype as $uval):
			
				if($uval->hierarchy_id == 'NULL') {
					
					$condition = "AND SH.hierarchy_id IS NULL";
				
				} else {
					$condition = "AND SH.hierarchy_id = ".$uval->hierarchy_id."";
				}
					
				$data.= "<optgroup label='".$uval->description."'>";
					
				$aSal= $this->_getSelectList('table_service_personnel AS s  LEFT JOIN table_service_personnel_hierarchy_relationship AS SH ON SH.service_personnel_id = s.service_personnel_id ', 's.service_personnel_id, sp_name','',
												" s.service_personnel_id IN ($Ids) ".$condition."
													and s.status='A' ORDER BY s.sp_name");
				if(is_array($aSal))
				{
					foreach($aSal as $key=>$value):
							
						if($value->service_personnel_id == $sals_ses_ID){ $selected = 'selected="selected"'; } else{ $selected = ""; }
							
						$data.= "<option value=".$value->service_personnel_id." ".$selected." >$value->sp_name</option>";
							
					endforeach;
				}
						
				$data.= "</optgroup>";
					
			endforeach;
		}  else {
			
			$aSal= $this->_getSelectList('table_service_personnel AS s', 's.service_personnel_id, sp_name',''," s.status!='D' and s.service_personnel_id=".$_SESSION['servicePersonnelId']);
				if(is_array($aSal))
				{
					foreach($aSal as $key=>$value):
							
						if($value->service_personnel_id == $sals_ses_ID){ $selected = 'selected="selected"'; } else{ $selected = ""; }
							
						$data.= "<option value=".$value->service_personnel_id." ".$selected." >$value->sp_name</option>";
							
					endforeach;
				}
		}
		
		} else if(!empty($res) && is_array($res) && $res[0]!=0) { // If a salesman logged IN
		
			$aSal= $this->_getSelectList('table_service_personnel AS s', 's.service_personnel_id, sp_name',''," s.status!='D' and  s.service_personnel_id=".$_SESSION['servicePersonnelId']);
				if(is_array($aSal))
				{
					foreach($aSal as $key=>$value):
							
						if($value->service_personnel_id == $sals_ses_ID){ $selected = 'selected="selected"'; } else{ $selected = ""; }
							
						$data.= "<option value=".$value->service_personnel_id." ".$selected." >$value->sp_name</option>";
							
					endforeach;
				}
		
		
		}  else {  // End of check service person  List array
		
			if($flexiLoad == 'flex')
				$data.= '<option value="All" >All</option>';		
			else if($flexiLoad == 'root')
				$data.= '<option value="">Please Select</option>';	
			else 
				$data.= '<option value="">All</option>';
				
			$aSal= $this->_getSelectList('table_service_personnel AS s', 's.service_personnel_id, sp_name',''," s.status!='D' and   s.service_personnel_id IN ($Ids)  ORDER BY s.sp_name");
				if(is_array($aSal))
				{
					foreach($aSal as $key=>$value):
							
						if($value->service_personnel_id == $sals_ses_ID){ $selected = 'selected="selected"'; } else{ $selected = ""; }
							
						$data.= "<option value=".$value->service_personnel_id." ".$selected." >$value->sp_name</option>";
							
					endforeach;
				}
		
		
		}
		} else {
		
			$data.= '<option value="" >No service person</option>';
		
		}
		
		return $data;
		
	} 
	

	// end of service person code //
	
	

	public function getSalesCondition($salsList) {
	
		 if(is_array($salsList) && !empty($salsList) && $salsList[0]!='') {
		   
		   	$Ids = implode(',', array_unique($salsList));
		   	$salesman =" AND s.salesman_id IN ($Ids)";
			return $salesman;
		} else {
			$salesman =" AND s.salesman_id IS NULL";
			return $salesman;
		}
	}
	

	
	public function checkValidSalesmen($salsList, $sid) {
	
		if(in_array($sid, $salsList)) 
			return true;
		else 
			return false;
	
	}
	
	
	public function getHierarchyIDoFSalesTeam($salesID) {
	
		if(isset($salesID) && $salesID!=0){
		
		$ausalesmen =$this->_getSelectList('table_salesman AS S LEFT JOIN table_salesman_hierarchy_relationship AS SH ON SH.salesman_id = S.salesman_id ',"SH.hierarchy_id",''," S.salesman_id = ".$salesID."  AND S.status!='D'");
			
			
			if(isset($ausalesmen[0]->hierarchy_id) && $ausalesmen[0]->hierarchy_id!='')
			{
				return $ausalesmen[0]->hierarchy_id;
			}
		} else {
		
			return false;
		
		}
	}

	//created by Maninder on 08th March 2017
	public function getHierarchyIDoFServiceTeam($salesID) {
	
		if(isset($salesID) && $salesID!=0){
		
		$ausalesmen =$this->_getSelectList('table_service_personnel AS S LEFT JOIN table_service_personnel_hierarchy_relationship AS SH ON SH.service_personnel_id = S.service_personnel_id ',"SH.hierarchy_id",''," S.service_personnel_id = ".$salesID."  AND S.status!='D'");
			
			
			if(isset($ausalesmen[0]->hierarchy_id) && $ausalesmen[0]->hierarchy_id!='')
			{
				return $ausalesmen[0]->hierarchy_id;
			}
		} else {
		
			return false;
		
		}
	}
	
	
	public function getMonthList($selectedMonth) {

		$data.= "<h3>Month: </h3><h6>";
		$data.='<select name="month" class="menulist">';
		$data.="<option>Select Month</option>";
		foreach($this->ARR_MONTHS as $key=>$value):
			if($key == $selectedMonth){ $selected = "selected"; } else{ $selected = ""; }
			$data.="<option value=".$key." $selected>".$value."</option>";
		endforeach;
		$data.="</select>";
		$data.= "</h6>";
		return $data;
	
	}
	
	
	public function getMonthList2($selectedMonth) {

		$data.="<option>Select Month</option>";
		foreach($this->month_names as $key=>$value):
			if($value == $selectedMonth){ $selected = "selected"; } else{ $selected = ""; }
			$data.="<option value=".$value." $selected>".$value."</option>";
		endforeach;
		return $data;
	
	}
	
	
	public function getMonthList3($selectedMonth) {

		$data.="<option>Select Month</option>";
		foreach($this->ARR_MONTHS as $key=>$value):
			if($key == $selectedMonth){ $selected = "selected"; } else{ $selected = ""; }
			$data.="<option value=".$key." $selected>".$value."</option>";
		endforeach;
		return $data;
	
	}
	
	
	
	
	
	public function getYearList($selectedYear) {

		$data.= "<h3>Select Year: </h3><h6>";
		$data.='<select name="year" class="menulist">';
		$data.='<option value="">Select Year</option>';
		foreach($this->thisYear as $value):
			if($value == $selectedYear){ $selected = "selected"; } else{ $selected = ""; }
			$data.="<option value=".$value." $selected>".$value."</option>";
		endforeach;
		$data.="</select>";
		$data.= "</h6>";
		return $data;
	
	}
	
	
	public function getYearList2($selectedYear) {
		$data.='<option>Select Year</option>';
		foreach($this->thisYear as $value):
			if($value == $selectedYear){ $selected = "selected"; } else{ $selected = ""; }
				$data.="<option value=".$value." $selected>".$value."</option>";
		endforeach;
		return $data;
	
	}
	
	/*Here code starts*/
	public function getCustomerType($cust_type=NULL) {
		
		$custTypeList =  $this->_getSelectList2('table_customer_type','cus_type_id,type_name',''," type_name!='' ORDER BY type_name"); 
		
		if(is_array($custTypeList)){
		$data.="<option value='All'>All</option>";
		foreach($custTypeList as $value):
			if($cust_type==NULL){
				$data.="<option value=".$value->cus_type_id." $selected>".$value->type_name."</option>";
			} else {
				if($value->cus_type_id == $cust_type){ $selected = "selected"; } else{ $selected = ""; }
				$data.="<option value=".$value->cus_type_id." $selected>".$value->type_name."</option>";
			}
		endforeach;
		} else {		
			$data.= '<option value="" >No Customer Type</option>';
		}
		return $data;	
	}
	
	
	
	
	
	
	
	public function getCityList($selectedCity) {
	
		$data.= "<h3>District: </h3><h6>";
		$data.='<select name="city" class="menulist">';
		
		$cityList =  $this->_getSelectList2('city','city_id,city_name',''," city_name!='' ORDER BY city_name"); 
		
		if(is_array($cityList)){
		$data.="<option value=''>All</option>";
		foreach($cityList as $value):
			if($value->city_id == $selectedCity){ $selected = "selected"; } else{ $selected = ""; }
			$data.="<option value=".$value->city_id." $selected>".$value->city_name."</option>";
		endforeach;
		} else {
		
			$data.= '<option value="" >No City Available</option>';
		
		}
		$data.="</select>";
		$data.= "</h6>";
		return $data;
	
	}
	
	
	/************ Added for take order on 6th june 2014 ******************/
	
	public function getStateList($selectedState) {
	
	  $value =  $this->checkUserType();
	  
	   // for distributor 
	    if($value==3)
         {
		 $statevalue =  $this->_getSelectList2('table_distributors','distributor_id,state','',"distributor_name!='' and distributor_id=".$_SESSION['distributorId']." ORDER BY distributor_name");
		 $state=$statevalue[0]->state;
		 $stateList =  $this->_getSelectList2('state','state_id,state_name','',"state_name!='' and state_id=".$state."  ORDER BY state_name"); 	   	  
		 }
		 else
		 {
		$stateList =  $this->_getSelectList2('state','state_id,state_name',''," state_name!='' ORDER BY state_name");  
		 }
	    
		
		if(is_array($stateList)){
		if($value!=3)
		{
		if($flexiLoad == 'flex')
				$data.= '<option value="All" >All</option>';		
			else if($flexiLoad == 'state')
				$data.= '<option value="">Please Select</option>';	
			else 
				$data.= '<option value="">Please Select</option>';
		 }		
		foreach($stateList as $value):
			if($value->state_id == $selectedState){ $selected = "selected"; } else{ $selected = ""; }
			$data.="<option value=".$value->state_id." $selected>".$value->state_name."</option>";
		endforeach;
		} else {
		
			$data.= '<option value="" >No State Available</option>';
		
		}
		return $data;
	
	}
	
	/************ Added for take order on 6th june 2014 ******************/
	
	
	
	
	public function getCityListOptions($selectedCity,$flexiLoad ) {
		
		$cityList =  $this->_getSelectList2('city','city_id,city_name',''," city_name!='' ORDER BY city_name"); 
		if(is_array($cityList)){
		if($flexiLoad == 'flex')
				$data.= '<option value="All" >All</option>';		
			else if($flexiLoad == 'city')
				$data.= '<option value="">Please Select</option>';	
			else 
				$data.= '<option value="">All</option>';
		foreach($cityList as $value):
			if($value->city_id == $selectedCity){ $selected = "selected"; } else{ $selected = ""; }
			$data.="<option value=".$value->city_id." $selected>".$value->city_name."</option>";
		endforeach;
		} else {
		
			$data.= '<option value="" >No City Available</option>';
		
		}
		return $data;
	
	}
	
	
	public function getStateListOptions($selectedState,$flexiLoad ) {
		
		$stateList =  $this->_getSelectList2('state','state_id,state_name',''," state_name!='' ORDER BY state_name"); 
		if(is_array($stateList)){
		if($flexiLoad == 'flex')
				$data.= '<option value="All" >All</option>';		
			else if($flexiLoad == 'state')
				$data.= '<option value="">Please Select</option>';	
			else 
				$data.= '<option value="">All</option>';
		foreach($stateList as $value):
			if($value->state_id == $selectedState){ $selected = "selected"; } else{ $selected = ""; }
			$data.="<option value=".$value->state_id." $selected>".$value->state_name."</option>";
		endforeach;
		} else {
		
			$data.= '<option value="" >No State Available</option>';
		
		}
		return $data;
	
	}
	
	
	
	
	
	
	
 	public function checkAccess($pageAccess, $pageName) {  
 		
		//$userTypeSalesTeamID = $this->getsaleteamID();		
//		$hierarchyIDofloggedUser = $this->getHierarchyIDoFSalesTeam($userTypeSalesTeamID[0]); 	
//		/*$userType = " user_type='".$_SESSION['userLoginType']."' and hierarchy_id='".$hierarchyIDofloggedUser."'";
// 		$pageGroup=$this->_getselectList2('table_page_access AS A INNER JOIN table_cms AS C ON C.id = A.page_privilege_id','page_privilege_id,permisson_id, C.page_name, C.id',''," $userType AND C.page_name = '".$pageName."'");*/
//			$pageID=$this->_getselectList2('table_cms','parentPage',''," page_name='".$pageName."'");	
//			
//			
//	$userType = " user_type='".$_SESSION['userLoginType']."' and hierarchy_id='".$hierarchyIDofloggedUser."' AND  page_privilege_id = '".$pageID[0]->parentPage."'";
//			$pageGroup=$this->_getselectList('table_page_access AS A INNER JOIN table_cms AS C ON C.id = A.page_privilege_id','page_privilege_id,permisson_id, C.page_name, C.id',''," $userType");		
//		
//		//for($i=0;$i<count($pageGroup);$i++){
//		 			//echo $pageGroup[$i]->page_access_id;
//					
//						$allowedAccess = explode( ',',$pageGroup[0]->permisson_id);
//	 							if (in_array($pageAccess,$allowedAccess)) {
//   										$access="1";
//									}
//								else{
//										//$_SESSION['PageAccess']="True";
//										$access="1";
//									}
//								
//			//}					
//			
//			return $access;


$userTypeSalesTeamID = $this->getsaleteamID();		
		$hierarchyIDofloggedUser = $this->getHierarchyIDoFSalesTeam($userTypeSalesTeamID[0]); 	
		/*$userType = " user_type='".$_SESSION['userLoginType']."' and hierarchy_id='".$hierarchyIDofloggedUser."'";
 		$pageGroup=$this->_getselectList2('table_page_access AS A INNER JOIN table_cms AS C ON C.id = A.page_privilege_id','page_privilege_id,permisson_id, C.page_name, C.id',''," $userType AND C.page_name = '".$pageName."'");*/
			$pageID=$this->_getselectList2('table_cms','parentPage',''," page_name='".$pageName."'");	
			$pageAccessID=$this->_getselectList('table_module_enable','status',''," feature_id='2'");		
			$access_enable=$pageAccessID[0]->status;


		   if($_SESSION['userLoginType'] == 5) {
			 $hierarchyIDofloggedUser = $this->getHierarchyIDoFSalesTeam($userTypeSalesTeamID[0]);  
			 $hierarchyCondition = " and hierarchy_id='".$hierarchyIDofloggedUser."'";
		   } else {
		   	$hierarchyCondition = "";
		   }

			$userType = " user_type='".$_SESSION['userLoginType']."' $hierarchyCondition AND  page_privilege_id = '".$pageID[0]->parentPage."'";
			$pageGroup=$this->_getselectList('table_page_access AS A INNER JOIN table_cms AS C ON C.id = A.page_privilege_id','page_privilege_id,permisson_id, C.page_name, C.id',''," $userType");		
		
		//for($i=0;$i<count($pageGroup);$i++){
		 			//echo $pageGroup[$i]->page_access_id;
					if($access_enable=='' || $access_enable==0){$access="1";}
					else{
						$allowedAccess = explode( ',',$pageGroup[0]->permisson_id);
	 							if (in_array($pageAccess,$allowedAccess)) {
   										$access="1";
									}
								else{
										$_SESSION['PageAccess']="True";
										$access="0";
									}
								}
			//}					
			
			return $access;

		}
		
		
// function to get salesman list for consolidated report 1 Sep 2014

	public function getSalesmanConsolidatedList($salList,$filterby){
			/*$hierarchy_id=$this->_getSelectList2('table_salesman_hierarchy_relationship',"hierarchy_id",''," salesman_id='".$salList."' ");
            $hire_id=$hierarchy_id[0]->hierarchy_id;

            $hierarchy_id=$this->_getSelectList2('table_salesman_hierarchy_relationship',"salesman_id",''," hierarchy_id <='".$hire_id."' ");

			$array=array();
			foreach ($hierarchy_id as $value) 
			{
				  $array[] = $value->salesman_id;
			}

			$salList = implode(",", $array); */

				//$Ids=$_SESSION['SalOrderList'];
		   $aSal=$this->_getSelectList('table_salesman AS s  
		   LEFT JOIN table_salesman_hierarchy_relationship AS SH ON SH.salesman_id = s.salesman_id
		   LEFT JOIN table_salesman_hierarchy AS H ON H.hierarchy_id = SH.hierarchy_id
		   LEFT JOIN table_salesman_hierarchy AS H2 ON H2.hierarchy_id = SH.rpt_hierarchy_id AND H2.sort_order < H.sort_order 
		   LEFT JOIN table_salesman AS S2 ON S2.salesman_id = SH.rpt_user_id  AND H2.sort_order < H.sort_order ', 's.salesman_id, H.sort_order ',''," SH.rpt_user_id = '".$salList."'  ORDER BY s.salesman_name");
		   
		   $array1=array();
		   foreach ($aSal as $value) 
			{
			    $array1[] = $value->salesman_id;
			}

$salLists = implode(",", $array1); 
/*
			if(is_array($array1))
			  {
			   
			}else
			{$salLists=0;
				
			}*/

			
			$_objModuleClass = new ModuleClass();
			$fetaurearray = $_objModuleClass->getEnableModuleList();
		
		//Check hierarchy modules enabled
		if(in_array(1,$fetaurearray) && $this->getdistributorteamID() === false)
		{
						if($filterby == 1){	


                               if($salLists=='')
								{
									$salesman =" AND s.salesman_id IN (".$salList.")";//Changed for Indivisual Session Value @Chirag 09-Aug-2017
									return $salesman;

								}else{	
								$salesman=" AND s.salesman_id IN (".$salLists.")"; 
								return $salesman;
								}	

								//echo $salList."--".$filterby;	
								//$salesman=" AND s.salesman_id IN (".$salLists.")"; 				
								//$salesman = " AND s.salesman_id = '".$salList."'";
								//return $salesman;
							}
						
						else {
								//echo $salList."--".$filterby;
								$IDlist[]=$salList;	
								$sort_order = $this->getSortOrderofsalesTeam($IDlist[0]);							
								$list = $this->getSalesbottomhierarchy($IDlist, $sort_order);
								//print_r($list);
								 if(is_array($list) && !empty($list) && $list[0]!='') {		   
									$Ids = implode(',', array_unique($list));
									$salesman = " AND s.salesman_id IN ($Ids)";
									return $salesman;
								} else {
									$salesman =" AND s.salesman_id IS NULL";
									return $salesman;
								}
	
							}
						}
					else{
					
									$salesman = " AND s.salesman_id = '".$salList."'";
								return $salesman;
					}
	
		}

// End function to get salesman list for consolidated report 1 Sep 2014


public function getitemListOptions($selecteditem,$flexiLoad ) {
		
		$itemList =  $this->_getSelectList('table_item','item_id,item_name',''," item_name!='' ORDER BY item_name"); 
		//print_r($itemList);//exit;
		if(is_array($itemList)){
		if($flexiLoad == 'flex')
				$data.= '<option value="All" >All</option>';		
			else if($flexiLoad == 'item')
				$data.= '<option value="">Please Select</option>';	
			else 
				$data.= '<option value="">All</option>';
		foreach($itemList as $value):
			if($value->item_id == $selecteditem){ $selected = "selected"; } else{ $selected = ""; }
			$data.="<option value=".$value->item_id." $selected>".$value->item_name."</option>";
		endforeach;
		} else {
		
			$data.= '<option value="" >No Item Available</option>';
		
		}
		return $data;
	
	}


public function getRetailerListOptions($selectedret,$flexiLoad ) {
		
		$retailerList =  $this->_getSelectList('table_retailer','retailer_id,retailer_name',''," retailer_name!='' ORDER BY retailer_name"); 
		if(is_array($retailerList)){
		if($flexiLoad == 'flex')
				$data.= '<option value="All" >All</option>';		
			else if($flexiLoad == 'retailer')
				$data.= '<option value="">Please Select</option>';	
			else 
				$data.= '<option value="">All</option>';
		foreach($retailerList as $value):
			if($value->retailer_id == $selectedret){ $selected = "selected"; } else{ $selected = ""; }
			$data.="<option value=".$value->retailer_id." $selected>".$value->retailer_name."</option>";
		endforeach;
		} else {
		
			$data.= '<option value="" >No Retailer Available</option>';
		
		}
		return $data;
	
	}


public function getDistributorListOptions($selecteddis,$flexiLoad ) {

		$userType = $this->checkUserType();
		
		if(isset($userType) && $userType>1) {
		
				switch ($userType) {
					case 3:
						# code...
					     $condition = " AND distributor_id='".$_SESSION['distributorId']."'";
						break;
						
					default:
						# code...
					    $condition = "";
						break;
				}
		}
		
		//$distributorList =  $this->_getSelectList('table_distributors','distributor_id,distributor_name',''," distributor_name!='' ORDER BY distributor_name");

		$distributorList =  $this->_getSelectList('table_distributors','distributor_id,distributor_name',''," distributor_name!='' $condition ORDER BY distributor_name");

		if(is_array($distributorList)){
		if($flexiLoad == 'flex')
				$data.= '<option value="All" >All</option>';		
			else if($flexiLoad == 'distributor')
				$data.= '<option value="">Please Select</option>';	
			else 
				$data.= '<option value="">All</option>';
		foreach($distributorList as $value):
			if($value->distributor_id == $selecteddis){ $selected = "selected"; } else{ $selected = ""; }
			$data.="<option value=".$value->distributor_id." $selected>".$value->distributor_name."</option>";
		endforeach;
		} else {
		
			$data.= '<option value="" >No Distributor Available</option>';
		
		}
		return $data;
	
	}

		
public function getDistributorInvoiceOptions($selecteddis,$flexiLoad,$wearhouseID) {
		
		//$invoiceList =  $this->_getSelectList2('table_order_invoice_assign_wh inv','inv.warhouse_id,
		//	inv.tally_inv_master_id,inv.assign_date',''," inv.dispatch_status='A'   $condition ORDER BY
			// inv.tally_inv_master_id"); 
	if($wearhouseID=='')
	{


	}else{
$invoiceList =  $this->_getSelectList2('table_order_invoice_assign_wh','distinct(tally_inv_master_id),warhouse_id,order_id','',
	" dispatch_status='A' AND warhouse_id=".$wearhouseID."  ORDER BY tally_inv_master_id");
}
		
		if(is_array($invoiceList)){
		if($flexiLoad == 'flex')
				$data.= '<option value="All" >All</option>';		
			else if($flexiLoad == 'invoice')
				$data.= '<option value="">Please Select</option>';	
			else 
				$data.= '<option value="">All</option>';
		foreach($invoiceList as $value):
			if($value->tally_inv_master_id == $selecteddis){ $selected = "selected"; } else{ $selected = ""; }
			$data.="<option value=".$value->tally_inv_master_id." $selected>".$value->tally_inv_master_id."(".$value->order_id.")"."</option>";
		endforeach;
		} else {
		
			$data.= '<option value="" >No Invoice Available</option>';
		
		}
		return $data;
	
	}



// Get the category array 15 Sep 2014 AJAY

	public function getCategoryList($catArray) {
	$category = array();
	$catIds = '';
	if(is_array($catArray) && sizeof($catArray)>0) 
	{ $catIds = implode(',',$catArray);  $cond = " category_id IN (".$catIds.") AND "; 
	$category = $this->_getSelectList('table_category','category_id, category_name','',"  $cond cat_type='C' AND status = 'A' ORDER BY category_name"); 
	}  else {
	$category = $this->_getSelectList('table_category','category_id, category_name',''," cat_type='C' AND status = 'A' ORDER BY category_name"); 
	}
	return $category;
	}

// Get the category array 15 Sep 2014 AJAY


// Get the category array 15 Sep 2014 AJAY

	public function getItemList($catId) {
	$item = array();
	$cond = '';
	
	if(isset($catId) && $catId!="" ) $cond = " category_id =".$catId." AND "; 
		
	$item = $this->_getSelectList('table_item','item_id, item_name, category_id','',"  ".$cond." status = 'A' ORDER BY item_name"); 
	return $item;
	}

// Get the category array 15 Sep 2014 AJAY


	 
	 public function changeArrayKeyName($arr,$field) {  
	 $list = array();							 
	 if(is_array($arr) && sizeof($arr)>0){  foreach($arr as $key=>$value) { $list[$value->$field] = $arr[$key]; } } 
	 return $list; 
	 }

	 
	 public function changeArrayKeyNameTwoDimArr($arr,$field) {  
	 $list = array();							 
	 if(is_array($arr) && sizeof($arr)>0 && sizeof($field)>=2){  foreach($arr as $key=>$value) { 
	 	$list[$value->$field[0]][$value->$field[1]] = $arr[$key]; } } 
	 return $list; 
	 }


	public function createSubArrayBaseOnValue($arr,$field, $findinarr) {
	 $list = array();							 
	 if(is_array($arr) && sizeof($arr)>0 && sizeof($findinarr)>=1 && isset($field) && $field!=""){  
	 //echo "hello";
	 foreach($arr as $key=>$value) :
	 	//print_r($value);
	 	foreach($findinarr as $findVal) :
			//echo $findVal;
			//print_r($value);
			if($findVal==$value->$field) { 
				//print_r($value);
	 			$list[$findVal][$key] = $value; 
			}
		endforeach;
		
	 endforeach;
	 return $list;
	 }
	}
	







/************************ month list for distributor target (1st june 2015 Gaurav) *************************/

public function getDistributorMonthList($selectedMonth) {

		
		$data.='<option value="">Select Month</option>';
		foreach($this->ARR_MONTHS as $key=>$value):
			if($key == $selectedMonth){ $selected = "selected"; } else{ $selected = ""; }
			$data.="<option value=".$key." $selected>".$value."</option>";
		endforeach;
		return $data;
	
	}

/************************ month list for distributor target (1st june 2015 Gaurav) *************************/
/************************ year list for distributor target (1st june 2015 Gaurav) *************************/

public function getDistributorYearList($selectedYear) {
		
		$data.='<option value="">Select Year</option>';
		foreach($this->thisYear as $value):
			if($value == $selectedYear){ $selected = "selected"; } else{ $selected = ""; }
			$data.="<option value=".$value." $selected>".$value."</option>";
		endforeach;
		return $data;
	
	}

/************************ year list for distributor target (1st june 2015 Gaurav) *************************/

/************************ Distributor list for distributor target (5th june 2015 Gaurav) *************************/

public function getDistributorList($selectedDistributor) {
		$distributor = $this->_getSelectList('table_distributors','distributor_id, distributor_code',''," status = 'A' ORDER BY distributor_code");
		$data.='<option value="">Select Distributor</option>';
		foreach($distributor as $value):
			if($value == $selectedDistributor){ $selected = "selected"; } else{ $selected = ""; }
			$data.="<option value=".$value->distributor_id." $selected>".$value->distributor_code."</option>";
		endforeach;
		return $data;
	
	}

/************************ Distributor list for distributor target (5th june 2015 Gaurav) *************************/



/********************************************************
* get the list of years with the help of range function
* Author : Ajay
* Created : 14th April 2015
* */

	public function getYearListRoute($startFrom, $endFrom, $selectedYear, $flexiLoad) {


		if(isset($startFrom) && is_numeric((int)$startFrom) && strlen($startFrom)==4) {

			$startFrom = $startFrom;

		} else {

			$startFrom = date('Y');
		}

		if(isset($endFrom) && is_numeric((int)$endFrom)  && strlen($endFrom)==4){

			$endFrom = $endFrom;

		} else {

			$endFrom = date('Y', strtotime('+1 year'));
		}

		$yearList = range($startFrom, $endFrom);
		//print_r($yearList);

		if($flexiLoad == 'All')
				$data.= '<option value="All" >All</option>';		
			else if($flexiLoad == 'AllNULL')
				$data.= '<option value="">All</option>';
			else if($flexiLoad == 'select')
				$data.= '<option value="">Please Select</option>';	
				
		foreach($yearList as $value):
			if($value == $selectedYear){ $selected = "selected"; } else{ $selected = ""; }
			$data.="<option value=".$value." $selected>".$value."</option>";
		endforeach;


		return $data;
	
	}
  /* 
    Description: create a function List of months 
    Aouther: Nizam
    Date : 27 August
  */

	public function Months($id)
   {
   	//echo $id;
   	  $months=array('Janaury','February','March','April','May','June','July','August','September','October','November','December');
   	           $data.='<option value="">Please Select</option>';
   	   $key=1;        
       foreach ($months as $value) {
            if($key == $id){ $selected = "selected"; } else { $selected = ""; }
			   $data.="<option value=".$key." $selected >".$value."</option>";
       	$key++;        
       }
   	 return $data;
   }
	




 /***************************************************************************
 * DESC : Get the division Ids of all the hierarchy salesmen
 * Author : AJAY
 * Created : 2015-11-18
 *
 **/


 public function getAllDivisonOfSelectedSalesmen ($salesList) {

 	$salesmenDivisions = array();
 	$data = array();

 	if(is_array($salesList) && sizeof($salesList)>0) {

 		$salesmanIds = implode(",", $salesList);

 		$salesmenDivisions = $this->_getSelectList('table_salesman','DISTINCT(division_id)',''," salesman_id IN (".$salesmanIds.")");

 		if(sizeof($salesmenDivisions)>0) {

 			foreach ($salesmenDivisions as $key => $value) {
 				# code...
 				$data[] = $value->division_id;
 			}

 		}


		return $data;


 	}

 }








 

 	// AJAY@01-03-2017 warehouse master list


	public function getWarehouseList($selecteddis,$flexiLoad, $condition) {
		
		$warehouseList =  $this->_getSelectList('table_warehouse WS','WS.warehouse_id,WS.warehouse_name',''," WS.warehouse_name!=''  $condition ORDER BY WS.warehouse_name"); 
		if(is_array($warehouseList)){
		if($flexiLoad == 'flex')
				$data.= '<option value="All" >All</option>';		
			else if($flexiLoad == 'warehouse')
				$data.= '<option value="">Please Select</option>';	
			else 
				$data.= '<option value="">All</option>';
		foreach($warehouseList as $value):
			if($_SESSION['userLoginType']==7)
			{
				if($value->warehouse_id == $_SESSION['warehouseId']){ $selected = "selected"; } else{ $selected = ""; }
			$data.="<option value=".$value->warehouse_id." $selected>".$value->warehouse_name."</option>";	
				$_SESSION['sid'] = $_SESSION['warehouseId'];
			}
			else
			{
				if($value->warehouse_id == $selecteddis){ $selected = "selected"; } else{ $selected = ""; }
			$data.="<option value=".$value->warehouse_id." $selected>".$value->warehouse_name."</option>";	
			}
			
		endforeach;
		} else {
		
			$data.= '<option value="" >No Warehouse Available</option>';
		
		}
		return $data;
	
	}






// Distributor List for dispatch stock to Deler (|@| arvind 20-04-2017)

public function getDistributorListDispatch($selecteddis,$flexiLoad, $condition) {
	
		
		$distributoreList =  $this->_getSelectList('table_distributors d','d.distributor_id,d.distributor_name',''," d.distributor_name!=''  $condition ORDER BY d.distributor_name"); 
		if(is_array($distributoreList)){
		if($flexiLoad == 'flex')
				$data.= '<option value="All" >All</option>';		
			else if($flexiLoad == 'distributors')
				$data.= '<option value="">Please Select</option>';	
			else 
				$data.= '<option value="">All</option>';
		foreach($distributoreList as $value):
			if($value->distributor_id == $selecteddis){ $selected = "selected"; } else{ $selected = ""; }
			$data.="<option value=".$value->distributor_id." $selected>".$value->distributor_name."</option>";
		endforeach;
		} else {
		
			$data.= '<option value="" >No Distributor Available</option>';
		
		}
		return $data;
	
	}


	public function getDealerListOptions($selecteddis,$flexiLoad) {

		
		$dealerList =  $this->_getSelectList('table_retailer r','r.retailer_id,r.retailer_name',''," r.retailer_name!='' ORDER BY r.retailer_id"); 
		if(is_array($dealerList)){
		if($flexiLoad == 'flex')
				$data.= '<option value="All">All</option>';		
			else if($flexiLoad == 'dealers')
				$data.= '<option value="">Please Select</option>';	
			else 
				$data.= '<option value="">All</option>';
		foreach($dealerList as $value):
			if($value->retailer_id == $selecteddis){ $selected = "selected"; } else{ $selected = ""; }
			$data.="<option value=".$value->retailer_id." $selected>".$value->retailer_name."</option>";
		endforeach;
		} else {
		
			$data.= '<option value="" >No Dealer Available</option>';
		
		}
		return $data;
	
	}

public function stockTransferwhtowhinvoice($itemcode,$itemsbsn,$to_warehouse_id,$warehouse_id,$returnArray)

{
	
	$wh_state_id=$this->_getSelectList('table_warehouse AS WH',"state_id,warehouse_name",''," WH.warehouse_id='".$warehouse_id."'  ");
           $state_id=$wh_state_id[0]->state_id;
           $warehouse_name=$wh_state_id[0]->warehouse_name;//From from warehouse
           $wh_state_id1=$this->_getSelectList('table_warehouse AS WH',"state_id,warehouse_name",''," WH.warehouse_id='".$to_warehouse_id."'  ");
           $state_id1=$wh_state_id1[0]->state_id;
           $Towarehouse_name=$wh_state_id1[0]->warehouse_name;//To from warehouse
            $dateFormat1 = date('Ymd'); // 20171202
            $DATE = $dateFormat1;

           if($state_id==$state_id1)
           {
             
	 		$wh_name=$warehouse_name;
	 		$Towh_name=$Towarehouse_name;
	 	    $arr = explode(' ',trim($wh_name));
	 	    $arr1 = explode(' ',trim($Towh_name));
	 	    $challan=ucwords(strtolower($arr[1]));
            $voucher_type=ucwords(strtolower($arr[1]))." Delivery Challan GST";
            $voucher_typereport=ucwords(strtolower($arr1[1]))." Receipt Note";
           // $godown_name="CHN ". ucwords(strtolower($arr1[1]))." Sales";
           // $ledgername=$voucher_type." Stock Transfer Agnt F Form";
           // $ledgername=ucwords(strtolower($arr[1]))." Test Pass Return";
            $ledgername="Eastman Auto &amp; Power limited-".$challan;
            $ledgername_report="Receipt Note ".ucwords(strtolower($arr1[1]));
            //$BASICUSERDESCRIPTION=$auRec[0]->bsn;
            $requestXMLSTART='<ENVELOPE>
			<HEADER>
			<TALLYREQUEST>Import Data</TALLYREQUEST>
			</HEADER>
			<BODY>
			<IMPORTDATA>
			<REQUESTDESC>
			<REPORTNAME>Vouchers</REPORTNAME>
			<STATICVARIABLES>
			<SVCURRENTCOMPANY>EastmanLtd</SVCURRENTCOMPANY>
			</STATICVARIABLES>
			</REQUESTDESC>
			<REQUESTDATA>
			<TALLYMESSAGE xmlns:UDF="TallyUDF">

			<VOUCHER VCHTYPE="'.$voucher_type.'" ACTION="Create" OBJVIEW="Invoice Voucher View">

			<BASICBUYERADDRESS.LIST TYPE="String">
			<BASICBUYERADDRESS>Flat No. 101, First Floor</BASICBUYERADDRESS>
			<BASICBUYERADDRESS>1, Community Centre,</BASICBUYERADDRESS>
			<BASICBUYERADDRESS>Nariana Industrial Area</BASICBUYERADDRESS>
			<BASICBUYERADDRESS>Phase-1, New Delhi-110028</BASICBUYERADDRESS>

			</BASICBUYERADDRESS.LIST>
			<OLDAUDITENTRYIDS.LIST TYPE="Number">
			<OLDAUDITENTRYIDS>-1</OLDAUDITENTRYIDS>
			</OLDAUDITENTRYIDS.LIST>
			<DATE>'.$DATE.'</DATE>
			<REFERENCEDATE>'.$DATE.'</REFERENCEDATE>

			<GSTREGISTRATIONTYPE></GSTREGISTRATIONTYPE>
			<VATDEALERTYPE></VATDEALERTYPE>
			<TAXUNITNAME>Default Tax Unit</TAXUNITNAME>
			<CLASSNAME></CLASSNAME>
			<PARTYNAME>'.$ledgername.'</PARTYNAME>
			<VOUCHERTYPENAME>'.$voucher_type.'</VOUCHERTYPENAME>
			<REFERENCE></REFERENCE>
			<VOUCHERNUMBER></VOUCHERNUMBER>
			<PARTYLEDGERNAME>'.$ledgername.'</PARTYLEDGERNAME>
			<BASICBASEPARTYNAME>'.$ledgername.'</BASICBASEPARTYNAME>
			<CSTFORMISSUETYPE/>
			<CSTFORMRECVTYPE/>
			<FBTPAYMENTTYPE>Default</FBTPAYMENTTYPE>
			<PERSISTEDVIEW>Invoice Voucher View</PERSISTEDVIEW>
			<BASICBUYERNAME>EastmanLtd</BASICBUYERNAME>
			<BASICDATETIMEOFINVOICE>3-Aug-2017 at 15:58</BASICDATETIMEOFINVOICE>
			<BASICDATETIMEOFREMOVAL>3-Aug-2017 at 15:58</BASICDATETIMEOFREMOVAL>
			<VCHGSTCLASS/>
			<ENTEREDBY></ENTEREDBY>
			<DIFFACTUALQTY>No</DIFFACTUALQTY>
			<ISMSTFROMSYNC>No</ISMSTFROMSYNC>
			<ASORIGINAL>No</ASORIGINAL>
			<AUDITED>No</AUDITED>
			<FORJOBCOSTING>No</FORJOBCOSTING>
			<ISOPTIONAL>No</ISOPTIONAL>
			<EFFECTIVEDATE>20170801</EFFECTIVEDATE>
			<USEFOREXCISE>No</USEFOREXCISE>
			<ISFORJOBWORKIN>No</ISFORJOBWORKIN>
			<ALLOWCONSUMPTION>No</ALLOWCONSUMPTION>
			<USEFORINTEREST>No</USEFORINTEREST>
			<USEFORGAINLOSS>No</USEFORGAINLOSS>
			<USEFORGODOWNTRANSFER>No</USEFORGODOWNTRANSFER>
			<USEFORCOMPOUND>No</USEFORCOMPOUND>
			<USEFORSERVICETAX>No</USEFORSERVICETAX>
			<ISEXCISEVOUCHER>No</ISEXCISEVOUCHER>
			<EXCISETAXOVERRIDE>No</EXCISETAXOVERRIDE>
			<USEFORTAXUNITTRANSFER>No</USEFORTAXUNITTRANSFER>
			<EXCISEOPENING>No</EXCISEOPENING>
			<USEFORFINALPRODUCTION>No</USEFORFINALPRODUCTION>
			<ISTDSOVERRIDDEN>No</ISTDSOVERRIDDEN>
			<ISTCSOVERRIDDEN>No</ISTCSOVERRIDDEN>
			<ISTDSTCSCASHVCH>No</ISTDSTCSCASHVCH>
			<INCLUDEADVPYMTVCH>No</INCLUDEADVPYMTVCH>
			<ISSUBWORKSCONTRACT>No</ISSUBWORKSCONTRACT>
			<ISVATOVERRIDDEN>No</ISVATOVERRIDDEN>
			<IGNOREORIGVCHDATE>No</IGNOREORIGVCHDATE>
			<ISSERVICETAXOVERRIDDEN>No</ISSERVICETAXOVERRIDDEN>
			<ISISDVOUCHER>No</ISISDVOUCHER>
			<ISEXCISEOVERRIDDEN>No</ISEXCISEOVERRIDDEN>
			<ISEXCISESUPPLYVCH>No</ISEXCISESUPPLYVCH>
			<ISGSTOVERRIDDEN>No</ISGSTOVERRIDDEN>
			<GSTNOTEXPORTED>No</GSTNOTEXPORTED>
			<ISVATPRINCIPALACCOUNT>No</ISVATPRINCIPALACCOUNT>
			<ISSHIPPINGWITHINSTATE>No</ISSHIPPINGWITHINSTATE>
			<ISCANCELLED>No</ISCANCELLED>
			<HASCASHFLOW>Yes</HASCASHFLOW>
			<ISPOSTDATED>No</ISPOSTDATED>
			<USETRACKINGNUMBER>No</USETRACKINGNUMBER>
			<ISINVOICE>Yes</ISINVOICE>
			<MFGJOURNAL>No</MFGJOURNAL>
			<HASDISCOUNTS>Yes</HASDISCOUNTS>
			<ASPAYSLIP>No</ASPAYSLIP>
			<ISCOSTCENTRE>No</ISCOSTCENTRE>
			<ISSTXNONREALIZEDVCH>No</ISSTXNONREALIZEDVCH>
			<ISEXCISEMANUFACTURERON>No</ISEXCISEMANUFACTURERON>
			<ISBLANKCHEQUE>No</ISBLANKCHEQUE>
			<ISVOID>No</ISVOID>
			<ISONHOLD>No</ISONHOLD>
			<ORDERLINESTATUS>No</ORDERLINESTATUS>
			<VATISAGNSTCANCSALES>No</VATISAGNSTCANCSALES>
			<VATISPURCEXEMPTED>No</VATISPURCEXEMPTED>
			<ISVATRESTAXINVOICE>No</ISVATRESTAXINVOICE>
			<VATISASSESABLECALCVCH>No</VATISASSESABLECALCVCH>
			<ISVATDUTYPAID>Yes</ISVATDUTYPAID>
			<ISDELIVERYSAMEASCONSIGNEE>No</ISDELIVERYSAMEASCONSIGNEE>
			<ISDISPATCHSAMEASCONSIGNOR>No</ISDISPATCHSAMEASCONSIGNOR>
			<ISDELETED>No</ISDELETED>
			<CHANGEVCHMODE>No</CHANGEVCHMODE>
			<ALTERID></ALTERID>
			<MASTERID></MASTERID>
			<VOUCHERKEY></VOUCHERKEY>
			<EXCLUDEDTAXATIONS.LIST></EXCLUDEDTAXATIONS.LIST>
			<OLDAUDITENTRIES.LIST></OLDAUDITENTRIES.LIST>
			<ACCOUNTAUDITENTRIES.LIST></ACCOUNTAUDITENTRIES.LIST>
			<AUDITENTRIES.LIST></AUDITENTRIES.LIST>
			<DUTYHEADDETAILS.LIST></DUTYHEADDETAILS.LIST>
			<SUPPLEMENTARYDUTYHEADDETAILS.LIST></SUPPLEMENTARYDUTYHEADDETAILS.LIST>
			<INVOICEDELNOTES.LIST></INVOICEDELNOTES.LIST>
			<INVOICEORDERLIST.LIST></INVOICEORDERLIST.LIST>
			<INVOICEINDENTLIST.LIST></INVOICEINDENTLIST.LIST>
			<ATTENDANCEENTRIES.LIST></ATTENDANCEENTRIES.LIST>
			<ORIGINVOICEDETAILS.LIST></ORIGINVOICEDETAILS.LIST>
			<INVOICEEXPORTLIST.LIST></INVOICEEXPORTLIST.LIST>
			<LEDGERENTRIES.LIST>
			<OLDAUDITENTRYIDS.LIST TYPE="Number">
			<OLDAUDITENTRYIDS>-1</OLDAUDITENTRYIDS>
			</OLDAUDITENTRYIDS.LIST>
			<LEDGERNAME>'.$ledgername.'</LEDGERNAME>
			<GSTCLASS/>
			<ISDEEMEDPOSITIVE>No</ISDEEMEDPOSITIVE>
			<LEDGERFROMITEM>No</LEDGERFROMITEM>
			<REMOVEZEROENTRIES>No</REMOVEZEROENTRIES>
			<ISPARTYLEDGER>Yes</ISPARTYLEDGER>
			<ISLASTDEEMEDPOSITIVE>No</ISLASTDEEMEDPOSITIVE>
			<AMOUNT></AMOUNT>
			<SERVICETAXDETAILS.LIST></SERVICETAXDETAILS.LIST>

			<BILLALLOCATIONS.LIST></BILLALLOCATIONS.LIST>
			<INTERESTCOLLECTION.LIST></INTERESTCOLLECTION.LIST>
			<OLDAUDITENTRIES.LIST></OLDAUDITENTRIES.LIST>
			<ACCOUNTAUDITENTRIES.LIST></ACCOUNTAUDITENTRIES.LIST>
			<AUDITENTRIES.LIST></AUDITENTRIES.LIST>
			<INPUTCRALLOCS.LIST></INPUTCRALLOCS.LIST>
			<DUTYHEADDETAILS.LIST></DUTYHEADDETAILS.LIST>
			<EXCISEDUTYHEADDETAILS.LIST></EXCISEDUTYHEADDETAILS.LIST>
			<RATEDETAILS.LIST></RATEDETAILS.LIST>
			<SUMMARYALLOCS.LIST></SUMMARYALLOCS.LIST>
			<STPYMTDETAILS.LIST></STPYMTDETAILS.LIST>
			<EXCISEPAYMENTALLOCATIONS.LIST></EXCISEPAYMENTALLOCATIONS.LIST>
			<TAXBILLALLOCATIONS.LIST></TAXBILLALLOCATIONS.LIST>
			<TAXOBJECTALLOCATIONS.LIST></TAXOBJECTALLOCATIONS.LIST>
			<TDSEXPENSEALLOCATIONS.LIST></TDSEXPENSEALLOCATIONS.LIST>
			<VATSTATUTORYDETAILS.LIST></VATSTATUTORYDETAILS.LIST>
			<COSTTRACKALLOCATIONS.LIST></COSTTRACKALLOCATIONS.LIST>
			<REFVOUCHERDETAILS.LIST></REFVOUCHERDETAILS.LIST>
			<INVOICEWISEDETAILS.LIST></INVOICEWISEDETAILS.LIST>
			<VATITCDETAILS.LIST></VATITCDETAILS.LIST>
			<ADVANCETAXDETAILS.LIST></ADVANCETAXDETAILS.LIST>
			</LEDGERENTRIES.LIST>
			<LEDGERENTRIES.LIST>

				<TAXCLASSIFICATIONNAME/>
				<LEDGERNAME>'.$ledgername.'</LEDGERNAME>
				<METHODTYPE>As User Defined Value</METHODTYPE>
				<VOUCHERFBTCATEGORY/>
				<GSTCLASS/>
				<ISDEEMEDPOSITIVE>No</ISDEEMEDPOSITIVE>
				<LEDGERFROMITEM>No</LEDGERFROMITEM>
				<REMOVEZEROENTRIES>No</REMOVEZEROENTRIES>
				<ISPARTYLEDGER>No</ISPARTYLEDGER>
				<ISLASTDEEMEDPOSITIVE>No</ISLASTDEEMEDPOSITIVE>
				<SERVICETAXDETAILS.LIST></SERVICETAXDETAILS.LIST>
				<BANKALLOCATIONS.LIST></BANKALLOCATIONS.LIST>
				<BILLALLOCATIONS.LIST></BILLALLOCATIONS.LIST>
				<INTERESTCOLLECTION.LIST></INTERESTCOLLECTION.LIST>
				<OLDAUDITENTRIES.LIST></OLDAUDITENTRIES.LIST>
				<ACCOUNTAUDITENTRIES.LIST></ACCOUNTAUDITENTRIES.LIST>
				<AUDITENTRIES.LIST></AUDITENTRIES.LIST>
				<INPUTCRALLOCS.LIST></INPUTCRALLOCS.LIST>
				<INVENTORYALLOCATIONS.LIST></INVENTORYALLOCATIONS.LIST>
				<DUTYHEADDETAILS.LIST></DUTYHEADDETAILS.LIST>
				<EXCISEDUTYHEADDETAILS.LIST></EXCISEDUTYHEADDETAILS.LIST>
				<RATEDETAILS.LIST></RATEDETAILS.LIST>
				<SUMMARYALLOCS.LIST></SUMMARYALLOCS.LIST>
				<STPYMTDETAILS.LIST></STPYMTDETAILS.LIST>
				<EXCISEPAYMENTALLOCATIONS.LIST></EXCISEPAYMENTALLOCATIONS.LIST>
				<TAXBILLALLOCATIONS.LIST></TAXBILLALLOCATIONS.LIST>
				<TAXOBJECTALLOCATIONS.LIST></TAXOBJECTALLOCATIONS.LIST>
				<TDSEXPENSEALLOCATIONS.LIST></TDSEXPENSEALLOCATIONS.LIST>
				<VATSTATUTORYDETAILS.LIST></VATSTATUTORYDETAILS.LIST>
				<REFVOUCHERDETAILS.LIST></REFVOUCHERDETAILS.LIST>
				<INVOICEWISEDETAILS.LIST></INVOICEWISEDETAILS.LIST>
				<VATITCDETAILS.LIST></VATITCDETAILS.LIST>
				<ADVANCETAXDETAILS.LIST></ADVANCETAXDETAILS.LIST>
				</LEDGERENTRIES.LIST>
				<LEDGERENTRIES.LIST>
				<TAXCLASSIFICATIONNAME/>
				<LEDGERNAME>Not for Sale</LEDGERNAME>
				<METHODTYPE>As User Defined Value</METHODTYPE>
				<VOUCHERFBTCATEGORY/>
				<GSTCLASS/>
				<ISDEEMEDPOSITIVE>No</ISDEEMEDPOSITIVE>
				<LEDGERFROMITEM>No</LEDGERFROMITEM>
				<REMOVEZEROENTRIES>No</REMOVEZEROENTRIES>
				<ISPARTYLEDGER>No</ISPARTYLEDGER>
				<ISLASTDEEMEDPOSITIVE>No</ISLASTDEEMEDPOSITIVE>
				<SERVICETAXDETAILS.LIST></SERVICETAXDETAILS.LIST>
				<BANKALLOCATIONS.LIST></BANKALLOCATIONS.LIST>
				<BILLALLOCATIONS.LIST></BILLALLOCATIONS.LIST>
				<INTERESTCOLLECTION.LIST></INTERESTCOLLECTION.LIST>
				<OLDAUDITENTRIES.LIST></OLDAUDITENTRIES.LIST>
				<ACCOUNTAUDITENTRIES.LIST></ACCOUNTAUDITENTRIES.LIST>
				<AUDITENTRIES.LIST></AUDITENTRIES.LIST>
				<INPUTCRALLOCS.LIST></INPUTCRALLOCS.LIST>
				<INVENTORYALLOCATIONS.LIST></INVENTORYALLOCATIONS.LIST>
				<DUTYHEADDETAILS.LIST></DUTYHEADDETAILS.LIST>
				<EXCISEDUTYHEADDETAILS.LIST></EXCISEDUTYHEADDETAILS.LIST>
				<RATEDETAILS.LIST></RATEDETAILS.LIST>
				<SUMMARYALLOCS.LIST></SUMMARYALLOCS.LIST>
				<STPYMTDETAILS.LIST></STPYMTDETAILS.LIST>
				<EXCISEPAYMENTALLOCATIONS.LIST></EXCISEPAYMENTALLOCATIONS.LIST>
				<TAXBILLALLOCATIONS.LIST></TAXBILLALLOCATIONS.LIST>
				<TAXOBJECTALLOCATIONS.LIST></TAXOBJECTALLOCATIONS.LIST>
				<TDSEXPENSEALLOCATIONS.LIST></TDSEXPENSEALLOCATIONS.LIST>
				<VATSTATUTORYDETAILS.LIST></VATSTATUTORYDETAILS.LIST>
				<REFVOUCHERDETAILS.LIST></REFVOUCHERDETAILS.LIST>
				<INVOICEWISEDETAILS.LIST></INVOICEWISEDETAILS.LIST>
				<VATITCDETAILS.LIST></VATITCDETAILS.LIST>
				<ADVANCETAXDETAILS.LIST></ADVANCETAXDETAILS.LIST>
				</LEDGERENTRIES.LIST>';


			  $requestXMLSTARTReport='<ENVELOPE>
			<HEADER>
			<TALLYREQUEST>Import Data</TALLYREQUEST>
			</HEADER>
			<BODY>
			<IMPORTDATA>
			<REQUESTDESC>
			<REPORTNAME>Vouchers</REPORTNAME>
			<STATICVARIABLES>
			<SVCURRENTCOMPANY>EastmanLtd</SVCURRENTCOMPANY>
			</STATICVARIABLES>
			</REQUESTDESC>
			<REQUESTDATA>
			<TALLYMESSAGE xmlns:UDF="TallyUDF">

			<VOUCHER VCHTYPE="'.$voucher_typereport.'" ACTION="Create" OBJVIEW="Invoice Voucher View">

			<BASICBUYERADDRESS.LIST TYPE="String">
			<BASICBUYERADDRESS>Flat No. 101, First Floor</BASICBUYERADDRESS>
			<BASICBUYERADDRESS>1, Community Centre,</BASICBUYERADDRESS>
			<BASICBUYERADDRESS>Nariana Industrial Area</BASICBUYERADDRESS>
			<BASICBUYERADDRESS>Phase-1, New Delhi-110028</BASICBUYERADDRESS>

			</BASICBUYERADDRESS.LIST>
			<OLDAUDITENTRYIDS.LIST TYPE="Number">
			<OLDAUDITENTRYIDS>-1</OLDAUDITENTRYIDS>
			</OLDAUDITENTRYIDS.LIST>
			<DATE>'.$DATE.'</DATE>
			<REFERENCEDATE>'.$DATE.'</REFERENCEDATE>

			<GSTREGISTRATIONTYPE></GSTREGISTRATIONTYPE>
			<VATDEALERTYPE></VATDEALERTYPE>
			<TAXUNITNAME>Default Tax Unit</TAXUNITNAME>
			<CLASSNAME></CLASSNAME>
			<PARTYNAME>'.$ledgername.'</PARTYNAME>
			<VOUCHERTYPENAME>'.$voucher_typereport.'</VOUCHERTYPENAME>
			<REFERENCE></REFERENCE>
			<VOUCHERNUMBER></VOUCHERNUMBER>
			<PARTYLEDGERNAME>'.$ledgername.'</PARTYLEDGERNAME>
			<BASICBASEPARTYNAME>'.$ledgername.'</BASICBASEPARTYNAME>
			<CSTFORMISSUETYPE/>
			<CSTFORMRECVTYPE/>
			<FBTPAYMENTTYPE>Default</FBTPAYMENTTYPE>
			<PERSISTEDVIEW>Invoice Voucher View</PERSISTEDVIEW>
			<BASICBUYERNAME>EastmanLtd</BASICBUYERNAME>
			<BASICDATETIMEOFINVOICE>3-Aug-2017 at 15:58</BASICDATETIMEOFINVOICE>
			<BASICDATETIMEOFREMOVAL>3-Aug-2017 at 15:58</BASICDATETIMEOFREMOVAL>
			<VCHGSTCLASS/>
			<ENTEREDBY></ENTEREDBY>
			<DIFFACTUALQTY>No</DIFFACTUALQTY>
			<ISMSTFROMSYNC>No</ISMSTFROMSYNC>
			<ASORIGINAL>No</ASORIGINAL>
			<AUDITED>No</AUDITED>
			<FORJOBCOSTING>No</FORJOBCOSTING>
			<ISOPTIONAL>No</ISOPTIONAL>
			<EFFECTIVEDATE>20170801</EFFECTIVEDATE>
			<USEFOREXCISE>No</USEFOREXCISE>
			<ISFORJOBWORKIN>No</ISFORJOBWORKIN>
			<ALLOWCONSUMPTION>No</ALLOWCONSUMPTION>
			<USEFORINTEREST>No</USEFORINTEREST>
			<USEFORGAINLOSS>No</USEFORGAINLOSS>
			<USEFORGODOWNTRANSFER>No</USEFORGODOWNTRANSFER>
			<USEFORCOMPOUND>No</USEFORCOMPOUND>
			<USEFORSERVICETAX>No</USEFORSERVICETAX>
			<ISEXCISEVOUCHER>No</ISEXCISEVOUCHER>
			<EXCISETAXOVERRIDE>No</EXCISETAXOVERRIDE>
			<USEFORTAXUNITTRANSFER>No</USEFORTAXUNITTRANSFER>
			<EXCISEOPENING>No</EXCISEOPENING>
			<USEFORFINALPRODUCTION>No</USEFORFINALPRODUCTION>
			<ISTDSOVERRIDDEN>No</ISTDSOVERRIDDEN>
			<ISTCSOVERRIDDEN>No</ISTCSOVERRIDDEN>
			<ISTDSTCSCASHVCH>No</ISTDSTCSCASHVCH>
			<INCLUDEADVPYMTVCH>No</INCLUDEADVPYMTVCH>
			<ISSUBWORKSCONTRACT>No</ISSUBWORKSCONTRACT>
			<ISVATOVERRIDDEN>No</ISVATOVERRIDDEN>
			<IGNOREORIGVCHDATE>No</IGNOREORIGVCHDATE>
			<ISSERVICETAXOVERRIDDEN>No</ISSERVICETAXOVERRIDDEN>
			<ISISDVOUCHER>No</ISISDVOUCHER>
			<ISEXCISEOVERRIDDEN>No</ISEXCISEOVERRIDDEN>
			<ISEXCISESUPPLYVCH>No</ISEXCISESUPPLYVCH>
			<ISGSTOVERRIDDEN>No</ISGSTOVERRIDDEN>
			<GSTNOTEXPORTED>No</GSTNOTEXPORTED>
			<ISVATPRINCIPALACCOUNT>No</ISVATPRINCIPALACCOUNT>
			<ISSHIPPINGWITHINSTATE>No</ISSHIPPINGWITHINSTATE>
			<ISCANCELLED>No</ISCANCELLED>
			<HASCASHFLOW>Yes</HASCASHFLOW>
			<ISPOSTDATED>No</ISPOSTDATED>
			<USETRACKINGNUMBER>No</USETRACKINGNUMBER>
			<ISINVOICE>Yes</ISINVOICE>
			<MFGJOURNAL>No</MFGJOURNAL>
			<HASDISCOUNTS>Yes</HASDISCOUNTS>
			<ASPAYSLIP>No</ASPAYSLIP>
			<ISCOSTCENTRE>No</ISCOSTCENTRE>
			<ISSTXNONREALIZEDVCH>No</ISSTXNONREALIZEDVCH>
			<ISEXCISEMANUFACTURERON>No</ISEXCISEMANUFACTURERON>
			<ISBLANKCHEQUE>No</ISBLANKCHEQUE>
			<ISVOID>No</ISVOID>
			<ISONHOLD>No</ISONHOLD>
			<ORDERLINESTATUS>No</ORDERLINESTATUS>
			<VATISAGNSTCANCSALES>No</VATISAGNSTCANCSALES>
			<VATISPURCEXEMPTED>No</VATISPURCEXEMPTED>
			<ISVATRESTAXINVOICE>No</ISVATRESTAXINVOICE>
			<VATISASSESABLECALCVCH>No</VATISASSESABLECALCVCH>
			<ISVATDUTYPAID>Yes</ISVATDUTYPAID>
			<ISDELIVERYSAMEASCONSIGNEE>No</ISDELIVERYSAMEASCONSIGNEE>
			<ISDISPATCHSAMEASCONSIGNOR>No</ISDISPATCHSAMEASCONSIGNOR>
			<ISDELETED>No</ISDELETED>
			<CHANGEVCHMODE>No</CHANGEVCHMODE>
			<ALTERID></ALTERID>
			<MASTERID></MASTERID>
			<VOUCHERKEY></VOUCHERKEY>
			<EXCLUDEDTAXATIONS.LIST></EXCLUDEDTAXATIONS.LIST>
			<OLDAUDITENTRIES.LIST></OLDAUDITENTRIES.LIST>
			<ACCOUNTAUDITENTRIES.LIST></ACCOUNTAUDITENTRIES.LIST>
			<AUDITENTRIES.LIST></AUDITENTRIES.LIST>
			<DUTYHEADDETAILS.LIST></DUTYHEADDETAILS.LIST>
			<SUPPLEMENTARYDUTYHEADDETAILS.LIST></SUPPLEMENTARYDUTYHEADDETAILS.LIST>
			<INVOICEDELNOTES.LIST></INVOICEDELNOTES.LIST>
			<INVOICEORDERLIST.LIST></INVOICEORDERLIST.LIST>
			<INVOICEINDENTLIST.LIST></INVOICEINDENTLIST.LIST>
			<ATTENDANCEENTRIES.LIST></ATTENDANCEENTRIES.LIST>
			<ORIGINVOICEDETAILS.LIST></ORIGINVOICEDETAILS.LIST>
			<INVOICEEXPORTLIST.LIST></INVOICEEXPORTLIST.LIST>
			<LEDGERENTRIES.LIST>
			<OLDAUDITENTRYIDS.LIST TYPE="Number">
			<OLDAUDITENTRYIDS>-1</OLDAUDITENTRYIDS>
			</OLDAUDITENTRYIDS.LIST>
			<LEDGERNAME>'.$ledgername.'</LEDGERNAME>
			<GSTCLASS/>
			<ISDEEMEDPOSITIVE>No</ISDEEMEDPOSITIVE>
			<LEDGERFROMITEM>No</LEDGERFROMITEM>
			<REMOVEZEROENTRIES>No</REMOVEZEROENTRIES>
			<ISPARTYLEDGER>Yes</ISPARTYLEDGER>
			<ISLASTDEEMEDPOSITIVE>No</ISLASTDEEMEDPOSITIVE>
			<AMOUNT></AMOUNT>
			<SERVICETAXDETAILS.LIST></SERVICETAXDETAILS.LIST>

			<BILLALLOCATIONS.LIST></BILLALLOCATIONS.LIST>
			<INTERESTCOLLECTION.LIST></INTERESTCOLLECTION.LIST>
			<OLDAUDITENTRIES.LIST></OLDAUDITENTRIES.LIST>
			<ACCOUNTAUDITENTRIES.LIST></ACCOUNTAUDITENTRIES.LIST>
			<AUDITENTRIES.LIST></AUDITENTRIES.LIST>
			<INPUTCRALLOCS.LIST></INPUTCRALLOCS.LIST>
			<DUTYHEADDETAILS.LIST></DUTYHEADDETAILS.LIST>
			<EXCISEDUTYHEADDETAILS.LIST></EXCISEDUTYHEADDETAILS.LIST>
			<RATEDETAILS.LIST></RATEDETAILS.LIST>
			<SUMMARYALLOCS.LIST></SUMMARYALLOCS.LIST>
			<STPYMTDETAILS.LIST></STPYMTDETAILS.LIST>
			<EXCISEPAYMENTALLOCATIONS.LIST></EXCISEPAYMENTALLOCATIONS.LIST>
			<TAXBILLALLOCATIONS.LIST></TAXBILLALLOCATIONS.LIST>
			<TAXOBJECTALLOCATIONS.LIST></TAXOBJECTALLOCATIONS.LIST>
			<TDSEXPENSEALLOCATIONS.LIST></TDSEXPENSEALLOCATIONS.LIST>
			<VATSTATUTORYDETAILS.LIST></VATSTATUTORYDETAILS.LIST>
			<COSTTRACKALLOCATIONS.LIST></COSTTRACKALLOCATIONS.LIST>
			<REFVOUCHERDETAILS.LIST></REFVOUCHERDETAILS.LIST>
			<INVOICEWISEDETAILS.LIST></INVOICEWISEDETAILS.LIST>
			<VATITCDETAILS.LIST></VATITCDETAILS.LIST>
			<ADVANCETAXDETAILS.LIST></ADVANCETAXDETAILS.LIST>
			</LEDGERENTRIES.LIST>
			<LEDGERENTRIES.LIST>

				<TAXCLASSIFICATIONNAME/>
				<LEDGERNAME>'.$ledgername_report.'</LEDGERNAME>
				<METHODTYPE>As User Defined Value</METHODTYPE>
				<VOUCHERFBTCATEGORY/>
				<GSTCLASS/>
				<ISDEEMEDPOSITIVE>No</ISDEEMEDPOSITIVE>
				<LEDGERFROMITEM>No</LEDGERFROMITEM>
				<REMOVEZEROENTRIES>No</REMOVEZEROENTRIES>
				<ISPARTYLEDGER>No</ISPARTYLEDGER>
				<ISLASTDEEMEDPOSITIVE>No</ISLASTDEEMEDPOSITIVE>
				<SERVICETAXDETAILS.LIST></SERVICETAXDETAILS.LIST>
				<BANKALLOCATIONS.LIST></BANKALLOCATIONS.LIST>
				<BILLALLOCATIONS.LIST></BILLALLOCATIONS.LIST>
				<INTERESTCOLLECTION.LIST></INTERESTCOLLECTION.LIST>
				<OLDAUDITENTRIES.LIST></OLDAUDITENTRIES.LIST>
				<ACCOUNTAUDITENTRIES.LIST></ACCOUNTAUDITENTRIES.LIST>
				<AUDITENTRIES.LIST></AUDITENTRIES.LIST>
				<INPUTCRALLOCS.LIST></INPUTCRALLOCS.LIST>
				<INVENTORYALLOCATIONS.LIST></INVENTORYALLOCATIONS.LIST>
				<DUTYHEADDETAILS.LIST></DUTYHEADDETAILS.LIST>
				<EXCISEDUTYHEADDETAILS.LIST></EXCISEDUTYHEADDETAILS.LIST>
				<RATEDETAILS.LIST></RATEDETAILS.LIST>
				<SUMMARYALLOCS.LIST></SUMMARYALLOCS.LIST>
				<STPYMTDETAILS.LIST></STPYMTDETAILS.LIST>
				<EXCISEPAYMENTALLOCATIONS.LIST></EXCISEPAYMENTALLOCATIONS.LIST>
				<TAXBILLALLOCATIONS.LIST></TAXBILLALLOCATIONS.LIST>
				<TAXOBJECTALLOCATIONS.LIST></TAXOBJECTALLOCATIONS.LIST>
				<TDSEXPENSEALLOCATIONS.LIST></TDSEXPENSEALLOCATIONS.LIST>
				<VATSTATUTORYDETAILS.LIST></VATSTATUTORYDETAILS.LIST>
				<REFVOUCHERDETAILS.LIST></REFVOUCHERDETAILS.LIST>
				<INVOICEWISEDETAILS.LIST></INVOICEWISEDETAILS.LIST>
				<VATITCDETAILS.LIST></VATITCDETAILS.LIST>
				<ADVANCETAXDETAILS.LIST></ADVANCETAXDETAILS.LIST>
				</LEDGERENTRIES.LIST>
				<LEDGERENTRIES.LIST>
				<TAXCLASSIFICATIONNAME/>
				<LEDGERNAME>Not for Sale</LEDGERNAME>
				<METHODTYPE>As User Defined Value</METHODTYPE>
				<VOUCHERFBTCATEGORY/>
				<GSTCLASS/>
				<ISDEEMEDPOSITIVE>No</ISDEEMEDPOSITIVE>
				<LEDGERFROMITEM>No</LEDGERFROMITEM>
				<REMOVEZEROENTRIES>No</REMOVEZEROENTRIES>
				<ISPARTYLEDGER>No</ISPARTYLEDGER>
				<ISLASTDEEMEDPOSITIVE>No</ISLASTDEEMEDPOSITIVE>
				<SERVICETAXDETAILS.LIST></SERVICETAXDETAILS.LIST>
				<BANKALLOCATIONS.LIST></BANKALLOCATIONS.LIST>
				<BILLALLOCATIONS.LIST></BILLALLOCATIONS.LIST>
				<INTERESTCOLLECTION.LIST></INTERESTCOLLECTION.LIST>
				<OLDAUDITENTRIES.LIST></OLDAUDITENTRIES.LIST>
				<ACCOUNTAUDITENTRIES.LIST></ACCOUNTAUDITENTRIES.LIST>
				<AUDITENTRIES.LIST></AUDITENTRIES.LIST>
				<INPUTCRALLOCS.LIST></INPUTCRALLOCS.LIST>
				<INVENTORYALLOCATIONS.LIST></INVENTORYALLOCATIONS.LIST>
				<DUTYHEADDETAILS.LIST></DUTYHEADDETAILS.LIST>
				<EXCISEDUTYHEADDETAILS.LIST></EXCISEDUTYHEADDETAILS.LIST>
				<RATEDETAILS.LIST></RATEDETAILS.LIST>
				<SUMMARYALLOCS.LIST></SUMMARYALLOCS.LIST>
				<STPYMTDETAILS.LIST></STPYMTDETAILS.LIST>
				<EXCISEPAYMENTALLOCATIONS.LIST></EXCISEPAYMENTALLOCATIONS.LIST>
				<TAXBILLALLOCATIONS.LIST></TAXBILLALLOCATIONS.LIST>
				<TAXOBJECTALLOCATIONS.LIST></TAXOBJECTALLOCATIONS.LIST>
				<TDSEXPENSEALLOCATIONS.LIST></TDSEXPENSEALLOCATIONS.LIST>
				<VATSTATUTORYDETAILS.LIST></VATSTATUTORYDETAILS.LIST>
				<REFVOUCHERDETAILS.LIST></REFVOUCHERDETAILS.LIST>
				<INVOICEWISEDETAILS.LIST></INVOICEWISEDETAILS.LIST>
				<VATITCDETAILS.LIST></VATITCDETAILS.LIST>
				<ADVANCETAXDETAILS.LIST></ADVANCETAXDETAILS.LIST>
				</LEDGERENTRIES.LIST>




			';


						 
							 
	$requestXMLEND='					
<PAYROLLMODEOFPAYMENT.LIST>      </PAYROLLMODEOFPAYMENT.LIST>
      <ATTDRECORDS.LIST>      </ATTDRECORDS.LIST>
     </VOUCHER>
    </TALLYMESSAGE>

   </REQUESTDATA>
  </IMPORTDATA>
 </BODY>
</ENVELOPE>';


           }

           else
           {

           	$wh_name=$warehouse_name;
	 		$Towh_name=$Towarehouse_name;
	 	    $arr = explode(' ',trim($wh_name));
	 	    $arr1 = explode(' ',trim($Towh_name));
	 	    $challan=ucwords(strtolower($arr[1]));
            $voucher_type=ucwords(strtolower($arr[1]))." Tax Invoice GST";
            $voucher_typereport=ucwords(strtolower($arr1[1]))." Receipt Note";
           // $godown_name="CHN ". ucwords(strtolower($arr1[1]))." Sales";
            $ledgername="Eastman Auto &amp; Power limited-".$challan;
            $ledgername_report="Receipt Note ".ucwords(strtolower($arr1[1]));
           // $godown_name="CHN ". ucwords(strtolower($arr1[1]))." Sales";
           // $ledgername=$voucher_type." Stock Transfer Agnt F Form";
            $ledgernamegst=ucwords(strtolower($arr[1]))." GST Sales @ 18%";
            
            //$BASICUSERDESCRIPTION=$auRec[0]->bsn;
            $requestXMLSTART='<ENVELOPE>
			<HEADER>
			<TALLYREQUEST>Import Data</TALLYREQUEST>
			</HEADER>
			<BODY>
			<IMPORTDATA>
			<REQUESTDESC>
			<REPORTNAME>Vouchers</REPORTNAME>
			<STATICVARIABLES>
			<SVCURRENTCOMPANY>EastmanLtd</SVCURRENTCOMPANY>
			</STATICVARIABLES>
			</REQUESTDESC>
			<REQUESTDATA>
			<TALLYMESSAGE xmlns:UDF="TallyUDF">

			<VOUCHER VCHTYPE="'.$voucher_type.'" ACTION="Create" OBJVIEW="Invoice Voucher View">

			<BASICBUYERADDRESS.LIST TYPE="String">
			<BASICBUYERADDRESS>Flat No. 101, First Floor</BASICBUYERADDRESS>
			<BASICBUYERADDRESS>1, Community Centre,</BASICBUYERADDRESS>
			<BASICBUYERADDRESS>Nariana Industrial Area</BASICBUYERADDRESS>
			<BASICBUYERADDRESS>Phase-1, New Delhi-110028</BASICBUYERADDRESS>

			</BASICBUYERADDRESS.LIST>
			<OLDAUDITENTRYIDS.LIST TYPE="Number">
			<OLDAUDITENTRYIDS>-1</OLDAUDITENTRYIDS>
			</OLDAUDITENTRYIDS.LIST>
			<DATE>'.$DATE.'</DATE>
			<REFERENCEDATE>'.$DATE.'</REFERENCEDATE>

			<GSTREGISTRATIONTYPE></GSTREGISTRATIONTYPE>
			<VATDEALERTYPE></VATDEALERTYPE>
			<TAXUNITNAME>Default Tax Unit</TAXUNITNAME>
			<CLASSNAME></CLASSNAME>
			<PARTYNAME>'.$ledgername.'</PARTYNAME>
			<VOUCHERTYPENAME>'.$voucher_type.'</VOUCHERTYPENAME>
			<REFERENCE></REFERENCE>
			<VOUCHERNUMBER></VOUCHERNUMBER>
			<PARTYLEDGERNAME>'.$ledgername.'</PARTYLEDGERNAME>
			<BASICBASEPARTYNAME>'.$ledgername.'</BASICBASEPARTYNAME>
			<CSTFORMISSUETYPE/>
			<CSTFORMRECVTYPE/>
			<FBTPAYMENTTYPE>Default</FBTPAYMENTTYPE>
			<PERSISTEDVIEW>Invoice Voucher View</PERSISTEDVIEW>
			<BASICBUYERNAME>EastmanLtd</BASICBUYERNAME>
			<BASICDATETIMEOFINVOICE>3-Aug-2017 at 15:58</BASICDATETIMEOFINVOICE>
			<BASICDATETIMEOFREMOVAL>3-Aug-2017 at 15:58</BASICDATETIMEOFREMOVAL>
			<VCHGSTCLASS/>
			<ENTEREDBY></ENTEREDBY>
			<DIFFACTUALQTY>No</DIFFACTUALQTY>
			<ISMSTFROMSYNC>No</ISMSTFROMSYNC>
			<ASORIGINAL>No</ASORIGINAL>
			<AUDITED>No</AUDITED>
			<FORJOBCOSTING>No</FORJOBCOSTING>
			<ISOPTIONAL>No</ISOPTIONAL>
			<EFFECTIVEDATE>20170801</EFFECTIVEDATE>
			<USEFOREXCISE>No</USEFOREXCISE>
			<ISFORJOBWORKIN>No</ISFORJOBWORKIN>
			<ALLOWCONSUMPTION>No</ALLOWCONSUMPTION>
			<USEFORINTEREST>No</USEFORINTEREST>
			<USEFORGAINLOSS>No</USEFORGAINLOSS>
			<USEFORGODOWNTRANSFER>No</USEFORGODOWNTRANSFER>
			<USEFORCOMPOUND>No</USEFORCOMPOUND>
			<USEFORSERVICETAX>No</USEFORSERVICETAX>
			<ISEXCISEVOUCHER>No</ISEXCISEVOUCHER>
			<EXCISETAXOVERRIDE>No</EXCISETAXOVERRIDE>
			<USEFORTAXUNITTRANSFER>No</USEFORTAXUNITTRANSFER>
			<EXCISEOPENING>No</EXCISEOPENING>
			<USEFORFINALPRODUCTION>No</USEFORFINALPRODUCTION>
			<ISTDSOVERRIDDEN>No</ISTDSOVERRIDDEN>
			<ISTCSOVERRIDDEN>No</ISTCSOVERRIDDEN>
			<ISTDSTCSCASHVCH>No</ISTDSTCSCASHVCH>
			<INCLUDEADVPYMTVCH>No</INCLUDEADVPYMTVCH>
			<ISSUBWORKSCONTRACT>No</ISSUBWORKSCONTRACT>
			<ISVATOVERRIDDEN>No</ISVATOVERRIDDEN>
			<IGNOREORIGVCHDATE>No</IGNOREORIGVCHDATE>
			<ISSERVICETAXOVERRIDDEN>No</ISSERVICETAXOVERRIDDEN>
			<ISISDVOUCHER>No</ISISDVOUCHER>
			<ISEXCISEOVERRIDDEN>No</ISEXCISEOVERRIDDEN>
			<ISEXCISESUPPLYVCH>No</ISEXCISESUPPLYVCH>
			<ISGSTOVERRIDDEN>No</ISGSTOVERRIDDEN>
			<GSTNOTEXPORTED>No</GSTNOTEXPORTED>
			<ISVATPRINCIPALACCOUNT>No</ISVATPRINCIPALACCOUNT>
			<ISSHIPPINGWITHINSTATE>No</ISSHIPPINGWITHINSTATE>
			<ISCANCELLED>No</ISCANCELLED>
			<HASCASHFLOW>Yes</HASCASHFLOW>
			<ISPOSTDATED>No</ISPOSTDATED>
			<USETRACKINGNUMBER>No</USETRACKINGNUMBER>
			<ISINVOICE>Yes</ISINVOICE>
			<MFGJOURNAL>No</MFGJOURNAL>
			<HASDISCOUNTS>Yes</HASDISCOUNTS>
			<ASPAYSLIP>No</ASPAYSLIP>
			<ISCOSTCENTRE>No</ISCOSTCENTRE>
			<ISSTXNONREALIZEDVCH>No</ISSTXNONREALIZEDVCH>
			<ISEXCISEMANUFACTURERON>No</ISEXCISEMANUFACTURERON>
			<ISBLANKCHEQUE>No</ISBLANKCHEQUE>
			<ISVOID>No</ISVOID>
			<ISONHOLD>No</ISONHOLD>
			<ORDERLINESTATUS>No</ORDERLINESTATUS>
			<VATISAGNSTCANCSALES>No</VATISAGNSTCANCSALES>
			<VATISPURCEXEMPTED>No</VATISPURCEXEMPTED>
			<ISVATRESTAXINVOICE>No</ISVATRESTAXINVOICE>
			<VATISASSESABLECALCVCH>No</VATISASSESABLECALCVCH>
			<ISVATDUTYPAID>Yes</ISVATDUTYPAID>
			<ISDELIVERYSAMEASCONSIGNEE>No</ISDELIVERYSAMEASCONSIGNEE>
			<ISDISPATCHSAMEASCONSIGNOR>No</ISDISPATCHSAMEASCONSIGNOR>
			<ISDELETED>No</ISDELETED>
			<CHANGEVCHMODE>No</CHANGEVCHMODE>
			<ALTERID></ALTERID>
			<MASTERID></MASTERID>
			<VOUCHERKEY></VOUCHERKEY>
			<EXCLUDEDTAXATIONS.LIST></EXCLUDEDTAXATIONS.LIST>
			<OLDAUDITENTRIES.LIST></OLDAUDITENTRIES.LIST>
			<ACCOUNTAUDITENTRIES.LIST></ACCOUNTAUDITENTRIES.LIST>
			<AUDITENTRIES.LIST></AUDITENTRIES.LIST>
			<DUTYHEADDETAILS.LIST></DUTYHEADDETAILS.LIST>
			<SUPPLEMENTARYDUTYHEADDETAILS.LIST></SUPPLEMENTARYDUTYHEADDETAILS.LIST>
			<INVOICEDELNOTES.LIST></INVOICEDELNOTES.LIST>
			<INVOICEORDERLIST.LIST></INVOICEORDERLIST.LIST>
			<INVOICEINDENTLIST.LIST></INVOICEINDENTLIST.LIST>
			<ATTENDANCEENTRIES.LIST></ATTENDANCEENTRIES.LIST>
			<ORIGINVOICEDETAILS.LIST></ORIGINVOICEDETAILS.LIST>
			<INVOICEEXPORTLIST.LIST></INVOICEEXPORTLIST.LIST>
			<LEDGERENTRIES.LIST>
			<OLDAUDITENTRYIDS.LIST TYPE="Number">
			<OLDAUDITENTRYIDS>-1</OLDAUDITENTRYIDS>
			</OLDAUDITENTRYIDS.LIST>
			<LEDGERNAME>'.$ledgername.'</LEDGERNAME>
			<GSTCLASS/>
			<ISDEEMEDPOSITIVE>No</ISDEEMEDPOSITIVE>
			<LEDGERFROMITEM>No</LEDGERFROMITEM>
			<REMOVEZEROENTRIES>No</REMOVEZEROENTRIES>
			<ISPARTYLEDGER>Yes</ISPARTYLEDGER>
			<ISLASTDEEMEDPOSITIVE>No</ISLASTDEEMEDPOSITIVE>
			<AMOUNT></AMOUNT>
			<SERVICETAXDETAILS.LIST></SERVICETAXDETAILS.LIST>

			<BILLALLOCATIONS.LIST></BILLALLOCATIONS.LIST>
			<INTERESTCOLLECTION.LIST></INTERESTCOLLECTION.LIST>
			<OLDAUDITENTRIES.LIST></OLDAUDITENTRIES.LIST>
			<ACCOUNTAUDITENTRIES.LIST></ACCOUNTAUDITENTRIES.LIST>
			<AUDITENTRIES.LIST></AUDITENTRIES.LIST>
			<INPUTCRALLOCS.LIST></INPUTCRALLOCS.LIST>
			<DUTYHEADDETAILS.LIST></DUTYHEADDETAILS.LIST>
			<EXCISEDUTYHEADDETAILS.LIST></EXCISEDUTYHEADDETAILS.LIST>
			<RATEDETAILS.LIST></RATEDETAILS.LIST>
			<SUMMARYALLOCS.LIST></SUMMARYALLOCS.LIST>
			<STPYMTDETAILS.LIST></STPYMTDETAILS.LIST>
			<EXCISEPAYMENTALLOCATIONS.LIST></EXCISEPAYMENTALLOCATIONS.LIST>
			<TAXBILLALLOCATIONS.LIST></TAXBILLALLOCATIONS.LIST>
			<TAXOBJECTALLOCATIONS.LIST></TAXOBJECTALLOCATIONS.LIST>
			<TDSEXPENSEALLOCATIONS.LIST></TDSEXPENSEALLOCATIONS.LIST>
			<VATSTATUTORYDETAILS.LIST></VATSTATUTORYDETAILS.LIST>
			<COSTTRACKALLOCATIONS.LIST></COSTTRACKALLOCATIONS.LIST>
			<REFVOUCHERDETAILS.LIST></REFVOUCHERDETAILS.LIST>
			<INVOICEWISEDETAILS.LIST></INVOICEWISEDETAILS.LIST>
			<VATITCDETAILS.LIST></VATITCDETAILS.LIST>
			<ADVANCETAXDETAILS.LIST></ADVANCETAXDETAILS.LIST>
			</LEDGERENTRIES.LIST>
			<LEDGERENTRIES.LIST>

				<TAXCLASSIFICATIONNAME/>
				<LEDGERNAME>'.$ledgername.'</LEDGERNAME>
				<METHODTYPE>As User Defined Value</METHODTYPE>
				<VOUCHERFBTCATEGORY/>
				<GSTCLASS/>
				<ISDEEMEDPOSITIVE>No</ISDEEMEDPOSITIVE>
				<LEDGERFROMITEM>No</LEDGERFROMITEM>
				<REMOVEZEROENTRIES>No</REMOVEZEROENTRIES>
				<ISPARTYLEDGER>No</ISPARTYLEDGER>
				<ISLASTDEEMEDPOSITIVE>No</ISLASTDEEMEDPOSITIVE>
				<SERVICETAXDETAILS.LIST></SERVICETAXDETAILS.LIST>
				<BANKALLOCATIONS.LIST></BANKALLOCATIONS.LIST>
				<BILLALLOCATIONS.LIST></BILLALLOCATIONS.LIST>
				<INTERESTCOLLECTION.LIST></INTERESTCOLLECTION.LIST>
				<OLDAUDITENTRIES.LIST></OLDAUDITENTRIES.LIST>
				<ACCOUNTAUDITENTRIES.LIST></ACCOUNTAUDITENTRIES.LIST>
				<AUDITENTRIES.LIST></AUDITENTRIES.LIST>
				<INPUTCRALLOCS.LIST></INPUTCRALLOCS.LIST>
				<INVENTORYALLOCATIONS.LIST></INVENTORYALLOCATIONS.LIST>
				<DUTYHEADDETAILS.LIST></DUTYHEADDETAILS.LIST>
				<EXCISEDUTYHEADDETAILS.LIST></EXCISEDUTYHEADDETAILS.LIST>
				<RATEDETAILS.LIST></RATEDETAILS.LIST>
				<SUMMARYALLOCS.LIST></SUMMARYALLOCS.LIST>
				<STPYMTDETAILS.LIST></STPYMTDETAILS.LIST>
				<EXCISEPAYMENTALLOCATIONS.LIST></EXCISEPAYMENTALLOCATIONS.LIST>
				<TAXBILLALLOCATIONS.LIST></TAXBILLALLOCATIONS.LIST>
				<TAXOBJECTALLOCATIONS.LIST></TAXOBJECTALLOCATIONS.LIST>
				<TDSEXPENSEALLOCATIONS.LIST></TDSEXPENSEALLOCATIONS.LIST>
				<VATSTATUTORYDETAILS.LIST></VATSTATUTORYDETAILS.LIST>
				<REFVOUCHERDETAILS.LIST></REFVOUCHERDETAILS.LIST>
				<INVOICEWISEDETAILS.LIST></INVOICEWISEDETAILS.LIST>
				<VATITCDETAILS.LIST></VATITCDETAILS.LIST>
				<ADVANCETAXDETAILS.LIST></ADVANCETAXDETAILS.LIST>
				</LEDGERENTRIES.LIST>
				<LEDGERENTRIES.LIST>
				<TAXCLASSIFICATIONNAME/>
				<LEDGERNAME>'.$ledgernamegst.'</LEDGERNAME>
				<METHODTYPE>As User Defined Value</METHODTYPE>
				<VOUCHERFBTCATEGORY/>
				<GSTCLASS/>
				<ISDEEMEDPOSITIVE>No</ISDEEMEDPOSITIVE>
				<LEDGERFROMITEM>No</LEDGERFROMITEM>
				<REMOVEZEROENTRIES>No</REMOVEZEROENTRIES>
				<ISPARTYLEDGER>No</ISPARTYLEDGER>
				<ISLASTDEEMEDPOSITIVE>No</ISLASTDEEMEDPOSITIVE>
				<SERVICETAXDETAILS.LIST></SERVICETAXDETAILS.LIST>
				<BANKALLOCATIONS.LIST></BANKALLOCATIONS.LIST>
				<BILLALLOCATIONS.LIST></BILLALLOCATIONS.LIST>
				<INTERESTCOLLECTION.LIST></INTERESTCOLLECTION.LIST>
				<OLDAUDITENTRIES.LIST></OLDAUDITENTRIES.LIST>
				<ACCOUNTAUDITENTRIES.LIST></ACCOUNTAUDITENTRIES.LIST>
				<AUDITENTRIES.LIST></AUDITENTRIES.LIST>
				<INPUTCRALLOCS.LIST></INPUTCRALLOCS.LIST>
				<INVENTORYALLOCATIONS.LIST></INVENTORYALLOCATIONS.LIST>
				<DUTYHEADDETAILS.LIST></DUTYHEADDETAILS.LIST>
				<EXCISEDUTYHEADDETAILS.LIST></EXCISEDUTYHEADDETAILS.LIST>
				<RATEDETAILS.LIST></RATEDETAILS.LIST>
				<SUMMARYALLOCS.LIST></SUMMARYALLOCS.LIST>
				<STPYMTDETAILS.LIST></STPYMTDETAILS.LIST>
				<EXCISEPAYMENTALLOCATIONS.LIST></EXCISEPAYMENTALLOCATIONS.LIST>
				<TAXBILLALLOCATIONS.LIST></TAXBILLALLOCATIONS.LIST>
				<TAXOBJECTALLOCATIONS.LIST></TAXOBJECTALLOCATIONS.LIST>
				<TDSEXPENSEALLOCATIONS.LIST></TDSEXPENSEALLOCATIONS.LIST>
				<VATSTATUTORYDETAILS.LIST></VATSTATUTORYDETAILS.LIST>
				<REFVOUCHERDETAILS.LIST></REFVOUCHERDETAILS.LIST>
				<INVOICEWISEDETAILS.LIST></INVOICEWISEDETAILS.LIST>
				<VATITCDETAILS.LIST></VATITCDETAILS.LIST>
				<ADVANCETAXDETAILS.LIST></ADVANCETAXDETAILS.LIST>
				</LEDGERENTRIES.LIST>';




          $requestXMLSTARTReport='<ENVELOPE>
			<HEADER>
			<TALLYREQUEST>Import Data</TALLYREQUEST>
			</HEADER>
			<BODY>
			<IMPORTDATA>
			<REQUESTDESC>
			<REPORTNAME>Vouchers</REPORTNAME>
			<STATICVARIABLES>
			<SVCURRENTCOMPANY>EastmanLtd</SVCURRENTCOMPANY>
			</STATICVARIABLES>
			</REQUESTDESC>
			<REQUESTDATA>
			<TALLYMESSAGE xmlns:UDF="TallyUDF">

			<VOUCHER VCHTYPE="'.$voucher_typereport.'" ACTION="Create" OBJVIEW="Invoice Voucher View">

			<BASICBUYERADDRESS.LIST TYPE="String">
			<BASICBUYERADDRESS>Flat No. 101, First Floor</BASICBUYERADDRESS>
			<BASICBUYERADDRESS>1, Community Centre,</BASICBUYERADDRESS>
			<BASICBUYERADDRESS>Nariana Industrial Area</BASICBUYERADDRESS>
			<BASICBUYERADDRESS>Phase-1, New Delhi-110028</BASICBUYERADDRESS>

			</BASICBUYERADDRESS.LIST>
			<OLDAUDITENTRYIDS.LIST TYPE="Number">
			<OLDAUDITENTRYIDS>-1</OLDAUDITENTRYIDS>
			</OLDAUDITENTRYIDS.LIST>
			<DATE>'.$DATE.'</DATE>
			<REFERENCEDATE>'.$DATE.'</REFERENCEDATE>

			<GSTREGISTRATIONTYPE></GSTREGISTRATIONTYPE>
			<VATDEALERTYPE></VATDEALERTYPE>
			<TAXUNITNAME>Default Tax Unit</TAXUNITNAME>
			<CLASSNAME></CLASSNAME>
			<PARTYNAME>'.$ledgername.'</PARTYNAME>
			<VOUCHERTYPENAME>'.$voucher_typereport.'</VOUCHERTYPENAME>
			<REFERENCE></REFERENCE>
			<VOUCHERNUMBER></VOUCHERNUMBER>
			<PARTYLEDGERNAME>'.$ledgername.'</PARTYLEDGERNAME>
			<BASICBASEPARTYNAME>'.$ledgername.'</BASICBASEPARTYNAME>
			<CSTFORMISSUETYPE/>
			<CSTFORMRECVTYPE/>
			<FBTPAYMENTTYPE>Default</FBTPAYMENTTYPE>
			<PERSISTEDVIEW>Invoice Voucher View</PERSISTEDVIEW>
			<BASICBUYERNAME>EastmanLtd</BASICBUYERNAME>
			<BASICDATETIMEOFINVOICE>3-Aug-2017 at 15:58</BASICDATETIMEOFINVOICE>
			<BASICDATETIMEOFREMOVAL>3-Aug-2017 at 15:58</BASICDATETIMEOFREMOVAL>
			<VCHGSTCLASS/>
			<ENTEREDBY></ENTEREDBY>
			<DIFFACTUALQTY>No</DIFFACTUALQTY>
			<ISMSTFROMSYNC>No</ISMSTFROMSYNC>
			<ASORIGINAL>No</ASORIGINAL>
			<AUDITED>No</AUDITED>
			<FORJOBCOSTING>No</FORJOBCOSTING>
			<ISOPTIONAL>No</ISOPTIONAL>
			<EFFECTIVEDATE>20170801</EFFECTIVEDATE>
			<USEFOREXCISE>No</USEFOREXCISE>
			<ISFORJOBWORKIN>No</ISFORJOBWORKIN>
			<ALLOWCONSUMPTION>No</ALLOWCONSUMPTION>
			<USEFORINTEREST>No</USEFORINTEREST>
			<USEFORGAINLOSS>No</USEFORGAINLOSS>
			<USEFORGODOWNTRANSFER>No</USEFORGODOWNTRANSFER>
			<USEFORCOMPOUND>No</USEFORCOMPOUND>
			<USEFORSERVICETAX>No</USEFORSERVICETAX>
			<ISEXCISEVOUCHER>No</ISEXCISEVOUCHER>
			<EXCISETAXOVERRIDE>No</EXCISETAXOVERRIDE>
			<USEFORTAXUNITTRANSFER>No</USEFORTAXUNITTRANSFER>
			<EXCISEOPENING>No</EXCISEOPENING>
			<USEFORFINALPRODUCTION>No</USEFORFINALPRODUCTION>
			<ISTDSOVERRIDDEN>No</ISTDSOVERRIDDEN>
			<ISTCSOVERRIDDEN>No</ISTCSOVERRIDDEN>
			<ISTDSTCSCASHVCH>No</ISTDSTCSCASHVCH>
			<INCLUDEADVPYMTVCH>No</INCLUDEADVPYMTVCH>
			<ISSUBWORKSCONTRACT>No</ISSUBWORKSCONTRACT>
			<ISVATOVERRIDDEN>No</ISVATOVERRIDDEN>
			<IGNOREORIGVCHDATE>No</IGNOREORIGVCHDATE>
			<ISSERVICETAXOVERRIDDEN>No</ISSERVICETAXOVERRIDDEN>
			<ISISDVOUCHER>No</ISISDVOUCHER>
			<ISEXCISEOVERRIDDEN>No</ISEXCISEOVERRIDDEN>
			<ISEXCISESUPPLYVCH>No</ISEXCISESUPPLYVCH>
			<ISGSTOVERRIDDEN>No</ISGSTOVERRIDDEN>
			<GSTNOTEXPORTED>No</GSTNOTEXPORTED>
			<ISVATPRINCIPALACCOUNT>No</ISVATPRINCIPALACCOUNT>
			<ISSHIPPINGWITHINSTATE>No</ISSHIPPINGWITHINSTATE>
			<ISCANCELLED>No</ISCANCELLED>
			<HASCASHFLOW>Yes</HASCASHFLOW>
			<ISPOSTDATED>No</ISPOSTDATED>
			<USETRACKINGNUMBER>No</USETRACKINGNUMBER>
			<ISINVOICE>Yes</ISINVOICE>
			<MFGJOURNAL>No</MFGJOURNAL>
			<HASDISCOUNTS>Yes</HASDISCOUNTS>
			<ASPAYSLIP>No</ASPAYSLIP>
			<ISCOSTCENTRE>No</ISCOSTCENTRE>
			<ISSTXNONREALIZEDVCH>No</ISSTXNONREALIZEDVCH>
			<ISEXCISEMANUFACTURERON>No</ISEXCISEMANUFACTURERON>
			<ISBLANKCHEQUE>No</ISBLANKCHEQUE>
			<ISVOID>No</ISVOID>
			<ISONHOLD>No</ISONHOLD>
			<ORDERLINESTATUS>No</ORDERLINESTATUS>
			<VATISAGNSTCANCSALES>No</VATISAGNSTCANCSALES>
			<VATISPURCEXEMPTED>No</VATISPURCEXEMPTED>
			<ISVATRESTAXINVOICE>No</ISVATRESTAXINVOICE>
			<VATISASSESABLECALCVCH>No</VATISASSESABLECALCVCH>
			<ISVATDUTYPAID>Yes</ISVATDUTYPAID>
			<ISDELIVERYSAMEASCONSIGNEE>No</ISDELIVERYSAMEASCONSIGNEE>
			<ISDISPATCHSAMEASCONSIGNOR>No</ISDISPATCHSAMEASCONSIGNOR>
			<ISDELETED>No</ISDELETED>
			<CHANGEVCHMODE>No</CHANGEVCHMODE>
			<ALTERID></ALTERID>
			<MASTERID></MASTERID>
			<VOUCHERKEY></VOUCHERKEY>
			<EXCLUDEDTAXATIONS.LIST></EXCLUDEDTAXATIONS.LIST>
			<OLDAUDITENTRIES.LIST></OLDAUDITENTRIES.LIST>
			<ACCOUNTAUDITENTRIES.LIST></ACCOUNTAUDITENTRIES.LIST>
			<AUDITENTRIES.LIST></AUDITENTRIES.LIST>
			<DUTYHEADDETAILS.LIST></DUTYHEADDETAILS.LIST>
			<SUPPLEMENTARYDUTYHEADDETAILS.LIST></SUPPLEMENTARYDUTYHEADDETAILS.LIST>
			<INVOICEDELNOTES.LIST></INVOICEDELNOTES.LIST>
			<INVOICEORDERLIST.LIST></INVOICEORDERLIST.LIST>
			<INVOICEINDENTLIST.LIST></INVOICEINDENTLIST.LIST>
			<ATTENDANCEENTRIES.LIST></ATTENDANCEENTRIES.LIST>
			<ORIGINVOICEDETAILS.LIST></ORIGINVOICEDETAILS.LIST>
			<INVOICEEXPORTLIST.LIST></INVOICEEXPORTLIST.LIST>
			<LEDGERENTRIES.LIST>
			<OLDAUDITENTRYIDS.LIST TYPE="Number">
			<OLDAUDITENTRYIDS>-1</OLDAUDITENTRYIDS>
			</OLDAUDITENTRYIDS.LIST>
			<LEDGERNAME>'.$ledgername.'</LEDGERNAME>
			<GSTCLASS/>
			<ISDEEMEDPOSITIVE>No</ISDEEMEDPOSITIVE>
			<LEDGERFROMITEM>No</LEDGERFROMITEM>
			<REMOVEZEROENTRIES>No</REMOVEZEROENTRIES>
			<ISPARTYLEDGER>Yes</ISPARTYLEDGER>
			<ISLASTDEEMEDPOSITIVE>No</ISLASTDEEMEDPOSITIVE>
			<AMOUNT></AMOUNT>
			<SERVICETAXDETAILS.LIST></SERVICETAXDETAILS.LIST>

			<BILLALLOCATIONS.LIST></BILLALLOCATIONS.LIST>
			<INTERESTCOLLECTION.LIST></INTERESTCOLLECTION.LIST>message
			<OLDAUDITENTRIES.LIST></OLDAUDITENTRIES.LIST>
			<ACCOUNTAUDITENTRIES.LIST></ACCOUNTAUDITENTRIES.LIST>
			<AUDITENTRIES.LIST></AUDITENTRIES.LIST>
			<INPUTCRALLOCS.LIST></INPUTCRALLOCS.LIST>
			<DUTYHEADDETAILS.LIST></DUTYHEADDETAILS.LIST>
			<EXCISEDUTYHEADDETAILS.LIST></EXCISEDUTYHEADDETAILS.LIST>
			<RATEDETAILS.LIST></RATEDETAILS.LIST>
			<SUMMARYALLOCS.LIST></SUMMARYALLOCS.LIST>
			<STPYMTDETAILS.LIST></STPYMTDETAILS.LIST>
			<EXCISEPAYMENTALLOCATIONS.LIST></EXCISEPAYMENTALLOCATIONS.LIST>
			<TAXBILLALLOCATIONS.LIST></TAXBILLALLOCATIONS.LIST>
			<TAXOBJECTALLOCATIONS.LIST></TAXOBJECTALLOCATIONS.LIST>
			<TDSEXPENSEALLOCATIONS.LIST></TDSEXPENSEALLOCATIONS.LIST>
			<VATSTATUTORYDETAILS.LIST></VATSTATUTORYDETAILS.LIST>
			<COSTTRACKALLOCATIONS.LIST></COSTTRACKALLOCATIONS.LIST>
			<REFVOUCHERDETAILS.LIST></REFVOUCHERDETAILS.LIST>
			<INVOICEWISEDETAILS.LIST></INVOICEWISEDETAILS.LIST>
			<VATITCDETAILS.LIST></VATITCDETAILS.LIST>
			<ADVANCETAXDETAILS.LIST></ADVANCETAXDETAILS.LIST>
			</LEDGERENTRIES.LIST>
			<LEDGERENTRIES.LIST>

				<TAXCLASSIFICATIONNAME/>
				<LEDGERNAME>'.$ledgername_report.'</LEDGERNAME>
				<METHODTYPE>As User Defined Value</METHODTYPE>
				<VOUCHERFBTCATEGORY/>
				<GSTCLASS/>
				<ISDEEMEDPOSITIVE>No</ISDEEMEDPOSITIVE>
				<LEDGERFROMITEM>No</LEDGERFROMITEM>
				<REMOVEZEROENTRIES>No</REMOVEZEROENTRIES>
				<ISPARTYLEDGER>No</ISPARTYLEDGER>
				<ISLASTDEEMEDPOSITIVE>No</ISLASTDEEMEDPOSITIVE>
				<SERVICETAXDETAILS.LIST></SERVICETAXDETAILS.LIST>
				<BANKALLOCATIONS.LIST></BANKALLOCATIONS.LIST>
				<BILLALLOCATIONS.LIST></BILLALLOCATIONS.LIST>
				<INTERESTCOLLECTION.LIST></INTERESTCOLLECTION.LIST>
				<OLDAUDITENTRIES.LIST></OLDAUDITENTRIES.LIST>
				<ACCOUNTAUDITENTRIES.LIST></ACCOUNTAUDITENTRIES.LIST>
				<AUDITENTRIES.LIST></AUDITENTRIES.LIST>
				<INPUTCRALLOCS.LIST></INPUTCRALLOCS.LIST>
				<INVENTORYALLOCATIONS.LIST></INVENTORYALLOCATIONS.LIST>
				<DUTYHEADDETAILS.LIST></DUTYHEADDETAILS.LIST>
				<EXCISEDUTYHEADDETAILS.LIST></EXCISEDUTYHEADDETAILS.LIST>
				<RATEDETAILS.LIST></RATEDETAILS.LIST>
				<SUMMARYALLOCS.LIST></SUMMARYALLOCS.LIST>
				<STPYMTDETAILS.LIST></STPYMTDETAILS.LIST>
				<EXCISEPAYMENTALLOCATIONS.LIST></EXCISEPAYMENTALLOCATIONS.LIST>
				<TAXBILLALLOCATIONS.LIST></TAXBILLALLOCATIONS.LIST>
				<TAXOBJECTALLOCATIONS.LIST></TAXOBJECTALLOCATIONS.LIST>
				<TDSEXPENSEALLOCATIONS.LIST></TDSEXPENSEALLOCATIONS.LIST>
				<VATSTATUTORYDETAILS.LIST></VATSTATUTORYDETAILS.LIST>
				<REFVOUCHERDETAILS.LIST></REFVOUCHERDETAILS.LIST>
				<INVOICEWISEDETAILS.LIST></INVOICEWISEDETAILS.LIST>
				<VATITCDETAILS.LIST></VATITCDETAILS.LIST>
				<ADVANCETAXDETAILS.LIST></ADVANCETAXDETAILS.LIST>
				</LEDGERENTRIES.LIST>
				<LEDGERENTRIES.LIST>
				<TAXCLASSIFICATIONNAME/>
				<LEDGERNAME>'.$ledgernamegst.'</LEDGERNAME>
				<METHODTYPE>As User Defined Value</METHODTYPE>
				<VOUCHERFBTCATEGORY/>
				<GSTCLASS/>
				<ISDEEMEDPOSITIVE>No</ISDEEMEDPOSITIVE>
				<LEDGERFROMITEM>No</LEDGERFROMITEM>
				<REMOVEZEROENTRIES>No</REMOVEZEROENTRIES>
				<ISPARTYLEDGER>No</ISPARTYLEDGER>
				<ISLASTDEEMEDPOSITIVE>No</ISLASTDEEMEDPOSITIVE>
				<SERVICETAXDETAILS.LIST></SERVICETAXDETAILS.LIST>
				<BANKALLOCATIONS.LIST></BANKALLOCATIONS.LIST>
				<BILLALLOCATIONS.LIST></BILLALLOCATIONS.LIST>
				<INTERESTCOLLECTION.LIST></INTERESTCOLLECTION.LIST>
				<OLDAUDITENTRIES.LIST></OLDAUDITENTRIES.LIST>
				<ACCOUNTAUDITENTRIES.LIST></ACCOUNTAUDITENTRIES.LIST>
				<AUDITENTRIES.LIST></AUDITENTRIES.LIST>
				<INPUTCRALLOCS.LIST></INPUTCRALLOCS.LIST>
				<INVENTORYALLOCATIONS.LIST></INVENTORYALLOCATIONS.LIST>
				<DUTYHEADDETAILS.LIST></DUTYHEADDETAILS.LIST>
				<EXCISEDUTYHEADDETAILS.LIST></EXCISEDUTYHEADDETAILS.LIST>
				<RATEDETAILS.LIST></RATEDETAILS.LIST>
				<SUMMARYALLOCS.LIST></SUMMARYALLOCS.LIST>
				<STPYMTDETAILS.LIST></STPYMTDETAILS.LIST>
				<EXCISEPAYMENTALLOCATIONS.LIST></EXCISEPAYMENTALLOCATIONS.LIST>
				<TAXBILLALLOCATIONS.LIST></TAXBILLALLOCATIONS.LIST>
				<TAXOBJECTALLOCATIONS.LIST></TAXOBJECTALLOCATIONS.LIST>
				<TDSEXPENSEALLOCATIONS.LIST></TDSEXPENSEALLOCATIONS.LIST>
				<VATSTATUTORYDETAILS.LIST></VATSTATUTORYDETAILS.LIST>
				<REFVOUCHERDETAILS.LIST></REFVOUCHERDETAILS.LIST>
				<INVOICEWISEDETAILS.LIST></INVOICEWISEDETAILS.LIST>
				<VATITCDETAILS.LIST></VATITCDETAILS.LIST>
				<ADVANCETAXDETAILS.LIST></ADVANCETAXDETAILS.LIST>
				</LEDGERENTRIES.LIST>
	';
						 
							 
	$requestXMLEND='					
<PAYROLLMODEOFPAYMENT.LIST>      </PAYROLLMODEOFPAYMENT.LIST>
      <ATTDRECORDS.LIST>      </ATTDRECORDS.LIST>
     </VOUCHER>
    </TALLYMESSAGE>

   </REQUESTDATA>
  </IMPORTDATA>
 </BODY>
</ENVELOPE>';


           }
                  $ALLINVENTORYENTRIESITEMREPORT=array();
                  $ALLINVENTORYENTRIESITEMs=array();
               $itemnameis=array_unique($itemcode);

            $itemname= array_values(array_filter($itemnameis));

            for($j=0;$j<count($itemname);$j++)
            {
            //$STOCKITEM=$itemname[$j];
            $item_details=$this->_getSelectList2('table_item AS I',"item_name,item_division,item_id",''," I.item_code='".$itemname[$j]."'  ");
           // print_r($item_details[0]->item_name);exit;
            if($item_details[0]->item_division==3)
				        	{
                                $godown_name="CHN ". ucwords(strtolower($arr[1]))." Sales";
                                $godown_namereport="CHN ". ucwords(strtolower($arr1[1]))." Sales";
                                

				        	}else if($item_details[0]->item_division==1)
				        	{
				        		 $godown_name="AMPS ". ucwords(strtolower($arr[1]))." Sales";
				        		 $godown_namereport="AMPS ". ucwords(strtolower($arr1[1]))." Sales";

				        	}else if($item_details[0]->item_division==2)
				        	{
				        		 $godown_name="Inst ". ucwords(strtolower($arr[1]))." Sales";
				        		 $godown_namereport="Inst ". ucwords(strtolower($arr1[1]))." Sales";
				        	}




				        	



            $STOCKITEM=$item_details[0]->item_name;
			
	         $ACTUALQTY= $itemsbsn[$itemname[$j]]['count'];


	        $ALLINVENTORYENTRIESITEMs[] ='
													<ALLINVENTORYENTRIES.LIST>
													<STOCKITEMNAME>'.$STOCKITEM.'</STOCKITEMNAME>
													<ISDEEMEDPOSITIVE>Yes</ISDEEMEDPOSITIVE>
													<ISLASTDEEMEDPOSITIVE>Yes</ISLASTDEEMEDPOSITIVE>
													<ISAUTONEGATE>No</ISAUTONEGATE>
													<ISCUSTOMSCLEARANCE>No</ISCUSTOMSCLEARANCE>
													<ISTRACKCOMPONENT>No</ISTRACKCOMPONENT>
													<ISTRACKPRODUCTION>No</ISTRACKPRODUCTION>
													<ISPRIMARYITEM>No</ISPRIMARYITEM>
													<ISSCRAP>No</ISSCRAP>
													<RATE></RATE>
													<AMOUNT></AMOUNT>
													<ACTUALQTY>'.$ACTUALQTY.' nos</ACTUALQTY>
													<BILLEDQTY>'.$ACTUALQTY.' nos</BILLEDQTY>
													<INCLVATRATE></INCLVATRATE>
													<BATCHALLOCATIONS.LIST>
													<GODOWNNAME>'.$godown_name.'</GODOWNNAME>
													<BATCHNAME>Primary Batch</BATCHNAME>
													<DESTINATIONGODOWNNAME>'.$godown_name.'</DESTINATIONGODOWNNAME>
													<INDENTNO/>
													<ORDERNO/>
													<TRACKINGNUMBER/>
													<DYNAMICCSTISCLEARED>No</DYNAMICCSTISCLEARED>
													<AMOUNT></AMOUNT>
													<ACTUALQTY>'.$ACTUALQTY.' nos</ACTUALQTY>
													<BILLEDQTY>'.$ACTUALQTY.' nos</BILLEDQTY>
													<INCLVATRATE></INCLVATRATE>
													<ADDITIONALDETAILS.LIST></ADDITIONALDETAILS.LIST>
													<VOUCHERCOMPONENTLIST.LIST></VOUCHERCOMPONENTLIST.LIST>
													</BATCHALLOCATIONS.LIST>
													<ACCOUNTINGALLOCATIONS.LIST>
													<OLDAUDITENTRYIDS.LIST TYPE="Number">
													<OLDAUDITENTRYIDS>-1</OLDAUDITENTRYIDS>
													</OLDAUDITENTRYIDS.LIST>
													<LEDGERNAME>'.$ledgername.'</LEDGERNAME>
													<GSTCLASS/>
													<ISDEEMEDPOSITIVE>Yes</ISDEEMEDPOSITIVE>
													<LEDGERFROMITEM>No</LEDGERFROMITEM>
													<REMOVEZEROENTRIES>No</REMOVEZEROENTRIES>
													<ISPARTYLEDGER>No</ISPARTYLEDGER>
													<ISLASTDEEMEDPOSITIVE>Yes</ISLASTDEEMEDPOSITIVE>
													<AMOUNT></AMOUNT>
													<SERVICETAXDETAILS.LIST></SERVICETAXDETAILS.LIST>
													<BANKALLOCATIONS.LIST></BANKALLOCATIONS.LIST>
													<BILLALLOCATIONS.LIST></BILLALLOCATIONS.LIST>
													<INTERESTCOLLECTION.LIST></INTERESTCOLLECTION.LIST>
													<OLDAUDITENTRIES.LIST></OLDAUDITENTRIES.LIST>
													<ACCOUNTAUDITENTRIES.LIST></ACCOUNTAUDITENTRIES.LIST>
													<AUDITENTRIES.LIST></AUDITENTRIES.LIST>
													<INPUTCRALLOCS.LIST></INPUTCRALLOCS.LIST>
													<DUTYHEADDETAILS.LIST></DUTYHEADDETAILS.LIST>
													<EXCISEDUTYHEADDETAILS.LIST></EXCISEDUTYHEADDETAILS.LIST>
													<RATEDETAILS.LIST></RATEDETAILS.LIST>
													<SUMMARYALLOCS.LIST></SUMMARYALLOCS.LIST>
													<STPYMTDETAILS.LIST></STPYMTDETAILS.LIST>
													<EXCISEPAYMENTALLOCATIONS.LIST></EXCISEPAYMENTALLOCATIONS.LIST>
													<TAXBILLALLOCATIONS.LIST></TAXBILLALLOCATIONS.LIST>
													<TAXOBJECTALLOCATIONS.LIST></TAXOBJECTALLOCATIONS.LIST>
													<TDSEXPENSEALLOCATIONS.LIST></TDSEXPENSEALLOCATIONS.LIST>
													<VATSTATUTORYDETAILS.LIST></VATSTATUTORYDETAILS.LIST>
													<COSTTRACKALLOCATIONS.LIST></COSTTRACKALLOCATIONS.LIST>
													<REFVOUCHERDETAILS.LIST></REFVOUCHERDETAILS.LIST>
													<INVOICEWISEDETAILS.LIST></INVOICEWISEDETAILS.LIST>
													<VATITCDETAILS.LIST></VATITCDETAILS.LIST>
													<ADVANCETAXDETAILS.LIST></ADVANCETAXDETAILS.LIST>
													</ACCOUNTINGALLOCATIONS.LIST>
													<DUTYHEADDETAILS.LIST></DUTYHEADDETAILS.LIST>
													<SUPPLEMENTARYDUTYHEADDETAILS.LIST></SUPPLEMENTARYDUTYHEADDETAILS.LIST>
													<TAXOBJECTALLOCATIONS.LIST></TAXOBJECTALLOCATIONS.LIST>
													<REFVOUCHERDETAILS.LIST></REFVOUCHERDETAILS.LIST>
													<EXCISEALLOCATIONS.LIST></EXCISEALLOCATIONS.LIST>
													<EXPENSEALLOCATIONS.LIST></EXPENSEALLOCATIONS.LIST>
													</ALLINVENTORYENTRIES.LIST>';



 $ALLINVENTORYENTRIESITEMREPORT[] ='
													<ALLINVENTORYENTRIES.LIST>
													<STOCKITEMNAME>'.$STOCKITEM.'</STOCKITEMNAME>
													<ISDEEMEDPOSITIVE>Yes</ISDEEMEDPOSITIVE>
													<ISLASTDEEMEDPOSITIVE>Yes</ISLASTDEEMEDPOSITIVE>
													<ISAUTONEGATE>No</ISAUTONEGATE>
													<ISCUSTOMSCLEARANCE>No</ISCUSTOMSCLEARANCE>
													<ISTRACKCOMPONENT>No</ISTRACKCOMPONENT>
													<ISTRACKPRODUCTION>No</ISTRACKPRODUCTION>
													<ISPRIMARYITEM>No</ISPRIMARYITEM>
													<ISSCRAP>No</ISSCRAP>
													<RATE></RATE>
													<AMOUNT></AMOUNT>
													<ACTUALQTY>'.$ACTUALQTY.' nos</ACTUALQTY>
													<BILLEDQTY>'.$ACTUALQTY.' nos</BILLEDQTY>
													<INCLVATRATE></INCLVATRATE>
													<BATCHALLOCATIONS.LIST>
													<GODOWNNAME>'.$godown_namereport.'</GODOWNNAME>
													<BATCHNAME>Primary Batch</BATCHNAME>
													<DESTINATIONGODOWNNAME>'.$godown_namereport.'</DESTINATIONGODOWNNAME>
													<INDENTNO/>
													<ORDERNO/>
													<TRACKINGNUMBER/>
													<DYNAMICCSTISCLEARED>No</DYNAMICCSTISCLEARED>
													<AMOUNT></AMOUNT>
													<ACTUALQTY>'.$ACTUALQTY.' nos</ACTUALQTY>
													<BILLEDQTY>'.$ACTUALQTY.' nos</BILLEDQTY>
													<INCLVATRATE></INCLVATRATE>
													<ADDITIONALDETAILS.LIST></ADDITIONALDETAILS.LIST>
													<VOUCHERCOMPONENTLIST.LIST></VOUCHERCOMPONENTLIST.LIST>
													</BATCHALLOCATIONS.LIST>
													<ACCOUNTINGALLOCATIONS.LIST>
													<OLDAUDITENTRYIDS.LIST TYPE="Number">
													<OLDAUDITENTRYIDS>-1</OLDAUDITENTRYIDS>
													</OLDAUDITENTRYIDS.LIST>
													<LEDGERNAME>'.$ledgername_report.'</LEDGERNAME>
													<GSTCLASS/>
													<ISDEEMEDPOSITIVE>Yes</ISDEEMEDPOSITIVE>
													<LEDGERFROMITEM>No</LEDGERFROMITEM>
													<REMOVEZEROENTRIES>No</REMOVEZEROENTRIES>
													<ISPARTYLEDGER>No</ISPARTYLEDGER>
													<ISLASTDEEMEDPOSITIVE>Yes</ISLASTDEEMEDPOSITIVE>
													<AMOUNT></AMOUNT>
													<SERVICETAXDETAILS.LIST></SERVICETAXDETAILS.LIST>
													<BANKALLOCATIONS.LIST></BANKALLOCATIONS.LIST>
													<BILLALLOCATIONS.LIST></BILLALLOCATIONS.LIST>
													<INTERESTCOLLECTION.LIST></INTERESTCOLLECTION.LIST>
													<OLDAUDITENTRIES.LIST></OLDAUDITENTRIES.LIST>
													<ACCOUNTAUDITENTRIES.LIST></ACCOUNTAUDITENTRIES.LIST>
													<AUDITENTRIES.LIST></AUDITENTRIES.LIST>
													<INPUTCRALLOCS.LIST></INPUTCRALLOCS.LIST>
													<DUTYHEADDETAILS.LIST></DUTYHEADDETAILS.LIST>
													<EXCISEDUTYHEADDETAILS.LIST></EXCISEDUTYHEADDETAILS.LIST>
													<RATEDETAILS.LIST></RATEDETAILS.LIST>
													<SUMMARYALLOCS.LIST></SUMMARYALLOCS.LIST>
													<STPYMTDETAILS.LIST></STPYMTDETAILS.LIST>
													<EXCISEPAYMENTALLOCATIONS.LIST></EXCISEPAYMENTALLOCATIONS.LIST>
													<TAXBILLALLOCATIONS.LIST></TAXBILLALLOCATIONS.LIST>
													<TAXOBJECTALLOCATIONS.LIST></TAXOBJECTALLOCATIONS.LIST>
													<TDSEXPENSEALLOCATIONS.LIST></TDSEXPENSEALLOCATIONS.LIST>
													<VATSTATUTORYDETAILS.LIST></VATSTATUTORYDETAILS.LIST>
													<COSTTRACKALLOCATIONS.LIST></COSTTRACKALLOCATIONS.LIST>
													<REFVOUCHERDETAILS.LIST></REFVOUCHERDETAILS.LIST>
													<INVOICEWISEDETAILS.LIST></INVOICEWISEDETAILS.LIST>
													<VATITCDETAILS.LIST></VATITCDETAILS.LIST>
													<ADVANCETAXDETAILS.LIST></ADVANCETAXDETAILS.LIST>
													</ACCOUNTINGALLOCATIONS.LIST>
													<DUTYHEADDETAILS.LIST></DUTYHEADDETAILS.LIST>
													<SUPPLEMENTARYDUTYHEADDETAILS.LIST></SUPPLEMENTARYDUTYHEADDETAILS.LIST>
													<TAXOBJECTALLOCATIONS.LIST></TAXOBJECTALLOCATIONS.LIST>
													<REFVOUCHERDETAILS.LIST></REFVOUCHERDETAILS.LIST>
													<EXCISEALLOCATIONS.LIST></EXCISEALLOCATIONS.LIST>
													<EXPENSEALLOCATIONS.LIST></EXPENSEALLOCATIONS.LIST>
													</ALLINVENTORYENTRIES.LIST>';


              
          }

             

            $requestXMLITEMLIST = implode(' ', $ALLINVENTORYENTRIESITEMs);
			$requestXMLITEMLISTREPORT = implode(' ', $ALLINVENTORYENTRIESITEMREPORT);
			//echo"<pre>";
			//print_R($requestXMLITEMLISTREPORT);
			 //      echo"----------------------------------";
			 //      print_R($requestXMLITEMLIST);
		    $requestXML = $requestXMLSTART.$requestXMLITEMLIST.$requestXMLEND;


		   // $requestXMLREPORT = $requestXMLSTARTReport.$requestXMLITEMLISTREPORT.$requestXMLEND;
		   // $first=array();
			//$second=array();
			$serverwr = 'http://quytech14.ddns.net:8000/';
            $headers = array( "Content-type: text/xml" ,"Content-length: ".strlen($requestXML) ,"Connection: close" );
			$ch3 = curl_init();
			curl_setopt($ch3, CURLOPT_URL, $serverwr);
			curl_setopt($ch3, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch3, CURLOPT_TIMEOUT, 100);
			curl_setopt($ch3, CURLOPT_POST, true);
			curl_setopt($ch3, CURLOPT_POSTFIELDS, $requestXML);
			curl_setopt($ch3, CURLOPT_HTTPHEADER, $headers);
			$data = curl_exec($ch3);
			echo"<pre>";
			print_R($data);
			exit;

			
           if(curl_errno($ch3)){
		    echo json_encode(array('status'=>'Failed', 'data'=> print curl_error($ch3)));

		    }else{

		    	   $p = xml_parser_create();
        xml_parse_into_struct($p, $data, $vals, $index);
       xml_parser_free($p);
		   if(isset($vals[$index['LASTVCHID'][0]]['value']) && $vals[$index['LASTVCHID'][0]]['value']>0) {

		    $tallymasterno=$vals[$index['LASTVCHID'][0]]['value'];
        //$attributeArray = array();
      //  $attributeArray['TALLYMASTERNO'] = $vals[$index['LASTVCHID'][0]]['value'];
        
        //$yu=$this->_dbUpdate2($attributeArray,'table_warehouse_stk_trn_dts', "transfer_id =".$transfer_id );  
//print_R($yu);exit;

       $first=array('status'=>'Push To Tally Successfully', 'data'=> " Push To Tally Successfully.");


            //echo json_encode(array('status'=>'Push To Tally Successfully', 'data'=> " Push To Tally Successfully.", 'id'=>$vals[$index['LASTVCHID'][0]]['value']));

    } else {
      
      if(isset($vals[$index['LINEERROR'][0]]['value'])) 
        $message = $vals[$index['LINEERROR'][0]]['value'];
      else 
        $message = $vals[0]['value'];


          $first = array('status'=>'Failed', 'data'=> $message);
    }


    curl_close($ch3);

      

               } 
      

             $tallyretun=   json_encode(array("first"=>$first['data'], "second"=>$second['data']));
              
             //print_R($tallyretun);
             $jsonAsObject   = json_decode($tallyretun);
             $jsonAsArray    = json_decode($tallyretun, TRUE);
             $fromwh="From Warehouse:".$jsonAsObject->first;
             $towh="To Warehouse:".$jsonAsObject->second;
             $message=$fromwh.$towh;
             return $message;

	
}





}

?>
