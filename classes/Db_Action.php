<?php

/********************************************************************************************************

 FileName: class.db_action.php

 Package: Class

 Purpose: Class File is used for manage the sql query like insert,update,select,delete etc.

/*******************************************************************************************************/ 

class Db_Action extends DB

{	

	/**
	 * The Create a pagination class object
	 * @var object
	 */

	var $paging;

	/**
	 * Define the list of pages object
	 * @var object
	 */

	var $pagelist;

	/**
	 * Ceate a sort class object
	 * @var object
	 */

	var $objsort;
	
	public $globalparameter = array();

	//-------------------------------------------------------------//----------------------------------------------------------//

	function __construct($pagerFunName='frmsubmit')

	{		

		//$this->sql_db = new sql_db(MYSQL_HOST_NAME, MYSQL_USER_NAME, MYSQL_PASSWORD, MYSQL_DB_NAME, false);// create a connection

		parent::__construct(MYSQL_HOST_NAME, MYSQL_USER_NAME, MYSQL_PASSWORD, MYSQL_DB_NAME, false);

		$this->paging = new Pager($pagerFunName); //create a pagination class object		

		$this->objsort=new SortData(); //create a sorting class object

		$this->globalparameter = array(
			'salesman'=>array('table'=>'table_salesman', 'key'=>'salesman_id', 'Alias'=>'S','salesman_id'=>'','condition'=>'','detail'=>''),
			'retailer'=>array('table'=>'table_retailer', 'key'=>'retailer_id', 'Alias'=>'R','retailer_id'=>'','condition'=>'','detail'=>''),
			'distributor'=>array('table'=>'table_distributors', 'key'=>'distributor_id', 'Alias'=>'D','distributor_id'=>'','condition'=>'','detail'=>''),
			'retailer'=>array('table'=>'table_retailer', 'key'=>'retailer_id', 'Alias'=>'R','retailer_id'=>'','condition'=>'','detail'=>''),
			'city'=>array('table'=>'city', 'key'=>'city_id', 'Alias'=>'C','city_id'=>'','condition'=>'','detail'=>''),
			'state'=>array('table'=>'state', 'key'=>'state_id', 'Alias'=>'ST','state_id'=>'','condition'=>'','detail'=>''),
			'category'=>array('table'=>'table_category', 'key'=>'category_id', 'Alias'=>'CAT','category_id'=>'','condition'=>'','detail'=>''),
			'items'=>array('table'=>'table_items', 'key'=>'item_id', 'Alias'=>'ITM','item_id'=>'','condition'=>'','detail'=>''),
			'others'=>array('account_id'=>'', 'from_date'=>'', 'to_date'=>'','orderby'=>'','gap'=>'', 'month'=>'', 'year'=>'','items'=>''));

	}

	/**	 
	 * inserted record
	 * @param array an associative array of fields and values
	 * @param string the name of the table to insert the record into	
	 * @param string the SQL query for inserting the record
	 * @return string the id of the inserted record
	 */
	 
	 
	/* function getSalesCondition($tableAlias, $key, $salsList) {
	
		 if(is_array($salsList) && !empty($salsList) && $salsList[0]!='') {
		   
		   	$Ids = implode(',', array_unique($salsList));
		   		$condition =" AND ".$tableAlias.".".$key." IN ($Ids)";
			return $condition;
		} 
	} */


	function _dbInsert($cols,$table_name)

	{		

		$col1=array_keys($cols);

		$col=implode(', ',$col1);

		$vals=array_values($cols);

		$cnt=1;

		foreach($vals as $val) {

		if($cnt==1)

		$values = "'".$val."'";

		else

		$values .= ",'".$val."'";

		$cnt++;

		}

		$sql = "insert into ".$table_name." (".$col.") values (".$values.")";

		//echo $sql;exit;

		$this->sql_query($sql);

		$current_id = $this->sql_nextid();

		return $current_id;

	}

	/**	
	 * updated record
	 * @param array an associative array of fields and values
	 * @param string the name of the table to update the record into	
	 * @param string the where clause (e.g. WHERE "recId = 21")
	 * @param string the SQL query for updating the record
	 * @return bool true if operation is successfull, false if not
	 */

	function _dbUpdate($cols,$table_name,$condition='')

	{		

		$col1=array_keys($cols);
		$alias_name = $this->getAlias($table_name);
		$vals=array_values($cols);

		$i=0;

		$cnt=1;

		foreach($vals as $val) {

		if($cnt==1)

		$values = $col1[$i]."='".$val."'";

		else

		$values .= ",".$col1[$i]."='".$val."'";

		$cnt++;

		$i++;

		}

		$sql = "update ".$table_name." set ".$values."  "."WHERE ".$alias_name.""."."."account_id=".$_SESSION['accountId']." and ".$condition;

		//echo "<li>".$sql;

		$this->sql_query($sql);

	}
	
	
	
	
	
	function _dbUpdate2($cols,$table_name,$condition='')

	{		

		$col1=array_keys($cols);
		//$alias_name = $this->getAlias($table_name);
		$vals=array_values($cols);
		$where = $this->getCondition($condition);

		$i=0;

		$cnt=1;

		foreach($vals as $val) {

		if($cnt==1)

		$values = $col1[$i]."='".$val."'";

		else

		$values .= ",".$col1[$i]."='".$val."'";

		$cnt++;

		$i++;

		}

		$sql = "update ".$table_name." set ".$values."  ".$where.$condition;

		//echo "<li>".$sql;

		$this->sql_query($sql);

	}
	
	
	
	
	

	/**
	 * Deleting method.	
	 * @param array an associative array of condition field values for the records to delete
	 * @param string the name of the table
	 * @param string the name of the id
	 * @param string for other condition in where clause 
	 * @return bool true if the operation is a succes, false if not
	 */

	function _dbDelete($delids,$table_name,$col,$othercondition='')

	{		

		//echo "delete from ".$table_name." ".$col."='$value' ".$othercondition;

		if(is_array($delids))

		{

			foreach($delids as $value)

			{

				//echo "delete from ".$table_name." ".$col."='$value' ".$othercondition;

				$this->sql_query("delete from ".$table_name." WHERE ".$col."='$value' ".$othercondition);

			}

		}

		else

		{

			$this->sql_query("delete from ".$table_name." WHERE ".$col."='$delids' ".$othercondition);

			

		}

	}

	/**
	 * Abstracts the process of getting a record set from the database.
	 * for count no of record.
	 * @param string the name of the table
	 * @param Array for the name of the fields
	 * @param string the where clause (e.g. WHERE "recId = 21")
	 * @return count no of records
	 */

	function _chkExists($table_name,$column_names='',$condition='')

	{		

		$sql='select '.$column_names.' from '.$table_name.' '.$condition;		

		$result =  $this->sql_query($sql);

		$count=$this->sql_numrows($result);

		

		return $count;

	}
	
	
	
	
	// Get Alias Name 
	
	
		function getAlias($table_name) {
	
	
			$table_str = preg_replace( "/\s+/", " ", trim($table_name));
			
			$strCount= preg_replace("/[_]+/","",$table_str);
			
 			$joinarray = array('left', 'right', 'full', 'inner','join');
			 
			 if(str_word_count($strCount)>=2){
			 
			  preg_match('/^(?>\S+\s*){1,3}/', $table_str, $match);
			  $arr = rtrim($match[0]);
			  $arr2 = explode(' ', $arr);
			  
			  if(strtolower($arr2[1]) == 'as') { return $alias_name = $arr2[2]; } 
			  else if(in_array(strtolower($arr2[1]), $joinarray)) { return $alias_name = $arr2[0]; } 
			  else { return $alias_name = $arr2[1];  } 
			
			 } else {
			  
			 return  $alias_name = trim($table_str);
			 
			 }


	}
	
	// ENd of alias name

	/**
	 * Abstracts the process of getting a record set from the database using PEAR::DB.
	 * The method gets the record set and then adds the rowNum associative array item
	 * for displaying the record number.
	 * @param string the name of the table
	 * @param Array for the name of the fields
	 * @param string the value of the record limit 
	 * @param string the where clause (e.g. WHERE "recId = 21")
	 * @param string the set default order of record 
	 * @return array the recordset 
	 */

	function _getSelectList($table_name,$column_names='',$limit='',$condition='',$defaultorder='',  $order_name="")

	{			

		$p= $this->paging;
		
		// Custom Function to get the Alias name of table
			$alias_name = $this->getAlias($table_name);
			$where = $this->getCondition($condition);			
			if($where!= "")  {$and = " and ";}
			
			

			if($column_names=='') { $column_names='*'; }

			$sql='select '.$column_names.' from '.$table_name.' '."WHERE ".$alias_name.""."."."account_id=".$_SESSION['accountId'].$and.$condition.'';
		
			$result = $this->sql_query($sql);

			$count=$this->sql_numrows($result);

			if(!isset($_REQUEST["page"]) && $_REQUEST["page"] == ""){			

			$_SESSION["sortorder"]="";

			} 	
		
			if(isset($_GET["field_name"]))

			$order_name=$_GET["field_name"];

			$sortSql = $this->objsort->getOrderSql($defaultorder, $order_name);

		

			$sql .= $sortSql;
			if($limit!='')

			{

			$start = $p->findStart($limit);

			$pages = $p->findPages($count, $limit); 
			
			$sql2 = $sql.' LIMIT '.$limit;
                        
			}
                       
			else { $sql2 = $sql; }
                       // echo $sql2 ;
			$result =$this->sql_query($sql2);
	
			$this->pagelist=$p->pageList($_REQUEST['page'], $pages);

			if($this->sql_numrows($result)>0) { $rows=$this->loadObjectList(); }

			return $rows;

		}
	



	// Select Query 2
	
			
		function getCondition($condition){
		//$condition=" Order By"
		$table_str = preg_replace( "/\s+/", " ", trim($condition));
		   $strCount= preg_replace("/[_]+/","",$table_str);
			$joinarray = array('order', 'group','and'); 
			if(str_word_count($strCount)>=1){
			
			 preg_match('/^(?>\S+\s*){1,1}/', $table_str, $match);
			 $arr = rtrim($match[0]);
			 $arr2 = explode(' ', $arr);    // print_r($arr2);
			 
		   
		  	 if(in_array(strtolower($arr2[0]), $joinarray)) { return $alias_name = ""; }  
			 else{ return $alias_name ="WHERE ";}
			
			}
		   }
		  
			
	function _getTruncate($table_name)

	{
		$sql='TRUNCATE '.$table_name.'';
		$result = $this->sql_query($sql);
	}
	
	function _getSelectList2($table_name,$column_names='',$limit='',$condition='',$defaultorder='',  $order_name="")

	{			

		$p= $this->paging;

		if($column_names=='')

		{

			$column_names='*';

		}
		$where = $this->getCondition($condition);
		
		
		$sql='select '.$column_names.' from '.$table_name.' '.$where.$condition.'';

		$result = $this->sql_query($sql);

		$count=$this->sql_numrows($result);

		if(!isset($_REQUEST["page"]) && $_REQUEST["page"] == ""){			

			$_SESSION["sortorder"]="";

		} 	


		if(isset($_GET["field_name"]))

			$order_name=$_GET["field_name"];

		$sortSql = $this->objsort->getOrderSql($defaultorder, $order_name);

		

		$sql .= $sortSql;

		if($limit!='')

		{

			$start = $p->findStart($limit);

			$pages = $p->findPages($count, $limit); 

		
                        $sql2 = $sql.' LIMIT '.$limit;

		}

		else

		{

			$sql2 = $sql;

		}
              //  echo "<br>";
               // echo $sql2;
		$result =$this->sql_query($sql2);

		$this->pagelist=$p->pageList($_REQUEST['page'], $pages);

		if($this->sql_numrows($result)>0)

		{

			$rows=$this->loadObjectList();

		}


		return $rows;

	}

	// End of select Query2














	function getUnion($sql1,$sql2,$limit='',$order='')

	{
		$p= $this->paging;
		$sql="(".$sql1.") union (".$sql2.") $order ";	
		
		$result = $this->sql_query($sql);

		$count=$this->sql_numrows($result);
		
		if($limit!='')

		{

			$start = $p->findStart($limit);

			$pages = $p->findPages($count, $limit); 

		

			$sql2 = $sql.' LIMIT '.$start.','.$limit;

		}

		else

		{

			$sql2 = $sql;

		}

		$result =$this->sql_query($sql2);

		$this->pagelist=$p->pageList($_REQUEST['page'], $pages);	

		if($this->sql_numrows($result)>0)

		{

			$rows=$this->loadObjectList();

		}	

		return $rows;

	}

	/**
	 * Abstracts the process of getting a paging set from the database.
	 * for count no of paging.
	 * @return the list of paging with linking format
	 */

	function paging()

	{

		return $this->pagelist;

	}

	/**
	 * Abstracts the process of executing a query using - inserts, updates, deletes.
	 * @param string the SQL to execute
	 */
	 
	 
# The ChangeDateFormate
	 function _changeDate($date) {
	 if($date=='0000-00-00' || $date==''){
	 	$new_date='';
	 }
	 else{
     		 $new_date = date('j M Y', strtotime($date));
        }
		return $new_date;
}



# The ChangeDateFormate
	 

/************************************************** Calculate Durations ******************************************/	 
	 
	 function _getDuration($duration,$start_date,$end_date)
		{
			$start = strtotime($start_date);
			$end = strtotime($end_date);
			$new_date=array($start_date);
			$date_arr=array();
/************************************************** Calculate Count Of Days ******************************************/
			
			$days_count = ceil(abs($end - $start) / 86400)+1;
/************************************************** Calculate Count Of Week ******************************************/
			$totaldays = ceil(abs($end - $start) / 86400);
			$week_count= $totaldays/7;
/************************************************** Calculate Count Of Month ******************************************/
			$months = 1;
			while (($start = strtotime('+1 MONTH', $start)) <= $end){ $months++; }
			if($duration=="5"){ $months=$months/3;}
			if($duration=="6"){ $months=$months/6;}
/************************************************** Calculate Count Of Year ******************************************/
			/*$year = 0;
			while (($start = strtotime('+1 YEAR', $start)) <= $end){ $year++; }*/
			$date_parts1=explode("-", $start_date);
		   	$date_parts2=explode("-", $end_date);
			$diff_year = $date_parts2[0] - $date_parts1[0];
			
			
/************************************************** Start Calculating Dates For days ******************************************/		
			if($duration=="1")
			{
				for($i=0;$i<$days_count;$i++)
					{
						$date = strtotime ('+1 days', strtotime($start_date)) ;
						$start_date = date ('Y-m-d' , $date);
						$date_arr[$i] = $start_date;
						$week_last_date=$start_date;
						array_push($new_date,$week_last_date);
						$get_newdate[]=array($new_date[$i],$new_date[$i+1]);
					}
			}
			
/************************************************** Start Calculating Dates For Week ******************************************/		
			if($duration=="2")
			{
				for($i=0;$i<$week_count;$i++)
					{
						$date = strtotime ('+7 days', strtotime($start_date)) ;
						$start_date = date ('Y-m-d' , $date);
						$date_arr[$i] = $start_date;
						$week_last_date=$start_date;
						array_push($new_date,$week_last_date);
						$get_newdate[]=array($new_date[$i],$new_date[$i+1]);
					}
			}
/************************************************** Start Calculating Dates For Month ******************************************/	
			if($duration=="4" || $duration=="5" || $duration=="6")
			{
				for($i=0;$i<$months;$i++)
					{
						if($duration=="4"){$date = strtotime ('+1 months', strtotime($start_date)) ;}
						if($duration=="5"){$date = strtotime ('+3 months', strtotime($start_date)) ;}
						if($duration=="6"){ $date = strtotime ('+6 months', strtotime($start_date)) ;}						
						$start_date = date ('Y-m-d' , $date);
						$date_arr[$i] = $start_date;
						$week_last_date=$start_date;
						array_push($new_date,$week_last_date);
						$get_newdate[]=array($new_date[$i],$new_date[$i+1]);
					}
			}
/************************************************** Start Calculating Dates For Month ******************************************/	
			if($duration=="7")
			{
				for($i=0;$i<$diff_year;$i++)
					{
						$date = strtotime ('+1 year', strtotime($start_date)) ;
						$start_date = date ('Y-m-d' , $date);
						$date_arr[$i] = $start_date;
						$week_last_date=$start_date;
						array_push($new_date,$week_last_date);
						$get_newdate[]=array($new_date[$i],$new_date[$i+1]);
					}
			}
			
/************************************************** Return result date in an array ******************************************/	
			return $get_newdate;
}
	 
	 public function returnObjectArrayToIndexArray($arr,$field) {  
							  $list = array();							 
							  if(is_array($arr)){							  
							   foreach($arr as $value) {							   
								$list[] = $value->$field;							   
							   }
							  }							 
							  return $list;
							 }

	public function mergarraybyindex($finalArray, $newArray) {
	//echo count($newArray);
	if(is_array($newArray) && count($newArray)>0){
		foreach($newArray as $key=>$value):
		//echo $key;
			if (array_key_exists($key, $finalArray)) {
			//print_r($newArray[$key]);
				foreach($newArray[$key] as $innerkey=>$val):
				//echo $innerkey;
					if (array_key_exists($innerkey, $finalArray[$key])) {
						$finalArray[$key][$innerkey] = $val;
					}
				endforeach;
			}
		endforeach;
	}
	return $finalArray;
	}							 

	function mysql_query($sql='')

	{

		

		$result = $this->sql_query($sql);	

		return $result;	

	}	 

		
	
	  /**
   * Multi-array search
   *
   * @param array $array
   * @param array $search
   * @return array
   */
 public function multi_array_search($array, $search)
  {
 //echo "<pre>";
 // print_r($array);
  //print_r($search);

    // Create the result array
    $result = array();

    // Iterate over each array element
    foreach ($array as $key => $value)
    {
	//print_r($value);
      // Iterate over each search condition
      foreach ($search as $k => $v)
      {
	  	//echo $k;
		//echo $value->$k;
		//echo $v;
        // If the array element does not meet the search condition then continue to the next element
        /*if (!isset($value->$k) || $value->$k != $v)
        {
          continue;
        }*/
		
		if($value->$k == $v) { $flag = true; continue; } else { $flag = false;  break; }

      }

      // Add the array element's key to the result array
      //$result[] = $value;
	  
	  if($flag == true) {
	  	return $value;
	  }

    }

    // Return the result array
    return $result;

  }
  
  /************************************* Start Function To Remove Special Characters *************************************/

public function clean($string) {

   $string = trim($string); // Replaces all spaces before and after the words.   
   $clear = trim(preg_replace('/ +/', ' ', preg_replace('/[^A-Za-z0-9\-\'\,\(\) ]/', '', urldecode(html_entity_decode(strip_tags($string)))))); // Removes special chars.
  return $clear;
   
}

/************************************* End Function To Remove Special Characters ***************************************/
  
  
  public function changeEntities($string){
  
 
  
  }
  
  
  /************************ Month list for distributor target (5th june 2015 Gaurav) *************************/
public function getMonthName($month){	
	$monthArray=array('January'=>1,'February'=>2,'March'=>3,'April'=>4,'May'=>5,'June'=>6,'July'=>7,'August'=>8,'September'=>9,'October'=>10,'November'=>11, 'December'=>12);
		
	foreach($monthArray as $key=>$value)
		{		
			if($value==$month){				
					$monthName=$key;				
				}		
		}	
	return $monthName;	
}

/************************ Month list for distributor target (5th june 2015 Gaurav) *************************/	
  
  
  
  	public function getSalesmanFullDetails($salesman_id) {

	if(isset($salesman_id))
		$sal_array = $this->_getSelectList2('table_salesman AS S LEFT JOIN state AS ST ON ST.state_id = S.state LEFT JOIN city AS C ON C.city_id = S.city LEFT JOIN table_salesman_hierarchy_relationship AS H ON H.salesman_id= S.salesman_id LEFT JOIN table_salesman AS S2 ON S2.salesman_id = H.rpt_user_id LEFT JOIN table_salesman_hierarchy AS SH ON SH.hierarchy_id = H.hierarchy_id LEFT JOIN table_web_users AS U ON U.salesman_id = S.salesman_id ',"S.salesman_id, S.account_id,ST.state_name, C.city_name, SH.description AS salesman_lvl,H.rpt_user_id AS rpt_id, S2.salesman_name AS rpt_to,S2.salesman_code AS rpt_person_code, S.salesman_name,S.salesman_id, U.email_id,U.user_type",''," S.salesman_id='".$salesman_id."'");
	
	
	return $sal_array;
	}
	
	
	
	public function getRetailerFullDetails($retailer_id) {

		if(isset($retailer_id))
		$rel_array = $this->_getSelectList('table_retailer AS R LEFT JOIN state AS ST ON ST.state_id = R.state LEFT JOIN city AS C ON C.city_id = R.city LEFT JOIN table_relationship AS REL ON REL.relationship_id= R.relationship_id',"ST.state_name, C.city_name, R.retailer_name,R.retailer_id, REL.relationship_code",''," R.retailer_id='".$retailer_id."'");

		return $rel_array;
	}
	
	
/***************************** Get Distributor Login Salesman ***************************/
	public function getDistributorWiseSalesmanDetails($disId){
				$retArray=array();
				$salArray=array();
				$getRetailerList=$this->_getSelectList('table_retailer as r','r.retailer_id',''," r.distributor_id='".$disId."'");			
				foreach($getRetailerList as $key=>$value){					
							$retArray[]=$value->retailer_id;					
					}
				if(sizeof($retArray)>0){ 
					
						$retList=implode(',',$retArray);		
						$SalList=$this->_getSelectList('table_order as o','DISTINCT(o.salesman_id)','',"  o.retailer_id IN (".$retList.")");
						foreach($SalList as $key=>$Salvalue){					
									$salArray[]=$Salvalue->salesman_id;					
							}
							//$List=implode(',',$salArray);
				
				}
				else {
				$salArray="";
			}	
			return($salArray);
		} 
	
/***************************** Get Distributor Login Salesman ***************************/
	
}?>
