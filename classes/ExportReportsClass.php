<?php class ExportReportsClass extends DB_Action {

	public $paramArrayList;
	public $data;
	var $user_id;
	var $table_name;
	var $key;
	var $condition;

	public function __construct($parameter) {
	
	parent::__construct();
	
	$this->data = array();
	//print_r($parameter);
	
	// Param Variables
	$this->salesman 		= $parameter['salesman']['condition']; // selected salesman_id condition
	$this->retailer 		= $parameter['retailer']['condition'];	// selected retailer_id condition
	$this->distributor 		= $parameter['distributor']['condition']; // selected distributor_id condition
	$this->city 			= $parameter['city']['condition'];
	$this->state 			= $parameter['state']['condition'];
	$this->category 		= $parameter['category']['condition'];
	$this->items 			= $parameter['items']['condition'];
	$this->account_id 		= $parameter['others']['account_id'];
	$this->from_date 		= $parameter['others']['from_date'];	// Selected From Date
	$this->to_date 			= $parameter['others']['to_date'];	// Selected End Date
	$this->orderbyoption 	= $parameter['others']['orderby']; // With Telephonic/Only Telephonic/Without Telephonic Order
	$this->gap 				= $parameter['others']['gap'];
	$this->month 			= $parameter['others']['month'];
	$this->year 			= $parameter['others']['year'];
	$this->itemslist 		= $parameter['others']['items'];
	
	
	if($this->from_date!=''){
		$this->fromdate = " AND O.date_of_order >= '".date('Y-m-d', strtotime($this->from_date))."'";
	} else {
		$this->fromdate = " AND O.date_of_order >= '".date('Y-m-d')."'";
	}
	
	if($this->to_date!=''){
		$this->todate = " AND O.date_of_order <= '".date('Y-m-d', strtotime($this->to_date))."'";
	} else {
		$this->todate = " AND O.date_of_order <= '".date('Y-m-d')."'";
	}
		
		//if(isset($parameter['salesman']['salesman_id']) && count($parameter['salesman']['salesman_id'])>0)  
			//$this->salesman = " AND ".$parameter['salesman']['Alias'].".".$parameter['salesman']['salesman_id']." IN ";
	
		


	}
	


 	// Salasman Order List
	public function admin_order_list() {
	
	$data=" Distributor Name\t Salesman\t Salesman State/City\t Reporting to\t Retailer\t Retailer Class\t Retailer Address\t Retailer Market\t Date\t Time\t Total Invoice Ammount\t Total Number Of Item\t Order Status\t Comments\n";
	
	$sort = " ORDER BY O.date_of_order desc,O.time_of_order desc";
	
	//echo $where.= " O.account_id = ".$this->account_id." AND ".$this->table_name.".".$this->key." IN (".$this->user_id.")".$this->fromdate.$this->todate." AND r.new='' ".$orderby;
	
		if(isset($this->orderbyoption) && $this->orderbyoption!="") {
			if($this->orderbyoption=1)
			 $this->orderby = ""; 
			elseif($this->orderbyoption==2)
				$this->orderby = " AND O.order_type='Adhoc' ";
			 else if($this->orderbyoption==3)
				$this->orderby =" AND O.order_type!='Adhoc' ";
		} 
	
	
	if(isset($this->account_id) && $this->account_id!="")
		$where = " O.account_id =".$this->account_id.$this->salesman.$this->fromdate.$this->todate." AND R.new=''".$this->orderbyoption;
	else 
		throw new Exception('No account id found. <br>');

	
	$auRec = $this->_getSelectList2("table_order AS O LEFT JOIN table_retailer AS R ON O.retailer_id = R.retailer_id LEFT JOIN table_relationship AS RR ON RR.relationship_id = R.relationship_id LEFT JOIN table_salesman AS S ON O.salesman_id = S.salesman_id LEFT JOIN table_distributors AS D ON O.distributor_id = D.distributor_id", " O.*, R.retailer_name, R.retailer_address, R.retailer_location, RR.relationship_code, R.lat AS retlat, R.lng AS retlng, S.salesman_name, D.distributor_name",'',$where.$sort,'');
	

	
	if(is_array($auRec)){
	 for($i=0;$i<count($auRec);$i++)
	 {
	 	if($auRec[$i]->order_type=='Yes')
		{
			if($auRec[$i]->order_status=='A'){ $status="New Order"; }
			
    		else if($auRec[$i]->order_status=='I'){ $status="Processed"; }
			
			else if($auRec[$i]->order_status=='D'){ $status="Dispatched"; }
			
			$color = ($auRec[$i]->order_status=='A')?"A":"P";
		} 
		
		
		if($auRec[$i]->order_type=='No')
		{
		 	$status=($auRec[$i]->order_status=='A')?"No Order":"Processed";
		 	$color=($auRec[$i]->order_status=='A')?"I":"P";
		}
		

		if($auRec[$i]->order_type=='Adhoc')
		{
			if($auRec[$i]->order_status=='A'){ $status="New Adhoc Order"; } 
			
			else if($auRec[$i]->order_status=='I'){ $status="Processed"; }
			
			else if($auRec[$i]->order_status=='D'){ $status="Dispatched"; }
			
			$color=($auRec[$i]->order_status=='A')?"O":"P";
			
		}
		
		$auRec2= $this->_getSelectList2('table_order_detail',"count( distinct(item_id)) as total_item",''," order_id = ".$auRec[$i]->order_id." and type = 1");
		
		$date_of_order = $this->_changeDate($auRec[$i]->date_of_order);   
		$remove = array("\n","\r");
		
		
	 	// Get Salesman full details
		$salDetails = $this->getSalesmanFullDetails($auRec[$i]->salesman_id);
		
		
		$data.="".$auRec[$i]->distributor_name."\t".$auRec[$i]->salesman_name."\t".$salDetails[0]->state_name.'/'.$salDetails[0]->city_name."\t".$salDetails[0]->rpt_to."\t".$auRec[$i]->retailer_name."\t".$auRec[$i]->relationship_code."\t".mysql_escape_string($auRec[$i]->retailer_address)."\t".str_replace($remove, ' ',$auRec[$i]->retailer_location)."\t".$date_of_order."\t".$auRec[$i]->time_of_order."\t".$auRec[$i]->acc_total_invoice_amount."\t".$auRec2[0]->total_item."\t".$status."\t".mysql_escape_string($auRec[$i]->comments)."\n";
		}
	}
	
	 else {
	$data.="Report Not Available \n";
	}
	//echo "<pre>";
	//echo $data;
	
	return $data;
	
	}
	
	// End of salasman Order List
	
	
	
	


	// Sales Return Report
	
	
	public function salesreturns(){
		
		$data="Salesman\t Distributor\t Retailer\t Number Of Item\t Date\t  Time\n";
		$sort = " ORDER BY O.date_of_order desc,O.time_of_order desc";
		$where = " O.account_id =".$this->account_id."  AND O.order_status='A' AND ostype IN ('Q')".$this->salesman.$this->fromdate.$this->todate;	
		
		$auRec=$this->_getSelectList2('table_sales_return AS O 
			LEFT JOIN table_retailer AS R ON O.retailer_id=R.retailer_id 
			LEFT JOIN table_salesman AS S ON O.salesman_id=S.salesman_id 
			LEFT JOIN table_distributors AS D ON O.distributor_id=D.distributor_id',
			"O.*,
			R.retailer_name,
			R.retailer_address,
			R.retailer_location,
			R.lat AS retlat,
			R.lng AS retlng,
			S.salesman_name,
			D.distributor_name",'',$where.$sort,'');
			
			if(is_array($auRec)){
			
			for($i=0;$i<count($auRec);$i++){
		
		if(ucwords(strtolower($auRec[$i]->order_type))=='Yes'){
			if($auRec[$i]->order_status=='A'){
				$status="New Sales Return";
			}
			
			if($auRec[$i]->order_status=='I')
			{
				$status="Processed";
			}
			
			if($auRec[$i]->order_status=='D')
			{
				$status="Dispatched";
			}
		} 
			
		$auRec2=$this->_getSelectList2('table_sales_return_detail INNER JOIN table_item ON table_item.item_id = table_sales_return_detail.item_id INNER JOIN table_price on table_sales_return_detail.item_id=table_price.item_id',"count(table_sales_return_detail.item_id) AS total_item, SUM(quantity * item_mrp) AS Total ",''," sales_order_id = ".$auRec[$i]->sales_order_id."");
		
	if($auRec[$i]->distributor_name!=''){
	$distributor=$auRec[$i]->distributor_name;
	}
	else{
	$distributor="-";
	}
	
		$data.="".$auRec[$i]->salesman_name."\t".$distributor."\t".$auRec[$i]->retailer_name."\t".$auRec2[0]->total_item."\t".$this->_changeDate($auRec[$i]->date_of_order)."\t".$auRec[$i]->time_of_order."\n";
		}
		}
		
		
		else {
	$data.="Report Not Available \n";
	}
		
		return $data;
	
	
	
	}
	
	
	// End of Sales Return Report

	
	
	
	
	
 	// Salesman Activity Report
	public function activity_report() {
	
	$data=" Salesman\t Date\t Day\t Market\t Total Calls\t Total Productive Calls\t Total No. of New Retailer\t Total Amount of Order\n";	
	if(isset($this->account_id) && $this->account_id!="")
			$where = " O.account_id =".$this->account_id.$this->salesman.$this->fromdate.$this->todate."";
			
		else 
			throw new Exception('No account id found. <br>');
	
			$sortBy=" Group by O.date_of_order,O.salesman_id ORDER BY O.date_of_order asc,s.salesman_name asc ";
			
	$data=" Salesman\t Date\t Day\t Market\t Total Calls\t Total Productive Calls\t Total No. of New Retailer\t Total Productive Calls\n";	
		$auRet=$this->_getSelectList2('table_order as O left join table_salesman as s on s.salesman_id=O.salesman_id',"O.date_of_order,O.salesman_id,s.salesman_name",'',$where.$sortBy);
			if(is_array($auRet)){
				for($i=0;$i<count($auRet);$i++)
				{
					
				$td1=$this->_getSelectList('table_order as o left join table_retailer as r on o.retailer_id=r.retailer_id','count(order_id) as total_call, sum( o.total_invoice_amount ) as total_amt ',''," o.date_of_order='".$auRet[$i]->date_of_order."' and r.new='' and o.salesman_id='".$auRet[$i]->salesman_id."'"); 

				$td2=$this->_getSelectList('table_order as o left join table_retailer as r on o.retailer_id=r.retailer_id','count(order_id) as productive_call',''," o.date_of_order='".$auRet[$i]->date_of_order."' and o.order_type != 'No' and r.new='' and o.salesman_id='".$auRet[$i]->salesman_id."'");
				
				$td3=$this->_getSelectList('table_retailer','count(retailer_id) as total_retailer',''," start_date='".$auRet[$i]->date_of_order."' and salesman_id='".$auRet[$i]->salesman_id."'");
				
				$td4=$this->_getSelectList('table_retailer','DISTINCT(retailer_location)',''," retailer_id in(Select DISTINCT(retailer_id) from table_order where salesman_id='".$auRet[$i]->salesman_id."' and date_of_order='".$auRet[$i]->date_of_order."') Order by retailer_location asc");
				for($a=0;$a<count($td4);$a++)
				{
					$market.=$comma.$td4[$a]->retailer_location;
					$comma=", ";
				}
				$data.="".$auRet[$i]->salesman_name."\t".$this->_changeDate($auRet[$i]->date_of_order)."\t".date("l",strtotime($auRet[$i]->date_of_order))."\t".strtoupper($market)."\t".$td1[0]->total_call."\t".$td2[0]->productive_call."\t".$td3[0]->total_retailer."\t".$td1[0]->total_amt."\n";
				
				unset($market);
				unset($comma);
				}
			} else {
			
				$data.="Report Not Available \n";
			
			}
			return $data;
	
	}
	
	// End of Salesman Activity Report
	
	public function attendance_report() {
	
	
		if(isset($this->account_id) && $this->account_id!="")
			$where = " S.account_id =".$this->account_id.$this->salesman." AND (O.activity_date BETWEEN '".$this->from_date."' AND '".$this->to_date."')";
			
		else 
			throw new Exception('No account id found. <br>');
	$sortByOrder=" order by S.salesman_name asc, O.activity_date asc";
	
	$auAttSt=$this->_getSelectList2('sal_id as O left join act_in as N on O.salesman_id=N.salesman_id and O.activity_date=N.activity_date left join act_out as AO on O.salesman_id=AO.salesman_id and O.activity_date=AO.activity_date left join table_salesman as S on S.salesman_id=O.salesman_id',"S.salesman_name,S.salesman_id,N.attendance_start,AO.attendance_end,O.activity_date,N.activity_id as in_activity,AO.activity_id as out_activity",'',$where.$sortByOrder);
	
	$data=" Salesman\t Date\t Day\t In Time\t Out Time\t Working Hours(-20 minutes)\n ";
	
	
		$daywork = array();
			if(is_array($auAttSt)){
				for($i=0;$i<count($auAttSt);$i++)
				{
				$daywork[] = $auAttSt[$i]->activity_date;
				if($auAttSt[$i]->attendance_start!='' && $auAttSt[$i]->attendance_end!='') {	
				$time1 = date('H:i:s', strtotime('+20 minutes', strtotime($auAttSt[$i]->attendance_start))); // start time + 20min
				$time2 = $auAttSt[$i]->attendance_end; //End Time
				list($hours, $minutes, $sec) = explode(':', $time1);
				$startTimestamp = mktime($hours, $minutes, $sec);
				list($hours, $minutes, $sec) = explode(':', $time2);
				$endTimestamp = mktime($hours, $minutes, $sec);
				$diff = $endTimestamp - $startTimestamp;
				if($diff > 0){
				$minutes = ($diff / 60) % 60;
				$hours = floor($diff / 3600);
				$seconds = $diff  % 60;
				} else {
				$minutes = ($diff / 60) % 60;
				$hours = ceil($diff / 3600);
				$seconds = abs($diff  % 60);
				}
				if($seconds<=9){
				$seconds="0".$seconds;
				}
				 
				if (strstr($hours,'-')) 
					{
						if($hours=='0'){  $hours = '00'; }
						else
						{
					            $hours = '-'.date('H', strtotime($hours.':00:00'));
						}
					} 
				else{
					 $minutes =  date('i', strtotime('00:'.$minutes.':00'));
					
					$hours = 	date('H', strtotime($hours.':00:00'));
					}
				if (strstr($minutes,'-')){
						if($minutes<0 && $minutes>='-9' )
							{
								$minutes = '-0'.abs($minutes);
							}
					}
				if (strstr($hours,'-') || strstr($minutes,'-')){
					
					            $hours = '-'.date('H', strtotime($hours.':00:00'));
							    $minutes = date('i', strtotime('00:'.abs($minutes).':00'));
					
					} 
				  $work_hours=$hours.":".$minutes.":".$seconds; 
				///$time1 = date('H:i:s', strtotime('+20 minutes', strtotime($auAttSt[$i]->start_time))); // start time + 20min
				} else {
				$work_hours="-";
				}
				
				
				if($auAttSt[$i]->attendance_start!=''){
					$inTime = $auAttSt[$i]->attendance_start;
				}
				else {
				
					$inTime = "In Time Not Available";
				}
				
				if($auAttSt[$i]->attendance_end!=''){
					$outTime = $auAttSt[$i]->attendance_end;
				}
				else {
				
					$outTime = "Out Time Not Available";
				}
				
				
				$data.="".$auAttSt[$i]->salesman_name."\t".$this->_changeDate($auAttSt[$i]->activity_date)."\t".date("l",strtotime($auAttSt[$i]->activity_date))."\t".$inTime."\t".$outTime."\t".$work_hours."\n";
				
				
		}
	}
	else { 
	$data.="Report Not Available \n";
	
	}
		return $data;	
	}
	
	public function salesman_gap() { }
		
	public function salesman_total_sale() { 	
	

						
						
	
	}
	
	public function sale_montly_basis_sale() { 
	
	
	$data=" Salesman\t Retailer\t Item\t Item Code\t Month\t Year\t Day1\t  Day2\t Day3\t Day4\t Day5\t Day6\t Day7\t Day8\t Day9\t Day10\t Day11\t Day12\t Day13\t Day14\t Day15\t Day16\t Day17\t Day18\t Day19\t Day20\t Day21\t Day22\t Day23\t Day24\t Day25\t Day26\t Day27\t Day28\t Day29\t Day30\t Day31\n";
	
	//echo $_SESSION['distributorIDSale'].$_SESSION['itemID'].$_SESSION['dismonth'].$_SESSION['disCyear'];
	

		
		if($this->from_date!=''){
			$fromdate=" and O.date_of_order >= '".$this->from_date."'";
		} else {
			$fromdate = "and O.date_of_order >= '".date('Y-m-d')."'";
		}
		
		if($this->to_date!=''){
			$todate=" and O.date_of_order <= '".$this->to_date."'";
		} else {
			$todate="and O.date_of_order <= '".date('Y-m-d')."'";
		}
		
		

		//$sortname = 'month';
		//$sortorder = 'ASC';
		$sort = " ORDER BY month ASC";
		
		$where = " O.account_id='".$this->account_id."' ".$this->salesman." $fromdate $todate";
		$groupby = ' GROUP BY R.retailer_id, S.salesman_id, item_id, month';
		
		$auRec=$this->_getSelectList2('salereldailywiseitemreport AS O 
					LEFT JOIN table_retailer AS R ON O.retailer_id = R.retailer_id 
					LEFT JOIN table_salesman AS S ON O.salesman_id = S.salesman_id
					LEFT JOIN table_item AS I ON O.item_id = I.item_id',		
			"O.retailer_id AS RID,
			O.salesman_id AS SID,
			 DATE_FORMAT(O.date_of_order, '%b') AS DOODR,
			 O.Day AS Day,
			 O.item_name AS item_name,
			 I.item_code AS item_code,
			 S.salesman_name AS s_name,
			 O.item_id AS item_id,
			 O.month AS month,
			 O.year AS year,
			 R.retailer_name AS retailer_name,
			 R.retailer_address,
			 R.retailer_address2",'',$where.$groupby.$sort,'');
			// print_r($auRec);
			 for($i=0;$i<count($auRec);$i++){
		
		if(isset($auRec[$i]->item_id)){
	$auRec2=$this->_getSelectList2('salereldailywiseitemreport AS O',"totalSaleUnit,Day,retailer_id,salesman_id,item_name",''," O.account_id='".$this->account_id."' AND retailer_id = ".$auRec[$i]->RID." AND salesman_id = ".$auRec[$i]->SID." AND item_id = ".$auRec[$i]->item_id." AND month = ".$auRec[$i]->month."");

			$row1  = '-';
			$row2  = '-';
			$row3  = '-';
			$row4  = '-';
			$row5  = '-';
			$row6  = '-';
			$row7  = '-';
			$row8  = '-';
			$row9  = '-';
			$row10 = '-';
			$row11 = '-';
			$row12 = '-';
			$row13 = '-';
			$row14 = '-';
			$row15 = '-';
			$row16 = '-';
			$row17 = '-';
			$row18 = '-';
			$row19 = '-';
			$row20 = '-';
			$row21 = '-';
			$row22 = '-';
			$row23 = '-';
			$row24 = '-';
			$row25 = '-';
			$row26 = '-';
			$row27 = '-';
			$row28 = '-';
			$row29 = '-';
			$row30 = '-';
			$row31 = '-';

			for($j=0;$j<count($auRec2);$j++){
			
				${'row'.$auRec2[$j]->Day} = $auRec2[$j]->totalSaleUnit;
			
			}
			
		$data.="".$auRec[$i]->s_name."\t".$auRec[$i]->retailer_name."\t".$auRec[$i]->item_name."\t".$auRec[$i]->item_code."\t".$auRec[$i]->DOODR."\t".$auRec[$i]->year."\t".$row1."\t".$row2."\t".$row3."\t".$row4."\t".$row5."\t".$row6."\t".$row7."\t".$row8."\t".$row9."\t".$row10."\t".$row11."\t".$row12."\t".$row13."\t".$row14."\t".$row15."\t".$row16."\t".$row17."\t".$row18."\t".$row19."\t".$row20."\t".$row21."\t".$row22."\t".$row23."\t".$row24."\t".$row25."\t".$row26."\t".$row27."\t".$row28."\t".$row29."\t".$row30."\t".$row31."\n";
		}
		}
	return $data;
	
	
	
	}	
	
	// New Retailer Report
	
	public function new_retailer_report() { 
	
		$data=" Retailer\t Added Date\t State\t City\t Market\t Address\t Phone Number\t Contact Person\t Contact Number\n ";
			$auRet=$this->_getSelectList2('table_retailer as S left join state as st on st.state_id=S.state left join city as c on c.city_id=S.city',"S.retailer_id, S.retailer_name, S.start_date, S.retailer_location, S.retailer_address, S.retailer_phone_no, S.contact_person, S.contact_number, st.state_name, c.city_name",''," S.account_id =".$this->account_id." AND (S.start_date BETWEEN '".$this->from_date."' AND '".$this->to_date."') ".$this->salesman." ORDER BY S.start_date desc, c.city_name asc");
			if(is_array($auRet)){
				for($i=0;$i<count($auRet);$i++)
				{
				$data.="".$auRet[$i]->retailer_name."\t".$this->_changeDate($auRet[$i]->start_date)."\t".$auRet[$i]->state_name."\t". $auRet[$i]->city_name."\t".$auRet[$i]->retailer_location."\t".$auRet[$i]->retailer_address."\t".$auRet[$i]->retailer_phone_no."\t".$auRet[$i]->contact_person."\t".$auRet[$i]->contact_number."\n";
				}
				}
				else{
				
				$data.="Report Not Available \n";
				
				
				}
	return $data;
	
	}	
	
	// End Of New Retailer Report
	
	
	// Retailer Wise Report	
	
		public function retailer_report(){
				
			$data=" Retailer\t Market\t Total\t Pro/Tot\t % Share\t ";		
			
			if(isset($this->account_id) && $this->account_id!="")
			$where = " O.account_id =".$this->account_id.$this->salesman.$this->fromdate.$this->todate." AND r.new=''";
			
		else 
			throw new Exception('No account id found. <br>');
			
			$sortByOrder=" GROUP BY O.retailer_id ORDER BY total desc LIMIT 1 ";
			
		$auRet=$this->_getSelectList2('table_order as O left join table_retailer as r on O.retailer_id=r.retailer_id 
			LEFT JOIN table_salesman AS S ON S.salesman_id = O.salesman_id',"COUNT(O.retailer_id) as total",'',$where.$sortByOrder);
			
			if(is_array($auRet)){
				$row_total=$auRet[0]->total;
				 for($a=0;$a<$auRet[0]->total;$a++){
				 	$sNo= $a+1;
					$nDate="Date".$sNo;
					$nOValue="Order Value".$sNo;
				 	$data.=" $nDate\t Salesman\t $nOValue\t";
				 }
				$data.="\n";
			$sortByOrderList=" GROUP BY O.retailer_id  Order by total_amt desc";		
			$orderList=$this->_getSelectList2('table_order as O left join table_retailer as r on O.retailer_id=r.retailer_id LEFT JOIN table_salesman AS s ON s.salesman_id = O.salesman_id',"sum(O.acc_total_invoice_amount) as total_amt,O.order_id,O.retailer_id,r.retailer_name,r.retailer_location",'',$where.$sortByOrderList);
			if(is_array($orderList)){
				for($i=0;$i<count($orderList);$i++)
				{ 
				
				$orderTotal=$this->_getSelectList2('table_order as O left join table_retailer as r on O.retailer_id=r.retailer_id LEFT JOIN table_salesman AS s ON s.salesman_id = O.salesman_id',"count(O.retailer_id) as total_cal",'',	$where." AND O.retailer_id='".$orderList[$i]->retailer_id."'");
				
				$orderTotalPro=$this->_getSelectList('table_order as O left join table_retailer as r on O.retailer_id=r.retailer_id LEFT JOIN table_salesman AS s ON s.salesman_id = O.salesman_id',"count(O.retailer_id) as total_cal_pro",'',$where." AND O.retailer_id='".$orderList[$i]->retailer_id."' AND order_type!='No' ");
				
				$Netorder=$this->_getSelectList('table_order as O left join table_retailer as r on O.retailer_id=r.retailer_id LEFT JOIN table_salesman AS s ON s.salesman_id = O.salesman_id',"sum(O.acc_total_invoice_amount) as net_amt",'',$where);
				
				$total_pro=$orderTotalPro[0]->total_cal_pro."/".$orderTotal[0]->total_cal;
				$data.="".$orderList[$i]->retailer_name."\t".$orderList[$i]->retailer_location."\t".$orderList[$i]->total_amt."\t".$total_pro."\t".round($orderList[$i]->total_amt/$Netorder[0]->net_amt*100,2) ."%"."\t";
				
				$orderDet=$this->_getSelectList('table_order as O left join table_retailer as r on O.retailer_id=r.retailer_id LEFT JOIN table_salesman AS s ON s.salesman_id = O.salesman_id',"O.order_id,O.date_of_order,O.acc_total_invoice_amount,O.order_type,s.salesman_name",'',$where." AND O.retailer_id='".$orderList[$i]->retailer_id."' Order by O.date_of_order asc ");								
			
			foreach($orderDet as $val){
											
				$date_order=$this->_changeDate($val->date_of_order);
				$name=$val->salesman_name;
				 if($val->order_type!='No'){
						$orderDet =	$val->acc_total_invoice_amount;
					} 
				else {
						$orderDet =	$val->acc_total_invoice_amount;
		 		 	 }			
					$data.="".$date_order."\t".$name."\t".$orderDet."\t";								
								
					}				
			
			for($c=0;$c<$row_total-count($orderDet);$c++){
				$data.=""."-"."\t"."-"."\t"."-"."\t";
			}		
				$data.=" \n";	
			}
			
			}
		}
			
		else {
		
			$data.="Report Not Available \n";
		}
	
		return $data;
		}
	
	
	// End Of Retailer Report
	
	
	// Retailer Opening Stock Report
	public function retopeningstockbydate (){
	
		$data=" Salesman\t Retailer\t Category\t Item\t Stock Value\t Date\t Time\n";	
		$sort=" ORDER BY O.date_of_order desc,O.time_of_order desc";
  		$where = "  O.account_id =".$this->account_id."  AND  O.ostype='R'".$this->retailer.$this->fromdate.$this->todate; 
  
  		$auRec=$this->_getSelectList2('table_order_os AS O 
			LEFT JOIN table_order_detail_os AS tod ON tod.os_id=O.os_id 
			LEFT JOIN table_retailer AS R ON R.retailer_id=O.retailer_id 
			LEFT JOIN table_salesman AS S ON S.salesman_id=O.salesman_id 
			LEFT JOIN table_item AS I ON I.item_id=tod.item_id 
			LEFT JOIN table_category AS C ON C.category_id=I.category_id',
			"O.*,tod.*,
			R.retailer_name,
			S.salesman_name,
			I.item_name,
			C.category_name",'',$where.$sort,'');
	
	if(is_array($auRec)){
	
		 for($i=0;$i<count($auRec);$i++){
		 
			$data.="".$auRec[$i]->salesman_name."\t".$auRec[$i]->retailer_name."\t".$auRec[$i]->category_name."\t".$auRec[$i]->item_name."\t".$auRec[$i]->quantity."\t".$this->_changeDate($auRec[$i]->date_of_order)."\t".$auRec[$i]->time_of_order."\n";
			}
		}
		
		return $data;
		
	}

	// End Of Retailer Opening Stock Report

 	// Product Wise Report
	public function productwise_report() {		
	   
	   if($this->category!=''){$catList=$this->category;}
	   
	   
	   if($this->items!=''){$itemList=$this->items;}
	   
	   //echo $this->fromdate."<br>".$this->todate."<br>";
	   
	   	$data=" Category\t Item\t Item Code\t Number Of Order\t Quantity\t Price\t Total\n";		
		$sort = " ORDER BY date_of_order desc,time_of_order desc";
		$groupby  = " GROUP BY od.item_id having od.item_id!=''";
		$where = " od.type=1 and O.account_id='".$_SESSION['accountId']."'".$catList.$itemList.$this->fromdate.$this->todate;		

		
		$auRec=$this->_getSelectList2('table_order AS O
					LEFT JOIN table_order_detail AS od ON od.order_id = O.order_id
					LEFT JOIN table_item AS I ON I.item_id = od.item_id
					LEFT JOIN table_category AS C ON C.category_id = I.category_id 
					left join table_price as tp on tp.item_id= od.item_id',
					"COUNT(od.item_id) AS total, sum(od.quantity) AS Quantity, SUM(od.quantity * tp.item_mrp) AS ttlprice, category_name, od.item_id, I.item_name, I.item_code, od.price,O.date_of_order",
					'',$where.$groupby.$sort,'');
	
	 for($i=0;$i<count($auRec);$i++){
	 
		$data.="".$auRec[$i]->category_name."\t".$auRec[$i]->item_name."\t".$auRec[$i]->item_code."\t".$auRec[$i]->total."\t".$auRec[$i]->Quantity."\t".$auRec[$i]->price."\t".$auRec[$i]->ttlprice."\n";
		} 
	   
	   
	   return $data;
	   //exit;
	}
	
	// End of Product Wise Report
	
	
	
	
	// Distributor Wise Report
	public function distributor_report() {
				$disListID= $this->distributor;	
				$DistName=$this->_getSelectList('table_distributors AS D','distributor_name',''," $disListID ");
				$recCount=count($DistName);
				//$disListName=$DistName[0]->distributor_name;			
				
				if($recCount==1){
					$list =  $disListID;
					$dist =  $DistName[0]->distributor_name;
		
				}
				else{
				$list="";
				$dist="All Distributors";
		
				}
				
		$data="Distributor: ".$dist."\t From Date: ".$this->from_date."\t To Date: ".$this->to_date."\n";
		
		if(isset($this->account_id) && $this->account_id!="")
			$where = " ".$this->fromdate.$this->todate.$this->salesman." $list  AND O.order_type!='No'";
			
		else 
			throw new Exception('No account id found. <br>');
			$order_by=" GROUP BY O.distributor_id ORDER BY total desc LIMIT 1";
			$auRet=$this->_getSelectList('table_order AS O left join table_distributors AS D on O.distributor_id=D.distributor_id left join table_salesman as S on O.salesman_id=S.salesman_id',"COUNT(O.distributor_id) as total",'',$where,'');
			
			if(is_array($auRet)){
		$data1="Distributor Name\t Total\t % Share\t";
		$row_total=$auRet[0]->total;
			for($a=0;$a<$auRet[0]->total;$a++){	 
					$no=$a+1;
					$data1.="Date ".$no."\t Salesman\t Order Value ".$no."\t";
			}
			$data1.=" \n";
			$data.=$data1;
			
			$order_by_1=" GROUP BY O.distributor_id  Order by total_amt desc";
			
			
			$orderList=$this->_getSelectList('table_order AS O left join table_distributors AS D on O.distributor_id=D.distributor_id left join table_salesman AS S on O.salesman_id=S.salesman_id',"sum(O.acc_total_invoice_amount) as total_amt,O.order_id,O.distributor_id,D.distributor_name",'',$where.$order_by_1,'');
		if(is_array($orderList)){
			for($i=0;$i<count($orderList);$i++)
			{
			
			$data2=$orderList[$i]->distributor_name."\t ";
			$data2.=$orderList[$i]->total_amt."\t ";
			$orderTotal=$this->_getSelectList('table_order AS O left join table_distributors AS D on O.distributor_id=D.distributor_id',"count(O.distributor_id) as total_cal",''," O.distributor_id='".$orderList[$i]->distributor_id."' ".$this->fromdate.$this->todate." AND O.order_type!='No'",'');
			
		
			
			$Netorder=$this->_getSelectList('table_order AS O left join table_distributors AS D on O.distributor_id=D.distributor_id left join table_salesman AS S on O.salesman_id=S.salesman_id',"sum(O.acc_total_invoice_amount) as net_amt",'',$where,'');
			$data2.=round($orderList[$i]->total_amt/$Netorder[0]->net_amt*100,2) ."% \t ";
			
			//$order_by_2=" ";
			
			$orderDet=$this->_getSelectList('table_order AS O left join table_distributors AS D on O.distributor_id=D.distributor_id left join table_salesman as S on O.salesman_id=S.salesman_id',"O.order_id,O.date_of_order,O.acc_total_invoice_amount,O.order_type,S.salesman_name",''," O.distributor_id='".$orderList[$i]->distributor_id."' ".$this->salesman.$this->fromdate.$this->todate." AND O.order_type!='No' Order by O.date_of_order asc");
				
				//echo count($orderDet)."Det<br>";
				
				for($b=0;$b<count($orderDet);$b++)
				{
				$data2.="".$this->_changeDate($orderDet[$b]->date_of_order)."\t ".$orderDet[$b]->salesman_name."\t ";
				//$data2.=;
					if($orderDet[$b]->order_type!='No'){
					$data2.=$orderDet[$b]->acc_total_invoice_amount."\t ";
					} else {
					$data2.=$orderDet[$b]->acc_total_invoice_amount."\t ";
					}
				}  
					//echo $row_total - count($orderDet)."minus<br>";
				
				for($c=0;$c<$row_total-count($orderDet);$c++){
					$data2.="-\t ";
					$data2.="-\t ";
					$data2.="-\t ";
					}
					
					
					$orderAmount=$this->_getSelectList('table_order AS O left join table_distributors AS D on O.distributor_id=D.distributor_id',"sum(O.acc_total_invoice_amount) as total_amt",''," O.distributor_id='".$orderList[$i]->distributor_id."' ".$this->fromdate.$this->todate." AND O.order_type!='No' ");
					$data2.="\n"; 					
				    $data.=$data2;
					
			}
		}
		
	
	} else {
	$data.="Report Not Available \n";
	}
				
				
				
				return $data;
		}
	
	// End of distributor Wise Report
	

 	// Distributor Monthly Sale
	public function dis_montly_basis_sale() {	
	
	
	$data=" Retailer\t Distributor\t Item\t Item Code\t Month\t Year\t Day1\t  Day2\t Day3\t Day4\t Day5\t Day6\t Day7\t Day8\t Day9\t Day10\t Day11\t Day12\t Day13\t Day14\t Day15\t Day16\t Day17\t Day18\t Day19\t Day20\t Day21\t Day22\t Day23\t Day24\t Day25\t Day26\t Day27\t Day28\t Day29\t Day30\t Day31\n";
	
	//echo $_SESSION['distributorIDSale'].$_SESSION['itemID'].$_SESSION['dismonth'].$_SESSION['disCyear'];
	
	/*if($_SESSION['distributorIDSale']!=''){
			$distributor=" and D.distributor_id ='".$_SESSION['distributorIDSale']."'";
		}
		
		if($_SESSION['itemID']!=''){
			$Item=" and O.item_id ='".$_SESSION['itemID']."'";
		}*/
		
		if($this->from_date!=''){
			$fromdate=" and O.date_of_order >= '".$this->from_date."'";
		} else {
			$fromdate = "and O.date_of_order >= '".date('Y-m-d')."'";
		}
		
		if($this->to_date!=''){
			$todate=" and O.date_of_order <= '".$this->to_date."'";
		} else {
			$todate=" and O.date_of_order <= '".date('Y-m-d')."'";
		}
		
		$sortname = 'month';
		$sortorder = 'ASC';
		$sort = " ORDER BY $sortname $sortorder";
		
		$where = "  O.account_id='".$this->account_id."' ".$this->distributor." $fromdate $todate";
		$groupby = " GROUP BY R.retailer_id , item_id, month";
		
		$auRec=$this->_getSelectList2('disretdailywiseitemreport AS O
					LEFT JOIN table_retailer AS R ON O.retailer_id = R.retailer_id 
					LEFT JOIN table_distributors AS D ON O.distributor_id = D.distributor_id	
					LEFT JOIN table_item AS i ON O.item_id = i.item_id',	
			"O.retailer_id AS RID,
			 DATE_FORMAT(O.date_of_order, '%b') AS DOODR,
			 O.Day AS Day,
			 O.item_name AS item_name,
			 i.item_code AS item_code,
			 O.item_id AS item_id,
			 O.month AS month,
			 O.year AS year,
			 R.retailer_name AS retailer_name,
			 D.distributor_name As distributor_name,
			 R.retailer_address,
			 R.retailer_address2",'',$where.$groupby.$sort,'');
			// print_r($auRec);
			 for($i=0;$i<count($auRec);$i++){

	$auRec2=$this->_getSelectList('disretdailywiseitemreport AS O',"totalSaleUnit,Day,retailer_id,item_name",''," retailer_id = ".$auRec[$i]->RID." AND item_id = ".$auRec[$i]->item_id." AND month = ".$auRec[$i]->month."");

			$row1  = '-';
			$row2  = '-';
			$row3  = '-';
			$row4  = '-';
			$row5  = '-';
			$row6  = '-';
			$row7  = '-';
			$row8  = '-';
			$row9  = '-';
			$row10 = '-';
			$row11 = '-';
			$row12 = '-';
			$row13 = '-';
			$row14 = '-';
			$row15 = '-';
			$row16 = '-';
			$row17 = '-';
			$row18 = '-';
			$row19 = '-';
			$row20 = '-';
			$row21 = '-';
			$row22 = '-';
			$row23 = '-';
			$row24 = '-';
			$row25 = '-';
			$row26 = '-';
			$row27 = '-';
			$row28 = '-';
			$row29 = '-';
			$row30 = '-';
			$row31 = '-';

			for($j=0;$j<count($auRec2);$j++){
			
				${'row'.$auRec2[$j]->Day} = $auRec2[$j]->totalSaleUnit;

			}
			
		$data.="".$auRec[$i]->retailer_name."\t".$auRec[$i]->distributor_name."\t".$auRec[$i]->item_name."\t".$auRec[$i]->item_code."\t".$auRec[$i]->DOODR."\t".$auRec[$i]->year."\t".$row1."\t".$row2."\t".$row3."\t".$row4."\t".$row5."\t".$row6."\t".$row7."\t".$row8."\t".$row9."\t".$row10."\t".$row11."\t".$row12."\t".$row13."\t".$row14."\t".$row15."\t".$row16."\t".$row17."\t".$row18."\t".$row19."\t".$row20."\t".$row21."\t".$row22."\t".$row23."\t".$row24."\t".$row25."\t".$row26."\t".$row27."\t".$row28."\t".$row29."\t".$row30."\t".$row31."\n";
		}
	return $data;
	
	}
	
	// End of distributor Monthly Sale
	
	// Item Wise Report
	
	public function items_report(){
	
		$Total_Items=$this->_getSelectList2('table_order_detail as d left join table_order as o on o.order_id=d.order_id left join table_item as i on i.item_id=d.item_id left join table_retailer as r on o.retailer_id=r.retailer_id left join table_salesman as s on s.salesman_id=o.salesman_id left join table_category as c on c.category_id=i.category_id',"SUM(d.acc_total) as net_total",'',"  r.new='' and d.type=1 AND (o.date_of_order BETWEEN '".$this->from_date."' AND '".$this->to_date."') AND o.order_type!='No' ".$this->salesman." ");
			$net_amoumt=$Total_Items[0]->net_total;
			
			$auItem=$this->_getSelectList('table_item as i left join table_order_detail as d on i.item_id=d.item_id left join table_order as o on o.order_id=d.order_id left join table_retailer as r on o.retailer_id=r.retailer_id left join table_salesman as s on s.salesman_id=o.salesman_id left join table_category as c on c.category_id=i.category_id',"SUM(d.acc_quantity) as total_qty,SUM(d.acc_total) as total_amt,COUNT(Distinct o.order_id) as total_order,COUNT(Distinct o.retailer_id) As total_ret, i.item_code,i.item_name ,d.item_id, c.category_name",'',"  r.new='' and d.type=1 AND (o.date_of_order BETWEEN '".$this->from_date."' AND '".$this->to_date."') AND o.order_type!='No' ".$this->salesman."GROUP BY i.item_id ORDER BY total_amt DESC ");
			$item_id_2=array();
			$tot_qty=array();
			$tot_amt=array();
			if(is_array($auItem)){
			
			$data="Category\t Items\t % Share\t Total Amount\t Total Quantity\t Sold to (No of Retailer)\t In. (No of Order)\n";
			
			
			for($i=0;$i<count($auItem);$i++)	{ 
			$item_id_2[]= $auItem[$i]->item_id;
			$tot_qty[]=$auItem[$i]->total_qty;
			$tot_amt[]=$auItem[$i]->total_amt;
			if($i > 9)
			{
				$other = $other + $auItem[$i]->total_amt;
			}
			else
			{
				${'d'.$i} = $auItem[$i]->total_amt;
				${'ditem'.$i} = $auItem[$i]->item_code;
			}
				$data.="". $auItem[$i]->category_name."\t".$auItem[$i]->item_name."(".$auItem_not[$i]->item_code.")"."\t". round($auItem[$i]->total_amt/$net_amoumt*100,2) ."%"."\t".$auItem[$i]->total_amt."\t".$auItem[$i]->total_qty."\t".$auItem[$i]->total_ret."\t".$auItem[$i]->total_order." Order"."\n";
			
			}
		$Item_not_exit = array_diff($item_id_1, $item_id_2);
		foreach($Item_not_exit as $a=>$a_value)
		  {
		  $item_id_not.=$coma.$a_value;
		  $coma=",";
		  }
		unset($coma);
		//echo $item_id_not;
		//print_r($Item_not_exit);
		if($item_id_not!=''){
		$auItem_not=$this->_getSelectList2('table_item as i left join table_category as c on c.category_id=i.category_id',"i.item_code,i.item_name, c.category_name",''," i.account_id='".$this->account_id."' and i.item_id in ($item_id_not) ");
			if(is_array($auItem_not)){
				for($i=0;$i<count($auItem_not);$i++)	{ 
				
				$data.="". $auItem[$i]->category_name."\t".$auItem[$i]->item_name."(".$auItem_not[$i]->item_code.")"."\t". "-"."\t"."-"."\t"."-"."\t"."-"."\t"."-"."\n";
				
				}
			}
		}
		$data.="".""."\t".""."\t"."Total"."\t".array_sum($tot_amt)."\t".array_sum($tot_qty)."\t".""."\t".""."\n";
		}
		else { 
		
			$data.="Report Not Available \n";
		  }
			
	return $data;
	}
	
// End Of Item Wise Report
	
	
	public function transaction_list(){
	
	
		$data="Salesman\t Retailer\t Transaction Type\t Bank\t Amount\t Date\t Time\n";
		$sort = " ORDER BY O.transaction_date desc,O.transaction_time desc";
		
		
		
		if($this->from_date!=''){
			$fromdate=" and O.transaction_date >= '".date('Y-m-d', strtotime($this->from_date))."'";
		}
		else
		{
			$fromdate=" and O.transaction_date >= '".date('Y-m-d')."'";
		}
		if($this->to_date!=''){
			$todate=" and O.transaction_date <= '".date('Y-m-d', strtotime($this->to_date))."'";
		}
		else
		{
			$todate=" and O.transaction_date <= '".date('Y-m-d')."'";
		}
		
		
		
		if(isset($this->account_id) && $this->account_id!="")
			$where = " O.account_id =".$this->account_id.$this->salesman.$fromdate.$todate."";
			
		else 
			throw new Exception('No account id found. <br>');
		
		//$where = " $salesman $retailer $fromdate $todate";
		
			$auRec=$this->_getSelectList2('table_transaction_details as O left join table_retailer as R on O.retailer_id=R.retailer_id left join table_salesman as S on O.salesman_id=S.salesman_id',"O.*,S.salesman_name,R.retailer_name,R.retailer_location,R.retailer_address",'',$where.$sort,'');	
			
			if(is_array($auRec)){
			
			for($i=0;$i<count($auRec);$i++){
				
				if($auRec[$i]->transaction_type==1){$type="Cash";}else{$type="Cheque";}
	
$data.="".$auRec[$i]->salesman_name."\t".$auRec[$i]->retailer_name."\t".$type."\t".$auRec[$i]->issuing_bank."\t".round($auRec[$i]->total_sale_amount)."\t".$this->_changeDate($auRec[$i]->transaction_date)."\t".$auRec[$i]->transaction_time."\n";
		}
	}
	else{
	
		$data.="Report Not Available \n";
	
	}
	
	return $data;
	
	}
	
	
	public function daily_sales_report(){
	
	//$date=date('Y-m-d', strtotime($_POST['from']));
	//$day=date('D', strtotime($date));
	
	$where=" O.account_id='".$this->account_id."' ".$this->fromdate.$this->todate.$this->salesman." AND (retailer_id IS NOT NULL OR retailer_id<>'') group by retailer_id";
	
	
	//$where = "date_of_order='".$date."' AND salesman_id='".$_POST['sname']."' AND (retailer_id IS NOT NULL OR retailer_id<>'') group by retailer_id"; 
	$auRetailer = $this->_getSelectList('salereldailywiseitemreport as O left join table_salesman as S on S.salesman_id=O.salesman_id','O.*','',$where); 
	$salesman_name=$auRetailer[0]->salesman_name;
	$salesman_id=$auRetailer[0]->salesman_id;
	//print_r($auRetailer);
	$orderDate=$auRetailer[0]->date_of_order;
	$day=date('D', strtotime($orderDate));
	// for route name.
	$cond="salesman_id='".$salesman_id."' and '$orderDate'>from_date and '$orderDate'< to_date ";
	
	// for route name.
	//$cond="".$this->fromdate.$this->todate.$this->salesman."";
    $tblroute = $this->_getSelectList('table_route_schedule','*','',"$cond"); 
	//print_r($day);
	
	foreach($tblroute as $value){
   		
       $cond="route_schedule_id='".$value->route_schedule_id."' and $day!='' ";
        $tblrouteassign = $this->_getSelectList('table_route_schedule_by_day',"".$day."",'',"$cond");
		
		//print_r($tblrouteassign);		
		$routeID = $this->returnObjectArrayToIndexArray($tblrouteassign, $day);
		//print_r($routeID);
	    $routeIDwithcomma = implode(',', $routeID);
		//print_r($routeIDwithcomma);
		$retailerID = $this->returnObjectArrayToIndexArray($auRetailer, 'retailer_id');
		$retailerIDwithcomma = implode(',', $retailerID);
        
		if($retailerIDwithcomma==""){$retailerIDwithcomma=0;} else{$retailerIDwithcomma;}		
		if($routeIDwithcomma==""){$routeIDwithcomma=0;} else{ $routeIDwithcomma; }
		
		$rtID = $this->_getSelectList('table_route_retailer',"DISTINCT(route_id)",''," route_id IN (".$routeIDwithcomma.") AND retailer_id IN (".$retailerIDwithcomma.")");
		
		$rtfID = $this->returnObjectArrayToIndexArray($rtID, 'route_id');
		//print_r($rtfID);
		$routeIDfinal = implode(',',$rtfID);
		//print_r($routeIDfinal);
		if($routeIDfinal==""){$routeIDfinal=0;}else {$routeIDfinal;}
		//print_r($routeIDfinal);
		$tblroutefinal = $this->_getSelectList('table_route','*','',"route_id IN(".$routeIDfinal.")");
		//print_r($tblroutefinal);
		}
		
		
		$data = " Route Name\t";
		$j=0; 
		for($i=0;$i<count($tblroutefinal);$i++)
		{ 
		//print_r($rtID);exit;
		  
		  if($tblroutefinal[$i]->route_name!=""){ 
		  if($j>0){ echo ','.'&nbsp;' ;} 
		  
		  $data.="".$tblroutefinal[$i]->route_name."\n";
		
		 
		  $j++; 
		  }
		 
	    }
		
		
		$data.=" S.N.\t Retailer\t";
		
		if(isset($salesman_id)){
				 	 $ItemArray2 = $this->_getSelectList('salereldailywiseitemreport  
 as tod left join table_item as ti on tod.item_id=ti.item_id','DISTINCT(ti.item_code),ti.item_name,ti.item_id,tod.price',''," tod.date_of_order ='".$orderDate."' AND (tod.retailer_id!='' OR tod.retailer_id IS NOT NULL) AND tod.salesman_id =".$salesman_id." ORDER BY ti.item_id ASC");
 				//echo "<pre>";
 				//echo count($ItemArray2);
		           		if(is_array($ItemArray2))
						{
				           foreach($ItemArray2 as $itm){ 
						   		$itemName=$itm->item_name;
				 				$data.=" $itemName\t";
				 			 }
						}
					
					//print_r($ItemArray2);
					$final = $this->returnObjectArrayToIndexArray($ItemArray2, 'item_id');
					//print_r($final);
				    $itemID = implode(',', $final);
					}
					$data.=" Value\n";
					if(count($auRetailer)>0){
					$counter = 0; 
			      $MultiDimArr = array();
				  
				  foreach($auRetailer as $key=>$values){ 
				  
				  	$countKey=$key+1;
				  
				  $data.="".$countKey."\t".$values->retailer_name."\t";
				  
				   $ItemArray = $this->_getSelectList2('table_item AS ti LEFT OUTER JOIN salereldailywiseitemreport as tod on tod.item_id=ti.item_id','tod.item_id, tod.totalSaleUnit,ti.item_code,tod.price','',"tod.retailer_id =".$values->retailer_id." AND tod.date_of_order ='".$orderDate."' AND (tod.retailer_id!='' OR tod.retailer_id IS NOT NULL) AND ti.item_id IN (".$itemID.") AND  tod.salesman_id =".$salesman_id);   
		  //echo "<pre>";
		 // print_r( $ItemArray);           
						
		           		if(is_array($ItemArray))
						{
						  //echo count($ItemArray);
						  $unitsale=0;
						  $ValArr = array();
						  $ValArr2=array();
				           foreach($ItemArray as $key=>$itm){
						  $ValArr[$itm->item_id] = $itm->totalSaleUnit;
						  $ValArr2[$itm->item_id] = $itm->price;
						  
						 }
						//print_r($ValArr);
						$count = array();
						$rowtotal_price=0;
						for($i=0;$i<count($final);$i++){
						$count[$i] = 1;
						
						if($ValArr[$final[$i]]!='') {
										 //echo $ValArr2[$final[$i]].'('.$final[$i].')';
										//echo '<b>'.$ValArr[$final[$i]].'</b>';
										$data.="".$ValArr[$final[$i]]."\t";
									$MultiDimArr[$final[$i]]['qty'] = $MultiDimArr[$final[$i]]['qty'] + $ValArr[$final[$i]];
									$MultiDimArr[$final[$i]]['count'] = $MultiDimArr[$final[$i]]['count'] + $count[$i];
										$counter++;
									} else {
											$valArrItem="-";
										$data.="".$valArrItem."\t";
									}
									
										$rowtotal_price=$rowtotal_price+($ValArr[$final[$i]]*$ValArr2[$final[$i]]);	
										
										}
										
										$data.="".$rowtotal_price."\n";
										
										 $finalval=$finalval+$rowtotal_price;
										 } 
										 
										 } 
										 
										 $rdTorotal=" \t RD TOTAL"."/"." LINE TOTAL";
										 
										 $data.=" ". $rdTorotal."\t";
										 
										 
										  $tls=0;
						for($i=0;$i<count($final);$i++){
						
						 $rdTorotalValue=$MultiDimArr[$final[$i]]['qty']."/".$MultiDimArr[$final[$i]]['count'];
						$data.="". $rdTorotalValue."\t";
						$tls=$tls+$MultiDimArr[$final[$i]]['count'];
					}
					$data.="". $finalval."\n";
					$data.=" \t PC-". count($auRetailer)."\n";
					$data.=" \t TLS-". $tls."\n";
					$data.=" \t TOT VAL-".$finalval."\n";
					
					} else {
					
					$data.="Report Not Available \n";
					
					}
	
	return $data;
	
	}
	
	// Scheme Wise Report
	
	public function scheme_report(){
	
		$from_date=$this->from_date;
		$to_date=$this->to_date;
		$sal_query=$this->salesman;
	
		$rec="SELECT Count(distinct total.order_id) as total_order,sum(total.qty) as total_qty,sum(total.amt) as total_amt, Count(distinct total.retailer_id) as total_ret, total.dis, d.discount_desc, d.discount_type, d.discount_amount, f.free_qty from (SELECT S.order_id, S.acc_discount_id as dis,S.acc_free_item_qty as qty, S.acc_discount_amount as amt, S.salesman_id, S.retailer_id FROM table_order as S left join table_retailer as r on S.retailer_id=r.retailer_id WHERE S.account_id='".$this->account_id."' and S.order_type != 'No' and r.new='' and S.acc_discount_id!='' and (S.date_of_order BETWEEN '".$from_date."' AND '".$to_date."') $sal_query Group by S.order_id union all SELECT S.order_id, d.acc_discount_id as dis,d.acc_free_item_qty as qty, d.acc_discount_amount as amt, S.salesman_id, S.retailer_id FROM table_order as S inner join table_order_detail as d on S.order_id=d.order_id left join table_retailer as r on S.retailer_id=r.retailer_id WHERE S.account_id='".$this->account_id."' and S.order_type != 'No' and r.new='' and d.acc_discount_id!='' and (S.date_of_order BETWEEN '".$from_date."' AND '".$to_date."') $sal_query union all SELECT S.order_id, c.acc_discount_id as dis, c.acc_free_item_qty as qty, c.acc_discount_amount as amt, S.salesman_id, S.retailer_id FROM table_order as S inner join table_order_combo_detail as c on c.order_id=S.order_id left join table_retailer as r on S.retailer_id=r.retailer_id WHERE S.account_id='".$this->account_id."' and c.acc_discount_id!='' and (S.date_of_order BETWEEN '".$from_date."' AND '".$to_date."') and S.order_type != 'No' and r.new='' $sal_query ) as total left join table_discount_detail as d on d.discount_id=total.dis left join table_foc_detail as f on f.foc_id=d.foc_id Group by total.dis ORDER BY total_order desc";
		$this->query_result = mysql_query($rec,$this->db_connect_id ) or die("Error in query ".$rec.mysql_error());	
		$query_id=$this->query_result;
		$num=mysql_num_rows($this->query_result);
			if($num > 0){
			$i=0;
			$total_Retailer=array();
			$total_order=array();
			$total_dis=array();
			
			$data= " SNO.\t Schemes\t Total\t Total Sold to (No of Retailer) \n";
			
			
			while ($this->rowset[$query_id] = @mysql_fetch_array($query_id)){
			$i++;
			
				
			
			if($this->rowset[$query_id]['discount_type']==1){
				$total_discount=$this->rowset[$query_id]['total_order'];
				}
				if($this->rowset[$query_id]['discount_type']==2){
				$total_discount=round($this->rowset[$query_id]['total_amt']/$this->rowset[$query_id]['discount_amount'],1);
				}
				if($this->rowset[$query_id]['discount_type']==3){
				$total_discount= round($this->rowset[$query_id]['total_qty']/$this->rowset[$query_id]['free_qty'],1);
				}
				
				$data.="".$i."\t ".$this->rowset[$query_id]['discount_desc']."\t ".floatval($total_discount)."\t ".$this->rowset[$query_id]['total_ret']."\n";
				
				//echo floatval($total_discount);
				$total_dis[]=$total_discount;
				$total_Retailer[]=$this->rowset[$query_id]['total_ret'];
				
				
				} 
				$data.=" \t Total\t ".array_sum($total_dis)."\t".array_sum($total_Retailer)."\n";
				
				}
				
				else {
				
					$data.="Report Not Available \n";
				}
	
		return $data;
		
		
		
	}
	
	
	// End Scheme Wise Report
	
	

}
?>