<?php

// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
// error_reporting(E_ALL);
// include("../includes/config.inc.php");

/**
 * Description of FCM
 *
 * @author Kaushal
 */
class FCM {

    private $db;

    function __construct() {
        date_default_timezone_set("UTC");
        $this->db = mysqli_connect(MYSQL_HOST_NAME, MYSQL_USER_NAME, MYSQL_PASSWORD, MYSQL_DB_NAME);
        mysqli_set_charset($this->db, 'utf8');
    }

    public function send($notification_type, $booking_id = 0, $user_id = 0 , $booking_detail_id = 0) {
        $user_noti_details = $this->getUserDetails($user_id);
        $device_type = $user_noti_details['login_type'];
        $deviceID = $user_noti_details['gcm_regid'];
        
        if (isset($notification_type) && !empty($notification_type)) {            
            if ($notification_type == 'AL') {
                $notification_type_to_save = 'AL';
            }else if ($notification_type == 'AC') {
                $notification_type_to_save = 'AC';
            }else if ($notification_type == 'RJ') {
                $notification_type_to_save = 'RJ';
            }else if ($notification_type == 'SC') {
                $notification_type_to_save = 'SC';
            }else if ($notification_type == 'CC') {
                $notification_type_to_save = 'CC';
            } else {
                $notification_type_to_save = $notification_type;
            }

            $msg = $this->getNotificationString($notification_type, $user_id);
            
            $notification_id = $this->saveNotification($cleaner_id,$user_id,$booking_id,$notification_type_to_save, $msg['title'], $msg['body']);
            
            #check user notication type and check, is user enabled this type notification??
            if ($notification_type === 'AL' && $user_noti_details['booking_assigned_notification']) {
                if ($device_type == 'A') {
                    return $this->sendForAndroid($msg, $deviceID, $notification_type , $booking_detail_id);
                } elseif ($device_type == 'I') {
                    return $this->sendForIOS($msg, $deviceID, $notification_type , $booking_detail_id);
                }exit;
            }
            else if ($notification_type === 'AC' && $user_noti_details['booking_assigned_notification']) {
                if ($device_type == 'A') {
                    return $this->sendForAndroid($msg, $deviceID, $notification_type, $booking_detail_id);
                } elseif ($device_type == 'I') {
                    return $this->sendForIOS($msg, $deviceID, $notification_type , $booking_detail_id);
                    
                }exit;
            }
            else if ($notification_type === 'RJ' && $user_noti_details['booking_assigned_notification']) {
                if ($device_type == 'A') {
                    return $this->sendForAndroid($msg, $deviceID, $notification_type,$booking_detail_id);
                } elseif ($device_type == 'I') {
                    return $this->sendForIOS($msg, $deviceID, $notification_type , $booking_detail_id);
                }exit;
            }
            else if ($notification_type === 'SC' && $user_noti_details['booking_assigned_notification']) {
                if ($device_type == 'A') {
                    return $this->sendForAndroid($msg, $deviceID, $notification_type,$booking_detail_id);
                } elseif ($device_type == 'I') {
                    return $this->sendForIOS($msg, $deviceID, $notification_type , $booking_detail_id);
                }exit;
            }
            else if ($notification_type === 'CC' && $user_noti_details['booking_assigned_notification']) {
                if ($device_type == 'A') {
                    return $this->sendForAndroid($msg, $deviceID, $notification_type,$booking_detail_id);
                } elseif ($device_type == 'I') {
                    return $this->sendForIOS($msg, $deviceID, $notification_type , $booking_detail_id);
                }exit;
            }
        }
    }

    public function sendContactNotify($notification_type, $booking_id = 0, $user_id = 0) {

        $user_noti_details = $this->getUserDetails($user_id);
        $device_type = $user_noti_details['login_type'];
        $deviceID = $user_noti_details['gcm_regid'];
        
        if (isset($notification_type) && !empty($notification_type)) {            
            if ($notification_type == 'CS') {
                $notification_type_to_save = 'CS';
            }
            $msg = $this->getNotificationString($notification_type, $user_id);
            
            $notification_id = $this->saveNotification($cleaner_id,$user_id,$booking_id,$notification_type_to_save, $msg['title'], $msg['body']);
            
            #check user notication type and check, is user enabled this type notification??
            if ($notification_type === 'CS' && $user_noti_details['general_notifications']) {
                if ($device_type == 'A') {
                    return $this->sendForAndroid($msg, $deviceID, $notification_type);
                } elseif ($device_type == 'IOS') {
                    return $this->sendForIOS($msg, $deviceID, $table_availability_id, $notification_id, $user_id);
                }exit;
            }
        }
    }

    private function sendForAndroid($msg, $deviceID, $notification_type,$booking_detail_id) {
        
       
        $url = 'https://fcm.googleapis.com/fcm/send';
        $fields = array(
            'to' => $deviceID,
            'data' => array(
                "body" => $msg['body'],
                "title" => $msg['title'],
                "booking_id" => $booking_detail_id,
                "type" => $notification_type
            )
        );

        $fields = json_encode($fields);
       
        $headers = array(
            'Authorization: key=' . "AAAAzdjIaL4:APA91bEXNCtRPKLkUVFWUztNthAlzc2HYX8S4AffxNnInx0d-jvNULtrHj7mxskT0rBHubl2ql-anSl8IH2mwjRKuqktsiZlwRthOOa4mHtAF0wcw9MDGYMZyfXC6vF6xz0QBtSriRje",
            'Content-Type: application/json'
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);

        $result = curl_exec($ch);
        // echo "<pre>";
        // print_r($result);
        // exit;
        curl_close($ch);
        $this->writeLog($result); // Write log
        return $result;
    }

    private function sendForIOS($msg, $deviceID, $notification_type , $booking_detail_id = 0) {
        //Development Cert

        //$data = array("title"=> "Dummy Notification", "body"=> "Body of the message", "payload"=>array('from_usr'=>1, 'first_name'=> 'Jyoti', 'last_name'=> 'Sharma','actionType'=>"connect", 'actionTypeCode'=>"1"));

        $pemfilename =__DIR__."/ECleaning.pem"; 
        // Put your private key's passphrase here:
        $passphrase = '';
        // SIMPLE PUSH 
        $body['aps'] = array(
            'alert' => $msg['title'],
            'badge' => 0,
            'sound' => 'ringtone',
            ); // Create the payload body
        // echo "<pre>";print_r($data['payload']);die; 

        $payload['aps'] = array(
            'alert' => array(
                'title' => $msg['title'],
                'body' => $msg['body']
            ),
            'badge' => 0,
            'sound' => 'default'
        );
        $payload['payload'] = array(
                "booking_id" => $booking_detail_id,
                "type" => $notification_type,
                'title' => $msg['title'],
                'body' => $msg['body']
        );
        $body['payload'] = $payload['payload']; // Create the payload body
        // echo "<pre>";
        // print_r($body);
        // exit;
        $data = array("title"=> "Dummy Notification", "body"=> "Body of the message", "payload"=>array('from_usr'=>1, 'first_name'=> 'Jyoti', 'last_name'=> 'Sharma','actionType'=>"connect", 'actionTypeCode'=>"1"));
                            

        ////////////////////////////////////////////////////////////////////////////////
        $ctx = stream_context_create();
        stream_context_set_option($ctx, 'ssl', 'local_cert', $pemfilename);
        stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
        // $fp = stream_socket_client(
        //     'ssl://gateway.sandbox.push.apple.com:2195', $err,
        //     $errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx); // Open a connection to the APNS server

        $fp = stream_socket_client('ssl://gateway.sandbox.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx);
        $payload = json_encode($body); // Encode the payload as JSON
        $msg = chr(0) . pack('n', 32) . pack('H*', $deviceID) . pack('n', strlen($payload)) . $payload; // Build the binary notification
        $result = fwrite($fp, $msg, strlen($msg)); // Send it to the server
        return $result;
    }

    /**
     * Save notification history
     * @param string $notification_type
     * @param string $title
     * @param string $body
     * @param int $loco_user_id
     * @param int $table_availability_id
     * @return int notification insert id
     */
    protected function saveNotification($cleaner_id,$customer_id,$booking_id,$notification_type, $notification_title, $notification_body) {
        
        mysqli_query($this->db, "INSERT INTO `table_notification`(cleaner_id,customer_id,booking_id,notification_type,notification_title,notification,create_date) "
                . "VALUES ('$cleaner_id','$customer_id','$booking_id','$notification_type','$notification_title','$notification_body','" . date('Y-m-d H:i:s') . "')");

        return mysqli_insert_id($this->db);
    }

    /**
     * Get Notification String
     * @param string $notification_type
     * @param int $user_id
     * @return array message details
     */
    protected function getNotificationString($notification_type, $user_id) {
        // echo "Notification Type = ".$notification_type;
        // echo "User ID = ".$user_id;
        // exit;
        $msg = [];
        /* Get user app  language */
        $query = mysqli_query($this->db, "SELECT `app_language` FROM `table_customer` WHERE `customer_id`= $user_id");
        $user_detail = mysqli_fetch_assoc($query);
        $user_app_lang = $user_detail['app_language']; // User app launguage

        /* notification string */
        $notification_title_column = "notification_title_" . $user_app_lang;
        $notification_body_column = "notification_body_" . $user_app_lang;
        $query = mysqli_query($this->db, "SELECT `$notification_title_column`,`$notification_body_column` FROM `tbl_notification_content` WHERE `notification_type`='$notification_type'");

        $data = mysqli_fetch_array($query);
        $msg['title'] = $data[0];
        $msg['body'] = $data[1];
        // echo "<pre>";
        // print_r($msg);
        // exit;
        return $msg;
    }

    /**
     * @param int $user_id
     * @return array user details
     */
    protected function getUserDetails($user_id) {
        /* Get user all details */

        $query = mysqli_query($this->db, "SELECT * FROM `table_customer` WHERE `customer_id`= $user_id");
        $user_detail = mysqli_fetch_assoc($query);

        return $user_detail; // Return user details array
    }

    public function writeLog($data ,  $deviceID = null, $table_availability_id = null) {
        file_put_contents("../notification_log.log", date('Y-m-d H:i:s') . '=>' . print_r($data, true) .', device_id=>'.$deviceID. 'table_id=>'.$table_availability_id. "\r\n", FILE_APPEND);
    }

}
