<?php


// include("../includes/config.inc.php");

/**
 * Description of FCM
 *
 * @author Abhishek
 */

class FCM_Cleaner {

    private $db;

    function __construct() {
        date_default_timezone_set("UTC");
        $this->db = mysqli_connect(MYSQL_HOST_NAME, MYSQL_USER_NAME, MYSQL_PASSWORD, MYSQL_DB_NAME);
        mysqli_set_charset($this->db, 'utf8');
    }

    public function send($notification_type, $booking_id = 0, $cleaner_id = 0 , $booking_detail_id = 0) {
        $cleaner_noti_details = $this->getcleanerDetails($cleaner_id);
        $device_type = $cleaner_noti_details['login_type'];
        $deviceID = $cleaner_noti_details['gcm_regid'];
        
        if (isset($notification_type) && !empty($notification_type)) {            
            if ($notification_type == 'CAL') {
                $notification_type_to_save = 'CAL';
            }

            $msg = $this->getNotificationString($notification_type, $cleaner_id);
            
            $notification_id = $this->saveNotification($cleaner_id,$user_id,$booking_id,$notification_type_to_save, $msg['title'], $msg['body']);
            
            #check user notication type and check, is user enabled this type notification??
            if ($notification_type === 'CAL') {
                if ($device_type == 'A') {
                    return $this->sendForAndroid($msg, $deviceID, $notification_type , $booking_detail_id);
                } elseif ($device_type == 'I') {
                    return $this->sendForIOS($msg, $deviceID, $notification_type , $booking_detail_id);
                }exit;
            }
            
        }
    }


    private function sendForAndroid($msg, $deviceID, $notification_type,$booking_detail_id) {
        
       
        $url = 'https://fcm.googleapis.com/fcm/send';
        $fields = array(
            'to' => $deviceID,
            'data' => array(
                "body" => $msg['body'],
                "title" => $msg['title'],
                "booking_id" => $booking_detail_id,
                "type" => $notification_type
            )
        );

        // echo "<pre>";
        // print_r($fields);
        // exit;
        
        $fields = json_encode($fields);
       
        $headers = array(
            'Authorization: key=' . "AAAAZRFrPsI:APA91bFdCPr7SMcz8AMxduv8PNEzvZvrRChVuTIQFvW-RhzDcrW00Bb3SHHJVQt0utH9zpwvxwdi-gv1V-Dyec5Y-XX5UXltTLQawcexv8K1KJiyH8v-EEWKUAwFVg4a5rZp_zIyuX4P",
            'Content-Type: application/json'
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);

        $result = curl_exec($ch);
     
        curl_close($ch);
        $this->writeLog($result); // Write log
        return $result;
    }

    private function sendForIOS($msg, $deviceID, $notification_type , $booking_detail_id = 0) {

        $pemfilename =__DIR__."/ECleaning.pem"; 
        $passphrase = '';
       
        $body['aps'] = array(
            'alert' => $msg['title'],
            'badge' => 0,
            'sound' => 'ringtone',
            ); 
        $payload['aps'] = array(
            'alert' => array(
                'title' => $msg['title'],
                'body' => $msg['body']
            ),
            'badge' => 0,
            'sound' => 'default'
        );
        $payload['payload'] = array(
                "booking_id" => $booking_detail_id,
                "type" => $notification_type,
                'title' => $msg['title'],
                'body' => $msg['body']
        );
        $body['payload'] = $payload['payload']; // Create the payload body
     
        $data = array("title"=> "Dummy Notification", "body"=> "Body of the message", "payload"=>array('from_usr'=>1, 'first_name'=> 'Jyoti', 'last_name'=> 'Sharma','actionType'=>"connect", 'actionTypeCode'=>"1"));

        $ctx = stream_context_create();
        stream_context_set_option($ctx, 'ssl', 'local_cert', $pemfilename);
        stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
      
        $fp = stream_socket_client('ssl://gateway.sandbox.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx);
        $payload = json_encode($body); // Encode the payload as JSON
        $msg = chr(0) . pack('n', 32) . pack('H*', $deviceID) . pack('n', strlen($payload)) . $payload; // Build the binary notification
        $result = fwrite($fp, $msg, strlen($msg)); // Send it to the server
        return $result;
    }

    /**
     * Save notification history
     * @param string $notification_type
     * @param string $title
     * @param string $body
     * @return int notification insert id
     */
    protected function saveNotification($cleaner_id,$customer_id,$booking_id,$notification_type, $notification_title, $notification_body) {
        
        mysqli_query($this->db, "INSERT INTO `table_notification`(cleaner_id,customer_id,booking_id,notification_type,notification_title,notification,create_date) "
                . "VALUES ('$cleaner_id','$customer_id','$booking_id','$notification_type','$notification_title','$notification_body','" . date('Y-m-d H:i:s') . "')");

        return mysqli_insert_id($this->db);
    }

    /**
     * Get Notification String
     * @param string $notification_type
     * @param int $cleaner_id
     * @return array message details
     */
    protected function getNotificationString($notification_type, $cleaner_id) {
        $msg = [];
        /* Get user app  language */
        $query = mysqli_query($this->db, "SELECT `app_language` FROM `table_cleaner` WHERE `cleaner_id`= $cleaner_id");
        $user_detail = mysqli_fetch_assoc($query);
        $user_app_lang = $user_detail['app_language']; // User app launguage

        /* notification string */
        $notification_title_column = "notification_title_" . $user_app_lang;
        $notification_body_column = "notification_body_" . $user_app_lang;
        $query = mysqli_query($this->db, "SELECT `$notification_title_column`,`$notification_body_column` FROM `tbl_notification_content` WHERE `notification_type`='$notification_type'");

        $data = mysqli_fetch_array($query);
        $msg['title'] = $data[0];
        $msg['body'] = $data[1];
        return $msg;
    }

    /**
     * @param int cleaner_id
     * @return array cleaner details
     */
    protected function getcleanerDetails($cleaner_id) {
        /* Get user all details */

        $query = mysqli_query($this->db, "SELECT * FROM `table_cleaner` WHERE `cleaner_id`= $cleaner_id");
        $cleaner_detail = mysqli_fetch_assoc($query);

        return $cleaner_detail; // Return user details array
    }

    public function writeLog($data ,  $deviceID = null, $table_availability_id = null) {
        file_put_contents("../notification_log.log", date('Y-m-d H:i:s') . '=>' . print_r($data, true) .', device_id=>'.$deviceID. 'table_id=>'.$table_availability_id. "\r\n", FILE_APPEND);
    }

}
