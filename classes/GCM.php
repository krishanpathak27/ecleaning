<?php 
class GCM {
 
    //put your code here
    // constructor
    function __construct() {
 
    }
    
   /* const GOOGLE_API_KEY_CUSTOMER = 'AAAAAg4-gsY:APA91bHlPPYaojfTBbZ1K1Ypmw4P5b569QqpdaCsZh2ItDFw9iWK3O8M9QM1Vug2ikGI9KVizF8Dg5cwR7nRbL6_M3S2nykX5ERLir-s-Z6TlsxdWyh1aiC-Nq-UObEy3n3qZiWrl7Hz'; */

 	//$GOOGLE_API_KEY_CUSTOMER = 'AIzaSyALGba12FUBeKqMQOk46p8CjZhYb4UNxLI';

     /**
     * Sending Push Notification
     */
    public function send_notification($registatoin_ids, $message, $type) {
     // include config
       // include_once './config.php';
 
        // Set POST variables
        $url = 'https://android.googleapis.com/gcm/send';
 
        $fields = array(
            'registration_ids' => $registatoin_ids,
            'data' => $message,
        );

        
           $googleKey='AIzaSyALGba12FUBeKqMQOk46p8CjZhYb4UNxLI';
        
        $headers = array(
            'Authorization: key=' . $googleKey,
            'Content-Type: application/json'
        );
        // Open connection
        $ch = curl_init();
 
        // Set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_URL, $url);
 
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
 
        // Disabling SSL Certificate support temporarly
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
 
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
 
        // Execute post
        $result = curl_exec($ch);
        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }
 
        // Close connection
        curl_close($ch);
      //  echo $result;
    }

    function sendPushNotificationIOS($deviceToken, $message) {
      
      //echo "deviceToken->". $deviceToken; 
     
     // Put your device token here (without spaces):
    // $deviceToken = '42cb67da1c185bdffb279abdcf54f44b2ffe0abd10e3c068ac326cade59308ed';


    // // Put your alert message here:
   //  $message = 'Hello, 365onserver PUSH NOTIFICATION';
            

    // Put your private key's passphrase here:
    $passphrase = 'qwerty';

    ////////////////////////////////////////////////////////////////////////////////

    // Create a Stream
    $ctx = stream_context_create();
    
     $filepath    = str_replace('/admin', '/services', getcwd());
     
     $pemFilePath = $filepath.'/Certificates.pem';

     // Define the certificate to use
     
    //stream_context_set_option($ctx, 'ssl', 'local_cert', '/var/www/html/365onserver/services/apns-dev.pem');
     stream_context_set_option($ctx, 'ssl', 'local_cert', $pemFilePath);
    // Passphrase to the certificate
    stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);

    // Open a connection to the APNS server
    $url = 'ssl://gateway.sandbox.push.apple.com:2195';

    // Open a connection to the APNS server
    $fp = stream_socket_client($url, $err, $errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);

    // Check that we've connected
    if (!$fp)
        exit("Failed to connect: $err $errstr" . PHP_EOL);

    echo 'Connected to APNS' . PHP_EOL;

    // Create the payload body
    $body['aps'] = array(
        'alert' => array(
            'body' => $message,
            'action-loc-key' => 'Bango App',
        ),
        'badge' => 2,
        'sound' => 'oven.caf',
        );

    // Encode the payload as JSON
    $payload = json_encode($body);

    // Build the binary notification
    $msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;

    // Send it to the server
    $result = fwrite($fp, $msg, strlen($msg));

    if (!$result)
     echo 'Message not delivered' . PHP_EOL;
    else
     echo 'Message successfully delivered' . PHP_EOL;

    // Close the connection to the server
    fclose($fp);

    }
 
}

// $obj = new GCM;
// $registatoin_ids = "APA91bGd6JNopqY4Ns6ZuKa3m8sX1s2x80vgYMnSCNT6dLURZirZ3SxOdDlsGilRQX9Z6fhbyRy-klJEJ8Dd1Ve4ssh-26bGaoVj3LqLAKymJEY-5rPA9ArcWc3EeoMuJu-wve8PzKT6";
// $registatoin_ids = array($registatoin_ids);
// $message = array("message" => "Hello Nisha");
// $Type   =  'C';

// $obj->send_notification($registatoin_ids, $message,$Type);


?>
