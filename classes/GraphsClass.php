<?php require_once('Db_Action.php');


class GraphsClass extends DB_Action {

	private $salesmanCondition = "";
	private $salesmanList = array();	

	public function __construct() {
		 include("../includes/globalarraylist.php");

		 $this->_account_id = $_SESSION['accountId']; 

		 $this->divisionArr ='';

		 $this->divisionArr = implode(",", $divisionList);
		//From date  
		if(isset($_SESSION['FROM_DATE']) && $_SESSION['FROM_DATE'] != ''){  
			 $this->_CUR_MONTH_START_DATE = $_SESSION['FROM_DATE']; 

		}else
		{
			$this->_CUR_MONTH_START_DATE = date('Y-m-01');
		}

		//To date
		if(isset($_SESSION['TO_DATE']) && $_SESSION['TO_DATE'] != ''){
			$this->_CURDATE = $_SESSION['TO_DATE'];			
		}else
		{
			$this->_CURDATE = date('Y-m-d');
		}

		//attandance date
		if(isset($_SESSION['DATE_ATTENDANCE']) && $_SESSION['DATE_ATTENDANCE'] != '')
		{
			$_SESSION['DATE_ATTENDANCE'] = $_SESSION['DATE_ATTENDANCE'];			
		}else
		{
			$_SESSION['DATE_ATTENDANCE'] = date('Y-m-d');
		}
		
		$this->_YEAR_START_DATE = date('Y-01-01');
		
		parent::__construct();

		// Added this condition only when user logged in except admin
		if(isset($_SESSION['userLoginType']) && $_SESSION['userLoginType'] > 1) {

			$this->_objArrayList = new ArrayList();
			$this->salesmanList = $this->_objArrayList->SalesmanArrayList();

		}



	}




	/*********************************
	* DESC: Dashboard summary Total Calls, Productive calls, total invoice amount & newly added retailers
	* Author: AJAY
	* created: 2016-03-02
	*
	**/

	public function dashBoardSummary () {

		
		$resultSet["retailer"] = array('ttlCalls'=>0, 'productiveCalls'=>0, 'ttlAmnts'=>'0.0', 'newlyAddedRet'=>0);
		$resultSet["distributor"] = array('ttlCalls'=>0, 'productiveCalls'=>0, 'ttlAmnts'=>'0.0', 'newlyAddedRet'=>0);

		if(sizeof($this->salesmanList)>0) {  
			$salList = implode(",", $this->salesmanList); 
			$salesmanCondition = ' AND O.salesman_id IN ('.$salList.')';
		} 

		// Total Calls, Productive Calls AND Total Order Amount For Retailer***************************************
		/*$analyticResult = $this->_getSelectList('table_order AS O
		LEFT JOIN table_retailer AS R ON R.retailer_id = O.retailer_id
		LEFT JOIN table_retailer AS R2 ON R2.retailer_id = O.retailer_id AND O.order_type != "No" ',
		' count(R.retailer_id) AS total_call, count(R2.retailer_id) AS productive_call,count(DISTINCT(R2.retailer_id)) AS total_retailers , SUM( O.acc_total_invoice_amount ) AS total_amt ',''," O.retailer_id!=0 AND O.date_of_order >= '".$this->_CUR_MONTH_START_DATE."' and O.date_of_order <= '".$this->_CURDATE."' AND R.new='' $salesmanCondition ");
*/
		
$analyticResult = $this->_getSelectList('table_order AS O
		LEFT JOIN table_retailer AS R ON R.retailer_id = O.retailer_id
		LEFT JOIN table_retailer AS R2 ON R2.retailer_id = O.retailer_id AND O.order_type != "No" ',
		' count(R.retailer_id) AS total_call, count(R2.retailer_id) AS productive_call,count(DISTINCT(R2.retailer_id)) AS total_retailers , SUM( O.acc_total_invoice_amount ) AS total_amt ',''," O.retailer_id!=0 AND O.date_of_order >= '".$this->_CUR_MONTH_START_DATE."' and O.date_of_order <= '".$this->_CURDATE."' AND R.new=''  $salesmanCondition ");



		// O.date_of_order >= '".$this->_CUR_MONTH_START_DATE."' and O.date_of_order <= '".$this->_CURDATE."' AND

		if(sizeof($analyticResult)> 0 ) {

			foreach ($analyticResult as $key => $value) {
				# code...
				$resultSet["retailer"]['ttlCalls'] = $value->total_call;
				$resultSet["retailer"]['productiveCalls'] = $value->productive_call;
				$resultSet["retailer"]['totalRetailers'] = $value->total_retailers;
				if($value->total_amt>0)
					$resultSet["retailer"]['ttlAmnts'] = $value->total_amt;
			}
		}
$analyticResult2 = $this->_getSelectList('table_order AS O
		LEFT JOIN table_retailer AS R ON R.retailer_id = O.retailer_id ',
		' count(O.retailer_id) AS new_total_call',''," O.retailer_id!=0 AND O.date_of_order >= '".$this->_CUR_MONTH_START_DATE."' and O.date_of_order <= '".$this->_CURDATE."' AND R.new='' $salesmanCondition AND (O.order_type = 'Yes' or O.order_type='Adhoc')");

if(sizeof($analyticResult2)> 0 ) {

			foreach ($analyticResult2 as $key => $value) {
				# code...
				$resultSet["retailer"]['ttlnewCalls'] = $value->new_total_call;
				
			}
		}

		$analyticResult3 = $this->_getSelectList('table_order AS O
		LEFT JOIN table_retailer AS R ON R.retailer_id = O.retailer_id ',
		' count(O.retailer_id) AS no_total_call',''," O.retailer_id!=0 AND O.date_of_order >= '".$this->_CUR_MONTH_START_DATE."' and O.date_of_order <= '".$this->_CURDATE."' AND R.new='' $salesmanCondition AND O.order_type = 'No'");

if(sizeof($analyticResult3)> 0 ) {

			foreach ($analyticResult3 as $key => $value) {
				# code...
				$resultSet["retailer"]['nottlCalls'] = $value->no_total_call;
				
			}
		}


		// get the newly added retailer Count

		if(sizeof($this->salesmanList)>0) {  
			$salList = implode(",", $this->salesmanList); 
			$salesmanCondition1 = ' AND A.salesman_id IN ('.$salList.')';
		} 



		$whereCondition =" R.account_id = '".$_SESSION['accountId']."' AND A.activity_type = 5 AND activity_date BETWEEN '".$this->_CUR_MONTH_START_DATE."' AND '".$this->_CURDATE."' $salesmanCondition1 ";
		//AND activity_date BETWEEN ('".$this->_CUR_MONTH_START_DATE."' AND '".$this->_CURDATE."')


		$newlyAddedRetResult1 = $this->_getSelectList2('table_activity AS A INNER JOIN table_retailer AS R ON R.retailer_id = A.ref_id','count(*) AS total','',$whereCondition);
		if(sizeof($newlyAddedRetResult1)>0)
			$resultSet["retailer"]['newlyAddedRethot'] = $newlyAddedRetResult1[0]->total;


		// get the newly added retailer Count(hot)

		$whereCondition =" R.account_id = '".$_SESSION['accountId']."' AND A.activity_type = 5 AND activity_date BETWEEN '".$this->_CUR_MONTH_START_DATE."' AND '".$this->_CURDATE."' $salesmanCondition1 ";
		//AND activity_date BETWEEN ('".$this->_CUR_MONTH_START_DATE."' AND '".$this->_CURDATE."')
		$newlyAddedRetResult2 = $this->_getSelectList2('table_activity AS A INNER JOIN table_retailer AS R ON R.retailer_id = A.ref_id','count(*) AS total','',$whereCondition);
		if(sizeof($newlyAddedRetResult2)>0)
			$resultSet["retailer"]['newlyAddedRetcold'] = $newlyAddedRetResult2[0]->total;



		// get the newly added retailer Count (cold)

		$whereCondition =" R.account_id = '".$_SESSION['accountId']."' AND A.activity_type = 5 AND activity_date BETWEEN '".$this->_CUR_MONTH_START_DATE."' AND '".$this->_CURDATE."' $salesmanCondition1 ";
		//AND activity_date BETWEEN ('".$this->_CUR_MONTH_START_DATE."' AND '".$this->_CURDATE."')
		$newlyAddedRetResult = $this->_getSelectList2('table_activity AS A INNER JOIN table_retailer AS R ON R.retailer_id = A.ref_id','count(*) AS total','',$whereCondition);
		if(sizeof($newlyAddedRetResult)>0)
			$resultSet["retailer"]['newlyAddedRet'] = $newlyAddedRetResult[0]->total;





		
		// Total Calls, Productive Calls AND Total Order Amount For Distributors*********************************
		$analyticResult = $this->_getSelectList('table_order AS O
		LEFT JOIN table_distributors AS R ON R.distributor_id = O.distributor_id
		LEFT JOIN table_distributors AS R2 ON R2.distributor_id = O.distributor_id AND O.order_type != "No" ',
		' count(R.distributor_id) AS total_call, count(R2.distributor_id) AS productive_call,count(DISTINCT(R2.distributor_id)) AS total_distributors, SUM( O.acc_total_invoice_amount ) AS total_amt ',''," O.retailer_id=0 AND O.date_of_order >= '".$this->_CUR_MONTH_START_DATE."' and O.date_of_order <= '".$this->_CURDATE."' AND R.new=''  and O.order_status = 'A' $salesmanCondition "); 
		// O.date_of_order >= '".$this->_CUR_MONTH_START_DATE."' and O.date_of_order <= '".$this->_CURDATE."' AND

		if(sizeof($analyticResult)> 0 ) {

			foreach ($analyticResult as $key => $value) {
				# code...
				$resultSet["distributor"]['ttlCalls'] = $value->total_call;
				$resultSet["distributor"]['productiveCalls'] = $value->productive_call;
				$resultSet["distributor"]['totalDistributors'] = $value->total_distributors;
				if($value->total_amt>0)
					$resultSet["distributor"]['ttlAmnts'] = $value->total_amt;
			}
		}

		$analyticResult2 = $this->_getSelectList('table_order AS O
		LEFT JOIN table_distributors AS R ON R.distributor_id = O.distributor_id',
		' count(O.distributor_id) AS new_total_call',''," O.retailer_id=0 AND O.date_of_order >= '".$this->_CUR_MONTH_START_DATE."' and O.date_of_order <= '".$this->_CURDATE."' AND R.new='' $salesmanCondition AND (O.order_type = 'Yes' or O.order_type='Adhoc')  and O.order_status = 'A'"); 
		// O.date_of_order >= '".$this->_CUR_MONTH_START_DATE."' and O.date_of_order <= '".$this->_CURDATE."' AND

		if(sizeof($analyticResult2)> 0 ) {

			foreach ($analyticResult2 as $key => $value) {
				# code...
				$resultSet["distributor"]['ttlnewCalls'] = $value->new_total_call;
				
		}
	}

		$analyticResult3 = $this->_getSelectList('table_order AS O
		LEFT JOIN table_distributors AS R ON R.distributor_id = O.distributor_id ',
		' count(O.distributor_id) AS no_total_call',''," O.retailer_id=0 AND O.date_of_order >= '".$this->_CUR_MONTH_START_DATE."' and O.date_of_order <= '".$this->_CURDATE."' AND R.new='' $salesmanCondition AND (O.order_type = 'No') and O.order_status = 'A'"); 
		// O.date_of_order >= '".$this->_CUR_MONTH_START_DATE."' and O.date_of_order <= '".$this->_CURDATE."' AND

		if(sizeof($analyticResult3)> 0 ) {

			foreach ($analyticResult3 as $key => $value) {
				# code...
				$resultSet["distributor"]['nottlCalls'] = $value->no_total_call;
				
		}
	}


		// get the newly added distributor Count

		$whereCondition =" R.account_id = '".$_SESSION['accountId']."' AND A.activity_type = 24 AND activity_date BETWEEN '".$this->_CUR_MONTH_START_DATE."' AND '".$this->_CURDATE."' $salesmanCondition1 ";
		//AND activity_date BETWEEN '".$this->_CUR_MONTH_START_DATE."' AND '".$this->_CURDATE."'
		$newlyAddedRetResult = $this->_getSelectList2('table_activity AS A INNER JOIN table_distributors AS R ON R.distributor_id = A.ref_id','count(*) AS total','',$whereCondition);
		if(sizeof($newlyAddedRetResult)>0)
			$resultSet["distributor"]['newlyAddedRet'] = $newlyAddedRetResult[0]->total;
		/*****************************************************************************/

		// get the newly added distributor Count hot

		$whereCondition =" R.account_id = '".$_SESSION['accountId']."'  AND A.activity_type = 24 AND activity_date BETWEEN '".$this->_CUR_MONTH_START_DATE."' AND '".$this->_CURDATE."' $salesmanCondition1 ";
		//AND activity_date BETWEEN '".$this->_CUR_MONTH_START_DATE."' AND '".$this->_CURDATE."'
		$newlyAddedRetResult1 = $this->_getSelectList2('table_activity AS A INNER JOIN table_distributors AS R ON R.distributor_id = A.ref_id','count(*) AS total','',$whereCondition);
		if(sizeof($newlyAddedRetResult1)>0)
			$resultSet["distributor"]['newlyAddedRethot'] = $newlyAddedRetResult1[0]->total;
		/*****************************************************************************/


		// get the newly added distributor Count cold

		$whereCondition =" R.account_id = '".$_SESSION['accountId']."' AND A.activity_type = 24 AND activity_date BETWEEN '".$this->_CUR_MONTH_START_DATE."' AND '".$this->_CURDATE."' $salesmanCondition1 ";
		//AND activity_date BETWEEN '".$this->_CUR_MONTH_START_DATE."' AND '".$this->_CURDATE."'
		$newlyAddedRetResult2 = $this->_getSelectList2('table_activity AS A INNER JOIN table_distributors AS R ON R.distributor_id = A.ref_id','count(*) AS total','',$whereCondition);
		if(sizeof($newlyAddedRetResult2)>0)
			$resultSet["distributor"]['newlyAddedRetcold'] = $newlyAddedRetResult2[0]->total;
		/*****************************************************************************/


		return $resultSet;

	}




/*********************************
	* DESC: Salesman Attendance Graph
	* Author: Maninder
	* created: 2016-07-21
	*
	**/

	public function salesmanAttandance () {

	$resultSet = array('division'=>array(), 'notMarked'=>array(), 'Marked'=>array());	

	$result = $this->_getSelectList('table_division '," * ",''," status='A'  order by division_id");

	if(isset($_SESSION['userLoginType']) && $_SESSION['userLoginType'] > 1) {

//print_r($this->_objArrayList->SalesmanArrayList()); die;
 //echo $rtndata = $this->_objArrayList->GetSalesmenMenu($this->salsList, $_SESSION['SalAttList']);
/*
		$saldiv=$this->_getSelectList2('table_salesman as s '," s.division_id ",'',"s.salesman_id=".$_SESSION['salesmanId']);
			//print_r($saldiv);die;
			
	       $divisioncondition="AND d.division_id = ".$saldiv[0]->division_id;

		$salids = $this->_getSelectList2('table_salesman_hierarchy_relationship as thr LEFT JOIN table_salesman_hierarchy as th on thr.hierarchy_id=th.hierarchy_id  '," th.sort_order ",'',"thr.salesman_id=".$_SESSION['salesmanId']);
			$sal = $this->_getSelectList2('table_salesman_hierarchy as thr1  '," thr1.hierarchy_id ",'',"thr1.sort_order>".$salids[0]->sort_order);
			foreach ($sal as $key => $value) {
				# code...

				$salarray[]=$value->hierarchy_id;
			}
			
			$result1=implode(",",$salarray);
			
			$salesmanids=$this->_getSelectList2('table_salesman_hierarchy_relationship  as thr2  '," thr2.salesman_id ",'',"thr2.hierarchy_id IN(".$result1.")");


			foreach ($salesmanids as $key => $value) {
				# code...

				$salesmanidarray[]=$value->salesman_id;
			}*/
			$salesmanidarray=$this->_objArrayList->SalesmanArrayList();

			$result2=implode(",",$salesmanidarray);

			$salesmancondition="AND s.salesman_id IN(".$result2.")";

			/*print_r($salesmancondition);die;*/
			
		}

		if(sizeof($result)>0) {
			$rows=count($result);
		$division_val=array();
		$division_Name=array();
			$a=1;
			foreach ($result as $key => $value) {				
				$division_val[] =$value->division_id;
				$division_Name[] =$value->division_name;
				$a++;
			}			
			$resultSet['division'] =$division_Name;

		}

		//for not Attended
		$p=1;
		foreach ($division_val as $key1 => $value1) {
		 if($p<sizeof($division_val)){ $co=",";} else {$co="";}	
		 	$resultS = $this->_getSelectList('table_salesman as s  LEFT JOIN table_division as d on s.division_id=d.division_id '," count(*) as tactive ",''," s.status='A' and d.division_id='".$value1."' $salesmancondition   group by d.division_id order by d.division_name DESC");	


		 	$result1 = $this->_getSelectList2('table_activity_salesman_attendance as a  LEFT JOIN table_salesman as s on s.salesman_id=a.salesman_id LEFT JOIN table_division as d on d.division_id=s.division_id '," count(*) as attendence,d.division_name ",''," a.activity_type='11' and s.status='A'  and d.division_id=".$value1." and a.activity_date='".$_SESSION['DATE_ATTENDANCE']."' $salesmancondition  AND s.account_id='".$_SESSION['accountId']."' group by d.division_id,a.activity_date,a.activity_type order by d.division_name DESC");	
		 	//and a.activity_date='".$this->_CURDATE."'		 	

		 	$actul_not_attendence = $resultS[0]->tactive - $result1[0]->attendence;
		 	$resultSet['notMarked'][] = abs($actul_not_attendence);

		$p++;
		}

		//for attended
		$q=1;
		foreach ($division_val as $key2 => $value2) {
		 if($q<sizeof($division_val)){ $co=",";} else {$co="";}		 	

		 	$result11 = $this->_getSelectList2('table_activity_salesman_attendance as a  LEFT JOIN table_salesman as s on s.salesman_id=a.salesman_id LEFT JOIN table_division as d on d.division_id=s.division_id '," count(*) as attendence,d.division_name ",''," a.activity_type='11' and s.status='A' $salesman and d.division_id=".$value2." and a.activity_date='".$_SESSION['DATE_ATTENDANCE']."' $salesmancondition AND s.account_id='".$_SESSION['accountId']."'	  group by d.division_id,a.activity_date,a.activity_type order by d.division_name DESC");
		 	//and a.activity_date='".$this->_CURDATE."'			 	

		 	if($result11[0]->attendence!=""){ $act=$result11[0]->attendence;} else{$act=0;}
		 	$resultSet['Marked'][] = abs($act);
		$q++;
		}
		
		//return data			
		return $resultSet;
	}










	/*********************************
	* DESC: Attendance Graph
	* Author: Maninder Kumar
	* created: 2016-07-21
	*
	**/

	public function topRetailers () {

	$resultSet = array('month'=>date('M'), 'ttlAmnt'=>array(), 'data'=>array());

	if($this->divisionArr != '' && !empty($this->divisionArr)){
				$divCond = "AND R.division_id IN (".$this->divisionArr.")";
			}

			if(isset($_SESSION['userLoginType']) && $_SESSION['userLoginType'] > 1) {


			$salesmanidarray=$this->_objArrayList->SalesmanArrayList();

			$result2=implode(",",$salesmanidarray);

			$salesman="AND O.salesman_id IN(".$result2.")";

			

			
			/*print_r($salesmancondition);die;*/
			
		}	 	


	$result = $this->_getSelectList2('`table_order` AS O LEFT JOIN table_retailer AS R ON R.retailer_id = O.retailer_id Left join table_salesman as s on s.salesman_id=O.salesman_id ',
		" O.`account_id` , O.`retailer_id` , R.`retailer_name` , SUM( O.`total_invoice_amount` ) AS AMOUNTSUM, O.`date_of_order` ",'',
		" O.date_of_order >= '".$this->_CUR_MONTH_START_DATE."' and O.date_of_order <= '".$this->_CURDATE."' and  R.new!='1' and R.status='A'  $salesman $divCond  GROUP BY O.`retailer_id`  ORDER BY  `AMOUNTSUM` DESC LIMIT 0 , 10");

	//O.date_of_order >= '".$this->_CUR_MONTH_START_DATE."' and O.date_of_order <= '".$this->_CURDATE."' and

		if(sizeof($result)>0) {

			foreach ($result as $key => $value) {
				# code...
				$resultSet['ttlAmnt'][] = $value->AMOUNTSUM;
				$resultSet['data'][] 	= array('retailer_name'=> htmlentities($value->retailer_name), 'ttlSale'=>$value->AMOUNTSUM);
			}

		}
		//print_r($resultSet);
		return $resultSet;

	}


	/*********************************
	* DESC: Attendance Graph
	* Author: Maninder Kumar
	* created: 2016-07-21
	*
	**/

	public function topDistributors () {

	$resultSet = array('month'=>date('M'), 'ttlAmnt'=>array(), 'data'=>array());

	if($this->divisionArr != '' && !empty($this->divisionArr)){
				$divCond = "AND R.division_id IN (".$this->divisionArr.")";
			}

			if(isset($_SESSION['userLoginType']) && $_SESSION['userLoginType'] > 1) {


			$salesmanidarray=$this->_objArrayList->SalesmanArrayList();

			$result2=implode(",",$salesmanidarray);

			$salesman="AND O.salesman_id IN(".$result2.")";

			

			
			/*print_r($salesmancondition);die;*/
			
		}	 	



	$result = $this->_getSelectList2('`table_order` AS O LEFT JOIN table_distributors AS R ON R.distributor_id = O.distributor_id Left join table_salesman as s on s.salesman_id=O.salesman_id ',
		" O.`account_id` , O.`distributor_id` , R.`distributor_name` as retailer_name , SUM( O.`total_invoice_amount` ) AS AMOUNTSUM, O.`date_of_order` ",'',
		" O.date_of_order >= '".$this->_CUR_MONTH_START_DATE."' and O.date_of_order <= '".$this->_CURDATE."' and  R.new!='1' and R.status='A'  $salesman $divCond  GROUP BY O.`distributor_id`  ORDER BY  `AMOUNTSUM` DESC LIMIT 0 , 10");

	//O.date_of_order >= '".$this->_CUR_MONTH_START_DATE."' and O.date_of_order <= '".$this->_CURDATE."' and

		if(sizeof($result)>0) {

			foreach ($result as $key => $value) {
				# code...
				$resultSet['ttlAmnt'][] = $value->AMOUNTSUM;
				$resultSet['data'][] 	= array('retailer_name'=> htmlentities($value->retailer_name), 'ttlSale'=>$value->AMOUNTSUM);
			}

		}
		//print_r($resultSet);
		return $resultSet;

	}











	/*********************************
	* DESC: Order Performance for Distributors
	* Author: Manidner
	* created: 2016-07-21
	*
	**/

	public function orderPerfomanceThisYear () {

	$resultSet = array('1'=>0, '2'=>0, '3'=>0, '4'=>0, '5'=>0, '6'=>0, '7'=>0, '8'=>0, '9'=>0, '10'=>0, '11'=>0, '12'=>0);	

	if($this->divisionArr != '' && !empty($this->divisionArr)){
				$divCond = "AND R.division_id IN (".$this->divisionArr.")";
			}

			if(isset($_SESSION['userLoginType']) && $_SESSION['userLoginType'] > 1) {


			$salesmanidarray=$this->_objArrayList->SalesmanArrayList();

			$result2=implode(",",$salesmanidarray);

			$salesmancondition="AND O.salesman_id IN(".$result2.")";

			$monthcondition="AND thisMonth='".date('m')."'";

			
			/*print_r($salesmancondition);die;*/
			
		}	 	

	$result = $this->_getSelectList('table_order AS O 
		LEFT JOIN table_distributors AS R ON R.distributor_id = O.distributor_id ',
		" SUM(O.acc_total_invoice_amount) AS ttlAmnt, date_format(O.date_of_order,'%Y-%m') as YearMonth ,date_format(O.date_of_order,'%Y') AS thisYear , date_format(O.date_of_order,'%c') AS thisMonth ",'',
		" AND R.new='' $divCond $salesmancondition GROUP BY DATE_FORMAT(O.date_of_order,'%Y-%m') HAVING thisYear='".date('Y')."' $monthcondition ORDER BY thisMonth ASC ");
	

		if(sizeof($result)>0) {
			$sum = 0;
			foreach ($result as $key => $value) {
				# code...
				//$sum = $sum + $value->ttlAmnt;
				$resultSet[$value->thisMonth] = $value->ttlAmnt;
			}

		}

		return $resultSet;

	}

	/*********************************
	* DESC: Order Performance for Retailers
	* Author: Manidner
	* created: 2016-07-21
	*
	**/

	public function orderPerfomanceThisYearRet () {

	$resultSet = array('1'=>0, '2'=>0, '3'=>0, '4'=>0, '5'=>0, '6'=>0, '7'=>0, '8'=>0, '9'=>0, '10'=>0, '11'=>0, '12'=>0);	
	if($this->divisionArr != '' && !empty($this->divisionArr)){
				$divCond = "AND R.division_id IN (".$this->divisionArr.")";
			}

			if(isset($_SESSION['userLoginType']) && $_SESSION['userLoginType'] > 1) {


			$salesmanidarray=$this->_objArrayList->SalesmanArrayList();

			$result2=implode(",",$salesmanidarray);

			$salesmancondition="AND O.salesman_id IN(".$result2.")";

			$monthcondition="AND thisMonth='".date('m')."'";

			
			/*print_r($salesmancondition);die;*/
			
		}	 	

	$result = $this->_getSelectList('table_order AS O 
		LEFT JOIN table_retailer AS R ON R.retailer_id = O.retailer_id ',
		" SUM(O.acc_total_invoice_amount) AS ttlAmnt, date_format(O.date_of_order,'%Y-%m') as YearMonth ,date_format(O.date_of_order,'%Y') AS thisYear , date_format(O.date_of_order,'%c') AS thisMonth ",'',
		" AND R.new=''  $divCond $salesmancondition  GROUP BY DATE_FORMAT(O.date_of_order,'%Y-%m') HAVING thisYear='".date('Y')."' $monthcondition ORDER BY thisMonth ASC ");

		if(sizeof($result)>0) {
			$sum = 0;
			foreach ($result as $key => $value) {
				# code...
				//$sum = $sum + $value->ttlAmnt;
				$resultSet[$value->thisMonth] = $value->ttlAmnt;
			}

		}

		return $resultSet;

	}




	/*********************************
	* DESC: Distributor market shares
	* Author: Maninder
	* created: 2016-07-21
	*
	**/

	public function distributorsMarketSharePerformance () {

	$resultSet   = array();
	$percentage  = array();

	if(sizeof($this->salesmanList)>0) {  
		$salList = implode(",", $this->salesmanList); 
		$salesmanCondition = ' AND O.salesman_id IN ('.$salList.')';
	} 

	if(isset($_SESSION['userLoginType']) && $_SESSION['userLoginType'] > 1) {


			$salesmanidarray=$this->_objArrayList->SalesmanArrayList();

			$result2=implode(",",$salesmanidarray);

			$salesmancondition="AND O.salesman_id IN(".$result2.")";

			
			/*print_r($salesmancondition);die;*/
			
		}	 	

	if($this->divisionArr != '' && !empty($this->divisionArr)){
				$divCond = "AND D.division_id IN (".$this->divisionArr.")";
			}

	$distributorOrderList =  $this->_getSelectList('table_order AS O 
		LEFT JOIN table_distributors AS D ON D.distributor_id = O.distributor_id ',
		" SUM(O.acc_total_invoice_amount) as total_amt, O.distributor_id,D.distributor_name ",'',
		"  (O.date_of_order BETWEEN '".$this->_YEAR_START_DATE."' AND '".$this->_CURDATE."') $salesmancondition AND O.order_type!='No' $divCond  GROUP BY O.distributor_id ORDER BY total_amt DESC");

		$ttlSales = 0;
		foreach ($distributorOrderList as $key => $value) {
			# code...
			$ttlSales = $ttlSales + $value->total_amt;
		}		

		foreach ($distributorOrderList as $key => $value) {			
			$percentage[$value->distributor_id] = array('dname'=>$value->distributor_name, 'share'=>round($value->total_amt/$ttlSales * 100,2));
		}		

		 $d_name = array(); 
		 $p_value = array();
		 $j=0; 
		 $p_other = 0;

		 if(count($percentage)>10) {
		 	foreach($percentage as $x=>$value){
		 		if($j<10){
		 			$d_name[] = array('distributor_name'=>$value['dname'],'value'=>$value['share']);
					$j=$j+1;
				} else {
					$d_other = 'Others';
					$p_other = $p_other+$value['share'];
				}		
				
			}
		$d_name[] = array('distributor_name'=>Others,'value'=>$p_other);
		$resultSet = $d_name;


		} else {    
			$total=0;
			foreach($percentage as $x=>$value) {
		 		$d_name[] = array('distributor_name'=>$value['dname'],'value'=>$value['share']);
				$total= $total+$value['share'];
			}			
			$d_other = 'Others';
			$p_other = round(100-$total,2);
			$d_name[] = array('distributor_name'=>$d_other,'value'=>$p_other);

			$resultSet = $d_name;
		}		

	return $resultSet;

	}






	/*********************************
	* DESC: Top 5 salesman of this Year  for Distributors
	* Author: Maninder
	* created: 2016-07-21
	*
	**/

	public function top5SalesmenDistributors () {

	$resultSet   = array();

		if(sizeof($this->salesmanList)>0) {  
			$salList = implode(",", $this->salesmanList); 
			$salesmanCondition = ' AND O.salesman_id IN ('.$salList.')';
		} 

		if($this->divisionArr != '' && !empty($this->divisionArr)){
				$divCond = "AND d.division_id IN (".$this->divisionArr.")";
			}

		$result = $this->_getSelectList('table_order AS O  LEFT JOIN table_salesman AS S ON S.salesman_id = O.salesman_id 
			LEFT JOIN table_distributors AS d ON d.distributor_id=O.distributor_id ',
			" S.salesman_name, O.`date_of_order`, SUM( O.`acc_total_invoice_amount` ) AS AMOUNTSUM ",''," O.distributor_id>0  $salesmanCondition  $divCond  GROUP BY O.`salesman_id`, YEAR( O.`date_of_order` ), MONTH( O.`date_of_order` ) HAVING YEAR( O.date_of_order ) = YEAR( CURDATE( ) )  AND MONTH( O.date_of_order ) = MONTH( CURDATE( ) ) ORDER BY  AMOUNTSUM DESC LIMIT 0,5 ");

			if(sizeof($result)>0) {

				foreach ($result as $key => $value) {
					# code...
					$resultSet[] = array('salesman_name'=> htmlentities($value->salesman_name), 'ttlSale'=>$value->AMOUNTSUM);
				}

			}



	return $resultSet;

	}


	/*********************************
	* DESC: Top 5 salesman of this Year  for Retailers
	* Author: Maninder
	* created: 2016-07-21
	*
	**/

	public function top5SalesmenRetailers () {

	$resultSet   = array();

		if(sizeof($this->salesmanList)>0) {  
			$salList = implode(",", $this->salesmanList); 
			$salesmanCondition = ' AND O.salesman_id IN ('.$salList.')';
		} 

		if($this->divisionArr != '' && !empty($this->divisionArr)){
				$divCond = "AND d.division_id IN (".$this->divisionArr.")";
			}


		$result = $this->_getSelectList('table_order AS O  LEFT JOIN table_salesman AS S ON S.salesman_id = O.salesman_id 
			left join table_retailer AS d ON d.retailer_id=O.retailer_id ',
			" S.salesman_name, O.`date_of_order`, SUM( O.`acc_total_invoice_amount` ) AS AMOUNTSUM ",'',"  O.retailer_id>0   $salesmanCondition  $divCond GROUP BY O.`salesman_id`, YEAR( O.`date_of_order` ), MONTH( O.`date_of_order` ) HAVING YEAR( O.date_of_order ) = YEAR( CURDATE( ) )  AND MONTH( O.date_of_order ) = MONTH( CURDATE( ) ) ORDER BY  AMOUNTSUM DESC LIMIT 0,5 ");

			if(sizeof($result)>0) {

				foreach ($result as $key => $value) {
					# code...
					$resultSet[] = array('salesman_name'=> htmlentities($value->salesman_name), 'ttlSale'=>$value->AMOUNTSUM);
				}

			}



	return $resultSet;

	}





	/*********************************
	* DESC: Top 10 products of this Year  for Distributors
	* Author: Manidner
	* created: 2016-07-21
	*
	**/

	public function top10ProductsDistributors () {

	$resultSet   = array();

		if(sizeof($this->salesmanList)>0) {  
			$salList = implode(",", $this->salesmanList); 
			$salesmanCondition = ' AND O.salesman_id IN ('.$salList.')';
		}

		if($this->divisionArr != '' && !empty($this->divisionArr)){
				$divCond = "AND R.division_id IN (".$this->divisionArr.")";
			} 

		$result = $this->_getSelectList('table_order AS O 
			LEFT JOIN table_order_detail AS OD ON OD.order_id = O.order_id
			LEFT JOIN table_item AS I ON I.item_id = OD.item_id
			LEFT JOIN table_salesman AS S ON S.salesman_id = O.salesman_id 
			LEFT JOIN table_distributors AS R ON R.distributor_id = O.distributor_id',
			" I.item_id, I.item_name, I.item_code, O.account_id, SUM(  OD.`acc_quantity` ) AS QUANTITYSUM, O.`date_of_order` ",'',
			" R.new ='' AND I.item_id IS NOT NULL $salesmanCondition $divCond  GROUP BY OD.item_id, YEAR( O.`date_of_order` ) , MONTH( O.`date_of_order` )  HAVING YEAR( O.`date_of_order` ) = YEAR( CURDATE( ) )  AND MONTH( O.`date_of_order` ) = MONTH( CURDATE( ) ) ORDER BY  `QUANTITYSUM` DESC LIMIT 0,10 ");		

			if(sizeof($result)>0) {

				foreach ($result as $key => $value) {
					# code...
					$itemNameCode = htmlentities($value->item_name).'('.$value->item_code.')';
					$resultSet[] = array('item_name'=>$itemNameCode, 'ttlQty'=>$value->QUANTITYSUM);
				}

			}


	return $resultSet;

	}


	/*********************************
	* DESC: Top 10 products of this Year  for Retailers
	* Author: Manidner
	* created: 2016-07-21
	*
	**/

	public function top10ProductsRetailers () {

	$resultSet   = array();

		if(sizeof($this->salesmanList)>0) {  
			$salList = implode(",", $this->salesmanList); 
			$salesmanCondition = ' AND O.salesman_id IN ('.$salList.')';
		} 

		if($this->divisionArr != '' && !empty($this->divisionArr)){
				$divCond = "AND R.division_id IN (".$this->divisionArr.")";
			} 

		$result = $this->_getSelectList('table_order AS O 
			LEFT JOIN table_order_detail AS OD ON OD.order_id = O.order_id
			LEFT JOIN table_item AS I ON I.item_id = OD.item_id
			LEFT JOIN table_salesman AS S ON S.salesman_id = O.salesman_id 
			LEFT JOIN table_retailer AS R ON R.retailer_id = O.retailer_id',
			" I.item_id, I.item_name, I.item_code, O.account_id, SUM(  OD.`acc_quantity` ) AS QUANTITYSUM, O.`date_of_order` ",'',
			" R.new ='' AND I.item_id IS NOT NULL $salesmanCondition $divCond  GROUP BY OD.item_id, YEAR( O.`date_of_order` ) , MONTH( O.`date_of_order` )  HAVING YEAR( O.`date_of_order` ) = YEAR( CURDATE( ) )  AND MONTH( O.`date_of_order` ) = MONTH( CURDATE( ) ) ORDER BY  `QUANTITYSUM` DESC LIMIT 0,10 ");

			if(sizeof($result)>0) {

				foreach ($result as $key => $value) {
					# code...
					$itemNameCode = htmlentities($value->item_name).'('.$value->item_code.')';
					$resultSet[] = array('item_name'=>$itemNameCode, 'ttlQty'=>$value->QUANTITYSUM);
				}

			}


	return $resultSet;

	}






	/*********************************
	* DESC: Top 5 Cities of this Year for Distributors 
	* Author: Maninder Kumar
	* created: 2016-07-21
	*
	**/

	public function topCitiesDistributors () {

	    $resultSet   = array();

		if(sizeof($this->salesmanList)>0) {  
			$salList = implode(",", $this->salesmanList); 
			$salesmanCondition = ' AND O.salesman_id IN ('.$salList.')';
		}

		if($this->divisionArr != '' && !empty($this->divisionArr)){
				$divCond="AND s.division_id IN (".$this->divisionArr.")";
			}		

		$result = $this->_getSelectList('table_order AS O 
			LEFT JOIN table_distributors as s on s.distributor_id=O.distributor_id
			LEFT JOIN city AS C ON C.city_id = s.city',
			" C.city_id,C.city_name,O.account_id, SUM(`acc_total_invoice_amount`) AS TOTALSUM, O.date_of_order ",'',
			" C.`city_name`!='' AND O.distributor_id !='' $salesmanCondition  $divCond  GROUP BY s.city, YEAR( O.`date_of_order` ) , MONTH( O.`date_of_order` )  HAVING YEAR( O.`date_of_order` ) = YEAR( CURDATE( ) )  AND MONTH( O.`date_of_order` ) = MONTH( CURDATE( ) )  ORDER BY  `TOTALSUM` DESC LIMIT 0,5 ");

			if(sizeof($result)>0) {

				foreach ($result as $key => $value) {
					# code...
					$resultSet[] = array('city_name'=> htmlentities($value->city_name), 'ttlSale'=>$value->TOTALSUM);
				}

			}


	return $resultSet;

	}


	/*********************************
	* DESC: Top 5 Cities of this Year for Retailers 
	* Author: AJAY
	* created: 2016-07-21
	*
	**/

	public function topCitiesRetailers () {

	$resultSet   = array();

		if(sizeof($this->salesmanList)>0) {  
			$salList = implode(",", $this->salesmanList); 
			$salesmanCondition = ' AND O.salesman_id IN ('.$salList.')';
		}   

		if($this->divisionArr != '' && !empty($this->divisionArr)){
				$divCond="AND s.division_id IN (".$this->divisionArr.")";
			}	     

		$result = $this->_getSelectList('table_order AS O 
			LEFT JOIN table_retailer as s on s.retailer_id=O.retailer_id
			LEFT JOIN city AS C ON C.city_id = s.city',
			" C.city_id,C.city_name,O.account_id, SUM(`acc_total_invoice_amount`) AS TOTALSUM, O.date_of_order ",'',
			" C.`city_name`!=''  AND O.retailer_id !='' $salesmanCondition  $divCond GROUP BY s.city, YEAR( O.`date_of_order` ) , MONTH( O.`date_of_order` )  HAVING YEAR( O.`date_of_order` ) = YEAR( CURDATE( ) )  AND MONTH( O.`date_of_order` ) = MONTH( CURDATE( ) )  ORDER BY  `TOTALSUM` DESC LIMIT 0,5 ");

			if(sizeof($result)>0) {

				foreach ($result as $key => $value) {
					# code...
					$resultSet[] = array('city_name'=> htmlentities($value->city_name), 'ttlSale'=>$value->TOTALSUM);
				}

			}


	return $resultSet;

	}





	/*********************************
	* DESC: Top 5 products of  for Distributors
	* Author: Manidner
	* created: 2016-07-21
	*
	**/

	public function top5ProductsDistributors () {

	$resultSet   = array();

		if(sizeof($this->salesmanList)>0) {  
			$salList = implode(",", $this->salesmanList); 
			$salesmanCondition = ' AND O.salesman_id IN ('.$salList.')';
		} 

		if($this->divisionArr != '' && !empty($this->divisionArr)){
				$divCond = "AND R.division_id IN (".$this->divisionArr.")";
			} 

		$result = $this->_getSelectList('table_order AS O 
			LEFT JOIN table_order_detail AS OD ON OD.order_id = O.order_id
			LEFT JOIN table_item AS I ON I.item_id = OD.item_id
			LEFT JOIN table_salesman AS S ON S.salesman_id = O.salesman_id 
			LEFT JOIN table_distributors AS R ON R.distributor_id = O.distributor_id',
			" I.item_id, I.item_name, I.item_code, O.account_id, SUM(  OD.`acc_quantity` ) AS QUANTITYSUM, O.`date_of_order` ",'',
			" R.new ='' AND I.item_id IS NOT NULL $salesmanCondition $divCond  GROUP BY OD.item_id, YEAR( O.`date_of_order` ) , MONTH( O.`date_of_order` )  HAVING YEAR( O.`date_of_order` ) = YEAR( CURDATE( ) )  AND MONTH( O.`date_of_order` ) = MONTH( CURDATE( ) ) ORDER BY  `QUANTITYSUM` DESC LIMIT 0,5 ");		

			if(sizeof($result)>0) {

				foreach ($result as $key => $value) {
					# code...
					$itemNameCode = htmlentities($value->item_name).'('.$value->item_code.')';
					$resultSet[] = array('item_name'=>$itemNameCode, 'ttlQty'=>$value->QUANTITYSUM);
				}

			}


	return $resultSet;

	}


	/*********************************
	* DESC: Top 5 products of  for Retailers
	* Author: Manidner
	* created: 2016-07-21
	*
	**/

	public function top5ProductsRetailers () {

	$resultSet   = array();

		if(sizeof($this->salesmanList)>0) {  
			$salList = implode(",", $this->salesmanList); 
			$salesmanCondition = ' AND O.salesman_id IN ('.$salList.')';
		} 

		if($this->divisionArr != '' && !empty($this->divisionArr)){
				$divCond = "AND R.division_id IN (".$this->divisionArr.")";
			} 

		$result = $this->_getSelectList('table_order AS O 
			LEFT JOIN table_order_detail AS OD ON OD.order_id = O.order_id
			LEFT JOIN table_item AS I ON I.item_id = OD.item_id
			LEFT JOIN table_salesman AS S ON S.salesman_id = O.salesman_id 
			LEFT JOIN table_retailer AS R ON R.retailer_id = O.retailer_id',
			" I.item_id, I.item_name, I.item_code, O.account_id, SUM(  OD.`acc_quantity` ) AS QUANTITYSUM, O.`date_of_order` ",'',
			" R.new ='' AND I.item_id IS NOT NULL $salesmanCondition  $divCond GROUP BY OD.item_id, YEAR( O.`date_of_order` ) , MONTH( O.`date_of_order` )  HAVING YEAR( O.`date_of_order` ) = YEAR( CURDATE( ) )  AND MONTH( O.`date_of_order` ) = MONTH( CURDATE( ) ) ORDER BY  `QUANTITYSUM` DESC LIMIT 0,5 ");

			if(sizeof($result)>0) {

				foreach ($result as $key => $value) {
					# code...
					$itemNameCode = htmlentities($value->item_name).'('.$value->item_code.')';
					$resultSet[] = array('item_name'=>$itemNameCode, 'ttlQty'=>$value->QUANTITYSUM);
				}

			}


	return $resultSet;

	}



	/*********************************
	* DESC: No Order Reason Wise for Distributors
	* Author: Manidner
	* created: 2016-07-21
	*
	**/

	public function NoOrderDistributors () {

	$resultSet   = array();

		if(sizeof($this->salesmanList)>0) {  
			$salList = implode(",", $this->salesmanList); 
			$salesmanCondition = ' AND OD.salesman_id IN ('.$salList.')';
		} 

		if(isset($_SESSION['userLoginType']) && $_SESSION['userLoginType'] > 1) {


			$salesmanidarray=$this->_objArrayList->SalesmanArrayList();

			$result2=implode(",",$salesmanidarray);

			$salesmancondition="AND OD.salesman_id IN(".$result2.")";

			
			/*print_r($salesmancondition);die;*/
			
		}	 	

		if($this->divisionArr != '' && !empty($this->divisionArr)){
				$divCond = "AND S.division_id IN (".$this->divisionArr.")";
			}

		$result = $this->_getSelectList('table_order AS OD 
        LEFT JOIN table_distributors AS S ON OD.distributor_id=S.distributor_id ',
			" COUNT(OD.order_id) AS no_order,OD.tag_description ",'',
			" OD.order_type='no' AND OD.retailer_id = 0 AND OD.distributor_id > 0 AND OD.tag_id>0  AND date_of_order >= '".$this->_CUR_MONTH_START_DATE."' and date_of_order <= '".$this->_CURDATE."'  $divCond $salesmancondition GROUP BY OD.tag_id ");	
         //AND date_of_order >= '".$this->_CUR_MONTH_START_DATE."' and date_of_order <= '".$this->_CURDATE."' AND S.division_id IN ($divisionArr) 	

			if(sizeof($result)>0) {

				foreach ($result as $key => $value) {
					# code...					
					$resultSet[] = array('noOrderCount'=>$value->no_order, 'discription'=>$value->tag_description);
				}

			}


	return $resultSet;

	}


	/*********************************
	* DESC: No Order Reason Wise for Retailers
	* Author: Manidner
	* created: 2016-07-21
	*
	**/

	public function NoOrderRetailers () {

	$resultSet   = array();

		if(sizeof($this->salesmanList)>0) {  
			$salList = implode(",", $this->salesmanList); 
			$salesmanCondition = ' AND OD.salesman_id IN ('.$salList.')';
		} 

		if(isset($_SESSION['userLoginType']) && $_SESSION['userLoginType'] > 1) {


			$salesmanidarray=$this->_objArrayList->SalesmanArrayList();

			$result2=implode(",",$salesmanidarray);

			$salesmancondition="AND OD.salesman_id IN(".$result2.")";

			
			/*print_r($salesmancondition);die;*/
			
		}	 	

		if($this->divisionArr != '' && !empty($this->divisionArr)){
				$divCond = "AND S.division_id IN (".$this->divisionArr.")";
			}

		$result = $this->_getSelectList('table_order AS OD 
        LEFT JOIN table_retailer AS S ON OD.retailer_id=S.retailer_id ',
			" COUNT(OD.order_id) AS no_order,OD.tag_description ",'',
			" OD.order_type='no' AND OD.retailer_id > 0 AND OD.tag_id>0 AND date_of_order >= '".$this->_CUR_MONTH_START_DATE."' and date_of_order <= '".$this->_CURDATE."'  $divCond $salesmancondition GROUP BY OD.tag_id ");	
         //AND date_of_order >= '".$this->_CUR_MONTH_START_DATE."' and date_of_order <= '".$this->_CURDATE."' AND S.division_id IN ($divisionArr) 	

			if(sizeof($result)>0) {

				foreach ($result as $key => $value) {
					# code...					
					$resultSet[] = array('noOrderCount'=>$value->no_order, 'discription'=>$value->tag_description);
				}

			}


	return $resultSet;

	}


	/*********************************
	* DESC: Order No-Order Retailers And Distributors
	* Author: Maninder
	* created: 2016-07-21
	*
	**/

	public function orderNoOrder () {

	$resultSet = array('Order'=>array(), 'NoOrder'=>array());	
	//$resultSet = array('Order'=>array());	


	if(isset($_SESSION['userLoginType']) && $_SESSION['userLoginType'] > 1) {


			$salesmanidarray=$this->_objArrayList->SalesmanArrayList();

			$result2=implode(",",$salesmanidarray);

			$salesmancondition="AND OD.salesman_id IN(".$result2.")";

			/*print_r($salesmancondition);die;*/
			
		}	

		//for attended					 	
			if($this->divisionArr != '' && !empty($this->divisionArr)){
				$divCond="AND (TR1.division_id IN (".$this->divisionArr.")  OR TR2.division_id IN (".$this->divisionArr.")  OR TD1.division_id IN (".$this->divisionArr.")  OR TD2.division_id IN (".$this->divisionArr."))";
			}
		 	$result11 = $this->_getSelectList2('table_order AS OD 
    LEFT JOIN table_retailer as TR1 ON OD.retailer_id=TR1.retailer_id AND OD.order_type="yes" AND OD.retailer_id > 0 
    LEFT JOIN table_retailer as TR2 ON OD.retailer_id=TR2.retailer_id AND OD.order_type="no" AND OD.retailer_id > 0 
    LEFT JOIN table_distributors as TD1 ON OD.distributor_id=TD1.distributor_id AND OD.order_type="yes" AND OD.distributor_id > 0 
    LEFT JOIN table_distributors as TD2 ON OD.distributor_id=TD2.distributor_id AND OD.order_type="no" AND OD.distributor_id > 0 '," COUNT(TR1.retailer_id) AS retailer_new_order,COUNT(TR2.retailer_id) AS retailer_no_order,COUNT(TD1.distributor_id) AS distributor_new_order,COUNT(TD2.distributor_id) AS distributor_no_order ",''," date_of_order >= '".$this->_CUR_MONTH_START_DATE."' and date_of_order <= '".$this->_CURDATE."' $divCond $salesmancondition");
		 	//date_of_order >= '".$this->_CUR_MONTH_START_DATE."' and date_of_order <= '".$this->_CURDATE."'  AND TR1.division_id IN (".$this->divisionArr.")  AND TR2.division_id IN (".$this->divisionArr.")  AND TD1.division_id IN (".$this->divisionArr.")  AND TD2.division_id IN (".$this->divisionArr.")
		 		 	
		 	if(sizeof($result11)>0) {
		 		$resultSet['Order'][] = $result11[0]->distributor_new_order;
		 		$resultSet['Order'][] = $result11[0]->retailer_new_order;
		 		$resultSet['NoOrder'][] = $result11[0]->distributor_no_order;		 		
		 		$resultSet['NoOrder'][] = $result11[0]->retailer_no_order;
		 	}
		 	
		
		
		//return data			
		return $resultSet;
	}

	/*********************************
	* DESC: Hot and Cold Retailers And Distributors
	* Author: Maninder
	* created: 2016-07-21
	*
	**/

	public function HotColdRetDist () {

	$resultSet = array('Hot'=>array(), 'Cold'=>array());		
	$resultRetailer = array();
	$resultDistributor = array();


		//for attended	

		if($this->divisionArr != '' && !empty($this->divisionArr)){
				$divCond = "AND R.division_id IN (".$this->divisionArr.")";
				$divCond1 = "AND D.division_id IN (".$this->divisionArr.")";
			}	

			/*if(isset($_SESSION['userLoginType']) && $_SESSION['userLoginType'] > 1) {


			$salesmanidarray=$this->_objArrayList->SalesmanArrayList();

			$result2=implode(",",$salesmanidarray);

			$salesmancondition1="AND R.salesman_id IN(".$result2.")";

			$salesmancondition2="AND D.salesman_id IN(".$result2.")";

			/*print_r($salesmancondition);die;
			
		}	 	*/

		if(sizeof($this->salesmanList)>0) {  

			$salList = implode(",", $this->salesmanList); 
			$salesmancondition1 = ' AND R.salesman_id IN ('.$salList.')';
			$salesmancondition2='AND D.salesman_id IN('.$salList.')';
		} 

		 	$resultRetailer = $this->_getSelectList('table_retailer AS R '," COUNT(R.retailer_id) AS ttl, R.display_outlet ",''," R.status!='D' AND R.`display_outlet` IN ('hot', 'cold') $divCond $salesmancondition1 GROUP BY R.display_outlet");
		 	// and last_update_date >= '".$this->_CUR_MONTH_START_DATE."' and last_update_date <= '".$this->_CURDATE."'
		 	$resultDistributor = $this->_getSelectList('table_distributors AS D '," COUNT(D.distributor_id) AS ttl, D.display_outlet ",''," D.status!='D' AND D.`display_outlet` IN ('hot', 'cold') $divCond1 $salesmancondition2 GROUP BY D.display_outlet");

		 	// and last_update_date >= '".$this->_CUR_MONTH_START_DATE."' and last_update_date <= '".$this->_CURDATE."'


	// $resultDistributor = $this->_getSelectList2('table_activity AS A 
 //    LEFT JOIN table_retailer as TR1 ON A.ref_id=TR1.retailer_id AND A.activity_type="5" AND TR1.display_outlet="hot"  
 //    LEFT JOIN table_retailer as TR2 ON A.ref_id=TR2.retailer_id AND A.activity_type="5" AND TR2.display_outlet="cold"  
 //    LEFT JOIN table_distributors as TD1 ON A.ref_id=TD1.distributor_id AND A.activity_type="24" AND TD1.display_outlet="hot"  
 //    LEFT JOIN table_distributors as TD2 ON A.ref_id=TD2.distributor_id AND A.activity_type="24" AND TD2.display_outlet="cold"  '," COUNT(TR1.retailer_id) AS hot_retailer,COUNT(TR2.retailer_id) AS cold_retailer, COUNT(TD1.distributor_id) AS hot_distributor,COUNT(TD2.distributor_id) AS cold_distributor ",''," ");


     		// TR1.division_id IN (".$this->divisionArr.")  AND TR2.division_id IN (".$this->divisionArr.")  AND TD1.division_id IN (".$this->divisionArr.")  AND TD2.division_id IN (".$this->divisionArr.")	 	
		 		 	
		 	// if(sizeof($result11)>0) {
		 	// 	$resultSet['Hot'][] = $result11[0]->hot_distributor;
		 	// 	$resultSet['Hot'][] = $result11[0]->hot_retailer;
		 	// 	$resultSet['Cold'][] = $result11[0]->cold_distributor;		 		
		 	// 	$resultSet['Cold'][] = $result11[0]->cold_retailer;
		 	// } 



		 	foreach ($resultDistributor as $key => $value) {
		 		# code...
		 		if($value->display_outlet == 'cold'){
		 			$resultSet['Cold'][] =  $value->ttl;
		 		}

		 		if($value->display_outlet == 'hot'){
		 			$resultSet['Hot'][] = $value->ttl;
		 		}
		 	}



		 	foreach ($resultRetailer as $key => $value) {
		 		# code...
		 		if($value->display_outlet == 'cold'){
		 			$resultSet['Cold'][] =  $value->ttl;
		 		}

		 		if($value->display_outlet == 'hot'){
		 			$resultSet['Hot'][] = $value->ttl;
		 		}
		 	}


		//return data			
		return $resultSet;
	}


	/*********************************
	* DESC: No Order Reason Wise for Retailers and distributgor
	* Author: Manidner
	* created: 2016-07-21
	*
	**/

	public function NoOrderReasonWise () {

	$resultSet   = array();

		if(sizeof($this->salesmanList)>0) {  

			$salList = implode(",", $this->salesmanList); 
			$salesmanCondition = ' AND OD.salesman_id IN ('.$salList.')';
		} 

		if(isset($_SESSION['userLoginType']) && $_SESSION['userLoginType'] > 1) {


			$salesmanidarray=$this->_objArrayList->SalesmanArrayList();

			$result2=implode(",",$salesmanidarray);

			$salesmancondition="AND OD.salesman_id IN(".$result2.")";

			
			/*print_r($salesmancondition);die;*/
			
		}	 	



		if($this->divisionArr != '' && !empty($this->divisionArr)){
				$divCond = "AND S.division_id IN (".$this->divisionArr.")";				
			}

		$result = $this->_getSelectList('table_order AS OD 
        LEFT JOIN table_salesman AS S ON OD.salesman_id=S.salesman_id ',
			" COUNT(OD.order_id) AS no_order,OD.tag_description ",'',
			" OD.order_type='no' AND OD.tag_id>0 AND date_of_order >= '".$this->_CUR_MONTH_START_DATE."' and date_of_order <= '".$this->_CURDATE."' $divCond $salesmancondition GROUP BY OD.tag_id ");

         //AND date_of_order >= '".$this->_CUR_MONTH_START_DATE."' and date_of_order <= '".$this->_CURDATE."' AND S.division_id IN ($divisionArr) 	

			if(sizeof($result)>0) {

				foreach ($result as $key => $value) {
					# code...					
					$resultSet[] = array('noOrderCount'=>$value->no_order, 'discription'=>$value->tag_description);
				}

			}


	return $resultSet;

	}



	/*********************************
	* DESC: Top 10 product by values
	* Author: Manidner
	* created: 2016-07-22
	*
	**/

	public function top10ProdductbyValues () {

	$resultSet = array('month'=>date('M'), 'ttlAmnt'=>array(), 'data'=>array());

		if(sizeof($this->salesmanList)>0) {  
			$salList = implode(",", $this->salesmanList); 
			$salesmanCondition = ' AND tor.salesman_id IN ('.$salList.')';
		} 

		$result = $this->_getSelectList('table_order as tor left join table_order_detail as tord on tord.order_id = tor.order_id left join table_item as i on i.item_id =  tord.item_id ',
			" i.item_id, i.item_name,i.item_code, tor.account_id,SUM(acc_total) AS AMOUNTSUM,tord.price ",'',
			" tor.date_of_order >= '".$this->_CUR_MONTH_START_DATE."' and tor.date_of_order <= '".$this->_CURDATE."'	 GROUP BY tord.item_id ORDER BY  `AMOUNTSUM` DESC LIMIT 0,10 ");

         //tor.account_id ='$account_id' and tor.date_of_order >= '".$this->_CUR_MONTH_START_DATE."' and tor.date_of_order <= '".$this->_CURDATE."'		


			if(sizeof($result)>0) {

				foreach ($result as $key => $value) {
										
					$resultSet['ttlAmnt'][] = $value->AMOUNTSUM;
				    $resultSet['data'][] 	= $value;
				}

			}


	return $resultSet;

	}

	/*********************************
	* DESC: Total Assigned Retailers and Distributors
	* Author: Manidner
	* created: 2016-08-12
	* Updated: 2016-09-23 by Maninder
	**/

	public function assignedSummary () {

	$resultSet = array('retailers'=>'0', 'distributors'=>'0');

	 $yearFrom = date('Y',strtotime($this->_CUR_MONTH_START_DATE));
	 $yearTo = date('Y',strtotime($this->_CURDATE));
	 $monthFrom = date('n',strtotime($this->_CUR_MONTH_START_DATE));
	 $monthTo = date('n',strtotime($this->_CURDATE));
	 $dayFrom = date('j',strtotime($this->_CUR_MONTH_START_DATE));
	 $dayTo = date('j',strtotime($this->_CURDATE));
	 $allMonths=0;
	 $allMonthsFromYear=0;
	 $allMonthsToYear=0;
	 if($yearFrom == $yearTo){
	 		 	
 		while($monthFrom <= $monthTo){
 			$monthsIn[]=$monthFrom;
 		  $monthFrom++; 		  	 		
 	     }
	 	$allMonths=implode(',', $monthsIn);
	 }else if($yearFrom < $yearTo){
	 		 	
 		while($monthFrom <= 12){
 			$monthsInFromYear[]=$monthFrom;
 		  $monthFrom++; 		  		 		
 	     }

 	     $mn=1;
 	     while($mn <= $monthTo){
 			$monthsInToYear[]=$mn;
 		  $mn++; 		  		 		
 	     }

	 	$allMonthsFromYear=implode(',', $monthsInFromYear);
	 	$allMonthsToYear=implode(',', $monthsInToYear);
	 }else{
	 	$allMonths=0;
	 	$allMonthsFromYear=0;
	 	$allMonthsToYear=0;
	 }
       $allMon = $allMonthsFromYear.','.$allMonthsToYear;


       if($this->divisionArr != '' && !empty($this->divisionArr)){
				$divCond = "AND (RET.division_id IN (".$this->divisionArr.")  OR D.division_id IN (".$this->divisionArr."))";				
			}

			if(sizeof($this->salesmanList)>0) {  
			$salList = implode(",", $this->salesmanList); 
			$salesmanCondition = ' AND RS.salesman_id IN ('.$salList.')';
		} 


    
	//find total assigned retailers
	 $result =$this->_getSelectList('`table_route_scheduled` AS RS 
			LEFT JOIN table_route_schedule_details AS RSD ON RSD.`route_schedule_id` = RS.`route_schedule_id`
			LEFT JOIN table_route_retailer AS RR ON RR.route_id = RSD.route_id AND RR.status IN ("R", "D")
			LEFT JOIN table_retailer AS RET ON RET.retailer_id = RR.retailer_id AND RR.status = "R" AND DATE_FORMAT(RET.start_date, "%Y-%m-%d") < DATE_FORMAT(CONCAT(RS.year,"-",RS.month,"-",RSD.assign_day),"%Y-%m-%d") AND RET.status = "A"
			LEFT JOIN table_distributors AS D ON D.distributor_id = RR.retailer_id AND RR.status = "D" AND DATE_FORMAT(D.start_date, "%Y-%m-%d") < DATE_FORMAT(CONCAT(RS.year,"-",RS.month,"-",RSD.assign_day),"%Y-%m-%d") AND D.status = "A"
			LEFT JOIN table_route AS R ON R.route_id = RSD.route_id',
			"COUNT(RR.status) AS ttlRoutePersons, COUNT(RET.retailer_id) AS ttlRouteRetailers, COUNT(D.distributor_id) AS ttlRouteDistributors",'',
			"CASE WHEN '".$yearFrom."' = '".$yearTo."' THEN RS.month IN($allMonths) AND RS.year BETWEEN '".$yearFrom."' AND '".$yearTo."' AND RSD.assign_day BETWEEN '".$dayFrom."' AND '".$dayTo."' 
			ELSE 
				CASE 
			        WHEN '".$yearFrom."' < '".$yearTo."' THEN RS.month IN($allMon) AND RS.year BETWEEN '".$yearFrom."' AND '".$yearTo."' AND RSD.assign_day BETWEEN '".$dayFrom."' AND '".$dayTo."' 					
			    END 
			    
			END $divCond $salesmanCondition  ORDER BY RSD.assign_day ASC");	

		//AND RET.division_id IN (".$this->divisionArr.")  AND D.division_id IN (".$this->divisionArr.")

		if(sizeof($result)>0) {

			foreach ($result as $key => $value) {
				
				$resultSet['retailers'] = $value->ttlRouteRetailers;
				$resultSet['distributors'] = $value->ttlRouteDistributors;				
			}
		}

		return $resultSet;

	}

	/*********************************
	* DESC: Distributor Complaint
	* Created: 24May2017
	*
	**********************************/

	public function distributorComplaint() {

		$resultSet = array('1'=>array('t'=>0,'p'=>0,'ip'=>0,'r'=>0, 'c'=>0), '2'=>array('t'=>0,'p'=>0,'ip'=>0,'r'=>0, 'c'=>0), '3'=>array('t'=>0,'p'=>0,'ip'=>0,'r'=>0, 'c'=>0), '4'=>array('t'=>0,'p'=>0,'ip'=>0,'r'=>0, 'c'=>0), '5'=>array('t'=>0,'p'=>0,'ip'=>0,'r'=>0, 'c'=>0) , '6'=>array('t'=>0,'p'=>0,'ip'=>0,'r'=>0, 'c'=>0), '7'=>array('t'=>0,'p'=>0,'ip'=>0,'r'=>0, 'c'=>0), '8'=>array('t'=>0,'p'=>0,'ip'=>0,'r'=>0, 'c'=>0), '9'=>array('t'=>0,'p'=>0,'ip'=>0,'r'=>0, 'c'=>0), '10'=>array('t'=>0,'p'=>0,'ip'=>0,'r'=>0, 'c'=>0), '11'=>array('t'=>0,'p'=>0,'ip'=>0,'r'=>0, 'c'=>0), '12'=>array('t'=>0,'p'=>0,'ip'=>0,'r'=>0, 'c'=>0)); 

	    $condi = " AND c.distributor_id!=' ' ";

	    $year  = " AND  date_format(c.created_date, '%Y') = '".date('Y')."'"; 
	    
	    $orderBy = " ORDER BY c.complaint_id desc";
	    
	    // Fetch Monthly Distributor Total Complaint In Current Year 
	    $ttlCompltRec = $this->_getSelectList('table_complaint AS c ',
	        ' COUNT(*) AS ttlComplaint, MONTH( c.created_date ) AS month ',''," $condi $year GROUP BY date_format( c.created_date, '%m' ) $orderBy");

	    if(sizeof($ttlCompltRec)){

		    $auRec = $this->_getSelectList('table_complaint AS c ',
		        '  c.created_date AS created, c.complaint_id AS complaint_id, COUNT(*) AS ttlcomplaintstatuswise,  c.complain_status AS complain_status, MONTH( c.created_date ) AS month',''," $condi $year group by c.complain_status,   date_format( c.created_date, '%m' ) $orderBy");
			
			//echo count($auRec);
		    // echo "<pre>";
		    // print_r($auRec);


		    $ttlCompltResultSet = array(); 

			$consolidateResultSet = array();

			foreach ($ttlCompltRec as $k => $v) {
				
				$ttlCompltResultSet[$v->month]['t'] = $v->ttlComplaint;
			}

		    foreach ($auRec as $key => $value) {
		        # code...
		        $consolidateResultSet[$value->month][$value->complain_status] = $value->ttlcomplaintstatuswise;
		    }
		
			//echo count($auRec);
		    // echo "<pre>";
		    // print_r($consolidateResultSet);
		    // exit;


			if(is_array($consolidateResultSet)){
		        
		        // for($i=0;$i<count($consolidateResultSet);$i++){}

		        foreach ($resultSet as $key => $value) {

		            if(isset($consolidateResultSet[$key])) {

		            	// if(isset($consolidateResultSet[$key])) {
	              //       	$resultSet[$key]['t']  = $consolidateResultSet[$key]['P']+ $consolidateResultSet[$key]['I']+ $consolidateResultSet[$key]['R']+ $consolidateResultSet[$key]['C'];
	              //   	}

		            	if(isset($ttlCompltResultSet[$key]['t'])) {
				            $resultSet[$key]['t']  = $ttlCompltResultSet[$key]['t'];
				        }

		                if(isset($consolidateResultSet[$key]['P'])) {
		                    $resultSet[$key]['p']  = $consolidateResultSet[$key]['P'];
		                }

		                if(isset($consolidateResultSet[$key]['I'])) {
		                    $resultSet[$key]['ip']  = $consolidateResultSet[$key]['I'];
		                }

		                if(isset($consolidateResultSet[$key]['R'])) {
		                    $resultSet[$key]['r']  = $consolidateResultSet[$key]['R'];
		                }
		                if(isset($consolidateResultSet[$key]['C'])) {
		                    $resultSet[$key]['c']  = $consolidateResultSet[$key]['C'];
		                }
		                
		                // $resultSet[$key]['ip'] = $consolidateResultSet[$key]['I'];
		                // $resultSet[$key]['r']  = $consolidateResultSet[$key]['R'];
		                // $resultSet[$key]['c']  = $consolidateResultSet[$key]['C'];


		            } else{
		                continue;
		            }


		        }        // end of foreach loop
	         
	        } 
       
    	}   
  
    // echo "<pre>";
    // print_r($resultSet);
    // exit;

		return $resultSet;

	}

	/*********************************
	* DESC: Dealer Complaint
	* Created: 25May2017
	*
	**********************************/

	public function dealersComplaint() {

		$resultSet = array('1'=>array('t'=>0,'p'=>0,'ip'=>0,'r'=>0, 'c'=>0), '2'=>array('t'=>0,'p'=>0,'ip'=>0,'r'=>0, 'c'=>0), '3'=>array('t'=>0,'p'=>0,'ip'=>0,'r'=>0, 'c'=>0), '4'=>array('t'=>0,'p'=>0,'ip'=>0,'r'=>0, 'c'=>0), '5'=>array('t'=>0,'p'=>0,'ip'=>0,'r'=>0, 'c'=>0) , '6'=>array('t'=>0,'p'=>0,'ip'=>0,'r'=>0, 'c'=>0), '7'=>array('t'=>0,'p'=>0,'ip'=>0,'r'=>0, 'c'=>0), '8'=>array('t'=>0,'p'=>0,'ip'=>0,'r'=>0, 'c'=>0), '9'=>array('t'=>0,'p'=>0,'ip'=>0,'r'=>0, 'c'=>0), '10'=>array('t'=>0,'p'=>0,'ip'=>0,'r'=>0, 'c'=>0), '11'=>array('t'=>0,'p'=>0,'ip'=>0,'r'=>0, 'c'=>0), '12'=>array('t'=>0,'p'=>0,'ip'=>0,'r'=>0, 'c'=>0)); 

	    $condi = " AND c.retailer_id!='' ";

	    $year  = " AND  date_format(c.created_date, '%Y') = '".date('Y')."'"; 
	    
	    $orderBy = " ORDER BY c.complaint_id desc";

// Fetch Monthly Dealer Total Complaint In Current Year 
	    $ttlCompltRec = $this->_getSelectList('table_complaint AS c ',
	        ' COUNT(*) AS ttlComplaint, MONTH( c.created_date ) AS month ',''," $condi $year GROUP BY date_format( c.created_date, '%m' ) $orderBy");

	    if(sizeof($ttlCompltRec)){
	    
			$auRec = $this->_getSelectList('table_complaint AS c ',
		        ' c.complaint_id AS complaint_id, COUNT(*) AS ttlcomplaintstatuswise,  c.complain_status AS complain_status, MONTH( c.created_date ) AS month',''," $condi $year group by c.complain_status,   date_format( c.created_date, '%m' ) $orderBy");
		//echo count($auRec);
		    // echo "<pre>";
		    // print_r($auRec);


		    $ttlCompltResultSet = array(); 

			$consolidateResultSet = array();

			foreach ($ttlCompltRec as $k => $v) {
			    $ttlCompltResultSet[$v->month]['t'] = $v->ttlComplaint;
			}

		    foreach ($auRec as $key => $value) {
		        # code...
		        $consolidateResultSet[$value->month][$value->complain_status] = $value->ttlcomplaintstatuswise;
		    }
		//echo count($auRec);
		    // echo "<pre>";
		    // print_r($consolidateResultSet);
		    // exit;




		    if(is_array($consolidateResultSet)){
		        
		        // for($i=0;$i<count($consolidateResultSet);$i++){}

		        foreach ($resultSet as $key => $value) {

		            if(isset($consolidateResultSet[$key])) {

		            	// if(isset($consolidateResultSet[$key])) {
	              //       	$resultSet[$key]['t']  = $consolidateResultSet[$key]['P']+ $consolidateResultSet[$key]['I']+ $consolidateResultSet[$key]['R']+ $consolidateResultSet[$key]['C'];
	              //   	}

		            	if(isset($ttlCompltResultSet[$key]['t'])) {
			                    $resultSet[$key]['t']  = $ttlCompltResultSet[$key]['t'];
			            }

		                if(isset($consolidateResultSet[$key]['P'])) {
		                    $resultSet[$key]['p']  = $consolidateResultSet[$key]['P'];
		                }

		                if(isset($consolidateResultSet[$key]['I'])) {
		                    $resultSet[$key]['ip']  = $consolidateResultSet[$key]['I'];
		                }

		                if(isset($consolidateResultSet[$key]['R'])) {
		                    $resultSet[$key]['r']  = $consolidateResultSet[$key]['R'];
		                }
		                if(isset($consolidateResultSet[$key]['C'])) {
		                    $resultSet[$key]['c']  = $consolidateResultSet[$key]['C'];
		                }
		                
		                // $resultSet[$key]['ip'] = $consolidateResultSet[$key]['I'];
		                // $resultSet[$key]['r']  = $consolidateResultSet[$key]['R'];
		                // $resultSet[$key]['c']  = $consolidateResultSet[$key]['C'];


		            } else{
		                continue;
		            }


		        }        // end of foreach loop
	         
	    	}  

    	} 
  
    // echo "<pre>";
    // print_r($resultSet);
    // exit;

		return $resultSet;

	}


	/*********************************
	* DESC: Call Center Complaint
	* Created: 26May2017
	*
	**********************************/

	public function callCenterComplaint() {

	$resultSet = array('1'=>array('t'=>0, 'p'=>0, 'ip'=>0, 'r'=>0, 'c'=>0), '2'=>array('t'=>0, 'p'=>0, 'ip'=>0, 'r'=>0, 'c'=>0), '3'=>array('t'=>0, 'p'=>0, 'ip'=>0, 'r'=>0, 'c'=>0), '4'=>array('t'=>0, 'p'=>0, 'ip'=>0, 'r'=>0, 'c'=>0), '5'=>array('t'=>0, 'p'=>0, 'ip'=>0, 'r'=>0, 'c'=>0) , '6'=>array('t'=>0, 'p'=>0, 'ip'=>0, 'r'=>0, 'c'=>0), '7'=>array('t'=>0, 'p'=>0, 'ip'=>0, 'r'=>0, 'c'=>0), '8'=>array('t'=>0, 'p'=>0, 'ip'=>0, 'r'=>0, 'c'=>0), '9'=>array('t'=>0, 'p'=>0, 'ip'=>0, 'r'=>0, 'c'=>0), '10'=>array('t'=>0, 'p'=>0, 'ip'=>0, 'r'=>0, 'c'=>0), '11'=>array('t'=>0, 'p'=>0, 'ip'=>0, 'r'=>0, 'c'=>0), '12'=>array('t'=>0, 'p'=>0, 'ip'=>0, 'r'=>0, 'c'=>0)); 

	    $condi = " AND call_center_id > 0 ";

	    $year  = " AND  date_format(c.created_date, '%Y') = '".date('Y')."'"; 
	    
	    $orderBy = " ORDER BY c.complaint_id desc";

	    $ttlCompltRec = $this->_getSelectList('table_complaint AS c ',
	        ' COUNT(*) AS ttlComplaint, MONTH( c.created_date ) AS month ',''," $condi $year GROUP BY date_format( c.created_date, '%m' ) $orderBy");

	    if(sizeof($ttlCompltRec)){
	    
			$auRec = $this->_getSelectList('table_complaint AS c ',
		        ' c.complaint_id AS complaint_id, COUNT(*) AS ttlcomplaintstatuswise,  c.complain_status AS complain_status, MONTH( c.created_date ) AS month',''," $condi $year GROUP BY c.complain_status,   date_format( c.created_date, '%m' ) $orderBy");
			// echo count($auRec);
		 //    echo "<pre>";
		 //    print_r($auRec);


			$ttlCompltResultSet = array(); 

			$consolidateResultSet = array();

		    foreach ($ttlCompltRec as $k => $v) {
		    	$ttlCompltResultSet[$v->month]['t'] = $v->ttlComplaint;
		    }

		    //echo count($ttlCompltRec);
		    // echo "<pre>";
		    // print_r($ttlCompltResultSet);
		    // exit;
		    

		    foreach ($auRec as $key => $value) {
		        # code...
		        $consolidateResultSet[$value->month][$value->complain_status] = $value->ttlcomplaintstatuswise;
		    }
		
			//echo count($auRec);
		    // echo "<pre>";
		    // print_r($consolidateResultSet);
		    // exit;


			if(is_array($consolidateResultSet)){
		        
		        
		        foreach ($resultSet as $key => $value) {

		        	if(isset($consolidateResultSet[$key])) {

		            	if(isset($ttlCompltResultSet[$key]['t'])) {
		                    $resultSet[$key]['t']  = $ttlCompltResultSet[$key]['t'];
		                }

		            	// if(isset($consolidateResultSet[$key])) {
	              //       	$resultSet[$key]['t']  = $consolidateResultSet[$key]['P']+ $consolidateResultSet[$key]['I']+ $consolidateResultSet[$key]['R']+ $consolidateResultSet[$key]['C'];
	              //   	}

		                if(isset($consolidateResultSet[$key]['P'])) {
		                    $resultSet[$key]['p']  = $consolidateResultSet[$key]['P'];
		                }

		                if(isset($consolidateResultSet[$key]['I'])) {
		                    $resultSet[$key]['ip']  = $consolidateResultSet[$key]['I'];
		                }

		                if(isset($consolidateResultSet[$key]['R'])) {
		                    $resultSet[$key]['r']  = $consolidateResultSet[$key]['R'];
		                }
		                if(isset($consolidateResultSet[$key]['C'])) {
		                    $resultSet[$key]['c']  = $consolidateResultSet[$key]['C'];
		                }
		                
		                


		            } else{
		                continue;
		            }


		        }        // end of foreach loop
	         
	    	}

		}
  
    // echo "<pre>";
    // print_r($resultSet);
    // exit;

		return $resultSet;

	} // End of function


	/*********************************
	* DESC: Call Center Complaint Analytic
	* Created On: 9Jun2017
	* Created By: Pooja
	**********************************/

	public function complaintAnalytic() {

		$resultSet = array('T'=>0, 'P'=>0, 'I'=>0, 'R'=>0, 'C'=>0); 

	    $condi = " AND call_center_id > 0 ";

	    $orderBy = " ORDER BY c.complaint_id desc";

// Fetch Total Complaint
	    $auRec = $this->_getSelectList('table_complaint AS c', 'COUNT(*) AS ttlComplaint', '',"$condi  AND  c.created_date >= '".$this->_CUR_MONTH_START_DATE."' AND c.created_date <='".$this->_CURDATE."'");
	    
	    if(sizeof($auRec)> 0){

	    	$ttlComplaint = $auRec[0]->ttlComplaint;

// Fetch Total Complaint, Total Pending Complaint, Total In-Process Complaint, Total Rejected Complaint, Total Completed Complaint
			$analyticResult = $this->_getSelectList('table_complaint AS c',
		        ' c.complaint_id AS complaint_id, COUNT(*) AS ttlcomplaintstatuswise,  c.complain_status AS complain_status',''," $condi  AND  c.created_date >= '".$this->_CUR_MONTH_START_DATE."' AND c.created_date <='".$this->_CURDATE."'  GROUP BY c.complain_status $orderBy");
			
			// echo count($analyticResult);
		    // echo "<pre>";
		    // print_r($analyticResult);

		    // exit;

			if(sizeof($analyticResult)> 0 ) {
			    foreach ($analyticResult as $key => $value) {
			       
			       	$complaintSet[$value->complain_status] = $value->ttlcomplaintstatuswise;
			    }
			}

			$t=0; 
			$p=0;
			$i=0;
			$r=0;
			$c=0;
			if(isset($ttlComplaint) && !empty($ttlComplaint))
			{
				$t = $ttlComplaint; 
			}	

			if(isset($complaintSet['P']) && !empty($complaintSet['P'])){
				$p = $complaintSet['P'];
			}
			if(isset($complaintSet['I']) && !empty($complaintSet['I'])){
				$i = $complaintSet['I'];
			}
			if(isset($complaintSet['R']) && !empty($complaintSet['R'])){
				$r = $complaintSet['R'];
			}
			if(isset($complaintSet['C']) && !empty($complaintSet['C'])){
				$c = $complaintSet['C'];
			}


			
				foreach ($resultSet as $key => $value) {
					$resultSet['T'] = $t;
					$resultSet['P'] = $p;
					$resultSet['I'] = $i;
					$resultSet['R'] = $r;
					$resultSet['C'] = $c;
				}
			

			
			// echo "<pre>";
			// print_r($resultSet);
			// exit;

			
		}

		return $resultSet;

	} // End of function



     public function currentmonthtat () {

	$resultSet = array('month'=>date('M'), 'data'=>array());

	$condi = " AND call_center_id > 0 ";
	$first_day_this_month = date('Y-m-01');
    $last_day_this_month  = date('Y-m-t');
    $where="DATE_FORMAT( c.created_date, '%Y-%m-%d' ) >= '".$first_day_this_month."' AND DATE_FORMAT( c.created_date, '%Y-%m-%d' ) <= '".$last_day_this_month."' and c.call_center_id > 0 and c.complain_status='I' ";
    $where1="DATE_FORMAT( c.created_date, '%Y-%m-%d' ) >= '".$first_day_this_month."' AND DATE_FORMAT( c.created_date, '%Y-%m-%d' ) <= '".$last_day_this_month."' and c.call_center_id > 0  ";


    $tat_sum = $this->_getSelectList2('table_complaint as c','SUM(cctat1) as sumtat1,SUM(cctat2) as sumtat2','',$where,'');
    $total_complaints = $this->_getSelectList2('table_complaint as c','count(*) as total_complaints','',$where1,'');



	
	$avgtat1=$tat_sum[0]->sumtat1/$total_complaints[0]->total_complaints;
	$avgtat2=$tat_sum[0]->sumtat2/$total_complaints[0]->total_complaints;
				//$tat2_complaints = $_objAdmin->_getSelectList2('table_complaint','SUM(cctat2) as sum_tat2','',' call_center_id > 0 and complain_status="I"');

	

	

	$resultSet=array('tat1'=> $avgtat1, 'tat2'=>$avgtat2,'month'=>date('M'));
				
		//print_r($resultSet);
		return $resultSet;

	} // End of function

	



} // End of GraphClass

?>