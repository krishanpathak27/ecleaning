<?php
class Item extends Db_Action{
	function __construct($pagerFunName='frmsubmit'){

		parent::__construct();
		$this->autMsg="";
		$this->indian_all_states  = array_map('strtolower',array (
				 'AP' => 'Andhra Pradesh',
				 'AR' => 'Arunachal Pradesh',
				 'AS' => 'Assam',
				 'BR' => 'Bihar',
				 'CT' => 'Chhattisgarh',
				 'GA' => 'Goa',
				 'GJ' => 'Gujarat',
				 'HR' => 'Haryana',
				 'HP' => 'Himachal Pradesh',
				 'JK' => 'Jammu & Kashmir',
				 'JH' => 'Jharkhand',
				 'KA' => 'Karnataka',
				 'KL' => 'Kerala',
				 'MP' => 'Madhya Pradesh',
				 'MH' => 'Maharashtra',
				 'MN' => 'Manipur',
				 'ML' => 'Meghalaya',
				 'MZ' => 'Mizoram',
				 'NL' => 'Nagaland',
				 'OR' => 'Odisha',
				 'PB' => 'Punjab',
				 'RJ' => 'Rajasthan',
				 'SK' => 'Sikkim',
				 'TN' => 'Tamil Nadu',
				 'TR' => 'Tripura',
				 'UK' => 'Uttarakhand',
				 'UP' => 'Uttar Pradesh',
				 'WB' => 'West Bengal',
				 'AN' => 'Andaman & Nicobar',
				 'CH' => 'Chandigarh',
				 'DN' => 'Dadra and Nagar Haveli',
				 'DD' => 'Daman & Diu',
				 'DL' => 'Delhi',
				 'LD' => 'Lakshadweep',
				 'PY' => 'Puducherry',
				 'TL' => 'Telangana',
				 'SA' => 'Seemandhra',
				 'AD' => 'Andorra',
				'AE' => 'Utd.arab Emir.',
				 'AF' => 'Afghanistan',
				'AG' => 'Antigua/Barbuda',
				'AI' => 'Anguilla',
				'AL' => 'Albania',
				'AM' => 'Armenia',
				'ANTLS' => 'Dutch Antilles',
				'AO' => 'Angola',
				'AQ' => 'Antarctica',
				'ARG' => 'Argentina',
				'ASA' => 'Samoa, America',
				'AT' => 'Austria',
				'AU' => 'Australia',
				'AW' => 'Aruba',
				'AZ' => 'Azerbaijan',
				'BA' => 'Bosnia-Herz.',
				'BB' => 'Barbados',
				'BD' => 'Bangladesh',
				'BE' => 'Belgium',
				'BF' => 'Burkina Faso',
				'BG' => 'Bulgaria',
				'BH' => 'Bahrain',
				'BI' => 'Burundi',
				'BJ' => 'Benin',
				'BL' => 'Blue',
				'BM' => 'Bermuda',
				'BN' => 'Brunei Daruss.',
				'BO' => 'Bolivia',
				'BRZ' => 'Brazil',
				'BS' => 'Bahamas',
				'BT' => 'Bhutan',
				'BV' => 'Bouvet Islands',
				'BW' => 'Botswana',
				'BY' => 'Belarus',
				'BZ' => 'Belize',
				'CA' => 'Canada',
				'CC' => 'Coconut Islands',
				'CD' => 'Dem. Rep. Congo',
				'CF' => 'CAR',
				'CG' => 'Rep.of Congo',
				'CHLND' => 'Switzerland',
				'CI' => "Cote d'Ivoire",
				'CK' => 'Cook Islands',
				'CL' => 'Chile',
				'CM' => 'Cameroon',
				'CN' => 'China',
				'CO' => 'Colombia',
				'CR' => 'Costa Rica',
				'CS' => 'Serbia/Monten.',
				'CU' => 'Cuba',
				'CV' => 'Cape Verde',
				'CX' => 'Christmas Islnd',
				'CY' => 'Cyprus',
				'CZ' => 'Czech Republic',
				'DE' => 'Germany',
				'DJ' => 'Djibouti',
				'DK' => 'Denmark',
				'DM' => 'Dominica',
				'DO' => 'Dominican Rep.',
				'DZ' => 'Algeria',
				'EC' => 'Ecuador',
				'EE' => 'Estonia',
				'EG' => 'Egypt',
				'EH' => 'West Sahara',
				'ER' => 'Eritrea',
				'ES' => 'Spain',
				'ET' => 'Ethiopia',
				'EU' => 'European Union',
				'FI' => 'Finland',
				'FJ' => 'Fiji',
				'FK' => 'Falkland Islnds',
				'FM' => 'Micronesia',
				'FO' => 'Faroe Islands',
				'FR' => 'France',
				'GAB' => 'Gabon',
				'GB' => 'United Kingdom',
				'GD' => 'Grenada',
				'GE' => 'Georgia',
				'GF' => 'French Guayana',
				'GH' => 'Ghana',
				'GI' => 'Gibraltar',
				'GL' => 'Greenland',
				'GM' => 'Gambia',
				'GN' => 'Guinea',
				'GP' => 'Guadeloupe',
				'GQ' => 'Equatorial Guin',
				'GR' => 'Greece',
				'GS' => 'S. Sandwich Ins',
				'GT' => 'Guatemala',
				'GU' => 'Guam',
				'GW' => 'Guinea-Bissau',
				'GY' => 'Guyana',
				'HK' => 'Hong Kong',
				'HM' => 'Heard/McDon.Isl',
				'HN' => 'Honduras',
				'HRCR' => 'Croatia',
				'HT' => 'Haiti',
				'HU' => 'Hungary',
				'ID' => 'Indonesia',
				'IE' => 'Ireland',
				'IL' => 'Israel',
				'IN' => 'India',
				'IO' => 'Brit.Ind.Oc.Ter',
				'IQ' => 'Iraq',
				'IR' => 'Iran',
				'IS' => 'Iceland',
				'IT' => 'Italy',
				'IV' => 'Ivory Coast',
				'JM' => 'Jamaica',
				'JO' => 'Jordan',
				'JP' => 'Japan',
				'KE' => 'Kenya',
				'KG' => 'Kyrgyzstan',
				'KH' => 'Cambodia',
				'KI' => 'Kiribati',
				'KM' => 'Comoros',
				'KN' => 'St Kitts&Nevis',
				'KP' => 'North Korea',
				'KR' => 'South Korea',
				'KW' => 'Kuwait',
				'KY' => 'Cayman Islands',
				'KZ' => 'Kazakhstan',
				'LA' => 'Laos',
				'LB' => 'Lebanon',
				'LC' => 'St. Lucia',
				'LI' => 'Liechtenstein',
				'LK' => 'Sri Lanka',
				'LR' => 'Liberia',
				'LS' => 'Lesotho',
				'LT' => 'Lithuania',
				'LU' => 'Luxembourg',
				'LV' => 'Latvia',
				'LY' => 'Libya',
				'MA' => 'Morocco',
				'MC' => 'Monaco',
				'MD' => 'Moldova',
				'MG' => 'Madagascar',
				'MHIS' => 'Marshall Islnds',
				'MK' => 'Macedonia',
				'MLI' => 'Mali',
				'MM' => 'Burma',
				'MNGL' => 'Mongolia',
				'MO' => 'Macau',
				'MPIS' => 'N.Mariana Islnd',
				'MQ' => 'Martinique',
				'MR' => 'Mauretania',
				'MS' => 'Montserrat',
				'MT' => 'Malta',
				'MU' => 'Mauritius',
				'MV' => 'Maldives',
				'MW' => 'Malawi',
				'MX' => 'Mexico',
				'MY' => 'Malaysia',
				'MZBI' => 'Mozambique',
				'NA' => 'Namibia',
				'NC' => 'New Caledonia',
				'NE' => 'Niger',
				'NF' => 'Norfolk Islands',
				'NG' => 'Nigeria',
				'NI' => 'Nicaragua',
				'NLLNS' => 'Netherlands',
				'NO' => 'Norway',
				'NP' => 'Nepal',
				'NR' => 'Nauru',
				'NT' => 'NATO',
				'NU' => 'Niue',
				'NZ' => 'New Zealand',
				'OM' => 'Oman',
				'ORSS' => 'Orange',
				'PA' => 'Panama',
				'PE' => 'Peru',
				'PF' => 'Frenc.Polynesia',
				'PG' => 'Pap. New Guinea',
				'PH' => 'Philippines',
				'PK' => 'Pakistan',
				'PL' => 'Poland',
				'PM' => 'St.Pier,Miquel.',
				'PN' => 'Pitcairn Islnds',
				'PR' => 'Puerto Rico',
				'PS' => 'Palestine',
				'PT' => 'Portugal',
				'PW' => 'Palau',
				'PYGUY' => 'Paraguay',
				'QA' => 'Qatar',
				'RE' => 'Reunion',
				'RO' => 'Romania',
				'RU' => 'Russian Fed.',
				'RW' => 'Rwanda',
				'SABA' => 'Saudi Arabia',
				'SB' => 'Solomon Islands',
				'SC' => 'Seychelles',
				'SD' => 'Sudan',
				'SE' => 'Sweden',
				'SG' => 'Singapore',
				'SH' => 'Saint Helena',
				'SI' => 'Slovenia',
				'SJ' => 'Svalbard',
				'SKKIA' => 'Slovakia',
				'SL' => 'Sierra Leone',
				'SM' => 'San Marino',
				'SN' => 'Senegal',
				'SO' => 'Somalia',
				'SR' => 'Suriname',
				'ST' => 'S.Tome,Principe',
				'SV' => 'El Salvador',
				'SY' => 'Syria',
				'SZ' => 'Swaziland',
				'TC' => 'Turksh Caicosin',
				'TD' => 'Chad',
				'TF' => 'French S.Territ',
				'TG' => 'Togo',
				'TH' => 'Thailand',
				'TJ' => 'Tajikistan',
				'TK' => 'Tokelau Islands',
				'TLMOR' => 'East Timor',
				'TM' => 'Turkmenistan',
				'TNSIA' => 'Tunisia',
				'TO' => 'Tonga',
				'TP' => 'East Timor',
				'TREY' => 'Turkey',
				'TT' => 'Trinidad,Tobago',
				'TV' => 'Tuvalu',
				'TW' => 'Taiwan',
				'TZ' => 'Tanzania',
				'UA' => 'Ukraine',
				'UG' => 'Uganda',
				'UM' => 'Minor Outl.Isl.',
				'UN' => 'United Nations',
				'US' => 'Usa',
				'UY' => 'Uruguay',
				'UZ' => 'Uzbekistan',
				'VA' => 'Vatican City',
				'VC' => 'St. Vincent',
				'VE' => 'Venezuela',
				'VG' => 'Brit.Virgin Is.',
				'VI' => 'Amer.Virgin Is.',
				'VN' => 'Vietnam',
				'VU' => 'Vanuatu',
				'WF' => 'Wallis,Futuna',
				'WS' => 'Samoa',
				'YE' => 'Yemen',
				'YT' => 'Mayotte',
				'ZA' => 'South Africa',
				'ZM' => 'Zambia',
				'ZW' => 'Zimbabwe'
				));
	}
	
	
	

	
	
	
	
	
	
/************************************* Start Upload Category ***************************************/


function uploadCategoryFile(){
		$fields = array('Category Name*','Category Code');	
		$file = fopen($_FILES['fileToUpload']['tmp_name'],"r");
		$strErr="";
		$row=0;		
		while(! feof($file)){		  
			  $arrFile=fgetcsv($file);	
			  $flag=true;		  
			  if($row>0 && is_array($arrFile)) {
			    
			if(empty($arrFile[0]) || $arrFile[0]=="Category Name*"){							
							$strErr .= "Error in Row".$row." ,".$fields[0]." shouldn't be empty \n";
							$str_err_array =array($strErr);							
							$flag =false;
							} 
				 
				 
			if($flag){
				$condi=	" account_id=".$_SESSION['accountId']."  AND  LOWER(category_name)='".mysql_real_escape_string(strtolower($arrFile[0]))."'";
				$aCatRec=$this->_getSelectList('table_category','*','',$condi);
				if(is_array($aCatRec)) {					
				$strErr .=" Error in Row".$row." ,".$arrFile[0]." ," .$arrFile[1]." ,already exists in the system  \n";
				$str_err_array=array($strErr);						
				$flag=false;
				}
			}	
				
							  
				if($flag){	
					$data = array();					
					$data['account_id']=$this->clean($_SESSION['accountId']);		
					$data['category_name']=mysql_real_escape_string( $this->clean($arrFile[0]));
					$data['category_code']=mysql_real_escape_string( $this->clean($arrFile[1]));
					$data['last_update_date']=date('Y-m-d');
					$data['last_update_status']='New';
					$data['status']= 'A';
					$id=$this->_dbInsert($data,'table_category');				
				}			  
		  }
		  $row++;	   
		}	
		//print_r($str_err_array);
		fclose($file);	  
	   if($row<=1)
	   	return "no";
	   else 
	  	return $str_err_array;
	}

/************************************* End Upload Category ***************************************/


/************************************* Start Upload Color ***************************************/

function uploadColorFile(){		
		$fields = array('Color Description*','Color Code*');	
		$file = fopen($_FILES['fileToUpload']['tmp_name'],"r");
		$strErr="";
		$row=0;		
		while(! feof($file)){		  
			  $arrFile=fgetcsv($file);	
			  $flag=true;		  
			  if($row>0 && is_array($arrFile)) {	
			  
			  
				 if(empty($arrFile[0]) || $arrFile[0]=="Color Description*" || empty($arrFile[1]) || $arrFile[1]=="Color Code*"){
				 	//$strErr .="Error in Row".$row." ";
					if(empty($arrFile[0]) && empty($arrFile[1])){$strErr .= "Error in Row".$row." ,".$fields[0]." and ".$fields[1]."  shouldn't be empty \n";}
					else if(empty($arrFile[0])){$strErr .= "Error in Row".$row." ,".$fields[0]." shouldn't be empty \n";}
					else {$strErr .= "Error in Row".$row." ,".$fields[1]." shouldn't be empty \n";}
					$str_err_array=array($strErr);
					$flag =false;
				 }
				
				
				
				if($flag){
					$condi=	" account_id=".$_SESSION['accountId']."  AND  LOWER(color_code)='".mysql_real_escape_string(strtolower($arrFile[1]))."'";
					$aCatRec=$this->_getSelectList('table_color','*','',$condi);
					if(is_array($aCatRec)) {					
						
						$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,already exists in the system. \n";
						$str_err_array=array($strErr);
						$flag=false;
					}
				}	
				
							  
				if($flag){	
					$data = array();					
					$data['account_id']= $this->clean($_SESSION['accountId']);		
					$data['color_desc']= mysql_real_escape_string( $this->clean($arrFile[0]));
					$data['color_code']= mysql_real_escape_string( $this->clean($arrFile[1]));
					$data['last_update_date']=date('Y-m-d');
					$data['last_update_status']='New';
					$data['status']= 'A';
					$id=$this->_dbInsert($data,'table_color');				
				}			  
		  }	
		  $row++;	   
		}		
		fclose($file);	  
	   if($row<=1)
	   	return "no";
	   else 
	  	return $str_err_array;
	}

/************************************* End Upload Color ***************************************/




/************************************* Start Upload Items ***************************************/


function uploadItemListFile(){	
		$fields = array('Category Name*','Item Code*','Item Description*','Weight','D.P*','M R P*','*Brand','Offer Type','Item Erp Code', '*Division Name','*Item Segment','Color','Capacity','Volt','CCA','*Item Availability','*Warranty','*Prodata','*Grace');	// Remove Chain Name		
		$file = fopen($_FILES['fileToUpload']['tmp_name'],"r");
		$strErr="";
		$row=0;		
		
		while(! feof($file)){		  
			  $arrFile=fgetcsv($file);
			  $flag=true;		  
			  if($row>0 && is_array($arrFile)) {	

             
              if(empty($arrFile[0]) || empty($arrFile[1]) || empty($arrFile[2]) || empty($arrFile[4]) || empty($arrFile[5])){
				 if(empty($arrFile[0])){
					$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5]." ,".$arrFile[6].",".$fields[0]." shouldn't be empty \n";
					
				 }
				 else if(empty($arrFile[1])){
					$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5]." ,".$arrFile[6].",".$fields[1]." shouldn't be empty \n";
					
				 }
				else if(empty($arrFile[2])){
					$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5]." ,".$arrFile[6].",".$fields[2]." shouldn't be empty \n";
					
				 }
				 else if(empty($arrFile[4])){
					$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5]." ,".$arrFile[6].",".$fields[4]." shouldn't be empty \n";
					
				 }
				 else if(empty($arrFile[5])){
					$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5]." ,".$arrFile[6].",".$fields[5]." shouldn't be empty \n";
					
				 }
				 


					$str_err_array=array($strErr);
					$flag =false;
				 }
				 
				if($flag)
				{
					$condi=	" account_id=".$_SESSION['accountId']." AND LOWER(item_code)='".mysql_real_escape_string(strtolower($arrFile[1]))."' AND  LOWER(item_name)='".mysql_real_escape_string(strtolower($arrFile[2]))."'";
					$aCatRec=$this->_getSelectList('table_item','*','',$condi);
					if(is_array($aCatRec)) {
						$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5]." ,".$arrFile[6]." ,already exists in the system. \n";
						$str_err_array=array($strErr);						
						$flag=false;
					}
				}
				
				
				if($flag)
				{					
					 $condi="account_id=".$_SESSION['accountId']." AND LOWER(category_name)='".mysql_real_escape_string(strtolower($arrFile[0]))."'";
					 
				 	$catRec=$this->_getSelectList('table_category',"category_id",'',$condi);				  
					if(!is_array($catRec)){
						$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5]." ,".$arrFile[6]." ,Invalid category name. \n";
						$str_err_array=array($strErr);						
						$flag =false;
					}
				 }


				 // checking Brands
				 if($flag && $arrFile[6])
				{					
					 $condi="account_id=".$_SESSION['accountId']." AND LOWER(brand_name)='".mysql_real_escape_string(strtolower($arrFile[6]))."'";
					 
				 	$brandRec=$this->_getSelectList('table_brands',"brand_name,brand_id",'',$condi);				  
					if(!is_array($brandRec)){
						$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5]." ,".$arrFile[6].",Invalid brand name. \n";
						$str_err_array=array($strErr);						
						$flag =false;
					}else
					{
					$brandId=$brandRec[0]->brand_id;	
					}

				 }

				 /* Description : for Item Segment */
				// echo $arrFile[10];
				 if($flag && $arrFile[10]!="")
				{					
					 $condi="account_id=".$_SESSION['accountId']." AND LOWER(segment_name)='".mysql_real_escape_string(strtolower($arrFile[11]))."'";
					 
				 	$segmentRec=$this->_getSelectList('table_segment',"segment_name,segment_id",'',$condi);				  
					if(!is_array($segmentRec)){
						$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5]." ,".$arrFile[6].",".$arrFile[7].",".$arrFile[8].",".$arrFile[9].",".$arrFile[10]." ,Invalid Segment. \n";
						$str_err_array=array($strErr);						
						$flag =false;
					}
					else
					{
					$segmentId=$segmentRec[0]->segment_id;	
					}

				 }


				  /* Description : for Item Division */
				// echo $arrFile[10];
				 if($flag && $arrFile[9]!="")
				{					
					 $condi="account_id=".$_SESSION['accountId']." AND LOWER(division_name)='".mysql_real_escape_string(strtolower($arrFile[10]))."'";
					 
				 	$divisionRec=$this->_getSelectList('table_division',"division_name,division_id",'',$condi);				  
					if(!is_array($divisionRec)){
						$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5]." ,".$arrFile[6].",".$arrFile[7].",".$arrFile[8].",".$arrFile[9].",".$arrFile[10]." ,Invalid Division. \n";
						$str_err_array=array($strErr);						
						$flag =false;
					}
					else
					{
					$divisionId=$divisionRec[0]->division_id;	
					}

				 }

				 /*Description : for Sku name */

				//  if($flag && $arrFile[11]!="")
				// {					
				// 	 $condi="account_id=".$_SESSION['accountId']." AND LOWER(sku_name)='".mysql_real_escape_string(strtolower($arrFile[11]))."'";
					 
				//  	$skuRec=$this->_getSelectList('table_sku',"sku_name,sku_id",'',$condi);				  
					

				//  }


				  //Checking Offers

				 

				// for cases
				 // $cases     =mysql_real_escape_string($arrFile[6]);
				 // $casesList = explode(",", $cases);
				 
				 
				 // if($flag && !is_numeric($cases)){						
					// 		$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5]." ,".$arrFile[6].",".$arrFile[7]." ,".$arrFile[8]." ,Case Size should be in Numeric. \n";
					// 			$str_err_array=array($strErr);
					// 			$flag =false;
						
					// 	}

				 // Cases Sizes should be one.

				 // if($flag && sizeof($casesList)>1){
				 // 	 $strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5]." ,".$arrFile[6].",".$arrFile[7]." ,".$arrFile[8]." ,One item should be on case size. \n";
					// 			$str_err_array=array($strErr);
					// 			$flag =false;
				 //    }

				    /* Check cases in numeric values. */
				   
				    

				 


				 /*if($arrFile[6]!="")
				 {
                    for($i=0;$i<count($casesList);$i++)
                    {
                    $condition=	" LOWER(case_size)='".strtolower($casesList[$i])."'";	
                    $caseval=$this->_getSelectList('table_cases','case_id','',$condition);
                    if(!is_array($caseval))
                       {
                     $strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5]." ,".$arrFile[6].",".$arrFile[7]." ,".$arrFile[8]." ,Invalid casees sizes. \n";
								$str_err_array=array($strErr);
								$flag =false;
                       }

                     }

				   }*/



				 


				// COLOR SECTION    // code commented
				/*$color=mysql_real_escape_string($arrFile[6]);
				$colorList = explode(",", $color);
				
				if($arrFile[6]!='')
				{
					for($i=0;$i<count($colorList);$i++)
					{
						$condition=	" LOWER(color_code)='".strtolower($colorList[$i])."'";
						$col=$this->_getSelectList('table_color','color_id','',$condition);
							if(!is_array($col))
							{
								$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5]." ,".$arrFile[6]." ,Invalid color code. \n";
								$str_err_array=array($strErr);
								$flag =false;
							}
					}
				} else {
				
					// Add Default QTY attribute for all of the items into table_color.
				
				 $chkColor = $this->_getSelectList('table_color','color_id','', ' LOWER(color_code) = "qty"');
				 
					if(!is_array($chkColor) && $chkColor[0]->color_id =='') { 
						
						$data = array();					
						$data['account_id']=$this->clean($_SESSION['accountId']);		
						$data['color_code']= 'QTY';
						$data['color_desc']= 'QTY';
						$data['last_update_date']=date('Y-m-d');
						$data['last_update_status']='New';
						$data['status']= 'A';
						$colorID = $this->_dbInsert($data,'table_color');	
					
					}
				}*/
         




				 // Division Name AJAY@2016-04-28
				// if($flag) {
				// if($arrFile[12]!='') {
				// 	$division_ids = array();
				// 	$divisionString = mysql_real_escape_string($arrFile[12]);
				// 	$divisionList = explode(",", $divisionString);

				// 	for($i=0;$i<count($divisionList);$i++)
				// 	{
				// 		$condition=	" LOWER(division_name)='".strtolower(trim($divisionList[$i]))."'";
				// 		$col=$this->_getSelectList('table_division','division_id','',$condition);

				// 			if(!is_array($col)){
				// 				$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5]." ,".$arrFile[6]." ,Invalid ".trim($divisionList[$i])." division name. \n";
				// 				$str_err_array=array($strErr);
				// 				$flag =false;
				// 			} else {
				// 				$division_ids[] = $col[0]->division_id;
				// 			}
				// 	}
				// 	} 
				// }
				 // Division Name AJAY@2016-04-28









         
				if($flag)
				{	
					$data = array();					
					$data['account_id']=$this->clean($_SESSION['accountId']);		
					$data['category_id']= $catRec[0]->category_id;
					$data['item_code']= mysql_real_escape_string($arrFile[1]);
					$data['item_name']= mysql_real_escape_string($arrFile[2]);
					$data['item_size']= mysql_real_escape_string($arrFile[3]);
					$data['brand_id'] = $brandId;
					
					$data['item_erp_code']=mysql_escape_string($arrFile[8]);
					$data['item_erp_code_old']=mysql_escape_string($arrFile[7]);
					$data['model_code']=mysql_escape_string($arrFile[9]);
					$data['item_division']=$divisionId;
					$data['item_segment']=$segmentId;
					$data['item_color']=mysql_escape_string($arrFile[12]);
					$data['item_capacity']=mysql_escape_string($arrFile[13]);
					$data['item_volt']=mysql_escape_string($arrFile[14]);
					$data['item_availability']=mysql_escape_string($arrFile[16]);
					$data['item_cca']=mysql_escape_string($arrFile[15]);
					$data['item_warranty']=mysql_escape_string($arrFile[17]);
					$data['item_prodata']=mysql_escape_string($arrFile[18]);
					$data['item_grace']=mysql_escape_string($arrFile[19]);
					$data['item_name'] = mysql_real_escape_string($arrFile[6])." ".mysql_real_escape_string($arrFile[14])."AH".mysql_real_escape_string($arrFile[11])." (".mysql_escape_string($arrFile[1]).")";
					/* New added */
					// $data['variant_id'] =$variantId;
					// $data['sku_id']		=$skuId;

					$data['last_update_date']=date('Y-m-d');
					$data['last_update_status']='New';
					$data['status']= 'A';
					$item_id=$this->_dbInsert($data,'table_item');
					
					/*$color=mysql_real_escape_string($arrFile[6]);
					$colorList = explode(",", $color);*/
					$cases=mysql_real_escape_string($arrFile[6]);
				    $casesList = explode(",", $cases);
					
					if($arrFile[6]!='')
					{
						for($i=0;$i<count($casesList);$i++)
						{
							/*$condition=	" LOWER(color_code)='".strtolower($colorList[$i])."'";
							$col=$this->_getSelectList('table_color','color_id','',$condition);
							$data1['item_id']= $this->clean($item_id);
							$data1['color_id']= $this->clean($col[0]->color_id);
							$col_id=$this->_dbInsert($data1,'table_item_color');
								*/

							/*$condition=	" LOWER(case_size)='".strtolower($casesList[$i])."'";
							$caseVal  =$this->_getSelectList('table_cases','case_id','',$condition);
							$case['item_id'] 		= $item_id;
							$case['case_id'] 		= $caseVal[0]->case_id;
							$caseRelation =$this->_dbInsert($case,'table_item_case_relationship'); */


							$case['case_description']   =mysql_real_escape_string($arrFile[1]).'-'.mysql_escape_string($casesList[$i]);
							$case['case_size']          =mysql_escape_string($casesList[$i]);
							$case['account_id']			=$this->clean($_SESSION['accountId']);	
							$case['status']				='A';
							$caseId=$this->_dbInsert($case,'table_cases');

							$data2['item_id']=$item_id;	
							$data2['case_id']=$caseId;
							$rou_sch_id=$this->_dbInsert($data2,'table_item_case_relationship');




							
							 
						   // Default inserted a record with attribute ID
						   $data3['account_id']=$this->clean($_SESSION['accountId']); 
						   $data3['item_id'] = $this->clean($item_id);
						   $data3['category_id']= $this->clean($catRec[0]->category_id);
						   $data3['attribute_value_id']= $this->clean($caseVal[0]->case_id);
						   $data3['last_updated_date']=date('Y-m-d');
						   $data3['last_update_datetime']=date('Y-m-d h:i:s');
						   $data3['status']= 'A';   
						   $ndc_stk_id = $this->_dbInsert($data3,'table_item_ndc_stock');
												
							
							
							
							
						}
					} else {
					
							/*$chkColor = $this->_getSelectList('table_color','color_id','', ' LOWER(color_code) = "qty"');
							
							$data1['item_id']= $this->clean($item_id);
							$data1['color_id']= $this->clean($chkColor[0]->color_id);
							$col_id=$this->_dbInsert($data1,'table_item_color');
							*/
						   // Default inserted a record with attribute ID

						   $data3['account_id']= $this->clean($_SESSION['accountId']); 
						   $data3['item_id'] = $this->clean($item_id);
						   $data3['category_id']= $this->clean($catRec[0]->category_id);
						   $data3['attribute_value_id']= 0;
						   $data3['last_updated_date']=date('Y-m-d');
						   $data3['last_update_datetime']=date('Y-m-d h:i:s');
						   $data3['status']= 'A';   
						   $ndc_stk_id = $this->_dbInsert($data3,'table_item_ndc_stock');					
							
					
						}
					}

					//  if flag is true for ceses then we add.

			  // If item added successfully so added their division name AJAY@2016-04-28

				if(sizeof($division_ids)>0 && $flag) {

					foreach ($division_ids as $key => $value) {
						# code...
						   $divisionMapping = array();

						   $divisionMapping['item_id'] 		= $item_id;
						   $divisionMapping['division_id']	= $value;
						   $this->_dbInsert($divisionMapping,'table_item_division_relationship');
					}
				} 

				 // If item added successfully so added their division name AJAY@2016-04-28
				
				
				if($flag)
				{	
					$data = array();					
					$data['account_id']= $this->clean($_SESSION['accountId']);		
					$data['item_id']= $this->clean($item_id);
					$data['item_mrp']= mysql_real_escape_string($arrFile[5]);
					$data['item_dp']= mysql_real_escape_string($arrFile[4]);
					$data['last_update_date']=date('Y-m-d');
					$data['last_update_status']='New';
					$data['start_date']= $this->clean($_SESSION['StartDate']);	
					$data['end_date']= $this->clean($_SESSION['EndDate']);	
					$data['status']='A';
					$item_price_id=$this->_dbInsert($data,'table_price');				
				}


/************************** Remove Chain Name 18th March 2015 (Gaurav) *************************/


			 	// used for modrern trade 05 jan 2015(Ajay) 

				/*if($flag){
				$chainList = array();
				$itemChain = $arrFile[7];
				
				if(isset($itemChain)) {

				$chainList = explode (",",$itemChain);
				//echo "<pre>";
				//print_r($chainList);
				//exit;
				if(sizeof($chainList)> 0) {

				foreach ($chainList as $value) :

				$value = preg_replace( "/\r|\n/", "", trim($value) );

				$condi = " LOWER(chain_name)='".mysql_real_escape_string(strtolower(trim($value)))."'";
							
				$aRetRec = $this->_getSelectList('table_chain','chain_id','',$condi);				
					
					if(!is_array($aRetRec)){
						$data = array();	
						$data['account_id'] =$_SESSION['accountId'];			
						$data['chain_name'] = $value;
						$data['last_updated_on'] = date('Y-m-d H:i:s');
						$data['status']='A';
						$chain_id = $this->_dbInsert($data,'table_chain');
					} else {
						$chain_id = $aRetRec[0]->chain_id;
					}


				$chainData = array();
				
				$chainData['item_id'] = $item_id;	
				$chainData['chain_id'] = $chain_id;	
				$this->_dbInsert($chainData,'table_item_chain_relationship');

				endforeach;

				//exit;

				} // check size

				} // check chain list not empty

				} // check flag
				*/
				// used for modren trade 05 Jan 2015
/************************** Remove Chain Name 18th March 2015 (Gaurav) *************************/
		  }	
		  $row++;	   
		}		
		fclose($file);	  
	   if($row<=1)
	   	return "no";
	   else 
	  	return $str_err_array;
	}

/************************************* Start Upload Items ***************************************/

/************************************* Start Upload PTR For Items ***************************************/

function importItemsPTRFile(){		
		$fields = array('State Name*','City Name*','Item Code*',' PTR Price*');
		$file = fopen($_FILES['fileToUpload']['tmp_name'],"r");
		$strErr="";
		$row=0;		
		while(! feof($file)){		  
			  $arrFile=fgetcsv($file);	
			  $flag=true;		  
			  if($row>0 && is_array($arrFile)) {	
				 if(empty($arrFile[0]) || empty($arrFile[1]) || empty($arrFile[2]) || empty($arrFile[3])){
					 if(empty($arrFile[0])){
					$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3].",".$fields[0]." shouldn't be empty \n";
					
				 }
				 else if(empty($arrFile[1])){
					$strErr .="Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3].",".$fields[1]." shouldn't be empty \n";
					
				 }
				else if(empty($arrFile[2])){
					$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3].",".$fields[2]." shouldn't be empty \n";
					
				 }
				 else if(empty($arrFile[3])){
					$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]." ,".$fields[3]." shouldn't be empty \n";
					
				 }
				 	$str_err_array=array($strErr);
					$flag =false;
				 }
				 
				 
				 
				if($flag){					
					 $condi=	" LOWER(state_name)='".mysql_real_escape_string(strtolower($arrFile[0]))."'";
					 //echo $condi."<br/>";
				 	$StateRec=$this->_getSelectList2('state',"state_id",'',$condi);	
					$state_id=$StateRec[0]->state_id;
					if(!is_array($StateRec)){
						//$strErr .="Invalid City name: &nbsp;&nbsp;".implode(",",$arrFile)."";
						//$flag =false;
						$state = array();					
						$state['state_name']= mysql_real_escape_string( $this->clean($arrFile[0]));
						$state['last_update_date']=date('Y-m-d');
						$state['last_update_status']='New';
						$state_id=$this->_dbInsert($state,'state');
					}
				 }
				 
				if($flag){					
					 $condi =" LOWER(city_name)='".mysql_real_escape_string(strtolower($arrFile[1]))."' AND state_id='".$state_id."'";
					
				 	$CityRec=$this->_getSelectList2('city',"city_id",'',$condi);
					$city_id=$CityRec[0]->city_id;
					if(!is_array($CityRec)){
						
						$city = array();
						$city['state_id']= $this->clean($state_id);
						$city['city_name']= mysql_real_escape_string( $this->clean($arrFile[1]));
						$city['last_update_date']=date('Y-m-d');
						$city['last_update_status']='New';
						$city_id=$this->_dbInsert($city,'city');
					}
				 }
				 
				if($flag){					
					$condi=" LOWER(item_code)='".mysql_real_escape_string(strtolower($arrFile[2]))."'";
				 	$ItemRec=$this->_getSelectList('table_item',"item_id",'',$condi);
					$item_id=$ItemRec[0]->item_id;
					if(!is_array($ItemRec)){
			$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,Invalid Item Code. \n";
						$str_err_array=array($strErr);
						$flag =false;
					}
				 }
				 
				if($flag){					
					$condi=" item_id='".$item_id."' and city_id='".$city_id."'";
				 	$PtrRec=$this->_getSelectList('table_ptr',"ptr_id",'',$condi);
					if(is_array($PtrRec)){
						$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,Item Price Details already exists in the system. \n";
						$str_err_array=array($strErr);
						$flag =false;
					}
				 }
				 
				if($flag){	
					$data = array();					
					$data['account_id']= $this->clean($_SESSION['accountId']);		
					$data['item_id']= $this->clean($item_id);		
					$data['city_id']= $this->clean($city_id);
					$data['item_price']= mysql_real_escape_string($arrFile[3]);
					$data['last_update_date']=date('Y-m-d');
					$data['last_update_status']='New';
					$data['start_date']='New';
					$data['start_date']= $this->clean($_SESSION['StartDate']);	
					$data['end_date']= $this->clean($_SESSION['EndDate']);	
					$data['status']='A';	
					$item_price_id=$this->_dbInsert($data,'table_ptr');				
				}
		  }	
		  $row++;	   
		}		
		fclose($file);	  
	   if($row<=1)
	   	return "no";
	   else 
	  	return $str_err_array;
	}

/************************************* End Upload PTR For Items ***************************************/

/************************************* Start Upload Distributor ***************************************/


function uploadDistributorFile(){		
		
		//$fields = array('Distributor Name*','Phone No1*','Phone No2','Phone No3','Distributor Address','Distributor City*','State*','District*','Taluka Name*','zipcode','Contact Person1*','contact Phone No1*','Contact Person2','contact Phone No2','Contact Person3','contact Phone No3','Email-ID1*','Email-ID2','Email-ID3','Number To Send SMS*','Distributor Class','Distributor Region','Distributor Code*','Aadhar No','Pan No', 'Division Name');

		$fields = array('Distributor Name*','Phone No1*','Phone No2','Phone No3','Distributor Address','State*','City*','zipcode*','Contact Person1*','contact Phone No1*','Contact Person2','contact Phone No2','Contact Person3','contact Phone No3','Email-ID1*','Email-ID2','Email-ID3','Number To Send SMS*','Aadhar No*','Pan No*', 'Division Name*', 'Brand*','Country*','Region*','Zone*', 'Area', 'Credit Limit*', 'Tin Number*');
		
		$file = fopen($_FILES['fileToUpload']['tmp_name'],"r");
		$strErr="";
		$row=0;		
		while(! feof($file)){		  
			  $arrFile=fgetcsv($file); 

			  $flag=true;
		 if($row>0 && is_array($arrFile)) {	

		 	/*echo "<pre>";
	print_r($arrFile); 
	exit;*/
		 
		  if(empty($arrFile[0]) || $arrFile[0]=="Distributor Name*" || empty($arrFile[1]) || empty($arrFile[5]) || empty($arrFile[6]) || empty($arrFile[7]) || empty($arrFile[8]) || empty($arrFile[9]) || empty($arrFile[14]) || empty($arrFile[17]) || empty($arrFile[18])  || empty($arrFile[19]) || empty($arrFile[20]) || empty($arrFile[21])  || empty($arrFile[22]) || empty($arrFile[23]) || empty($arrFile[24]) || empty($arrFile[26]) || empty($arrFile[27]) ){

		  
		  		if(empty($arrFile[0]) || $arrFile[0]=="Distributor Name*" ){


					$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5]." ,".$arrFile[6]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].",".$arrFile[10]."  ,".$arrFile[11].",".$arrFile[12]." ,".$arrFile[13]." ,".$arrFile[14].",".$arrFile[15]." ,".$arrFile[16].",".$arrFile[17]."  ,".$arrFile[18].",".$arrFile[19]." , ".$arrFile[20]." ,".$arrFile[21]." ,".$arrFile[22]." ,".$arrFile[23].",".$arrFile[24]." , ".$arrFile[25]." ,".$arrFile[26]." ,".$arrFile[27]." , ".$fields[0]." shouldn't be empty  \n";
					
				 }
				 else if(empty($arrFile[1])){
					$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5]." ,".$arrFile[6]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].",".$arrFile[10]."  ,".$arrFile[11].",".$arrFile[12]." ,".$arrFile[13]." ,".$arrFile[14].",".$arrFile[15]." ,".$arrFile[16].",".$arrFile[17]."  ,".$arrFile[18].",".$arrFile[19]." , ".$arrFile[20]." ,".$arrFile[21]." ,".$arrFile[22]." ,".$arrFile[23]." , ".$arrFile[24]." , ".$arrFile[25]." ,".$arrFile[26]." ,".$arrFile[27]." , ".$fields[1]." shouldn't be empty  \n";
				 }
				else if(empty($arrFile[5])){
					$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5]." ,".$arrFile[6]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].",".$arrFile[10]."  ,".$arrFile[11].",".$arrFile[12]." ,".$arrFile[13]." ,".$arrFile[14].",".$arrFile[15]." ,".$arrFile[16].",".$arrFile[17]."  ,".$arrFile[18].",".$arrFile[19]." , ".$arrFile[20]." ,".$arrFile[21]." ,".$arrFile[22]." ,".$arrFile[23]." , ".$arrFile[24]." , ".$arrFile[25]." ,".$arrFile[26]." ,".$arrFile[27]." , ".$fields[5]." shouldn't be empty  \n";
				 }
				 else if(empty($arrFile[6])){
					$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5]." ,".$arrFile[6]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].",".$arrFile[10]."  ,".$arrFile[11].",".$arrFile[12]." ,".$arrFile[13]." ,".$arrFile[14].",".$arrFile[15]." ,".$arrFile[16].",".$arrFile[17]." ,".$arrFile[18].",".$arrFile[19]." , ".$arrFile[20]." ,".$arrFile[21]." ,".$arrFile[22]." ,".$arrFile[23].", ".$arrFile[24]." , ".$arrFile[25]." ,".$arrFile[26]." ,".$arrFile[27]." , ".$fields[6]." shouldn't be empty  \n";
				 }
				 else if(empty($arrFile[7])){
					$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5]." ,".$arrFile[6]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].",".$arrFile[10]."  ,".$arrFile[11].",".$arrFile[12]." ,".$arrFile[13]." ,".$arrFile[14].",".$arrFile[15]." ,".$arrFile[16].",".$arrFile[17]." ,".$arrFile[18].",".$arrFile[19]." , ".$arrFile[20]." ,".$arrFile[21]." ,".$arrFile[22]." ,".$arrFile[23].", ".$arrFile[24]." , ".$arrFile[25]." ,".$arrFile[26]." ,".$arrFile[27].", ".$fields[7]." shouldn't be empty  \n";
				 }
				 else if(empty($arrFile[8])){
					$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5]." ,".$arrFile[6]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].",".$arrFile[10]."  ,".$arrFile[11].",".$arrFile[12]." ,".$arrFile[13]." ,".$arrFile[14].",".$arrFile[15]." ,".$arrFile[16].",".$arrFile[17]."  ,".$arrFile[18].",".$arrFile[19]." , ".$arrFile[20]." ,".$arrFile[21]." ,".$arrFile[22]." ,".$arrFile[23]." , ".$arrFile[24]." , ".$arrFile[25]." ,".$arrFile[26]." ,".$arrFile[27].", ".$fields[8]." shouldn't be empty  \n";
				 }
				else if(empty($arrFile[9])){
					$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5]." ,".$arrFile[6]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].",".$arrFile[10]."  ,".$arrFile[11].",".$arrFile[12]." ,".$arrFile[13]." ,".$arrFile[14].",".$arrFile[15]." ,".$arrFile[16].",".$arrFile[17]."  ,".$arrFile[18].",".$arrFile[19]." , ".$arrFile[20]." ,".$arrFile[21]." ,".$arrFile[22].",".$arrFile[23]." , ".$arrFile[24]." , ".$arrFile[25]." ,".$arrFile[26]." ,".$arrFile[27].", ".$fields[9]." shouldn't be empty  \n";
				 }
				 else if(empty($arrFile[14])){
					$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5]." ,".$arrFile[6]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].",".$arrFile[10]."  ,".$arrFile[11].",".$arrFile[12]." ,".$arrFile[13]." ,".$arrFile[14].",".$arrFile[15]." ,".$arrFile[16].",".$arrFile[17]."  ,".$arrFile[18].",".$arrFile[19]." , ".$arrFile[20]." ,".$arrFile[21]." ,".$arrFile[22]." ,".$arrFile[23].", ".$arrFile[24]." , ".$arrFile[25]." ,".$arrFile[26]." ,".$arrFile[27].", ".$fields[14]." shouldn't be empty  \n";
				 }
				 else if(empty($arrFile[17])){
					$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5]." ,".$arrFile[6]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].",".$arrFile[10]."  ,".$arrFile[11].",".$arrFile[12]." ,".$arrFile[13]." ,".$arrFile[14].",".$arrFile[15]." ,".$arrFile[16].",".$arrFile[17]."  ,".$arrFile[18].",".$arrFile[19]." , ".$arrFile[20]." ,".$arrFile[21]." ,".$arrFile[22]." ,".$arrFile[23]." , ".$arrFile[24]." , ".$arrFile[25]." ,".$arrFile[26]." ,".$arrFile[27].", ".$fields[17]." shouldn't be empty  \n";
				 }
				 else if(empty($arrFile[18])){
					$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5]." ,".$arrFile[6]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].",".$arrFile[10]."  ,".$arrFile[11].",".$arrFile[12]." ,".$arrFile[13]." ,".$arrFile[14].",".$arrFile[15]." ,".$arrFile[16].",".$arrFile[17]."  ,".$arrFile[18].",".$arrFile[19]." , ".$arrFile[20]." ,".$arrFile[21]." ,".$arrFile[22]." ,".$arrFile[23]." , ".$arrFile[24]." , ".$arrFile[25]." ,".$arrFile[26]." ,".$arrFile[27].", ".$fields[18]." shouldn't be empty  \n";
				 }
				 else if(empty($arrFile[19])){
					$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5]." ,".$arrFile[6]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].",".$arrFile[10]."  ,".$arrFile[11].",".$arrFile[12]." ,".$arrFile[13]." ,".$arrFile[14].",".$arrFile[15]." ,".$arrFile[16].",".$arrFile[17]."  ,".$arrFile[18].",".$arrFile[19]." , ".$arrFile[20]." ,".$arrFile[21]." ,".$arrFile[22]." ,".$arrFile[23]." , ".$arrFile[24]." , ".$arrFile[25]." ,".$arrFile[26]." ,".$arrFile[27].", ".$fields[19]." shouldn't be empty  \n";
				 }
				 else if(empty($arrFile[20])){
					$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5]." ,".$arrFile[6]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].",".$arrFile[10]."  ,".$arrFile[11].",".$arrFile[12]." ,".$arrFile[13]." ,".$arrFile[14].",".$arrFile[15]." ,".$arrFile[16].",".$arrFile[17]."  ,".$arrFile[18].",".$arrFile[19]." , ".$arrFile[20]." ,".$arrFile[21]." ,".$arrFile[22]." ,".$arrFile[23]." , ".$arrFile[24]." , ".$arrFile[25]." ,".$arrFile[26]." ,".$arrFile[27].", ".$fields[20]." shouldn't be empty  \n";
				 }
				 else if(empty($arrFile[21])){
				 	$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5]." ,".$arrFile[6]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].",".$arrFile[10]."  ,".$arrFile[11].",".$arrFile[12]." ,".$arrFile[13]." ,".$arrFile[14].",".$arrFile[15]." ,".$arrFile[16].",".$arrFile[17]."  ,".$arrFile[18].",".$arrFile[19]." , ".$arrFile[20]." ,".$arrFile[21]." ,".$arrFile[22]." ,".$arrFile[23].", ".$arrFile[24]." , ".$arrFile[25]." ,".$arrFile[26]." ,".$arrFile[27].", ".$fields[21]." shouldn't be empty  \n";
				 }
				 else if(empty($arrFile[22])){
					$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5]." ,".$arrFile[6]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].",".$arrFile[10]."  ,".$arrFile[11].",".$arrFile[12]." ,".$arrFile[13]." ,".$arrFile[14].",".$arrFile[15]." ,".$arrFile[16].",".$arrFile[17]."  ,".$arrFile[18].",".$arrFile[19]." ,".$arrFile[20]." ,".$arrFile[21]." , ".$arrFile[22]." ,".$arrFile[23].", ".$arrFile[24]." , ".$arrFile[25]." ,".$arrFile[26]." ,".$arrFile[27].", ".$fields[22]." shouldn't be empty  \n";
				 }
				 else if(empty($arrFile[23])){
					$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5]." ,".$arrFile[6]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].",".$arrFile[10]."  ,".$arrFile[11].",".$arrFile[12]." ,".$arrFile[13]." ,".$arrFile[14].",".$arrFile[15]." ,".$arrFile[16].",".$arrFile[17]."  ,".$arrFile[18].",".$arrFile[19]." , ".$arrFile[20]." ,".$arrFile[21]." ,".$arrFile[22]." ,".$arrFile[23]." , ".$arrFile[24]." , ".$arrFile[25]." ,".$arrFile[26]." ,".$arrFile[27]." ,  ".$fields[23]." shouldn't be empty  \n";
				 }
				 else if(empty($arrFile[24])){
				 	$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5]." ,".$arrFile[6]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].",".$arrFile[10]."  ,".$arrFile[11].",".$arrFile[12]." ,".$arrFile[13]." ,".$arrFile[14].",".$arrFile[15]." ,".$arrFile[16].",".$arrFile[17]."  ,".$arrFile[18].",".$arrFile[19]." , ".$arrFile[20]." ,".$arrFile[21]." ,".$arrFile[22]." ,".$arrFile[23].", ".$arrFile[24]." , ".$arrFile[25]." ,".$arrFile[26]." ,".$arrFile[27].", ".$fields[24]." shouldn't be empty  \n";
				 }
				 else if(empty($arrFile[26])){
					$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5]." ,".$arrFile[6]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].",".$arrFile[10]."  ,".$arrFile[11].",".$arrFile[12]." ,".$arrFile[13]." ,".$arrFile[14].",".$arrFile[15]." ,".$arrFile[16].",".$arrFile[17]."  ,".$arrFile[18].",".$arrFile[19]." ,".$arrFile[20]." ,".$arrFile[21]." , ".$arrFile[22]." ,".$arrFile[23].", ".$arrFile[24]." , ".$arrFile[25]." ,".$arrFile[26]." ,".$arrFile[27].",  ".$fields[26]." shouldn't be empty  \n";
				 }
				 else if(empty($arrFile[27])){

				 	$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5]." ,".$arrFile[6]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].",".$arrFile[10]."  ,".$arrFile[11].",".$arrFile[12]." ,".$arrFile[13]." ,".$arrFile[14].",".$arrFile[15]." ,".$arrFile[16].",".$arrFile[17]."  ,".$arrFile[18].",".$arrFile[19]." , ".$arrFile[20]." ,".$arrFile[21]." ,".$arrFile[22]." ,".$arrFile[23].", ".$arrFile[24]." , ".$arrFile[25]." ,".$arrFile[26]." ,".$arrFile[27].", ".$fields[27]." shouldn't be empty  \n";
				 }
				/* else if(empty($arrFile[28])){
					$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5]." ,".$arrFile[6]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].",".$arrFile[10]."  ,".$arrFile[11].",".$arrFile[12]." ,".$arrFile[13]." ,".$arrFile[14].",".$arrFile[15]." ,".$arrFile[16].",".$arrFile[17]."  ,".$arrFile[18].",".$arrFile[19]." ,".$arrFile[20]." ,".$arrFile[21]." , ".$arrFile[22]." ,".$arrFile[23].", ".$arrFile[24]." , ".$arrFile[25]." ,".$arrFile[26]." ,".$arrFile[27]." , ".$fields[28]." shouldn't be empty  \n";
				 }*/

  
		  			$str_err_array=array($strErr);
					$flag =false;
		  }
		  
		//  echo "<pre>"; print_r($arrFile); exit;
		 
		  
		  
		  
		  
		  
		  
		  
// Distributor Check
		  if($flag){
 
					$condi=	" account_id=".$_SESSION['accountId']." AND distributor_phone_no='".mysql_real_escape_string(strtolower($arrFile[1]))."' AND  LOWER(distributor_name)='".mysql_real_escape_string( $this->clean(strtolower($arrFile[0])))."'"; 
					$aDisRec=$this->_getSelectList('table_distributors','*','',$condi);
					if(is_array($aDisRec)) {
						$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5]." ,".$arrFile[6]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].",".$arrFile[10]."  ,".$arrFile[11].",".$arrFile[12]." ,".$arrFile[13]." ,".$arrFile[14].",".$arrFile[15]." ,".$arrFile[16].",".$arrFile[17]."  ,".$arrFile[18].",".$arrFile[19].",".$arrFile[20].",".$arrFile[21].",".$arrFile[22].",".$arrFile[23]." , ".$arrFile[24]." , ".$arrFile[25]." ,".$arrFile[26]." ,".$arrFile[27]." , distributor already exists in the system. \n";
						$str_err_array=array($strErr);
						$flag =false;
					}
				}
				



		if($flag){				
				
				if (!is_numeric($arrFile[1]) || !is_numeric($arrFile[17])) {					
					$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5]." ,".$arrFile[6]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].",".$arrFile[10]."  ,".$arrFile[11].",".$arrFile[12]." ,".$arrFile[13]." ,".$arrFile[14].",".$arrFile[15]." ,".$arrFile[16].",".$arrFile[17]."  ,".$arrFile[18].",".$arrFile[19]." , ".$arrFile[20].",".$arrFile[21].",".$arrFile[22].", ".$arrFile[23].", ".$arrFile[24]." , ".$arrFile[25]." ,".$arrFile[26]." ,".$arrFile[27]."  ,Please Provide the valid Phone Number. \n";				
					$str_err_array=array($strErr);						
					$flag=false;					
				} 
		
		}





// Fetching Country ID
		if($flag)
		{

			$condi=" LOWER(country_name)='".mysql_real_escape_string(strtolower($arrFile[22]))."'"; 
			$CountryRec=$this->_getSelectList2('country',"country_id",'',$condi);	
			if(isset($CountryRec[0]->country_id) && $CountryRec[0]->country_id>0) {
				$country_id=$CountryRec[0]->country_id;
			} else {
				$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5]." ,".$arrFile[6]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].",".$arrFile[10]."  ,".$arrFile[11].",".$arrFile[12]." ,".$arrFile[13]." ,".$arrFile[14].",".$arrFile[15]." ,".$arrFile[16].",".$arrFile[17]."  ,".$arrFile[18].",".$arrFile[19].",".$arrFile[20].",".$arrFile[21].",".$arrFile[22].",".$arrFile[23]." , ".$arrFile[24]." , ".$arrFile[25]." ,".$arrFile[26]." ,".$arrFile[27]." , Country Not Found. \n";
						$str_err_array=array($strErr);
						$flag =false;
							
			}



		}

// Fetching Region ID
		if($flag)
		{
			$condi=" LOWER(region_name)='".mysql_real_escape_string(strtolower($arrFile[23]))."' and country_id = '".$country_id."'"; 
			$RegionRec=$this->_getSelectList2('table_region',"region_id",'',$condi);	
			

			if(isset($RegionRec[0]->region_id) && $RegionRec[0]->region_id>0) {
				$region_id=$RegionRec[0]->region_id;
			} else {
				$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5]." ,".$arrFile[6]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].",".$arrFile[10]."  ,".$arrFile[11].",".$arrFile[12]." ,".$arrFile[13]." ,".$arrFile[14].",".$arrFile[15]." ,".$arrFile[16].",".$arrFile[17]."  ,".$arrFile[18].",".$arrFile[19].",".$arrFile[20].",".$arrFile[21].",".$arrFile[22].",".$arrFile[23]." , ".$arrFile[24]." , ".$arrFile[25]." ,".$arrFile[26]." ,".$arrFile[27]." , Region Not Found. \n";
						$str_err_array=array($strErr);
						$flag =false;
							
			}


		}		

// Fetching Zone ID
		if($flag)
		{
			$condi=" LOWER(zone_name)='".mysql_real_escape_string(strtolower($arrFile[24]))."' and region_id = '".$region_id."'"; 
			$ZoneRec=$this->_getSelectList2('table_zone',"zone_id",'',$condi);	
			


			if(isset($ZoneRec[0]->zone_id) && $ZoneRec[0]->zone_id>0) {
				$zone_id=$ZoneRec[0]->zone_id;
			} else {
				$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5]." ,".$arrFile[6]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].",".$arrFile[10]."  ,".$arrFile[11].",".$arrFile[12]." ,".$arrFile[13]." ,".$arrFile[14].",".$arrFile[15]." ,".$arrFile[16].",".$arrFile[17]."  ,".$arrFile[18].",".$arrFile[19].",".$arrFile[20].",".$arrFile[21].",".$arrFile[22].",".$arrFile[23]." , ".$arrFile[24]." , ".$arrFile[25]." ,".$arrFile[26]." ,".$arrFile[27]."  , Zone Not Found. \n";
						$str_err_array=array($strErr);
						$flag =false;
							
			}




		}		

// Fetching State ID
		if($flag)
		{
			$condi=" LOWER(state_name)='".mysql_real_escape_string(strtolower($arrFile[5]))."' and zone_id = '".$zone_id."'"; 
			$StateRec=$this->_getSelectList2('state',"state_id",'',$condi);	
			$state_id=$StateRec[0]->state_id;
			if(!is_array($StateRec)){
				$state = array();
				$state['zone_id']=$this->clean($zone_id);
				$state['state_name']=mysql_real_escape_string( $this->clean($arrFile[5]));
				$state['last_update_date']=date('Y-m-d');
				$state['last_update_status']='New';
				$state_id=$this->_dbInsert($state,'state');
			}
		}

// Fetching City ID
		// if($flag)
		// {
		// 	$condi=" LOWER(city_name)='".mysql_real_escape_string(strtolower($arrFile[6]))."' and state_id = '".$state_id."'"; 
		// 	$CityRec=$this->_getSelectList2('city',"city_id",'',$condi);	
		// 	$city_id=$CityRec[0]->city_id;
		// }

		// AJAY@2017-05-23 live back to old record
			 if($flag){					
					$condi=" LOWER(city_name)='".mysql_real_escape_string(strtolower($arrFile[6]))."' AND state_id='".$state_id."'";
				 	$CityRec=$this->_getSelectList2('city',"city_id",'',$condi);	
					$city_id=$CityRec[0]->city_id;
					if(!is_array($CityRec)){
						$city = array();
						$city['state_id']=$this->clean($state_id);
						$city['city_name']=mysql_real_escape_string( $this->clean($arrFile[6]));
						$city['last_update_date']=date('Y-m-d');
						$city['last_update_status']='New';
						$city_id=$this->_dbInsert($city,'city');
					}
				 }



// State Check old


// if($flag){		
				
			// Get State ID with the help of State Name	
		// 	if (in_array(strtolower(trim($arrFile[5])), $this->indian_all_states)) 
		// 	{		
		// 			$condi=" LOWER(state_name)='".mysql_real_escape_string(strtolower($arrFile[5]))."'";
		// 		 	$StateRec=$this->_getSelectList2('state',"state_id",'',$condi);
		// 			$state_id=$StateRec[0]->state_id;
		// 			if(!is_array($StateRec)){
		// 				$state = array();					
		// 				$state['state_name']=mysql_real_escape_string( $this->clean($arrFile[5]));
		// 				$state['last_update_date']=date('Y-m-d');
		// 				$state['last_update_status']='New';
		// 				$state_id=$this->_dbInsert($state,'state');
		// 			}
		// 	} else {
		// 		$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5]." ,".$arrFile[6]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].",".$arrFile[10]."  ,".$arrFile[11].",".$arrFile[12]." ,".$arrFile[13]." ,".$arrFile[14].",".$arrFile[15]." ,".$arrFile[16].",".$arrFile[17]."  , Sorry No State exists. \n";
		// 				$str_err_array=array($strErr);
		// 				$flag =false;
							
		// 	}
		// }
	
// City Check Old
			 // if($flag){					
				// 	$condi=" LOWER(city_name)='".mysql_real_escape_string(strtolower($arrFile[6]))."' AND state_id='".$state_id."'";
				//  	$CityRec=$this->_getSelectList2('city',"city_id",'',$condi);	
				// 	$city_id=$CityRec[0]->city_id;
				// 	if(!is_array($CityRec)){
				// 		$city = array();
				// 		$city['state_id']=$this->clean($state_id);
				// 		$city['city_name']=mysql_real_escape_string( $this->clean($arrFile[6]));
				// 		$city['last_update_date']=date('Y-m-d');
				// 		$city['last_update_status']='New';
				// 		$city_id=$this->_dbInsert($city,'city');
				// 	}
				//  }


/* For Taluka */

		/* if($flag){				
				 $condi=" LOWER(taluka_name)='".mysql_real_escape_string($this->clean(strtolower($arrFile[8])))."' AND city_id='".$city_id."'";
		
				 	$TalukaRec=$this->_getSelectList('table_taluka',"taluka_id",'',$condi);	
					$taluka_id= $TalukaRec[0]->taluka_id;
					
					if(sizeof($TalukaRec)<=0){
						//$strErr .="Invalid city name: &nbsp;&nbsp;".implode(",",$arrFile)."";
						//$flag =false;
						$taluka = array();
						$taluka['account_id'] 	=$_SESSION['accountId'];	
						$taluka['city_id']		= $this->clean($city_id);
						$taluka['taluka_name']	= mysql_real_escape_string( $this->clean($arrFile[8]));
						$taluka['status'] 		='A';	
						$taluka_id=$this->_dbInsert($taluka,'table_taluka');
					}
				 }*/

/* for market */
 				if($flag){				
					// $condi=" LOWER(market_name)='".mysql_real_escape_string($this->clean(strtolower($arrFile[26])))."' AND taluka_id='".$taluka_id."'";
					
					 $condi=" LOWER(market_name)='".mysql_real_escape_string($this->clean(strtolower($arrFile[25])))."'";
			
					 	$MarketRec=$this->_getSelectList(' table_markets',"market_id",'',$condi);	
						$market_id= $MarketRec[0]->market_id;
						
						if(sizeof($MarketRec)<=0){
							//$strErr .="Invalid city name: &nbsp;&nbsp;".implode(",",$arrFile)."";
							//$flag =false;
							$market = array();
							$market['account_id'] 	=$_SESSION['accountId'];	
						//	$market['taluka_id']	=$taluka_id;
							$market['market_name']	= mysql_real_escape_string( $this->clean($arrFile[26]));
							$market['status'] 		='A';	
							$market_id=$this->_dbInsert($market,' table_markets');
						}
				}

// 	Check Distributor Class Name '			 
				// if(!empty($arrFile[18]))
				// {
				// 	$condi=" LOWER(relationship_code)='".mysql_real_escape_string(strtolower($arrFile[18]))."' ";
				//  	$RelationRec=$this->_getSelectList('table_relationship',"relationship_id",'',$condi);
				//  	if(is_array($RelationRec))
				//  	{
				//  	  $relationshipId =$RelationRec[0]->relationship_id;	
				//  	}	
					
				// } 
// Check distributor region

			// if(!empty($arrFile[19]))
			//    {
			//    		$condi=" LOWER(region_name)='".mysql_real_escape_string(strtolower($arrFile[19]))."' ";
			// 	 	$RegionRec=$this->_getSelectList2('table_region',"region_id",'',$condi);
			// 	 	if(is_array($RegionRec))
			// 	 	{
			// 	 	  $regionId =$RegionRec[0]->region_id;	
			// 	 	}
			// 	 	else
			// 	 	{
			// 	 		$region = array();
			// 			//$region['account_id']=$this->clean($_SESSION['accountId']);		
			// 			$region['region_name']=mysql_real_escape_string( $this->clean($arrFile[19]));
			// 			$region['status']='A';
			// 			$regionId=$this->_dbInsert($region,'table_region');	
			// 	 	}	
				 	


			//    }	


			 // Check division name


			if(!empty($arrFile[20]) && $flag) {
			   		$condi=" LOWER(division_name)='".mysql_real_escape_string(strtolower($arrFile[20]))."' ";
				 	$division_name_check =$this->_getSelectList('table_division',"division_id,division_code",'',$condi);
					if(sizeof($division_name_check) == 0) {
						$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5]." ,".$arrFile[6]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].",".$arrFile[10]."  ,".$arrFile[11].",".$arrFile[12]." ,".$arrFile[13]." ,".$arrFile[14].",".$arrFile[15]." ,".$arrFile[16].",".$arrFile[17]."  ,".$arrFile[18].",".$arrFile[19].",".$arrFile[20].",".$arrFile[21]." ,".$arrFile[22]." ,".$arrFile[23]." , ".$arrFile[24].",".$arrFile[25].",".$arrFile[26]." ,".$arrFile[27]." , division name not found in the system. \n";
						$str_err_array=array($strErr);
						$flag =false;
					} else {
						$division_id =$division_name_check[0]->division_id;	
					}
			  }	


			  // Check Brand


			if(!empty($arrFile[21]) && $flag) {
			   		$condi=" LOWER(brand_name)='".mysql_real_escape_string(strtolower($arrFile[21]))."' ";
				 	$brand_name_check =$this->_getSelectList('table_brands',"brand_id",'',$condi);
					if(sizeof($brand_name_check) == 0) {
						$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5]." ,".$arrFile[6]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].",".$arrFile[10]."  ,".$arrFile[11].",".$arrFile[12]." ,".$arrFile[13]." ,".$arrFile[14].",".$arrFile[15]." ,".$arrFile[16].",".$arrFile[17]."  ,".$arrFile[18].",".$arrFile[19].",".$arrFile[20].",".$arrFile[21]." ,".$arrFile[22]." ,".$arrFile[23]." , ".$arrFile[24].",".$arrFile[25].",".$arrFile[26]." ,".$arrFile[27]."  , brand name not found in the system. \n";
						$str_err_array=array($strErr);
						$flag =false;
					} else {
						$brand_id =$brand_name_check[0]->brand_id;	
					}
			  }	



// Check for distributor code 				

			/*	if($flag){
					$condi=" LOWER(distributor_code)='".mysql_real_escape_string(strtolower($arrFile[27]))."' ";
					$aDisCode=$this->_getSelectList('table_distributors','*','',$condi);
					if(is_array($aDisCode)) {
						$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5]." ,".$arrFile[6]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].",".$arrFile[10]."  ,".$arrFile[11].",".$arrFile[12]." ,".$arrFile[13]." ,".$arrFile[14].",".$arrFile[15]." ,".$arrFile[16].",".$arrFile[17]."  ,".$arrFile[18].",".$arrFile[19].",".$arrFile[20].", ".$arrFile[21]." ,".$arrFile[22]." ,".$arrFile[23]." , ".$arrFile[24].",".$arrFile[25].",".$arrFile[26]." ,".$arrFile[27]." ,".$arrFile[28].", distributor code already exists in the system. \n";
						$str_err_array=array($strErr);
						$flag =false;
					}
				}*/


// Random Generate Distributor Code

				if($flag)
				{
					$min = 0001;
					$max = 1111;
					$_objAdmin = new Admin();
					$con = mysql_connect(MYSQL_HOST_NAME,MYSQL_USER_NAME,MYSQL_PASSWORD);
					
					mysql_select_db(MYSQL_DB_NAME, $con);
					$state_id = str_pad($state_id,2,'0',STR_PAD_LEFT);
					$brand_id_local = $brand_id;
					if($brand_id_local !='1' || $brand_id_local !='2')
					{
						$brand_id_local = '3';
					}
					$false_code = $division_code.$state_id.$brand_id_local;
					while($min<=$max)
					{

						$invID = str_pad($min, 4, '0', STR_PAD_LEFT);
						$sql = "Select * from table_distributors where distributor_code = '".$false_code.$invID."'";
						$resultset = mysql_query($sql);
						if(mysql_num_rows($resultset)>0)
						{
							$min++;	
						}
						else
						{
							$distributor_code = $false_code.$invID;
							break;
						}
					}
				}

				 
// Database Insert Query				 
				 if($flag){	
					$data = array();		
					//$str = preg_replace('/[^A-Za-z0-9\. -]/', '', $str);
				
															
					$data['account_id']=$this->clean($_SESSION['accountId']);		
					$data['distributor_name']= mysql_real_escape_string( $this->clean($arrFile[0]));
					$data['distributor_address']= mysql_real_escape_string( $this->clean($arrFile[4]));
					$data['market_id']= $market_id;
					/*add relationship class and region */
					$data['relationship_id']		= $relationshipId;
					$data['region_id']				= $region_id;
					$data['division_id']			= $division_id;
					$data['brand_id']				= $brand_id;
					$data['country']				= $country_id;
					$data['state']					= $state_id;
					$data['city']					= $city_id;
					$data['zone']					= $zone_id;
					$data['zipcode']				= mysql_real_escape_string($arrFile[7]);
					$data['distributor_phone_no']	= mysql_real_escape_string($this->clean($arrFile[1]));
					$data['distributor_phone_no2']	= mysql_real_escape_string($this->clean($arrFile[2]));
					$data['distributor_phone_no3']	= mysql_real_escape_string($this->clean($arrFile[3]));
					$data['distributor_email']		= mysql_real_escape_string($arrFile[14]);
					$data['distributor_email2']		= mysql_real_escape_string($arrFile[15]);
					$data['distributor_email3']		= mysql_real_escape_string($arrFile[16]);
					$data['contact_person']			= mysql_real_escape_string($this->clean($arrFile[8]));
					$data['contact_person2']		= mysql_real_escape_string($this->clean($arrFile[10]));
					$data['contact_person3']		= mysql_real_escape_string($this->clean($arrFile[12]));
					$data['contact_number']			= mysql_real_escape_string($this->clean($arrFile[9]));
					$data['contact_number2']		= mysql_real_escape_string($this->clean($arrFile[11]));
					$data['contact_number3']		= mysql_real_escape_string($this->clean($arrFile[13]));
					$data['sms_number']				= mysql_real_escape_string($this->clean($arrFile[17]));
					$data['distributor_code']		= $distributor_code;
					/* new fields added */

					$data['aadhar_no']				= mysql_real_escape_string($this->clean($arrFile[18]));
					$data['pan_no']					= mysql_real_escape_string($this->clean($arrFile[19]));
					$data['tin_number'] 			= mysql_real_escape_string($this->clean($arrFile[27]));
					$data['credit_limit'] 			= mysql_real_escape_string($this->clean($arrFile[26]));
				/*	$data['taluka_id']= mysql_real_escape_string($taluka_id);*/


					$data['start_date']				= date('Y-m-d');
					$data['end_date']				= $this->clean($_SESSION['EndDate']);	
					$data['status']					= 'A';
					$dis_id 						=$this->_dbInsert($data,'table_distributors');
					
					/*$category=mysql_real_escape_string($arrFile[18]);
					$categoryList = explode(",", $category);

					if($arrFile[18]!=''){
					for($i=0;$i<count($categoryList);$i++){
						$condition=	" LOWER(category_name)='".strtolower($categoryList[$i])."'";
						$col=$this->_getSelectList('table_category','category_id','',$condition);
						if(!is_array($col)){
						
						$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5]." ,".$arrFile[6]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].",".$arrFile[10]."  ,".$arrFile[11].",".$arrFile[12]." ,".$arrFile[13]." ,".$arrFile[14].",".$arrFile[15]." ,".$arrFile[16].",".$arrFile[17]."  ,".$arrFile[18].",".$arrFile[19]." ,Color Error. \n";
						$str_err_array=array($strErr);
						$flag =false;
						}

						$data1['distributor_id']= $this->clean($dis_id);
						$data1['category_id']= $this->clean($col[0]->category_id);
						$col_id=$this->_dbInsert($data1,'table_distributors_category');
						}
					}*/

					/*-----Distributor assign to all categories-----*/
					if($dis_id!="")	
					$Category=$this->_getSelectList('table_category',"*",''," status='A' ");
					//$query1 = "SELECT * FROM `table_category`  where status='A' and account_id='".$_SESSION['accountId']."' ";
			 		//$catigory = mysql_query($query1);
				    foreach ($Category as $key => $value) {
				    $datadis['distributor_id']		=$dis_id;		
					$datadis['category_id']			=$value->category_id;	
					$id=$this->_dbInsert($datadis,'table_distributors_category');
				    }




				}
				
		  }
		    $row++;


		  
		 }

			fclose($file);	  

	   if($row<=1)
	   	return "no";
	   else 
	  	return $str_err_array;
	}
/************************************* End Upload Distributor ***************************************/



/************************************* Start Upload Customer ***************************************/


function uploadCustomerFile(){		
		
		$fields = array('Customer Name*','Phone No1*','Phone No2','Phone No3','Customer Address','Customer City*','State*','District*','Taluka Name*','zipcode','Contact Person1*','contact Phone No1*','Contact Person2','contact Phone No2','Contact Person3','contact Phone No3','Email-ID1*','Email-ID2','Email-ID3','Number To Send SMS*','Customer Class','Customer Region','Customer Code*','Aadhar No','Pan No','Interested');
		
		$file = fopen($_FILES['fileToUpload']['tmp_name'],"r");
		$strErr="";
		$row=0;		
		while(! feof($file)){		  
			  $arrFile=fgetcsv($file);	
			  $flag=true;
		 if($row>0 && is_array($arrFile)) {	

		 
		 
		  if(empty($arrFile[0]) || $arrFile[0]=="Customer Name*" || empty($arrFile[1]) || empty($arrFile[5]) || empty($arrFile[6]) || empty($arrFile[7]) || empty($arrFile[8]) || empty($arrFile[10]) || empty($arrFile[11]) || empty($arrFile[16]) || empty($arrFile[19]) || empty($arrFile[22]) ){
		  
		  if(empty($arrFile[0]) || $arrFile[0]=="Customer Name*" ){
					$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5]." ,".$arrFile[6]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].",".$arrFile[10]."  ,".$arrFile[11].",".$arrFile[12]." ,".$arrFile[13]." ,".$arrFile[14].",".$arrFile[15]." ,".$arrFile[16].",".$arrFile[17]."  ,".$arrFile[18].",".$arrFile[19]." , ".$arrFile[20]." ,".$arrFile[21]." ,".$arrFile[22]." ,".$arrFile[23]." , ".$arrFile[24]." ,".$arrFile[25]." ,    ".$fields[0]." shouldn't be empty  \n";
					
				 }
				 else if(empty($arrFile[1])){
					$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5]." ,".$arrFile[6]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].",".$arrFile[10]."  ,".$arrFile[11].",".$arrFile[12]." ,".$arrFile[13]." ,".$arrFile[14].",".$arrFile[15]." ,".$arrFile[16].",".$arrFile[17]."  ,".$arrFile[18].",".$arrFile[19]." , ".$arrFile[20]." ,".$arrFile[21]." ,".$arrFile[22]." ,".$arrFile[23]." , ".$arrFile[24]." ,".$arrFile[25]." , ".$fields[1]." shouldn't be empty  \n";
				 }
				else if(empty($arrFile[5])){
					$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5]." ,".$arrFile[6]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].",".$arrFile[10]."  ,".$arrFile[11].",".$arrFile[12]." ,".$arrFile[13]." ,".$arrFile[14].",".$arrFile[15]." ,".$arrFile[16].",".$arrFile[17]."  ,".$arrFile[18].",".$arrFile[19]." , ".$arrFile[20]." ,".$arrFile[21]." ,".$arrFile[22]." ,".$arrFile[23]." , ".$arrFile[24]." ,".$arrFile[25]." ,  ".$fields[5]." shouldn't be empty  \n";
				 }
				 else if(empty($arrFile[6])){
					$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5]." ,".$arrFile[6]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].",".$arrFile[10]."  ,".$arrFile[11].",".$arrFile[12]." ,".$arrFile[13]." ,".$arrFile[14].",".$arrFile[15]." ,".$arrFile[16].",".$arrFile[17]."  ,".$arrFile[18].",".$arrFile[19]." ,".$arrFile[20]." ,".$arrFile[21]." ,".$arrFile[22]." ,".$arrFile[23]." , ".$arrFile[24]." ,".$arrFile[25]." , ".$fields[6]." shouldn't be empty  \n";
				 }
				 else if(empty($arrFile[7])){
					$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5]." ,".$arrFile[6]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].",".$arrFile[10]."  ,".$arrFile[11].",".$arrFile[12]." ,".$arrFile[13]." ,".$arrFile[14].",".$arrFile[15]." ,".$arrFile[16].",".$arrFile[17]."  ,".$arrFile[18].",".$arrFile[19]." , ".$arrFile[20]." ,".$arrFile[21]." ,".$arrFile[22]." ,".$arrFile[23]." , ".$arrFile[24]." ,".$arrFile[25]." ,  ".$fields[7]." shouldn't be empty  \n";
				 }
				 else if(empty($arrFile[8])){
					$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5]." ,".$arrFile[6]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].",".$arrFile[10]."  ,".$arrFile[11].",".$arrFile[12]." ,".$arrFile[13]." ,".$arrFile[14].",".$arrFile[15]." ,".$arrFile[16].",".$arrFile[17]."  ,".$arrFile[18].",".$arrFile[19]." , ".$arrFile[20]." ,".$arrFile[21]." ,".$arrFile[22]." ,".$arrFile[23]." , ".$arrFile[24]." , ".$arrFile[25]." , ".$fields[8]." shouldn't be empty  \n";
				 }
				else if(empty($arrFile[10])){
					$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5]." ,".$arrFile[6]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].",".$arrFile[10]."  ,".$arrFile[11].",".$arrFile[12]." ,".$arrFile[13]." ,".$arrFile[14].",".$arrFile[15]." ,".$arrFile[16].",".$arrFile[17]."  ,".$arrFile[18].",".$arrFile[19]." , ".$arrFile[20]." ,".$arrFile[21]." ,".$arrFile[22]." ,".$arrFile[23]." , ".$arrFile[24]." ,".$arrFile[25]." ,  ".$fields[10]." shouldn't be empty  \n";
				 }
				 else if(empty($arrFile[11])){
					$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5]." ,".$arrFile[6]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].",".$arrFile[10]."  ,".$arrFile[11].",".$arrFile[12]." ,".$arrFile[13]." ,".$arrFile[14].",".$arrFile[15]." ,".$arrFile[16].",".$arrFile[17]."  ,".$arrFile[18].",".$arrFile[19]." ,".$arrFile[20]." ,".$arrFile[21]." ,".$arrFile[22]." ,".$arrFile[23]." , ".$arrFile[24]." ,".$arrFile[25]." , ".$fields[11]." shouldn't be empty  \n";
				 }
				 else if(empty($arrFile[16])){
					$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5]." ,".$arrFile[6]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].",".$arrFile[10]."  ,".$arrFile[11].",".$arrFile[12]." ,".$arrFile[13]." ,".$arrFile[14].",".$arrFile[15]." ,".$arrFile[16].",".$arrFile[17]."  ,".$arrFile[18].",".$arrFile[19]." , ".$arrFile[20]." ,".$arrFile[21]." ,".$arrFile[22]." ,".$arrFile[23]." , ".$arrFile[24]." ,".$arrFile[25]." , ".$fields[16]." shouldn't be empty  \n";
				 }
				else if(empty($arrFile[19])){
					$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5]." ,".$arrFile[6]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].",".$arrFile[10]."  ,".$arrFile[11].",".$arrFile[12]." ,".$arrFile[13]." ,".$arrFile[14].",".$arrFile[15]." ,".$arrFile[16].",".$arrFile[17]."  ,".$arrFile[18].",".$arrFile[19]." , ".$arrFile[20]." ,".$arrFile[21]." ,".$arrFile[22]." ,".$arrFile[23]." , ".$arrFile[24]." ,".$arrFile[25]." ,  ".$fields[19]." shouldn't be empty  \n";
				 }
				 else if(empty($arrFile[22])){
					$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5]." ,".$arrFile[6]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].",".$arrFile[10]."  ,".$arrFile[11].",".$arrFile[12]." ,".$arrFile[13]." ,".$arrFile[14].",".$arrFile[15]." ,".$arrFile[16].",".$arrFile[17]."  ,".$arrFile[18].",".$arrFile[19]." ,".$arrFile[20]." ,".$arrFile[21]." ,".$arrFile[22]." ,".$arrFile[23]." , ".$arrFile[24]." ,".$arrFile[25]." , ".$fields[22]." shouldn't be empty  \n";
				 }
		  
		  			$str_err_array=array($strErr);
					$flag =false;
		  }
		  
		  
		  
		  
		  
		  
		  
		  
		  
// Customer Check
		  if($flag){
					$condi=	" account_id=".$_SESSION['accountId']." AND customer_phone_no='".mysql_real_escape_string(strtolower($arrFile[1]))."' AND  LOWER(customer_name)='".mysql_real_escape_string( $this->clean(strtolower($arrFile[0])))."'";
					$aDisRec=$this->_getSelectList('table_customer','*','',$condi);
					if(is_array($aDisRec)) {
						$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5]." ,".$arrFile[6]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].",".$arrFile[10]."  ,".$arrFile[11].",".$arrFile[12]." ,".$arrFile[13]." ,".$arrFile[14].",".$arrFile[15]." ,".$arrFile[16].",".$arrFile[17]."  ,".$arrFile[18].",".$arrFile[19].",".$arrFile[20].",".$arrFile[21].",".$arrFile[22].",".$arrFile[23].",".$arrFile[24]." ,".$arrFile[25]." ,customer already exists in the system. \n";
						$str_err_array=array($strErr);
						$flag =false;
					}
				}
				



		if($flag){				
				
				if (!is_numeric($arrFile[1]) || !is_numeric($arrFile[19])) {					
					$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5]." ,".$arrFile[6]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].",".$arrFile[10]."  ,".$arrFile[11].",".$arrFile[12]." ,".$arrFile[13]." ,".$arrFile[14].",".$arrFile[15]." ,".$arrFile[16].",".$arrFile[17]."  ,".$arrFile[18].",".$arrFile[19].",".$arrFile[20].",".$arrFile[21]." ,".$arrFile[22].",".$arrFile[23].",".$arrFile[24]." ,".$arrFile[25]." ,Please Provide the valid Phone Number. \n";				
					$str_err_array=array($strErr);						
					$flag=false;					
				} 
		
		}
// State Check


if($flag){		
				
			// Get State ID with the help of State Name		
			if (in_array(strtolower(trim($arrFile[6])), $this->indian_all_states)) 
			{		
					$condi=" LOWER(state_name)='".mysql_real_escape_string(strtolower($arrFile[6]))."'";
				 	$StateRec=$this->_getSelectList2('state',"state_id",'',$condi);
					$state_id=$StateRec[0]->state_id;
					if(!is_array($StateRec)){
						$state = array();					
						$state['state_name']=mysql_real_escape_string( $this->clean($arrFile[6]));
						$state['last_update_date']=date('Y-m-d');
						$state['last_update_status']='New';
						$state_id=$this->_dbInsert($state,'state');
					}
			} else {
				$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5]." ,".$arrFile[6]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].",".$arrFile[10]."  ,".$arrFile[11].",".$arrFile[12]." ,".$arrFile[13]." ,".$arrFile[14].",".$arrFile[15]." ,".$arrFile[16].",".$arrFile[17]."  ,".$arrFile[18].",".$arrFile[19]." ,".$arrFile[20].",".$arrFile[21]." ,".$arrFile[22].",".$arrFile[23].",".$arrFile[24]." ,".$arrFile[25]." , Sorry No State exists. \n";
						$str_err_array=array($strErr);
						$flag =false;
							
			}
		}
	
// City Check			
			 if($flag){					
					$condi=" LOWER(city_name)='".mysql_real_escape_string(strtolower($arrFile[7]))."' AND state_id='".$state_id."'";
				 	$CityRec=$this->_getSelectList2('city',"city_id",'',$condi);	
					$city_id=$CityRec[0]->city_id;
					if(!is_array($CityRec)){
						$city = array();
						$city['state_id']=$this->clean($state_id);
						$city['city_name']=mysql_real_escape_string( $this->clean($arrFile[7]));
						$city['last_update_date']=date('Y-m-d');
						$city['last_update_status']='New';
						$city_id=$this->_dbInsert($city,'city');
					}
				 }


/* For Taluka */

		 if($flag){				
				 $condi=" LOWER(taluka_name)='".mysql_real_escape_string($this->clean(strtolower($arrFile[8])))."' AND city_id='".$city_id."'";
		
				 	$TalukaRec=$this->_getSelectList('table_taluka',"taluka_id",'',$condi);	
					$taluka_id= $TalukaRec[0]->taluka_id;
					
					if(sizeof($TalukaRec)<=0){
						//$strErr .="Invalid city name: &nbsp;&nbsp;".implode(",",$arrFile)."";
						//$flag =false;
						$taluka = array();
						$taluka['account_id'] 	=$_SESSION['accountId'];	
						$taluka['city_id']		= $this->clean($city_id);
						$taluka['taluka_name']	= mysql_real_escape_string( $this->clean($arrFile[8]));
						$taluka['status'] 		='A';	
						$taluka_id=$this->_dbInsert($taluka,'table_taluka');
					}
				 }

/* for market */
 if($flag){				
				 $condi=" LOWER(market_name)='".mysql_real_escape_string($this->clean(strtolower($arrFile[5])))."' AND taluka_id='".$taluka_id."'";
		
				 	$MarketRec=$this->_getSelectList(' table_markets',"market_id",'',$condi);	
					$market_id= $MarketRec[0]->market_id;
					
					if(sizeof($MarketRec)<=0){
						//$strErr .="Invalid city name: &nbsp;&nbsp;".implode(",",$arrFile)."";
						//$flag =false;
						$market = array();
						$market['account_id'] 	=$_SESSION['accountId'];	
						$market['taluka_id']	=$taluka_id;
						$market['market_name']	= mysql_real_escape_string( $this->clean($arrFile[5]));
						$market['status'] 		='A';	
						$market_id=$this->_dbInsert($market,' table_markets');
					}
				 }

/*// 	Check Customer Class Name '			 
				if(!empty($arrFile[20]))
				{
					$condi=" LOWER(relationship_code)='".mysql_real_escape_string(strtolower($arrFile[20]))."' ";
				 	$RelationRec=$this->_getSelectList('table_relationship',"relationship_id",'',$condi);
				 	if(is_array($RelationRec))
				 	{
				 	  $relationshipId =$RelationRec[0]->relationship_id;	
				 	}	
					
				} */
// Check customer region

			if(!empty($arrFile[21]))
			   {
			   		$condi=" LOWER(region_name)='".mysql_real_escape_string(strtolower($arrFile[21]))."' ";
				 	$RegionRec=$this->_getSelectList('table_region',"region_id",'',$condi);
				 	if(is_array($RegionRec))
				 	{
				 	  $regionId =$RegionRec[0]->region_id;	
				 	}
				 	else
				 	{
				 		$region = array();
						$region['account_id']=$this->clean($_SESSION['accountId']);		
						$region['region_name']=mysql_real_escape_string( $this->clean($arrFile[21]));
						$region['status']='A';
						$regionId=$this->_dbInsert($region,'table_region');	
				 	}	
				 	


			   }	

// Check for distributor code 				

				if($flag){
					$condi=" LOWER(customer_code)='".mysql_real_escape_string(strtolower($arrFile[22]))."' ";
					$aDisCode=$this->_getSelectList('table_customer','*','',$condi);
					if(is_array($aDisCode)) {
						$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5]." ,".$arrFile[6]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].",".$arrFile[10]."  ,".$arrFile[11].",".$arrFile[12]." ,".$arrFile[13]." ,".$arrFile[14].",".$arrFile[15]." ,".$arrFile[16].",".$arrFile[17]."  ,".$arrFile[18].",".$arrFile[19].",".$arrFile[20].",".$arrFile[21]." ,".$arrFile[22]." ,".$arrFile[23].",".$arrFile[24]." ,".$arrFile[25]." , customer code already exists in the system. \n";
						$str_err_array=array($strErr);
						$flag =false;
					}
				}



			/* display out */
				 if(mysql_real_escape_string(trim(strtolower($arrFile[25])))=='hot')
				   {
				   	$outlet='hot';
				   }
				  else if(mysql_real_escape_string(trim(strtolower($arrFile[25])))=='cold')
				  {
				  $outlet='cold';	
				  } 
				  else{$outlet=''; }


				 
// Database Insert Query				 
				 if($flag){	
					$data = array();		
					//$str = preg_replace('/[^A-Za-z0-9\. -]/', '', $str);
					
															
					$data['account_id']=$this->clean($_SESSION['accountId']);		
					$data['customer_name']= mysql_real_escape_string( $this->clean($arrFile[0]));
					$data['customer_address']= mysql_real_escape_string( $this->clean($arrFile[4]));
					$data['market_id']= $market_id;
					/*add relationship class and region */
					/*$data['relationship_id']	= $relationshipId;*/
					$data['region_id']			= $regionId;

					$data['country']= 'India';
					$data['state']= $this->clean($state_id);
					$data['city']= $this->clean($city_id);
					$data['zipcode']= mysql_real_escape_string($arrFile[9]);
					$data['customer_phone_no']= mysql_real_escape_string($this->clean($arrFile[1]));
					$data['customer_phone_no2']= mysql_real_escape_string($this->clean($arrFile[2]));
					$data['customer_phone_no3']= mysql_real_escape_string($this->clean($arrFile[3]));
					$data['customer_email']= mysql_real_escape_string($arrFile[16]);
					$data['customer_email2']= mysql_real_escape_string($arrFile[17]);
					$data['customer_email3']= mysql_real_escape_string($arrFile[18]);
					$data['contact_person']= mysql_real_escape_string($this->clean($arrFile[10]));
					$data['contact_person2']= mysql_real_escape_string($this->clean($arrFile[12]));
					$data['contact_person3']= mysql_real_escape_string($this->clean($arrFile[14]));
					$data['contact_number']= mysql_real_escape_string($this->clean($arrFile[11]));
					$data['contact_number2']= mysql_real_escape_string($this->clean($arrFile[13]));
					$data['contact_number3']= mysql_real_escape_string($this->clean($arrFile[15]));
					$data['sms_number']= mysql_real_escape_string($this->clean($arrFile[19]));
					$data['customer_code']= mysql_real_escape_string($this->clean($arrFile[22]));
					/* new fields added */

					$data['customer_aadhar_no']= mysql_real_escape_string($this->clean($arrFile[23]));
					$data['customer_pan_no']= mysql_real_escape_string($this->clean($arrFile[24]));
					$data['taluka_id']= mysql_real_escape_string($taluka_id);
					$data['display_outlet']= $outlet;

					$data['start_date']= date('Y-m-d');
					$data['end_date']= $this->clean($_SESSION['EndDate']);	
					$data['last_update_date']= date('Y-m-d');
					$data['status']= 'A';
					$dis_id=$this->_dbInsert($data,'table_customer');
					
					/*$category=mysql_real_escape_string($arrFile[18]);
					$categoryList = explode(",", $category);

					if($arrFile[18]!=''){
					for($i=0;$i<count($categoryList);$i++){
						$condition=	" LOWER(category_name)='".strtolower($categoryList[$i])."'";
						$col=$this->_getSelectList('table_category','category_id','',$condition);
						if(!is_array($col)){
						
						$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5]." ,".$arrFile[6]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].",".$arrFile[10]."  ,".$arrFile[11].",".$arrFile[12]." ,".$arrFile[13]." ,".$arrFile[14].",".$arrFile[15]." ,".$arrFile[16].",".$arrFile[17]."  ,".$arrFile[18].",".$arrFile[19]." ,Color Error. \n";
						$str_err_array=array($strErr);
						$flag =false;
						}

						$data1['distributor_id']= $this->clean($dis_id);
						$data1['category_id']= $this->clean($col[0]->category_id);
						$col_id=$this->_dbInsert($data1,'table_distributors_category');
						}
					}*/

					/*-----Customer assign to all categories-----*/
					/*if($dis_id!="")	
					$Category=$this->_getSelectList('table_category',"*",''," status='A' ");
					//$query1 = "SELECT * FROM `table_category`  where status='A' and account_id='".$_SESSION['accountId']."' ";
			 		//$catigory = mysql_query($query1);
				    foreach ($Category as $key => $value) {
				    $datadis['distributor_id']		=$dis_id;		
					$datadis['category_id']			=$value->category_id;	
					$id=$this->_dbInsert($datadis,'table_distributors_category');
				    }
*/



				}
				
		  }
		    $row++;
		  
		 }
			fclose($file);	  
	   if($row<=1)
	   	return "no";
	   else 
	  	return $str_err_array;
	}
/************************************* End Upload Customer ***************************************/	



/************************************* Start Upload Retailer ***************************************/	


	function uploadRetailerFile(){		
	
	//$fields = array('Retailer Name*','Phone No1*','Phone No2','Retailer Address','City*','State*','District*','Taluka Name*','Zipcode','Contact Person1*','contact Phone No1*' ,'Contact Person2','Contact Phone No2','Email-ID1*','Email-ID2','Retailer Class','Route Name','Retailer Channel','Distributor Code','Interested','Retailer Type','Aadhar No','Pan No','Retailer Code*', 'Division');
	
	$fields = array('Dealer Name*','Phone No1*','Phone No2','Dealer Address','State*','City*','Zipcode*','Contact Person1*','contact Phone No1*' ,'Contact Person2','Contact Phone No2','Email-ID1*','Email-ID2','Route Name','Distributor Code*','Aadhar No','Pan No','Dealer Code*', 'Division*','Brand*','Country*','Region*','Zone*', 'Area');
	
		$file = fopen($_FILES['fileToUpload']['tmp_name'],"r");
		$strErr="";
		$row=0;		
		while(! feof($file)){		  
			  $arrFile=fgetcsv($file);
			  // echo "<pre>";
			  // print_r($arrFile);
			  $flag=true;
		if($row>0 && is_array($arrFile)) {	
		


		// $relCode       = mysql_real_escape_string(trim($arrFile[12]));
		$division_name = mysql_real_escape_string(strtolower($arrFile[18]));
		$brand_name    = mysql_real_escape_string(strtolower($arrFile[19]));
		
			  
			 if(empty($arrFile[0]) || $arrFile[0]=="Dealer Name*" || empty($arrFile[1]) || empty($arrFile[4]) || empty($arrFile[5]) || empty($arrFile[6]) || empty($arrFile[7]) || empty($arrFile[8]) || empty($arrFile[11]) || empty($arrFile[14]) ||  empty($arrFile[17]) || empty($arrFile[18]) || empty($arrFile[19]) || empty($arrFile[20]) || empty($arrFile[21]) || empty($arrFile[22]) ){
		  
		  		if(empty($arrFile[0]) || $arrFile[0]=="Dealer Name*" ){
					$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5].",".$arrFile[6]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].",".$arrFile[10]."  ,".$arrFile[11].",".$arrFile[12]." ,".$arrFile[13].", ".$arrFile[14]."  ,".$arrFile[15].",".$arrFile[16]." ,".$arrFile[17].",".$arrFile[18]." ,".$arrFile[19].",".$arrFile[20]."  ,".$arrFile[21].",".$arrFile[22]." ,".$arrFile[23]." ,".$fields[0]." shouldn't be empty  \n";
				 }
				  else if(empty($arrFile[1])){
					$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5].",".$arrFile[6]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].",".$arrFile[10]."  ,".$arrFile[11].",".$arrFile[12]." ,".$arrFile[13].", ".$arrFile[14]."  ,".$arrFile[15].",".$arrFile[16]." ,".$arrFile[17].",".$arrFile[18]." ,".$arrFile[19].",".$arrFile[20]."  ,".$arrFile[21].",".$arrFile[22]." ,".$arrFile[23]." , ".$fields[1]." shouldn't be empty  \n";
				 }
				 else if(empty($arrFile[4])){
					$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5].",".$arrFile[6]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].",".$arrFile[10]."  ,".$arrFile[11].",".$arrFile[12]." ,".$arrFile[13].", ".$arrFile[14]."  ,".$arrFile[15].",".$arrFile[16]." ,".$arrFile[17].",".$arrFile[18]." ,".$arrFile[19].",".$arrFile[20]."  ,".$arrFile[21].",".$arrFile[22]." ,".$arrFile[23].", ".$fields[4]." shouldn't be empty  \n";
				 }
				else if(empty($arrFile[5])){
					$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5].",".$arrFile[6]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].",".$arrFile[10]."  ,".$arrFile[11].",".$arrFile[12]." ,".$arrFile[13].", ".$arrFile[14]."  ,".$arrFile[15].",".$arrFile[16]." ,".$arrFile[17].",".$arrFile[18]." ,".$arrFile[19].",".$arrFile[20]."  ,".$arrFile[21].",".$arrFile[22]." ,".$arrFile[23].", ".$fields[5]." shouldn't be empty  \n";
				 }
				 else if(empty($arrFile[6])){
					$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5].",".$arrFile[6]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].",".$arrFile[10]."  ,".$arrFile[11].",".$arrFile[12]." ,".$arrFile[13].", ".$arrFile[14]."  ,".$arrFile[15].",".$arrFile[16]." ,".$arrFile[17].",".$arrFile[18]." ,".$arrFile[19].",".$arrFile[20]."  ,".$arrFile[21].",".$arrFile[22]." ,".$arrFile[23].", ".$fields[6]." shouldn't be empty  \n";
				 }
				 else if(empty($arrFile[7])){
					$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5].",".$arrFile[6]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].",".$arrFile[10]."  ,".$arrFile[11].",".$arrFile[12]." ,".$arrFile[13].", ".$arrFile[14]."  ,".$arrFile[15].",".$arrFile[16]." ,".$arrFile[17].",".$arrFile[18]." ,".$arrFile[19].",".$arrFile[20]."  ,".$arrFile[21].",".$arrFile[22]." ,".$arrFile[23]." , ".$fields[7]." shouldn't be empty  \n";
				 }
				 else if(empty($arrFile[8])){
					$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5].",".$arrFile[6]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].",".$arrFile[10]."  ,".$arrFile[11].",".$arrFile[12]." ,".$arrFile[13].", ".$arrFile[14]."  ,".$arrFile[15].",".$arrFile[16]." ,".$arrFile[17].",".$arrFile[18]." ,".$arrFile[19].",".$arrFile[20]."  ,".$arrFile[21].",".$arrFile[22]." ,".$arrFile[23].", ".$fields[8]." shouldn't be empty  \n";
				 }
				else if(empty($arrFile[11])){
					$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5].",".$arrFile[6]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].",".$arrFile[10]."  ,".$arrFile[11].",".$arrFile[12]." ,".$arrFile[13].", ".$arrFile[14]."  ,".$arrFile[15].",".$arrFile[16]." ,".$arrFile[17].",".$arrFile[18]." ,".$arrFile[19].",".$arrFile[20]."  ,".$arrFile[21].",".$arrFile[22]." ,".$arrFile[23].", ".$fields[11]." shouldn't be empty  \n";
				 }
				 else if(empty($arrFile[14])){
					$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5].",".$arrFile[6]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].",".$arrFile[10]."  ,".$arrFile[11].",".$arrFile[12]." ,".$arrFile[13].", ".$arrFile[14]."  ,".$arrFile[15].",".$arrFile[16]." ,".$arrFile[17].",".$arrFile[18]." ,".$arrFile[19].",".$arrFile[20]."  ,".$arrFile[21].",".$arrFile[22]." ,".$arrFile[23].", ".$fields[14]." shouldn't be empty  \n";
				 }
				 else if(empty($arrFile[17])){
					$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5].",".$arrFile[6]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].",".$arrFile[10]."  ,".$arrFile[11].",".$arrFile[12]." ,".$arrFile[13].", ".$arrFile[14]."  ,".$arrFile[15].",".$arrFile[16]." ,".$arrFile[17].",".$arrFile[18]." ,".$arrFile[19].",".$arrFile[20]."  ,".$arrFile[21].",".$arrFile[22]." ,".$arrFile[23].", ".$fields[17]." shouldn't be empty  \n";
				 }
				 else if(empty($arrFile[18])){
					$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5].",".$arrFile[6]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].",".$arrFile[10]."  ,".$arrFile[11].",".$arrFile[12]." ,".$arrFile[13].", ".$arrFile[14]."  ,".$arrFile[15].",".$arrFile[16]." ,".$arrFile[17].",".$arrFile[18]." ,".$arrFile[19].",".$arrFile[20]."  ,".$arrFile[21].",".$arrFile[22]." ,".$arrFile[23]." , ".$fields[18]." shouldn't be empty  \n";
				 }
				else if(empty($arrFile[19])){
					$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5].",".$arrFile[6]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].",".$arrFile[10]."  ,".$arrFile[11].",".$arrFile[12]." ,".$arrFile[13].", ".$arrFile[14]."  ,".$arrFile[15].",".$arrFile[16]." ,".$arrFile[17].",".$arrFile[18]." ,".$arrFile[19].",".$arrFile[20]."  ,".$arrFile[21].",".$arrFile[22]." ,".$arrFile[23]." , ".$fields[19]." shouldn't be empty  \n";
				 }
				 else if(empty($arrFile[20])){
					$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5].",".$arrFile[6]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].",".$arrFile[10]."  ,".$arrFile[11].",".$arrFile[12]." ,".$arrFile[13].", ".$arrFile[14]."  ,".$arrFile[15].",".$arrFile[16]." ,".$arrFile[17].",".$arrFile[18]." ,".$arrFile[19].",".$arrFile[20]."  ,".$arrFile[21].",".$arrFile[22]." ,".$arrFile[23]." , ".$fields[20]." shouldn't be empty  \n";
				 }

				 else if(empty($arrFile[21])){
					$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5].",".$arrFile[6]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].",".$arrFile[10]."  ,".$arrFile[11].",".$arrFile[12]." ,".$arrFile[13].", ".$arrFile[14]."  ,".$arrFile[15].",".$arrFile[16]." ,".$arrFile[17].",".$arrFile[18]." ,".$arrFile[19].",".$arrFile[20]."  ,".$arrFile[21].",".$arrFile[22]." ,".$arrFile[23].", ".$fields[21]." shouldn't be empty  \n";
				 }
				 else if(empty($arrFile[22])){
					$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5].",".$arrFile[6]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].",".$arrFile[10]."  ,".$arrFile[11].",".$arrFile[12]." ,".$arrFile[13].", ".$arrFile[14]."  ,".$arrFile[15].",".$arrFile[16]." ,".$arrFile[17].",".$arrFile[18]." ,".$arrFile[19].",".$arrFile[20]."  ,".$arrFile[21].",".$arrFile[22]." ,".$arrFile[23].", ".$fields[22]." shouldn't be empty  \n";
				 }
				
		 			$str_err_array=array($strErr);						
						$flag=false;
		  }
		  

/************************** Remove Chain Name 18th March 2015 (Gaurav) *************************/
		// Check retailer chain or group 
		
	/*	if($flag){
		

		$relGroup = preg_replace( "/\r|\n/", "", trim($arrFile[15]));

		$condi = " LOWER(chain_name)='".mysql_real_escape_string(strtolower(trim($relGroup)))."'";
							
		$aRetRec = $this->_getSelectList('table_chain','chain_id','',$condi);				
			
		if(!is_array($aRetRec)){
			$data = array();	
			$data['account_id'] =$_SESSION['accountId'];			
			$data['chain_name'] = $relGroup;
			$data['last_updated_on'] = date('Y-m-d H:i:s');
			$data['status']='A';
			$chain_id = $this->_dbInsert($data,'table_chain');
		} else {
			$chain_id = $aRetRec[0]->chain_id;
		}
		
		// inesert the data into retailer chain relationship
			//$chainData = array();
					
			//$chainData['item_id'] = $item_id;	
			//$chainData['chain_id'] = $chain_id;	
			//$this->_dbInsert($chainData,'table_item_chain_relationship');

		} 
*/
		  
	/************************** Remove Chain Name 18th March 2015 (Gaurav) *************************/	  
	// Check retailer Class

	/*  For channel id */

	/*	$relChannel = mysql_real_escape_string(trim($arrFile[17]));
		if($relChannel!="")
		{	
		if($flag)
		{
		
		$condi=	" LOWER(channel_name)='".strtolower($relChannel)."'";
		$aRetRec= $this->_getSelectList('table_retailer_channel_master','channel_id','',$condi);				
			
					if(!is_array($aRetRec)){
						$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5]." ,".$arrFile[6]." ,".$arrFile[7].",".$arrFile[9].",".$arrFile[10]."  ,".$arrFile[11].",".$arrFile[12]." ,".$arrFile[13]." ,".$arrFile[14]." ,".$arrFile[15].",".$arrFile[16].",".$arrFile[17]." , Sorry No Channel exists. \n";
						$str_err_array=array($strErr);						
						$flag=false;
					} else {
						$channel = $aRetRec[0]->channel_id;
					}

		}
	}*/
		/*  For Retailer  type id */

	/*	$relType = mysql_real_escape_string(trim($arrFile[20]));
		if($relType!="")
		{	
		if($flag)
		{
		
		$condi=	" LOWER(type_name)='".strtolower($relType)."'";
		$aRetRec= $this->_getSelectList('table_retailer_type_master','type_id','',$condi);				
			
					if(!is_array($aRetRec)){
						$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5]." ,".$arrFile[6]." ,".$arrFile[7].",".$arrFile[9].",".$arrFile[10]."  ,".$arrFile[11].",".$arrFile[12]." ,".$arrFile[13]." ,".$arrFile[14]." ,".$arrFile[15].",".$arrFile[19].",".$arrFile[20]." , Sorry No Rtailer Type exits . \n";
						$str_err_array=array($strErr);						
						$flag=false;
					} else {
						$typeval = $aRetRec[0]->type_id;
					}

		}
	}*/
		/*  For distributor id */

		//$disCode = mysql_real_escape_string(trim($arrFile[18]));
		
		$disCode = mysql_real_escape_string(trim($arrFile[14]));
		if($disCode!="")
		{	
		if($flag)
		{
		
		$condi=	" LOWER(distributor_code)='".strtolower($disCode)."'";
		$aRetRec= $this->_getSelectList('table_distributors','distributor_id','',$condi);				
			
					if(!is_array($aRetRec)){
						$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5].",".$arrFile[6]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].",".$arrFile[10]."  ,".$arrFile[11].",".$arrFile[12]." ,".$arrFile[13].", ".$arrFile[14]."  ,".$arrFile[15].",".$arrFile[16]." ,".$arrFile[17].",".$arrFile[18]." ,".$arrFile[19].",".$arrFile[20]."  ,".$arrFile[21].",".$arrFile[22]." ,".$arrFile[23]."  , Sorry No Distributor code exists . \n";
						$str_err_array=array($strErr);						
						$flag=false;
					} else {
						$distributor = $aRetRec[0]->distributor_id;
					}

		}
	  }
	  else
	  {
	  //$distributor= mysql_real_escape_string(trim($arrFile[18]));
	  	$distributor= mysql_real_escape_string(trim($arrFile[14]));
	  }

		
		// if($flag){
		// $condi=	" LOWER(relationship_code)='".strtolower($relCode)."'";
		// $aRetRec= $this->_getSelectList('table_relationship','relationship_id','',$condi);				
		// 	if(!is_array($aRetRec)){
		// 				$data = array();	
		// 				$data['account_id'] =$_SESSION['accountId'];			
		// 				$data['relationship_code'] = $relCode;
		// 				$data['last_update_date'] = date('Y-m-d');
		// 				$data['last_update_status']='New';
		// 				$data['status']='A';
		// 				//echo "<pre>";
		// 				//print_r($data);
		// 				//exit;
		// 				$relationship_id = $this->_dbInsert($data,'table_relationship');
		// 			} else {
		// 				$relationship_id = $aRetRec[0]->relationship_id;
		// 			}
		
		// } 
		  
		// Old Code of Checking STate		
		// if($flag){		
				
		// 	// Get State ID with the help of State Name		
		// 	if (in_array(strtolower(trim($arrFile[4])), $this->indian_all_states)) 
		// 	{		
		// 			$condi=" LOWER(state_name)='".mysql_real_escape_string(strtolower($arrFile[4]))."'";
		// 		 	$StateRec=$this->_getSelectList2('state',"state_id",'',$condi);
		// 			$state_id=$StateRec[0]->state_id;
		// 			if(!is_array($StateRec)){
		// 				$state = array();					
		// 				$state['state_name']=mysql_real_escape_string($arrFile[4]);
		// 				$state['last_update_date']=date('Y-m-d');
		// 				$state['last_update_status']='New';
		// 				$state_id=$this->_dbInsert($state,'state');
		// 			}
		// 	} else {
		// 		$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5]." ,".$arrFile[6]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].",".$arrFile[10]."  ,".$arrFile[11].",".$arrFile[12]." , Sorry No State exists. \n";
		// 				$str_err_array=array($strErr);						
		// 				$flag=false;
							
		// 	}
		// }
		
		 // Old code of Checking City
		 // if($flag){				
			// 	 $condi=" LOWER(city_name)='".mysql_real_escape_string($this->clean(strtolower($arrFile[5])))."' AND state_id='".$state_id."'";
		
			// 	 	$CityRec=$this->_getSelectList2('city',"city_id",'',$condi);	
			// 		$city_id= $CityRec[0]->city_id;
			// 		if(!is_array($CityRec)){
			// 			//$strErr .="Invalid city name: &nbsp;&nbsp;".implode(",",$arrFile)."";
			// 			//$flag =false;
			// 			$city = array();
			// 			$city['state_id']= $this->clean($state_id);
			// 			$city['city_name']= mysql_real_escape_string( $this->clean($arrFile[5]));
			// 			$city['last_update_date']=date('Y-m-d');
			// 			$city['last_update_status']='New';
			// 			$city_id=$this->_dbInsert($city,'city');
			// 		}
			// 	 }
// Fetching Country ID
		if($flag)
		{

			$condi=" LOWER(country_name)='".mysql_real_escape_string(strtolower($arrFile[20]))."'"; 
			$CountryRec=$this->_getSelectList2('country',"country_id",'',$condi);	
			if(isset($CountryRec[0]->country_id) && $CountryRec[0]->country_id>0) {
				$country_id=$CountryRec[0]->country_id;
			} else {
				$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5]." ,".$arrFile[6]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].",".$arrFile[10]."  ,".$arrFile[11].",".$arrFile[12]." ,".$arrFile[13]." ,".$arrFile[14].",".$arrFile[15]." ,".$arrFile[16].",".$arrFile[17]."  ,".$arrFile[18].",".$arrFile[19].",".$arrFile[20].",".$arrFile[21].",".$arrFile[22].",".$arrFile[23]." , Country Not Found. \n";
						$str_err_array=array($strErr);
						$flag =false;
							
			}



		}

// Fetching Region ID
		if($flag)
		{
			$condi=" LOWER(region_name)='".mysql_real_escape_string(strtolower($arrFile[21]))."' and country_id = '".$country_id."'"; 
			$RegionRec=$this->_getSelectList2('table_region',"region_id",'',$condi);	
			

			if(isset($RegionRec[0]->region_id) && $RegionRec[0]->region_id>0) {
				$region_id=$RegionRec[0]->region_id;
			} else {
				$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5]." ,".$arrFile[6]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].",".$arrFile[10]."  ,".$arrFile[11].",".$arrFile[12]." ,".$arrFile[13]." ,".$arrFile[14].",".$arrFile[15]." ,".$arrFile[16].",".$arrFile[17]."  ,".$arrFile[18].",".$arrFile[19].",".$arrFile[20].",".$arrFile[21].",".$arrFile[22].",".$arrFile[23]." , Region Not Found. \n";
						$str_err_array=array($strErr);
						$flag =false;
							
			}


		}		

// Fetching Zone ID
		if($flag)
		{
			$condi=" LOWER(zone_name)='".mysql_real_escape_string(strtolower($arrFile[22]))."' and region_id = '".$region_id."'"; 
			$ZoneRec=$this->_getSelectList2('table_zone',"zone_id",'',$condi);	
			


			if(isset($ZoneRec[0]->zone_id) && $ZoneRec[0]->zone_id>0) {
				$zone_id=$ZoneRec[0]->zone_id;
			} else {
				$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5]." ,".$arrFile[6]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].",".$arrFile[10]."  ,".$arrFile[11].",".$arrFile[12]." ,".$arrFile[13]." ,".$arrFile[14].",".$arrFile[15]." ,".$arrFile[16].",".$arrFile[17]."  ,".$arrFile[18].",".$arrFile[19].",".$arrFile[20].",".$arrFile[21].",".$arrFile[22].",".$arrFile[23]." , Zone Not Found. \n";
						$str_err_array=array($strErr);
						$flag =false;
							
			}




		}			

// Fetching State ID
		if($flag)
		{
			$condi=" LOWER(state_name)='".mysql_real_escape_string(strtolower($arrFile[4]))."' and zone_id = '".$zone_id."'"; 
			$StateRec=$this->_getSelectList2('state',"state_id",'',$condi);	
			$state_id=$StateRec[0]->state_id;
			if(!is_array($StateRec)){
				$state = array();
				$state['zone_id']=$this->clean($zone_id);
				$state['state_name']=mysql_real_escape_string( $this->clean($arrFile[4]));
				$state['last_update_date']=date('Y-m-d');
				$state['last_update_status']='New';
				$state_id=$this->_dbInsert($state,'state');
			}
		}

// Fetching City ID
		// if($flag)
		// {
		// 	$condi=" LOWER(city_name)='".mysql_real_escape_string(strtolower($arrFile[6]))."' and state_id = '".$state_id."'"; 
		// 	$CityRec=$this->_getSelectList2('city',"city_id",'',$condi);	
		// 	$city_id=$CityRec[0]->city_id;
		// }

		// AJAY@2017-05-23 live back to old record
			 if($flag){					
					$condi=" LOWER(city_name)='".mysql_real_escape_string(strtolower($arrFile[5]))."' AND state_id='".$state_id."'";
				 	$CityRec=$this->_getSelectList2('city',"city_id",'',$condi);	
					$city_id=$CityRec[0]->city_id;
					if(!is_array($CityRec)){
						$city = array();
						$city['state_id']=$this->clean($state_id);
						$city['city_name']=mysql_real_escape_string( $this->clean($arrFile[5]));
						$city['last_update_date']=date('Y-m-d');
						$city['last_update_status']='New';
						$city_id=$this->_dbInsert($city,'city');
					}
				 }
	/* For Taluka */

		/* if($flag){				
				 $condi=" LOWER(taluka_name)='".mysql_real_escape_string($this->clean(strtolower($arrFile[7])))."' AND city_id='".$city_id."'";
		
				 	$TalukaRec=$this->_getSelectList('table_taluka',"taluka_id",'',$condi);	
					$taluka_id= $TalukaRec[0]->taluka_id;
					
					if(sizeof($TalukaRec)<=0){
						//$strErr .="Invalid city name: &nbsp;&nbsp;".implode(",",$arrFile)."";
						//$flag =false;
						$taluka = array();
						$taluka['account_id'] 	=$_SESSION['accountId'];	
						$taluka['city_id']		= $this->clean($city_id);
						$taluka['taluka_name']	= mysql_real_escape_string( $this->clean($arrFile[7]));
						$taluka['status'] 		='A';	
						$taluka_id=$this->_dbInsert($taluka,'table_taluka');
					}
				 }
*/
				 
/* for market */

 				if($flag){				
					// $condi=" LOWER(market_name)='".mysql_real_escape_string($this->clean(strtolower($arrFile[26])))."' AND taluka_id='".$taluka_id."'";
					
					 $condi=" LOWER(market_name)='".mysql_real_escape_string($this->clean(strtolower($arrFile[23])))."'";
			
					 	$MarketRec=$this->_getSelectList(' table_markets',"market_id",'',$condi);	
						$market_id= $MarketRec[0]->market_id;
						
						if(sizeof($MarketRec)<=0){
							//$strErr .="Invalid city name: &nbsp;&nbsp;".implode(",",$arrFile)."";
							//$flag =false;
							$market = array();
							$market['account_id'] 	=$_SESSION['accountId'];	
						//	$market['taluka_id']	=$taluka_id;
							$market['market_name']	= mysql_real_escape_string( $this->clean($arrFile[23]));
							$market['status'] 		='A';	
							$market_id=$this->_dbInsert($market,' table_markets');
						}
				}		 

				 
		if($flag){	
			//echo mysql_real_escape_string(strtolower( $this->clean($arrFile[0])))."<br>";			
$condi=	" LOWER(retailer_name)='".mysql_real_escape_string(strtolower($this->clean($arrFile[0])))."' AND state ='".$state_id."' AND city ='".$city_id."' AND retailer_phone_no ='".mysql_real_escape_string($arrFile[1])."'";			
					
					$aRetRec=$this->_getSelectList('table_retailer','*','',$condi);
					if(is_array($aRetRec)) {					
						$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5].",".$arrFile[6]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].",".$arrFile[10]."  ,".$arrFile[11].",".$arrFile[12]." ,".$arrFile[13].", ".$arrFile[14]."  ,".$arrFile[15].",".$arrFile[16]." ,".$arrFile[17].",".$arrFile[18]." ,".$arrFile[19].",".$arrFile[20]."  ,".$arrFile[21].",".$arrFile[22]." ,".$arrFile[23]." , Dealer already exists in the system. \n";
						$str_err_array=array($strErr);						
						$flag=false;
			}
	}
	
	
	if($flag){				
				
				if (!is_numeric($arrFile[1])) {					
					$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5].",".$arrFile[6]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].",".$arrFile[10]."  ,".$arrFile[11].",".$arrFile[12]." ,".$arrFile[13].", ".$arrFile[14]."  ,".$arrFile[15].",".$arrFile[16]." ,".$arrFile[17].",".$arrFile[18]." ,".$arrFile[19].",".$arrFile[20]."  ,".$arrFile[21].",".$arrFile[22]." ,".$arrFile[23]." , Please Provide the valid Phone Number. \n";				
					$str_err_array=array($strErr);						
					$flag=false;					
				} 
		
		}
	
	

			   // Check division name


			if(!empty($division_name) && $flag) {
			   		$condi=" LOWER(division_name)='".$division_name."' ";
				 	$division_name_check =$this->_getSelectList('table_division',"division_id",'',$condi);
					if(sizeof($division_name_check) == 0) {
						$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5].",".$arrFile[6]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].",".$arrFile[10]."  ,".$arrFile[11].",".$arrFile[12]." ,".$arrFile[13].", ".$arrFile[14]."  ,".$arrFile[15].",".$arrFile[16]." ,".$arrFile[17].",".$arrFile[18]." ,".$arrFile[19].",".$arrFile[20]."  ,".$arrFile[21].",".$arrFile[22]." ,".$arrFile[23].", division name not found in the system. \n";
						$str_err_array=array($strErr);
						$flag =false;
					} else {

						$division_id =$division_name_check[0]->division_id;	
					}
				 
			  }	

//check brands


			if(!empty($brand_name) && $flag) {
			   		$condi=" LOWER(brand_name)='".$brand_name."' ";
				 	$brand_name_check =$this->_getSelectList('table_brands',"brand_id",'',$condi);
					if(sizeof($brand_name_check) == 0) {
						$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5].",".$arrFile[6]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].",".$arrFile[10]."  ,".$arrFile[11].",".$arrFile[12]." ,".$arrFile[13].", ".$arrFile[14]."  ,".$arrFile[15].",".$arrFile[16]." ,".$arrFile[17].",".$arrFile[18]." ,".$arrFile[19].",".$arrFile[20]."  ,".$arrFile[21].",".$arrFile[22]." ,".$arrFile[23]." , brand name not found in the system. \n";
						$str_err_array=array($strErr);
						$flag =false;
					} else {

						$brand_id =$brand_name_check[0]->brand_id;	
					}
				 
			}	




	
	/*$route=mysql_real_escape_string($arrFile[14]);
				$routeList = explode(",", $route);
				if($arrFile[14]!=''){
					for($i=0;$i<count($routeList);$i++){
						$condition=	" LOWER(route_name)='".strtolower($routeList[$i])."'";
						$col=$this->_getSelectList('table_route','route_id','',$condition);
							if(!is_array($col)){
							$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5]." ,".$arrFile[6]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].",".$arrFile[10]."  ,".$arrFile[11].",".$arrFile[12]." ,".$arrFile[13]." ,".$arrFile[14]." , Invalid route name. \n";
						$str_err_array=array($strErr);						
						$flag=false;
							
							}
						}
					}
					*/
				 /* display out */
				/* if(mysql_real_escape_string(trim(strtolower($arrFile[19])))=='hot')
				   {
				   	$outlet='hot';
				   }
				  else if(mysql_real_escape_string(trim(strtolower($arrFile[19])))=='cold')
				  {
				  $outlet='cold';	
				  } 
				  else{
				  	$outlet=''; 
				  }	*/
					 
				if($flag){	
					$data = array();					
					$data['account_id']= $this->clean($_SESSION['accountId']);
					$data['relationship_id']= $relationship_id;
					//$data['chain_id']= $chain_id; // Remove Chain Name
					$data['retailer_name']=mysql_real_escape_string($this->clean($arrFile[0]));
					/* Med mofification */
					
					/* Retailer Code */
					$data['retailer_code']=mysql_real_escape_string($this->clean($arrFile[17]));
				//	$data['channel_id']= mysql_real_escape_string($channel);
					$data['distributor_id']= mysql_real_escape_string($distributor);
				//	$data['display_outlet']= mysql_real_escape_string($outlet);
				//	$data['type_id']= mysql_real_escape_string($typeval);


					$data['division_id']			= $division_id;
					$data['brand_id']               = $brand_id;
					/* Ends Med modification */

					$data['retailer_address']= mysql_real_escape_string($arrFile[3]);
					$data['market_id']= $market_id;
					$data['country']= $country_id;
					$data['state']= $state_id;
					$data['city']= $city_id;
					$data['region']= $region_id;
					$data['zone']= $zone_id;
					$data['retailer_phone_no']= mysql_real_escape_string( $this->clean($arrFile[1]));
					$data['retailer_phone_no2']= mysql_real_escape_string( $this->clean($arrFile[2]));
					$data['contact_person']= mysql_real_escape_string( $this->clean($arrFile[7]));
					$data['contact_number']= mysql_real_escape_string( $this->clean($arrFile[8]));
					$data['contact_person2']= mysql_real_escape_string( $this->clean($arrFile[9]));
					$data['contact_number2']= mysql_real_escape_string( $this->clean($arrFile[10]));
					$data['retailer_email']= mysql_real_escape_string($arrFile[11]);
					$data['retailer_email2']= mysql_real_escape_string($arrFile[12]);
					$data['zipcode']= mysql_real_escape_string( $this->clean($arrFile[6]));

					/* Added new fields */
					$data['aadhar_no']= mysql_real_escape_string( $this->clean($arrFile[15]));
					$data['pan_no']= mysql_real_escape_string( $this->clean($arrFile[16]));
				//	$data['retailer_code'] = mysql_real_escape_string($this->clean($arrFile[17]));
				//	$data['taluka_id']= $taluka_id;



					$data['start_date']= date('Y-m-d');
					$data['end_date']= $this->clean($_SESSION['EndDate']);
					$data['last_update_date']=date('Y-m-d');
					$data['last_update_status']='New';
					$data['status']= 'A';
					$data['survey_status']= 'I';

					$ret_id=$this->_dbInsert($data,'table_retailer');			
				
					 
					 //print_r($routeList);
					 //exit;
					 
					 file_put_contents("routename.log",print_r($routeList,true)."\r\n", FILE_APPEND);
					 
					 
					// if($arrFile[15]!=''){
						 $route=mysql_real_escape_string($arrFile[13]);
						 $routeList = explode(",", $route);
						for($i=0;$i<count($routeList);$i++){
							//$route_name=$routeList[$i];
							$condition=	" LOWER(route_name)='".strtolower($routeList[$i])."'";
						$resultset=$this->_getSelectList('table_route','route_id,state_id,city_id','',$condition);
						//file_put_contents("routename.log",print_r($condition,true)."\r\n", FILE_APPEND);
						if(sizeof($resultset)>0 && !empty($resultset) && $resultset[0]->route_id!="") {
								$route_id = $resultset[0]->route_id;
							 
									$stateArr = array_filter(explode(',', $resultset[0]->state_id));
									$cityArr = array_filter(explode(',', $resultset[0]->city_id));
									array_push($stateArr, $state_id);
									array_push($cityArr, $city_id);
									$comma_separated_state = implode(",", array_unique($stateArr));
									$comma_separated_city = implode(",", array_unique($cityArr));
								
								
										$data3=array();
										$data3['state_id']		=  $comma_separated_state;
										$data3['city_id']		=  $comma_separated_city;
										$result = $this->_dbUpdate($data3,'table_route'," route_id='".$route_id."'");
										$data5=array();
										$data5['account_id']	= $_SESSION['accountId'];
										$data5['route_id']	= $route_id; 
										$data5['retailer_id']	= $ret_id;
										$data5['status']	= 'R';
										$result_id = $this->_dbInsert($data5,'table_route_retailer');
								//file_put_contents("routeupdate.log",print_r($data3,true)."\r\n", FILE_APPEND);
						}
						
						else {				
								$comma_separated_state = $state_id;
								$comma_separated_city = $city_id;
								
								$data2 = array ();		
								$data2['account_id']		=  $_SESSION['accountId'];	
								$data2['state_id']		=  $comma_separated_state;
								$data2['city_id']		=  $comma_separated_city;
								$data2['route_name']		=  $routeList[$i];
								$data2['status']			=  'A';
								
								//file_put_contents("routedata.log",print_r($data2,true)."\r\n", FILE_APPEND);
										$resultRoute = $this->_dbInsert($data2,'table_route');	
										$data4=array();
										$data4['account_id']	= $_SESSION['accountId'];
										$data4['route_id']	= $resultRoute; 
										$data4['retailer_id']	= $ret_id;
										$data5['status']	= 'R';
										$result_id = $this->_dbInsert($data4,'table_route_retailer');		
							 }
	
						}
						 
					// }
			
	
									
			}
				
		}
		$row++;	 	  
		    
		}
		fclose($file);	  
	   if($row<=1)
	   	return "no";
	   else 
	  	return $str_err_array;
		
		
		
		
	
	}


/************************************* End Upload Retailer ***************************************/
/************************************* Start Upload Salesman ***************************************/
	function uploadSalesmanFile(){	
	
		$fields = array('Salesman Name*','State*','City*','Address','Phone No*','Username','Password','Category Name','Salesman Designation','Reporting Person','Division*','Country*','Region*','Zone*');		
		
		$file = fopen($_FILES['fileToUpload']['tmp_name'],"r");
		$strErr="";
		$row=0;		
		
		
		while(! feof($file)){		  
			  $arrFile=fgetcsv($file);	
			  $flag=true;		
			    
			  if($row>0 && is_array($arrFile)) {					
				
				if(empty($arrFile[0]) || $arrFile[0]=="Salesman Name*"){
										
						 //$strErr .= "Row".$row." = ".$fields[0]."shouldn't be empty.<br>";
						 $strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[4]."  ,".$arrFile[5]."  ,".$arrFile[8]." ,".$arrFile[10]." ,".$arrFile[11].",".$arrFile[12]."  ,".$arrFile[13]."  , ".$fields[0]." shouldn't be empty  \n";
						//$flag =false;
						
						 if(empty($arrFile[1])) {
								 
							 //$strErr .= "Row".$row." = ".$fields[2]."shouldn't be empty.<br>";
							 $strErr .= "Error in Row ".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4]."  ,".$arrFile[5]." ,".$arrFile[6]." ,".$arrFile[7].",".$arrFile[8]."  ,".$arrFile[9]."  , ".$fields[1]."State shouldn't be empty  \n";
							//$flag =false;
							
						} elseif(empty($arrFile[2])) {
								 
							 //$strErr .= "Row".$row." = ".$fields[2]."shouldn't be empty.<br>";
							 $strErr .= "Error in Row ".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4]."  ,".$arrFile[5]." ,".$arrFile[6]." ,".$arrFile[7].",".$arrFile[8]."  ,".$arrFile[9]."  , ".$fields[2]."City shouldn't be empty  \n";
							//$flag =false;
							
						} elseif(empty($arrFile[4])) {
											
							 //$strErr .= "Row".$row." = ".$fields[1]."shouldn't be empty.<br>";
							 $strErr .= "Error in Row ".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4]."  ,".$arrFile[5]." ,".$arrFile[6]." ,".$arrFile[7].",".$arrFile[8]."  ,".$arrFile[9]."  , ".$fields[4]."Phone No shouldn't be empty  \n";
							//$flag =false;
							
						}elseif(empty($arrFile[5])) {
											
							 //$strErr .= "Row".$row." = ".$fields[1]."shouldn't be empty.<br>";
							 $strErr .= "Error in Row ".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4]."  ,".$arrFile[5]." ,".$arrFile[6]." ,".$arrFile[7].",".$arrFile[8]."  ,".$arrFile[9]."  , ".$fields[5]."Username shouldn't be empty  \n";
							//$flag =false;
							
						}elseif(empty($arrFile[10])) {
											
							 //$strErr .= "Row".$row." = ".$fields[1]."shouldn't be empty.<br>";
							 $strErr .= "Error in Row ".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4]."  ,".$arrFile[5]." ,".$arrFile[6]." ,".$arrFile[7].",".$arrFile[8]."  ,".$arrFile[9]."  , ".$fields[10]."Division shouldn't be empty  \n";
							//$flag =false;
							
						}elseif(empty($arrFile[11])) {
											
							 //$strErr .= "Row".$row." = ".$fields[1]."shouldn't be empty.<br>";
							 $strErr .= "Error in Row ".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4]."  ,".$arrFile[5]." ,".$arrFile[6]." ,".$arrFile[7].",".$arrFile[8]."  ,".$arrFile[9]."  , ".$fields[11]."Country shouldn't be empty  \n";
							//$flag =false;
							
						}elseif(empty($arrFile[12])) {
											
							 //$strErr .= "Row".$row." = ".$fields[1]."shouldn't be empty.<br>";
							 $strErr .= "Error in Row ".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4]."  ,".$arrFile[5]." ,".$arrFile[6]." ,".$arrFile[7].",".$arrFile[8]."  ,".$arrFile[9]."  , ".$fields[12]."Region shouldn't be empty  \n";
							//$flag =false;
							
						}elseif(empty($arrFile[13])) {
											
							 //$strErr .= "Row".$row." = ".$fields[1]."shouldn't be empty.<br>";
							 $strErr .= "Error in Row ".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4]."  ,".$arrFile[5]." ,".$arrFile[6]." ,".$arrFile[7].",".$arrFile[8]."  ,".$arrFile[9]."  , ".$fields[13]."Zone shouldn't be empty  \n";
							//$flag =false;
							
						}
						$str_err_array=array($strErr);						
						$flag=false;
				
			}
				
				if($flag){
					
					$condi=	" account_id=".$_SESSION['accountId']."  AND  LOWER(salesman_name)='".mysql_escape_string(strtolower($arrFile[0]))."'";
					$aRetRec=$this->_getSelectList('table_salesman','*','',$condi);
					if(is_array($aRetRec)) {						
						$strErr .= "Error in Row ".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4]."  ,".$arrFile[5]." ,".$arrFile[6]." ,".$arrFile[7].",".$arrFile[8]."  ,".$arrFile[9]."  , Salesman already exists in the system  \n";
						$str_err_array=array($strErr);
						$flag =false;	
						
						
					}
				}
				
				if($flag){
					$condi=	" account_id=".$_SESSION['accountId']."  AND  LOWER(username)='".mysql_escape_string(strtolower($arrFile[5]))."'";
					$aRetRec=$this->_getSelectList('table_web_users','*','',$condi);
					if(is_array($aRetRec)) {
						$strErr .= "Error in Row ".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4]."  ,".$arrFile[5]." ,".$arrFile[6]." ,".$arrFile[7].",".$arrFile[8]."  ,".$arrFile[9]."   , Username already exists in the system  \n";
						$str_err_array=array($strErr);
						$flag =false;	
						
						
						
					}
				}
				
				
				
				if($arrFile[9]!=''){
				if($flag){
					$condi=	" account_id=".$_SESSION['accountId']."  AND  LOWER(salesman_name)='".mysql_escape_string(strtolower($arrFile[9]))."'";
					$rptPersonexist=$this->_getSelectList('table_salesman','*','',$condi);
					if(empty($rptPersonexist)) {
						$strErr .= "Error in Row ".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4]."  ,".$arrFile[5]." ,".$arrFile[6]." ,".$arrFile[7].",".$arrFile[8]."  ,".$arrFile[9]."   , Reporting person not exists in the system  \n";
						$str_err_array=array($strErr);
						$flag =false;
					}
					else {						
							$rpt_person_id=$rptPersonexist[0]->salesman_id;
								$condi=	" account_id=".$_SESSION['accountId']."  AND  salesman_id='".$rpt_person_id."'";
								$rptPersonHierarchy=$this->_getSelectList('table_salesman_hierarchy_relationship','*','',$condi);
								if(empty($rptPersonHierarchy)) {
									$strErr .= "Error in Row ".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4]."  ,".$arrFile[5]." ,".$arrFile[6]." ,".$arrFile[7].",".$arrFile[8]."  ,".$arrFile[9]."   ,  Hierarchy of reporting person does not exists in the system \n";
									$str_err_array=array($strErr);
									$flag =false;
								
								}
								else {						
										$rpt_hierarchy_id=$rptPersonHierarchy[0]->hierarchy_id;
										$condi=	" account_id=".$_SESSION['accountId']."  AND  hierarchy_id='".$rpt_hierarchy_id."'";
										$rptPersonHierarchy=$this->_getSelectList('table_salesman_hierarchy','*','',$condi);
										$rpt_sort_order=$rptPersonHierarchy[0]->sort_order;
									}
						}
					}
				}
				
				if($flag){
					$condi=	" account_id=".$_SESSION['accountId']."  AND  LOWER(description)='".mysql_escape_string(strtolower($arrFile[8]))."'";
					$hierarchyExist=$this->_getSelectList('table_salesman_hierarchy','*','',$condi);
					if(empty($hierarchyExist)) {
						$strErr .= "Error in Row ".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4]."  ,".$arrFile[5]." ,".$arrFile[6]." ,".$arrFile[7].",".$arrFile[8]."  ,".$arrFile[9]."  , Hierarchy does not exists in the system. \n";
						$str_err_array=array($strErr);
						$flag =false;
					}
					else {						
							$sal_sort_order=$hierarchyExist[0]->sort_order;
							$sal_hierarchy_id=$hierarchyExist[0]->hierarchy_id;						
						 }
				 }
				
				
				/* Check Division*/
				if($flag)
				{
					$condi="account_id=".$_SESSION['accountId']." AND LOWER(division_name)='".mysql_real_escape_string(strtolower($arrFile[10]))."'";
					 
				 	$divisionRec=$this->_getSelectList('table_division',"division_name,division_id",'',$condi);				  
					if(!is_array($divisionRec)){
						$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5]." ,".$arrFile[6].",".$arrFile[7].",".$arrFile[8].",".$arrFile[9].",".$arrFile[10]." ,Invalid Division. \n";
						$str_err_array=array($strErr);						
						$flag =false;
					}
				}

// Fetching Country ID
		if($flag)
		{
			$condi=" LOWER(country_name)='".mysql_real_escape_string(strtolower($arrFile[11]))."'"; 
			$CountryRec=$this->_getSelectList2('country',"country_id",'',$condi);	
			if(isset($CountryRec[0]->country_id) && $CountryRec[0]->country_id>0) {
				$country_id=$CountryRec[0]->country_id;
			} else {
				$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5]." ,".$arrFile[6]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].",".$arrFile[10]." , Country Not Found. \n";
						$str_err_array=array($strErr);
						$flag =false;
							
			}



		}

// Fetching Region ID
		if($flag)
		{
			$condi=" LOWER(region_name)='".mysql_real_escape_string(strtolower($arrFile[12]))."' and country_id = '".$country_id."'"; 
			$RegionRec=$this->_getSelectList2('table_region',"region_id",'',$condi);	
			

			if(isset($RegionRec[0]->region_id) && $RegionRec[0]->region_id>0) {
				$region_id=$RegionRec[0]->region_id;
			} else {
				$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5]." ,".$arrFile[6]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].",".$arrFile[10]."  ,".$arrFile[11].", Region Not Found. \n";
						$str_err_array=array($strErr);
						$flag =false;
							
			}


		}		

// Fetching Zone ID
		if($flag)
		{
			$condi=" LOWER(zone_name)='".mysql_real_escape_string(strtolower($arrFile[13]))."' and region_id = '".$region_id."'"; 
			$ZoneRec=$this->_getSelectList2('table_zone',"zone_id",'',$condi);	
			


			if(isset($ZoneRec[0]->zone_id) && $ZoneRec[0]->zone_id>0) {
				$zone_id=$ZoneRec[0]->zone_id;
			} else {
				$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5]." ,".$arrFile[6]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].",".$arrFile[10]."  ,".$arrFile[11].",".$arrFile[12].", Zone Not Found. \n";
						$str_err_array=array($strErr);
						$flag =false;
							
			}




		}		

// Fetching State ID
		if($flag)
		{
			$condi=" LOWER(state_name)='".mysql_real_escape_string(strtolower($arrFile[1]))."' and zone_id = '".$zone_id."'"; 
			$StateRec=$this->_getSelectList2('state',"state_id",'',$condi);	
			$state_id=$StateRec[0]->state_id;
			if(!is_array($StateRec)){
				$state = array();
				$state['zone_id']=$this->clean($zone_id);
				$state['state_name']=mysql_real_escape_string( $this->clean($arrFile[5]));
				$state['last_update_date']=date('Y-m-d');
				$state['last_update_status']='New';
				$state_id=$this->_dbInsert($state,'state');
			}
		}

// Fetching City ID
		// if($flag)
		// {
		// 	$condi=" LOWER(city_name)='".mysql_real_escape_string(strtolower($arrFile[6]))."' and state_id = '".$state_id."'"; 
		// 	$CityRec=$this->_getSelectList2('city',"city_id",'',$condi);	
		// 	$city_id=$CityRec[0]->city_id;
		// }

		// AJAY@2017-05-23 live back to old record
			 if($flag){					
					$condi=" LOWER(city_name)='".mysql_real_escape_string(strtolower($arrFile[2]))."' AND state_id='".$state_id."'";
				 	$CityRec=$this->_getSelectList2('city',"city_id",'',$condi);	
					$city_id=$CityRec[0]->city_id;
					if(!is_array($CityRec)){
						$city = array();
						$city['state_id']=$this->clean($state_id);
						$city['city_name']=mysql_real_escape_string( $this->clean($arrFile[6]));
						$city['last_update_date']=date('Y-m-d');
						$city['last_update_status']='New';
						$city_id=$this->_dbInsert($city,'city');
					}
				 }


			 // Check division name


			if(!empty($arrFile[10]) && $flag) {
			   		$condi=" LOWER(division_name)='".mysql_real_escape_string(strtolower($arrFile[10]))."' ";
				 	$division_name_check =$this->_getSelectList('table_division',"division_id,division_code",'',$condi);
				 	
					if(sizeof($division_name_check) == 0) {
						$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5]." ,".$arrFile[6]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].", division name not found in the system. \n";
						$str_err_array=array($strErr);
						$flag =false;
					} else {
						$division_id =$division_name_check[0]->division_id;	
					}
			  }
				
				

				
				 
				$category=mysql_escape_string($arrFile[7]);
				$categoryList = explode(",", $category);
				if($arrFile[7]!=''){
					for($i=0;$i<count($categoryList);$i++){
						$condition=	" LOWER(category_name)='".strtolower($categoryList[$i])."'";
						$col=$this->_getSelectList('table_category','category_id','',$condition);
							if(!is_array($col)){
							$strErr .= "Error in Row ".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4]."  ,".$arrFile[5]." ,".$arrFile[6]." ,".$arrFile[7].",".$arrFile[8]."  ,".$arrFile[9]."    ,  Invalid Category Name. \n";
							$str_err_array=array($strErr);
							$flag =false;
							}
						}
					}
				 
				 
				if($flag){	
					$data = array();	
					$data2=array();	
					$data3=array();	
					$data4=array();				
					$data['account_id']=$_SESSION['accountId'];
					$data['salesman_name']= mysql_escape_string($arrFile[0]);					
					$data['salesman_address']= mysql_escape_string($arrFile[3]);
					$data['region']				= $region_id;
					$data['division_id']			= $division_id;
					$data['country']				= $country_id;
					$data['state']					= $state_id;
					$data['city']					= $city_id;
					$data['zone']					= $zone_id;
					$data['salesman_phome_no']= mysql_escape_string($arrFile[4]);
					$data['start_date']= date('Y-m-d');
					$data['end_date']= $_SESSION['EndDate'];	
					$data['status']= 'A';
					$item_id=$this->_dbInsert($data,'table_salesman');
					
					// exit;
					
					$category=mysql_escape_string($arrFile[7]);
					 $categoryList = explode(",", $category);
					if($arrFile[7]!=''){
					for($i=0;$i<count($categoryList);$i++){
						$condition=	" LOWER(category_name)='".strtolower($categoryList[$i])."'";
						$col=$this->_getSelectList('table_category','category_id','',$condition);
						$data1['salesman_id']= $item_id;
						$data1['category_id']= $col[0]->category_id;
						$col_id=$this->_dbInsert($data1,'table_salesman_category');				
				}				
				}	
				if($arrFile[8]!='' && $arrFile[9]!=''){
						$data2['account_id']=$_SESSION['accountId'];
						$data2['salesman_id']= $item_id;
						$data2['username']= $arrFile[5];
						$data2['password']= md5($arrFile[6]);
						$data2['user_type']= '5';
						$data2['start_date']= $_SESSION['StartDate'];
						$data2['end_date']= $_SESSION['EndDate'];
						$data2['status']= 'A';
						$userId=$this->_dbInsert($data2,'table_web_users');				
						}
						
				if($arrFile[8]!='' && $arrFile[9]==''){
					$data3['account_id']=$_SESSION['accountId'];
					$data3['salesman_id']= $item_id;
					$data3['hierarchy_id']= $sal_hierarchy_id;
					$data3['user_type']= $_SESSION['userLoginType'];
					$data3['status']= '1';
					$relId=$this->_dbInsert($data3,'table_salesman_hierarchy_relationship');					
				}
				
				if($arrFile[9]!='' && $rpt_person_id!='' && $rpt_sort_order!='' && $rpt_hierarchy_id!='' ){	
					
					if($sal_sort_order>$rpt_sort_order){
						$data4['account_id']=$_SESSION['accountId'];
						$data4['salesman_id']= $item_id;
						$data4['hierarchy_id']= $sal_hierarchy_id;										
						$data4['rpt_user_id']= $rpt_person_id;
						$data4['rpt_hierarchy_id']= $rpt_hierarchy_id;
						$data4['user_type']= $_SESSION['userLoginType'];		
						$data4['status']= '1';										
						$relId=$this->_dbInsert($data4,'table_salesman_hierarchy_relationship');	
						}	
						
						else {
							$strErr .= "Error in Row ".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4]."  ,".$arrFile[5]." ,".$arrFile[6]." ,".$arrFile[7].",".$arrFile[8]."  ,".$arrFile[9]." , Reporting person hierarchy is lower than salesman hierarchy. \n";
							$str_err_array=array($strErr);
							$flag =false;
							}					
					}
				
				
		 	 }
		  }
		  $row++;	   
		}		
		fclose($file);	  
	   if($row<=1) 
	   	return "no";
	   else 
	  	return $str_err_array;
	}



/************************************* End Upload Salesman ***************************************/	
/************************************* Start Upload City ***************************************/	

	
	function uploadstatecity() {
		$fields = array('State Name*', 'District Name*','District Code*');	
		$file = fopen($_FILES['fileToUpload']['tmp_name'],"r");
		$strErr="";
		$row=0;		
		while(! feof($file))
		{		  
		$arrFile=fgetcsv($file);
		
		
			
		$flag=true;	
		
		
	 	if($row>0 && is_array($arrFile)) {	
	  
	  
	  
	    if(empty($arrFile[0]) || $arrFile[0]=="State Name*" || empty($arrFile[1]) ||$arrFile[1]=="City Name*" || empty($arrFile[2]) || $arrFile[2]=="District Code*"){
		  
		  		  if(empty($arrFile[0]) || $arrFile[0]=="State Name*" ){
					$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1].",".$arrFile[2].", ".$fields[0]." shouldn't be empty  \n";
				 }
				  else if(empty($arrFile[1]) || $arrFile[1]=="City Name*"){
					$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." , ".$arrFile[1].", ".$fields[1]." shouldn't be empty  \n";
				 }else if(empty($arrFile[2]) || $arrFile[2]=="District Code*"){
					$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." , ".$arrFile[2].", ".$fields[2]." shouldn't be empty  \n";
				 }

		 		$str_err_array=array($strErr);						
				$flag=false;
					
		  	}
		
				 
				 
		// if($flag)
		// {
		// 	$condi=" LOWER(state_name)='".mysql_real_escape_string(strtolower($arrFile[0]))."'";
		// 	$StateRec=$this->_getSelectList2('state',"state_id",'',$condi);
		// 	$state_id=$StateRec[0]->state_id;
		// 	if(!is_array($StateRec) )
		// 	{
		// 		$state = array();					
		// 		$state['state_name']= mysql_real_escape_string( $this->clean($arrFile[0]));
		// 		$state['last_update_date']=date('Y-m-d');
		// 		$state['last_update_status']='New';
		// 		$state_id=$this->_dbInsert($state,'state');
		// 	}
			
		// 	if($state_id=='') {
			
		// 	$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." , data has not been inserted \n";
		// 			$str_err_array=array($strErr);						
		// 			$flag=false;
			
		// 	}
		// }


		if($flag){		
				
			// Get State ID with the help of State Name		
			if (in_array(strtolower(trim($arrFile[0])), $this->indian_all_states)) 
			{		
					$condi=" LOWER(state_name)='".mysql_escape_string(strtolower($arrFile[0]))."'";
				 	$StateRec=$this->_getSelectList2('state',"state_id",'',$condi);
					$state_id=$StateRec[0]->state_id;
					if(!is_array($StateRec)){
						$state = array();					
						$state['state_name']=mysql_escape_string($arrFile[0]);
						$state['last_update_date']=date('Y-m-d');
						$state['last_update_status']='New';
						$state_id=$this->_dbInsert($state,'state');
					}
			} else {
				$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." , state doesn't exists \n";
					$str_err_array=array($strErr);						
					$flag=false;
					
			}
		}





								 
				 
		 if($flag)
		 {					
			$condi=" LOWER(city_code)='".mysql_real_escape_string(strtolower($arrFile[2]))."'  AND state_id = ".$state_id;
			$CityRec=$this->_getSelectList2('city',"city_id",'',$condi);	
			$city_id=$CityRec[0]->city_id;
			
				if(!is_array($CityRec))
				{
					$city = array();
					$city['state_id']= $this->clean($state_id);
					$city['city_name']= mysql_real_escape_string( $this->clean($arrFile[1]));
					$city['city_code']= mysql_real_escape_string( $this->clean($arrFile[2]));
					$city['last_update_date']= date('Y-m-d');
					$city['last_update_status']= 'New';
					$city_id=$this->_dbInsert($city,'city');
				} 
				else  
				{
					$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." , City already exists in the system \n";
					$str_err_array=array($strErr);						
					$flag=false;
				}
		  }
		  }
		$row++;	   

		}	
		fclose($file);	  
	  
	   if($row<=1)
	   	return "no";
	   else 
	  	return $str_err_array;
	
	}
/************************************* End Upload City ***************************************/	
/************************************* Start Upload Stock ***************************************/	

function uploadstockDetails() {
	

	$fields = array('Category Name*','Item Code*','Cases Size*','Number Of Cases*');
	$file = fopen($_FILES['fileToUpload']['tmp_name'],"r");
	$strErr="";
	$resultset = "";
	$row=0;	
	$data = array();
	$data2 = array();
	
	while(!feof($file))
	{		  
		$arrFile=fgetcsv($file);	
		$flag=true;		  
		
		// echo "<pre>"; print_r($arrFile); echo $arrFile[7];
		
		if($row>0 && is_array($arrFile)) 
		{	
			if(empty($arrFile[0]) || $arrFile[0]=="Category Name*" || empty($arrFile[1]) ||$arrFile[1]=="Item Code*" || empty($arrFile[2]) ||$arrFile[2]=="Cases Size*" || empty($arrFile[3]) ||$arrFile[3]=="Number Of Cases*"){
		if(empty($arrFile[0])){
			$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2]." ,".$arrFile[3]." , ".$fields[0]." shouldn't be empty  \n";
			
		} elseif(empty($arrFile[1])){
			$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2]." ,".$arrFile[3]." , ".$fields[1]." shouldn't be empty  \n";
		} elseif(empty($arrFile[2])){
			$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2]." ,".$arrFile[3]." , ".$fields[2]." shouldn't be empty  \n";
		} elseif(empty($arrFile[3])) {
			$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2]." ,".$arrFile[3]." , ".$fields[3]." shouldn't be empty  \n";					
		} 
			$str_err_array=array($strErr);						
			$flag=false;
		} 
		
	// Check category exists in the system.
	if($flag){
	$condi =" LOWER(category_name)='".mysql_real_escape_string(strtolower(trim($arrFile[0])))."'";
	$resultset = $this->_getSelectList('table_category','category_id','',$condi);
		if(!is_array($resultset) && empty($resultset) && $resultset[0]->category_id == "") {
				$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2]." ,".$arrFile[3]." ,Category doesn't exists in the system  \n";	
			$str_err_array=array($strErr);	
			$flag=false;
		} else {
			$category_id = $resultset[0]->category_id;
		}
	}
		
	// Check item exists in the system.		
	if($flag){
	$condi =" LOWER(item_code)='".mysql_real_escape_string(strtolower(trim($arrFile[1])))."'";
	$resultset = $this->_getSelectList('table_item','item_id','',$condi);
		if(!is_array($resultset) && empty($resultset) && $resultset[0]->item_id == "") {
		$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2]." ,".$arrFile[3]." ,Item doesn't exists in the system \n";	
			$str_err_array=array($strErr);	
			$flag=false;
			
		} else {
			$item_id = $resultset[0]->item_id;
		}
	}
		
	// Check attribute(size) exists in the system.		

	if($flag){  $condi =" LOWER(case_size)='".mysql_real_escape_string(strtolower(trim($arrFile[2])))."'";
	$resultset = $this->_getSelectList('table_cases','case_id','',$condi);
	
	if(!is_array($resultset) && empty($resultset) && $resultset[0]->case_id == "") {
		
		$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2]." ,".$arrFile[3]." ,Color Code doesn't exists in the system \n";	
			$str_err_array=array($strErr);	
			$flag=false;
		
		} else {
			$case_id = $resultset[0]->case_id;
		}
	}			
	
	
	// Insert/Update data in the system			
	if($flag){	
		$condi =" attribute_value_id ='".$case_id."' AND item_id ='".$item_id."' AND category_id ='".$category_id."'";
		$resultset = $this->_getSelectList('table_item_ndc_stock','stock_id, stock_value','',$condi);
		
		$data = array(); // AJAY@2016-08-21
		$data2 = array(); // AJAY@2016-08-21
		
		$data['account_id']= $this->clean($_SESSION['accountId']);		
		$data['item_id']= $this->clean($item_id);
		$data['category_id']= $this->clean($category_id);
		$data['attribute_value_id']= $this->clean($case_id);
		//$data['stock_value']= $arrFile[3]+$resultset[0]->stock_value;
		$data['stock_value']= $this->clean($arrFile[3]);
		$data['last_updated_date']= date('Y-m-d');
		$data['last_update_datetime']= date('Y-m-d H:i:s');	
		$data['status']= 'A';
		
		
		
		$data2['account_id']= $this->clean($_SESSION['accountId']);		
		$data2['item_id']= $this->clean($item_id);
		$data2['category_id']= $this->clean($category_id);
		$data2['attribute_value_id']= $this->clean($case_id);
		$data2['stock_value']= $this->clean($arrFile[3]);
		$data2['created_datetime']= date('Y-m-d H:i:s');	
		
		
	
		if(is_array($resultset) && sizeof($resultset)>0) 
		{
			$result = $this->_dbUpdate($data,'table_item_ndc_stock',$condi);
			$data2['stock_id']= $resultset[0]->stock_id;
			$result = $this->_dbInsert($data2,'table_item_ndc_stock_activity');
		} else {
			$result = $this->_dbInsert($data,'table_item_ndc_stock');
			$data2['stock_id']= $result;
			$result = $this->_dbInsert($data2,'table_item_ndc_stock_activity');
		}
		}
			
		}
		  $row++;	   
		}		
		
		fclose($file);	  
	   if($row<=1)
	   	return "no";
	   else 
	  	return $str_err_array;
	}	
/************************************* Start Upload Stock ***************************************/	


/*********************************** DISTRIBUTOR DISPATCHED STOCK *************************************************/

	function uploadDistributorDispatchstock() {
	
	
	$fields = array('Distributor Code*','Item Code*','Bill Date', 'Bill No.','Stock Value*');
	$file = fopen($_FILES['fileToUpload']['tmp_name'],"r");
	//$file = fopen("http://pepupsalesshakti.com/pepupsales/SAP/SAPSTOCK/Dealer_sap_stock.csv","r");


	$strErr="";
	$resultset = "";
	$row=0;	
	$data = array();
	$data2 = array();
	
	while(!feof($file)) {	
	
	$arrFile=fgetcsv($file);	
	$flag=true;		  
		
	if($row>0 && is_array($arrFile))  {	
		
		if(empty($arrFile[0]) || $arrFile[0]=="Distributor Code*" || empty($arrFile[1]) ||$arrFile[1]=="Item Code*" || empty($arrFile[4]) ||$arrFile[7]=="Stock Value*"){
			
			if(empty($arrFile[0])){
				$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]." ,".$arrFile[4].", ".$fields[0]." shouldn't be empty. \n";
				
			} elseif(empty($arrFile[2])){
				$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]." ,".$arrFile[4].", ".$fields[1]." shouldn't be empty. \n";
									
			}  elseif(empty($arrFile[4])) {
				$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]." ,".$arrFile[4].", ".$fields[4]." shouldn't be empty. \n";	
									
			} 
			$str_err_array=array($strErr);	
			$flag=false;
		}
		
		



		if($flag){
		
		if(empty($arrFile[2])) { 
		
		$bill_date = date('Y-m-d'); 
		$billdatecondi = "AND bill_date = '".$bill_date."'"; 
		$data2['bill_date']= $bill_date; 
		} else { 
		
		$bill_date = date('Y-m-d', strtotime($arrFile[2])); 
		$billdatecondi = "AND bill_date = '".$bill_date."'"; 
		}
		}
		
		if(empty($arrFile[3])) { $bill_no = "Default"; } else { $bill_no = mysql_real_escape_string($arrFile[3]); }
		
		
	// Check Distributor Code exists in the system.
	if($flag){
	$condi =" LOWER(distributor_code)='".mysql_real_escape_string(strtolower(trim($arrFile[0])))."'";
	$resultset = $this->_getSelectList('table_distributors','distributor_id','',$condi);
		if(!is_array($resultset) && empty($resultset) && $resultset[0]->distributor_id == "") {
			$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]." ,".$arrFile[4].", Distributor code doesn't exists in the system.\n";
			$str_err_array=array($strErr);	
			$flag=false;
		} else {
			$distributor_id = $resultset[0]->distributor_id;
		}
	}		
		
		

	if($flag){

	$category_id = 0;
	$case_id = 0;

	$condi =" LOWER(item_code)='".mysql_real_escape_string(strtolower(trim($arrFile[1])))."'"   ;
	$resultset = $this->_getSelectList('table_item','item_id, category_id','',$condi);
		if(!is_array($resultset) && empty($resultset) && $resultset[0]->item_id == "") {
			$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].", Item doesn't exists.\n";
			$str_err_array=array($strErr);	
			$flag=false;
		} else {
			$item_id = $resultset[0]->item_id;
			$category_id = $resultset[0]->category_id;
		}
	}
		
	// Check attribute( cases size) exists in the system.

	
	// Insert/Update data in the system			
	if($flag){	
	
			$condi =" distributor_id = '".$distributor_id."' AND item_id ='".$item_id."' AND bill_no = '".$bill_no."' ".$billdatecondi." AND status = 'I' ";
		   //echo sizeof($resultset);
			$resultset = $this->_getSelectList('table_item_dis_stk_inprocess','dis_stk_inpro_id, acpt_stock_value','',$condi);
			
			//echo "<pre>";
			//print_r($resultset);
			
			$data['account_id']= $this->clean($_SESSION['accountId']);		
			$data['distributor_id']= $this->clean($distributor_id);
			$data['item_id']= $this->clean($item_id);
			$data['category_id']= $this->clean($category_id);
			$data['attribute_value_id']= 0;
			$data['bill_date']= $this->clean($bill_date);
			$data['bill_no']= $this->clean($bill_no);
			$data['rec_stock_value']= $this->clean($arrFile[4]);
			$data['last_update_datetime']= date('Y-m-d H:i:s');	
			$data['created_datetime']= date('Y-m-d H:i:s');
			$data['status']= 'I';
		
			$data2['rec_stock_value']= $this->clean($arrFile[4]);
			$data2['last_update_datetime']= date('Y-m-d H:i:s');
			$data2['bill_no']= $bill_no;
			//print_r($data);
			//print_r($data2);
			if(is_array($resultset) && sizeof($resultset)>0) 
			{   
				//echo "Update";
				//$strErr .= "Row".$row." = This item stock detail already exists in the system";
				$result = $this->_dbUpdate($data2,'table_item_dis_stk_inprocess', $condi);
				
			} else {
				//echo "Insert";
				$result = $this->_dbInsert($data,'table_item_dis_stk_inprocess');
				
				
			}
			
			   // insert nds stock value.
		       $checkval="distributor_id='".$distributor_id."' and item_id='".$item_id."' and category_id='".$category_id."' and attribute_value_id='".$case_id."'";
			   $resultset = $this->_getSelectList('table_item_distributor_stock','*','',$checkval);

			   $data3 = array(); // AJAY@2016-08-21
			  
				if(sizeof($resultset)<=0){
				$data3['account_id']			= $this->clean($_SESSION['accountId']);
				$data3['distributor_id']		= $this->clean($distributor_id);
				$data3['item_id']				= $this->clean($item_id);
				$data3['category_id']			= $this->clean($category_id);
				$data3['attribute_value_id']	= $this->clean($case_id);
				$data3['dis_stock_value']		= 0;
				$data3['last_updated_date']		=date('Y-m-d');
				$data3['last_update_datetime']	= date('Y-m-d H:i:s');
				$data3['status']='A';
				$result=$this->_dbInsert($data3,'table_item_distributor_stock');
			    }
				else
				{
				$data3['last_updated_date']=date('Y-m-d');
				$data3['last_update_datetime']= date('Y-m-d H:i:s');
				$condi1="distributor_id='".$distributor_id."' and item_id='".$item_id."' and category_id='".$category_id."' and attribute_value_id='".$case_id."'";
				$result = $this->_dbUpdate($data3,'table_item_distributor_stock', $condi1);
				}
				//update only last update date in nds stock.
				
				$data4 = array(); // AJAY@2016-08-21
				
				$data4['last_updated_date']=date('Y-m-d');
				$data4['last_update_datetime']= date('Y-m-d H:i:s');
				$condnds=" item_id='".$item_id."' and category_id='".$category_id."' and attribute_value_id='".$case_id."'";
				$result = $this->_dbUpdate($data4,'table_item_ndc_stock', $condnds);
				
				$data5 = array(); // AJAY@2016-08-21
				
			   // Table Dispatch Activity Record inserted.
			$data5['account_id']= $this->clean($_SESSION['accountId']);		
			$data5['distributor_id']= $this->clean($distributor_id);
			$data5['item_id']= $this->clean($item_id);
			$data5['category_id']= $this->clean($category_id);
			$data5['attribute_value_id']= $this->clean($case_id);
			$data5['bill_date']= $this->clean($bill_date);
			$data5['bill_no']= $bill_no;
			$data5['dispatch_stock_value']= $this->clean($arrFile[4]);
			$data5['created_datetime']= date('Y-m-d H:i:s');	
			
		   $result_activity=$this->_dbInsert($data5,'table_item_dis_stk_dispatch_activity');  
				
		}
	}
		
		  $row++;	   
		}		
		
		fclose($file);	  
	   if($row<=1)
	   	return "no";
	   else 
	  	return $str_err_array;
}	
/*********************************** DISTRIBUTOR DISPATCHED STOCK *************************************************/
	/*********************************** RETAILER DUE *************************************************/
	
	function uploadRetailerDueFile(){	
	
		$fields = array('Retailer ID*','Retailer Name*','Due Amount');			
		$file = fopen($_FILES['fileToUpload']['tmp_name'],"r");
		$strErr="";
		$resultset = "";
		$row=0;	
		$data = array();
		$data2 = array();
	
	while(!feof($file))
	{	
	  
		$arrFile=fgetcsv($file);	
		$flag=true;		
		
		if($row>0 && is_array($arrFile)) 
		{		
				if(empty($arrFile[0]) || $arrFile[0]=="Retailer ID*" || empty($arrFile[1]) || $arrFile[1]=="Retailer Name*"){
				if(empty($arrFile[0])){										
						 $strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2]." , ".$fields[0]."shouldn't be empty.";						
			
						} elseif(empty($arrFile[1])) {
											
							 $strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2]." , ".$fields[1]."shouldn't be empty";
							
							
						} 
						$str_err_array=array($strErr);	
						$flag=false;
					}
					
					
				if($flag){
				 
					$condi=	"  LOWER(retailer_name)='".mysql_real_escape_string(strtolower($arrFile[1]))."' and retailer_id='".mysql_real_escape_string(strtolower($arrFile[0]))."'";
					
					$aDisRec=$this->_getSelectList('table_retailer','retailer_id','',$condi);					//exit;
					if(!is_array($aDisRec)) {
				$strErr .="Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2]." ,Retailer not exists in the system.\n";
						$str_err_array=array($strErr);	
						$flag=false;
					}
				}
				
				
			
				if($flag){	
					$data = array();
					$data2 = array();					
					$data['account_id']= $this->clean($_SESSION['accountId']);
					$data['retailer_id']= mysql_real_escape_string( $this->clean($aDisRec[0]->retailer_id));
					$data['due_amt']= mysql_real_escape_string( $this->clean($arrFile[2]));				
					$data['last_update_date']= date('Y-m-d');
					$data['last_update_datetime']= date('Y-m-d H:i:s');
					
					$data2['due_amt']= mysql_real_escape_string( $this->clean($arrFile[2]));				
					$data2['last_update_date']= date('Y-m-d');
					$data2['last_update_datetime']= date('Y-m-d H:i:s');	
					$condi1="retailer_id=".$aDisRec[0]->retailer_id." and account_id=".$_SESSION['accountId']." ";
					$aRetRec=$this->_getSelectList('table_retailer_due_details','*','',$condi1);
					if($arrFile[2]!=''){		
					if(is_array($aRetRec)){
					$item_id=$this->_dbUpdate($data2,'table_retailer_due_details'," retailer_id=".$aDisRec[0]->retailer_id);
					}
					else{
						$item_id=$this->_dbInsert($data,'table_retailer_due_details');
					}
					
					}
					
		 	 }	
		  }
		  $row++;	   
		}		
		fclose($file);	  
	   if($row<=1)
	   	return "no";
	   else 
	  	return $str_err_array;
	}
	
	/*********************************** RETAILER DUE *************************************************/
	
	/*********************************** DISTRIBUTOR DISPATCHED STOCK *************************************************/
	
	
	
	
	
	
	
	
	
	
	
	
/************************************* Upload distributor actual stock(10 Sep 2014) ***************************************/	

function uploadDistributorActualStock() {
	

	$fields = array('Distributor Code*','Category Name*','Item Code*','Cases Size','Number Of Cases*');
	$file = fopen($_FILES['fileToUpload']['tmp_name'],"r");
	$strErr="";
	$resultset = "";
	$row=0;	
    $disCd = '';
	$catName= '';
	$itmCd	= '';
	$caseSize	= '';
	$stkValue= '';
	
	
	while(!feof($file))
	{	
			$data = array();
			$data2 = array();
			$data3 = array();
	
		  
			$arrFile=fgetcsv($file);	
			$flag=true;	
		
			$disCd		= trim($arrFile[0]);
			$catName	= trim($arrFile[1]);
			$itmCd		= trim($arrFile[2]);
			$caseSize	= trim($arrFile[3]);
			$stkValue	= trim($arrFile[4]);
		
	//echo "<pre>"; print_r($arrFile);
	//exit;
		
if($row>0 && is_array($arrFile)) {	
	if(empty($disCd) || $disCd=="Distributor Code*" || empty($catName) ||$catName=="Category Name*" || empty($itmCd) || $itmCd=="Item Code*" || empty($stkValue) || $stkValue=="Number Of Cases*"){
	
	if(empty($disCd)){
	$strErr .= "Error in Row".$row." ,".$disCd.",".$catName." ,".$itmCd." ,".$caseSize." , ".$stkValue.", ".$fields[0]." shouldn't be empty  \n";
	} elseif(empty($catName)){
	$strErr .= "Error in Row".$row." ,".$disCd.",".$catName." ,".$itmCd." ,".$caseSize." , ".$stkValue.", ".$fields[1]." shouldn't be empty  \n";
	} elseif(empty($itmCd)){
	$strErr .= "Error in Row".$row." ,".$disCd.",".$catName." ,".$itmCd." ,".$caseSize." , ".$stkValue.", ".$fields[2]." shouldn't be empty  \n";
	} elseif(empty($stkValue)) {
	$strErr .= "Error in Row".$row." ,".$disCd.",".$catName." ,".$itmCd." ,".$caseSize." , ".$stkValue.", ".$fields[4]." shouldn't be empty  \n";					
	} 
	$str_err_array=array($strErr);						
	$flag=false;
	} 
	
	
	// Check Distributor Code exists in the system.
	if($flag){
	$condi =" LOWER(distributor_code)='".mysql_real_escape_string(strtolower(trim($disCd)))."'";
	$resultset = $this->_getSelectList('table_distributors','distributor_id','',$condi);
		if(!is_array($resultset) && empty($resultset) && $resultset[0]->distributor_id == "") {
			$strErr .= "Error in Row".$row." ,".$disCd.",".$catName." ,".$itmCd." ,".$caseSize." , ".$stkValue." , Distributor code doesn't exists in the system.\n";
			$str_err_array=array($strErr);	
			$flag=false;
		} else {
			$distributor_id = $resultset[0]->distributor_id;
		}
	}		
		
	// Check category exists in the system.
	if($flag){
	$condi =" LOWER(category_name)='".mysql_real_escape_string(strtolower($catName))."'";
	$resultset = $this->_getSelectList('table_category','category_id','',$condi);
		if(!is_array($resultset) && empty($resultset) && $resultset[0]->category_id == "") {
				$strErr .= "Error in Row".$row." ,".$disCd.",".$catName." ,".$itmCd." ,".$caseSize.", ".$stkValue." ,Category doesn't exists in the system  \n";	
			$str_err_array=array($strErr);	
			$flag=false;
		} else {
			$category_id = $resultset[0]->category_id;
		}
	}
		
	// Check item exists in the system.		
	if($flag){
	$condi ="  category_id ='".$category_id."' AND LOWER(item_code)='".mysql_real_escape_string(strtolower($itmCd))."'";
	$resultset = $this->_getSelectList('table_item','item_id','',$condi);
		if(!is_array($resultset) && empty($resultset) && $resultset[0]->item_id == "") {
		$strErr .= "Error in Row".$row." ,".$disCd.",".$catName." ,".$itmCd." ,".$caseSize." ,Item doesn't exists in this category or item code doesn't exists in the system. \n";	
			$str_err_array=array($strErr);	
			$flag=false;
			
		} else {
			$item_id = $resultset[0]->item_id;
		}
	}
		
	// Check attribute(size,color) exists in the system.

	//Check cases size in the system.	

	if($flag){  
	if($caseSize!="") { $condi =" LOWER(case_size) = '".mysql_real_escape_string(strtolower($caseSize))."' AND ic.item_id=".$item_id; } else { $condi =" LOWER(case_size) = '1' AND ic.item_id=".$item_id; }
	
	$resultset = $this->_getSelectList('table_cases as c left join table_item_case_relationship as ic on ic.case_id=c.case_id','c.case_id','',$condi);
	if(!is_array($resultset) && empty($resultset) && $resultset[0]->case_id == "") {
	$strErr .= "Error in Row".$row.",".$disCd.",".$catName.",".$itmCd.",".$caseSize.", Cases Sizes doesn't exists in the system\n";		
	$str_err_array=array($strErr);	
	$flag=false;
	} else { $case_id = $resultset[0]->case_id; }
	}			
	
	
	// Insert/Update data in the system			
	if($flag){	
		$condi =" distributor_id = '".$distributor_id."' AND category_id ='".$category_id."' AND item_id ='".$item_id."' AND attribute_value_id ='".$case_id."'";
		$resultset = $this->_getSelectList('table_item_distributor_stock','*','',$condi);
		//print_r($resultset);
		//exit;
		
		$data['account_id']= $_SESSION['accountId'];	
		$data['distributor_id']=  $distributor_id;	
		$data['item_id']=  $item_id;
		$data['category_id']= $category_id;
		$data['attribute_value_id']= $case_id;
		$data['last_updated_date']= date('Y-m-d');
		$data['last_update_datetime']= date('Y-m-d H:i:s');	
		$data['status']= 'A';
		
		$data2['account_id']= $_SESSION['accountId'];	
		$data2['distributor_id']=  $distributor_id;	
		$data2['item_id']=  $item_id;
		$data2['category_id']= $category_id;
		$data2['attribute_value_id']= $case_id;
		$data2['dispatch_stock_value']= $stkValue;  // Distributor Activity Value
		$data2['created_datetime']= date('Y-m-d H:i:s');	
		$data2['status']= 'A';
		
		
	
		if(is_array($resultset) && sizeof($resultset)>0) {
			//$data['dis_stock_value']= $resultset[0]->dis_stock_value + $stkValue;   // Distributor Actual Stock Value
			$data['dis_stock_value']= $stkValue;   // Distributor Actual Stock Value
			$result = $this->_dbUpdate($data,'table_item_distributor_stock',$condi);
		   	$result_activity = $this->_dbInsert($data2,'table_item_dis_stk_dispatch_activity');  
		} else {
			$data['dis_stock_value']= $stkValue;   // Distributor Actual Stock Value
			$result = $this->_dbInsert($data,'table_item_distributor_stock');
		   	$result_activity = $this->_dbInsert($data2,'table_item_dis_stk_dispatch_activity');  
		}
			
		//check insert/update only last update date in nds stock.
		
		$data3['account_id']= $_SESSION['accountId'];	
		$data3['item_id']=  $item_id;
		$data3['category_id']= $category_id;
		$data3['attribute_value_id']= $case_id;
		$data3['last_updated_date']= date('Y-m-d');
		$data3['last_update_datetime']= date('Y-m-d H:i:s');	
		$data3['status']= 'A';
		
		
		$condnds = " category_id='".$category_id."' AND item_id='".$item_id."' AND attribute_value_id='".$case_id."'";
		$ndcresultset = $this->_getSelectList('table_item_ndc_stock','*','',$condnds);
		if(isset($ndcresultset) && count($ndcresultset)>0)	 {	$this->_dbUpdate($data3,'table_item_ndc_stock', $condnds); 
		} else { $this->_dbInsert($data3,'table_item_ndc_stock', $condnds); }
			
		//check insert/update only last update date in nds stock.	

		}
			
		}
		  $row++;	   
		}		
		
		fclose($file);	  
	   if($row<=1)
	   	return "no";
	   else 
	  	return $str_err_array;
	}	



/************************************* Upload distributor actual stock(10 Sep 2014) ***************************************/
	
	
	
	
	
/*****************************************************
 * desc : Import SKU Promotions
 * created on : 05 Jan 2015
 * Author : AJAY
 *
 ***/

	
	

 // import data here
 function import_item_promotion () {

	$fields = array('Chain Name*','Item Code*','Offer*');
	$file = fopen($_FILES['fileToUpload']['tmp_name'],"r");
	$strErr="";
	$resultset = "";
	$row=0;	

	$chainName	= '';
	$itmCd		= '';
	$promo_desc		= '';
	
	
	while(!feof($file))
	{	
	$data = array();
	$data2 = array();
	$data3 = array();
	
		  
		$arrFile=fgetcsv($file);	
		$flag=true;	
		
		$chainName	= trim($arrFile[0]);
		$itmCd		= trim($arrFile[1]);
		$promo_desc	= trim($arrFile[2]);
		$start_date	= date('Y-m-d', strtotime(trim($arrFile[3])));
		$end_date	= date('Y-m-d', strtotime(trim($arrFile[4])));

		if(!isset($start_date) || empty($start_date) || $start_date=='1970-01-01') $start_date = NULL;
		if(!isset($end_date) || empty($end_date) || $end_date=='1970-01-01') $end_date = NULL;
		//echo $start_date;
		//echo $end_date;
		//echo "<pre>"; print_r($arrFile);
		//exit;
		
	if($row>0 && is_array($arrFile)) {

	if(empty($chainName) || $chainName=="Chain Name*" || empty($itmCd) || $itmCd=="Item Code*" || empty($promo_desc) || $promo_desc=="Offer*"){
	
	if(empty($chainName)){
	$strErr .= "Error in Row".$row." ,".$chainName.",".$itmCd." ,".$promo_desc." , ".$fields[0]." shouldn't be empty  \n";
	} elseif(empty($itmCd)){
	$strErr .= "Error in Row".$row." ,".$chainName.",".$itmCd." ,".$promo_desc." , ".$fields[1]." shouldn't be empty  \n";
	} elseif(empty($promo_desc)){
	$strErr .= "Error in Row".$row." ,".$chainName.",".$itmCd." ,".$promo_desc." , ".$fields[2]." shouldn't be empty  \n";
	}

	$str_err_array=array($strErr);						
	$flag=false;
	} 
	
	
	// Check item exists in the system.		
	if($flag){

	$itmCd = preg_replace( "/\r|\n/", "", $itmCd );

	$condi =" LOWER(item_code)='".mysql_real_escape_string(strtolower(trim($itmCd)))."'";
	$resultset = $this->_getSelectList('table_item','item_id','',$condi);
		if(!is_array($resultset) && empty($resultset) && $resultset[0]->item_id == "") {
			$strErr .= "Error in Row".$row." ,".$chainName.",".$itmCd." ,".$promo_desc." ,Item code doesn't exists in the system. \n";	
			$str_err_array=array($strErr);	
			$flag=false;
		} else {
			$item_id = $resultset[0]->item_id;
		}
	}


	// Check SKU CHAIN exists in the system.
	if($flag){

	$resultset = array ();
	$chainName = preg_replace( "/\r|\n/", "", trim($chainName));

	$condi = " LOWER(chain_name)='".mysql_real_escape_string(strtolower(trim($chainName)))."' AND ICR.item_id =".$item_id;
	//exit;
	$resultset = $this->_getSelectList('table_chain AS C LEFT JOIN table_item_chain_relationship AS ICR ON ICR.chain_id = C.chain_id ','C.chain_id','',$condi);

	//print_r($resultset);
	//exit;

		if(!is_array($resultset) && empty($resultset) && $resultset[0]->chain_id == "") {
			$strErr .= "Error in Row".$row." ,".$chainName.",".$itmCd." ,".$promo_desc.", ".$condi." , Chain doesn't exists in the system.\n";
			$str_err_array=array($strErr);	
			$flag=false;
		}  else {
			$chain_id = $resultset[0]->chain_id;
		}
	}		
		
	
	// Insert/Update data in the system			
	if($flag){	
		$data = array ();
		$condi =" item_id = '".$item_id."'";
		$resultset = $this->_getSelectList('table_promotion','*','',$condi);
		//print_r($resultset);
		//exit;
		
		$data['account_id']		=  $_SESSION['accountId'];	
		$data['item_id']		=  $item_id;
		$data['chain_id']		=  $chain_id;
		$data['promo_desc']		=  $promo_desc;  // promo_desc
		$data['start_date']		=  $start_date;
		$data['end_date']		=  $end_date;
		$data['last_updated_on']=  date('Y-m-d H:i:s');	
		$data['status']			=  'A';

		$result = $this->_dbInsert($data,'table_promotion');
	
			/* if(is_array($resultset) && sizeof($resultset)>0) {

				$data['promo_desc']	= $promo_desc;   // promo_desc
				$result = $this->_dbUpdate($data,'table_promotion',$condi);

			} else {

				$data['promo_desc']	= $promo_desc;   // promo_desc
				$result = $this->_dbInsert($data,'table_promotion');
			} */

		}
			
		}
		  $row++;	   
		}		
		
		fclose($file);	  
	   if($row<=1)
	   	return "no";
	   else 
	  	return $str_err_array;


 }
	
	
	
	
	
	
	
/***************************************************** Add Route ******************************************************/

	
	function uploadRouteList () {

	$fields = array('ID*','Retailer*','State*','City*','Market*','Route Name*');
	$file = fopen($_FILES['fileToUpload']['tmp_name'],"r");
	$strErr="";
	$resultset = "";
	$row=0;	

	$retailer_id ="";
	$retailer_name="";
	$state="";
	$city="";
	$market="";
	$route_name="";
	
	
	
	while(!feof($file))
	{	
	$data = array();
	$data2 = array();
	$data3 = array();
	
		  
		$arrFile=fgetcsv($file);	
		$flag=true;	
		
		
		$retailer_id =trim($arrFile[0]);
		$retailer_name=trim($arrFile[1]);
		$state=trim($arrFile[2]);
		$city=trim($arrFile[3]);
		$market=trim($arrFile[4]);
		$route_name=trim($arrFile[5]);

	if($row>0 && is_array($arrFile)) {

	if(empty($retailer_id) || $retailer_id=="ID*" || empty($retailer_name) || $retailer_name=="Retailer*" || empty($state) || $state=="State*" || empty($city) || $city=="City*" || empty($market) || $market=="Market*" || empty($route_name) || $route_name=="Route Name*"){
	
	if(empty($retailer_id)){
	$strErr .= "Error in Row".$row." ,".$retailer_id.",".$retailer_name." ,".$state." ,".$city.",".$market." ,".$route_name.", ".$fields[0]." shouldn't be empty  \n";
	} elseif(empty($retailer_name)){
	$strErr .= "Error in Row".$row." ,".$retailer_id.",".$retailer_name." ,".$state." ,".$city.",".$market." ,".$route_name." , ".$fields[1]." shouldn't be empty  \n";
	} elseif(empty($state)){
	$strErr .= "Error in Row".$row." ,".$retailer_id.",".$retailer_name." ,".$state." ,".$city.",".$market." ,".$route_name." , ".$fields[2]." shouldn't be empty  \n";
	} elseif(empty($city)){
	$strErr .= "Error in Row".$row." ,".$retailer_id.",".$retailer_name." ,".$state." ,".$city.",".$market." ,".$route_name.", ".$fields[3]." shouldn't be empty  \n";
	} elseif(empty($market)){
	$strErr .= "Error in Row".$row." ,".$retailer_id.",".$retailer_name." ,".$state." ,".$city.",".$market." ,".$route_name." , ".$fields[4]." shouldn't be empty  \n";
	} elseif(empty($route_name)){
	$strErr .= "Error in Row".$row." ,".$retailer_id.",".$retailer_name." ,".$state." ,".$city.",".$market." ,".$route_name." , ".$fields[5]." shouldn't be empty  \n";
	}

	$str_err_array=array($strErr);						
	$flag=false;
	} 
	
	
	// Check State exists in the system.		
	if($flag){
	
	$condi =" LOWER(state_name)='".$state."'";
	$resultset = $this->_getSelectList2('state','state_id','',$condi);
		if(!is_array($resultset) && empty($resultset) && $resultset[0]->state_id == "") {
			$strErr .= "Error in Row".$row." ,".$retailer_id.",".$retailer_name." ,".$state." ,".$city.",".$market." ,".$route_name." ,State doesn't exists in the system. \n";	
			$str_err_array=array($strErr);	
			$flag=false;
		} else {
			$state_id = $resultset[0]->state_id;
		}
	}
	
	
	// Check City exists in the system.		
	if($flag){
	
	$condi =" LOWER(city_name)='".$city."'";
	$resultset = $this->_getSelectList2('city','city_id','',$condi);
		if(!is_array($resultset) && empty($resultset) && $resultset[0]->city_id == "") {
			$strErr .= "Error in Row".$row." ,".$retailer_id.",".$retailer_name." ,".$state." ,".$city.",".$market." ,".$route_name." , City doesn't exists in the system. \n";	
			$str_err_array=array($strErr);	
			$flag=false;
		} else {
			$city_id = $resultset[0]->city_id;
		}
	}
	
	
	
	if($flag){
			
	$condi =" LOWER(location)='".$market."' and state_id='".$state_id."' and city='".$city_id."'";
	$resultset = $this->_getSelectList('table_market','location','',$condi);
		if(!is_array($resultset) && empty($resultset) && $resultset[0]->location == "") {
			$strErr .= "Error in Row".$row." ,".$retailer_id.",".$retailer_name." ,".$state." ,".$city.",".$market." ,".$route_name." ,Market doesn't exists in the system. \n";	
			$str_err_array=array($strErr);	
			$flag=false;
		} 
	}
	
	if($flag){
	$route_name=mysql_real_escape_string(strtolower(trim($route_name)));
	$condi =" LOWER(r.route_name)='".$route_name."'";
	$resultset = $this->_getSelectList('table_route as r','r.route_id,r.state_id,r.city_id','',$condi);
	//print_r($resultset);exit;
		if(is_array($resultset) && !empty($resultset) && $resultset[0]->route_id != "") {			
			$route_id = $resultset[0]->route_id;
			if($route_id!=''){		
			$stateArr = array_filter(explode(',', $resultset[0]->state_id));
			$cityArr = array_filter(explode(',', $resultset[0]->city_id));
			array_push($stateArr, $state_id);
			array_push($cityArr, $city_id);
			$comma_separated_state = implode(",", array_unique($stateArr));
			$comma_separated_city = implode(",", array_unique($cityArr));
		}
		
		} 
		else {				
				$comma_separated_state = $state_id;
				$comma_separated_city = $city_id;			
			 }
	}
	
	
	
	
	if($flag){

	//$itmCd = preg_replace( "/\r|\n/", "", $itmCd );
	$retailer_name=mysql_real_escape_string(strtolower(trim($retailer_name)));
	$state=mysql_real_escape_string(strtolower(trim($state)));
	$city=mysql_real_escape_string(strtolower(trim($city)));
	$market=mysql_real_escape_string(strtolower(trim($market)));
	
	
	$condi =" LOWER(retailer_name)='".$retailer_name."' and LOWER(retailer_location)='".$market."' and state='".$state_id."' and city='".$city_id."' and retailer_id='".$retailer_id."'";
	$resultset = $this->_getSelectList('table_retailer','retailer_id','',$condi);
		if(!is_array($resultset) && empty($resultset) && $resultset[0]->retailer_id == "") {
			$strErr .= "Error in Row".$row."  ,".$retailer_id.",".$retailer_name." ,".$state." ,".$city.",".$market." ,".$route_name." ,Retailer doesn't exists in the system. \n";	
			$str_err_array=array($strErr);	
			$flag=false;
		} else {
			$retailer_id = $resultset[0]->retailer_id;
		}
	}

	if($flag){	
	if($route_id!='' && $retailer_id!=''){		
		$condi =" route_id ='".$route_id."' and retailer_id='".$retailer_id."'";
		$resultset = $this->_getSelectList('table_route_retailer','retailer_id','',$condi);
			if(is_array($resultset) && !empty($resultset) && $resultset[0]->retailer_id != "") {
			$strErr .= "Error in Row".$row."  ,".$retailer_id.",".$retailer_name." ,".$state." ,".$city.",".$market." ,".$route_name." ,Retailer already mapped to the route. \n";	
			$str_err_array=array($strErr);	
			$flag=false;
		}
		
		}
	
	}
	
	
	// Insert/Update data in the system			
	if($flag){	
		if($route_id==''){
		$data = array ();		
		$data['account_id']		=  $_SESSION['accountId'];	
		$data['state_id']		=  $comma_separated_state;
		$data['city_id']		=  $comma_separated_city;
		$data['route_name']		=  $route_name;  // promo_desc
		$data['status']			=  'A';
		$resultRoute = $this->_dbInsert($data,'table_route');
		}
		else {
			
				$data['state_id']		=  $comma_separated_state;
				$data['city_id']		=  $comma_separated_city;
				$result = $this->_dbUpdate($data,'table_route'," route_id='".$route_id."'");
			}
			if(isset($resultRoute) && $resultRoute!='' && $route_id=='') {
				$data2['account_id']	= $_SESSION['accountId'];   // promo_desc
				$data2['route_id']	= $resultRoute; 
				$data2['retailer_id']	= $retailer_id;
				$result_id = $this->_dbInsert($data2,'table_route_retailer');
			} 
			else{
				$data2['account_id']	= $_SESSION['accountId'];   // promo_desc
				$data2['route_id']	= $route_id; 
				$data2['retailer_id']	= $retailer_id;
				$result_id = $this->_dbInsert($data2,'table_route_retailer');
				
				}
		}
			
		}
		  $row++;	   
		}		
		
		fclose($file);	  
	   if($row<=1)
	   	return "no";
	   else 
	  	return $str_err_array;
 
}
	
	
/***************************************************** Add Route ******************************************************/


	
/************************************* Upload distributor target (3rd June 2015) Gaurav ***************************************/	

function uploadDistributorTraget() {
	
	//echo "Heelloo";

	$fields = array('Distributor Code*', 'Item Code*', 'Cases Size*', 'Number Of Cases*', 'Target Type*');
	$file = fopen($_FILES['fileToUpload']['tmp_name'],"r");
	$strErr="";
	$resultset = "";
	$row=0;	

	$disCd = '';
	//$catName= '';
	$itmCd	= '';
	$caseSize	= '';
	$numberOfCases= '';
	$targetType= '';
	
	while(!feof($file))
	{	
	$data = array();
	$data2 = array();
	$data3 = array();
	
		  
		$arrFile=fgetcsv($file);	
		$flag=true;	
		
		$disCd		= trim($arrFile[0]);
		$itmCd	= trim($arrFile[1]);
		$caseSize		= trim($arrFile[2]);
		$numberOfCases		= trim($arrFile[3]);
		$targetType	= trim($arrFile[4]);
		$target_month=$_REQUEST['month'];
		$target_year=$_REQUEST['year'];
		
	//echo "<pre>"; print_r($row);
	
	
	//exit;
		
if($row>0 && is_array($arrFile)) {	
				
	
	
	if(empty($disCd) || $disCd=="Distributor Code*" || empty($itmCd) || $itmCd=="Item Code*" || empty($caseSize) || $caseSize=="Cases Size*" || empty($numberOfCases) || $numberOfCases=="Number Of Cases*" || empty($targetType) || $targetType=="Target Type*"){
	
	if(empty($disCd)){
	$strErr .= "Error in Row".$row." ,".$disCd.",".$itmCd." ,".$caseSize." ,".$numberOfCases." , ".$targetType.", ".$fields[0]." shouldn't be empty  \n";
	} elseif(empty($itmCd)){
	$strErr .= "Error in Row".$row." ,".$disCd.",".$itmCd." ,".$caseSize." ,".$numberOfCases." , ".$targetType.", ".$fields[1]." shouldn't be empty  \n";
	} elseif(empty($caseSize)){
	$strErr .= "Error in Row".$row." ,".$disCd.",".$itmCd." ,".$caseSize." ,".$numberOfCases." , ".$targetType.", ".$fields[2]." shouldn't be empty  \n";
	} elseif(empty($numberOfCases)) {
	$strErr .= "Error in Row".$row." ,".$disCd.",".$itmCd." ,".$caseSize." ,".$numberOfCases." , ".$targetType.", ".$fields[3]." shouldn't be empty  \n";					
	} elseif(empty($targetType)) {
	$strErr .= "Error in Row".$row." ,".$disCd.",".$itmCd." ,".$caseSize." ,".$numberOfCases." , ".$targetType.", ".$fields[4]." shouldn't be empty  \n";					
	} 	
	
	$str_err_array=array($strErr);		
	$flag=false;
	} 
	
	//echo "<pre>";print_r($disCd);
	// Check Distributor Code exists in the system.
	if($flag){
		//echo "<pre>"; print_r($disCd);
		//exit;
	$condi =" LOWER(distributor_code)='".mysql_real_escape_string(strtolower(trim($disCd)))."'";
	$resultset = $this->_getSelectList('table_distributors','distributor_id','',$condi);
		if(!is_array($resultset) && empty($resultset) && $resultset[0]->distributor_id == "") {
			$strErr .= "Error in Row".$row." ,".$disCd.",".$itmCd." ,".$caseSize." ,".$numberOfCases." , ".$targetType." , Distributor code doesn't exists in the system.\n";
			$str_err_array=array($strErr);	
			$flag=false;
		} else {
			$distributor_id = $resultset[0]->distributor_id;
		}
	}		
		
	// Check category exists in the system.
	
		
	// Check item exists in the system.		
	if($flag){
	$condi =" LOWER(item_code)='".mysql_real_escape_string(strtolower($itmCd))."'";
	$resultset = $this->_getSelectList('table_item','item_id','',$condi);
		if(!is_array($resultset) && empty($resultset) && $resultset[0]->item_id == "") {
		$strErr .= "Error in Row".$row." ,".$disCd.",".$itmCd." ,".$caseSize." ,".$numberOfCases." , ".$targetType." ,Item doesn't exists in this category or item code doesn't exists in the system. \n";	
			$str_err_array=array($strErr);	
			$flag=false;
			
		} else {
			$item_id = $resultset[0]->item_id;
		}
	}
		
	// Check attribute(size,color) exists in the system.

	//Check cases size in the system.	

	if($flag){  
	if($caseSize!="") { $condi =" LOWER(case_size) = '".mysql_real_escape_string(strtolower($caseSize))."' AND ic.item_id=".$item_id; } else { $condi =" LOWER(case_size) = '1' AND ic.item_id=".$item_id; }
	
	$resultset = $this->_getSelectList('table_cases as c left join table_item_case_relationship as ic on ic.case_id=c.case_id','c.case_id','',$condi);
	if(!is_array($resultset) && empty($resultset) && $resultset[0]->case_id == "") {
	$strErr .= "Error in Row".$row.",".$disCd.",".$itmCd." ,".$caseSize." ,".$numberOfCases." , ".$targetType.", Cases Sizes doesn't exists in the system\n";		
	$str_err_array=array($strErr);	
	$flag=false;
	} else { $case_id = $resultset[0]->case_id; }
	}			
	
	//Check target type in the system.	
	
	if($flag){
	$getTargetType=strtolower($targetType);	
	if($getTargetType=="primary"){$targetTypeId=1;}
	else if($getTargetType=="secondary"){$targetTypeId=2;}
		else{
				$strErr .= "Error in Row".$row." ,".$disCd.",".$itmCd." ,".$caseSize." ,".$numberOfCases." , ".$targetType." ,Target type doesn't exists in the system  \n";	
			$str_err_array=array($strErr);	
			$flag=false;
		}
	}
	
	if($flag){
		
		$resultset = $this->_getSelectList('table_distributors_target','distributor_target_id',''," distributor_id='".$distributor_id."' and target_month='".$target_month."' and target_year='".$target_year."' ");
	if(sizeof($resultset)>0){
			$strErr .= "Error in Row".$row." ,".$disCd.",".$itmCd." ,".$caseSize." ,".$numberOfCases." , ".$targetType." ,Target already exists in the system for distributor.  \n";	
			$str_err_array=array($strErr);	
			$flag=false;
		}
	}
	
	
		//echo "<pre>"; print_r($distributor_id);
		//exit;
	// Insert/Update data in the system			
	if($flag){	
		
		$data['account_id']= $_SESSION['accountId'];	
		$data['distributor_id']=  $distributor_id;	
		$data['target_month']= $target_month;
		$data['target_year']= $target_year;
		$data['created_date']= date('Y-m-d');
		$data['item_id']=  $item_id;
		$data['case_id']= $case_id;
		$data['no_of_cases']= $numberOfCases;
		$data['target_type']=  $targetTypeId;		
		//echo "<pre>"; print_r($arrFile);
		//exit;	
		$temData = $this->_dbInsert($data,'table_distributors_target_temp');  
		
			
		//check insert/update only last update date in nds stock.	

		}
			
		}
		  $row++;	   
		}		
		
		$disTarget = $this->_getSelectList('table_distributors_target_temp','account_id,distributor_id,target_month,target_year,created_date,target_type',''," GROUP BY distributor_id,target_month,target_year,target_type");
		if(sizeof($disTarget)>0){
		foreach($disTarget as $key=>$value){
						
					$data2['account_id']= $_SESSION['accountId'];	
					$data2['distributor_id']=  $value->distributor_id;	
					$data2['target_month']= $value->target_month;
					$data2['target_year']= $value->target_year;
					$data2['target_type']= $value->target_type;
					$data2['created_date']= date('Y-m-d');
					$data2['last_update_date']= date('Y-m-d');
					$data2['last_update_status']= "New";
					$data2['status']= "A";
			$disTargetID=$this->_dbInsert($data2,'table_distributors_target'); 	
			
			$disTargetItem = $this->_getSelectList('table_distributors_target_temp','item_id,case_id,no_of_cases,target_type',''," account_id='".$_SESSION['accountId']."' and distributor_id='".$value->distributor_id."' and target_month='".$value->target_month."' and target_year='".$value->target_year."' and target_type='".$value->target_type."'");	
			
			if(sizeof($disTargetItem)>0){
				foreach($disTargetItem as $key=>$valueItem){
					
					$getCaseList=$this->_getSelectList2('table_item_case_relationship as c left join table_cases as ca on c.case_id=ca.case_id',"ca.case_size",''," c.item_id='".$valueItem->item_id."' and ca.case_id='".$valueItem->case_id."' ORDER BY ca.case_size",'');
					
					$data3['distributor_target_id']=  $disTargetID;
					$data3['item_id']=  $valueItem->item_id;	
					$data3['case_id']= $valueItem->case_id;
					$data3['no_of_cases']= $valueItem->no_of_cases;
					$data3['target_type']= $valueItem->target_type;
					$data3['total_quantity']= ($getCaseList[0]->case_size)*$valueItem->no_of_cases;
					$data3['last_update_date']= date('Y-m-d');
					$data3['last_update_status']= "New";
					$data3['status']= "A";
					$disTargetItemID=$this->_dbInsert($data3,'table_distributors_target_item'); 	
				}
				
				}
			
			
			}
			
			
		}
		$disTargetTempDel = $this->_getTruncate('table_distributors_target_temp');
		
		fclose($file);	  
	   if($row<=1)
	   	return "no";
	   else 
	  	return $str_err_array;
	}	



/************************************* Upload distributor target (3rd June 2015 Gaurav) ***************************************/















/********************************************************************************
* DESC : Upload salesman orders by admin
* Author : AJAY
* Created : 29th July 2015
*
**/	

function uploadOrdersFile() {
	

	$fields = array('Salesman Name*', 'Salemsan Phone*', 'Distributor Code*', 'Retailer Name*', 'Retailer Phone*', 'Bill Date', 'Bill No', 'Date of Order*','Item Code*', 'Qty*');
	$file = fopen($_FILES['fileToUpload']['tmp_name'],"r");

	$strErr="";
	$resultset = "";
	$row=0;	

	$salesmanName 		= '';
	$salesmanPhone		= '';
	$distributorCode	= '';
	$retailerName 		= '';
	$retailerPhone 		= '';
	$billDate 			= '';
	$billNo 			= '';
	$dateOfOrder		= '';
	$itemCode 			= '';
	$qty 	 			= '';
	
	while(!feof($file)){	 

	$data = array();
	$data2 = array();
	$data3 = array();
	
		  
	$arrFile=fgetcsv($file);	
	$flag=true;	
		

	$salesmanName 		= strtolower(trim($arrFile[0]));
	$salesmanPhone		= trim($arrFile[1]);
	$distributorCode	= strtolower(trim($arrFile[2]));
	$retailerName 		= strtolower(trim($arrFile[3]));
	$retailerPhone 		= trim($arrFile[4]);
	$billDate 			= trim($arrFile[5]);
	$billNo 			= trim($arrFile[6]);
	$dateOfOrder        = trim($arrFile[7]); 	
	$dateOfOrder 		= date('Y-m-d', strtotime($dateOfOrder));
	$itemCode 			= strtolower(trim($arrFile[8]));
	$qty 	 			= trim($arrFile[9]);
		
	//echo "<pre>"; print_r($row);
	//exit;
		
if($row>0 && is_array($arrFile)) {	
				
	
	if(empty($salesmanName) || $salesmanName=="Salesman Name*" || empty($salesmanPhone) || $salesmanPhone=="Salemsan Phone*" || empty($distributorCode) || $distributorCode=="Distributor Code*" || empty($retailerName) || $retailerName=="Retailer Name*" || empty($retailerPhone) || $retailerPhone=="Retailer Phone*" || empty($dateOfOrder) || $dateOfOrder=="Date of Order*" || empty($itemCode) || $itemCode=="Item Code*" || empty($qty) || $qty=="Qty*"){
	
	if(empty($salesmanName)){

	$strErr .= "Error in Row".$row." ,".$salesmanName.",".$salesmanPhone." ,".$distributorCode." ,".$retailerName." , ".$retailerPhone.", ".$billDate.", ".$billNo.", ".$dateOfOrder.", ".$itemCode.",  ".$qty.", ".$fields[0]." shouldn't be empty  \n";
	} elseif(empty($salesmanPhone)){
	$strErr .= "Error in Row".$row." ,".$salesmanName.",".$salesmanPhone." ,".$distributorCode." ,".$retailerName." , ".$retailerPhone.",".$billDate.", ".$billNo.",  ".$dateOfOrder.", ".$itemCode.",  ".$qty.", ".$fields[1]." shouldn't be empty  \n";
	} elseif(empty($distributorCode)){
	$strErr .= "Error in Row".$row." ,".$salesmanName.",".$salesmanPhone." ,".$distributorCode." ,".$retailerName." , ".$retailerPhone.", ".$billDate.", ".$billNo.", ".$dateOfOrder.", ".$itemCode.",  ".$qty.", ".$fields[2]." shouldn't be empty  \n";
	} elseif(empty($retailerName)) {
	$strErr .= "Error in Row".$row." ,".$salesmanName.",".$salesmanPhone." ,".$distributorCode." ,".$retailerName." , ".$retailerPhone.", ".$billDate.", ".$billNo.", ".$dateOfOrder.", ".$itemCode.",  ".$qty.", ".$fields[3]." shouldn't be empty  \n";
	} elseif(empty($retailerPhone)) {
	$strErr .= "Error in Row".$row." ,".$salesmanName.",".$salesmanPhone." ,".$distributorCode." ,".$retailerName." , ".$retailerPhone.", ".$billDate.", ".$billNo.", ".$dateOfOrder.", ".$itemCode.",  ".$qty.", ".$fields[4]." shouldn't be empty  \n";				
	} elseif(empty($dateOfOrder)) {
	$strErr .= "Error in Row".$row." ,".$salesmanName.",".$salesmanPhone." ,".$distributorCode." ,".$retailerName." , ".$retailerPhone.", ".$billDate.", ".$billNo.",  ".$dateOfOrder.", ".$itemCode.",  ".$qty.", ".$fields[5]." shouldn't be empty  \n";				
	} 	elseif(empty($itemCode)) {
	$strErr .= "Error in Row".$row." ,".$salesmanName.",".$salesmanPhone." ,".$distributorCode." ,".$retailerName." , ".$retailerPhone.", ".$billDate.", ".$billNo.", ".$dateOfOrder.", ".$itemCode.",  ".$qty.", ".$fields[6]." shouldn't be empty  \n";				
	} 	elseif(empty($qty) && $qty > 0) {
	$strErr .= "Error in Row".$row." ,".$salesmanName.",".$salesmanPhone." ,".$distributorCode." ,".$retailerName." , ".$retailerPhone.", ".$billDate.", ".$billNo.", ".$dateOfOrder.", ".$itemCode.",  ".$qty.", ".$fields[7]." shouldn't be empty  \n";				
	} 

	
	$str_err_array=array($strErr);		
	$flag=false;


	} 
	


	// Check salesman with phone number exists in the system.

	if($flag){

	$condi =" LOWER(salesman_name)='".mysql_real_escape_string($salesmanName)."' AND  salesman_phome_no ='".$salesmanPhone."'";

	$resultset = $this->_getSelectList('table_salesman','salesman_id','',$condi);

		if(!is_array($resultset) && empty($resultset) && $resultset[0]->salesman_id == "") {

			$strErr .= "Error in Row".$row." ,".$salesmanName.",".$salesmanPhone." ,".$distributorCode." ,".$retailerName." , ".$retailerPhone.",".$billDate.", ".$billNo.",  ".$dateOfOrder.", ".$itemCode.",  ".$qty.", invalid salesman  \n";	
			$str_err_array=array($strErr);	
			$flag=false;

		} else {
			$salesman_id = $resultset[0]->salesman_id;
		}
	}		
	


	// Check distributor with the help of distributor code exists in the system.

	if($flag){

	$condi =" LOWER(distributor_code) ='".mysql_real_escape_string($distributorCode)."'";

	$resultset = $this->_getSelectList('table_distributors','distributor_id','',$condi);

		if(!is_array($resultset) && empty($resultset) && $resultset[0]->distributor_id == "") {

			$strErr .= "Error in Row".$row." ,".$salesmanName.",".$salesmanPhone." ,".$distributorCode." ,".$retailerName." , ".$retailerPhone.",".$billDate.", ".$billNo.",  ".$dateOfOrder.", ".$itemCode.",  ".$qty.", Distributor code doesn't exists in the system  \n";
			$str_err_array=array($strErr);	
			$flag=false;

		} else {

			$distributor_id = $resultset[0]->distributor_id;
		}
	}		
		



	// Check retailer with phone number and distributor exists in the system.

	if($flag){

	$condi =" LOWER(retailer_name)='".mysql_real_escape_string($retailerName)."' AND  retailer_phone_no ='".$retailerPhone."' AND distributor_id =".$distributor_id;

	$resultset = $this->_getSelectList('table_retailer','retailer_id','',$condi);

		if(!is_array($resultset) && empty($resultset) && $resultset[0]->retailer_id == "") {

			$strErr .= "Error in Row".$row." ,".$salesmanName.",".$salesmanPhone." ,".$distributorCode." ,".$retailerName." , ".$retailerPhone.", ".$billDate.", ".$billNo.", ".$dateOfOrder.", ".$itemCode.",  ".$qty.", invalid retailer or retailer doesn't mapped with this ditributor  \n";	
			$str_err_array=array($strErr);	
			$flag=false;

		} else {

			$retailer_id = $resultset[0]->retailer_id;

		}
	}		




	// Check item exists in the system.	

	if($flag){

	$condi =" LOWER(item_code)='".mysql_real_escape_string($itemCode)."'";

	$resultset = $this->_getSelectList('table_item','item_id','',$condi);

		if(!is_array($resultset) && empty($resultset) && $resultset[0]->item_id == "") {

			$strErr .= "Error in Row".$row." ,".$salesmanName.",".$salesmanPhone." ,".$distributorCode." ,".$retailerName." , ".$retailerPhone.", ".$billDate.", ".$billNo.", ".$dateOfOrder.", ".$itemCode.",  ".$qty.", item doesn't exists in the system  \n";
			$str_err_array=array($strErr);	
			$flag=false;
			
		} else {

			$item_id = $resultset[0]->item_id;

		}
	}


	// Check Item Price

	if($flag) {

		$resultset = $this->_getSelectList('table_price','item_mrp, item_dp',''," item_id='".$item_id."' AND start_date<='".date('Y-m-d')."' AND end_date>='".date('Y-m-d')."'");

		if(sizeof($resultset) == 0  || $resultset[0]->item_mrp <= 0 ) {

			$strErr .= "Error in Row".$row." ,".$salesmanName.",".$salesmanPhone." ,".$distributorCode." ,".$retailerName." , ".$retailerPhone.", ".$billDate.", ".$billNo.", ".$dateOfOrder.", ".$itemCode.",  ".$qty.", item price(mrp) doesn't exists in the system or should not be zero. \n";
			$str_err_array=array($strErr);	
			$flag=false;
			
		} else {

			$item_price = $resultset[0]->item_mrp;

		}

	}


	// Check date of order is valid date or not 



	if($flag) {

		if(strtotime($dateOfOrder) == strtotime('1970-01-01')) {

			$strErr .= "Error in Row".$row." ,".$salesmanName.",".$salesmanPhone." ,".$distributorCode." ,".$retailerName." , ".$retailerPhone.", ".$billDate.", ".$billNo.", ".$dateOfOrder.", ".$itemCode.",  ".$qty.", date of order is not in valid format(Y-m-d)  \n";
			$str_err_array=array($strErr);	
			$flag=false;

		}

	}



	  $order_id  = NULL;
	
	  if($billNo > 0 && !empty($billDate)) { $order_status  = "D"; } else {  $order_status = "A"; }








	// Add order into table_order

	if($flag){	

		if($salesman_id > 0 && $distributor_id > 0 && $retailer_id > 0 && $item_id ) {

			$data = array();

			$resultset = $this->_getSelectList('table_order','order_id',''," salesman_id='".$salesman_id."' AND distributor_id='".$distributor_id."' AND retailer_id ='".$retailer_id."' AND date_of_order = '".$dateOfOrder."'");

	        
	        if(sizeof($resultset) > 0  && $resultset[0]->order_id >0) {

	          $order_id = $resultset[0]->order_id ;

	        } else { // Insert new Order

	        $data['account_id']                   =   $_SESSION['accountId'];
	        $data['salesman_id']                  =   $salesman_id;
	        $data['distributor_id']               =   $distributor_id;
	        $data['retailer_id']                  =   $retailer_id;
	        $data['bill_date']                    =   $billDate;
	        $data['bill_no']                      =   $billNo;
	        $data['date_of_order']                =   $dateOfOrder;	
	        $data['time_of_order']                =   NULL;
	        $data['lat']                          =   NULL;
	        $data['lng']                          =   NULL;
	        $data['accuracy_level']               =   NULL;
	        $data['comments']                     =   NULL;
	        $data['order_type']                   =   "Yes"; // Yes , No , Adhoc
	        $data['location_provider']            =   NULL;
	        $data['tag_id']                       =   NULL;
	        $data['tag_description']              =   NULL;
	        $data['total_invoice_amount']         =   0;
	        $data['acc_total_invoice_amount']     =   0;

	        $data['last_update_date']             =   $date;
	        $data['last_update_status']           =   'New';
	        $data['order_status']                 =   $order_status;  // A = New Order, I = Processed , D = Dispatched 

	        // Actual Shceme Details 

	        $data['discount_type']                =   NULL;    // 1 = Amount, 2 = FOC, 3 = Percentage
	        $data['discount_amount']              =   NULL;
	        $data['dicount_percentage']           =   NULL;
	        $data['free_item_id']                 =   NULL;     // FOC Item ID If discount type FOC
	        $data['free_item_qty']                =   NULL;     // How many FOC Item qty
	        $data['discount_id']                  =   NULL;     // Scheme ID
	        $data['discount_desc']                =   NULL;     // Scheme Description


	        $data['acc_discount_type']            =   NULL;    // 1 = Amount, 2 = FOC, 3 = Percentage
	        $data['acc_discount_amount']          =   NULL;
	        $data['acc_dicount_percentage']       =   NULL;
	        $data['acc_free_item_id']             =   NULL;     // FOC Item ID If discount type FOC
	        $data['acc_free_item_qty']            =   NULL;     // How many FOC Items
	        $data['acc_discount_id']              =   NULL;     // Scheme ID


	        $order_id= $this->_dbInsert($data,'table_order'); 

	        }



	        /***************************************************************************************
	        * DESC : Add/Update order detail items
	        * Author : AJAY
	        * Created : 29th June 2015
	        * 
	        **/


	        if(isset($order_id) && $order_id > 0 && $item_id > 0) {


	        $data  = array();


	        $discount_id            =   NULL;
	        $discount_desc          =   NULL;
	        $discount_type          =   NULL;
	        $discount_amount        =   NULL;
	        $discount_percentage    =   NULL;
	        $free_item_id           =   NULL;
	        $total_free_quantity    =   NULL;


	        $acc_discount_type      =   NULL;
	        $acc_discount_amount    =   NULL;
	        $acc_dicount_percentage =   NULL;
	        $acc_free_item_id       =   NULL;
	        $acc_free_item_qty      =   NULL;
	        $acc_discount_id        =   NULL;

	        if($qty > 0) {


				$resultset = $this->_getSelectList2('table_order_detail','item_id',''," order_id='".$order_id."' AND item_id='".$item_id."'");

		        
		        if(sizeof($resultset) > 0  && $resultset[0]->item_id >0) {
	       
			        $data['price']                  	=   $item_price;
			        $data['quantity']               	=   $qty;
			        $data['total']                      =   $qty * $item_price;
			        $data['acc_quantity']               =   $qty;
			        $data['acc_total']                  =   $data['total'];
			       	$data['last_update_date']           =   $date;
			        $data['last_update_status']         =   'Update';

			        $this->_dbUpdate2($data,'table_order_detail', " order_id='".$order_id."' AND item_id='".$item_id."'");

		        } else { // Insert new Order


			        $data['order_id']               	=   $order_id;
			        $data['item_id']               	 	=   $item_id;
			        $data['free_item_id']           	=   $free_item_id;        
			        $data['price']                  	=   $item_price;
			        $data['quantity']               	=   $qty;
			        $data['total']                      =   $qty * $item_price;
			        $data['acc_quantity']               =   $qty;
			        $data['acc_total']                  =   $data['total'];
			        $data['color_id']                   =   NULL;
			        $data['color_type']                 =   NULL;        
			        $data['type']                       =   1 ;     // 1 = Normal, 2 = Free Item
			        $data['price_type']                 =   1;     // 1 = MRP , 2 = DP , 3 = PTR
			        $data['discount_id']                =   $discount_id;
			        $data['discount_desc']              =   $discount_desc;
			        $data['discount_type']              =   $discount_type;
			        $data['discount_amount']            =   $discount_amount;
			        $data['discount_percentage']        =   $discount_percentage;
			        $data['total_free_quantity']        =   $total_free_quantity;
			        $data['last_update_date']           =   $date;
			        $data['last_update_status']         =   'New';
			        $data['order_detail_status']        =   '1';   // 1 = New , 2 = Accepted, 3 = Rejected, 4 = Discount Applicable, 5 = Discount Not Applicable
			        $data['tag_id']                     =   NULL;
			        $data['acc_discount_type']          =   $acc_discount_type;
			        $data['acc_discount_amount']        =   $acc_discount_amount;
			        $data['acc_dicount_percentage']     =   $acc_dicount_percentage;
			        $data['acc_free_item_id']           =   $acc_free_item_id;
			        $data['acc_free_item_qty']          =   $acc_free_item_qty;
			        $data['acc_discount_id']            =   $acc_discount_id;

			        $this->_dbInsert($data,'table_order_detail'); // Saved item detail

			     }

			   } // End of check quantity



		       /************************************************************************
		       * DESC : Update table order total invoice amount (get total amount from table_order_detail)
		       * Auhtor : AJAY
		       * Created : 29th July 2015
		       *
		       *
		       **/

		       $data = array();

		       $getTtlInvoiceAmount = $this->_getSelectList2('table_order_detail','SUM(total) AS total_invoice_amount',''," order_id='".$order_id."' GROUP BY order_id ");

			        if(sizeof($getTtlInvoiceAmount) > 0 && $getTtlInvoiceAmount[0]->total_invoice_amount > 0 ) {

			         $data['total_invoice_amount']      = $getTtlInvoiceAmount[0]->total_invoice_amount;
			     	 $data['acc_total_invoice_amount']  =  $getTtlInvoiceAmount[0]->total_invoice_amount;

			     	 	$this->_dbUpdate2($data,'table_order', " order_id=".$order_id);

			       }




	        } // end of check order Id



		}  // end of check retailer id, salesman id, distributor id
		
	} // end of flag tag
			
	} // check size of row read by fgetcsv function


	$row++;	   
	
	} // End of while loop


		fclose($file);	  
	   if($row<=1)
	   	return "no";
	   else 
	  	return $str_err_array;



 }	// End of function




// Added taluka and market data list ajay@2015-10-07

function uploadDistrictTalukas() {

	$fields = array('Taluka Name*', 'District Name*','Taluka Code*');	
	$file = fopen($_FILES['fileToUpload']['tmp_name'],"r");
	$strErr="";
	$row=0;		

	while(! feof($file)){

	$arrFile=fgetcsv($file);
	$flag=true;	

	if($row>0 && is_array($arrFile)) {	

		if(empty($arrFile[0]) || $arrFile[0]=="Taluka Name*" || empty($arrFile[1]) ||$arrFile[1]=="District Name*" || empty($arrFile[2]) ||$arrFile[2]=="Taluka Code*"){
			if(empty($arrFile[0]) || $arrFile[0]=="Taluka Name*" ){
				$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2]." , ".$fields[0]." shouldn't be empty  \n";
			} else if(empty($arrFile[1]) || $arrFile[1]=="District Name*"){
				$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2]." , ".$fields[1]." shouldn't be empty  \n";
			}else if(empty($arrFile[2]) || $arrFile[2]=="Taluka Code*"){
				$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2]." , ".$fields[2]." shouldn't be empty  \n";
			}				
			$str_err_array=array($strErr);						
			$flag=false;
		}
		
	$taluka_name = $this->clean(mysql_real_escape_string(strtolower(trim($arrFile[0]))));	
	$city_name = $this->clean(mysql_real_escape_string(strtolower(trim($arrFile[1]))));
	$taluka_code = $this->clean(mysql_real_escape_string(strtolower(trim($arrFile[2]))));
				 
				 
		if($flag) {
			$condi=" LOWER(city_name)='".$city_name."'";
			$CityRec=$this->_getSelectList2('city',"city_id",'',$condi);
			$city_id= $CityRec[0]->city_id;
			
			if($city_id=='') {
				$strErr .= "Error in Row".$row." ,".$taluka_name.",".$city_name." , District not found \n";
				$str_err_array=array($strErr);						
				$flag=false;
			}
		}
								 
				 
		 if($flag) {		

			$condi=" LOWER(taluka_code)='".$taluka_code."'  AND city_id = ".$city_id;
			$TalukaRec = $this->_getSelectList('table_taluka',"taluka_id, taluka_name,taluka_code",'',$condi);	
			$taluka_id   = $TalukaRec[0]->taluka_id;
			
				if(!is_array($TalukaRec)){
					$taluka = array();
					$taluka['account_id'] = $_SESSION['accountId'];
					$taluka['city_id'] = $city_id;
					$taluka['taluka_name']= $taluka_name;
					$taluka['taluka_code']= $taluka_code;
					$taluka['status']= 'A';
					$taluka_id = $this->_dbInsert($taluka,'table_taluka');
				}  else  {
					$strErr .= "Error in Row".$row." ,".$taluka_name.",".$city_name." , Taluka already exists in the system \n";
					$str_err_array=array($strErr);						
					$flag=false;
				}
		  }
		  }
		$row++;	   

		}	
		fclose($file);	  
	  
	   if($row<=1)
	   	return "no";
	   else 
	  	return $str_err_array;
	
	}






function uploadTalukaMarkets() {

	$fields = array('City Name*', 'Taluka Name*','Market code*');	
	$file = fopen($_FILES['fileToUpload']['tmp_name'],"r");
	$strErr="";
	$row=0;		

	while(! feof($file)){

	$arrFile=fgetcsv($file);
	$flag=true;	

	if($row>0 && is_array($arrFile)) {	

		if(empty($arrFile[0]) || $arrFile[0]=="City Name*" || empty($arrFile[1]) ||$arrFile[1]=="Taluka Name*" || empty($arrFile[2]) ||$arrFile[2]=="Market code*"){
			if(empty($arrFile[0]) || $arrFile[0]=="City Name*" ){
				$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2]." , ".$fields[0]." shouldn't be empty  \n";
			} else if(empty($arrFile[1]) || $arrFile[1]=="Taluka Name*"){
				$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2]." , ".$fields[1]." shouldn't be empty  \n";
			}else if(empty($arrFile[2]) || $arrFile[2]=="Market code*"){
				$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2]." , ".$fields[2]." shouldn't be empty  \n";
			}				
			$str_err_array=array($strErr);						
			$flag=false;
		}
		
	$market_name = $this->clean(mysql_real_escape_string(strtolower(trim($arrFile[0]))));	
	$taluka_name = $this->clean(mysql_real_escape_string(strtolower(trim($arrFile[1]))));
	$market_code = $this->clean(mysql_real_escape_string(strtolower(trim($arrFile[2]))));

				 
				 
		if($flag) {
			$condi =" LOWER(taluka_name)='".$taluka_name."'";
			$TalukaRec =$this->_getSelectList('table_taluka',"taluka_id",'',$condi);
			$taluka_id = $TalukaRec[0]->taluka_id;
			
			if($taluka_id=='') {
				$strErr .= "Error in Row".$row." ,".$market_name.",".$taluka_name." , Taluka not found \n";
				$str_err_array=array($strErr);						
				$flag=false;
			}
		}
								 
				 
		 if($flag) {		

			$condi=" LOWER(market_code)='".$market_code."'  AND taluka_id = ".$taluka_id;
			$MarketRec = $this->_getSelectList('table_markets',"market_id, market_name",'',$condi);	
			$market_id   = $MarketRec[0]->market_id;
			
				if(!is_array($MarketRec)){
					$market = array();
					$market['account_id'] = $_SESSION['accountId'];
					$market['taluka_id'] = $taluka_id;
					$market['market_name']= $market_name;
					$market['market_code']= $market_code;
					$market['status']= 'A';
					$market_id = $this->_dbInsert($market,'table_markets');
				}  else  {
					$strErr .= "Error in Row".$row." ,".$market_name.",".$taluka_name." , City already exists in the system \n";
					$str_err_array=array($strErr);						
					$flag=false;
				}
		  }
		  }
		$row++;	   

		}	
		fclose($file);	  
	  
	   if($row<=1)
	   	return "no";
	   else 
	  	return $str_err_array;
	
	}


// Added taluka and market data list ajay@2015-10-07





	/************************** Add Monthly Route Assignments ***********************************/

	
	function uploadMonthlyRouteList () {
    
    //echo 'aaa'; exit;
     
     $fields = array('Salesman Name*','Route Name*','Year*','Month*','day1','day2','day3','day4','day5','day6','day7','day8','day9','day10','day11','day12','day13','day14','day15','day16','day17','day18','day19','day20','day21','day22','day23','day24','day25','day26','day27','day28','day29','day30','day31');
	$file = fopen($_FILES['fileToUpload']['tmp_name'],"r");
	$strErr="";
	$resultset = "";
	$row=0;	

	$salesman_name ="";
	$route_name="";
	$year="";
	$month="";
	$day1="";
	$day2="";
	$day3="";
	$day4="";
	$day5="";
	$day6="";
	$day7="";
	$day8="";
	$day9="";
	$day10="";
	$day11="";
	$day12="";
	$day13="";
	$day14="";
	$day15="";
	$day16="";	
	$day17="";	
	$day18="";	
	$day19="";	
	$day20="";
	$day21="";
	$day22="";
	$day23="";
	$day24="";
	$day25="";
	$day26="";
	$day27="";
	$day28="";
	$day29="";
	$day30="";
	$day31="";	

	
	while(!feof($file))
	{	
	$data = array();
	$data2 = array();
	
		  
		$arrFile=fgetcsv($file);	
		$flag=true;	
		
		
		
		$day1            =trim($arrFile[4]);
		$day2            =trim($arrFile[5]);
		$day3            =trim($arrFile[6]);
		$day4            =trim($arrFile[7]);
		$day5            =trim($arrFile[8]);
		$day6            =trim($arrFile[9]);
		$day7            =trim($arrFile[10]);
		$day8            =trim($arrFile[11]);
		$day9            =trim($arrFile[12]);
		$day10           =trim($arrFile[13]);
		$day11           =trim($arrFile[14]);
		$day12           =trim($arrFile[15]);
		$day13           =trim($arrFile[16]);
		$day14           =trim($arrFile[17]);
		$day15           =trim($arrFile[18]);
		$day16           =trim($arrFile[19]);
		$day17           =trim($arrFile[20]);
		$day18           =trim($arrFile[21]);
		$day19           =trim($arrFile[22]);
		$day20           =trim($arrFile[23]);
		$day21           =trim($arrFile[24]);
		$day22           =trim($arrFile[25]);
		$day23           =trim($arrFile[26]);
		$day24           =trim($arrFile[27]);
		$day25           =trim($arrFile[28]);
		$day26           =trim($arrFile[29]);
		$day27           =trim($arrFile[30]);
		$day28           =trim($arrFile[31]);
		$day29           =trim($arrFile[32]);
		$day30           =trim($arrFile[33]);
		$day31           =trim($arrFile[34]);
		$salesman_name 	 =trim($arrFile[0]);
		$route_name 	 =trim($arrFile[1]);
		$year            =trim($arrFile[2]);
		$month           =trim($arrFile[3]);


       /* Day not assignment */





		/*echo '<pre>';
		print_r($arrFile);
	    

	    echo '<pre>';
	    $arr=array_filter($arrFile);

	    print_r($arr);

        echo sizeof($arr);
       
	    for($i=3;$i<sizeof($arr)-4;$i++)
	       {	
	       	$abc[]=$arr;
           

	       }       
*/


	if($row>0 && is_array($arrFile)) {
	if(empty($salesman_name) || $route_name=="Route Name*" || empty($year) || $year=="Year*" || empty($month) || $month=="Month*" ){
	
	if(empty($salesman_name)){
	$strErr .= "Error in Row".$row." ,".$salesman_name.",".$route_name." ,".$year." ,".$month.", ".$fields[0]." shouldn't be empty  \n";
	} elseif(empty($route_name)){
	$strErr .= "Error in Row".$row." ,".$salesman_name.",".$route_name." ,".$year." ,".$month.", ".$fields[1]." shouldn't be empty  \n";
	} elseif(empty($year)){
	$strErr .= "Error in Row".$row." ,".$salesman_name.",".$route_name." ,".$year." ,".$month.", ".$fields[2]." shouldn't be empty  \n";
	} elseif(empty($month)){
	$strErr .= "Error in Row".$row." ,".$salesman_name.",".$route_name." ,".$year." ,".$month.", ".$fields[3]." shouldn't be empty  \n";
	}
    $str_err_array=array($strErr);						
	$flag=false;
	} 
	
	// All days should not be empty

	



	// Check Salesman exists in the system or not.		
	if($flag){
	
	 $condi =" LOWER(salesman_name)='".$salesman_name."'";
	$resultset = $this->_getSelectList('table_salesman','salesman_name,salesman_id','',$condi);
		if(!is_array($resultset) && empty($resultset) && $resultset[0]->salesman_id == "") {
			$strErr .= "Error in Row".$row." ,".$salesman_name.",".$route_name." ,".$year." ,".$month." ,Salesman doesn't exists in the system. \n";	
			$str_err_array=array($strErr);	
			$flag=false;
		} else {
			$salesman_id = $resultset[0]->salesman_id;
		}
	}
	
	
	if($flag){
			
	$condi =" LOWER(route_name)='".$route_name."' ";
	$resultset = $this->_getSelectList('table_route','route_name,route_id','',$condi);
		if(!is_array($resultset) && empty($resultset) && $resultset[0]->location == "") {
			$strErr .= "Error in Row".$row." ,".$salesman_name.",".$route_name." Route Name doesn't exists in the system. \n";	
			$str_err_array=array($strErr);	
			$flag=false;
		}
		else{
			$route_id = $resultset[0]->route_id;	
		} 
	}

 // Route Name alredy exits


	/*if($flag){
			
	$condi =" salesman_id ='".$salesman_id."' month='".$month."' year='".$year."' ";
	$resultset = $this->_getSelectList('table_route_scheduled','salesman_id,month,year','',$condi);
		if(!is_array($resultset) && empty($resultset) && $resultset[0]->location == "") {
			$strErr .= "Error in Row".$row." ,".$salesman_name.",".$route_name." Route Name doesn't exists in the system. \n";	
			$str_err_array=array($strErr);	
			$flag=false;
		}
		else{
			$route_id = $resultset[0]->route_id;	
		} 
	}	*/


	
	// Message show in excel sheet current month, with have current to previous date should not be updted.

	/*if($falg)
		{
			$condi= " ";	

		}
	*/

    if($flag)
    {	$current_month   =date('n');
    	//$monthValue = date("m", strtotime($month));
		if($current_month=="1" || $current_month=="3" || $current_month=="5" || $current_month=="7" || $current_month=="8" || $current_month=="10" || $current_month=="12"){
			$monthValue = date("m", strtotime($month)-1);
		} else {
			$monthValue = date("m", strtotime($month));
		}
    	if($monthValue<$current_month && $year<=date('Year')){
    		$strErr .= $month."  Month and Year could not be less than the current year. \n";	
			$str_err_array=array($strErr);	
			$flag=false;

    	}


    }

	
	// Insert/Update data in the system	

	if($flag)
	  {         
	  			/* Find Already exits route plans */
	  			 //$monthValue = date("m", strtotime($month)); 
	  			if($current_month=="1" || $current_month=="3" || $current_month=="5" || $current_month=="7" || $current_month=="8" || $current_month=="10" || $current_month=="12"){
					$monthValue = date("m", strtotime($month)-1);
				} else {
					$monthValue = date("m", strtotime($month));
				}
	  			 $condi =" LOWER(salesman_id)='".$salesman_id."' and  month='".$monthValue."' and year='".$year."' ";
				 $route_exits = $this->_getSelectList('table_route_scheduled','salesman_id,month,year,route_schedule_id','',$condi);
				 /*echo '<pre>';
				 print_r($route_exits);
				 echo sizeof($route_exits);*/
				 
				 if(sizeof($route_exits)>0 && $route_exits[0]->route_schedule_id!="")
				 {
				  $route_schedule_id=$route_exits[0]->route_schedule_id; 
				 }
				else
				{ 
	  	        //$monthValue = date("m", strtotime($month)); 
				if($current_month=="1" || $current_month=="3" || $current_month=="5" || $current_month=="7" || $current_month=="8" || $current_month=="10" || $current_month=="12"){
					$monthValue = date("m", strtotime($month)-1);
				} else {
					$monthValue = date("m", strtotime($month));
				}
	  			if($route_id!='') {
				$data['account_id']			= $_SESSION['accountId'];   // promo_desc
				$data['salesman_id']		= $salesman_id; 
				$data['month']				= $monthValue;
				$data['year']				= $year;
				$data['created_date']		= date('Y-m-d');
				$data['status']				= 'A';
				$route_schedule_id = $this->_dbInsert($data,'table_route_scheduled');
				}
			   }	
			

			if($route_schedule_id!="")
			{        $current_month   =date('n');
					 $dayValue        =date('d');
					 /* Already Exits Route details have been first delete */
					 $excondi =" LOWER(route_id)='".$route_id."' and route_schedule_id='".$route_schedule_id."' ";
				     $route_detaild_exits = $this->_getSelectList2('table_route_schedule_details','route_id,route_schedule_id','',$excondi);

				     if(is_array($route_detaild_exits)){
				     $this->mysql_query("delete from table_route_schedule_details where route_schedule_id='".$route_schedule_id."'");	
				     }
					 

			  		for($i=4;$i<sizeof($arrFile); $i++)
			  		{     
			  			   
			  			  if($current_month==$monthValue){ 
                         if($arrFile[$i]!=""){ $key=$i; $assign_day= $key+$dayValue-4;

                          if($route_id!='') {
							$data2['route_id']				= $route_id;   // promo_desc
							$data2['assign_day']			= $assign_day; 
							$data2['route_schedule_id']		= $route_schedule_id;
							$data2['status']				= 'A';
							$route_detail_id = $this->_dbInsert($data2,'table_route_schedule_details');
							 }
			  			 }
			  		}

			  		else{

			  			if($arrFile[$i]!=""){ $key=$i; $assign_day= $key-3;

                          if($route_id!='') {
							$data2['route_id']				= $route_id;   // promo_desc
							$data2['assign_day']			= $assign_day; 
							$data2['route_schedule_id']		= $route_schedule_id;
							$data2['status']				= 'A';
							$route_detail_id = $this->_dbInsert($data2,'table_route_schedule_details');
							 }
			  			 }
			  	}
	  	    }  // ends loop
	     } // ends route_schedule_id condition

      } // ends flag

			
		}
		  $row++;	   
		}		
		
		fclose($file);	  
	   if($row<=1)
	   	return "no";
	   else 
	  	return $str_err_array;
 
  }


		/********************************** Serial no scan List 24 feb 2016 By Abhishek *************************/
	function uploadserialscan() {
		$fields = array('ID*', 'Status*');	
		$file = fopen($_FILES['fileToUpload']['tmp_name'],"r");
		$strErr="";
		$row=0;		
		while(! feof($file))
		{		  
		$arrFile=fgetcsv($file);
		
		
			
		$flag=true;	
		
		
	 	if($row>0 && is_array($arrFile)) {	
	  
	  
	  
	    if(empty($arrFile[0]) || $arrFile[0]=="ID*" || empty($arrFile[6]) ||$arrFile[6]=="Status*"){
		  
		  		  if(empty($arrFile[0]) || $arrFile[0]=="ID*" ){
					$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[6].", ".$fields[0]." shouldn't be empty  \n";
				 }
				  else if(empty($arrFile[6]) || $arrFile[6]=="Status*"){
					$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[6]." , ".$fields[1]." shouldn't be empty  \n";
				 }
		 		$str_err_array=array($strErr);						
				$flag=false;
					
		  	}
		  	

		if($flag){
			$sql = "UPDATE table_serial_number_scan SET serial_number_status='".$arrFile[6]."',last_update_status='Update',last_update_date='' WHERE serial_id='".$arrFile[0]."'";
			mysql_query($sql) or die(mysql_error());
			
		}								 
		}
		$row++;	   
		}	
		fclose($file);	  
	  
	   if($row<=1)
	   	return "no";
	   else 
	  	return $str_err_array;
	
	}
	/********************************** Serial no scan List 24 feb 2016 By Abhishek *************************/


/********************* Start Upload Warehouses File 20 Feb 2017 By Pooja ****************************************/	


	function uploadWarehouseFile(){		
	
		$fields = array('Warehouse Prefix*', 'Warehouse Name*', 'Warehouse Code*', 'Warehouse Phone No*', 'Warehouse Address', 'Country*', 'Region*', 'Zone*', 'State*', 'City*', 'Area', 'Warehouse Incharge Phone No1*', 'Warehouse Incharge Phone No2', 'Warehouse Incharge Phone No3', 'Warehouse Incharge Email-ID1*', 'Warehouse Incharge Email-ID2', 'Warehouse Incharge Email-ID3', 'Warehouse Incharge Name*', 'Pincode/Zipcode');
	
		$file = fopen($_FILES['fileToUpload']['tmp_name'],"r");
		$strErr="";
		$row=0;		
		while(! feof($file)){		  
			  $arrFile=fgetcsv($file);
			 /* echo "<pre>";
			  print_r($arrFile); */
			  $flag=true;
		if($row>0 && is_array($arrFile)) {	
		

			//$division_name = mysql_real_escape_string(strtolower($arrFile[4]));
			
		
			  
			 if(empty($arrFile[0]) || $arrFile[0]=="Warehouse Prefix*" || empty($arrFile[1]) || empty($arrFile[2]) || empty($arrFile[3]) || empty($arrFile[5]) || empty($arrFile[6]) || empty($arrFile[7]) || empty($arrFile[8]) ||empty($arrFile[9]) || empty($arrFile[11]) || empty($arrFile[14]) || empty($arrFile[17]) )
			{
		  		if(empty($arrFile[0]) || $arrFile[0]=="Warehouse Prefix*" )
		  		{
					$strErr .= "Error in Row".$row." , ".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5].", ".$arrFile[6]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].",".$arrFile[10]."  ,".$arrFile[11].",".$arrFile[12].", ".$arrFile[13]." ,".$arrFile[14].",".$arrFile[15]." ,".$arrFile[16].",".$arrFile[17]."  ,".$arrFile[18]." ,".$fields[0]." shouldn't be empty  \n";
				}
		  		else if(empty($arrFile[1]))
		  		{
					$strErr .= "Error in Row".$row." , ".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5].", ".$arrFile[6]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].",".$arrFile[10]."  ,".$arrFile[11].",".$arrFile[12].", ".$arrFile[13]." ,".$arrFile[14].",".$arrFile[15]." ,".$arrFile[16].",".$arrFile[17]."  ,".$arrFile[18]." ,".$fields[1]." shouldn't be empty  \n";
				}
				else if(empty($arrFile[2]))
				{
					$strErr .= "Error in Row".$row." , ".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5].", ".$arrFile[6]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].",".$arrFile[10]."  ,".$arrFile[11].",".$arrFile[12].", ".$arrFile[13]." ,".$arrFile[14].",".$arrFile[15]." ,".$arrFile[16].",".$arrFile[17]."  ,".$arrFile[18]." , ".$fields[2]." shouldn't be empty  \n";
				}
				else if(empty($arrFile[3]))
				{
					$strErr .= "Error in Row".$row." , ".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5].", ".$arrFile[6]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].",".$arrFile[10]."  ,".$arrFile[11].",".$arrFile[12].", ".$arrFile[13]." ,".$arrFile[14].",".$arrFile[15]." ,".$arrFile[16].",".$arrFile[17]."  ,".$arrFile[18]." , ".$fields[3]." shouldn't be empty  \n";
				}
				else if(empty($arrFile[5]))
				{
					$strErr .= "Error in Row".$row." , ".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5].", ".$arrFile[6]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].",".$arrFile[10]."  ,".$arrFile[11].",".$arrFile[12].", ".$arrFile[13]." ,".$arrFile[14].",".$arrFile[15]." ,".$arrFile[16].",".$arrFile[17]."  ,".$arrFile[18]." , ".$fields[5]." shouldn't be empty  \n";
				}
				else if(empty($arrFile[6]))
				{
					$strErr .= "Error in Row".$row." , ".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5].", ".$arrFile[6]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].",".$arrFile[10]."  ,".$arrFile[11].",".$arrFile[12].", ".$arrFile[13]." ,".$arrFile[14].",".$arrFile[15]." ,".$arrFile[16].",".$arrFile[17]."  ,".$arrFile[18]." , ".$fields[6]." shouldn't be empty  \n";
				}
				else if(empty($arrFile[7]))
		  		{
					$strErr .= "Error in Row".$row." , ".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5].", ".$arrFile[6]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].",".$arrFile[10]."  ,".$arrFile[11].",".$arrFile[12].", ".$arrFile[13]." ,".$arrFile[14].",".$arrFile[15]." ,".$arrFile[16].",".$arrFile[17]."  ,".$arrFile[18]." ,".$fields[7]." shouldn't be empty  \n";
				}
				else if(empty($arrFile[8]))
				{
					$strErr .= "Error in Row".$row." , ".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5].", ".$arrFile[6]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].",".$arrFile[10]."  ,".$arrFile[11].",".$arrFile[12].", ".$arrFile[13]." ,".$arrFile[14].",".$arrFile[15]." ,".$arrFile[16].",".$arrFile[17]."  ,".$arrFile[18]." , ".$fields[8]." shouldn't be empty  \n";
				}
				else if(empty($arrFile[9]))
				{
					$strErr .= "Error in Row".$row." , ".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5].", ".$arrFile[6]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].",".$arrFile[10]."  ,".$arrFile[11].",".$arrFile[12].", ".$arrFile[13]." ,".$arrFile[14].",".$arrFile[15]." ,".$arrFile[16].",".$arrFile[17]."  ,".$arrFile[18]." , ".$fields[9]." shouldn't be empty  \n";
				}
				else if(empty($arrFile[11]))
		  		{
					$strErr .= "Error in Row".$row." , ".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5].", ".$arrFile[6]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].",".$arrFile[10]."  ,".$arrFile[11].",".$arrFile[12].", ".$arrFile[13]." ,".$arrFile[14].",".$arrFile[15]." ,".$arrFile[16].",".$arrFile[17]."  ,".$arrFile[18]." ,".$fields[11]." shouldn't be empty  \n";
				}
				else if(empty($arrFile[14]))
				{
					$strErr .= "Error in Row".$row." , ".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5].", ".$arrFile[6]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].",".$arrFile[10]."  ,".$arrFile[11].",".$arrFile[12].", ".$arrFile[13]." ,".$arrFile[14].",".$arrFile[15]." ,".$arrFile[16].",".$arrFile[17]."  ,".$arrFile[18]." , ".$fields[14]." shouldn't be empty  \n";
				}
				else if(empty($arrFile[17]))
				{
					$strErr .= "Error in Row".$row." , ".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5].", ".$arrFile[6]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].",".$arrFile[10]."  ,".$arrFile[11].",".$arrFile[12].", ".$arrFile[13]." ,".$arrFile[14].",".$arrFile[15]." ,".$arrFile[16].",".$arrFile[17]."  ,".$arrFile[18]." , ".$fields[17]." shouldn't be empty  \n";
				}



					$str_err_array=array($strErr);						
					$flag=false;
		  	}
		  

		 // Warehouse Check
		  if($flag){
					$condi=	" account_id=".$_SESSION['accountId']." AND warehouse_phone_no='".mysql_real_escape_string(strtolower($arrFile[3]))."' AND  LOWER(warehouse_name)='".mysql_real_escape_string( $this->clean(strtolower($arrFile[1])))."' AND warehouse_prefix = '".mysql_real_escape_string( $this->clean(strtolower($arrFile[0])))."'";
					$aDisRec=$this->_getSelectList('table_warehouse','*','',$condi);
					if(is_array($aDisRec)) {
						$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5].", ".$arrFile[6]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].",".$arrFile[10]."  ,".$arrFile[11].",".$arrFile[12].", ".$arrFile[13]." ,".$arrFile[14].",".$arrFile[15]." ,".$arrFile[16].",".$arrFile[17]."  ,".$arrFile[18]."  ,already exists in the system. \n";
						$str_err_array=array($strErr);
						$flag =false;
					}
				}
				


		/* For Valid Phone Number*/
		if($flag){				
				
				if (!is_numeric($arrFile[3]) || !is_numeric($arrFile[11])) {					
					$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5].", ".$arrFile[6]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].",".$arrFile[10]."  ,".$arrFile[11].",".$arrFile[12].", ".$arrFile[13]." ,".$arrFile[14].",".$arrFile[15]." ,".$arrFile[16].",".$arrFile[17]."  ,".$arrFile[18]."  ,Please Provide the valid Phone Number. \n";				
					$str_err_array=array($strErr);						
					$flag=false;					
				} 
		
		}
		/* for checking Warehouse Prefix Existence*/
		if($flag){				
				$query = "SELECT warehouse_prefix FROM table_warehouse WHERE warehouse_prefix = '".$arrFile[0]."' LIMIT 1";
				$result = mysql_query($query);
				$num = mysql_num_rows($result);
				if($num==1)
				{
					$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5].", ".$arrFile[6]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].",".$arrFile[10]."  ,".$arrFile[11].",".$arrFile[12].", ".$arrFile[13]." ,".$arrFile[14].",".$arrFile[15]." ,".$arrFile[16].",".$arrFile[17]."  ,".$arrFile[18]."  ,Please Provide a unique warehouse prefix for all Warehouses. \n";				
					$str_err_array=array($strErr);						
					$flag=false;
				}
		
		}

		/*  For warehouseCode */

		$warehouseCode = mysql_real_escape_string(trim($arrFile[2]));
		if($warehouseCode!="")
		{	
		if($flag)
		{
		
		$condi=	" LOWER(warehouse_code)='".strtolower($warehouseCode)."'";
		$aRetRec= $this->_getSelectList('table_warehouse','warehouse_id','',$condi);				
			
					if(!is_array($aRetRec)){
						$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5].", ".$arrFile[6]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].",".$arrFile[10]."  ,".$arrFile[11].",".$arrFile[12].", ".$arrFile[13]." ,".$arrFile[14].",".$arrFile[15]." ,".$arrFile[16].",".$arrFile[17]."  ,".$arrFile[18]." , Sorry No warehouse code exists . \n";
						$str_err_array=array($strErr);						
						$flag=false;
					} else {
						$warehouse_id = $aRetRec[0]->warehouse_id;
					}

		}
	  }
	  else
	  {
	  
	  	$warehouse_id = mysql_real_escape_string(trim($arrFile[1]));
	  }

//	  $TallyWarehouseID = mysql_real_escape_string(trim($arrFile[2]));
		

// Fetching Country ID 		
		if($flag)
		{

			$condi=" LOWER(country_name)='".mysql_real_escape_string(strtolower($arrFile[5]))."'"; 
			$CountryRec=$this->_getSelectList2('country',"country_id",'',$condi);	
			if(isset($CountryRec[0]->country_id) && $CountryRec[0]->country_id>0) {
				$country_id=$CountryRec[0]->country_id;
			} else {
				$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5]." ,".$arrFile[6]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].",".$arrFile[10]."  ,".$arrFile[11].",".$arrFile[12]." ,".$arrFile[13]." ,".$arrFile[14].",".$arrFile[15]." ,".$arrFile[16].",".$arrFile[17]."  ,".$arrFile[18]." , Country Not Found. \n";
						$str_err_array=array($strErr);
						$flag =false;
							
			}



		}

// Fetching Region ID
		if($flag)
		{
			$condi=" LOWER(region_name)='".mysql_real_escape_string(strtolower($arrFile[6]))."' and country_id = '".$country_id."'"; 
			$RegionRec=$this->_getSelectList2('table_region',"region_id",'',$condi);	
			

			if(isset($RegionRec[0]->region_id) && $RegionRec[0]->region_id>0) {
				$region_id=$RegionRec[0]->region_id;
			} else {
				$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5]." ,".$arrFile[6]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].",".$arrFile[10]."  ,".$arrFile[11].",".$arrFile[12]." ,".$arrFile[13]." ,".$arrFile[14].",".$arrFile[15]." ,".$arrFile[16].",".$arrFile[17]."  ,".$arrFile[18]." , Region Not Found. \n";
						$str_err_array=array($strErr);
						$flag =false;
							
			}


		}		

// Fetching Zone ID
		if($flag)
		{
			$condi=" LOWER(zone_name)='".mysql_real_escape_string(strtolower($arrFile[7]))."' and region_id = '".$region_id."'"; 
			$ZoneRec=$this->_getSelectList2('table_zone',"zone_id",'',$condi);	
			


			if(isset($ZoneRec[0]->zone_id) && $ZoneRec[0]->zone_id>0) {
				$zone_id=$ZoneRec[0]->zone_id;
			} else {
				$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5]." ,".$arrFile[6]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].",".$arrFile[10]."  ,".$arrFile[11].",".$arrFile[12]." ,".$arrFile[13]." ,".$arrFile[14].",".$arrFile[15]." ,".$arrFile[16].",".$arrFile[17]."  ,".$arrFile[18].", Zone Not Found. \n";
						$str_err_array=array($strErr);
						$flag =false;
							
			}




		}		

// Fetching State ID
		if($flag)
		{
			$condi=" LOWER(state_name)='".mysql_real_escape_string(strtolower($arrFile[8]))."' and zone_id = '".$zone_id."'"; 
			$StateRec=$this->_getSelectList2('state',"state_id",'',$condi);	
			$state_id=$StateRec[0]->state_id;
			if(!is_array($StateRec)){
				$state = array();
				$state['zone_id']=$this->clean($zone_id);
				$state['state_name']=mysql_real_escape_string( $this->clean($arrFile[8]));
				$state['last_update_date']=date('Y-m-d');
				$state['last_update_status']='New';
				$state_id=$this->_dbInsert($state,'state');
			}
		}

// Fetching City ID
		if($flag){					
					$condi=" LOWER(city_name)='".mysql_real_escape_string(strtolower($arrFile[9]))."' AND state_id='".$state_id."'";
				 	$CityRec=$this->_getSelectList2('city',"city_id",'',$condi);	
					$city_id=$CityRec[0]->city_id;
					if(!is_array($CityRec)){
						$city = array();
						$city['state_id']=$this->clean($state_id);
						$city['city_name']=mysql_real_escape_string( $this->clean($arrFile[9]));
						$city['last_update_date']=date('Y-m-d');
						$city['last_update_status']='New';
						$city_id=$this->_dbInsert($city,'city');
					}
				 }

/* for market */
		if($flag){				
					

			$condi=" LOWER(market_name)='".mysql_real_escape_string($this->clean(strtolower($arrFile[10])))."'";
			
			$MarketRec=$this->_getSelectList(' table_markets',"market_id",'',$condi);	
			$market_id= $MarketRec[0]->market_id;

			if(sizeof($MarketRec)<=0){
							//$strErr .="Invalid city name: &nbsp;&nbsp;".implode(",",$arrFile)."";
							//$flag =false;
				$market = array();
				$market['account_id'] 	=$_SESSION['accountId'];	
						
				$market['market_name']	= mysql_real_escape_string( $this->clean($arrFile[10]));
				$market['status'] 		='A';	
				$market_id=$this->_dbInsert($market,' table_markets');
			}
		}
	
				 
		if($flag){	
			//echo mysql_real_escape_string(strtolower( $this->clean($arrFile[0])))."<br>";			
			$condi=	" LOWER(warehouse_name)='".mysql_real_escape_string(strtolower($this->clean($arrFile[1])))."' AND state_id ='".$state_id."' AND city_id ='".$city_id."' AND warehouse_phone_no ='".mysql_real_escape_string($arrFile[3])."'";			
					
					$aRetRec=$this->_getSelectList('table_warehouse','*','',$condi);
					if(is_array($aRetRec)) {					
						$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5].", ".$arrFile[6]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].",".$arrFile[10]."  ,".$arrFile[11].",".$arrFile[12].", ".$arrFile[13]." ,".$arrFile[14].",".$arrFile[15]." ,".$arrFile[16].",".$arrFile[17]."  ,".$arrFile[18]."  , Warehouse already exists in the system. \n";
						$str_err_array=array($strErr);						
						$flag=false;
			}
		}



	
	
	// if($flag){				
				
	// 			if (!is_numeric($arrFile[4])) {					
	// 				$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5]." ,".$arrFile[6]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].",".$arrFile[10]."  ,".$arrFile[11].", Please Provide the valid Phone Number. \n";				
	// 				$str_err_array=array($strErr);						
	// 				$flag=false;					
	// 			} 
		
	// 	}
	
	

			   // Check division name


			// if(!empty($division_name) && $flag) {
			//    		$condi=" LOWER(division_name)='".$division_name."' ";
			// 	 	$division_name_check =$this->_getSelectList('table_division',"division_id",'',$condi);
			// 		if(sizeof($division_name_check) == 0) {
			// 			$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5]." ,".$arrFile[6]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].",".$arrFile[10]."  ,".$arrFile[11]." , division name not found in the system. \n";
			// 			$str_err_array=array($strErr);
			// 			$flag =false;
			// 		} else {

			// 			$division_id =$division_name_check[0]->division_id;	
			// 		}
				 
			//   }	


				if($flag){	
					$data = array();		
							
					$data['account_id']			= $this->clean($_SESSION['accountId']);
				//	$data['tally_warehouse_id'] = mysql_real_escape_string($this->clean($arrFile[3]));
				//	$data['division_id']		= $division_id;
					$data['warehouse_prefix']	= mysql_real_escape_string($this->clean($arrFile[0]));
					$data['warehouse_name']		= mysql_real_escape_string($this->clean($arrFile[1]));
					$data['warehouse_code'] 	= mysql_real_escape_string($this->clean($arrFile[2]));
					$data['warehouse_phone_no']	= mysql_real_escape_string( $this->clean($arrFile[3]));
					$data['warehouse_address']	= mysql_real_escape_string($this->clean($arrFile[4]));
					$data['country_id']			= $country_id;
					$data['region_id']			= $region_id;
					$data['zone_id']			= $zone_id;
					$data['state_id']			= $state_id;
					$data['city_id']			= $city_id;
					$data['market_id']			= $market_id;
					$data['warehouse_incharge_person_phone_no'] =mysql_real_escape_string($this->clean($arrFile[11])); 
					$data['warehouse_incharge_person_phone_no2'] =mysql_real_escape_string($this->clean($arrFile[12]));
					$data['warehouse_incharge_person_phone_no3'] =mysql_real_escape_string($this->clean($arrFile[13]));
					$data['warehouse_incharge_person_email'] =mysql_real_escape_string($arrFile[14]); 
					$data['warehouse_incharge_person_email2'] =mysql_real_escape_string($arrFile[15]); 
					$data['warehouse_incharge_person_email3'] =mysql_real_escape_string($arrFile[16]);
					$data['warehouse_incharge_person']	=mysql_real_escape_string($this->clean($arrFile[17]));
					$data['zipcode']			= mysql_real_escape_string( $this->clean($arrFile[18]));
					$data['start_date']			= date('Y-m-d');
					$data['end_date']			= $this->clean($_SESSION['EndDate']);
					$data['last_update_date']	= date('Y-m-d');
					$data['last_update_status'] = 'New';
					$data['status']				= 'A';
					

					$ret_id=$this->_dbInsert($data,'table_warehouse');			
				
					 
				}
				
		}
		$row++;	 	  
		    
		}
		fclose($file);	  
	   if($row<=1)
	   	return "no";
	   else 
	  	return $str_err_array;
		
		
	}


/************************************* End Upload Warehouse ***************************************/


/********************* Start Upload Service Person File  ****************************************/		


	function uploadServicePersonFile(){		
	
		$fields = array('Service Personnel Name*', 'Service Personnel Code*', 'Division*' , 'Warehouse', 'Phone Number*', 'Address', 'Country*', 'Region*' , 'Zone*' , 'State*' , 'City*', 'Area', 'Pincode/Zipcode');
	
		$file = fopen($_FILES['fileToUpload']['tmp_name'],"r");
		$strErr="";
		$row=0;	

		while(! feof($file)){		  
			  $arrFile=fgetcsv($file);
			 
		
			 
			  $flag=true;
		if($row>0 && is_array($arrFile)) {	
		 

			$division_name = mysql_real_escape_string(strtolower($arrFile[2]));
			
		/*  echo "<pre>";
	print_r($arrFile); 
	exit;*/
			  
			if(empty($arrFile[0]) || $arrFile[0]=="Service Personnel Name*" || empty($arrFile[1]) || empty($arrFile[2]) || empty($arrFile[4]) || empty($arrFile[6]) || empty($arrFile[7]) || empty($arrFile[8]) || empty($arrFile[9]) || empty($arrFile[10]) ){
				  
		
		  		if(empty($arrFile[0]) || $arrFile[0]=="Service Personnel Name*" ){
					$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].",".$arrFile[10]."  ,".$arrFile[11].", ".$arrFile[12]." ,".$fields[0]." shouldn't be empty  \n";
				}
				else if(empty($arrFile[1])){
					$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].",".$arrFile[10]."  ,".$arrFile[11].", ".$arrFile[12]." , ".$fields[1]." shouldn't be empty  \n";
				}
				else if(empty($arrFile[2])){
					$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].",".$arrFile[10]."  ,".$arrFile[11].", ".$arrFile[12]." , ".$fields[2]." shouldn't be empty  \n";
				}
				else if(empty($arrFile[4])){
					$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].",".$arrFile[10]."  ,".$arrFile[11].", ".$arrFile[12]." , ".$fields[4]." shouldn't be empty  \n";
				}
				else if(empty($arrFile[6])){
					$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].",".$arrFile[10]."  ,".$arrFile[11].", ".$arrFile[12]." , ".$fields[6]." shouldn't be empty  \n";
				}
				else if(empty($arrFile[7])){
					$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].",".$arrFile[10]."  ,".$arrFile[11].", ".$arrFile[12]." , ".$fields[7]." shouldn't be empty  \n";
				}
				else if(empty($arrFile[8])){
					$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].",".$arrFile[10]."  ,".$arrFile[11].", ".$arrFile[12]." , ".$fields[8]." shouldn't be empty  \n";
				}
				else if(empty($arrFile[9])){
					$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].",".$arrFile[10]."  ,".$arrFile[11].", ".$arrFile[12]." , ".$fields[9]." shouldn't be empty  \n";
				}
				else if(empty($arrFile[10])){
					$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].",".$arrFile[10]."  ,".$arrFile[11].", ".$arrFile[12]." , ".$fields[10]." shouldn't be empty  \n";
				}
			
				 
		 			$str_err_array=array($strErr);						
						$flag=false;
		  }
		  

		 // Warehouse Check
		  if($flag){

					$condi=	"  AND sp_phone_no='".mysql_real_escape_string(strtolower($arrFile[4]))."' AND  LOWER(sp_name)='".mysql_real_escape_string( $this->clean(strtolower($arrFile[0])))."'";
    
					$aDisRec=$this->_getSelectList('table_service_personnel','*','',$condi);

					if(is_array($aDisRec)) {
						$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5]." ,".$arrFile[6]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].",".$arrFile[10]."  ,".$arrFile[11].", ".$arrFile[12]."  ,already exists in the system. \n";
						$str_err_array=array($strErr);
						$flag =false;

					}
				}
				



		if($flag){				
				
				if (!is_numeric($arrFile[4])) {					
					$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5]." ,".$arrFile[6]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].",".$arrFile[10].",".$arrFile[11].", ".$arrFile[12]." ,Please Provide the valid Phone Number. \n";					
					$str_err_array=array($strErr);						
					$flag=false;					
				} 
		
		}

		//  For ServicePersonCode 

		$sp_code = mysql_real_escape_string(trim($arrFile[1]));

		if($sp_code!="")
		{

			if($flag)
			{
	
				$condi=	" LOWER(sp_code)='".strtolower($sp_code)."'";
				$aRetRec= $this->_getSelectList('table_service_personnel','	service_personnel_id','',$condi);				
	
					if(!is_array($aRetRec)){
						$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5]." ,".$arrFile[6]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].",".$arrFile[10].",".$arrFile[11].", ".$arrFile[12]." , Sorry No Service Personnel Code exists . \n";
						$str_err_array=array($strErr);						
						$flag=false;
					} else {
						 $service_personnel_id = $aRetRec[0]->service_personnel_id;

					}

			}
	  	}
	  	else
	  	{
	  
	  		$service_personnel_id = mysql_real_escape_string(trim($arrFile[1]));
	  	}


// Fetching Country ID
		if($flag)
		{

			$condi=" LOWER(country_name)='".mysql_real_escape_string(strtolower($arrFile[6]))."'"; 
			$CountryRec=$this->_getSelectList2('country',"country_id",'',$condi);	
			if(isset($CountryRec[0]->country_id) && $CountryRec[0]->country_id>0) {
				$country_id=$CountryRec[0]->country_id; 
			} else {
				$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5]." ,".$arrFile[6]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].",".$arrFile[10]."  ,".$arrFile[11].",".$arrFile[12]."  , Country Not Found. \n";
						$str_err_array=array($strErr);
						$flag =false;
							
			}



		}

// Fetching Region ID
		if($flag)
		{

			$condi=" LOWER(region_name)='".mysql_real_escape_string(strtolower($arrFile[7]))."' and country_id = '".$country_id."'"; 
			$RegionRec=$this->_getSelectList2('table_region',"region_id",'',$condi);	
			

			if(isset($RegionRec[0]->region_id) && $RegionRec[0]->region_id>0) {
				$region_id=$RegionRec[0]->region_id; 
			} else {
				$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5]." ,".$arrFile[6]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].",".$arrFile[10]."  ,".$arrFile[11].",".$arrFile[12]." , Region Not Found. \n";
						$str_err_array=array($strErr);
						$flag =false;
							
			}


		}		

// Fetching Zone ID
		if($flag)
		{
			$condi=" LOWER(zone_name)='".mysql_real_escape_string(strtolower($arrFile[8]))."' and region_id = '".$region_id."'"; 
			$ZoneRec=$this->_getSelectList2('table_zone',"zone_id",'',$condi);	
			


			if(isset($ZoneRec[0]->zone_id) && $ZoneRec[0]->zone_id>0) {
				$zone_id=$ZoneRec[0]->zone_id; 
			} else {
				$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5]." ,".$arrFile[6]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].",".$arrFile[10]."  ,".$arrFile[11].",".$arrFile[12]."  , Zone Not Found. \n";
						$str_err_array=array($strErr);
						$flag =false;
							
			}




		}			

// Fetching State ID
		// if($flag)
		// {
		// 	$condi=" LOWER(state_name)='".mysql_real_escape_string(strtolower($arrFile[9]))."' and zone_id = '".$zone_id."'"; 
		// 	$StateRec=$this->_getSelectList2('state',"state_id",'',$condi);	
		// 	$state_id=$StateRec[0]->state_id;
		// }


		if($flag)
		{
			$condi=" LOWER(state_name)='".mysql_real_escape_string(strtolower($arrFile[9]))."' and zone_id = '".$zone_id."'"; 
			$StateRec=$this->_getSelectList2('state',"state_id",'',$condi);	
			$state_id=$StateRec[0]->state_id;
			if(!is_array($StateRec)){
				$state = array();
				$state['zone_id']=$this->clean($zone_id);
				$state['state_name']=mysql_real_escape_string( $this->clean($arrFile[9]));
				$state['last_update_date']=date('Y-m-d');
				$state['last_update_status']='New';
				$state_id=$this->_dbInsert($state,'state');
			}
		}

// Fetching City ID
		// if($flag)
		// {
		// 	$condi=" LOWER(city_name)='".mysql_real_escape_string(strtolower($arrFile[10]))."' and state_id = '".$state_id."'"; 
		// 	$CityRec=$this->_getSelectList2('city',"city_id",'',$condi);	
		// 	$city_id=$CityRec[0]->city_id;

		// }

		if($flag){					
					$condi=" LOWER(city_name)='".mysql_real_escape_string(strtolower($arrFile[10]))."' AND state_id='".$state_id."'";
				 	$CityRec=$this->_getSelectList2('city',"city_id",'',$condi);	
					$city_id=$CityRec[0]->city_id;
					if(!is_array($CityRec)){
						$city = array();
						$city['state_id']=$this->clean($state_id);
						$city['city_name']=mysql_real_escape_string( $this->clean($arrFile[10]));
						$city['last_update_date']=date('Y-m-d');
						$city['last_update_status']='New';
						$city_id=$this->_dbInsert($city,'city');
					}
				 }

/* For Area */
		if($flag){				
							

			$condi=" LOWER(market_name)='".mysql_real_escape_string($this->clean(strtolower($arrFile[11])))."'";

			$MarketRec=$this->_getSelectList(' table_markets',"market_id",'',$condi);	
			$market_id= $MarketRec[0]->market_id;

			if(sizeof($MarketRec)<=0){
									
				$market = array();
				$market['account_id'] 	=$_SESSION['accountId'];	
								
				$market['market_name']	= mysql_real_escape_string( $this->clean($arrFile[11]));
				$market['status'] 		='A';	
				$market_id=$this->_dbInsert($market,' table_markets');
			}
		}		
	
				 
		if($flag){	
					
			$condi=	" LOWER(sp_name)='".mysql_real_escape_string(strtolower($this->clean($arrFile[0])))."' AND state_id ='".$state_id."' AND city_id ='".$city_id."' AND sp_phone_no ='".mysql_real_escape_string($arrFile[4])."'";			
					
			$aRetRec=$this->_getSelectList('table_service_personnel','*','',$condi);
				if(is_array($aRetRec)) {					
					$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5]." ,".$arrFile[6]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].",".$arrFile[10].",".$arrFile[11].", ".$arrFile[12]." , Service Personnel already exists in the system. \n";
					$str_err_array=array($strErr);						
					$flag=false;
				}
	    }
	
	
		if($flag){				
				
				if (!is_numeric($arrFile[4])) {					
					$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5]." ,".$arrFile[6]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].",".$arrFile[10]."  ,".$arrFile[11].", ".$arrFile[12].", Please Provide the valid Phone Number. \n";				
					$str_err_array=array($strErr);						
					$flag=false;					
				} 
		
		}
	
	

		// Check division name


		if(!empty($division_name) && $flag) {

			   		$condi=" LOWER(division_name)='".$division_name."' ";
				 	$division_name_check =$this->_getSelectList('table_division',"division_id",'',$condi);
					if(sizeof($division_name_check) == 0) {
						$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5]." ,".$arrFile[6]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].",".$arrFile[10]."  ,".$arrFile[11].", ".$arrFile[12]." , division name not found in the system. \n";
						$str_err_array=array($strErr);
						$flag =false;
					} else {

						$division_id =$division_name_check[0]->division_id;	

					}
				 
		}		

		// Fetch WarehouseId

		if($flag){	
					
			$condi="LOWER(warehouse_name)='".mysql_real_escape_string(strtolower($this->clean($arrFile[3])))."'";			
			$warehouseIdRec = $this->_getSelectList2('table_warehouse',"warehouse_id",'',$condi);	
		    $warehouseId = $warehouseIdRec[0]->warehouse_id;

				
	    }



			if($flag){	

					$data = array();		
							
					$data['account_id']			    =   $this->clean($_SESSION['accountId']);
					$data['division_id']		    =   $division_id;
					$data['warehouse_id']		    =   $warehouseId;
					$data['sp_name']				=	mysql_real_escape_string($this->clean($arrFile[0]));
					$data['sp_code']				=	mysql_real_escape_string($this->clean($arrFile[1]));
					$data['sp_address']				=	mysql_real_escape_string($this->clean($arrFile[5]));
					$data['sp_phone_no']			=	mysql_real_escape_string($this->clean($arrFile[4]));
					$data['country_id']				= 	$country_id;
					$data['region_id']				= 	$region_id;
					$data['zone_id']				= 	$zone_id;
					$data['state_id']				= 	$state_id;
					$data['city_id']				= 	$city_id;	
					$data['market_id']				=   $market_id;
					$data['zipcode']				=	mysql_real_escape_string($this->clean($arrFile[12]));		
					$data['start_date']				=	date('Y-m-d');	
					$data['end_date']				=	$this->clean($_SESSION['EndDate']);
					$data['last_update_date']		=	date('Y-m-d');
					$data['last_update_status']		=	'New';
					$data['status']					=	'A';			
					$tsp_id      =   $this->_dbInsert($data,'table_service_personnel');				
				
					 
			}
				
		}
		$row++;	 	  
		    
		}
		fclose($file);	  
	   if($row<=1)
	   	return "no";
	   else 
	  	return $str_err_array;
		
	}


/************************************* End Upload Service Person ***************************************/














/*******************************************************
* DESC: Upload Warehouse Stock By Company
* Author: AJAY@2017-02-24
*
*
*
********************************************************/	


// function uploadWarehouseActualStock() {
	
// 	$fields = array('Warehouse Code*','Category Name*','Item Code*','BSN*', 'Batch No', 'Manufacture Date*', 'Warranty Period(In Months)*', 'ProRata(In Months)*', 'Grace Period(In Months)*');
// 	$file = fopen($_FILES['fileToUpload']['tmp_name'],"r");
// 	$strErr="";
// 	$resultset = "";
// 	$row=0;	

//     $whCd = '';
// 	$catName= '';
// 	$itmCd	= '';
// 	$bsn	= '';
// 	$stkValue= '';
// 	$manfDate = '';
// 	$warrantyPeriod = '';
// 	$proRata = '';
// 	$gracePeriod = '';

	
	
// 	while(!feof($file))
// 	{	
// 			$data = array();
// 			$data2 = array();
// 			$data3 = array();
	
		  
// 			$arrFile=fgetcsv($file);	
// 			$flag=true;	
		
// 			$whCd		     = trim($arrFile[0]);
// 			$catName	     = trim($arrFile[1]);
// 			$itmCd		     = trim($arrFile[2]);
// 			$bsn	         = trim($arrFile[3]);
// 			$batchNo	     = trim($arrFile[4]);
// 			$manfDate        = date('Y-m-d', strtotime($arrFile[5]));
// 			$warrantyPeriod  = trim($arrFile[6]);
// 			$proRata         = trim($arrFile[7]);
// 			$gracePeriod     = trim($arrFile[8]);
				
// 	//echo "<pre>"; print_r($arrFile);
// 	//exit;
		
// 	if($row>0 && is_array($arrFile)) {	
// 	if(empty($whCd) || $whCd=="Warehouse Code*" || empty($catName) || $catName=="Category Name*" || empty($itmCd) || $itmCd=="Item Code*" || empty($bsn) || $bsn=="BSN*" || empty($manfDate) || $manfDate=="Manufacture Date*" || empty($warrantyPeriod) || $warrantyPeriod=="Warranty Period(In Months)*" || empty($proRata) || $proRata=="ProRata(In Months)*" || empty($gracePeriod) ||  $gracePeriod=="Grace Period(In Months)*"){
	
// 	if(empty($whCd)){
// 	$strErr .= "Error in Row".$row." ,".$whCd.",".$catName." ,".$itmCd." ,".$bsn." , ".$batchNo." , ".$manfDate.",, ".$warrantyPeriod.", ".$proRata.", ".$gracePeriod.",  ".$fields[0]." shouldn't be empty  \n";
// 	} elseif(empty($catName)){
// 	$strErr .= "Error in Row".$row." ,".$whCd.",".$catName." ,".$itmCd." ,".$bsn." , ".$batchNo." , ".$manfDate.",, ".$warrantyPeriod.", ".$proRata.", ".$gracePeriod.",".$fields[1]." shouldn't be empty  \n";
// 	} elseif(empty($itmCd)){
// 	$strErr .= "Error in Row".$row." ,".$whCd.",".$catName." ,".$itmCd." ,".$bsn." , ".$batchNo." , ".$manfDate.",, ".$warrantyPeriod.", ".$proRata.", ".$gracePeriod.",".$fields[2]." shouldn't be empty  \n";
// 	} elseif(empty($bsn)) {
// 	$strErr .= "Error in Row".$row." ,".$whCd.",".$catName." ,".$itmCd." ,".$bsn." , ".$batchNo." , ".$manfDate.",, ".$warrantyPeriod.", ".$proRata.", ".$gracePeriod.",".$fields[3]." shouldn't be empty  \n";					
// 	} elseif(empty($manfDate)) {
// 	$strErr .= "Error in Row".$row." ,".$whCd.",".$catName." ,".$itmCd." ,".$bsn." , ".$batchNo." , ".$manfDate.",, ".$warrantyPeriod.", ".$proRata.", ".$gracePeriod.",".$fields[4]." shouldn't be empty  \n";					
// 	} elseif(empty($warrantyPeriod)) {
// 	$strErr .= "Error in Row".$row." ,".$whCd.",".$catName." ,".$itmCd." ,".$bsn." , ".$batchNo." , ".$manfDate.",, ".$warrantyPeriod.", ".$proRata.", ".$gracePeriod.",".$fields[5]." shouldn't be empty  \n";					
// 	} elseif(empty($proRata)) {
// 	$strErr .= "Error in Row".$row." ,".$whCd.",".$catName." ,".$itmCd." ,".$bsn." , ".$batchNo." , ".$manfDate.",, ".$warrantyPeriod.", ".$proRata.", ".$gracePeriod.",".$fields[6]." shouldn't be empty  \n";					
// 	}  elseif(empty($gracePeriod)) {
// 	$strErr .= "Error in Row".$row." ,".$whCd.",".$catName." ,".$itmCd." ,".$bsn." , ".$batchNo." , ".$manfDate.",, ".$warrantyPeriod.", ".$proRata.", ".$gracePeriod.",".$fields[7]." shouldn't be empty  \n";					
// 	} 
// 	$str_err_array=array($strErr);						
// 	$flag=false;
// 	} 
	




// 	// Check BSN already exists at any warehouse

// 	if($flag){
// 	$condi =" LOWER(BSN.bsn)='".mysql_real_escape_string(strtolower(trim($bsn)))."'";
// 	$resultset = $this->_getSelectList('table_warehouse_stock AS WS LEFT JOIN table_item_bsn AS BSN ON BSN.bsn_id = WS.bsn_id','WS.warehouse_id','',$condi);
// 		if(is_array($resultset) && !empty($resultset) && $resultset[0]->warehouse_id!= "") {
// 			$strErr .= "Error in Row".$row." ,".$whCd.",".$catName." ,".$itmCd." ,".$bsn." , ".$batchNo." , ".$manfDate.", ".$warrantyPeriod.", ".$proRata.", ".$gracePeriod." , Sorry this bsn already dispatched to a warehouse.\n";
// 			$str_err_array=array($strErr);	
// 			$flag=false;
// 		}
// 	}		



	
// 	// Check Warehouse Code exists in the system.
// 	if($flag){
// 	$condi =" LOWER(warehouse_code)='".mysql_real_escape_string(strtolower(trim($whCd)))."'";
// 	$resultset = $this->_getSelectList('table_warehouse','warehouse_id','',$condi);
// 		if(!is_array($resultset) && empty($resultset) && $resultset[0]->warehouse_id == "") {
// 			$strErr .= "Error in Row".$row." ,".$whCd.",".$catName." ,".$itmCd." ,".$bsn." , ".$batchNo." , ".$manfDate.",, ".$warrantyPeriod.", ".$proRata.", ".$gracePeriod." , Warehouse Code doesn't exists in the system.\n";
// 			$str_err_array=array($strErr);	
// 			$flag=false;
// 		} else {
// 			$warehouse_id = $resultset[0]->warehouse_id;
// 		}
// 	}		
		

// 	// Check category exists in the system.
// 	if($flag){
// 	$condi =" LOWER(category_name)='".mysql_real_escape_string(strtolower($catName))."'";
// 	$resultset = $this->_getSelectList('table_category','category_id','',$condi);
// 		if(!is_array($resultset) && empty($resultset) && $resultset[0]->category_id == "") {
// 				$strErr .= "Error in Row".$row." ,".$whCd.",".$catName." ,".$itmCd." ,".$bsn." , ".$batchNo." , ".$manfDate.",, ".$warrantyPeriod.", ".$proRata.", ".$gracePeriod." ,Category doesn't exists in the system  \n";	
// 			$str_err_array=array($strErr);	
// 			$flag=false;
// 		} else {
// 			$category_id = $resultset[0]->category_id;
// 		}
// 	}
		
// 	// Check item exists in the system.		

// 	if($flag){
// 	$condi ="  category_id ='".$category_id."' AND LOWER(item_code)='".mysql_real_escape_string(strtolower($itmCd))."'";
// 	$resultset = $this->_getSelectList('table_item','item_id','',$condi);
// 		if(!is_array($resultset) && empty($resultset) && $resultset[0]->item_id == "") {
// 		$strErr .= "Error in Row".$row." ,".$whCd.",".$catName." ,".$itmCd." ,".$bsn." , ".$batchNo." , ".$manfDate.",, ".$warrantyPeriod.", ".$proRata.", ".$gracePeriod." ,Item doesn't exists in this category or item code doesn't exists in the system. \n";	
// 			$str_err_array=array($strErr);	
// 			$flag=false;
			
// 		} else {
// 			$item_id = $resultset[0]->item_id;
// 		}
// 	}
		
// 	// Check attribute(size,color) exists in the system.



// 	// Check BSN STOCK exists in the system.	

// 	// if($flag){  

// 	// if($bsn!="") { $condi =" LOWER(bsn) = '".mysql_real_escape_string(strtolower($bsn))."' AND BSN.item_id=".$item_id; } else { $condi =""; }
	
// 	// $resultset = $this->_getSelectList('table_item_bsn AS BSN','BSN.bsn_id','',$condi);
		
// 	// 	if(!is_array($resultset) && empty($resultset) && $resultset[0]->bsn_id == "") {
// 	// 		$strErr .= "Error in Row".$row.",".$whCd.",".$catName." ,".$itmCd." ,".$bsn." , ".$batchNo." , ".$manfDate.",, ".$warrantyPeriod.", ".$proRata.", ".$gracePeriod.", Battery BSN doesn't exists in the system\n";		
// 	// 		$str_err_array=array($strErr);	
// 	// 		$flag=false;
// 	// 	} else { 
// 	// 		$bsn_id = $resultset[0]->bsn_id; 
// 	// 	}
// 	// }			



// 	// Check manufacturing date 


// 	if($flag){  
// 		// Date format should be as Month/Day/Year
// 		$currentDate = strtotime(date('Y-m-d'));
// 		//echo $manfDate;
// 		//echo "<br>";
// 		//echo date("jS F, Y", strtotime("01/15/17")); // 15/01/17
// 		//$manufacturingDate = date('Y-m-d', strtotime($manfDate));
// 		//exit;

// 		if($manfDate == '1970-01-01') {
// 			$strErr .= "Error in Row".$row.",".$whCd.",".$catName." ,".$itmCd." ,".$bsn." , ".$batchNo." , ".$manfDate.",, ".$warrantyPeriod.", ".$proRata.", ".$gracePeriod.", Invalid battery manufacturing date\n";		
// 			$str_err_array=array($strErr);	
// 			$flag=false;
// 		} 
// 	}			



// 	// Check Warranty Periods should be numeric


// 	if($flag){  
// 		//echo is_int(intval($warrantyPeriod)); 
// 		if(is_int(intval($warrantyPeriod)) == false) {
// 			$strErr .= "Error in Row".$row.",".$whCd.",".$catName." ,".$itmCd." ,".$bsn." , ".$batchNo." , ".$manfDate.",, ".$warrantyPeriod.", ".$proRata.", ".$gracePeriod.", Warranty Period Should be Numeric!\n";		
// 			$str_err_array=array($strErr);	
// 			$flag=false;
// 		} 
// 	}			




// 	// Check ProRata Periods should be numeric


// 	if($flag){  

// 		if(is_int(intval($proRata)) == false) {
// 			$strErr .= "Error in Row".$row.",".$whCd.",".$catName." ,".$itmCd." ,".$bsn." , ".$batchNo." , ".$manfDate.",, ".$warrantyPeriod.", ".$proRata.", ".$gracePeriod.", ProRata Period Should be Numeric!\n";		
// 			$str_err_array=array($strErr);	
// 			$flag=false;
// 		} 
// 	}		


// 	// Check Grace Periods should be numeric


// 	if($flag){  

// 		if(is_int(intval($gracePeriod)) == false) {
// 			$strErr .= "Error in Row".$row.",".$whCd.",".$catName." ,".$itmCd." ,".$bsn." , ".$batchNo." , ".$manfDate.",, ".$warrantyPeriod.", ".$proRata.", ".$gracePeriod.", Grace Period Should be Numeric!\n";		
// 			$str_err_array=array($strErr);	
// 			$flag=false;
// 		} 
// 	}		





	
	
// 	// Insert/Update data in the system			

// 	if($flag){	

// 		// 	bsn Master Data Entry Array 

// 		$resultset = $this->_getSelectList('table_item_bsn AS BSN ','BSN.bsn_id','', " BSN.bsn ='".$bsn."'");

// 		if(is_array($resultset) && isset($resultset[0]->bsn_id) && !empty($resultset[0]->bsn_id)) {

// 			$bsn_id = $resultset[0]->bsn_id;

// 		} else { 

// 			// Create new stock entry of a BSN
// 			$bsnArray = array();

// 			$bsnArray['account_id']        		= $_SESSION['accountId'];	
// 			$bsnArray['dispatch_warehouse_id']  = $warehouse_id;	
// 			$bsnArray['item_id']           		= $item_id;
// 			$bsnArray['category_id']       		= $category_id;
// 			$bsnArray['batch_no']       		= $batchNo;
// 			$bsnArray['bsn']       				= $bsn;
// 			$bsnArray['stock_value']       		= 1;
// 			$bsnArray['manufacture_date']       = $manfDate;
// 			$bsnArray['warranty_period']       	= $warrantyPeriod;
// 			$bsnArray['warranty_prorata']       = $proRata;
// 			$bsnArray['warranty_grace_period']  = $gracePeriod;
// 			$bsnArray['warranty_end_date']      = date('Y-m-d', strtotime("+".$warrantyPeriod." months", strtotime($manfDate)));
// 			$bsnArray['last_updated_date']	 	= date('Y-m-d');	
// 			$bsnArray['last_update_datetime']	= date('Y-m-d H:i:s');	
// 			$bsnArray['created_datetime']		= date('Y-m-d H:i:s');	
// 			$bsnArray['status']					= 'A';
			
// 			$bsn_id = $this->_dbInsert($bsnArray,'table_item_bsn');  

// 		}

// 		// 	bsn lifecycle activity data array 

// 		$data = array();

// 		$data['account_id'] 			=  $_SESSION['accountId'];	
// 		$data['item_id'] 				=  $item_id;
// 		$data['bsn_id'] 		        =  $bsn_id;
// 		$data['bsn'] 		            =  $bsn;	
// 		$data['to_warehouse_id'] 		=  $warehouse_id;	
// 		$data['user_type'] 		        =  $_SESSION['userLoginType'];	
// 		$data['web_user_id'] 		    =  $_SESSION['PepUpSalesUserId'];	
// 		$data['created_datetime'] 		=  date('Y-m-d H:i:s');	
// 		$data['activity_type'] 		    =  'D'; // D- Dispatched, R-Return, C-Complain
// 		$data['status'] 		        =  'A';		

// 		$this->_dbInsert($data,'table_item_bsn_lifecycle_activity');  




// 		// 	Add New BSN Warehouse Stock 

// 		$whStock = array();

// 		$whStock['account_id'] 				=  $_SESSION['accountId'];	
// 		$whStock['warehouse_id'] 		    =  $warehouse_id;
// 		$whStock['item_id'] 				=  $item_id;
// 		$whStock['bsn_id'] 		        	=  $bsn_id;
// 		$whStock['stock_value']       		=  1;
// 		$whStock['last_updated_date']	 	=  date('Y-m-d');	
// 		$whStock['last_update_datetime']	=  date('Y-m-d H:i:s');	
// 		$whStock['created_datetime']		=  date('Y-m-d H:i:s');	
// 		$whStock['status']		            =  'I';

// 		$this->_dbInsert($whStock,'table_warehouse_stock');  
		

// 		}
			
// 	}
// 	  $row++;	   
// 	}		
		
// 	fclose($file);	  
// 	  if($row<=1)
// 	   	return "no";
// 	   else 
// 	  	return $str_err_array;
// 	}	





function uploadWarehouseActualStock() {
	
	//$fields = array('Warehouse Code*','Category Name*','Item Code*','BSN*', 'Batch No', 'Manufacture Date*', 'Warranty Period(In Months)*', 'ProRata(In Months)*', 'Grace Period(In Months)*');
	$fields = array('Item Code*','BSN*', 'Batch No', 'Manufacture Date*(YYYY-MM-DD)', 'Dispatch Date*(YYYY-MM-DD)' );

	$file = fopen($_FILES['fileToUpload']['tmp_name'],"r");
	$strErr="";
	$resultset = "";
	$row=0;	

    $whCd = '';
	$catName= '';
	$itmCd	= '';
	$bsn	= '';
	$stkValue= '';
	$manfDate = '';
	$warrantyPeriod = '';
	$proRata = '';
	$gracePeriod = '';

	
	while(!feof($file))
	{	
			$data = array();
			$data2 = array();
			$data3 = array();
	
		  
			$arrFile=fgetcsv($file);	
			$flag=true;	
			$bsn	         = trim($arrFile[0]);
			// $whCd		     = trim($arrFile[0]);
			// $catName	     = trim($arrFile[1]);

			$item_code = substr($bsn, 0, 3);
			$model_code = substr($bsn,8,4);
			$item_details = $this->_getSelectList2('table_item','*','',' item_erp_code="'.$item_code.'" and model_code="'.$model_code.'"');
			$itmCd		     = $item_details[0]->item_code;
			$items_ids       = $item_details[0]->item_id;
			// $bsn	         = trim($arrFile[1]);
			$batchNo	     = trim($arrFile[2]);

			// $manfDate        = date('Y-m-d', strtotime($arrFile[3]));
			// $dispatch_date   = date('Y-m-d', strtotime($arrFile[4]));
			$day = substr($bsn,3,1);
			$month = substr($bsn,4,1);
			$year = substr($bsn,5,1);
			$day_value = $this->_getSelectList2('table_bsn_logic_day','*','',' character_value="'.$day.'"');				

			$month_value = $this->_getSelectList2('table_bsn_logic_month','*','',' character_value="'.$month.'"');

			$year_value = $this->_getSelectList2('table_bsn_logic_year','*','',' character_value="'.$year.'"');

			$manfDate        = $year_value[0]->year."-".date('m',strtotime($month_value[0]->month))."-".str_pad($day_value[0]->day,2,'0',STR_PAD_LEFT);
			$dispatch_date   = date('Y-m-d');
			
			// $warrantyPeriod  = trim($arrFile[6]);
			// $proRata         = trim($arrFile[7]);
			// $gracePeriod     = trim($arrFile[8]);
				
	//echo "<pre>"; print_r($arrFile);
	//exit;
		
	if($row>=0 && is_array($arrFile) && isset($_POST['warehouse_id']) && $_POST['warehouse_id']>0) {	

	$warehouse_id = trim($_POST['warehouse_id']);



	if(empty($itmCd) || empty($bsn) || $bsn=="BSN*" || empty($manfDate)){
	
	// if(empty($whCd)){
	// $strErr .= "Error in Row".$row." ,".$itmCd." ,".$bsn." , ".$batchNo." , ".$manfDate.",  ".$fields[0]." shouldn't be empty  \n";
	// } elseif(empty($catName)){
	// $strErr .= "Error in Row".$row." ,".$itmCd." ,".$bsn." , ".$batchNo." , ".$manfDate.",".$fields[1]." shouldn't be empty  \n";
	// } else 
	if(empty($itmCd)){
	$strErr .= "Error in Row".$row." ,".$itmCd." ,".$bsn." , ".$batchNo." , ".$manfDate.", ".$dispatch_date.",".$fields[0]." not matching  \n";
	} elseif(empty($bsn)) {
	$strErr .= "Error in Row".$row." ,".$itmCd." ,".$bsn." , ".$batchNo." , ".$manfDate.", ".$dispatch_date.",".$fields[1]." not matching  \n";					
	} elseif(empty($manfDate)) {
	$strErr .= "Error in Row".$row." ,".$itmCd." ,".$bsn." , ".$batchNo." , ".$manfDate.", ".$dispatch_date.",".$fields[3]." not matching  \n";					
	}  elseif(empty($dispatch_date)) {
	 $strErr .= "Error in Row".$row." ,".$itmCd." ,".$bsn." , ".$batchNo." , ".$manfDate.",".$fields[4]." not matching  \n";					
	} 
	 //elseif(empty($proRata)) {
	// $strErr .= "Error in Row".$row." ,".$itmCd." ,".$bsn." , ".$batchNo." , ".$manfDate.",".$fields[6]." shouldn't be empty  \n";					
	// }  elseif(empty($gracePeriod)) {
	// $strErr .= "Error in Row".$row." ,".$itmCd." ,".$bsn." , ".$batchNo." , ".$manfDate.",".$fields[7]." shouldn't be empty  \n";					
	// } 
	$str_err_array=array($strErr);						
	$flag=false;
	} 
	


	
	// Check BSN already exists at any warehouse

	if($flag){
	$condi =" LOWER(BSN.bsn)='".mysql_real_escape_string(strtolower(trim($bsn)))."'";
	$resultset = $this->_getSelectList('table_warehouse_stock AS WS LEFT JOIN table_item_bsn AS BSN ON BSN.bsn_id = WS.bsn_id','WS.warehouse_id','',$condi);
		if(is_array($resultset) && !empty($resultset) && $resultset[0]->warehouse_id!= "") {
			$strErr .= "Error in Row".$row." ,".$itmCd." ,".$bsn." , ".$batchNo." , ".$manfDate." , Sorry this bsn already dispatched to a warehouse.\n";
			$str_err_array=array($strErr);	
			$flag=false;
		}
	}		



	
	// Check Warehouse Code exists in the system.
	if($flag){
	//$condi =" LOWER(warehouse_code)='".mysql_real_escape_string(strtolower(trim($whCd)))."'";
	$condi =" warehouse_id ='".$warehouse_id."'";
	$resultset = $this->_getSelectList('table_warehouse','warehouse_id','',$condi);
		if(!is_array($resultset) && empty($resultset) && $resultset[0]->warehouse_id == "") {
			$strErr .= "Error in Row".$row." ,".$itmCd." ,".$bsn." , ".$batchNo." , ".$manfDate.", ".$dispatch_date.", Warehouse Code doesn't exists in the system.\n";
			$str_err_array=array($strErr);	
			$flag=false;
		} else {
			$warehouse_id = $resultset[0]->warehouse_id;
		}
	}		
		

	// Check category exists in the system.
	// if($flag){
	// $condi =" LOWER(category_name)='".mysql_real_escape_string(strtolower($catName))."'";
	// $resultset = $this->_getSelectList('table_category','category_id','',$condi);
	// 	if(!is_array($resultset) && empty($resultset) && $resultset[0]->category_id == "") {
	// 			$strErr .= "Error in Row".$row." ,".$itmCd." ,".$bsn." , ".$batchNo." , ".$manfDate.", ,Category doesn't exists in the system  \n";	
	// 		$str_err_array=array($strErr);	
	// 		$flag=false;
	// 	} else {
	// 		$category_id = $resultset[0]->category_id;
	// 	}
	// }
		
	// Check item exists in the system.		

	if($flag){
	$condi ="  LOWER(item_code)='".mysql_real_escape_string(strtolower($itmCd))."'";
	$resultset = $this->_getSelectList('table_item','item_id, item_grace, item_warranty, item_prodata, category_id','',$condi);
		if(!is_array($resultset) && empty($resultset) && $resultset[0]->item_id == "") {
		$strErr .= "Error in Row".$row." ,".$itmCd." ,".$bsn." , ".$batchNo." , ".$manfDate.", ".$dispatch_date.", Item doesn't exists in this category or item code doesn't exists in the system. \n";	
			$str_err_array=array($strErr);	
			$flag=false;
			
		} else {
			$item_id = $resultset[0]->item_id;
			$category_id = $resultset[0]->category_id;
			$warrantyPeriod = $resultset[0]->item_warranty;
			$proRata = $resultset[0]->item_prodata;
			$gracePeriod = $resultset[0]->item_grace;
		}
	}
		
	// Check attribute(size,color) exists in the system.


	if($flag)
	{
		if(!(count($item_details)>0 && count($month_value)>0 && count($day_value)>0 && count($year_value)>0))
		{
			$strErr .= "Error in Row".$row.",".$itmCd." ,".$bsn." , ".$batchNo." , ".$manfDate.", ".$dispatch_date.", Invalid BSN Format. Please check the BSN No\n";		
			$str_err_array=array($strErr);	
			$flag=false;
		}
	}

	// Check BSN STOCK exists in the system.	

	// if($flag){  

	// if($bsn!="") { $condi =" LOWER(bsn) = '".mysql_real_escape_string(strtolower($bsn))."' AND BSN.item_id=".$item_id; } else { $condi =""; }
	
	// $resultset = $this->_getSelectList('table_item_bsn AS BSN','BSN.bsn_id','',$condi);
		
	// 	if(!is_array($resultset) && empty($resultset) && $resultset[0]->bsn_id == "") {
	// 		$strErr .= "Error in Row".$row.",".$itmCd." ,".$bsn." , ".$batchNo." , ".$manfDate.", Battery BSN doesn't exists in the system\n";		
	// 		$str_err_array=array($strErr);	
	// 		$flag=false;
	// 	} else { 
	// 		$bsn_id = $resultset[0]->bsn_id; 
	// 	}
	// }			



	// Check manufacturing date 


	if($flag){  
		// Date format should be as Month/Day/Year
		//$currentDate = strtotime(date('Y-m-d'));
		//echo $manfDate;
		//echo "<br>";
		//echo date("jS F, Y", strtotime("01/15/17")); // 15/01/17
		//$manufacturingDate = date('Y-m-d', strtotime($manfDate));
		//exit;

			if (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$manfDate))
		    {
		        $dateStaus = true;
		    }else{
		        $dateStaus = false;
		    }


		 //  $manfDate =  date('Y-m-d', strtotime($manfDate));

		if(date('Y-m-d', strtotime($manfDate)) == '1970-01-01' || strtotime(date('Y-m-d', strtotime($manfDate)))<= strtotime('1970-01-01') || $dateStaus == false) {

			$strErr .= "Error in Row".$row.",".$itmCd." ,".$bsn." , ".$batchNo." , ".$manfDate.", ".$dispatch_date.", Invalid battery manufacturing date. Date format should be in this format(YYYY-mm-dd)\n";		
			$str_err_array=array($strErr);	
			$flag=false;
		} 
	}			
	if($flag){  
		// Date format should be as Month/Day/Year
		//$currentDate = strtotime(date('Y-m-d'));
		//echo $manfDate;
		//echo "<br>";
		//echo date("jS F, Y", strtotime("01/15/17")); // 15/01/17
		//$manufacturingDate = date('Y-m-d', strtotime($manfDate));
		//exit;

			if (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$dispatch_date))
		    {
		        $dateStaus = true;
		    }else{
		        $dateStaus = false;
		    }

		 
		// $dispatch_date =  date('Y-m-d', strtotime($dispatch_date));

		if(date('Y-m-d', strtotime($dispatch_date)) == '1970-01-01' || strtotime($dispatch_date) <= strtotime('1970-01-01') || $dateStaus == false || strtotime($dispatch_date) > strtotime(date('Y-m-d'))) {

			$strErr .= "Error in Row".$row.",".$itmCd." ,".$bsn." , ".$batchNo." , ".$manfDate.", ".$dispatch_date.", Invalid dispatched date(Less than current date). Date format should be in this format(YYYY-mm-dd)\n";		
			$str_err_array=array($strErr);	
			$flag=false;
		} 
	}		



	// Check Warranty Periods should be numeric


	// if($flag){  
	// 	//echo is_int(intval($warrantyPeriod)); 
	// 	if(is_int(intval($warrantyPeriod)) == false) {
	// 		$strErr .= "Error in Row".$row.",".$itmCd." ,".$bsn." , ".$batchNo." , ".$manfDate.", Warranty Period Should be Numeric!\n";		
	// 		$str_err_array=array($strErr);	
	// 		$flag=false;
	// 	} 
	// }			




	// Check ProRata Periods should be numeric


	// if($flag){  

	// 	if(is_int(intval($proRata)) == false) {
	// 		$strErr .= "Error in Row".$row.",".$itmCd." ,".$bsn." , ".$batchNo." , ".$manfDate.", ProRata Period Should be Numeric!\n";		
	// 		$str_err_array=array($strErr);	
	// 		$flag=false;
	// 	} 
	// }		


	// Check Grace Periods should be numeric


	// if($flag){  

	// 	if(is_int(intval($gracePeriod)) == false) {
	// 		$strErr .= "Error in Row".$row.",".$itmCd." ,".$bsn." , ".$batchNo." , ".$manfDate.", Grace Period Should be Numeric!\n";		
	// 		$str_err_array=array($strErr);	
	// 		$flag=false;
	// 	} 
	// }		





	
	
	// Insert/Update data in the system			

	if($flag){	

		// 	bsn Master Data Entry Array 

		$resultset = $this->_getSelectList('table_item_bsn AS BSN ','BSN.bsn_id','', " BSN.bsn ='".$bsn."'");

		if(is_array($resultset) && isset($resultset[0]->bsn_id) && !empty($resultset[0]->bsn_id)) {

			$bsn_id = $resultset[0]->bsn_id;

		} else { 

			// Create new stock entry of a BSN
			$bsnArray = array();

			$bsnArray['account_id']        		= $_SESSION['accountId'];	
			$bsnArray['dispatch_warehouse_id']  = $warehouse_id;	
			$bsnArray['item_id']           		= $items_ids;
			$bsnArray['category_id']       		= $category_id;
			$bsnArray['batch_no']       		= $batchNo;
			$bsnArray['bsn']       				= $bsn;
			$bsnArray['stock_value']       		= 1;
			$bsnArray['manufacture_date']       = $manfDate;
			$bsnArray['warranty_period']       	= $warrantyPeriod;
			$bsnArray['warranty_prorata']       = $proRata;
			$bsnArray['warranty_grace_period']  = $gracePeriod;
			$bsnArray['warehouse_dispatch_date']= date('Y-m-d');
			$bsnArray['factory_dispatch_date']  = $dispatch_date;
			//$bsnArray['warranty_end_date']      = date('Y-m-d', strtotime("+".$warrantyPeriod." months", strtotime($manfDate)));
			$bsnArray['last_updated_date']	 	= date('Y-m-d');	
			$bsnArray['last_update_datetime']	= date('Y-m-d H:i:s');	
			$bsnArray['created_datetime']		= date('Y-m-d H:i:s');	
			$bsnArray['user_type']  			= $_SESSION['userLoginType'];
			$bsnArray['web_user_id']  			= $_SESSION['PepUpSalesUserId'];
			
			$bsnArray['status']					= 'A';
			
			$bsn_id = $this->_dbInsert($bsnArray,'table_item_bsn');  

		}

		// 	bsn lifecycle activity data array 

		$data = array();

		$data['account_id'] 			=  $_SESSION['accountId'];	
		$data['item_id'] 				=  $item_id;
		$data['bsn_id'] 		        =  $bsn_id;
		$data['bsn'] 		            =  $bsn;	
		$data['to_warehouse_id'] 		=  $warehouse_id;	
		$data['user_type'] 		        =  $_SESSION['userLoginType'];	
		$data['web_user_id'] 		    =  $_SESSION['PepUpSalesUserId'];	
		$data['created_datetime'] 		=  date('Y-m-d H:i:s');	
		$data['activity_type'] 		    =  'D'; // D- Dispatched, R-Return, C-Complain
		$data['status'] 		        =  'A';		

		$this->_dbInsert($data,'table_item_bsn_lifecycle_activity');  




		// 	Add New BSN Warehouse Stock 

		$whStock = array();

		$whStock['account_id'] 				=  $_SESSION['accountId'];	
		$whStock['warehouse_id'] 		    =  $warehouse_id;
		$whStock['item_id'] 				=  $item_id;
		$whStock['bsn_id'] 		        	=  $bsn_id;
		$whStock['stock_value']       		=  1;
		$whStock['dispatch_date']			=  $dispatch_date;
		$whStock['last_updated_date']	 	=  date('Y-m-d');	
		$whStock['last_update_datetime']	=  date('Y-m-d H:i:s');	
		$whStock['created_datetime']		=  date('Y-m-d H:i:s');	
		if($_SESSION['userLoginType']==7)
		{
			$whStock['status']		            =  'A';		//when warehouse uploading/importing its own existing stock @04-09-2017 Chirag
		}
		else
		{
			$whStock['status']		            =  'I';		//When company dispatching the stock to warehouse @04-09-2017 Chirag
		}

		$this->_dbInsert($whStock,'table_warehouse_stock');  
		

		}
			
	}
	  $row++;	   
	}		
		
	fclose($file);	

	// print_r($str_err_array);

	// exit;  
	  if($row<=1)
	   	return "no";
	   else 
	  	return $str_err_array;
	}	






/*******************************************************
* DESC: Upload Warehouse Stock By Company
* Author: AJAY@2017-02-24
*
*
*
********************************************************/	









/******************************************************************
* DESC: Upload Scanned BSN from warehouse dispatched from company
* Author: Chirag@2017-09-08
*
*
*
********************************************************/	


function uploadWarehouseScannedStock($warehouse_id) {
	
	$file = fopen($_FILES['fileToUpload']['tmp_name'],"r");
	$strErr="";
	$resultset = "";
	$row=0;	

    $BSN = '';

	
	
	while(!feof($file))
	{	
			$data = array();
			$data2 = array();
			$data3 = array();
		  
			$bsn = fgets($file);	
			$flag=true;	
	
				
	// echo "<pre>"; 
	// echo $row;
	// print_r($bsn);
	// exit;

	$BSNNO = mysql_real_escape_string(strtolower(trim($bsn)));
		
	if(!empty($BSNNO) && $warehouse_id>0) {	

	// Check BSN already exists at any warehouse

	if($flag){
	$condi =" LOWER(BSN.bsn)='".$BSNNO."' AND WS.status='I' AND WS.warehouse_id = '".$warehouse_id."' ";
	$resultset = $this->_getSelectList('table_warehouse_stock AS WS LEFT JOIN table_item_bsn AS BSN ON BSN.bsn_id = WS.bsn_id','WS.warehouse_id, WS.warehouse_stock_id','',$condi);
		if(sizeof($resultset)==0 && empty($resultset) && !isset($resultset[0]->warehouse_id)) {
			$strErr .= "Error in Row".$row." ,".$BSNNO.", Sorry this bsn already scanned/Accepted Or Not Found.\n";
			$str_err_array=array($strErr);	
			$flag=false;
		} else {

			$warehouse_stock_id = $resultset[0]->warehouse_stock_id;

		}
	}		


	if($flag){
	$condi =" LOWER(BSN.bsn)='".$BSNNO."' AND WS.status='I' AND WS.warehouse_id = '".$warehouse_id."' and transfer_type='F'";
	$resultset = $this->_getSelectList('table_warehouse_stock AS WS LEFT JOIN table_item_bsn AS BSN ON BSN.bsn_id = WS.bsn_id','WS.warehouse_id, WS.warehouse_stock_id','',$condi);
		if(sizeof($resultset)==0 && empty($resultset) && !isset($resultset[0]->warehouse_id)) {
			$strErr .= "Error in Row".$row." ,".$BSNNO.", BSN not Found in Company Dispatched.\n";
			$str_err_array=array($strErr);	
			$flag=false;
		} else {

			$warehouse_stock_id = $resultset[0]->warehouse_stock_id;

		}
	}		


	// Insert/Update data in the system			

	if($flag){	

		// 	bsn Master Data Entry Array 

		// $resultset = $this->_getSelectList('table_item_bsn AS BSN ','BSN.bsn_id','', " LOWER(BSN.bsn) ='".$bsn."'");

		// if(is_array($resultset) && isset($resultset[0]->bsn_id) && !empty($resultset[0]->bsn_id)) {

		// 	$bsn_id = $resultset[0]->bsn_id;

		// }

		// 	Add New BSN Warehouse Stock 

		$whStock = array();
		$whStock['last_updated_date']	 	=  date('Y-m-d');	
		$whStock['last_update_datetime']	=  date('Y-m-d H:i:s');	
		$whStock['status']		            =  'A';
		$this->_dbUpdate($whStock,'table_warehouse_stock', " warehouse_stock_id=".$warehouse_stock_id);
		

		}

	  $row++;	   	
	}
	
	}		
		
	fclose($file);	  
	  if($row<=0)
	   	return "no";
	   else 
	  	return $str_err_array;
	}	



/*******************************************************************
* DESC: Upload Scanned BSN from warehouse Dispatched from Company
* Author: Chirag@2017-09-08
*
*
*
********************************************************/		

	


/*******************************************************************
* DESC: Upload Scanned BSN from warehouse transferred from Warehouse
* Author: Chirag@2017-09-08
*
*
*
********************************************************/	


function uploadWarehouseScannedStockWarehouse($warehouse_id) {
	
	$file = fopen($_FILES['fileToUpload']['tmp_name'],"r");
	$strErr="";
	$resultset = "";
	$row=0;	

    $BSN = '';

	
	
	while(!feof($file))
	{	
			$data = array();
			$data2 = array();
			$data3 = array();
		  
			$bsn = fgets($file);	
			$flag=true;	
	
				
	// echo "<pre>"; 
	// echo $row;
	// print_r($bsn);
	// exit;

	$BSNNO = mysql_real_escape_string(strtolower(trim($bsn)));
		
	if($row+1>0 && !empty($BSNNO) && $warehouse_id>0) {	

	// Check BSN already exists at any warehouse

	if($flag){
	$condi =" LOWER(BSN.bsn)='".$BSNNO."' AND WS.status='I' AND WS.warehouse_id = '".$warehouse_id."' ";
	$resultset = $this->_getSelectList('table_warehouse_stock AS WS LEFT JOIN table_item_bsn AS BSN ON BSN.bsn_id = WS.bsn_id','WS.warehouse_id, WS.warehouse_stock_id','',$condi);
		if(sizeof($resultset)==0 && empty($resultset) && !isset($resultset[0]->warehouse_id)) {
			$strErr .= "Error in Row".$row." ,".$BSNNO.", Sorry this bsn already scanned/Accepted Or Not Found.\n";
			$str_err_array=array($strErr);	
			$flag=false;
		} else {

			$warehouse_stock_id = $resultset[0]->warehouse_stock_id;

		}
	}


	if($flag){
	$condi =" LOWER(BSN.bsn)='".$BSNNO."' AND WS.status='I' AND WS.warehouse_id = '".$warehouse_id."' and WS.transfer_type = 'W'";
	$resultset = $this->_getSelectList('table_warehouse_stock AS WS LEFT JOIN table_item_bsn AS BSN ON BSN.bsn_id = WS.bsn_id','WS.warehouse_id, WS.warehouse_stock_id','',$condi);
		if(sizeof($resultset)==0 && empty($resultset) && !isset($resultset[0]->warehouse_id)) {
			$strErr .= "Error in Row".$row." ,".$BSNNO.", BSN not Found in Warehouse Dispatched.\n";
			$str_err_array=array($strErr);	
			$flag=false;
		} else {

			$warehouse_stock_id = $resultset[0]->warehouse_stock_id;

		}
	}		


	// Insert/Update data in the system			

	if($flag){	

		// 	bsn Master Data Entry Array 

		// $resultset = $this->_getSelectList('table_item_bsn AS BSN ','BSN.bsn_id','', " LOWER(BSN.bsn) ='".$bsn."'");

		// if(is_array($resultset) && isset($resultset[0]->bsn_id) && !empty($resultset[0]->bsn_id)) {

		// 	$bsn_id = $resultset[0]->bsn_id;

		// }

		// 	Add New BSN Warehouse Stock 

		$whStock = array();
		$whStock['last_updated_date']	 	=  date('Y-m-d');	
		$whStock['last_update_datetime']	=  date('Y-m-d H:i:s');	
		$whStock['status']		            =  'A';
		$this->_dbUpdate($whStock,'table_warehouse_stock', " warehouse_stock_id=".$warehouse_stock_id);
		

		}
			
	}
	  $row++;	   
	}		
		
	fclose($file);	  
	  if($row<=1)
	   	return "no";
	   else 
	  	return $str_err_array;
	}	




/********************************************************************
* DESC: Upload Scanned BSN from warehouse transferred from warehouse
* Author: Chirag@2017-09-08
*
*
*
********************************************************/





/**************************************************
* DESC: Warehouse Dispatches stock to distributor or our system validate the FIFO
* AUTHOR: AJAY
* Created : 2017-03-18
*
*
********************************************/



	public function uploadWarehouseDispatchdStockToDistributor ($paramSet) {


		// echo "<pre>";
		// print_r($paramSet);
		// print_r($_FILES);
		// exit;
		



		if(sizeof($paramSet)>0 && isset($paramSet['distributor_id']) && $paramSet['distributor_id']>0 && isset($paramSet['invoice_id']) && !empty($paramSet['invoice_id']) && isset($paramSet['date_id']) && !empty($paramSet['date_id']) ) {


			$warehouse_id 		= $paramSet['warehouse_id2'];
			$distributor_id     = $paramSet['distributor_id'];
			$bill_no 	 		= trim($paramSet['invoice_id']);
			$bill_date 	 		= date('Y-m-d', strtotime($paramSet['date_id']));

			$delivery_challan_no 	 		= ($paramSet['challan_no'])?$paramSet['challan_no']:'';
			$delivery_challan_date 	 		= ($paramSet['challan_date'])?date('Y-m-d', strtotime($paramSet['challan_date'])):'';
			$transporterName				= ($paramSet['transporter_name'])?$paramSet['transporter_name']:'';

			$file = fopen($_FILES['fileToUpload']['tmp_name'],"r");
			$strErr="";
			$resultset = "";
			$row=0;	

		    $BSN = '';
//warehouse to distributor push in tally commeted by arvind

		   $dist_name=$this->_getSelectList2('table_distributors AS d',"distributor_name,distributor_code",''," d.distributor_id='".$distributor_id."'  ");
           $wh_name=$this->_getSelectList2('table_warehouse AS w',"warehouse_name",''," w.warehouse_id='".$warehouse_id."'  ");
         
           $name=$dist_name[0]->distributor_name;
           $dist_code  =$dist_name[0]->distributor_code;

           $warehouse_name=$wh_name[0]->warehouse_name;


           $wh_name=$warehouse_name;
	 	   $arr = explode(' ',trim($wh_name));
	 	   $challan=ucwords(strtolower($arr[1]));
           $voucher_type=ucwords(strtolower($arr[1]))." Tax Invoice";
           $ledgername=ucwords(strtolower($arr[1]))." GST Sales @ 18%";
           $requestXMLSTART='<ENVELOPE>
			<HEADER>
			<TALLYREQUEST>Import Data</TALLYREQUEST>
			</HEADER>
			<BODY>
			<IMPORTDATA>
			<REQUESTDESC>
			<REPORTNAME>Vouchers</REPORTNAME>
			<STATICVARIABLES>
			<SVCURRENTCOMPANY>EastmanLtd</SVCURRENTCOMPANY>
			</STATICVARIABLES>
			</REQUESTDESC>
			<REQUESTDATA>
			<TALLYMESSAGE xmlns:UDF="TallyUDF">

			<VOUCHER VCHTYPE="'.$voucher_type.'" ACTION="Create" OBJVIEW="Invoice Voucher View">

			<BASICBUYERADDRESS.LIST TYPE="String">
			<BASICBUYERADDRESS>Flat No. 101, First Floor</BASICBUYERADDRESS>
			<BASICBUYERADDRESS>1, Community Centre,</BASICBUYERADDRESS>
			<BASICBUYERADDRESS>Nariana Industrial Area</BASICBUYERADDRESS>
			<BASICBUYERADDRESS>Phase-1, New Delhi-110028</BASICBUYERADDRESS>

			</BASICBUYERADDRESS.LIST>
			<OLDAUDITENTRYIDS.LIST TYPE="Number">
			<OLDAUDITENTRYIDS>-1</OLDAUDITENTRYIDS>
			</OLDAUDITENTRYIDS.LIST>
			<DATE>20170801</DATE>
			<REFERENCEDATE>20170801</REFERENCEDATE>

			<GSTREGISTRATIONTYPE></GSTREGISTRATIONTYPE>
			<VATDEALERTYPE></VATDEALERTYPE>
			<TAXUNITNAME>Default Tax Unit</TAXUNITNAME>
			<CLASSNAME>Test Pass Returned</CLASSNAME>
			<PARTYNAME>'.$name.'</PARTYNAME>
			<VOUCHERTYPENAME>'.$voucher_type.'</VOUCHERTYPENAME>
			<REFERENCE></REFERENCE>
			<VOUCHERNUMBER></VOUCHERNUMBER>
			<PARTYLEDGERNAME>'.$name."(".$dist_code.")".'</PARTYLEDGERNAME>
			<BASICBASEPARTYNAME>'.$name."(".$dist_code.")".'</BASICBASEPARTYNAME>
			<CSTFORMISSUETYPE/>
			<CSTFORMRECVTYPE/>
			<FBTPAYMENTTYPE>Default</FBTPAYMENTTYPE>
			<PERSISTEDVIEW>Invoice Voucher View</PERSISTEDVIEW>
			<BASICBUYERNAME>EastmanLtd</BASICBUYERNAME>
			<BASICDATETIMEOFINVOICE>3-Aug-2017 at 15:58</BASICDATETIMEOFINVOICE>
			<BASICDATETIMEOFREMOVAL>3-Aug-2017 at 15:58</BASICDATETIMEOFREMOVAL>
			<VCHGSTCLASS/>
			<ENTEREDBY></ENTEREDBY>
			<DIFFACTUALQTY>No</DIFFACTUALQTY>
			<ISMSTFROMSYNC>No</ISMSTFROMSYNC>
			<ASORIGINAL>No</ASORIGINAL>
			<AUDITED>No</AUDITED>
			<FORJOBCOSTING>No</FORJOBCOSTING>
			<ISOPTIONAL>No</ISOPTIONAL>
			<EFFECTIVEDATE>20170801</EFFECTIVEDATE>
			<USEFOREXCISE>No</USEFOREXCISE>
			<ISFORJOBWORKIN>No</ISFORJOBWORKIN>
			<ALLOWCONSUMPTION>No</ALLOWCONSUMPTION>
			<USEFORINTEREST>No</USEFORINTEREST>
			<USEFORGAINLOSS>No</USEFORGAINLOSS>
			<USEFORGODOWNTRANSFER>No</USEFORGODOWNTRANSFER>
			<USEFORCOMPOUND>No</USEFORCOMPOUND>
			<USEFORSERVICETAX>No</USEFORSERVICETAX>
			<ISEXCISEVOUCHER>No</ISEXCISEVOUCHER>
			<EXCISETAXOVERRIDE>No</EXCISETAXOVERRIDE>
			<USEFORTAXUNITTRANSFER>No</USEFORTAXUNITTRANSFER>
			<EXCISEOPENING>No</EXCISEOPENING>
			<USEFORFINALPRODUCTION>No</USEFORFINALPRODUCTION>
			<ISTDSOVERRIDDEN>No</ISTDSOVERRIDDEN>
			<ISTCSOVERRIDDEN>No</ISTCSOVERRIDDEN>
			<ISTDSTCSCASHVCH>No</ISTDSTCSCASHVCH>
			<INCLUDEADVPYMTVCH>No</INCLUDEADVPYMTVCH>
			<ISSUBWORKSCONTRACT>No</ISSUBWORKSCONTRACT>
			<ISVATOVERRIDDEN>No</ISVATOVERRIDDEN>
			<IGNOREORIGVCHDATE>No</IGNOREORIGVCHDATE>
			<ISSERVICETAXOVERRIDDEN>No</ISSERVICETAXOVERRIDDEN>
			<ISISDVOUCHER>No</ISISDVOUCHER>
			<ISEXCISEOVERRIDDEN>No</ISEXCISEOVERRIDDEN>
			<ISEXCISESUPPLYVCH>No</ISEXCISESUPPLYVCH>
			<ISGSTOVERRIDDEN>No</ISGSTOVERRIDDEN>
			<GSTNOTEXPORTED>No</GSTNOTEXPORTED>
			<ISVATPRINCIPALACCOUNT>No</ISVATPRINCIPALACCOUNT>
			<ISSHIPPINGWITHINSTATE>No</ISSHIPPINGWITHINSTATE>
			<ISCANCELLED>No</ISCANCELLED>
			<HASCASHFLOW>Yes</HASCASHFLOW>
			<ISPOSTDATED>No</ISPOSTDATED>
			<USETRACKINGNUMBER>No</USETRACKINGNUMBER>
			<ISINVOICE>Yes</ISINVOICE>
			<MFGJOURNAL>No</MFGJOURNAL>
			<HASDISCOUNTS>Yes</HASDISCOUNTS>
			<ASPAYSLIP>No</ASPAYSLIP>
			<ISCOSTCENTRE>No</ISCOSTCENTRE>
			<ISSTXNONREALIZEDVCH>No</ISSTXNONREALIZEDVCH>
			<ISEXCISEMANUFACTURERON>No</ISEXCISEMANUFACTURERON>
			<ISBLANKCHEQUE>No</ISBLANKCHEQUE>
			<ISVOID>No</ISVOID>
			<ISONHOLD>No</ISONHOLD>
			<ORDERLINESTATUS>No</ORDERLINESTATUS>
			<VATISAGNSTCANCSALES>No</VATISAGNSTCANCSALES>
			<VATISPURCEXEMPTED>No</VATISPURCEXEMPTED>
			<ISVATRESTAXINVOICE>No</ISVATRESTAXINVOICE>
			<VATISASSESABLECALCVCH>No</VATISASSESABLECALCVCH>
			<ISVATDUTYPAID>Yes</ISVATDUTYPAID>
			<ISDELIVERYSAMEASCONSIGNEE>No</ISDELIVERYSAMEASCONSIGNEE>
			<ISDISPATCHSAMEASCONSIGNOR>No</ISDISPATCHSAMEASCONSIGNOR>
			<ISDELETED>No</ISDELETED>
			<CHANGEVCHMODE>No</CHANGEVCHMODE>
			<ALTERID></ALTERID>
			<MASTERID></MASTERID>
			<VOUCHERKEY></VOUCHERKEY>
			<EXCLUDEDTAXATIONS.LIST></EXCLUDEDTAXATIONS.LIST>
			<OLDAUDITENTRIES.LIST></OLDAUDITENTRIES.LIST>
			<ACCOUNTAUDITENTRIES.LIST></ACCOUNTAUDITENTRIES.LIST>
			<AUDITENTRIES.LIST></AUDITENTRIES.LIST>
			<DUTYHEADDETAILS.LIST></DUTYHEADDETAILS.LIST>
			<SUPPLEMENTARYDUTYHEADDETAILS.LIST></SUPPLEMENTARYDUTYHEADDETAILS.LIST>
			<INVOICEDELNOTES.LIST></INVOICEDELNOTES.LIST>
			<INVOICEORDERLIST.LIST></INVOICEORDERLIST.LIST>
			<INVOICEINDENTLIST.LIST></INVOICEINDENTLIST.LIST>
			<ATTENDANCEENTRIES.LIST></ATTENDANCEENTRIES.LIST>
			<ORIGINVOICEDETAILS.LIST></ORIGINVOICEDETAILS.LIST>
			<INVOICEEXPORTLIST.LIST></INVOICEEXPORTLIST.LIST>
			<LEDGERENTRIES.LIST>
			<OLDAUDITENTRYIDS.LIST TYPE="Number">
			<OLDAUDITENTRYIDS>-1</OLDAUDITENTRYIDS>
			</OLDAUDITENTRYIDS.LIST>
			<LEDGERNAME>'.$name."(".$dist_code.")".'</LEDGERNAME>
			<GSTCLASS/>
			<ISDEEMEDPOSITIVE>No</ISDEEMEDPOSITIVE>
			<LEDGERFROMITEM>No</LEDGERFROMITEM>
			<REMOVEZEROENTRIES>No</REMOVEZEROENTRIES>
			<ISPARTYLEDGER>Yes</ISPARTYLEDGER>
			<ISLASTDEEMEDPOSITIVE>No</ISLASTDEEMEDPOSITIVE>
			<AMOUNT></AMOUNT>
			<SERVICETAXDETAILS.LIST></SERVICETAXDETAILS.LIST>

			<BILLALLOCATIONS.LIST></BILLALLOCATIONS.LIST>
			<INTERESTCOLLECTION.LIST></INTERESTCOLLECTION.LIST>
			<OLDAUDITENTRIES.LIST></OLDAUDITENTRIES.LIST>
			<ACCOUNTAUDITENTRIES.LIST></ACCOUNTAUDITENTRIES.LIST>
			<AUDITENTRIES.LIST></AUDITENTRIES.LIST>
			<INPUTCRALLOCS.LIST></INPUTCRALLOCS.LIST>
			<DUTYHEADDETAILS.LIST></DUTYHEADDETAILS.LIST>
			<EXCISEDUTYHEADDETAILS.LIST></EXCISEDUTYHEADDETAILS.LIST>
			<RATEDETAILS.LIST></RATEDETAILS.LIST>
			<SUMMARYALLOCS.LIST></SUMMARYALLOCS.LIST>
			<STPYMTDETAILS.LIST></STPYMTDETAILS.LIST>
			<EXCISEPAYMENTALLOCATIONS.LIST></EXCISEPAYMENTALLOCATIONS.LIST>
			<TAXBILLALLOCATIONS.LIST></TAXBILLALLOCATIONS.LIST>
			<TAXOBJECTALLOCATIONS.LIST></TAXOBJECTALLOCATIONS.LIST>
			<TDSEXPENSEALLOCATIONS.LIST></TDSEXPENSEALLOCATIONS.LIST>
			<VATSTATUTORYDETAILS.LIST></VATSTATUTORYDETAILS.LIST>
			<COSTTRACKALLOCATIONS.LIST></COSTTRACKALLOCATIONS.LIST>
			<REFVOUCHERDETAILS.LIST></REFVOUCHERDETAILS.LIST>
			<INVOICEWISEDETAILS.LIST></INVOICEWISEDETAILS.LIST>
			<VATITCDETAILS.LIST></VATITCDETAILS.LIST>
			<ADVANCETAXDETAILS.LIST></ADVANCETAXDETAILS.LIST>
			</LEDGERENTRIES.LIST>
			<LEDGERENTRIES.LIST>

				<TAXCLASSIFICATIONNAME/>
				<LEDGERNAME>'.$ledgername.'</LEDGERNAME>
				<METHODTYPE>As User Defined Value</METHODTYPE>
				<VOUCHERFBTCATEGORY/>
				<GSTCLASS/>
				<ISDEEMEDPOSITIVE>No</ISDEEMEDPOSITIVE>
				<LEDGERFROMITEM>No</LEDGERFROMITEM>
				<REMOVEZEROENTRIES>No</REMOVEZEROENTRIES>
				<ISPARTYLEDGER>No</ISPARTYLEDGER>
				<ISLASTDEEMEDPOSITIVE>No</ISLASTDEEMEDPOSITIVE>
				<SERVICETAXDETAILS.LIST></SERVICETAXDETAILS.LIST>
				<BANKALLOCATIONS.LIST></BANKALLOCATIONS.LIST>
				<BILLALLOCATIONS.LIST></BILLALLOCATIONS.LIST>$bsnid_a$bsnid_a$bsnid_a$bsnid_a$bsnid_a$bsni$bsnid_a$bsnid_ad_a
				<INTERESTCOLLECTION.LIST></INTERESTCOLLECTION.LIST>
				<OLDAUDITENTRIES.LIST></OLDAUDITENTRIES.LIST>
				<ACCOUNTAUDITENTRIES.LIST></ACCOUNTAUDITENTRIES.LIST>
				<AUDITENTRIES.LIST></AUDITENTRIES.LIST>
				<INPUTCRALLOCS.LIST></INPUTCRALLOCS.LIST>
				<INVENTORYALLOCATIONS.LIST></INVENTORYALLOCATIONS.LIST>
				<DUTYHEADDETAILS.LIST></DUTYHEADDETAILS.LIST>
				<EXCISEDUTYHEADDETAILS.LIST></EXCISEDUTYHEADDETAILS.LIST>
				<RATEDETAILS.LIST></RATEDETAILS.LIST>
				<SUMMARYALLOCS.LIST></SUMMARYALLOCS.LIST>
				<STPYMTDETAILS.LIST></STPYMTDETAILS.LIST>
				<EXCISEPAYMENTALLOCATIONS.LIST></EXCISEPAYMENTALLOCATIONS.LIST>
				<TAXBILLALLOCATIONS.LIST></TAXBILLALLOCATIONS.LIST>
				<TAXOBJECTALLOCATIONS.LIST></TAXOBJECTALLOCATIONS.LIST>
				<TDSEXPENSEALLOCATIONS.LIST></TDSEXPENSEALLOCATIONS.LIST>
				<VATSTATUTORYDETAILS.LIST></VATSTATUTORYDETAILS.LIST>
				<REFVOUCHERDETAILS.LIST></REFVOUCHERDETAILS.LIST>
				<INVOICEWISEDETAILS.LIST></INVOICEWISEDETAILS.LIST>
				<VATITCDETAILS.LIST></VATITCDETAILS.LIST>
				<ADVANCETAXDETAILS.LIST></ADVANCETAXDETAILS.LIST>
				</LEDGERENTRIES.LIST>
				<LEDGERENTRIES.LIST>
				<TAXCLASSIFICATIONNAME/>
				<LEDGERNAME></LEDGERNAME>
				<METHODTYPE>As User Defined Value</METHODTYPE>
				<VOUCHERFBTCATEGORY/>
				<GSTCLASS/>
				<ISDEEMEDPOSITIVE>No</ISDEEMEDPOSITIVE>
				<LEDGERFROMITEM>No</LEDGERFROMITEM>
				<REMOVEZEROENTRIES>No</REMOVEZEROENTRIES>
				<ISPARTYLEDGER>No</ISPARTYLEDGER>
				<ISLASTDEEMEDPOSITIVE>No</ISLASTDEEMEDPOSITIVE>
				<SERVICETAXDETAILS.LIST></SERVICETAXDETAILS.LIST>
				<BANKALLOCATIONS.LIST></BANKALLOCATIONS.LIST>
				<BILLALLOCATIONS.LIST></BILLALLOCATIONS.LIST>
				<INTERESTCOLLECTION.LIST></INTERESTCOLLECTION.LIST>
				<OLDAUDITENTRIES.LIST></OLDAUDITENTRIES.LIST>
				<ACCOUNTAUDITENTRIES.LIST></ACCOUNTAUDITENTRIES.LIST>
				<AUDITENTRIES.LIST></AUDITENTRIES.LIST>
				<INPUTCRALLOCS.LIST></INPUTCRALLOCS.LIST>
				<INVENTORYALLOCATIONS.LIST></INVENTORYALLOCATIONS.LIST>
				<DUTYHEADDETAILS.LIST></DUTYHEADDETAILS.LIST>
				<EXCISEDUTYHEADDETAILS.LIST></EXCISEDUTYHEADDETAILS.LIST>
				<RATEDETAILS.LIST></RATEDETAILS.LIST>
				<SUMMARYALLOCS.LIST></SUMMARYALLOCS.LIST>
				<STPYMTDETAILS.LIST></STPYMTDETAILS.LIST>
				<EXCISEPAYMENTALLOCATIONS.LIST></EXCISEPAYMENTALLOCATIONS.LIST>
				<TAXBILLALLOCATIONS.LIST></TAXBILLALLOCATIONS.LIST>
				<TAXOBJECTALLOCATIONS.LIST></TAXOBJECTALLOCATIONS.LIST>
				<TDSEXPENSEALLOCATIONS.LIST></TDSEXPENSEALLOCATIONS.LIST>
				<VATSTATUTORYDETAILS.LIST></VATSTATUTORYDETAILS.LIST>
				<REFVOUCHERDETAILS.LIST></REFVOUCHERDETAILS.LIST>
				<INVOICEWISEDETAILS.LIST></INVOICEWISEDETAILS.LIST>
				<VATITCDETAILS.LIST></VATITCDETAILS.LIST>
				<ADVANCETAXDETAILS.LIST></ADVANCETAXDETAILS.LIST>
				</LEDGERENTRIES.LIST>';
							 
	$requestXMLEND='					
<PAYROLLMODEOFPAYMENT.LIST>      </PAYROLLMODEOFPAYMENT.LIST>
      <ATTDRECORDS.LIST>      </ATTDRECORDS.LIST>
     </VOUCHER>
    </TALLYMESSAGE>

   </REQUESTDATA>
  </IMPORTDATA>
 </BODY>
</ENVELOPE>';








			$itemsbsn=array();
			$itemname=array();
			$bsnidarray=array();



			
			
			while(!feof($file))
			{	

				$data = array();
				$data2 = array();
				$data3 = array();
				$bsn = fgets($file);	
				$flag=true;	
			
						
			// echo "<pre>"; 
			// echo $row;
			// print_r($bsn);
			// echo $flag;
			//exit;

			$BSNNO = mysql_real_escape_string(strtolower(trim($bsn)));
			$bsn = $BSNNO;
			$bsn_id = "";
			$item_id = "";
			$category_id = "";
			$bsnAgeign = "";
			$bsnFIFOStatus = "";

			if($row+1>0 && !empty($BSNNO) && $warehouse_id>0) {	

			// Check BSN already exists at any warehouse

			if($flag){
			$condi =" LOWER(BSN.bsn)='".$BSNNO."' AND WS.status='A' AND WS.warehouse_id = '".$warehouse_id."' ";
			$resultset = $this->_getSelectList('table_warehouse_stock AS WS LEFT JOIN table_item_bsn AS BSN ON BSN.bsn_id = WS.bsn_id','WS.warehouse_id, WS.warehouse_stock_id, BSN.bsn_id, BSN.item_id, BSN.status AS bsnStatus, BSN.category_id, datediff(NOW(),WS.dispatch_date) AS WhStockAgeing , datediff(NOW(),WS.stk_recharged_date) AS RechargeAgeing ','',$condi);

			//print_r($resultset);
			//exit;

				if(sizeof($resultset)==0 && empty($resultset) && !isset($resultset[0]->warehouse_id)) {
					$strErr .= "Error in Row".$row." ,".$BSNNO.", Sorry this bsn already scanned/Accepted/Already Dispatched/ Not Found.\n";
					$str_err_array=array($strErr);	
					$flag=false;
				} else {

					$warehouse_stock_id = $resultset[0]->warehouse_stock_id;
					$bsn_id = $resultset[0]->bsn_id;
					$item_id = $resultset[0]->item_id;
					$category_id = $resultset[0]->category_id;
					$bsnAgeign   =  $resultset[0]->WhStockAgeing;
					$RechargeAgeing = $resultset[0]->RechargeAgeing;
					$bsnStatus = $resultset[0]->bsnStatus;


					if($bsnAgeign>90) {
						if($RechargeAgeing>0 && $RechargeAgeing<90) {
							$bsnFIFOStatus = 'Brown';  // Recharged Battery 
						} else {
							$bsnFIFOStatus = 'Recharge';  // Need to be recharge
						}
						
					} else if($bsnAgeign>=60 && $bsnAgeign<90) {
						$bsnFIFOStatus = 'Green';	// First Lots
					} else if($bsnAgeign>=30 && $bsnAgeign<60) {
						$bsnFIFOStatus = 'Yellow';	// Second Lots
					} else if($bsnAgeign>=0 && $bsnAgeign<30) {
						$bsnFIFOStatus = 'Red';		// Third or new lots
					}



				}
			}		

					// echo "<pre>";
					// print_r($resultset);
					// echo "<br>";
					// echo $bsnFIFOStatus;
					// echo "<br>";
					//exit;
			// Insert/Update data in the system			

			if($flag){	

				if(isset($BSNNO) && !empty($BSNNO) && $item_id>0 && $bsnFIFOStatus!="") {

					// Check FIFO here of every BSN

					$condi = " WS.warehouse_id = '".$warehouse_id."' AND WS.item_id ='".$item_id."' AND WS.status = 'A'  GROUP BY FIFO_STATUS, WS.warehouse_id, WS.item_id  " ;

					$resultset = $this->_getSelectList('view_getWarehouseFIFO AS WS',"*",'',$condi);

					// echo "<pre>";
					// print_r($resultset);
					// //exit;

					if(is_array($resultset) && sizeof($resultset)>0)  { 

						// Get the list of all battery with thier status

						foreach ($resultset as $key => $value) {

							if($flag == false)  {
								break;
							}

							// echo "<pre>";
							// print_r($value);

							// Check if the battery

							$resultsetSubSet = $resultset;

							//echo $value->FIFO_STATUS.'-'.$bsnFIFOStatus.'<br>';

							if($value->FIFO_STATUS == 'Recharge' && $bsnFIFOStatus == 'Recharge' ) {

								$strErr .= "Error in Row".$row." ,".$BSNNO.", Need to be Recharge.\n";
								$str_err_array_subset=array($strErr);	
								$flag=false;
								break;

							} else if($value->FIFO_STATUS == 'Brown' && $bsnFIFOStatus == 'Brown' ) {

								$flag = true;
								break;

							} else if($value->FIFO_STATUS == 'Green' && $bsnFIFOStatus == 'Green' ) {
								
								// Check if there is any Recharged battery avaible.
								foreach ($resultsetSubSet as $key2 => $value2) {
									# code...
									if($value2->FIFO_STATUS == 'Brown' && $value2->ttlStock>0) {
										$strErr .= "Error in Row".$row." ,".$BSNNO.", Please Maintain FIFO, Firstly Dispatch Recharged Batteries.\n";
										$str_err_array=array($strErr);	
										$flag=false;
										break;
									}							

								}
								
								break;	

							} else if($value->FIFO_STATUS == 'Yellow' && $bsnFIFOStatus == 'Yellow' ) {

								// Check if there is any Recharged battery avaible.
								foreach ($resultsetSubSet as $key2 => $value2) {
									# code...
									if($value2->FIFO_STATUS == 'Brown'  && $value2->ttlStock>0) {
										$strErr .= "Error in Row".$row." ,".$BSNNO.", Please Maintain FIFO, Firstly Dispatch Recharged Batteries.\n";
										$str_err_array=array($strErr);	
										$flag=false;
										break;
									} else 	if($value2->FIFO_STATUS == 'Green'  && $value2->ttlStock>0) {
										$strErr .= "Error in Row".$row." ,".$BSNNO.", Please Maintain FIFO, Firstly Dispatch First Lots Batteries.\n";
										$str_err_array=array($strErr);	
										$flag=false;
										break;
									} 					

								}


							} else if($value->FIFO_STATUS == 'Red' && $bsnFIFOStatus == 'Red' ) {
								//echo "hoorey";
								//print_r($resultsetSubSet);
								//exit;
								// Check if there is any Recharged battery avaible.
								foreach ($resultsetSubSet as $key2 => $value2) {
									# code...
									//echo $value2->FIFO_STATUS;
									if($value2->FIFO_STATUS == 'Brown'  && $value2->ttlStock>0) {
										$strErr .= "Error in Row".$row." ,".$BSNNO.", Please Maintain FIFO, Firstly Dispatch Recharged Batteries.\n";
										$str_err_array=array($strErr);	
										$flag=false;
										break;
									} else 	if($value2->FIFO_STATUS == 'Green'  && $value2->ttlStock>0) {
										$strErr .= "Error in Row".$row." ,".$BSNNO.", Please Maintain FIFO, Firstly Dispatch First Lots Batteries.\n";
										$str_err_array=array($strErr);	
										$flag=false;

										break;
										//echo "TYYYYY";
									}  else if($value2->FIFO_STATUS == 'Yellow'  && $value2->ttlStock>0) {
										// echo "Yellow";
										// exit;
										$strErr .= "Error in Row".$row." ,".$BSNNO.", Please Maintain FIFO, Firstly Dispatch Second Lots Batteries.\n";
										$str_err_array=array($strErr);	
										$flag=false;
										break;
									}	 	 						

								}
								
							} else {

								// Check if there is any Recharged battery avaible.
								foreach ($resultsetSubSet as $key2 => $value2) {
									# code...
									if($value2->FIFO_STATUS ==  $bsnFIFOStatus && $value2->ttlStock>0) {
										$flag = true;	
										break;

									} else {
										//$strErr .= "Error in Row".$row." ,".$BSNNO.", Stock not found!.\n";
										//$str_err_array=array($strErr);	
										$flag=false;
									}		 	 						

								}

							
							}
						}


					}

					// echo $flag;
					// print_r($strErr);
					// exit;

					if($flag==true) {

						// Add Stock distributor in-process table as GRN

						//$condi = " distributor_id = '".$distributor_id."' AND bsn_id ='".$bsn_id."' AND status IN ('A', 'I')";
						$condi = "  bsn_id ='".$bsn_id."' AND status IN ('A', 'I')";
						$resultset = $this->_getSelectList('table_item_dis_stk_inprocess','dis_stk_inpro_id, acpt_stock_value','',$condi);

						if(is_array($resultset) && sizeof($resultset)>0)  {   

						// Update Warehouse dispatches to distributor activity
							// $disInProcess = array();

							// $disInProcess['last_update_datetime'] 		= date('Y-m-d H:i:s');
							// $disInProcess['bill_date'] 					= $bill_date;
							// $disInProcess['bill_no']                    = $bill_no;

							// $this->_dbUpdate($disInProcess,'table_item_dis_stk_inprocess', 'dis_stk_inpro_id="'.$resultset[0]->dis_stk_inpro_id.'"');
								$strErr .= "Error in Row".$row." ,".$BSNNO.", Duplicate BSN.\n";
								$str_err_array=array($strErr);	
								$flag=false;

						} else {
										
							// Add Warehouse dispatches to distributor activity
							$disInProcess = array();

							$disInProcess['account_id']              	= $_SESSION['accountId'];
							$disInProcess['warehouse_id']      			= $warehouse_id;	
							$disInProcess['distributor_id'] 			= $distributor_id;
							$disInProcess['item_id']                    = $item_id;
							$disInProcess['bsn_id']                    	= $bsn_id;
							$disInProcess['category_id']                = $category_id;
							$disInProcess['attribute_value_id']  		= 0;
							$disInProcess['bill_date'] 					= $bill_date;
							$disInProcess['bill_no']                    = $bill_no;
							$disInProcess['rec_stock_value']            = 1;
							$disInProcess['delivery_challan_no']        = $delivery_challan_no;
							$disInProcess['delivery_challan_date']      = $delivery_challan_date;
							$disInProcess['transporter_name']	        = $transporterName;
							$disInProcess['last_update_datetime']   	= date('Y-m-d H:i:s');	
							$disInProcess['created_datetime'] 			= date('Y-m-d H:i:s');
							$disInProcess['status']  					= 'I';

							// print_r($disInProcess);
							// exit;

							$dis_stk_inpro_id = $this->_dbInsert($disInProcess,'table_item_dis_stk_inprocess');

                            $itemname[]=$item_id;
                            $bsnidarray[]=$bsn_id;
								
							if(isset($itemsbsn[$disInProcess['item_id']]))
							{
							    $count_bsn = (int)$itemsbsn[$disInProcess['item_id']]['count']+1;
							 	$itemsbsn[$disInProcess['item_id']]['count'] = $count_bsn;
								
							}
							 else
							{
							 	$itemsbsn[$disInProcess['item_id']]['count'] = 1;	
						    }


							// Add All the BSN Detail with invoice ID for refereces

							$bsnTracking['account_id']   				=  $_SESSION['accountId'];
							$bsnTracking['tally_inv_master_id']   		=  $bill_no;
							$bsnTracking['distributor_id']   			=  $distributor_id;
							$bsnTracking['warehouse_id']   				=  $warehouse_id;
							$bsnTracking['item_id']   					=  $item_id;
							$bsnTracking['bsn_id']   					=  $bsn_id;
							$bsnTracking['bsn']   						=  $bsn;
							$bsnTracking['challan_no']        			= $delivery_challan_no;
							$bsnTracking['challan_date']      			= $delivery_challan_date;
							$bsnTracking['transporter_name']	        = $transporterName;
							$bsnTracking['last_update_date']   			= date('Y-m-d');	
							$bsnTracking['created_datetime'] 			= date('Y-m-d H:i:s');
							$bsnTracking['status']  					= 'A';  

							$this->_dbInsert($bsnTracking,'table_order_invoice_bsn_tracking');



						}




						$whStock = array();
						$whStock['last_updated_date']	 	=  date('Y-m-d');	
						$whStock['last_update_datetime']	=  date('Y-m-d H:i:s');	
						$whStock['status']		            =  'D';
						$this->_dbUpdate($whStock,'table_warehouse_stock', " warehouse_stock_id=".$warehouse_stock_id);



						// Update BSN table sale distributor and date of dispatch
						if($bsnStatus !='S' || $bsnStatus !='D') {

							$bsnArray['sale_distributor_id']    		= $distributor_id;	
							$bsnArray['distributor_dispatch_date']    	= date('Y-m-d');
							$bsnArray['last_updated_date']	 			= date('Y-m-d');	
							$bsnArray['last_update_datetime']			= date('Y-m-d H:i:s');	
							$bsnArray['status']							= 'D'; // Dispatched to distributor
								
							$this->_dbUpdate($bsnArray,'table_item_bsn', " bsn_id='".$bsn_id."'");  

						}



					}

				
				} else {

					$strErr .= "Error in Row".$row." ,".$BSNNO.", Sorry this bsn or bsn model/Item or bsn status Not Found.\n";
					$str_err_array=array($strErr);	
					$flag=false;	

			}
			
			} // Check BSN Available in warehouse Stock

			} // Check warehouse ID/BSN

		 $row++;	   

		}  // While loop end here



        $itembsnid=implode(',',($bsnidarray));
            $ALLINVENTORYENTRIESITEM=array();
            $itemnameis=array_unique($itemname);

            $itemname= array_values(array_filter($itemnameis));

            for($j=0;$j<count($itemname);$j++)
            {
            //$STOCKITEM=$itemname[$j];
            $item_details=$this->_getSelectList2('table_item AS I',"item_name,item_division",''," I.item_id='".$itemname[$j]."'  ");
           // print_r($item_details[0]->item_name);exit;
            if($item_details[0]->item_division==3)
				        	{
                                $godown_name="CHN ". ucwords(strtolower($arr[1]))." Sales";
                                

				        	}else if($item_details[0]->item_division==1)
				        	{
				        		 $godown_name="AMPS ". ucwords(strtolower($arr[1]))." Sales";

				        	}else if($item_details[0]->item_division==2)
				        	{
				        		 $godown_name="Inst ". ucwords(strtolower($arr[1]))." Sales";
				        	}
            $STOCKITEM=$item_details[0]->item_name;
			if(isset($itemsbsn[$itemname[$j]]))
			{
	        $ACTUALQTY= $itemsbsn[$itemname[$j]]['count'];

              }

               $ALLINVENTORYENTRIESITEM[] ='
													<ALLINVENTORYENTRIES.LIST>
													<STOCKITEMNAME>'.$STOCKITEM.'</STOCKITEMNAME>
													<ISDEEMEDPOSITIVE>Yes</ISDEEMEDPOSITIVE>
													<ISLASTDEEMEDPOSITIVE>Yes</ISLASTDEEMEDPOSITIVE>
													<ISAUTONEGATE>No</ISAUTONEGATE>
													<ISCUSTOMSCLEARANCE>No</ISCUSTOMSCLEARANCE>
													<ISTRACKCOMPONENT>No</ISTRACKCOMPONENT>
													<ISTRACKPRODUCTION>No</ISTRACKPRODUCTION>
													<ISPRIMARYITEM>No</ISPRIMARYITEM>
													<ISSCRAP>No</ISSCRAP>
													<RATE></RATE>
													<AMOUNT></AMOUNT>
													<ACTUALQTY>'.$ACTUALQTY.' nos</ACTUALQTY>
													<BILLEDQTY>'.$ACTUALQTY.' nos</BILLEDQTY>
													<INCLVATRATE></INCLVATRATE>
													<BATCHALLOCATIONS.LIST>
												     <GODOWNNAME>'.$godown_name.'</GODOWNNAME>
                                                    <BATCHNAME>Primary Batch</BATCHNAME>
													<DESTINATIONGODOWNNAME>'.$godown_name.'</DESTINATIONGODOWNNAME>
													<INDENTNO/>
													<ORDERNO/>
													<TRACKINGNUMBER/>
													<DYNAMICCSTISCLEARED>No</DYNAMICCSTISCLEARED>
													<AMOUNT></AMOUNT>
													<ACTUALQTY>'.$ACTUALQTY.' nos</ACTUALQTY>
													<BILLEDQTY>'.$ACTUALQTY.' nos</BILLEDQTY>
													<INCLVATRATE></INCLVATRATE>
													<ADDITIONALDETAILS.LIST></ADDITIONALDETAILS.LIST>
													<VOUCHERCOMPONENTLIST.LIST></VOUCHERCOMPONENTLIST.LIST>
													</BATCHALLOCATIONS.LIST>
													<ACCOUNTINGALLOCATIONS.LIST>
													<OLDAUDITENTRYIDS.LIST TYPE="Number">
													<OLDAUDITENTRYIDS>-1</OLDAUDITENTRYIDS>
													</OLDAUDITENTRYIDS.LIST>
													<LEDGERNAME>'.$ledgername.'</LEDGERNAME>
													<GSTCLASS/>
													<ISDEEMEDPOSITIVE>Yes</ISDEEMEDPOSITIVE>
													<LEDGERFROMITEM>No</LEDGERFROMITEM>
													<REMOVEZEROENTRIES>No</REMOVEZEROENTRIES>
													<ISPARTYLEDGER>No</ISPARTYLEDGER>
													<ISLASTDEEMEDPOSITIVE>Yes</ISLASTDEEMEDPOSITIVE>
													<AMOUNT></AMOUNT>
													<SERVICETAXDETAILS.LIST></SERVICETAXDETAILS.LIST>
													<BANKALLOCATIONS.LIST></BANKALLOCATIONS.LIST>
													<BILLALLOCATIONS.LIST></BILLALLOCATIONS.LIST>
													<INTERESTCOLLECTION.LIST></INTERESTCOLLECTION.LIST>
													<OLDAUDITENTRIES.LIST></OLDAUDITENTRIES.LIST>
													<ACCOUNTAUDITENTRIES.LIST></ACCOUNTAUDITENTRIES.LIST>
													<AUDITENTRIES.LIST></AUDITENTRIES.LIST>
													<INPUTCRALLOCS.LIST></INPUTCRALLOCS.LIST>
													<DUTYHEADDETAILS.LIST></DUTYHEADDETAILS.LIST>
													<EXCISEDUTYHEADDETAILS.LIST></EXCISEDUTYHEADDETAILS.LIST>
													<RATEDETAILS.LIST></RATEDETAILS.LIST>
													<SUMMARYALLOCS.LIST></SUMMARYALLOCS.LIST>
													<STPYMTDETAILS.LIST></STPYMTDETAILS.LIST>
													<EXCISEPAYMENTALLOCATIONS.LIST></EXCISEPAYMENTALLOCATIONS.LIST>
													<TAXBILLALLOCATIONS.LIST></TAXBILLALLOCATIONS.LIST>
													<TAXOBJECTALLOCATIONS.LIST></TAXOBJECTALLOCATIONS.LIST>
													<TDSEXPENSEALLOCATIONS.LIST></TDSEXPENSEALLOCATIONS.LIST>
													<VATSTATUTORYDETAILS.LIST></VATSTATUTORYDETAILS.LIST>
													<COSTTRACKALLOCATIONS.LIST></COSTTRACKALLOCATIONS.LIST>
													<REFVOUCHERDETAILS.LIST></REFVOUCHERDETAILS.LIST>
													<INVOICEWISEDETAILS.LIST></INVOICEWISEDETAILS.LIST>
													<VATITCDETAILS.LIST></VATITCDETAILS.LIST>
													<ADVANCETAXDETAILS.LIST></ADVANCETAXDETAILS.LIST>
													</ACCOUNTINGALLOCATIONS.LIST>
													<DUTYHEADDETAILS.LIST></DUTYHEADDETAILS.LIST>
													<SUPPLEMENTARYDUTYHEADDETAILS.LIST></SUPPLEMENTARYDUTYHEADDETAILS.LIST>
													<TAXOBJECTALLOCATIONS.LIST></TAXOBJECTALLOCATIONS.LIST>
													<REFVOUCHERDETAILS.LIST></REFVOUCHERDETAILS.LIST>
													<EXCISEALLOCATIONS.LIST></EXCISEALLOCATIONS.LIST>
													<EXPENSEALLOCATIONS.LIST></EXPENSEALLOCATIONS.LIST>
													</ALLINVENTORYENTRIES.LIST>';


}

if($dis_stk_inpro_id >0 )
{
	        $requestXMLITEMLIST = implode(' ', $ALLINVENTORYENTRIESITEM);
			       
		    $requestXML = $requestXMLSTART.$requestXMLITEMLIST.$requestXMLEND;
		    $first=array();
		   // echo"<pre>";
		   // print_R($requestXML);exit;
				
			$server = 'http://quytech14.ddns.net:8000/';
			$headers = array( "Content-type: text/xml" ,"Content-length: ".strlen($requestXML) ,"Connection: close" );
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $server);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_TIMEOUT, 100);
			curl_setopt($ch, CURLOPT_POST, true);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $requestXML);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			$data = curl_exec($ch);
			//echo"<pre>";
		
			
		     if(curl_errno($ch)){
		    echo json_encode(array('status'=>'Failed', 'data'=> print curl_error($ch)));

		    }else{

		    	   $p = xml_parser_create();
        xml_parse_into_struct($p, $data, $vals, $index);
       xml_parser_free($p);
		   if(isset($vals[$index['LASTVCHID'][0]]['value']) && $vals[$index['LASTVCHID'][0]]['value']>0) {

		   
        $attributeArray = array();
        $attributeArray['TALLYMASTERNO'] = $vals[$index['LASTVCHID'][0]]['value'];
        
        //$yu=$this->_dbUpdate2($attributeArray,'table_warehouse_stk_trn_dts', "transfer_id =".$transfer_id );  
        $this->_dbUpdate($attributeArray,'table_item_dis_stk_inprocess', " 	bsn_id IN(".$itembsnid.")" );   
//print_R($yu);exit;

      

            $first=array('status'=>'Pushed To Tally Successfully', 'data'=> " Pushed To Tally Successfully.", 'id'=>$vals[$index['LASTVCHID'][0]]['value']);

    } else {
      
      if(isset($vals[$index['LINEERROR'][0]]['value'])) 
        $message = $vals[$index['LINEERROR'][0]]['value'];
      else 
        $message = $vals[0]['value'];


         $first=array('status'=>$message, 'data'=> $message);
    }


    curl_close($ch);

      

               } 

   

	}

echo json_encode(array('data'=> $first));





		 //fclose($file);	

		 // echo "<pre>";
		 // print_r($str_err_array);
		 // echo $row;
		 
		// exit;
			//fclose($file);	  
			  if($row<=0)
			   	return "no";
			   else 
			  	return $str_err_array;
			   //return array('str_err_array'=>$str_err_array,'tallymsg'=>$first);


		} else { // Check all the parameters

			return array('status'=>'failed', 'msg'=>'Imput parameters missing, Please Select Warehouse, Distributor, Invoice date and number!');
		}


	} // End of warehouse dispatches function








// commeted by arvind @4-05-2017 import area





	
	function uploadcityarea() {

		
		$fields = array('City Name*', 'Area Name*','Area Code*');		
		$file = fopen($_FILES['fileToUpload']['tmp_name'],"r");
		$strErr="";
		$row=0;		

		while(! feof($file))
		{		  
		$arrFile=fgetcsv($file);
		
		
			
		$flag=true;	
		
		
	 	if($row>0 && is_array($arrFile)) {	
	  
	 
	  
	    if( empty($arrFile[0]) ||$arrFile[0]=="City Name*" || empty($arrFile[1]) || $arrFile[1]=="Area Name*" || empty($arrFile[2]) || $arrFile[2]=="Area Code*"){
		  
		  		  if(empty($arrFile[0]) || $arrFile[0]=="City Name*" ){
					$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1].",".$arrFile[2].", ".$fields[0]." shouldn't be empty  \n";
				 } 
				  else if(empty($arrFile[1]) || $arrFile[1]=="Area Name*"){
					$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." , ".$arrFile[1].", ".$fields[1]." shouldn't be empty  \n";
				 }else if(empty($arrFile[2]) || $arrFile[2]=="Area Code*"){
					$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." , ".$arrFile[2].", ".$fields[2]." shouldn't be empty  \n";
				 }
				 // else if(empty($arrFile[3]) || $arrFile[3]=="Area Name*"){
     //                 $strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." , ".$arrFile[3].", ".$fields[3]." shouldn't be empty  \n";
				 // }

		 		$str_err_array=array($strErr);						
				$flag=false;
					
		  	}
		
				 
	


								 
				 
		 	if($flag)
		 	{	

		 			
				$condi=" LOWER(city_name)='".mysql_real_escape_string(strtolower($arrFile[0]))."'";
				$CityRec=$this->_getSelectList2('city',"city_id",'',$condi);	

				$city_id=$CityRec[0]->city_id;
				
					// if(!is_array($CityRec))
					// {
					// 	$city = array();
					// 	//$city['state_id']= $this->clean($state_id);
					// 	$city['city_name']= mysql_real_escape_string( $this->clean($arrFile[0]));
					// 	//$city['city_code']= mysql_real_escape_string( $this->clean($arrFile[2]));
					// 	$city['last_update_date']= date('Y-m-d');
					// 	$city['last_update_status']= 'New';
					// 	$city_id=$this->_dbInsert($city,'city');
					// } 
					// else  
					// {
					// 	$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." , City does't exist \n";
					// 	$str_err_array=array($strErr);						
					// 	$flag=false;
					// }

				if(sizeof($CityRec) <=0){
					$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1].", ".$arrFile[2]." , City does't exist \n";
					$str_err_array=array($strErr);						
					$flag=false;
				}
		  	}

            if($flag)
		 	{					
				$condi=" LOWER(market_code)='".mysql_real_escape_string(strtolower($arrFile[2]))."'  AND city_id = ".$city_id;
				$AreaRec=$this->_getSelectList2('table_markets',"market_id",'',$condi);	
				$market_id=$AreaRec[0]->market_id;
				
					// if(!is_array($AreaRec)<=0)
					// {
					// 	$area = array();
					// 	$area['city_id']= $this->clean($city_id);
					// 	$area['market_name']= mysql_real_escape_string( $this->clean($arrFile[1]));
					// 	$area['market_code']= mysql_real_escape_string( $this->clean($arrFile[2]));
					// 	//$area['last_update_date']= date('Y-m-d');
					// 	//$area['last_update_status']= 'New';
					// 	$market_id=$this->_dbInsert($area,'table_markets');
					// } 
					// else  
					// {
					// 	$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." , Area already exists in the system \n";
					// 	$str_err_array=array($strErr);						
					// 	$flag=false;
					// }

				if(sizeof($AreaRec)>0){
					
				
					$strErr .= "Error in Row".$row." ,".$arrFile[0].", ".$arrFile[1].", ".$arrFile[2]." , Area already exists in the system \n";
					$str_err_array=array($strErr);						
					$flag=false;
				
		  		}
			}

			if($flag){
					$area = array();
					$area['account_id'] 	=$_SESSION['accountId'];	
					$area['city_id']		=$this->clean($city_id);
					$area['market_name']	= mysql_real_escape_string($this->clean($arrFile[1]));
					$area['market_code']		= mysql_real_escape_string($this->clean($arrFile[2]));
					$area['status'] 		='A';	
					$area_id  =  $this->_dbInsert($area,' table_markets');
			}

		}
		$row++;	   
		} // End of while loop



		

       



			
		fclose($file);	  
	  
	   if($row<=1)
	   	return "no";
	   else 
	  	return $str_err_array;
	
	}
/************************************* End Upload Area ***************************************/	


























// Distributor dispatches to Dealer

public function uploadDistributorDispatchToDealer ($paramSet) {

//  echo "<pre>";
		//  print_r($paramSet);
		//  print_r($_FILES);
		// exit;
		



		if(sizeof($paramSet)>0 && isset($paramSet['distributor']) && $paramSet['distributor']>0 && isset($paramSet['dealer_id']) && $paramSet['dealer_id']>0 ) {

			$distributor_id 		= $paramSet['distributor'];
			$dealer_id              = $paramSet['dealer_id'];
			//$bill_no 	 		= trim($paramSet['invoice_id']);
			$bill_date 	 		    = date('Y-m-d', strtotime($paramSet['date_id']));

			$delivery_challan_no 	 		= ($paramSet['challan_no'])?$paramSet['challan_no']:'';
			$delivery_challan_date 	 		= ($paramSet['challan_date'])?date('Y-m-d', strtotime($paramSet['challan_date'])):'';


			$file = fopen($_FILES['fileToUpload']['tmp_name'],"r");
			$strErr="";
			$resultset = "";
			$row=0;	

		    $BSN = '';




			
			
			while(!feof($file))
			{	

				$data = array();
				$data2 = array();
				$data3 = array();
				$bsn = fgets($file);	
				$flag=true;	
			
						
			 // echo "<pre>"; 
			 // echo $row;
			 // print_r($bsn);
			 // echo $flag;
			 // exit;

			$BSNNO = mysql_real_escape_string(strtolower(trim($bsn)));

			$bsn = $BSNNO;
			$bsn_id = "";
			$item_id = "";
			$category_id = "";
			$bsnAgeign = "";
			$bsnFIFOStatus = "";
			// $condi ="ib.bsn='".$BSNNO."'";
			// $bsn_name = $this->_getSelectList('table_item_bsn AS ib','ib.bsn_id as bsn_id ','',$condi);
            //$bsn_id=$bsn_name[0]->bsn_id;
            
		if($row+1>0 && !empty($BSNNO) && $distributor_id>0) {	

			// Check BSN already exists at any warehouse

			if($flag){

			$condi =" LOWER(BSN.bsn)='".$BSNNO."' AND D.status='A' AND D.distributor_id = '".$distributor_id."' ";
			$resultset = $this->_getSelectList('table_item_distributor_stock AS D 
				LEFT JOIN table_item_bsn AS BSN ON BSN.bsn_id = D.bsn_id',
			   'D.distributor_id as distributor_id , D.dis_stk_id as dis_stk_id, BSN.bsn_id, BSN.item_id,
				BSN.status AS bsnStatus, BSN.category_id','',$condi);
		 //    echo"<pre>";
			// print_r($resultset);
			// exit;



				if(sizeof($resultset)==0 && empty($resultset) ) {
					$strErr .= "Error in Row".$row." ,".$BSNNO.", Sorry this bsn already scanned/Accepted/Already Dispatched/ Not Found.\n";
					$str_err_array=array($strErr);	
					$flag=false;
					//echo"hello";
					//exit;
				} else {
                    //echo"else part";
                
					$dis_stk_id = $resultset[0]->dis_stk_id;
					$bsn_id = $resultset[0]->bsn_id;
					$item_id = $resultset[0]->item_id;
					$category_id = $resultset[0]->category_id;
					$bsnAgeign   =  $resultset[0]->WhStockAgeing;
					$RechargeAgeing = $resultset[0]->RechargeAgeing;
					$bsnStatus = $resultset[0]->bsnStatus;





                   	$dealerInProcess = array();

							$dealerInProcess['account_id']              = $_SESSION['accountId'];
							$dealerInProcess['retailer_id']      		= $dealer_id;	
							$dealerInProcess['distributor_id'] 			= $distributor_id;
							$dealerInProcess['item_id']                 = $item_id;
							$dealerInProcess['bsn_id']                  = $bsn_id;
							$dealerInProcess['category_id']             = $category_id;
							$dealerInProcess['attribute_value_id']  	= 0;
							//$dealerInProcess['bill_date'] 			= $bill_date;
							//$dealerInProcess['bill_no']               = $bill_no;
							$dealerInProcess['rec_stock_value']         = 1;
							

							$dealerInProcess['last_update_datetime']   	= date('Y-m-d H:i:s');	
							$dealerInProcess['created_datetime'] 		= date('Y-m-d H:i:s');
							$dealerInProcess['status']  				= 'I';

							// print_r($disInProcess);
							// exit;

							$dis_stk_inpro_id = $this->_dbInsert($dealerInProcess,'table_item_dealer_stk_inprocess');




                         $whStock = array();
						 $whStock['last_updated_date']	 	=  date('Y-m-d');	
						 $whStock['last_update_datetime']	=  date('Y-m-d H:i:s');	
						 $whStock['status']		            =  'D';
						 $this->_dbUpdate($whStock,'table_item_distributor_stock', " dis_stk_id=".$dis_stk_id);



                        if($bsnStatus !='S' || $bsnStatus !='D') {

							$bsnArray['sale_retailer_id']    		= $dealer_id;	
							$bsnArray['retailer_dispatch_date']    	= date('Y-m-d');
							$bsnArray['last_updated_date']	 			= date('Y-m-d');	
							$bsnArray['last_update_datetime']			= date('Y-m-d H:i:s');	
							$bsnArray['status']							= 'D'; // Dispatched to Dealer
								
							$this->_dbUpdate($bsnArray,'table_item_bsn', " bsn_id='".$bsn_id."'");  

						}






   // exit;


					// if($bsnAgeign>90) {
					// 	if($RechargeAgeing>0 && $RechargeAgeing<90) {
					// 		$bsnFIFOStatus = 'Brown';  // Recharged Battery 
					// 	} else {
					// 		$bsnFIFOStatus = 'Recharge';  // Need to be recharge
					// 	}
						
					// } else if($bsnAgeign>=60 && $bsnAgeign<90) {
					// 	$bsnFIFOStatus = 'Green';	// First Lots
					// } else if($bsnAgeign>=30 && $bsnAgeign<60) {
					// 	$bsnFIFOStatus = 'Yellow';	// Second Lots
					// } else if($bsnAgeign>=0 && $bsnAgeign<30) {
					// 	$bsnFIFOStatus = 'Red';		// Third or new lots
					// }



				}
			}		

					// echo "<pre>";
					// print_r($resultset);
					// echo "<br>";
					// echo $bsnFIFOStatus;
					// echo "<br>";
					//exit;
			// Insert/Update data in the system			

			if($flag){	

				//if(isset($BSNNO) && !empty($BSNNO) && $item_id>0 && $bsnFIFOStatus!="") {

					// // Check FIFO here of every BSN

					// $condi = " WS.warehouse_id = '".$warehouse_id."' AND WS.item_id ='".$item_id."' AND WS.status = 'A'  GROUP BY FIFO_STATUS, WS.warehouse_id, WS.item_id  " ;

					// $resultset = $this->_getSelectList('view_getWarehouseFIFO AS WS',"*",'',$condi);

					// // echo "<pre>";
					// // print_r($resultset);
					// // //exit;

					// if(is_array($resultset) && sizeof($resultset)>0)  { 

					// 	// Get the list of all battery with thier status

					// 	foreach ($resultset as $key => $value) {

					// 		if($flag == false)  {
					// 			break;
					// 		}

					// 		// echo "<pre>";
					// 		// print_r($value);

					// 		// Check if the battery

					// 		$resultsetSubSet = $resultset;

					// 		//echo $value->FIFO_STATUS.'-'.$bsnFIFOStatus.'<br>';

					// 		if($value->FIFO_STATUS == 'Recharge' && $bsnFIFOStatus == 'Recharge' ) {

					// 			$strErr .= "Error in Row".$row." ,".$BSNNO.", Need to be Recharge.\n";
					// 			$str_err_array_subset=array($strErr);	
					// 			$flag=false;
					// 			break;

					// 		} else if($value->FIFO_STATUS == 'Brown' && $bsnFIFOStatus == 'Brown' ) {

					// 			$flag = true;
					// 			break;

					// 		} else if($value->FIFO_STATUS == 'Green' && $bsnFIFOStatus == 'Green' ) {
								
					// 			// Check if there is any Recharged battery avaible.
					// 			foreach ($resultsetSubSet as $key2 => $value2) {
					// 				# code...
					// 				if($value2->FIFO_STATUS == 'Brown' && $value2->ttlStock>0) {
					// 					$strErr .= "Error in Row".$row." ,".$BSNNO.", Please Maintain FIFO, Firstly Dispatch Recharged Batteries.\n";
					// 					$str_err_array=array($strErr);	
					// 					$flag=false;
					// 					break;
					// 				}							

					// 			}
								
					// 			break;	

					// 		} else if($value->FIFO_STATUS == 'Yellow' && $bsnFIFOStatus == 'Yellow' ) {

					// 			// Check if there is any Recharged battery avaible.
					// 			foreach ($resultsetSubSet as $key2 => $value2) {
					// 				# code...
					// 				if($value2->FIFO_STATUS == 'Brown'  && $value2->ttlStock>0) {
					// 					$strErr .= "Error in Row".$row." ,".$BSNNO.", Please Maintain FIFO, Firstly Dispatch Recharged Batteries.\n";
					// 					$str_err_array=array($strErr);	
					// 					$flag=false;
					// 					break;
					// 				} else 	if($value2->FIFO_STATUS == 'Green'  && $value2->ttlStock>0) {
					// 					$strErr .= "Error in Row".$row." ,".$BSNNO.", Please Maintain FIFO, Firstly Dispatch First Lots Batteries.\n";
					// 					$str_err_array=array($strErr);	
					// 					$flag=false;
					// 					break;
					// 				} 					

					// 			}


					// 		} else if($value->FIFO_STATUS == 'Red' && $bsnFIFOStatus == 'Red' ) {
					// 			//echo "hoorey";
					// 			//print_r($resultsetSubSet);
					// 			//exit;
					// 			// Check if there is any Recharged battery avaible.
					// 			foreach ($resultsetSubSet as $key2 => $value2) {
					// 				# code...
					// 				//echo $value2->FIFO_STATUS;
					// 				if($value2->FIFO_STATUS == 'Brown'  && $value2->ttlStock>0) {
					// 					$strErr .= "Error in Row".$row." ,".$BSNNO.", Please Maintain FIFO, Firstly Dispatch Recharged Batteries.\n";
					// 					$str_err_array=array($strErr);	
					// 					$flag=false;
					// 					break;
					// 				} else 	if($value2->FIFO_STATUS == 'Green'  && $value2->ttlStock>0) {
					// 					$strErr .= "Error in Row".$row." ,".$BSNNO.", Please Maintain FIFO, Firstly Dispatch First Lots Batteries.\n";
					// 					$str_err_array=array($strErr);	
					// 					$flag=false;

					// 					break;
					// 					//echo "TYYYYY";
					// 				}  else if($value2->FIFO_STATUS == 'Yellow'  && $value2->ttlStock>0) {
					// 					// echo "Yellow";
					// 					// exit;
					// 					$strErr .= "Error in Row".$row." ,".$BSNNO.", Please Maintain FIFO, Firstly Dispatch Second Lots Batteries.\n";
					// 					$str_err_array=array($strErr);	
					// 					$flag=false;
					// 					break;
					// 				}	 	 						

					// 			}
								
					// 		} else {

					// 			// Check if there is any Recharged battery avaible.
					// 			foreach ($resultsetSubSet as $key2 => $value2) {
					// 				# code...
					// 				if($value2->FIFO_STATUS ==  $bsnFIFOStatus && $value2->ttlStock>0) {
					// 					$flag = true;	
					// 					break;

					// 				} else {
					// 					//$strErr .= "Error in Row".$row." ,".$BSNNO.", Stock not found!.\n";
					// 					//$str_err_array=array($strErr);	
					// 					$flag=false;
					// 				}		 	 						

					// 			}

							
					// 		}
					// 	}


					// }

					// // echo $flag;
					// // print_r($strErr);
					// // exit;

					// if($flag==true) {

					// 	// Add Stock distributor in-process table as GRN

					// 	//$condi = " distributor_id = '".$distributor_id."' AND bsn_id ='".$bsn_id."' AND status IN ('A', 'I')";
					// 	$condi = "  bsn_id ='".$bsn_id."' AND status IN ('A', 'I')";
					// 	$resultset = $this->_getSelectList('table_item_dis_stk_inprocess','dis_stk_inpro_id, acpt_stock_value','',$condi);

					// 	if(is_array($resultset) && sizeof($resultset)>0)  {   

					// 	// Update Warehouse dispatches to distributor activity
					// 		// $disInProcess = array();

					// 		// $disInProcess['last_update_datetime'] 		= date('Y-m-d H:i:s');
					// 		// $disInProcess['bill_date'] 					= $bill_date;
					// 		// $disInProcess['bill_no']                    = $bill_no;

					// 		// $this->_dbUpdate($disInProcess,'table_item_dis_stk_inprocess', 'dis_stk_inpro_id="'.$resultset[0]->dis_stk_inpro_id.'"');
					// 			$strErr .= "Error in Row".$row." ,".$BSNNO.", Duplicate BSN.\n";
					// 			$str_err_array=array($strErr);	
					// 			$flag=false;

					// 	} else {
										
					// 		// Add Warehouse dispatches to distributor activity
					// 		$disInProcess = array();

					// 		$disInProcess['account_id']              	= $_SESSION['accountId'];
					// 		$disInProcess['warehouse_id']      			= $warehouse_id;	
					// 		$disInProcess['distributor_id'] 			= $distributor_id;
					// 		$disInProcess['item_id']                    = $item_id;
					// 		$disInProcess['bsn_id']                    	= $bsn_id;
					// 		$disInProcess['category_id']                = $category_id;
					// 		$disInProcess['attribute_value_id']  		= 0;
					// 		$disInProcess['bill_date'] 					= $bill_date;
					// 		$disInProcess['bill_no']                    = $bill_no;
					// 		$disInProcess['rec_stock_value']            = 1;
					// 		$disInProcess['delivery_challan_no']        = $delivery_challan_no;
					// 		$disInProcess['delivery_challan_date']      = $delivery_challan_date;

					// 		$disInProcess['last_update_datetime']   	= date('Y-m-d H:i:s');	
					// 		$disInProcess['created_datetime'] 			= date('Y-m-d H:i:s');
					// 		$disInProcess['status']  					= 'I';

					// 		// print_r($disInProcess);
					// 		// exit;

					// 		$dis_stk_inpro_id = $this->_dbInsert($disInProcess,'table_item_dis_stk_inprocess');




					// 		// Add All the BSN Detail with invoice ID for refereces

					// 		$bsnTracking['account_id']   				=  $_SESSION['accountId'];
					// 		$bsnTracking['tally_inv_master_id']   		=  $bill_no;
					// 		$bsnTracking['distributor_id']   			=  $distributor_id;
					// 		$bsnTracking['warehouse_id']   				=  $warehouse_id;
					// 		$bsnTracking['item_id']   					=  $item_id;
					// 		$bsnTracking['bsn_id']   					=  $bsn_id;
					// 		$bsnTracking['bsn']   						=  $bsn;
					// 		$bsnTracking['challan_no']        			= $delivery_challan_no;
					// 		$bsnTracking['challan_date']      			= $delivery_challan_date;
					// 		$bsnTracking['last_update_date']   			= date('Y-m-d');	
					// 		$bsnTracking['created_datetime'] 			= date('Y-m-d H:i:s');
					// 		$bsnTracking['status']  					= 'A';  

					// 		$this->_dbInsert($bsnTracking,'table_order_invoice_bsn_tracking');



					// 	}




					// 	$whStock = array();
					// 	$whStock['last_updated_date']	 	=  date('Y-m-d');	
					// 	$whStock['last_update_datetime']	=  date('Y-m-d H:i:s');	
					// 	$whStock['status']		            =  'D';
					// 	$this->_dbUpdate($whStock,'table_warehouse_stock', " warehouse_stock_id=".$warehouse_stock_id);



					// 	// Update BSN table sale distributor and date of dispatch
					// 	if($bsnStatus !='S' || $bsnStatus !='D') {

					// 		$bsnArray['sale_distributor_id']    		= $distributor_id;	
					// 		$bsnArray['distributor_dispatch_date']    	= date('Y-m-d');
					// 		$bsnArray['last_updated_date']	 			= date('Y-m-d');	
					// 		$bsnArray['last_update_datetime']			= date('Y-m-d H:i:s');	
					// 		$bsnArray['status']							= 'D'; // Dispatched to distributor
								
					// 		$this->_dbUpdate($bsnArray,'table_item_bsn', " bsn_id='".$bsn_id."'");  

					// 	}



					// }

				
				//} 

			// 	else {

			// 		$strErr .= "Error in Row".$row." ,".$BSNNO.", Sorry this bsn or bsn model/Item or bsn status Not Found.\n";
			// 		$str_err_array=array($strErr);	
			// 		$flag=false;	

			// }
			
			} // Check BSN Available in warehouse Stock

			} // Check warehouse ID/BSN

		 $row++;	   

		}  // While loop end here
		 fclose($file);	

		 // echo "<pre>";
		 // print_r($str_err_array);
		 // echo $row;
		 
		// exit;
			fclose($file);	  
			  if($row<=0)
			   	return "no";
			   else 
			  	return $str_err_array;

		} else { // Check all the parameters

			return array('status'=>'failed', 'msg'=>'Imput parameters missing, Please Select Warehouse, Distributor, Invoice date and number!');
		}



	} // End of Distributor dispatches function



/*************************** Start uploading Bulk BSNs By Chirag Gupta May 15,2017 *********************************/ 


function uploadBsnListFile(){	
		$fields = array('BSN*','Date of Sale*','Batch Number');	// Remove Chain Name		
		$file = fopen($_FILES['fileToUpload']['tmp_name'],"r");
		$strErr="";
		$row=0;		
		
		while(! feof($file)){		  
			  $arrFile=fgetcsv($file);
			  $flag=true;		  
			  if($row>0 && is_array($arrFile)) {	

             
              if(empty($arrFile[0]) || empty($arrFile[1])){
				 if(empty($arrFile[0])){
					$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." shouldn't be empty \n";
					
				 }
				 else if(empty($arrFile[3]))
				 {
				 	$strErr .= "Error in Row".$row." ,".$arrFile[3]." shouldn't be empty \n";
				 	$status = $arrFile[3];
				 }



					$str_err_array=array($strErr);
					$flag =false;
				 }
				 
				if($flag)
				{
					$bsn = $arrFile[0];
					$item_code = substr($bsn, 0, 2);
        			$month = substr($bsn, 2, 2);
        			$year = substr($bsn, 4, 1);
        			$manufacture_date = "201".$year."-".$month."-01";
					$item_details = $this->_getSelectList2('table_item','','',' item_erp_code="'.$item_code.'"');

					if(count($item_details)<0) {
						$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5]." ,".$arrFile[6]." ,Item not found corresponding to entered BSN. \n";
						$str_err_array=array($strErr);						
						$flag=false;
					}
				}
				        
				if($flag)
				{	
					$data = array();					
					$data['account_id']=$this->clean($_SESSION['accountId']);	
					$item_details = $this->_getSelectList2('table_item','','',' item_erp_code="'.$item_code.'"');	
					$data['item_id']    			= $item_details[0]->item_id;
					$data['batch_no']				= mysql_escape_string($arrFile[2]);
					$data['manufacture_date']		= $manufacture_date;
					$data['date_of_sale']			= mysql_real_escape_string(date('Y-m-d',strtotime($arrFile[1]))) ;
					$data['recharged_date']			= mysql_real_escape_string(date('Y-m-d',strtotime($arrFile[4]))) ;
					$data['factory_dispatch_date']	= mysql_real_escape_string(date('Y-m-d',strtotime($arrFile[5]))) ;
					$warranty_period 				= $item_details[0]->item_warranty;
					$warranty_prorata 				= $item_details[0]->item_prodata;
					$warranty_grace_period			= $item_details[0]->item_grace;
					$category_id 					= $item_details[0]->category_id;

					if($warranty_period>0)
					$data['warranty_period']=$warranty_period;

					if($warranty_prorata>0)
						$data['warranty_prorata']=$warranty_prorata;

					if($warranty_grace_period>0)
						$data['warranty_grace_period']=$warranty_grace_period;

					if($category_id>0)
						$data['category_id']=$category_id;

					$data['bsn']       				= $bsn;
					$data['stock_value']       		= 1;
					$data['last_updated_date']		= date('Y-m-d');
					$data['last_update_datetime']	= date('Y-m-d H:i:s');
					$data['created_datetime']		= date('Y-m-d H:i:s');
					$data['user_type'] 		        = (isset($_SESSION['userLoginType']))?$_SESSION['userLoginType']:0;	
					$data['web_user_id'] 		    = (isset($_SESSION['userLoginType']))?$_SESSION['PepUpSalesUserId']:0;
					if(strtolower($arrFile[3])=='available')
					{
						$final_status = 'A';
					}
					else if(strtolower($arrFile[3])=='sold')
					{
						$final_status = 'S';
					}
					else if(strtolower($arrFile[3])=='damage')
					{
						$final_status = 'DMG';
					}
					else if(strtolower($arrFile[3])=='replace')
					{
						$final_status = 'R';
					}
					else if(strtolower($arrFile[3])=='dispatched')
					{
						$final_status = 'D';
					}
					else if(strtolower($arrFile[3])=='complaint')
					{
						$final_status = 'C';
					}
					$data['status']					= $final_status;
					$bsn_id = $this->_dbInsert($data,'table_item_bsn'); 					
				}
		  }	
		  $row++;	   
		}		
		fclose($file);	  
	   if($row<=1)
	   	return "no";
	   else 
	  	return $str_err_array;
	}




/********************************* End uploading Bulk BSNs By Chirag Gupta May 15,2017 **************************************/ 



/********************* Start Upload Service Distributor CSV File 16 May 2017 By Pooja ***************************/		


	function uploadServiceDistributorFile(){		
	
		
		$fields = array('Service Distributor Name*', 'Service Distributor Code*', 'Phone No*' , 'Address', 'Country*', 'Region*' , 'Zone*' , 'State*' , 'City*', 'Area', 'Pincode/Zipcode');
	
		$file = fopen($_FILES['fileToUpload']['tmp_name'],"r");
		$strErr="";
		$row=0;	

		while(! feof($file)){		  
			  $arrFile=fgetcsv($file);
			 
		
			 
			  $flag=true;
		if($row>0 && is_array($arrFile)) {	
		 

			
			
		  /*echo "<pre>";
	print_r($arrFile); 
	exit;*/
			  
			if(empty($arrFile[0]) || $arrFile[0]=="Service Distributor Name*" || empty($arrFile[1]) || empty($arrFile[2]) || empty($arrFile[4])  || empty($arrFile[5]) || empty($arrFile[6]) || empty($arrFile[7]) || empty($arrFile[8]) ){
				  
		
		  		if(empty($arrFile[0]) || $arrFile[0]=="Service Distributor Name*" ){
					$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].", ".$arrFile[10].", ".$fields[0]." shouldn't be empty  \n";
				}
				else if(empty($arrFile[1])){
					$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].", ".$arrFile[10].", ".$fields[1]." shouldn't be empty  \n";
				}
				else if(empty($arrFile[2])){
					$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].",  ".$arrFile[10].", ".$fields[2]." shouldn't be empty  \n";
				}
				else if(empty($arrFile[4])){
					$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].", ".$arrFile[10].", ".$fields[4]." shouldn't be empty  \n";
				}
				else if(empty($arrFile[5])){
					$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].", ".$arrFile[10].", ".$fields[5]." shouldn't be empty  \n";
				}
				else if(empty($arrFile[6])){
					$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].", ".$arrFile[10].", ".$fields[6]." shouldn't be empty  \n";
				}
				else if(empty($arrFile[7])){
					$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].", ".$arrFile[10].", ".$fields[7]." shouldn't be empty  \n";
				}
				else if(empty($arrFile[8])){
					$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].", ".$arrFile[10].", ".$fields[8]." shouldn't be empty  \n";
				}
			
				 
		 			$str_err_array=array($strErr);						
						$flag=false;
		    }
		  

		
				



		if($flag){				
				
				if (!is_numeric($arrFile[2])) {					
					$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5]." ,".$arrFile[6]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].", ".$arrFile[10].", Please Provide the valid Phone Number. \n";					
					$str_err_array=array($strErr);						
					$flag=false;					
				} 
		
		}


	
// Check for service distributor code 				

				if($flag){
					$condi=" LOWER(service_distributor_code)='".mysql_real_escape_string(strtolower($arrFile[1]))."' ";
					$aDisCode=$this->_getSelectList('table_service_distributor','*','',$condi);
					if(is_array($aDisCode)) {
						$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5]." ,".$arrFile[6]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].",".$arrFile[10]." , service distributor code already exists in the system. \n";
						$str_err_array=array($strErr);
						$flag =false;
					}
				}

// Fetching Country ID
		if($flag)
		{

			$condi=" LOWER(country_name)='".mysql_real_escape_string(strtolower($arrFile[4]))."'"; 
			$CountryRec=$this->_getSelectList2('country',"country_id",'',$condi);	
			if(isset($CountryRec[0]->country_id) && $CountryRec[0]->country_id>0) {
				$country_id=$CountryRec[0]->country_id;
			} else {
				$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5]." ,".$arrFile[6]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].",".$arrFile[10]."  ,".$arrFile[11].",".$arrFile[12]." ,".$arrFile[13]." ,".$arrFile[14].",".$arrFile[15]." ,".$arrFile[16].",".$arrFile[17]."  ,".$arrFile[18].",".$arrFile[19].",".$arrFile[20].",".$arrFile[21].",".$arrFile[22].",".$arrFile[23]." , ".$arrFile[24]." , ".$arrFile[25]." ,".$arrFile[26]." ,".$arrFile[27]." ,".$arrFile[28]." , Country Not Found. \n";
						$str_err_array=array($strErr);
						$flag =false;
							
			}



		}

// Fetching Region ID
		if($flag)
		{
			$condi=" LOWER(region_name)='".mysql_real_escape_string(strtolower($arrFile[5]))."' and country_id = '".$country_id."'"; 
			$RegionRec=$this->_getSelectList2('table_region',"region_id",'',$condi);	
			

			if(isset($RegionRec[0]->region_id) && $RegionRec[0]->region_id>0) {
				$region_id=$RegionRec[0]->region_id;
			} else {
				$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5]." ,".$arrFile[6]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].",".$arrFile[10]."  ,".$arrFile[11].",".$arrFile[12]." ,".$arrFile[13]." ,".$arrFile[14].",".$arrFile[15]." ,".$arrFile[16].",".$arrFile[17]."  ,".$arrFile[18].",".$arrFile[19].",".$arrFile[20].",".$arrFile[21].",".$arrFile[22].",".$arrFile[23]." , ".$arrFile[24]." , ".$arrFile[25]." ,".$arrFile[26]." ,".$arrFile[27]." ,".$arrFile[28]." , Region Not Found. \n";
						$str_err_array=array($strErr);
						$flag =false;
							
			}


		}		

// Fetching Zone ID
		if($flag)
		{
			$condi=" LOWER(zone_name)='".mysql_real_escape_string(strtolower($arrFile[6]))."' and region_id = '".$region_id."'"; 
			$ZoneRec=$this->_getSelectList2('table_zone',"zone_id",'',$condi);	
			


			if(isset($ZoneRec[0]->zone_id) && $ZoneRec[0]->zone_id>0) {
				$zone_id=$ZoneRec[0]->zone_id;
			} else {
				$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5]." ,".$arrFile[6]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].",".$arrFile[10]."  ,".$arrFile[11].",".$arrFile[12]." ,".$arrFile[13]." ,".$arrFile[14].",".$arrFile[15]." ,".$arrFile[16].",".$arrFile[17]."  ,".$arrFile[18].",".$arrFile[19].",".$arrFile[20].",".$arrFile[21].",".$arrFile[22].",".$arrFile[23]." , ".$arrFile[24]." , ".$arrFile[25]." ,".$arrFile[26]." ,".$arrFile[27]." ,".$arrFile[28]." , Zone Not Found. \n";
						$str_err_array=array($strErr);
						$flag =false;
							
			}




		}		

// Fetching State ID
		if($flag)
		{
			$condi=" LOWER(state_name)='".mysql_real_escape_string(strtolower($arrFile[7]))."' and zone_id = '".$zone_id."'"; 
			$StateRec=$this->_getSelectList2('state',"state_id",'',$condi);	
			$state_id=$StateRec[0]->state_id;
			if(!is_array($StateRec)){
				$state = array();
				$state['zone_id']=$this->clean($zone_id);
				$state['state_name']=mysql_real_escape_string( $this->clean($arrFile[7]));
				$state['last_update_date']=date('Y-m-d');
				$state['last_update_status']='New';
				$state_id=$this->_dbInsert($state,'state');
			}
		}

// Fetching City ID
		// if($flag)
		// {
		// 	$condi=" LOWER(city_name)='".mysql_real_escape_string(strtolower($arrFile[6]))."' and state_id = '".$state_id."'"; 
		// 	$CityRec=$this->_getSelectList2('city',"city_id",'',$condi);	
		// 	$city_id=$CityRec[0]->city_id;
		// }

		// AJAY@2017-05-23 live back to old record
			 if($flag){					
					$condi=" LOWER(city_name)='".mysql_real_escape_string(strtolower($arrFile[8]))."' AND state_id='".$state_id."'";
				 	$CityRec=$this->_getSelectList2('city',"city_id",'',$condi);	
					$city_id=$CityRec[0]->city_id;
					if(!is_array($CityRec)){
						$city = array();
						$city['state_id']=$this->clean($state_id);
						$city['city_name']=mysql_real_escape_string( $this->clean($arrFile[8]));
						$city['last_update_date']=date('Y-m-d');
						$city['last_update_status']='New';
						$city_id=$this->_dbInsert($city,'city');
					}
				 }

// Fetching Area ID

		// if($flag) {		

		// 	$condi=" LOWER(market_name)='".mysql_real_escape_string(strtolower($arrFile[9]))."' and city_id = ".$city_id;
		// 	$AreaRec = $this->_getSelectList('table_markets',"market_id, market_name, market_code",'',$condi);	
		// 	$market_id   = $AreaRec[0]->market_id;
				
		//   }

		if($flag){				
					
					$condi=" LOWER(market_name)='".mysql_real_escape_string($this->clean(strtolower($arrFile[9])))."'";
			
					 	$MarketRec=$this->_getSelectList(' table_markets',"market_id",'',$condi);	
						$market_id= $MarketRec[0]->market_id;
						
						if(sizeof($MarketRec)<=0){
							//$strErr .="Invalid city name: &nbsp;&nbsp;".implode(",",$arrFile)."";
							//$flag =false;
							$market = array();
							$market['account_id'] 	=$_SESSION['accountId'];	
						//	$market['taluka_id']	=$taluka_id;
							$market['market_name']	= mysql_real_escape_string( $this->clean($arrFile[9]));
							$market['status'] 		='A';	
							$market_id=$this->_dbInsert($market,' table_markets');
						}
				}
	
				 
		if($flag){	
			//echo mysql_real_escape_string(strtolower( $this->clean($arrFile[0])))."<br>";			
			$condi=	" LOWER(service_distributor_name)='".mysql_real_escape_string(strtolower($this->clean($arrFile[0])))."' AND state_id ='".$state_id."' AND city_id ='".$city_id."' AND service_distributor_phone_no ='".mysql_real_escape_string($arrFile[2])."'";			
					
			$aRetRec=$this->_getSelectList('table_service_distributor','*','',$condi);
				if(is_array($aRetRec)) {					
					$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5]." ,".$arrFile[6]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9]." , ".$arrFile[10].", Service Distributor already exists in the system. \n";
					$str_err_array=array($strErr);						
					$flag=false;
				}
	    }
	
	
		if($flag){				
				
				if (!is_numeric($arrFile[2])) {					
					$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5]." ,".$arrFile[6]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].",".$arrFile[10]." , Please Provide the valid Phone Number. \n";				
					$str_err_array=array($strErr);						
					$flag=false;					
				} 
		
		}
	
	

			

		



			if($flag){	

					$data = array();		
							
					$data['account_id']	=   $this->clean($_SESSION['accountId']);
					
					
					$data['service_distributor_name']	  =	mysql_real_escape_string($this->clean($arrFile[0]));
					$data['service_distributor_code']	  =	mysql_real_escape_string($this->clean($arrFile[1]));
					$data['service_distributor_phone_no'] =	mysql_real_escape_string($this->clean($arrFile[2]));
					$data['service_distributor_address']  =	mysql_real_escape_string($this->clean($arrFile[3]));
					$data['country_id']				= 	$country_id;
					$data['region_id']				= 	$region_id;
					$data['zone_id']				= 	$zone_id;
					$data['state_id']				= 	$state_id;
					$data['city_id']				= 	$city_id;
					$data['market_id']				=   $market_id;	
					$data['zipcode']				=	mysql_real_escape_string($this->clean($arrFile[10]));		
					$data['start_date']				=	date('Y-m-d');	
					$data['end_date']				=	$this->clean($_SESSION['EndDate']);
					$data['last_update_date']		=	date('Y-m-d');
					$data['last_update_status']		=	'New';
					$data['status']					=	'A';			
					$tsd_id      =   $this->_dbInsert($data,'table_service_distributor');				
				
					 
			}
				
		}
		$row++;	 	  
		    
		}
		fclose($file);	  
	   if($row<=1)
	   	return "no";
	   else 
	  	return $str_err_array;
		
	}


/************************************* End Upload Service Distributor CSV File ***************************************/


/********************* Start Upload Service Distributor Service Personnel CSV File 16 May 2017 By Pooja ***************************/		


	function uploadServiceDistributorSpCsv(){		
	
		
		$fields = array('Service Distributor SP Name*', 'Service Distributor SP Code*', 'Service Distributor Name*','Phone No*' , 'Address', 'Country*', 'Region*' , 'Zone*' , 'State*' , 'City*', 'Area', 'Pincode/Zipcode');
	
		$file = fopen($_FILES['fileToUpload']['tmp_name'],"r");
		$strErr="";
		$row=0;	

		while(! feof($file)){		  
			  $arrFile=fgetcsv($file);
			 
		
			 
			  $flag=true;
		if($row>0 && is_array($arrFile)) {	
		 

			
			
		  /*echo "<pre>";
	print_r($arrFile); 
	exit;*/
			  
			
			if(empty($arrFile[0]) || $arrFile[0]=="Service Distributor SP Name*" || empty($arrFile[1]) || empty($arrFile[2]) || empty($arrFile[3])  || empty($arrFile[5]) || empty($arrFile[6]) || empty($arrFile[7])  || empty($arrFile[8]) || empty($arrFile[9]) ){ 
		
		  		if(empty($arrFile[0]) || $arrFile[0]=="Service Distributor SP Name" ){
					$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].", ".$arrFile[10].", ".$arrFile[11].", ".$fields[0]." shouldn't be empty  \n";
				}
				else if(empty($arrFile[1])){
					$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].", ".$arrFile[10].", ".$arrFile[11].", ".$fields[1]." shouldn't be empty  \n";
				}
				else if(empty($arrFile[2])){
					$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].",  ".$arrFile[10].", ".$arrFile[11].", ".$fields[2]." shouldn't be empty  \n";
				}
				else if(empty($arrFile[3])){
					$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].", ".$arrFile[10].",  ".$arrFile[11].", ".$fields[3]." shouldn't be empty  \n";
				}
				else if(empty($arrFile[5])){
					$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].", ".$arrFile[10].", ".$arrFile[11].",  ".$fields[5]." shouldn't be empty  \n";
				}
				else if(empty($arrFile[6])){
					$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].", ".$arrFile[10].", ".$arrFile[11].",  ".$fields[6]." shouldn't be empty  \n";
				}
				else if(empty($arrFile[7])){
					$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].", ".$arrFile[10].", ".$arrFile[11].",  ".$fields[7]." shouldn't be empty  \n";
				}
				else if(empty($arrFile[8])){
					$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].", ".$arrFile[10].", ".$arrFile[11].", ".$fields[8]." shouldn't be empty  \n";
				}
				else if(empty($arrFile[9])){
					$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].", ".$arrFile[10].", ".$arrFile[11].", ".$fields[9]." shouldn't be empty  \n";
				}
				
			
				 
		 			$str_err_array=array($strErr);						
						$flag=false;
		    }
		  


// Check for service distributor service personnel code 				

				if($flag){
					$condi=" LOWER(service_distributor_sp_code)='".mysql_real_escape_string(strtolower($arrFile[1]))."' ";
					$aDisCode=$this->_getSelectList('table_service_distributor_sp','*','',$condi);
					if(is_array($aDisCode)) {
						$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5]." ,".$arrFile[6]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].",".$arrFile[10]." , ".$arrFile[11].",  service distributor service personnel code already exists in the system. \n";
						$str_err_array=array($strErr);
						$flag =false;
					}
				}

// Fetch serviceDistributorId

		if($flag)
		{
			
			$condi=" LOWER(service_distributor_name)='".mysql_real_escape_string(strtolower($arrFile[2]))."'"; 
			$sDisIdRec=$this->_getSelectList2('table_service_distributor',"service_distributor_id",'',$condi);	
			$service_distributor_id = $sDisIdRec[0]->service_distributor_id;
		}



// Fetching Country ID
		if($flag)
		{

			$condi=" LOWER(country_name)='".mysql_real_escape_string(strtolower($arrFile[5]))."'"; 
			$CountryRec=$this->_getSelectList2('country',"country_id",'',$condi);	
			if(isset($CountryRec[0]->country_id) && $CountryRec[0]->country_id>0) {
				$country_id=$CountryRec[0]->country_id;
			} else {
				$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5]." ,".$arrFile[6]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].",".$arrFile[10]."  ,".$arrFile[11]." , Country Not Found. \n";
						$str_err_array=array($strErr);
						$flag =false;
							
			}



		}

// Fetching Region ID
		if($flag)
		{
			$condi=" LOWER(region_name)='".mysql_real_escape_string(strtolower($arrFile[6]))."' and country_id = '".$country_id."'"; 
			$RegionRec=$this->_getSelectList2('table_region',"region_id",'',$condi);	
			

			if(isset($RegionRec[0]->region_id) && $RegionRec[0]->region_id>0) {
				$region_id=$RegionRec[0]->region_id;
			} else {
				$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5]." ,".$arrFile[6]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].",".$arrFile[10]."  ,".$arrFile[11]." , Region Not Found. \n";
						$str_err_array=array($strErr);
						$flag =false;
							
			}


		}		

// Fetching Zone ID
		if($flag)
		{
			$condi=" LOWER(zone_name)='".mysql_real_escape_string(strtolower($arrFile[7]))."' and region_id = '".$region_id."'"; 
			$ZoneRec=$this->_getSelectList2('table_zone',"zone_id",'',$condi);	
			


			if(isset($ZoneRec[0]->zone_id) && $ZoneRec[0]->zone_id>0) {
				$zone_id=$ZoneRec[0]->zone_id;
			} else {
				$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5]." ,".$arrFile[6]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].",".$arrFile[10]."  ,".$arrFile[11]." , Zone Not Found. \n";
						$str_err_array=array($strErr);
						$flag =false;
							
			}




		}		

// Fetching State ID
		if($flag)
		{
			$condi=" LOWER(state_name)='".mysql_real_escape_string(strtolower($arrFile[8]))."' and zone_id = '".$zone_id."'"; 
			$StateRec=$this->_getSelectList2('state',"state_id",'',$condi);	
			$state_id=$StateRec[0]->state_id;
			if(!is_array($StateRec)){
				$state = array();
				$state['zone_id']=$this->clean($zone_id);
				$state['state_name']=mysql_real_escape_string( $this->clean($arrFile[8]));
				$state['last_update_date']=date('Y-m-d');
				$state['last_update_status']='New';
				$state_id=$this->_dbInsert($state,'state');
			}
		}

// Fetching City ID
		// if($flag)
		// {
		// 	$condi=" LOWER(city_name)='".mysql_real_escape_string(strtolower($arrFile[9]))."' and state_id = '".$state_id."'"; 
		// 	$CityRec=$this->_getSelectList2('city',"city_id",'',$condi);	
		// 	$city_id=$CityRec[0]->city_id;
		// }

		// AJAY@2017-05-23 live back to old record
			 if($flag){					
					$condi=" LOWER(city_name)='".mysql_real_escape_string(strtolower($arrFile[9]))."' AND state_id='".$state_id."'";
				 	$CityRec=$this->_getSelectList2('city',"city_id",'',$condi);	
					$city_id=$CityRec[0]->city_id;
					if(!is_array($CityRec)){
						$city = array();
						$city['state_id']=$this->clean($state_id);
						$city['city_name']=mysql_real_escape_string( $this->clean($arrFile[9]));
						$city['last_update_date']=date('Y-m-d');
						$city['last_update_status']='New';
						$city_id=$this->_dbInsert($city,'city');
					}
				 }

// Fetching Area ID

		// if($flag) {		

		// 	$condi=" LOWER(market_name)='".mysql_real_escape_string(strtolower($arrFile[10]))."' and city_id = ".$city_id;
		// 	$AreaRec = $this->_getSelectList('table_markets',"market_id, market_name, market_code",'',$condi);	
		// 	$market_id   = $AreaRec[0]->market_id;
				
		//   }

		if($flag){				
					
					$condi=" LOWER(market_name)='".mysql_real_escape_string($this->clean(strtolower($arrFile[10])))."'";
			
					 	$MarketRec=$this->_getSelectList(' table_markets',"market_id",'',$condi);	
						$market_id= $MarketRec[0]->market_id;
						
						if(sizeof($MarketRec)<=0){
							//$strErr .="Invalid city name: &nbsp;&nbsp;".implode(",",$arrFile)."";
							//$flag =false;
							$market = array();
							$market['account_id'] 	=$_SESSION['accountId'];	
						//	$market['taluka_id']	=$taluka_id;
							$market['market_name']	= mysql_real_escape_string( $this->clean($arrFile[10]));
							$market['status'] 		='A';	
							$market_id=$this->_dbInsert($market,' table_markets');
						}
				}
	
				 
		if($flag){	
			//echo mysql_real_escape_string(strtolower( $this->clean($arrFile[0])))."<br>";			
			$condi=	" LOWER(service_distributor_sp_name)='".mysql_real_escape_string(strtolower($this->clean($arrFile[0])))."' AND state_id ='".$state_id."' AND city_id ='".$city_id."' AND service_distributor_sp_phone_no ='".mysql_real_escape_string($arrFile[3])."'";			
					
			$aRetRec=$this->_getSelectList('table_service_distributor_sp','*','',$condi);
				if(is_array($aRetRec)) {					
					$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5]." ,".$arrFile[6]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9]." , ".$arrFile[10].", ".$arrFile[11].",  Service Distributor SP already exists in the system. \n";
					$str_err_array=array($strErr);						
					$flag=false;
				}
	    }
	
	
		if($flag){				
				
				if (!is_numeric($arrFile[3])) {					
					$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5]." ,".$arrFile[6]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].",".$arrFile[10]." , ".$arrFile[11].", ".$arrFile[12].", Please Provide the valid Phone Number. \n";				
					$str_err_array=array($strErr);						
					$flag=false;					
				} 
		
		}
	
	

			if($flag){	

					$data = array();		
							
					$data['account_id']	=   $this->clean($_SESSION['accountId']);
					$data['service_distributor_id'] = $service_distributor_id;
					$data['service_distributor_sp_name']	  =	mysql_real_escape_string($this->clean($arrFile[0]));
					$data['service_distributor_sp_code']	  =	mysql_real_escape_string($this->clean($arrFile[1]));
					$data['service_distributor_sp_phone_no'] =	mysql_real_escape_string($this->clean($arrFile[3]));
					$data['service_distributor_sp_address']  =	mysql_real_escape_string($this->clean($arrFile[4]));
					$data['country_id']				= 	$country_id;
					$data['region_id']				= 	$region_id;
					$data['zone_id']				= 	$zone_id;
					$data['state_id']				= 	$state_id;
					$data['city_id']				= 	$city_id;
					$data['market_id']				=   $market_id;	
					$data['zipcode']				=	mysql_real_escape_string($this->clean($arrFile[11]));		
					$data['start_date']				=	date('Y-m-d');	
					$data['end_date']				=	$this->clean($_SESSION['EndDate']);
					$data['last_update_date']		=	date('Y-m-d');
					$data['last_update_status']		=	'New';
					$data['status']					=	'A';			
					$tsd_id      =   $this->_dbInsert($data,'table_service_distributor_sp');				
				
					 
			}
				
		}
		$row++;	 	  
		    
		}
		fclose($file);	  
	   if($row<=1)
	   	return "no";
	   else 
	  	return $str_err_array;
		
	}


/************************************* End Upload Service Distributor Service Personnel CSV File *****************************/



/****************************** Start Upload Dealers From Distributor Panel *********************************/	


	function uploadDealersListFile(){		
	
	//$fields = array('Retailer Name*','Phone No1*','Phone No2','Retailer Address','City*','State*','District*','Taluka Name*','Zipcode','Contact Person1*','contact Phone No1*' ,'Contact Person2','Contact Phone No2','Email-ID1*','Email-ID2','Retailer Class','Route Name','Retailer Channel','Distributor Code','Interested','Retailer Type','Aadhar No','Pan No','Retailer Code*', 'Division');
	
	$fields = array('Dealer Name*','Phone No1*','Phone No2','Dealer Address','State*','City*','Zipcode*','Contact Person1*','contact Phone No1*' ,'Contact Person2','Contact Phone No2','Email-ID1*','Email-ID2','Aadhar No','Pan No','Dealer Code*', 'Division*','Brand*','Country*','Region*','Zone*', 'Area');
	
		$file = fopen($_FILES['fileToUpload']['tmp_name'],"r");
		$strErr="";
		$row=0;		
		while(! feof($file)){		  
			  $arrFile=fgetcsv($file);
			  // echo "<pre>";
			  // print_r($arrFile);
			  $flag=true;
		if($row>0 && is_array($arrFile)) {	
		


		// $relCode       = mysql_real_escape_string(trim($arrFile[15]));
		$division_name = mysql_real_escape_string(strtolower($arrFile[16]));
		$brand_name    = mysql_real_escape_string(strtolower($arrFile[17]));
		
			  
			 if(empty($arrFile[0]) || $arrFile[0]=="Dealer Name*" || empty($arrFile[1]) || empty($arrFile[4]) || empty($arrFile[5]) || empty($arrFile[6]) || empty($arrFile[7]) || empty($arrFile[8]) || empty($arrFile[11]) ||  empty($arrFile[15]) || empty($arrFile[16]) || empty($arrFile[17]) || empty($arrFile[18]) || empty($arrFile[19]) || empty($arrFile[20]) ){
		  
		  		if(empty($arrFile[0]) || $arrFile[0]=="Dealer Name*" ){
					$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5].",".$arrFile[6]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].",".$arrFile[10]."  ,".$arrFile[11].",".$arrFile[12]." ,".$arrFile[13].", ".$arrFile[14]."  ,".$arrFile[15].",".$arrFile[16]." ,".$arrFile[17].",".$arrFile[18]." ,".$arrFile[19].",".$arrFile[20]."  ,".$arrFile[21]." ,".$fields[0]." shouldn't be empty  \n";
				 }
				  else if(empty($arrFile[1])){
					$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5].",".$arrFile[6]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].",".$arrFile[10]."  ,".$arrFile[11].",".$arrFile[12]." ,".$arrFile[13].", ".$arrFile[14]."  ,".$arrFile[15].",".$arrFile[16]." ,".$arrFile[17].",".$arrFile[18]." ,".$arrFile[19].",".$arrFile[20]."  ,".$arrFile[21]." , ".$fields[1]." shouldn't be empty  \n";
				 }
				 else if(empty($arrFile[4])){
					$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5].",".$arrFile[6]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].",".$arrFile[10]."  ,".$arrFile[11].",".$arrFile[12]." ,".$arrFile[13].", ".$arrFile[14]."  ,".$arrFile[15].",".$arrFile[16]." ,".$arrFile[17].",".$arrFile[18]." ,".$arrFile[19].",".$arrFile[20]."  ,".$arrFile[21]." , ".$fields[4]." shouldn't be empty  \n";
				 }
				else if(empty($arrFile[5])){
					$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5].",".$arrFile[6]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].",".$arrFile[10]."  ,".$arrFile[11].",".$arrFile[12]." ,".$arrFile[13].", ".$arrFile[14]."  ,".$arrFile[15].",".$arrFile[16]." ,".$arrFile[17].",".$arrFile[18]." ,".$arrFile[19].",".$arrFile[20]."  ,".$arrFile[21]." , ".$fields[5]." shouldn't be empty  \n";
				 }
				 else if(empty($arrFile[6])){
					$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5].",".$arrFile[6]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].",".$arrFile[10]."  ,".$arrFile[11].",".$arrFile[12]." ,".$arrFile[13].", ".$arrFile[14]."  ,".$arrFile[15].",".$arrFile[16]." ,".$arrFile[17].",".$arrFile[18]." ,".$arrFile[19].",".$arrFile[20]."  ,".$arrFile[21]." , ".$fields[6]." shouldn't be empty  \n";
				 }
				 else if(empty($arrFile[7])){
					$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5].",".$arrFile[6]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].",".$arrFile[10]."  ,".$arrFile[11].",".$arrFile[12]." ,".$arrFile[13].", ".$arrFile[14]."  ,".$arrFile[15].",".$arrFile[16]." ,".$arrFile[17].",".$arrFile[18]." ,".$arrFile[19].",".$arrFile[20]."  ,".$arrFile[21]." , ".$fields[7]." shouldn't be empty  \n";
				 }
				 else if(empty($arrFile[8])){
					$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5].",".$arrFile[6]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].",".$arrFile[10]."  ,".$arrFile[11].",".$arrFile[12]." ,".$arrFile[13].", ".$arrFile[14]."  ,".$arrFile[15].",".$arrFile[16]." ,".$arrFile[17].",".$arrFile[18]." ,".$arrFile[19].",".$arrFile[20]."  ,".$arrFile[21]." , ".$fields[8]." shouldn't be empty  \n";
				 }
				else if(empty($arrFile[11])){
					$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5].",".$arrFile[6]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].",".$arrFile[10]."  ,".$arrFile[11].",".$arrFile[12]." ,".$arrFile[13].", ".$arrFile[14]."  ,".$arrFile[15].",".$arrFile[16]." ,".$arrFile[17].",".$arrFile[18]." ,".$arrFile[19].",".$arrFile[20]."  ,".$arrFile[21]." , ".$fields[11]." shouldn't be empty  \n";
				 }
				 // else if(empty($arrFile[13])){
					// $strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5].",".$arrFile[6]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].",".$arrFile[10]."  ,".$arrFile[11].",".$arrFile[12]." ,".$arrFile[13].", ".$arrFile[14]."  ,".$arrFile[15].",".$arrFile[16]." ,".$arrFile[17].",".$arrFile[18]." ,".$arrFile[19].",".$arrFile[20]."  ,".$arrFile[21].",".$arrFile[22]." , ".$fields[13]." shouldn't be empty  \n";
				 // }
				 else if(empty($arrFile[15])){
					$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5].",".$arrFile[6]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].",".$arrFile[10]."  ,".$arrFile[11].",".$arrFile[12]." ,".$arrFile[13].", ".$arrFile[14]."  ,".$arrFile[15].",".$arrFile[16]." ,".$arrFile[17].",".$arrFile[18]." ,".$arrFile[19].",".$arrFile[20]."  ,".$arrFile[21]." , ".$fields[15]." shouldn't be empty  \n";
				 }
				 else if(empty($arrFile[16])){
					$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5].",".$arrFile[6]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].",".$arrFile[10]."  ,".$arrFile[11].",".$arrFile[12]." ,".$arrFile[13].", ".$arrFile[14]."  ,".$arrFile[15].",".$arrFile[16]." ,".$arrFile[17].",".$arrFile[18]." ,".$arrFile[19].",".$arrFile[20]."  ,".$arrFile[21]." , ".$fields[16]." shouldn't be empty  \n";
				 }
				else if(empty($arrFile[17])){
					$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5].",".$arrFile[6]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].",".$arrFile[10]."  ,".$arrFile[11].",".$arrFile[12]." ,".$arrFile[13].", ".$arrFile[14]."  ,".$arrFile[15].",".$arrFile[16]." ,".$arrFile[17].",".$arrFile[18]." ,".$arrFile[19].",".$arrFile[20]."  ,".$arrFile[21]." , ".$fields[17]." shouldn't be empty  \n";
				 }
				 else if(empty($arrFile[18])){
					$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5].",".$arrFile[6]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].",".$arrFile[10]."  ,".$arrFile[11].",".$arrFile[12]." ,".$arrFile[13].", ".$arrFile[14]."  ,".$arrFile[15].",".$arrFile[16]." ,".$arrFile[17].",".$arrFile[18]." ,".$arrFile[19].",".$arrFile[20]."  ,".$arrFile[21]." , ".$fields[18]." shouldn't be empty  \n";
				 }

				 else if(empty($arrFile[19])){
					$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5].",".$arrFile[6]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].",".$arrFile[10]."  ,".$arrFile[11].",".$arrFile[12]." ,".$arrFile[13].", ".$arrFile[14]."  ,".$arrFile[15].",".$arrFile[16]." ,".$arrFile[17].",".$arrFile[18]." ,".$arrFile[19].",".$arrFile[20]."  ,".$arrFile[21]." , ".$fields[19]." shouldn't be empty  \n";
				 }
				 else if(empty($arrFile[20])){
					$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5].",".$arrFile[6]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].",".$arrFile[10]."  ,".$arrFile[11].",".$arrFile[12]." ,".$arrFile[13].", ".$arrFile[14]."  ,".$arrFile[15].",".$arrFile[16]." ,".$arrFile[17].",".$arrFile[18]." ,".$arrFile[19].",".$arrFile[20]."  ,".$arrFile[21]." , ".$fields[20]." shouldn't be empty  \n";
				 }
				
		 			$str_err_array=array($strErr);						
						$flag=false;
		  }
		  

/************************** Remove Chain Name 18th March 2015 (Gaurav) *************************/
		// Check retailer chain or group 
		
	/*	if($flag){
		

		$relGroup = preg_replace( "/\r|\n/", "", trim($arrFile[15]));

		$condi = " LOWER(chain_name)='".mysql_real_escape_string(strtolower(trim($relGroup)))."'";
							
		$aRetRec = $this->_getSelectList('table_chain','chain_id','',$condi);				
			
		if(!is_array($aRetRec)){
			$data = array();	
			$data['account_id'] =$_SESSION['accountId'];			
			$data['chain_name'] = $relGroup;
			$data['last_updated_on'] = date('Y-m-d H:i:s');
			$data['status']='A';
			$chain_id = $this->_dbInsert($data,'table_chain');
		} else {
			$chain_id = $aRetRec[0]->chain_id;
		}
		
		// inesert the data into retailer chain relationship
			//$chainData = array();
					
			//$chainData['item_id'] = $item_id;	
			//$chainData['chain_id'] = $chain_id;	
			//$this->_dbInsert($chainData,'table_item_chain_relationship');

		} 
*/
		  
	/************************** Remove Chain Name 18th March 2015 (Gaurav) *************************/	  
	// Check retailer Class

	/*  For channel id */

	/*	$relChannel = mysql_real_escape_string(trim($arrFile[17]));
		if($relChannel!="")
		{	
		if($flag)
		{
		
		$condi=	" LOWER(channel_name)='".strtolower($relChannel)."'";
		$aRetRec= $this->_getSelectList('table_retailer_channel_master','channel_id','',$condi);				
			
					if(!is_array($aRetRec)){
						$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5]." ,".$arrFile[6]." ,".$arrFile[7].",".$arrFile[9].",".$arrFile[10]."  ,".$arrFile[11].",".$arrFile[12]." ,".$arrFile[13]." ,".$arrFile[14]." ,".$arrFile[15].",".$arrFile[16].",".$arrFile[17]." , Sorry No Channel exists. \n";
						$str_err_array=array($strErr);						
						$flag=false;
					} else {
						$channel = $aRetRec[0]->channel_id;
					}

		}
	}*/
		/*  For Retailer  type id */

	/*	$relType = mysql_real_escape_string(trim($arrFile[20]));
		if($relType!="")
		{	
		if($flag)
		{
		
		$condi=	" LOWER(type_name)='".strtolower($relType)."'";
		$aRetRec= $this->_getSelectList('table_retailer_type_master','type_id','',$condi);				
			
					if(!is_array($aRetRec)){
						$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5]." ,".$arrFile[6]." ,".$arrFile[7].",".$arrFile[9].",".$arrFile[10]."  ,".$arrFile[11].",".$arrFile[12]." ,".$arrFile[13]." ,".$arrFile[14]." ,".$arrFile[15].",".$arrFile[19].",".$arrFile[20]." , Sorry No Rtailer Type exits . \n";
						$str_err_array=array($strErr);						
						$flag=false;
					} else {
						$typeval = $aRetRec[0]->type_id;
					}

		}
	}*/
		/*  For distributor id */

		
		
		// $disCode = mysql_real_escape_string(trim($arrFile[13]));
		// if($disCode!="")
		// {	
		// if($flag)
		// {
		
		// $condi=	" LOWER(distributor_code)='".strtolower($disCode)."'";
		// $aRetRec= $this->_getSelectList('table_distributors','distributor_id','',$condi);				
			
		// 			if(!is_array($aRetRec)){
		// 				$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5].",".$arrFile[6]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].",".$arrFile[10]."  ,".$arrFile[11].",".$arrFile[12]." ,".$arrFile[13].", ".$arrFile[14]."  ,".$arrFile[15].",".$arrFile[16]." ,".$arrFile[17].",".$arrFile[18]." ,".$arrFile[19].",".$arrFile[20]."  ,".$arrFile[21].",".$arrFile[22]." , Sorry No Distributor code exists . \n";
		// 				$str_err_array=array($strErr);						
		// 				$flag=false;
		// 			} else {
		// 				$distributor = $aRetRec[0]->distributor_id;
		// 			}

		// }
	 //  }
	 //  else
	 //  {
	 
	 //  	$distributor= mysql_real_escape_string(trim($arrFile[13]));
	 //  }

		
		// if($flag){
		// $condi=	" LOWER(relationship_code)='".strtolower($relCode)."'";
		// $aRetRec= $this->_getSelectList('table_relationship','relationship_id','',$condi);				
		// 	if(!is_array($aRetRec)){
		// 				$data = array();	
		// 				$data['account_id'] =$_SESSION['accountId'];			
		// 				$data['relationship_code'] = $relCode;
		// 				$data['last_update_date'] = date('Y-m-d');
		// 				$data['last_update_status']='New';
		// 				$data['status']='A';
		// 				//echo "<pre>";
		// 				//print_r($data);
		// 				//exit;
		// 				$relationship_id = $this->_dbInsert($data,'table_relationship');
		// 			} else {
		// 				$relationship_id = $aRetRec[0]->relationship_id;
		// 			}
		
		// } 
		  
		// Old Code of Checking STate		
		// if($flag){		
				
		// 	// Get State ID with the help of State Name		
		// 	if (in_array(strtolower(trim($arrFile[4])), $this->indian_all_states)) 
		// 	{		
		// 			$condi=" LOWER(state_name)='".mysql_real_escape_string(strtolower($arrFile[4]))."'";
		// 		 	$StateRec=$this->_getSelectList2('state',"state_id",'',$condi);
		// 			$state_id=$StateRec[0]->state_id;
		// 			if(!is_array($StateRec)){
		// 				$state = array();					
		// 				$state['state_name']=mysql_real_escape_string($arrFile[4]);
		// 				$state['last_update_date']=date('Y-m-d');
		// 				$state['last_update_status']='New';
		// 				$state_id=$this->_dbInsert($state,'state');
		// 			}
		// 	} else {
		// 		$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5]." ,".$arrFile[6]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].",".$arrFile[10]."  ,".$arrFile[11].",".$arrFile[12]." , Sorry No State exists. \n";
		// 				$str_err_array=array($strErr);						
		// 				$flag=false;
							
		// 	}
		// }
		
		 // Old code of Checking City
		 // if($flag){				
			// 	 $condi=" LOWER(city_name)='".mysql_real_escape_string($this->clean(strtolower($arrFile[5])))."' AND state_id='".$state_id."'";
		
			// 	 	$CityRec=$this->_getSelectList2('city',"city_id",'',$condi);	
			// 		$city_id= $CityRec[0]->city_id;
			// 		if(!is_array($CityRec)){
			// 			//$strErr .="Invalid city name: &nbsp;&nbsp;".implode(",",$arrFile)."";
			// 			//$flag =false;
			// 			$city = array();
			// 			$city['state_id']= $this->clean($state_id);
			// 			$city['city_name']= mysql_real_escape_string( $this->clean($arrFile[5]));
			// 			$city['last_update_date']=date('Y-m-d');
			// 			$city['last_update_status']='New';
			// 			$city_id=$this->_dbInsert($city,'city');
			// 		}
			// 	 }
// Fetching Country ID
		if($flag)
		{

			$condi=" LOWER(country_name)='".mysql_real_escape_string(strtolower($arrFile[18]))."'"; 
			$CountryRec=$this->_getSelectList2('country',"country_id",'',$condi);	
			if(isset($CountryRec[0]->country_id) && $CountryRec[0]->country_id>0) {
				$country_id=$CountryRec[0]->country_id;
			} else {
				$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5]." ,".$arrFile[6]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].",".$arrFile[10]."  ,".$arrFile[11].",".$arrFile[12]." ,".$arrFile[13]." ,".$arrFile[14].",".$arrFile[15]." ,".$arrFile[16].",".$arrFile[17]."  ,".$arrFile[18].",".$arrFile[19].",".$arrFile[20].",".$arrFile[21]." , Country Not Found. \n";
						$str_err_array=array($strErr);
						$flag =false;
							
			}



		}

// Fetching Region ID
		if($flag)
		{
			$condi=" LOWER(region_name)='".mysql_real_escape_string(strtolower($arrFile[19]))."' and country_id = '".$country_id."'"; 
			$RegionRec=$this->_getSelectList2('table_region',"region_id",'',$condi);	
			

			if(isset($RegionRec[0]->region_id) && $RegionRec[0]->region_id>0) {
				$region_id=$RegionRec[0]->region_id;
			} else {
				$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5]." ,".$arrFile[6]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].",".$arrFile[10]."  ,".$arrFile[11].",".$arrFile[12]." ,".$arrFile[13]." ,".$arrFile[14].",".$arrFile[15]." ,".$arrFile[16].",".$arrFile[17]."  ,".$arrFile[18].",".$arrFile[19].",".$arrFile[20].",".$arrFile[21]." , Region Not Found. \n";
						$str_err_array=array($strErr);
						$flag =false;
							
			}


		}		

// Fetching Zone ID
		if($flag)
		{
			$condi=" LOWER(zone_name)='".mysql_real_escape_string(strtolower($arrFile[20]))."' and region_id = '".$region_id."'"; 
			$ZoneRec=$this->_getSelectList2('table_zone',"zone_id",'',$condi);	
			


			if(isset($ZoneRec[0]->zone_id) && $ZoneRec[0]->zone_id>0) {
				$zone_id=$ZoneRec[0]->zone_id;
			} else {
				$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5]." ,".$arrFile[6]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].",".$arrFile[10]."  ,".$arrFile[11].",".$arrFile[12]." ,".$arrFile[13]." ,".$arrFile[14].",".$arrFile[15]." ,".$arrFile[16].",".$arrFile[17]."  ,".$arrFile[18].",".$arrFile[19].",".$arrFile[20].",".$arrFile[21]." , Zone Not Found. \n";
						$str_err_array=array($strErr);
						$flag =false;
							
			}




		}			

// Fetching State ID
		if($flag)
		{
			$condi=" LOWER(state_name)='".mysql_real_escape_string(strtolower($arrFile[4]))."' and zone_id = '".$zone_id."'"; 
			$StateRec=$this->_getSelectList2('state',"state_id",'',$condi);	
			$state_id=$StateRec[0]->state_id;
			if(!is_array($StateRec)){
				$state = array();
				$state['zone_id']=$this->clean($zone_id);
				$state['state_name']=mysql_real_escape_string( $this->clean($arrFile[4]));
				$state['last_update_date']=date('Y-m-d');
				$state['last_update_status']='New';
				$state_id=$this->_dbInsert($state,'state');
			}
		}

// Fetching City ID
		// if($flag)
		// {
		// 	$condi=" LOWER(city_name)='".mysql_real_escape_string(strtolower($arrFile[6]))."' and state_id = '".$state_id."'"; 
		// 	$CityRec=$this->_getSelectList2('city',"city_id",'',$condi);	
		// 	$city_id=$CityRec[0]->city_id;
		// }

		// AJAY@2017-05-23 live back to old record
		if($flag){					
			$condi=" LOWER(city_name)='".mysql_real_escape_string(strtolower($arrFile[5]))."' AND state_id='".$state_id."'";
			$CityRec=$this->_getSelectList2('city',"city_id",'',$condi);	
			$city_id=$CityRec[0]->city_id;
			if(!is_array($CityRec)){
				$city = array();
				$city['state_id']=$this->clean($state_id);
				$city['city_name']=mysql_real_escape_string( $this->clean($arrFile[5]));
				$city['last_update_date']=date('Y-m-d');
				$city['last_update_status']='New';
				$city_id=$this->_dbInsert($city,'city');
			}
		}
	/* For Taluka */

		/* if($flag){				
				 $condi=" LOWER(taluka_name)='".mysql_real_escape_string($this->clean(strtolower($arrFile[7])))."' AND city_id='".$city_id."'";
		
				 	$TalukaRec=$this->_getSelectList('table_taluka',"taluka_id",'',$condi);	
					$taluka_id= $TalukaRec[0]->taluka_id;
					
					if(sizeof($TalukaRec)<=0){
						//$strErr .="Invalid city name: &nbsp;&nbsp;".implode(",",$arrFile)."";
						//$flag =false;
						$taluka = array();
						$taluka['account_id'] 	=$_SESSION['accountId'];	
						$taluka['city_id']		= $this->clean($city_id);
						$taluka['taluka_name']	= mysql_real_escape_string( $this->clean($arrFile[7]));
						$taluka['status'] 		='A';	
						$taluka_id=$this->_dbInsert($taluka,'table_taluka');
					}
				 }
*/
				 
/* for market */

		if($flag){				
							// $condi=" LOWER(market_name)='".mysql_real_escape_string($this->clean(strtolower($arrFile[26])))."' AND taluka_id='".$taluka_id."'";

			$condi=" LOWER(market_name)='".mysql_real_escape_string($this->clean(strtolower($arrFile[21])))."'";

			$MarketRec=$this->_getSelectList(' table_markets',"market_id",'',$condi);	
			$market_id= $MarketRec[0]->market_id;

			if(sizeof($MarketRec)<=0){
									//$strErr .="Invalid city name: &nbsp;&nbsp;".implode(",",$arrFile)."";
									//$flag =false;
				$market = array();
				$market['account_id'] 	=$_SESSION['accountId'];	
								//	$market['taluka_id']	=$taluka_id;
				$market['market_name']	= mysql_real_escape_string( $this->clean($arrFile[21]));
				$market['status'] 		='A';	
				$market_id=$this->_dbInsert($market,' table_markets');
			}
		}		 

				 
		if($flag){	
			//echo mysql_real_escape_string(strtolower( $this->clean($arrFile[0])))."<br>";			
			$condi=	" LOWER(retailer_name)='".mysql_real_escape_string(strtolower($this->clean($arrFile[0])))."' AND state ='".$state_id."' AND city ='".$city_id."' AND retailer_phone_no ='".mysql_real_escape_string($arrFile[1])."' AND distributor_id='".$_SESSION['distributorId']."'";			
					
			$aRetRec=$this->_getSelectList('table_retailer','*','',$condi);
			if(is_array($aRetRec)) {					
				$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5].",".$arrFile[6]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].",".$arrFile[10]."  ,".$arrFile[11].",".$arrFile[12]." ,".$arrFile[13].", ".$arrFile[14]."  ,".$arrFile[15].",".$arrFile[16]." ,".$arrFile[17].",".$arrFile[18]." ,".$arrFile[19].",".$arrFile[20]."  ,".$arrFile[21]." , Dealer already exists in the system. \n";
						$str_err_array=array($strErr);						
						$flag=false;
			}
		}
	
	
		if($flag){				

			if (!is_numeric($arrFile[1])) {					
				$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5].",".$arrFile[6]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].",".$arrFile[10]."  ,".$arrFile[11].",".$arrFile[12]." ,".$arrFile[13].", ".$arrFile[14]."  ,".$arrFile[15].",".$arrFile[16]." ,".$arrFile[17].",".$arrFile[18]." ,".$arrFile[19].",".$arrFile[20]."  ,".$arrFile[21]." , Please Provide the valid Phone Number. \n";				
				$str_err_array=array($strErr);						
				$flag=false;					
			} 

		}
	
	

			   // Check division name


		if(!empty($division_name) && $flag) {
			$condi=" LOWER(division_name)='".$division_name."' ";
			$division_name_check =$this->_getSelectList('table_division',"division_id",'',$condi);
			if(sizeof($division_name_check) == 0) {
				$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5].",".$arrFile[6]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].",".$arrFile[10]."  ,".$arrFile[11].",".$arrFile[12]." ,".$arrFile[13].", ".$arrFile[14]."  ,".$arrFile[15].",".$arrFile[16]." ,".$arrFile[17].",".$arrFile[18]." ,".$arrFile[19].",".$arrFile[20]."  ,".$arrFile[21]." , division name not found in the system. \n";
				$str_err_array=array($strErr);
				$flag =false;
			} else {

				$division_id =$division_name_check[0]->division_id;	
			}

		}	

//check brands


		if(!empty($brand_name) && $flag) {
			$condi=" LOWER(brand_name)='".$brand_name."' ";
			$brand_name_check =$this->_getSelectList('table_brands',"brand_id",'',$condi);
			if(sizeof($brand_name_check) == 0) {
				$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5].",".$arrFile[6]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].",".$arrFile[10]."  ,".$arrFile[11].",".$arrFile[12]." ,".$arrFile[13].", ".$arrFile[14]."  ,".$arrFile[15].",".$arrFile[16]." ,".$arrFile[17].",".$arrFile[18]." ,".$arrFile[19].",".$arrFile[20]."  ,".$arrFile[21]." , brand name not found in the system. \n";
				$str_err_array=array($strErr);
				$flag =false;
			} else {

				$brand_id =$brand_name_check[0]->brand_id;	
			}

		}	




	
	/*$route=mysql_real_escape_string($arrFile[14]);
				$routeList = explode(",", $route);
				if($arrFile[14]!=''){
					for($i=0;$i<count($routeList);$i++){
						$condition=	" LOWER(route_name)='".strtolower($routeList[$i])."'";
						$col=$this->_getSelectList('table_route','route_id','',$condition);
							if(!is_array($col)){
							$strErr .= "Error in Row".$row." ,".$arrFile[0].",".$arrFile[1]." ,".$arrFile[2].",".$arrFile[3]."  ,".$arrFile[4].",".$arrFile[5]." ,".$arrFile[6]." ,".$arrFile[7].",".$arrFile[8]." ,".$arrFile[9].",".$arrFile[10]."  ,".$arrFile[11].",".$arrFile[12]." ,".$arrFile[13]." ,".$arrFile[14]." , Invalid route name. \n";
						$str_err_array=array($strErr);						
						$flag=false;
							
							}
						}
					}
					*/
				 /* display out */
				/* if(mysql_real_escape_string(trim(strtolower($arrFile[19])))=='hot')
				   {
				   	$outlet='hot';
				   }
				  else if(mysql_real_escape_string(trim(strtolower($arrFile[19])))=='cold')
				  {
				  $outlet='cold';	
				  } 
				  else{
				  	$outlet=''; 
				  }	*/
					 
				if($flag){	
					$data = array();					
					$data['account_id']= $this->clean($_SESSION['accountId']);
					$data['relationship_id']= $relationship_id;
					//$data['chain_id']= $chain_id; // Remove Chain Name
					$data['retailer_name']=mysql_real_escape_string($this->clean($arrFile[0]));
					/* Med mofification */
					
					/* Retailer Code */
					$data['retailer_code']=mysql_real_escape_string($this->clean($arrFile[16]));
				//	$data['channel_id']= mysql_real_escape_string($channel);
					$data['distributor_id']= $_SESSION['distributorId'];
				//	$data['display_outlet']= mysql_real_escape_string($outlet);
				//	$data['type_id']= mysql_real_escape_string($typeval);


					$data['division_id'] = $division_id;
					$data['brand_id'] = $brand_id;
					/* Ends Med modification */

					$data['retailer_address']= mysql_real_escape_string($arrFile[3]);
					$data['market_id']= $market_id;
					$data['country']= $country_id;
					$data['state']= $state_id;
					$data['city']= $city_id;
					$data['region']= $region_id;
					$data['zone']= $zone_id;
					$data['retailer_phone_no']= mysql_real_escape_string( $this->clean($arrFile[1]));
					$data['retailer_phone_no2']= mysql_real_escape_string( $this->clean($arrFile[2]));
					$data['contact_person']= mysql_real_escape_string( $this->clean($arrFile[7]));
					$data['contact_number']= mysql_real_escape_string( $this->clean($arrFile[8]));
					$data['contact_person2']= mysql_real_escape_string( $this->clean($arrFile[9]));
					$data['contact_number2']= mysql_real_escape_string( $this->clean($arrFile[10]));
					$data['retailer_email']= mysql_real_escape_string($arrFile[11]);
					$data['retailer_email2']= mysql_real_escape_string($arrFile[12]);
					$data['zipcode']= mysql_real_escape_string( $this->clean($arrFile[6]));

					/* Added new fields */
					$data['aadhar_no']= mysql_real_escape_string( $this->clean($arrFile[13]));
					$data['pan_no']= mysql_real_escape_string( $this->clean($arrFile[14]));
				//	$data['retailer_code'] = mysql_real_escape_string($this->clean($arrFile[17]));
				//	$data['taluka_id']= $taluka_id;

					$data['start_date']= date('Y-m-d');
					$data['end_date']= $this->clean($_SESSION['EndDate']);
					$data['last_update_date']=date('Y-m-d');
					$data['last_update_status']='New';
					$data['status']= 'A';
					$data['survey_status']= 'I';

					$ret_id=$this->_dbInsert($data,'table_retailer');			
				
					 
					 //print_r($routeList);
					 //exit;
					 
					 //file_put_contents("routename.log",print_r($routeList,true)."\r\n", FILE_APPEND);
					 
					 
					// if($arrFile[13]!=''){
						// $route=mysql_real_escape_string($arrFile[13]);
						// $routeList = explode(",", $route);
						//for($i=0;$i<count($routeList);$i++){
							//$route_name=$routeList[$i];
						// 	$condition=	" LOWER(route_name)='".strtolower($routeList[$i])."'";
						// $resultset=$this->_getSelectList('table_route','route_id,state_id,city_id','',$condition);
						//file_put_contents("routename.log",print_r($condition,true)."\r\n", FILE_APPEND);
						// if(sizeof($resultset)>0 && !empty($resultset) && $resultset[0]->route_id!="") {
						// 		$route_id = $resultset[0]->route_id;
							 
						// 			$stateArr = array_filter(explode(',', $resultset[0]->state_id));
						// 			$cityArr = array_filter(explode(',', $resultset[0]->city_id));
						// 			array_push($stateArr, $state_id);
						// 			array_push($cityArr, $city_id);
						// 			$comma_separated_state = implode(",", array_unique($stateArr));
						// 			$comma_separated_city = implode(",", array_unique($cityArr));
								
								
						// 				$data3=array();
						// 				$data3['state_id']		=  $comma_separated_state;
						// 				$data3['city_id']		=  $comma_separated_city;
						// 				$result = $this->_dbUpdate($data3,'table_route'," route_id='".$route_id."'");
						// 				$data5=array();
						// 				$data5['account_id']	= $_SESSION['accountId'];
						// 				$data5['route_id']	= $route_id; 
						// 				$data5['retailer_id']	= $ret_id;
						// 				$data5['status']	= 'R';
						// 				$result_id = $this->_dbInsert($data5,'table_route_retailer');
						// 		//file_put_contents("routeupdate.log",print_r($data3,true)."\r\n", FILE_APPEND);
						// }
						
						// else {				
						// 		$comma_separated_state = $state_id;
						// 		$comma_separated_city = $city_id;
								
						// 		$data2 = array ();		
						// 		$data2['account_id']		=  $_SESSION['accountId'];	
						// 		$data2['state_id']		=  $comma_separated_state;
						// 		$data2['city_id']		=  $comma_separated_city;
						// 		$data2['route_name']		=  $routeList[$i];
						// 		$data2['status']			=  'A';
								
						// 		//file_put_contents("routedata.log",print_r($data2,true)."\r\n", FILE_APPEND);
						// 				$resultRoute = $this->_dbInsert($data2,'table_route');	
						// 				$data4=array();
						// 				$data4['account_id']	= $_SESSION['accountId'];
						// 				$data4['route_id']	= $resultRoute; 
						// 				$data4['retailer_id']	= $ret_id;
						// 				$data5['status']	= 'R';
						// 				$result_id = $this->_dbInsert($data4,'table_route_retailer');		
						// 	 }
	
						//}
						 
					// }
			
	
									
			}
				
		}
		$row++;	 	  
		    
		}
		fclose($file);	  
	   if($row<=1)
	   	return "no";
	   else 
	  	return $str_err_array;
		
		
		
		
	
	}


/************************************* End Upload Dealers List ***************************************/



/************************************* Dispatching to Distributor with Delivery Challan @01-09-2017 Chirag *********************/

function uploadWarehouseDispatchStockToDistributorChallan($paramSet)
{



		if(sizeof($paramSet)>0 && isset($paramSet['distributor_id']) && $paramSet['distributor_id']>0 && isset($paramSet['invoice_id']) && !empty($paramSet['invoice_id']) && isset($paramSet['date_id']) && !empty($paramSet['date_id']) ) {
			// print_r("I am here");
			// exit;

			$warehouse_id 		= $paramSet['warehouse_id2'];
			$distributor_id     = $paramSet['distributor_id'];
			$bill_no 	 		= trim($paramSet['invoice_id']);
			$bill_date 	 		= date('Y-m-d', strtotime($paramSet['date_id']));

			$delivery_challan_no 	 		= ($paramSet['challan_no'])?$paramSet['challan_no']:'';
			$delivery_challan_date 	 		= ($paramSet['challan_date'])?date('Y-m-d', strtotime($paramSet['challan_date'])):'';
			$transporterName				= ($paramSet['transporter_name'])?$paramSet['transporter_name']:'';

			
			$file = $_SESSION['dispatch_info'];
			$strErr="";
			$resultset = "";
			$row=0;	

		    $BSN = '';

             $dist_name=$this->_getSelectList2('table_distributors AS d',"distributor_name,distributor_code",''," d.distributor_id='".$distributor_id."'  ");
           $wh_name=$this->_getSelectList2('table_warehouse AS w',"warehouse_name",''," w.warehouse_id='".$warehouse_id."'  ");
         
            $name=$dist_name[0]->distributor_name;
            $dist_code  =$dist_name[0]->distributor_code;

            $warehouse_name=$wh_name[0]->warehouse_name;


            $wh_name=$warehouse_name;
	 	    $arr = explode(' ',trim($wh_name));
	 	    $challan=ucwords(strtolower($arr[1]));
            $voucher_type=ucwords(strtolower($arr[1]))." Tax Invoice";
            $ledgername=ucwords(strtolower($arr[1]))." GST Sales @ 18%";
             $dateFormat1 = date('Ymd'); // 20171202
            $DATE = $dateFormat1;

             $requestXMLSTART='<ENVELOPE>
			<HEADER>
			<TALLYREQUEST>Import Data</TALLYREQUEST>
			</HEADER>
			<BODY>
			<IMPORTDATA>
			<REQUESTDESC>
			<REPORTNAME>Vouchers</REPORTNAME>
			<STATICVARIABLES>
			<SVCURRENTCOMPANY>EastmanLtd</SVCURRENTCOMPANY>
			</STATICVARIABLES>
			</REQUESTDESC>
			<REQUESTDATA>
			<TALLYMESSAGE xmlns:UDF="TallyUDF">

			<VOUCHER VCHTYPE="'.$voucher_type.'" ACTION="Create" OBJVIEW="Invoice Voucher View">

			<BASICBUYERADDRESS.LIST TYPE="String">
			<BASICBUYERADDRESS>Flat No. 101, First Floor</BASICBUYERADDRESS>
			<BASICBUYERADDRESS>1, Community Centre,</BASICBUYERADDRESS>
			<BASICBUYERADDRESS>Nariana Industrial Area</BASICBUYERADDRESS>
			<BASICBUYERADDRESS>Phase-1, New Delhi-110028</BASICBUYERADDRESS>

			</BASICBUYERADDRESS.LIST>
			<OLDAUDITENTRYIDS.LIST TYPE="Number">
			<OLDAUDITENTRYIDS>-1</OLDAUDITENTRYIDS>
			</OLDAUDITENTRYIDS.LIST>
			<DATE>'.$DATE.'</DATE>
			<REFERENCEDATE>'.$DATE.'</REFERENCEDATE>

			<GSTREGISTRATIONTYPE></GSTREGISTRATIONTYPE>
			<VATDEALERTYPE></VATDEALERTYPE>
			<TAXUNITNAME>Default Tax Unit</TAXUNITNAME>
			<CLASSNAME>Test Pass Returned</CLASSNAME>
			<PARTYNAME>'.$name.'</PARTYNAME>
			<VOUCHERTYPENAME>'.$voucher_type.'</VOUCHERTYPENAME>
			<REFERENCE></REFERENCE>
			<VOUCHERNUMBER></VOUCHERNUMBER>
			<PARTYLEDGERNAME>'.$name."(".$dist_code.")".'</PARTYLEDGERNAME>
			<BASICBASEPARTYNAME>'.$name."(".$dist_code.")".'</BASICBASEPARTYNAME>
			<CSTFORMISSUETYPE/>
			<CSTFORMRECVTYPE/>
			<FBTPAYMENTTYPE>Default</FBTPAYMENTTYPE>
			<PERSISTEDVIEW>Invoice Voucher View</PERSISTEDVIEW>
			<BASICBUYERNAME>EastmanLtd</BASICBUYERNAME>
			<BASICDATETIMEOFINVOICE>3-Aug-2017 at 15:58</BASICDATETIMEOFINVOICE>
			<BASICDATETIMEOFREMOVAL>3-Aug-2017 at 15:58</BASICDATETIMEOFREMOVAL>
			<VCHGSTCLASS/>
			<ENTEREDBY></ENTEREDBY>
			<DIFFACTUALQTY>No</DIFFACTUALQTY>
			<ISMSTFROMSYNC>No</ISMSTFROMSYNC>
			<ASORIGINAL>No</ASORIGINAL>
			<AUDITED>No</AUDITED>
			<FORJOBCOSTING>No</FORJOBCOSTING>
			<ISOPTIONAL>No</ISOPTIONAL>
			<EFFECTIVEDATE>20170801</EFFECTIVEDATE>
			<USEFOREXCISE>No</USEFOREXCISE>
			<ISFORJOBWORKIN>No</ISFORJOBWORKIN>
			<ALLOWCONSUMPTION>No</ALLOWCONSUMPTION>
			<USEFORINTEREST>No</USEFORINTEREST>
			<USEFORGAINLOSS>No</USEFORGAINLOSS>
			<USEFORGODOWNTRANSFER>No</USEFORGODOWNTRANSFER>
			<USEFORCOMPOUND>No</USEFORCOMPOUND>
			<USEFORSERVICETAX>No</USEFORSERVICETAX>
			<ISEXCISEVOUCHER>No</ISEXCISEVOUCHER>
			<EXCISETAXOVERRIDE>No</EXCISETAXOVERRIDE>
			<USEFORTAXUNITTRANSFER>No</USEFORTAXUNITTRANSFER>
			<EXCISEOPENING>No</EXCISEOPENING>
			<USEFORFINALPRODUCTION>No</USEFORFINALPRODUCTION>
			<ISTDSOVERRIDDEN>No</ISTDSOVERRIDDEN>
			<ISTCSOVERRIDDEN>No</ISTCSOVERRIDDEN>
			<ISTDSTCSCASHVCH>No</ISTDSTCSCASHVCH>
			<INCLUDEADVPYMTVCH>No</INCLUDEADVPYMTVCH>
			<ISSUBWORKSCONTRACT>No</ISSUBWORKSCONTRACT>
			<ISVATOVERRIDDEN>No</ISVATOVERRIDDEN>
			<IGNOREORIGVCHDATE>No</IGNOREORIGVCHDATE>
			<ISSERVICETAXOVERRIDDEN>No</ISSERVICETAXOVERRIDDEN>
			<ISISDVOUCHER>No</ISISDVOUCHER>
			<ISEXCISEOVERRIDDEN>No</ISEXCISEOVERRIDDEN>
			<ISEXCISESUPPLYVCH>No</ISEXCISESUPPLYVCH>
			<ISGSTOVERRIDDEN>No</ISGSTOVERRIDDEN>
			<GSTNOTEXPORTED>No</GSTNOTEXPORTED>
			<ISVATPRINCIPALACCOUNT>No</ISVATPRINCIPALACCOUNT>
			<ISSHIPPINGWITHINSTATE>No</ISSHIPPINGWITHINSTATE>
			<ISCANCELLED>No</ISCANCELLED>
			<HASCASHFLOW>Yes</HASCASHFLOW>
			<ISPOSTDATED>No</ISPOSTDATED>
			<USETRACKINGNUMBER>No</USETRACKINGNUMBER>
			<ISINVOICE>Yes</ISINVOICE>
			<MFGJOURNAL>No</MFGJOURNAL>
			<HASDISCOUNTS>Yes</HASDISCOUNTS>
			<ASPAYSLIP>No</ASPAYSLIP>
			<ISCOSTCENTRE>No</ISCOSTCENTRE>
			<ISSTXNONREALIZEDVCH>No</ISSTXNONREALIZEDVCH>
			<ISEXCISEMANUFACTURERON>No</ISEXCISEMANUFACTURERON>
			<ISBLANKCHEQUE>No</ISBLANKCHEQUE>
			<ISVOID>No</ISVOID>
			<ISONHOLD>No</ISONHOLD>
			<ORDERLINESTATUS>No</ORDERLINESTATUS>
			<VATISAGNSTCANCSALES>No</VATISAGNSTCANCSALES>
			<VATISPURCEXEMPTED>No</VATISPURCEXEMPTED>
			<ISVATRESTAXINVOICE>No</ISVATRESTAXINVOICE>
			<VATISASSESABLECALCVCH>No</VATISASSESABLECALCVCH>
			<ISVATDUTYPAID>Yes</ISVATDUTYPAID>
			<ISDELIVERYSAMEASCONSIGNEE>No</ISDELIVERYSAMEASCONSIGNEE>
			<ISDISPATCHSAMEASCONSIGNOR>No</ISDISPATCHSAMEASCONSIGNOR>
			<ISDELETED>No</ISDELETED>
			<CHANGEVCHMODE>No</CHANGEVCHMODE>
			<ALTERID></ALTERID>
			<MASTERID></MASTERID>
			<VOUCHERKEY></VOUCHERKEY>
			<EXCLUDEDTAXATIONS.LIST></EXCLUDEDTAXATIONS.LIST>
			<OLDAUDITENTRIES.LIST></OLDAUDITENTRIES.LIST>
			<ACCOUNTAUDITENTRIES.LIST></ACCOUNTAUDITENTRIES.LIST>
			<AUDITENTRIES.LIST></AUDITENTRIES.LIST>
			<DUTYHEADDETAILS.LIST></DUTYHEADDETAILS.LIST>
			<SUPPLEMENTARYDUTYHEADDETAILS.LIST></SUPPLEMENTARYDUTYHEADDETAILS.LIST>
			<INVOICEDELNOTES.LIST></INVOICEDELNOTES.LIST>
			<INVOICEORDERLIST.LIST></INVOICEORDERLIST.LIST>
			<INVOICEINDENTLIST.LIST></INVOICEINDENTLIST.LIST>
			<ATTENDANCEENTRIES.LIST></ATTENDANCEENTRIES.LIST>
			<ORIGINVOICEDETAILS.LIST></ORIGINVOICEDETAILS.LIST>
			<INVOICEEXPORTLIST.LIST></INVOICEEXPORTLIST.LIST>
			<LEDGERENTRIES.LIST>
			<OLDAUDITENTRYIDS.LIST TYPE="Number">
			<OLDAUDITENTRYIDS>-1</OLDAUDITENTRYIDS>
			</OLDAUDITENTRYIDS.LIST>
			<LEDGERNAME>'.$name."(".$dist_code.")".'</LEDGERNAME>
			<GSTCLASS/>
			<ISDEEMEDPOSITIVE>No</ISDEEMEDPOSITIVE>
			<LEDGERFROMITEM>No</LEDGERFROMITEM>
			<REMOVEZEROENTRIES>No</REMOVEZEROENTRIES>
			<ISPARTYLEDGER>Yes</ISPARTYLEDGER>
			<ISLASTDEEMEDPOSITIVE>No</ISLASTDEEMEDPOSITIVE>
			<AMOUNT></AMOUNT>
			<SERVICETAXDETAILS.LIST></SERVICETAXDETAILS.LIST>

			<BILLALLOCATIONS.LIST></BILLALLOCATIONS.LIST>
			<INTERESTCOLLECTION.LIST></INTERESTCOLLECTION.LIST>
			<OLDAUDITENTRIES.LIST></OLDAUDITENTRIES.LIST>
			<ACCOUNTAUDITENTRIES.LIST></ACCOUNTAUDITENTRIES.LIST>
			<AUDITENTRIES.LIST></AUDITENTRIES.LIST>
			<INPUTCRALLOCS.LIST></INPUTCRALLOCS.LIST>
			<DUTYHEADDETAILS.LIST></DUTYHEADDETAILS.LIST>
			<EXCISEDUTYHEADDETAILS.LIST></EXCISEDUTYHEADDETAILS.LIST>
			<RATEDETAILS.LIST></RATEDETAILS.LIST>
			<SUMMARYALLOCS.LIST></SUMMARYALLOCS.LIST>
			<STPYMTDETAILS.LIST></STPYMTDETAILS.LIST>
			<EXCISEPAYMENTALLOCATIONS.LIST></EXCISEPAYMENTALLOCATIONS.LIST>
			<TAXBILLALLOCATIONS.LIST></TAXBILLALLOCATIONS.LIST>
			<TAXOBJECTALLOCATIONS.LIST></TAXOBJECTALLOCATIONS.LIST>
			<TDSEXPENSEALLOCATIONS.LIST></TDSEXPENSEALLOCATIONS.LIST>
			<VATSTATUTORYDETAILS.LIST></VATSTATUTORYDETAILS.LIST>
			<COSTTRACKALLOCATIONS.LIST></COSTTRACKALLOCATIONS.LIST>
			<REFVOUCHERDETAILS.LIST></REFVOUCHERDETAILS.LIST>
			<INVOICEWISEDETAILS.LIST></INVOICEWISEDETAILS.LIST>
			<VATITCDETAILS.LIST></VATITCDETAILS.LIST>
			<ADVANCETAXDETAILS.LIST></ADVANCETAXDETAILS.LIST>
			</LEDGERENTRIES.LIST>
			<LEDGERENTRIES.LIST>

				<TAXCLASSIFICATIONNAME/>
				<LEDGERNAME>'.$ledgername.'</LEDGERNAME>
				<METHODTYPE>As User Defined Value</METHODTYPE>
				<VOUCHERFBTCATEGORY/>
				<GSTCLASS/>
				<ISDEEMEDPOSITIVE>No</ISDEEMEDPOSITIVE>
				<LEDGERFROMITEM>No</LEDGERFROMITEM>
				<REMOVEZEROENTRIES>No</REMOVEZEROENTRIES>
				<ISPARTYLEDGER>No</ISPARTYLEDGER>
				<ISLASTDEEMEDPOSITIVE>No</ISLASTDEEMEDPOSITIVE>
				<SERVICETAXDETAILS.LIST></SERVICETAXDETAILS.LIST>
				<BANKALLOCATIONS.LIST></BANKALLOCATIONS.LIST>
				<BILLALLOCATIONS.LIST></BILLALLOCATIONS.LIST>
				<INTERESTCOLLECTION.LIST></INTERESTCOLLECTION.LIST>
				<OLDAUDITENTRIES.LIST></OLDAUDITENTRIES.LIST>
				<ACCOUNTAUDITENTRIES.LIST></ACCOUNTAUDITENTRIES.LIST>
				<AUDITENTRIES.LIST></AUDITENTRIES.LIST>
				<INPUTCRALLOCS.LIST></INPUTCRALLOCS.LIST>
				<INVENTORYALLOCATIONS.LIST></INVENTORYALLOCATIONS.LIST>
				<DUTYHEADDETAILS.LIST></DUTYHEADDETAILS.LIST>
				<EXCISEDUTYHEADDETAILS.LIST></EXCISEDUTYHEADDETAILS.LIST>
				<RATEDETAILS.LIST></RATEDETAILS.LIST>
				<SUMMARYALLOCS.LIST></SUMMARYALLOCS.LIST>
				<STPYMTDETAILS.LIST></STPYMTDETAILS.LIST>
				<EXCISEPAYMENTALLOCATIONS.LIST></EXCISEPAYMENTALLOCATIONS.LIST>
				<TAXBILLALLOCATIONS.LIST></TAXBILLALLOCATIONS.LIST>
				<TAXOBJECTALLOCATIONS.LIST></TAXOBJECTALLOCATIONS.LIST>
				<TDSEXPENSEALLOCATIONS.LIST></TDSEXPENSEALLOCATIONS.LIST>
				<VATSTATUTORYDETAILS.LIST></VATSTATUTORYDETAILS.LIST>
				<REFVOUCHERDETAILS.LIST></REFVOUCHERDETAILS.LIST>
				<INVOICEWISEDETAILS.LIST></INVOICEWISEDETAILS.LIST>
				<VATITCDETAILS.LIST></VATITCDETAILS.LIST>
				<ADVANCETAXDETAILS.LIST></ADVANCETAXDETAILS.LIST>
				</LEDGERENTRIES.LIST>
				<LEDGERENTRIES.LIST>
				<TAXCLASSIFICATIONNAME/>
				<LEDGERNAME></LEDGERNAME>
				<METHODTYPE>As User Defined Value</METHODTYPE>
				<VOUCHERFBTCATEGORY/>
				<GSTCLASS/>
				<ISDEEMEDPOSITIVE>No</ISDEEMEDPOSITIVE>
				<LEDGERFROMITEM>No</LEDGERFROMITEM>
				<REMOVEZEROENTRIES>No</REMOVEZEROENTRIES>
				<ISPARTYLEDGER>No</ISPARTYLEDGER>
				<ISLASTDEEMEDPOSITIVE>No</ISLASTDEEMEDPOSITIVE>
				<SERVICETAXDETAILS.LIST></SERVICETAXDETAILS.LIST>
				<BANKALLOCATIONS.LIST></BANKALLOCATIONS.LIST>
				<BILLALLOCATIONS.LIST></BILLALLOCATIONS.LIST>
				<INTERESTCOLLECTION.LIST></INTERESTCOLLECTION.LIST>
				<OLDAUDITENTRIES.LIST></OLDAUDITENTRIES.LIST>
				<ACCOUNTAUDITENTRIES.LIST></ACCOUNTAUDITENTRIES.LIST>
				<AUDITENTRIES.LIST></AUDITENTRIES.LIST>
				<INPUTCRALLOCS.LIST></INPUTCRALLOCS.LIST>
				<INVENTORYALLOCATIONS.LIST></INVENTORYALLOCATIONS.LIST>
				<DUTYHEADDETAILS.LIST></DUTYHEADDETAILS.LIST>
				<EXCISEDUTYHEADDETAILS.LIST></EXCISEDUTYHEADDETAILS.LIST>
				<RATEDETAILS.LIST></RATEDETAILS.LIST>
				<SUMMARYALLOCS.LIST></SUMMARYALLOCS.LIST>
				<STPYMTDETAILS.LIST></STPYMTDETAILS.LIST>
				<EXCISEPAYMENTALLOCATIONS.LIST></EXCISEPAYMENTALLOCATIONS.LIST>
				<TAXBILLALLOCATIONS.LIST></TAXBILLALLOCATIONS.LIST>
				<TAXOBJECTALLOCATIONS.LIST></TAXOBJECTALLOCATIONS.LIST>
				<TDSEXPENSEALLOCATIONS.LIST></TDSEXPENSEALLOCATIONS.LIST>
				<VATSTATUTORYDETAILS.LIST></VATSTATUTORYDETAILS.LIST>
				<REFVOUCHERDETAILS.LIST></REFVOUCHERDETAILS.LIST>
				<INVOICEWISEDETAILS.LIST></INVOICEWISEDETAILS.LIST>
				<VATITCDETAILS.LIST></VATITCDETAILS.LIST>
				<ADVANCETAXDETAILS.LIST></ADVANCETAXDETAILS.LIST>
				</LEDGERENTRIES.LIST>';
							 
	$requestXMLEND='					
<PAYROLLMODEOFPAYMENT.LIST>      </PAYROLLMODEOFPAYMENT.LIST>
      <ATTDRECORDS.LIST>      </ATTDRECORDS.LIST>
     </VOUCHER>
    </TALLYMESSAGE>

   </REQUESTDATA>
  </IMPORTDATA>
 </BODY>
</ENVELOPE>';



                 
			$itemsbsn=array();
			$itemname=array();
			$bsnidarray=array();

			for($ij=0;$ij<sizeof($file);$ij++)
			{	
				$data = array();
				$data2 = array();
				$data3 = array();
				$bsn = $file[$ij]['bsn_no'];	
				$flag=true;	
			
						
			// echo "<pre>"; 
			// echo $row;
			// print_r($bsn);
			// echo $flag;
			// exit;

			$BSNNO = mysql_real_escape_string(strtolower(trim($bsn)));
			$bsn = $BSNNO;
			$bsn_id = "";
			$item_id = "";
			$category_id = "";
			$bsnAgeign = "";
			$bsnFIFOStatus = "";

			if($row+1>0 && !empty($BSNNO) && $warehouse_id>0) {

			// Check BSN already exists at any warehouse

			if($flag){
			$condi =" LOWER(BSN.bsn)='".$BSNNO."' AND WS.status='A' AND WS.warehouse_id = '".$warehouse_id."' ";
			$resultset = $this->_getSelectList('table_warehouse_stock AS WS LEFT JOIN table_item_bsn AS BSN ON BSN.bsn_id = WS.bsn_id','WS.warehouse_id, WS.warehouse_stock_id, BSN.bsn_id, BSN.item_id, BSN.status AS bsnStatus, BSN.category_id, datediff(NOW(),WS.dispatch_date) AS WhStockAgeing , datediff(NOW(),WS.stk_recharged_date) AS RechargeAgeing ','',$condi);

			// print_r($resultset);
			// exit;

				if(sizeof($resultset)==0 && empty($resultset) && !isset($resultset[0]->warehouse_id)) {
					$strErr .= "Error in Row".$row." ,".$BSNNO.", Sorry this bsn already scanned/Accepted/Already Dispatched/ Not Found.\n";
					$str_err_array=array($strErr);	
					$flag=false;
				} else {

					$warehouse_stock_id = $resultset[0]->warehouse_stock_id;
					$bsn_id = $resultset[0]->bsn_id;
					$item_id = $resultset[0]->item_id;
					$category_id = $resultset[0]->category_id;
					$bsnAgeign   =  $resultset[0]->WhStockAgeing;
					$RechargeAgeing = $resultset[0]->RechargeAgeing;
					$bsnStatus = $resultset[0]->bsnStatus;


					if($bsnAgeign>90) {
						if($RechargeAgeing>0 && $RechargeAgeing<90) {
							$bsnFIFOStatus = 'Brown';  // Recharged Battery 
						} else {
							$bsnFIFOStatus = 'Recharge';  // Need to be recharge
						}
						
					} else if($bsnAgeign>=60 && $bsnAgeign<90) {
						$bsnFIFOStatus = 'Green';	// First Lots
					} else if($bsnAgeign>=30 && $bsnAgeign<60) {
						$bsnFIFOStatus = 'Yellow';	// Second Lots
					} else if($bsnAgeign>=0 && $bsnAgeign<30) {
						$bsnFIFOStatus = 'Red';		// Third or new lots
					}



				}
			}		

					// echo "<pre>";
					// print_r($resultset);
					// echo "<br>";
					// echo $bsnFIFOStatus;
					// echo "<br>";
					//exit;
			// Insert/Update data in the system			

			if($flag){	

				if(isset($BSNNO) && !empty($BSNNO) && $item_id>0 && $bsnFIFOStatus!="") {

					// Check FIFO here of every BSN

					$condi = " WS.warehouse_id = '".$warehouse_id."' AND WS.item_id ='".$item_id."' AND WS.status = 'A'  GROUP BY FIFO_STATUS, WS.warehouse_id, WS.item_id  " ;

					$resultset = $this->_getSelectList('view_getWarehouseFIFO AS WS',"*",'',$condi);

					// echo "<pre>";
					// print_r($resultset);
					// exit;

					if(is_array($resultset) && sizeof($resultset)>0)  { 

						// Get the list of all battery with thier status

						foreach ($resultset as $key => $value) {

							if($flag == false)  {
								break;
							}

							// echo "<pre>";
							// print_r($value);exit;

							// Check if the battery

							$resultsetSubSet = $resultset;

							//echo $value->FIFO_STATUS.'-'.$bsnFIFOStatus.'<br>';

							if($value->FIFO_STATUS == 'Recharge' && $bsnFIFOStatus == 'Recharge' ) {

								$strErr .= "Error in Row".$row." ,".$BSNNO.", Need to be Recharge.\n";
								$str_err_array_subset=array($strErr);	
								$flag=false;
								break;

							} else if($value->FIFO_STATUS == 'Brown' && $bsnFIFOStatus == 'Brown' ) {

								$flag = true;
								break;

							} else if($value->FIFO_STATUS == 'Green' && $bsnFIFOStatus == 'Green' ) {
								
								// Check if there is any Recharged battery avaible.
								foreach ($resultsetSubSet as $key2 => $value2) {
									# code...
									if($value2->FIFO_STATUS == 'Brown' && $value2->ttlStock>0) {
										$strErr .= "Error in Row".$row." ,".$BSNNO.", Please Maintain FIFO, Firstly Dispatch Recharged Batteries.\n";
										$str_err_array=array($strErr);	
										$flag=false;
										break;
									}							

								}
								
								break;	

							} else if($value->FIFO_STATUS == 'Yellow' && $bsnFIFOStatus == 'Yellow' ) {

								// Check if there is any Recharged battery avaible.
								foreach ($resultsetSubSet as $key2 => $value2) {
									# code...
									if($value2->FIFO_STATUS == 'Brown'  && $value2->ttlStock>0) {
										$strErr .= "Error in Row".$row." ,".$BSNNO.", Please Maintain FIFO, Firstly Dispatch Recharged Batteries.\n";
										$str_err_array=array($strErr);	
										$flag=false;
										break;
									} else 	if($value2->FIFO_STATUS == 'Green'  && $value2->ttlStock>0) {
										$strErr .= "Error in Row".$row." ,".$BSNNO.", Please Maintain FIFO, Firstly Dispatch First Lots Batteries.\n";
										$str_err_array=array($strErr);	
										$flag=false;
										break;
									} 					

								}


							} else if($value->FIFO_STATUS == 'Red' && $bsnFIFOStatus == 'Red' ) {
								//echo "hoorey";
								//print_r($resultsetSubSet);
								//exit;
								// Check if there is any Recharged battery avaible.
								foreach ($resultsetSubSet as $key2 => $value2) {
									# code...
									//echo $value2->FIFO_STATUS;
									if($value2->FIFO_STATUS == 'Brown'  && $value2->ttlStock>0) {
										$strErr .= "Error in Row".$row." ,".$BSNNO.", Please Maintain FIFO, Firstly Dispatch Recharged Batteries.\n";
										$str_err_array=array($strErr);	
										$flag=false;
										break;
									} else 	if($value2->FIFO_STATUS == 'Green'  && $value2->ttlStock>0) {
										$strErr .= "Error in Row".$row." ,".$BSNNO.", Please Maintain FIFO, Firstly Dispatch First Lots Batteries.\n";
										$str_err_array=array($strErr);	
										$flag=false;

										break;
										//echo "TYYYYY";
									}  else if($value2->FIFO_STATUS == 'Yellow'  && $value2->ttlStock>0) {
										// echo "Yellow";
										// exit;
										$strErr .= "Error in Row".$row." ,".$BSNNO.", Please Maintain FIFO, Firstly Dispatch Second Lots Batteries.\n";
										$str_err_array=array($strErr);	
										$flag=false;
										break;
									}	 	 						

								}
								
							} else {

								// Check if there is any Recharged battery avaible.
								foreach ($resultsetSubSet as $key2 => $value2) {
									# code...
									if($value2->FIFO_STATUS ==  $bsnFIFOStatus && $value2->ttlStock>0) {
										$flag = true;	
										break;

									} else {
										//$strErr .= "Error in Row".$row." ,".$BSNNO.", Stock not found!.\n";
										//$str_err_array=array($strErr);	
										$flag=false;
									}		 	 						

								}

							
							}
						}


					}

					// echo $flag;
					// print_r($strErr);
					// exit;

					if($flag==true) {

						// Add Stock distributor in-process table as GRN

						//$condi = " distributor_id = '".$distributor_id."' AND bsn_id ='".$bsn_id."' AND status IN ('A', 'I')";
						$condi = "  bsn_id ='".$bsn_id."' AND status IN ('A', 'I')";
						$resultset = $this->_getSelectList('table_item_dis_stk_inprocess','dis_stk_inpro_id, acpt_stock_value','',$condi);

						if(is_array($resultset) && sizeof($resultset)>0)  {   

						// Update Warehouse dispatches to distributor activity
							// $disInProcess = array();

							// $disInProcess['last_update_datetime'] 		= date('Y-m-d H:i:s');
							// $disInProcess['bill_date'] 					= $bill_date;
							// $disInProcess['bill_no']                    = $bill_no;

							// $this->_dbUpdate($disInProcess,'table_item_dis_stk_inprocess', 'dis_stk_inpro_id="'.$resultset[0]->dis_stk_inpro_id.'"');
								$strErr .= "Error in Row".$row." ,".$BSNNO.", Duplicate BSN.\n";
								$str_err_array=array($strErr);	
								$flag=false;

						} else {
										
							// Add Warehouse dispatches to distributor activity
							$disInProcess = array();

							$disInProcess['account_id']              	= $_SESSION['accountId'];
							$disInProcess['warehouse_id']      			= $warehouse_id;	
							$disInProcess['distributor_id'] 			= $distributor_id;
							$disInProcess['item_id']                    = $item_id;
							$disInProcess['bsn_id']                    	= $bsn_id;
							$disInProcess['category_id']                = $category_id;
							$disInProcess['attribute_value_id']  		= 0;
							$disInProcess['bill_date'] 					= $bill_date;
							$disInProcess['bill_no']                    = $bill_no;
							$disInProcess['rec_stock_value']            = 1;
							$disInProcess['delivery_challan_no']        = $delivery_challan_no;
							$disInProcess['delivery_challan_date']      = $delivery_challan_date;
							$disInProcess['transporter_name']	        = $transporterName;
							$disInProcess['last_update_datetime']   	= date('Y-m-d H:i:s');	
							$disInProcess['created_datetime'] 			= date('Y-m-d H:i:s');
							$disInProcess['status']  					= 'I';

							// print_r($disInProcess);
							// exit;

							$dis_stk_inpro_id = $this->_dbInsert($disInProcess,'table_item_dis_stk_inprocess');

                            $itemname[]=$item_id;
                            $bsnidarray[]=$bsn_id;
								
							if(isset($itemsbsn[$disInProcess['item_id']]))
							{
							    $count_bsn = (int)$itemsbsn[$disInProcess['item_id']]['count']+1;
							 	$itemsbsn[$disInProcess['item_id']]['count'] = $count_bsn;
								
							}
							 else
							{
							 	$itemsbsn[$disInProcess['item_id']]['count'] = 1;	
						    }


							// Add All the BSN Detail with invoice ID for refereces

							$bsnTracking['account_id']   				=  $_SESSION['accountId'];
							$bsnTracking['tally_inv_master_id']   		=  $bill_no;
							$bsnTracking['distributor_id']   			=  $distributor_id;
							$bsnTracking['warehouse_id']   				=  $warehouse_id;
							$bsnTracking['item_id']   					=  $item_id;
							$bsnTracking['bsn_id']   					=  $bsn_id;
							$bsnTracking['bsn']   						=  $bsn;
							$bsnTracking['challan_no']        			= $delivery_challan_no;
							$bsnTracking['challan_date']      			= $delivery_challan_date;
							$bsnTracking['transporter_name']	        = $transporterName;
							$bsnTracking['last_update_date']   			= date('Y-m-d');	
							$bsnTracking['created_datetime'] 			= date('Y-m-d H:i:s');
							$bsnTracking['status']  					= 'A';  

							$this->_dbInsert($bsnTracking,'table_order_invoice_bsn_tracking');



						}




						$whStock = array();
						$whStock['last_updated_date']	 	=  date('Y-m-d');	
						$whStock['last_update_datetime']	=  date('Y-m-d H:i:s');	
						$whStock['status']		            =  'D';
						$this->_dbUpdate($whStock,'table_warehouse_stock', " warehouse_stock_id=".$warehouse_stock_id);



						// Update BSN table sale distributor and date of dispatch
						if($bsnStatus !='S' || $bsnStatus !='D') {

							$bsnArray['sale_distributor_id']    		= $distributor_id;	
							$bsnArray['distributor_dispatch_date']    	= date('Y-m-d');
							$bsnArray['last_updated_date']	 			= date('Y-m-d');	
							$bsnArray['last_update_datetime']			= date('Y-m-d H:i:s');	
							$bsnArray['status']							= 'D'; // Dispatched to distributor
								
							$this->_dbUpdate($bsnArray,'table_item_bsn', " bsn_id='".$bsn_id."'");  

						}



					}

				
				} else {

					$strErr .= "Error in Row".$row." ,".$BSNNO.", Sorry this bsn or bsn model/Item or bsn status Not Found.\n";
					$str_err_array=array($strErr);	
					$flag=false;	

			}
			
			} // Check BSN Available in warehouse Stock

			} // Check warehouse ID/BSN

		 $row++;	   

		}  


         $itembsnid=implode(',',($bsnidarray));
            $ALLINVENTORYENTRIESITEM=array();
            $itemnameis=array_unique($itemname);

            $itemname= array_values(array_filter($itemnameis));

            for($j=0;$j<count($itemname);$j++)
            {
            //$STOCKITEM=$itemname[$j];
            $item_details=$this->_getSelectList2('table_item AS I',"item_name,item_division",''," I.item_id='".$itemname[$j]."'  ");
           // print_r($item_details[0]->item_name);exit;
               if($item_details[0]->item_division==3)
				        	{
                                $godown_name="CHN ". ucwords(strtolower($arr[1]))." Sales";
                                

				        	}else if($item_details[0]->item_division==1)
				        	{
				        		 $godown_name="AMPS ". ucwords(strtolower($arr[1]))." Sales";

				        	}else if($item_details[0]->item_division==2)
				        	{
				        		 $godown_name="Inst ". ucwords(strtolower($arr[1]))." Sales";
				        	}
            $STOCKITEM=$item_details[0]->item_name;
			if(isset($itemsbsn[$itemname[$j]]))
			{
	        $ACTUALQTY= $itemsbsn[$itemname[$j]]['count'];

              }

               $ALLINVENTORYENTRIESITEM[] ='
													<ALLINVENTORYENTRIES.LIST>
													<STOCKITEMNAME>'.$STOCKITEM.'</STOCKITEMNAME>
													<ISDEEMEDPOSITIVE>Yes</ISDEEMEDPOSITIVE>
													<ISLASTDEEMEDPOSITIVE>Yes</ISLASTDEEMEDPOSITIVE>
													<ISAUTONEGATE>No</ISAUTONEGATE>
													<ISCUSTOMSCLEARANCE>No</ISCUSTOMSCLEARANCE>
													<ISTRACKCOMPONENT>No</ISTRACKCOMPONENT>
													<ISTRACKPRODUCTION>No</ISTRACKPRODUCTION>
													<ISPRIMARYITEM>No</ISPRIMARYITEM>
													<ISSCRAP>No</ISSCRAP>
													<RATE></RATE>
													<AMOUNT></AMOUNT>
													<ACTUALQTY>'.$ACTUALQTY.' nos</ACTUALQTY>
													<BILLEDQTY>'.$ACTUALQTY.' nos</BILLEDQTY>
													<INCLVATRATE></INCLVATRATE>
													<BATCHALLOCATIONS.LIST>
												     <GODOWNNAME>'.$godown_name.'</GODOWNNAME>
                                                    <BATCHNAME>Primary Batch</BATCHNAME>
													<DESTINATIONGODOWNNAME>'.$godown_name.'</DESTINATIONGODOWNNAME>
													<INDENTNO/>
													<ORDERNO/>
													<TRACKINGNUMBER/>
													<DYNAMICCSTISCLEARED>No</DYNAMICCSTISCLEARED>
													<AMOUNT></AMOUNT>
													<ACTUALQTY>'.$ACTUALQTY.' nos</ACTUALQTY>
													<BILLEDQTY>'.$ACTUALQTY.' nos</BILLEDQTY>
													<INCLVATRATE></INCLVATRATE>
													<ADDITIONALDETAILS.LIST></ADDITIONALDETAILS.LIST>
													<VOUCHERCOMPONENTLIST.LIST></VOUCHERCOMPONENTLIST.LIST>
													</BATCHALLOCATIONS.LIST>
													<ACCOUNTINGALLOCATIONS.LIST>
													<OLDAUDITENTRYIDS.LIST TYPE="Number">
													<OLDAUDITENTRYIDS>-1</OLDAUDITENTRYIDS>
													</OLDAUDITENTRYIDS.LIST>
													<LEDGERNAME>'.$ledgername.'</LEDGERNAME>
													<GSTCLASS/>
													<ISDEEMEDPOSITIVE>Yes</ISDEEMEDPOSITIVE>
													<LEDGERFROMITEM>No</LEDGERFROMITEM>
													<REMOVEZEROENTRIES>No</REMOVEZEROENTRIES>
													<ISPARTYLEDGER>No</ISPARTYLEDGER>
													<ISLASTDEEMEDPOSITIVE>Yes</ISLASTDEEMEDPOSITIVE>
													<AMOUNT></AMOUNT>
													<SERVICETAXDETAILS.LIST></SERVICETAXDETAILS.LIST>
													<BANKALLOCATIONS.LIST></BANKALLOCATIONS.LIST>
													<BILLALLOCATIONS.LIST></BILLALLOCATIONS.LIST>
													<INTERESTCOLLECTION.LIST></INTERESTCOLLECTION.LIST>
													<OLDAUDITENTRIES.LIST></OLDAUDITENTRIES.LIST>
													<ACCOUNTAUDITENTRIES.LIST></ACCOUNTAUDITENTRIES.LIST>
													<AUDITENTRIES.LIST></AUDITENTRIES.LIST>
													<INPUTCRALLOCS.LIST></INPUTCRALLOCS.LIST>
													<DUTYHEADDETAILS.LIST></DUTYHEADDETAILS.LIST>
													<EXCISEDUTYHEADDETAILS.LIST></EXCISEDUTYHEADDETAILS.LIST>
													<RATEDETAILS.LIST></RATEDETAILS.LIST>
													<SUMMARYALLOCS.LIST></SUMMARYALLOCS.LIST>
													<STPYMTDETAILS.LIST></STPYMTDETAILS.LIST>
													<EXCISEPAYMENTALLOCATIONS.LIST></EXCISEPAYMENTALLOCATIONS.LIST>
													<TAXBILLALLOCATIONS.LIST></TAXBILLALLOCATIONS.LIST>
													<TAXOBJECTALLOCATIONS.LIST></TAXOBJECTALLOCATIONS.LIST>
													<TDSEXPENSEALLOCATIONS.LIST></TDSEXPENSEALLOCATIONS.LIST>
													<VATSTATUTORYDETAILS.LIST></VATSTATUTORYDETAILS.LIST>
													<COSTTRACKALLOCATIONS.LIST></COSTTRACKALLOCATIONS.LIST>
													<REFVOUCHERDETAILS.LIST></REFVOUCHERDETAILS.LIST>
													<INVOICEWISEDETAILS.LIST></INVOICEWISEDETAILS.LIST>
													<VATITCDETAILS.LIST></VATITCDETAILS.LIST>
													<ADVANCETAXDETAILS.LIST></ADVANCETAXDETAILS.LIST>
													</ACCOUNTINGALLOCATIONS.LIST>
													<DUTYHEADDETAILS.LIST></DUTYHEADDETAILS.LIST>
													<SUPPLEMENTARYDUTYHEADDETAILS.LIST></SUPPLEMENTARYDUTYHEADDETAILS.LIST>
													<TAXOBJECTALLOCATIONS.LIST></TAXOBJECTALLOCATIONS.LIST>
													<REFVOUCHERDETAILS.LIST></REFVOUCHERDETAILS.LIST>
													<EXCISEALLOCATIONS.LIST></EXCISEALLOCATIONS.LIST>
													<EXPENSEALLOCATIONS.LIST></EXPENSEALLOCATIONS.LIST>
													</ALLINVENTORYENTRIES.LIST>';
//echo"----";print_R($quantity);

}
//echo"<pre>";
//print_R($ALLINVENTORYENTRIESITEM);exit;
if($dis_stk_inpro_id >0 )
{
	        $requestXMLITEMLIST = implode(' ', $ALLINVENTORYENTRIESITEM);
			       
		    $requestXML = $requestXMLSTART.$requestXMLITEMLIST.$requestXMLEND;
		    $first=array();
		   // echo"<pre>";
		   // print_R($requestXML);exit;
				
			$server = 'http://quytech14.ddns.net:8000/';
			$headers = array( "Content-type: text/xml" ,"Content-length: ".strlen($requestXML) ,"Connection: close" );
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $server);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_TIMEOUT, 100);
			curl_setopt($ch, CURLOPT_POST, true);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $requestXML);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			$data = curl_exec($ch);
			//echo"<pre>";
		
			
		     if(curl_errno($ch)){
		    echo json_encode(array('status'=>'Failed', 'data'=> print curl_error($ch)));

		    }else{

		    	   $p = xml_parser_create();
        xml_parse_into_struct($p, $data, $vals, $index);
       xml_parser_free($p);
		   if(isset($vals[$index['LASTVCHID'][0]]['value']) && $vals[$index['LASTVCHID'][0]]['value']>0) {

		   
        $attributeArray = array();
        $attributeArray['TALLYMASTERNO'] = $vals[$index['LASTVCHID'][0]]['value'];
        
        //$yu=$this->_dbUpdate2($attributeArray,'table_warehouse_stk_trn_dts', "transfer_id =".$transfer_id );  
        $this->_dbUpdate($attributeArray,'table_item_dis_stk_inprocess', " 	bsn_id IN(".$itembsnid.")" );   
//print_R($yu);exit;

      

            $first=array('status'=>'Pushed To Tally Successfully', 'data'=> " Pushed To Tally Successfully.", 'id'=>$vals[$index['LASTVCHID'][0]]['value']);

    } else {
      
      if(isset($vals[$index['LINEERROR'][0]]['value'])) 
        $message = $vals[$index['LINEERROR'][0]]['value'];
      else 
        $message = $vals[0]['value'];


         $first=array('status'=>$message, 'data'=> $message);
    }


    curl_close($ch);

      

               }

               } 


             $tallyretun=   json_encode(array("first"=>$first['data']));
              
             //print_R($tallyretun);
             $jsonAsObject   = json_decode($tallyretun);
             
             $fromwh=$jsonAsObject->first;










		// For loop end here
		 // fclose($file);	

		 // echo "<pre>";
		 // print_r($str_err_array);
		 // echo $row;
		 
		// exit;
			// fclose($file);	  
			  if($row<=0)
			   	return "no";
			   else 
			  //	return $str_err_array;
			  return array('str_err_array'=>$str_err_array,'tallymsg'=>$fromwh);


		} else { // Check all the parameters

			return array('status'=>'failed', 'msg'=>'Imput parameters missing, Please Select Warehouse, Distributor, Invoice date and number!');
		}


	
}


/******************************** Code for Saving Dispatches of Warehouse @Chirag 2017-09-04 **********************************/

function uploadWarehouseActualStockDispatchSummary() {

	//$fields = array('Warehouse Code*','Category Name*','Item Code*','BSN*', 'Batch No', 'Manufacture Date*', 'Warranty Period(In Months)*', 'ProRata(In Months)*', 'Grace Period(In Months)*');
	$fields = array('Item Code*','BSN*', 'Batch No', 'Manufacture Date*(YYYY-MM-DD)', 'Dispatch Date*(YYYY-MM-DD)' );

	$file = $_SESSION['dispatch_wrhs_info'];
	$strErr="";
	$resultset = "";
	$row=0;	

    $whCd = '';
	$catName= '';
	$itmCd	= '';
	$bsn	= '';
	$stkValue= '';
	$manfDate = '';
	$warrantyPeriod = '';
	$proRata = '';
	$gracePeriod = '';

	
	for($ij=0;$ij<sizeof($file);$ij++)
	{
			$data = array();
			$data2 = array();
			$data3 = array();
		  
			$bsn = $file[$ij]['bsn_no'];	
			$flag=true;	
			$BSNNO = mysql_real_escape_string(trim($bsn));
			$bsn = $BSNNO;
			// $whCd		     = trim($arrFile[0]);
			// $catName	     = trim($arrFile[1]);

			if(isset($bsn) && strlen($bsn)==17)
			{
				$item_code = substr($bsn, 0, 3);
				$model_code = substr($bsn,8,4);
				$item_details = $this->_getSelectList2('table_item','*','',' item_erp_code="'.$item_code.'" and model_code="'.$model_code.'"');
				$itmCd		     = $item_details[0]->item_code;
				$items_ids       = $item_details[0]->item_id;
				// $bsn	         = trim($arrFile[1]);
				// $batchNo	     = trim($arrFile[2]);

				// $manfDate        = date('Y-m-d', strtotime($arrFile[3]));
				// $dispatch_date   = date('Y-m-d', strtotime($arrFile[4]));
				$day = substr($bsn,3,1);
				$month = substr($bsn,4,1);
				$year = substr($bsn,5,1);
				$day_value = $this->_getSelectList2('table_bsn_logic_day','*','',' character_value="'.$day.'"');				

				$month_value = $this->_getSelectList2('table_bsn_logic_month','*','',' character_value="'.$month.'"');

				$year_value = $this->_getSelectList2('table_bsn_logic_year','*','',' character_value="'.$year.'"');

				$manfDate        = $year_value[0]->year."-".date('m',strtotime($month_value[0]->month))."-".str_pad($day_value[0]->day,2,'0',STR_PAD_LEFT);
				$dispatch_date   = date('Y-m-d');
				if(is_array($_SESSION['dispatch_wrhs_info']) && isset($_SESSION['warehouses_id']) && $_SESSION['warehouses_id']>0) {
				$warehouse_id = trim($_SESSION['warehouses_id']);



				if(empty($itmCd) || empty($bsn) || $bsn=="BSN*" || empty($manfDate)){
				
				// if(empty($whCd)){
				// $strErr .= "Error in Row".$row." ,".$itmCd." ,".$bsn." , ".$batchNo." , ".$manfDate.",  ".$fields[0]." shouldn't be empty  \n";
				// } elseif(empty($catName)){
				// $strErr .= "Error in Row".$row." ,".$itmCd." ,".$bsn." , ".$batchNo." , ".$manfDate.",".$fields[1]." shouldn't be empty  \n";
				// } else 
				if(empty($itmCd)){
				$strErr .= "Error in Row".$row." ,".$itmCd." ,".$bsn." , ".$batchNo." , ".$manfDate.", ".$dispatch_date.",".$fields[0]." not matching  \n";
				} elseif(empty($bsn)) {
				$strErr .= "Error in Row".$row." ,".$itmCd." ,".$bsn." , ".$batchNo." , ".$manfDate.", ".$dispatch_date.",".$fields[1]." not matching  \n";					
				} elseif(empty($manfDate)) {
				$strErr .= "Error in Row".$row." ,".$itmCd." ,".$bsn." , ".$batchNo." , ".$manfDate.", ".$dispatch_date.",".$fields[3]." not matching  \n";					
				}  elseif(empty($dispatch_date)) {
				 $strErr .= "Error in Row".$row." ,".$itmCd." ,".$bsn." , ".$batchNo." , ".$manfDate.",".$fields[4]." not matching  \n";					
				} 
				 //elseif(empty($proRata)) {
				// $strErr .= "Error in Row".$row." ,".$itmCd." ,".$bsn." , ".$batchNo." , ".$manfDate.",".$fields[6]." shouldn't be empty  \n";					
				// }  elseif(empty($gracePeriod)) {
				// $strErr .= "Error in Row".$row." ,".$itmCd." ,".$bsn." , ".$batchNo." , ".$manfDate.",".$fields[7]." shouldn't be empty  \n";					
				// } 
				$str_err_array=array($strErr);						
				$flag=false;
				} 
				


				
				// Check BSN already exists at any warehouse

				if($flag){
				$condi =" LOWER(BSN.bsn)='".mysql_real_escape_string(strtolower(trim($bsn)))."'";
				$resultset = $this->_getSelectList('table_warehouse_stock AS WS LEFT JOIN table_item_bsn AS BSN ON BSN.bsn_id = WS.bsn_id','WS.warehouse_id','',$condi);
					if(is_array($resultset) && !empty($resultset) && $resultset[0]->warehouse_id!= "") {
						$strErr .= "Error in Row".$row." ,".$itmCd." ,".$bsn." , ".$batchNo." , ".$manfDate." , Sorry this bsn already dispatched to a warehouse.\n";
						$str_err_array=array($strErr);	
						$flag=false;
					}
				}		



				
				// Check Warehouse Code exists in the system.
				if($flag){
				//$condi =" LOWER(warehouse_code)='".mysql_real_escape_string(strtolower(trim($whCd)))."'";
				$condi =" warehouse_id ='".$warehouse_id."'";
				$resultset = $this->_getSelectList('table_warehouse','warehouse_id','',$condi);
					if(!is_array($resultset) && empty($resultset) && $resultset[0]->warehouse_id == "") {
						$strErr .= "Error in Row".$row." ,".$itmCd." ,".$bsn." , ".$batchNo." , ".$manfDate.", ".$dispatch_date.", Warehouse Code doesn't exists in the system.\n";
						$str_err_array=array($strErr);	
						$flag=false;
					} else {
						$warehouse_id = $resultset[0]->warehouse_id;
					}
				}		
					

				// Check category exists in the system.
				// if($flag){
				// $condi =" LOWER(category_name)='".mysql_real_escape_string(strtolower($catName))."'";
				// $resultset = $this->_getSelectList('table_category','category_id','',$condi);
				// 	if(!is_array($resultset) && empty($resultset) && $resultset[0]->category_id == "") {
				// 			$strErr .= "Error in Row".$row." ,".$itmCd." ,".$bsn." , ".$batchNo." , ".$manfDate.", ,Category doesn't exists in the system  \n";	
				// 		$str_err_array=array($strErr);	
				// 		$flag=false;
				// 	} else {
				// 		$category_id = $resultset[0]->category_id;
				// 	}
				// }
					
				// Check item exists in the system.		

				if($flag){
				$condi ="  LOWER(item_code)='".mysql_real_escape_string(strtolower($itmCd))."'";
				$resultset = $this->_getSelectList('table_item','item_id, item_grace, item_warranty, item_prodata, category_id','',$condi);
					if(!is_array($resultset) && empty($resultset) && $resultset[0]->item_id == "") {
					$strErr .= "Error in Row".$row." ,".$itmCd." ,".$bsn." , ".$batchNo." , ".$manfDate.", ".$dispatch_date.", Item doesn't exists in this category or item code doesn't exists in the system. \n";	
						$str_err_array=array($strErr);	
						$flag=false;
						
					} else {
						$item_id = $resultset[0]->item_id;
						$category_id = $resultset[0]->category_id;
						$warrantyPeriod = $resultset[0]->item_warranty;
						$proRata = $resultset[0]->item_prodata;
						$gracePeriod = $resultset[0]->item_grace;
					}
				}
					
				// Check attribute(size,color) exists in the system.


				if($flag)
				{
					if(!(count($item_details)>0 && count($month_value)>0 && count($day_value)>0 && count($year_value)>0))
					{
						$strErr .= "Error in Row".$row.",".$itmCd." ,".$bsn." , ".$batchNo." , ".$manfDate.", ".$dispatch_date.", Invalid BSN Format. Please check the BSN No\n";		
						$str_err_array=array($strErr);	
						$flag=false;
					}
				}

				// Check BSN STOCK exists in the system.	

				// if($flag){  

				// if($bsn!="") { $condi =" LOWER(bsn) = '".mysql_real_escape_string(strtolower($bsn))."' AND BSN.item_id=".$item_id; } else { $condi =""; }
				
				// $resultset = $this->_getSelectList('table_item_bsn AS BSN','BSN.bsn_id','',$condi);
					
				// 	if(!is_array($resultset) && empty($resultset) && $resultset[0]->bsn_id == "") {
				// 		$strErr .= "Error in Row".$row.",".$itmCd." ,".$bsn." , ".$batchNo." , ".$manfDate.", Battery BSN doesn't exists in the system\n";		
				// 		$str_err_array=array($strErr);	
				// 		$flag=false;
				// 	} else { 
				// 		$bsn_id = $resultset[0]->bsn_id; 
				// 	}
				// }			



				// Check manufacturing date 


				if($flag){  
					// Date format should be as Month/Day/Year
					//$currentDate = strtotime(date('Y-m-d'));
					//echo $manfDate;
					//echo "<br>";
					//echo date("jS F, Y", strtotime("01/15/17")); // 15/01/17
					//$manufacturingDate = date('Y-m-d', strtotime($manfDate));
					//exit;

						if (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$manfDate))
					    {
					        $dateStaus = true;
					    }else{
					        $dateStaus = false;
					    }


					 //  $manfDate =  date('Y-m-d', strtotime($manfDate));

					if(date('Y-m-d', strtotime($manfDate)) == '1970-01-01' || strtotime(date('Y-m-d', strtotime($manfDate)))<= strtotime('1970-01-01') || $dateStaus == false) {

						$strErr .= "Error in Row".$row.",".$itmCd." ,".$bsn." , ".$batchNo." , ".$manfDate.", ".$dispatch_date.", Invalid battery manufacturing date. Date format should be in this format(YYYY-mm-dd)\n";		
						$str_err_array=array($strErr);	
						$flag=false;
					} 
				}			
				if($flag){  
					// Date format should be as Month/Day/Year
					//$currentDate = strtotime(date('Y-m-d'));
					//echo $manfDate;
					//echo "<br>";
					//echo date("jS F, Y", strtotime("01/15/17")); // 15/01/17
					//$manufacturingDate = date('Y-m-d', strtotime($manfDate));
					//exit;

						if (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$dispatch_date))
					    {
					        $dateStaus = true;
					    }else{
					        $dateStaus = false;
					    }

					 
					// $dispatch_date =  date('Y-m-d', strtotime($dispatch_date));

					if(date('Y-m-d', strtotime($dispatch_date)) == '1970-01-01' || strtotime($dispatch_date) <= strtotime('1970-01-01') || $dateStaus == false || strtotime($dispatch_date) > strtotime(date('Y-m-d'))) {

						$strErr .= "Error in Row".$row.",".$itmCd." ,".$bsn." , ".$batchNo." , ".$manfDate.", ".$dispatch_date.", Invalid dispatched date(Less than current date). Date format should be in this format(YYYY-mm-dd)\n";		
						$str_err_array=array($strErr);	
						$flag=false;
					} 
				}



				
				
				// Insert/Update data in the system			

				if($flag){	

					// 	bsn Master Data Entry Array 

					$resultset = $this->_getSelectList('table_item_bsn AS BSN ','BSN.bsn_id','', " BSN.bsn ='".$bsn."'");

					if(is_array($resultset) && isset($resultset[0]->bsn_id) && !empty($resultset[0]->bsn_id)) {

						$bsn_id = $resultset[0]->bsn_id;

					} else { 

						// Create new stock entry of a BSN
						$bsnArray = array();

						$bsnArray['account_id']        		= $_SESSION['accountId'];	
						$bsnArray['dispatch_warehouse_id']  = $warehouse_id;	
						$bsnArray['item_id']           		= $items_ids;
						$bsnArray['category_id']       		= $category_id;
						$bsnArray['batch_no']       		= $batchNo;
						$bsnArray['bsn']       				= $bsn;
						$bsnArray['stock_value']       		= 1;
						$bsnArray['manufacture_date']       = $manfDate;
						$bsnArray['warranty_period']       	= $warrantyPeriod;
						$bsnArray['warranty_prorata']       = $proRata;
						$bsnArray['warranty_grace_period']  = $gracePeriod;
						$bsnArray['warehouse_dispatch_date']= date('Y-m-d');
						$bsnArray['factory_dispatch_date']  = $dispatch_date;
						//$bsnArray['warranty_end_date']      = date('Y-m-d', strtotime("+".$warrantyPeriod." months", strtotime($manfDate)));
						$bsnArray['last_updated_date']	 	= date('Y-m-d');	
						$bsnArray['last_update_datetime']	= date('Y-m-d H:i:s');	
						$bsnArray['created_datetime']		= date('Y-m-d H:i:s');	
						$bsnArray['user_type']  			= $_SESSION['userLoginType'];
						$bsnArray['web_user_id']  			= $_SESSION['PepUpSalesUserId'];
						
						$bsnArray['status']					= 'A';
						
						$bsn_id = $this->_dbInsert($bsnArray,'table_item_bsn');  

					}

					// 	bsn lifecycle activity data array 

					$data = array();

					$data['account_id'] 			=  $_SESSION['accountId'];	
					$data['item_id'] 				=  $item_id;
					$data['bsn_id'] 		        =  $bsn_id;
					$data['bsn'] 		            =  $bsn;	
					$data['to_warehouse_id'] 		=  $warehouse_id;	
					$data['user_type'] 		        =  $_SESSION['userLoginType'];	
					$data['web_user_id'] 		    =  $_SESSION['PepUpSalesUserId'];	
					$data['created_datetime'] 		=  date('Y-m-d H:i:s');	
					$data['activity_type'] 		    =  'D'; // D- Dispatched, R-Return, C-Complain
					$data['status'] 		        =  'A';		

					$this->_dbInsert($data,'table_item_bsn_lifecycle_activity');  




					// 	Add New BSN Warehouse Stock 

					$whStock = array();

					$whStock['account_id'] 				=  $_SESSION['accountId'];	
					$whStock['warehouse_id'] 		    =  $warehouse_id;
					$whStock['item_id'] 				=  $item_id;
					$whStock['bsn_id'] 		        	=  $bsn_id;
					$whStock['stock_value']       		=  1;
					$whStock['dispatch_date']			=  $dispatch_date;
					$whStock['invoice_no']				=  $_SESSION['invoice_id'];
					$whStock['invoice_date']			=  $_SESSION['invoice_date'];
					$whStock['last_updated_date']	 	=  date('Y-m-d');	
					$whStock['last_update_datetime']	=  date('Y-m-d H:i:s');	
					$whStock['created_datetime']		=  date('Y-m-d H:i:s');	
					if($_SESSION['userLoginType']==7)
					{
						$whStock['transfer_type']			=  'W';		//W = When Warehouse Transferred the stock
						$whStock['status']		            =  'A';		//when warehouse uploading/importing its own existing stock @04-09-2017 Chirag
					}
					else
					{
						$whStock['transfer_type']			=  'F';		//F = When factory added the stock
						$whStock['status']		            =  'I';		//When company dispatching the stock to warehouse @04-09-2017 Chirag
					}

					$this->_dbInsert($whStock,'table_warehouse_stock');  
					

					}
					$row++;	
				}
			}
			else if(isset($bsn) && strlen($bsn)==10 && $_SESSION['userLoginType']==7)
			{

				$item_code = substr($bsn, 0, 2);
				$month = substr($bsn, 2, 2);
				$year = substr($bsn, 4, 1);
				$manfDate = "201".$year."-".$month."-01";		
				$data['account_id']        		= $_SESSION['accountId'];
				$item_details = $this->_getSelectList2('table_item','','',' item_erp_code_old="'.$item_code.'"');
				$itmCd		     = $item_details[0]->item_code;
				$items_ids = $item_details[0]->item_id;
				$dispatch_date   = date('Y-m-d');
				if(is_array($_SESSION['dispatch_wrhs_info']) && isset($_SESSION['warehouses_id']) && $_SESSION['warehouses_id']>0) {
				$warehouse_id = trim($_SESSION['warehouses_id']);



				if(empty($itmCd) || empty($bsn) || $bsn=="BSN*" || empty($manfDate)){
				
				// if(empty($whCd)){
				// $strErr .= "Error in Row".$row." ,".$itmCd." ,".$bsn." , ".$batchNo." , ".$manfDate.",  ".$fields[0]." shouldn't be empty  \n";
				// } elseif(empty($catName)){
				// $strErr .= "Error in Row".$row." ,".$itmCd." ,".$bsn." , ".$batchNo." , ".$manfDate.",".$fields[1]." shouldn't be empty  \n";
				// } else 
				if(empty($itmCd)){
				$strErr .= "Error in Row".$row." ,".$itmCd." ,".$bsn." , ".$batchNo." , ".$manfDate.", ".$dispatch_date.",".$fields[0]." not matching  \n";
				} elseif(empty($bsn)) {
				$strErr .= "Error in Row".$row." ,".$itmCd." ,".$bsn." , ".$batchNo." , ".$manfDate.", ".$dispatch_date.",".$fields[1]." not matching  \n";					
				} elseif(empty($manfDate)) {
				$strErr .= "Error in Row".$row." ,".$itmCd." ,".$bsn." , ".$batchNo." , ".$manfDate.", ".$dispatch_date.",".$fields[3]." not matching  \n";					
				}  elseif(empty($dispatch_date)) {
				 $strErr .= "Error in Row".$row." ,".$itmCd." ,".$bsn." , ".$batchNo." , ".$manfDate.",".$fields[4]." not matching  \n";					
				} 
				 //elseif(empty($proRata)) {
				// $strErr .= "Error in Row".$row." ,".$itmCd." ,".$bsn." , ".$batchNo." , ".$manfDate.",".$fields[6]." shouldn't be empty  \n";					
				// }  elseif(empty($gracePeriod)) {
				// $strErr .= "Error in Row".$row." ,".$itmCd." ,".$bsn." , ".$batchNo." , ".$manfDate.",".$fields[7]." shouldn't be empty  \n";					
				// } 
				$str_err_array=array($strErr);						
				$flag=false;
				} 
				


				
				// Check BSN already exists at any warehouse

				if($flag){
				$condi =" LOWER(BSN.bsn)='".mysql_real_escape_string(strtolower(trim($bsn)))."'";
				$resultset = $this->_getSelectList('table_warehouse_stock AS WS LEFT JOIN table_item_bsn AS BSN ON BSN.bsn_id = WS.bsn_id','WS.warehouse_id','',$condi);
					if(is_array($resultset) && !empty($resultset) && $resultset[0]->warehouse_id!= "") {
						$strErr .= "Error in Row".$row." ,".$itmCd." ,".$bsn." , ".$batchNo." , ".$manfDate." , Sorry this bsn already dispatched to a warehouse.\n";
						$str_err_array=array($strErr);	
						$flag=false;
					}
				}		



				
				// Check Warehouse Code exists in the system.
				if($flag){
				//$condi =" LOWER(warehouse_code)='".mysql_real_escape_string(strtolower(trim($whCd)))."'";
				$condi =" warehouse_id ='".$warehouse_id."'";
				$resultset = $this->_getSelectList('table_warehouse','warehouse_id','',$condi);
					if(!is_array($resultset) && empty($resultset) && $resultset[0]->warehouse_id == "") {
						$strErr .= "Error in Row".$row." ,".$itmCd." ,".$bsn." , ".$batchNo." , ".$manfDate.", ".$dispatch_date.", Warehouse Code doesn't exists in the system.\n";
						$str_err_array=array($strErr);	
						$flag=false;
					} else {
						$warehouse_id = $resultset[0]->warehouse_id;
					}
				}		
					
				if($flag){
				$condi ="  LOWER(item_code)='".mysql_real_escape_string(strtolower($itmCd))."'";
				$resultset = $this->_getSelectList('table_item','item_id, item_grace, item_warranty, item_prodata, category_id','',$condi);
					if(!is_array($resultset) && empty($resultset) && $resultset[0]->item_id == "") {
					$strErr .= "Error in Row".$row." ,".$itmCd." ,".$bsn." , ".$batchNo." , ".$manfDate.", ".$dispatch_date.", Item doesn't exists in this category or item code doesn't exists in the system. \n";	
						$str_err_array=array($strErr);	
						$flag=false;
						
					} else {
						$item_id = $resultset[0]->item_id;
						$category_id = $resultset[0]->category_id;
						$warrantyPeriod = $resultset[0]->item_warranty;
						$proRata = $resultset[0]->item_prodata;
						$gracePeriod = $resultset[0]->item_grace;
					}
				}
					
				// Check attribute(size,color) exists in the system.




				if($flag){  
					// Date format should be as Month/Day/Year
					//$currentDate = strtotime(date('Y-m-d'));
					//echo $manfDate;
					//echo "<br>";
					//echo date("jS F, Y", strtotime("01/15/17")); // 15/01/17
					//$manufacturingDate = date('Y-m-d', strtotime($manfDate));
					//exit;

						if (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$manfDate))
					    {
					        $dateStaus = true;
					    }else{
					        $dateStaus = false;
					    }


					 //  $manfDate =  date('Y-m-d', strtotime($manfDate));

					if(date('Y-m-d', strtotime($manfDate)) == '1970-01-01' || strtotime(date('Y-m-d', strtotime($manfDate)))<= strtotime('1970-01-01') || $dateStaus == false) {

						$strErr .= "Error in Row".$row.",".$itmCd." ,".$bsn." , ".$batchNo." , ".$manfDate.", ".$dispatch_date.", Invalid battery manufacturing date. Date format should be in this format(YYYY-mm-dd)\n";		
						$str_err_array=array($strErr);	
						$flag=false;
					} 
				}			
				if($flag){  
					// Date format should be as Month/Day/Year
					//$currentDate = strtotime(date('Y-m-d'));
					//echo $manfDate;
					//echo "<br>";
					//echo date("jS F, Y", strtotime("01/15/17")); // 15/01/17
					//$manufacturingDate = date('Y-m-d', strtotime($manfDate));
					//exit;

						if (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$dispatch_date))
					    {
					        $dateStaus = true;
					    }else{
					        $dateStaus = false;
					    }

					 
					// $dispatch_date =  date('Y-m-d', strtotime($dispatch_date));

					if(date('Y-m-d', strtotime($dispatch_date)) == '1970-01-01' || strtotime($dispatch_date) <= strtotime('1970-01-01') || $dateStaus == false || strtotime($dispatch_date) > strtotime(date('Y-m-d'))) {

						$strErr .= "Error in Row".$row.",".$itmCd." ,".$bsn." , ".$batchNo." , ".$manfDate.", ".$dispatch_date.", Invalid dispatched date(Less than current date). Date format should be in this format(YYYY-mm-dd)\n";		
						$str_err_array=array($strErr);	
						$flag=false;
					} 
				}



				
				
				// Insert/Update data in the system			

				if($flag){	

					// 	bsn Master Data Entry Array 

					$resultset = $this->_getSelectList('table_item_bsn AS BSN ','BSN.bsn_id','', " BSN.bsn ='".$bsn."'");

					if(is_array($resultset) && isset($resultset[0]->bsn_id) && !empty($resultset[0]->bsn_id)) {

						$bsn_id = $resultset[0]->bsn_id;

					} else { 

						// Create new stock entry of a BSN
						$bsnArray = array();

						$bsnArray['account_id']        		= $_SESSION['accountId'];	
						$bsnArray['dispatch_warehouse_id']  = $warehouse_id;	
						$bsnArray['item_id']           		= $items_ids;
						$bsnArray['category_id']       		= $category_id;
						$bsnArray['batch_no']       		= $batchNo;
						$bsnArray['bsn']       				= $bsn;
						$bsnArray['stock_value']       		= 1;
						$bsnArray['manufacture_date']       = $manfDate;
						$bsnArray['warranty_period']       	= $warrantyPeriod;
						$bsnArray['warranty_prorata']       = $proRata;
						$bsnArray['warranty_grace_period']  = $gracePeriod;
						$bsnArray['warehouse_dispatch_date']= date('Y-m-d');
						$bsnArray['factory_dispatch_date']  = $dispatch_date;
						//$bsnArray['warranty_end_date']      = date('Y-m-d', strtotime("+".$warrantyPeriod." months", strtotime($manfDate)));
						$bsnArray['last_updated_date']	 	= date('Y-m-d');	
						$bsnArray['last_update_datetime']	= date('Y-m-d H:i:s');	
						$bsnArray['created_datetime']		= date('Y-m-d H:i:s');	
						$bsnArray['user_type']  			= $_SESSION['userLoginType'];
						$bsnArray['web_user_id']  			= $_SESSION['PepUpSalesUserId'];
						
						$bsnArray['status']					= 'A';
						
						$bsn_id = $this->_dbInsert($bsnArray,'table_item_bsn');  

					}

					// 	bsn lifecycle activity data array 

					$data = array();

					$data['account_id'] 			=  $_SESSION['accountId'];	
					$data['item_id'] 				=  $item_id;
					$data['bsn_id'] 		        =  $bsn_id;
					$data['bsn'] 		            =  $bsn;	
					$data['to_warehouse_id'] 		=  $warehouse_id;	
					$data['user_type'] 		        =  $_SESSION['userLoginType'];	
					$data['web_user_id'] 		    =  $_SESSION['PepUpSalesUserId'];	
					$data['created_datetime'] 		=  date('Y-m-d H:i:s');	
					$data['activity_type'] 		    =  'D'; // D- Dispatched, R-Return, C-Complain
					$data['status'] 		        =  'A';		

					$this->_dbInsert($data,'table_item_bsn_lifecycle_activity');  




					// 	Add New BSN Warehouse Stock 

					$whStock = array();

					$whStock['account_id'] 				=  $_SESSION['accountId'];	
					$whStock['warehouse_id'] 		    =  $warehouse_id;
					$whStock['item_id'] 				=  $item_id;
					$whStock['bsn_id'] 		        	=  $bsn_id;
					$whStock['stock_value']       		=  1;
					$whStock['dispatch_date']			=  $dispatch_date;
					$whStock['last_updated_date']	 	=  date('Y-m-d');	
					$whStock['last_update_datetime']	=  date('Y-m-d H:i:s');	
					$whStock['created_datetime']		=  date('Y-m-d H:i:s');	
					if($_SESSION['userLoginType']==7)
					{
						$whStock['status']		            =  'A';		//when warehouse uploading/importing its own existing stock @04-09-2017 Chirag
					}
					else
					{
						$whStock['status']		            =  'I';		//When company dispatching the stock to warehouse @04-09-2017 Chirag
					}

					$this->_dbInsert($whStock,'table_warehouse_stock');  
					

					}
					$row++;	
				}
			
			}
			else if(isset($bsn) && strlen($bsn)==10 && $_SESSION['userLoginType']!=7)
			{
				$strErr .= "Error in Row".$row.",".$itmCd." ,".$bsn." , ".$batchNo." , ".$manfDate.", ".$dispatch_date.", Please dispatch the new BSN only.\n";		
						$str_err_array=array($strErr);	
						$flag=false;
			}
			

	
	  	   
	}		
		
	fclose($file);	

	// print_r($str_err_array);

	// exit;  
	  if($row<=0)
	   	return "no";
	   else 
	  	return $str_err_array;
	}	





	
/******************************** Code for Saving Dispatches of Warehouse @Chirag 2017-09-04 **********************************/


} // End of class

?>
