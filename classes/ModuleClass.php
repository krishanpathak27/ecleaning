<?php 
class ModuleClass extends Db_Action {

	
	public function __construct() {
		
		parent::__construct();
		$feature = array();
		$featureList = array();
	
	}
	
	
	public function getEnableModuleList(){
	
		$feature =  $this->_getSelectList('table_module_enable AS ME 
			LEFT JOIN table_modules AS M ON M.feature_id = ME.feature_id', 'ME.feature_id','',
						" ME.status = 1 AND M.status = 1");
		
		if(is_array($feature)) {
			$featureList = $this->returnObjectArrayToIndexArray($feature,'feature_id');
			return $featureList;
		} else {
		
			return $featureList;
		
		}
	}
	
	
	public function getEnableModuleListCustomParam($paramList){
	
		$feature =  $this->_getSelectList2('table_module_enable AS ME 
			LEFT JOIN table_modules AS M ON M.feature_id = ME.feature_id', 'ME.feature_id','',
						" ME.account_id=".$paramList['account_id']." AND ME.status = 1 AND M.status = 1");
		
		if(is_array($feature)) {
			$featureList = $this->returnObjectArrayToIndexArray($feature,'feature_id');
			return $featureList;
		} else {
		
			return $featureList;
		
		}
	}	
	
	
	
	
	
	
	// Used by webservice class for activity sales web service
	public function getEnableModuleListUseByWebService(){
	
		$salUserType=$this->_getSelectList2('table_salesman', 'account_id',''," salesman_id='".$_REQUEST['salesman_id']."' and session_id='".$_REQUEST['session_id']."'");
		$account_id = $salUserType[0]->account_id;
		
		$feature =  $this->_getSelectList2('table_module_enable AS ME LEFT JOIN table_modules AS M ON M.feature_id = ME.feature_id','ME.feature_id',''," ME.account_id='".$account_id."' and ME.status = 1 AND M.status = 1");
		
		if(is_array($feature)) {
			$featureList = $this->returnObjectArrayToIndexArray($feature,'feature_id');
			return $featureList;
		} else {
		
			return $featureList;
		
		}
	}

	
	
	public function getFeatureModulel() {
  	
	$feature = $this->_getSelectList("table_feature_map as tfm left join table_feature as tf on tfm.feature_id=tf.feature_id",'tfm.feature_id','','tfm.status="A" and tf.status="A"');
	  if(is_array($feature)) 
		  $featureList = $this->returnObjectArrayToIndexArray($feature,'feature_id');
	   return $featureList;
  	}


}
?>