<?php 

/********************************************************************************************************

	* FileName: class.pager.php

	* Package: Class		

	* Purpose: Class File is used for manage the pagination of slected records.



/*******************************************************************************************************/ 



//



class Pager 

{ 

	function __construct($varFunName='frmsubmit')

	{

		$this->funName=$varFunName;

	}



   function findStart($limit) 

    {	

     if ((!isset($_REQUEST['page'])) || ($_REQUEST['page'] == "1") || ($_REQUEST['page'] <= 0)) 

      { 

        $start = 0; 

        $_REQUEST['page'] = 1; 

      } 

     else 

      {

		  if($_REQUEST['page']=='') 

		  {

			$start=0;

		  }

		  else

		  {

		   $start = ($_REQUEST['page']-1) * $limit; 

		  }

      }	

     return $start; 

    } 



   function findPages($count, $limit) 

    { 

      $pages = (($count % $limit) == 0) ? $count / $limit : floor($count / $limit) + 1 ; 

     return $pages; 

    } 



   function pageList($curpage, $pages) 

   {   		

		$flag=true;				

		if($curpage=='' || $curpage==1) {

			 $curpage=1;

			 $j=$curpage+9;

			 $k= $curpage;	

			 $_SESSION['nxt']=$j;

			 $_SESSION['prev']=$k;

		}		 

		if($curpage>$_SESSION['nxt']){

			$_SESSION['nxt']=$curpage;

			$_SESSION['prev']=$curpage-9;

			$j=$_SESSION['nxt'];

			$k=$_SESSION['prev'];

			$flag=false;

		} elseif($curpage<$_SESSION['prev']){

			$_SESSION['nxt']=$curpage+9;

			$_SESSION['prev']=$curpage;

			$j=$_SESSION['nxt'];

			$k=$_SESSION['prev'];

			$flag=false;

		} elseif($curpage==$pages)

		{			

			$_SESSION['nxt']=$pages;

			$_SESSION['prev']=$pages-9;

			$j=$_SESSION['nxt'];

			$k=$_SESSION['prev'];

			$flag=false;

		}

		$page_list  = ""; 

		 if (($curpage != 1) && ($curpage)) 

		 { 

			$page_list .= "  <a class='pagi'  href=\"javascript:".$this->funName."(1);\" title=\"First Page\">&laquo;</a> ";

	

		 }

		

		 if (($curpage-1) > 0) 

		 { 

		   $page_list .= "<a class='pagi'  href=\"javascript:".$this->funName."(".($_SESSION['prev']-1).");\" title=\"Previous Page\">&#8249;</a> ";

		 } 

		 

		 if($curpage%$_SESSION['nxt']==0)

		 {

			if($flag) {

				$j=$curpage+9;

				$k= $curpage;	

				$_SESSION['nxt']=$j;

				$_SESSION['prev']=$k;

			}

		 } else {

			$j=$_SESSION['nxt'];

			$k=$_SESSION['prev'];

		 }

		

		//echo $k." cur ".$j;

		 for ($i=1; $i<=$pages; $i++) 

		 {

			if($i>=$k && $i<=$j) { 

			   if ($i == $curpage) 

				{ 

				 $page_list .= "<b>".$i."</b>"; 

				} 

			   else 

				{ 

				 $page_list .= "<a class='pagi'  href=\"javascript:".$this->funName."(".$i.");\" title=\"Page ".$i."\">".$i."</a>";

				} 

			   $page_list .= " "; 

			  // $k=$i;

			 }

		 }   

		 if (($curpage+10) <= $pages) 

		  { 

		   $page_list .= "<a class='pagi'  href=\"javascript:".$this->funName."(".($_SESSION['nxt']+1).");\" title=\"Next Page\">&#8250;</a> ";

		  } 

	

		 if (($curpage != $pages) && ($pages != 0)) 

		  { 

		   $page_list .= "<a class='pagi'  href=\"javascript:".$this->funName."(".$pages.");\" title=\"Last Page\">&raquo;</a> "; 

		  } 

		 $page_list .= "\n"; 

    	 return $page_list; 

    } 

 

   function nextPrev($curpage, $pages) 

    { 

     $next_prev  = ""; 



     if (($curpage-10) <= 0) 

      { 

       $next_prev .= "Previous"; 

      } 

     else 

      { 

       $next_prev .= "<a class='pagi'  href=\"".$_SERVER['PHP_SELF']."?page=".($curpage-1)."\">Previous</a>"; 

      } 



     $next_prev .= " | "; 



     if (($curpage+10) > $pages) 

      { 

       $next_prev .= "Next"; 

      } 

     else 

      { 

       $next_prev .= "<a class='pagi'  href=\"".$_SERVER['PHP_SELF']."?page=".($curpage+1)."\">Next</a>"; 

      } 



     return $next_prev; 

    } 

  } 

?>