<?php
 
class PepupsalesFactory 
{
  public static function build($class)
  {

    $controller = ucwords($class)."Controller";
    if(class_exists($controller))
    {
      return new $controller();
    }
    else {
      throw new Exception("Invalid class name given.");
    }
  }
  
}