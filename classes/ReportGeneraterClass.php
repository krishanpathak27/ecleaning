<?php Class ReportGeneraterClass extends DB_Action implements DefaultInterface {

	var $result;
	var $data;	
	var $getData;
	var $type; 
	var $condition;
	public $paramlist; 
	public $dataList;
    public $IDarray;
	public $IDlist;

	public function __construct() {
	
		//$this = new Db_Action();
		
		parent::__construct();
		$this->fetaurearray = array();
		$this->IDarray = array();
		$this->IDlist = array();
		$this->aSal = array();
		$this->condition = '';
		$this->distributorSalesmen = array();
		$this->thisYear = range(date('Y'), 2013);
		$this->ARR_MONTHS = array('01'=>'Jan','02'=>'Feb','03'=>'Mar','04'=>'Apr','05'=>'May','06'=>'Jun','07'=>'Jul','08'=>'Aug','09'=>'Sep','10'=>'Oct','11'=>'Nov','12'=>'Dec');
		$this->month_names = array("January","February","March","April","May","June","July","August","September","October","November","December");

	
	}
	  
	  
	 public function getUserType($selectedType) {
	 
	 $_objArrayList = new ArrayList();
	 
	 $userType = $_objArrayList->checkUserType();
	 
	 if(isset($userType) && $userType>1) $cond = " AND id=".$userType; 
	  	
	  	$result = $this->_getSelectList2('table_user_type',"type, status, id",''," status='A' $cond AND account_id<=0");
		if(is_array($result)){
			$data.= '<option value="">Please Select</option>';
			foreach($result as $value):
				if($value->id == $selectedType){ $selected = "selected"; } else{ $selected = ""; }
				$data.="<option value=".$value->id." $selected>".$value->type."</option>";
			endforeach;
		} else {
			$data = '<option value="">No Record</option>';
		}
	  	return $data;
	 }
	 
	 
	 public function createOptionMenu($getData, $key, $attribute, $default, $selectedValue) {
	 	
		$data = '';
	 	if(is_array($getData)){
			if($default == 'single') $data.= '<option value="">Please Select</option>';
			else if($default == 'optgroup') $data.= '<optgroup label="Please Select"></optgroup>'; 
			foreach($getData as $value):
				if(is_array($selectedValue)) {
					if(in_array($value->$key, $selectedValue)){  $selected = "selected"; } else { $selected = ""; }
				} else {
					if($value->$key == $selectedValue){  $selected = "selected"; } else { $selected = ""; }
				}
				$data.="<option value=".$value->$key." $selected>".$value->$attribute."</option>";
			endforeach;
		} else {
			$data = '<option value="">No Record</option>';
			//$data = '<optgroup label="No Record"></optgroup>';
		}
	  	return $data;
	 }
	 
	 
	 
	 
 	public function createCheckboxMenu($getData, $key, $attribute, $default, $selectedValue) {
	 	$selectedValue = explode(',',$selectedValue);
		$data.= '<ul class="checkboxListStyle">';
	 	if(is_array($getData)){
			//if($default == 'checkall') $data.= '<div class="optlegend"><input type="checkbox" id="checkall" name="checkall" value="" />Checkall</div>';
			foreach($getData as $value):
				if(is_array($selectedValue)) {
					if(in_array($value->$key, $selectedValue)){  $checked = "checked"; } else { $checked = ""; }
				} else {
					if($value->$key == $selectedValue){  $checked = "checked"; } else { $checked = ""; }
				}
				$data.='<li><input type="checkbox" name="report_id[]" value='.$value->$key.' '.$checked.' />'.$value->$attribute.'</li>';
			endforeach;
		} else {
			$data = '<li>No Report</li>';
		}
		$data.='</ul>';
	  	return $data;
	 }
	 
	 
	 
	 
	 
	 
	 
	 public function getUserTypeList($type,$optiontype, $selectedValue) {
		
		switch ($type) {
		
			// If User Type SUPER ADMIN
			case 0 : 
				
				$list = $this->_getSelectList('table_web_users AS U LEFT JOIN table_account_admin
 AS SA ON SA.operator_id = U.operator_id ', 'SA.operator_name,SA.operator_id,SA.account_id',''," SA.status='A' AND SA.end_date>=".date('Y-m-d')." ORDER BY SA.operator_name");
				break;
			
			// If User Type Account
			case 1 : 
				$result = $this->_getSelectList('table_web_users AS U LEFT JOIN table_account
 AS A ON A.account_id = U.account_id ', 'A.company_name,A.account_id',''," A.status='A' AND U.user_type = ".$type." AND A.end_date>=".date('Y-m-d')." ORDER BY A.company_name");	
 
 				$list = $this->createOptionMenu($result,'account_id','company_name', $optiontype,$selectedValue);
				
				break;
				// If User Type Company User
			case 2 : 
			
				$result = $this->_getSelectList('table_web_users AS U LEFT JOIN table_account_admin
 AS A ON A.operator_id = U.operator_id ', 'A.operator_name,A.account_id',''," A.status='A' AND U.user_type = ".$type." AND A.end_date>=".date('Y-m-d')." ORDER BY A.operator_name");	
 
 				$list = $this->createOptionMenu($result,'account_id','company_name', $optiontype,$selectedValue);
			
			
				break;
				// If User Type Distributor
			case 3 : 
				$result = $this->_getSelectList('table_distributors AS D LEFT JOIN table_web_users
 AS U ON U.distributor_id = D.distributor_id ', 'D.distributor_name, D.distributor_id',''," D.status='A' ORDER BY D.distributor_name");	
 
 				$list = $this->createOptionMenu($result,'distributor_id','distributor_name', $optiontype,$selectedValue);
 
				break;
				// If User Type Retailer
			case 4 : 
				$result = $this->_getSelectList('table_retailer AS R LEFT JOIN table_web_users
 AS U ON U.retailer_id = R.retailer_id ', 'R.retailer_name, R.retailer_id',''," R.status='A' ORDER BY R.retailer_name");
 				
				$list = $this->createOptionMenu($result,'retailer_id','retailer_name', $optiontype,$selectedValue);
 					
				break;
				// If User Type Salesman
			case 5 : 
				
				// Call the ModuleClass method   
				$_objArrayList = new ArrayList();
				$salsList = $_objArrayList->SalesmanArrayList();
				$list = $_objArrayList->GetSalesmenMenu($salsList, $selectedValue, 'root');
				break;
				// If User Type QA
			case 6 : 
				$result = $this->_getSelectList('table_quality_checker AS Q LEFT JOIN table_web_users
 AS U ON U.qchecker_id = Q.qchecker_id ', 'Q.qchecker_name, Q.qchecker_id',''," Q.status='A' ORDER BY Q.qchecker_name");
 				
				$list = $this->createOptionMenu($result,'qchecker_id','qchecker_name',$optiontype,$selectedValue);
				
				break;
			 default:
       			$list = $this->createOptionMenu();
				break;
		} // End of Switch case
		
		
		// Check array isn't empty
		if(is_array($list)){
			// Return arraylist
			return  $list;
			
		} else {
			return  $list;
		
		}
	}

	
	public function getReportsList($type,$optiontype,$selectedValue) {
		
		if(isset($type)) {
		$result = $this->_getSelectList('table_navigation_privilege AS P', 'P.page_privileges',''," user_type=".$type);
		if(is_array($result)) $data = $this->_getSelectList2('table_cms AS C', 'C.id, C.page_alias_name',''," C.status = 'A' AND entity_name = 'R' AND page_name<>'' AND id IN (".$result[0]->page_privileges.")");
			$list = $this->createCheckboxMenu($data,'id','page_alias_name',$optiontype,$selectedValue);
		} else {
			$list = $this->createOptionMenu();
		}
		return  $list;
	}
	
	
	
	
	
	

	
	public function UserArrayList($paramList) {
	
		$type = $paramList['user_type'];
		$user_id = $paramList['user_id'];
		$account_id = $paramList['account_id'];
		
		if($type!="" && $user_id!="" && $account_id!="") {
		
		switch ($type) {

		case 0 : // If User Type SUPER ADMIN
				
		$dataList = $this->_getSelectList2('table_web_users AS U LEFT JOIN table_account_admin
 AS SA ON SA.operator_id = U.operator_id ', 'SA.operator_name,SA.operator_id,SA.account_id',''," SA.status='A' AND SA.end_date>=".date('Y-m-d')." ORDER BY SA.operator_name");
		break;
			
			
		case 1 : // If User Type Company Account
		$result = $this->_getSelectList2('table_web_users AS U LEFT JOIN table_account
 AS A ON A.account_id = U.account_id ', 'A.company_name,A.account_id, U.email_id, U.user_type',''," A.account_id=".$account_id." AND A.status='A' AND U.user_type = ".$type." AND A.end_date>=".date('Y-m-d')." ORDER BY A.company_name");
 	
 		$dataList = $this->returnObjectArrayToIndexArray($result, 'account_id');
	    $dataList = array_merge(array('Company'=>array('account_id'=>$dataList,'condition'=>'')),array('detail'=>array('type'=>1,'id'=>$result[0]->account_id,'account_id'=>$result[0]->account_id,'name'=>$result[0]->company_name,'email'=>$result[0]->email_id,'email2'=>'','email3'=>''))); 
		
		break;
				
		case 2 : // If User Type Company User
			
		$dataList = $this->_getSelectList2('table_web_users AS U LEFT JOIN table_account_admin
 AS A ON A.operator_id = U.operator_id ', 'A.operator_name,A.account_id',''," A.account_id=".$account_id." AND A.status='A' AND U.user_type = ".$type." AND A.end_date>=".date('Y-m-d')." ORDER BY A.operator_name");	
		break;
				
		case 3 : // If User Type Distributor
		$result = $this->_getSelectList2('table_distributors AS D LEFT JOIN table_web_users
 AS U ON U.distributor_id = D.distributor_id ', ' U.user_type, D.account_id, D.distributor_id, D.distributor_name, D.distributor_email, D.distributor_email2, D.distributor_email3',''," D.account_id=".$account_id."  AND D.distributor_id = ".$user_id." AND D.status='A' ORDER BY D.distributor_name");	
 
 		$dataList = $this->returnObjectArrayToIndexArray($result, 'distributor_id');
		
			if(count($dataList)>0)
				$this->condition = $this->getSalesCondition('D', 'distributor_id', $dataList);
		
		$dataList = array_merge(array('distributor'=>array('distributor_id'=>$dataList, 'condition'=>$this->condition)),array('detail'=>array('type'=>3,'id'=>$result[0]->distributor_id,'account_id'=>$result[0]->account_id,'name'=>$result[0]->distributor_name,'email'=>$result[0]->distributor_email,'email2'=>$result[0]->distributor_email2,'email3'=>$result[0]->distributor_email3)));
		
		break;
				
		case 4 : // If User Type Retailer
		$result = $this->_getSelectList2('table_retailer AS R LEFT JOIN table_web_users
 AS U ON U.retailer_id = R.retailer_id ', 'R.retailer_name,R.account_id, U.user_type, R.retailer_id, R.retailer_email, R.retailer_email2',''," R.account_id=".$account_id." AND R.retailer_id = ".$user_id." AND R.status='A' ORDER BY R.retailer_name");
 
 		$dataList = $this->returnObjectArrayToIndexArray($result, 'retailer_id');
		
			if(count($dataList)>0)
				$this->condition = $this->getSalesCondition('R', 'retailer_id', $dataList);
		
		
		$dataList = array_merge(array('retailer'=>array('retailer_id'=>$dataList, 'condition'=>$this->condition)),array('detail'=>array('type'=>4,'id'=>$result[0]->retailer_id,'account_id'=>$result[0]->account_id,'name'=>$result[0]->retailer_name,'email'=>$result[0]->retailer_email,'email2'=>$result[0]->retailer_email2,'email3'=>'')));
 
		break;
				
		case 5 : // If User Type Salesman
			$_objArrayList = new ArrayList(); // ArrayList object created
			$_objModuleClass = new ModuleClass(); // Create ModuleClass Object 
			$fetaurearray = $_objModuleClass->getEnableModuleListCustomParam($paramList); // Get feature list enabled for this account
			if(in_array(1,$fetaurearray)){ // Check hierarchy(1) Module exists into this array
			
				$hierachyLevel = $this->getSalesmanHierarchyLevel($paramList);
				$this->IDarray = array();	
				$dataList = $this->getSalesbottomhierarchy(array($user_id), $hierachyLevel);
				
			} else { // Get the salesman details
				$dataList = array($user_id);
			}
			
			//print_r($dataList);
			$result = $this->getSalesmanFullDetails($user_id);
			
			if(count($dataList)>0)
				$this->condition = $this->getSalesCondition('S', 'salesman_id', $dataList);
			
		$dataList = array_merge(array('salesman'=>array('salesman_id'=>$dataList, 'condition'=>$this->condition)),array('detail'=>array('type'=>5,'id'=>$result[0]->salesman_id,'account_id'=>$result[0]->account_id,'name'=>$result[0]->salesman_name,'email'=>$result[0]->email_id,'email2'=>'','email3'=>'')));
			
		break;
				
		case 6 : // If User Type QA
		$result = $this->_getSelectList2('table_quality_checker AS Q LEFT JOIN table_web_users
 AS U ON U.qchecker_id = Q.qchecker_id ', 'Q.qchecker_name,Q.user_type,Q.qchecker_id, U.email_id',''," Q.account_id=".$account_id." AND Q.status='A' ORDER BY Q.qchecker_name");
 
 		$dataList = $this->returnObjectArrayToIndexArray($result, 'qchecker_id');
		$dataList = array_merge(array('quality'=>array('qchecker_id'=>$dataList)),array('detail'=>array('type'=>6,'id'=>$result[0]->qchecker_id,'account_id'=>$result[0]->account_id,'name'=>$result[0]->qchecker_name,'email'=>$result[0]->email_id,'email2'=>'','email3'=>'')));
		
		break;
			
		default:
       	$dataList = array();
		break;
	} // End of Switch case
		
		
	}
		// Check array isn't empty
		if(is_array($dataList)){
			return  $dataList;
		} else {
			return  $dataList;
		
		}
	}
	
	
	public function getSalesCondition($tableAlias, $key, $salsList) {
	
		 if(is_array($salsList) && !empty($salsList) && $salsList[0]!='') {
		   
		   	$Ids = implode(',', array_unique($salsList));
		   		$condition =" AND ".$tableAlias.".".$key." IN ($Ids)";
			return $condition;
		} else {
				$condition =" AND ".$tableAlias.".".$key." IS NULL";
			return $condition;
		}
	}
	
	
	
	public function getSalesmanHierarchyLevel($paramList) {
	
		$getSortOrder = $this->_getSelectList2('table_salesman AS s  
		LEFT JOIN table_salesman_hierarchy_relationship AS SH ON SH.salesman_id = s.salesman_id 
		LEFT JOIN table_salesman_hierarchy AS H ON H.hierarchy_id = SH.hierarchy_id ', 
		'H.sort_order',''," s.account_id = ".$paramList['account_id']." AND s.salesman_id = ".$paramList['user_id']." ORDER BY s.salesman_name");
		
		if(is_array($getSortOrder) && isset($getSortOrder[0]->sort_order)) 
			return $getSortOrder[0]->sort_order; else return $sort_order = false;
							
	}
	
	
	public function getSalesbottomhierarchy($salID, $sortOrder) {
		/*echo "<pre>";
		echo "Request :";
		echo "<br>";
		print_r($salID);*/
		if($salID[0]!='') 
		{
				$this->IDarray = array_merge($this->IDarray, $salID);
				$Ids = implode(',', $salID);
				
				if(isset($sortOrder) && $sortOrder!=0 && $sortOrder!=''){  $sort = "AND H.sort_order >= ".$sortOrder; }
				

				$aSal=$this->_getSelectList2('table_salesman AS s  LEFT JOIN table_salesman_hierarchy_relationship AS SH ON SH.salesman_id = s.salesman_id LEFT JOIN table_salesman_hierarchy AS H ON H.hierarchy_id = SH.hierarchy_id ', 's.salesman_id','',
						" SH.rpt_user_id IN ($Ids) $sort ORDER BY s.salesman_name");
						
					if(is_object($aSal) || is_array($aSal)) 
					{
						$IDlist = $this->returnObjectArrayToIndexArray($aSal, 'salesman_id');
						/*echo "Result :";
						echo "<br>";
						print_r($IDlist);*/
						//$this->IDarray = array_merge($this->IDarray, $IDlist);
						
						//print_r($this->IDarray);
						return $this->getSalesbottomhierarchy($IDlist, $sortOrder);
					}  
					
			} 
			return 	$this->IDarray;
		}

}?>