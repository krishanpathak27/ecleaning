<?php 

	class SchemeClass extends Db_Action {


	public function __construct() {

		parent::__construct();

		$this->_DATE = date('Y-m-d');

	}




	  	/***********************************************************
	  	* Desc : get the scheme details with discounts
	  	* Author : AJAY
	  	* Created : 8th June 2015
	  	* Discount Types : 1 = Percentage, 2 = Amount, 3 = FOC
	  	* type : 1 = Normal, 2 = Free Item
	  	*/



		public function getItemSchemeDetail ($discount_id, $item_qty, $item_price) {

		$schemeDiscountDetails = array();
		$itemSchemeResultSet = array();
		$discount_amount = 0;
		$foc_id = 0;

		if(isset($discount_id) && !empty($discount_id)) {

		$itemSchemeResultSet = $this->_getSelectList('table_discount AS D
			LEFT JOIN table_discount_detail AS DD ON DD.discount_id = D.discount_id',
			"D.discount_id, D.discount, D.party_type, D.item_type, D.mode, D.status, D.start_date, D.end_date, DD.*",''," D.discount_id =".$discount_id);

			if(is_array($itemSchemeResultSet) && sizeof($itemSchemeResultSet)>0) {

				/*echo "<pre>";
				print_r($itemSchemeResultSet);
				exit;*/

				foreach ($itemSchemeResultSet as $key => $value) {

					// Check Discount Type of this scheme : 1 = Percentage, 2 = Amount, 3 = FOC

					switch ($value->discount_type) {
						
						case 1 : // check for percentage

							$discount_amount = ($item_qty * $item_price * $value->discount_percentage)/100;


							$schemeDiscountDetails = array('discount_id'=>$discount_id, 'discount_type'=>$value->discount_type, 'discount_desc'=>$value->discount_desc, 'type'=>1, 'discount_percentage'=>$value->discount_percentage, 'discount_amount'=>$discount_amount, 'free_item_id'=>NULL, 'total_free_quantity'=>NULL);
							//print_r($schemeDiscountDetails);
							//exit;

							break;

						case 2 : // check for Amount

							// check if requested quantity of item > then minimun quantity of scheme

							if($item_qty > $value->minimum_quantity) {

								$remainder =  $item_qty % $value->minimum_quantity;
								$discount_amount = 	  (($item_qty - $remainder) / $value->minimum_quantity) * $value->discount_amount;

							} else {

								$discount_amount = $value->discount_amount;
							}
							

							$schemeDiscountDetails = array('discount_id'=>$discount_id, 'discount_type'=>$value->discount_type, 'discount_desc'=>$value->discount_desc, 'type'=>1, 'discount_percentage'=>NULL, 'discount_amount'=>$discount_amount, 'free_item_id'=>NULL, 'total_free_quantity'=>NULL);


							break;

						case 3 : // check for FOC

							$foc_id = $value->foc_id;
							$free_item_id = NULL;
							$total_free_quantity = NULL;

							$focDetail = $this->_getSelectList2('table_foc_detail AS FOC',"FOC.free_item_id, FOC.free_qty",''," FOC.foc_detail_id =".$value->foc_id);

								if(sizeof($focDetail)>0) {

									$free_item_id = $focDetail[0]->free_item_id;
									$total_free_quantity = $focDetail[0]->free_qty;

									// check if requested quantity of item > then minimun quantity of scheme

									if($item_qty > $value->minimum_quantity) {

										$remainder =  $item_qty % $value->minimum_quantity;
										$total_free_quantity = 	  (($item_qty - $remainder) / $value->minimum_quantity) * $total_free_quantity;

									} 
								}

								$schemeDiscountDetails = array('discount_id'=>$discount_id, 'discount_type'=>$value->discount_type, 'discount_desc'=>$value->discount_desc, 'type'=>2, 'discount_percentage'=>NULL, 'discount_amount'=>NULL, 'free_item_id'=>$free_item_id, 'total_free_quantity'=>$total_free_quantity);

							break;		

						default:
							# code...
							break;
					}

				} // End foreach
				
			} // check discount details exists or not

		} // check discount id should not be empty

		return $schemeDiscountDetails;

	} // End of the function





	  	/***********************************************************
	  	* Desc : check party type of requested retailer for that discount Id
	  	* Author : AJAY
	  	* Created : 7th June 2015
	  	* Party Types : 1 = All, 2 = state, 3 = city, 4 = Retailer 
	  	*
	  	*/


	  	function checkPartyType($retailer_id, $discount_id, $party_type) {



	  	$state_id = 0;
	  	$city_id = 0;

	  	$retailerDetails = $this->_getSelectList('table_retailer AS R', "R.retailer_id, R.city, R.state",''," retailer_id = ".$retailer_id." AND status = 'A'");


	  	$status = false;

	  	if(sizeof($retailerDetails)>0) {

	  		$state_id = $retailerDetails->state;
	  		$city_id = $retailerDetails->city;

		  	switch ($party_type) {

		  		case '2': // Check State of selected retailer for the scheme

		  			$partyTypeResult = $this->_getSelectList2('table_discount_party AS DP',"DP.state_id",''," DP.discount_id ='".$discount_id."' AND DP.state_id = '".$state_id."'");

		  			if(sizeof($partyTypeResult) > 0 ) {

		  				$status = true;
		  			}

		  			# code...
		  			break;


				case '3': // Check City of selected retailer for the scheme

		  			$partyTypeResult = $this->_getSelectList2('table_discount_party AS DP',"DP.city_id",''," DP.discount_id ='".$discount_id."' AND DP.city_id = '".$city_id."'");

		  			if(sizeof($partyTypeResult) > 0 ) {

		  				$status = true;
		  			}

		  			# code...
		  			break;	  	


				case '4': // Check this scheme is for this retailer or not




				/***********************************************************************************************
				* DESC : Check distributor discount
				* Author : AJAY
				* Created : 17 July 2015
				*
				**/	

				//echo $discount_id;
				//echo $retailer_id;
				//exit;

					$flag = false;

					$flag = $this->checkDistributorSchemeMappedRetailer ($discount_id, $retailer_id) ;



					if($flag == false ) {

			  			$partyTypeResult = $this->_getSelectList2('table_discount_party AS DP',"DP.retailer_id",''," DP.discount_id ='".$discount_id."' AND DP.retailer_id = '".$retailer_id."'");

			  			//print_r($partyTypeResult);

			  			if(sizeof($partyTypeResult) > 0 ) {

			  				$status = true;
			  			}	

		  			}	else {

		  				$status = true;
		  			}		


		  			# code...
		  			break;	  		  					
		  		
		  		default:

		  			$status = true;
		  			# code...
		  			break;
		  	} // End of switch case

	  	}	// End of condition od retailer checks

	  	return $status;

	  } // End of function





	/***********************************************************************************************
	* DESC : Check distributor discount applicable on distributor retailer
	* Author : AJAY
	* Created : 17 July 2015
	*
	**/	


	  protected function checkDistributorSchemeMappedRetailer ( $discount_id, $retailer_id ) {

	  	$status = false;
	  	$distributor_ids = 0;
	  	$distributor_id = 0;
	  	$loggedInDistributor_id = 0;

	  	if(isset($_SESSION['distributorId'])){

	  		$loggedInDistributor_id = $_SESSION['distributorId'];
	  	}

	  	if( $discount_id > 0 && $retailer_id > 0 ) {

	  		//echo $discount_id;
			//echo $retailer_id;
			//exit;

	  		$distributorSchemeDistributors = $this->_getSelectList('table_discount AS D LEFT JOIN table_discount_distributor_mapping AS DDM ON DDM.discount_id = D.discount_id',"D.discount_id, D.is_dis_discount, DDM.distributor_id ",''," D.discount_id =".$discount_id);
	  		//echo "<pre>";
	  		//print_r($distributorSchemeDistributors);
	  		

		  		if(sizeof($distributorSchemeDistributors) > 0 && $distributorSchemeDistributors[0]->is_dis_discount == 1 ) {

		  			$distributor_ids = trim($distributorSchemeDistributors[0]->distributor_id);
		  			$dislist = array_filter(explode(",",$distributor_ids));

		  			//print_r($dislist );

		  			if(sizeof($dislist)> 0 && in_array($loggedInDistributor_id, $dislist)) {
		  				//echo "Hey";

			  			$findRetailer = $this->_getSelectList('table_retailer AS R'," R.retailer_id ",''," R.distributor_id=".$loggedInDistributor_id." AND R.retailer_id =".$retailer_id);
			  			//print_r($findRetailer);
						//exit;
			  			if(sizeof($findRetailer)> 0 && $findRetailer[0]->retailer_id > 0) {

			  				$status = true;

			  			}


		  			}

		  		}


	  	} 

	  	return $status;
	  }








	  public function addRetailerOrderByDistributor($orderPostData) {

	  	/*echo "<pre>";
	    print_r($orderPostData);
	  	exit;*/

	    $order_tmp_id  = NULL;
	    $order_status = "A";
	    $bill_date = NULL;
	    $bill_no = NULL;

	    $item_array       = array_filter($orderPostData[item_id]);
	    $item_price_array = array_filter($orderPostData[item_price]);
	    $qty_array        = array_filter($orderPostData[qty]);    
	    $ttlamt_array     = array_filter($orderPostData[ttlamt]);
	    $scheme_array     = array_filter($orderPostData[scheme]);

	   //print_r($scheme_array );
	    //exit;


   	 	if(trim($orderPostData['retailer_id'])!="" && sizeof($item_array)>0 && sizeof($qty_array)>0 ) {

   	 		$bill_date     = date('Y-m-d', strtotime($orderPostData['invoice_date']));

        	if(isset($orderPostData['invoice_date']) && isset($orderPostData['invoice_number']) && trim($orderPostData['invoice_number'])!="" ) {

          		$order_status  = "D";
          		$bill_no       = $orderPostData['invoice_number'];

        	}
      
	        $data['account_id']                   =   $orderPostData['account_id'];
	        $data['salesman_id']                  =   NULL;
	        $data['distributor_id']               =   $orderPostData['distributor_id'];
	        $data['retailer_id']                  =   $orderPostData['retailer_id'];
	        $data['bill_date']                    =   $bill_date;
	        $data['bill_no']                      =   $bill_no;
	        $data['date_of_order']                =   date('Y-m-d');
	        $data['time_of_order']                =   date('H:i:s');
	        $data['lat']                          =   NULL;
	        $data['lng']                          =   NULL;
	        $data['accuracy_level']               =   NULL;
	        $data['comments']                     =   NULL;
	        $data['order_type']                   =   "Yes"; // Yes , No , Adhoc
	        $data['location_provider']            =   NULL;
	        $data['tag_id']                       =   NULL;
	        $data['tag_description']              =   NULL;
	        $data['total_invoice_amount']         =  0;
	        $data['acc_total_invoice_amount']     =  0;

	        $data['last_update_date']             =   $date;
	        $data['last_update_status']           =   'New';
	        $data['order_status']                 =   $order_status;  // A = New Order, I = Processed , D = Dispatched 

	        // Actual Shceme Details 

	        $data['discount_type']                =   NULL;    // 1 = Amount, 2 = FOC, 3 = Percentage
	        $data['discount_amount']              =   NULL;
	        $data['dicount_percentage']           =   NULL;
	        $data['free_item_id']                 =   NULL;     // FOC Item ID If discount type FOC
	        $data['free_item_qty']                =   NULL;     // How many FOC Item qty
	        $data['discount_id']                  =   NULL;     // Scheme ID
	        $data['discount_desc']                =   NULL;     // Scheme Description


	        $data['acc_discount_type']            =   NULL;    // 1 = Amount, 2 = FOC, 3 = Percentage
	        $data['acc_discount_amount']          =   NULL;
	        $data['acc_dicount_percentage']       =   NULL;
	        $data['acc_free_item_id']             =   NULL;     // FOC Item ID If discount type FOC
	        $data['acc_free_item_qty']            =   NULL;     // How many FOC Items
	        $data['acc_discount_id']              =   NULL;     // Scheme ID


	        //INSERT INTO `quytech_pepupsales`.`table_order` (`order_id`, `account_id`, `salesman_id`, `distributor_id`, `retailer_id`, `bill_date`, `bill_no`, `date_of_order`, `time_of_order`, `lat`, `lng`, `accuracy_level`, `comments`, `order_type`, `location_provider`, `tag_id`, `tag_description`, `total_invoice_amount`, `acc_total_invoice_amount`, `discount_type`, `discount_amount`, `dicount_percentage`, `free_item_id`, `free_item_qty`, `discount_id`, `discount_desc`, `last_update_date`, `last_update_status`, `order_status`, `acc_discount_type`, `acc_discount_amount`, `acc_dicount_percentage`, `acc_free_item_id`, `acc_free_item_qty`, `acc_discount_id`) VALUES (NULL, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');


	        // Update last saved order
	        if(isset($orderPostData['order_tmp_id']) && $orderPostData['order_tmp_id'] !="" && is_int((int) $orderPostData['order_tmp_id']) ) {

	          $OrderData = array();


	          $OrderData['bill_date']                    =   $bill_date;
	          $OrderData['bill_no']                      =   $bill_no;		
	          $OrderData['last_update_date']             =   $date;
	          $OrderData['last_update_status']           =   'Update';

	          $order_tmp_id = $orderPostData['order_tmp_id'];

		      $this->_dbUpdate($OrderData,'table_order_tmp', " order_tmp_id=".$order_tmp_id);
       		  // Delete all the order details item from table_order_tmp_detail and insert again with new changes

       			$this->mysql_query("DELETE FROM `table_order_tmp_detail` WHERE `order_tmp_id` = ".$order_tmp_id);


	        } else { // Insert new Order

	        	$order_tmp_id = $this->_dbInsert($data,'table_order_tmp'); 

	        }


        	

        	//$order_id = $this->_dbInsert($data,'table_order'); 
    
       		if($order_tmp_id>0){

	      

	        $total_invoice_amount = 0;
	        $OrderData = array();


	      	foreach($item_array as $ikey=>$iValue ){
	      
	        $item_price = 0;
	        $data = array();
	        $schemeResult = array();

	       	$type                   =   1; // Default set 1 = Normal, 2 = Free Item

	        $discount_id            =   NULL;
	        $discount_desc          =   NULL;
	        $discount_type          =   NULL;
	        $discount_amount        =   NULL;
	        $discount_percentage    =   NULL;
	        $free_item_id           =   NULL;
	        $total_free_quantity    =   NULL;


	        $acc_discount_type      =   NULL;
	        $acc_discount_amount    =   NULL;
	        $acc_dicount_percentage =   NULL;
	        $acc_free_item_id       =   NULL;
	        $acc_free_item_qty      =   NULL;
	        $acc_discount_id        =   NULL;

	        if(trim($iValue)!="" && is_int( (int)$iValue)) {

	        $item_id      = trim($iValue);
	        $item_price   = trim($item_price_array[$ikey]);
	        $item_qty 	  = trim($qty_array[$ikey]);
	        

	        if($item_qty > 0) {

		        $data['order_tmp_id']               =   $order_tmp_id;
		        $data['item_id']                    =   $item_id;
		        $data['free_item_id']               =   $free_item_id;        
		        $data['price']                      =   $item_price;
		        $data['quantity']                   =   trim($qty_array[$ikey]);
		        $data['total']                      =   $data['quantity'] * $data['price'];
		        $data['acc_quantity']               =   $data['quantity'];
		        $data['acc_total']                  =   $data['total'];
		        $data['color_id']                   =   NULL;
		        $data['color_type']                 =   NULL;        
		        $data['type']                       =   1 ;     // 1 = Normal, 2 = Free Item
		        $data['price_type']                 =   1;     // 1 = MRP , 2 = DP , 3 = PTR
		        // $data['discount_id']                =   $discount_id;
		        // $data['discount_desc']              =   $discount_desc;
		        // $data['discount_type']              =   $discount_type;
		        // $data['discount_amount']            =   $discount_amount;
		        // $data['discount_percentage']        =   $discount_percentage;
		        // $data['total_free_quantity']        =   $total_free_quantity;
		        $data['last_update_date']           =   $date;
		        $data['last_update_status']         =   'New';
		        $data['order_detail_status']        =   '2';   // 1 = New , 2 = Accepted, 3 = Rejected, 4 = Discount Applicable, 5 = Discount Not Applicable
		        // $data['tag_id']                     =   NULL;
		        // $data['acc_discount_type']          =   $acc_discount_type;
		        // $data['acc_discount_amount']        =   $acc_discount_amount;
		        // $data['acc_dicount_percentage']     =   $acc_dicount_percentage;
		        // $data['acc_free_item_id']           =   $acc_free_item_id;
		        // $data['acc_free_item_qty']          =   $acc_free_item_qty;
		        // $data['acc_discount_id']            =   $acc_discount_id;


		        $total_invoice_amount = $total_invoice_amount + $data['total'] ;

		        $this->_dbInsert($data,'table_order_tmp_detail'); // Saved item detail


				//print_r($iValue);
		        // get Scheme Details applied this item

		        if(isset($scheme_array[$ikey]) && !empty($scheme_array[$ikey]))  {

		          $discount_id  = trim($scheme_array[$ikey]);

		           $schemeResult = $this->getItemSchemeDetail($discount_id, $item_qty,$item_price);

		           //print_r($schemeResult);

		              if(is_array($schemeResult) && sizeof($schemeResult)>0) {


		              	$data['free_item_id']               =   $free_item_id;        
		        		$data['price']                      =   0;
		        		$data['quantity']                   =   $schemeResult['total_free_quantity'];
		        		$data['total']                      =   $schemeResult['discount_amount'];
		        		$data['acc_quantity']               =   $schemeResult['total_free_quantity'];
		        		$data['acc_total']                  =   $schemeResult['discount_amount'];


		                  $type                   =   2;  // 1 = Normal, 2 = Free Item
		                  $discount_id            =   $schemeResult['discount_id'];
		                  $discount_desc          =   $schemeResult['discount_desc'];
		                  $discount_type          =   $schemeResult['discount_type'];
		                  $discount_amount        =   $schemeResult['discount_amount'];
		                  $discount_percentage    =   $schemeResult['discount_percentage'];
		                  $free_item_id           =   $schemeResult['free_item_id'];
		                  $total_free_quantity    =   $schemeResult['total_free_quantity'];

		                  $acc_discount_id        =   $discount_id;
		                  $acc_discount_type      =   $discount_type;
		                  $acc_discount_amount    =   $discount_amount;
		                  $acc_dicount_percentage =   $discount_percentage;
		                  $acc_free_item_id       =   $free_item_id;
		                  $acc_free_item_qty      =   $total_free_quantity;
		              }


		              // Added scheme details 

				        $data['type']                       =   $type ;     // 1 = Normal, 2 = Free Item
				        $data['discount_id']                =   $discount_id;
				        $data['discount_desc']              =   $discount_desc;
				        $data['discount_type']              =   $discount_type;
				        $data['discount_amount']            =   $discount_amount;
				        $data['discount_percentage']        =   $discount_percentage;
				        $data['total_free_quantity']        =   $total_free_quantity;


				        $data['acc_discount_type']          =   $acc_discount_type;
				        $data['acc_discount_amount']        =   $acc_discount_amount;
				        $data['acc_dicount_percentage']     =   $acc_dicount_percentage;
				        $data['acc_free_item_id']           =   $acc_free_item_id;
				        $data['acc_free_item_qty']          =   $acc_free_item_qty;
				        $data['acc_discount_id']            =   $acc_discount_id;

				        $data['order_detail_status']        =   '4';


		              $this->_dbInsert($data,'table_order_tmp_detail'); // Saved item Scheme Details if any scheme exists

		        }


		    }

		        // get Scheme Details applied this item


		        	

		        //unset($data['order_tmp_id']);
		        //$data['order_id'] = $order_id;
		        //$this->_dbInsert($data,'table_order_detail');

	        
	        	} // End of check item Id not null
	    	  }	// End of item array foreach loop


		      $OrderData['total_invoice_amount']      = $total_invoice_amount;
		      $OrderData['acc_total_invoice_amount']  =  $total_invoice_amount;

		      $this->_dbUpdate($OrderData,'table_order_tmp', " order_tmp_id=".$order_tmp_id);

		     //$this->_dbUpdate($OrderData,'table_order', " order_id=".$order_id);


		  } // End of check order saved successfully 

		} // Check post data should not be empty


		//exit;
		return $order_tmp_id;

	} // End of the function 
























	 /***********************************************************
	  	* Desc : Check Schemes for this current reuqest with these parameters (Item ID, Retailer ID, Item Quantity)
	  	* Author : AJAY
	  	* Created : 7th June 2015
	  	* Base Logic :  
	  		- Firstly find all the schemes are active
	  		- Thereafter check the scheme type like All, Category, Items
	  		   - Into second step we check party type All, state, city, retailer
	  		- Saved all the active and party based schemes into a dataset
	  	* Discount Modes : 1 = Quantity, 2 = Amount
	  	* Item Type : 1 = ALL, 2 Category, 3 Items
	  	* Party Types : 1 = All, 2 = State, 3 = City, 4 = Retailer
	  	* Discount schemes : 1 = Exclusive, 2 = Non Exclusive
	  	* Discount Types : 1 = Percentage, 2 = Amount, 3 = FOC
	  */

	public function checkSchemes() {


	//print_r($_REQUEST);

	if(isset($_REQUEST["item_id"]) && isset($_REQUEST["retailer_id"]) && isset($_REQUEST["quantity"]) && is_int((int)$_REQUEST["item_id"]) && is_int((int)$_REQUEST['retailer_id']) && is_int((int)$_REQUEST["quantity"]) ) {

	$item_id = trim($_REQUEST['item_id']);
	$retailer_id = trim($_REQUEST['retailer_id']);
	$quantity = trim($_REQUEST['quantity']);

	$schemeArraySet = array();
	$category_id = 0;

		$itemSchemeResultSet = $this->_getSelectList('table_discount AS D  
			LEFT JOIN table_discount_detail AS DD ON DD.discount_id = D.discount_id',
			"D.discount_id, D.discount, D.party_type, D.item_type, D.mode, D.status, D.start_date, D.end_date, DD.*",
			''," D.start_date<='".$this->_DATE."' AND D.end_date>='".$this->_DATE."' AND D.status = 'A' AND D.mode = 1 AND DD.minimum_quantity<=".$quantity);


			if(is_array($itemSchemeResultSet) && sizeof($itemSchemeResultSet)>0) {

				foreach ($itemSchemeResultSet as $key => $value) {


				 if($quantity >= $value->minimum_quantity  ) {

					switch ($value->item_type) {

						case '1': // Check for scheme type all
							# code...
								
								$party_type_result = $this->checkPartyType($retailer_id, $value->discount_id, $value->party_type);

								if($party_type_result) {

									$schemeArraySet[] = array('discount_id'=>$value->discount_id, 'discount_desc'=>$value->discount_desc);

								}

								

							break;


						case '2': // Check for scheme type is category
							# code...
								
								$party_type_result = $this->checkPartyType($retailer_id, $value->discount_id, $value->party_type);

								if($party_type_result) {


									$getItemDetails = $this->_getSelectList('table_item AS I',"I.category_id",''," I.item_id = '".$item_id."'");

									//print_r($getItemDetails);

									if(sizeof($getItemDetails)>0)  {

										$category_id = $getItemDetails[0]->category_id;

										$itemTypeCategoryResult = $this->_getSelectList2('table_discount_item AS DI',"DI.category_id",''," DI.discount_id ='".$value->discount_id."' AND DI.category_id = '".$category_id."'");

											if(sizeof($itemTypeCategoryResult) > 0 ) {

									  			$schemeArraySet[] = array('discount_id'=>$value->discount_id, 'discount_desc'=>$value->discount_desc);
									  		}

									}

								}

								

							break;

							case '3': // Check for scheme type is item
							# code...
								
								$party_type_result = $this->checkPartyType($retailer_id, $value->discount_id, $value->party_type);
								//echo "Hello";
								

								if($party_type_result) {

										$itemTypeItemResult = $this->_getSelectList2('table_discount_item AS DI',"DI.item_id",''," DI.discount_id ='".$value->discount_id."' AND DI.item_id = '".$item_id."'");

											//echo $party_type_result;
											//print_r($itemTypeItemResult);
											//exit;

											if(sizeof($itemTypeItemResult) > 0 ) {

									  			$schemeArraySet[] = array('discount_id'=>$value->discount_id, 'discount_desc'=>$value->discount_desc);
									  		}

									}

							break;
						
						default:
							# code...
							break;


					} 	// End of check item types swicth case


				} 	// End of check quantity


			} 	// check any discount schemes for loop

		}	// Check if any schemes exists


		//print_r($schemeArraySet);
		//exit;

		if( sizeof($schemeArraySet)>0 ) {
			foreach ($schemeArraySet as $key=>$value) {?>
				<option value="<?php echo $value['discount_id'];?>" <?php if(isset($_REQUEST["selected_discount_id"]) && $_REQUEST["selected_discount_id"] == $value['discount_id']) {?>selected <?php }?> ><?php echo $value['discount_desc'];?></option>
			<?php } } else { ?>
				<option value="0">No Scheme</option>
		<?php }


		}  // final tag closed 


	} // End of function













	 /***********************************************************
	  	* Desc : Check Invoice Schemes for this current reuqest order with these parameters (Order Id, Total Invoice Amount)
	  	* Author : AJAY
	  	* Created : 11th June 2015
	  	* Base Logic :  
	  		- Firstly find all the invoice schemes are active and fullfill the minimun amount crieteria.
	  		- Check order if any item have exclusive scheme so we will check non - exclusive invoice shcemes, If not so we will check for both.
	  		- second step we check party type All, state, city, retailer
	  		- Saved all the active and party based schemes into a dataset
	  	* Discount Modes : 1 = Quantity, 2 = Amount
	  	* Discount : 1 = Exclusive, 2 = Non Exclusive
	  	* Discount Type : 1 = Percentage, 2 = Amount, 3 = FOC
	  	* Table Order Tmp Detail : Types : 1 = None, 2 = Free Item
	  */

	public function checkInvoiceSchemes($retailer_id, $order_tmp_id = NULL, $total_invoice_amount = NULL) {

		$result = array();
		$invoiceSchemeResultSet = array();
		$checkOrderExclusiveScheme = array();
		$schemeArraySet = array();


		if(is_int((int)$retailer_id) && $retailer_id > 0 && is_int((int)$order_tmp_id) & is_numeric((int)$total_invoice_amount) && $total_invoice_amount > 0 ) {

			$order_tmp_id = trim($order_tmp_id);
			$retailer_id = trim($retailer_id);
			$total_invoice_amount = trim($total_invoice_amount);


			$category_id = 0;
			$condition = "";

			// Before get all the invoice schemes need to be check any exclusive shceme applied in this order or not.

			$checkOrderExclusiveScheme = $this->_getSelectList('table_order_tmp AS O
			LEFT JOIN table_order_tmp_detail AS OD ON OD.order_tmp_id = O.order_tmp_id
			LEFT JOIN table_discount AS D ON D.discount_id = OD.discount_id',
			"OD.discount_id",''," O.order_tmp_id = ".$order_tmp_id." AND OD.type = 2 AND D.discount = 1");
			//echo "<pre>";
			//print_r($checkOrderExclusiveScheme);

				if(sizeof($checkOrderExclusiveScheme) > 0 ) {

					$condition = " AND D.discount = 2";
				}


			// fetch all the invoice shcemes 
				// Discount Modes : 1 = Quantity, 2 = Amount

			$invoiceSchemeResultSet = $this->_getSelectList('table_discount AS D  
			LEFT JOIN table_discount_detail AS DD ON DD.discount_id = D.discount_id
			LEFT JOIN table_discount_item AS DI ON DI.discount_id = D.discount_id',
			"D.discount_id, D.discount, D.party_type, D.item_type, D.mode, DI.item_id, D.status, D.mode, D.is_open, D.start_date, D.end_date, DD.*", '', " D.start_date<='".$this->_DATE."' AND D.end_date>='".$this->_DATE."' AND D.status = 'A' AND D.mode = 2 AND DI.item_id IS NULL AND DD.minimum_amount<='".$total_invoice_amount."'".$condition);

			//echo "<pre>";
					
			//print_r($invoiceSchemeResultSet);


			if(is_array($invoiceSchemeResultSet) && sizeof($invoiceSchemeResultSet)>0) {

				foreach ($invoiceSchemeResultSet as $key => $value) {

					//echo $total_invoice_amount .">=". $value->minimum_amount ;
					//echo "<br>";

				 if($total_invoice_amount >= $value->minimum_amount ) {
				 	//echo "Yes";
					//echo "<br>";
					switch ($value->mode) {

						case '2': // Check for scheme mode Amount and invoice
							# code...
								
								$party_type_result = $this->checkPartyType($retailer_id, $value->discount_id, $value->party_type);

								//print_r($party_type_result);

								if($party_type_result) {

									$schemeArraySet[] = array('discount_id'=>$value->discount_id, 'discount_desc'=>$value->discount_desc, 'is_open'=>$value->is_open, 'discount_type'=>$value->discount_type);

								}

								

							break;

						
						default:
							# code...
							break;


					} 	// End of check item types swicth case


				} 	// End of check quantity


			} 	// check any discount schemes for loop

		}	// Check if any schemes exists


	} // End of check Paramaters


		return $schemeArraySet;

	}










		/***********************************************************
	  	* Desc : Calculate Invoice Scheme discount amount
	  	* Author : AJAY
	  	* Created : 12th June 2015
	  	* Discount Types : 1 = Percentage, 2 = Amount, 3 = FOC
	  	* is_open discount : 1 = Yes, 2 = No
	  	*/



		public function getInvoiceSchemeDetail ($discount_id, $order_tmp_id, $is_open_discount) {



		$schemeDiscountDetails = array();
		$itemSchemeResultSet = array();
		$discount_amount = 0;
		$foc_id = 0;

		if(isset($discount_id) && !empty($discount_id) && is_int((int) $order_tmp_id) && $order_tmp_id > 0 ) {

      	$tmp_order_detail = $this->_getSelectList('table_order_tmp AS O',"O.total_invoice_amount", '', " O.order_tmp_id =".$order_tmp_id);

      		if(sizeof($tmp_order_detail)>0 &&  $tmp_order_detail[0]->total_invoice_amount > 0 ) {

      			$total_invoice_amount = $tmp_order_detail[0]->total_invoice_amount;

      			$invoiceScheme = $this->_getSelectList('table_discount AS D  
      				LEFT JOIN table_discount_detail AS DD ON DD.discount_id = D.discount_id
      				LEFT JOIN table_discount_item AS DI ON DI.discount_id = D.discount_id',
      				"D.discount_id, D.discount, D.party_type, D.item_type, D.mode, DI.item_id, D.is_open, D.status, D.mode, D.is_open, D.start_date, D.end_date, DD.*", '', " D.discount_id =".$_POST['discount_id']);

      			if(is_array($invoiceScheme) && sizeof($invoiceScheme)>0) {


      			foreach ($invoiceScheme as $key => $value) {


      				switch ($value->discount_type) {

      				 case 1 : // calculate percentage discount

      				  if($value->is_open == 1) {  // Check this is open discount or not

      				  //	echo $is_open_discount;


      				  	if($is_open_discount > 0 && $is_open_discount <= 100)
      				  		$discount_amount = ($total_invoice_amount * $is_open_discount)/100;
      				  		$value->discount_percentage = $is_open_discount;

      				  } else  { 

      				  	$discount_amount = ($total_invoice_amount * $value->discount_percentage)/100;
      				  
      				  }   // End of check this is open discount or not




      				  $schemeDiscountDetails = array('discount_id'=>$discount_id, 'discount_type'=>$value->discount_type, 'discount_desc'=>$value->discount_desc, 'type'=>1, 'discount_percentage'=>$value->discount_percentage, 'discount_amount'=>$discount_amount, 'free_item_id'=>NULL, 'total_free_quantity'=>NULL);
		              //print_r($schemeDiscountDetails);
		              //exit;

             	break;

             	case 2 : // Calculate for Amount discount

              		

              		 if($value->is_open == 1 ) {  // Check this is open discount or not

              		 	if($total_invoice_amount > $is_open_discount)  $discount_amount =  $is_open_discount;
              		 	

              		 } else  { 

              		 	// check if requested total invoice amount of order > then minimun amount of scheme

              		 	if($total_invoice_amount > $value->minimum_amount) {

              		 			$remainder =  $total_invoice_amount % $value->minimum_amount;
								$discount_amount = 	  (($total_invoice_amount - $remainder) / $value->minimum_amount) * $value->discount_amount;
								

              		 	} else {

              		 		$discount_amount = $value->discount_amount;
              		 	}
              		 } // End of check this is open discount or not


              		$schemeDiscountDetails = array('discount_id'=>$discount_id, 'discount_type'=>$value->discount_type, 'discount_desc'=>$value->discount_desc, 'type'=>1, 'discount_percentage'=>NULL, 'discount_amount'=>$discount_amount, 'free_item_id'=>NULL, 'total_free_quantity'=>NULL);

              	break;


              	case 3 : // check for FOC

              	$foc_id = $value->foc_id;

              	$free_item_id = NULL;

              	$total_free_quantity = NULL;

              	$focDetail = $this->_getSelectList2('table_foc_detail AS FOC',"FOC.free_item_id, FOC.free_qty",''," FOC.foc_detail_id =".$value->foc_id);

                if(sizeof($focDetail)>0) {

                  $free_item_id = $focDetail[0]->free_item_id;
                  $total_free_quantity = $focDetail[0]->free_qty;

                  

                  if($value->is_open == 1) {   // Check this is open discount or not


                  } else  { 

                  		// check if order invoice amount > then minimun amount of scheme

                       if($total_invoice_amount > $value->minimum_amount) {

                        $remainder =  $total_invoice_amount % $value->minimum_amount;
                        $total_free_quantity =    (($total_invoice_amount - $remainder) / $value->minimum_amount) * $total_free_quantity;

                      } 

                   }   // End of check this is open discount or not

                } // End of check FOC Item Details

                $schemeDiscountDetails = array('discount_id'=>$discount_id, 'discount_type'=>$value->discount_type, 'discount_desc'=>$value->discount_desc, 'type'=>2, 'discount_percentage'=>NULL, 'discount_amount'=>NULL, 'free_item_id'=>$free_item_id, 'total_free_quantity'=>$total_free_quantity);

              break;    

            	default:
             		 # code...
              	break;
         
         	 } // End of switch case

         	 } // End of foreach loop
				
			} // End of check discount details exists or not

		  } // End of check order exists or not

		 } // End of check disocunt Id exists or not

		return $schemeDiscountDetails;

	} // End of the function

























































}  // End of class ?>