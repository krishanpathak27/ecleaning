<?php
/********************************************************************************************************
	** FileName: class.sort.php
	** Package: Class		
	** Purpose: Class File is used for manage the sorting

/*******************************************************************************************************/ 
	class SortData
	{
		function getOrderSql($default_order_field_name="", $order_name="")
		{
			$order_type = "desc";			
			if($order_name=="" && ($default_order_field_name == "" || $default_order_field_name == "no"))
			{
				return "";
			}						
			else if(isset($_GET["field_name"]) && $_GET["field_name"] != "")
			{
				$order_field_name = $_GET["field_name"];
				
				if($order_field_name == $_SESSION[$order_name."sess_order_field_name"])
				{
					if($_SESSION[$order_name."sess_order_field_type"] == "asc")
					{
						$order_type = "desc";
					}
					else
					{
						$order_type = "asc";
					}
				}
			}
			else if(isset($_SESSION['sortorder']) && $_SESSION['sortorder']!="")
			{
				$order_name=$_SESSION['sortorder'];
				$order_field_name = $_SESSION[$order_name."sess_order_field_name"];
				$order_type = $_SESSION[$order_name."sess_order_field_type"];
				
			}
			else if($default_order_field_name!="")
			{
				return $default_order_field_name;
			} 			
			else
			{
				return "";
			}
			$_SESSION[$order_name."sess_order_field_name"] = $order_field_name;
			$_SESSION[$order_name."sess_order_field_type"] = $order_type;
			$_SESSION['sortorder']=$order_name;
			$sql = " order by ".$order_field_name." ".$order_type;
			
			return $sql;
		}
		
	}
?>