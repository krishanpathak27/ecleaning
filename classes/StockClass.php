<?php 

	class StockClass extends Db_Action {


	public function __construct() {

		parent::__construct();

		$this->_DATE = date('Y-m-d');

	}




	  	/***********************************************************
	  	* Desc : get the scheme details with discounts
	  	* Author : AJAY
	  	* Created : 8th June 2015
	  	* Discount Types : 1 = Percentage, 2 = Amount, 3 = FOC
	  	* type : 1 = Normal, 2 = Free Item
	  	*/



		public function getItemSchemeDetail ($discount_id, $item_qty, $item_price) {

		$schemeDiscountDetails = array();
		$itemSchemeResultSet = array();
		$discount_amount = 0;
		$foc_id = 0;

		if(isset($discount_id) && !empty($discount_id)) {

		$itemSchemeResultSet = $this->_getSelectList('table_discount AS D
			LEFT JOIN table_discount_detail AS DD ON DD.discount_id = D.discount_id',
			"D.discount_id, D.discount, D.party_type, D.item_type, D.mode, D.status, D.start_date, D.end_date, DD.*",''," D.discount_id =".$discount_id);

			if(is_array($itemSchemeResultSet) && sizeof($itemSchemeResultSet)>0) {

				/*echo "<pre>";
				print_r($itemSchemeResultSet);
				exit;*/

				foreach ($itemSchemeResultSet as $key => $value) {

					// Check Discount Type of this scheme : 1 = Percentage, 2 = Amount, 3 = FOC

					switch ($value->discount_type) {
						
						case 1 : // check for percentage

							$discount_amount = ($item_qty * $item_price * $value->discount_percentage)/100;


							$schemeDiscountDetails = array('discount_id'=>$discount_id, 'discount_type'=>$value->discount_type, 'discount_desc'=>$value->discount_desc, 'type'=>1, 'discount_percentage'=>$value->discount_percentage, 'discount_amount'=>$discount_amount, 'free_item_id'=>NULL, 'total_free_quantity'=>NULL);
							//print_r($schemeDiscountDetails);
							//exit;

							break;

						case 2 : // check for Amount

							// check if requested quantity of item > then minimun quantity of scheme

							if($item_qty > $value->minimum_quantity) {

								$remainder =  $item_qty % $value->minimum_quantity;
								$discount_amount = 	  (($item_qty - $remainder) / $value->minimum_quantity) * $value->discount_amount;

							} else {

								$discount_amount = $value->discount_amount;
							}
							

							$schemeDiscountDetails = array('discount_id'=>$discount_id, 'discount_type'=>$value->discount_type, 'discount_desc'=>$value->discount_desc, 'type'=>1, 'discount_percentage'=>NULL, 'discount_amount'=>$discount_amount, 'free_item_id'=>NULL, 'total_free_quantity'=>NULL);


							break;

						case 3 : // check for FOC

							$foc_id = $value->foc_id;
							$free_item_id = NULL;
							$total_free_quantity = NULL;

							$focDetail = $this->_getSelectList2('table_foc_detail AS FOC',"FOC.free_item_id, FOC.free_qty",''," FOC.foc_detail_id =".$value->foc_id);

								if(sizeof($focDetail)>0) {

									$free_item_id = $focDetail[0]->free_item_id;
									$total_free_quantity = $focDetail[0]->free_qty;

									// check if requested quantity of item > then minimun quantity of scheme

									if($item_qty > $value->minimum_quantity) {

										$remainder =  $item_qty % $value->minimum_quantity;
										$total_free_quantity = 	  (($item_qty - $remainder) / $value->minimum_quantity) * $total_free_quantity;

									} 
								}

								$schemeDiscountDetails = array('discount_id'=>$discount_id, 'discount_type'=>$value->discount_type, 'discount_desc'=>$value->discount_desc, 'type'=>2, 'discount_percentage'=>NULL, 'discount_amount'=>NULL, 'free_item_id'=>$free_item_id, 'total_free_quantity'=>$total_free_quantity);

							break;		

						default:
							# code...
							break;
					}

				} // End foreach
				
			} // check discount details exists or not

		} // check discount id should not be empty

		return $schemeDiscountDetails;

	} // End of the function





	public function getBatteryAgeing ($manufacture_date, $recharged_date, $distributor_dispatch_date) {
		

		if($distributor_dispatch_date!='0000-00-00' && !empty(trim($distributor_dispatch_date))) {
			$currentDate = strtotime($distributor_dispatch_date);
		} else if($recharged_date!='0000-00-00'  && !empty(trim($recharged_date))) {
			$currentDate = strtotime($recharged_date);
		} else {
			$currentDate = strtotime($this->_DATE);
		}

		
		$manufacture_date = strtotime($manufacture_date);
		$datediff = $currentDate - $manufacture_date;
		$batteryAge = floor($datediff / (60 * 60 * 24));

		// $currentDate = new DateTime($this->_DATE);
		// $manufacture_date = new DateTime($manufacture_date);
		// $batteryAge = $currentDate->diff($manufacture_date)->format("%a");

		return $batteryAge;
	}




		public function getBatteryStockStatus ($stockStatus) {

		// BSN Status
		// status(I-In-process, A-Available, D-Dispatched, S-Sold, C-Complaint, DMG-Damage, P-Pending, R-Reject, SCRP-Scraped)
			
			switch ($stockStatus) {
				case 'A':
					# code...
					$status = "Available";
					break;

				case 'I':
					# code...
					$status = "In-Process";
					break;

				case 'D':
					# code...
					$status = "Dispatched";
					break;

				case 'S':
					# code...
					$status = "Sold";
					break;


				case 'C':
					# code...
					$status = "Complaint";
					break;


				case 'DMG':
					# code...
					$status = "Damage";
					break;


				case 'RPL':
					# code...
					$status = "Replace";
					break;


				case 'P':
					
					$status = "Pending";
					break;

				case 'R':
					
					$status = "Reject";
					break;

				case 'SCRP':
					# code...
					$status = "Scraped";
					break;

				default:
					# code...
					$status = "-";
					break;
			}

			return $status;
		}


		public function getBatteryStockStatusDistributor ($stockStatus1) {

		// BSN Status
		// status(I-In-process, A-Available, D-Dispatched, S-Sold, C-Complaint, DMG-Damage, P-Pending, R-Return)
			
			switch ($stockStatus1) {
				case 'A':
					# code...
					$status = "Available";
					break;

				case 'I':
					# code...
					$status = "In-Process";
					break;

				case 'D':
					# code...
					$status = "Dispatched";
					break;

				case 'S':
					# code...
					$status = "Sold";
					break;


				case 'C':
					# code...
					$status = "Complaint";
					break;


				case 'DMG':
					# code...
					$status = "Damage";
					break;


				case 'RPL':
					# code...
					$status = "Replace";
					break;

				case 'P':
				
					$status = "Pending";
					break;

				case 'R':

					$status = "Return";
					break;

				default:
					# code...
					$status = "-";
					break;
			}

			return $status;
		}

public function getBatteryStockStatusDealer ($dealerstockstatus) {

		// BSN Status
		// status(I-In-process, A-Available, D-Dispatched, S-Sold, C-Complaint, DMG-Damage)
			
			switch ($dealerstockstatus) {
				case 'A':
					# code...
					$status = "Available";
					break;

				case 'I':
					# code...
					$status = "In-Process";
					break;

				case 'D':
					# code...
					$status = "Dispatched";
					break;

				case 'S':
					# code...
					$status = "Sold";
					break;


				case 'C':
					# code...
					$status = "Complaint";
					break;


				case 'DMG':
					# code...
					$status = "Damage";
					break;


				case 'RPL':
					# code...
					$status = "Replace";
					break;
				default:
					# code...
					$status = "-";
					break;
			}

			return $status;
		}


		public function getSaleRtrnStockStatus ($stockStatus) {

		// BSN Status
		// status(I-In-process, A-Available, D-Dispatched, S-Sold, C-Complaint, DMG-Damage, P-Pending, R-Reject)
			
			switch ($stockStatus) {
				case 'A':
					# code...
					$status = "Available";
					break;

				case 'I':
					# code...
					$status = "In-Process";
					break;

				case 'D':
					# code...
					$status = "Dispatched";
					break;

				case 'S':
					# code...
					$status = "Sold";
					break;


				case 'C':
					# code...
					$status = "Complaint";
					break;


				case 'DMG':
					# code...
					$status = "Damage";
					break;


				case 'RPL':
					# code...
					$status = "Replace";
					break;


				case 'P':
					
					$status = "Pending";
					break;

				case 'R':
					
					$status = "Reject";
					break;

				default:
					# code...
					$status = "-";
					break;
			}

			return $status;
		}




	public function warehouseDispatches ($paramSet) {

		if(sizeof($paramSet)>0 && isset($paramSet['warehouse_id']) && $paramSet['warehouse_id']>0 && isset($paramSet['distributor_id']) && $paramSet['distributor_id']>0 && isset($paramSet['invoice_no']) && !empty($paramSet['invoice_no']) && isset($paramSet['invoice_date']) && !empty($paramSet['invoice_date']) ) {

			$warehouse_id 		= $paramSet['warehouse_id'];
			$distributor_id     = $paramSet['distributor_id'];
			$bill_no 	 		= trim($paramSet['invoice_no']);
			$bill_date 	 		= date('Y-m-d', strtotime($paramSet['invoice_date']));

			$delivery_challan_no 	 		= ($paramSet['challan_no'])?$paramSet['challan_no']:'';
			$delivery_challan_date 	 		= ($paramSet['challan_date'])?date('Y-m-d', strtotime($paramSet['challan_date'])):'';

			if(isset($paramSet['selectedBsn']) && sizeof($paramSet['selectedBsn'])>0 && isset($paramSet['Bsn']) && sizeof($paramSet['Bsn'])>0) {

				foreach ($paramSet['selectedBsn'] as $key => $value) {
					# code...

					// Firslty Check BSN available into bsn master or not

					if(isset($paramSet['Bsn'][$key]) && !empty($paramSet['Bsn'][$key])) {

						$bsn = trim($paramSet['Bsn'][$key]);
						$bsn_id = "";
						$item_id = "";
						$category_id = "";

						$bsnDetail = $this->_getSelectList('table_item_bsn	 AS BSN',"BSN.bsn_id, BSN.status, BSN.item_id, BSN.category_id",''," BSN.bsn ='".$bsn."'");
						// echo "<pre>";
						// print_r($bsnDetail);
						// exit;

						if(isset($bsnDetail[0]->bsn_id)) {

							$bsn_id = $bsnDetail[0]->bsn_id;
							$item_id = $bsnDetail[0]->item_id;
							$category_id = $bsnDetail[0]->category_id;

							// Update BSN table sale distributor and date of dispatch
							if($bsnDetail->status !='S' || $bsnDetail->status !='D') {

								$bsnArray['sale_distributor_id']    		= $distributor_id;	
								$bsnArray['distributor_dispatch_date']    	= date('Y-m-d');
								$bsnArray['last_updated_date']	 			= date('Y-m-d');	
								$bsnArray['last_update_datetime']			= date('Y-m-d H:i:s');	
								$bsnArray['status']							= 'D'; // Dispatched to distributor
								
								$this->_dbUpdate($bsnArray,'table_item_bsn', " bsn='".$bsn."'");  

							}


						} else {

								// Create new stock entry of a BSN

								$bsnArray = array();

								$bsnArray['account_id']        		= $_SESSION['accountId'];	
								$bsnArray['dispatch_warehouse_id']  = $warehouse_id;
								$bsnArray['sale_distributor_id']    = $distributor_id;	
								$bsnArray['item_id']           		= '';
								$bsnArray['category_id']       		= '';
								$bsnArray['batch_no']       		= '';
								$bsnArray['bsn']       				= $bsn;
								$bsnArray['stock_value']       		= 1;
								$bsnArray['manufacture_date']       = '';
								$bsnArray['warranty_period']       	= '';
								$bsnArray['warranty_prorata']       = '';
								$bsnArray['warranty_grace_period']  = '';
								$bsnArray['warranty_end_date']      = '';
								$bsnArray['user_type'] 		        =  (isset($_SESSION['userLoginType']))?$_SESSION['userLoginType']:0;	
								$bsnArray['web_user_id'] 		    =  (isset($_SESSION['userLoginType']))?$_SESSION['PepUpSalesUserId']:0;
								$bsnArray['last_updated_date']	 	= date('Y-m-d');	
								$bsnArray['last_update_datetime']	= date('Y-m-d H:i:s');	
								$bsnArray['created_datetime']		= date('Y-m-d H:i:s');	
								$bsnArray['status']					= 'A';
								
								$bsn_id = $this->_dbInsert($bsnArray,'table_item_bsn');  

						}



								// 	bsn lifecycle activity data array 

								$data = array();

								$data['account_id'] 			=  $_SESSION['accountId'];	
								$data['item_id'] 				=  $item_id;
								$data['bsn_id'] 		        =  $bsn_id;
								$data['bsn'] 		            =  $bsn;	
								$data['from_warehouse_id']      =  $warehouse_id;	
								$data['distributor_id'] 		=  $distributor_id;	
								$data['bill_date'] 				=  $bill_date;
								$data['bill_no'] 				=  $bill_no;
								$data['user_type'] 		        =  $_SESSION['userLoginType'];	
								$data['web_user_id'] 		    =  $_SESSION['PepUpSalesUserId'];	
								$data['created_datetime'] 		=  date('Y-m-d H:i:s');	
								$data['activity_type'] 		    =  'D'; // D- Dispatched, R-Return, C-Complain
								$data['status'] 		        =  'A';		

								$this->_dbInsert($data,'table_item_bsn_lifecycle_activity');  



								// Add Stock distributor in-process table as GRN

								$condi = " distributor_id = '".$distributor_id."' AND bsn_id ='".$bsn_id."' AND status IN ('A', 'I')";
								$resultset = $this->_getSelectList('table_item_dis_stk_inprocess','dis_stk_inpro_id, acpt_stock_value','',$condi);


								if(is_array($resultset) && sizeof($resultset)>0)  {   

									// Update Warehouse dispatches to distributor activity
									$disInProcess = array();

									$disInProcess['last_update_datetime'] 		= date('Y-m-d H:i:s');
									$disInProcess['bill_date'] 					= $bill_date;
									$disInProcess['bill_no']                    = $bill_no;


									$this->_dbUpdate($disInProcess,'table_item_dis_stk_inprocess', 'dis_stk_inpro_id="'.$resultset[0]->dis_stk_inpro_id.'"');

								} else {
									
									// Add Warehouse dispatches to distributor activity
									$disInProcess = array();

									$disInProcess['account_id']              	= $_SESSION['accountId'];
									$disInProcess['warehouse_id']      			= $warehouse_id;	
									$disInProcess['distributor_id'] 			= $distributor_id;
									$disInProcess['item_id']                    = $item_id;
									$disInProcess['bsn_id']                    	= $bsn_id;
									$disInProcess['category_id']                = $category_id;
									$disInProcess['attribute_value_id']  		= 0;
									$disInProcess['bill_date'] 					= $bill_date;
									$disInProcess['bill_no']                    = $bill_no;
									$disInProcess['rec_stock_value']            = 1;
									$disInProcess['delivery_challan_no']        = $delivery_challan_no;
									$disInProcess['delivery_challan_date']      = $delivery_challan_date;

									$disInProcess['last_update_datetime']   	= date('Y-m-d H:i:s');	
									$disInProcess['created_datetime'] 			= date('Y-m-d H:i:s');
									$disInProcess['status']  					= 'I';

									$dis_stk_inpro_id = $this->_dbInsert($disInProcess,'table_item_dis_stk_inprocess');

								}



								// Update BSN Status from warehouse Stock or BSN Master
		 

								$condi = " warehouse_id = '".$warehouse_id."' AND bsn_id ='".$bsn_id."'";
								$resultset = $this->_getSelectList('table_warehouse_stock AS WS','WS.warehouse_stock_id, WS.status ','',$condi);

								if(is_array($resultset) && sizeof($resultset)>0)  { 

									// Update Warehouse BSN Status
									$whStock = array();
									if($resultset[0]->status == 'A') {
										$whStock['last_updated_date']	 		=  date('Y-m-d');	
										$whStock['last_update_datetime'] 		=  date('Y-m-d H:i:s');
										$whStock['status']		            	=  'D';

										$this->_dbUpdate($whStock,'table_warehouse_stock', 'warehouse_stock_id="'.$resultset[0]->warehouse_stock_id.'"');
									} else if($resultset[0]->status == 'D') {
										return array('status'=>'failed', 'msg'=>'This BSN('.$bsn.') Already Discpatched');
									} else if($resultset[0]->status == 'I') {
										return array('status'=>'failed', 'msg'=>'Please Accept/Scan BSN('.$bsn.') Before Discpatches');
									} else if($resultset[0]->status == 'S') {
										return array('status'=>'failed', 'msg'=>'BSN('.$bsn.') Already Sold');
									} else if($resultset[0]->status == 'DMG') {
										return array('status'=>'failed', 'msg'=>'BSN('.$bsn.') Damage');
									}



								} else {

									// 	Add New BSN Warehouse Stock 

									$whStock = array();

									$whStock['account_id'] 				=  $_SESSION['accountId'];	
									$whStock['warehouse_id'] 		    =  $warehouse_id;
									$whStock['item_id'] 				=  $item_id;
									$whStock['bsn_id'] 		        	=  $bsn_id;
									$whStock['stock_value']       		=  1;
									$whStock['last_updated_date']	 	=  date('Y-m-d');	
									$whStock['last_update_datetime']	=  date('Y-m-d H:i:s');	
									$whStock['created_datetime']		=  date('Y-m-d H:i:s');	
									$whStock['status']		            =  'D';

									$this->_dbInsert($whStock,'table_warehouse_stock');  

								}  

					}


				}

				return array('status'=>'success', 'msg'=>'Stock dispatched successfully');

			} else {

				return array('status'=>'failed', 'msg'=>'No Bsn Selected!');
			}




		} else {
			return array('status'=>'failed', 'msg'=>'Imput parameters missing, Please Select Warehouse, Distributor, Invoice date and number!');
		}


	}





	public function warehouseGenerateMRN ($paramSet) {

		// echo "<pre>";
		// print_r($paramSet);
		// exit;
		
	if(isset($_SESSION['warehouseId']) && !empty($_SESSION['warehouseId']) ) {

		// if(sizeof($paramSet)>0 && isset($paramSet['distributorID']) && $paramSet['distributorID']>0 && isset($paramSet['mrn_no']) && $paramSet['mrn_no']>0 && isset($paramSet['mrn_date']) && !empty($paramSet['mrn_date']) && isset($paramSet['challan_no']) && !empty($paramSet['challan_no']) && isset($paramSet['challan_date']) && !empty($paramSet['challan_date'])) {

		if(sizeof($paramSet)>0 && ((isset($paramSet['distributorID']) && $paramSet['distributorID']>0) OR (isset($paramSet['customer_contact']) && $paramSet['customer_contact']>0) && isset($paramSet['mrn_no'])) && $paramSet['mrn_no']!='' && isset($paramSet['mrn_date']) && !empty($paramSet['mrn_date'])) {

			$warehouse_id 		= $_SESSION['warehouseId'];
			$distributor_id     = $paramSet['distributorID'];
			$mrn_no 	 		= trim($paramSet['mrn_no']);
			$mrn_date 	 		= date('Y-m-d', strtotime($paramSet['mrn_date']));
		    $mrn_prefix = $paramSet['mrn_prefix'];
			$mrn_series = $paramSet['mrn_series'];

			$delivery_challan_no 	 		= (trim($paramSet['challan_no']))?trim($paramSet['challan_no']):'';
			$delivery_challan_date 	 		= (trim($paramSet['challan_date']))?date('Y-m-d', strtotime($paramSet['challan_date'])):'';

			$receiving_mode 	 			= $paramSet['receiving_mode'];


			$customer_contact 	 			= trim($paramSet['customer_contact']);
			$customer_id 					= 0;
			$customer_name 					= trim($paramSet['customer_name']);
			$customer_address 				= trim($paramSet['customer_address']);


			$frieght_amount 				= trim($paramSet['frieght_amount']);
			$frieght_invoice_no 			= trim($paramSet['frieght_invoice_no']);
			$frieght_bill_date 				= date('Y-m-d', strtotime($paramSet['frieght_bill_date'])); 
			

			// echo "<pre>";
			// print_r($paramSet);
			// exit;


			if(isset($paramSet['customer_contact']) && !empty(trim($paramSet['customer_contact']))) {

				$customer_contact 	 = trim($paramSet['customer_contact']);
				
				// Check Customer
				$customerDetail = $this->_getSelectList('table_customer AS C',"C.customer_id",''," C.customer_phone_no ='".$customer_contact."' OR C.customer_phone_no2 = '".$customer_contact."' OR  C.customer_phone_no3='".$customer_contact."'");

				if(isset($customerDetail[0]->customer_id)) {

					$customer_id = $customerDetail[0]->customer_id;
					$customer_name = $customerDetail[0]->customer_name;
					$customer_address = $customerDetail[0]->customer_address;

				} else {

					// Create a Customer


					$customer = array();
					$customer['account_id'] 			=  $_SESSION['accountId'];	
					$customer['customer_name'] 			=  $customer_name;
					$customer['customer_address'] 		=  $customer_address;
					$customer['customer_phone_no'] 		=  $customer_contact;	
					$customer['last_update_date'] 		=  date('Y-m-d');
					$customer['last_update_status'] 	=  'New';
					$customer['start_date'] 			=  date('Y-m-d');
					$customer['end_date'] 				= date('Y-m-d', strtotime('+2 years', strtotime(date('Y-m-d'))));
					$customer['status'] 		        =  'I';		

					$customer_id = $this->_dbInsert($customer,'table_customer');  

				}

			}



			if(isset($paramSet['bsn']) && sizeof($paramSet['bsn'])>0) {

			
				if(isset($_REQUEST['id']) && intval($_REQUEST['id'])>0) {
					$requestMRNID = trim($_REQUEST['id']);
					$mrnDetail = $this->_getSelectList('table_wh_mrn AS MRN',"MRN.mrn_id, MRN.mrn_no, MRN.mrn_date",''," MRN.mrn_id ='".$requestMRNID."'");

				} else {

					// $mrnDetail = $this->_getSelectList('table_wh_mrn AS MRN',"MRN.mrn_id, MRN.mrn_no, MRN.mrn_date",''," MRN.warehouse_id = '".$warehouse_id."' AND  MRN.mrn_no='".$mrn_no."' AND  MRN.mrn_date='".$mrn_date."' ");

					// Get the MRN NO At Runtime

					 $mrnDetail = $this->_getSelectList('table_wh_mrn AS MRN',"MRN.mrn_id, MRN.mrn_no, MRN.mrn_date",''," MRN.mrn_no='".$mrn_no."'");

					 if(isset($mrnDetail[0]->mrn_id) && $mrnDetail[0]->mrn_id>0) {

					 	  $mrnSeriesData = $this->getWarehouseLastMRNSeriesNo($warehouse_id);

						  if(sizeof($mrnSeriesData['data'])>0 && isset($mrnSeriesData['data']['mrn_no'])) {
						    	// echo "<pre>";
						    	// print_r($mrnSeriesData);

						       $mrn_no = $mrnSeriesData['data']['mrn_no'];
						       $mrn_prefix = $mrnSeriesData['data']['mrn_prefix'];
						       $mrn_series = $mrnSeriesData['data']['mrn_series'];
						  }
					 }

					 $mrnDetail = array();
				}


				// echo "<pre>";
				// print_r($mrnDetail);
				// exit;

				if(isset($mrnDetail[0]->mrn_id) && $mrnDetail[0]->mrn_id>0) {

						$mrn_id = $mrnDetail[0]->mrn_id;

					// Update MRN 

					// // Check While Warehouse Add New MRN
					// if(!isset($_REQUEST['id'])) {
					// 	return array('status'=>'failed', 'msg'=>'Duplicate MRN Number & Date!');
					// }

					// Check if Warehouse would like to change the MRN NO/DATE When edit MRN
					if(isset($_REQUEST['id']) && intval($_REQUEST['id'])>0) {
						$mrnDetail2 = $this->_getSelectList('table_wh_mrn AS MRN',"MRN.mrn_id",''," MRN.mrn_id!='".$requestMRNID."' AND  MRN.mrn_no='".$mrn_no."'");

						if(isset($mrnDetail2[0]->mrn_id) && $mrnDetail2[0]->mrn_id>0) {
							return array('status'=>'failed', 'msg'=>'Duplicate MRN Number!');
						} else {


							

								$mrn = array();
								$mrn['mrn_no'] 					=  $mrn_no;
								$mrn['mrn_date'] 				=  $mrn_date;
								//$mrn['service_distributor_id']  =  $distributor_id;	
								$mrn['challan_no']       		=  $delivery_challan_no;
								$mrn['challan_date'] 		    =  $delivery_challan_date;	
								$mrn['receiving_mode'] 			=  $receiving_mode;
								$mrn['customer_id'] 			=  $customer_id;
								$mrn['customer_name'] 			=  $customer_name;
								$mrn['customer_address'] 		=  $customer_address;
								$mrn['customer_contact'] 		=  $customer_contact;
								$mrn['frieght_amount'] 			=  $frieght_amount;
								$mrn['frieght_invoice_no'] 		=  $frieght_invoice_no;
								$mrn['frieght_bill_date'] 		=  $frieght_bill_date;
								$mrn['mrn_remarks'] 			=  '';
								$mrn['last_update_date'] 		=  date('Y-m-d');		

								$this->_dbUpdate($mrn,'table_wh_mrn', " mrn_id='".$mrn_id."'");  


						}
					}


				// } else if(sizeof(array_filter($paramSet['bsn']))>0 && sizeof(array_filter($paramSet['item_id']))>0 && isset($paramSet['item_id'][0]) && $paramSet['item_id'][0]>0 && isset($paramSet['manufacture_date'][0]) && strtotime(date('Y-m-d')) >= strtotime($paramSet['manufacture_date'][0]) && isset($paramSet['warranty_end_date'][0]) && strtotime(date('Y-m-d')) <= strtotime($paramSet['warranty_end_date'][0])){

				} else if(sizeof(array_filter($paramSet['bsn']))>0){					

					// print_r($paramSet['bsn']);
					// exit;

					// Add MRN Master Data

					$mrn = array();
					$mrn['account_id'] 				=  $_SESSION['accountId'];	
					$mrn['mrn_series'] 				=  $mrn_series;
					$mrn['mrn_prefix'] 				=  $mrn_prefix;
					$mrn['mrn_no'] 					=  $mrn_no;
					$mrn['mrn_date'] 				=  $mrn_date;
					$mrn['warehouse_id'] 			=  $warehouse_id;	
					$mrn['service_distributor_id']  =  $distributor_id;	
					$mrn['challan_no']       		=  $delivery_challan_no;
					$mrn['challan_date'] 		    =  $delivery_challan_date;	
					$mrn['receiving_mode'] 			=  $receiving_mode;
					$mrn['customer_id'] 			=  $customer_id;
					$mrn['customer_name'] 			=  $customer_name;
					$mrn['customer_address'] 		=  $customer_address;
					$mrn['customer_contact'] 		=  $customer_contact;
					$mrn['frieght_amount'] 			=  $frieght_amount;
					$mrn['frieght_invoice_no'] 		=  $frieght_invoice_no;
					$mrn['frieght_bill_date'] 		=  $frieght_bill_date;
					$mrn['mrn_remarks'] 			=  '';
					$mrn['last_update_date'] 		=  date('Y-m-d');	
					$mrn['created_datetime'] 		=  date('Y-m-d H:i:s');	
					$mrn['status'] 		        =  'A';		

					$mrn_id = $this->_dbInsert($mrn,'table_wh_mrn');  

				} else {
					return array('status'=>'failed', 'msg'=>'No BSN Selected!');
				}

// push to tally xml code
				 $wh_name=$this->_getSelectList2('table_warehouse AS w',"warehouse_name,warehouse_address,warehouse_phone_no,state_id,city_id",''," w.warehouse_id='".$warehouse_id."'  ");
				
                 $dist_name=$this->_getSelectList2('table_distributors AS d',"distributor_name,distributor_code,state,city,distributor_address,distributor_phone_no",''," d.distributor_id='".$distributor_id."'  ");
             //$warehouse_name=$wh_name[0]->warehouse_name;
           
            $state_id=$this->_getSelectList2('state AS s',"state_name",''," s.state_id='".$dist_name[0]->state."'  ");
              $city_id=$this->_getSelectList2('city AS c',"city_name",''," c.city_id='".$dist_name[0]->city."'  ");
               $markup_value=$this->_getSelectList2('table_markup AS m',"price",''," m.warehouse_id='".$warehouse_id."'  ");
                  
                   $name=$dist_name[0]->distributor_name;
                $dist_code  =$dist_name[0]->distributor_code;



               $markuprate=$markup_value[0]->price;
               //$markuprate=14;
         
             $warehouse_name=$wh_name[0]->warehouse_name;
             $contact_no=$dist_name[0]->distributor_phone_no;
             $dist_address=$dist_name[0]->distributor_address;
             $state=$state_id[0]->state_name;
             $city=$city_id[0]->city_name;
          
            $wh_name=$warehouse_name;
	 	    $arr = explode(' ',trim($wh_name));
	 	    $challan=ucwords(strtolower($arr[1]));
	 	    $partyledger="Eastman Auto &amp; Power limited-".$challan;
            $voucher_type=ucwords(strtolower($arr[1]))." Receipt Note";
            //$ledgername=ucwords(strtolower($arr[1]))." GST Sales @ 18%";
             $ledgername="Receipt Note ". $challan;
             $MRNNO=$mrn_no;
              $dateFormat1 = date('Ymd'); // 20171202
            $DATE = $dateFormat1;
             //echo $partyledger;
            // exit;
             $requestXMLSTART='<ENVELOPE>
			<HEADER>
			<TALLYREQUEST>Import Data</TALLYREQUEST>
			</HEADER>
			<BODY>
			<IMPORTDATA>
			<REQUESTDESC>
			<REPORTNAME>Vouchers</REPORTNAME>
			<STATICVARIABLES>
			<SVCURRENTCOMPANY>EastmanLtd</SVCURRENTCOMPANY>
			</STATICVARIABLES>
			</REQUESTDESC>
			<REQUESTDATA>
			<TALLYMESSAGE xmlns:UDF="TallyUDF">

			<VOUCHER VCHTYPE="'.$voucher_type.'" ACTION="Create" OBJVIEW="Invoice Voucher View">

			<ADDRESS.LIST TYPE="String">
            <ADDRESS>'.$name.'</ADDRESS>
            <ADDRESS>'.$dist_address.'</ADDRESS>
            <ADDRESS>'.$city.' </ADDRESS>
            <ADDRESS> '.$state.'</ADDRESS>
            <ADDRESS>Mobile No'.$contact_no.'</ADDRESS>
            </ADDRESS.LIST>
			<BASICBUYERADDRESS.LIST TYPE="String">
			<BASICBUYERADDRESS>'.$name.'</BASICBUYERADDRESS>
			<BASICBUYERADDRESS>'.$dist_address.'</BASICBUYERADDRESS>
			<BASICBUYERADDRESS>'.$city.'</BASICBUYERADDRESS>
			<BASICBUYERADDRESS>'.$state.'</BASICBUYERADDRESS>
			<BASICBUYERADDRESS>Mobile No:'.$contact_no.'</BASICBUYERADDRESS>

			</BASICBUYERADDRESS.LIST>
			<OLDAUDITENTRYIDS.LIST TYPE="Number">
			<OLDAUDITENTRYIDS>-1</OLDAUDITENTRYIDS>
			</OLDAUDITENTRYIDS.LIST>
			<DATE>'.$DATE.'</DATE>
			<REFERENCEDATE>'.$DATE.'</REFERENCEDATE>
            <NARRATION>MRN NO IS '.$MRNNO.' </NARRATION>
			<GSTREGISTRATIONTYPE></GSTREGISTRATIONTYPE>
			<VATDEALERTYPE></VATDEALERTYPE>
			<TAXUNITNAME>Default Tax Unit</TAXUNITNAME>
			<CLASSNAME></CLASSNAME>
			<PARTYNAME>Eastman Auto &amp; Power limited</PARTYNAME>
			<VOUCHERTYPENAME>'.$voucher_type.'</VOUCHERTYPENAME>
			<REFERENCE></REFERENCE>
			<VOUCHERNUMBER></VOUCHERNUMBER>
			<PARTYLEDGERNAME>'.$name."(".$dist_code.")".'</PARTYLEDGERNAME>
			<BASICBASEPARTYNAME>'.$name."(".$dist_code.")".'</BASICBASEPARTYNAME>
			<CSTFORMISSUETYPE/>
			<CSTFORMRECVTYPE/>
			<FBTPAYMENTTYPE>Default</FBTPAYMENTTYPE>
			<PERSISTEDVIEW>Invoice Voucher View</PERSISTEDVIEW>
			<BASICBUYERNAME>'.$partyledger.'</BASICBUYERNAME>
			<BASICDATETIMEOFINVOICE>3-Aug-2017 at 15:58</BASICDATETIMEOFINVOICE>
			<BASICDATETIMEOFREMOVAL>3-Aug-2017 at 15:58</BASICDATETIMEOFREMOVAL>
			<VCHGSTCLASS/>
			<ENTEREDBY></ENTEREDBY>
			<DIFFACTUALQTY>No</DIFFACTUALQTY>
			<ISMSTFROMSYNC>No</ISMSTFROMSYNC>
			<ASORIGINAL>No</ASORIGINAL>
			<AUDITED>No</AUDITED>
			<FORJOBCOSTING>No</FORJOBCOSTING>
			<ISOPTIONAL>No</ISOPTIONAL>
			<EFFECTIVEDATE>20170801</EFFECTIVEDATE>
			<USEFOREXCISE>No</USEFOREXCISE>
			<ISFORJOBWORKIN>No</ISFORJOBWORKIN>
			<ALLOWCONSUMPTION>No</ALLOWCONSUMPTION>
			<USEFORINTEREST>No</USEFORINTEREST>
			<USEFORGAINLOSS>No</USEFORGAINLOSS>
			<USEFORGODOWNTRANSFER>No</USEFORGODOWNTRANSFER>
			<USEFORCOMPOUND>No</USEFORCOMPOUND>
			<USEFORSERVICETAX>No</USEFORSERVICETAX>
			<ISEXCISEVOUCHER>No</ISEXCISEVOUCHER>
			<EXCISETAXOVERRIDE>No</EXCISETAXOVERRIDE>
			<USEFORTAXUNITTRANSFER>No</USEFORTAXUNITTRANSFER>
			<EXCISEOPENING>No</EXCISEOPENING>
			<USEFORFINALPRODUCTION>No</USEFORFINALPRODUCTION>
			<ISTDSOVERRIDDEN>No</ISTDSOVERRIDDEN>
			<ISTCSOVERRIDDEN>No</ISTCSOVERRIDDEN>
			<ISTDSTCSCASHVCH>No</ISTDSTCSCASHVCH>
			<INCLUDEADVPYMTVCH>No</INCLUDEADVPYMTVCH>
			<ISSUBWORKSCONTRACT>No</ISSUBWORKSCONTRACT>
			<ISVATOVERRIDDEN>No</ISVATOVERRIDDEN>
			<IGNOREORIGVCHDATE>No</IGNOREORIGVCHDATE>
			<ISSERVICETAXOVERRIDDEN>No</ISSERVICETAXOVERRIDDEN>
			<ISISDVOUCHER>No</ISISDVOUCHER>
			<ISEXCISEOVERRIDDEN>No</ISEXCISEOVERRIDDEN>
			<ISEXCISESUPPLYVCH>No</ISEXCISESUPPLYVCH>
			<ISGSTOVERRIDDEN>No</ISGSTOVERRIDDEN>
			<GSTNOTEXPORTED>No</GSTNOTEXPORTED>
			<ISVATPRINCIPALACCOUNT>No</ISVATPRINCIPALACCOUNT>
			<ISSHIPPINGWITHINSTATE>No</ISSHIPPINGWITHINSTATE>
			<ISCANCELLED>No</ISCANCELLED>
			<HASCASHFLOW>Yes</HASCASHFLOW>
			<ISPOSTDATED>No</ISPOSTDATED>
			<USETRACKINGNUMBER>No</USETRACKINGNUMBER>
			<ISINVOICE>Yes</ISINVOICE>
			<MFGJOURNAL>No</MFGJOURNAL>
			<HASDISCOUNTS>Yes</HASDISCOUNTS>
			<ASPAYSLIP>No</ASPAYSLIP>
			<ISCOSTCENTRE>No</ISCOSTCENTRE>
			<ISSTXNONREALIZEDVCH>No</ISSTXNONREALIZEDVCH>
			<ISEXCISEMANUFACTURERON>No</ISEXCISEMANUFACTURERON>
			<ISBLANKCHEQUE>No</ISBLANKCHEQUE>
			<ISVOID>No</ISVOID>
			<ISONHOLD>No</ISONHOLD>
			<ORDERLINESTATUS>No</ORDERLINESTATUS>
			<VATISAGNSTCANCSALES>No</VATISAGNSTCANCSALES>
			<VATISPURCEXEMPTED>No</VATISPURCEXEMPTED>
			<ISVATRESTAXINVOICE>No</ISVATRESTAXINVOICE>
			<VATISASSESABLECALCVCH>No</VATISASSESABLECALCVCH>
			<ISVATDUTYPAID>Yes</ISVATDUTYPAID>
			<ISDELIVERYSAMEASCONSIGNEE>No</ISDELIVERYSAMEASCONSIGNEE>
			<ISDISPATCHSAMEASCONSIGNOR>No</ISDISPATCHSAMEASCONSIGNOR>
			<ISDELETED>No</ISDELETED>
			<CHANGEVCHMODE>No</CHANGEVCHMODE>
			<ALTERID></ALTERID>
			<MASTERID></MASTERID>
			<VOUCHERKEY></VOUCHERKEY>
			<EXCLUDEDTAXATIONS.LIST></EXCLUDEDTAXATIONS.LIST>
			<OLDAUDITENTRIES.LIST></OLDAUDITENTRIES.LIST>
			<ACCOUNTAUDITENTRIES.LIST></ACCOUNTAUDITENTRIES.LIST>
			<AUDITENTRIES.LIST></AUDITENTRIES.LIST>
			<DUTYHEADDETAILS.LIST></DUTYHEADDETAILS.LIST>
			<SUPPLEMENTARYDUTYHEADDETAILS.LIST></SUPPLEMENTARYDUTYHEADDETAILS.LIST>
			<INVOICEDELNOTES.LIST></INVOICEDELNOTES.LIST>
			<INVOICEORDERLIST.LIST></INVOICEORDERLIST.LIST>
			<INVOICEINDENTLIST.LIST></INVOICEINDENTLIST.LIST>
			<ATTENDANCEENTRIES.LIST></ATTENDANCEENTRIES.LIST>
			<ORIGINVOICEDETAILS.LIST></ORIGINVOICEDETAILS.LIST>
			<INVOICEEXPORTLIST.LIST></INVOICEEXPORTLIST.LIST>
			<LEDGERENTRIES.LIST>
			<OLDAUDITENTRYIDS.LIST TYPE="Number">
			<OLDAUDITENTRYIDS>-1</OLDAUDITENTRYIDS>
			</OLDAUDITENTRYIDS.LIST>
			<LEDGERNAME>'.$partyledger.'</LEDGERNAME>
			<GSTCLASS/>
			<ISDEEMEDPOSITIVE>No</ISDEEMEDPOSITIVE>
			<LEDGERFROMITEM>No</LEDGERFROMITEM>
			<REMOVEZEROENTRIES>No</REMOVEZEROENTRIES>
			<ISPARTYLEDGER>Yes</ISPARTYLEDGER>
			<ISLASTDEEMEDPOSITIVE>No</ISLASTDEEMEDPOSITIVE>
			<AMOUNT></AMOUNT>
			<SERVICETAXDETAILS.LIST></SERVICETAXDETAILS.LIST>

			<BILLALLOCATIONS.LIST></BILLALLOCATIONS.LIST>
			<INTERESTCOLLECTION.LIST></INTERESTCOLLECTION.LIST>
			<OLDAUDITENTRIES.LIST></OLDAUDITENTRIES.LIST>
			<ACCOUNTAUDITENTRIES.LIST></ACCOUNTAUDITENTRIES.LIST>
			<AUDITENTRIES.LIST></AUDITENTRIES.LIST>
			<INPUTCRALLOCS.LIST></INPUTCRALLOCS.LIST>
			<DUTYHEADDETAILS.LIST></DUTYHEADDETAILS.LIST>
			<EXCISEDUTYHEADDETAILS.LIST></EXCISEDUTYHEADDETAILS.LIST>
			<RATEDETAILS.LIST></RATEDETAILS.LIST>
			<SUMMARYALLOCS.LIST></SUMMARYALLOCS.LIST>
			<STPYMTDETAILS.LIST></STPYMTDETAILS.LIST>
			<EXCISEPAYMENTALLOCATIONS.LIST></EXCISEPAYMENTALLOCATIONS.LIST>
			<TAXBILLALLOCATIONS.LIST></TAXBILLALLOCATIONS.LIST>
			<TAXOBJECTALLOCATIONS.LIST></TAXOBJECTALLOCATIONS.LIST>
			<TDSEXPENSEALLOCATIONS.LIST></TDSEXPENSEALLOCATIONS.LIST>
			<VATSTATUTORYDETAILS.LIST></VATSTATUTORYDETAILS.LIST>
			<COSTTRACKALLOCATIONS.LIST></COSTTRACKALLOCATIONS.LIST>
			<REFVOUCHERDETAILS.LIST></REFVOUCHERDETAILS.LIST>
			<INVOICEWISEDETAILS.LIST></INVOICEWISEDETAILS.LIST>
			<VATITCDETAILS.LIST></VATITCDETAILS.LIST>
			<ADVANCETAXDETAILS.LIST></ADVANCETAXDETAILS.LIST>
			</LEDGERENTRIES.LIST>
			<LEDGERENTRIES.LIST>

				<TAXCLASSIFICATIONNAME/>
				<LEDGERNAME>'.$partyledger.'</LEDGERNAME>
				<METHODTYPE>As User Defined Value</METHODTYPE>
				<VOUCHERFBTCATEGORY/>
				<GSTCLASS/>
				<ISDEEMEDPOSITIVE>No</ISDEEMEDPOSITIVE>
				<LEDGERFROMITEM>No</LEDGERFROMITEM>
				<REMOVEZEROENTRIES>No</REMOVEZEROENTRIES>
				<ISPARTYLEDGER>No</ISPARTYLEDGER>
				<ISLASTDEEMEDPOSITIVE>No</ISLASTDEEMEDPOSITIVE>
				<SERVICETAXDETAILS.LIST></SERVICETAXDETAILS.LIST>
				<BANKALLOCATIONS.LIST></BANKALLOCATIONS.LIST>
				<BILLALLOCATIONS.LIST></BILLALLOCATIONS.LIST>
				<INTERESTCOLLECTION.LIST></INTERESTCOLLECTION.LIST>
				<OLDAUDITENTRIES.LIST></OLDAUDITENTRIES.LIST>
				<ACCOUNTAUDITENTRIES.LIST></ACCOUNTAUDITENTRIES.LIST>
				<AUDITENTRIES.LIST></AUDITENTRIES.LIST>
				<INPUTCRALLOCS.LIST></INPUTCRALLOCS.LIST>
				<INVENTORYALLOCATIONS.LIST></INVENTORYALLOCATIONS.LIST>
				<DUTYHEADDETAILS.LIST></DUTYHEADDETAILS.LIST>
				<EXCISEDUTYHEADDETAILS.LIST></EXCISEDUTYHEADDETAILS.LIST>
				<RATEDETAILS.LIST></RATEDETAILS.LIST>
				<SUMMARYALLOCS.LIST></SUMMARYALLOCS.LIST>
				<STPYMTDETAILS.LIST></STPYMTDETAILS.LIST>
				<EXCISEPAYMENTALLOCATIONS.LIST></EXCISEPAYMENTALLOCATIONS.LIST>
				<TAXBILLALLOCATIONS.LIST></TAXBILLALLOCATIONS.LIST>
				<TAXOBJECTALLOCATIONS.LIST></TAXOBJECTALLOCATIONS.LIST>
				<TDSEXPENSEALLOCATIONS.LIST></TDSEXPENSEALLOCATIONS.LIST>
				<VATSTATUTORYDETAILS.LIST></VATSTATUTORYDETAILS.LIST>
				<REFVOUCHERDETAILS.LIST></REFVOUCHERDETAILS.LIST>
				<INVOICEWISEDETAILS.LIST></INVOICEWISEDETAILS.LIST>
				<VATITCDETAILS.LIST></VATITCDETAILS.LIST>
				<ADVANCETAXDETAILS.LIST></ADVANCETAXDETAILS.LIST>
				</LEDGERENTRIES.LIST>
				<LEDGERENTRIES.LIST>
				<TAXCLASSIFICATIONNAME/>
				<LEDGERNAME></LEDGERNAME>
				<METHODTYPE>As User Defined Value</METHODTYPE>
				<VOUCHERFBTCATEGORY/>
				<GSTCLASS/>
				<ISDEEMEDPOSITIVE>No</ISDEEMEDPOSITIVE>
				<LEDGERFROMITEM>No</LEDGERFROMITEM>
				<REMOVEZEROENTRIES>No</REMOVEZEROENTRIES>
				<ISPARTYLEDGER>No</ISPARTYLEDGER>
				<ISLASTDEEMEDPOSITIVE>No</ISLASTDEEMEDPOSITIVE>
				<SERVICETAXDETAILS.LIST></SERVICETAXDETAILS.LIST>
				<BANKALLOCATIONS.LIST></BANKALLOCATIONS.LIST>
				<BILLALLOCATIONS.LIST></BILLALLOCATIONS.LIST>
				<INTERESTCOLLECTION.LIST></INTERESTCOLLECTION.LIST>
				<OLDAUDITENTRIES.LIST></OLDAUDITENTRIES.LIST>
				<ACCOUNTAUDITENTRIES.LIST></ACCOUNTAUDITENTRIES.LIST>
				<AUDITENTRIES.LIST></AUDITENTRIES.LIST>
				<INPUTCRALLOCS.LIST></INPUTCRALLOCS.LIST>
				<INVENTORYALLOCATIONS.LIST></INVENTORYALLOCATIONS.LIST>
				<DUTYHEADDETAILS.LIST></DUTYHEADDETAILS.LIST>
				<EXCISEDUTYHEADDETAILS.LIST></EXCISEDUTYHEADDETAILS.LIST>
				<RATEDETAILS.LIST></RATEDETAILS.LIST>
				<SUMMARYALLOCS.LIST></SUMMARYALLOCS.LIST>
				<STPYMTDETAILS.LIST></STPYMTDETAILS.LIST>
				<EXCISEPAYMENTALLOCATIONS.LIST></EXCISEPAYMENTALLOCATIONS.LIST>
				<TAXBILLALLOCATIONS.LIST></TAXBILLALLOCATIONS.LIST>
				<TAXOBJECTALLOCATIONS.LIST></TAXOBJECTALLOCATIONS.LIST>
				<TDSEXPENSEALLOCATIONS.LIST></TDSEXPENSEALLOCATIONS.LIST>
				<VATSTATUTORYDETAILS.LIST></VATSTATUTORYDETAILS.LIST>
				<REFVOUCHERDETAILS.LIST></REFVOUCHERDETAILS.LIST>
				<INVOICEWISEDETAILS.LIST></INVOICEWISEDETAILS.LIST>
				<VATITCDETAILS.LIST></VATITCDETAILS.LIST>
				<ADVANCETAXDETAILS.LIST></ADVANCETAXDETAILS.LIST>
				</LEDGERENTRIES.LIST>';
							 
	$requestXMLEND='					
<PAYROLLMODEOFPAYMENT.LIST>      </PAYROLLMODEOFPAYMENT.LIST>
      <ATTDRECORDS.LIST>      </ATTDRECORDS.LIST>
     </VOUCHER>
    </TALLYMESSAGE>

   </REQUESTDATA>
  </IMPORTDATA>
 </BODY>
</ENVELOPE>';




                   //$itemiddd=array();
                $ALLINVENTORYENTRIESITEM=array();
                $itemsbsn=array();
                $bsn_name=array();
                $itemids=array();



				foreach ($paramSet['bsn'] as $key => $value) {
					# code...

					// Firslty Check BSN available into bsn master or not
					 $serialNumber  = $key;

					// echo "<pre>";
					// echo $paramSet['warranty_end_date_'.$serialNumber];
					// echo "<br>";
					// echo $paramSet['manufacture_date_'.$serialNumber];
					// print_r($paramSet);
					// //$completeItemKeyString = $itemDefaultString.$itemSerialNumber;
					// echo strtotime(date('Y-m-d')) .'<='. strtotime(date('Y-m-d', strtotime($paramSet['warranty_end_date_'.$serialNumber])));
					// echo "<pre>";
					// echo strtotime(date('Y-m-d')) .'<='. strtotime(date('Y-m-d', strtotime($paramSet['manufacture_date_'.$serialNumber])));
					// exit;


					//if(!empty($value) && isset($paramSet['item_id_'.$serialNumber]) && $paramSet['item_id_'.$serialNumber]>0 && isset($paramSet['manufacture_date_'.$serialNumber]) && strtotime(date('Y-m-d')) >= strtotime(date('Y-m-d', strtotime($paramSet['manufacture_date_'.$serialNumber]))) && isset($paramSet['warranty_end_date_'.$serialNumber]) && strtotime(date('Y-m-d')) <= strtotime(date('Y-m-d', strtotime($paramSet['warranty_end_date_'.$serialNumber])))){

					 if(!empty($value) && isset($paramSet['item_id_'.$serialNumber]) && $paramSet['item_id_'.$serialNumber]>0){

					// if(!empty($value) && isset($paramSet['item_id_'.$serialNumber]) && $paramSet['item_id_'.$serialNumber]>0 && isset($paramSet['manufacture_date_'.$serialNumber]) && strtotime(date('Y-m-d')) >= strtotime(date('Y-m-d', strtotime($paramSet['manufacture_date_'.$serialNumber]))) && isset($paramSet['warranty_end_date_'.$serialNumber])){

						$item_id = $paramSet['item_id_'.$serialNumber];

						$manufacture_date 	 		= date('Y-m-d', strtotime($paramSet['manufacture_date_'.$serialNumber]));
						$warranty_end_date 	 		= date('Y-m-d', strtotime($paramSet['warranty_end_date_'.$serialNumber]));

						// Document Receive Date

						$doc_received_date 	 		= date('Y-m-d', strtotime($paramSet['doc_received_date_'.$serialNumber]));
						$service_personnel_id		= $paramSet['service_personnel_id_'.$serialNumber];
						$complaint_id 	 			= $paramSet['complaint_id_'.$serialNumber];



						//echo $paramSet['doc_received_sts_1'];
						//echo "doc_received_sts_".$key+1;
						// $string1 = "doc_received_sts_";
						// $string2 = $key+1;
						// $string3 = $string1.$string2;
						//$doc_received_sts 	 		= trim($paramSet[$string3]);


						$doc_received_sts 	 		=  $paramSet['doc_received_sts_'.$serialNumber];


						$bsn = trim($value);
						$bsn_id = "";
						$item_id = "";
						$category_id = "";
						
						$bsnDetail = $this->_getSelectList('table_item_bsn AS BSN',"BSN.*",''," BSN.bsn ='".$bsn."'");
						
						// echo "<pre>";
						// print_r($bsnDetail);
						// exit;

						// if(isset($bsnDetail[0]->bsn_id)) {



						// 	// Update BSN table sale distributor and date of dispatch
						// 	if($bsnDetail->status !='C' || $bsnDetail->status !='DMG' || $bsnDetail->status !='RPL') {
						// 		$bsnArray = array();

						// 		$bsnArray['manufacture_date']    		= $manufacture_date;
						// 		$bsnArray['warranty_end_date']    		= $warranty_end_date;
						// 		$bsnArray['last_updated_date']	 		= date('Y-m-d');	
						// 		$bsnArray['last_update_datetime']		= date('Y-m-d H:i:s');	
						// 		//$bsnArray['status']							= 'C'; // Dispatched to distributor C-Complaint

						// 		$this->_dbUpdate($bsnArray,'table_item_bsn', " bsn='".$bsn."'");  

						// 	}


						// } else {


						// 	// Get the Item Details
						// 		$ItemDetail = $this->_getSelectList('table_item AS I',"I.item_id, I.category_id",''," I.item_id ='".$paramSet['item_id'][$key]."'");
						// 		$item_id = $ItemDetail[0]->item_id;
						// 		$category_id = $ItemDetail[0]->category_id;

						// 		// Create new stock entry of a BSN

						// 		$bsnArray = array();

						// 		$bsnArray['account_id']        		= $_SESSION['accountId'];	
						// 		$bsnArray['dispatch_warehouse_id']  = $warehouse_id;
						// 		$bsnArray['sale_distributor_id']    = $distributor_id;	
						// 		$bsnArray['item_id']           		= $item_id;
						// 		$bsnArray['category_id']       		= $category_id;
						// 		$bsnArray['batch_no']       		= '';
						// 		$bsnArray['bsn']       				= $bsn;
						// 		$bsnArray['stock_value']       		= 1;
						// 		$bsnArray['manufacture_date']       = $manufacture_date;
						// 		$bsnArray['warranty_period']       	= '';
						// 		$bsnArray['warranty_prorata']       = '';
						// 		$bsnArray['warranty_grace_period']  = '';
						// 		$bsnArray['warranty_end_date']      =  $warranty_end_date;
						// 		$bsnArray['user_type'] 		        =  (isset($_SESSION['userLoginType']))?$_SESSION['userLoginType']:0;	
						// 		$bsnArray['web_user_id'] 		    =  (isset($_SESSION['userLoginType']))?$_SESSION['PepUpSalesUserId']:0;
						// 		$bsnArray['last_updated_date']	 	= date('Y-m-d');	
						// 		$bsnArray['last_update_datetime']	= date('Y-m-d H:i:s');	
						// 		$bsnArray['created_datetime']		= date('Y-m-d H:i:s');	
						// 		$bsnArray['status']					= 'A';
								
						// 		$bsn_id = $this->_dbInsert($bsnArray,'table_item_bsn');  

						// }



								// 	bsn lifecycle activity data array 

								// $data = array();

								// $data['account_id'] 			=  $_SESSION['accountId'];	
								// $data['item_id'] 				=  $item_id;
								// $data['bsn_id'] 		        =  $bsn_id;
								// $data['bsn'] 		            =  $bsn;	
								// $data['from_warehouse_id']      =  0;	
								// $data['to_warehouse_id']        =  $warehouse_id;
								// $data['distributor_id'] 		=  $distributor_id;	
								// $data['bill_date'] 				=  '';
								// $data['bill_no'] 				=  '';
								// $data['user_type'] 		        =  $_SESSION['userLoginType'];	
								// $data['web_user_id'] 		    =  $_SESSION['PepUpSalesUserId'];	
								// $data['created_datetime'] 		=  date('Y-m-d H:i:s');	
								// $data['activity_type'] 		    =  'C'; // D- Dispatched, R-Return, C-Complain
								// $data['status'] 		        =  'A';		

								// $this->_dbInsert($data,'table_item_bsn_lifecycle_activity');  



								// Add Complaint Battery To MRN Detail Table


							if(isset($bsnDetail[0]->bsn_id)) {


								$bsn_id = $bsnDetail[0]->bsn_id;
								$item_id = $bsnDetail[0]->item_id;
								$category_id = $bsnDetail[0]->category_id;

								//$warranty_end_date = $bsnDetail[0]->warranty_end_date;

								$condi = " MRND.bsn_id ='".$bsn_id."' AND MRND.status IN ('P', 'I')";
								$resultset = $this->_getSelectList2('table_wh_mrn_detail AS MRND INNER JOIN table_wh_mrn AS MRN ON MRN.mrn_id = MRND.mrn_id ','MRND.mrn_detail_id, MRND.status, MRN.warehouse_id ','',$condi);


								$bsnArrayIDs = array();
 								$bsnArrayIDs[] = $bsn_id;

								if(is_array($resultset) && sizeof($resultset)>0)  {   

									$mrnDtsStatus = $resultset[0]->status;

									// Update Warehouse dispatches to distributor activity
									$mrnDetail = array();
									$mrnDetail['manfacturer_date']        	= $manufacture_date;
									$mrnDetail['bsn_warranty_end_date']     = $warranty_end_date;
									$mrnDetail['doc_received_date']   		= $doc_received_date;	
									$mrnDetail['doc_received_sts']   		= $doc_received_sts;
									$mrnDetail['service_personnel_id']		= $service_personnel_id;

									if(isset($_GET['action']) && $_GET['action'] == 'edit'){
										if(isset($_SESSION['warehouseId']) && $_SESSION['warehouseId']>0){
											if($_SESSION['warehouseId'] == $resultset[0]->warehouse_id)
												$this->_dbUpdate2($mrnDetail,'table_wh_mrn_detail', 'mrn_detail_id="'.$resultset[0]->mrn_detail_id.'"');
										} else {
												//$this->_dbUpdate2($mrnDetail,'table_wh_mrn_detail', 'mrn_detail_id="'.$resultset[0]->mrn_detail_id.'"');
										}

									} else {
										$msgs = "This Battery Serial No. ".$bsn." already in process!";
										return array('status'=>'failed', 'msg'=>$msgs);
									}

								} else {
									
									
									// Add MRN BSN DETAIL
									$mrnDetail = array();

									$mrnDetail['mrn_id']      			    = $mrn_id;	
									$mrnDetail['bsn_id']                    = $bsn_id;
									$mrnDetail['bsn']               		= $bsn;
									$mrnDetail['batch_no']  				= '';
									$mrnDetail['item_id']                   = $item_id;
									$mrnDetail['category_id']               = $category_id;
									$mrnDetail['complaint_id']     			= $complaint_id;
									$mrnDetail['color_id']        			= $color_id;
									$mrnDetail['brand_id']        			= $brand_id;
									$mrnDetail['manfacturer_date']        	= date('Y-m-d',strtotime($manufacture_date));
									$mrnDetail['bsn_warranty_end_date']     = $warranty_end_date;
									$mrnDetail['service_personnel_id']		= $service_personnel_id;
									$mrnDetail['doc_received_date']   		= $doc_received_date;	
									$mrnDetail['doc_received_sts']   		= $doc_received_sts;
									if($warranty_end_date=='1970-01-01' || $warranty_end_date=='')
									{
										$mrnDetail['bsn_reg'] 			= 'N';	
									}
									else
									{
										$mrnDetail['bsn_reg'] 			= 'Y';		
									}
									$mrnDetail['created_datetime'] 			= '';
									$mrnDetail['status']  					= 'I';


									$mrnDtsStatus = 'P';

   $item_details=$this->_getSelectList2('table_item_bsn AS Ib',"item_id",''," Ib.bsn='".$bsn."'  ");
                                   $itemid=$item_details[0]->item_id;
                                   $itemids[]=$item_details[0]->item_id;


                                  		
							if(isset($itemsbsn[$itemid]))
							{
							    $count_bsn = (int)$itemsbsn[$itemid]['count']+1;
							 	$itemsbsn[$itemid]['count'] = $count_bsn;
							 	$bsn_name[$itemid][BSN]=$bsn;
								
							}
							 else
							{
								$bsn_name[$itemid][BSN]=$bsn;
							 	$itemsbsn[$itemid]['count'] = 1;	
						    }






									$mrn_detail_id = $this->_dbInsert($mrnDetail,'table_wh_mrn_detail');

									if($complaint_id > 0)
									{
										$getBCFId = $this->_getSelectList('table_wh_mrn_bcf as BCF','bcf_id','',' bsn="'.$bsn.'" and BCF.status="I"');
										$updateMRND['mrn_detail_id'] = $mrn_detail_id;
										$updateMRND['mrn_id'] = $mrn_id;
										$this->_dbUpdate2($updateMRND, "table_wh_mrn_bcf"," bcf_id='".$getBCFId[0]->bcf_id."'");
									}

									//Creating of BCF Automatically
									/************* Creating of BCF Automatically when Assigned to Service Personnel **********/
									$bsnDetail = $this->_getSelectList2('table_item_bsn AS BSN 
									LEFT JOIN table_item AS I ON I.item_id = BSN.item_id  
									LEFT JOIN table_segment AS SEG ON SEG.segment_id = I.item_segment 
									LEFT JOIN table_distributors AS D ON D.distributor_id = BSN.sale_distributor_id
									LEFT JOIN table_retailer AS R ON R.retailer_id = BSN.sale_retailer_id
									LEFT JOIN table_customer AS C ON C.customer_id = BSN.customer_id
									LEFT JOIN table_brands AS BRND ON BRND.brand_id = I.brand_id
									LEFT JOIN table_category as CAT ON CAT.category_id = I.category_id
									LEFT JOIN table_order as ORDER1 ON BSN.order_id = ORDER1.order_id
									LEFT JOIN table_complaint as COMPLAINT on COMPLAINT.bsn_id = BSN.bsn_id
									LEFT JOIN table_order_invoice_bsn_tracking as TOIBT on TOIBT.bsn_id = BSN.bsn_id
									LEFT JOIN table_customer as CUSTOMER on CUSTOMER.customer_id = BSN.customer_id
									LEFT JOIN country as CNTRY on CNTRY.country_id = CUSTOMER.country
									LEFT JOIN table_region as REG on REG.region_id = CUSTOMER.region_id
									LEFT JOIN table_zone as ZN on ZN.zone_id = CUSTOMER.zone
									LEFT JOIN state as ST on ST.state_id = CUSTOMER.state
									LEFT JOIN city as CT on CT.city_id = CUSTOMER.city
									LEFT JOIN table_wh_mrn_detail as MRNDtl on MRNDtl.bsn_id = BSN.bsn_id 
									LEFT JOIN table_wh_mrn as MRN on MRN.mrn_id = MRNDtl.mrn_id
									','BSN.date_of_sale, BSN.item_id,BSN.bsn, I.item_name, I.item_code, I.item_prodata, I.item_warranty,BSN.bsn_id, BSN.warranty_period, BSN.warranty_prorata,BSN.batch_no,BSN.manufacture_date,BSN.warranty_end_date,BSN.date_of_sale, SEG.segment_name, SEG.segment_id, BSN.sale_distributor_id, BSN.sale_retailer_id, D.distributor_name, D.distributor_code, R.retailer_name, R.retailer_code, C.customer_name, C.customer_phone_no, C.customer_address, C.zipcode, C.customer_email,C.customer_address2,BRND.brand_id,BRND.brand_name,CAT.category_name,ORDER1.bill_no,ORDER1.bill_date,COMPLAINT.created_date,CUSTOMER.*,CNTRY.country_name,REG.region_name,ZN.zone_name,ST.state_name,CT.city_name,TOIBT.challan_no,TOIBT.challan_date,MRN.mrn_no,MRN.mrn_date,MRN.mrn_id,MRNDtl.mrn_detail_id,CAT.category_id,COMPLAINT.complaint_id,COMPLAINT.created_date as complaint_date,COMPLAINT.remark as complaint_remarks,MRN.service_distributor_id as SERDIS,CUSTOMER.customer_id as CUST_ID','',"  LOWER(BSN.bsn) = '".strtolower(trim($bsn))."' and BSN.status IN ('S','D') ORDER BY BSN.bsn");

									$check_existing_bcf = $this->_getSelectList2('table_wh_mrn_bcf as BCF','*','',' bsn="'.$bsn.'" and mrn_id="'.$mrn_id.'" and BCF.status !="C"');
									
									if(count($check_existing_bcf)<=0)
									{
										// Check BCF Series/Prefix at runtime
										$bcfSeriesData = $this->getWarehouseLastBCFSeriesNo();

											$bcf_prefix = $bcfSeriesData['data']['bcf_prefix'];
											$bcf_series = $bcfSeriesData['data']['bcf_series'];
											$bcf_no		= $bcfSeriesData['data']['bcf_no'];
										 
										$bcf_detail['account_id'] 					= $_SESSION['accountId'];
										$bcf_detail['mrn_id'] 	  					= $mrn_id;
										$bcf_detail['mrn_detail_id']				= $mrn_detail_id;
										$bcf_detail['bcf_series']					= $bcf_series;
										$bcf_detail['bcf_prefix']					= $bcf_prefix;
										$bcf_detail['bcf_no']						= $bcf_no;
										$bcf_detail['bcf_date']						= date('Y-m-d');
										$bcf_detail['service_personnel_id']			= $service_personnel_id;
										$bcf_detail['warehouse_id']					= $_SESSION['warehouseId'];
										$bcf_detail['service_distributor_id']		= $bsnDetail[0]->SERDIS;
										$bcf_detail['sale_distributor_id']			= $bsnDetail[0]->sale_distributor_id;
										$bcf_detail['customer_id']					= $bsnDetail[0]->CUST_ID;
										$bcf_detail['complaint_id']					= $bsnDetail[0]->complaint_id;
										$bcf_detail['bsn_id']						= $bsnDetail[0]->bsn_id;
										$bcf_detail['item_id']						= $bsnDetail[0]->item_id;
										$bcf_detail['category_id']					= $bsnDetail[0]->category_id;
										$bcf_detail['brand_id']						= $bsnDetail[0]->brand_id;
										$bcf_detail['color_id']						= $bsnDetail[0]->color_id;
										$bcf_detail['bsn']							= $bsnDetail[0]->bsn;
										$bcf_detail['batch_no']						= $bsnDetail[0]->batch_no;
										$bcf_detail['manfacturer_date']				= $bsnDetail[0]->manufacture_date;
										$bcf_detail['bsn_warranty_end_date']		= $bsnDetail[0]->warranty_end_date;
										$bcf_detail['EAPL_Invoice_no']				= $bsnDetail[0]->bill_no;
										$bcf_detail['EAPL_Invoice_date']			= $bsnDetail[0]->bill_date;
										$bcf_detail['date_of_sale']					= $bsnDetail[0]->date_of_sale;
										$bcf_detail['complaint_date']				= $bsnDetail[0]->complaint_date;
										$bcf_detail['challan_no']					= $bsnDetail[0]->challan_no;
										$bcf_detail['challan_date']					= $bsnDetail[0]->challan_date;
										$bcf_detail['customer_name']				= $bsnDetail[0]->customer_name;
										$bcf_detail['cus_state_id']					= $bsnDetail[0]->state;
										$bcf_detail['cus_city_id']					= $bsnDetail[0]->city;
										$bcf_detail['customer_phone_no']			= $bsnDetail[0]->customer_phone_no;
										$bcf_detail['customer_email_id']			= $bsnDetail[0]->customer_email;
										$bcf_detail['customer_address']				= $bsnDetail[0]->customer_address;
										$bcf_detail['customer_address2']			= $bsnDetail[0]->customer_address2;
										$bcf_detail['customer_contact']				= $bsnDetail[0]->customer_phone_no2;
										$bcf_detail['customer_complaint_remarks']	= $bsnDetail[0]->complaint_remarks;
										$bcf_detail['last_update_date']				= date('Y-m-d');
										$bcf_detail['created_datetime']				= date('Y-m-d H:i:s');
										$bcf_detail['status']						= 'I';		//Setting BCF in IN-PROCESS mode

										$ids = $this->_dbInsert($bcf_detail,'table_wh_mrn_bcf');
									}
									
								/************* Creating of BCF Automatically when Assigned to Service Personnel ***********/

									// Check if there any pending BCF 
									//$BCFresultset = $this->_getSelectList2('table_wh_mrn_bcf AS BCF ','BCF.bcf_id','',' BCF.bsn = "'.trim($bsn).'" AND BCF.status = "P" AND ');



								}



								// Update BSN Status from warehouse Stock or BSN Master
		 

								$condi = " warehouse_id = '".$warehouse_id."' AND bsn_id ='".$bsn_id."'";
								$resultset = $this->_getSelectList('table_warehouse_stock AS WS','WS.warehouse_stock_id, WS.status ','',$condi);

								if(is_array($resultset) && sizeof($resultset)>0)  { 

									// Update Warehouse BSN Status
									$whStock = array();
									if($resultset[0]->status == 'S' || $resultset[0]->status == 'D' || $resultset[0]->status == 'A') {
										$whStock['last_updated_date']	 		=  date('Y-m-d');	
										$whStock['last_update_datetime'] 		=  date('Y-m-d H:i:s');

										if($mrnDtsStatus == 'P') // If BSN Status is pending 
											$whStock['status']		            	=  'C'; //C-Complaint

										$this->_dbUpdate($whStock,'table_warehouse_stock', 'warehouse_stock_id="'.$resultset[0]->warehouse_stock_id.'"');
									} else if($resultset[0]->status == 'D') {
										return array('status'=>'failed', 'msg'=>'This BSN('.$bsn.') Already Discpatched');
									} else if($resultset[0]->status == 'I') {
										return array('status'=>'failed', 'msg'=>'Please Accept/Scan BSN('.$bsn.') Before Discpatches');
									} else if($resultset[0]->status == 'S') {
										return array('status'=>'failed', 'msg'=>'BSN('.$bsn.') Already Sold');
									} else if($resultset[0]->status == 'DMG') {
										return array('status'=>'failed', 'msg'=>'BSN('.$bsn.') Damage');
									}



								} else {

									// 	Add New BSN Warehouse Stock 

									$whStock = array();

									$whStock['account_id'] 				=  $_SESSION['accountId'];	
									$whStock['warehouse_id'] 		    =  $warehouse_id;
									$whStock['item_id'] 				=  $item_id;
									$whStock['bsn_id'] 		        	=  $bsn_id;
									$whStock['stock_value']       		=  1;
									$whStock['last_updated_date']	 	=  date('Y-m-d');	
									$whStock['last_update_datetime']	=  date('Y-m-d H:i:s');	
									$whStock['created_datetime']		=  date('Y-m-d H:i:s');	
									$whStock['status']		            =  'C'; //Complaint

									$this->_dbInsert($whStock,'table_warehouse_stock');  

								}  
							} else {
							}

					} else {
						//return array('status'=>'failed', 'msg'=>'Sorry Item Name can not be empty!');
						return array('status'=>'failed', 'msg'=>'BSN Not Found OR Please Check BSN manufacture/warranty dates!');

					}


				}

//MRN push in Tally


            $ALLINVENTORYENTRIESITEM=array();
            $itemnameis=array_unique($itemids);

            $itemname= array_values(array_filter($itemnameis));

            for($j=0;$j<count($itemname);$j++)
            {
           
            
            //$item_details=$this->_getSelectList2('table_item AS I',"item_name,item_division",''," I.item_id='".$itemname[$j]."'  ");

             $item_details=$this->_getSelectList2('table_item AS I left join table_price AS P ON P.item_id=I.item_id',"item_name,item_division,item_dp",''," I.item_id='".$itemname[$j]."'  ");
           // print_r($item_details[0]->item_name);exit;
            $STOCKITEM="Def.(Scrap)".$item_details[0]->item_name;
             $price=$item_details[0]->item_dp;
            if($item_details[0]->item_division==3)
				{
		            $godown_name="CHN ". ucwords(strtolower($arr[1]))." Defective";
		                                  //print_R($godown_name);exit;

				}   else if($item_details[0]->item_division==1)
			    {
					$godown_name="AMPS ". ucwords(strtolower($arr[1]))." Defective";

				}   else if($item_details[0]->item_division==2)
				{
					$godown_name="Inst ". ucwords(strtolower($arr[1]))." Defective";
			    }
           

			        if(isset($itemsbsn[$itemname[$j]]))
			    {
	                $ACTUALQTY= $itemsbsn[$itemname[$j]]['count'];
	                $BASICUSERDESCRIPTION=$bsn_name[$itemname[$j]]['BSN'];
	                //$sd=implode(' ',$BASICUSERDESCRIPTION);
	               /*  $amount=$ACTUALQTY * $price ;
	                $mamount= $amount * $markuprate/100;
	                $amount="-".$amount;
	                $totalamount=$mamount + $amount;
	                $totalamount= "-".$totalamount; */
	                 $mamount=$price * $markuprate/100;
	                 
	                $RATE=$mamount + $price;
	                $amount=$ACTUALQTY * $RATE ;
	                $totalamount= "-".$amount;


                }

                                                                
                   $ALLINVENTORYENTRIESITEM[] ='
													<ALLINVENTORYENTRIES.LIST>
													 <BASICUSERDESCRIPTION.LIST TYPE="String">
                                                   <BASICUSERDESCRIPTION>'.$BASICUSERDESCRIPTION.'</BASICUSERDESCRIPTION>
                                                     </BASICUSERDESCRIPTION.LIST>
													<STOCKITEMNAME>'.$STOCKITEM.'</STOCKITEMNAME>
													<ISDEEMEDPOSITIVE>Yes</ISDEEMEDPOSITIVE>
													<ISLASTDEEMEDPOSITIVE>Yes</ISLASTDEEMEDPOSITIVE>
													<ISAUTONEGATE>No</ISAUTONEGATE>
													<ISCUSTOMSCLEARANCE>No</ISCUSTOMSCLEARANCE>
													<ISTRACKCOMPONENT>No</ISTRACKCOMPONENT>
													<ISTRACKPRODUCTION>No</ISTRACKPRODUCTION>
													<ISPRIMARYITEM>No</ISPRIMARYITEM>
													<ISSCRAP>No</ISSCRAP>
													<RATE>'.$RATE.'</RATE>
													<AMOUNT>'.$totalamount.'</AMOUNT>
													<ACTUALQTY>'.$ACTUALQTY.' nos</ACTUALQTY>
													<BILLEDQTY>'.$ACTUALQTY.' nos</BILLEDQTY>
													<INCLVATRATE></INCLVATRATE>
													<BATCHALLOCATIONS.LIST>
												    <GODOWNNAME>'.$godown_name.'</GODOWNNAME>
                                                    <BATCHNAME></BATCHNAME>
													<DESTINATIONGODOWNNAME>'.$godown_name.'</DESTINATIONGODOWNNAME>
													<INDENTNO/>
													<ORDERNO/>
													<TRACKINGNUMBER/>
													<DYNAMICCSTISCLEARED>No</DYNAMICCSTISCLEARED>
													<AMOUNT>'.$totalamount.'</AMOUNT>
													<ACTUALQTY>'.$ACTUALQTY.' nos</ACTUALQTY>
													<BILLEDQTY>'.$ACTUALQTY.' nos</BILLEDQTY>
													<INCLVATRATE></INCLVATRATE>
													<ADDITIONALDETAILS.LIST></ADDITIONALDETAILS.LIST>
													<VOUCHERCOMPONENTLIST.LIST></VOUCHERCOMPONENTLIST.LIST>
													</BATCHALLOCATIONS.LIST>
													<ACCOUNTINGALLOCATIONS.LIST>
													<OLDAUDITENTRYIDS.LIST TYPE="Number">
													<OLDAUDITENTRYIDS>-1</OLDAUDITENTRYIDS>
													</OLDAUDITENTRYIDS.LIST>
													<LEDGERNAME>'.$ledgername.'</LEDGERNAME>
													<GSTCLASS/>
													<ISDEEMEDPOSITIVE>Yes</ISDEEMEDPOSITIVE>
													<LEDGERFROMITEM>No</LEDGERFROMITEM>
													<REMOVEZEROENTRIES>No</REMOVEZEROENTRIES>
													<ISPARTYLEDGER>No</ISPARTYLEDGER>
													<ISLASTDEEMEDPOSITIVE>Yes</ISLASTDEEMEDPOSITIVE>
													<AMOUNT>'.$totalamount.'</AMOUNT>
													<SERVICETAXDETAILS.LIST></SERVICETAXDETAILS.LIST>
													<BANKALLOCATIONS.LIST></BANKALLOCATIONS.LIST>
													<BILLALLOCATIONS.LIST></BILLALLOCATIONS.LIST>
													<INTERESTCOLLECTION.LIST></INTERESTCOLLECTION.LIST>
													<OLDAUDITENTRIES.LIST></OLDAUDITENTRIES.LIST>
													<ACCOUNTAUDITENTRIES.LIST></ACCOUNTAUDITENTRIES.LIST>
													<AUDITENTRIES.LIST></AUDITENTRIES.LIST>
													<INPUTCRALLOCS.LIST></INPUTCRALLOCS.LIST>
													<DUTYHEADDETAILS.LIST></DUTYHEADDETAILS.LIST>
													<EXCISEDUTYHEADDETAILS.LIST></EXCISEDUTYHEADDETAILS.LIST>
													<RATEDETAILS.LIST></RATEDETAILS.LIST>
													<SUMMARYALLOCS.LIST></SUMMARYALLOCS.LIST>
													<STPYMTDETAILS.LIST></STPYMTDETAILS.LIST>
													<EXCISEPAYMENTALLOCATIONS.LIST></EXCISEPAYMENTALLOCATIONS.LIST>
													<TAXBILLALLOCATIONS.LIST></TAXBILLALLOCATIONS.LIST>
													<TAXOBJECTALLOCATIONS.LIST></TAXOBJECTALLOCATIONS.LIST>
													<TDSEXPENSEALLOCATIONS.LIST></TDSEXPENSEALLOCATIONS.LIST>
													<VATSTATUTORYDETAILS.LIST></VATSTATUTORYDETAILS.LIST>
													<COSTTRACKALLOCATIONS.LIST></COSTTRACKALLOCATIONS.LIST>
													<REFVOUCHERDETAILS.LIST></REFVOUCHERDETAILS.LIST>
													<INVOICEWISEDETAILS.LIST></INVOICEWISEDETAILS.LIST>
													<VATITCDETAILS.LIST></VATITCDETAILS.LIST>
													<ADVANCETAXDETAILS.LIST></ADVANCETAXDETAILS.LIST>
													</ACCOUNTINGALLOCATIONS.LIST>
													<DUTYHEADDETAILS.LIST></DUTYHEADDETAILS.LIST>
													<SUPPLEMENTARYDUTYHEADDETAILS.LIST></SUPPLEMENTARYDUTYHEADDETAILS.LIST>
													<TAXOBJECTALLOCATIONS.LIST></TAXOBJECTALLOCATIONS.LIST>
													<REFVOUCHERDETAILS.LIST></REFVOUCHERDETAILS.LIST>
													<EXCISEALLOCATIONS.LIST></EXCISEALLOCATIONS.LIST>
													<EXPENSEALLOCATIONS.LIST></EXPENSEALLOCATIONS.LIST>
													</ALLINVENTORYENTRIES.LIST>';




}
//if($ids > 0)
//{

  $requestXMLITEMLIST = implode(' ', $ALLINVENTORYENTRIESITEM);
			       
		    $requestXML = $requestXMLSTART.$requestXMLITEMLIST.$requestXMLEND;
		   // echo"<pre>";
		   // print_R($requestXML);exit;
		    $first=array();
				
			$server = 'http://quytech14.ddns.net:8000/';
			$headers = array( "Content-type: text/xml" ,"Content-length: ".strlen($requestXML) ,"Connection: close" );
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, TALLYIP);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_TIMEOUT, 100);
			curl_setopt($ch, CURLOPT_POST, true);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $requestXML);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			$data = curl_exec($ch);
			
		
			
		     if(curl_errno($ch)){
		    echo json_encode(array('status'=>'Failed', 'data'=> print curl_error($ch)));
		   // echo "  something went wrong..... try later";
		    }else{
		    
		    	   $p = xml_parser_create();
        xml_parse_into_struct($p, $data, $vals, $index);
       xml_parser_free($p);
		   if(isset($vals[$index['LASTVCHID'][0]]['value']) && $vals[$index['LASTVCHID'][0]]['value']>0) {

		   
        $attributeArray = array();
        $attributeArray['TALLYMASTERNO'] = $vals[$index['LASTVCHID'][0]]['value'];
        
        $this->_dbUpdate2($attributeArray,'table_wh_mrn', "mrn_id =".$mrn_id );  
//print_R($yu);exit;

      

            $first=array('status'=>'Pushed To Tally Successfully', 'data'=> " Pushed To Tally Successfully.");

    } else {
      
      if(isset($vals[$index['LINEERROR'][0]]['value'])) 
        $message = $vals[$index['LINEERROR'][0]]['value'];
      else 
        $message = $vals[0]['value'];


         //echo json_encode(array('status'=>'Failed', 'data'=> $message));
         $first=array('status'=>$message, 'data'=> $message);
    }


    curl_close($ch);

      

               } 


                $tallyretun=   json_encode(array("first"=>$first['data']));
              
             //print_R($tallyretun);
             $jsonAsObject   = json_decode($tallyretun);
             
             $fromwh=$jsonAsObject->first;


               
				//}


								// // While Update MRN Details going to delete all the removed bsn from current request
								// if(sizeof($bsnArrayIDs)>0) {

								// 	$mrnDetailUpdate['status'] = 'D';
								// 	$bsnArrayIDs = implode(",", $bsnArrayIDs);
								// 	$this->_dbUpdate2($mrnDetailUpdate,'table_wh_mrn_detail', 'mrn_id="'.$mrn_id.'" AND bsn_id NOT IN ('.$bsnArrayIDs.')');

								// }

				return array('status'=>'success', 'msg'=>'MRN Generated successfully', 'id'=>$mrn_id,'tallymsg'=>$fromwh);

			} else {

				return array('status'=>'failed', 'msg'=>'No Bsn Selected!');
			}




		} else {
			return array('status'=>'failed', 'msg'=>'Imput parameters missing, Please Select Warehouse, Distributor, MRN date and number');
		} 

	} else {
		return array('status'=>'failed', 'msg'=>'Please Select a warehouse');
	}


	}





















	public function dateFormatter ($requestDate) {

		$formattedDate = "-";

		if($requestDate != '0000-00-00') {

			$formattedDate = date('d M Y',strtotime($requestDate));

			if($formattedDate=='01 Jan 1970' || $formattedDate=='1970-01-01') {
				$formattedDate = "";
			}
		}

		return $formattedDate;
	}



		public function getBatteryMRNRecieveModeStatus ($recieveModeStatus) {

		// BSN Status
		// status(I-In-process, A-Available, D-Dispatched, S-Sold, C-Complaint, DMG-Damage)
			
			switch ($recieveModeStatus) {
				case 'C':
					# code...
					$status = "Direct Customer";
					break;

				case 'V':
					# code...
					$status = "Van";
					break;

				case 'T':
					# code...
					$status = "Transport";
					break;

				
				default:
					# code...
					$status = "-";
					break;
			}

			return $status;
		}



		public function getBCFStatus ($bcfsts) {

		// BSN Status
		// status(P-Pending, A-Accepted, R-Rejected)
			
			switch (trim($bcfsts)) {
				case 'P':
					# code...
					$status = "Pending";
					break;

				case 'A':
					# code...
					$status = "Accepted";
					break;

				case 'R':
					# code...
					$status = "Rejected";
					break;

				
				default:
					# code...
					$status = "-";
					break;
			}

			return $status;
		}



/***************************************************
* DESC : Add BCF(BATTERY CLAIM FORM) VIA Warehouse/Service Personnel
* Author: AJAY@2017-03-05
*
*
***************************************************/

		public function generateBCF ($paramSet) {
		// echo "<pre>";
		
		
		
		
		if(sizeof($paramSet)>0 && isset($paramSet['bcf_no']) && !empty($paramSet['bcf_no']) && isset($paramSet['bcf_date']) && $paramSet['bcf_date']>0 && isset($paramSet['bsn_no']) && !empty($paramSet['bsn_no']) && isset($paramSet['item_id']) && !empty($paramSet['item_id'])) {

			$bcf_no 	 		= trim($paramSet['bcf_no']);
			$bcf_series 	 	= trim($paramSet['bcf_series']);
			$bcf_prefix 	 	= trim($paramSet['bcf_prefix']);
			$bcf_id 			= $_REQUEST['id'];
			$bcf_date 	 		= date('Y-m-d', strtotime($paramSet['bcf_date']));
			$mrn_no 	 		= trim($paramSet['mrn_no']);
			$mrn_date 	 		= date('Y-m-d', strtotime($paramSet['mrn_date']));

			$sale_distributor_id 		= trim($paramSet['sale_distributor_id']);
			$service_distributor_id     = trim($paramSet['service_distributor_id']);
			$service_personnel_id     	= trim($paramSet['service_personnel_id']);
			
			
			$bsn = trim($paramSet['bsn_no']);
			$item_id = trim($paramSet['item_id']);
			$batch_no = trim($paramSet['batch_no']);
			$date_of_sale = date('Y-m-d', strtotime($paramSet['date_of_sale']));
			$warranty_end_date = date('Y-m-d', strtotime($paramSet['warranty_end_date']));
			$manufacture_date = date('Y-m-d', strtotime($paramSet['manufacture_date']));
			$EAPL_Invoice_date = date('Y-m-d', strtotime($paramSet['EAPL_Invoice_date']));
			$EAPL_Invoice_no = trim($paramSet['EAPL_Invoice_no']);
			
			$complaint_date = date('Y-m-d', strtotime($paramSet['complaint_date']));
			$complaint_id = $paramSet['complaint_id'];
			$datasource_id = trim($paramSet['datasource_id']);

			$challan_no 	 		= (trim($paramSet['challan_no']))?trim($paramSet['challan_no']):'';
			$challan_date 	 		= (trim($paramSet['challan_date']))?date('Y-m-d', strtotime($paramSet['challan_date'])):'';

			$customer_phone_no 	 			= trim($paramSet['customer_phone_no']);
			$customer_id 					= 0;
			$customer_name 					= trim($paramSet['customer_name']);
			$customer_address 				= trim($paramSet['customer_address']);
			$customer_address2 				= trim($paramSet['customer_address2']);
			$customer_address3 				= trim($paramSet['customer_address3']);
			$customer_mobile_no				= trim($paramSet['customer_mobile_no']);
			$customer_email_id 				= trim($paramSet['customer_email_id']);
			$cus_state_id 					= trim($paramSet['state_id']);
			$cus_city_id		 			= trim($paramSet['city_id']);


			$vehicle_reg_no 				= trim($paramSet['vehicle_reg_no']);
			$customer_complaint_remarks 	= trim($paramSet['customer_complaint_remarks']);
			$vehicle_id 					= trim($paramSet['vehicle_id']); 
			

			// echo "<pre>";
			// print_r($paramSet);
			// exit;


				if(isset($_SESSION['warehouseId']) && $_SESSION['warehouseId']>0) {
						$warehouse_id = $_SESSION['warehouseId'];
					} else if(isset($_SESSION['servicePersonnelId']) && $_SESSION['servicePersonnelId']>0){
						
						$spDetail = $this->_getSelectList('table_service_personnel AS SP',"SP.service_personnel_id, SP.warehouse_id",''," SP.service_personnel_id='".$_SESSION['servicePersonnelId']."'");

						// print_r($spDetail );
						// exit;

							if($spDetail[0]->warehouse_id == 0) {
								
								return array('status'=>'failed', 'msg'=>'No Warehouse found!');
								
							} else {
							$warehouse_id = $spDetail[0]->warehouse_id;
							$service_personnel_id = $spDetail[0]->service_personnel_id;
								
							}
					} else {
						
						// $spDetail = $this->_getSelectList('table_service_personnel AS SP',"SP.service_personnel_id, SP.warehouse_id",''," SP.service_personnel_id='".$paramSet['service_personnel_id']."'");

						// 	if($spDetail[0]->warehouse_id == 0) {
								
						// 		return array('status'=>'failed', 'msg'=>'No Warehouse found!');
								
						// 	} else {
						// 	$warehouse_id = $spDetail[0]->warehouse_id;
						// 	$service_personnel_id = $spDetail[0]->service_personnel_id;
						// 	}
						
					}






			if(isset($paramSet['customer_phone_no']) && !empty(trim($paramSet['customer_phone_no']))) {

				$customer_phone_no 	 = trim($paramSet['customer_phone_no']);
				
				// Check Customer
				$customerDetail = $this->_getSelectList('table_customer AS C',"C.customer_id",''," C.customer_phone_no ='".$customer_phone_no."' OR C.customer_phone_no2 = '".$customer_phone_no."' OR  C.customer_phone_no3='".$customer_phone_no."'");

				if(isset($customerDetail[0]->customer_id)) {

					$customer_id = $customerDetail[0]->customer_id;

					$customer['customer_name'] 			=  $customer_name;
					$customer['customer_address'] 		=  $customer_address;
					$customer['customer_address2'] 		=  $customer_address2;
					//$customer['customer_address3'] 		=  $customer_address3;
					$customer['state'] 					=  $cus_state_id;
					$customer['city'] 					=  $cus_city_id;
					$customer['customer_phone_no'] 		=  $customer_phone_no;
					$customer['country'] 				=  $_POST['country_id'];
					$customer['region_id'] 				=  $_POST['region_id'];
					$customer['zone'] 					=  $_POST['zone_id'];
					$customer['customer_landline_no'] 	=  $customer_mobile_no;
					$customer['customer_email'] 		=  $customer_email_id;


					// $customer_name = $customerDetail[0]->customer_name;
					// $customer_address = $customerDetail[0]->customer_address;
					// $customer_address2 = $customerDetail[0]->customer_address2;
					// $customer_address3 = $customerDetail[0]->customer_address3;
					// $customer_phone_no = $customerDetail[0]->customer_phone_no;
					// $customer_email = $customerDetail[0]->customer_email_id;
					// $customer_landline_no = $customerDetail[0]->customer_mobile_no;


					$this->_dbUpdate($customer,'table_customer', " customer_id='".$customer_id."'");  
					
				} else {

					// Create a Customer


					$customer = array();
					$customer['account_id'] 			=  $_SESSION['accountId'];	
					$customer['customer_name'] 			=  $customer_name;
					$customer['customer_address'] 		=  $customer_address;
					$customer['customer_address2'] 		=  $customer_address2;
					//$customer['customer_address3'] 		=  $customer_address3;
					$customer['state'] 					=  $cus_state_id;
					$customer['city'] 					=  $cus_city_id;
					$customer['customer_phone_no'] 		=  $customer_phone_no;
					$customer['customer_landline_no'] 	=  $customer_mobile_no;
					$customer['customer_email'] 		=  $customer_email_id;
					$customer['last_update_date'] 		=  date('Y-m-d');
					$customer['last_update_status'] 	=  'New';
					$customer['start_date'] 			=  date('Y-m-d');
					$customer['end_date'] 				= date('Y-m-d', strtotime('+2 years', strtotime(date('Y-m-d'))));
					$customer['status'] 		        =  'I';		

					$customer_id = $this->_dbInsert($customer,'table_customer');  

				}

			}
			$bsnCust['customer_id']	= $customer_id;
			$this->_dbUpdate($bsnCust,'table_item_bsn', " bsn='".$bsn."'"); 
			
			if(!empty($bsn)) {

				// Check is pending MRN of this BSN
				$mrn_id = 0;
				$mrnDetail = $this->_getSelectList2('table_wh_mrn_detail AS MRND LEFT JOIN table_wh_mrn AS MRN ON MRN.mrn_id = MRND.mrn_id',"MRND.mrn_id, MRND.mrn_detail_id, MRN.mrn_no, MRN.mrn_date",''," LOWER(MRND.bsn) = '".strtolower($bsn)."' AND MRND.status = 'P' AND MRN.warehouse_id = '".$warehouse_id."' ");
				
				if(isset($mrnDetail[0]->mrn_id)) {

					$mrn_id = $mrnDetail[0]->mrn_id;
					$mrn_no = $mrnDetail[0]->mrn_no;
					$mrn_date = $mrnDetail[0]->mrn_date;
					$mrn_detail_id = $mrnDetail[0]->mrn_detail_id;
					$paramSet['status']    = 'I';

					//return array('status'=>'failed', 'msg'=>'Duplicate BCF!');
					
				}


					// Firslty Check BSN available into bsn master or not

					if(!empty($bsn) && !empty($paramSet['item_id'])>0) {
						
						$bsn_id = "";
						$item_id = "";
						$category_id = "";

						$bsnDetail = $this->_getSelectList('table_item_bsn AS BSN',"BSN.bsn_id, BSN.status, BSN.item_id, BSN.category_id",''," BSN.bsn ='".$bsn."'");
						// echo "<pre>";
						// print_r($bsnDetail);
						// exit;

						if(isset($bsnDetail[0]->bsn_id)) {

							$bsn_id = $bsnDetail[0]->bsn_id;
							$item_id = $bsnDetail[0]->item_id;
							$category_id = $bsnDetail[0]->category_id;

							// Update BSN table sale distributor and date of dispatch
							// if($bsnDetail->status !='C' || $bsnDetail->status !='DMG' || $bsnDetail->status !='RPL') {


							// 	//$bsnArray['manufacture_date']    		= $manufacture_date;
							// 	//$bsnArray['warranty_end_date']    		= $warranty_end_date;
							// 	$bsnArray['last_updated_date']	 		= date('Y-m-d');	
							// 	$bsnArray['last_update_datetime']		= date('Y-m-d H:i:s');	
							// 	//$bsnArray['status']					= 'C'; // Dispatched to distributor C-Complaint

							// 	$this->_dbUpdate($bsnArray,'table_item_bsn', " bsn='".$bsn."'");  

							// }


						} else {


							// Get the Item Details
								// $ItemDetail = $this->_getSelectList('table_item AS I',"I.item_id, I.item_warranty, I.item_prodata, I.item_grace,   I.category_id",''," I.item_id ='".$paramSet['item_id']."'");
								// $item_id = $ItemDetail[0]->item_id;
								// $category_id = $ItemDetail[0]->category_id;
								// $item_warranty = $ItemDetail[0]->item_warranty;
								// $item_prodata = $ItemDetail[0]->item_prodata;
								// $item_grace = $ItemDetail[0]->item_grace;

								// // Create new stock entry of a BSN

								// $bsnArray = array();

								// $bsnArray['account_id']        		= $_SESSION['accountId'];	
								// $bsnArray['dispatch_warehouse_id']  = 0;
								// $bsnArray['sale_distributor_id']    = $sale_distributor_id;	
								// $bsnArray['item_id']           		= $item_id;
								// $bsnArray['category_id']       		= $category_id;
								// $bsnArray['batch_no']       		= $batch_no;
								// $bsnArray['bsn']       				= $bsn;
								// $bsnArray['stock_value']       		= 1;
								// $bsnArray['manufacture_date']       = $manufacture_date;
								// $bsnArray['warranty_period']       	=  $item_warranty;
								// $bsnArray['warranty_prorata']       =  $item_prodata;
								// $bsnArray['warranty_grace_period']  =  $item_grace;
								// $bsnArray['warranty_end_date']      =  $warranty_end_date;
								// $bsnArray['date_of_sale']      		=  $date_of_sale;
								// $bsnArray['user_type'] 		        =  (isset($_SESSION['userLoginType']))?$_SESSION['userLoginType']:0;	
								// $bsnArray['web_user_id'] 		    =  (isset($_SESSION['userLoginType']))?$_SESSION['PepUpSalesUserId']:0;
								// $bsnArray['last_updated_date']	 	= date('Y-m-d');	
								// $bsnArray['last_update_datetime']	= date('Y-m-d H:i:s');	
								// $bsnArray['created_datetime']		= date('Y-m-d H:i:s');	
								// $bsnArray['status']					= 'A';
								
								// $bsn_id = $this->_dbInsert($bsnArray,'table_item_bsn');  

						}



								// Add BCF

								$condi = " BCF.bsn = '".trim($bsn)."' AND BCF.status IN ('P', 'I')";
								$resultset = $this->_getSelectList('table_wh_mrn_bcf AS BCF','BCF.bcf_id, BCF.status','',$condi);


								
								//echo "<pre>";
								//print_r($resultset);
								//print_r($_REQUEST);
								//exit;


								if(is_array($resultset) && sizeof($resultset)>0)  {   
									if(isset($bcf_id) && $bcf_id>0) {
										
									// Update Warehouse dispatches to distributor activity
									$bcfDetail = array();
									$bcfDetail['mrn_id']      			    = $mrn_id;
									$bcfDetail['mrn_detail_id']				= $mrn_detail_id;
									//$bcfDetail['bcf_no']      			= $bcf_no;
									//$bcfDetail['bcf_date']      			= $bcf_date;
									$bcfDetail['service_personnel_id']      = $service_personnel_id;
									$bcfDetail['warehouse_id']      		= $warehouse_id;
									$bcfDetail['service_distributor_id']    = $service_distributor_id;
									$bcfDetail['sale_distributor_id']      	= $sale_distributor_id;
									$bcfDetail['retailer_id']      			= 0;
									$bcfDetail['customer_id']      			= $customer_id;
									$bcfDetail['complaint_id']      		= $complaint_id;
									$bcfDetail['bsn_id']                    = $bsn_id;
									$bcfDetail['bsn']               		= $bsn;
									$bcfDetail['batch_no']  				= $batch_no;
									$bcfDetail['item_id']                   = $item_id;
									$bcfDetail['category_id']               = $category_id;

									$bcfDetail['manfacturer_date']        	= $manufacture_date;
									$bcfDetail['bsn_warranty_end_date']     = $warranty_end_date;

									$bcfDetail['EAPL_Invoice_no']   		= $EAPL_Invoice_no;	
									$bcfDetail['EAPL_Invoice_date']   		= $EAPL_Invoice_date;
									$bcfDetail['date_of_sale']   			= $date_of_sale;
									$bcfDetail['complaint_date']   			= $complaint_date;
									$bcfDetail['datasource_id']   			= $datasource_id;
									$bcfDetail['challan_no']   				= $challan_no;
									$bcfDetail['challan_date']   			= $challan_date;
									$bcfDetail['vehicle_id']   				= $vehicle_id;
									$bcfDetail['vehicle_reg_no']   			= $vehicle_reg_no;
									$bcfDetail['customer_name']   			= $customer_name;
									
									$bcfDetail['cus_state_id']   			= $cus_state_id;
									$bcfDetail['cus_city_id']   			= $cus_city_id;
									$bcfDetail['customer_phone_no']   		= $customer_phone_no;
									$bcfDetail['customer_mobile_no']   		= $customer_mobile_no;
									$bcfDetail['customer_email_id']   		= $customer_email_id;
									$bcfDetail['customer_address'] 			= $customer_address;
									$bcfDetail['customer_address2']   		= $customer_address2;
									$bcfDetail['customer_address3']   		= $customer_address3;

									$bcfDetail['customer_complaint_remarks']= $customer_complaint_remarks;
									
									$bcfDetail['last_update_date'] 			= date('Y-m-d');
									$bcfDetail['status']  					= 'I';


									$this->_dbUpdate2($bcfDetail,'table_wh_mrn_bcf', 'bcf_id="'.$bcf_id.'"');


									$data['service_personnel_id']=mysql_escape_string($_POST['service_personnel_id']);
									$data['service_distributor_id']=mysql_escape_string($_POST['service_distributor_id']);
									$data['daf_no']=mysql_escape_string($_POST['daf_no']);
									// $data['daf_date']=mysql_escape_string($_POST['daf_date']);
									$data['daf_date']=date('Y-m-d', strtotime(mysql_escape_string($_POST['daf_date'])));
									$data['failure_id']=mysql_escape_string($_POST['failure_id']);
									$data['OCV1']=mysql_escape_string($_POST['OCV1']);
									$data['electric_level_id']=mysql_escape_string($_POST['electric_level_id']);
									$data['account_id']=mysql_escape_string($_POST['account_id']);
									$data['last_update_date']=date('Y-m-d');
									$data['last_update_status']='Update';
									$job_id=$this->_dbUpdate2($data,'table_wh_mrn_bcf_jobcard', 'jobcard_id="'.$_POST['jobcard_id'].'"');
									mysql_query ('Delete FROM table_wh_mrn_bcf_jobcard_gravity WHERE jobcard_id='.$_POST['jobcard_id']);


									$data1['jobcard_id']= mysql_escape_string($_POST['jobcard_id']);
									$data1['first_positive']= mysql_escape_string($_POST['firstb']);
									$data1['second']= mysql_escape_string($_POST['secondb']);
									$data1['third']= mysql_escape_string($_POST['thirdb']);
									$data1['fourth']= mysql_escape_string($_POST['fourthb']);
									$data1['fifth']= mysql_escape_string($_POST['fifthb']);
									$data1['sixth_negative']= mysql_escape_string($_POST['sixthb']);
									$data1['before_after']= 'before';
									$id_before_gravity = $this->_dbInsert($data1,'table_wh_mrn_bcf_jobcard_gravity');
										
									} else {
										return array('status'=>'failed', 'msg'=>'Already a pending BCF-Jobcard exists of this bsn!');
									}

									$msg = "BCF Updated successfully";
								} else {
									
									// Add BCF BSN DETAIL
									$bcfDetail = array();


									// Check BCF Series/Prefix at runtime

									 $bcfDetail = $this->_getSelectList('table_wh_mrn_bcf AS BCF',"BCF.bcf_id",''," LOWER(BCF.bcf_no) ='".strtolower($bcf_no)."'");

									 if(isset($bcfDetail[0]->bcf_id) && $bcfDetail[0]->bcf_id>0) {

									 	  $bcfSeriesData = $this->getWarehouseLastBCFSeriesNo();

										  if(sizeof($bcfSeriesData['data'])>0 && isset($bcfSeriesData['data']['bcf_no'])) {
										    	// echo "<pre>";
										    	// print_r($mrnSeriesData);

										       $bcf_no = $bcfSeriesData['data']['bcf_no'];
										       $bcf_prefix = $bcfSeriesData['data']['bcf_prefix'];
										       $bcf_series = $bcfSeriesData['data']['bcf_series'];
										  } else {
										  		return array('status'=>'failed', 'msg'=>'Duplicate BCF NO!');
										  }
									 }

									// echo "<pre>";
									// print_r($bcfSeriesData);

									
									
									$bcfDetail['account_id']      			= $_SESSION['accountId'];
									$bcfDetail['mrn_id']      			    = $mrn_id;
									$bcfDetail['mrn_detail_id']				= $mrn_detail_id;
									$bcfDetail['bcf_no']      			    = $bcf_no;

									$bcfDetail['bcf_prefix']      			= $bcf_prefix;
									$bcfDetail['bcf_series']      		    = $bcf_series;

									$bcfDetail['bcf_date']      			= $bcf_date;
									$bcfDetail['service_personnel_id']      = $service_personnel_id;
									$bcfDetail['warehouse_id']      		= $warehouse_id;
									$bcfDetail['service_distributor_id']    = $service_distributor_id;
									$bcfDetail['sale_distributor_id']      	= $sale_distributor_id;
									$bcfDetail['retailer_id']      			= 0;
									$bcfDetail['customer_id']      			= $customer_id;
									$bcfDetail['complaint_id']      		= $complaint_id;
									$bcfDetail['bsn_id']                    = $bsn_id;
									$bcfDetail['bsn']               		= $bsn;
									$bcfDetail['batch_no']  				= $batch_no;
									$bcfDetail['item_id']                   = $item_id;
									$bcfDetail['category_id']               = $category_id;

									$bcfDetail['manfacturer_date']        	= $manufacture_date;
									$bcfDetail['bsn_warranty_end_date']     = $warranty_end_date;

									$bcfDetail['EAPL_Invoice_no']   		= $EAPL_Invoice_no;	
									$bcfDetail['EAPL_Invoice_date']   		= $EAPL_Invoice_date;
									$bcfDetail['date_of_sale']   			= $date_of_sale;
									$bcfDetail['complaint_date']   			= $complaint_date;
									
									$bcfDetail['datasource_id']   			= $datasource_id;
									$bcfDetail['challan_no']   				= $challan_no;
									$bcfDetail['challan_date']   			= $challan_date;
									$bcfDetail['vehicle_id']   				= $vehicle_id;
									$bcfDetail['vehicle_reg_no']   			= $vehicle_reg_no;
									$bcfDetail['customer_name']   			= $customer_name;
									
									$bcfDetail['cus_state_id']   			= $cus_state_id;
									$bcfDetail['cus_city_id']   			= $cus_city_id;
									$bcfDetail['customer_phone_no']   		= $customer_phone_no;
									$bcfDetail['customer_mobile_no']   		= $customer_mobile_no;
									$bcfDetail['customer_email_id']   		= $customer_email_id;
									$bcfDetail['customer_address'] 			= $customer_address;
									$bcfDetail['customer_address2']   		= $customer_address2;
									$bcfDetail['customer_address3']   		= $customer_address3;

									$bcfDetail['customer_complaint_remarks']= $customer_complaint_remarks;
									
									$bcfDetail['last_update_date'] 			= date('Y-m-d');
									$bcfDetail['created_datetime'] 			= date('Y-m-d H:i:s');
									$bcfDetail['status']  					= 'I';	//BCF to be set as In-Process


									// print_r($bcfDetail);
									// exit;

									$bcf_id = $this->_dbInsert($bcfDetail,'table_wh_mrn_bcf');
									if($bcf_id > 0)
									{
										// Check BCF Series/Prefix at runtime AJAY@2017-03-16

										 $JCDetail = $this->_getSelectList('table_wh_mrn_bcf_jobcard',"jobcard_id",''," LOWER(jobcard_no) ='".strtolower(trim($_POST['jobcard_no']))."'");

										 if(isset($JCDetail[0]->jobcard_id) && $JCDetail[0]->jobcard_id>0) {
										  // $_objStock = new StockClass;
										  $JCSeriesData = $this->getWarehouseLastJOBCARDSeriesNo();

										  if(sizeof($JCSeriesData['data'])>0 && isset($JCSeriesData['data']['jobcard_no'])) {
										  // echo "<pre>";
										  // print_r($mrnSeriesData);

										   $jobcard_no = $JCSeriesData['data']['jobcard_no'];
										   $jobcard_prefix = $JCSeriesData['data']['jobcard_prefix'];
										   $jobcard_series = $JCSeriesData['data']['jobcard_series'];

										  } else { $_SESSION['MSG']= 'Duplicate JOB CARD SERIES NO!'; header('location: jobcard.php');  }

										} else {
											$jobcard_no = trim($_POST['jobcard_no']);
											$jobcard_prefix = trim($_POST['jobcard_prefix']);
											$jobcard_series = trim($_POST['jobcard_series']);
										}

										$data['jobcard_no']= $jobcard_no;
										$data['jobcard_prefix']= $jobcard_prefix;
										$data['jobcard_series']= $jobcard_series;


										// Check For Duplicacy
										 $JCDetail = $this->_getSelectList('table_wh_mrn_bcf_jobcard AS JC LEFT JOIN table_wh_mrn_bcf AS BCF ON BCF.bcf_id =  JC.bcf_id ',"JC.jobcard_id",''," LOWER(bsn) ='".strtolower(trim($_POST['bsn']))."' AND JC.status IN ('P', 'I')");


										 if(isset($JCDetail[0]->jobcard_id) && $JCDetail[0]->jobcard_id>0) {
										  $_SESSION['msg']= 'Duplicate BSN JOBCARD'; 
										  header('location: jobcard.php'); 
										 }
										 
										$data['jobcard_date']=date('Y-m-d', strtotime(mysql_escape_string($_POST['jobcard_date'])));
										$data['bcf_id']=$bcf_id;
										$data['service_personnel_id']=mysql_escape_string($_POST['service_personnel_id']);
										$data['service_distributor_id']=mysql_escape_string($_POST['service_distributor_id']);
										$data['daf_no']=mysql_escape_string($_POST['daf_no']);
										// $data['daf_date']=mysql_escape_string($_POST['daf_date']);
										$data['daf_date']=date('Y-m-d', strtotime(mysql_escape_string($_POST['daf_date'])));
										$data['failure_id']=mysql_escape_string($_POST['failure_id']);
										$data['OCV1']=mysql_escape_string($_POST['OCV1']);
										$data['electric_level_id']=mysql_escape_string($_POST['electric_level_id']);
										$data['battery_sts'] = mysql_escape_string($_POST['battery_sts']);
										$data['account_id']=mysql_escape_string($_POST['account_id']);
										$data['last_update_date']=date('Y-m-d');
										$data['last_update_status']='New';
										$data['initial_reading_save_time'] = date('Y-m-d H:i:s');
										$data['under_process'] = "I";
										$data['status'] = 'I';
										$id=$this->_dbInsert($data,'table_wh_mrn_bcf_jobcard');
										mysql_query ('Delete FROM table_wh_mrn_bcf_jobcard_gravity WHERE jobcard_id='.$id);


										$data1['jobcard_id']= mysql_escape_string($id);
										$data1['first_positive']= mysql_escape_string($_POST['firstb']);
										$data1['second']= mysql_escape_string($_POST['secondb']);
										$data1['third']= mysql_escape_string($_POST['thirdb']);
										$data1['fourth']= mysql_escape_string($_POST['fourthb']);
										$data1['fifth']= mysql_escape_string($_POST['fifthb']);
										$data1['sixth_negative']= mysql_escape_string($_POST['sixthb']);
										$data1['before_after']= 'before';
										$id_before_gravity = $this->_dbInsert($data1,'table_wh_mrn_bcf_jobcard_gravity');
									}
									$msg = "BCF-Jobcard Generated successfully";

								}


					} else {
						return array('status'=>'failed', 'msg'=>'Sorry Item Name can not be empty!');

					}

				return array('status'=>'success', 'msg'=>$msg, 'id'=>$bcf_id);

			} else {

				return array('status'=>'failed', 'msg'=>'No Bsn Selected!');
			}




		} else {
			return array('status'=>'failed', 'msg'=>'Input parameters missing, Please Select BCF NO & DATE, BSN AND BSN ITEM');
		} 


	}
	
	
	// END OF BCF





	/***Stock Dispatch to Distributor by Warehouse App
	* by Maninder kumar 
	*on 06th March 2017********/

	public function warehouseDispatchesbyApp ($paramSet) {	
			// echo"<pre>";
			// print_r($paramSet);exit;

		if(sizeof($paramSet)>0 && isset($paramSet['warehouse_id']) && $paramSet['warehouse_id']>0 && isset($paramSet['distributor_id']) && $paramSet['distributor_id']>0 && isset($paramSet['invoice_no']) && !empty($paramSet['invoice_no']) ) {

			$warehouse_id 		= $paramSet['warehouse_id'];
			$distributor_id     = $paramSet['distributor_id'];
			$bill_no 	 		= trim($paramSet['invoice_no']);
			$bill_date 	 		= date('Y-m-d', strtotime($paramSet['invoice_date']));

			$delivery_challan_no 	 		= ($paramSet['challan_no'])?$paramSet['challan_no']:'';
			$delivery_challan_date 	 		= ($paramSet['challan_date'])?date('Y-m-d', strtotime($paramSet['challan_date'])):'';



			/******Delivery Challan details***/
			$transporter 		= trim($paramSet['transporter']);
			$trnsport_mode     = trim($paramSet['trnsport_mode']);
			$permitNo 	 		= trim($paramSet['permitNo']);
			$eapl_invoice 		= trim($paramSet['eapl_invoice']);
			$datte 	 		= date('Y-m-d', strtotime($paramSet['datte']));
			$vehicleNo     = trim($paramSet['vehicleNo']);
			$eapl_date 	 		= date('Y-m-d', strtotime($paramSet['eapl_date']));
			$grNo 	 		= trim($paramSet['grNo']);
			$contactNo 	 		= trim($paramSet['contactNo']);	

			if(isset($paramSet['selectedBsn']) && sizeof($paramSet['selectedBsn'])>0) {


				$returnArray = array();
				$k = 0;
				foreach ($paramSet['selectedBsn'] as $key => $value) {
					# code...

					// Firslty Check BSN available into bsn master or not

					//if(isset($paramSet['Bsn'][$key]) && !empty($paramSet['Bsn'][$key])) {

						$bsn = trim($value['bsn']);
						$bsn_id = trim($value['bsn_id']);
						$item_id = trim($value['item_id']);
						$category_id = trim($value['category_id']);

						$bsnDetail = $this->_getSelectList2('table_item_bsn	 AS BSN',"BSN.bsn_id, BSN.status, BSN.item_id, BSN.category_id",''," BSN.bsn ='".$bsn."'");
						// echo "<pre>";
						// print_r($bsnDetail);
						// exit;

						if(isset($bsnDetail[0]->bsn_id)) {

							$bsn_id = $bsnDetail[0]->bsn_id;
							$item_id = $bsnDetail[0]->item_id;
							$category_id = $bsnDetail[0]->category_id;

							// Update BSN table sale distributor and date of dispatch
							if($bsnDetail->status !='S' || $bsnDetail->status !='D') {

								$bsnArray['sale_distributor_id']    		= $distributor_id;	
								$bsnArray['distributor_dispatch_date']    	= date('Y-m-d');
								$bsnArray['last_updated_date']	 			= date('Y-m-d');	
								$bsnArray['last_update_datetime']			= date('Y-m-d H:i:s');	
								$bsnArray['status']							= 'D'; // Dispatched to distributor
								
								$this->_dbUpdate2($bsnArray,'table_item_bsn', " bsn='".$bsn."'");  

							}


						} else {

								// Create new stock entry of a BSN

								$bsnArray = array();

								$bsnArray['account_id']        		= $_SESSION['accountId'];	
								$bsnArray['dispatch_warehouse_id']  = $warehouse_id;
								$bsnArray['sale_distributor_id']    = $distributor_id;	
								$bsnArray['item_id']           		= '';
								$bsnArray['category_id']       		= '';
								$bsnArray['batch_no']       		= '';
								$bsnArray['bsn']       				= $bsn;
								$bsnArray['stock_value']       		= 1;
								$bsnArray['manufacture_date']       = '';
								$bsnArray['warranty_period']       	= '';
								$bsnArray['warranty_prorata']       = '';
								$bsnArray['warranty_grace_period']  = '';
								$bsnArray['warranty_end_date']      = '';
								$bsnArray['user_type'] 		        =  (isset($_SESSION['userLoginType']))?$_SESSION['userLoginType']:0;	
								$bsnArray['web_user_id'] 		    =  (isset($_SESSION['userLoginType']))?$_SESSION['PepUpSalesUserId']:0;
								$bsnArray['last_updated_date']	 	= date('Y-m-d');	
								$bsnArray['last_update_datetime']	= date('Y-m-d H:i:s');	
								$bsnArray['created_datetime']		= date('Y-m-d H:i:s');	
								$bsnArray['status']					= 'A';
								
								$bsn_id = $this->_dbInsert($bsnArray,'table_item_bsn');  

						}



								// 	bsn lifecycle activity data array 

								$data = array();

								$data['account_id'] 			=  $_SESSION['accountId'];	
								$data['item_id'] 				=  $item_id;
								$data['bsn_id'] 		        =  $bsn_id;
								$data['bsn'] 		            =  $bsn;	
								$data['from_warehouse_id']      =  $warehouse_id;	
								$data['distributor_id'] 		=  $distributor_id;	
								$data['bill_date'] 				=  $bill_date;
								$data['bill_no'] 				=  $bill_no;
								$data['user_type'] 		        =  $_SESSION['userLoginType'];	
								$data['web_user_id'] 		    =  $_SESSION['PepUpSalesUserId'];	
								$data['created_datetime'] 		=  date('Y-m-d H:i:s');	
								$data['activity_type'] 		    =  'D'; // D- Dispatched, R-Return, C-Complain
								$data['status'] 		        =  'A';		

								$this->_dbInsert($data,'table_item_bsn_lifecycle_activity');  



								// Add Stock distributor in-process table as GRN

								$condi = " distributor_id = '".$distributor_id."' AND bsn_id ='".$bsn_id."' AND status IN ('A', 'I')";
								$resultset = $this->_getSelectList2('table_item_dis_stk_inprocess','dis_stk_inpro_id, acpt_stock_value','',$condi);


								if(is_array($resultset) && sizeof($resultset)>0)  {   

									// Update Warehouse dispatches to distributor activity
									$disInProcess = array();

									$disInProcess['last_update_datetime'] 		= date('Y-m-d H:i:s');
									$disInProcess['bill_date'] 					= $bill_date;
									$disInProcess['bill_no']                    = $bill_no;


									$this->_dbUpdate2($disInProcess,'table_item_dis_stk_inprocess', 'dis_stk_inpro_id="'.$resultset[0]->dis_stk_inpro_id.'"');

								} else {
									
									// Add Warehouse dispatches to distributor activity
									$disInProcess = array();

									$disInProcess['account_id']              	= $_SESSION['accountId'];
									$disInProcess['warehouse_id']      			= $warehouse_id;	
									$disInProcess['distributor_id'] 			= $distributor_id;
									$disInProcess['item_id']                    = $item_id;
									$disInProcess['bsn_id']                    	= $bsn_id;
									$disInProcess['category_id']                = $category_id;
									$disInProcess['attribute_value_id']  		= 0;
									$disInProcess['bill_date'] 					= $bill_date;
									$disInProcess['bill_no']                    = $bill_no;
									$disInProcess['rec_stock_value']            = 1;
									$disInProcess['delivery_challan_no']        = $delivery_challan_no;
									$disInProcess['delivery_challan_date']      = $delivery_challan_date;

									$disInProcess['last_update_datetime']   	= date('Y-m-d H:i:s');	
									$disInProcess['created_datetime'] 			= date('Y-m-d H:i:s');
									$disInProcess['status']  					= 'I';

									$dis_stk_inpro_id = $this->_dbInsert($disInProcess,'table_item_dis_stk_inprocess');

								}



								// Update BSN Status from warehouse Stock or BSN Master
		 

								$condi = " warehouse_id = '".$warehouse_id."' AND bsn_id ='".$bsn_id."'";
								$resultset = $this->_getSelectList2('table_warehouse_stock AS WS','WS.warehouse_stock_id ','',$condi);

								if(is_array($resultset) && sizeof($resultset)>0)  { 

									// Update Warehouse BSN Status
									$whStock = array();
									if($resultset[0]->status == 'A') {
										$whStock['last_updated_date']	 		=  date('Y-m-d');	
										$whStock['last_update_datetime'] 		=  date('Y-m-d H:i:s');
										$whStock['status']		            	=  'D';

										$this->_dbUpdate2($whStock,'table_warehouse_stock', 'warehouse_stock_id="'.$resultset[0]->warehouse_stock_id.'"');
									} else if($resultset[0]->status == 'D') {
										//return array('status'=>'failed', 'msg'=>'This BSN('.$bsn.') Already Discpatched');
										$returnArray[$k]['bsn'] = $bsn;
										$returnArray[$k]['status'] = 'D';
									} else if($resultset[0]->status == 'I') {
										//return array('status'=>'failed', 'msg'=>'Please Accept/Scan BSN('.$bsn.') Before Discpatches');
										$returnArray[$k]['bsn'] = $bsn;
										$returnArray[$k]['status'] = 'I';
									} else if($resultset[0]->status == 'S') {
										//return array('status'=>'failed', 'msg'=>'BSN('.$bsn.') Already Sold');
										$returnArray[$k]['bsn'] = $bsn;
										$returnArray[$k]['status'] = 'S';
									} else if($resultset[0]->status == 'DMG') {
										//return array('status'=>'failed', 'msg'=>'BSN('.$bsn.') Damage');
										$returnArray[$k]['bsn'] = $bsn;
										$returnArray[$k]['status'] = 'DMG';
									}


								} else {

									// 	Add New BSN Warehouse Stock 

									$whStock = array();

									$whStock['account_id'] 				=  $_SESSION['accountId'];	
									$whStock['warehouse_id'] 		    =  $warehouse_id;
									$whStock['item_id'] 				=  $item_id;
									$whStock['bsn_id'] 		        	=  $bsn_id;
									$whStock['stock_value']       		=  1;
									$whStock['last_updated_date']	 	=  date('Y-m-d');	
									$whStock['last_update_datetime']	=  date('Y-m-d H:i:s');	
									$whStock['created_datetime']		=  date('Y-m-d H:i:s');	
									$whStock['status']		            =  'D';

									$this->_dbInsert($whStock,'table_warehouse_stock');  

								}



								//Add Delivery Challan details 								
								$DelChln = array();

								$DelChln['bsn_id'] 				             =  $bsn_id;	
								$DelChln['delivery_chln_trans'] 			 =  $transporter;	
								$DelChln['delivery_chln_trans_mode'] 		 =  $trnsport_mode;	
								$DelChln['delivery_chln_eapl_invoice'] 		 =  $eapl_invoice;
								$DelChln['delivery_chln_eapl_invoice_date']	 =  $eapl_date;
								$DelChln['delivery_chln_road_permit_number'] =  $permitNo;
								$DelChln['delivery_chln_vehicle_number']     =  $vehicleNo;
								$DelChln['delivery_chaln_date']	 			 =  $datte;	
								$DelChln['delivery_chaln_gr_no']			 =  $grNo;
								$DelChln['delivery_chaln_contact']			 =  $contactNo;
								$DelChln['status']		           			 =  'A';

								$this->_dbInsert($DelChln,'table_wh_mrn_bcs');  

					//}

				  $k++;
				}

					$updtInvoice = array();

					$updtInvoice['last_update_datetime'] 		= date('Y-m-d H:i:s');				
					$updtInvoice['dispatch_status']            = 'D';


					$this->_dbUpdate2($updtInvoice,'table_order_invoice_assign_wh', 'invoice_id="'.$bill_no.'"');


				return array('status'=>'success', 'code'=>'1000');

			} else {

				return array('status'=>'failed', 'code'=>'1006');
			}




		} else {
			return array('status'=>'failed', 'code'=>'1006');
		}


	
	} //End of Stock Dispatch from warehouse app















	/********************************************************************
	* DESC: Get BSN Status
	* Author: AJAY
	* Created: 2017-03-14
	*
	**/

	public function getBSNStatus ($bsnNo) {

	$bsn_id = 0;

	if(!empty($bsnNo) && $bsnNo!="") {

		$bsn = strtolower(trim($bsnNo));

		$bsnDetail = $this->_getSelectList2('table_item_bsn AS BSN',"BSN.bsn_id, BSN.status, BSN.item_id, BSN.category_id",''," LOWER(BSN.bsn) ='".$bsn."'");
		// echo "<pre>";
		// print_r($bsnDetail);
		// exit;

		if(isset($bsnDetail[0]->bsn_id)) {
			$bsn_id =  $bsnDetail[0]->bsn_id;
		}



	}

	return $bsn_id;

	}








   public function getBSNDetail ($bsnNo) {

	$bsnObjectArray = array();

		if(!empty($bsnNo) && $bsnNo!="") {

			$bsn = strtolower(trim($bsnNo));

			$bsnDetail = $this->_getSelectList2('table_item_bsn AS BSN',"BSN.bsn_id, BSN.date_of_sale, BSN.status, BSN.customer_id, BSN.item_id, BSN.category_id, BSN.warranty_end_date",''," LOWER(BSN.bsn) ='".$bsn."' AND BSN.date_of_sale!='0000-00-00' ");
			// echo "<pre>";
			// print_r($bsnDetail);
			// exit;

			if(isset($bsnDetail[0]->bsn_id)) {
				$bsnObjectArray =  $bsnDetail[0];
			}



		}

		return $bsnObjectArray;

   }
























	/********************************************************************
	* DESC: Get MRN SERIES
	* Author: AJAY
	* Created: 2017-03-15
	*
	**/

	public function getBSNWarrantyEndDate ($dateOfSale, $warrantyPeriod, $proRata) {
		// echo $dateOfSale;
		// echo "<br>";

		$warrantyEndDate = "";
		$totalWarranty = intVal($proRata) + intVal($warrantyPeriod);
			if(!empty($dateOfSale) && !empty($totalWarranty) && intVal($totalWarranty)>0) {

				if($dateOfSale != '1970-01-01' && $dateOfSale!='0000-00-00' && $dateOfSale!= '') {
					$warrantyEndDate    = date('Y-m-d', strtotime("+".$totalWarranty." months", strtotime($dateOfSale)));
					//$this->dateFormatter($warrantyEndDate);
				} else {
					$warrantyEndDate = "-";
				}
			}

		
		// echo  $warrantyEndDate;
		// echo "/n";
		return $warrantyEndDate;

	}


public function getBSNExpiryDate ($dispatch_date, $warrantyPeriod, $proRata)
	{
		$warrantyEndDate = "";
		$totalWarranty = intVal($proRata) + intVal($warrantyPeriod);
			if(!empty($dispatch_date) && !empty($totalWarranty) && intVal($totalWarranty)>0) {

				if($dispatch_date != '1970-01-01' && $dispatch_date!='0000-00-00' && $dispatch_date!= '') {
					$warrantyEndDate    = date('Y-m-d', strtotime("+".$totalWarranty." months", strtotime($dispatch_date)));
					//$this->dateFormatter($warrantyEndDate);
				} else {
					$warrantyEndDate = "-";
				}
			}

		
		// echo  $warrantyEndDate;
		// echo "/n";
		return $warrantyEndDate;
	}





	/********************************************************************
	* DESC: Get BSN SERIES
	* Author: AJAY
	* Created: 2017-03-15
	*
	**/

	public function getWarehouseLastMRNSeriesNo ($warehouseId) {

		$warehouse_mrn_series_no = "";

		$resultArray = array();
		$data        = array();

		if(!empty($warehouseId) && intVal($warehouseId)>0) {

			$resultset = $this->_getSelectList('table_warehouse AS W','W.warehouse_prefix ',''," W.warehouse_id='".$warehouseId."' AND (W.warehouse_prefix!='' OR W.warehouse_prefix!=0) ");
			if(is_array($resultset) && sizeof($resultset)>0)  { 

				if(strlen(trim($resultset[0]->warehouse_prefix))==2) {

					$data['mrn_prefix'] = 'MR'.$resultset[0]->warehouse_prefix;

					// Get the last/Max mrn series value of this warehouse
					$mrnDetail = $this->_getSelectList('table_wh_mrn AS MRN',"MAX(MRN.mrn_series) AS mrn_series_no ",''," MRN.warehouse_id ='".$warehouseId."'");
					if(isset($mrnDetail[0]->mrn_series_no) && $mrnDetail[0]->mrn_series_no>0) {

						$data['mrn_series'] = $mrnDetail[0]->mrn_series_no+1;
					} else {
						$data['mrn_series'] = 1;
					}


					if($data['mrn_prefix']!='' && $data['mrn_series']>0){
						$data['mrn_no'] = $data['mrn_prefix'].$data['mrn_series'];

						$resultArray = array('status'=>'success', 'data'=>$data);

					}




				}

			}

		}

		return $resultArray;

	}









	/********************************************************************
	* DESC: Get BCF Status
	* Author: AJAY
	* Created: 2017-03-16
	*
	**/

	public function getWarehouseLastBCFSeriesNo () {

		$warehouse_mrn_series_no = "";

		$resultArray = array();
		$data        = array();


		if(isset($_SESSION['userLoginType']) && $_SESSION['userLoginType']==7) { // Warehouse Login
			$warehouseId = $_SESSION['warehouseId'];
		} else if(isset($_SESSION['userLoginType']) && $_SESSION['userLoginType']==8) { // Service Personnel Login
			$service_personnel_id = $_SESSION['servicePersonnelId'];

			$spDetail = $this->_getSelectList('table_service_personnel',"warehouse_id",'',"service_personnel_id='".$service_personnel_id."' AND warehouse_id>0");
			// print_r($spDetail);
			// exit;

			if($spDetail[0]->warehouse_id == 0) {
				return array('status'=>'failed', 'msg'=>'No Warehouse found!');
			} else {
				$warehouseId = $spDetail[0]->warehouse_id;
			}
		}




		if(!empty($warehouseId) && intVal($warehouseId)>0) {

			$resultset = $this->_getSelectList('table_warehouse AS W','W.warehouse_prefix ',''," W.warehouse_id='".$warehouseId."' AND (W.warehouse_prefix!='' OR W.warehouse_prefix!=0) ");

			//print_r($resultset);
			


			if(is_array($resultset) && sizeof($resultset)>0)  { 

				if(strlen(trim($resultset[0]->warehouse_prefix))==2) {

					$data['bcf_prefix'] = 'FR'.$resultset[0]->warehouse_prefix;

					// Get the last/Max mrn series value of this warehouse
					$bcfDetail = $this->_getSelectList('table_wh_mrn_bcf AS BCF',"MAX(BCF.bcf_series) AS bcf_series_no ",''," BCF.warehouse_id ='".$warehouseId."'");
					if(isset($bcfDetail[0]->bcf_series_no) && $bcfDetail[0]->bcf_series_no>0) {

						$data['bcf_series'] = $bcfDetail[0]->bcf_series_no+1;
					} else {
						$data['bcf_series'] = 1;
					}


					if($data['bcf_prefix']!='' && $data['bcf_series']>0){
						$data['bcf_no'] = $data['bcf_prefix'].$data['bcf_series'];

						$resultArray = array('status'=>'success', 'data'=>$data);

					}




				}

			}

		}
		//exit;
		return $resultArray;

	}



	/********************************************************************
	* DESC: Get JOB CARD Series
	* Author: AJAY
	* Created: 2017-03-16
	*
	**/

	public function getWarehouseLastJOBCARDSeriesNo () {

		$resultArray = array();
		$data        = array();


		if(isset($_SESSION['userLoginType']) && $_SESSION['userLoginType']==7) { // Warehouse Login
			$warehouseId = $_SESSION['warehouseId'];
		} else if(isset($_SESSION['userLoginType']) && $_SESSION['userLoginType']==8) { // Service Personnel Login
			$service_personnel_id = $_SESSION['servicePersonnelId'];

			$spDetail = $this->_getSelectList('table_service_personnel',"warehouse_id",'',"service_personnel_id='".$service_personnel_id."' AND warehouse_id>0");
			// print_r($spDetail);
			// exit;

			if($spDetail[0]->warehouse_id == 0) {
				return array('status'=>'failed', 'msg'=>'No Warehouse found!');
			} else {
				$warehouseId = $spDetail[0]->warehouse_id;
			}
		}




		if(!empty($warehouseId) && intVal($warehouseId)>0) {

			$resultset = $this->_getSelectList('table_warehouse AS W','W.warehouse_prefix ',''," W.warehouse_id='".$warehouseId."' AND (W.warehouse_prefix!='' OR W.warehouse_prefix!=0) ");

			//print_r($resultset);
		

			if(is_array($resultset) && sizeof($resultset)>0)  { 

				if(strlen(trim($resultset[0]->warehouse_prefix))==2) {

					$data['jobcard_prefix'] = 'JC'.$resultset[0]->warehouse_prefix;

					// Get the last/Max mrn series value of this warehouse
					$JCDetail = $this->_getSelectList('table_wh_mrn_bcf_jobcard AS JC LEFT JOIN table_wh_mrn_bcf AS BCF ON BCF.bcf_id = JC.bcf_id',"MAX(JC.jobcard_series) AS jobcard_series_no ",''," BCF.warehouse_id ='".$warehouseId."'");
					if(isset($JCDetail[0]->jobcard_series_no) && $JCDetail[0]->jobcard_series_no>0) {

						$data['jobcard_series'] = $JCDetail[0]->jobcard_series_no+1;
					} else {
						$data['jobcard_series'] = 1;
					}


					if($data['jobcard_prefix']!='' && $data['jobcard_series']>0){
						$data['jobcard_no'] = $data['jobcard_prefix'].$data['jobcard_series'];

						$resultArray = array('status'=>'success', 'data'=>$data);

					}




				}

			}

		}
		//exit;
		return $resultArray;

	}
	/********************************************************************
	* DESC: Get JOB CARD Series
	* Author: AJAY
	* Created: 2017-03-16
	*
	**/

	public function getWarehouseLastBCSSeriesNo () {

		$resultArray = array();
		$data        = array();


		if(isset($_SESSION['userLoginType']) && $_SESSION['userLoginType']==7) { // Warehouse Login
			$warehouseId = $_SESSION['warehouseId'];
		} else if(isset($_SESSION['userLoginType']) && $_SESSION['userLoginType']==8) { // Service Personnel Login
			$service_personnel_id = $_SESSION['servicePersonnelId'];

			$spDetail = $this->_getSelectList('table_service_personnel',"warehouse_id",'',"service_personnel_id='".$service_personnel_id."' AND warehouse_id>0");
			// print_r($spDetail);
			// exit;

			if($spDetail[0]->warehouse_id == 0) {
				return array('status'=>'failed', 'msg'=>'No Warehouse found!');
			} else {
				$warehouseId = $spDetail[0]->warehouse_id;
			}
		}




		if(!empty($warehouseId) && intVal($warehouseId)>0) {

			$resultset = $this->_getSelectList('table_warehouse AS W','W.warehouse_prefix ',''," W.warehouse_id='".$warehouseId."' AND (W.warehouse_prefix!='' OR W.warehouse_prefix!=0) ");

			//print_r($resultset);
		

			if(is_array($resultset) && sizeof($resultset)>0)  { 

				if(strlen(trim($resultset[0]->warehouse_prefix))==2) {

					$data['bcs_prefix'] = 'DC'.$resultset[0]->warehouse_prefix;

					// Get the last/Max mrn series value of this warehouse
					$BCSDetail = $this->_getSelectList2('table_wh_mrn_bcs AS BCS LEFT JOIN table_wh_mrn_bcf_jobcard AS JC ON JC.jobcard_id = BCS.jobcard_id LEFT JOIN table_wh_mrn_bcf AS BCF ON BCF.bcf_id = JC.bcf_id',"MAX(BCS.bcs_series) AS bcs_series_no ",''," BCF.warehouse_id ='".$warehouseId."'");
					if(isset($BCSDetail[0]->bcs_series_no) && $BCSDetail[0]->bcs_series_no>0) {

						$data['bcs_series'] = $BCSDetail[0]->bcs_series_no+1;
					} else {
						$data['bcs_series'] = 1;
					}


					if($data['bcs_prefix']!='' && $data['bcs_series']>0){
						$data['bcs_no'] = $data['bcs_prefix'].$data['bcs_series'];

						$resultArray = array('status'=>'success', 'data'=>$data);

					}




				}

			}

		}
		//exit;
		return $resultArray;

	}






			/************************************************************************
			* DESC: Get the Battery Usage or check its in ProRata
			* AUTHOR : AJAY
			* CREATED : 2017-03-22
			*
			*
			************************************************************************/

			// public function getBatteryProRataCliamSlabDetail($item_id, $totalUsageMonths) {

			// 	if($item_id>0) {

			// 		$icsDetail = $this->_getSelectList2('table_item_claim_slabs AS ICS',"ICS.*",''," ICS.item_id ='".$item_id."' AND ICS.slab_start_month<='".$totalUsageMonths."' AND ICS.slab_end_month>='".$totalUsageMonths."' ");
			// 		// echo "<pre>";
			// 		// print_r($bsnDetail);
			// 		// exit;

			// 		if(isset($icsDetail[0]->slab_discount) && $icsDetail[0]->slab_discount>0) {
			// 			return "Claim Slab(".$icsDetail[0]->slab_start_month."-".$icsDetail[0]->slab_end_month.") :".intVal($icsDetail[0]->slab_discount)."%";
			// 		}


			// 	}


			// }
			

			public function getBatteryProRataCliamSlabDetail($item_id, $totalUsageMonths, $warranty_period) {

				if($item_id>0) {

					$icsDetail = $this->_getSelectList2('table_item_claim_slabs AS ICS',"ICS.*",''," ICS.item_id ='".$item_id."' AND ICS.slab_start_month<='".$totalUsageMonths."' AND ICS.slab_end_month>='".$totalUsageMonths."' ");
					// echo "<pre>";
					// print_r($bsnDetail);
					// exit;

					if(isset($icsDetail[0]->slab_discount) && $icsDetail[0]->slab_discount>0) {
						return array("msg"=>"Claim Slab(".$icsDetail[0]->slab_start_month."-".$icsDetail[0]->slab_end_month.") :".intVal($icsDetail[0]->slab_discount)."%",'claimDiscount'=>intVal($icsDetail[0]->slab_discount)) ;
					} 

					if($warranty_period < $totalUsageMonths) {
						return array("msg"=>"But No Claim Slab found!", 'claimDiscount'=>0);
					}

					return "";
				}


			}
			

			



			/************************************************************************
			* DESC: Get the Battery Usage or check its in ProRata
			* AUTHOR : AJAY
			* CREATED : 2017-03-22
			*
			*
			************************************************************************/



 /***********************************************************************************
 * DESC: Battery Replacement Process
 * Author : AJAY
 * Created At: 2017-03-27
 *
 *
 ************************************************************************************/


  public function BatteryReplacementProcess ($warehouseId, $paramSet) {


  	$defective_bsn_id 		= $_POST['bsn_id'];
  	$replace_battery_bsn_no = $_POST['rpl_battery_bsn'];
  	$service_distributor_id = $_POST['service_distributor_id'];
  	$bcsStatus  			= $_POST['status'];

  	$jobcard_no  			= $_POST['jobcard_no'];
  	$bcf_no  				= $_POST['bcf_no'];
  	$mrn_no  				= trim($_POST['mrn_no']);

  	$status = false;
  	$battery_already_replaced = false;

  	if($warehouseId>0 && $service_distributor_id>0 ) {

	  	// echo "<pre>";
	  	// print_r($paramSet);
	  	// exit;

  		if($bcsStatus =='R' ) {

  			$complaintStatus = "R"; 		// P-Pending, I-InProcess, R-Rejected, C-Completed
  			$complaintResolutionStatus = ""; // R-Repair, RPL-Replace
  			$mrnDetailStatus = "C";			// P-Pending, I-InProcess, D-Delete, C-Completed, R-Rejected
  			$bcfStatus = "C";				// P-Pending, D-Delete, I-Inprocess,  R-Rejected, C-Completed
  			$jobcardStatus = "CLS"; 		// P-Pending, I-InProcess, D-Delete, C-Completed, R-Rejected, CLS-Close


  			// No Need Replacement battery, No need to update any stock
  			// Update the complaint and close the mrn bsn process


  		} else if($bcsStatus == C && !empty($replace_battery_bsn_no)  &&  $defective_bsn_id>0) {

  			$complaintStatus = "C";			// P-Pending, I-InProcess, R-Rejected, C-Completed
  			$complaintResolutionStatus = "RPL"; // R-Repair, RPL-Replace
  			$mrnDetailStatus = "C";			// P-Pending, I-InProcess, D-Delete, C-Completed, R-Rejected
  			$bcfStatus = "C";				// P-Pending, D-Delete, I-Inprocess,  R-Rejected, C-Completed
  			$jobcardStatus = "CLS"; 		// P-Pending, I-InProcess, D-Delete, C-Completed, R-Rejected, CLS-Close


  			$date_of_sale = "0000-00-00";
  			// Get the complaint detail of this BSN

  			$complaintDetail = $this->_getSelectList('table_complaint AS C',"C.*",''," C.bsn_id ='".$defective_bsn_id."' AND C.warehouse_id = '".$_SESSION['warehouseId']."' AND  C.complain_status IN ('I','P') ");

	  			if(sizeof($complaintDetail)>0 && isset($complaintDetail[0]->bsn_id)) {

	  			
	  			$complaint_id = $complaintDetail[0]->complaint_id;

				$complaint_distributor_id = $complaintDetail[0]->distributor_id;
				$issue_by_distributor_id = $complaintDetail[0]->issue_by_distributor_id;
				$issue_by_retailer_id = $complaintDetail[0]->issue_by_retailer_id;
				$replace_battery_bsn = $complaintDetail[0]->replace_battery_bsn;


				if(strlen(trim($replace_battery_bsn))>0 && $issue_by_retailer_id>0) {

					$battery_already_replaced = true;

				} else if(strlen(trim($replace_battery_bsn))>0 && $issue_by_distributor_id>0 ) {

					$battery_already_replaced = true;

				} else {
					$battery_already_replaced = false;
				}

				// Update the complaint status

			}
			// echo $battery_already_replaced;
			// exit;
			// Get BSN Detail

			//$bsnDetail = $this->_getSelectList('table_item_bsn AS BSN',"BSN.bsn_id, BSN.date_of_sale, BSN.status, BSN.customer_id, BSN.item_id, BSN,category_id, BSN.category_id",''," BSN.bsn_id ='".$defective_bsn_id."' AND BSN.status NOT IN  ('DMG','RPL') ");

			$bsnDetail = $this->_getSelectList('table_item_bsn AS BSN',"BSN.bsn_id, BSN.date_of_sale, BSN.status, BSN.customer_id, BSN.item_id, BSN,category_id, BSN.category_id, BSN.warranty_end_date",''," BSN.bsn_id ='".$defective_bsn_id."'");

			if(isset($bsnDetail[0]->bsn_id)) {

				$date_of_sale = $bsnDetail[0]->date_of_sale;
				$warranty_end_date = $bsnDetail[0]->warranty_end_date;
				$customer_id = $bsnDetail[0]->customer_id;
				$bsn_id    = $bsnDetail[0]->bsn_id;
				$item_id    = $bsnDetail[0]->item_id;
				$category_id = $bsnDetail[0]->category_id;

				// Update Defective Battery Status

				$bsnArray = array();
				$bsnArray['user_type'] 		        =  (isset($_SESSION['userLoginType']))?$_SESSION['userLoginType']:0;	
				$bsnArray['web_user_id'] 		    =  (isset($_SESSION['userLoginType']))?$_SESSION['PepUpSalesUserId']:0;

				if($battery_already_replaced == true) 
					$bsnArray['wh_issue_new_bsn']		= 	trim($replace_battery_bsn_no);
				else 
					$bsnArray['replace_battery_bsn']	=  trim($replace_battery_bsn_no);


				$bsnArray['last_updated_date']	 	=  date('Y-m-d');	
				$bsnArray['last_update_datetime']	=  date('Y-m-d H:i:s');	
				$bsnArray['status'] 				=  "RPL";


				$this->_dbUpdate($bsnArray,'table_item_bsn', "bsn_id='".$defective_bsn_id."'") ;



				// Get the Complaint Detail

				// AJAY@2017-03-27

				if($battery_already_replaced == false) {

					// Update the status of replace battery BSN as SOLD and forward the warranty detail of old bsn to this battery

					$bsnArray = array();
					$bsnArray['dispatch_warehouse_id']  =   $_SESSION['warehouseId'];
					$bsnArray['user_type'] 		        =  (isset($_SESSION['userLoginType']))?$_SESSION['userLoginType']:0;	
					$bsnArray['web_user_id'] 		    =  (isset($_SESSION['userLoginType']))?$_SESSION['PepUpSalesUserId']:0;
					$bsnArray['customer_id']			=  $customer_id;
					$bsnArray['date_of_sale']	 		=  $date_of_sale;
					$bsnArray['warranty_end_date']	 	=  $warranty_end_date;
					$bsnArray['last_updated_date']	 	=  date('Y-m-d');	
					$bsnArray['last_update_datetime']	=  date('Y-m-d H:i:s');	
					$bsnArray['status'] 				=  "S";

					$this->_dbUpdate($bsnArray,'table_item_bsn', "LOWER(bsn)='".mysql_escape_string(strtolower(trim($replace_battery_bsn_no)))."'") ;

					// Update warehouse stock also
					$whStock = array();

					$whStock['last_updated_date']	 	=  date('Y-m-d');	
					$whStock['last_update_datetime']	=  date('Y-m-d H:i:s');	
					$whStock['created_datetime']		=  date('Y-m-d H:i:s');	
					$whStock['status']		            =  'S';

					$this->_dbUpdate($whStock,'table_warehouse_stock', " bsn_id ='".$bsn_id."'") ; 


				} else if($battery_already_replaced == true) {

					// Add into warehouse stock(AS In-Process) of replace battery BSN and mark bsn status as dispatched into bsn table.

					$bsnArray = array();
					$bsnArray['dispatch_warehouse_id']  =   $_SESSION['warehouseId'];
					$bsnArray['user_type'] 		        =  (isset($_SESSION['userLoginType']))?$_SESSION['userLoginType']:0;	
					$bsnArray['web_user_id'] 		    =  (isset($_SESSION['userLoginType']))?$_SESSION['PepUpSalesUserId']:0;
					$bsnArray['last_updated_date']	 	=  date('Y-m-d');	
					$bsnArray['last_update_datetime']	=  date('Y-m-d H:i:s');	
					$bsnArray['status'] 				=  "D";

					$this->_dbUpdate($bsnArray,'table_item_bsn', "LOWER(bsn)='".mysql_escape_string(strtolower(trim($replace_battery_bsn_no)))."'") ;

					// Update warehouse stock also
					$whStock = array();

					$whStock['last_updated_date']	 	=  date('Y-m-d');	
					$whStock['last_update_datetime']	=  date('Y-m-d H:i:s');	
					$whStock['created_datetime']		=  date('Y-m-d H:i:s');	
					$whStock['status']		            =  'D';

					$this->_dbUpdate($whStock,'table_warehouse_stock', " bsn_id ='".$bsn_id."' AND status != 'DMG' ") ; 


					// Add Warehouse dispatches to distributor activity


					$condi = " distributor_id = '".$service_distributor_id."' AND bsn_id ='".$bsn_id."' AND status IN ('A', 'I')";
					$resultset = $this->_getSelectList('table_item_dis_stk_inprocess','*','',$condi);

					//exit;

					if(is_array($resultset) && sizeof($resultset)==0)  {   
					// Update Warehouse dispatches to distributor activity
						
						$disInProcess = array();

						$random_number = rand();

						$disInProcess['account_id']              	= $_SESSION['accountId'];
						$disInProcess['warehouse_id']      			= $warehouseId;	
						$disInProcess['distributor_id'] 			= $service_distributor_id;
						$disInProcess['item_id']                    = $item_id;
						$disInProcess['bsn_id']                    	= $bsn_id;
						$disInProcess['category_id']                = $category_id;
						$disInProcess['attribute_value_id']  		= 0;
						$disInProcess['bill_date'] 					= date('Y-m-d');
						$disInProcess['bill_no']                    = $random_number;
						$disInProcess['rec_stock_value']            = 1;
						$disInProcess['delivery_challan_no']        = $random_number;
						$disInProcess['delivery_challan_date']      = date('Y-m-d');

						$disInProcess['last_update_datetime']   	= date('Y-m-d H:i:s');	
						$disInProcess['created_datetime'] 			= date('Y-m-d H:i:s');
						$disInProcess['status']  					= 'I';
						// print_r($disInProcess);
						// exit;

						$dis_stk_inpro_id = $this->_dbInsert($disInProcess,'table_item_dis_stk_inprocess');
					}


				} // BSN replaced by ditributor or dealer

			}	// Check BSN exists


		} 




			// Update the complaint status as rejected

  				$complaintArray = array();

  				if($battery_already_replaced == false) {
  					$complaintArray['replace_battery_bsn'] 			=  $replace_battery_bsn_no;
  					//$complaintArray['issue_by_distributor_id'] 	=  $complaintStatus;
  					//$complaintArray['issue_by_retailer_id'] 		=  $complaintStatus;
  					$complaintArray['issue_by_warehouse_id'] 		=  $warehouseId;
  					$complaintArray['date_of_new_bsn_issue'] 		=  date('Y-m-d');
  				}

  				
  				$complaintArray['last_updated_date']			=  date('Y-m-d H:i:s');	
				$complaintArray['complain_status'] 				=  $complaintStatus;

  				$this->_dbUpdate($complaintArray,'table_complaint AS C', " C.bsn_id ='".$defective_bsn_id."' AND C.warehouse_id = '".$_SESSION['warehouseId']."' AND  C.complain_status IN ('I','P') ") ;


  			// Update the mrn detail status as completed

			$mrnDetail = $this->_getSelectList2('table_wh_mrn_detail AS MRND LEFT JOIN table_wh_mrn AS MRN ON MRN.mrn_id = MRND.mrn_id',"MRND.mrn_id, MRND.mrn_detail_id, MRN.mrn_no, MRN.mrn_date",'',"  MRND.bsn_id ='".$defective_bsn_id."' AND MRN.warehouse_id = '".$_SESSION['warehouseId']."' AND  MRN.mrn_no = '".$mrn_no."'  ");
				
				if(isset($mrnDetail[0]->mrn_detail_id)) {

				$mrn_detail_id = $mrnDetail[0]->mrn_detail_id;

  				$mrnDetailArray = array();
  				
  				$mrnDetailArray['last_update_date']		=  date('Y-m-d H:i:s');	
				$mrnDetailArray['status'] 				=  $mrnDetailStatus;

  				$this->_dbUpdate2($mrnDetailArray,'table_wh_mrn_detail AS MRND', "  MRND.mrn_detail_id = '".$mrn_detail_id."' ") ;
					
				}

  			// Update the bcf status as completed

			 $bcfDetail = $this->_getSelectList('table_wh_mrn_bcf AS BCF',"BCF.bcf_id",''," LOWER(BCF.bcf_no) ='".strtolower($bcf_no)."'");

				if(isset($bcfDetail[0]->bcf_id) && $bcfDetail[0]->bcf_id>0) {

					$bcf_id = $bcfDetail[0]->bcf_id;

	  				$bcfArray = array();
	  				$bcfArray['complaint_id']				= $complaint_id;
	  				$bcfArray['last_update_date']			=  date('Y-m-d H:i:s');	
					$bcfArray['status'] 					=  $bcfStatus;

	  				$this->_dbUpdate($bcfArray,'table_wh_mrn_bcf AS BCF', "  BCF.bcf_id = '".$bcf_id."' ") ;
	  			}


  			// Update the jobcard status as completed

				 $JCDetail = $this->_getSelectList('table_wh_mrn_bcf_jobcard AS JC',"JC.jobcard_id",''," LOWER(JC.jobcard_no) ='".strtolower($jobcard_no)."'");

				if(isset($JCDetail[0]->jobcard_id) && $JCDetail[0]->jobcard_id>0) {

					$jobcard_id = $JCDetail[0]->jobcard_id;

	  				$JCArray = array();
	  				$JCArray['last_update_date']		=  date('Y-m-d H:i:s');	
					$JCArray['status'] 					=  $jobcardStatus;

	  				$this->_dbUpdate($JCArray,'table_wh_mrn_bcf_jobcard AS JC', "  JC.jobcard_id = '".$jobcard_id."' ") ;
	  			}



	  	$status = true;

  	} 





  	return $status;

  }







	/*****************************************************************************
	 * DESC: Create FOC Invoice into tally
	 *       Three type of invoices:-
	 *       1. Defective battery return foc invoice with deleivery challan as mrn return
	 *       2. Issue new battery as goodwill replacement & Create FOC invoice into tally with details of both batteries.
	 *       3. Issue new battery cause of prorata warranty & create FOC Invoice into tally with item price or claim slab discount 
	 *          with both battery detail.
	 * Author: AJAY
	 * Created: 2017-04-13
	 *
	*****************************************************************************/


  public function generateFocInvoiceInTally ($bcs_id) {


  	// Firstly check the status of bcs 

  	if($bcs_id>0) {

  		$bcsDetail = $this->_getSelectList2(" table_wh_mrn_bcs AS BCS 
  			LEFT JOIN table_wh_mrn_bcf_jobcard AS JC ON JC.jobcard_id = BCS.jobcard_id 
  			LEFT JOIN table_item_bsn AS BSN ON BSN.bsn_id = BCS.bcs_id
  			LEFT JOIN table_item AS I ON I.item_id = BSN.item_id
  			LEFT JOIN table_distributors AS D ON D.distributor_id = BCS.dispatch_distributor_id
  			","BCS.bcs_id, BCS.status, JC.is_goodwill, JC.goodwill_approve, BCS.is_prorata, BCS.foc_discount, BCS.defective_battery_bsn, BCS.rpl_battery_bsn, BCS.rpl_battery_bsn_id, BCS.foc_item_id, BCS.foc_item_price, BCS.foc_invoice_tally_id, BCS.foc_voucher_id, I.item_id AS defective_item_id, I.item_name AS defective_item_name, I.item_code AS defective_item_code, D.distributor_name, D.distributor_code, D.distributor_address, D.distributor_phone_no, BSN.date_of_sale, BSN.warranty_period, BSN.warranty_prorata, BCS.dispatch_distributor_id ",''," BCS.bcs_id ='".$bcs_id."' AND BCS.status IN ('C', 'R')");

		// echo "<pre>";
		// print_r($bcsDetail);
		// exit;


	  		if(sizeof($bcsDetail)>0 && isset($bcsDetail[0]->bcs_id) && $bcsDetail[0]->foc_invoice_tally_id ==0 && $bcsDetail[0]->foc_voucher_id == 0) {

	  


	  			// Create Invoice and get the voucher id

	  			$bcsStatus 					= $bcsDetail[0]->status;
	  			$foc_invoice_tally_id 		= $bcsDetail[0]->foc_invoice_tally_id;
	  			$rpl_battery_bsn 			= trim($bcsDetail[0]->rpl_battery_bsn);
	  			$rpl_battery_bsn_id 		= trim($bcsDetail[0]->rpl_battery_bsn_id);


	  			// Dispatch Distributor detail
	  			$distributor_id             = $bcsDetail[0]->dispatch_distributor_id;
	  			$distributor_name           = $bcsDetail[0]->distributor_name;
	  			$distributor_code           = $bcsDetail[0]->distributor_code;
	  			$distributor_address        = $bcsDetail[0]->distributor_address;
	  			$distributor_phone_no       = $bcsDetail[0]->distributor_phone_no;

	  			// Defective Battery Detail

	  			$defective_item_id          = $bcsDetail[0]->defective_item_id;
	  			$defective_item_name        = $bcsDetail[0]->defective_item_name;
	  			$defective_item_code        = $bcsDetail[0]->defective_item_code;
	  			$defective_battery_bsn      = $bcsDetail[0]->defective_battery_bsn;
	  			$date_of_sale      			= $bcsDetail[0]->date_of_sale;
	  			$warranty_period      		= $bcsDetail[0]->warranty_period;
	  			$warranty_prorata     		= $bcsDetail[0]->warranty_prorata;


	  			
	  			// Is Goodwill Replacement
	  			$is_goodwill 				= strtolower(trim($bcsDetail[0]->is_goodwill));
	  			$goodwill_approve 			= strtolower(trim($bcsDetail[0]->goodwill_approve));

	  			// Prorata warranty
	  			$is_prorata 				= strtolower(trim($bcsDetail[0]->is_prorata));
	  			$foc_item_price 			= $bcsDetail[0]->foc_item_price;
	  			$foc_item_id 				= $bcsDetail[0]->foc_item_id;
	  			$foc_discount 				= $bcsDetail[0]->foc_discount;
	  			

				$requestParam = array();


				$requestParam['distributor_id'] 			= $distributor_id;
				$requestParam['distributor_name'] 			= $distributor_name;
				$requestParam['distributor_code'] 			= $distributor_code;
				$requestParam['distributor_address'] 		= $distributor_address;
				$requestParam['distributor_phone_no'] 		= $distributor_phone_no;

				$requestParam['defective_item_id'] 			= $defective_item_id;
				$requestParam['defective_item_name'] 		= $defective_item_name;
				$requestParam['defective_battery_bsn'] 		= $defective_battery_bsn;
				$requestParam['defective_item_code'] 		= $defective_item_code;

				$requestParam['rpl_bsn_price']				= $foc_item_price;
				$requestParam['rpl_bsn_foc_discount']		= $foc_discount;



				if($rpl_battery_bsn_id>0) {

					$bsnDetail = $this->_getSelectList('table_item_bsn AS BSN LEFT JOIN table_item AS I ON I.item_id = BSN.item_id ',"BSN.item_id, I.item_name, I.item_code, BSN.bsn_id",''," BSN.bsn_id ='".$rpl_battery_bsn_id."'");
				} else {

					$bsnDetail = $this->_getSelectList('table_item_bsn AS BSN LEFT JOIN table_item AS I ON I.item_id = BSN.item_id ',"BSN.item_id, I.item_name, I.item_code, BSN.bsn_id",''," LOWER(BSN.bsn) ='".strtolower($rpl_battery_bsn)."'");

				}


					// echo "<pre>";
					// print_r($bsnDetail);

					if(isset($bsnDetail[0]->bsn_id)) {

						$replace_bsn_id    		= $bsnDetail[0]->bsn_id;
						$replace_bsn_item_id    = $bsnDetail[0]->item_id;
						$replace_bsn_item_name  = $bsnDetail[0]->item_name;


						$requestParam['replace_bsn_id'] 			= $replace_bsn_id;
						$requestParam['replace_bsn_item_id'] 		= $replace_bsn_item_id;
						$requestParam['replace_bsn_item_name'] 		= $replace_bsn_item_name;
						$requestParam['rpl_battery_bsn']			= $rpl_battery_bsn;


					}


				if($bcsStatus =='C' && $distributor_id>0) {


					// Its Goodwill Replacement
					if($is_goodwill == 'y' && $goodwill_approve=='y' && $foc_invoice_tally_id==0 && !empty($rpl_battery_bsn)) {

						//echo "Goodwill Replacement";
						// Generate NEW Battery FOC INVOICE into tally AS Goodwill Replacement	

						$requestParam['status']			= "Goodwill";
						return $tallyResponse 			=	$this->pushTallyFOCInvoiceINTally((object)$requestParam);




					} else if($is_prorata == 'y' && $foc_invoice_tally_id==0 && $foc_discount>0 && $foc_item_id>0 && $foc_item_price>0) {
						
						//echo "ProRata Warranty";
						// Generate NEW Battery FOC INVOICE into tally AS Prorata replacement & get the slab discount.
						$warrantyUpTo = $this->getBSNWarrantyEndDate ($date_of_sale, $warranty_period, $warranty_prorata);

						//if(strtotime($warrantyUpTo) >= strtotime(date('Y-m-d')) ){
							$requestParam['status']			= "Prorata";
							return $tallyResponse 			=	$this->pushTallyFOCInvoiceINTally((object)$requestParam);
						// } else {
						// 	return $error = "Battery Not into warranty";
						// }



					
					} else if($foctype=='rejectfoc')
						{
							$warrantyUpTo = $this->getBSNWarrantyEndDate ($date_of_sale, $warranty_period, $warranty_prorata);
							$requestParam['status']			= 	"Recharged";
							return $tallyResponse 			=	$this->pushTallyFOCInvoiceINTally((object)$requestParam);
						}
					 else if($foctype=='unsoldfoc')
					 {
					 	// $warrantyUpTo = $this->getBSNWarrantyEndDate ($date_of_sale, $warranty_period, $warranty_prorata);
						$requestParam['status']			= 	"unsoldfoc";
						return $tallyResponse 			=	$this->pushTallyFOCInvoiceINTally((object)$requestParam);
					 }

					 else {

					//echo "Warranty Replacement";	
					// Generate Replace Battery FOC INVOICE into tally AS warranty replacement


						$warrantyUpTo = $this->getBSNWarrantyEndDate ($date_of_sale, $warranty_period, $warranty_prorata);

						//if(strtotime($warrantyUpTo) >= strtotime(date('Y-m-d')) ){
							$requestParam['status']			=   "Warranty";
							return $tallyResponse 			=	$this->pushTallyFOCInvoiceINTally((object)$requestParam);
						// } else {
						// 	return $error = "Battery Not into warranty";
						// }


					}



					

			 	} else if($bcsStatus =='R') {

			 		//echo "Rejected";
			 		// Generate defective Battery FOC INVOICE into tally AS MRN Return
						$requestParam['status']			=   "Return";
						return $tallyResponse 			=	$this->pushTallyFOCInvoiceINTally((object)$requestParam);
			 	}
	  		} else if(sizeof($bcsDetail)>0 && isset($bcsDetail[0]->bcs_id) && $bcsDetail[0]->foc_invoice_tally_id>0 && $bcsDetail[0]->foc_voucher_id == 0) {
	  
	  			return $this->getVoucherNumberFromTally($bcsDetail[0]->foc_invoice_tally_id);

	  		}


  	} 








  }


	/*****************************************************************************
	 * DESC: Create FOC Invoice into tally
	 *       Three type of invoices:-
	 *       1. Defective battery return foc invoice with deleivery challan as mrn return
	 *       2. Issue new battery as goodwill replacement & Create FOC invoice into tally with details of both batteries.
	 *       3. Issue new battery cause of prorata warranty & create FOC Invoice into tally with item price or claim slab discount 
	 *          with both battery detail.
	 * Author: AJAY
	 * Created: 2017-04-13
	 *
	*****************************************************************************/







	public function pushTallyFOCInvoiceINTally ($r) {

		// echo "<pre>";
		// print_r($r);
		// exit;

		if(sizeof($r)>0 && !empty($r->distributor_name) && !empty($r->defective_item_name) && !empty($r->defective_battery_bsn) ){ 


				$address 				= $r->distributor_address;
				$phone_no 				= $r->distributor_phone_no;
				$dist_code 				= $r->distributor_code;
				$name 					= $r->distributor_name;
				$DATE 					= date('Ymd', strtotime(date('y-m-d'))); // 20171202


			if($r->status == "Goodwill") {


				$STOCKITEMNAME 			= $r->replace_bsn_item_name;
				$BASICUSERDESCRIPTION   = "Old BSN(".$r->defective_battery_bsn."), Replace BSN(".$r->rpl_battery_bsn.")";
				$RATE 					=  0;
				$DISCOUNT 			 	=  0;
				$AMOUNT 				=  0;



			} else if($r->status == "Prorata"){

				$STOCKITEMNAME 			= $r->replace_bsn_item_name;
				$BASICUSERDESCRIPTION   = "Old BSN(".$r->defective_battery_bsn."), Replace BSN(".$r->rpl_battery_bsn.")";
				$RATE 					= $r->rpl_bsn_price;
				$DISCOUNT 			 	= $r->rpl_bsn_foc_discount;
				$AMOUNT 				= $RATE - ($RATE*$DISCOUNT)/100;



			} else if($r->status == "Warranty"){

				$STOCKITEMNAME 			= $r->replace_bsn_item_name;
				$BASICUSERDESCRIPTION   = "Old BSN(".$r->defective_battery_bsn."), Replace BSN(".$r->rpl_battery_bsn.")";
				$RATE 					=  0;
				$DISCOUNT 			 	=  0;
				$AMOUNT 				=  0;



			} else if($r->status == "Return" || $r->status == "Recharged"){


				
				$STOCKITEMNAME 			= $r->defective_battery_bsn;
				$BASICUSERDESCRIPTION   = "Old BSN(".$r->defective_battery_bsn.") Return ";
				$RATE 					=  0;
				$DISCOUNT 			 	=  0;
				$AMOUNT 				=  0;
			}
			else if($r->status == "unsoldfoc")
			{
				$STOCKITEMNAME 			= $r->defective_battery_bsn;
				$BASICUSERDESCRIPTION   = "Old BSN(".$r->defective_battery_bsn."), Replace BSN(".$r->rpl_battery_bsn.")";
				$RATE 					=  0;
				$DISCOUNT 			 	=  0;
				$AMOUNT 				=  0;
			}


		$requestXML = '<ENVELOPE><HEADER><TALLYREQUEST>Import Data</TALLYREQUEST></HEADER>
       <BODY>
       <IMPORTDATA>

       <REQUESTDESC>
       <REPORTNAME>Vouchers</REPORTNAME>
       <STATICVARIABLES>
       <SVCURRENTCOMPANY>EastmanLtd</SVCURRENTCOMPANY>
       </STATICVARIABLES>
       </REQUESTDESC>

       <REQUESTDATA>
       <TALLYMESSAGE>
       <VOUCHER VCHTYPE="Complaint Voucher" ACTION="Create" OBJVIEW="Invoice Voucher View">
       <ADDRESS.LIST TYPE="String">
       <ADDRESS>'.$address.'</ADDRESS>
       </ADDRESS.LIST>
       <BASICBUYERADDRESS.LIST TYPE="String">
       <BASICBUYERADDRESS>'.$address.'</BASICBUYERADDRESS>
       <BASICBUYERADDRESS>'.$phone_no.'</BASICBUYERADDRESS>
       </BASICBUYERADDRESS.LIST>
       <OLDAUDITENTRYIDS.LIST TYPE="Number">
       <OLDAUDITENTRYIDS>-1</OLDAUDITENTRYIDS>
       </OLDAUDITENTRYIDS.LIST>
       <DATE>'.$DATE.'</DATE>
 
       <VATDEALERTYPE/>
       <TAXUNITNAME>Default Tax Unit</TAXUNITNAME>
       <PARTYNAME>'.$name."(".$dist_code.")".'</PARTYNAME>
       <VOUCHERTYPENAME>Complaint Voucher</VOUCHERTYPENAME>
       <REFERENCE></REFERENCE>
       <VOUCHERNUMBER>1</VOUCHERNUMBER>
       <PARTYLEDGERNAME>'.$name."(".$dist_code.")".'</PARTYLEDGERNAME>
       <BASICBASEPARTYNAME>'.$name."(".$dist_code.")".'</BASICBASEPARTYNAME>
       <CSTFORMISSUETYPE/><CSTFORMRECVTYPE/>
       <FBTPAYMENTTYPE>Default</FBTPAYMENTTYPE>
       <PERSISTEDVIEW>Invoice Voucher View</PERSISTEDVIEW>
       <BASICBUYERNAME>'.$name.'</BASICBUYERNAME>
       
       <VCHGSTCLASS/>
       <ENTEREDBY>a</ENTEREDBY>
       <DIFFACTUALQTY>No</DIFFACTUALQTY>
       <ISMSTFROMSYNC>No</ISMSTFROMSYNC>
       <ASORIGINAL>No</ASORIGINAL>
       <AUDITED>No</AUDITED>
       <FORJOBCOSTING>No</FORJOBCOSTING>
       <ISOPTIONAL>No</ISOPTIONAL>
      
       <USEFOREXCISE>No</USEFOREXCISE>
       <ISFORJOBWORKIN>No</ISFORJOBWORKIN>
       <ALLOWCONSUMPTION>No</ALLOWCONSUMPTION>
       <USEFORINTEREST>No</USEFORINTEREST>
       <USEFORGAINLOSS>No</USEFORGAINLOSS>
       <USEFORGODOWNTRANSFER>No</USEFORGODOWNTRANSFER>
       <USEFORCOMPOUND>No</USEFORCOMPOUND>
       <USEFORSERVICETAX>No</USEFORSERVICETAX>
       <ISEXCISEVOUCHER>No</ISEXCISEVOUCHER>
       <EXCISETAXOVERRIDE>No</EXCISETAXOVERRIDE>
       <USEFORTAXUNITTRANSFER>No</USEFORTAXUNITTRANSFER>
       <EXCISEOPENING>No</EXCISEOPENING>
       <USEFORFINALPRODUCTION>No</USEFORFINALPRODUCTION>
       <ISTDSOVERRIDDEN>No</ISTDSOVERRIDDEN>
       <ISTCSOVERRIDDEN>No</ISTCSOVERRIDDEN>
       <ISTDSTCSCASHVCH>No</ISTDSTCSCASHVCH>
       <INCLUDEADVPYMTVCH>No</INCLUDEADVPYMTVCH>
       <ISSUBWORKSCONTRACT>No</ISSUBWORKSCONTRACT>
       <ISVATOVERRIDDEN>No</ISVATOVERRIDDEN>
       <IGNOREORIGVCHDATE>No</IGNOREORIGVCHDATE>
       <ISSERVICETAXOVERRIDDEN>No</ISSERVICETAXOVERRIDDEN>
       <ISISDVOUCHER>No</ISISDVOUCHER>
       <ISEXCISEOVERRIDDEN>No</ISEXCISEOVERRIDDEN>
       <ISEXCISESUPPLYVCH>No</ISEXCISESUPPLYVCH>
       <ISVATPRINCIPALACCOUNT>No</ISVATPRINCIPALACCOUNT>
       <ISSHIPPINGWITHINSTATE>No</ISSHIPPINGWITHINSTATE>
       <ISCANCELLED>No</ISCANCELLED>
       <HASCASHFLOW>No</HASCASHFLOW>
       <ISPOSTDATED>No</ISPOSTDATED>
       <USETRACKINGNUMBER>No</USETRACKINGNUMBER>
       <ISINVOICE>Yes</ISINVOICE>
       <MFGJOURNAL>No</MFGJOURNAL>
       <HASDISCOUNTS>No</HASDISCOUNTS>
       <ASPAYSLIP>No</ASPAYSLIP>
       <ISCOSTCENTRE>No</ISCOSTCENTRE>
       <ISSTXNONREALIZEDVCH>No</ISSTXNONREALIZEDVCH>
       <ISEXCISEMANUFACTURERON>No</ISEXCISEMANUFACTURERON>
       <ISBLANKCHEQUE>No</ISBLANKCHEQUE>
       <ISVOID>No</ISVOID>
       <ISONHOLD>No</ISONHOLD>
       <ORDERLINESTATUS>No</ORDERLINESTATUS>
       <VATISAGNSTCANCSALES>No</VATISAGNSTCANCSALES>
       <VATISPURCEXEMPTED>No</VATISPURCEXEMPTED>
       <ISVATRESTAXINVOICE>No</ISVATRESTAXINVOICE>
       <VATISASSESABLECALCVCH>Yes</VATISASSESABLECALCVCH>
       <ISDELETED>No</ISDELETED>
       <CHANGEVCHMODE>No</CHANGEVCHMODE>
       <ALTERID> </ALTERID>
       <MASTERID> </MASTERID>
    
       <EXCLUDEDTAXATIONS.LIST>      
       </EXCLUDEDTAXATIONS.LIST>
       <OLDAUDITENTRIES.LIST>      </OLDAUDITENTRIES.LIST>
       <ACCOUNTAUDITENTRIES.LIST>      </ACCOUNTAUDITENTRIES.LIST>
       <AUDITENTRIES.LIST>      </AUDITENTRIES.LIST>
       <DUTYHEADDETAILS.LIST>      </DUTYHEADDETAILS.LIST>
       <SUPPLEMENTARYDUTYHEADDETAILS.LIST>      </SUPPLEMENTARYDUTYHEADDETAILS.LIST>
       <INVOICEDELNOTES.LIST>      </INVOICEDELNOTES.LIST>
       <INVOICEORDERLIST.LIST>      </INVOICEORDERLIST.LIST>
       <INVOICEINDENTLIST.LIST>      </INVOICEINDENTLIST.LIST>
       <ATTENDANCEENTRIES.LIST>      </ATTENDANCEENTRIES.LIST>
       <ORIGINVOICEDETAILS.LIST>      </ORIGINVOICEDETAILS.LIST>
       <INVOICEEXPORTLIST.LIST>      </INVOICEEXPORTLIST.LIST>
       <LEDGERENTRIES.LIST>
       <OLDAUDITENTRYIDS.LIST TYPE="Number">
       <OLDAUDITENTRYIDS>-1</OLDAUDITENTRYIDS>
       </OLDAUDITENTRYIDS.LIST>
       <LEDGERNAME>'.$name.'</LEDGERNAME>
       <GSTCLASS/>
       <ISDEEMEDPOSITIVE>Yes</ISDEEMEDPOSITIVE>
       <LEDGERFROMITEM>No</LEDGERFROMITEM>
       <REMOVEZEROENTRIES>No</REMOVEZEROENTRIES>
       <ISPARTYLEDGER>Yes</ISPARTYLEDGER>
       
       <INPUTCRALLOCS.LIST>       
       </INPUTCRALLOCS.LIST>
       <DUTYHEADDETAILS.LIST>       
       </DUTYHEADDETAILS.LIST>
       <EXCISEDUTYHEADDETAILS.LIST>      
        </EXCISEDUTYHEADDETAILS.LIST>
        <SUMMARYALLOCS.LIST>       
        </SUMMARYALLOCS.LIST>
        <STPYMTDETAILS.LIST>       
        </STPYMTDETAILS.LIST>
        <EXCISEPAYMENTALLOCATIONS.LIST>       
        </EXCISEPAYMENTALLOCATIONS.LIST>
        
        <VATITCDETAILS.LIST>    </VATITCDETAILS.LIST>
        </LEDGERENTRIES.LIST>
        <ALLINVENTORYENTRIES.LIST>
        <BASICUSERDESCRIPTION.LIST TYPE="String">
        <BASICUSERDESCRIPTION>'.$BASICUSERDESCRIPTION.'</BASICUSERDESCRIPTION>
        </BASICUSERDESCRIPTION.LIST>
        <STOCKITEMNAME>'.$STOCKITEMNAME.'</STOCKITEMNAME>
        <ISDEEMEDPOSITIVE>No</ISDEEMEDPOSITIVE>
        <ISLASTDEEMEDPOSITIVE>No</ISLASTDEEMEDPOSITIVE>
        <ISAUTONEGATE>No</ISAUTONEGATE>
        <ISCUSTOMSCLEARANCE>No</ISCUSTOMSCLEARANCE>
        <ISTRACKCOMPONENT>No</ISTRACKCOMPONENT>
        <ISTRACKPRODUCTION>No</ISTRACKPRODUCTION>
        <ISPRIMARYITEM>No</ISPRIMARYITEM>
        <ISSCRAP>No</ISSCRAP>

        <RATE>'.$RATE.'</RATE>
        <DISCOUNT>'.$DISCOUNT.'</DISCOUNT>
        <AMOUNT>'.$AMOUNT.'</AMOUNT>
        <ACTUALQTY>1 Nos</ACTUALQTY>
        <BILLEDQTY>1 Nos</BILLEDQTY>



        <BATCHALLOCATIONS.LIST>
        <GODOWNNAME>AMPS Agra Sales</GODOWNNAME>
        <BATCHNAME>Primary Batch</BATCHNAME>
      
        <INDENTNO/>
      
        <TRACKINGNUMBER/>
        <DYNAMICCSTISCLEARED>No</DYNAMICCSTISCLEARED>
        
        <ADDITIONALDETAILS.LIST>        </ADDITIONALDETAILS.LIST>
        <VOUCHERCOMPONENTLIST.LIST>        </VOUCHERCOMPONENTLIST.LIST>
        </BATCHALLOCATIONS.LIST>


        <ACCOUNTINGALLOCATIONS.LIST>
        <OLDAUDITENTRYIDS.LIST TYPE="Number">
        <OLDAUDITENTRYIDS>-1</OLDAUDITENTRYIDS>
        </OLDAUDITENTRYIDS.LIST>

        <LEDGERNAME>Agra</LEDGERNAME>

        <GSTCLASS/>
        <ISDEEMEDPOSITIVE>No</ISDEEMEDPOSITIVE>
        <LEDGERFROMITEM>No</LEDGERFROMITEM>
        <REMOVEZEROENTRIES>No</REMOVEZEROENTRIES>
        <ISPARTYLEDGER>No</ISPARTYLEDGER>
        <ISLASTDEEMEDPOSITIVE>No</ISLASTDEEMEDPOSITIVE>
        <SERVICETAXDETAILS.LIST>        </SERVICETAXDETAILS.LIST>
       
        </ACCOUNTINGALLOCATIONS.LIST><DUTYHEADDETAILS.LIST>       
        </DUTYHEADDETAILS.LIST>
        <SUPPLEMENTARYDUTYHEADDETAILS.LIST>       </SUPPLEMENTARYDUTYHEADDETAILS.LIST>
        <TAXOBJECTALLOCATIONS.LIST>       </TAXOBJECTALLOCATIONS.LIST>
        <REFVOUCHERDETAILS.LIST>       </REFVOUCHERDETAILS.LIST>
        <EXCISEALLOCATIONS.LIST>       </EXCISEALLOCATIONS.LIST>
        <EXPENSEALLOCATIONS.LIST>       </EXPENSEALLOCATIONS.LIST>
        </ALLINVENTORYENTRIES.LIST>
        <PAYROLLMODEOFPAYMENT.LIST>     
         </PAYROLLMODEOFPAYMENT.LIST>
         <ATTDRECORDS.LIST>     
          </ATTDRECORDS.LIST>
          </VOUCHER>
          </TALLYMESSAGE>
          </REQUESTDATA>
          </IMPORTDATA>
          </BODY>
          </ENVELOPE>';






			// Curl request to tally server

	       $server = 'http://quytech14.ddns.net:8000/';
	       $headers = array( "Content-type: text/xml" ,"Content-length: ".strlen($requestXML) ,"Connection: close" );

	        $ch = curl_init();
	        curl_setopt($ch, CURLOPT_URL, $server);
	        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
	        curl_setopt($ch, CURLOPT_POST, true);
	        curl_setopt($ch, CURLOPT_POSTFIELDS, $requestXML);
	        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	        $data = curl_exec($ch);

	        if(curl_errno($ch)){
	               print curl_error($ch);
	               echo "  something went wrong..... try later";
	        } else {
	              $replace= preg_replace('/&#x?[0-9A-E]/', ' ', $data);
	              $xml = simplexml_load_string($replace);
	              $json = json_encode($xml);
	              $array = json_decode($json,TRUE);
	              curl_close($ch);
	              // print_r($array);
	              // exit;
	              return $array;
	       }



		// Tally Error Response
		// Array
		// (
		//     [LINEERROR] => Ledger 'AJAY 3' does not exist!
		//     [CREATED] => 0
		//     [ALTERED] => 0
		//     [DELETED] => 0
		//     [LASTVCHID] => 0
		//     [LASTMID] => 0
		//     [COMBINED] => 0
		//     [IGNORED] => 0
		//     [ERRORS] => 1
		//     [CANCELLED] => 0
		// )


			// Tally Success Response
		// 		Array
		// (
		//     [CREATED] => 1
		//     [ALTERED] => 0
		//     [DELETED] => 0
		//     [LASTVCHID] => 82
		//     [LASTMID] => 0
		//     [COMBINED] => 0
		//     [IGNORED] => 0
		//     [ERRORS] => 0
		//     [CANCELLED] => 0
		// )




		} 


	}
	/**************************************************************
	* DESC: Get Voucher Number from tally
	* Author: AJAY
	* Created: 2017-04-17
	*
	*
	*
	****************************************************************/



	public function getVoucherNumberFromTally ($foc_invoice_tally_id) {
			// echo "Hello2";
			// echo $foc_invoice_tally_id;
	  // 			exit;


		if($foc_invoice_tally_id>0) {

			$LASTVCHID = $foc_invoice_tally_id;

	  			// INVOICE ALREADY CREATED, NOW GOING TO GET VOUCHER ID


				$requestXML='<ENVELOPE>
				<HEADER>
				<VERSION>1</VERSION>
				<TALLYREQUEST>EXPORT DATA</TALLYREQUEST>
				<TYPE>Object</TYPE>
				<SUBTYPE>VOUCHER</SUBTYPE>
				<ID TYPE="Name">ID:'.$LASTVCHID.'</ID>
				</HEADER>
				<BODY>
				<DESC>
				<STATICVARIABLES>
				 <SVCURRENTCOMPANY>EastmanLtd</SVCURRENTCOMPANY>   
				<SVEXPORTFORMAT>$$SysName:XML</SVEXPORTFORMAT>
				</STATICVARIABLES>
				<FETCHLIST>
				<FETCH>Date</FETCH>
				<FETCH>VoucherTypeName</FETCH>
				<FETCH>VoucherNumber</FETCH>
				</FETCHLIST>
				</DESC>
				</BODY>
				</ENVELOPE>';

				   $array = array();
				   $server = 'http://quytech14.ddns.net:8000/';
				  // $server = 'http://192.168.1.26:9000/';
				   $headers = array( "Content-type: text/xml" ,"Content-length: ".strlen($requestXML) ,"Connection: close" );
				   $ch = curl_init();
				   curl_setopt($ch, CURLOPT_URL, $server);
				   curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
				   curl_setopt($ch, CURLOPT_TIMEOUT, 100);
				   curl_setopt($ch, CURLOPT_POST, true);
				   curl_setopt($ch, CURLOPT_POSTFIELDS, $requestXML);
				   curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
				   $data = curl_exec($ch);

				   //HEADER('Content-type:TEXT/XML');
				 
				    if(curl_errno($ch)){
				     // echo json_encode(array('status'=>'Failed', 'data'=> print curl_error($ch)));
				      return array('LINEERROR'=>curl_error($ch));

				    } else {

				     	$replace= preg_replace('/&#x?[0-9A-E]/', ' ', $data);
					     $xml = simplexml_load_string($replace);
					     $json = json_encode($xml);
					     $array = json_decode($json,TRUE);


				   		if(isset($array['BODY']['DATA']['TALLYMESSAGE']['VOUCHER']['VOUCHERNUMBER']) && $array['BODY']['DATA']['TALLYMESSAGE']['VOUCHER']['VOUCHERNUMBER']>0 ) {

				   			return array('UPDATED'=>1, 'VOUCHERNUMBER'=>$array['BODY']['DATA']['TALLYMESSAGE']['VOUCHER']['VOUCHERNUMBER']);
				   		} 
				    }


		} else {
			return array('LINEERROR'=>"Tally LASTVCHID Not Found!");
		}

	}




	/**************************************************************
	* DESC: Get Voucher Number from tally
	* Author: AJAY
	* Created: 2017-04-17
	*
	*
	*
	****************************************************************/



	/****************************************************************
	* DESC : Warehouse transfer stock to another warehouse
	* Author: AJAY
	* Created: 2017-04-25
	*
	*
	*****************************************************************/


		public function stockTransferToWH ($paramSet) {


		// echo "<pre>";
		// print_r($paramSet);
		// $tranferStockArray  = array_filter($paramSet['tranfer_stock_value']);
		// print_r($tranferStockArray);
		//exit;

		if(sizeof($paramSet)>0 && isset($paramSet['from_warehouse_id']) && $paramSet['from_warehouse_id']>0 && isset($paramSet['to_warehouse_id']) && $paramSet['to_warehouse_id']>0 && isset($paramSet['dispatch_no']) && !empty($paramSet['dispatch_no']) && isset($paramSet['dispatch_date']) && !empty($paramSet['dispatch_date']) ) {


			$tranferStockArray = array();
			$itemListArray     = array();
        	$warehouse_id 		= $paramSet['from_warehouse_id'];
			$to_warehouse_id     = $paramSet['to_warehouse_id'];
			$bill_no 	 		= trim($paramSet['dispatch_no']);
			$bill_date 	 		= date('Y-m-d', strtotime($paramSet['dispatch_date']));

//Tally wh to wh commeted by arvind

		  $warehouse_id 		= $paramSet['from_warehouse_id'];
		
		   $to_warehouse_id     = $paramSet['to_warehouse_id'];

		
		   $bill_no 	 		= trim($paramSet['dispatch_no']);
		   $bill_date 	 		= date('Y-m-d', strtotime($paramSet['dispatch_date']));
           $wh_state_id=$this->_getSelectList('table_warehouse AS WH',"state_id,warehouse_address,warehouse_name,warehouse_phone_no,city_id",''," WH.warehouse_id='".$warehouse_id."'  ");
           $state_id=$wh_state_id[0]->state_id;
           $warehouse_name=$wh_state_id[0]->warehouse_name;//From from warehouse

           $markup_value=$this->_getSelectList2('table_markup AS m',"price",''," m.warehouse_id='".$to_warehouse_id."'  ");
               $markuprate=$markup_value[0]->price; //price markup

           $wh_state_id1=$this->_getSelectList('table_warehouse AS WH',"state_id,warehouse_name,warehouse_phone_no,city_id,warehouse_address",''," WH.warehouse_id='".$to_warehouse_id."'  ");
           $state_id1=$wh_state_id1[0]->state_id;

           $Towarehouse_name=$wh_state_id1[0]->warehouse_name;//To from warehouse
            $dateFormat1 = date('Ymd'); // 20171202
            $DATE = $dateFormat1;

           $state_name_fromwh=$this->_getSelectList2('state AS s',"state_name",''," s.state_id='".$wh_state_id[0]->state_id."'  ");
           $city_id_fromwh=$this->_getSelectList2('city AS c',"city_name",''," c.city_id='".$wh_state_id[0]->city_id."'  ");

           $state_name_towh=$this->_getSelectList2('state AS s',"state_name",''," s.state_id='".$wh_state_id1[0]->state_id."'  ");
           $city_id_towh=$this->_getSelectList2('city AS c',"city_name",''," c.city_id='".$wh_state_id1[0]->city_id."'  ");


           if($state_id==$state_id1)
           {
             
	 		$wh_name=$warehouse_name;
	 		$Towh_name=$Towarehouse_name;
	 	    $arr = explode(' ',trim($wh_name));
	 	    $arr1 = explode(' ',trim($Towh_name));
	 	    $challan=ucwords(strtolower($arr[1]));
	 	    $challanto=ucwords(strtolower($arr1[1]));

            $voucher_type=ucwords(strtolower($arr[1]))." Delivery Challan GST";
            $voucher_typereport=ucwords(strtolower($arr1[1]))." Receipt Note";
           // $godown_name="CHN ". ucwords(strtolower($arr1[1]))." Sales";
           // $ledgername=$voucher_type." Stock Transfer Agnt F Form";
            $ledgernameto="Eastman Auto &amp; Power limited-".$challanto;
            $ledgername="Eastman Auto &amp; Power limited-".$challan;
            $ledgername_report_from=ucwords(strtolower($arr[1]))." Branch Transfer";
            $ledgername_report="Receipt Note ".ucwords(strtolower($arr1[1]));
            //$BASICUSERDESCRIPTION=$auRec[0]->bsn;
            $requestXMLSTART='<ENVELOPE>
			<HEADER>
			<TALLYREQUEST>Import Data</TALLYREQUEST>
			</HEADER>
			<BODY>
			<IMPORTDATA>
			<REQUESTDESC>
			<REPORTNAME>Vouchers</REPORTNAME>
			<STATICVARIABLES>
			<SVCURRENTCOMPANY>EastmanLtd</SVCURRENTCOMPANY>
			</STATICVARIABLES>
			</REQUESTDESC>
			<REQUESTDATA>
			<TALLYMESSAGE xmlns:UDF="TallyUDF">

			<VOUCHER VCHTYPE="'.$voucher_type.'" ACTION="Create" OBJVIEW="Invoice Voucher View">
			<ADDRESS.LIST TYPE="String">
            <ADDRESS>'.$wh_state_id1[0]->warehouse_address.' </ADDRESS>
            <ADDRESS>'.$city_id_towh[0]->city_name.','.$state_name_towh[0]->state_name.'</ADDRESS>
            <ADDRESS>Mobile No:'.$wh_state_id1[0]->warehouse_phone_no.'</ADDRESS>
           
            </ADDRESS.LIST>

			<BASICBUYERADDRESS.LIST TYPE="String">

			<BASICBUYERADDRESS>'.$wh_state_id1[0]->warehouse_address.'</BASICBUYERADDRESS>
			<BASICBUYERADDRESS>'.$city_id_towh[0]->city_name.','.$state_name_towh[0]->state_name.'</BASICBUYERADDRESS>
			<BASICBUYERADDRESS>Mobile No:'.$wh_state_id1[0]->warehouse_phone_no.'</BASICBUYERADDRESS>

			</BASICBUYERADDRESS.LIST>
			<OLDAUDITENTRYIDS.LIST TYPE="Number">
			<OLDAUDITENTRYIDS>-1</OLDAUDITENTRYIDS>
			</OLDAUDITENTRYIDS.LIST>
			<DATE>'.$DATE.'</DATE>
			<REFERENCEDATE>'.$DATE.'</REFERENCEDATE>

			<GSTREGISTRATIONTYPE></GSTREGISTRATIONTYPE>
			<VATDEALERTYPE></VATDEALERTYPE>
			<TAXUNITNAME>Default Tax Unit</TAXUNITNAME>
			<CLASSNAME></CLASSNAME>
			<PARTYNAME>'.$ledgernameto.'</PARTYNAME>
			<VOUCHERTYPENAME>'.$voucher_type.'</VOUCHERTYPENAME>
			<REFERENCE></REFERENCE>
			<VOUCHERNUMBER></VOUCHERNUMBER>
			<PARTYLEDGERNAME>'.$ledgernameto.'</PARTYLEDGERNAME>
			<BASICBASEPARTYNAME>'.$ledgernameto.'</BASICBASEPARTYNAME>
			<CSTFORMISSUETYPE/>
			<CSTFORMRECVTYPE/>
			<FBTPAYMENTTYPE>Default</FBTPAYMENTTYPE>
			<PERSISTEDVIEW>Invoice Voucher View</PERSISTEDVIEW>
			<BASICBUYERNAME>'.$ledgernameto.'</BASICBUYERNAME>
			<BASICFINALDESTINATION>'.$city_id_towh[0]->city_name.'</BASICFINALDESTINATION>
			<BASICDATETIMEOFINVOICE>3-Aug-2017 at 15:58</BASICDATETIMEOFINVOICE>
			<BASICDATETIMEOFREMOVAL>3-Aug-2017 at 15:58</BASICDATETIMEOFREMOVAL>
			<VCHGSTCLASS/>
			<ENTEREDBY></ENTEREDBY>
			<DIFFACTUALQTY>No</DIFFACTUALQTY>
			<ISMSTFROMSYNC>No</ISMSTFROMSYNC>
			<ASORIGINAL>No</ASORIGINAL>
			<AUDITED>No</AUDITED>
			<FORJOBCOSTING>No</FORJOBCOSTING>
			<ISOPTIONAL>No</ISOPTIONAL>
			<EFFECTIVEDATE>20170801</EFFECTIVEDATE>
			<USEFOREXCISE>No</USEFOREXCISE>
			<ISFORJOBWORKIN>No</ISFORJOBWORKIN>
			<ALLOWCONSUMPTION>No</ALLOWCONSUMPTION>
			<USEFORINTEREST>No</USEFORINTEREST>
			<USEFORGAINLOSS>No</USEFORGAINLOSS>
			<USEFORGODOWNTRANSFER>No</USEFORGODOWNTRANSFER>
			<USEFORCOMPOUND>No</USEFORCOMPOUND>
			<USEFORSERVICETAX>No</USEFORSERVICETAX>
			<ISEXCISEVOUCHER>No</ISEXCISEVOUCHER>
			<EXCISETAXOVERRIDE>No</EXCISETAXOVERRIDE>
			<USEFORTAXUNITTRANSFER>No</USEFORTAXUNITTRANSFER>
			<EXCISEOPENING>No</EXCISEOPENING>
			<USEFORFINALPRODUCTION>No</USEFORFINALPRODUCTION>
			<ISTDSOVERRIDDEN>No</ISTDSOVERRIDDEN>
			<ISTCSOVERRIDDEN>No</ISTCSOVERRIDDEN>
			<ISTDSTCSCASHVCH>No</ISTDSTCSCASHVCH>
			<INCLUDEADVPYMTVCH>No</INCLUDEADVPYMTVCH>
			<ISSUBWORKSCONTRACT>No</ISSUBWORKSCONTRACT>
			<ISVATOVERRIDDEN>No</ISVATOVERRIDDEN>
			<IGNOREORIGVCHDATE>No</IGNOREORIGVCHDATE>
			<ISSERVICETAXOVERRIDDEN>No</ISSERVICETAXOVERRIDDEN>
			<ISISDVOUCHER>No</ISISDVOUCHER>
			<ISEXCISEOVERRIDDEN>No</ISEXCISEOVERRIDDEN>
			<ISEXCISESUPPLYVCH>No</ISEXCISESUPPLYVCH>
			<ISGSTOVERRIDDEN>No</ISGSTOVERRIDDEN>
			<GSTNOTEXPORTED>No</GSTNOTEXPORTED>
			<ISVATPRINCIPALACCOUNT>No</ISVATPRINCIPALACCOUNT>
			<ISSHIPPINGWITHINSTATE>No</ISSHIPPINGWITHINSTATE>
			<ISCANCELLED>No</ISCANCELLED>
			<HASCASHFLOW>Yes</HASCASHFLOW>
			<ISPOSTDATED>No</ISPOSTDATED>
			<USETRACKINGNUMBER>No</USETRACKINGNUMBER>
			<ISINVOICE>Yes</ISINVOICE>
			<MFGJOURNAL>No</MFGJOURNAL>
			<HASDISCOUNTS>Yes</HASDISCOUNTS>
			<ASPAYSLIP>No</ASPAYSLIP>
			<ISCOSTCENTRE>No</ISCOSTCENTRE>
			<ISSTXNONREALIZEDVCH>No</ISSTXNONREALIZEDVCH>
			<ISEXCISEMANUFACTURERON>No</ISEXCISEMANUFACTURERON>
			<ISBLANKCHEQUE>No</ISBLANKCHEQUE>
			<ISVOID>No</ISVOID>
			<ISONHOLD>No</ISONHOLD>
			<ORDERLINESTATUS>No</ORDERLINESTATUS>
			<VATISAGNSTCANCSALES>No</VATISAGNSTCANCSALES>
			<VATISPURCEXEMPTED>No</VATISPURCEXEMPTED>
			<ISVATRESTAXINVOICE>No</ISVATRESTAXINVOICE>
			<VATISASSESABLECALCVCH>No</VATISASSESABLECALCVCH>
			<ISVATDUTYPAID>Yes</ISVATDUTYPAID>
			<ISDELIVERYSAMEASCONSIGNEE>No</ISDELIVERYSAMEASCONSIGNEE>
			<ISDISPATCHSAMEASCONSIGNOR>No</ISDISPATCHSAMEASCONSIGNOR>
			<ISDELETED>No</ISDELETED>
			<CHANGEVCHMODE>No</CHANGEVCHMODE>
			<ALTERID></ALTERID>
			<MASTERID></MASTERID>
			<VOUCHERKEY></VOUCHERKEY>
			<EXCLUDEDTAXATIONS.LIST></EXCLUDEDTAXATIONS.LIST>
			<OLDAUDITENTRIES.LIST></OLDAUDITENTRIES.LIST>
			<ACCOUNTAUDITENTRIES.LIST></ACCOUNTAUDITENTRIES.LIST>
			<AUDITENTRIES.LIST></AUDITENTRIES.LIST>
			<DUTYHEADDETAILS.LIST></DUTYHEADDETAILS.LIST>
			<SUPPLEMENTARYDUTYHEADDETAILS.LIST></SUPPLEMENTARYDUTYHEADDETAILS.LIST>
			<INVOICEDELNOTES.LIST></INVOICEDELNOTES.LIST>
			<INVOICEORDERLIST.LIST></INVOICEORDERLIST.LIST>
			<INVOICEINDENTLIST.LIST></INVOICEINDENTLIST.LIST>
			<ATTENDANCEENTRIES.LIST></ATTENDANCEENTRIES.LIST>
			<ORIGINVOICEDETAILS.LIST></ORIGINVOICEDETAILS.LIST>
			<INVOICEEXPORTLIST.LIST></INVOICEEXPORTLIST.LIST>
			<LEDGERENTRIES.LIST>
			<OLDAUDITENTRYIDS.LIST TYPE="Number">
			<OLDAUDITENTRYIDS>-1</OLDAUDITENTRYIDS>
			</OLDAUDITENTRYIDS.LIST>
			<LEDGERNAME>'.$ledgername.'</LEDGERNAME>
			<GSTCLASS/>
			<ISDEEMEDPOSITIVE>No</ISDEEMEDPOSITIVE>
			<LEDGERFROMITEM>No</LEDGERFROMITEM>
			<REMOVEZEROENTRIES>No</REMOVEZEROENTRIES>
			<ISPARTYLEDGER>Yes</ISPARTYLEDGER>
			<ISLASTDEEMEDPOSITIVE>No</ISLASTDEEMEDPOSITIVE>
			<AMOUNT></AMOUNT>
			<SERVICETAXDETAILS.LIST></SERVICETAXDETAILS.LIST>

			<BILLALLOCATIONS.LIST></BILLALLOCATIONS.LIST>
			<INTERESTCOLLECTION.LIST></INTERESTCOLLECTION.LIST>
			<OLDAUDITENTRIES.LIST></OLDAUDITENTRIES.LIST>
			<ACCOUNTAUDITENTRIES.LIST></ACCOUNTAUDITENTRIES.LIST>
			<AUDITENTRIES.LIST></AUDITENTRIES.LIST>
			<INPUTCRALLOCS.LIST></INPUTCRALLOCS.LIST>
			<DUTYHEADDETAILS.LIST></DUTYHEADDETAILS.LIST>
			<EXCISEDUTYHEADDETAILS.LIST></EXCISEDUTYHEADDETAILS.LIST>
			<RATEDETAILS.LIST></RATEDETAILS.LIST>
			<SUMMARYALLOCS.LIST></SUMMARYALLOCS.LIST>
			<STPYMTDETAILS.LIST></STPYMTDETAILS.LIST>
			<EXCISEPAYMENTALLOCATIONS.LIST></EXCISEPAYMENTALLOCATIONS.LIST>
			<TAXBILLALLOCATIONS.LIST></TAXBILLALLOCATIONS.LIST>
			<TAXOBJECTALLOCATIONS.LIST></TAXOBJECTALLOCATIONS.LIST>
			<TDSEXPENSEALLOCATIONS.LIST></TDSEXPENSEALLOCATIONS.LIST>
			<VATSTATUTORYDETAILS.LIST></VATSTATUTORYDETAILS.LIST>
			<COSTTRACKALLOCATIONS.LIST></COSTTRACKALLOCATIONS.LIST>
			<REFVOUCHERDETAILS.LIST></REFVOUCHERDETAILS.LIST>
			<INVOICEWISEDETAILS.LIST></INVOICEWISEDETAILS.LIST>
			<VATITCDETAILS.LIST></VATITCDETAILS.LIST>
			<ADVANCETAXDETAILS.LIST></ADVANCETAXDETAILS.LIST>
			</LEDGERENTRIES.LIST>
			<LEDGERENTRIES.LIST>

				<TAXCLASSIFICATIONNAME/>
				<LEDGERNAME>'.$ledgername_report_from.'</LEDGERNAME>
				<METHODTYPE>As User Defined Value</METHODTYPE>
				<VOUCHERFBTCATEGORY/>
				<GSTCLASS/>
				<ISDEEMEDPOSITIVE>No</ISDEEMEDPOSITIVE>
				<LEDGERFROMITEM>No</LEDGERFROMITEM>
				<REMOVEZEROENTRIES>No</REMOVEZEROENTRIES>
				<ISPARTYLEDGER>No</ISPARTYLEDGER>
				<ISLASTDEEMEDPOSITIVE>No</ISLASTDEEMEDPOSITIVE>
				<SERVICETAXDETAILS.LIST></SERVICETAXDETAILS.LIST>
				<BANKALLOCATIONS.LIST></BANKALLOCATIONS.LIST>
				<BILLALLOCATIONS.LIST></BILLALLOCATIONS.LIST>
				<INTERESTCOLLECTION.LIST></INTERESTCOLLECTION.LIST>
				<OLDAUDITENTRIES.LIST></OLDAUDITENTRIES.LIST>
				<ACCOUNTAUDITENTRIES.LIST></ACCOUNTAUDITENTRIES.LIST>
				<AUDITENTRIES.LIST></AUDITENTRIES.LIST>
				<INPUTCRALLOCS.LIST></INPUTCRALLOCS.LIST>
				<INVENTORYALLOCATIONS.LIST></INVENTORYALLOCATIONS.LIST>
				<DUTYHEADDETAILS.LIST></DUTYHEADDETAILS.LIST>
				<EXCISEDUTYHEADDETAILS.LIST></EXCISEDUTYHEADDETAILS.LIST>
				<RATEDETAILS.LIST></RATEDETAILS.LIST>
				<SUMMARYALLOCS.LIST></SUMMARYALLOCS.LIST>
				<STPYMTDETAILS.LIST></STPYMTDETAILS.LIST>
				<EXCISEPAYMENTALLOCATIONS.LIST></EXCISEPAYMENTALLOCATIONS.LIST>
				<TAXBILLALLOCATIONS.LIST></TAXBILLALLOCATIONS.LIST>
				<TAXOBJECTALLOCATIONS.LIST></TAXOBJECTALLOCATIONS.LIST>
				<TDSEXPENSEALLOCATIONS.LIST></TDSEXPENSEALLOCATIONS.LIST>
				<VATSTATUTORYDETAILS.LIST></VATSTATUTORYDETAILS.LIST>
				<REFVOUCHERDETAILS.LIST></REFVOUCHERDETAILS.LIST>
				<INVOICEWISEDETAILS.LIST></INVOICEWISEDETAILS.LIST>
				<VATITCDETAILS.LIST></VATITCDETAILS.LIST>
				<ADVANCETAXDETAILS.LIST></ADVANCETAXDETAILS.LIST>
				</LEDGERENTRIES.LIST>
				<LEDGERENTRIES.LIST>
				<TAXCLASSIFICATIONNAME/>
				<LEDGERNAME></LEDGERNAME>
				<METHODTYPE>As User Defined Value</METHODTYPE>
				<VOUCHERFBTCATEGORY/>
				<GSTCLASS/>
				<ISDEEMEDPOSITIVE>No</ISDEEMEDPOSITIVE>
				<LEDGERFROMITEM>No</LEDGERFROMITEM>
				<REMOVEZEROENTRIES>No</REMOVEZEROENTRIES>
				<ISPARTYLEDGER>No</ISPARTYLEDGER>
				<ISLASTDEEMEDPOSITIVE>No</ISLASTDEEMEDPOSITIVE>
				<SERVICETAXDETAILS.LIST></SERVICETAXDETAILS.LIST>
				<BANKALLOCATIONS.LIST></BANKALLOCATIONS.LIST>
				<BILLALLOCATIONS.LIST></BILLALLOCATIONS.LIST>
				<INTERESTCOLLECTION.LIST></INTERESTCOLLECTION.LIST>
				<OLDAUDITENTRIES.LIST></OLDAUDITENTRIES.LIST>
				<ACCOUNTAUDITENTRIES.LIST></ACCOUNTAUDITENTRIES.LIST>
				<AUDITENTRIES.LIST></AUDITENTRIES.LIST>
				<INPUTCRALLOCS.LIST></INPUTCRALLOCS.LIST>
				<INVENTORYALLOCATIONS.LIST></INVENTORYALLOCATIONS.LIST>
				<DUTYHEADDETAILS.LIST></DUTYHEADDETAILS.LIST>
				<EXCISEDUTYHEADDETAILS.LIST></EXCISEDUTYHEADDETAILS.LIST>
				<RATEDETAILS.LIST></RATEDETAILS.LIST>
				<SUMMARYALLOCS.LIST></SUMMARYALLOCS.LIST>
				<STPYMTDETAILS.LIST></STPYMTDETAILS.LIST>
				<EXCISEPAYMENTALLOCATIONS.LIST></EXCISEPAYMENTALLOCATIONS.LIST>
				<TAXBILLALLOCATIONS.LIST></TAXBILLALLOCATIONS.LIST>
				<TAXOBJECTALLOCATIONS.LIST></TAXOBJECTALLOCATIONS.LIST>
				<TDSEXPENSEALLOCATIONS.LIST></TDSEXPENSEALLOCATIONS.LIST>
				<VATSTATUTORYDETAILS.LIST></VATSTATUTORYDETAILS.LIST>
				<REFVOUCHERDETAILS.LIST></REFVOUCHERDETAILS.LIST>
				<INVOICEWISEDETAILS.LIST></INVOICEWISEDETAILS.LIST>
				<VATITCDETAILS.LIST></VATITCDETAILS.LIST>
				<ADVANCETAXDETAILS.LIST></ADVANCETAXDETAILS.LIST>
				</LEDGERENTRIES.LIST>';

    


						 
							 
	$requestXMLEND='					
<PAYROLLMODEOFPAYMENT.LIST>      </PAYROLLMODEOFPAYMENT.LIST>
      <ATTDRECORDS.LIST>      </ATTDRECORDS.LIST>
     </VOUCHER>
    </TALLYMESSAGE>

   </REQUESTDATA>
  </IMPORTDATA>
 </BODY>
</ENVELOPE>';


           }

           else
           {

           	$wh_name=$warehouse_name;
	 		$Towh_name=$Towarehouse_name;
	 	    $arr = explode(' ',trim($wh_name));
	 	    $arr1 = explode(' ',trim($Towh_name));
	 	    $challan=ucwords(strtolower($arr[1]));
	 	     $challanto=ucwords(strtolower($arr1[1]));
            $voucher_type=ucwords(strtolower($arr[1]))." Tax Invoice GST";
            $voucher_typereport=ucwords(strtolower($arr1[1]))." Receipt Note";
           // $godown_name="CHN ". ucwords(strtolower($arr1[1]))." Sales";
            $ledgername="Eastman Auto &amp; Power limited-".$challan;
            $ledgername_report="Receipt Note ".ucwords(strtolower($arr1[1]));
            $ledgername_report_from=ucwords(strtolower($arr[1]))." Branch Transfer";

            $ledgernameto="Eastman Auto &amp; Power limited-".$challanto;
           // $ledgername=$voucher_type." Stock Transfer Agnt F Form";
            $ledgernamegst=ucwords(strtolower($arr[1]))." GST Sales @ 18%";
            
            //$BASICUSERDESCRIPTION=$auRec[0]->bsn;
            $requestXMLSTART='<ENVELOPE>
			<HEADER>
			<TALLYREQUEST>Import Data</TALLYREQUEST>
			</HEADER>
			<BODY>
			<IMPORTDATA>
			<REQUESTDESC>
			<REPORTNAME>Vouchers</REPORTNAME>
			<STATICVARIABLES>
			<SVCURRENTCOMPANY>EastmanLtd</SVCURRENTCOMPANY>
			</STATICVARIABLES>
			</REQUESTDESC>
			<REQUESTDATA>
			<TALLYMESSAGE xmlns:UDF="TallyUDF">

			<VOUCHER VCHTYPE="'.$voucher_type.'" ACTION="Create" OBJVIEW="Invoice Voucher View">
            <ADDRESS.LIST TYPE="String">
            <ADDRESS>'.$wh_state_id1[0]->warehouse_address.' </ADDRESS>
            <ADDRESS>'.$city_id_towh[0]->city_name.','.$state_name_towh[0]->state_name.'</ADDRESS>
            <ADDRESS>Mobile No:'.$wh_state_id1[0]->warehouse_phone_no.'</ADDRESS>
           
            </ADDRESS.LIST>
			<BASICBUYERADDRESS.LIST TYPE="String">
				<BASICBUYERADDRESS>'.$wh_state_id1[0]->warehouse_address.'</BASICBUYERADDRESS>
			<BASICBUYERADDRESS>'.$city_id_towh[0]->city_name.','.$state_name_towh[0]->state_name.'</BASICBUYERADDRESS>
			<BASICBUYERADDRESS>Mobile No:'.$wh_state_id1[0]->warehouse_phone_no.'</BASICBUYERADDRESS>
			</BASICBUYERADDRESS.LIST>
			<OLDAUDITENTRYIDS.LIST TYPE="Number">
			<OLDAUDITENTRYIDS>-1</OLDAUDITENTRYIDS>
			</OLDAUDITENTRYIDS.LIST>
			<DATE>'.$DATE.'</DATE>
			<REFERENCEDATE>'.$DATE.'</REFERENCEDATE>

			<GSTREGISTRATIONTYPE></GSTREGISTRATIONTYPE>
			<VATDEALERTYPE></VATDEALERTYPE>
			<TAXUNITNAME>Default Tax Unit</TAXUNITNAME>
			<CLASSNAME></CLASSNAME>
			<PARTYNAME>'.$ledgernameto.'</PARTYNAME>
			<VOUCHERTYPENAME>'.$voucher_type.'</VOUCHERTYPENAME>
			<REFERENCE></REFERENCE>
			<VOUCHERNUMBER></VOUCHERNUMBER>
			<PARTYLEDGERNAME>'.$ledgernameto.'</PARTYLEDGERNAME>
			<BASICBASEPARTYNAME>'.$ledgernameto.'</BASICBASEPARTYNAME>
			<CSTFORMISSUETYPE/>
			<CSTFORMRECVTYPE/>
			<FBTPAYMENTTYPE>Default</FBTPAYMENTTYPE>
			<PERSISTEDVIEW>Invoice Voucher View</PERSISTEDVIEW>
			<BASICBUYERNAME>'.$ledgernameto.'</BASICBUYERNAME>
			<BASICFINALDESTINATION>'.$city_id_towh[0]->city_name.'</BASICFINALDESTINATION>
			<BASICDATETIMEOFINVOICE>3-Aug-2017 at 15:58</BASICDATETIMEOFINVOICE>
			<BASICDATETIMEOFREMOVAL>3-Aug-2017 at 15:58</BASICDATETIMEOFREMOVAL>
			<VCHGSTCLASS/>
			<ENTEREDBY></ENTEREDBY>
			<DIFFACTUALQTY>No</DIFFACTUALQTY>
			<ISMSTFROMSYNC>No</ISMSTFROMSYNC>
			<ASORIGINAL>No</ASORIGINAL>
			<AUDITED>No</AUDITED>
			<FORJOBCOSTING>No</FORJOBCOSTING>
			<ISOPTIONAL>No</ISOPTIONAL>
			<EFFECTIVEDATE>20170801</EFFECTIVEDATE>
			<USEFOREXCISE>No</USEFOREXCISE>
			<ISFORJOBWORKIN>No</ISFORJOBWORKIN>
			<ALLOWCONSUMPTION>No</ALLOWCONSUMPTION>
			<USEFORINTEREST>No</USEFORINTEREST>
			<USEFORGAINLOSS>No</USEFORGAINLOSS>
			<USEFORGODOWNTRANSFER>No</USEFORGODOWNTRANSFER>
			<USEFORCOMPOUND>No</USEFORCOMPOUND>
			<USEFORSERVICETAX>No</USEFORSERVICETAX>
			<ISEXCISEVOUCHER>No</ISEXCISEVOUCHER>
			<EXCISETAXOVERRIDE>No</EXCISETAXOVERRIDE>
			<USEFORTAXUNITTRANSFER>No</USEFORTAXUNITTRANSFER>
			<EXCISEOPENING>No</EXCISEOPENING>
			<USEFORFINALPRODUCTION>No</USEFORFINALPRODUCTION>
			<ISTDSOVERRIDDEN>No</ISTDSOVERRIDDEN>
			<ISTCSOVERRIDDEN>No</ISTCSOVERRIDDEN>
			<ISTDSTCSCASHVCH>No</ISTDSTCSCASHVCH>
			<INCLUDEADVPYMTVCH>No</INCLUDEADVPYMTVCH>
			<ISSUBWORKSCONTRACT>No</ISSUBWORKSCONTRACT>
			<ISVATOVERRIDDEN>No</ISVATOVERRIDDEN>
			<IGNOREORIGVCHDATE>No</IGNOREORIGVCHDATE>
			<ISSERVICETAXOVERRIDDEN>No</ISSERVICETAXOVERRIDDEN>
			<ISISDVOUCHER>No</ISISDVOUCHER>
			<ISEXCISEOVERRIDDEN>No</ISEXCISEOVERRIDDEN>
			<ISEXCISESUPPLYVCH>No</ISEXCISESUPPLYVCH>
			<ISGSTOVERRIDDEN>No</ISGSTOVERRIDDEN>
			<GSTNOTEXPORTED>No</GSTNOTEXPORTED>
			<ISVATPRINCIPALACCOUNT>No</ISVATPRINCIPALACCOUNT>
			<ISSHIPPINGWITHINSTATE>No</ISSHIPPINGWITHINSTATE>
			<ISCANCELLED>No</ISCANCELLED>
			<HASCASHFLOW>Yes</HASCASHFLOW>
			<ISPOSTDATED>No</ISPOSTDATED>
			<USETRACKINGNUMBER>No</USETRACKINGNUMBER>
			<ISINVOICE>Yes</ISINVOICE>
			<MFGJOURNAL>No</MFGJOURNAL>
			<HASDISCOUNTS>Yes</HASDISCOUNTS>
			<ASPAYSLIP>No</ASPAYSLIP>
			<ISCOSTCENTRE>No</ISCOSTCENTRE>
			<ISSTXNONREALIZEDVCH>No</ISSTXNONREALIZEDVCH>
			<ISEXCISEMANUFACTURERON>No</ISEXCISEMANUFACTURERON>
			<ISBLANKCHEQUE>No</ISBLANKCHEQUE>
			<ISVOID>No</ISVOID>
			<ISONHOLD>No</ISONHOLD>
			<ORDERLINESTATUS>No</ORDERLINESTATUS>
			<VATISAGNSTCANCSALES>No</VATISAGNSTCANCSALES>
			<VATISPURCEXEMPTED>No</VATISPURCEXEMPTED>
			<ISVATRESTAXINVOICE>No</ISVATRESTAXINVOICE>
			<VATISASSESABLECALCVCH>No</VATISASSESABLECALCVCH>
			<ISVATDUTYPAID>Yes</ISVATDUTYPAID>
			<ISDELIVERYSAMEASCONSIGNEE>No</ISDELIVERYSAMEASCONSIGNEE>
			<ISDISPATCHSAMEASCONSIGNOR>No</ISDISPATCHSAMEASCONSIGNOR>
			<ISDELETED>No</ISDELETED>
			<CHANGEVCHMODE>No</CHANGEVCHMODE>
			<ALTERID></ALTERID>
			<MASTERID></MASTERID>
			<VOUCHERKEY></VOUCHERKEY>
			<EXCLUDEDTAXATIONS.LIST></EXCLUDEDTAXATIONS.LIST>
			<OLDAUDITENTRIES.LIST></OLDAUDITENTRIES.LIST>
			<ACCOUNTAUDITENTRIES.LIST></ACCOUNTAUDITENTRIES.LIST>
			<AUDITENTRIES.LIST></AUDITENTRIES.LIST>
			<DUTYHEADDETAILS.LIST></DUTYHEADDETAILS.LIST>
			<SUPPLEMENTARYDUTYHEADDETAILS.LIST></SUPPLEMENTARYDUTYHEADDETAILS.LIST>
			<INVOICEDELNOTES.LIST></INVOICEDELNOTES.LIST>
			<INVOICEORDERLIST.LIST></INVOICEORDERLIST.LIST>
			<INVOICEINDENTLIST.LIST></INVOICEINDENTLIST.LIST>
			<ATTENDANCEENTRIES.LIST></ATTENDANCEENTRIES.LIST>
			<ORIGINVOICEDETAILS.LIST></ORIGINVOICEDETAILS.LIST>
			<INVOICEEXPORTLIST.LIST></INVOICEEXPORTLIST.LIST>
			<LEDGERENTRIES.LIST>
			<OLDAUDITENTRYIDS.LIST TYPE="Number">
			<OLDAUDITENTRYIDS>-1</OLDAUDITENTRYIDS>
			</OLDAUDITENTRYIDS.LIST>
			<LEDGERNAME>'.$ledgername.'</LEDGERNAME>
			<GSTCLASS/>
			<ISDEEMEDPOSITIVE>No</ISDEEMEDPOSITIVE>
			<LEDGERFROMITEM>No</LEDGERFROMITEM>
			<REMOVEZEROENTRIES>No</REMOVEZEROENTRIES>
			<ISPARTYLEDGER>Yes</ISPARTYLEDGER>
			<ISLASTDEEMEDPOSITIVE>No</ISLASTDEEMEDPOSITIVE>
			<AMOUNT></AMOUNT>
			<SERVICETAXDETAILS.LIST></SERVICETAXDETAILS.LIST>

			<BILLALLOCATIONS.LIST></BILLALLOCATIONS.LIST>
			<INTERESTCOLLECTION.LIST></INTERESTCOLLECTION.LIST>
			<OLDAUDITENTRIES.LIST></OLDAUDITENTRIES.LIST>
			<ACCOUNTAUDITENTRIES.LIST></ACCOUNTAUDITENTRIES.LIST>
			<AUDITENTRIES.LIST></AUDITENTRIES.LIST>
			<INPUTCRALLOCS.LIST></INPUTCRALLOCS.LIST>
			<DUTYHEADDETAILS.LIST></DUTYHEADDETAILS.LIST>
			<EXCISEDUTYHEADDETAILS.LIST></EXCISEDUTYHEADDETAILS.LIST>
			<RATEDETAILS.LIST></RATEDETAILS.LIST>
			<SUMMARYALLOCS.LIST></SUMMARYALLOCS.LIST>
			<STPYMTDETAILS.LIST></STPYMTDETAILS.LIST>
			<EXCISEPAYMENTALLOCATIONS.LIST></EXCISEPAYMENTALLOCATIONS.LIST>
			<TAXBILLALLOCATIONS.LIST></TAXBILLALLOCATIONS.LIST>
			<TAXOBJECTALLOCATIONS.LIST></TAXOBJECTALLOCATIONS.LIST>
			<TDSEXPENSEALLOCATIONS.LIST></TDSEXPENSEALLOCATIONS.LIST>
			<VATSTATUTORYDETAILS.LIST></VATSTATUTORYDETAILS.LIST>
			<COSTTRACKALLOCATIONS.LIST></COSTTRACKALLOCATIONS.LIST>
			<REFVOUCHERDETAILS.LIST></REFVOUCHERDETAILS.LIST>
			<INVOICEWISEDETAILS.LIST></INVOICEWISEDETAILS.LIST>
			<VATITCDETAILS.LIST></VATITCDETAILS.LIST>
			<ADVANCETAXDETAILS.LIST></ADVANCETAXDETAILS.LIST>
			</LEDGERENTRIES.LIST>
			<LEDGERENTRIES.LIST>

				<TAXCLASSIFICATIONNAME/>
				<LEDGERNAME>'.$ledgername.'</LEDGERNAME>
				<METHODTYPE>As User Defined Value</METHODTYPE>
				<VOUCHERFBTCATEGORY/>
				<GSTCLASS/>
				<ISDEEMEDPOSITIVE>No</ISDEEMEDPOSITIVE>
				<LEDGERFROMITEM>No</LEDGERFROMITEM>
				<REMOVEZEROENTRIES>No</REMOVEZEROENTRIES>
				<ISPARTYLEDGER>No</ISPARTYLEDGER>
				<ISLASTDEEMEDPOSITIVE>No</ISLASTDEEMEDPOSITIVE>
				<SERVICETAXDETAILS.LIST></SERVICETAXDETAILS.LIST>
				<BANKALLOCATIONS.LIST></BANKALLOCATIONS.LIST>
				<BILLALLOCATIONS.LIST></BILLALLOCATIONS.LIST>
				<INTERESTCOLLECTION.LIST></INTERESTCOLLECTION.LIST>
				<OLDAUDITENTRIES.LIST></OLDAUDITENTRIES.LIST>
				<ACCOUNTAUDITENTRIES.LIST></ACCOUNTAUDITENTRIES.LIST>
				<AUDITENTRIES.LIST></AUDITENTRIES.LIST>
				<INPUTCRALLOCS.LIST></INPUTCRALLOCS.LIST>
				<INVENTORYALLOCATIONS.LIST></INVENTORYALLOCATIONS.LIST>
				<DUTYHEADDETAILS.LIST></DUTYHEADDETAILS.LIST>
				<EXCISEDUTYHEADDETAILS.LIST></EXCISEDUTYHEADDETAILS.LIST>
				<RATEDETAILS.LIST></RATEDETAILS.LIST>
				<SUMMARYALLOCS.LIST></SUMMARYALLOCS.LIST>
				<STPYMTDETAILS.LIST></STPYMTDETAILS.LIST>
				<EXCISEPAYMENTALLOCATIONS.LIST></EXCISEPAYMENTALLOCATIONS.LIST>
				<TAXBILLALLOCATIONS.LIST></TAXBILLALLOCATIONS.LIST>
				<TAXOBJECTALLOCATIONS.LIST></TAXOBJECTALLOCATIONS.LIST>
				<TDSEXPENSEALLOCATIONS.LIST></TDSEXPENSEALLOCATIONS.LIST>
				<VATSTATUTORYDETAILS.LIST></VATSTATUTORYDETAILS.LIST>
				<REFVOUCHERDETAILS.LIST></REFVOUCHERDETAILS.LIST>
				<INVOICEWISEDETAILS.LIST></INVOICEWISEDETAILS.LIST>
				<VATITCDETAILS.LIST></VATITCDETAILS.LIST>
				<ADVANCETAXDETAILS.LIST></ADVANCETAXDETAILS.LIST>
				</LEDGERENTRIES.LIST>
				<LEDGERENTRIES.LIST>
				<TAXCLASSIFICATIONNAME/>
				<LEDGERNAME></LEDGERNAME>
				<METHODTYPE>As User Defined Value</METHODTYPE>
				<VOUCHERFBTCATEGORY/>
				<GSTCLASS/>
				<ISDEEMEDPOSITIVE>No</ISDEEMEDPOSITIVE>
				<LEDGERFROMITEM>No</LEDGERFROMITEM>
				<REMOVEZEROENTRIES>No</REMOVEZEROENTRIES>
				<ISPARTYLEDGER>No</ISPARTYLEDGER>
				<ISLASTDEEMEDPOSITIVE>No</ISLASTDEEMEDPOSITIVE>
				<SERVICETAXDETAILS.LIST></SERVICETAXDETAILS.LIST>
				<BANKALLOCATIONS.LIST></BANKALLOCATIONS.LIST>
				<BILLALLOCATIONS.LIST></BILLALLOCATIONS.LIST>
				<INTERESTCOLLECTION.LIST></INTERESTCOLLECTION.LIST>
				<OLDAUDITENTRIES.LIST></OLDAUDITENTRIES.LIST>
				<ACCOUNTAUDITENTRIES.LIST></ACCOUNTAUDITENTRIES.LIST>
				<AUDITENTRIES.LIST></AUDITENTRIES.LIST>
				<INPUTCRALLOCS.LIST></INPUTCRALLOCS.LIST>
				<INVENTORYALLOCATIONS.LIST></INVENTORYALLOCATIONS.LIST>
				<DUTYHEADDETAILS.LIST></DUTYHEADDETAILS.LIST>
				<EXCISEDUTYHEADDETAILS.LIST></EXCISEDUTYHEADDETAILS.LIST>
				<RATEDETAILS.LIST></RATEDETAILS.LIST>
				<SUMMARYALLOCS.LIST></SUMMARYALLOCS.LIST>
				<STPYMTDETAILS.LIST></STPYMTDETAILS.LIST>
				<EXCISEPAYMENTALLOCATIONS.LIST></EXCISEPAYMENTALLOCATIONS.LIST>
				<TAXBILLALLOCATIONS.LIST></TAXBILLALLOCATIONS.LIST>
				<TAXOBJECTALLOCATIONS.LIST></TAXOBJECTALLOCATIONS.LIST>
				<TDSEXPENSEALLOCATIONS.LIST></TDSEXPENSEALLOCATIONS.LIST>
				<VATSTATUTORYDETAILS.LIST></VATSTATUTORYDETAILS.LIST>
				<REFVOUCHERDETAILS.LIST></REFVOUCHERDETAILS.LIST>
				<INVOICEWISEDETAILS.LIST></INVOICEWISEDETAILS.LIST>
				<VATITCDETAILS.LIST></VATITCDETAILS.LIST>
				<ADVANCETAXDETAILS.LIST></ADVANCETAXDETAILS.LIST>
				</LEDGERENTRIES.LIST>';


							 
	$requestXMLEND='					
<PAYROLLMODEOFPAYMENT.LIST>      </PAYROLLMODEOFPAYMENT.LIST>
      <ATTDRECORDS.LIST>      </ATTDRECORDS.LIST>
     </VOUCHER>
    </TALLYMESSAGE>

   </REQUESTDATA>
  </IMPORTDATA>
 </BODY>
</ENVELOPE>';


           }






			if(isset($paramSet['tranfer_stock_value']) && sizeof($paramSet['tranfer_stock_value'])>0 && isset($paramSet['itemId']) && sizeof($paramSet['itemId'])>0) {


				$tranferStockArray  = array_filter($paramSet['tranfer_stock_value']);
				$itemListArray      = $paramSet['itemId'];
				$availableStkArray  = $paramSet['from_wh_stk_val'];
				$ALLINVENTORYENTRIESITEM=array();
				$ALLINVENTORYENTRIESITEMREPORT=array();

//Tally code commeted by arvind 

		      foreach($tranferStockArray as $key => $value)
{
                     $mykey = $key;
                     $ACTUALQTY = $value;
                    
                    
 if(isset($itemListArray[$mykey]))
 {
 	//echo "<pre>";
 	$item_id=$itemListArray[$mykey];
//print_R($item_id);

                             // $item_name=$this->_getSelectList2('table_item AS i',"item_name,item_division",''," i.item_id='".$item_id."'  ");

                               $item_name=$this->_getSelectList2('table_item AS I left join table_price AS P ON P.item_id=I.item_id',"item_name,item_division,item_dp",''," I.item_id='".$item_id."'  ");
                                $price=$item_name[0]->item_dp;

				        	$STOCKITEM=$item_name[0]->item_name;
				        	if($item_name[0]->item_division==3)
				        	{
                                $godown_name="CHN ". ucwords(strtolower($arr[1]))." Sales";
                               // $godown_namereport="CHN ". ucwords(strtolower($arr1[1]))." Sales";
                                  //print_R($godown_name);exit;

				        	}else if($item_name[0]->item_division==1)
				        	{
				        		 $godown_name="AMPS ". ucwords(strtolower($arr[1]))." Sales";
				        		 //$godown_namereport="AMPS ". ucwords(strtolower($arr1[1]))." Sales";

				        	}else if($item_name[0]->item_division==2)
				        	{
				        		 $godown_name="Inst ". ucwords(strtolower($arr[1]))." Sales";
				        		// $godown_namereport="Inst ". ucwords(strtolower($arr1[1]))." Sales";
				        	}

				        	 $mamount=$price * $markuprate/100;
	                
				                $RATE=$mamount + $price;
				                $amount=$ACTUALQTY * $RATE ;
				                $totalamount= "-".$amount;
                                 
				     

                     $ALLINVENTORYENTRIESITEM[] ='
													<ALLINVENTORYENTRIES.LIST>
													<STOCKITEMNAME>'.$STOCKITEM.'</STOCKITEMNAME>
													<ISDEEMEDPOSITIVE>Yes</ISDEEMEDPOSITIVE>
													<ISLASTDEEMEDPOSITIVE>Yes</ISLASTDEEMEDPOSITIVE>
													<ISAUTONEGATE>No</ISAUTONEGATE>
													<ISCUSTOMSCLEARANCE>No</ISCUSTOMSCLEARANCE>
													<ISTRACKCOMPONENT>No</ISTRACKCOMPONENT>
													<ISTRACKPRODUCTION>No</ISTRACKPRODUCTION>
													<ISPRIMARYITEM>No</ISPRIMARYITEM>
													<ISSCRAP>No</ISSCRAP>
													<RATE>'.$RATE.'</RATE>
													<AMOUNT>'.$totalamount.'</AMOUNT>
													<ACTUALQTY>'.$ACTUALQTY.' nos</ACTUALQTY>
													<BILLEDQTY>'.$ACTUALQTY.' nos</BILLEDQTY>
													<INCLVATRATE></INCLVATRATE>
													<BATCHALLOCATIONS.LIST>
													<GODOWNNAME>'.$godown_name.'</GODOWNNAME>
													<BATCHNAME>Primary Batch</BATCHNAME>
													<DESTINATIONGODOWNNAME>'.$godown_name.'</DESTINATIONGODOWNNAME>
													<INDENTNO/>
													<ORDERNO/>
													<TRACKINGNUMBER/>
													<DYNAMICCSTISCLEARED>No</DYNAMICCSTISCLEARED>
													<AMOUNT>'.$totalamount.'</AMOUNT>
													<ACTUALQTY>'.$ACTUALQTY.' nos</ACTUALQTY>
													<BILLEDQTY>'.$ACTUALQTY.' nos</BILLEDQTY>
													<INCLVATRATE></INCLVATRATE>
													<ADDITIONALDETAILS.LIST></ADDITIONALDETAILS.LIST>
													<VOUCHERCOMPONENTLIST.LIST></VOUCHERCOMPONENTLIST.LIST>
													</BATCHALLOCATIONS.LIST>
													<ACCOUNTINGALLOCATIONS.LIST>
													<OLDAUDITENTRYIDS.LIST TYPE="Number">
													<OLDAUDITENTRYIDS>-1</OLDAUDITENTRYIDS>
													</OLDAUDITENTRYIDS.LIST>
													<LEDGERNAME>'.$ledgername_report_from.'</LEDGERNAME>
													<GSTCLASS/>
													<ISDEEMEDPOSITIVE>Yes</ISDEEMEDPOSITIVE>
													<LEDGERFROMITEM>No</LEDGERFROMITEM>
													<REMOVEZEROENTRIES>No</REMOVEZEROENTRIES>
													<ISPARTYLEDGER>No</ISPARTYLEDGER>
													<ISLASTDEEMEDPOSITIVE>Yes</ISLASTDEEMEDPOSITIVE>
													<AMOUNT></AMOUNT>
													<SERVICETAXDETAILS.LIST></SERVICETAXDETAILS.LIST>
													<BANKALLOCATIONS.LIST></BANKALLOCATIONS.LIST>
													<BILLALLOCATIONS.LIST></BILLALLOCATIONS.LIST>
													<INTERESTCOLLECTION.LIST></INTERESTCOLLECTION.LIST>
													<OLDAUDITENTRIES.LIST></OLDAUDITENTRIES.LIST>
													<ACCOUNTAUDITENTRIES.LIST></ACCOUNTAUDITENTRIES.LIST>
													<AUDITENTRIES.LIST></AUDITENTRIES.LIST>
													<INPUTCRALLOCS.LIST></INPUTCRALLOCS.LIST>
													<DUTYHEADDETAILS.LIST></DUTYHEADDETAILS.LIST>
													<EXCISEDUTYHEADDETAILS.LIST></EXCISEDUTYHEADDETAILS.LIST>
													<RATEDETAILS.LIST></RATEDETAILS.LIST>
													<SUMMARYALLOCS.LIST></SUMMARYALLOCS.LIST>
													<STPYMTDETAILS.LIST></STPYMTDETAILS.LIST>
													<EXCISEPAYMENTALLOCATIONS.LIST></EXCISEPAYMENTALLOCATIONS.LIST>
													<TAXBILLALLOCATIONS.LIST></TAXBILLALLOCATIONS.LIST>
													<TAXOBJECTALLOCATIONS.LIST></TAXOBJECTALLOCATIONS.LIST>
													<TDSEXPENSEALLOCATIONS.LIST></TDSEXPENSEALLOCATIONS.LIST>
													<VATSTATUTORYDETAILS.LIST></VATSTATUTORYDETAILS.LIST>
													<COSTTRACKALLOCATIONS.LIST></COSTTRACKALLOCATIONS.LIST>
													<REFVOUCHERDETAILS.LIST></REFVOUCHERDETAILS.LIST>
													<INVOICEWISEDETAILS.LIST></INVOICEWISEDETAILS.LIST>
													<VATITCDETAILS.LIST></VATITCDETAILS.LIST>
													<ADVANCETAXDETAILS.LIST></ADVANCETAXDETAILS.LIST>
													</ACCOUNTINGALLOCATIONS.LIST>
													<DUTYHEADDETAILS.LIST></DUTYHEADDETAILS.LIST>
													<SUPPLEMENTARYDUTYHEADDETAILS.LIST></SUPPLEMENTARYDUTYHEADDETAILS.LIST>
													<TAXOBJECTALLOCATIONS.LIST></TAXOBJECTALLOCATIONS.LIST>
													<REFVOUCHERDETAILS.LIST></REFVOUCHERDETAILS.LIST>
													<EXCISEALLOCATIONS.LIST></EXCISEALLOCATIONS.LIST>
													<EXPENSEALLOCATIONS.LIST></EXPENSEALLOCATIONS.LIST>
													</ALLINVENTORYENTRIES.LIST>';


/*
 $ALLINVENTORYENTRIESITEMREPORT[] ='
													<ALLINVENTORYENTRIES.LIST>
													<STOCKITEMNAME>'.$STOCKITEM.'</STOCKITEMNAME>
													<ISDEEMEDPOSITIVE>Yes</ISDEEMEDPOSITIVE>
													<ISLASTDEEMEDPOSITIVE>Yes</ISLASTDEEMEDPOSITIVE>
													<ISAUTONEGATE>No</ISAUTONEGATE>
													<ISCUSTOMSCLEARANCE>No</ISCUSTOMSCLEARANCE>
													<ISTRACKCOMPONENT>No</ISTRACKCOMPONENT>
													<ISTRACKPRODUCTION>No</ISTRACKPRODUCTION>
													<ISPRIMARYITEM>No</ISPRIMARYITEM>
													<ISSCRAP>No</ISSCRAP>
													<RATE></RATE>
													<AMOUNT></AMOUNT>
													<ACTUALQTY>'.$ACTUALQTY.' nos</ACTUALQTY>
													<BILLEDQTY>'.$ACTUALQTY.' nos</BILLEDQTY>
													<INCLVATRATE></INCLVATRATE>
													<BATCHALLOCATIONS.LIST>
													<GODOWNNAME>'.$godown_namereport.'</GODOWNNAME>
													<BATCHNAME>Primary Batch</BATCHNAME>
													<DESTINATIONGODOWNNAME>'.$godown_namereport.'</DESTINATIONGODOWNNAME>
													<INDENTNO/>
													<ORDERNO/>
													<TRACKINGNUMBER/>
													<DYNAMICCSTISCLEARED>No</DYNAMICCSTISCLEARED>
													<AMOUNT></AMOUNT>
													<ACTUALQTY>'.$ACTUALQTY.' nos</ACTUALQTY>
													<BILLEDQTY>'.$ACTUALQTY.' nos</BILLEDQTY>
													<INCLVATRATE></INCLVATRATE>
													<ADDITIONALDETAILS.LIST></ADDITIONALDETAILS.LIST>
													<VOUCHERCOMPONENTLIST.LIST></VOUCHERCOMPONENTLIST.LIST>
													</BATCHALLOCATIONS.LIST>
													<ACCOUNTINGALLOCATIONS.LIST>
													<OLDAUDITENTRYIDS.LIST TYPE="Number">
													<OLDAUDITENTRYIDS>-1</OLDAUDITENTRYIDS>
													</OLDAUDITENTRYIDS.LIST>
													<LEDGERNAME>'.$ledgername_report.'</LEDGERNAME>
													<GSTCLASS/>
													<ISDEEMEDPOSITIVE>Yes</ISDEEMEDPOSITIVE>
													<LEDGERFROMITEM>No</LEDGERFROMITEM>
													<REMOVEZEROENTRIES>No</REMOVEZEROENTRIES>
													<ISPARTYLEDGER>No</ISPARTYLEDGER>
													<ISLASTDEEMEDPOSITIVE>Yes</ISLASTDEEMEDPOSITIVE>
													<AMOUNT></AMOUNT>
													<SERVICETAXDETAILS.LIST></SERVICETAXDETAILS.LIST>
													<BANKALLOCATIONS.LIST></BANKALLOCATIONS.LIST>
													<BILLALLOCATIONS.LIST></BILLALLOCATIONS.LIST>
													<INTERESTCOLLECTION.LIST></INTERESTCOLLECTION.LIST>
													<OLDAUDITENTRIES.LIST></OLDAUDITENTRIES.LIST>
													<ACCOUNTAUDITENTRIES.LIST></ACCOUNTAUDITENTRIES.LIST>
													<AUDITENTRIES.LIST></AUDITENTRIES.LIST>
													<INPUTCRALLOCS.LIST></INPUTCRALLOCS.LIST>
													<DUTYHEADDETAILS.LIST></DUTYHEADDETAILS.LIST>
													<EXCISEDUTYHEADDETAILS.LIST></EXCISEDUTYHEADDETAILS.LIST>
													<RATEDETAILS.LIST></RATEDETAILS.LIST>
													<SUMMARYALLOCS.LIST></SUMMARYALLOCS.LIST>
													<STPYMTDETAILS.LIST></STPYMTDETAILS.LIST>
													<EXCISEPAYMENTALLOCATIONS.LIST></EXCISEPAYMENTALLOCATIONS.LIST>
													<TAXBILLALLOCATIONS.LIST></TAXBILLALLOCATIONS.LIST>
													<TAXOBJECTALLOCATIONS.LIST></TAXOBJECTALLOCATIONS.LIST>
													<TDSEXPENSEALLOCATIONS.LIST></TDSEXPENSEALLOCATIONS.LIST>
													<VATSTATUTORYDETAILS.LIST></VATSTATUTORYDETAILS.LIST>
													<COSTTRACKALLOCATIONS.LIST></COSTTRACKALLOCATIONS.LIST>
													<REFVOUCHERDETAILS.LIST></REFVOUCHERDETAILS.LIST>
													<INVOICEWISEDETAILS.LIST></INVOICEWISEDETAILS.LIST>
													<VATITCDETAILS.LIST></VATITCDETAILS.LIST>
													<ADVANCETAXDETAILS.LIST></ADVANCETAXDETAILS.LIST>
													</ACCOUNTINGALLOCATIONS.LIST>
													<DUTYHEADDETAILS.LIST></DUTYHEADDETAILS.LIST>
													<SUPPLEMENTARYDUTYHEADDETAILS.LIST></SUPPLEMENTARYDUTYHEADDETAILS.LIST>
													<TAXOBJECTALLOCATIONS.LIST></TAXOBJECTALLOCATIONS.LIST>
													<REFVOUCHERDETAILS.LIST></REFVOUCHERDETAILS.LIST>
													<EXCISEALLOCATIONS.LIST></EXCISEALLOCATIONS.LIST>
													<EXPENSEALLOCATIONS.LIST></EXPENSEALLOCATIONS.LIST>
													</ALLINVENTORYENTRIES.LIST>';

*/




}
}         //$server = 'http://quytech14.ddns.net:8000/';

            $requestXMLITEMLIST = implode(' ', $ALLINVENTORYENTRIESITEM);
			//$requestXMLITEMLISTREPORT = implode(' ', $ALLINVENTORYENTRIESITEMREPORT);
			       
		    $requestXML = $requestXMLSTART.$requestXMLITEMLIST.$requestXMLEND;
		    //$requestXMLREPORT = $requestXMLSTARTReport.$requestXMLITEMLISTREPORT.$requestXMLEND;
		    $first=array();
			//$second=array();

//curl run for xml code commeted by arvind
            $header = array( "Content-type: text/xml" ,"Content-length: ".strlen($requestXML) ,"Connection: close" );
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, TALLYIP);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_TIMEOUT, 100);
			curl_setopt($ch, CURLOPT_POST, true);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $requestXML);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
			$data = curl_exec($ch);
			//print_R($data);
           if(curl_errno($ch)){
		    echo json_encode(array('status'=>'Failed', 'data'=> print curl_error($ch)));

		    }else{

		    	   $p = xml_parser_create();
        xml_parse_into_struct($p, $data, $vals, $index);
       xml_parser_free($p);
		   if(isset($vals[$index['LASTVCHID'][0]]['value']) && $vals[$index['LASTVCHID'][0]]['value']>0) {

		    $tallymasterno=$vals[$index['LASTVCHID'][0]]['value'];
        //$attributeArray = array();
      //  $attributeArray['TALLYMASTERNO'] = $vals[$index['LASTVCHID'][0]]['value'];
        
        //$yu=$this->_dbUpdate2($attributeArray,'table_warehouse_stk_trn_dts', "transfer_id =".$transfer_id );  
//print_R($yu);exit;

       $first=array('status'=>'Pushed To Tally Successfully', 'data'=> " Pushed To Tally Successfully.");


            //echo json_encode(array('status'=>'Push To Tally Successfully', 'data'=> " Push To Tally Successfully.", 'id'=>$vals[$index['LASTVCHID'][0]]['value']));

    } else {
      
      if(isset($vals[$index['LINEERROR'][0]]['value'])) 
        $message = $vals[$index['LINEERROR'][0]]['value'];
      else 
        $message = $vals[0]['value'];


          $first = array('status'=>'Failed', 'data'=> $message);
    }


    curl_close($ch);

      

               } 
               /*
//$server = 'http://quytech14.ddns.net:8000/';
            $headers = array( "Content-type: text/xml" ,"Content-length: ".strlen($requestXMLREPORT) ,"Connection: close" );
			$ch1 = curl_init();
			curl_setopt($ch1, CURLOPT_URL, TALLYIP);
			curl_setopt($ch1, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch1, CURLOPT_TIMEOUT, 100);
			curl_setopt($ch1, CURLOPT_POST, true);
			curl_setopt($ch1, CURLOPT_POSTFIELDS, $requestXMLREPORT);
			curl_setopt($ch1, CURLOPT_HTTPHEADER, $headers);
			$data1 = curl_exec($ch1);
			//print_R($data1);
			 if(curl_errno($ch1)){
		    echo json_encode(array('status'=>'Failed', 'data'=> print curl_error($ch1)));

		    }else{

		    	   $p1 = xml_parser_create();
        xml_parse_into_struct($p1, $data1, $vals1, $index1);
       xml_parser_free($p1);
		   if(isset($vals1[$index1['LASTVCHID'][0]]['value']) && $vals1[$index1['LASTVCHID'][0]]['value']>0) {

		  $tallymasternoreport=$vals1[$index1['LASTVCHID'][0]]['value'];
        //$attributeArray = array();
       // $attributeArray['TALLYMASTERNOREPORT'] = $vals1[$index1['LASTVCHID'][0]]['value'];
        
        //$yu=$this->_dbUpdate2($attributeArray,'table_warehouse_stk_trn_dts', "transfer_id =".$transfer_id );  
//print_R($yu);exit;

     $second=array('status'=>'Push To Tally Successfully', 'data'=> " Push To Tally Successfully.");
          //  echo json_encode(array('status'=>'Push To Tally Successfully', 'data'=> " Push To Tally Successfully.", 'id'=>$vals1[$index1['LASTVCHID'][0]]['value']));
         // echo json_encode(array('status'=>'Push To Tally Successfully'));

    } else {
      
      if(isset($vals1[$index1['LINEERROR'][0]]['value'])) 
        $message = $vals1[$index1['LINEERROR'][0]]['value'];
      else 
        $message = $vals1[0]['value'];


          $second=array('status'=>'Failed', 'data'=> $message);
    }


    curl_close($ch1);

      

               } */

             $tallyretun=   json_encode(array("first"=>$first['data']));
              
             //print_R($tallyretun);
             $jsonAsObject   = json_decode($tallyretun);
             $jsonAsArray    = json_decode($tallyretun, TRUE);
             $fromwh="From Warehouse:".$jsonAsObject->first;
             $towh="To Warehouse:".$jsonAsObject->second;
            // $message=$fromwh.$towh;
             $message=$jsonAsObject->first;








				$transferBSNArray = array();


				// Get for duplicacy 

				$whStkTrnsDetail = $this->_getSelectList('table_warehouse_stk_trns AS WHST',"*",''," WHST.bill_no='".$bill_no."' AND WHST.bill_date= '".$bill_date."' ");
					if(isset($whStkTrnsDetail[0]->transfer_id) && $whStkTrnsDetail[0]->transfer_id>0) {
						return array('status'=>'failed', 'msg'=>'Duplicate Dispatched No/Date!');

					} else {

						foreach ($tranferStockArray as $key => $value) {
							# code..

							$transferStk = $value;
							$actualStk   = $availableStkArray[$key];

							if(isset($itemListArray[$key]) && !empty($itemListArray[$key])) {

								$item_id = trim($itemListArray[$key]);
								// Firslty Check warehouse stock itemwise
								

								 $whSBNDetail = $this->_getSelectList('table_warehouse_stock AS WS LEFT JOIN table_item_bsn AS BSN ON BSN.bsn_id = WS.bsn_id ',"WS.*, BSN.bsn ",''," WS.warehouse_id ='".$warehouse_id."' AND WS.item_id = '".$item_id."' AND WS.status = 'A' AND datediff(NOW(),WS.dispatch_date) < 90  LIMIT 0, $value ");

								// echo "<pre>";
								// print_r($whSBNDetail);
							
								 foreach ($whSBNDetail as $key => $value) {
								 	# code...
								 	//echo $key;
									$transferBSNArray[$value->item_id][] = array( 'warehouse_stock_id'=>$value->warehouse_stock_id, 'warehouse_id'=>$value->warehouse_id,  'item_id'=>$value->item_id,'bsn_id'=>$value->bsn_id, 'bsn'=>$value->bsn, 'actualStk'=>$actualStk, 'transferStk'=>$transferStk);
									//print_r($transferBSNArray);
								 }
								
								///exit;
							}

						}





								// echo "<pre>";
								// print_r($transferBSNArray);
								// exit;

						// Created transfer bsn arraylist

						if(sizeof($transferBSNArray)>0) {


							// table_warehouse_stk_trns

							$data = array();

							$data['account_id'] 			=  $_SESSION['accountId'];	
							$data['from_warehouse_id'] 		=  $warehouse_id;
							$data['to_warehouse_id'] 		=  $to_warehouse_id;
							$data['bill_date'] 				=  $bill_date;
							$data['bill_no'] 				=  $bill_no;
							$data['user_type'] 		        =  $_SESSION['userLoginType'];	
							$data['web_user_id'] 		    =  $_SESSION['PepUpSalesUserId'];	
							$data['transfer_datetime'] 		=  date('Y-m-d H:i:s');	
							$data['created_date'] 		    =  date('Y-m-d');	
							$data['last_update_date'] 		=  date('Y-m-d');	
							$data['status']		            =  'A';

							$transfer_id = $this->_dbInsert($data,'table_warehouse_stk_trns');  








								foreach ($transferBSNArray as $key => $bsnSubset) {
										# code...

									foreach ($bsnSubset as $key => $value) {
										# code...

										// 	Record bsn transfer to warehouse

										$whStock = array();
										$whStock['transfer_id'] 			=  $transfer_id;
										$whStock['item_id'] 				=  $value['item_id'];
										$whStock['bsn_id'] 		    		=  $value['bsn_id'];
										$whStock['bsn']       				=  $value['bsn'];
										

										$stocktransfer=$this->_dbInsert($whStock,'table_warehouse_stk_trn_dts');  








										// Remove the bsn from warehouse and add into to warehouse
										$whStock = array();
										$whStock['warehouse_id']	 			=  $to_warehouse_id;
										$whStock['last_updated_date']	 		=  date('Y-m-d');	
										$whStock['last_update_datetime'] 		=  date('Y-m-d H:i:s');
										$whStock['transfer_type']				=  'W'; 	//W = when stock transferred from WH.
										$whStock['status']		            	=  'I';

										$this->_dbUpdate($whStock,'table_warehouse_stock', 'warehouse_stock_id="'.$value['warehouse_stock_id'].'"');

									}




								}			
									if($transfer_id >0)
							{
                                     $this->whtowhtransferlist($transfer_id,$tallymasterno,$tallymasternoreport);

                            }
                           return array('status'=>'success', 'msg'=>'Stock transfer successfully','firstw'=>$message);
							//return array('status'=>'success', 'msg'=>'Stock transfer successfully');
						} else {


							return array('status'=>'success', 'msg'=>'Please input correct qty of stock transfer!');
						}


					}

				

			} else {

				return array('status'=>'failed', 'msg'=>'No Bsn Selected!');
			}




		} else {
			return array('status'=>'failed', 'msg'=>'Imput parameters missing, Please Select Warehouse, dispacth date and number!');
		}


	}




function whtowhtransferlist($tranfer,$tallymasterno,$tallymasternoreport)
{
/*
echo"<pre>";
print_R($tranfer);
echo"<pre>";
print_R($tallymasterno);
echo"<pre>";
print_R($tallymasternoreport);*/
	$attributeArray = array();
	//$attributeArray['TALLYMASTERNOREPORT'] = $tallymasternoreport;
	$attributeArray['TALLYMASTERNO'] = $tallymasterno;
        
 $op=$this->_dbUpdate2($attributeArray,'table_warehouse_stk_trn_dts', "transfer_id =".$tranfer );
 $bsnidDetail = $this->_getSelectList2(' table_warehouse_stk_trn_dts',"bsn_id",'',"transfer_id ='".$tranfer."'");
 

 foreach ($bsnidDetail as  $value)
    {
 	$id=$value->bsn_id;
 	$attribute['TALLYMASTERNO'] = 0;
 	$this->_dbUpdate2($attribute,'table_warehouse_stock', "bsn_id =".$id );
     

      }
//print_R($op);

}
	

	/****************************************************************
	* DESC : Warehouse transfer stock to another warehouse
	* Author: AJAY
	* Created: 2017-04-25
	*
	*
	*****************************************************************/
	/*********************** Warehouse to Warehouse Transfer text file importing @06-September-2017 Chirag Gupta *****************/

		public function stockTransferToWHUpload($paramSet)
		{



			if(sizeof($paramSet)>0 && isset($paramSet['from_warehouse_id']) && $paramSet['from_warehouse_id']>0 && isset($paramSet['to_warehouse_id']) && $paramSet['to_warehouse_id']>0 && isset($paramSet['dispatch_no']) && !empty($paramSet['dispatch_no']) && isset($paramSet['dispatch_date']) && !empty($paramSet['dispatch_date']))
			{

				$tranferStockArray = array();
				$itemListArray     = array();
				$warehouse_id 		= $paramSet['from_warehouse_id'];
				$to_warehouse_id     = $paramSet['to_warehouse_id'];
				$bill_no 	 		= trim($paramSet['dispatch_no']);
				$bill_date 	 		= date('Y-m-d', strtotime($paramSet['dispatch_date']));

                   $wh_state_id=$this->_getSelectList('table_warehouse AS WH',"state_id,warehouse_address,warehouse_name,warehouse_phone_no,city_id",''," WH.warehouse_id='".$warehouse_id."'  ");
           $state_id=$wh_state_id[0]->state_id;
           $warehouse_name=$wh_state_id[0]->warehouse_name;//From from warehouse


           $wh_state_id1=$this->_getSelectList('table_warehouse AS WH',"state_id,warehouse_name,warehouse_phone_no,city_id,warehouse_address",''," WH.warehouse_id='".$to_warehouse_id."'  ");
           $state_id1=$wh_state_id1[0]->state_id;

           $Towarehouse_name=$wh_state_id1[0]->warehouse_name;//To from warehouse
            $dateFormat1 = date('Ymd'); // 20171202
            $DATE = $dateFormat1;

           $state_name_fromwh=$this->_getSelectList2('state AS s',"state_name",''," s.state_id='".$wh_state_id[0]->state_id."'  ");
           $city_id_fromwh=$this->_getSelectList2('city AS c',"city_name",''," c.city_id='".$wh_state_id[0]->city_id."'  ");

           $state_name_towh=$this->_getSelectList2('state AS s',"state_name",''," s.state_id='".$wh_state_id1[0]->state_id."'  ");
           $city_id_towh=$this->_getSelectList2('city AS c',"city_name",''," c.city_id='".$wh_state_id1[0]->city_id."'  ");




                 if($state_id==$state_id1)
           {
             
	 		$wh_name=$warehouse_name;
	 		$Towh_name=$Towarehouse_name;
	 	    $arr = explode(' ',trim($wh_name));
	 	    $arr1 = explode(' ',trim($Towh_name));
	 	    $challan=ucwords(strtolower($arr[1]));
	 	    $challanto=ucwords(strtolower($arr1[1]));

            $voucher_type=ucwords(strtolower($arr[1]))." Delivery Challan GST";
            $voucher_typereport=ucwords(strtolower($arr1[1]))." Receipt Note";
           // $godown_name="CHN ". ucwords(strtolower($arr1[1]))." Sales";
           // $ledgername=$voucher_type." Stock Transfer Agnt F Form";
            $ledgernameto="Eastman Auto &amp; Power limited-".$challanto;
            $ledgername="Eastman Auto &amp; Power limited-".$challan;
            $ledgername_report_from=ucwords(strtolower($arr[1]))." Branch Transfer";
            $ledgername_report="Receipt Note ".ucwords(strtolower($arr1[1]));
            //$BASICUSERDESCRIPTION=$auRec[0]->bsn;
            $requestXMLSTART='<ENVELOPE>
			<HEADER>
			<TALLYREQUEST>Import Data</TALLYREQUEST>
			</HEADER>
			<BODY>
			<IMPORTDATA>
			<REQUESTDESC>
			<REPORTNAME>Vouchers</REPORTNAME>
			<STATICVARIABLES>
			<SVCURRENTCOMPANY>EastmanLtd</SVCURRENTCOMPANY>
			</STATICVARIABLES>
			</REQUESTDESC>
			<REQUESTDATA>
			<TALLYMESSAGE xmlns:UDF="TallyUDF">

			<VOUCHER VCHTYPE="'.$voucher_type.'" ACTION="Create" OBJVIEW="Invoice Voucher View">

			 <ADDRESS.LIST TYPE="String">
            <ADDRESS>'.$wh_state_id1[0]->warehouse_address.' </ADDRESS>
            <ADDRESS>'.$city_id_towh[0]->city_name.','.$state_name_towh[0]->state_name.'</ADDRESS>
            <ADDRESS>Mobile No:'.$wh_state_id1[0]->warehouse_phone_no.'</ADDRESS>
           
            </ADDRESS.LIST>
           <BASICBUYERADDRESS.LIST TYPE="String">

			<BASICBUYERADDRESS>'.$wh_state_id1[0]->warehouse_address.'</BASICBUYERADDRESS>
			<BASICBUYERADDRESS>'.$city_id_towh[0]->city_name.','.$state_name_towh[0]->state_name.'</BASICBUYERADDRESS>
			<BASICBUYERADDRESS>Mobile No:'.$wh_state_id1[0]->warehouse_phone_no.'</BASICBUYERADDRESS>

			</BASICBUYERADDRESS.LIST>
			<OLDAUDITENTRYIDS.LIST TYPE="Number">
			<OLDAUDITENTRYIDS>-1</OLDAUDITENTRYIDS>
			</OLDAUDITENTRYIDS.LIST>
			<DATE>'.$DATE.'</DATE>
			<REFERENCEDATE>'.$DATE.'</REFERENCEDATE>

			<GSTREGISTRATIONTYPE></GSTREGISTRATIONTYPE>
			<VATDEALERTYPE></VATDEALERTYPE>
			<TAXUNITNAME>Default Tax Unit</TAXUNITNAME>
			<CLASSNAME></CLASSNAME>
			<PARTYNAME>'.$ledgernameto.'</PARTYNAME>
			<VOUCHERTYPENAME>'.$voucher_type.'</VOUCHERTYPENAME>
			<REFERENCE></REFERENCE>
			<VOUCHERNUMBER></VOUCHERNUMBER>
			<PARTYLEDGERNAME>'.$ledgernameto.'</PARTYLEDGERNAME>
			<BASICBASEPARTYNAME>'.$ledgernameto.'</BASICBASEPARTYNAME>
			<CSTFORMISSUETYPE/>
			<CSTFORMRECVTYPE/>
			<FBTPAYMENTTYPE>Default</FBTPAYMENTTYPE>
			<PERSISTEDVIEW>Invoice Voucher View</PERSISTEDVIEW>
			<BASICBUYERNAME>'.$ledgernameto.'</BASICBUYERNAME>
			<BASICFINALDESTINATION>'.$city_id_towh[0]->city_name.'</BASICFINALDESTINATION>
			<BASICDATETIMEOFINVOICE>3-Aug-2017 at 15:58</BASICDATETIMEOFINVOICE>
			<BASICDATETIMEOFREMOVAL>3-Aug-2017 at 15:58</BASICDATETIMEOFREMOVAL>
			<VCHGSTCLASS/>
			<ENTEREDBY></ENTEREDBY>
			<DIFFACTUALQTY>No</DIFFACTUALQTY>
			<ISMSTFROMSYNC>No</ISMSTFROMSYNC>
			<ASORIGINAL>No</ASORIGINAL>
			<AUDITED>No</AUDITED>
			<FORJOBCOSTING>No</FORJOBCOSTING>
			<ISOPTIONAL>No</ISOPTIONAL>
			<EFFECTIVEDATE>20170801</EFFECTIVEDATE>
			<USEFOREXCISE>No</USEFOREXCISE>
			<ISFORJOBWORKIN>No</ISFORJOBWORKIN>
			<ALLOWCONSUMPTION>No</ALLOWCONSUMPTION>
			<USEFORINTEREST>No</USEFORINTEREST>
			<USEFORGAINLOSS>No</USEFORGAINLOSS>
			<USEFORGODOWNTRANSFER>No</USEFORGODOWNTRANSFER>
			<USEFORCOMPOUND>No</USEFORCOMPOUND>
			<USEFORSERVICETAX>No</USEFORSERVICETAX>
			<ISEXCISEVOUCHER>No</ISEXCISEVOUCHER>
			<EXCISETAXOVERRIDE>No</EXCISETAXOVERRIDE>
			<USEFORTAXUNITTRANSFER>No</USEFORTAXUNITTRANSFER>
			<EXCISEOPENING>No</EXCISEOPENING>
			<USEFORFINALPRODUCTION>No</USEFORFINALPRODUCTION>
			<ISTDSOVERRIDDEN>No</ISTDSOVERRIDDEN>
			<ISTCSOVERRIDDEN>No</ISTCSOVERRIDDEN>
			<ISTDSTCSCASHVCH>No</ISTDSTCSCASHVCH>
			<INCLUDEADVPYMTVCH>No</INCLUDEADVPYMTVCH>
			<ISSUBWORKSCONTRACT>No</ISSUBWORKSCONTRACT>
			<ISVATOVERRIDDEN>No</ISVATOVERRIDDEN>
			<IGNOREORIGVCHDATE>No</IGNOREORIGVCHDATE>
			<ISSERVICETAXOVERRIDDEN>No</ISSERVICETAXOVERRIDDEN>
			<ISISDVOUCHER>No</ISISDVOUCHER>
			<ISEXCISEOVERRIDDEN>No</ISEXCISEOVERRIDDEN>
			<ISEXCISESUPPLYVCH>No</ISEXCISESUPPLYVCH>
			<ISGSTOVERRIDDEN>No</ISGSTOVERRIDDEN>
			<GSTNOTEXPORTED>No</GSTNOTEXPORTED>
			<ISVATPRINCIPALACCOUNT>No</ISVATPRINCIPALACCOUNT>
			<ISSHIPPINGWITHINSTATE>No</ISSHIPPINGWITHINSTATE>
			<ISCANCELLED>No</ISCANCELLED>
			<HASCASHFLOW>Yes</HASCASHFLOW>
			<ISPOSTDATED>No</ISPOSTDATED>
			<USETRACKINGNUMBER>No</USETRACKINGNUMBER>
			<ISINVOICE>Yes</ISINVOICE>
			<MFGJOURNAL>No</MFGJOURNAL>
			<HASDISCOUNTS>Yes</HASDISCOUNTS>
			<ASPAYSLIP>No</ASPAYSLIP>
			<ISCOSTCENTRE>No</ISCOSTCENTRE>
			<ISSTXNONREALIZEDVCH>No</ISSTXNONREALIZEDVCH>
			<ISEXCISEMANUFACTURERON>No</ISEXCISEMANUFACTURERON>
			<ISBLANKCHEQUE>No</ISBLANKCHEQUE>
			<ISVOID>No</ISVOID>
			<ISONHOLD>No</ISONHOLD>
			<ORDERLINESTATUS>No</ORDERLINESTATUS>
			<VATISAGNSTCANCSALES>No</VATISAGNSTCANCSALES>
			<VATISPURCEXEMPTED>No</VATISPURCEXEMPTED>
			<ISVATRESTAXINVOICE>No</ISVATRESTAXINVOICE>
			<VATISASSESABLECALCVCH>No</VATISASSESABLECALCVCH>
			<ISVATDUTYPAID>Yes</ISVATDUTYPAID>
			<ISDELIVERYSAMEASCONSIGNEE>No</ISDELIVERYSAMEASCONSIGNEE>
			<ISDISPATCHSAMEASCONSIGNOR>No</ISDISPATCHSAMEASCONSIGNOR>
			<ISDELETED>No</ISDELETED>
			<CHANGEVCHMODE>No</CHANGEVCHMODE>
			<ALTERID></ALTERID>
			<MASTERID></MASTERID>
			<VOUCHERKEY></VOUCHERKEY>
			<EXCLUDEDTAXATIONS.LIST></EXCLUDEDTAXATIONS.LIST>
			<OLDAUDITENTRIES.LIST></OLDAUDITENTRIES.LIST>
			<ACCOUNTAUDITENTRIES.LIST></ACCOUNTAUDITENTRIES.LIST>
			<AUDITENTRIES.LIST></AUDITENTRIES.LIST>
			<DUTYHEADDETAILS.LIST></DUTYHEADDETAILS.LIST>
			<SUPPLEMENTARYDUTYHEADDETAILS.LIST></SUPPLEMENTARYDUTYHEADDETAILS.LIST>
			<INVOICEDELNOTES.LIST></INVOICEDELNOTES.LIST>
			<INVOICEORDERLIST.LIST></INVOICEORDERLIST.LIST>
			<INVOICEINDENTLIST.LIST></INVOICEINDENTLIST.LIST>
			<ATTENDANCEENTRIES.LIST></ATTENDANCEENTRIES.LIST>
			<ORIGINVOICEDETAILS.LIST></ORIGINVOICEDETAILS.LIST>
			<INVOICEEXPORTLIST.LIST></INVOICEEXPORTLIST.LIST>
			<LEDGERENTRIES.LIST>
			<OLDAUDITENTRYIDS.LIST TYPE="Number">
			<OLDAUDITENTRYIDS>-1</OLDAUDITENTRYIDS>
			</OLDAUDITENTRYIDS.LIST>
			<LEDGERNAME>'.$ledgername.'</LEDGERNAME>
			<GSTCLASS/>
			<ISDEEMEDPOSITIVE>No</ISDEEMEDPOSITIVE>
			<LEDGERFROMITEM>No</LEDGERFROMITEM>
			<REMOVEZEROENTRIES>No</REMOVEZEROENTRIES>
			<ISPARTYLEDGER>Yes</ISPARTYLEDGER>
			<ISLASTDEEMEDPOSITIVE>No</ISLASTDEEMEDPOSITIVE>
			<AMOUNT></AMOUNT>
			<SERVICETAXDETAILS.LIST></SERVICETAXDETAILS.LIST>

			<BILLALLOCATIONS.LIST></BILLALLOCATIONS.LIST>
			<INTERESTCOLLECTION.LIST></INTERESTCOLLECTION.LIST>
			<OLDAUDITENTRIES.LIST></OLDAUDITENTRIES.LIST>
			<ACCOUNTAUDITENTRIES.LIST></ACCOUNTAUDITENTRIES.LIST>
			<AUDITENTRIES.LIST></AUDITENTRIES.LIST>
			<INPUTCRALLOCS.LIST></INPUTCRALLOCS.LIST>
			<DUTYHEADDETAILS.LIST></DUTYHEADDETAILS.LIST>
			<EXCISEDUTYHEADDETAILS.LIST></EXCISEDUTYHEADDETAILS.LIST>
			<RATEDETAILS.LIST></RATEDETAILS.LIST>
			<SUMMARYALLOCS.LIST></SUMMARYALLOCS.LIST>
			<STPYMTDETAILS.LIST></STPYMTDETAILS.LIST>
			<EXCISEPAYMENTALLOCATIONS.LIST></EXCISEPAYMENTALLOCATIONS.LIST>
			<TAXBILLALLOCATIONS.LIST></TAXBILLALLOCATIONS.LIST>
			<TAXOBJECTALLOCATIONS.LIST></TAXOBJECTALLOCATIONS.LIST>
			<TDSEXPENSEALLOCATIONS.LIST></TDSEXPENSEALLOCATIONS.LIST>
			<VATSTATUTORYDETAILS.LIST></VATSTATUTORYDETAILS.LIST>
			<COSTTRACKALLOCATIONS.LIST></COSTTRACKALLOCATIONS.LIST>
			<REFVOUCHERDETAILS.LIST></REFVOUCHERDETAILS.LIST>
			<INVOICEWISEDETAILS.LIST></INVOICEWISEDETAILS.LIST>
			<VATITCDETAILS.LIST></VATITCDETAILS.LIST>
			<ADVANCETAXDETAILS.LIST></ADVANCETAXDETAILS.LIST>
			</LEDGERENTRIES.LIST>
			<LEDGERENTRIES.LIST>

				<TAXCLASSIFICATIONNAME/>
				<LEDGERNAME>'.$ledgername_report_from.'</LEDGERNAME>
				<METHODTYPE>As User Defined Value</METHODTYPE>
				<VOUCHERFBTCATEGORY/>
				<GSTCLASS/>
				<ISDEEMEDPOSITIVE>No</ISDEEMEDPOSITIVE>
				<LEDGERFROMITEM>No</LEDGERFROMITEM>
				<REMOVEZEROENTRIES>No</REMOVEZEROENTRIES>
				<ISPARTYLEDGER>No</ISPARTYLEDGER>
				<ISLASTDEEMEDPOSITIVE>No</ISLASTDEEMEDPOSITIVE>
				<SERVICETAXDETAILS.LIST></SERVICETAXDETAILS.LIST>
				<BANKALLOCATIONS.LIST></BANKALLOCATIONS.LIST>
				<BILLALLOCATIONS.LIST></BILLALLOCATIONS.LIST>
				<INTERESTCOLLECTION.LIST></INTERESTCOLLECTION.LIST>
				<OLDAUDITENTRIES.LIST></OLDAUDITENTRIES.LIST>
				<ACCOUNTAUDITENTRIES.LIST></ACCOUNTAUDITENTRIES.LIST>
				<AUDITENTRIES.LIST></AUDITENTRIES.LIST>
				<INPUTCRALLOCS.LIST></INPUTCRALLOCS.LIST>
				<INVENTORYALLOCATIONS.LIST></INVENTORYALLOCATIONS.LIST>
				<DUTYHEADDETAILS.LIST></DUTYHEADDETAILS.LIST>
				<EXCISEDUTYHEADDETAILS.LIST></EXCISEDUTYHEADDETAILS.LIST>
				<RATEDETAILS.LIST></RATEDETAILS.LIST>
				<SUMMARYALLOCS.LIST></SUMMARYALLOCS.LIST>
				<STPYMTDETAILS.LIST></STPYMTDETAILS.LIST>
				<EXCISEPAYMENTALLOCATIONS.LIST></EXCISEPAYMENTALLOCATIONS.LIST>
				<TAXBILLALLOCATIONS.LIST></TAXBILLALLOCATIONS.LIST>
				<TAXOBJECTALLOCATIONS.LIST></TAXOBJECTALLOCATIONS.LIST>
				<TDSEXPENSEALLOCATIONS.LIST></TDSEXPENSEALLOCATIONS.LIST>
				<VATSTATUTORYDETAILS.LIST></VATSTATUTORYDETAILS.LIST>
				<REFVOUCHERDETAILS.LIST></REFVOUCHERDETAILS.LIST>
				<INVOICEWISEDETAILS.LIST></INVOICEWISEDETAILS.LIST>
				<VATITCDETAILS.LIST></VATITCDETAILS.LIST>
				<ADVANCETAXDETAILS.LIST></ADVANCETAXDETAILS.LIST>
				</LEDGERENTRIES.LIST>
				<LEDGERENTRIES.LIST>
				<TAXCLASSIFICATIONNAME/>
				<LEDGERNAME></LEDGERNAME>
				<METHODTYPE>As User Defined Value</METHODTYPE>
				<VOUCHERFBTCATEGORY/>
				<GSTCLASS/>
				<ISDEEMEDPOSITIVE>No</ISDEEMEDPOSITIVE>
				<LEDGERFROMITEM>No</LEDGERFROMITEM>
				<REMOVEZEROENTRIES>No</REMOVEZEROENTRIES>
				<ISPARTYLEDGER>No</ISPARTYLEDGER>
				<ISLASTDEEMEDPOSITIVE>No</ISLASTDEEMEDPOSITIVE>
				<SERVICETAXDETAILS.LIST></SERVICETAXDETAILS.LIST>
				<BANKALLOCATIONS.LIST></BANKALLOCATIONS.LIST>
				<BILLALLOCATIONS.LIST></BILLALLOCATIONS.LIST>
				<INTERESTCOLLECTION.LIST></INTERESTCOLLECTION.LIST>
				<OLDAUDITENTRIES.LIST></OLDAUDITENTRIES.LIST>
				<ACCOUNTAUDITENTRIES.LIST></ACCOUNTAUDITENTRIES.LIST>
				<AUDITENTRIES.LIST></AUDITENTRIES.LIST>
				<INPUTCRALLOCS.LIST></INPUTCRALLOCS.LIST>
				<INVENTORYALLOCATIONS.LIST></INVENTORYALLOCATIONS.LIST>
				<DUTYHEADDETAILS.LIST></DUTYHEADDETAILS.LIST>
				<EXCISEDUTYHEADDETAILS.LIST></EXCISEDUTYHEADDETAILS.LIST>
				<RATEDETAILS.LIST></RATEDETAILS.LIST>
				<SUMMARYALLOCS.LIST></SUMMARYALLOCS.LIST>
				<STPYMTDETAILS.LIST></STPYMTDETAILS.LIST>
				<EXCISEPAYMENTALLOCATIONS.LIST></EXCISEPAYMENTALLOCATIONS.LIST>
				<TAXBILLALLOCATIONS.LIST></TAXBILLALLOCATIONS.LIST>
				<TAXOBJECTALLOCATIONS.LIST></TAXOBJECTALLOCATIONS.LIST>
				<TDSEXPENSEALLOCATIONS.LIST></TDSEXPENSEALLOCATIONS.LIST>
				<VATSTATUTORYDETAILS.LIST></VATSTATUTORYDETAILS.LIST>
				<REFVOUCHERDETAILS.LIST></REFVOUCHERDETAILS.LIST>
				<INVOICEWISEDETAILS.LIST></INVOICEWISEDETAILS.LIST>
				<VATITCDETAILS.LIST></VATITCDETAILS.LIST>
				<ADVANCETAXDETAILS.LIST></ADVANCETAXDETAILS.LIST>
				</LEDGERENTRIES.LIST>';


		/*	  $requestXMLSTARTReport='<ENVELOPE>
			<HEADER>
			<TALLYREQUEST>Import Data</TALLYREQUEST>
			</HEADER>
			<BODY>
			<IMPORTDATA>
			<REQUESTDESC>
			<REPORTNAME>Vouchers</REPORTNAME>
			<STATICVARIABLES>
			<SVCURRENTCOMPANY>EastmanLtd</SVCURRENTCOMPANY>
			</STATICVARIABLES>
			</REQUESTDESC>
			<REQUESTDATA>
			<TALLYMESSAGE xmlns:UDF="TallyUDF">

			<VOUCHER VCHTYPE="'.$voucher_typereport.'" ACTION="Create" OBJVIEW="Invoice Voucher View">

			<BASICBUYERADDRESS.LIST TYPE="String">
			<BASICBUYERADDRESS>Flat No. 101, First Floor</BASICBUYERADDRESS>
			<BASICBUYERADDRESS>1, Community Centre,</BASICBUYERADDRESS>
			<BASICBUYERADDRESS>Nariana Industrial Area</BASICBUYERADDRESS>
			<BASICBUYERADDRESS>Phase-1, New Delhi-110028</BASICBUYERADDRESS>

			</BASICBUYERADDRESS.LIST>
			<OLDAUDITENTRYIDS.LIST TYPE="Number">
			<OLDAUDITENTRYIDS>-1</OLDAUDITENTRYIDS>
			</OLDAUDITENTRYIDS.LIST>
			<DATE>'.$DATE.'</DATE>
			<REFERENCEDATE>'.$DATE.'</REFERENCEDATE>

			<GSTREGISTRATIONTYPE></GSTREGISTRATIONTYPE>
			<VATDEALERTYPE></VATDEALERTYPE>
			<TAXUNITNAME>Default Tax Unit</TAXUNITNAME>
			<CLASSNAME></CLASSNAME>
			<PARTYNAME>'.$ledgernameto.'</PARTYNAME>
			<VOUCHERTYPENAME>'.$voucher_typereport.'</VOUCHERTYPENAME>
			<REFERENCE></REFERENCE>
			<VOUCHERNUMBER></VOUCHERNUMBER>
			<PARTYLEDGERNAME>'.$ledgernameto.'</PARTYLEDGERNAME>
			<BASICBASEPARTYNAME>'.$ledgernameto.'</BASICBASEPARTYNAME>
			<CSTFORMISSUETYPE/>
			<CSTFORMRECVTYPE/>
			<FBTPAYMENTTYPE>Default</FBTPAYMENTTYPE>
			<PERSISTEDVIEW>Invoice Voucher View</PERSISTEDVIEW>
			<BASICBUYERNAME>EastmanLtd</BASICBUYERNAME>
			<BASICDATETIMEOFINVOICE>3-Aug-2017 at 15:58</BASICDATETIMEOFINVOICE>
			<BASICDATETIMEOFREMOVAL>3-Aug-2017 at 15:58</BASICDATETIMEOFREMOVAL>
			<VCHGSTCLASS/>
			<ENTEREDBY></ENTEREDBY>
			<DIFFACTUALQTY>No</DIFFACTUALQTY>
			<ISMSTFROMSYNC>No</ISMSTFROMSYNC>
			<ASORIGINAL>No</ASORIGINAL>
			<AUDITED>No</AUDITED>
			<FORJOBCOSTING>No</FORJOBCOSTING>
			<ISOPTIONAL>No</ISOPTIONAL>
			<EFFECTIVEDATE>20170801</EFFECTIVEDATE>
			<USEFOREXCISE>No</USEFOREXCISE>
			<ISFORJOBWORKIN>No</ISFORJOBWORKIN>
			<ALLOWCONSUMPTION>No</ALLOWCONSUMPTION>
			<USEFORINTEREST>No</USEFORINTEREST>
			<USEFORGAINLOSS>No</USEFORGAINLOSS>
			<USEFORGODOWNTRANSFER>No</USEFORGODOWNTRANSFER>
			<USEFORCOMPOUND>No</USEFORCOMPOUND>
			<USEFORSERVICETAX>No</USEFORSERVICETAX>
			<ISEXCISEVOUCHER>No</ISEXCISEVOUCHER>
			<EXCISETAXOVERRIDE>No</EXCISETAXOVERRIDE>
			<USEFORTAXUNITTRANSFER>No</USEFORTAXUNITTRANSFER>
			<EXCISEOPENING>No</EXCISEOPENING>
			<USEFORFINALPRODUCTION>No</USEFORFINALPRODUCTION>
			<ISTDSOVERRIDDEN>No</ISTDSOVERRIDDEN>
			<ISTCSOVERRIDDEN>No</ISTCSOVERRIDDEN>
			<ISTDSTCSCASHVCH>No</ISTDSTCSCASHVCH>
			<INCLUDEADVPYMTVCH>No</INCLUDEADVPYMTVCH>
			<ISSUBWORKSCONTRACT>No</ISSUBWORKSCONTRACT>
			<ISVATOVERRIDDEN>No</ISVATOVERRIDDEN>
			<IGNOREORIGVCHDATE>No</IGNOREORIGVCHDATE>
			<ISSERVICETAXOVERRIDDEN>No</ISSERVICETAXOVERRIDDEN>
			<ISISDVOUCHER>No</ISISDVOUCHER>
			<ISEXCISEOVERRIDDEN>No</ISEXCISEOVERRIDDEN>
			<ISEXCISESUPPLYVCH>No</ISEXCISESUPPLYVCH>
			<ISGSTOVERRIDDEN>No</ISGSTOVERRIDDEN>
			<GSTNOTEXPORTED>No</GSTNOTEXPORTED>
			<ISVATPRINCIPALACCOUNT>No</ISVATPRINCIPALACCOUNT>
			<ISSHIPPINGWITHINSTATE>No</ISSHIPPINGWITHINSTATE>
			<ISCANCELLED>No</ISCANCELLED>
			<HASCASHFLOW>Yes</HASCASHFLOW>
			<ISPOSTDATED>No</ISPOSTDATED>
			<USETRACKINGNUMBER>No</USETRACKINGNUMBER>
			<ISINVOICE>Yes</ISINVOICE>
			<MFGJOURNAL>No</MFGJOURNAL>
			<HASDISCOUNTS>Yes</HASDISCOUNTS>
			<ASPAYSLIP>No</ASPAYSLIP>
			<ISCOSTCENTRE>No</ISCOSTCENTRE>
			<ISSTXNONREALIZEDVCH>No</ISSTXNONREALIZEDVCH>
			<ISEXCISEMANUFACTURERON>No</ISEXCISEMANUFACTURERON>
			<ISBLANKCHEQUE>No</ISBLANKCHEQUE>
			<ISVOID>No</ISVOID>
			<ISONHOLD>No</ISONHOLD>
			<ORDERLINESTATUS>No</ORDERLINESTATUS>
			<VATISAGNSTCANCSALES>No</VATISAGNSTCANCSALES>
			<VATISPURCEXEMPTED>No</VATISPURCEXEMPTED>
			<ISVATRESTAXINVOICE>No</ISVATRESTAXINVOICE>
			<VATISASSESABLECALCVCH>No</VATISASSESABLECALCVCH>
			<ISVATDUTYPAID>Yes</ISVATDUTYPAID>
			<ISDELIVERYSAMEASCONSIGNEE>No</ISDELIVERYSAMEASCONSIGNEE>
			<ISDISPATCHSAMEASCONSIGNOR>No</ISDISPATCHSAMEASCONSIGNOR>
			<ISDELETED>No</ISDELETED>
			<CHANGEVCHMODE>No</CHANGEVCHMODE>
			<ALTERID></ALTERID>
			<MASTERID></MASTERID>
			<VOUCHERKEY></VOUCHERKEY>
			<EXCLUDEDTAXATIONS.LIST></EXCLUDEDTAXATIONS.LIST>
			<OLDAUDITENTRIES.LIST></OLDAUDITENTRIES.LIST>
			<ACCOUNTAUDITENTRIES.LIST></ACCOUNTAUDITENTRIES.LIST>
			<AUDITENTRIES.LIST></AUDITENTRIES.LIST>
			<DUTYHEADDETAILS.LIST></DUTYHEADDETAILS.LIST>
			<SUPPLEMENTARYDUTYHEADDETAILS.LIST></SUPPLEMENTARYDUTYHEADDETAILS.LIST>
			<INVOICEDELNOTES.LIST></INVOICEDELNOTES.LIST>
			<INVOICEORDERLIST.LIST></INVOICEORDERLIST.LIST>
			<INVOICEINDENTLIST.LIST></INVOICEINDENTLIST.LIST>
			<ATTENDANCEENTRIES.LIST></ATTENDANCEENTRIES.LIST>
			<ORIGINVOICEDETAILS.LIST></ORIGINVOICEDETAILS.LIST>
			<INVOICEEXPORTLIST.LIST></INVOICEEXPORTLIST.LIST>
			<LEDGERENTRIES.LIST>
			<OLDAUDITENTRYIDS.LIST TYPE="Number">
			<OLDAUDITENTRYIDS>-1</OLDAUDITENTRYIDS>
			</OLDAUDITENTRYIDS.LIST>
			<LEDGERNAME>'.$ledgernameto.'</LEDGERNAME>
			<GSTCLASS/>
			<ISDEEMEDPOSITIVE>No</ISDEEMEDPOSITIVE>
			<LEDGERFROMITEM>No</LEDGERFROMITEM>
			<REMOVEZEROENTRIES>No</REMOVEZEROENTRIES>
			<ISPARTYLEDGER>Yes</ISPARTYLEDGER>
			<ISLASTDEEMEDPOSITIVE>No</ISLASTDEEMEDPOSITIVE>
			<AMOUNT></AMOUNT>
			<SERVICETAXDETAILS.LIST></SERVICETAXDETAILS.LIST>

			<BILLALLOCATIONS.LIST></BILLALLOCATIONS.LIST>
			<INTERESTCOLLECTION.LIST></INTERESTCOLLECTION.LIST>
			<OLDAUDITENTRIES.LIST></OLDAUDITENTRIES.LIST>
			<ACCOUNTAUDITENTRIES.LIST></ACCOUNTAUDITENTRIES.LIST>
			<AUDITENTRIES.LIST></AUDITENTRIES.LIST>
			<INPUTCRALLOCS.LIST></INPUTCRALLOCS.LIST>
			<DUTYHEADDETAILS.LIST></DUTYHEADDETAILS.LIST>
			<EXCISEDUTYHEADDETAILS.LIST></EXCISEDUTYHEADDETAILS.LIST>
			<RATEDETAILS.LIST></RATEDETAILS.LIST>
			<SUMMARYALLOCS.LIST></SUMMARYALLOCS.LIST>
			<STPYMTDETAILS.LIST></STPYMTDETAILS.LIST>
			<EXCISEPAYMENTALLOCATIONS.LIST></EXCISEPAYMENTALLOCATIONS.LIST>
			<TAXBILLALLOCATIONS.LIST></TAXBILLALLOCATIONS.LIST>
			<TAXOBJECTALLOCATIONS.LIST></TAXOBJECTALLOCATIONS.LIST>
			<TDSEXPENSEALLOCATIONS.LIST></TDSEXPENSEALLOCATIONS.LIST>
			<VATSTATUTORYDETAILS.LIST></VATSTATUTORYDETAILS.LIST>
			<COSTTRACKALLOCATIONS.LIST></COSTTRACKALLOCATIONS.LIST>
			<REFVOUCHERDETAILS.LIST></REFVOUCHERDETAILS.LIST>
			<INVOICEWISEDETAILS.LIST></INVOICEWISEDETAILS.LIST>
			<VATITCDETAILS.LIST></VATITCDETAILS.LIST>
			<ADVANCETAXDETAILS.LIST></ADVANCETAXDETAILS.LIST>
			</LEDGERENTRIES.LIST>
			<LEDGERENTRIES.LIST>

				<TAXCLASSIFICATIONNAME/>
				<LEDGERNAME>'.$ledgername_report.'</LEDGERNAME>
				<METHODTYPE>As User Defined Value</METHODTYPE>
				<VOUCHERFBTCATEGORY/>
				<GSTCLASS/>
				<ISDEEMEDPOSITIVE>No</ISDEEMEDPOSITIVE>
				<LEDGERFROMITEM>No</LEDGERFROMITEM>
				<REMOVEZEROENTRIES>No</REMOVEZEROENTRIES>
				<ISPARTYLEDGER>No</ISPARTYLEDGER>
				<ISLASTDEEMEDPOSITIVE>No</ISLASTDEEMEDPOSITIVE>
				<SERVICETAXDETAILS.LIST></SERVICETAXDETAILS.LIST>
				<BANKALLOCATIONS.LIST></BANKALLOCATIONS.LIST>
				<BILLALLOCATIONS.LIST></BILLALLOCATIONS.LIST>
				<INTERESTCOLLECTION.LIST></INTERESTCOLLECTION.LIST>
				<OLDAUDITENTRIES.LIST></OLDAUDITENTRIES.LIST>
				<ACCOUNTAUDITENTRIES.LIST></ACCOUNTAUDITENTRIES.LIST>
				<AUDITENTRIES.LIST></AUDITENTRIES.LIST>
				<INPUTCRALLOCS.LIST></INPUTCRALLOCS.LIST>
				<INVENTORYALLOCATIONS.LIST></INVENTORYALLOCATIONS.LIST>
				<DUTYHEADDETAILS.LIST></DUTYHEADDETAILS.LIST>
				<EXCISEDUTYHEADDETAILS.LIST></EXCISEDUTYHEADDETAILS.LIST>
				<RATEDETAILS.LIST></RATEDETAILS.LIST>
				<SUMMARYALLOCS.LIST></SUMMARYALLOCS.LIST>
				<STPYMTDETAILS.LIST></STPYMTDETAILS.LIST>
				<EXCISEPAYMENTALLOCATIONS.LIST></EXCISEPAYMENTALLOCATIONS.LIST>
				<TAXBILLALLOCATIONS.LIST></TAXBILLALLOCATIONS.LIST>
				<TAXOBJECTALLOCATIONS.LIST></TAXOBJECTALLOCATIONS.LIST>
				<TDSEXPENSEALLOCATIONS.LIST></TDSEXPENSEALLOCATIONS.LIST>
				<VATSTATUTORYDETAILS.LIST></VATSTATUTORYDETAILS.LIST>
				<REFVOUCHERDETAILS.LIST></REFVOUCHERDETAILS.LIST>
				<INVOICEWISEDETAILS.LIST></INVOICEWISEDETAILS.LIST>
				<VATITCDETAILS.LIST></VATITCDETAILS.LIST>
				<ADVANCETAXDETAILS.LIST></ADVANCETAXDETAILS.LIST>
				</LEDGERENTRIES.LIST>
				<LEDGERENTRIES.LIST>
				<TAXCLASSIFICATIONNAME/>
				<LEDGERNAME>Not for Sale</LEDGERNAME>
				<METHODTYPE>As User Defined Value</METHODTYPE>
				<VOUCHERFBTCATEGORY/>
				<GSTCLASS/>
				<ISDEEMEDPOSITIVE>No</ISDEEMEDPOSITIVE>
				<LEDGERFROMITEM>No</LEDGERFROMITEM>
				<REMOVEZEROENTRIES>No</REMOVEZEROENTRIES>
				<ISPARTYLEDGER>No</ISPARTYLEDGER>
				<ISLASTDEEMEDPOSITIVE>No</ISLASTDEEMEDPOSITIVE>
				<SERVICETAXDETAILS.LIST></SERVICETAXDETAILS.LIST>
				<BANKALLOCATIONS.LIST></BANKALLOCATIONS.LIST>
				<BILLALLOCATIONS.LIST></BILLALLOCATIONS.LIST>
				<INTERESTCOLLECTION.LIST></INTERESTCOLLECTION.LIST>
				<OLDAUDITENTRIES.LIST></OLDAUDITENTRIES.LIST>
				<ACCOUNTAUDITENTRIES.LIST></ACCOUNTAUDITENTRIES.LIST>
				<AUDITENTRIES.LIST></AUDITENTRIES.LIST>
				<INPUTCRALLOCS.LIST></INPUTCRALLOCS.LIST>
				<INVENTORYALLOCATIONS.LIST></INVENTORYALLOCATIONS.LIST>
				<DUTYHEADDETAILS.LIST></DUTYHEADDETAILS.LIST>
				<EXCISEDUTYHEADDETAILS.LIST></EXCISEDUTYHEADDETAILS.LIST>
				<RATEDETAILS.LIST></RATEDETAILS.LIST>
				<SUMMARYALLOCS.LIST></SUMMARYALLOCS.LIST>
				<STPYMTDETAILS.LIST></STPYMTDETAILS.LIST>
				<EXCISEPAYMENTALLOCATIONS.LIST></EXCISEPAYMENTALLOCATIONS.LIST>
				<TAXBILLALLOCATIONS.LIST></TAXBILLALLOCATIONS.LIST>
				<TAXOBJECTALLOCATIONS.LIST></TAXOBJECTALLOCATIONS.LIST>
				<TDSEXPENSEALLOCATIONS.LIST></TDSEXPENSEALLOCATIONS.LIST>
				<VATSTATUTORYDETAILS.LIST></VATSTATUTORYDETAILS.LIST>
				<REFVOUCHERDETAILS.LIST></REFVOUCHERDETAILS.LIST>
				<INVOICEWISEDETAILS.LIST></INVOICEWISEDETAILS.LIST>
				<VATITCDETAILS.LIST></VATITCDETAILS.LIST>
				<ADVANCETAXDETAILS.LIST></ADVANCETAXDETAILS.LIST>
				</LEDGERENTRIES.LIST>




			';*/


						 
							 
	$requestXMLEND='					
<PAYROLLMODEOFPAYMENT.LIST>      </PAYROLLMODEOFPAYMENT.LIST>
      <ATTDRECORDS.LIST>      </ATTDRECORDS.LIST>
     </VOUCHER>
    </TALLYMESSAGE>

   </REQUESTDATA>
  </IMPORTDATA>
 </BODY>
</ENVELOPE>';


           }

           else
           {

           	$wh_name=$warehouse_name;
	 		$Towh_name=$Towarehouse_name;
	 	    $arr = explode(' ',trim($wh_name));
	 	    $arr1 = explode(' ',trim($Towh_name));
	 	    $challan=ucwords(strtolower($arr[1]));
	 	     $challanto=ucwords(strtolower($arr1[1]));
            $voucher_type=ucwords(strtolower($arr[1]))." Tax Invoice GST";
            $voucher_typereport=ucwords(strtolower($arr1[1]))." Receipt Note";
           // $godown_name="CHN ". ucwords(strtolower($arr1[1]))." Sales";
            $ledgername="Eastman Auto &amp; Power limited-".$challan;
            $ledgername_report="Receipt Note ".ucwords(strtolower($arr1[1]));
            $ledgername_report_from=ucwords(strtolower($arr[1]))." Branch Transfer";

            $ledgernameto="Eastman Auto &amp; Power limited-".$challanto;
           // $ledgername=$voucher_type." Stock Transfer Agnt F Form";
            $ledgernamegst=ucwords(strtolower($arr[1]))." GST Sales @ 18%";
            
            //$BASICUSERDESCRIPTION=$auRec[0]->bsn;
            $requestXMLSTART='<ENVELOPE>
			<HEADER>
			<TALLYREQUEST>Import Data</TALLYREQUEST>
			</HEADER>
			<BODY>
			<IMPORTDATA>
			<REQUESTDESC>
			<REPORTNAME>Vouchers</REPORTNAME>
			<STATICVARIABLES>
			<SVCURRENTCOMPANY>EastmanLtd</SVCURRENTCOMPANY>
			</STATICVARIABLES>
			</REQUESTDESC>
			<REQUESTDATA>
			<TALLYMESSAGE xmlns:UDF="TallyUDF">

			<VOUCHER VCHTYPE="'.$voucher_type.'" ACTION="Create" OBJVIEW="Invoice Voucher View">

			<ADDRESS.LIST TYPE="String">
            <ADDRESS>'.$wh_state_id1[0]->warehouse_address.' </ADDRESS>
            <ADDRESS>'.$city_id_towh[0]->city_name.','.$state_name_towh[0]->state_name.'</ADDRESS>
            <ADDRESS>Mobile No:'.$wh_state_id1[0]->warehouse_phone_no.'</ADDRESS>
            </ADDRESS.LIST>
			<BASICBUYERADDRESS.LIST TYPE="String">
				<BASICBUYERADDRESS>'.$wh_state_id1[0]->warehouse_address.'</BASICBUYERADDRESS>
			<BASICBUYERADDRESS>'.$city_id_towh[0]->city_name.','.$state_name_towh[0]->state_name.'</BASICBUYERADDRESS>
			<BASICBUYERADDRESS>Mobile No:'.$wh_state_id1[0]->warehouse_phone_no.'</BASICBUYERADDRESS>
			</BASICBUYERADDRESS.LIST>
			<OLDAUDITENTRYIDS.LIST TYPE="Number">
			<OLDAUDITENTRYIDS>-1</OLDAUDITENTRYIDS>
			</OLDAUDITENTRYIDS.LIST>
			<DATE>'.$DATE.'</DATE>
			<REFERENCEDATE>'.$DATE.'</REFERENCEDATE>

			<GSTREGISTRATIONTYPE></GSTREGISTRATIONTYPE>
			<VATDEALERTYPE></VATDEALERTYPE>
			<TAXUNITNAME>Default Tax Unit</TAXUNITNAME>
			<CLASSNAME></CLASSNAME>
			<PARTYNAME>'.$ledgernameto.'</PARTYNAME>
			<VOUCHERTYPENAME>'.$voucher_type.'</VOUCHERTYPENAME>
			<REFERENCE></REFERENCE>
			<VOUCHERNUMBER></VOUCHERNUMBER>
			<PARTYLEDGERNAME>'.$ledgernameto.'</PARTYLEDGERNAME>
			<BASICBASEPARTYNAME>'.$ledgernameto.'</BASICBASEPARTYNAME>
			<CSTFORMISSUETYPE/>
			<CSTFORMRECVTYPE/>
			<FBTPAYMENTTYPE>Default</FBTPAYMENTTYPE>
			<PERSISTEDVIEW>Invoice Voucher View</PERSISTEDVIEW>
			<BASICBUYERNAME>'.$ledgernameto.'</BASICBUYERNAME>
			<BASICFINALDESTINATION>'.$city_id_towh[0]->city_name.'</BASICFINALDESTINATION>
			<BASICDATETIMEOFINVOICE>3-Aug-2017 at 15:58</BASICDATETIMEOFINVOICE>
			<BASICDATETIMEOFREMOVAL>3-Aug-2017 at 15:58</BASICDATETIMEOFREMOVAL>
			<VCHGSTCLASS/>
			<ENTEREDBY></ENTEREDBY>
			<DIFFACTUALQTY>No</DIFFACTUALQTY>
			<ISMSTFROMSYNC>No</ISMSTFROMSYNC>
			<ASORIGINAL>No</ASORIGINAL>
			<AUDITED>No</AUDITED>
			<FORJOBCOSTING>No</FORJOBCOSTING>
			<ISOPTIONAL>No</ISOPTIONAL>
			<EFFECTIVEDATE>20170801</EFFECTIVEDATE>
			<USEFOREXCISE>No</USEFOREXCISE>
			<ISFORJOBWORKIN>No</ISFORJOBWORKIN>
			<ALLOWCONSUMPTION>No</ALLOWCONSUMPTION>
			<USEFORINTEREST>No</USEFORINTEREST>
			<USEFORGAINLOSS>No</USEFORGAINLOSS>
			<USEFORGODOWNTRANSFER>No</USEFORGODOWNTRANSFER>
			<USEFORCOMPOUND>No</USEFORCOMPOUND>
			<USEFORSERVICETAX>No</USEFORSERVICETAX>
			<ISEXCISEVOUCHER>No</ISEXCISEVOUCHER>
			<EXCISETAXOVERRIDE>No</EXCISETAXOVERRIDE>
			<USEFORTAXUNITTRANSFER>No</USEFORTAXUNITTRANSFER>
			<EXCISEOPENING>No</EXCISEOPENING>
			<USEFORFINALPRODUCTION>No</USEFORFINALPRODUCTION>
			<ISTDSOVERRIDDEN>No</ISTDSOVERRIDDEN>
			<ISTCSOVERRIDDEN>No</ISTCSOVERRIDDEN>
			<ISTDSTCSCASHVCH>No</ISTDSTCSCASHVCH>
			<INCLUDEADVPYMTVCH>No</INCLUDEADVPYMTVCH>
			<ISSUBWORKSCONTRACT>No</ISSUBWORKSCONTRACT>
			<ISVATOVERRIDDEN>No</ISVATOVERRIDDEN>
			<IGNOREORIGVCHDATE>No</IGNOREORIGVCHDATE>
			<ISSERVICETAXOVERRIDDEN>No</ISSERVICETAXOVERRIDDEN>
			<ISISDVOUCHER>No</ISISDVOUCHER>
			<ISEXCISEOVERRIDDEN>No</ISEXCISEOVERRIDDEN>
			<ISEXCISESUPPLYVCH>No</ISEXCISESUPPLYVCH>
			<ISGSTOVERRIDDEN>No</ISGSTOVERRIDDEN>
			<GSTNOTEXPORTED>No</GSTNOTEXPORTED>
			<ISVATPRINCIPALACCOUNT>No</ISVATPRINCIPALACCOUNT>
			<ISSHIPPINGWITHINSTATE>No</ISSHIPPINGWITHINSTATE>
			<ISCANCELLED>No</ISCANCELLED>
			<HASCASHFLOW>Yes</HASCASHFLOW>
			<ISPOSTDATED>No</ISPOSTDATED>
			<USETRACKINGNUMBER>No</USETRACKINGNUMBER>
			<ISINVOICE>Yes</ISINVOICE>
			<MFGJOURNAL>No</MFGJOURNAL>
			<HASDISCOUNTS>Yes</HASDISCOUNTS>
			<ASPAYSLIP>No</ASPAYSLIP>
			<ISCOSTCENTRE>No</ISCOSTCENTRE>
			<ISSTXNONREALIZEDVCH>No</ISSTXNONREALIZEDVCH>
			<ISEXCISEMANUFACTURERON>No</ISEXCISEMANUFACTURERON>
			<ISBLANKCHEQUE>No</ISBLANKCHEQUE>
			<ISVOID>No</ISVOID>
			<ISONHOLD>No</ISONHOLD>
			<ORDERLINESTATUS>No</ORDERLINESTATUS>
			<VATISAGNSTCANCSALES>No</VATISAGNSTCANCSALES>
			<VATISPURCEXEMPTED>No</VATISPURCEXEMPTED>
			<ISVATRESTAXINVOICE>No</ISVATRESTAXINVOICE>
			<VATISASSESABLECALCVCH>No</VATISASSESABLECALCVCH>
			<ISVATDUTYPAID>Yes</ISVATDUTYPAID>
			<ISDELIVERYSAMEASCONSIGNEE>No</ISDELIVERYSAMEASCONSIGNEE>
			<ISDISPATCHSAMEASCONSIGNOR>No</ISDISPATCHSAMEASCONSIGNOR>
			<ISDELETED>No</ISDELETED>
			<CHANGEVCHMODE>No</CHANGEVCHMODE>
			<ALTERID></ALTERID>
			<MASTERID></MASTERID>
			<VOUCHERKEY></VOUCHERKEY>
			<EXCLUDEDTAXATIONS.LIST></EXCLUDEDTAXATIONS.LIST>
			<OLDAUDITENTRIES.LIST></OLDAUDITENTRIES.LIST>
			<ACCOUNTAUDITENTRIES.LIST></ACCOUNTAUDITENTRIES.LIST>
			<AUDITENTRIES.LIST></AUDITENTRIES.LIST>
			<DUTYHEADDETAILS.LIST></DUTYHEADDETAILS.LIST>
			<SUPPLEMENTARYDUTYHEADDETAILS.LIST></SUPPLEMENTARYDUTYHEADDETAILS.LIST>
			<INVOICEDELNOTES.LIST></INVOICEDELNOTES.LIST>
			<INVOICEORDERLIST.LIST></INVOICEORDERLIST.LIST>
			<INVOICEINDENTLIST.LIST></INVOICEINDENTLIST.LIST>
			<ATTENDANCEENTRIES.LIST></ATTENDANCEENTRIES.LIST>
			<ORIGINVOICEDETAILS.LIST></ORIGINVOICEDETAILS.LIST>
			<INVOICEEXPORTLIST.LIST></INVOICEEXPORTLIST.LIST>
			<LEDGERENTRIES.LIST>
			<OLDAUDITENTRYIDS.LIST TYPE="Number">
			<OLDAUDITENTRYIDS>-1</OLDAUDITENTRYIDS>
			</OLDAUDITENTRYIDS.LIST>
			<LEDGERNAME>'.$ledgername.'</LEDGERNAME>
			<GSTCLASS/>
			<ISDEEMEDPOSITIVE>No</ISDEEMEDPOSITIVE>
			<LEDGERFROMITEM>No</LEDGERFROMITEM>
			<REMOVEZEROENTRIES>No</REMOVEZEROENTRIES>
			<ISPARTYLEDGER>Yes</ISPARTYLEDGER>
			<ISLASTDEEMEDPOSITIVE>No</ISLASTDEEMEDPOSITIVE>
			<AMOUNT></AMOUNT>
			<SERVICETAXDETAILS.LIST></SERVICETAXDETAILS.LIST>

			<BILLALLOCATIONS.LIST></BILLALLOCATIONS.LIST>
			<INTERESTCOLLECTION.LIST></INTERESTCOLLECTION.LIST>
			<OLDAUDITENTRIES.LIST></OLDAUDITENTRIES.LIST>
			<ACCOUNTAUDITENTRIES.LIST></ACCOUNTAUDITENTRIES.LIST>
			<AUDITENTRIES.LIST></AUDITENTRIES.LIST>
			<INPUTCRALLOCS.LIST></INPUTCRALLOCS.LIST>
			<DUTYHEADDETAILS.LIST></DUTYHEADDETAILS.LIST>
			<EXCISEDUTYHEADDETAILS.LIST></EXCISEDUTYHEADDETAILS.LIST>
			<RATEDETAILS.LIST></RATEDETAILS.LIST>
			<SUMMARYALLOCS.LIST></SUMMARYALLOCS.LIST>
			<STPYMTDETAILS.LIST></STPYMTDETAILS.LIST>
			<EXCISEPAYMENTALLOCATIONS.LIST></EXCISEPAYMENTALLOCATIONS.LIST>
			<TAXBILLALLOCATIONS.LIST></TAXBILLALLOCATIONS.LIST>
			<TAXOBJECTALLOCATIONS.LIST></TAXOBJECTALLOCATIONS.LIST>
			<TDSEXPENSEALLOCATIONS.LIST></TDSEXPENSEALLOCATIONS.LIST>
			<VATSTATUTORYDETAILS.LIST></VATSTATUTORYDETAILS.LIST>
			<COSTTRACKALLOCATIONS.LIST></COSTTRACKALLOCATIONS.LIST>
			<REFVOUCHERDETAILS.LIST></REFVOUCHERDETAILS.LIST>
			<INVOICEWISEDETAILS.LIST></INVOICEWISEDETAILS.LIST>
			<VATITCDETAILS.LIST></VATITCDETAILS.LIST>
			<ADVANCETAXDETAILS.LIST></ADVANCETAXDETAILS.LIST>
			</LEDGERENTRIES.LIST>
			<LEDGERENTRIES.LIST>

				<TAXCLASSIFICATIONNAME/>
				<LEDGERNAME>'.$ledgername.'</LEDGERNAME>
				<METHODTYPE>As User Defined Value</METHODTYPE>
				<VOUCHERFBTCATEGORY/>
				<GSTCLASS/>
				<ISDEEMEDPOSITIVE>No</ISDEEMEDPOSITIVE>
				<LEDGERFROMITEM>No</LEDGERFROMITEM>
				<REMOVEZEROENTRIES>No</REMOVEZEROENTRIES>
				<ISPARTYLEDGER>No</ISPARTYLEDGER>
				<ISLASTDEEMEDPOSITIVE>No</ISLASTDEEMEDPOSITIVE>
				<SERVICETAXDETAILS.LIST></SERVICETAXDETAILS.LIST>
				<BANKALLOCATIONS.LIST></BANKALLOCATIONS.LIST>
				<BILLALLOCATIONS.LIST></BILLALLOCATIONS.LIST>
				<INTERESTCOLLECTION.LIST></INTERESTCOLLECTION.LIST>
				<OLDAUDITENTRIES.LIST></OLDAUDITENTRIES.LIST>
				<ACCOUNTAUDITENTRIES.LIST></ACCOUNTAUDITENTRIES.LIST>
				<AUDITENTRIES.LIST></AUDITENTRIES.LIST>
				<INPUTCRALLOCS.LIST></INPUTCRALLOCS.LIST>
				<INVENTORYALLOCATIONS.LIST></INVENTORYALLOCATIONS.LIST>
				<DUTYHEADDETAILS.LIST></DUTYHEADDETAILS.LIST>
				<EXCISEDUTYHEADDETAILS.LIST></EXCISEDUTYHEADDETAILS.LIST>
				<RATEDETAILS.LIST></RATEDETAILS.LIST>
				<SUMMARYALLOCS.LIST></SUMMARYALLOCS.LIST>
				<STPYMTDETAILS.LIST></STPYMTDETAILS.LIST>
				<EXCISEPAYMENTALLOCATIONS.LIST></EXCISEPAYMENTALLOCATIONS.LIST>
				<TAXBILLALLOCATIONS.LIST></TAXBILLALLOCATIONS.LIST>
				<TAXOBJECTALLOCATIONS.LIST></TAXOBJECTALLOCATIONS.LIST>
				<TDSEXPENSEALLOCATIONS.LIST></TDSEXPENSEALLOCATIONS.LIST>
				<VATSTATUTORYDETAILS.LIST></VATSTATUTORYDETAILS.LIST>
				<REFVOUCHERDETAILS.LIST></REFVOUCHERDETAILS.LIST>
				<INVOICEWISEDETAILS.LIST></INVOICEWISEDETAILS.LIST>
				<VATITCDETAILS.LIST></VATITCDETAILS.LIST>
				<ADVANCETAXDETAILS.LIST></ADVANCETAXDETAILS.LIST>
				</LEDGERENTRIES.LIST>
				<LEDGERENTRIES.LIST>
				<TAXCLASSIFICATIONNAME/>
				<LEDGERNAME>'.$ledgernamegst.'</LEDGERNAME>
				<METHODTYPE>As User Defined Value</METHODTYPE>
				<VOUCHERFBTCATEGORY/>
				<GSTCLASS/>
				<ISDEEMEDPOSITIVE>No</ISDEEMEDPOSITIVE>
				<LEDGERFROMITEM>No</LEDGERFROMITEM>
				<REMOVEZEROENTRIES>No</REMOVEZEROENTRIES>
				<ISPARTYLEDGER>No</ISPARTYLEDGER>
				<ISLASTDEEMEDPOSITIVE>No</ISLASTDEEMEDPOSITIVE>
				<SERVICETAXDETAILS.LIST></SERVICETAXDETAILS.LIST>
				<BANKALLOCATIONS.LIST></BANKALLOCATIONS.LIST>
				<BILLALLOCATIONS.LIST></BILLALLOCATIONS.LIST>
				<INTERESTCOLLECTION.LIST></INTERESTCOLLECTION.LIST>
				<OLDAUDITENTRIES.LIST></OLDAUDITENTRIES.LIST>
				<ACCOUNTAUDITENTRIES.LIST></ACCOUNTAUDITENTRIES.LIST>
				<AUDITENTRIES.LIST></AUDITENTRIES.LIST>
				<INPUTCRALLOCS.LIST></INPUTCRALLOCS.LIST>
				<INVENTORYALLOCATIONS.LIST></INVENTORYALLOCATIONS.LIST>
				<DUTYHEADDETAILS.LIST></DUTYHEADDETAILS.LIST>
				<EXCISEDUTYHEADDETAILS.LIST></EXCISEDUTYHEADDETAILS.LIST>
				<RATEDETAILS.LIST></RATEDETAILS.LIST>
				<SUMMARYALLOCS.LIST></SUMMARYALLOCS.LIST>
				<STPYMTDETAILS.LIST></STPYMTDETAILS.LIST>
				<EXCISEPAYMENTALLOCATIONS.LIST></EXCISEPAYMENTALLOCATIONS.LIST>
				<TAXBILLALLOCATIONS.LIST></TAXBILLALLOCATIONS.LIST>
				<TAXOBJECTALLOCATIONS.LIST></TAXOBJECTALLOCATIONS.LIST>
				<TDSEXPENSEALLOCATIONS.LIST></TDSEXPENSEALLOCATIONS.LIST>
				<VATSTATUTORYDETAILS.LIST></VATSTATUTORYDETAILS.LIST>
				<REFVOUCHERDETAILS.LIST></REFVOUCHERDETAILS.LIST>
				<INVOICEWISEDETAILS.LIST></INVOICEWISEDETAILS.LIST>
				<VATITCDETAILS.LIST></VATITCDETAILS.LIST>
				<ADVANCETAXDETAILS.LIST></ADVANCETAXDETAILS.LIST>
				</LEDGERENTRIES.LIST>';


	 
							 
	$requestXMLEND='					
<PAYROLLMODEOFPAYMENT.LIST>      </PAYROLLMODEOFPAYMENT.LIST>
      <ATTDRECORDS.LIST>      </ATTDRECORDS.LIST>
     </VOUCHER>
    </TALLYMESSAGE>

   </REQUESTDATA>
  </IMPORTDATA>
 </BODY>
</ENVELOPE>';


           }









                $itemcode=array();
                $itemsbsn=array();
			
              
				$whStkTrnsDetail = $this->_getSelectList('table_warehouse_stk_trns AS WHST',"*",''," WHST.bill_no='".$bill_no."' AND WHST.bill_date= '".$bill_date."' ");

				if(!(is_array($whStkTrnsDetail) && sizeof($whStkTrnsDetail)>0))
				{
					for($i=0;$i<sizeof($_SESSION['wh_transfer']);$i++)
					{
						$whSBNDetail = $this->_getSelectList('table_warehouse_stock AS WS LEFT JOIN table_item_bsn AS BSN ON BSN.bsn_id = WS.bsn_id left join table_item as ITEM on ITEM.item_id = BSN.item_id left join table_brands as BRND on BRND.brand_id = ITEM.brand_id left join table_category as CAT on CAT.category_id = ITEM.category_id',"WS.*, BSN.bsn,CAT.category_name,BRND.brand_name,ITEM.item_name,ITEM.item_code,ITEM.item_warranty,ITEM.item_prodata ",''," WS.warehouse_id ='".$warehouse_id."' AND BSN.bsn = '".$_SESSION['wh_transfer'][$i]['bsn_no']."' AND WS.status = 'A' AND datediff(NOW(),WS.dispatch_date) < 90");	
				
						if(isset($whSBNDetail) && sizeof($whSBNDetail)>0)
						{
							$transferBSNArray[$i]['item_id'] = $whSBNDetail[0]->item_id;
							$transferBSNArray[$i]['bsn_id'] = $whSBNDetail[0]->bsn_id; 
							$transferBSNArray[$i]['bsn'] = $whSBNDetail[0]->bsn;
							$transferBSNArray[$i]['ws_id'] = $whSBNDetail[0]->warehouse_stock_id;


                           
						}

                         $itemcode[]=$_SESSION['wh_transfer'][$i]['item_code'];

                          if(isset($itemsbsn[$_SESSION['wh_transfer'][$i]['item_code']]))
                       {
                          $count_bsn = (int)$itemsbsn[$_SESSION['wh_transfer'][$i]['item_code']]['count']+1;
                          $itemsbsn[$_SESSION['wh_transfer'][$i]['item_code']]['count'] = $count_bsn;
                
                       }
                          else
                       {
                          $itemsbsn[$_SESSION['wh_transfer'][$i]['item_code']]['count'] = 1; 
                          }


					}


					$itemnameis=array_unique($itemcode);

            $itemname= array_values(array_filter($itemnameis));

            for($j=0;$j<count($itemname);$j++)
            {
           
            $item_details=$this->_getSelectList2('table_item AS I',"item_name,item_division,item_id",''," I.item_code='".$itemname[$j]."'  ");
           // print_r($item_details[0]->item_name);exit;
            if($item_details[0]->item_division==3)
				        	{
                                $godown_name="CHN ". ucwords(strtolower($arr[1]))." Sales";
                              //  $godown_namereport="CHN ". ucwords(strtolower($arr1[1]))." Sales";
                                

				        	}else if($item_details[0]->item_division==1)
				        	{
				        		 $godown_name="AMPS ". ucwords(strtolower($arr[1]))." Sales";
				        		// $godown_namereport="AMPS ". ucwords(strtolower($arr1[1]))." Sales";

				        	}else if($item_details[0]->item_division==2)
				        	{
				        		 $godown_name="Inst ". ucwords(strtolower($arr[1]))." Sales";
				        		// $godown_namereport="Inst ". ucwords(strtolower($arr1[1]))." Sales";
				        	}





             $STOCKITEM=$item_details[0]->item_name;
			
	         $ACTUALQTY= $itemsbsn[$itemname[$j]]['count'];

	        $ALLINVENTORYENTRIESITEM[] ='
													<ALLINVENTORYENTRIES.LIST>
													<STOCKITEMNAME>'.$STOCKITEM.'</STOCKITEMNAME>
													<ISDEEMEDPOSITIVE>Yes</ISDEEMEDPOSITIVE>
													<ISLASTDEEMEDPOSITIVE>Yes</ISLASTDEEMEDPOSITIVE>
													<ISAUTONEGATE>No</ISAUTONEGATE>
													<ISCUSTOMSCLEARANCE>No</ISCUSTOMSCLEARANCE>
													<ISTRACKCOMPONENT>No</ISTRACKCOMPONENT>
													<ISTRACKPRODUCTION>No</ISTRACKPRODUCTION>
													<ISPRIMARYITEM>No</ISPRIMARYITEM>
													<ISSCRAP>No</ISSCRAP>
													<RATE></RATE>
													<AMOUNT></AMOUNT>
													<ACTUALQTY>'.$ACTUALQTY.' nos</ACTUALQTY>
													<BILLEDQTY>'.$ACTUALQTY.' nos</BILLEDQTY>
													<INCLVATRATE></INCLVATRATE>
													<BATCHALLOCATIONS.LIST>
													<GODOWNNAME>'.$godown_name.'</GODOWNNAME>
													<BATCHNAME>Primary Batch</BATCHNAME>
													<DESTINATIONGODOWNNAME>'.$godown_name.'</DESTINATIONGODOWNNAME>
													<INDENTNO/>
													<ORDERNO/>
													<TRACKINGNUMBER/>
													<DYNAMICCSTISCLEARED>No</DYNAMICCSTISCLEARED>
													<AMOUNT></AMOUNT>
													<ACTUALQTY>'.$ACTUALQTY.' nos</ACTUALQTY>
													<BILLEDQTY>'.$ACTUALQTY.' nos</BILLEDQTY>
													<INCLVATRATE></INCLVATRATE>
													<ADDITIONALDETAILS.LIST></ADDITIONALDETAILS.LIST>
													<VOUCHERCOMPONENTLIST.LIST></VOUCHERCOMPONENTLIST.LIST>
													</BATCHALLOCATIONS.LIST>
													<ACCOUNTINGALLOCATIONS.LIST>
													<OLDAUDITENTRYIDS.LIST TYPE="Number">
													<OLDAUDITENTRYIDS>-1</OLDAUDITENTRYIDS>
													</OLDAUDITENTRYIDS.LIST>
													<LEDGERNAME>'.$ledgername_report_from.'</LEDGERNAME>
													<GSTCLASS/>
													<ISDEEMEDPOSITIVE>Yes</ISDEEMEDPOSITIVE>
													<LEDGERFROMITEM>No</LEDGERFROMITEM>
													<REMOVEZEROENTRIES>No</REMOVEZEROENTRIES>
													<ISPARTYLEDGER>No</ISPARTYLEDGER>
													<ISLASTDEEMEDPOSITIVE>Yes</ISLASTDEEMEDPOSITIVE>
													<AMOUNT></AMOUNT>
													<SERVICETAXDETAILS.LIST></SERVICETAXDETAILS.LIST>
													<BANKALLOCATIONS.LIST></BANKALLOCATIONS.LIST>
													<BILLALLOCATIONS.LIST></BILLALLOCATIONS.LIST>
													<INTERESTCOLLECTION.LIST></INTERESTCOLLECTION.LIST>
													<OLDAUDITENTRIES.LIST></OLDAUDITENTRIES.LIST>
													<ACCOUNTAUDITENTRIES.LIST></ACCOUNTAUDITENTRIES.LIST>
													<AUDITENTRIES.LIST></AUDITENTRIES.LIST>
													<INPUTCRALLOCS.LIST></INPUTCRALLOCS.LIST>
													<DUTYHEADDETAILS.LIST></DUTYHEADDETAILS.LIST>
													<EXCISEDUTYHEADDETAILS.LIST></EXCISEDUTYHEADDETAILS.LIST>
													<RATEDETAILS.LIST></RATEDETAILS.LIST>
													<SUMMARYALLOCS.LIST></SUMMARYALLOCS.LIST>
													<STPYMTDETAILS.LIST></STPYMTDETAILS.LIST>
													<EXCISEPAYMENTALLOCATIONS.LIST></EXCISEPAYMENTALLOCATIONS.LIST>
													<TAXBILLALLOCATIONS.LIST></TAXBILLALLOCATIONS.LIST>
													<TAXOBJECTALLOCATIONS.LIST></TAXOBJECTALLOCATIONS.LIST>
													<TDSEXPENSEALLOCATIONS.LIST></TDSEXPENSEALLOCATIONS.LIST>
													<VATSTATUTORYDETAILS.LIST></VATSTATUTORYDETAILS.LIST>
													<COSTTRACKALLOCATIONS.LIST></COSTTRACKALLOCATIONS.LIST>
													<REFVOUCHERDETAILS.LIST></REFVOUCHERDETAILS.LIST>
													<INVOICEWISEDETAILS.LIST></INVOICEWISEDETAILS.LIST>
													<VATITCDETAILS.LIST></VATITCDETAILS.LIST>
													<ADVANCETAXDETAILS.LIST></ADVANCETAXDETAILS.LIST>
													</ACCOUNTINGALLOCATIONS.LIST>
													<DUTYHEADDETAILS.LIST></DUTYHEADDETAILS.LIST>
													<SUPPLEMENTARYDUTYHEADDETAILS.LIST></SUPPLEMENTARYDUTYHEADDETAILS.LIST>
													<TAXOBJECTALLOCATIONS.LIST></TAXOBJECTALLOCATIONS.LIST>
													<REFVOUCHERDETAILS.LIST></REFVOUCHERDETAILS.LIST>
													<EXCISEALLOCATIONS.LIST></EXCISEALLOCATIONS.LIST>
													<EXPENSEALLOCATIONS.LIST></EXPENSEALLOCATIONS.LIST>
													</ALLINVENTORYENTRIES.LIST>';


/*
 $ALLINVENTORYENTRIESITEMREPORT[] ='
													<ALLINVENTORYENTRIES.LIST>
													<STOCKITEMNAME>'.$STOCKITEM.'</STOCKITEMNAME>
													<ISDEEMEDPOSITIVE>Yes</ISDEEMEDPOSITIVE>
													<ISLASTDEEMEDPOSITIVE>Yes</ISLASTDEEMEDPOSITIVE>
													<ISAUTONEGATE>No</ISAUTONEGATE>
													<ISCUSTOMSCLEARANCE>No</ISCUSTOMSCLEARANCE>
													<ISTRACKCOMPONENT>No</ISTRACKCOMPONENT>
													<ISTRACKPRODUCTION>No</ISTRACKPRODUCTION>
													<ISPRIMARYITEM>No</ISPRIMARYITEM>
													<ISSCRAP>No</ISSCRAP>
													<RATE></RATE>
													<AMOUNT></AMOUNT>
													<ACTUALQTY>'.$ACTUALQTY.' nos</ACTUALQTY>
													<BILLEDQTY>'.$ACTUALQTY.' nos</BILLEDQTY>
													<INCLVATRATE></INCLVATRATE>
													<BATCHALLOCATIONS.LIST>
													<GODOWNNAME>'.$godown_namereport.'</GODOWNNAME>
													<BATCHNAME>Primary Batch</BATCHNAME>
													<DESTINATIONGODOWNNAME>'.$godown_namereport.'</DESTINATIONGODOWNNAME>
													<INDENTNO/>
													<ORDERNO/>
													<TRACKINGNUMBER/>
													<DYNAMICCSTISCLEARED>No</DYNAMICCSTISCLEARED>
													<AMOUNT></AMOUNT>
													<ACTUALQTY>'.$ACTUALQTY.' nos</ACTUALQTY>
													<BILLEDQTY>'.$ACTUALQTY.' nos</BILLEDQTY>
													<INCLVATRATE></INCLVATRATE>
													<ADDITIONALDETAILS.LIST></ADDITIONALDETAILS.LIST>
													<VOUCHERCOMPONENTLIST.LIST></VOUCHERCOMPONENTLIST.LIST>
													</BATCHALLOCATIONS.LIST>
													<ACCOUNTINGALLOCATIONS.LIST>
													<OLDAUDITENTRYIDS.LIST TYPE="Number">
													<OLDAUDITENTRYIDS>-1</OLDAUDITENTRYIDS>
													</OLDAUDITENTRYIDS.LIST>
													<LEDGERNAME>'.$ledgername_report.'</LEDGERNAME>
													<GSTCLASS/>
													<ISDEEMEDPOSITIVE>Yes</ISDEEMEDPOSITIVE>
													<LEDGERFROMITEM>No</LEDGERFROMITEM>
													<REMOVEZEROENTRIES>No</REMOVEZEROENTRIES>
													<ISPARTYLEDGER>No</ISPARTYLEDGER>
													<ISLASTDEEMEDPOSITIVE>Yes</ISLASTDEEMEDPOSITIVE>
													<AMOUNT></AMOUNT>
													<SERVICETAXDETAILS.LIST></SERVICETAXDETAILS.LIST>
													<BANKALLOCATIONS.LIST></BANKALLOCATIONS.LIST>
													<BILLALLOCATIONS.LIST></BILLALLOCATIONS.LIST>
													<INTERESTCOLLECTION.LIST></INTERESTCOLLECTION.LIST>
													<OLDAUDITENTRIES.LIST></OLDAUDITENTRIES.LIST>
													<ACCOUNTAUDITENTRIES.LIST></ACCOUNTAUDITENTRIES.LIST>
													<AUDITENTRIES.LIST></AUDITENTRIES.LIST>
													<INPUTCRALLOCS.LIST></INPUTCRALLOCS.LIST>
													<DUTYHEADDETAILS.LIST></DUTYHEADDETAILS.LIST>
													<EXCISEDUTYHEADDETAILS.LIST></EXCISEDUTYHEADDETAILS.LIST>
													<RATEDETAILS.LIST></RATEDETAILS.LIST>
													<SUMMARYALLOCS.LIST></SUMMARYALLOCS.LIST>
													<STPYMTDETAILS.LIST></STPYMTDETAILS.LIST>
													<EXCISEPAYMENTALLOCATIONS.LIST></EXCISEPAYMENTALLOCATIONS.LIST>
													<TAXBILLALLOCATIONS.LIST></TAXBILLALLOCATIONS.LIST>
													<TAXOBJECTALLOCATIONS.LIST></TAXOBJECTALLOCATIONS.LIST>
													<TDSEXPENSEALLOCATIONS.LIST></TDSEXPENSEALLOCATIONS.LIST>
													<VATSTATUTORYDETAILS.LIST></VATSTATUTORYDETAILS.LIST>
													<COSTTRACKALLOCATIONS.LIST></COSTTRACKALLOCATIONS.LIST>
													<REFVOUCHERDETAILS.LIST></REFVOUCHERDETAILS.LIST>
													<INVOICEWISEDETAILS.LIST></INVOICEWISEDETAILS.LIST>
													<VATITCDETAILS.LIST></VATITCDETAILS.LIST>
													<ADVANCETAXDETAILS.LIST></ADVANCETAXDETAILS.LIST>
													</ACCOUNTINGALLOCATIONS.LIST>
													<DUTYHEADDETAILS.LIST></DUTYHEADDETAILS.LIST>
													<SUPPLEMENTARYDUTYHEADDETAILS.LIST></SUPPLEMENTARYDUTYHEADDETAILS.LIST>
													<TAXOBJECTALLOCATIONS.LIST></TAXOBJECTALLOCATIONS.LIST>
													<REFVOUCHERDETAILS.LIST></REFVOUCHERDETAILS.LIST>
													<EXCISEALLOCATIONS.LIST></EXCISEALLOCATIONS.LIST>
													<EXPENSEALLOCATIONS.LIST></EXPENSEALLOCATIONS.LIST>
													</ALLINVENTORYENTRIES.LIST>';  */



              
          }

/*
            $server = 'http://quytech14.ddns.net:8000/';

            $requestXMLITEMLIST = implode(' ', $ALLINVENTORYENTRIESITEM);
			$requestXMLITEMLISTREPORT = implode(' ', $ALLINVENTORYENTRIESITEMREPORT);
			       
		    $requestXML = $requestXMLSTART.$requestXMLITEMLIST.$requestXMLEND;
		    $requestXMLREPORT = $requestXMLSTARTReport.$requestXMLITEMLISTREPORT.$requestXMLEND;
		    $first=array();
			$second=array();

//curl run for xml code commeted by arvind
 $header = array( "Content-type: text/xml" ,"Content-length: ".strlen($requestXML) ,"Connection: close" );
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $server);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_TIMEOUT, 100);
			curl_setopt($ch, CURLOPT_POST, true);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $requestXML);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
			$data = curl_exec($ch);
			//print_R($data);
           if(curl_errno($ch)){
		    echo json_encode(array('status'=>'Failed', 'data'=> print curl_error($ch)));

		    }else{

		    	   $p = xml_parser_create();
        xml_parse_into_struct($p, $data, $vals, $index);
       xml_parser_free($p);
		   if(isset($vals[$index['LASTVCHID'][0]]['value']) && $vals[$index['LASTVCHID'][0]]['value']>0) {

		    $tallymasterno=$vals[$index['LASTVCHID'][0]]['value'];
        //$attributeArray = array();
      //  $attributeArray['TALLYMASTERNO'] = $vals[$index['LASTVCHID'][0]]['value'];
        
        //$yu=$this->_dbUpdate2($attributeArray,'table_warehouse_stk_trn_dts', "transfer_id =".$transfer_id );  
//print_R($yu);exit;

       $first=array('status'=>'Push To Tally Successfully', 'data'=> " Push To Tally Successfully.");


            //echo json_encode(array('status'=>'Push To Tally Successfully', 'data'=> " Push To Tally Successfully.", 'id'=>$vals[$index['LASTVCHID'][0]]['value']));

    } else {
      
      if(isset($vals[$index['LINEERROR'][0]]['value'])) 
        $message = $vals[$index['LINEERROR'][0]]['value'];
      else 
        $message = $vals[0]['value'];


          $first = array('status'=>'Failed', 'data'=> $message);
    }


    curl_close($ch);

      

               } 
               
//$server = 'http://quytech14.ddns.net:8000/';
            $headers = array( "Content-type: text/xml" ,"Content-length: ".strlen($requestXMLREPORT) ,"Connection: close" );
			$ch1 = curl_init();
			curl_setopt($ch1, CURLOPT_URL, $server);
			curl_setopt($ch1, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch1, CURLOPT_TIMEOUT, 100);
			curl_setopt($ch1, CURLOPT_POST, true);
			curl_setopt($ch1, CURLOPT_POSTFIELDS, $requestXMLREPORT);
			curl_setopt($ch1, CURLOPT_HTTPHEADER, $headers);
			$data1 = curl_exec($ch1);
			//print_R($data1);
			 if(curl_errno($ch1)){
		    echo json_encode(array('status'=>'Failed', 'data'=> print curl_error($ch1)));

		    }else{

		    	   $p1 = xml_parser_create();
        xml_parse_into_struct($p1, $data1, $vals1, $index1);
       xml_parser_free($p1);
		   if(isset($vals1[$index1['LASTVCHID'][0]]['value']) && $vals1[$index1['LASTVCHID'][0]]['value']>0) {

		  $tallymasternoreport=$vals1[$index1['LASTVCHID'][0]]['value'];
        //$attributeArray = array();
       // $attributeArray['TALLYMASTERNOREPORT'] = $vals1[$index1['LASTVCHID'][0]]['value'];
        
        //$yu=$this->_dbUpdate2($attributeArray,'table_warehouse_stk_trn_dts', "transfer_id =".$transfer_id );  
//print_R($yu);exit;

     $second=array('status'=>'Push To Tally Successfully', 'data'=> " Push To Tally Successfully.");
          //  echo json_encode(array('status'=>'Push To Tally Successfully', 'data'=> " Push To Tally Successfully.", 'id'=>$vals1[$index1['LASTVCHID'][0]]['value']));
         // echo json_encode(array('status'=>'Push To Tally Successfully'));

    } else {
      
      if(isset($vals1[$index1['LINEERROR'][0]]['value'])) 
        $message = $vals1[$index1['LINEERROR'][0]]['value'];
      else 
        $message = $vals1[0]['value'];


          $second=array('status'=>'Failed', 'data'=> $message);
    }


    curl_close($ch1);

      

               } 

             $tallyretun=   json_encode(array("first"=>$first['data'], "second"=>$second['data']));
              
             //print_R($tallyretun);
             $jsonAsObject   = json_decode($tallyretun);
             $jsonAsArray    = json_decode($tallyretun, TRUE);
             $fromwh="From Warehouse:".$jsonAsObject->first;
             $towh="To Warehouse:".$jsonAsObject->second;
             $message=$fromwh.$towh;
	*/






					if(sizeof($transferBSNArray)>0)
					{
							// table_warehouse_stk_trns

							$data = array();

							$data['account_id'] 			=  $_SESSION['accountId'];	
							$data['from_warehouse_id'] 		=  $warehouse_id;
							$data['to_warehouse_id'] 		=  $to_warehouse_id;
							$data['bill_date'] 				=  $bill_date;
							$data['bill_no'] 				=  $bill_no;
							$data['user_type'] 		        =  $_SESSION['userLoginType'];	
							$data['web_user_id'] 		    =  $_SESSION['PepUpSalesUserId'];	
							$data['transfer_datetime'] 		=  date('Y-m-d H:i:s');	
							$data['created_date'] 		    =  date('Y-m-d');	
							$data['last_update_date'] 		=  date('Y-m-d');	
							$data['status']		            =  'A';

							$transfer_id = $this->_dbInsert($data,'table_warehouse_stk_trns');  

							for($j=0;$j<sizeof($transferBSNArray);$j++)
							{	

								// 	Record bsn transfer to warehouse

								$whStock = array();
								$whStock['transfer_id'] 			=  $transfer_id;
								$whStock['item_id'] 				=  $transferBSNArray[$j]['item_id'];
								$whStock['bsn_id'] 		    		=  $transferBSNArray[$j]['bsn_id'];
								$whStock['bsn']       				=  $transferBSNArray[$j]['bsn'];
								

								$stocktransfer=$this->_dbInsert($whStock,'table_warehouse_stk_trn_dts');  

                                  
								// Remove the bsn from warehouse and add into to warehouse
								$whStock = array();
								$whStock['warehouse_id']	 			=  $to_warehouse_id;
								$whStock['last_updated_date']	 		=  date('Y-m-d');	
								$whStock['last_update_datetime'] 		=  date('Y-m-d H:i:s');
								$whStock['transfer_type']				=  'W';		//W = transferred from warehouse
								$whStock['status']		            	=  'I';

								$this->_dbUpdate($whStock,'table_warehouse_stock', 'warehouse_stock_id="'.$transferBSNArray[$j]['ws_id'].'"');
								
							}

                             if($stocktransfer > 0)
                             {

                                  $server = 'http://quytech14.ddns.net:8000/';

            $requestXMLITEMLIST = implode(' ', $ALLINVENTORYENTRIESITEM);
			//$requestXMLITEMLISTREPORT = implode(' ', $ALLINVENTORYENTRIESITEMREPORT);
			       
		    $requestXML = $requestXMLSTART.$requestXMLITEMLIST.$requestXMLEND;
		   // $requestXMLREPORT = $requestXMLSTARTReport.$requestXMLITEMLISTREPORT.$requestXMLEND;
		    $first=array();
			//$second=array();

//curl run for xml code commeted by arvind
 $header = array( "Content-type: text/xml" ,"Content-length: ".strlen($requestXML) ,"Connection: close" );
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $server);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_TIMEOUT, 100);
			curl_setopt($ch, CURLOPT_POST, true);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $requestXML);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
			$data = curl_exec($ch);
			//print_R($data);
           if(curl_errno($ch)){
		    echo json_encode(array('status'=>'Failed', 'data'=> print curl_error($ch)));

		    }else{

		    	   $p = xml_parser_create();
        xml_parse_into_struct($p, $data, $vals, $index);
       xml_parser_free($p);
		   if(isset($vals[$index['LASTVCHID'][0]]['value']) && $vals[$index['LASTVCHID'][0]]['value']>0) {

		    $tallymasterno=$vals[$index['LASTVCHID'][0]]['value'];
        $attributeArray = array();
        $attributeArray['TALLYMASTERNO'] = $vals[$index['LASTVCHID'][0]]['value'];
        
        $yu=$this->_dbUpdate2($attributeArray,'table_warehouse_stk_trn_dts', "transfer_id =".$transfer_id );  
//print_R($yu);exit;

       $first=array('status'=>'Push To Tally Successfully', 'data'=> " Push To Tally Successfully.");


            //echo json_encode(array('status'=>'Push To Tally Successfully', 'data'=> " Push To Tally Successfully.", 'id'=>$vals[$index['LASTVCHID'][0]]['value']));

    } else {
      
      if(isset($vals[$index['LINEERROR'][0]]['value'])) 
        $message = $vals[$index['LINEERROR'][0]]['value'];
      else 
        $message = $vals[0]['value'];


          $first = array('status'=>'Failed', 'data'=> $message);
    }


    curl_close($ch);

      

               } 
               /*
//$server = 'http://quytech14.ddns.net:8000/';
            $headers = array( "Content-type: text/xml" ,"Content-length: ".strlen($requestXMLREPORT) ,"Connection: close" );
			$ch1 = curl_init();
			curl_setopt($ch1, CURLOPT_URL, $server);
			curl_setopt($ch1, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch1, CURLOPT_TIMEOUT, 100);
			curl_setopt($ch1, CURLOPT_POST, true);
			curl_setopt($ch1, CURLOPT_POSTFIELDS, $requestXMLREPORT);
			curl_setopt($ch1, CURLOPT_HTTPHEADER, $headers);
			$data1 = curl_exec($ch1);
			//print_R($data1);
			 if(curl_errno($ch1)){
		    echo json_encode(array('status'=>'Failed', 'data'=> print curl_error($ch1)));

		    }else{

		    	   $p1 = xml_parser_create();
        xml_parse_into_struct($p1, $data1, $vals1, $index1);
       xml_parser_free($p1);
		   if(isset($vals1[$index1['LASTVCHID'][0]]['value']) && $vals1[$index1['LASTVCHID'][0]]['value']>0) {

		  $tallymasternoreport=$vals1[$index1['LASTVCHID'][0]]['value'];
        //$attributeArray = array();
       // $attributeArray['TALLYMASTERNOREPORT'] = $vals1[$index1['LASTVCHID'][0]]['value'];
        
        //$yu=$this->_dbUpdate2($attributeArray,'table_warehouse_stk_trn_dts', "transfer_id =".$transfer_id );  
//print_R($yu);exit;

     $second=array('status'=>'Push To Tally Successfully', 'data'=> " Push To Tally Successfully.");
          //  echo json_encode(array('status'=>'Push To Tally Successfully', 'data'=> " Push To Tally Successfully.", 'id'=>$vals1[$index1['LASTVCHID'][0]]['value']));
         // echo json_encode(array('status'=>'Push To Tally Successfully'));

    } else {
      
      if(isset($vals1[$index1['LINEERROR'][0]]['value'])) 
        $message = $vals1[$index1['LINEERROR'][0]]['value'];
      else 
        $message = $vals1[0]['value'];


          $second=array('status'=>'Failed', 'data'=> $message);
    }


    curl_close($ch1);

      

               } */

             $tallyretun=   json_encode(array("first"=>$first['data']));
              
             //print_R($tallyretun);
             $jsonAsObject   = json_decode($tallyretun);
             $jsonAsArray    = json_decode($tallyretun, TRUE);
             $fromwh="From Warehouse:".$jsonAsObject->first;
             $towh="To Warehouse:".$jsonAsObject->second;
            // $message=$fromwh.$towh;
             $message=$jsonAsObject->first;
	

                             }




                           return array('status'=>'success', 'msg'=>'Stock transfer successfully','transferid'=>$message);
							//return array('status'=>'success', 'msg'=>'Stock transfer successfully');
						
					}
					
				}
				else
				{
					return array('status'=>'failed', 'msg'=>'Duplicate Dispatched No/Date!');
				}	
			}
			
		}


		public function warehouseTowarehousestally($bsnid_a)
		{
           
             $itemids=array();
 	$itemsbsn=array();
 	$bsn_name=array();
    $bsnnam=array();

    $implodebsn=implode("','",$bsnid_a);
    //print_R("'$implodebsn'");
//exit;
  for($i=0;$i<count($bsnid_a);$i++)
  {
  	//print_R($bsnid_a[$i]);exit;
      $bsnnam[]=$bsnid_a[$i];
  	  $item_details=$this->_getSelectList2('table_item_bsn AS Ib',"item_id",''," Ib.bsn='".$bsnid_a[$i]."'  ");
      $itemid=$item_details[0]->item_id;
     
      $itemids[]=$item_details[0]->item_id;
         if(isset($itemsbsn[$itemid]))
				{
					$count_bsn = (int)$itemsbsn[$itemid]['count']+1;
					$itemsbsn[$itemid]['count'] = $count_bsn;

					$bsn_name[$itemid][BSN]=$bsnid_a;
				}
					else
				{
					$itemsbsn[$itemid]['count'] = 1;	
					$bsn_name[$itemid][BSN]=$bsnid_a;
				}



  }
  //$bsnnum=implode("','",$bsnnam);
  $bsnnum = implode("', '", $bsnnam);

  
  $arraybsnid=array();
  $bsnnidd=$this->_getSelectList2('table_item_bsn AS Ib',"bsn_id",''," Ib.bsn In('".$bsnnum. "') ");
 
  foreach ($bsnnidd as $value) 
  {
    $arraybsnid[] = $value->bsn_id;
}

$itembsnid=implode(',',($arraybsnid));

        /*$where1="ib.bsn='".$auRec[0]->bsn."'";
        	$auRec=$this->_getSelectList2('table_item_bsn AS ib
	 		left join table_warehouse_stock AS ws on ws.bsn_id = ib.bsn_id 
	 		left join table_item AS i on i.item_id = ws.item_id ',"ib.*,ws.bsn_id as bsnid,i.item_name as item_name,i.item_division as item_division",'',$where1,'');*/

	 		// $wh_name=$this->_getSelectList2('table_warehouse AS w',"warehouse_name",''," w.warehouse_id='". $_SESSION['warehouseId']."'  ");
             $warehouse_name=$wh_name[0]->warehouse_name;
              
               

              $wh_name=$this->_getSelectList2('table_warehouse AS w',"warehouse_name,warehouse_address,warehouse_phone_no,state_id,city_id",''," w.warehouse_id='". $_SESSION['warehouseId']."'  ");
              $state_id=$this->_getSelectList2('state AS s',"state_name",''," s.state_id='".$wh_name[0]->state_id."'  ");
              $city_id=$this->_getSelectList2('city AS c',"city_name",''," c.city_id='".$wh_name[0]->city_id."'  ");
               $markup_value=$this->_getSelectList2('table_markup AS m',"price",''," m.warehouse_id='".$_SESSION['warehouseId']."'  ");
               $markuprate=$markup_value[0]->price;
               //$markuprate=14;
         
             $warehouse_name=$wh_name[0]->warehouse_name;
             $contact_no=$wh_name[0]->warehouse_phone_no;
             $warehouse_address=$wh_name[0]->warehouse_address;
             $state=$state_id[0]->state_name;
             $city=$city_id[0]->city_name;














	 		$STOCKITEM = htmlspecialchars($auRec[0]->item_name, ENT_QUOTES);
	 		//print_R($STOCKITEM);
	 		$wh_name=$warehouse_name;
            $arr = explode(' ',trim($wh_name));
             $challan=ucwords(strtolower($arr[1]));
            $voucher_type=ucwords(strtolower($arr[1]))." Receipt Note";
             //$ledgername="Eastman Auto &amp; Power limited-".$challan;
            $partyledger="Eastman Auto &amp; Power limited-".$challan;

            //$date=$auRec[0]->manufacture_date;
            $dateFormat1 = date('Ymd'); // 20171202
            $DATE = $dateFormat1;


             	
            
            $ledgername_report="Receipt Note ".ucwords(strtolower($arr[1]));
            $ledgername_report_from=ucwords(strtolower($arr[1]))." Branch Transfer";

            $ledgernameto="Eastman Auto &amp; Power limited-".$challanto;
           // $ledgername=$voucher_type." Stock Transfer Agnt F Form";
            $ledgernamegst=ucwords(strtolower($arr[1]))." GST Sales @ 18%";


           $requestXMLSTART='<ENVELOPE>
			<HEADER>
			<TALLYREQUEST>Import Data</TALLYREQUEST>
			</HEADER>
			<BODY>
			<IMPORTDATA>
			<REQUESTDESC>
			<REPORTNAME>Vouchers</REPORTNAME>
			<STATICVARIABLES>
			<SVCURRENTCOMPANY>EastmanLtd</SVCURRENTCOMPANY>
			</STATICVARIABLES>
			</REQUESTDESC>
			<REQUESTDATA>
			<TALLYMESSAGE xmlns:UDF="TallyUDF">

			<VOUCHER VCHTYPE="'.$voucher_type.'" ACTION="Create" OBJVIEW="Invoice Voucher View">

			 <ADDRESS.LIST TYPE="String">
            <ADDRESS>'.$warehouse_name.'</ADDRESS>
            <ADDRESS>'.$warehouse_address.'</ADDRESS>
            <ADDRESS>'.$city.' </ADDRESS>
            <ADDRESS> '.$state.'</ADDRESS>
            <ADDRESS>Mobile No'.$contact_no.'</ADDRESS>
            </ADDRESS.LIST>
			<BASICBUYERADDRESS.LIST TYPE="String">
			<BASICBUYERADDRESS>'.$warehouse_name.'</BASICBUYERADDRESS>
			<BASICBUYERADDRESS>'.$warehouse_address.'</BASICBUYERADDRESS>
			<BASICBUYERADDRESS>'.$city.'</BASICBUYERADDRESS>
			<BASICBUYERADDRESS>'.$state.'</BASICBUYERADDRESS>
			<BASICBUYERADDRESS>Mobile No:'.$contact_no.'</BASICBUYERADDRESS>

			</BASICBUYERADDRESS.LIST>
			<OLDAUDITENTRYIDS.LIST TYPE="Number">
			<OLDAUDITENTRYIDS>-1</OLDAUDITENTRYIDS>
			</OLDAUDITENTRYIDS.LIST>
			<DATE>'.$DATE.'</DATE>
			<REFERENCEDATE>'.$DATE.'</REFERENCEDATE>

			<GSTREGISTRATIONTYPE></GSTREGISTRATIONTYPE>
			<VATDEALERTYPE></VATDEALERTYPE>
			<TAXUNITNAME>Default Tax Unit</TAXUNITNAME>
			<PARTYNAME>Eastman Auto &amp; Power limited</PARTYNAME>
			<VOUCHERTYPENAME>'.$voucher_type.'</VOUCHERTYPENAME>
			<REFERENCE></REFERENCE>
			<VOUCHERNUMBER></VOUCHERNUMBER>
			<PARTYLEDGERNAME>'.$partyledger.'</PARTYLEDGERNAME>
			<BASICBASEPARTYNAME>'.$partyledger.'</BASICBASEPARTYNAME>
			<CSTFORMISSUETYPE/>
			<CSTFORMRECVTYPE/>
			<FBTPAYMENTTYPE>Default</FBTPAYMENTTYPE>
			<PERSISTEDVIEW>Invoice Voucher View</PERSISTEDVIEW>
			<BASICBUYERNAME>'.$partyledger.'</BASICBUYERNAME>
			<BASICDATETIMEOFINVOICE>3-Aug-2017 at 15:58</BASICDATETIMEOFINVOICE>
			<BASICDATETIMEOFREMOVAL>3-Aug-2017 at 15:58</BASICDATETIMEOFREMOVAL>
			<VCHGSTCLASS/>
			<ENTEREDBY></ENTEREDBY>
			<DIFFACTUALQTY>No</DIFFACTUALQTY>
			<ISMSTFROMSYNC>No</ISMSTFROMSYNC>
			<ASORIGINAL>No</ASORIGINAL>
			<AUDITED>No</AUDITED>
			<FORJOBCOSTING>No</FORJOBCOSTING>
			<ISOPTIONAL>No</ISOPTIONAL>
			<EFFECTIVEDATE>20170801</EFFECTIVEDATE>
			<USEFOREXCISE>No</USEFOREXCISE>
			<ISFORJOBWORKIN>No</ISFORJOBWORKIN>
			<ALLOWCONSUMPTION>No</ALLOWCONSUMPTION>
			<USEFORINTEREST>No</USEFORINTEREST>
			<USEFORGAINLOSS>No</USEFORGAINLOSS>
			<USEFORGODOWNTRANSFER>No</USEFORGODOWNTRANSFER>
			<USEFORCOMPOUND>No</USEFORCOMPOUND>
			<USEFORSERVICETAX>No</USEFORSERVICETAX>
			<ISEXCISEVOUCHER>No</ISEXCISEVOUCHER>
			<EXCISETAXOVERRIDE>No</EXCISETAXOVERRIDE>
			<USEFORTAXUNITTRANSFER>No</USEFORTAXUNITTRANSFER>
			<EXCISEOPENING>No</EXCISEOPENING>
			<USEFORFINALPRODUCTION>No</USEFORFINALPRODUCTION>
			<ISTDSOVERRIDDEN>No</ISTDSOVERRIDDEN>
			<ISTCSOVERRIDDEN>No</ISTCSOVERRIDDEN>
			<ISTDSTCSCASHVCH>No</ISTDSTCSCASHVCH>
			<INCLUDEADVPYMTVCH>No</INCLUDEADVPYMTVCH>
			<ISSUBWORKSCONTRACT>No</ISSUBWORKSCONTRACT>
			<ISVATOVERRIDDEN>No</ISVATOVERRIDDEN>
			<IGNOREORIGVCHDATE>No</IGNOREORIGVCHDATE>
			<ISSERVICETAXOVERRIDDEN>No</ISSERVICETAXOVERRIDDEN>
			<ISISDVOUCHER>No</ISISDVOUCHER>
			<ISEXCISEOVERRIDDEN>No</ISEXCISEOVERRIDDEN>
			<ISEXCISESUPPLYVCH>No</ISEXCISESUPPLYVCH>
			<ISGSTOVERRIDDEN>No</ISGSTOVERRIDDEN>
			<GSTNOTEXPORTED>No</GSTNOTEXPORTED>
			<ISVATPRINCIPALACCOUNT>No</ISVATPRINCIPALACCOUNT>
			<ISSHIPPINGWITHINSTATE>No</ISSHIPPINGWITHINSTATE>
			<ISCANCELLED>No</ISCANCELLED>
			<HASCASHFLOW>Yes</HASCASHFLOW>
			<ISPOSTDATED>No</ISPOSTDATED>
			<USETRACKINGNUMBER>No</USETRACKINGNUMBER>
			<ISINVOICE>Yes</ISINVOICE>
			<MFGJOURNAL>No</MFGJOURNAL>
			<HASDISCOUNTS>Yes</HASDISCOUNTS>
			<ASPAYSLIP>No</ASPAYSLIP>
			<ISCOSTCENTRE>No</ISCOSTCENTRE>
			<ISSTXNONREALIZEDVCH>No</ISSTXNONREALIZEDVCH>
			<ISEXCISEMANUFACTURERON>No</ISEXCISEMANUFACTURERON>
			<ISBLANKCHEQUE>No</ISBLANKCHEQUE>
			<ISVOID>No</ISVOID>
			<ISONHOLD>No</ISONHOLD>
			<ORDERLINESTATUS>No</ORDERLINESTATUS>
			<VATISAGNSTCANCSALES>No</VATISAGNSTCANCSALES>
			<VATISPURCEXEMPTED>No</VATISPURCEXEMPTED>
			<ISVATRESTAXINVOICE>No</ISVATRESTAXINVOICE>
			<VATISASSESABLECALCVCH>No</VATISASSESABLECALCVCH>
			<ISVATDUTYPAID>Yes</ISVATDUTYPAID>
			<ISDELIVERYSAMEASCONSIGNEE>No</ISDELIVERYSAMEASCONSIGNEE>
			<ISDISPATCHSAMEASCONSIGNOR>No</ISDISPATCHSAMEASCONSIGNOR>
			<ISDELETED>No</ISDELETED>
			<CHANGEVCHMODE>No</CHANGEVCHMODE>
			<ALTERID></ALTERID>
			<MASTERID></MASTERID>
			<VOUCHERKEY></VOUCHERKEY>
			<EXCLUDEDTAXATIONS.LIST></EXCLUDEDTAXATIONS.LIST>
			<OLDAUDITENTRIES.LIST></OLDAUDITENTRIES.LIST>
			<ACCOUNTAUDITENTRIES.LIST></ACCOUNTAUDITENTRIES.LIST>
			<AUDITENTRIES.LIST></AUDITENTRIES.LIST>
			<DUTYHEADDETAILS.LIST></DUTYHEADDETAILS.LIST>
			<SUPPLEMENTARYDUTYHEADDETAILS.LIST></SUPPLEMENTARYDUTYHEADDETAILS.LIST>
			<INVOICEDELNOTES.LIST></INVOICEDELNOTES.LIST>
			<INVOICEORDERLIST.LIST></INVOICEORDERLIST.LIST>
			<INVOICEINDENTLIST.LIST></INVOICEINDENTLIST.LIST>
			<ATTENDANCEENTRIES.LIST></ATTENDANCEENTRIES.LIST>
			<ORIGINVOICEDETAILS.LIST></ORIGINVOICEDETAILS.LIST>
			<INVOICEEXPORTLIST.LIST></INVOICEEXPORTLIST.LIST>
			<LEDGERENTRIES.LIST>
			<OLDAUDITENTRYIDS.LIST TYPE="Number">
			<OLDAUDITENTRYIDS>-1</OLDAUDITENTRYIDS>
			</OLDAUDITENTRYIDS.LIST>
			<LEDGERNAME>'.$ledgername_report.'</LEDGERNAME>
			<GSTCLASS/>
			<ISDEEMEDPOSITIVE>No</ISDEEMEDPOSITIVE>
			<LEDGERFROMITEM>No</LEDGERFROMITEM>
			<REMOVEZEROENTRIES>No</REMOVEZEROENTRIES>
			<ISPARTYLEDGER>Yes</ISPARTYLEDGER>
			<ISLASTDEEMEDPOSITIVE>No</ISLASTDEEMEDPOSITIVE>
			<AMOUNT></AMOUNT>
			<SERVICETAXDETAILS.LIST></SERVICETAXDETAILS.LIST>

			<BILLALLOCATIONS.LIST></BILLALLOCATIONS.LIST>
			<INTERESTCOLLECTION.LIST></INTERESTCOLLECTION.LIST>
			<OLDAUDITENTRIES.LIST></OLDAUDITENTRIES.LIST>
			<ACCOUNTAUDITENTRIES.LIST></ACCOUNTAUDITENTRIES.LIST>
			<AUDITENTRIES.LIST></AUDITENTRIES.LIST>
			<INPUTCRALLOCS.LIST></INPUTCRALLOCS.LIST>
			<DUTYHEADDETAILS.LIST></DUTYHEADDETAILS.LIST>
			<EXCISEDUTYHEADDETAILS.LIST></EXCISEDUTYHEADDETAILS.LIST>
			<RATEDETAILS.LIST></RATEDETAILS.LIST>
			<SUMMARYALLOCS.LIST></SUMMARYALLOCS.LIST>
			<STPYMTDETAILS.LIST></STPYMTDETAILS.LIST>
			<EXCISEPAYMENTALLOCATIONS.LIST></EXCISEPAYMENTALLOCATIONS.LIST>
			<TAXBILLALLOCATIONS.LIST></TAXBILLALLOCATIONS.LIST>
			<TAXOBJECTALLOCATIONS.LIST></TAXOBJECTALLOCATIONS.LIST>
			<TDSEXPENSEALLOCATIONS.LIST></TDSEXPENSEALLOCATIONS.LIST>
			<VATSTATUTORYDETAILS.LIST></VATSTATUTORYDETAILS.LIST>
			<COSTTRACKALLOCATIONS.LIST></COSTTRACKALLOCATIONS.LIST>
			<REFVOUCHERDETAILS.LIST></REFVOUCHERDETAILS.LIST>
			<INVOICEWISEDETAILS.LIST></INVOICEWISEDETAILS.LIST>
			<VATITCDETAILS.LIST></VATITCDETAILS.LIST>
			<ADVANCETAXDETAILS.LIST></ADVANCETAXDETAILS.LIST>
			</LEDGERENTRIES.LIST>';
			$requestXMLEND='					
<PAYROLLMODEOFPAYMENT.LIST>      </PAYROLLMODEOFPAYMENT.LIST>
      <ATTDRECORDS.LIST>      </ATTDRECORDS.LIST>
     </VOUCHER>
    </TALLYMESSAGE>

   </REQUESTDATA>
  </IMPORTDATA>
 </BODY>
</ENVELOPE>';





            $ALLINVENTORYENTRIESITEM=array();
            $itemnameis=array_unique($itemids);

            $itemname= array_values(array_filter($itemnameis));

            for($j=0;$j<count($itemname);$j++)
            {
               //$item_details=$this->_getSelectList2('table_item AS I',"item_name,item_division",''," I.item_id='".$itemname[$j]."'  ");


             $item_details=$this->_getSelectList2('table_item AS I left join table_price AS P ON P.item_id=I.item_id',"item_name,item_division,item_dp",''," I.item_id='".$itemname[$j]."'  ");
             $price=$item_details[0]->item_dp;


         
            $STOCKITEM = htmlspecialchars($item_details[0]->item_name, ENT_QUOTES);

            if($item_details[0]->item_division==3)
				{
		            $godown_name="CHN ". ucwords(strtolower($arr[1]))." Sales";
		                                  //print_R($godown_name);exit;

				}   else if($item_details[0]->item_division==1)
			    {
					$godown_name="AMPS ". ucwords(strtolower($arr[1]))." Sales";

				}   else if($item_details[0]->item_division==2)
				{
					$godown_name="Inst ". ucwords(strtolower($arr[1]))." Sales";
			    }
           

			        if(isset($itemsbsn[$itemname[$j]]))
			    {
	                $ACTUALQTY= $itemsbsn[$itemname[$j]]['count'];
	                $BASICUSERDESCRIPTION=$bsn_name[$itemname[$j]]['BSN'];

                     $amount=$ACTUALQTY * $price ;
	                $mamount= $amount * $markuprate/100;
	                $amount="-".$amount;
	                $totalamount=$mamount + $amount;
	                $totalamount= "-".$totalamount;



	               // echo"<pre>";
	                //print_R($BASICUSERDESCRIPTION);exit;
	                //$sd=implode(' ',$BASICUSERDESCRIPTION);

                }
              
//print_R($bsnnam);exit;

                           $ALLINVENTORYENTRIESITEM[] ='
														<ALLINVENTORYENTRIES.LIST>
													<STOCKITEMNAME>'.$STOCKITEM.'</STOCKITEMNAME>
													<ISDEEMEDPOSITIVE>Yes</ISDEEMEDPOSITIVE>
													<ISLASTDEEMEDPOSITIVE>Yes</ISLASTDEEMEDPOSITIVE>
													<ISAUTONEGATE>No</ISAUTONEGATE>
													<ISCUSTOMSCLEARANCE>No</ISCUSTOMSCLEARANCE>
													<ISTRACKCOMPONENT>No</ISTRACKCOMPONENT>
													<ISTRACKPRODUCTION>No</ISTRACKPRODUCTION>
													<ISPRIMARYITEM>No</ISPRIMARYITEM>
													<ISSCRAP>No</ISSCRAP>
													<RATE></RATE>
													<AMOUNT></AMOUNT>
													<ACTUALQTY>'.$ACTUALQTY.' nos</ACTUALQTY>
													<BILLEDQTY>'.$ACTUALQTY.' nos</BILLEDQTY>
													<INCLVATRATE></INCLVATRATE>
													<BATCHALLOCATIONS.LIST>
													<GODOWNNAME>'.$godown_name.'</GODOWNNAME>
													<BATCHNAME>Primary Batch</BATCHNAME>
													<DESTINATIONGODOWNNAME>'.$godown_name.'</DESTINATIONGODOWNNAME>
													<INDENTNO/>
													<ORDERNO/>
													<TRACKINGNUMBER/>
													<DYNAMICCSTISCLEARED>No</DYNAMICCSTISCLEARED>
													<AMOUNT></AMOUNT>
													<ACTUALQTY>'.$ACTUALQTY.' nos</ACTUALQTY>
													<BILLEDQTY>'.$ACTUALQTY.' nos</BILLEDQTY>
													<INCLVATRATE></INCLVATRATE>
													<ADDITIONALDETAILS.LIST></ADDITIONALDETAILS.LIST>
													<VOUCHERCOMPONENTLIST.LIST></VOUCHERCOMPONENTLIST.LIST>
													</BATCHALLOCATIONS.LIST>
													<ACCOUNTINGALLOCATIONS.LIST>
													<OLDAUDITENTRYIDS.LIST TYPE="Number">
													<OLDAUDITENTRYIDS>-1</OLDAUDITENTRYIDS>
													</OLDAUDITENTRYIDS.LIST>
													<LEDGERNAME>'.$ledgername_report.'</LEDGERNAME>
													<GSTCLASS/>
													<ISDEEMEDPOSITIVE>Yes</ISDEEMEDPOSITIVE>
													<LEDGERFROMITEM>No</LEDGERFROMITEM>
													<REMOVEZEROENTRIES>No</REMOVEZEROENTRIES>
													<ISPARTYLEDGER>No</ISPARTYLEDGER>
													<ISLASTDEEMEDPOSITIVE>Yes</ISLASTDEEMEDPOSITIVE>
													<AMOUNT></AMOUNT>
													<SERVICETAXDETAILS.LIST></SERVICETAXDETAILS.LIST>
													<BANKALLOCATIONS.LIST></BANKALLOCATIONS.LIST>
													<BILLALLOCATIONS.LIST></BILLALLOCATIONS.LIST>
													<INTERESTCOLLECTION.LIST></INTERESTCOLLECTION.LIST>
													<OLDAUDITENTRIES.LIST></OLDAUDITENTRIES.LIST>
													<ACCOUNTAUDITENTRIES.LIST></ACCOUNTAUDITENTRIES.LIST>
													<AUDITENTRIES.LIST></AUDITENTRIES.LIST>
													<INPUTCRALLOCS.LIST></INPUTCRALLOCS.LIST>
													<DUTYHEADDETAILS.LIST></DUTYHEADDETAILS.LIST>
													<EXCISEDUTYHEADDETAILS.LIST></EXCISEDUTYHEADDETAILS.LIST>
													<RATEDETAILS.LIST></RATEDETAILS.LIST>
													<SUMMARYALLOCS.LIST></SUMMARYALLOCS.LIST>
													<STPYMTDETAILS.LIST></STPYMTDETAILS.LIST>
													<EXCISEPAYMENTALLOCATIONS.LIST></EXCISEPAYMENTALLOCATIONS.LIST>
													<TAXBILLALLOCATIONS.LIST></TAXBILLALLOCATIONS.LIST>
													<TAXOBJECTALLOCATIONS.LIST></TAXOBJECTALLOCATIONS.LIST>
													<TDSEXPENSEALLOCATIONS.LIST></TDSEXPENSEALLOCATIONS.LIST>
													<VATSTATUTORYDETAILS.LIST></VATSTATUTORYDETAILS.LIST>
													<COSTTRACKALLOCATIONS.LIST></COSTTRACKALLOCATIONS.LIST>
													<REFVOUCHERDETAILS.LIST></REFVOUCHERDETAILS.LIST>
													<INVOICEWISEDETAILS.LIST></INVOICEWISEDETAILS.LIST>
													<VATITCDETAILS.LIST></VATITCDETAILS.LIST>
													<ADVANCETAXDETAILS.LIST></ADVANCETAXDETAILS.LIST>
													</ACCOUNTINGALLOCATIONS.LIST>
													<DUTYHEADDETAILS.LIST></DUTYHEADDETAILS.LIST>
													<SUPPLEMENTARYDUTYHEADDETAILS.LIST></SUPPLEMENTARYDUTYHEADDETAILS.LIST>
													<TAXOBJECTALLOCATIONS.LIST></TAXOBJECTALLOCATIONS.LIST>
													<REFVOUCHERDETAILS.LIST></REFVOUCHERDETAILS.LIST>
													<EXCISEALLOCATIONS.LIST></EXCISEALLOCATIONS.LIST>
													<EXPENSEALLOCATIONS.LIST></EXPENSEALLOCATIONS.LIST>
													</ALLINVENTORYENTRIES.LIST>';






            }
                  $requestXMLITEMLIST = implode(' ', $ALLINVENTORYENTRIESITEM);
			       
		    $requestXML = $requestXMLSTART.$requestXMLITEMLIST.$requestXMLEND;
		    //echo"<pre>";
		    //print_R($requestXML);exit;
				$first=array();
			//$server = 'http://quytech14.ddns.net:8000/';
			$headers = array( "Content-type: text/xml" ,"Content-length: ".strlen($requestXML) ,"Connection: close" );
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, TALLYIP);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_TIMEOUT, 100);
			curl_setopt($ch, CURLOPT_POST, true);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $requestXML);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			$data = curl_exec($ch);
		//	echo"<pre>";
		//print_R($data);
			//exit;
		    if(curl_errno($ch)){
		    echo json_encode(array('status'=>'Failed', 'data'=> print curl_error($ch)));
		   // echo "  something went wrong..... try later";
		    }else{

		    	   $p = xml_parser_create();
        xml_parse_into_struct($p, $data, $vals, $index);
       xml_parser_free($p);
		   if(isset($vals[$index['LASTVCHID'][0]]['value']) && $vals[$index['LASTVCHID'][0]]['value']>0) {

		   
        $attributeArray = array();
       
         $attributeArray['TALLYMASTERNOREPORT'] = $vals[$index['LASTVCHID'][0]]['value'];
        
        //$yu=$this->_dbUpdate2($attributeArray,'table_warehouse_stk_trn_dts', "transfer_id =".$transfer_id );  
        
        $this->_dbUpdate2($attributeArray,'table_warehouse_stk_trn_dts', " bsn IN("."'$implodebsn'".")" );   

        //$resultSet = getOrderNoFromSalesOrder ($attributeArray['LASTVCHID']) ;


            // if($resultSet['status'] == 'Failed'){  // Save LastVCHID ONlY
            //   $TALLYORDERNO = 0;
            //   $_objAdmin->_dbUpdate($attributeArray,'table_order', " order_id =".$_REQUEST['id']);   

            // } else { // Save Order No And LastVCHID
             // $TALLYORDERNO = $resultSet['TALLYORDERNO'];
              
          
            //}

            $first=array('status'=>'Pushed To Tally Successfully', 'data'=> " Pushed To Tally Successfully.", 'id'=>$vals[$index['LASTVCHID'][0]]['value']);

    } else {
      
      if(isset($vals[$index['LINEERROR'][0]]['value'])) 
        $message = $vals[$index['LINEERROR'][0]]['value'];
      else 
        $message = $vals[0]['value'];


         $first=array('status'=>$message, 'data'=> $message);
    }


    curl_close($ch);
         
        $tallyretun=   json_encode(array("first"=>$first['data']));
              
             //print_R($tallyretun);
             $jsonAsObject   = json_decode($tallyretun);
            
            // $message=$fromwh.$towh;
             $message=$jsonAsObject->first;
      

               }

         return $message;






		}


		public function warehouseTowarehousestallyDMG($bsnid_a)
		{
              $itemids=array();
 	$itemsbsn=array();
 	$bsn_name=array();
    $bsnnam=array();

    $implodebsn=implode("','",$bsnid_a);

  for($i=0;$i<count($bsnid_a);$i++)
  {
  	//print_R($bsnid_a[$i]);exit;
      $bsnnam[]=$bsnid_a[$i];
  	  $item_details=$this->_getSelectList2('table_item_bsn AS Ib',"item_id",''," Ib.bsn='".$bsnid_a[$i]."'  ");
      $itemid=$item_details[0]->item_id;
     
      $itemids[]=$item_details[0]->item_id;
         if(isset($itemsbsn[$itemid]))
				{
					$count_bsn = (int)$itemsbsn[$itemid]['count']+1;
					$itemsbsn[$itemid]['count'] = $count_bsn;

					$bsn_name[$itemid][BSN]=$bsnid_a;
				}
					else
				{
					$itemsbsn[$itemid]['count'] = 1;	
					$bsn_name[$itemid][BSN]=$bsnid_a;
				}



  }
  //$bsnnum=implode("','",$bsnnam);
  $bsnnum = implode("', '", $bsnnam);

  
  $arraybsnid=array();
  $bsnnidd=$this->_getSelectList2('table_item_bsn AS Ib',"bsn_id",''," Ib.bsn In('".$bsnnum. "') ");
 
  foreach ($bsnnidd as $value) 
  {
    $arraybsnid[] = $value->bsn_id;
}

$itembsnid=implode(',',($arraybsnid));

        /*$where1="ib.bsn='".$auRec[0]->bsn."'";
        	$auRec=$this->_getSelectList2('table_item_bsn AS ib
	 		left join table_warehouse_stock AS ws on ws.bsn_id = ib.bsn_id 
	 		left join table_item AS i on i.item_id = ws.item_id ',"ib.*,ws.bsn_id as bsnid,i.item_name as item_name,i.item_division as item_division",'',$where1,'');*/

	 		 $wh_name=$this->_getSelectList2('table_warehouse AS w',"warehouse_name",''," w.warehouse_id='". $_SESSION['warehouseId']."'  ");
             $warehouse_name=$wh_name[0]->warehouse_name;
            
	 		//$STOCKITEM = htmlspecialchars($auRec[0]->item_name, ENT_QUOTES);
	 		//print_R($STOCKITEM);
	 		$wh_name=$warehouse_name;
            $arr = explode(' ',trim($wh_name));
             $challan=ucwords(strtolower($arr[1]));
            $voucher_type=ucwords(strtolower($arr[1]))." Receipt Note";
             //$ledgername="Eastman Auto &amp; Power limited-".$challan;
            $partyledger="Eastman Auto &amp; Power limited-".$challan;

            //$date=$auRec[0]->manufacture_date;
            $dateFormat1 = date('Ymd'); // 20171202
            $DATE = $dateFormat1;


             	
            
            $ledgername_report="Receipt Note ".ucwords(strtolower($arr[1]));
            $ledgername_report_from=ucwords(strtolower($arr[1]))." Branch Transfer";

            $ledgernameto="Eastman Auto &amp; Power limited-".$challanto;
           // $ledgername=$voucher_type." Stock Transfer Agnt F Form";
            $ledgernamegst=ucwords(strtolower($arr[1]))." GST Sales @ 18%";


           $requestXMLSTART='<ENVELOPE>
			<HEADER>
			<TALLYREQUEST>Import Data</TALLYREQUEST>
			</HEADER>
			<BODY>
			<IMPORTDATA>
			<REQUESTDESC>
			<REPORTNAME>Vouchers</REPORTNAME>
			<STATICVARIABLES>
			<SVCURRENTCOMPANY>EastmanLtd</SVCURRENTCOMPANY>
			</STATICVARIABLES>
			</REQUESTDESC>
			<REQUESTDATA>
			<TALLYMESSAGE xmlns:UDF="TallyUDF">

			<VOUCHER VCHTYPE="'.$voucher_type.'" ACTION="Create" OBJVIEW="Invoice Voucher View">

			<BASICBUYERADDRESS.LIST TYPE="String">
			<BASICBUYERADDRESS>Flat No. 101, First Floor</BASICBUYERADDRESS>
			<BASICBUYERADDRESS>1, Community Centre,</BASICBUYERADDRESS>
			<BASICBUYERADDRESS>Nariana Industrial Area</BASICBUYERADDRESS>
			<BASICBUYERADDRESS>Phase-1, New Delhi-110028</BASICBUYERADDRESS>

			</BASICBUYERADDRESS.LIST>
			<OLDAUDITENTRYIDS.LIST TYPE="Number">
			<OLDAUDITENTRYIDS>-1</OLDAUDITENTRYIDS>
			</OLDAUDITENTRYIDS.LIST>
			<DATE>'.$DATE.'</DATE>
			<REFERENCEDATE>'.$DATE.'</REFERENCEDATE>

			<GSTREGISTRATIONTYPE></GSTREGISTRATIONTYPE>
			<VATDEALERTYPE></VATDEALERTYPE>
			<TAXUNITNAME>Default Tax Unit</TAXUNITNAME>
			<PARTYNAME>Eastman Auto &amp; Power limited</PARTYNAME>
			<VOUCHERTYPENAME>'.$voucher_type.'</VOUCHERTYPENAME>
			<REFERENCE></REFERENCE>
			<VOUCHERNUMBER></VOUCHERNUMBER>
			<PARTYLEDGERNAME>'.$partyledger.'</PARTYLEDGERNAME>
			<BASICBASEPARTYNAME>'.$partyledger.'</BASICBASEPARTYNAME>
			<CSTFORMISSUETYPE/>
			<CSTFORMRECVTYPE/>
			<FBTPAYMENTTYPE>Default</FBTPAYMENTTYPE>
			<PERSISTEDVIEW>Invoice Voucher View</PERSISTEDVIEW>
			<BASICBUYERNAME>EastmanLtd</BASICBUYERNAME>
			<BASICDATETIMEOFINVOICE>3-Aug-2017 at 15:58</BASICDATETIMEOFINVOICE>
			<BASICDATETIMEOFREMOVAL>3-Aug-2017 at 15:58</BASICDATETIMEOFREMOVAL>
			<VCHGSTCLASS/>
			<ENTEREDBY></ENTEREDBY>
			<DIFFACTUALQTY>No</DIFFACTUALQTY>
			<ISMSTFROMSYNC>No</ISMSTFROMSYNC>
			<ASORIGINAL>No</ASORIGINAL>
			<AUDITED>No</AUDITED>
			<FORJOBCOSTING>No</FORJOBCOSTING>
			<ISOPTIONAL>No</ISOPTIONAL>
			<EFFECTIVEDATE>20170801</EFFECTIVEDATE>
			<USEFOREXCISE>No</USEFOREXCISE>
			<ISFORJOBWORKIN>No</ISFORJOBWORKIN>
			<ALLOWCONSUMPTION>No</ALLOWCONSUMPTION>
			<USEFORINTEREST>No</USEFORINTEREST>
			<USEFORGAINLOSS>No</USEFORGAINLOSS>
			<USEFORGODOWNTRANSFER>No</USEFORGODOWNTRANSFER>
			<USEFORCOMPOUND>No</USEFORCOMPOUND>
			<USEFORSERVICETAX>No</USEFORSERVICETAX>
			<ISEXCISEVOUCHER>No</ISEXCISEVOUCHER>
			<EXCISETAXOVERRIDE>No</EXCISETAXOVERRIDE>
			<USEFORTAXUNITTRANSFER>No</USEFORTAXUNITTRANSFER>
			<EXCISEOPENING>No</EXCISEOPENING>
			<USEFORFINALPRODUCTION>No</USEFORFINALPRODUCTION>
			<ISTDSOVERRIDDEN>No</ISTDSOVERRIDDEN>
			<ISTCSOVERRIDDEN>No</ISTCSOVERRIDDEN>
			<ISTDSTCSCASHVCH>No</ISTDSTCSCASHVCH>
			<INCLUDEADVPYMTVCH>No</INCLUDEADVPYMTVCH>
			<ISSUBWORKSCONTRACT>No</ISSUBWORKSCONTRACT>
			<ISVATOVERRIDDEN>No</ISVATOVERRIDDEN>
			<IGNOREORIGVCHDATE>No</IGNOREORIGVCHDATE>
			<ISSERVICETAXOVERRIDDEN>No</ISSERVICETAXOVERRIDDEN>
			<ISISDVOUCHER>No</ISISDVOUCHER>
			<ISEXCISEOVERRIDDEN>No</ISEXCISEOVERRIDDEN>
			<ISEXCISESUPPLYVCH>No</ISEXCISESUPPLYVCH>
			<ISGSTOVERRIDDEN>No</ISGSTOVERRIDDEN>
			<GSTNOTEXPORTED>No</GSTNOTEXPORTED>
			<ISVATPRINCIPALACCOUNT>No</ISVATPRINCIPALACCOUNT>
			<ISSHIPPINGWITHINSTATE>No</ISSHIPPINGWITHINSTATE>
			<ISCANCELLED>No</ISCANCELLED>
			<HASCASHFLOW>Yes</HASCASHFLOW>
			<ISPOSTDATED>No</ISPOSTDATED>
			<USETRACKINGNUMBER>No</USETRACKINGNUMBER>
			<ISINVOICE>Yes</ISINVOICE>
			<MFGJOURNAL>No</MFGJOURNAL>
			<HASDISCOUNTS>Yes</HASDISCOUNTS>
			<ASPAYSLIP>No</ASPAYSLIP>
			<ISCOSTCENTRE>No</ISCOSTCENTRE>
			<ISSTXNONREALIZEDVCH>No</ISSTXNONREALIZEDVCH>
			<ISEXCISEMANUFACTURERON>No</ISEXCISEMANUFACTURERON>
			<ISBLANKCHEQUE>No</ISBLANKCHEQUE>
			<ISVOID>No</ISVOID>
			<ISONHOLD>No</ISONHOLD>
			<ORDERLINESTATUS>No</ORDERLINESTATUS>
			<VATISAGNSTCANCSALES>No</VATISAGNSTCANCSALES>
			<VATISPURCEXEMPTED>No</VATISPURCEXEMPTED>
			<ISVATRESTAXINVOICE>No</ISVATRESTAXINVOICE>
			<VATISASSESABLECALCVCH>No</VATISASSESABLECALCVCH>
			<ISVATDUTYPAID>Yes</ISVATDUTYPAID>
			<ISDELIVERYSAMEASCONSIGNEE>No</ISDELIVERYSAMEASCONSIGNEE>
			<ISDISPATCHSAMEASCONSIGNOR>No</ISDISPATCHSAMEASCONSIGNOR>
			<ISDELETED>No</ISDELETED>
			<CHANGEVCHMODE>No</CHANGEVCHMODE>
			<ALTERID></ALTERID>
			<MASTERID></MASTERID>
			<VOUCHERKEY></VOUCHERKEY>
			<EXCLUDEDTAXATIONS.LIST></EXCLUDEDTAXATIONS.LIST>
			<OLDAUDITENTRIES.LIST></OLDAUDITENTRIES.LIST>
			<ACCOUNTAUDITENTRIES.LIST></ACCOUNTAUDITENTRIES.LIST>
			<AUDITENTRIES.LIST></AUDITENTRIES.LIST>
			<DUTYHEADDETAILS.LIST></DUTYHEADDETAILS.LIST>
			<SUPPLEMENTARYDUTYHEADDETAILS.LIST></SUPPLEMENTARYDUTYHEADDETAILS.LIST>
			<INVOICEDELNOTES.LIST></INVOICEDELNOTES.LIST>
			<INVOICEORDERLIST.LIST></INVOICEORDERLIST.LIST>
			<INVOICEINDENTLIST.LIST></INVOICEINDENTLIST.LIST>
			<ATTENDANCEENTRIES.LIST></ATTENDANCEENTRIES.LIST>
			<ORIGINVOICEDETAILS.LIST></ORIGINVOICEDETAILS.LIST>
			<INVOICEEXPORTLIST.LIST></INVOICEEXPORTLIST.LIST>
			<LEDGERENTRIES.LIST>
			<OLDAUDITENTRYIDS.LIST TYPE="Number">
			<OLDAUDITENTRYIDS>-1</OLDAUDITENTRYIDS>
			</OLDAUDITENTRYIDS.LIST>
			<LEDGERNAME>'.$ledgername_report.'</LEDGERNAME>
			<GSTCLASS/>
			<ISDEEMEDPOSITIVE>No</ISDEEMEDPOSITIVE>
			<LEDGERFROMITEM>No</LEDGERFROMITEM>
			<REMOVEZEROENTRIES>No</REMOVEZEROENTRIES>
			<ISPARTYLEDGER>Yes</ISPARTYLEDGER>
			<ISLASTDEEMEDPOSITIVE>No</ISLASTDEEMEDPOSITIVE>
			<AMOUNT></AMOUNT>
			<SERVICETAXDETAILS.LIST></SERVICETAXDETAILS.LIST>

			<BILLALLOCATIONS.LIST></BILLALLOCATIONS.LIST>
			<INTERESTCOLLECTION.LIST></INTERESTCOLLECTION.LIST>
			<OLDAUDITENTRIES.LIST></OLDAUDITENTRIES.LIST>
			<ACCOUNTAUDITENTRIES.LIST></ACCOUNTAUDITENTRIES.LIST>
			<AUDITENTRIES.LIST></AUDITENTRIES.LIST>
			<INPUTCRALLOCS.LIST></INPUTCRALLOCS.LIST>
			<DUTYHEADDETAILS.LIST></DUTYHEADDETAILS.LIST>
			<EXCISEDUTYHEADDETAILS.LIST></EXCISEDUTYHEADDETAILS.LIST>
			<RATEDETAILS.LIST></RATEDETAILS.LIST>
			<SUMMARYALLOCS.LIST></SUMMARYALLOCS.LIST>
			<STPYMTDETAILS.LIST></STPYMTDETAILS.LIST>
			<EXCISEPAYMENTALLOCATIONS.LIST></EXCISEPAYMENTALLOCATIONS.LIST>
			<TAXBILLALLOCATIONS.LIST></TAXBILLALLOCATIONS.LIST>
			<TAXOBJECTALLOCATIONS.LIST></TAXOBJECTALLOCATIONS.LIST>
			<TDSEXPENSEALLOCATIONS.LIST></TDSEXPENSEALLOCATIONS.LIST>
			<VATSTATUTORYDETAILS.LIST></VATSTATUTORYDETAILS.LIST>
			<COSTTRACKALLOCATIONS.LIST></COSTTRACKALLOCATIONS.LIST>
			<REFVOUCHERDETAILS.LIST></REFVOUCHERDETAILS.LIST>
			<INVOICEWISEDETAILS.LIST></INVOICEWISEDETAILS.LIST>
			<VATITCDETAILS.LIST></VATITCDETAILS.LIST>
			<ADVANCETAXDETAILS.LIST></ADVANCETAXDETAILS.LIST>
			</LEDGERENTRIES.LIST>';
			$requestXMLEND='					
<PAYROLLMODEOFPAYMENT.LIST>      </PAYROLLMODEOFPAYMENT.LIST>
      <ATTDRECORDS.LIST>      </ATTDRECORDS.LIST>
     </VOUCHER>
    </TALLYMESSAGE>

   </REQUESTDATA>
  </IMPORTDATA>
 </BODY>
</ENVELOPE>';





            $ALLINVENTORYENTRIESITEM=array();
            $itemnameis=array_unique($itemids);

            $itemname= array_values(array_filter($itemnameis));

            for($j=0;$j<count($itemname);$j++)
            {
               $item_details=$this->_getSelectList2('table_item AS I',"item_name,item_division",''," I.item_id='".$itemname[$j]."'  ");
           // print_r($item_details[0]->item_name);exit;
            //$STOCKITEM=$item_details[0]->item_name;
               $STOCKITEM="Def.(Scrap)".$item_details[0]->item_name;
            //$STOCKITEM = htmlspecialchars($item_details[0]->item_name, ENT_QUOTES);

            if($item_details[0]->item_division==3)
				{
		            $godown_name="CHN ". ucwords(strtolower($arr[1]))." Defective";
		                                  //print_R($godown_name);exit;

				}   else if($item_details[0]->item_division==1)
			    {
					$godown_name="AMPS ". ucwords(strtolower($arr[1]))." Defective";

				}   else if($item_details[0]->item_division==2)
				{
					$godown_name="Inst ". ucwords(strtolower($arr[1]))." Defective";
			    }
           

			        if(isset($itemsbsn[$itemname[$j]]))
			    {
	                $ACTUALQTY= $itemsbsn[$itemname[$j]]['count'];
	                $BASICUSERDESCRIPTION=$bsn_name[$itemname[$j]]['BSN'];
	               // echo"<pre>";
	                //print_R($BASICUSERDESCRIPTION);exit;
	                //$sd=implode(' ',$BASICUSERDESCRIPTION);

                }
              
//print_R($bsnnam);exit;

                           $ALLINVENTORYENTRIESITEM[] ='
														<ALLINVENTORYENTRIES.LIST>
													<STOCKITEMNAME>'.$STOCKITEM.'</STOCKITEMNAME>
													<ISDEEMEDPOSITIVE>Yes</ISDEEMEDPOSITIVE>
													<ISLASTDEEMEDPOSITIVE>Yes</ISLASTDEEMEDPOSITIVE>
													<ISAUTONEGATE>No</ISAUTONEGATE>
													<ISCUSTOMSCLEARANCE>No</ISCUSTOMSCLEARANCE>
													<ISTRACKCOMPONENT>No</ISTRACKCOMPONENT>
													<ISTRACKPRODUCTION>No</ISTRACKPRODUCTION>
													<ISPRIMARYITEM>No</ISPRIMARYITEM>
													<ISSCRAP>No</ISSCRAP>
													<RATE></RATE>
													<AMOUNT></AMOUNT>
													<ACTUALQTY>'.$ACTUALQTY.' nos</ACTUALQTY>
													<BILLEDQTY>'.$ACTUALQTY.' nos</BILLEDQTY>
													<INCLVATRATE></INCLVATRATE>
													<BATCHALLOCATIONS.LIST>
													<GODOWNNAME>'.$godown_name.'</GODOWNNAME>
													<BATCHNAME>Primary Batch</BATCHNAME>
													<DESTINATIONGODOWNNAME>'.$godown_name.'</DESTINATIONGODOWNNAME>
													<INDENTNO/>
													<ORDERNO/>
													<TRACKINGNUMBER/>
													<DYNAMICCSTISCLEARED>No</DYNAMICCSTISCLEARED>
													<AMOUNT></AMOUNT>
													<ACTUALQTY>'.$ACTUALQTY.' nos</ACTUALQTY>
													<BILLEDQTY>'.$ACTUALQTY.' nos</BILLEDQTY>
													<INCLVATRATE></INCLVATRATE>
													<ADDITIONALDETAILS.LIST></ADDITIONALDETAILS.LIST>
													<VOUCHERCOMPONENTLIST.LIST></VOUCHERCOMPONENTLIST.LIST>
													</BATCHALLOCATIONS.LIST>
													<ACCOUNTINGALLOCATIONS.LIST>
													<OLDAUDITENTRYIDS.LIST TYPE="Number">
													<OLDAUDITENTRYIDS>-1</OLDAUDITENTRYIDS>
													</OLDAUDITENTRYIDS.LIST>
													<LEDGERNAME>'.$ledgername_report.'</LEDGERNAME>
													<GSTCLASS/>
													<ISDEEMEDPOSITIVE>Yes</ISDEEMEDPOSITIVE>
													<LEDGERFROMITEM>No</LEDGERFROMITEM>
													<REMOVEZEROENTRIES>No</REMOVEZEROENTRIES>
													<ISPARTYLEDGER>No</ISPARTYLEDGER>
													<ISLASTDEEMEDPOSITIVE>Yes</ISLASTDEEMEDPOSITIVE>
													<AMOUNT></AMOUNT>
													<SERVICETAXDETAILS.LIST></SERVICETAXDETAILS.LIST>
													<BANKALLOCATIONS.LIST></BANKALLOCATIONS.LIST>
													<BILLALLOCATIONS.LIST></BILLALLOCATIONS.LIST>
													<INTERESTCOLLECTION.LIST></INTERESTCOLLECTION.LIST>
													<OLDAUDITENTRIES.LIST></OLDAUDITENTRIES.LIST>
													<ACCOUNTAUDITENTRIES.LIST></ACCOUNTAUDITENTRIES.LIST>
													<AUDITENTRIES.LIST></AUDITENTRIES.LIST>
													<INPUTCRALLOCS.LIST></INPUTCRALLOCS.LIST>
													<DUTYHEADDETAILS.LIST></DUTYHEADDETAILS.LIST>
													<EXCISEDUTYHEADDETAILS.LIST></EXCISEDUTYHEADDETAILS.LIST>
													<RATEDETAILS.LIST></RATEDETAILS.LIST>
													<SUMMARYALLOCS.LIST></SUMMARYALLOCS.LIST>
													<STPYMTDETAILS.LIST></STPYMTDETAILS.LIST>
													<EXCISEPAYMENTALLOCATIONS.LIST></EXCISEPAYMENTALLOCATIONS.LIST>
													<TAXBILLALLOCATIONS.LIST></TAXBILLALLOCATIONS.LIST>
													<TAXOBJECTALLOCATIONS.LIST></TAXOBJECTALLOCATIONS.LIST>
													<TDSEXPENSEALLOCATIONS.LIST></TDSEXPENSEALLOCATIONS.LIST>
													<VATSTATUTORYDETAILS.LIST></VATSTATUTORYDETAILS.LIST>
													<COSTTRACKALLOCATIONS.LIST></COSTTRACKALLOCATIONS.LIST>
													<REFVOUCHERDETAILS.LIST></REFVOUCHERDETAILS.LIST>
													<INVOICEWISEDETAILS.LIST></INVOICEWISEDETAILS.LIST>
													<VATITCDETAILS.LIST></VATITCDETAILS.LIST>
													<ADVANCETAXDETAILS.LIST></ADVANCETAXDETAILS.LIST>
													</ACCOUNTINGALLOCATIONS.LIST>
													<DUTYHEADDETAILS.LIST></DUTYHEADDETAILS.LIST>
													<SUPPLEMENTARYDUTYHEADDETAILS.LIST></SUPPLEMENTARYDUTYHEADDETAILS.LIST>
													<TAXOBJECTALLOCATIONS.LIST></TAXOBJECTALLOCATIONS.LIST>
													<REFVOUCHERDETAILS.LIST></REFVOUCHERDETAILS.LIST>
													<EXCISEALLOCATIONS.LIST></EXCISEALLOCATIONS.LIST>
													<EXPENSEALLOCATIONS.LIST></EXPENSEALLOCATIONS.LIST>
													</ALLINVENTORYENTRIES.LIST>';






            }
                  $requestXMLITEMLIST = implode(' ', $ALLINVENTORYENTRIESITEM);
			       
		    $requestXML = $requestXMLSTART.$requestXMLITEMLIST.$requestXMLEND;
		    //echo"<pre>";
		    //print_R($requestXML);exit;
				
			$server = 'http://quytech14.ddns.net:8000/';
			$headers = array( "Content-type: text/xml" ,"Content-length: ".strlen($requestXML) ,"Connection: close" );
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, TALLYIP);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_TIMEOUT, 100);
			curl_setopt($ch, CURLOPT_POST, true);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $requestXML);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			$data = curl_exec($ch);
			//echo"<pre>";
		//print_R($data);
			//exit;
		    if(curl_errno($ch)){
		    echo json_encode(array('status'=>'Failed', 'data'=> print curl_error($ch)));
		   // echo "  something went wrong..... try later";
		    }else{

		    	   $p = xml_parser_create();
        xml_parse_into_struct($p, $data, $vals, $index);
        xml_parser_free($p);
       
		   if(isset($vals[$index['LASTVCHID'][0]]['value']) && $vals[$index['LASTVCHID'][0]]['value']>0) {

		   
        $attributeArray = array();
       
         $attributeArray['TALLYMASTERNOREPORT'] = $vals[$index['LASTVCHID'][0]]['value'];
         
        
        //$yu=$this->_dbUpdate2($attributeArray,'table_warehouse_stk_trn_dts', "transfer_id =".$transfer_id );  
        
        $queryis=$this->_dbUpdate2($attributeArray,'table_warehouse_stk_trn_dts', " bsn IN("."'$implodebsn'".")" );   
        
        //$resultSet = getOrderNoFromSalesOrder ($attributeArray['LASTVCHID']) ;


            // if($resultSet['status'] == 'Failed'){  // Save LastVCHID ONlY
            //   $TALLYORDERNO = 0;
            //   $_objAdmin->_dbUpdate($attributeArray,'table_order', " order_id =".$_REQUEST['id']);   

            // } else { // Save Order No And LastVCHID
             // $TALLYORDERNO = $resultSet['TALLYORDERNO'];
              
          
            //}

            return json_encode(array('status'=>'Pushed To Tally Successfully', 'data'=> " Pushed To Tally Successfully.", 'id'=>$vals[$index['LASTVCHID'][0]]['value']));

    } else {
      
      if(isset($vals[$index['LINEERROR'][0]]['value'])) 
        $message = $vals[$index['LINEERROR'][0]]['value'];
      else 
        $message = $vals[0]['value'];


         return json_encode(array('status'=>$message, 'data'=> $message));
    }


    curl_close($ch);

      

               }

         




		}

	/*********************** Warehouse to Warehouse Transfer text file importing @06-September-2017 Chirag Gupta *****************/




	/*********************** Generate Delivery Challan Series by Chirag Gupta @ 12 September, 2017 *******************************/


	public function getWarehouseLastDeliveryChallanNo () {
		
		$resultArray = array();
		$data        = array();


		if(isset($_SESSION['userLoginType']) && $_SESSION['userLoginType']==7) { // Warehouse Login
			$warehouseId = $_SESSION['warehouseId'];
		} 
		if(!empty($warehouseId) && intVal($warehouseId)>0) {

			$resultset = $this->_getSelectList('table_warehouse AS W','W.warehouse_prefix ',''," W.warehouse_id='".$warehouseId."' AND (W.warehouse_prefix!='' OR W.warehouse_prefix!=0) ");

			//print_r($resultset);
			


			if(is_array($resultset) && sizeof($resultset)>0)  {

				if(strlen(trim($resultset[0]->warehouse_prefix))==2) {

					$data['dlc_prefix'] = 'DLC'.$resultset[0]->warehouse_prefix;

					// Get the last/Max mrn series value of this warehouse
					$dlcDetail = $this->_getSelectList('table_delivery_challan AS DLC',"MAX(DLC.delivery_challan_series) AS dlc_series_no ",''," DLC.warehouse_id ='".$warehouseId."'");

					if(isset($dlcDetail[0]->dlc_series_no) && $dlcDetail[0]->dlc_series_no>0) {

						$data['dlc_series'] = $dlcDetail[0]->dlc_series_no+1;
					} else {
						$data['dlc_series'] = 1;
					}


					if($data['dlc_prefix']!='' && $data['dlc_series']>0){
						$data['dlc_no'] = $data['dlc_prefix'].$data['dlc_series'];

						$resultArray = array('status'=>'success', 'data'=>$data);

					}




				}

			}

		}
		//exit;
		return $resultArray;

	}
	/*********************** Generate Delivery Challan Series by Chirag Gupta @ 12 September, 2017 *******************************/


}  // End of class ?>