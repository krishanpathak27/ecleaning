<?php 
class WebService extends Db_Action {

	//public static $salArrayList = array();
	
	var $value;
	var $access;
	var $salesman;
	var $sortOrder;
	var $cityList;
	var $cond;
    public $IDarray;
	public $IDlist;
	public $distributorSalesmen;
	public $fetaurearray;
	public $ARR_MONTHS;
	public $thisYear;
	public $month_names;

	
	
	
	public function __construct() {
	
		//$this = new Db_Action();
		
		parent::__construct();
		$this->fetaurearray = array();
		$this->IDarray = array();
		$this->IDlist = array();
		$this->aSal = array();
		$this->distributorSalesmen = array();
		$this->thisYear = range(date('Y'), 2013);
		$this->ARR_MONTHS = array('01'=>'Jan','02'=>'Feb','03'=>'Mar','04'=>'Apr','05'=>'May','06'=>'Jun','07'=>'Jul','08'=>'Aug','09'=>'Sep','10'=>'Oct','11'=>'Nov','12'=>'Dec');
		$this->month_names = array("January","February","March","April","May","June","July","August","September","October","November","December");
	
	}
	
	
	public static function checkUserType() {
	
		if(isset($_SESSION['userLoginType']) && $_SESSION['userLoginType']!='') 
		
			return $_SESSION['userLoginType']; else  return false;
	
	}
	
	
	
	public  function getListofRoutes() {
	
		/*$userType = $this->checkUserType();
		
		if(isset($userType) && $userType>1) {
		
			if(isset($_SESSION['WEBUSERID']))
				$cond = " AND ur.web_user_id ='".$_SESSION['WEBUSERID']."' ";
			else 
				$cond = " AND ur.web_user_id IS NULL ";
			
		}*/
		
		
		$auRut= $this->_getSelectList('table_route AS r LEFT JOIN table_user_relationships AS ur ON ur.route_id = r.route_id ',"r.route_id, r.route_name ",''," r.status='A' $cond ");
		
		return $auRut;
		
	
	}
	
	
	public function getFilterCondForRouteSchemeIncentiveTargets($salesmanID){
	
	
	$userType = $this->checkUserType();
		
		if(isset($userType) && $userType>1) {
		
				switch($userType) {
				
					case 5 : 
						array_push($salesmanID, $_SESSION['salesmanId']);
						$IDlist = implode(',', array_filter(array_unique($salesmanID)));
		   				$this->cond =" AND s.salesman_id IN ($IDlist)";
						break;
					case 4 :
						$this->cond = " AND ur.web_user_id ='".$_SESSION['WEBUSERID']."' ";
						break;
					case 3 : 
						$this->cond = " AND ur.web_user_id ='".$_SESSION['WEBUSERID']."' ";
						break;	
			}
			
			return $this->cond;
		} 
	
	}
	
	
	public function getSalesbottomhierarchy($salID, $sortOrder) {
		
		if($salID[0]!='') 
		{
				
				$Ids = implode(',', $salID);
				
			if(isset($sortOrder) && $sortOrder!=0 && $sortOrder!=''){  $sort = "AND H.sort_order >= ".$sortOrder; }
				$salUserType=$this->_getSelectList2('table_salesman', 'account_id',''," salesman_id='".$_REQUEST['salesman_id']."' and session_id='".$_REQUEST['session_id']."'");
				
						$account_id = $salUserType[0]->account_id;
				
				$aSal=$this->_getSelectList2('table_salesman AS s  LEFT JOIN table_salesman_hierarchy_relationship AS SH ON SH.salesman_id = s.salesman_id LEFT JOIN table_salesman_hierarchy AS H ON H.hierarchy_id = SH.hierarchy_id ', 's.salesman_id','',"  s.account_id=".$account_id." and SH.rpt_user_id IN (".$Ids.") $sort ORDER BY s.salesman_name");
						
						//echo "<pre>";
					//print_r($aSal);
					if(is_object($aSal) || is_array($aSal)) 
					{
						//$IDlist = array_map(function($obj){  return $IDlist[] = $obj->salesman_id; }, $aSal);
						
						$IDlist = $this->returnObjectArrayToIndexArray($aSal, 'salesman_id');
						
						//print_r($IDlist);
						//$list = array(100);
						$this->IDarray = array_merge($this->IDarray, $IDlist);
						//print_r($this->IDarray);
						$this->getSalesbottomhierarchy($IDlist, $sortOrder);
						
					}  
			} 
				
			return 	$this->IDarray = array_merge($this->IDarray, $salID);		
		}
	
	
	public function getsaleteamID() {
		
		if(isset($_SESSION['salesmanId']) && $_SESSION['salesmanId']!=''  && $_SESSION['salesmanId']!=0) 
		
			return array($_SESSION['salesmanId']); else  return false;
	}
	
	
	public function getdistributorteamID() {
		
		if(isset($_SESSION['distributorId']) && $_SESSION['distributorId']!='' && $_SESSION['distributorId']!=0) 
		
			return array($_SESSION['distributorId']); else  return false;
	}
	
	
	
	public function getSortOrderofsalesTeam($SalID) {
			
			$salUserType=$this->_getSelectList2('table_salesman', 'account_id','',
							" salesman_id='".$_REQUEST['salesman_id']."' and session_id='".$_REQUEST['session_id']."'");
						$account_id = $salUserType[0]->account_id;
						//exit;
						//print_r( $SalID);
			//echo "Hello<br>";
						 $getSortOrder = $this->_getSelectList2('table_salesman AS s  
		LEFT JOIN table_salesman_hierarchy_relationship AS SH ON SH.salesman_id = s.salesman_id 
		LEFT JOIN table_salesman_hierarchy AS H ON H.hierarchy_id = SH.hierarchy_id ', 
		'H.sort_order',''," s.account_id=".$account_id." and s.salesman_id = ".$SalID." ORDER BY s.salesman_name");
			//print_r($getSortOrder);			
		/*echo "<pre>";
		print_r($getSortOrder);*/
		//print_r($getSortOrder[0]->sort_order);echo "<br>";
		
		if(is_array($getSortOrder) && isset($getSortOrder[0]->sort_order)) 
			return $getSortOrder[0]->sort_order; else return $sort_order = false;
							
	}
	
	
	

	public function SalesmanArrayList() {
				$salUserType=$this->_getSelectList2('table_web_users as w left join table_salesman as s on w.salesman_id=s.salesman_id', 'w.user_type','',
					" s.salesman_id='".$_REQUEST['salesman_id']."' and s.session_id='".$_REQUEST['session_id']."'");
				$value = $salUserType[0]->user_type;
		
		switch ($value) {
		
				// If User Type Admin
			case 0 : 
				
				$salArrayList=$this->_getSelectList('table_salesman AS s', 'salesman_id,salesman_name','',
					" ORDER BY s.salesman_name");
				break;
				// If User Type Account
			case 1 : 
			
				$aSal = $this->_getSelectList('table_salesman AS s', 'salesman_id','',
					" ORDER BY s.salesman_name");
				
				//$list = array_map(function($obj){  return $IDlist[] = $obj->salesman_id; }, $aSal);
				
				$list = $this->returnObjectArrayToIndexArray($aSal, 'salesman_id');
				
				return $list;
					
				break;
				// If User Type Company User
			case 2 : 
				$salArrayList=$this->_getSelectList('table_salesman AS s', 'salesman_id,salesman_name','',
					" ORDER BY s.salesman_name");
				break;
				// If User Type Distributor
			case 3 : 
			
				$res = $this->getdistributorteamID();
				//print_r($res);
				if($res) {
				
				$aSal = $this->_getSelectList('table_order AS O 
					LEFT JOIN table_salesman AS S ON S.salesman_id = O.salesman_id', 'DISTINCT(S.salesman_id)','',
					" O.distributor_id = ".$res[0]." ORDER BY S.salesman_name");
				
				
				
				$list = $this->returnObjectArrayToIndexArray($aSal, 'salesman_id');
				
				//$list = array_map(function($obj){  return $IDlist[] = $obj->salesman_id; }, $aSal);
				
				}

				
				//return array($list,$distributorSalesmen);
				return $list;
				break;
				// If User Type Retailer
			case 4 : 
				$salArrayList=$this->_getSelectList('table_salesman AS s', 'salesman_id,salesman_name','',
					" ORDER BY s.salesman_name");
				break;
				// If User Type Salesman
			case 5 : 
			
						$salUserType=$this->_getSelectList2('table_salesman', 'salesman_id','',
							" salesman_id='".$_REQUEST['salesman_id']."' and session_id='".$_REQUEST['session_id']."'");
						$res = array($salUserType[0]->salesman_id);
						//print_r($res);
				
				
				if($res) 
				{	
				// Call the ModuleClass method   
				
				$_objModuleClass = new ModuleClass();
				$fetaurearray = $_objModuleClass->getEnableModuleListUseByWebService();
				//print_r($fetaurearray);
				//exit;
				//Check hierarchy modules enabled or not
				if(in_array(1,$fetaurearray))
				{
					/*echo "<pre>";
					print_r($res);*/
						
						$sort_order = $this->getSortOrderofsalesTeam($res[0]);
						//print_r($sort_order);	
						$list = $this->getSalesbottomhierarchy($res, $sort_order);
						 //print_r($list);
						// If a salesman Login
						if(empty($list)) { 
						 	 $list = array($_REQUEST['salesman_id']);
						 	//print_r($list);
						 } 
						return $list;
				} else {
				
						// If a salesman Login
						if(empty($list)) {
						 	 $list = array($_REQUEST['salesman_id']);
						 	//print_r($list);
						 } 
						return $list;
						
						
					} 
				} 

				break;
				// If User Type QA
			case 6 : 
					$salArrayList=$this->_getSelectList('table_salesman AS s', 'salesman_id,salesman_name','',
					" ORDER BY s.salesman_name");
				
				break;
			 default:
       			echo "Sorry no user type exists!";
				break;
		} // End of Switch case
		
		
		 
		// Check array isn't empty
		if(is_array($salArrayList)){
		
			// Return salesmen arraylist
			return  $salArrayList;
			
		} else {
			
			return  $salArrayList;
		
		}
	
	}
	
	
	
	public function GetSalesmenMenu($salArray, $sals_ses_ID, $flexiLoad) {
	
	if(is_array($salArray) && !empty($salArray))
	{
	
		$Ids = implode(',', array_unique($salArray));
		
		$res = $this->getsaleteamID();
		
		// Call the ModuleClass method   
		$_objModuleClass = new ModuleClass();
		$fetaurearray = $_objModuleClass->getEnableModuleListUseByWebService();
		
		//Check hierarchy modules enabled
		if(in_array(1,$fetaurearray) && $this->getdistributorteamID() === false)
		{
			//  Get Salesmen Team ID	
			
			if(!empty($res) && is_array($res) && $res[0]!=0) {
			
				$auRec = $this->_getSelectList('table_salesman as sl  
						LEFT JOIN table_salesman_hierarchy_relationship AS SH ON SH.salesman_id = sl.salesman_id ',
							"SH.hierarchy_id",''," sl.salesman_id=".$_SESSION['salesmanId']);
				
				if(isset($auRec[0]->hierarchy_id) && $auRec[0]->hierarchy_id!=0 && $auRec[0]->hierarchy_id!='') {
				
					$usertype = $this->_getSelectList('table_salesman_hierarchy','hierarchy_id,description','',"  
					sort_order > (SELECT sort_order FROM table_salesman_hierarchy WHERE hierarchy_id = ".$auRec[0]->hierarchy_id.") 					AND status = 'A' ORDER BY sort_order ASC");
				} 
					
			}  else {
			
				$usertype = $this->_getSelectList('table_salesman_hierarchy','hierarchy_id,description','',"
				 status = 'A' ORDER BY sort_order ASC");
				 
			 $usertype[] = (object) array('hierarchy_id'=>'NULL', 'description'=>'Default Level(Salesmen)'); 
			}
		
				
		if(is_array($usertype))
		{
			if($flexiLoad == 'flex')
				$data.= '<option value="All" >All</option>';		
			else if($flexiLoad == 'root')
				$data.= '<option value="">Please Select</option>';	
			else 
				$data.= '<option value="">All</option>';
				
			foreach($usertype as $uval):
			
				if($uval->hierarchy_id == 'NULL') {
					
					$condition = "AND SH.hierarchy_id IS NULL";
				
				} else {
					$condition = "AND SH.hierarchy_id = ".$uval->hierarchy_id."";
				}
					
				$data.= "<optgroup label='".$uval->description."'>";
					
				$aSal= $this->_getSelectList('table_salesman AS s  LEFT JOIN table_salesman_hierarchy_relationship AS SH ON SH.salesman_id = s.salesman_id ', 's.salesman_id, salesman_name','',
												" s.salesman_id IN ($Ids) ".$condition."
													ORDER BY s.salesman_name");
				if(is_array($aSal))
				{
					foreach($aSal as $key=>$value):
							
						if($value->salesman_id == $sals_ses_ID){ $selected = 'selected="selected"'; } else{ $selected = ""; }
							
						$data.= "<option value=".$value->salesman_id." ".$selected." >$value->salesman_name</option>";
							
					endforeach;
				}
						
				$data.= "</optgroup>";
					
			endforeach;
		}  else {
			
			$aSal= $this->_getSelectList('table_salesman AS s', 's.salesman_id, salesman_name',''," s.salesman_id=".$_SESSION['salesmanId']);
				if(is_array($aSal))
				{
					foreach($aSal as $key=>$value):
							
						if($value->salesman_id == $sals_ses_ID){ $selected = 'selected="selected"'; } else{ $selected = ""; }
							
						$data.= "<option value=".$value->salesman_id." ".$selected." >$value->salesman_name</option>";
							
					endforeach;
				}
		}
		
		} else if(!empty($res) && is_array($res) && $res[0]!=0) { // If a salesman logged IN
		
			$aSal= $this->_getSelectList('table_salesman AS s', 's.salesman_id, salesman_name',''," s.salesman_id=".$_SESSION['salesmanId']);
				if(is_array($aSal))
				{
					foreach($aSal as $key=>$value):
							
						if($value->salesman_id == $sals_ses_ID){ $selected = 'selected="selected"'; } else{ $selected = ""; }
							
						$data.= "<option value=".$value->salesman_id." ".$selected." >$value->salesman_name</option>";
							
					endforeach;
				}
		
		
		}  else {  // End of check salesmen List array
		
			if($flexiLoad == 'flex')
				$data.= '<option value="All" >All</option>';		
			else if($flexiLoad == 'root')
				$data.= '<option value="">Please Select</option>';	
			else 
				$data.= '<option value="">All</option>';
				
			$aSal= $this->_getSelectList('table_salesman AS s', 's.salesman_id, salesman_name','',"  s.salesman_id IN ($Ids)  ORDER BY s.salesman_name");
				if(is_array($aSal))
				{
					foreach($aSal as $key=>$value):
							
						if($value->salesman_id == $sals_ses_ID){ $selected = 'selected="selected"'; } else{ $selected = ""; }
							
						$data.= "<option value=".$value->salesman_id." ".$selected." >$value->salesman_name</option>";
							
					endforeach;
				}
		
		
		}
		} else {
		
			$data.= '<option value="" >No salesman</option>';
		
		}
		
		return $data;
		
	} // Method End brace
	
	
	
	
	
	
	public function getSalesCondition($salsList) {
	
		 if(is_array($salsList) && !empty($salsList) && $salsList[0]!='') {
		   
		   	$Ids = implode(',', array_unique($salsList));
		   	$salesman =" AND s.salesman_id IN ($Ids)";
			return $salesman;
		} else {
			$salesman =" AND s.salesman_id IS NULL";
			return $salesman;
		}
	}
	

	
	public function checkValidSalesmen($salsList, $sid) {
	
		if(in_array($sid, $salsList)) 
			return true;
		else 
			return false;
	
	}
	
	
	public function getHierarchyIDoFSalesTeam($salesID) {
	
		if(isset($salesID) && $salesID!=0){
		
		$ausalesmen =$this->_getSelectList('table_salesman AS S LEFT JOIN table_salesman_hierarchy_relationship AS SH ON SH.salesman_id = S.salesman_id ',"SH.hierarchy_id",''," S.salesman_id = ".$salesID."  AND S.status='A'");
			
			
			if(isset($ausalesmen[0]->hierarchy_id) && $ausalesmen[0]->hierarchy_id!='')
			{
				return $ausalesmen[0]->hierarchy_id;
			}
		} else {
		
			return false;
		
		}
	}
	
	
	public function getMonthList($selectedMonth) {}
	
	
	public function getMonthList2($selectedMonth) {}
	
	
	public function getMonthList3($selectedMonth) {}

	
	public function getYearList($selectedYear) {}
	
	
	public function getYearList2($selectedYear) {}
	
	
	
	public function getCityList($selectedCity) {}
	
	
	/************ Added for take order on 6th june 2014 ******************/
	
	public function getStateList($selectedState) {}
	
	/************ Added for take order on 6th june 2014 ******************/
	
	public function getCityListOptions($selectedCity,$flexiLoad ) {}
	
 	public function checkAccess($pageAccess, $pageName) {}

}

?>