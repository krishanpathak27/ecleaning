<script>
$( document ).ready(function() {
    var userType = '<?php echo $_SESSION['userLoginType'] ?>'
    var id = '<?php echo $_SESSION['supervisor_id'] ?>'
    if(userType == '5' && id != null) {
        $('#supervisor_id').val(id);
        $('#supervisor_id').attr('disabled',true);
    }
});


</script>
            <?php if($dis_name_err!=''){?>
	          <div id="message-red">
	            <div class="bg-warning pdng-md mrgn-btm-lg">
	              <div class="pull-left bold">Error. <?php echo $dis_name_err; ?></div>
	              <div class="pull-right"><a class="close-red"><i class="fa fa-close"></i></a></div>
	              <div class="clear"></div>
	            </div>
	          </div>

        	<?php } ?></div>
<div class="panel-heading">
              <h4 class="panel-title">Cleaner Form</h4>
              <hr>
            </div>
            <div class="panel-body">   
             	<form name="frmPre" id="frmPre" method="post" action="cleaner.php" enctype="multipart/form-data" >  
                	<div class="row">
	                  <div class="col-lg-3 col-md-3 col-xs-12 bold">
	                    Cleaner Name:
	                  </div>
	                  <div class="col-lg-3 col-md-3 col-xs-12">
	                    <div class="form-group">

	                    <input type="text" name="cleaner_name" id="cleaner_name" class="required form-control" value="<?php echo $auRec[0]->cleaner_name; ?>" maxlength="255" placeholder="Name"/>
	                     
	                    </div>
	                  </div>
                  	</div>
                   	<div class="row">
	                  	<div class="col-lg-3 col-md-3 col-xs-12 bold">
	                    	Cleaner Address:
	                  	</div>

	                  	<div class="col-lg-3 col-md-3 col-xs-12">
	                    	<div class="form-group">

	                    	<input type="text" style="width:590px;" name="cleaner_address" id="cleaner_address" class="required text form-control" value="<?php echo $auRec[0]->cleaner_address; ?>" placeholder="Address"/>
	                    	</div>
	                  	</div>
                	</div> 
                   	<div class="row">
	                  	<div class="col-lg-3 col-md-3 col-xs-12 bold">
	                    	Cleaner Phone No:
	                  	</div>

	                  	<div class="col-lg-3 col-md-3 col-xs-12">
	                    	<div class="form-group">

	                    	<input type="text" style="width:590px;" name="cleaner_phone_no" id="cleaner_phone_no" class="required number form-control" value="<?php echo $auRec[0]->cleaner_phone_no; ?>" placeholder="Phone NO"/>
	                    	</div>
	                  	</div>
                	</div>  
                     
                 	<div class="row">
	                  	<div class="col-lg-3 col-md-3 col-xs-12 bold">
	                    	Cleaner Code:
	                  	</div>
                  		<div class="col-lg-3 col-md-3 col-xs-12">
                    		<div class="form-group">
                    		<input type="text" style="width:590px;" name="cleaner_code" id="cleaner_code" class="required form-control" value="<?php echo $auRec[0]->cleaner_code; ?>" placeholder="Code"/>
                    		
                    			
                    		</div>
                  		</div>
                  	</div>

                  	<div class="row">
	                  	<div class="col-lg-3 col-md-3 col-xs-12 bold">
	                    	Cleaner Email:
	                  	</div>

	                  	<div class="col-lg-3 col-md-3 col-xs-12">
	                    	<div class="form-group">
	                    	<input type="text" style="width:590px;" name="cleaner_email" id="cleaner_email" class="required email form-control" class="text" value="<?php echo $auRec[0]->cleaner_email; ?>" placeholder="Email"/>
	                    		
	                    
	                    	</div>
	                  	</div>
                	</div> 

                	<div class="row">
	                  	<div class="col-lg-3 col-md-3 col-xs-12 bold">
	                    	Supervisor:
	                  	</div>

	                  	<div class="col-lg-3 col-md-3 col-xs-12">
	                    	<div class="form-group">
<select name="supervisor_id" id="supervisor_id" class="styledselect_form_4 required form-control" >
					<option value="">Select Supervisor</option>
					<?php 
					$condi = "";
					
					
					$branchList=$_objAdmin->_getSelectList('table_supervisor',"supervisor_id,supervisor_name",''," status='A'");
						for($i=0;$i<count($branchList);$i++){
						
						if($branchList[$i]->supervisor_id==$auRec[0]->supervisor_id){$select="selected";} else {$select="";}
							
					 ?>
					 <option value="<?php echo $branchList[$i]->supervisor_id; ?>" <?php echo $select ?>><?php echo $branchList[$i]->supervisor_name; ?></option>
					 
					 <?php } ?>
				</select>
	                    
	                    	</div>
	                  	</div>
                	</div> 

                	<div class="row">
	                  	<div class="col-lg-3 col-md-3 col-xs-12 bold">
	                    	Status:
	                  	</div>

	                  	<div class="col-lg-3 col-md-3 col-xs-12">
	                    	<div class="form-group">
	                    		<select name="status" id="status" class="styledselect_form_3 form-control">
			
			<option value="I" <?php if($auRec[0]->status=='I') echo "selected";?> >Inactive</option>
			<option value="A" <?php if($auRec[0]->status=='A') echo "selected";?> >Active</option>
								</select>
	                    	
	                    
	                    	</div>
	                  	</div>
                	</div>

                		<div class="row">
	                  	<div class="col-lg-3 col-md-3 col-xs-12 bold">
	                    	Cleaner Profile Pic:
	                  	</div>

	                  	<div class="col-lg-3 col-md-3 col-xs-12">
	                    	<div class="form-group">
	                    	<input type="file" style="width:590px;" name="image" id="image" placeholder="Profile Pic" class="form-control"/>
	                    	
	                    		
	                    
	                    	</div>
	                  	</div>
                	</div>

	                <div class="row mrgn-top-sm">
	                  <div class="col-md-12 col-sm-12 col-xs-12 text-right">
	                  		<input name="cleaner_add" type="hidden" value="yes" />
			<input name="cleaner_id" type="hidden" value="<?php echo $auRec[0]->cleaner_id; ?>" />
			<input name="account_id" type="hidden" value="<?php echo $_SESSION['accountId']; ?>" />
						<input type="button" value="Back" class="form-reset btn bg-info" onclick="location.href='cleaner.php';" />
						<input name="save" class="form-submit btn bg-lgtgreen" type="submit" id="save" value="Save" />
			<input name="submit" class="form-submit btn bg-lgtgreen" type="submit" id="submit" value="Save & Continue" />
						
	                   </div>
	                </div>
                </form>
              </div> 


    


