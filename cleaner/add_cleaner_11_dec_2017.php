<script src="javascripts/jquery-1.8.2.js" type="text/javascript"></script>
<script src="javascripts/jquery-ui.js"></script>
<script type="text/javascript" src="javascripts/validate.js"></script>
<script type="text/javascript" src="javascripts/select_dropdowns.js"></script>
 <script>
  $(function() {
    $( "#datepicker" ).datepicker({dateFormat: 'd M yy',changeMonth: true, changeYear: true, yearRange: '1930:2050'});
	
  });

  </script>
  
<!--  start message-blue -->
	<?php if($auRec[0]->cleaner_id=="" && $dis_name_err==''){ ?>
	<div id="message-blue">
	</div>
	<?php } ?>
<!--  end message-blue -->
<!--  start message-red -->
	<?php if($dis_name_err!=''){?>
	<div id="message-red">
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
	<tr>
		<td class="red-left">Error. <?php echo $dis_name_err; ?></td>
		<td class="red-right"><a class="close-red"><img src="images/icon_close_red.gif"   alt="" /></a></td>
	</tr>
	</table>
	</div>
	<?php } ?>
<!--  end message-red -->
<!--  start step-holder -->
<div id="step-holder">
<div class="step-no">1</div>
<div class="step-dark-left">Cleaner Form</div>
<div class="step-dark-right">&nbsp;</div>
<div class="step-no-off">2</div>
<div class="step-light-left">Login Form</div>
<div class="clear"></div>
</div>
	
	<!--  end step-holder -->
	<form name="frmPre" id="frmPre" method="post" action="cleaner.php" enctype="multipart/form-data" >
		<table border="0" cellpadding="0" cellspacing="0"  id="id-form">
		<tr>
			<th valign="top">Cleaner Name:</th>
			<td><input type="text" name="cleaner_name" id="cleaner_name" class="required" value="<?php echo $auRec[0]->cleaner_name; ?>" maxlength="255" placeholder="Name"/></td>
			<td></td>
			<td></td>
		</tr>
                <tr>
			<th valign="top">Cleaner Phone No:</th>
			<td><input type="text" name="cleaner_phone_no" id="cleaner_phone_no" class="required number" maxlength="15" value="<?php echo $auRec[0]->cleaner_phone_no; ?>" placeholder="Phone Number"/></td>
		</tr>
                <tr>
			<th valign="top">Cleaner Address:</th>
			<td colspan="3">
			<input type="text" style="width:590px;" name="cleaner_address" id="cleaner_address" class="text" value="<?php echo $auRec[0]->cleaner_address; ?>" placeholder="Address"/>
			</td>
		</tr>
                <tr>
			<th valign="top">Cleaner Code:</th>
			<td colspan="3">
			<input type="text" style="width:590px;" name="cleaner_code" id="cleaner_code" class="required" value="<?php echo $auRec[0]->cleaner_code; ?>" placeholder="Code"/>
			</td>
		</tr>
                <tr>
			<th valign="top">Cleaner Email:</th>
			<td colspan="3">
			<input type="text" style="width:590px;" name="cleaner_email" id="cleaner_email" class="required" class="text" value="<?php echo $auRec[0]->cleaner_email; ?>" placeholder="Email"/>
			</td>
		</tr>
                <tr>
			<th valign="top">Supervisor:</th>
			<td>
				<select name="supervisor_id" id="supervisor_id" class="styledselect_form_4 required" >
					<option value="">Select Supervisor</option>
					<?php 
					$condi = "";
					
					
					$branchList=$_objAdmin->_getSelectList('table_supervisor',"supervisor_id,supervisor_name",''," status='A'");
						for($i=0;$i<count($branchList);$i++){
						
						if($branchList[$i]->supervisor_id==$auRec[0]->supervisor_id){$select="selected";} else {$select="";}
							
					 ?>
					 <option value="<?php echo $branchList[$i]->supervisor_id; ?>" <?php echo $select ?>><?php echo $branchList[$i]->supervisor_name; ?></option>
					 
					 <?php } ?>
				</select>
				
			</td>
			<td></td>
		</tr>
                <tr>
			<th valign="top">Status:</th>
			<td>
			<select name="status" id="status" class="styledselect_form_3">
			
			<option value="I" <?php if($auRec[0]->status=='I') echo "selected";?> >Inactive</option>
			<option value="A" <?php if($auRec[0]->status=='A') echo "selected";?> >Active</option>
			</td>
			<td></td>
		</tr>
                <tr>
			<th valign="top">Cleaner Profile Pic:</th>
			<td colspan="3">
			<input type="file" style="width:590px;" name="image" id="image" placeholder="Profile Pic"/>
			</td>
		</tr>
				

	
		<tr>
		<th>&nbsp;</th>
		<td valign="top" colspan="3">
			<input name="cleaner_add" type="hidden" value="yes" />
			<input name="cleaner_id" type="hidden" value="<?php echo $auRec[0]->cleaner_id; ?>" />
			<input name="account_id" type="hidden" value="<?php echo $_SESSION['accountId']; ?>" />
			<!--<input type="reset" value="Reset!" class="form-reset">-->
			<input type="button" value="Back" class="form-reset" onclick="location.href='cleaner.php';" />
			<input name="save" class="form-submit" type="submit" id="save" value="Save" />
			<input name="submit" class="form-submit" type="submit" id="submit" value="Save & Continue" />
		</td>
		</tr>
		</table>

	</form>