<script>
$(document).ready(function(){
$('#username').keyup(username_check);
});
	
function username_check(){	
var username = $('#username').val();
if(username == "" || username.length < 1){
$('#username').css('border', 'solid 1px #dc0000');
$('#tick').hide();
}else{

jQuery.ajax({
   type: "POST",
   url: "check.php",
   data: 'username='+ username,
   cache: false,
   success: function(response){
if(response == 1){
	$('#username').css('border', 'solid 1px #dc0000');	
	$('#tick').hide();
	$('#cross').fadeIn();
	}else{
	$('#username').css('border', 'solid 1px #dc0000');
	$('#cross').hide();
	$('#tick').fadeIn();
	     }
}
});
}
}
</script>
<style>
#username{
	padding:3px;
	font-size:12px;
	border:solid 1px #dc0000;
}

#tick{display:none}
#cross{display:none}
	

</style>
            <?php if($login_err!=''){?>
	          <div id="message-red">
	            <div class="bg-warning pdng-md mrgn-btm-lg">
	              <div class="pull-left bold">Error. <?php echo $login_err; ?></div>
	              <div class="pull-right"><a class="close-red"><i class="fa fa-close"></i></a></div>
	              <div class="clear"></div>
	            </div>
	          </div>

        	<?php } ?></div>
<div class="panel-heading">
              <h4 class="panel-title">Login Form</h4>
              <hr>
            </div>
<!--  end step-holder -->
            <div class="panel-body">   
             	<form name="frmPre" id="frmPre" method="post" action="cleaner.php" enctype="multipart/form-data" >        
                	<div class="row">
	                  <div class="col-lg-3 col-md-3 col-xs-12 bold">
	                    User name:
	                  </div>
	                  <div class="col-lg-3 col-md-3 col-xs-12">
	                    <div class="form-group">
	                    <input type="text" name="username" id="username" class="required form-control" value="<?php echo $auRec[0]->username; ?>"/>
							<div id="tick"><span style=" color:#088A08;"><b>User Name Available</b></span><img src="images/tick.png" width="16" height="16"/></div>
							<div id="cross"><span style=" color:#FF0000;"><b>User Name Not Available</b></span><img  src="images/cross.png" width="16" height="16"/></div>

 
	                    </div>
	                  </div>
                  	</div>
                   
                   	<div class="row">
	                  	<div class="col-lg-3 col-md-3 col-xs-12 bold">
	                    	Password:
	                  	</div>

	                  	<div class="col-lg-3 col-md-3 col-xs-12">
	                    	<div class="form-group">

						<input type="password" name="password" id="password" <?php if($auRec[0]->web_user_id!=''){ ?>class="text form-control"<?php } else { ?>class="required minlength form-control" <?php } ?>  minlength="8" />

	                    	

	                    		
	                     		
	                    
	                    	</div>
	                  	</div>
                	</div>  
                     
                 	<div class="row">
	                  	<div class="col-lg-3 col-md-3 col-xs-12 bold">
	                    	Confirm-Password:
	                  	</div>
                  		<div class="col-lg-3 col-md-3 col-xs-12">
                    		<div class="form-group">
                    		<input type="password" name="conf_pass" id="conf_pass" equalto="#password" <?php if($auRec[0]->web_user_id!=''){ ?>class="text form-control"<?php } else { ?>class="required form-control" <?php } ?>/>
                    			
                    		</div>
                  		</div>
                  	</div>

                  	<div class="row">
	                  	<div class="col-lg-3 col-md-3 col-xs-12 bold">
	                    	Email ID:
	                  	</div>

	                  	<div class="col-lg-3 col-md-3 col-xs-12">
	                    	<div class="form-group">

							<?php if( $_REQUEST['id']!=""){ ?>
							<input type="text" name="email" id="email" class="required email form-control" value="<?php echo $auRec[0]->email_id; ?>"/>
							<?php } else {
							$EmailRec=$_objAdmin->_getSelectList('table_cleaner',"cleaner_email",''," cleaner_id=".$_SESSION['disId']);
							?>
							<input type="text" name="email" id="email" class="required email form-control" value="<?php echo $EmailRec[0]->cleaner_email; ?>"/>
							<?php } ?>



	                    		
	                    	</div>
	                  	</div>
                	</div> 

                	

                	
	                <div class="row mrgn-top-sm">
	                  <div class="col-md-12 col-sm-12 col-xs-12 text-right">
	                  	<input type="hidden" name="cleaner_id" value="<?php echo $_SESSION['disId']; ?>" />
			<input type="hidden" name="web_id" value="<?php echo $auRec[0]->web_user_id; ?>"/>
			<input type="hidden" name="user_type" value="3" />
			<input name="account_id" type="hidden" value="<?php echo $_SESSION['accountId']; ?>" />
			<input name="start_date" type="hidden" value="<?php echo $_SESSION['StartDate']; ?>" />
			<input name="end_date" type="hidden" value="<?php echo $_SESSION['EndDate']; ?>" />
			<input name="dis_login" type="hidden" value="yes" />
						<button type="button" class="btn bg-info"><i class="fa fa-refresh mrgn-rgt-sm" aria-hidden="true"></i>Reset</button>
						<input type="button" value="Skip!" class="form-submit btn bg-info" onclick="location.href='cleaner.php?skip';" />
						<input name="submit" class="form-submit btn bg-lgtgreen" type="submit" id="submit" value="Save & Continue" />
						
	                   </div>
	                </div>
                </form>
              </div> 


    


