<?php
include("includes/config.inc.php");
include("includes/function.php");
//include("includes/globalarraylist.php");

$page_name = "Cleaner Login List";


$_objAdmin = new Admin();

if(isset($_REQUEST['Id']) && $_REQUEST['Id']!=""){
	$cid=$_objAdmin->_dbUpdate(array("session_id"=>''),'table_cleaner', " cleaner_id='".($_REQUEST['Id'])."'");
	$sus="Cleaner Logout successfully";
	$datetime="Date: ".$date.", Time: ".$time;
	$page="Logout By Admin";
	file_put_contents("../cleaner_services/Log/CleanerID".($_REQUEST['Id']).".log",print_r($datetime,true)."\r\n", FILE_APPEND);
	file_put_contents("../cleaner_services/Log/CleanerID".($_REQUEST['Id']).".log",print_r($page,true)."\r\n", FILE_APPEND);
	file_put_contents("../cleaner_services/Log/CleanerID".($_REQUEST['Id']).".log",print_r($sus,true), FILE_APPEND);
	header("Location: cleaner_login_list.php?sus=sus");
}	


if(isset($_REQUEST['show']) && $_REQUEST['show']=="yes"){
	$_objAdmin->showLoginCleaner($sp);
	die;
}


?>

<?php include("header.inc.php") ?>
<div class="m-grid__item m-grid__item--fluid m-wrapper" style="background-color:">
    <!-- BEGIN: Subheader -->
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="#" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__separator">
                        -
                    </li>
                    <li class="m-nav__item">
                        <a href="#" class="m-nav__link">
                            <span class="m-nav__link-text">
                               User
                            </span>
                        </a>
                    </li>
                    <li class="m-nav__separator">
                        -
                    </li>
                    <li class="m-nav__item">
                        <a href="" class="m-nav__link">
                            <span class="m-nav__link-text">
                                Cleaner Login Status
                            </span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <!-- END: Subheader -->
    <div class="m-content">
        <?php 
            if(isset($_REQUEST['sus']) && $_REQUEST['sus'] != "") { ?>
            <div role="alert" style="background: #d7fbdc;" class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-dismissible m--margin-bottom-30">
                <div class="m-alert__icon" >
                        <i class="flaticon-exclamation m--font-brand"></i>
                </div>
                <div class="m-alert__text">
                       <?php echo "Cleaner Logout successfully!."; ?> 
                </div>
            </div>
            <?php } ?>
        <div class="m-portlet m-portlet--mobile">
            
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            Cleaner Login Status List
                        </h3>
                    </div>
                </div>
            </div>
            <div class="m-portlet__body">
                <!--begin: Search Form -->
                <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
                    <div class="row align-items-center">
                        <div class="col-xl-8 order-2 order-xl-1">
                            <div class="form-group m-form__group row align-items-center">
                                <div class="col-md-4">
                                    <div class="m-input-icon m-input-icon--left">
                                        <input type="text" class="form-control m-input" placeholder="Search..." id="generalSearch">
                                        <span class="m-input-icon__icon m-input-icon__icon--left">
                                            <span>
                                                <i class="la la-search"></i>
                                            </span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
                <!--end: Search Form -->
                <!--begin: Datatable -->
                <div class="m_datatable" id="locked_right"></div>
                <!--end: Datatable -->
            </div>
        </div>
    </div>
</div>
</div>
</div>
<!-- start footer -->         
<?php include("footer.php") ?>
<script type="text/javascript" >
    var DefaultDatatableDemo = function () {
        //== Private functions

        // basic demo
        var demo = function () {
            var datatable = $('.m_datatable').mDatatable({
                data: {
                    type: 'remote',
                    source: {
                        read: {
                            url: '<?php echo $ajaxUrl . '?pageType=cleanerLoginStatusList' ?>'
                        }
                    },
                    pageSize: 20,
                    serverPaging: true,
                    serverFiltering: true,
                    serverSorting: true
                },
                layout: {
                    theme: 'default',
                    class: '',
                    scroll: true,
                    footer: false
                },
                sortable: true,
                filterable: false,
                pagination: true,
                search: {
                    input: $('#generalSearch')
                },
                columns: [{
                        field: "cleaner_name",
                        title: "Cleaner Name",
                        sortable: 'asc'
                    }, {
                        field: "username",
                        title: "User Name",
                        width: 150,
                        responsive: {visible: 'lg'}
                    }, {
                        field: "email_id",
                        title: "Email ID",
                        sortable: false,
                        responsive: {visible: 'lg'}
                    }, {
                        field: "cleaner_phone_no",
                        title: "Phone Number",
                        width: 150,
                        responsive: {visible: 'lg'}
                    }, {
                        field: "Login_status",
                        title: "Login Status",
                        width: 100,

                        
                    }, {
                        field: "Actions",
                        width: 110,
                        title: "Actions",
                        sortable: false,
                        locked: {right: 'xl'},
                        overflow: 'visible',
                        template: function (row, index, datatable){

                            if(row.Login_status == 'Logged In'){
                                $change_status='\<a href="cleaner_login_list.php?Id='+row.cleaner_id+'" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only edit m-btn--pill" title="Change Status">\
                                        <i class="la la-i-cursor">\
                                    </a>';
                            }else{
                            	$change_status='';
                            }
                            var dropup = (datatable.getPageSize() - index) <= 4 ? 'dropup' : '';
                            return $change_status;
                        }
                    }]
            });
        };

        return {
            // public functions
            init: function () {
                demo();
            }
        };
    }();

    jQuery(document).ready(function () {
        DefaultDatatableDemo.init();
    });



</script>


