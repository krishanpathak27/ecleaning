<?php
include("../includes/config.inc.php");
include("xml.php");
header("Content-type: text/xml");
$dom = new DOMDocument('1.0', 'utf-8');
$_objAdmin = new Admin();

$node = $dom->createElement("WhiteSpot");
$parnode = $dom->appendChild($node);
if($_REQUEST['cleaner_id']=='' ||  $_REQUEST['s_id']==''){	
    $node = $dom->createElement("Sts");
    $newnode = $parnode->appendChild($node);
    $newnode->setAttribute("Code", "1001");
    //$newnode->setAttribute("message", "Request Blank");
    $dom->formatOutput = true;
    echo $dom->saveXML();
    $datetime="Date: ".$date.", Time: ".$time;
    $page="Cleaner Profile Update";
    file_put_contents("Log/Cleaner".$_REQUEST['cleaner_id'].".log",print_r($datetime,true)."\r\n", FILE_APPEND);
    file_put_contents("Log/Cleaner".$_REQUEST['cleaner_id'].".log",print_r($page,true)."\r\n", FILE_APPEND);
    file_put_contents("Log/Cleaner".$_REQUEST['cleaner_id'].".log",print_r($_REQUEST,true), FILE_APPEND);
    file_put_contents("Log/Cleaner".$_REQUEST['cleaner_id'].".log",print_r($dom->saveXML(),true), FILE_APPEND);
    die;
}

$con = mysql_connect(MYSQL_HOST_NAME,MYSQL_USER_NAME,MYSQL_PASSWORD);
if (!$con) {
    $node = $dom->createElement("Sts");
    $newnode = $parnode->appendChild($node);
    $newnode->setAttribute("Code", "1002");
    //$newnode->setAttribute("message", "database not connect");
    $dom->formatOutput = true;
    echo $dom->saveXML();
    $datetime="Date: ".$date.", Time: ".$time;
    $page="Cleaner Profile Update";
    file_put_contents("Log/Cleaner".$_REQUEST['cleaner_id'].".log",print_r($datetime,true)."\r\n", FILE_APPEND);
    file_put_contents("Log/Cleaner".$_REQUEST['cleaner_id'].".log",print_r($page,true)."\r\n", FILE_APPEND);
    file_put_contents("Log/Cleaner".$_REQUEST['cleaner_id'].".log",print_r($_REQUEST,true), FILE_APPEND);
    file_put_contents("Log/Cleaner".$_REQUEST['cleaner_id'].".log",print_r($dom->saveXML(),true), FILE_APPEND);
    die;
}

mysql_select_db(MYSQL_DB_NAME, $con);
if($_REQUEST['cleaner_id']!='' && $_REQUEST['s_id']!=''){
    $condi="where cleaner_id='".$_REQUEST['cleaner_id']."' and session_id='".$_REQUEST['s_id']."'";
    $userRec = mysql_query("SELECT cleaner_id FROM table_cleaner ".$condi);
    if(mysql_num_rows($userRec)<=0){
        $node = $dom->createElement("Sts");
        $newnode = $parnode->appendChild($node);
        $newnode->setAttribute("Code", "1004");
        $dom->formatOutput = true;
        echo $dom->saveXML();
        $end_datetime="End Date: ".date('d/m/Y H:i:s');
        $page="Cleaner Profile Update";
        file_put_contents("Log/Cleaner".$_REQUEST['cleaner_id'].".log",print_r($datetime,true)."\r\n", FILE_APPEND);
        file_put_contents("Log/Cleaner".$_REQUEST['cleaner_id'].".log",print_r($end_datetime,true)."\r\n", FILE_APPEND);
        file_put_contents("Log/Cleaner".$_REQUEST['cleaner_id'].".log",print_r($page,true)."\r\n", FILE_APPEND);
        file_put_contents("Log/Cleaner".$_REQUEST['cleaner_id'].".log",print_r($_REQUEST,true), FILE_APPEND);
        file_put_contents("Log/Cleaner".$_REQUEST['cleaner_id'].".log",print_r($dom->saveXML(),true), FILE_APPEND);
        mysql_close($con);
        die;
    }
}

if(!isset($_REQUEST['xmlData']) || $_REQUEST['xmlData']==''){
    $node = $dom->createElement("Sts");
    $newnode = $parnode->appendChild($node);
    $newnode->setAttribute("Code", "1006");
    $dom->formatOutput = true;
    echo $dom->saveXML();
    $datetime="Date: ".$date.", Time: ".$time;
    $page="Cleaner Profile Update";
    file_put_contents("Log/Cleaner".$_REQUEST['cleaner_id'].".log",print_r($datetime,true)."\r\n", FILE_APPEND);
    file_put_contents("Log/Cleaner".$_REQUEST['cleaner_id'].".log",print_r($page,true)."\r\n", FILE_APPEND);
    file_put_contents("Log/Cleaner".$_REQUEST['cleaner_id'].".log",print_r($_REQUEST,true), FILE_APPEND);
    file_put_contents("Log/Cleaner".$_REQUEST['cleaner_id'].".log",print_r($dom->saveXML(),true), FILE_APPEND);
    die;
}

$arrXMLData=xml2array($_REQUEST['xmlData']);
if(!is_array($arrXMLData)){
    $node = $dom->createElement("Sts");
    $newnode = $parnode->appendChild($node);
    $newnode->setAttribute("Code", "1006");
    $dom->formatOutput = true;
    echo $dom->saveXML();
    $datetime="Date: ".$date.", Time: ".$time;
    $page="Cleaner Profile Update";
    file_put_contents("Log/Cleaner".$_REQUEST['cleaner_id'].".log",print_r($datetime,true)."\r\n", FILE_APPEND);
    file_put_contents("Log/Cleaner".$_REQUEST['cleaner_id'].".log",print_r($page,true)."\r\n", FILE_APPEND);
    file_put_contents("Log/Cleaner".$_REQUEST['cleaner_id'].".log",print_r($_REQUEST,true), FILE_APPEND);
    file_put_contents("Log/Cleaner".$_REQUEST['cleaner_id'].".log",print_r($dom->saveXML(),true), FILE_APPEND);
    die;
}

$cleaneDetail = $arrXMLData[ClnDtl_attr];
if(trim($cleaneDetail['cleaner_id'])!= ''){
    $cleaner_id = trim($cleaneDetail['cleaner_id']);
    $cleaner_address = trim($cleaneDetail['address']);
    $contact_number = trim($cleaneDetail['phoneNo']);
    $cleaner_email = trim($cleaneDetail['email']);
    $cleanerName = trim($cleaneDetail['name']);
    $profilePic = '';
    if(isset($cleaneDetail['profile_pic']) && !empty($cleaneDetail['profile_pic']))
    {   
        $num=$_objAdmin->randimg(10);
        $binary=base64_decode($cleaneDetail['profile_pic']);
        $image="img_".$num.".jpg";
        $upload="../images/cleaner/".$image;
        $file = fopen($upload, 'wb');
        fwrite($file, $binary);	
        fclose($file);
        $profilePic = $image;
    }
    
    $last_update_date = $date;
    $last_update_status = "Update";
    if(isset($profilePic) && !empty($profilePic)) {
        $sql = "UPDATE table_cleaner SET cleaner_address='$cleaner_address',cleaner_name='$cleanerName',image='$profilePic',cleaner_phone_no='$contact_number',cleaner_email='$cleaner_email',last_update_status = 'Update',last_update_date = '$date' where cleaner_id='".$cleaner_id."'";
    } else {
        $sql = "UPDATE table_cleaner SET cleaner_address='$cleaner_address',cleaner_name='$cleanerName',cleaner_phone_no='$contact_number',cleaner_email='$cleaner_email',last_update_status = 'Update',last_update_date = '$date' where cleaner_id='".$cleaner_id."'";
    }
    mysql_query($sql) or die(mysql_error());
}

if($sql!=''){
    $node = $dom->createElement("Sts");
    $newnode = $parnode->appendChild($node);
    $newnode->setAttribute("Code", "0");
    // $newnode->setAttribute("Success", "Profile updated successfully");
    $dom->formatOutput = true;
    echo $dom->saveXML();
    $datetime="Date: ".$date.", Time: ".$time;
    $page="Cleaner Profile Update";
    file_put_contents("Log/Cleaner".$_REQUEST['cleaner_id'].".log",print_r($datetime,true)."\r\n", FILE_APPEND);
    file_put_contents("Log/Cleaner".$_REQUEST['cleaner_id'].".log",print_r($page,true)."\r\n", FILE_APPEND);
    file_put_contents("Log/Cleaner".$_REQUEST['cleaner_id'].".log",print_r($_REQUEST,true), FILE_APPEND);
    file_put_contents("Log/Cleaner".$_REQUEST['cleaner_id'].".log",print_r($dom->saveXML(),true), FILE_APPEND);
    die;
} else {
    $node = $dom->createElement("Sts");
    $newnode = $parnode->appendChild($node);
    $newnode->setAttribute("Code", "1007");
    $dom->formatOutput = true;
    echo $dom->saveXML();
    $datetime="Date: ".$date.", Time: ".$time;
    $page="Cleaner Profile Update";
    file_put_contents("Log/Cleaner".$_REQUEST['cleaner_id'].".log",print_r($datetime,true)."\r\n", FILE_APPEND);
    file_put_contents("Log/Cleaner".$_REQUEST['cleaner_id'].".log",print_r($page,true)."\r\n", FILE_APPEND);
    file_put_contents("Log/Cleaner".$_REQUEST['cleaner_id'].".log",print_r($_REQUEST,true), FILE_APPEND);
    file_put_contents("Log/Cleaner".$_REQUEST['cleaner_id'].".log",print_r($dom->saveXML(),true), FILE_APPEND);
    die;
}

?>
