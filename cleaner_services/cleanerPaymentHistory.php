<?php
header("Content-type: text/xml");
include("../includes/config.inc.php");
$dom = new DOMDocument('1.0', 'utf-8');
$node = $dom->createElement("WhiteSpot");
$parnode = $dom->appendChild($node);
if($_REQUEST['cleaner_id']=='' ||  $_REQUEST['s_id']==''){
    $node = $dom->createElement("Sts");
    $newnode = $parnode->appendChild($node);
    $newnode->setAttribute("Code", "1001");
    //$newnode->setAttribute("message", "Request Blank");
    $dom->formatOutput = true;
    echo $dom->saveXML();
    $datetime="Date: ".$date.", Time: ".$time;
    $page="Payment History";
    file_put_contents("Log/Cleaner".$_REQUEST['cleaner_id'].".log",print_r($datetime,true)."\r\n", FILE_APPEND);
    file_put_contents("Log/Cleaner".$_REQUEST['cleaner_id'].".log",print_r($page,true)."\r\n", FILE_APPEND);
    file_put_contents("Log/Cleaner".$_REQUEST['cleaner_id'].".log",print_r($_REQUEST,true), FILE_APPEND);
    file_put_contents("Log/Cleaner".$_REQUEST['cleaner_id'].".log",print_r($dom->saveXML(),true), FILE_APPEND);
    die;
}
$con = mysql_connect(MYSQL_HOST_NAME,MYSQL_USER_NAME,MYSQL_PASSWORD);
if (!$con) {
    $node = $dom->createElement("Sts");
    $newnode = $parnode->appendChild($node);
    $newnode->setAttribute("Code", "1002");
    //$newnode->setAttribute("message", "database not connect");
    $dom->formatOutput = true;
    echo $dom->saveXML();
    $datetime="Date: ".$date.", Time: ".$time;
    $page="Payment History";
    file_put_contents("Log/Cleaner".$_REQUEST['cleaner_id'].".log",print_r($datetime,true)."\r\n", FILE_APPEND);
    file_put_contents("Log/Cleaner".$_REQUEST['cleaner_id'].".log",print_r($page,true)."\r\n", FILE_APPEND);
    file_put_contents("Log/Cleaner".$_REQUEST['cleaner_id'].".log",print_r($_REQUEST,true), FILE_APPEND);
    file_put_contents("Log/Cleaner".$_REQUEST['cleaner_id'].".log",print_r($dom->saveXML(),true), FILE_APPEND);
    die;
}
mysql_select_db(MYSQL_DB_NAME, $con);
if($_REQUEST['cleaner_id']!='' && $_REQUEST['s_id']!=''){
    $condi="where cleaner_id='".$_REQUEST['cleaner_id']."' and session_id='".$_REQUEST['s_id']."'";
    $userRec = mysql_query("SELECT cleaner_id FROM table_cleaner ".$condi);
    if(mysql_num_rows($userRec)<=0){
        $node = $dom->createElement("Sts");
        $newnode = $parnode->appendChild($node);
        $newnode->setAttribute("Code", "1004");
        $dom->formatOutput = true;
        echo $dom->saveXML();
        $end_datetime="End Date: ".date('d/m/Y H:i:s');
        $page="Payment History";
        file_put_contents("Log/Cleaner".$_REQUEST['cleaner_id'].".log",print_r($datetime,true)."\r\n", FILE_APPEND);
        file_put_contents("Log/Cleaner".$_REQUEST['cleaner_id'].".log",print_r($end_datetime,true)."\r\n", FILE_APPEND);
        file_put_contents("Log/Cleaner".$_REQUEST['cleaner_id'].".log",print_r($page,true)."\r\n", FILE_APPEND);
        file_put_contents("Log/Cleaner".$_REQUEST['cleaner_id'].".log",print_r($_REQUEST,true), FILE_APPEND);
        file_put_contents("Log/Cleaner".$_REQUEST['cleaner_id'].".log",print_r($dom->saveXML(),true), FILE_APPEND);
        mysql_close($con);
        die;
    }
}

$condi="where bd.cleaner_id='".$_REQUEST['cleaner_id']."' and ba.booking_status = 'CB'";
$sql = mysql_query("SELECT br.booking_id FROM table_booking_register as br "
        . "LEFT JOIN table_booking_details as bd on bd.booking_id = br.booking_id "
        . "LEFT JOIN table_booking_allotment as ba on ba.booking_detail_id = bd.booking_detail_id ".$condi);
if(mysql_num_rows($sql)<=0){	
    $node = $dom->createElement("Sts");
    $newnode = $parnode->appendChild($node);
    $newnode->setAttribute("Code", "1005");
    $dom->formatOutput = true;
    echo $dom->saveXML();
    $end_datetime="End Date: ".date('d/m/Y H:i:s');
    $page="Payment History";
    file_put_contents("Log/Cleaner".$_REQUEST['cleaner_id'].".log",print_r($datetime,true)."\r\n", FILE_APPEND);
    file_put_contents("Log/Cleaner".$_REQUEST['cleaner_id'].".log",print_r($end_datetime,true)."\r\n", FILE_APPEND);
    file_put_contents("Log/Cleaner".$_REQUEST['cleaner_id'].".log",print_r($page,true)."\r\n", FILE_APPEND);
    file_put_contents("Log/Cleaner".$_REQUEST['cleaner_id'].".log",print_r($_REQUEST,true), FILE_APPEND);
    file_put_contents("Log/Cleaner".$_REQUEST['cleaner_id'].".log",print_r($dom->saveXML(),true), FILE_APPEND);
    mysql_close($con);
    die;
}

$condi="where bd.cleaner_id='".$_REQUEST['cleaner_id']."' and ba.booking_status = 'CB'";
$paymentSql = mysql_query("select br.customer_id,bd.cleaning_date,tc.customer_name,th.payment_mode,br.total_cost_paid,cs.slot_start_time,cs.slot_end_time from table_booking_register as br "
        . "LEFT JOIN table_payment_history as th on th.payment_id = br.payment_id "
        . "LEFT JOIN table_customer as tc on tc.customer_id = br.customer_id "
        . "LEFT JOIN table_booking_details as bd on bd.booking_id = br.booking_id "
        . "LEFT JOIN table_booking_allotment as ba on ba.booking_detail_id = bd.booking_detail_id "
        . "LEFT JOIN table_calender_slots as cs on cs.slot_id = bd.slot_id ".$condi);
$node = $dom->createElement("Sts");
$newnode = $parnode->appendChild($node);
$newnode->setAttribute("Code", "0");

while($row = mysql_fetch_array($paymentSql)){
    $node = $dom->createElement("PaymentHistory");
    $newnode = $parnode->appendChild($node);
    $newnode->setAttribute("customer_name", htmlspecialchars($row['customer_name']));
    $newnode->setAttribute("payment_mode", htmlspecialchars($row['payment_mode']));
    $newnode->setAttribute("total_cost_paid", htmlspecialchars($row['total_cost_paid']));
    $newnode->setAttribute("slot_start_time", date("H:i A",strtotime($row['slot_start_time'])));
    $newnode->setAttribute("slot_end_time", date("H:i A",strtotime($row['slot_end_time'])));
    $newnode->setAttribute("cleaning_date",date("d M Y",strtotime($row['cleaning_date'])));
}

$dom->formatOutput = true;
echo $dom->saveXML();
$end_datetime="End Date: ".date('d/m/Y H:i:s');
$page="Payment History";
file_put_contents("Log/Cleaner".$_REQUEST['cleaner_id'].".log",print_r($datetime,true)."\r\n", FILE_APPEND);
file_put_contents("Log/Cleaner".$_REQUEST['cleaner_id'].".log",print_r($end_datetime,true)."\r\n", FILE_APPEND);
file_put_contents("Log/Cleaner".$_REQUEST['cleaner_id'].".log",print_r($page,true)."\r\n", FILE_APPEND);
file_put_contents("Log/Cleaner".$_REQUEST['cleaner_id'].".log",print_r($_REQUEST,true), FILE_APPEND);
file_put_contents("Log/Cleaner".$_REQUEST['cleaner_id'].".log",print_r($dom->saveXML(),true), FILE_APPEND);
mysql_close($con);
die;

?>
