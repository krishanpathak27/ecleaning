<?php 
include("../includes/config.inc.php");
header("Content-type: text/xml");
$dom = new DOMDocument('1.0', 'utf-8');
$node = $dom->createElement("WhiteSpot");
$parnode = $dom->appendChild($node);
if($_REQUEST['cleaner_id']=='' ||  $_REQUEST['s_id']=='' || $_REQUEST['booking_detail_id']==''){
    $node = $dom->createElement("Sts");
    $newnode = $parnode->appendChild($node);
    $newnode->setAttribute("Code", "1001");
    //$newnode->setAttribute("message", "Request Blank");
    $dom->formatOutput = true;
    echo $dom->saveXML();
    $datetime="Date: ".$date.", Time: ".$time;
    $page="Route Plan Detail";
    file_put_contents("Log/Cleaner".$_REQUEST['cleaner_id'].".log",print_r($datetime,true)."\r\n", FILE_APPEND);
    file_put_contents("Log/Cleaner".$_REQUEST['cleaner_id'].".log",print_r($page,true)."\r\n", FILE_APPEND);
    file_put_contents("Log/Cleaner".$_REQUEST['cleaner_id'].".log",print_r($_REQUEST,true), FILE_APPEND);
    file_put_contents("Log/Cleaner".$_REQUEST['cleaner_id'].".log",print_r($dom->saveXML(),true), FILE_APPEND);
    die;
}
$con = mysql_connect(MYSQL_HOST_NAME,MYSQL_USER_NAME,MYSQL_PASSWORD);
if (!$con) {
    $node = $dom->createElement("Sts");
    $newnode = $parnode->appendChild($node);
    $newnode->setAttribute("Code", "1002");
    //$newnode->setAttribute("message", "database not connect");
    $dom->formatOutput = true;
    echo $dom->saveXML();
    $datetime="Date: ".$date.", Time: ".$time;
    $page="Route Plan Detail";
    file_put_contents("Log/Cleaner".$_REQUEST['cleaner_id'].".log",print_r($datetime,true)."\r\n", FILE_APPEND);
    file_put_contents("Log/Cleaner".$_REQUEST['cleaner_id'].".log",print_r($page,true)."\r\n", FILE_APPEND);
    file_put_contents("Log/Cleaner".$_REQUEST['cleaner_id'].".log",print_r($_REQUEST,true), FILE_APPEND);
    file_put_contents("Log/Cleaner".$_REQUEST['cleaner_id'].".log",print_r($dom->saveXML(),true), FILE_APPEND);
    die;
}
mysql_select_db(MYSQL_DB_NAME, $con);
if($_REQUEST['cleaner_id']!='' && $_REQUEST['s_id']!=''){
    $condi="where cleaner_id='".$_REQUEST['cleaner_id']."' and session_id='".$_REQUEST['s_id']."'";
    $userRec = mysql_query("SELECT cleaner_id FROM table_cleaner ".$condi);
    if(mysql_num_rows($userRec)<=0){
        $node = $dom->createElement("Sts");
        $newnode = $parnode->appendChild($node);
        $newnode->setAttribute("Code", "1004");
        $dom->formatOutput = true;
        echo $dom->saveXML();
        $end_datetime="End Date: ".date('d/m/Y H:i:s');
        $page="Route Plan Detail";
        file_put_contents("Log/Cleaner".$_REQUEST['cleaner_id'].".log",print_r($datetime,true)."\r\n", FILE_APPEND);
        file_put_contents("Log/Cleaner".$_REQUEST['cleaner_id'].".log",print_r($end_datetime,true)."\r\n", FILE_APPEND);
        file_put_contents("Log/Cleaner".$_REQUEST['cleaner_id'].".log",print_r($page,true)."\r\n", FILE_APPEND);
        file_put_contents("Log/Cleaner".$_REQUEST['cleaner_id'].".log",print_r($_REQUEST,true), FILE_APPEND);
        file_put_contents("Log/Cleaner".$_REQUEST['cleaner_id'].".log",print_r($dom->saveXML(),true), FILE_APPEND);
        mysql_close($con);
        die;
    }
}

$condi="where DTL.booking_detail_id='".$_REQUEST['booking_detail_id']."' and REG.status = 'A'";
$routePlan = mysql_query("SELECT DTL.booking_id FROM table_booking_details as DTL left join table_booking_register as REG on REG.booking_id = DTL.booking_id ".$condi);

if(mysql_num_rows($routePlan)<=0){	
    $node = $dom->createElement("Sts");
    $newnode = $parnode->appendChild($node);
    $newnode->setAttribute("Code", "1005");
    $dom->formatOutput = true;
    echo $dom->saveXML();
    $end_datetime="End Date: ".date('d/m/Y H:i:s');
    $page="Route Plan Detail";
    file_put_contents("Log/Cleaner".$_REQUEST['cleaner_id'].".log",print_r($datetime,true)."\r\n", FILE_APPEND);
    file_put_contents("Log/Cleaner".$_REQUEST['cleaner_id'].".log",print_r($end_datetime,true)."\r\n", FILE_APPEND);
    file_put_contents("Log/Cleaner".$_REQUEST['cleaner_id'].".log",print_r($page,true)."\r\n", FILE_APPEND);
    file_put_contents("Log/Cleaner".$_REQUEST['cleaner_id'].".log",print_r($_REQUEST,true), FILE_APPEND);
    file_put_contents("Log/Cleaner".$_REQUEST['cleaner_id'].".log",print_r($dom->saveXML(),true), FILE_APPEND);
    mysql_close($con);
    die;
}
$condi=" where br.status = 'A' and bd.booking_detail_id = '".$_REQUEST['booking_detail_id']."' GROUP BY br.`booking_id`";
$routePlan = mysql_query("SELECT bd.booking_detail_id,br.booking_id,bd.cleaning_date,bd.area_sq_feets,br.customer_id,c.customer_name,c.customer_number,c.customer_email,c.image,br.cleaning_actual_cost,br.total_cost_paid,bd.special_instructions,b.building_name,ca.customer_address,cs.slot_start_time,cs.slot_end_time,alt.booking_status,ut.unit_name,ut.unit_type_id FROM table_booking_register as br "
. "left join table_customer as c on c.customer_id = br.customer_id "
        . "left join table_customer_address ca on br.customer_address_id = ca.customer_address_id "
        . "left join table_buildings as b on ca.building_id = b.building_id "
        . "left join table_booking_details as bd on bd.booking_id = br.booking_id "
        . "left join table_calender_slots as cs on cs.slot_id = bd.slot_id "
        . "LEFT JOIN table_booking_allotment as alt on alt.booking_detail_id = bd.booking_detail_id "
        . "LEFT JOIN table_booking_room_type_mapping AS rtm ON rtm.booking_id = br.booking_id "
        ."LEFT JOIN table_category_type AS ct ON ct.room_category_id = rtm.room_category_id "
        ."LEFT JOIN table_unit_type AS ut ON ct.unit_type_id = ut.unit_type_id ".$condi);

//conditions for room catogory 
$condi2=" where br.status = 'A' and bd.booking_detail_id = '".$_REQUEST['booking_detail_id']."' GROUP BY br.`booking_id`";
$roomCat = mysql_query("SELECT br.booking_id,rtm.counter,ct.room_category,ct.unit_type_id,ut.unit_name,ALOTTMENT.booking_status "
                ."FROM table_booking_register AS br "
                ."LEFT JOIN  table_booking_room_type_mapping AS rtm ON rtm.booking_id = br.booking_id "
                ."LEFT JOIN  table_category_type AS ct ON ct.room_category_id = rtm.room_category_id "
                ."LEFT JOIN table_unit_type AS ut ON ct.unit_type_id = ut.unit_type_id "
                ."LEFT JOIN table_booking_details AS bd ON bd.booking_id = br.booking_id "
                ."LEFT JOIN table_booking_allotment as ALOTTMENT on ALOTTMENT.booking_detail_id = bd.booking_detail_id  ".$condi2);
$roomCatArray = array();

while($row = mysql_fetch_array($roomCat)){
    $unitType = $row['unit_name'];
    $unitTypeId = $row['unit_type_id'];
    $string = $row['counter']." ".$row['room_category'];
    array_push($roomCatArray, $string);
}

$node = $dom->createElement("Sts");
$newnode = $parnode->appendChild($node);
$newnode->setAttribute("Code", "0");

while($row = mysql_fetch_array($routePlan)){
    $node = $dom->createElement("routePlanDetail");
    $newnode = $parnode->appendChild($node);
    $newnode->setAttribute("booking_detail_id", htmlspecialchars($row['booking_detail_id']));
    $newnode->setAttribute("booking_id", htmlspecialchars($row['booking_id']));
    $newnode->setAttribute("customer_id", htmlspecialchars($row['customer_id']));
    $newnode->setAttribute("customer_name", htmlspecialchars($row['customer_name']));
    $newnode->setAttribute("building_name", htmlspecialchars($row['building_name']));
    $newnode->setAttribute("customer_address", htmlspecialchars($row['customer_address']));
    $newnode->setAttribute("slot_start_time", date("H:i A",strtotime($row['slot_start_time'])));
    $newnode->setAttribute("slot_end_time", date("H:i A",strtotime($row['slot_end_time'])));
    $newnode->setAttribute("customer_number",htmlspecialchars($row['customer_number']));
    $newnode->setAttribute("customer_email",htmlspecialchars($row['customer_email']));
    $newnode->setAttribute("area_sq_feets",htmlspecialchars($row['area_sq_feets']));
    $newnode->setAttribute("booking_status",htmlspecialchars($row['booking_status']));
    $newnode->setAttribute("image","services/customer_image/".htmlspecialchars($row['image']));
    if($row['image'] !== NULL) {
        $newnode->setAttribute("special_instructions",htmlspecialchars($row['special_instructions']));
    } else {
        $newnode->setAttribute("special_instructions","");
    }
    $newnode->setAttribute("total_cost_paid", htmlspecialchars($row['total_cost_paid']));
    $newnode->setAttribute("cleaning_actual_cost", htmlspecialchars($row['cleaning_actual_cost']));
    $newnode->setAttribute("cleaning_date", date("d M Y",strtotime($row['cleaning_date'])));
    $newnode->setAttribute("unit_type_id", htmlspecialchars($row['unit_type_id']));
    $newnode->setAttribute("unit_type",htmlspecialchars($row['unit_name']));
    foreach($roomCatArray as $roomCatArrayTemp){
        $node = $dom->createElement("roomcategory");
        $newnode = $parnode->appendChild($node);
        $newnode->setAttribute("room_category", htmlspecialchars($roomCatArrayTemp));
    }
    
}
$dom->formatOutput = true;
echo $dom->saveXML();
$end_datetime="End Date: ".date('d/m/Y H:i:s');
$page="Route Plan Detail";
file_put_contents("Log/Cleaner".$_REQUEST['cleaner_id'].".log",print_r($datetime,true)."\r\n", FILE_APPEND);
file_put_contents("Log/Cleaner".$_REQUEST['cleaner_id'].".log",print_r($end_datetime,true)."\r\n", FILE_APPEND);
file_put_contents("Log/Cleaner".$_REQUEST['cleaner_id'].".log",print_r($page,true)."\r\n", FILE_APPEND);
file_put_contents("Log/Cleaner".$_REQUEST['cleaner_id'].".log",print_r($_REQUEST,true), FILE_APPEND);
file_put_contents("Log/Cleaner".$_REQUEST['cleaner_id'].".log",print_r($dom->saveXML(),true), FILE_APPEND);
mysql_close($con);
die;


?>
