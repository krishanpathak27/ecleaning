<?php
include("includes/config.inc.php");
include("includes/function.php");
//include("includes/globalarraylist.php");

$page_name = "WhiteSpot Cleaning Type";


$_objAdmin = new Admin();

if (isset($_REQUEST['stid']) && $_REQUEST['value'] != "") {
    if ($_REQUEST['value'] == 1) {
        $status = 'I';
    } else {
        $status = 'A';
    }
    $cid = $_objAdmin->_dbUpdate(array("status" => $status, "last_update_date" => $date), 'table_cleaning_type', "cleaning_type_id='" . $_REQUEST['stid'] . "'");
    header("Location: cleaningType.php?suc=Status Changes Successfully");
}

if (isset($_REQUEST['delete']) && $_REQUEST['delete'] == 'yes' && $_REQUEST['stid'] != "") {
    $id = $_objAdmin->_dbUpdate(array("last_update_date" => date('Y-m-d'), "status" => 'D'), 'table_cleaning_type', " cleaning_type_id='" . $_REQUEST['stid'] . "'");
    header("Location: cleaningType.php?suc=Deleted Successfully");
}
include("header.inc.php");
?>
<div class="m-grid__item m-grid__item--fluid m-wrapper" style="background-color:">
    <!-- BEGIN: Subheader -->
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="#" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__separator">
                        -
                    </li>
                    <li class="m-nav__item">
                        <a href="#" class="m-nav__link">
                            <span class="m-nav__link-text">
                                Master
                            </span>
                        </a>
                    </li>
                    <li class="m-nav__separator">
                        -
                    </li>
                    <li class="m-nav__item">
                        <a href="" class="m-nav__link">
                            <span class="m-nav__link-text">
                                Cleaning Type
                            </span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <!-- END: Subheader -->
    <div class="m-content">
        <?php 
            if (isset($_REQUEST['suc']) && $_REQUEST['suc'] != "") { ?>
            <div role="alert" style="background: #d7fbdc;" class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-dismissible m--margin-bottom-30">
                <div class="m-alert__icon" >
                        <i class="flaticon-exclamation m--font-brand"></i>
                </div>
                <div class="m-alert__text">
                       <?php echo  $_REQUEST['suc']; ?> 
                </div>
            </div>
            <?php } ?>
        <div class="m-portlet m-portlet--mobile">
            
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            Cleaning Type List
                        </h3>
                    </div>
                </div>
            </div>
            <div class="m-portlet__body">
                <!--begin: Search Form -->
                <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
                    <div class="row align-items-center">
                        <div class="col-xl-8 order-2 order-xl-1">
                            <div class="form-group m-form__group row align-items-center">
                                <div class="col-md-4">
                                    <div class="m-input-icon m-input-icon--left">
                                        <input type="text" class="form-control m-input" placeholder="Search..." id="generalSearch">
                                        <span class="m-input-icon__icon m-input-icon__icon--left">
                                            <span>
                                                <i class="la la-search"></i>
                                            </span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-4 order-1 order-xl-2 m--align-right add">
                            <a href="addCleaningType.php" class="btn btn-warning m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                                <span>
                                    <i class="la la-cart-plus"></i>
                                    <span>
                                        Add Cleaning Type
                                    </span>
                                </span>
                            </a>
                            <div class="m-separator m-separator--dashed d-xl-none "></div>
                        </div>
                    </div>
                </div>
                <!--end: Search Form -->
                <!--begin: Datatable -->
                <div class="unitType" id="cleaningType"></div>
                <!--end: Datatable -->
            </div>
        </div>
    </div>
</div>
</div>
</div>
<!-- start footer -->         
<?php include("footer.php") ?>
<script type="text/javascript" >
    var DefaultDatatableDemo = function () {
        //== Private functions

        // basic demo
        var demo = function () {
            var datatable = $('.unitType').mDatatable({
                data: {
                    type: 'remote',
                    source: {
                        read: {
                            url: '<?php echo $ajaxUrl . '?pageType=cleaningType' ?>'
                        }
                    },
                    pageSize: 20,
                    serverPaging: true,
                    serverFiltering: true,
                    serverSorting: true
                },
                layout: {
                    theme: 'default',
                    class: '',
                    scroll: true,
                    footer: false
                },
                sortable: true,
                filterable: false,
                pagination: true,
                search: {
                    input: $('#generalSearch')
                },
                columns: [{
                        field: "cleaning_type_id",
                        title: "#",
                        sortable: false,
                        width: 40,
                        selector: {class: 'm-checkbox--solid m-checkbox--brand'}
                    }, {
                        field: "cleaning_type",
                        title: "Cleaning Type",
                        sortable: 'asc'
                    },{
                        field: "unit_name",
                        title: "Unit Name",
                        sortable: 'asc'
                    }, {
                        field: "Status",
                        title: "Status",
                        width: 100,
                        locked: {right: 'xl'},
                        template: function (row) {
                            var status = {
                                1: {'title': 'Active', 'class': 'm-badge--brand'},
                                2: {'title': 'Inactive', 'class': 'm-badge--warning'}
                            };
                            return '<span class="m-badge ' + status[row.Status].class + ' m-badge--wide">' + status[row.Status].title + '</span>';

                        }
                    }, {
                        field: "Actions",
                        width: 110,
                        title: "Actions",
                        sortable: false,
                        locked: {right: 'xl'},
                        overflow: 'visible',
                        template: function (row, index, datatable) {
                            var dropup = (datatable.getPageSize() - index) <= 4 ? 'dropup' : '';
                            var botton = '\
                                                <a href="cleaningType.php?stid='+row.cleaning_type_id+'&value='+row.Status+'" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Change Status">\
                                                        <i class="la la-i-cursor"></i>\
                                        ';
                            var edit = '<?php echo $edit; ?>';
                            if(edit == 1) {
                                botton += '\<a href="addCleaningType.php?id='+row.cleaning_type_id+'" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only edit m-btn--pill" title="Edit Details">\
                                                        <i class="la la-edit"></i>\
                                                </a>\
                                        ';
                            }
                            var del = '<?php echo $del; ?>';
                            console.log(del);
                            if(del == 1) {
                                botton +='\
                                               \<a href="cleaningType.php?stid='+row.cleaning_type_id+'&delete=yes" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill delete" title="Delete">\
                                                        <i class="la la-trash"></i>\
                                                </a>\
                                        '
                            }
                            return botton;
                        }
                    }]
            });
        };

        return {
            // public functions
            init: function () {
                demo();
            }
        };
    }();

    jQuery(document).ready(function () {
        DefaultDatatableDemo.init();
    });



</script>


