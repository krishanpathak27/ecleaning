<?php
include("includes/config.inc.php");
include("includes/function.php");
$page_name="Company Login";
$_objAdmin = new Admin();
$_objItem = new Item();

if(isset($_REQUEST['id']) && $_REQUEST['id']!="")
{
	$auRec=$_objAdmin->_getSelectList2('table_web_users',"*",''," account_id='".$_REQUEST['id']."' and user_type = 1 and status='A'");//echo $auRec[0]->status;
	//if(count($auRec)<=0) header("Location: manage_account.php");
}

?>

<?php include("header.inc.php") ?>
<link rel="stylesheet" href="./base/jquery.ui.all.css">
<script src="./Autocomplete/jquery-1.5.1.js"></script>
<script src="./Autocomplete/jquery.ui.core.js"></script>
<script src="./Autocomplete/jquery.ui.widget.js"></script>
<script src="./Autocomplete/jquery.ui.button.js"></script>
<script src="./Autocomplete/jquery.ui.position.js"></script>
<script src="./Autocomplete/jquery.ui.menu.js"></script>
<script src="./Autocomplete/jquery.ui.autocomplete.js"></script>
<script src="./Autocomplete/jquery.ui.tooltip.js"></script>
<script src="./Autocomplete/jquery.ui.datepicker.js"></script>
<link rel="stylesheet" href="./base/demos.css">
<script type="text/javascript" src="./javascripts/validate.js"></script>
<style>
#username{
	padding:3px;
	font-size:12px;
	border:solid 1px #dc0000;
}

#tick{display:none}
#cross{display:none}
	

</style>

<script>
    $(function() {
        $( "#from" ).datepicker({
			dateFormat: "d M yy",
            defaultDate: "+1w",
            changeMonth: true,
            numberOfMonths: 3,
            onSelect: function( selectedDate ) {
                $( "#to" ).datepicker( "option", "minDate", selectedDate );
            }
        });
        $( "#to" ).datepicker({
			dateFormat: "d M yy",
            defaultDate: "+1w",
            changeMonth: true,
            numberOfMonths: 3,
            onSelect: function( selectedDate ) {
                $( "#from" ).datepicker( "option", "maxDate", selectedDate );
            }
        });
    });
</script>
<script>
$(document).ready(function(){
$('#username').keyup(username_check);
});
	
function username_check(){	
var username = $('#username').val();
if(username == "" || username.length < 1){
$('#username').css('border', 'solid 1px #dc0000');
$('#tick').hide();
}else{

jQuery.ajax({
   type: "POST",
   url: "check.php",
   data: 'username='+ username,
   cache: false,
   success: function(response){
if(response == 1){
	$('#username').css('border', 'solid 1px #dc0000');	
	$('#tick').hide();
	$('#cross').fadeIn();
	}else{
	$('#username').css('border', 'solid 1px #dc0000');
	$('#cross').hide();
	$('#tick').fadeIn();
	     }
}
});
}
}
</script>
	


<div class="clear"></div>
<!-- start content-outer -->
<div id="content-outer">
<!-- start content -->
	<div id="content">
	<div id="page-heading"><h1><span style="color: #d74343; font-weight: bold;">Add Company Account</span></h1></div>
		<table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">
		<tr>
			<td>
			<!--  start content-table-inner -->
			<div id="content-table-inner">
				<table border="0" width="100%" cellpadding="0" cellspacing="0">
				<tr valign="top">
					<td>
					<!--  start message-red -->
					
					<!--  end message-red -->
				

					<!--  start message-green -->
					




<!--  start step-holder -->
<div id="step-holder">
<div class="step-no-off">1</div>
<div class="step-light-left">Company Form</div>
<div class="step-light-right">&nbsp;</div>
<div class="step-no">2</div>
<div class="step-dark-left">Login Form</div>
<div class="step-dark-round">&nbsp;</div>
<div class="clear"></div>
</div>
<!--  end step-holder -->
	<!--  start message-red -->
	<?php if($login_err!=''){?>
	<div id="message-red">
		<table border="0" width="100%" cellpadding="0" cellspacing="0">
		<tr>
			<td class="red-left">Error. <?php echo $login_err; ?></td>
			<td class="red-right"><a class="close-red"><img src="images/icon_close_red.gif"   alt="" /></a></td>
		</tr>
		</table>
	</div>
	<?php } ?>
	 <?php if($_SESSION['user_exit']!=''){?>
	<div id="message-red">
		<table border="0" width="100%" cellpadding="0" cellspacing="0">
		<tr>
			<td class="red-left">Error. <?php echo $_SESSION['user_exit'];  unset($_SESSION['user_exit']);?></td>
			<td class="red-right"><a class="close-red"><img src="images/icon_close_red.gif"   alt="" /></a></td>
		</tr>
		</table>
	</div>
	<?php } ?>
	<!--  end message-red -->
	<!-- start id-form -->
	<form name="frmPre" id="frmPre" method="post" action="<?php if(isset($_REQUEST['id']) && $_REQUEST['id']!=""){ echo "edit_company_account.php?id=".$_REQUEST['id'];} else {echo "add_company_account.php";} ?>" enctype="multipart/form-data" >
		<table border="0" cellpadding="0" cellspacing="0"  id="id-form">
		<tr>
			<th valign="top">User name:</th>
			<td><input type="text" name="username" id="username" class="required" value="<?php echo $auRec[0]->username; ?>"/>
			<div id="tick"><span style=" color:#088A08;"><b>User Name Available</b></span><img src="images/tick.png" width="16" height="16"/></div>
			<div id="cross"><span style=" color:#FF0000;"><b>User Name Not Available</b></span><img  src="images/cross.png" width="16" height="16"/></div>
			</td>
			<td></td>
		</tr>
		<tr>
			<th valign="top">Password:</th>
			<td><input type="password" name="password" id="password" <?php if($auRec[0]->web_user_id!=''){ ?>class="text"<?php } else { ?>class="required minlength" <?php } ?>  minlength="8" /></td>
			<td>
			</td>
		</tr>
		<tr>
			<th valign="top">Confirm-Password:</th>
			<td><input type="password" name="conf_pass" id="conf_pass" equalto="#password" <?php if($auRec[0]->web_user_id!=''){ ?>class="text"<?php } else { ?>class="required" <?php } ?>/></td>
			<td>
			</td>
		</tr>
		<tr>
			<th valign="top">Email ID:</th>
			<td><input type="text" name="email" id="email" class="required email" value="<?php echo $auRec[0]->email_id; ?>"/></td>
			<td>
			</td>
		</tr>
		<th>&nbsp;</th>
		<td valign="top">
		<?php 
		if($_REQUEST['id']!=''){
				$auRecAc=$_objAdmin->_getSelectList2('table_account',"*",''," account_id='".$_REQUEST['id']."'");
		
			}
			else{
			$auRecAc=$_objAdmin->_getSelectList2('table_account',"*",''," account_id='".$_SESSION['CompId']."'");
			
			}
		?>
		
		
			<input type="hidden" name="comp_acc_id" value="<?php echo $_SESSION['CompId']; ?>" />
			<input type="hidden" name="web_id" value="<?php echo $auRec[0]->web_user_id; ?>"/>
			<input type="hidden" name="user_type" value="1" />
			<input type="hidden" name="comp_status" value="<?php echo $auRecAc[0]->status; ?>" />
			<input name="account_id" type="hidden" value="<?php echo $_REQUEST['id']; ?>" />
			<input name="start_date" type="hidden" value="<?php echo $auRecAc[0]->start_date; ?>" />
			<input name="end_date" type="hidden" value="<?php echo $auRecAc[0]->end_date; ?>" />
			<input name="comp_login" type="hidden" value="yes" />
			<input type="reset" value="Reset!" class="form-reset">
			<input name="submit" class="form-submit" type="submit" id="submit" value="Save Company" />
		</td>
		</table>
	  
	</form>