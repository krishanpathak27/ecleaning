<?php
include("includes/config.inc.php");
include("includes/function.php");
$_objAdmin = new Admin();

// print_r($_SESSION);
// if(isset($_REQUEST['id']) && $_REQUEST['value']!="")
// {
// 	if($_REQUEST['value']=="Active")
// 	{	
// 		$status='I';
// 	}
// 	else
// 	{
// 		$status='A';
// 	}
// 	$cid=$_objAdmin->_dbUpdate(array("status"=>$status),'table_company_hierarchy', " hierarchy_id='".$_REQUEST['id']."'");
// 	//$Typeid=$_objAdmin->_dbUpdate(array("status"=>$status),'table_user_type', " hierarchy_id='".$_REQUEST['id']."'");
// 	header("Location: company_hierarchy.php");
// }	




// if($_REQUEST['id']!="" && $_REQUEST['delete']="yes" )
// {

//         mysql_query("DELETE FROM table_company_hierarchy WHERE hierarchy_id='".$_REQUEST['id']."'");
// 		mysql_query("DELETE FROM table_user_type WHERE hierarchy_id='".$_REQUEST['id']."'AND parentType ='2'");
//            header("Location: company_hierarchy.php");
// }







if(isset($_REQUEST['show']) && $_REQUEST['show']=="yes")
{
	$_objAdmin->complaintHistoryASD();
	die;
}	

?>

<?php include("header.inc.php") ?>
 
 <style>
.sortable		{ padding:0; }
.sortable li	{ padding:4px 8px; color:#000; cursor:move; list-style:none; width:350px; background:#ddd; margin:10px 0; border:1px solid #999; }
#message-box		{ background:#fffea1; border:2px solid #fc0; padding:4px 8px; margin:0 0 14px 0; width:350px; }

h3
{
font-family:Arial,Helvetica,sans-serif !important;
font-size: 14px !important;
font-weight:normal !important;
}
</style>

<script type="text/javascript" src="javascripts/select_dropdowns.js"></script>
<!-- start content-outer -->
<div id="content-outer">
<!-- start content -->
<div id="content">
<div id="page-heading"><h1><span style="color: #d74343; font-family: Tahoma; font-weight: bold;"> Complaints History</span></h1></div>
<table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">
<!--<tr>
	<th rowspan="3" class="sized"><img src="images/side_shadowleft.jpg" width="20" height="300" alt="" /></th>
	<th class="topleft"></th>
	<td id="tbl-border-top">&nbsp;</td>
	<th class="topright"></th>
	<th rowspan="3" class="sized"><img src="images/side_shadowright.jpg" width="20" height="300" alt="" /></th>
</tr>-->
<tr>
	<td id="tbl-border-left"></td>
	<td>
	<!--  start content-table-inner -->
	<div id="content-table-inner">
	
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
	<tr valign="top">
	<td>
		<?php 

			if($_SESSION['userLoginType']==1)
			{
				$total_complaints = $_objAdmin->_getSelectList2('table_complaint','count(*) as total_complaints','',' call_center_id > 0');
				$pending_complaints = $_objAdmin->_getSelectList2('table_complaint','count(*) as pending','',' call_center_id > 0 and complain_status="P"');
				$inprocess_complaints = $_objAdmin->_getSelectList2('table_complaint','count(*) as inprocess','',' call_center_id > 0 and complain_status="I"');
				$completed_complaints = $_objAdmin->_getSelectList2('table_complaint','count(*) as completed','',' call_center_id > 0 and complain_status="C"');
				$rejected_complaints = $_objAdmin->_getSelectList2('table_complaint','count(*) as rejected','',' call_center_id > 0 and complain_status="R"');
				$tat1_complaints = $_objAdmin->_getSelectList2('table_complaint','SUM(cctat1) as sum_tat1','',' call_center_id > 0 and complain_status="I"');
				$tat2_complaints = $_objAdmin->_getSelectList2('table_complaint','SUM(cctat2) as sum_tat2','',' call_center_id > 0 and complain_status="I"');
		?>
			<table border="0" width="100%" cellpadding="0" cellspacing="0"> 
	      		<tr>      
	        		<td><h3>Total Complaints : <?php echo $total_complaints[0]->total_complaints;?></h3></td>
	        		<td><h3>Pending Complaints : <?php echo $pending_complaints[0]->pending;?></h3></td>
	        		<td><h3>In-Process Complaints : <?php echo $inprocess_complaints[0]->inprocess;?></h3></td>
	      		</tr>
	      		<tr>
	      			<td><h3>Completed Complaints : <?php echo $completed_complaints[0]->completed;?></h3></td>
	      			<td><h3>Rejected Complaints : <?php echo $rejected_complaints[0]->rejected;?></h3></td>
	      		</tr>
	      		<tr>
	      			<td colspan="3"><h3>Avg. TAT1 (time from Raising of Complaint to Reaching of SP) : <?php echo $tat1_complaints[0]->sum_tat1/$total_complaints[0]->total_complaints;?></h3></td>
	      		</tr>
	      		<tr>
	      			<td colspan="3"><h3>Avg. TAT2 (time from Raising of Complaint to Submitting BCF) : <?php echo $tat2_complaints[0]->sum_tat2/$total_complaints[0]->total_complaints;?></h3></td>
	      		</tr>
	    	</table>
	    <?php
	    	}
	    ?>
	    
		<!-- start id-form -->
		<table  border="0" cellspacing="0" cellpadding="0"  align="center" width="100%">
				<tr>
				<td align="lift" valign="top"  >
				<table id="flex1" style="display:none"></table>
					<script type="text/javascript">complaintHistoryASD();</script>
				</td>
				</tr>
				</table>
	<!-- end id-form  -->

	</td>
	<td>

	<!-- right bar-->
	<?php
		// if($_SESSION['userLoginType']==1) 
			include("rightbar/asd_complaints_bar.php");
	?>
</td>
</tr>
<tr>
<td></td>
<td></td>
</tr>
</table>
<div class="clear"></div>
</div>
<!--  end content-table-inner  -->
</td>
<td id="tbl-border-right"></td>
</tr>
<tr>
	<th class="sized bottomleft"></th>
	<td id="tbl-border-bottom">&nbsp;</td>
	<th class="sized bottomright"></th>
</tr>
</table>
<div class="clear">&nbsp;</div>
</div>
<!--  end content -->
<div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->
<div class="clear">&nbsp;</div> 
<!-- start footer -->         
<?php include("footer.php") ?>
<!-- end footer -->
 
</body>
</html>