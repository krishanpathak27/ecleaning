<?php
include("includes/config.inc.php");
include("includes/function.php");
$page_name="Completed Bookings";
$_objAdmin = new Admin();




//Made session for Booking status
if(isset($_POST['showBookingStatus']) && $_POST['showBookingStatus'] == 'yes'){	

	$_SESSION['bookingStatus']=$_POST['bookingStatus'];
	//header("Location: booking.php");
}
/***End booking status**/	
if(isset($_REQUEST['show']) && $_REQUEST['show']=="yes"){
    $_objAdmin->showCompletedBookings();
    die;
}


?>

<?php include("header.inc.php"); ?>

<div class="content-wrapper">
    <div class="container-fluid"> 
		<ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="#">Home</a>
        </li>
        <li class="breadcrumb-item active">Admin</li>
      </ol>
        
        
        
	  	<div class="row pdng-top-lg pdng-btm-lg">
                    
                    <div class="accordian">
                        <div class="panel-group">
	        <div class="col-lg-12 col-md-12 col-sm-12"> 
	          	<div id="content-table-inner" class="panel panel-default card_bg"> 
<div class="panel-heading">
              <h4 class="panel-title">Completed Bookings</h4>
              <hr>
            </div>
<div class="panel-body">


				
					 <div class="table-responsive table table-bordred table-striped">
								<!-- start id-form -->
								<table  border="0" cellspacing="0" cellpadding="0"  align="center" width="100%">
									<tr>
										<td align="lift" valign="top"  >
											<table id="flex1" style="display:none"></table>
											<script type="text/javascript">showCompletedBookings();</script>
										</td>
									</tr>
								</table>
							<!-- end id-form  -->
 					</div>

</div>
				</div>
			</div>
		</div>
                    </div>
                </div>
	</div>
</div>
	<div class="clear"></div> 
	<!-- start footer -->         
	<?php include("footer.php") ?>
	<!-- end footer -->
<!-- start content-outer -->
