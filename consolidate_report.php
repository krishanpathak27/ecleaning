<?php
include("includes/config.inc.php");
include("includes/function.php");
include("includes/globalarraylist.php");
$page_name="Consolidate Report";


if(isset($_POST['showReport']) && $_POST['showReport'] == 'yes')
{	
	if($_REQUEST['sal']!="") 
	{
		$_SESSION['rptSal']=$_REQUEST['sal'];
	}
	if($_REQUEST['from']!="") 
	{
		$_SESSION['rptForm']=date('Y-m-d', strtotime($_REQUEST['from']));	
	}
	if($_REQUEST['to']!="") 
	{
		$_SESSION['rptTo']=date('Y-m-d', strtotime($_REQUEST['to']));	
	}
} else {
	$_SESSION['rptForm']= date("Y-m-d");
	$_SESSION['rptTo']= date("Y-m-d");
 }

  if(isset($_POST['export']) == 'Export to Excel'){
 	if($_REQUEST['from']!="") 
	{
		$from_date = $_SESSION['rptForm']= date('Y-m-d', strtotime($_REQUEST['from']));	
	}
	if($_REQUEST['to']!="") 
	{
		$to_date = $_SESSION['rptTo'] = date('Y-m-d', strtotime($_REQUEST['to']));	
	}
	header("location: export.inc.php?export_consolidated_report&fdate=".$_SESSION['rptForm']."&tdate=".$_SESSION['rptTo']."");
} 
include("header.inc.php")
?>
<style type="text/css"> 
#page-heading { margin: 0 0 15px 2px; } 
.style1 { height:30px; background-color:#eaeaea; border:1px solid #666666; }
.style2 { height:28px; border-top:1px solid #666666; }
.style3 { border-bottom:1px solid #666666; font-size:16px; font-weight:bold; }
.headerContainer { float:left; position:relative; width:100%; }
.innerColumn1 { float:left; width:350px; height:auto; border:0px solid #000000; }
.innerColumn { float:left; width:200px; height:auto; border:0px solid #000000; }
.innerColumnlast { float:left; width:200px; height:auto; border:0px solid #000000; }
.parentColumn1 { float:left; width:90px; height:auto; border:0px solid #000000; font-weight:bold;}
.parentColumn { float:left; width:150px; height:20px; border:0px solid #000000; }
.headerColumn1 { float:left; margin-left:20px; width:300px; height:30px; border:0px solid #000000; }
.headerColumn { float:left; width:180px; height:auto; border:0px solid #000000; margin-left:10px; font-weight:bold;}
</style>
<link rel="stylesheet" href="css/jquery-ui-1.11.2.css">
  <script src="javascripts/jquery-1.8.3.js"></script>
  <script src="javascripts/jquery-ui-1.9.2.js"></script>
   <script src="javascripts/dateNextPrev.js" type="text/javascript"></script>
    <script>
    $(function() {
	  $("div.parentColumn").accordion({
		autoHeight: false,
		collapsible: true,
		active: false,
	});
  });
  </script>
    <script> 
  function exporttoexcel(){
  $(function () {
	  $("div.parentColumn").accordion({
		autoHeight: false,
		collapsible: true,
		active: false,
	});
  });
}
</script>
  <script>
  $(function() {
	  $("div.accordian").accordion({
		autoHeight: false,
		collapsible: true,
		active: false,
	});
  });
  </script>
 <!-- start content-outer -->
<div id="content-outer">
<!-- start content -->
<div id="content">
<div id="page-heading"><h1><span style="color: #d74343; font-family: Tahoma; font-weight: bold;">Consolidated Report</span></h1></div>
<table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">
<tr>
	<td id="tbl-border-left"></td>
	<td>
	<!--  start content-table-inner -->
	<div id="content-table-inner" style="line-height: 24px;">
	
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
	<tr valign="top">
	<td>
		<!-- start id-form -->
		<form name="frmPre" id="frmPre" method="post" action="<?php $_SERVER['PHP_SELF']; ?>" enctype="multipart/form-data" >
			<table border="0" width="100%" cellpadding="0" cellspacing="0">
				<tr>
				<td><h3>&nbsp;&nbsp;Salesman:</h3><h6>
	  	<select name="sal" id="sal" class="styledselect_form_5" style="" >
			<?php echo $rtndata = $_objArrayList->GetSalesmenMenu($salsList, $_SESSION['rptSal'], 'flex');?>
		</select></h6></td>
				<td><h3>From Date: </h3><h6><img src="css/images/prev.png" height="18" width="18" onclick="dateFromPrev();"> <input type="text" id="from" name="from" class="date" style="width:150px" value="<?php  echo $_objAdmin->_changeDate($_SESSION['rptForm']); ?>"  readonly /><img src="css/images/next.png" height="18" width="18" onclick="dateFromNext();"></h6></td>
				<td><h3>To Date: </h3><h6><img src="css/images/prev.png" height="18" width="18" onclick="dateToPrev();"> <input type="text" id="to" name="to" class="date" style="width:150px" value="<?php echo $_objAdmin->_changeDate($_SESSION['rptTo']); ?>"  readonly /><img src="css/images/next.png" height="18" width="18" onclick="dateToNext();"></h6></td>
				<td><h3></h3><input name="showReport" type="hidden" value="yes" />
				<input name="submit" class="result-submit" type="submit" id="submit" value="View Details" />
				<a id="dlink"  style="display:none;"></a>
				<input type="submit" name="export" value="Export to Excel" onclick="exporttoexcel();" class="result-submit">
				<input type="button" value="Reset!" class="form-reset" onclick="location.href='consolidate_report.php?reset=yes';" /></td>		
				</tr>
				
			</table>
	</form>
		
		<table  border="0" cellspacing="0" cellpadding="0"  align="center" width="100%">
				<tr>
				<td align="lift" valign="top"  >
				
	<table width="100%" border="0" cellspacing="5" cellpadding="5">
		  <tr class="style1">
		  	<td><div class="headerColumn">Hierarchy Level</td>
			<td><div class="headerColumn">Reporting To</td>
			<td><div class="parentColumn1">Individual Total Quantity</div></td>
			<td><div class="parentColumn1">Individual Total Amount</div></td>
			<td><div class="parentColumn1">Hierarchy Total Quantity</div></td>
			<td><div class="parentColumn1">Hierarchy Total Amount</div></td>
		  </tr>
  </table>
<div id="faqs-container" class="">
<?php

 if(isset($_SESSION['rptSal']) && $_SESSION['rptSal']!="All")
	{
	
		 $qrySet="select SH.sort_order from table_salesman_hierarchy_relationship as SHR LEFT JOIN table_salesman_hierarchy as SH ON SHR.hierarchy_id=SH.hierarchy_id WHERE SH.account_id=".$_SESSION['accountId']." AND SHR.salesman_id='".$_SESSION['rptSal']."'";
		
		 $resultSet=mysql_query($qrySet);
		 $row = mysql_fetch_assoc($resultSet);
		 $curLevelOrder=$row['sort_order']; 
		 $salID=array($_SESSION['rptSal']);
		 $getList=$_objArrayList->getSalesbottomhierarchy($salID, $curLevelOrder);
		 if(!empty($getList)) {
					 $getList=implode(',',$getList);
					 $salesman = " AND SHR.salesman_id IN ($getList)";
		 }
		 else {
		 	$salesman = " AND SHR.salesman_id ='".$_SESSION['rptSal']."'";
		 
		 }
				
	} 

  $qry="select s.salesman_id, s.salesman_name, SHR.rpt_user_id AS parent_id, SH.description AS SalLevel,SH.sort_order, S2.salesman_name AS rptPerson, H2.description AS rptLevel
 	    from table_salesman_hierarchy_relationship AS SHR 
        LEFT JOIN table_salesman AS s ON s.salesman_id = SHR.salesman_id 
        LEFT JOIN table_salesman_hierarchy AS SH ON SH.hierarchy_id = SHR.hierarchy_id
        LEFT JOIN table_salesman_hierarchy AS H2 ON H2.hierarchy_id = SHR.rpt_hierarchy_id
		LEFT JOIN table_salesman AS S2 ON S2.salesman_id = SHR.rpt_user_id

 
  WHERE SH.account_id=".$_SESSION['accountId']." $salesman ORDER BY SH.sort_order";
  
 $result=mysql_query($qry);


 $salesmanHirearchySet = array();

 while($row = mysql_fetch_assoc($result)){ 

	 $salesmanHirearchySet[$row['salesman_id']] = array("parent_id" => $row['parent_id'], "rptPerson" =>$row['rptPerson'], 'rptLevel'=>$row['rptLevel'], "salesman_name" => $row['salesman_name'],'level'=>$row['SalLevel'],'salesman_id'=>$row['salesman_id'],'sort_order'=>$row['sort_order']);   
  }

 function createTree($array, $currentParent, $currLevel = 0, $prevLevel = -1, $sort_order=0) {
 
 		
foreach ($array as $key => $value) {
	
if ($currentParent == $value['parent_id'] && $sort_order < $value['sort_order']) 
{ 
	
		$qrySelAmount="SELECT SUM(O.acc_total_invoice_amount) as total
		FROM table_order as O
		left join table_retailer as R on O.retailer_id=R.retailer_id
		left join table_salesman as s on O.salesman_id=s.salesman_id 
		left join table_distributors as d on O.distributor_id=d.distributor_id
		WHERE O.salesman_id='".$value['salesman_id']."' and O.date_of_order >='".$_SESSION['rptForm']."' AND O.date_of_order <='".$_SESSION['rptTo']."' and R.status='A'";
		
		
		$qrySelQuantity="SELECT SUM(OD.acc_quantity) as totalQuantity
		FROM table_order as O
		left join table_order_detail as OD on O.order_id=OD.order_id 
		left join table_retailer as R on O.retailer_id=R.retailer_id
		left join table_salesman as s on O.salesman_id=s.salesman_id 
		left join table_distributors as d on O.distributor_id=d.distributor_id
		WHERE O.salesman_id='".$value['salesman_id']."' and OD.total_free_quantity='' AND O.date_of_order >='".$_SESSION['rptForm']."' AND O.date_of_order <='".$_SESSION['rptTo']."' and R.status='A' AND R.new='' AND d.status='A' AND d.new=''";
		
		
 	$resultSelAmount=mysql_query($qrySelAmount);
	$rowSelAmount = mysql_fetch_assoc($resultSelAmount);
	
	$resultSelQuantity=mysql_query($qrySelQuantity);
	$rowSelQuantity = mysql_fetch_assoc($resultSelQuantity);
	
	$salesmanTotal=$rowSelAmount['total'];
	$salesmanTotalQuantity=$rowSelQuantity['totalQuantity'];
	
	$salID=array($value['salesman_id']);
	$sortOrder=$value['sort_order'];
	
		$objArrayList= new ArrayList();
		$getList=$objArrayList->getSalesbottomhierarchy($salID, $sortOrder);

		$getList=implode(',',$getList);
		
		$levelWiseAmountSum="SELECT SUM(O.acc_total_invoice_amount) as total
		FROM table_order as O		
		left join table_retailer as R ON O.retailer_id=R.retailer_id
		left join table_salesman as s on O.salesman_id=s.salesman_id 
		left join table_distributors as d on O.distributor_id=d.distributor_id
		WHERE O.salesman_id IN ($getList) AND O.date_of_order >='".$_SESSION['rptForm']."' AND O.date_of_order <='".$_SESSION['rptTo']."'";
		
		$levelWiseQuantitySum="SELECT SUM(OD.acc_quantity) as totalQuantity
		FROM table_order as O
		left join table_order_detail as OD on O.order_id=OD.order_id
		left join table_retailer as R ON O.retailer_id=R.retailer_id
		left join table_salesman as s on O.salesman_id=s.salesman_id 
		left join table_distributors as d on O.distributor_id=d.distributor_id
		WHERE O.salesman_id IN ($getList) AND OD.total_free_quantity='' AND O.date_of_order >='".$_SESSION['rptForm']."' AND O.date_of_order <='".$_SESSION['rptTo']."' and R.status='A' AND R.new='' AND d.status='A' AND d.new=''";
		
		
		$resultAmountSum=mysql_query($levelWiseAmountSum);
		$salAmountSum = mysql_fetch_assoc($resultAmountSum);
		
		$resultQuantitySum=mysql_query($levelWiseQuantitySum);
		$salQuantitySum = mysql_fetch_assoc($resultQuantitySum);
		
		$levelTotal=$salAmountSum['total'];
		$levelTotalQuantity=$salQuantitySum['totalQuantity'];
	               

    if($currLevel > $prevLevel) echo "</h3><div class='accordian'><h3>"; //echo "<br/>"; 
	else if($currLevel == $prevLevel) echo "</h3><br/><h3>";  
	
	$selfTotalQuantity  = $salesmanTotalQuantity ? $salesmanTotalQuantity : 0;
	$selfTotalAmount    = $salesmanTotal ? $salesmanTotal : '0.00';
	$levelTotalQuantity = $levelTotalQuantity ? $levelTotalQuantity : 0;
	$levelTotalAmount   = $levelTotal ? $levelTotal : '0.00';
	if($value['parent_id'] == 0){
		$value['rptLevel'] = 'N/A';
	}
    	echo '<table width="100%" border="0">
			  <tr >
				<td><div class="headerColumn">'.$value['salesman_name'].' ('.$value['level'].')</div></td>
				<td><div class="headerColumn">'.$value['rptPerson'].' ('.$value['rptLevel'].')</div></td>
				<td><div class="parentColumn1">'.$selfTotalQuantity.'</div></td>
				<td><div class="parentColumn1">'.$selfTotalAmount.'</div></td>
				<td><div class="parentColumn1">'.$levelTotalQuantity.'</div></td>
				<td><div class="parentColumn1">'.$levelTotalAmount.'</div></td>
			  </tr>
			 </table>';
	     
    if ($currLevel > $prevLevel) { $prevLevel = $currLevel; } 
    $currLevel++; 
			
    createTree ($array, $key, $currLevel, $prevLevel,$value['sort_order']);
	
    $currLevel--;      

    }  

}

if ($currLevel == $prevLevel) echo " </div>"; echo "</br>";
}
?>
<?php
 if(mysql_num_rows($result)!=0)
 {
?>
<?php 
	if($_SESSION['userLoginType']=='5' || $_SESSION['rptSal']!=''){
	
	
	if(isset($_SESSION['userLoginType'])){ $salesmanID=$_SESSION['salesmanId']; }
	if(isset($_SESSION['rptSal'])){ $salesmanID=$_SESSION['rptSal']; }
	//echo $salesmanID;
 $qrySetResult="select rpt_user_id
  from table_salesman_hierarchy_relationship 
  WHERE account_id=".$_SESSION['accountId']." AND salesman_id='".$salesmanID."'";
 $resultSetquery=mysql_query($qrySetResult);
 	$row = mysql_fetch_assoc($resultSetquery);
	$curLevel=$row['rpt_user_id']; 
 if($row['rpt_user_id']=='')
 	{
		$curLevel=0;
	}
 }
 else {
 	$curLevel=0;
 }
 createTree($salesmanHirearchySet, $curLevel); ?>
<?php
}
?>
</div>
</td>
				</tr>
				</table>
	<!-- end id-form  -->

	</td>
	<td>

	<!-- right bar-->
</td>
</tr>

</table>
<div class="clear"></div>
</div>
<!--  end content-table-inner  -->
</td>
<td id="tbl-border-right"></td>
</tr>
<tr>
	<th class="sized bottomleft"></th>
	<td id="tbl-border-bottom">&nbsp;</td>
	<th class="sized bottomright"></th>
</tr>
</table>
<div class="clear">&nbsp;</div>
</div>
<!--  end content -->
<div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->
<div class="clear">&nbsp;</div> 
<!-- start footer -->         
<?php include("footer.php") ?>
<!-- end footer -->
<script>

	$(document).ready(function () {
		$("br").hide();
	});
	
    $(function() {
        $( "#from" ).datepicker({
			dateFormat: "d M yy",
            defaultDate: "w",
            changeMonth: true,
            numberOfMonths: 1,
            onSelect: function( selectedDate ) {
                $( "#to" ).datepicker( "option", "minDate", selectedDate );
            }
        });
        $( "#to" ).datepicker({
			dateFormat: "d M yy",
            defaultDate: "-w",
            changeMonth: true,
            numberOfMonths: 1,
            onSelect: function( selectedDate ) {
                $( "#from" ).datepicker( "option", "maxDate", selectedDate );
            }
        });
    });
</script>
</body>
</html>