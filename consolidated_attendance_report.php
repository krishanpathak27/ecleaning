<?php
/***********************************************************************************
** Formatting of code and resolved issue related to absent in hierarchy by Gyanendra. 
** Date 21st jan 2015
************************************************************************************/

include("includes/config.inc.php");
include("includes/function.php");
include("includes/globalarraylist.php");
$page_name="Consolidated Attendance Report";
$bottomLevel = "";

if(isset($_POST['showReport']) && $_POST['showReport'] == 'yes')
{	
	if($_REQUEST['sal']!="") 
	{
		$_SESSION['rptSal']=$_REQUEST['sal'];
	}
	if($_REQUEST['from']!="") 
	{
		$_SESSION['rptForm']=date('Y-m-d', strtotime($_REQUEST['from']));	
	}
	if($_REQUEST['to']!="") 
	{
		$_SESSION['rptTo']=date('Y-m-d', strtotime($_REQUEST['to']));	
	}
	if($_POST['gap']!="") 
	{
		$_SESSION['GapTime']=$_POST['gap'];	
	}
} else {
	$_SESSION['rptForm']= date("Y-m-d");
	$_SESSION['rptTo']= date("Y-m-d");
	$_SESSION['GapTime']="60";	

 }
 
 if(isset($_REQUEST['reset']) && $_REQUEST['reset'] == 'yes')
{
	unset($_SESSION['rptSal']);
	unset($_SESSION['GapTime']);
	header("Location: consolidated_attendance_report.php");
}
  if(isset($_POST['export']) == 'Export to Excel'){
 	if($_REQUEST['from']!="") 
	{
		$from_date = $_SESSION['rptForm']= date('Y-m-d', strtotime($_REQUEST['from']));	
	}
	if($_REQUEST['to']!="") 
	{
		$to_date = $_SESSION['rptTo'] = date('Y-m-d', strtotime($_REQUEST['to']));	
	}
	header("location: export.inc.php?export_consolidated_attendance_report&fdate=".$_SESSION['rptForm']."&tdate=".$_SESSION['rptTo']."");
} 
 
include("header.inc.php")
?>
<style type="text/css"> 
.headerColumn1 { float:left; margin-left:20px; width:180px; height:30px; border:0px solid #000000; }
.headerColumn { float:left; width:180px; height:auto; border:0px solid #000000; margin-left:10px; font-weight:bold;}
.headerColumnDay { float:left; width:auto; height:30px; padding:10px; border:0px solid #000000;}
.parentColumn1 { float:left; width:730px; height:auto; border:0px solid #000000; font-weight:bold;}
.parentColumn { float:left; width:150px; height:20px; border:0px solid #000000; text-align:center; }
.parentColumnDay { float:left; width:2000px; height:90px; border:0px solid #000000;}

.classRowSpan { border-top:1px solid #000000;border-left:1px solid #000000;border-bottom:1px solid #000000;  padding:3px;  width:1%;}
.dayclass { width:333px; position: relative; border:1px solid #000000; padding:3px; }
</style>
<link rel="stylesheet" href="css/jquery-ui-1.11.2.css">
  <script src="javascripts/jquery-1.8.3.js"></script>
  <script src="javascripts/jquery-ui-1.9.2.js"></script>
   <script src="javascripts/dateNextPrev.js" type="text/javascript"></script>
   <script>
       $(function() {
	  $("div.parentColumn").accordion({
		autoHeight: false,
		collapsible: true,
		active: false,
	});
  });
  </script>
    <script> 
  function exporttoexcel(){
  $(function () {
	  $("div.parentColumn").accordion({
		autoHeight: false,
		collapsible: true,
		active: false,
	});
  });
}
</script>
  <script>
  $(function() {
	  $("div.accordian").accordion({
		autoHeight: false,
		collapsible: true,
		active: false,
	});
  });
  </script>
 <!-- start content-outer -->
<div id="content-outer">
<!-- start content -->
<div id="content">
<div id="page-heading"><h1><span style="color: #d74343; font-family: Tahoma; font-weight: bold;">Consolidated Attendance Report</span></h1></div>
<table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">
<tr>
	<td id="tbl-border-left"></td>
	<td>
	<!--  start content-table-inner -->
	<div id="content-table-inner" style="line-height: 24px;">
	
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
	<tr valign="top">
	<td>
		<!-- start id-form -->
		<form name="frmPre" id="frmPre" method="post" action="<?php $_SERVER['PHP_SELF']; ?>" enctype="multipart/form-data" >
			<table border="0" width="100%" cellpadding="0" cellspacing="0">
				<tr>
				<td><h3>&nbsp;&nbsp;Salesman:</h3><h6>
	  	<select name="sal" id="sal" class="styledselect_form_5" style="" >
			<?php echo $rtndata = $_objArrayList->GetSalesmenMenu($salsList, $_SESSION['rptSal'], 'flex');?>
		</select></h6></td>
		<td ><!--<h3>Time Gap: </h3>
		<h6><select name="gap" id="gap" class="" style="border: solid 1px #BDBDBD; color: #393939;font-family: Arial;font-size: 12px;border-radius:5px ;-moz-border-radius:5px;-o-border-radius:5px;-ms-border-radius:5px;-webkit-border-radius:5px;width: 100px;height: 30px;" >
		<option value="60" <?php //if($_SESSION['GapTime']==60) echo "selected"?>>1 hour</option>
		<option value="30" <?php //if($_SESSION['GapTime']==30) echo "selected"?>>30 min</option>
		<option value="15" <?php //if($_SESSION['GapTime']==15) echo "selected"?>>15 min</option>
		</select></h6>--></td>
				<td><h3>From Date: </h3><h6><img src="css/images/prev.png" height="18" width="18" onclick="dateFromPrev();"> <input type="text" id="from" name="from" class="date" style="width:150px" value="<?php  echo $_objAdmin->_changeDate($_SESSION['rptForm']); ?>"  readonly /><img src="css/images/next.png" height="18" width="18" onclick="dateFromNext();"></h6></td>
				<td><h3>To Date: </h3><h6><img src="css/images/prev.png" height="18" width="18" onclick="dateToPrev();"> <input type="text" id="to" name="to" class="date" style="width:150px" value="<?php echo $_objAdmin->_changeDate($_SESSION['rptTo']); ?>"  readonly /><img src="css/images/next.png" height="18" width="18" onclick="dateToNext();"></h6></td>
				<td><h3></h3><input name="showReport" type="hidden" value="yes" /></td>	

				</tr>
				<tr>
				<td></td>
				<td></td>
				<td></td>
				<td>
				<input name="submit" class="result-submit" type="submit" id="submit" value="View Details" />
				<input type="button" value="Reset!" class="form-reset" onclick="location.href='consolidated_attendance_report.php?reset=yes';" />
				<input type="submit" name="export" value="Export to Excel" onclick="exporttoexcel();" class="result-submit">
				</td>
				</tr>				
			</table>
	</form>
		<div style="overflow-y:scroll; width:1000px;  height:500px;">
				<table  border="0" cellspacing="0" cellpadding="0"  align="center" width="100%">
				<tr>
				<td align="lift" valign="top"  >
<div id="faqs-container" class="">
<?php
 if(isset($_SESSION['rptSal']) && $_SESSION['rptSal']!="All")
	{
		$appUserActivityID = "";

		 $qrySet="select SH.sort_order from table_salesman_hierarchy_relationship as SHR LEFT JOIN table_salesman_hierarchy as SH ON SHR.hierarchy_id=SH.hierarchy_id WHERE SH.account_id=".$_SESSION['accountId']." AND SHR.salesman_id='".$_SESSION['rptSal']."'";
		
		 $resultSet 	= mysql_query($qrySet);
		 $row 			= mysql_fetch_assoc($resultSet);
		 $curLevelOrder = $row['sort_order']; 
		 $salID         = array($_SESSION['rptSal']);

		 $getList = $_objArrayList->getSalesbottomhierarchy($salID, $curLevelOrder);

		 if(!empty($getList)) {
					 $getList=implode(',',$getList);
					 $salesman = " AND SHR.salesman_id IN ($getList)";
		 } else {
		 	$salesman = " AND SHR.salesman_id ='".$_SESSION['rptSal']."'";		 
		 }
				
	} 
	
	function DateCheck($start_date,$end_date){

				$start      = strtotime($start_date);
				$end        = strtotime($end_date);
				$new_date   = array($start_date);
				$date_arr   = array();
				$days_count = ceil(abs($end - $start) / 86400)+1;

				for($i=0;$i<$days_count;$i++)
					{
						$date 			= strtotime ('+1 days', strtotime($start_date)) ;
						$start_date 	= date ('Y-m-d' , $date);
						$date_arr[$i] 	= $start_date;
						$week_last_date	= $start_date;
						array_push($new_date, $week_last_date);
						$get_newdate[] = array($new_date[$i], $new_date[$i+1]);
					}
					return $get_newdate;
				}	
			
		$dayArray = array();
			
		$d1 = "".$_SESSION['rptForm']."";
		$d2 = "".$_SESSION['rptTo']."";

		$duration_dates = DateCheck($d1,$d2);

		foreach($duration_dates as $value)
		{
			$duration_start_date = $value[0];
			$dayArray[]          = date('D',strtotime($duration_start_date));
			
		}
		
		$dayCount = count($dayArray);
		
	function convertToHoursMins($time, $format = '%d:%d') {
    	settype($time, 'integer');
    		if ($time < 1) {
        	return;
    	}
   		$hours = floor($time/60);
    	$minutes = $time%60;

    	return sprintf($format, $hours, $minutes);
	}
	 /***************************** get bottom level from hierarchy **********************************/
 	  $horder = "select TSH.sort_order AS sort_order from table_salesman_hierarchy AS TSH WHERE TSH.account_id=".$_SESSION['accountId']." ORDER BY TSH.sort_order DESC LIMIT 1";
 	    
 		$lastLevel = mysql_query($horder);
 			while($lastLevelOrder = mysql_fetch_assoc($lastLevel)){ 
 					$bottomLevel = $lastLevelOrder['sort_order'];
 			}
	
  		$qry = "select s.salesman_id, s.salesman_name, SHR.rpt_user_id AS parent_id, SH.description AS SalLevel,SH.sort_order
  				from table_salesman_hierarchy_relationship AS SHR 
 				 LEFT JOIN table_salesman AS s ON s.salesman_id = SHR.salesman_id 
 				 LEFT JOIN table_salesman_hierarchy AS SH ON SHR.hierarchy_id = SH.hierarchy_id
 				 WHERE SH.account_id=".$_SESSION['accountId']." $salesman ORDER BY SH.sort_order";
  
 		$result               = mysql_query($qry);
 		$salesmanHirearchySet = array();

		while($row = mysql_fetch_assoc($result)){ 

 		/****************** Start Calculation For Individual Salesman *******************/
 
 		$getTotalWorkingHours = mysql_query("SELECT SEC_TO_TIME(SUM(TIME_TO_SEC(diffe))) as timeSum FROM salesman_working_time WHERE salesman_id=".$row['salesman_id']." AND activity_date >='".$_SESSION['rptForm']."' AND activity_date <='".$_SESSION['rptTo']."'");
		 $individualWorkinghours = mysql_fetch_assoc($getTotalWorkingHours);		 
		 
		 if($individualWorkinghours['timeSum']>0 && !empty($individualWorkinghours['timeSum'])){		 
		 	
				$salIndiWorkingTime = date("H:i:s",strtotime("-20 minutes",strtotime($individualWorkinghours['timeSum'])));
					 
			 } else {
			 	$salIndiWorkingTime="NA";
			 }	

		/************************* To get total present individually *************************************/	 

	 	$getTotalPresent = mysql_query("SELECT count(activity_id) as totalPresent FROM act_in WHERE salesman_id='".$row['salesman_id']."' AND activity_date >='".$_SESSION['rptForm']."' AND activity_date <='".$_SESSION['rptTo']."'");
	 
			 $individualPresent = mysql_fetch_assoc($getTotalPresent);
	 
			if($individualPresent['totalPresent']>0 && !empty($individualPresent['totalPresent'])){

		 		$salIndiPresent = $individualPresent['totalPresent'];

			 } else {
			 	$salIndiPresent = "NA";
			 }
 		/************************* To get total Absent individually *************************************/	

		$salIndividualAbsent = $dayCount - $salIndiPresent;
		
		/************************* To get total Missing hours individually *************************************/

		$getIndividualGap = mysql_query("SELECT salesman_id,ref_type,ref_id,activity_date,end_time,activity_id,Sec_to_time(@diff) AS starttime,end_time,IF(@diff = 0, 0,Time_to_sec(end_time) - @diff)/60 AS diff,@diff := Time_to_sec(end_time) FROM table_activity,(SELECT @diff := 0) AS x WHERE salesman_id='".$row['salesman_id']."' and activity_date>='".$_SESSION['rptForm']."' and activity_date<='".$_SESSION['rptTo']."' and activity_type in (3,4,5,10,11,12) ORDER  BY table_activity.activity_date asc, table_activity.end_time asc");
		
		while($resultIndividualGap = mysql_fetch_assoc($getIndividualGap)){
		
			if($resultIndividualGap['diff'] > $_SESSION['GapTime']){ 
		
				if($resultIndividualGap['diff']>60){
					$individualMissingHours = convertToHoursMins(floor($resultIndividualGap['diff']), '%d hours %d minutes');
				} else {
					$individualMissingHours = floor($resultIndividualGap['diff'])." minutes";
				}
			} else {		
				$individualMissingHours= "NA";
			}
		}
		
		/****************************** End Calculation For Individual Salesman *******************************/

	/**************************** Start Calculation For Hirearchy Of Salesman ************************************/
	
		$salID        = array($row['salesman_id']);
		$sortOrder    = $row['sort_order'];
		$objArrayList = new ArrayList();
		$getList      = $objArrayList->getSalesbottomhierarchy($salID, $sortOrder);
		$getHierList  = $getList;
		

		/******************** find out whether user level is app user or not *******************************/
		unset($appUser);
		unset($appUserSalID);
		for($i=0; $i<count($getHierList); $i++){
			
			$getActivityId = mysql_query("SELECT activity_id as activity_id FROM table_activity WHERE salesman_id =".$getHierList[$i]." AND activity_date >='".$_SESSION['rptForm']."' AND activity_date <='".$_SESSION['rptTo']."' AND activity_type in (3,4,5,10,11,12)");
			
			$activityID = mysql_fetch_assoc($getActivityId);  
			if($activityID['activity_id']>0){
				 $appUser[] 	 = $activityID['activity_id'];
				 $appUserSalID[] = $getHierList[$i];
			}
		}

		$appUserCount = count($appUser); 
		$getList      = implode(',',$appUserSalID);

		/************* Calculation of Working Hours of Hirearchy ****************/

		$getTotalWorkingHoursHierarchy = mysql_query("SELECT SEC_TO_TIME(SUM(TIME_TO_SEC(diffe))) as timeSumHier FROM salesman_working_time WHERE salesman_id in (".$getList.") AND activity_date >='".$_SESSION['rptForm']."' AND activity_date <='".$_SESSION['rptTo']."'");
		
		$hierarchyWorkinghours = mysql_fetch_assoc($getTotalWorkingHoursHierarchy);
		
		 if($hierarchyWorkinghours['timeSumHier']>0 && !empty($hierarchyWorkinghours['timeSumHier'])){				
				$salHierWorkingTime = $hierarchyWorkinghours['timeSumHier']; 
				$salHierWorkingTime = date("H:i:s",strtotime("-20 minutes",strtotime($hierarchyWorkinghours['timeSumHier'])));
		} else {
				$salHierWorkingTime="NA"; 
		}
		
		/************* Calculation of total present of Hirearchy ****************/

		$getTotalHirearchyPresent = mysql_query("SELECT count(activity_id) as totalhierPresent FROM act_in WHERE salesman_id in (".$getList.") AND activity_date >='".$_SESSION['rptForm']."' AND activity_date <='".$_SESSION['rptTo']."'");
	 	
	 	$hierarchyPresent = mysql_fetch_assoc($getTotalHirearchyPresent);	
	 
	 	if($hierarchyPresent['totalhierPresent']>0 && !empty($hierarchyPresent['totalhierPresent'])){		 
		 		$salhiearchyPresent = $hierarchyPresent['totalhierPresent']; 		 
		} else {
			   $salhiearchyPresent = "NA";
		}
		
		/************* Calculation of total Absent of Hirearchy ****************/

			$salHierarchyAbsent = $dayCount*$appUserCount-$salhiearchyPresent;
		
		/************* Calculation of Missing Hours of Hirearchy ****************/

		$hierarchyMissingHours = "";

		$getHierarchyGap=mysql_query("SELECT Sec_to_time(@diff) AS starttime,end_time,IF(@diff = 0, 0,Time_to_sec(end_time) - @diff)/60 AS diff,@diff := Time_to_sec(end_time) FROM table_activity,(SELECT @diff := 0) AS x WHERE salesman_id IN (".$getList.") and activity_date>='".$_SESSION['rptForm']."' and activity_date<='".$_SESSION['rptTo']."' and activity_type in (3,4,5,10,11,12) ORDER  BY table_activity.activity_date asc, table_activity.end_time asc");
		
			while($resultHierarchyGap = mysql_fetch_assoc($getHierarchyGap)){
		
					if($resultHierarchyGap['diff']>$_SESSION['GapTime'] ){ 
				
						if($resultHierarchyGap['diff']>60){
								$hierarchyMissingHours = convertToHoursMins(floor($resultHierarchyGap['diff']), '%d hours %d minutes');
						} else {
								$hierarchyMissingHours = floor($resultHierarchyGap['diff'])." minutes";
						}
					} else {			
						$hierarchyMissingHours = "NA";
					}
			}

	 /***************************** End Calculation For Hirearchy Of Salesman ***********************************/

	 $salesmanHirearchySet[$row['salesman_id']] = array("parent_id" => $row['parent_id'], "salesman_name" => $row['salesman_name'],'level'=>$row['SalLevel'],'salesman_id'=>$row['salesman_id'],'sort_order'=>$row['sort_order'],'individualWorking'=>$salIndiWorkingTime,'hierarchyWorking'=>$salHierWorkingTime,'individualTotalPresent'=>$salIndiPresent,'hierarchyTotalPresent'=>$salhiearchyPresent,'individualAbsent'=>$salIndividualAbsent,'hierarchyAbsent'=>$salHierarchyAbsent,'individualMissingWorkingTime'=>$individualMissingHours,'hierarchyMissingWorkingTime'=>$hierarchyMissingHours, 'bottomLevel'=>$bottomLevel, 'appSalId'=>$appUserSalID);   
  	}


function createTree($array, $currentParent, $currLevel = 0, $prevLevel = -1, $sort_order=0) {
 
foreach ($array as $key => $value) {

	
if ($currentParent == $value['parent_id'] && $sort_order < $value['sort_order']) 
{   

		
		$salesmanWorkingTime           		 = $value['individualWorking'];
		$salesmanHierarchyWorkingTime  		 = $value['hierarchyWorking'];
		$salesmanPresentTotal          		 = $value['individualTotalPresent'];
		$salesmanHierarchyPresentTotal 		 = $value['hierarchyTotalPresent'];
		$salesmanAbsentTotal          		 = $value['individualAbsent'];
		$salesmanHierarchyAbsentTotal  		 = $value['hierarchyAbsent'];
		$salesmanMissingWorkingTime        	 = $value['individualMissingWorkingTime'];
		$salesmanHierarchyMissingWorkingTime = $value['hierarchyMissingWorkingTime'];
		

    if($currLevel > $prevLevel) echo "</h3><div class='accordian'><h3>"; 
	else if($currLevel == $prevLevel) echo "</h3><br/><h3>";


$dataConsolidated = "";

$dataIndividual   = "";

if($value['sort_order'] !== $value['bottomLevel']){	
	

	$dataConsolidated ='<table style=\' width:100%; font-size:10px;\'>';
					$dataConsolidated.='<tr><td class="classRowSpan" rowspan=\'1\'>Consolidated</td>';
						$dataConsolidated.='<td>';
							$dataConsolidated.='<table>';
								$dataConsolidated.='<tr>';
									//$dataConsolidated.='<td class="dayclass"><span style=\' font-weight:bold;\'>Working Hours</span></td>';
									//$dataConsolidated.='<td class="dayclass"><span style=\' font-weight:bold;\'>Missing Working Hours</span></td>';
									$dataConsolidated.='<td class="dayclass"><span style=\' font-weight:bold;\'>Present</span></td>';
									$dataConsolidated.='<td class="dayclass"><span style=\' font-weight:bold;\'>Absent</span></td>';
								$dataConsolidated.='</tr>';
								$dataConsolidated.='<tr>';
									//$dataConsolidated.='<td class="dayclass">'.$salesmanHierarchyWorkingTime.'</td>';
									//$dataConsolidated.='<td class="dayclass">'.$salesmanHierarchyMissingWorkingTime.'</td>';
									$dataConsolidated.='<td class="dayclass">'.$salesmanHierarchyPresentTotal.'</td>';
									$dataConsolidated.='<td class="dayclass">'.$salesmanHierarchyAbsentTotal.'</td>';
								$dataConsolidated.='</tr>';
							$dataConsolidated.='</table>';
							$dataConsolidated.='</td>';
						$dataConsolidated.='</tr></td>';
						$dataConsolidated.='</tr>';
					$dataConsolidated.='</table>';
					
} if(in_array($value['salesman_id'], $value['appSalId'])){
			
					$dataIndividual.='<table style=\' width:100%; font-size:10px;\'>';
					$dataIndividual.='<tr><td class="classRowSpan" rowspan=\'1\'>Individual</td>';
						$dataIndividual.='<td>';
							$dataIndividual.='<table>';
								$dataIndividual.='<tr>';
									//$dataIndividual.='<td class="dayclass"><span style=\' font-weight:bold;\'>Working Hours</span></td>';
									//$dataIndividual.='<td class="dayclass"><span style=\' font-weight:bold;\'>Missing Working Hours</span></td>';
									$dataIndividual.='<td class="dayclass"><span style=\' font-weight:bold;\'>Present</span></td>';
									$dataIndividual.='<td class="dayclass"><span style=\' font-weight:bold;\'>Absent</span></td>';
								$dataIndividual.='</tr>';
								$dataIndividual.='<tr>';
									//$dataIndividual.='<td class="dayclass">'.$salesmanWorkingTime.'</td>';
									//$dataIndividual.='<td class="dayclass">'.$salesmanMissingWorkingTime.'</td>';
									$dataIndividual.='<td class="dayclass">'.$salesmanPresentTotal.'</td>';
									$dataIndividual.='<td class="dayclass">'.$salesmanAbsentTotal.'</td>';
								$dataIndividual.='</tr>';
							$dataIndividual.='</table>';
							$dataIndividual.='</td>';
						$dataIndividual.='</tr></td>';
						$dataIndividual.='</tr>';
					$dataIndividual.='</table>';
				}
    	echo '<table width="100%" border="0">
			 	 <tr >			  
					<td><div class="headerColumn">'.$value['salesman_name'].' ('.$value['level'].')</div></td>
					<td><div class="parentColumn1">'.$dataConsolidated.$dataIndividual.'</div></td>				
			  	</tr>
			 </table>';
	
    if ($currLevel > $prevLevel) { $prevLevel = $currLevel; } 
    $currLevel++; 
			
    createTree ($array, $key, $currLevel, $prevLevel,$value['sort_order']);
	
    $currLevel--;      

    }  

}

if ($currLevel == $prevLevel) echo " </div>"; echo "</br>";
}
?>
<?php
 if(mysql_num_rows($result)!=0)
 {
?>
<?php 
	if($_SESSION['userLoginType']=='5' || $_SESSION['rptSal']!=''){
	
	
	if(isset($_SESSION['userLoginType'])){ $salesmanID=$_SESSION['salesmanId']; }
	if(isset($_SESSION['rptSal'])){ $salesmanID=$_SESSION['rptSal']; }

 $qrySetResult="select rpt_user_id
  from table_salesman_hierarchy_relationship 
  WHERE account_id=".$_SESSION['accountId']." AND salesman_id='".$salesmanID."'";
 $resultSetquery=mysql_query($qrySetResult);
 	$row = mysql_fetch_assoc($resultSetquery);
	$curLevel=$row['rpt_user_id']; 
 if($row['rpt_user_id']=='')
 	{
		$curLevel=0;
	}
 }
 else {
 	$curLevel=0;
 }
 createTree($salesmanHirearchySet, $curLevel); ?>
<?php
}
?>
</div>
</td>
	</tr>
		</table>
		</div>
	<!-- end id-form  -->

	</td>
	<td>

	<!-- right bar-->
</td>
</tr>

</table>
<div class="clear"></div>
</div>
<!--  end content-table-inner  -->
</td>
<td id="tbl-border-right"></td>
</tr>
<tr>
	<th class="sized bottomleft"></th>
	<td id="tbl-border-bottom">&nbsp;</td>
	<th class="sized bottomright"></th>
</tr>
</table>
<div class="clear">&nbsp;</div>
</div>
<!--  end content -->
<div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->
<div class="clear">&nbsp;</div> 
<!-- start footer -->         
<?php include("footer.php") ?>
<!-- end footer -->
<script>

	$(document).ready(function () {
		$("br").hide();
	});
	
    $(function() {
        $( "#from" ).datepicker({
			dateFormat: "d M yy",
            defaultDate: "w",
            changeMonth: true,
            numberOfMonths: 1,
            onSelect: function( selectedDate ) {
                $( "#to" ).datepicker( "option", "minDate", selectedDate );
            }
        });
        $( "#to" ).datepicker({
			dateFormat: "d M yy",
            defaultDate: "-w",
            changeMonth: true,
            numberOfMonths: 1,
            onSelect: function( selectedDate ) {
                $( "#from" ).datepicker( "option", "maxDate", selectedDate );
            }
        });
    });
</script>
</body>
</html>