<?php
include("includes/config.inc.php");
include("includes/function.php");
include("includes/globalarraylist.php");

$page_name="Consolidated Consumer Details";

if(isset($_POST['showReport']) && $_POST['showReport'] == 'yes')
{	/*echo '<pre>';
    print_r($_POST);
    exit;
*/

	if($_POST['retailer']!="") 
	{
		$ret_id=$_POST['retailer'];
		$_SESSION['retailer']=$ret_id;
		
		$retCond=" and r.retailer_id='".$ret_id."'";	
	}
	else {
			$_SESSION['retailer']="";
		}
	if($_POST['state']!="")
	{
     $state=$_POST['state'];
    }
    if($_POST['city']!="")
    {
		$city=$_POST['city'];
		$_SESSION['city']=$city;	    
		$countCityCond=" and r.city='".$city."'";    
    }
    
    if($_POST['market']!="")
    {
		$location=$_POST['market'];
		$_SESSION['market']=$location;
		$countMarketCond=" and r.retailer_location='".$location."'";    
    }
		

	if($_POST['from']!="") 
	{
	$from_date=$_objAdmin->_changeDate($_POST['from']);	
	}
	if($_POST['to']!="") 
	{
	$to_date=$_objAdmin->_changeDate($_POST['to']);	
	}
	
	if($_POST['sal']!="") 
	{
		$sal="and s.salesman_id='".$_POST['sal']."'";
		$salCond=" and rs.salesman_id='".$_POST['sal']."'";
		
	} else {
	$sal="";
	}
	
	
} else {
$from_date= $_objAdmin->_changeDate(date("Y-m-d"));
$to_date= $_objAdmin->_changeDate(date("Y-m-d"));
unset($_SESSION['retailer']);

}
if(isset($_REQUEST['reset']) && $_REQUEST['reset'] == 'yes')
{
	unset($_SESSION['city']);
	unset($_SESSION['market']);
	unset($_SESSION['retailer']);
	unset($state);
	header("Location:consolidated_radata_report.php");
}
if($ret_id!=''){
$RetName=$_objAdmin->_getSelectList('table_retailer','retailer_name','',"retailer_id='".$sal_id."'"); 
$ret_name=$RetName[0]->retailer_name;
$ret_list=" r.retailer_id=".$ret_id." ";
} 
else {
	$ret_name="All Outlets";
	$ret_list="";

}

//echo $ret_list."Hello";
/*  for state filter   */




/*if($_POST['sal']!=''){
	$SalesName=$_objAdmin->_getSelectList2('table_salesman','salesman_name',''," salesman_id='".$sal_id."'"); 
	$sal_name=$SalesName[0]->salesman_name;
} else {
$sal_name="All Brand Executive";
} */

include("header.inc.php")
?>
<style type="text/css"> 
#page-heading { margin: 0 0 15px 2px; } 
.style1 { height:auto; background-color:#eaeaea; border:1px solid #666666; 	}
.style2 { height:28px; border-top:1px solid #666666; }
.style3 { border-bottom:1px solid #666666; font-size:16px; font-weight:bold; }
.innerColumn1 { float:left; width:350px; height:auto; border:0px solid #000000; }
.innerColumn { float:left; width:200px; height:auto; border:0px solid #000000; }
.innerColumnlast { float:left; width:200px; height:auto; border:0px solid #000000; }
.parentColumn1 { float:left; width:90px; height:auto; border:0px solid #000000; font-weight:bold;}
.parentColumn { float:left; height:auto; width:90px; border:0px solid #000000; font-weight:bold;  font-size:12px;}
.headerColumn1 { float:left; margin-left:20px; width:70px; height:30px; border:0px solid #000000; font-weight:bold; font-size:12px;}
.headerColumn { float:left; width:180px; height:auto; border:0px solid #000000; margin-left:10px; font-weight:bold;}
.headerColumn2 { float:left; height:25px;  border:0px solid #000000; font-weight:bold; font-size:12px;}
</style>
<link rel="stylesheet" href="css/jquery-ui-1.11.2.css">
  <script src="javascripts/jquery-1.8.3.js"></script>
  <script src="javascripts/jquery-ui-1.9.2.js"></script>
   <script src="javascripts/dateNextPrev.js" type="text/javascript"></script>
  <script>
  $(function() {
	  $("div.accordian").accordion({
		autoHeight: false,
		collapsible: true,
		active: false,
	});
  });
  </script>
  <script type="text/javascript">
 $(document).ready(function()
      {
    	 //$('#address2').live(function(){
    	 //showRetailer();	
    	 $(document).on('change', "#city", function(event) {
    	 var str=$("#city").val();
    	 var type='city';
    	 showRetailer2(str); 
         $.ajax({
		'type': 'POST',
		'url': 'ret_dropdown_market.php',
		'data': 'c='+str+'&type='+type,
		'success' : function(mystring) {
		//alert(mystring);
		document.getElementById("outputMarket").innerHTML = mystring;
		//$('#wdarea').hide();
		}
	 });
        
    });
      
  });
    /*-  for market search  */
   function showMarket(id,market)
	{    //alert(id);
		var type='state';
         $.ajax({
		'type': 'POST',
		'url': 'ret_dropdown_market.php',
		'data': 'c='+id+'&type='+type+'&market='+market,
		'success' : function(mystring) {
		//alert(mystring);
		document.getElementById("outputMarket").innerHTML = mystring;
		//$('#wdarea').hide();
		}
	 });


	}
	 
  
  function showRetailers($id)
  {
  	    var str=$id;
  	    //alert(str);
        $.ajax({
		'type': 'POST',
		'url': 'retailer_search.php',
		'data': 'c='+str,
		'success' : function(mystring) {
		//alert(mystring);
		document.getElementById("retailer_list").innerHTML = mystring;
		//$('#wdarea').hide();
		}
	 });
  }

  function showRetailer2($id)
  {
  	    var str=$id;
  	    //alert(str);
        $.ajax({
		'type': 'POST',
		'url': 'retailer_search.php',
		'data': 'd='+str,
		'success' : function(mystring) {
		//alert(mystring);
		document.getElementById("retailer_list").innerHTML = mystring;
		//$('#wdarea').hide();
		}
	 });
  }

function showStateCitylist(str,id)
{
	//alert(str);
	//alert(id);
		$.ajax({
		'type': 'POST',
		'url': 'dropdown_citylist.php',
		'data': 's='+str+'&c='+id,
		'success' : function(mystring) {
		//alert(mystring);
		document.getElementById("outputcitylist").innerHTML = mystring;
		
		}
	});
}




</script>

 <!-- start content-outer -->
<div id="content-outer">
<!-- start content -->
<div id="content" >
<div id="page-heading"><h1><span style="color: #d74343; font-family: Tahoma; font-weight: bold;">Consolidated Consumer Details</span></h1></div>

<table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table" >
<tr>
	<td id="tbl-border-left"></td>
	<td>
	<!--  start content-table-inner -->
	<div id="content-table-inner" style="line-height: 24px;" >
	
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
	<tr valign="top">
	<td>
		<!-- start id-form -->
<form name="frmPre" id="frmPre" method="post" action="" enctype="multipart/form-data" >
	<table border="0" width="100%" cellpadding="0" cellspacing="0" >
	
		<tr>
		<td ><h3>State: </h3><h6>
		<select name="state" id="state" class="styledselect_form_3" onChange="showStateCitylist(this.value,'<?php echo $_POST['city']; ?>'),showRetailers(this.value),showMarket(this.value,'<?php echo $_POST['market']; ?>')"  style="border: solid 1px #BDBDBD; color: #393939;font-family: Arial;font-size: 12px;border-radius:5px ;-moz-border-radius:5px;-o-border-radius:5px;-ms-border-radius:5px;-webkit-border-radius:5px;width:150px;height: 30px;" >
		<option value="" >All States</option>
		<?php
		 $getList=$_objArrayList->checkExistingRetailerState();
		 $auUsr=$_objAdmin->_getSelectList2('state','state_id,state_name','',"$getList ORDER BY state_name");
		if(is_array($auUsr)){
		for($i=0;$i<count($auUsr);$i++){
		?>
		<option value="<?php echo $auUsr[$i]->state_id;?>" <?php if ($auUsr[$i]->state_id==$_POST['state']){ ?> selected <?php } ?>><?php echo $auUsr[$i]->state_name;?></option>
		<?php } }?>
		</select></h6></td>
		<td><h3> Select City</h3><h6>
		<div id="outputcitylist"><select name="city" class="styledselect_form_3"><option value="">Select city</option></select> <div>
		</h6>
		</td>
		<td ><h3>Outlet's WD Area: </h3><h6>
		<select id="outputMarket" name="market" class="styledselect_form_3" style="border: solid 1px #BDBDBD; color: #393939;font-family: Arial;font-size: 12px;border-radius:5px ;-moz-border-radius:5px;-o-border-radius:5px;-ms-border-radius:5px;-webkit-border-radius:5px;width:150px;height: 30px;" >
		<option value="">Select Wd Area</option>
		<?php 
		$auUsr=$_objAdmin->_getSelectList('table_retailer','DISTINCT retailer_location',''," ORDER BY retailer_location");
		if(is_array($auUsr)){
		for($i=0;$i<count($auUsr);$i++){
		?>
		<option value="<?php echo $auUsr[$i]->retailer_location;?>" <?php if ($auUsr[$i]->retailer_location==$_POST['market']){ ?> selected <?php } ?>><?php echo $auUsr[$i]->retailer_location;?></option>
		<?php } }?>
		</select>
		</h6></td>
		<td ><h3>Outlet Name: </h3><h6> 
		<select name="retailer" id="retailer_list" class="styledselect_form_3" style="border: solid 1px #BDBDBD; color: #393939;font-family: Arial;font-size: 12px;border-radius:5px ;-moz-border-radius:5px;-o-border-radius:5px;-ms-border-radius:5px;-webkit-border-radius:5px;width: 150px;height: 30px;" >
		<option value="" >All Brand Executive </option>
		<?php $aSal=$_objAdmin->_getSelectList('table_retailer','*',''," status='A' ORDER BY retailer_name"); 
		if(is_array($aSal)){
		for($i=0;$i<count($aSal);$i++){ ?>
		<option value="<?php echo $aSal[$i]->retailer_id;?>" <?php if ($aSal[$i]->retailer_id==$_POST['retailer']){ ?> selected <?php } ?>><?php echo $aSal[$i]->retailer_name;?></option>
		<?php } }?>
		</select></h6></td>
		<td ><h3>Brand Executive: </h3><h6>
		<select name="sal" id="sal" class="" style="border: solid 1px #BDBDBD; color: #393939;font-family: Arial;font-size: 12px;border-radius:5px ;-moz-border-radius:5px;-o-border-radius:5px;-ms-border-radius:5px;-webkit-border-radius:5px;width: 125px;height: 30px;" >
		<option value="" >All Brand Executive</option>
		<?php $asales=$_objAdmin->_getSelectList2('table_salesman','*',''," account_id='".$_SESSION['accountId']."' and status='A' ORDER BY salesman_name"); 
		if(is_array($asales)){
		for($i=0;$i<count($asales);$i++){
		?>
		<option value="<?php echo $asales[$i]->salesman_id;?>" <?php if ($asales[$i]->salesman_id==$_POST['sal']){ ?> selected <?php } ?>><?php echo $asales[$i]->salesman_name;?></option>
		<?php } }?>
		</select></h6></td>
		
	</tr>
	<tr>
	    <td><h3>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;From Date: </h3><h6><img src="css/images/prev.png" height="18" width="18" onclick="dateFromPrev();"> <input type="text" id="from" name="from" class="date" style="width:100px" value="<?php  echo $from_date;?>"  readonly /> <img src="css/images/next.png" height="18" width="18" onclick="dateFromNext();"></h6></td>
		<td><h3>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;To Date: </h3><h6><img src="css/images/prev.png" height="18" width="18" onclick="dateToPrev();"> <input type="text" id="to" name="to" class="date" style="width:100px" value="<?php echo $to_date; ?>"  readonly /> <img src="css/images/next.png" height="18" width="18" onclick="dateToNext();"></h6></td>
	    <input name="showReport" type="hidden" value="yes" />
		<td>
		<input name="submit" class="result-submit" type="submit" id="submit" value="View Details" />
		<input type="button" value="Reset!" class="form-reset" onclick="location.href='consolidated_radata_report.php?reset=yes';"/>
		</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
	</table>
	</form>
		<div style="width:1000px; height:auto;">
		<table  border="0" cellspacing="5" cellpadding="5"  align="center" width="100%">
				<tr>
				<td align="" valign="top"  >
<div id="faqs-container" >

<?php
	
	/*  for state filter   */

		if($state!="")
		{
			$stateCond=" state_id=".$state."";
		}
		else
		{
			$stateArray=array();
			$retStateArray = $_objAdmin->_getSelectList('table_retailer',"DISTINCT(state)",'',"");
			foreach ($retStateArray as $arrValue) 
					{
						$stateArray[]=$arrValue->state;
					}
			$stateArrayList=implode(',',array_filter($stateArray));
					
			
			$stateCond=" state_id IN ($stateArrayList)";
		}	
	
	
		$where = " $stateCond ORDER BY state_name";  
  		$result = $_objAdmin->_getSelectList2('state',"state_id, state_name",'',$where);
  		foreach ($result as $key=>$Statevalue ): 		
  		
  		$retState=$_objAdmin->_getSelectList('table_retailer_consumer_details as rs left join table_retailer as r on r.retailer_id=rs.retailer_id
  		 left join state as st on st.state_id=r.state',"count(DISTINCT(rs.retailer_id)) as totalRetailer",'',
  		  " r.state='".$Statevalue->state_id."' $countCityCond $countMarketCond $retCond $salCond and (rs.app_date BETWEEN '".date('Y-m-d', strtotime($from_date))."' AND '".date('Y-m-d', strtotime($to_date))."')");	
  		   
  		   if(is_array($retState) && !empty($retState)){ $retStateCount=$retState[0]->totalRetailer;} else { $retStateCount="NA";}
  		   
  		   $retStateCity=$_objAdmin->_getSelectList('table_retailer_consumer_details as rs left join table_retailer as r on r.retailer_id=rs.retailer_id
  		 left join state as st on st.state_id=r.state',"count(DISTINCT(r.city)) as totalCity",'',
  		  " r.state='".$Statevalue->state_id."' $countCityCond $countMarketCond $retCond $salCond and (rs.app_date BETWEEN '".date('Y-m-d', strtotime($from_date))."' AND '".date('Y-m-d', strtotime($to_date))."')");	
  		   
  		   if(is_array($retStateCity) && !empty($retStateCity)){ $retStateCityCount=$retStateCity[0]->totalCity;} else { $rretStateLocationCount="NA";}
		
			$retStateLocation=$_objAdmin->_getSelectList('table_retailer_consumer_details as rs left join table_retailer as r on r.retailer_id=rs.retailer_id
  		 left join state as st on st.state_id=r.state',"count(DISTINCT(r.retailer_location)) as totalLocation",'',
  		  " r.state='".$Statevalue->state_id."' $countCityCond $countMarketCond $retCond $salCond and (rs.app_date BETWEEN '".date('Y-m-d', strtotime($from_date))."' AND '".date('Y-m-d', strtotime($to_date))."')");	
  		   
  		   if(is_array($retStateLocation) && !empty($retStateLocation)){ $retStateLocationCount=$retStateLocation[0]->totalLocation;} else { $retStateCityCount="NA";}
		
  		
  		
  		
  		
  		
?>
	<div class='accordian'>
			<h3>
				<table width="100%" border="0">
				  <tr >
					<td><div class="headerColumn"><?php echo $Statevalue->state_name; ?></div></td>
					<td><div class="headerColumn"><?php echo "Total City- ".$retStateCityCount; ?></div></td>
					<td><div class="headerColumn"><?php echo "Total WD Area- ".$retStateLocationCount; ?></div></td>
					<td><div class="headerColumn"><?php echo "Total Outlet- ".$retStateCount; ?></div></td>					
				  </tr>
				</table>
			</h3>
			<div class='accordian'>
	<?php 
	
		
		/* for city filter */
			if($city!="")
			{
				$cityCond=" and city_id=".$city."";
			}
			else
			{
				$CityArray=array();
			$retCityArray = $_objAdmin->_getSelectList('table_retailer',"DISTINCT(city)",'',"");
			foreach ($retCityArray as $arrValue) 
					{
						$CityArray[]=$arrValue->city;
					}
			$cityArrayList=implode(',',array_filter($CityArray));
					
			
			$cityCond=" and city_id IN ($cityArrayList)";
				
				
				
				//$cityCond="";
			}
	
			//echo $cityCond;
	
			$Citywhere = " state_id='".$Statevalue->state_id."' $cityCond ORDER BY city_name";  
			$Cityresult = $_objAdmin->_getSelectList2('city',"city_id,city_name,state_id",'',$Citywhere);
			//echo "<pre>";
				//print_r($Cityresult);
			foreach ($Cityresult as $Cityvalue ) :
		$retCity=$_objAdmin->_getSelectList('table_retailer_consumer_details as rs left join table_retailer as r on r.retailer_id=rs.retailer_id
  		 left join city as ct on ct.city_id=r.city',"count(DISTINCT(rs.retailer_id)) as totalRetailer",'',
  		  " r.state='".$Statevalue->state_id."' $countMarketCond and r.city='".$Cityvalue->city_id."' $retCond $salCond and (rs.app_date BETWEEN '".date('Y-m-d', strtotime($from_date))."' AND '".date('Y-m-d', strtotime($to_date))."')");	
  		   
  		   if(is_array($retCity) && !empty($retCity) && $retCity[0]->totalRetailer>0){ $retCityCount=$retCity[0]->totalRetailer;} else { $retCityCount="NA";}
  		   
  		   $retCityLocation=$_objAdmin->_getSelectList('table_retailer_consumer_details as rs left join table_retailer as r on r.retailer_id=rs.retailer_id
  		 left join state as st on st.state_id=r.state',"count(DISTINCT(r.retailer_location)) as totalLocation",'',
  		  " r.state='".$Statevalue->state_id."' $countMarketCond and r.city='".$Cityvalue->city_id."' $retCond $salCond and (rs.app_date BETWEEN '".date('Y-m-d', strtotime($from_date))."' AND '".date('Y-m-d', strtotime($to_date))."')");	
  		   
  		   if(is_array($retCityLocation) && !empty($retCityLocation) && $retCityLocation[0]->totalLocation>0 ){ $retCityLocationCount=$retCityLocation[0]->totalLocation;} else { $retCityLocationCount="NA";}
		
  		   
				
	 ?>
		
			<h3>
					<table width="100%" border="0">
					  <tr >
						<td><div class="headerColumn"><?php echo $Cityvalue->city_name; ?></div></td>
						<td><div class="headerColumn"><?php echo "Total WD Area- ".$retCityLocationCount; ?></div></td>
						<td><div class="headerColumn"><?php echo "Total Outlet- ".$retCityCount; ?></div></td>
					  </tr>
					</table>
			</h3>
			<div class='accordian'>	
			
			<?php 
			
				/* for location */
					if($location!="")
					{
						$locationCond=" and location='".$location."'";
					}
					else
					{
						$locationCond="";
					}
				
				$Marketwhere = " city='".$Cityvalue->city_id."' and state_id='".$Statevalue->state_id."' $locationCond ORDER BY location";  
				$Marketresult = $_objAdmin->_getSelectList('table_market',"location",'',$Marketwhere);
			if(is_array($Marketresult) && !empty($Marketresult)){
			foreach ($Marketresult as $Marketvalue ) :
			
			$retMarket=$_objAdmin->_getSelectList('table_retailer_consumer_details as rs left join 
			table_retailer as r on r.retailer_id=rs.retailer_id ',"count(DISTINCT(rs.retailer_id)) as totalRetailer",'',
  		  " r.state='".$Statevalue->state_id."' and r.city='".$Cityvalue->city_id."' and r.retailer_location='".$Marketvalue->location."' $retCond $salCond and (rs.app_date BETWEEN '".date('Y-m-d', strtotime($from_date))."' AND '".date('Y-m-d', strtotime($to_date))."')");	
  		   
  		   if(is_array($retMarket) && !empty($retMarket) && $retMarket[0]->totalRetailer>0){ $retMarketCount=$retMarket[0]->totalRetailer;} else { $retMarketCount="NA";}
			
						
			?>
					
					<h3>
						<table width="100%" border="0">
						  <tr >
							<td><div class="headerColumn"><?php echo $Marketvalue->location; ?></div></td>
							<td><div class="headerColumn"><?php echo "Total Outlet- ".$retMarketCount; ?></div></td>
						  </tr>
						</table>
					</h3>
				<div style="overflow:scroll; width:807px;">
					<table  border="0" width="100%" cellpadding="0" cellspacing="0" id="report_export" name="report_export" >
						<tr  bgcolor="#6E6E6E" style="color: #fff;font-weight: bold;" >
							<td style="padding:10px;" width="10%">Outlet Name</td>
							<td style="padding:10px;" width="10%">SFA Code</td>
							<td style="padding:10px;" width="10%">Brand Executive</td>
							<td style="padding:10px;" width="10%">State</td>
							<td style="padding:10px;" width="10%">City</td>
							<td style="padding:10px;" width="10%">Added Date</td>
							<td style="padding:10px;" width="10%">Consumer Name</td>
							<td style="padding:10px;" width="10%">Consumer Number</td>
							<td style="padding:10px;" width="10%">Consumer Brand</td>
							<td style="padding:10px;" width="10%">Total day</td>
							<td style="padding:10px;" width="10%">Frequency Of Purchase</td>
							<td style="padding:10px;" width="10%">Pack consumer</td>
							<td style="padding:10px;" width="10%">No. Of Packets</td>
							<td style="padding:10px;" width="10%">No. Of Sticks</td>
							<td style="padding:10px;" width="10%">Comments</td>
						</tr>
								<?php
			$no_ret = array();
			$auRet=$_objAdmin->_getSelectList('table_retailer_consumer_details as trc 
			left join table_retailer as r on r.retailer_id=trc.retailer_id 
			left join table_salesman as s on s.salesman_id=trc.salesman_id 
			left join state as st on st.state_id=r.state left join city as c on c.city_id=r.city',
			"trc.*,r.retailer_name,r.outlet_name,st.state_name,c.city_name,s.salesman_name",'',
			" $ret_list $sal and st.state_id='".$Statevalue->state_id."' and c.city_id='".$Cityvalue->city_id."' and r.retailer_location='".$Marketvalue->location."' and (trc.app_date BETWEEN '".date('Y-m-d', strtotime($from_date))."' AND '".date('Y-m-d', strtotime($to_date))."') ORDER BY trc.app_date desc, s.salesman_name asc");
			if(is_array($auRet)){
				for($i=0;$i<count($auRet);$i++)
				{
				$no_ret[] = $auRet[$i]->consumer_detail_id;
				if($auRet[$i]->outlet_name!=''){ $sfa= $auRet[$i]->outlet_name;} else { $sfa="NA";}
				
				
				?>
				<tr  style="border-bottom:2px solid #6E6E6E;" >
				<td style="padding:10px;" width="10%"><?php echo $auRet[$i]->retailer_name;?> </td>
				<td style="padding:10px;" width="10%"><?php echo $sfa;?> </td>
				<td style="padding:10px;" width="10%"><?php echo $auRet[$i]->salesman_name;?> </td>
				<td style="padding:10px;" width="10%"><?php echo $auRet[$i]->state_name;?> </td>
				<td style="padding:10px;" width="10%"><?php echo $auRet[$i]->city_name;?> </td>
				<td style="padding:10px;" width="10%"><?php echo $_objAdmin->_changeDate($auRet[$i]->app_date);?> </td>	
				<td style="padding:10px;" width="10%"><?php echo $auRet[$i]->consumer_name;?> </td>
				<td style="padding:10px;" width="10%"><?php echo $auRet[$i]->con_phone_number;?> </td>
				<td style="padding:10px;" width="10%"><?php echo $auRet[$i]->con_previous_brand;?> </td>
				<td style="padding:10px;" width="10%"><?php echo $auRet[$i]->total_day_first_purchase;?> </td>
				<td style="padding:10px;" width="10%"><?php echo $auRet[$i]->feq_of_purchase;?> </td>
				<td style="padding:10px;" width="10%"><?php echo $auRet[$i]->pack_consumer;?> </td>
				<td style="padding:10px;" width="10%"><?php echo $auRet[$i]->no_of_packets;?> </td>				
				<td style="padding:10px;" width="10%"><?php echo $auRet[$i]->no_of_sticks;?> </td>
				<td style="padding:10px;" width="10%"><?php echo $auRet[$i]->comments;?> </td>								
				</tr>
				<?php
				}
				?>
				<tr  >
				<td style="padding:10px;" colspan="11"><b>Total Number of consumer:</b> <?php echo count($no_ret) ?></td>
				</tr>
			<?php
			} else {
			?>
			<tr  style="border-bottom:2px solid #6E6E6E;" align="center" >
				<td style="padding:10px;" colspan="11">Report Not Available</td>
				<td></td>
				<td></td>
			</tr>
			<?php
			}
			?>
					</table>
				</div>
		<?php endforeach; } ?>
		</br> </div>
	<?php endforeach; ?>
	</div><br></div><br>
	</div>
	<?php  endforeach; ?>
</div>
</td>
</tr>
</table>
</div>
	<!-- end id-form  -->

	</td>
	<td>

	<!-- right bar-->
</td>
</tr>

</table>
<div class="clear"></div>
</div>
<!--  end content-table-inner  -->
</td>
<td id="tbl-border-right"></td>
</tr>
<tr>
	<th class="sized bottomleft"></th>
	<td id="tbl-border-bottom">&nbsp;</td>
	<th class="sized bottomright"></th>
</tr>
</table>

<div class="clear">&nbsp;</div>
</div>
<!--  end content -->
<div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->
<div class="clear">&nbsp;</div> 
<!-- start footer -->         
<?php include("footer.php") ?>
<!-- end footer -->
<script>

	$(document).ready(function () {
		$("br").hide();
	});
	
    $(function() {
        $( "#from" ).datepicker({
			dateFormat: "d M yy",
            defaultDate: "w",
            changeMonth: true,
            numberOfMonths: 1,
            onSelect: function( selectedDate ) {
                $( "#to" ).datepicker( "option", "minDate", selectedDate );
            }
        });
        $( "#to" ).datepicker({
			dateFormat: "d M yy",
            defaultDate: "-w",
            changeMonth: true,
            numberOfMonths: 1,
            onSelect: function( selectedDate ) {
                $( "#from" ).datepicker( "option", "maxDate", selectedDate );
            }
        });
    });
</script>
</body>
</html>
