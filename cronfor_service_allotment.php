<?php
include("includes/config.inc.php");

$dom = new DOMDocument('1.0', 'utf-8');
$node = $dom->createElement("WhiteSpot");
$parnode = $dom->appendChild($node);
$_objgcm = new GCM();
//mysql connections
$con = mysql_connect(MYSQL_HOST_NAME,MYSQL_USER_NAME,MYSQL_PASSWORD);
if (!$con) {
    $node = $dom->createElement("Sts");
    $newnode = $parnode->appendChild($node);
    $newnode->setAttribute("Code", "1002");
    //$newnode->setAttribute("message", "database not connect");
    $dom->formatOutput = true;
    echo $dom->saveXML();
    $datetime="Date: ".$date.", Time: ".$time;
    $page="cron for service allotment";
    file_put_contents("Log/Cron.log",print_r($datetime,true)."\r\n", FILE_APPEND);
    file_put_contents("Log/Cron.log",print_r($page,true)."\r\n", FILE_APPEND);
    file_put_contents("Log/Cron.log",print_r($_REQUEST,true), FILE_APPEND);
    file_put_contents("Log/Cron.log",print_r($dom->saveXML(),true), FILE_APPEND);
    die;
}
mysql_select_db(MYSQL_DB_NAME, $con);
$todayDate = date('Y-m-d');
$sql = "SELECT br.booking_id,bd.booking_detail_id,ca.building_id,bd.cleaning_date,cs.slot_start_time,br.customer_id,cs.slot_end_time,br.total_cost_paid,b.building_name,ca.customer_address "
        ."FROM  table_booking_register AS br "
        ."LEFT JOIN table_booking_details AS bd ON br.booking_id = bd.booking_id "
        ."LEFT JOIN table_booking_allotment AS ba ON bd.booking_detail_id = ba.booking_detail_id "
        ."LEFT JOIN  table_calender_slots AS cs ON cs.slot_id = bd.slot_id "
	."LEFT JOIN table_customer_address AS ca ON ca.customer_address_id = br.customer_address_id "
        ."LEFT JOIN table_buildings AS b ON b.building_id = ca.building_id "
        ."WHERE br.status = 'A' AND ba.booking_status='UA' AND bd.status = 'A' AND bd.cleaning_date = '".$todayDate."'";

$rec = mysql_query($sql);
if(mysql_num_rows($rec)<=0){
    $node = $dom->createElement("Sts");
    $newnode = $parnode->appendChild($node);
    $newnode->setAttribute("Code", "1004");
    $dom->formatOutput = true;
    echo $dom->saveXML();
    $end_datetime="End Date: ".date('d/m/Y H:i:s');
    $page="cron for service allotment";
    file_put_contents("Log/Cron.log",print_r($datetime,true)."\r\n", FILE_APPEND);
    file_put_contents("Log/Cron.log",print_r($end_datetime,true)."\r\n", FILE_APPEND);
    file_put_contents("Log/Cron.log",print_r($page,true)."\r\n", FILE_APPEND);
    file_put_contents("Log/Cron.log",print_r($_REQUEST,true), FILE_APPEND);
    file_put_contents("Log/Cron.log",print_r($dom->saveXML(),true), FILE_APPEND);
    mysql_close($con);
    die;
}
$filterDataArray = array();
while($row = mysql_fetch_array($rec)){
    $temp['booking_id'] = $row['booking_id'];
    $temp['booking_detail_id'] = $row['booking_detail_id'];
    $temp['cleaning_date'] = $row['cleaning_date'];
    $temp['slot_start_time'] = $row['slot_start_time'];
    $temp['slot_end_time'] = $row['slot_end_time'];
    $temp['total_cost_paid'] = $row['total_cost_paid'];
    $temp['building_name'] = $row['building_name'];
    $temp['customer_address'] = $row['customer_address'];
    $temp['customer_id'] = $row['customer_id'];
    if(array_key_exists($row['building_id'], $filterDataArray)){
        array_push($filterDataArray[$row['building_id']],$temp);
    } else {
        $filterDataArray[$row['building_id']][0] = $temp;
    }
}
foreach($filterDataArray as $key=>$filterDataArrayTemp) {
    if($key != 0) {
        $sqlCleaner = mysql_query("SELECT c.cleaner_id,c.cleaner_name FROM `table_cleaner`  AS c "
                        ."LEFT JOIN `table_supervisor` AS s ON s.supervisor_id = c.supervisor_id "
                        ."WHERE s.building_id = ".$key." AND s.status = 'A' AND c.status='A'");
        if(mysql_num_rows($sqlCleaner)<=0){
            foreach($filterDataArrayTemp as $dataTemp) {
                $sqlUpdate = "UPDATE table_booking_allotment SET not_alloted_reason='No Cleaner Available',last_update_date = now(),booking_alloted='no' where booking_detail_id='".$dataTemp['booking_detail_id']."'";
                mysql_query($sqlUpdate) or die(mysql_error());
            }
        } else {
            $cleanersArray = array();
            while($row = mysql_fetch_array($sqlCleaner)){
                $cleanerTemp['cleaner_id'] = $row['cleaner_id'];
                $cleanerTemp['cleaner_name'] = $row['cleaner_name'];
                $cleanerTemp['allotment'] = 0;
                array_push($cleanersArray,$cleanerTemp);
            }
            $cleanerIncrement = 0;
            foreach($filterDataArrayTemp as $dataTemp) {
                if($cleanersArray[$cleanerIncrement]['allotment'] < 5) {
                    $sqlUpdate = "UPDATE table_booking_allotment SET cleaner_id='".$cleanersArray[$cleanerIncrement]['cleaner_id']."',last_update_date = now(),booking_alloted='yes',booking_alloted_date=now(),booking_status='AL' where booking_detail_id='".$dataTemp['booking_detail_id']."'";
                    mysql_query($sqlUpdate) or die(mysql_error());
                    $cleanersArray[$cleanerIncrement]['allotment']++;
                    
                    //push notification for allotment
                    $condicleaner=" where cleaner_id='".$cleanersArray[$cleanerIncrement]['cleaner_id']."'";
                    $cleaner = mysql_query("SELECT gcm_id FROM table_cleaner".$condicleaner);
                    $grm_id = "";
                    while($row = mysql_fetch_array($cleaner)){
                        $grm_id = $row['gcm_id'];
                    }
                    
                    $message = $dataTemp['booking_id']."|@|".$dataTemp['booking_detail_id']."|@|".$dataTemp['customer_id']."|@|".$dataTemp['cleaning_date']."|@|".$dataTemp['slot_start_time']."|@|".$dataTemp['slot_end_time']."|@|".$dataTemp['total_cost_paid']."|@|".$dataTemp['building_name']."|@|".$dataTemp['customer_address']."|@|NEW";
                    $registatoin_id = array($grm_id);
                    $message = array("message" => $message);
                    $result = $_objgcm->send_notification($registatoin_id, $message);
                    
                    //insert in notification table
                    $sql = "INSERT INTO table_notification (cleaner_id,customer_id,booking_id,notification,create_date) VALUES "
                        . "('".$cleanersArray[$cleanerIncrement]['cleaner_id']."','".$dataTemp['customer_id']."','".$dataTemp['booking_id']."','Booking alloted',now())";
                    mysql_query($sql) or die(mysql_error());
                } else {
                    $sqlUpdate = "UPDATE table_booking_allotment SET not_alloted_reason='Cleaner Threshold',last_update_date = now(),booking_alloted='no' where booking_detail_id='".$dataTemp['booking_detail_id']."'";
                    mysql_query($sqlUpdate) or die(mysql_error());
                }
                $cleanerIncrement++;
                if(!array_key_exists($cleanerIncrement, $cleanersArray)) {
                    $cleanerIncrement = 0;
                }
            }
        }
    } else {
        foreach($filterDataArrayTemp as $dataTemp) {
            $sqlUpdate = "UPDATE table_booking_allotment SET not_alloted_reason='No Building Available',last_update_date = now(),booking_alloted='no' where booking_detail_id='".$dataTemp['booking_detail_id']."'";
            mysql_query($sqlUpdate) or die(mysql_error());
        }
    }
}
$node = $dom->createElement("Status");
$newnode = $parnode->appendChild($node);
$newnode->setAttribute("Code", "0");
//$newnode->setAttribute("message", "");
$node = $dom->createElement("cron");
$newnode = $parnode->appendChild($node);
$newnode->setAttribute("Success", "cron run successfully");
$dom->formatOutput = true;
echo $dom->saveXML();
$datetime="Date: ".$date.", Time: ".$time;
$page="cron for service allotment";
file_put_contents("Log/Cron.log",print_r($datetime,true)."\r\n", FILE_APPEND);
file_put_contents("Log/Cron.log",print_r($page,true)."\r\n", FILE_APPEND);
file_put_contents("Log/Cron.log",print_r($_REQUEST,true), FILE_APPEND);
file_put_contents("Log/Cron.log",print_r($dom->saveXML(),true), FILE_APPEND);
header("Location: http://whitespot-cleaning.com/admin/index.php");
die;
