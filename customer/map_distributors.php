<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
<script>
function showstage_city(str)
{
document.getElementById("address2").options.length = 0;
	if (str=="")
	{
		document.getElementById("address2").innerHTML="";
		return;
	}	 
	if (window.XMLHttpRequest)
	{// code for IE7+, Firefox, Chrome, Opera, Safari
	//alert("in 1");
		xmlhttp=new XMLHttpRequest();
		xmlhttp.open("POST","change_state.php?q="+str,true);
		xmlhttp.send();
		//alert("xml send");
		xmlhttp.onreadystatechange=function()
		{
		     //alert("in onready state");
			if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				var x=document.getElementById("address2");
				var xmlDoc=xmlhttp.responseXML;
				i=xmlDoc.getElementsByTagName("option");
				var lenx=i.length;
				for(var k=0;k<lenx;k++)
				{
					try
					{
						var option=window.document.createElement("option");
						option.value=i[k].getAttribute('value');
						option.text=i[k].getAttribute('description');
						x.add(option);
					}
					catch(err)
					{
					alert(err.message);
					}
				}
			}
		}
	}
	else 
	{// code for IE6, IE5
 		xmlhttp= new ActiveXObject("Microsoft.XMLHTTP");
		xmlhttp.open("POST","change_state.php?q="+str,true);
		xmlhttp.send();
		xmlhttp.onreadystatechange=function()
		{
			if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				var i;
				var x=document.getElementById("address2");
				var xmlDoc=xmlhttp.responseXML;
				i=xmlDoc.getElementsByTagName("option");
				var len=i.length;
				for(var j=0;j<len;j++)
				{
					var option=document.createElement("option");
					option.value=xmlDoc.documentElement.childNodes[j].attributes[0].nodeValue;
				    option.text=xmlDoc.documentElement.childNodes[j].attributes[1].nodeValue;
					try
					{
						x.add(option,x.options[null]);
					}
					catch (e)
					{
						x.add(option,null);
					}
				}
			}
		}
	}
}
</script>
<script type="text/javascript">

    var location1;
    var location2;

    var address1;
    var address2;

    var latlng;
    var geocoder;
    var map;

    var distance;
	
	var map;
    var markersArray = [];

    // finds the coordinates for the two locations and calls the showMap() function
    function initialize()
    {
        geocoder = new google.maps.Geocoder(); // creating a new geocode object

        // getting the two address values
        address1 = document.getElementById("address1").value+','+document.getElementById("address2").value;
        //address2 = document.getElementById("address2").value;

        // finding out the coordinates
        if (geocoder) 
        {
            geocoder.geocode( { 'address': address1}, function(results, status) 
            {
                if (status == google.maps.GeocoderStatus.OK) 
                {
                    //location of first address (latitude + longitude)
                    location1 = results[0].geometry.location;
                } else 
                {
                    alert("Geocode was not successful for the following reason: " + status);
                }
            });
            geocoder.geocode( { 'address': address1}, function(results, status) 
            {
                if (status == google.maps.GeocoderStatus.OK) 
                {
                    //location of second address (latitude + longitude)
                    location2 = results[0].geometry.location;
                    // calling the showMap() function to create and show the map 
                    showMap();
                } else 
                {
                    alert("Geocode was not successful for the following reason: " + status);
                }
            });
        }
		
		$(this).hide();
    }

    // creates and shows the map
    function showMap()
    {
        // center of the map (compute the mean value between the two locations)
        latlng = new google.maps.LatLng((location1.lat()+location2.lat())/2,(location1.lng()+location2.lng())/2);
        // set map options
            // set zoom level
            // set center
            // map type
        var mapOptions = 
        {
            zoom: 13,
            center: latlng,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        // create a new map object
            // set the div id where it will be shown
            // set the map options
        map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);
		 // add a click event handler to the map object
            google.maps.event.addListener(map, "click", function(event)
            {
                // place a marker
                placeMarker(event.latLng);
                // display the lat/lng in your form's lat/lng fields
                document.getElementById("latFld").value = event.latLng.lat();
                document.getElementById("lngFld").value = event.latLng.lng();
            });
				<?php if($auRec[0]->lat!=""){?>
				var image = 'images/location.png';
				var myLatLng = new google.maps.LatLng(<?php echo $auRec[0]->lat; ?>,<?php echo $auRec[0]->lng; ?>);
                var beachMarker = new google.maps.Marker({
                position: myLatLng,
                map: map,
                icon: image
                });
				<?php } ?>
	}
	function placeMarker(location) {
            // first remove all markers if there are any
            deleteOverlays();

            var marker = new google.maps.Marker({
                position: location, 
                map: map
            });

            // add marker in markers array
            markersArray.push(marker);

            //map.setCenter(location);
        }

        // Deletes all markers in the array by removing references to them
        function deleteOverlays() {
            if (markersArray) {
                for (i in markersArray) {
                    markersArray[i].setMap(null);
                }
            markersArray.length = 0;
            }
        }
    
</script> 
<!--  start step-holder -->
<div id="step-holder">
<div class="step-no-off">1</div>
<div class="step-light-left"><a href="">Distributor Form</a></div>
<div class="step-light-right">&nbsp;</div>
<div class="step-no-off">2</div>
<div class="step-light-left"><a href="">Login Form</a></div>
<div class="step-light-right">&nbsp;</div>
<div class="step-no">3</div>
<div class="step-dark-left">Map</div>
<div class="step-dark-round">&nbsp;</div>
<div class="clear"></div>
</div>
<!--  end step-holder -->
	<!--  start message-red -->
	<?php if($login_err!=''){?>
	<div id="message-red">
		<table border="0" width="100%" cellpadding="0" cellspacing="0">
		<tr>
			<td class="red-left">Error. <?php echo $map_err; ?></td>
			<td class="red-right"><a class="close-red"><img src="images/icon_close_red.gif"   alt="" /></a></td>
		</tr>
		</table>
	</div>
	<?php } ?>
	<!--  end message-red -->
	<!-- start id-form -->
	<form name="frmPre" id="frmPre" method="post" action="distributors.php" enctype="multipart/form-data" >
		<table border="0" cellpadding="0" cellspacing="0"  id="id-form">
		<tr>
			<th valign="top">Point on Map:</th>
			<td><input type="button" class="form-submit" value="Show on Map" onClick="initialize();"/></td>
			<td>
			</td>
		</tr>
		<tr>
			<td colspan="3"  align="center" ><p><div id="map_canvas" style="width:800px; height:400px"></div>
		</tr>
		<th>&nbsp;</th>
		<td valign="top">
			<input type="hidden" name="dis_id" value="<?php echo $_SESSION['disId']; ?>" />
			<input type="hidden" name="user_type" value="3" />
			<input type="hidden" id="latFld" name="latitude" value="<?php echo $auRec[0]->lat; ?>">
			<input type="hidden" id="lngFld" name="longitude" value="<?php echo $auRec[0]->lng; ?>">
			<input name="address1" id="address1" type="hidden" value="<?php echo $auRec[0]->distributor_location; ?>"/>
			<input name="address2" id="address2" type="hidden" value="<?php echo $auRec[0]->city_name; ?>"/>
			<input name="dis_map" type="hidden" value="yes" />
			<?php if($_SESSION['update']==''){ ?>
			<input name="submit" class="form-submit" type="submit" id="submit" value="Save & Add Next Distributor" />
			<input name="submit" class="form-submit" type="submit" id="submit" value="Save" />
			<?php } else { ?>
			<input name="submit" class="form-submit" type="submit" id="submit" value="Save Distributor" />
			<?php } ?>
		</td>
		</table>
	  
	</form>