<?php
include("includes/config.inc.php");
include("includes/function.php");
$_objAdmin = new Admin();

if (isset($_REQUEST['id']) && $_REQUEST['id'] != "") {
    $where .= "tsf.feedback_type='customer' AND tsf.customer_id!=0 and tsf.feedback_id = '".$_REQUEST['id']."'";
    $table = 'table_service_feedback as tsf left join table_booking_register as tbr on tbr.booking_id=tsf.booking_id left join table_customer as tc on tc.customer_id=tsf.customer_id';
    $clms = 'tc.customer_id,tc.customer_name,tc.customer_number,tc.customer_email,tsf.*,tbr.booking_id';
    $auRec = $_objAdmin->_getSelectList($table, $clms, $limit, $where . ' ' . $sort, '','');
    if (count($auRec) <= 0)
        header("Location:customer_feedback_details.php");
}
include("header.inc.php");
?>
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <!-- BEGIN: Subheader -->
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">
                    Customer Feedback
                </h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="#" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__separator">
                        -
                    </li>
                    <li class="m-nav__item">
                        <a href="" class="m-nav__link">
                            <span class="m-nav__link-text">
                                Master
                            </span>
                        </a>
                    </li>
                    <li class="m-nav__separator">
                        -
                    </li>
                    <li class="m-nav__item">
                        <a href="" class="m-nav__link">
                            <span class="m-nav__link-text">
                                Reports
                            </span>
                        </a>
                    </li>  
                </ul>
            </div> 
        </div>
    </div>
    <!-- END: Subheader -->
    <div class="m-content">
        <div class="row">
            <div class="col-lg-12">
                <!--begin::Portlet--> 
                <div class="m-portlet">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <span class="m-portlet__head-icon m--hide">
                                    <i class="la la-gear"></i>
                                </span>
                                <h3 class="m-portlet__head-text">
                                    Customer Feedback Detail
                                </h3>
                            </div>
                        </div>
                    </div>
                    <!--begin::Form-->
                    <form class="m-form m-form--fit m-form--label-align-right">

                        <div class="m-portlet__body">
                            <div class="m-form__content">
                                <div class="m-alert m-alert--icon alert alert-danger m--hide" role="alert" id="m_form_1_msg">
                                    <div class="m-alert__icon">
                                        <i class="la la-warning"></i>
                                    </div>
                                    <div class="m-alert__text">
                                        Oh snap! Change a few things up and try submitting again.
                                    </div>
                                    <div class="m-alert__close">
                                        <button type="button" class="close" data-close="alert" aria-label="Close"></button>
                                    </div>
                                </div>
                            </div>
                            <?php if (isset($err) && !empty($err)) { ?>
                                <div class="m-form__content">
                                    <div class="m-alert m-alert--icon alert alert-danger" role="alert" id="m_form_1_msg1">
                                        <div class="m-alert__icon">
                                            <i class="la la-warning"></i>
                                        </div>
                                        <div class="m-alert__text">
                                            <?php echo $err; ?>
                                        </div>
                                        <div class="m-alert__close">
                                            <button type="button" class="close" data-close="alert" aria-label="Close"></button>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                            <div class="form-group m-form__group row">
                                <div class="col-lg-4">
                                    <label>
                                        Comment:
                                    </label>
                                    <div><?php echo $auRec[0]->comments;?></div>

                                </div>
                                <div class="col-lg-4">
                                    <label>
                                        Booking Id:
                                    </label>
                                    <div><?php echo $auRec[0]->booking_id;?></div>

                                </div>
                                 <div class="col-lg-4">
                                    <label>
                                        Customer Name:
                                    </label>
                                    <div><?php echo $auRec[0]->customer_name;?></div>

                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                               <div class="col-lg-4">
                                    <label>
                                         Customer Email:
                                    </label>
                                    <div><?php echo $auRec[0]->customer_email;?></div>

                                </div>
                                <div class="col-lg-4">
                                    <label>
                                       Rating:
                                    </label>
                                    <div><?php echo $auRec[0]->star_rating;?></div>

                                </div>
                                <div class="col-lg-4">
                                    <label>
                                        Date:
                                    </label>
                                    <div><?php echo $auRec[0]->created_date;?></div>

                                </div>
                                
                            </div>
                        </div>
                        <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                            <div class="m-form__actions m-form__actions--solid">
                                <div class="row">
                                    <div class="col-lg-4"></div>
                                    <div class="col-lg-8">
                                        <a href="customer_feedback.php"  class="btn btn-secondary">
                                            Back
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </form>
                    <!--end::Form-->
                </div>
                <!--end::Portlet-->
            </div>
        </div>
    </div>
</div>
</div>
</div>
<!-- start footer -->         
<?php include("footer.php") ?>

