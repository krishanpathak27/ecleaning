<?php
/*
* Created Date 21 jan 2016
* By Abhishek Jaiswal
*/
include("includes/config.inc.php");
include("includes/function.php");
include("includes/globalarraylist.php");
include("header.inc.php");
include("customer_map.php");?>
<script src="javascripts/jquery-1.8.2.js" type="text/javascript"></script>
<script src="javascripts/jquery-ui.js"></script>
<script type="text/javascript" src="javascripts/validate.js"></script>
<!-- start content-outer -->
<div id="content-outer">
<!-- start content -->
<div id="content">
	<div id="page-heading">
		<h1>
			<span style="color: #d74343; font-weight: bold;">Customer Geo Tagging</span>
		</h1>
	</div>
	<div class="map_wrap">
		<div class="siderbarmap" style="padding-bottom:20px;">
			<table border="0" width="100%" cellpadding="0" cellspacing="0">
				<tr>
					<td>
						<h3>Customer: </h3>
						<h6>
							<select name="customer" id="customer" class="menulist">
								<option value="all">All</option>
								<option value="retailer">Retailer</option>
								<option value="distributor">Distributor</option>
								<option value="customer">Mechanic</option>
							</select>
						</h6>
					</td>
					<td>
						<h3>Interest: </h3>
						<h6>
							<select name="customer" id="interest" class="menulist">
								<option value="all">All</option>
								<option value="hot">HOT</option>
								<option value="cold">COLD</option>
							</select>
						</h6>
					</td>
					<td>
						<h3>Division: </h3>
						<h6>
							<select name="customer" id="division" class="menulist">
								<option value="all">All</option>
								<?php 
									foreach ($divisionRec as $key => $value) {
										echo "<option value='$value->division_id'>$value->division</option>";
									}
								?>
							</select>
						</h6>
					</td>
					<td>
						<h3>Count: </h3>
						<h3 id="count"><?php echo sizeof($retRec)+sizeof($distRec)+sizeof($custRec)?> </h3>
					</td>
				</tr>
			</table>
			<!-- <ul>
				<b>Retailer (<?php echo sizeof($retRec);?>)</b>&nbsp;&emsp;&nbsp;
				<input id="retailerCheckbox" type="checkbox" onclick="toggleGroup('retailer'); toggleGroup1('retailer')" checked="checked" />&nbsp;&emsp;&nbsp; <b>Distributor (<?php echo sizeof($distRec);?>) </b>&nbsp;&emsp;&nbsp;
				<input id="distributorCheckbox" type="checkbox" onclick="toggleGroup('distributor')" checked="checked" />&nbsp;&emsp;&nbsp; <b>Mechanic (<?php echo sizeof($custRec);?>)</b>&nbsp;&emsp;&nbsp;
				<input id="customerCheckbox" type="checkbox" onclick="toggleGroup('customer')" checked="checked" />&nbsp;&emsp;&nbsp;
				Hot (<?php echo sizeof($custRec);?>)</b>&nbsp;&emsp;&nbsp;
				<input id="customerCheckbox" type="checkbox" onclick="toggleGroup('hot')" checked="checked" />
				Cold (<?php echo sizeof($custRec);?>)</b>&nbsp;&emsp;&nbsp;
				<input id="customerCheckbox" type="checkbox" onclick="toggleGroup('cold')" checked="checked" />
			</ul> -->
		</div>
		<table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">
			<tr>
				<td>
					<div id="map_canvas"></div>
				</td>
			</tr>
		</table>
	</div>
	<!--  end content-table-inner  -->
	<div class="clear">&nbsp;</div>
</div>
<!--  end content -->
<div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->
<div class="clear">&nbsp;</div> 
<!-- start footer -->         
<?php include("footer.php");?>
<!-- end footer -->
 
</body>
</html>