<?php
/*
* Created Date 21 jan 2016
* By Abhishek Jaiswal
*/

if($_SESSION['userLoginType'] == 5){
  $divisionIdString = implode(",", $divisionList);
  $condition = " division_id IN ($divisionIdString) AND ";
  $condition1 = " division_id IN ($divisionIdString) ";
} else {
  $condition = " ";
}

$divisionRec=$_objAdmin->_getSelectList2('table_division',"division_name as division,division_id",'',$condition1);

$retRec=$_objAdmin->_getSelectList2('table_retailer',"retailer_name as name,retailer_address as address,lat,lng,lat_lng_capcure_accuracy,display_outlet,division_id",''," $condition lat != '' AND lng != '' AND lat > 0 AND lng > 0 ORDER BY retailer_id ASC");
$distRec=$_objAdmin->_getSelectList2('table_distributors',"distributor_name as name,distributor_id,distributor_address as address,lat,lng,lat_lng_capcure_accuracy,display_outlet,division_id",''," $condition lat != '' AND lng != '' AND lat > 0 AND lng > 0 ORDER BY distributor_id ASC");
$custRec=$_objAdmin->_getSelectList2('table_customer',"customer_name as name,customer_address as address,lat,lng,lat_lng_capcure_accuracy,display_outlet",''," lat != '' AND lng != '' AND lat > 0 AND lng > 0 ORDER BY customer_id ASC");
x?>

<style type="text/css">
	html { height: 100% }
	body { height: 100%; margin: 0; padding: 0 }
	#map_canvas { height: 600px; }
	.tooltip {
		position:absolute;
		width: 150px;
		height: 150px;
		padding: 5px;
		margin:350px,120px,0px,100px;
		border: 1px solid gray;
		font-size: 9pt;
		font-family: Verdana;
		color: #000;
	}
</style>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>
<script type="text/javascript" src="javascripts/custom_map_tooltip.js"></script>
<script src="javascripts/jquery.min.1.10.2.js"></script>
<script type="text/javascript">
var infoWindow = new google.maps.InfoWindow();
	var customIcons = {
	retailer: {
	icon: 'images/retailerM.png'
	},
	distributor: {
	icon: 'images/distributorM.png'
	},
	customer: {
	icon: 'images/mechanicM.png'
	}
	};

	var markerGroups = {
  "all":[],
  "allInterest":[],
  "allDivision":[],
	"retailer": [],
  "retailerInterest":[],
  "retailerDivision":[],
  "distributor": [],
  "distributorInterest": [],
	"distributorDivision": [],
  "customer": [],
    "customerInterest": [],
	"customerDivision": []
	};


function load() {
  var map = new google.maps.Map(document.getElementById("map_canvas"), {
    center: new google.maps.LatLng('21.0000', '78.0000'),
    zoom: 5,
    mapTypeId: 'roadmap'
  });
  var infoWindow = new google.maps.InfoWindow();



  map.set('styles', [{
    zoomControl: false
  }, {
    featureType: "road.highway",
    elementType: "geometry.fill",
    stylers: [{
      color: "#ffd986"
    }]
  }, {
    featureType: "road.arterial",
    elementType: "geometry.fill",
    stylers: [{
      color: "#9e574f"
    }]
  }, {
    featureType: "road.local",
    elementType: "geometry.fill",
    stylers: [{
        color: "#d0cbc0"
      }, {
        weight: 1.1
      }

    ]
  }, {
    featureType: 'road',
    elementType: 'labels',
    stylers: [{
      saturation: -100
    }]
  }, {
    featureType: 'landscape',
    elementType: 'geometry',
    stylers: [{
      hue: '#ffff00'
    }, {
      gamma: 1.4
    }, {
      saturation: 82
    }, {
      lightness: 96
    }]
  }, {
    featureType: 'poi.school',
    elementType: 'geometry',
    stylers: [{
      hue: '#fff700'
    }, {
      lightness: -15
    }, {
      saturation: 99
    }]
  }]);

  //         downloadUrl("markers.xml", function (data) {

  
    <?php foreach ($distRec as $key => $value) { ?>
		var name = "<?php echo addslashes(str_replace(array('\r\n', '\r', '\n'), '', $value->name)); ?>";
		var address = "<?php echo addslashes(str_replace(array('\r\n', '\r', '\n'), '', $value->address)); ?>";
		var type = "distributor";
    var interest = "<?php echo addslashes(str_replace(array('\r\n', '\r', '\n'), '', $value->display_outlet)); ?>";
    var division = "<?php echo $value->division_id; ?>";
		var point = new google.maps.LatLng(<?php echo $value->lat;?>,<?php echo $value->lng;?>);
    	var html = "<b>" + name + "</b> <br/>" + address;
		var title="Name: <?php echo strip_tags(str_replace(array('\r\n', '\r', '\n'), '', $value->name)); ?>, Lat: <?php echo substr($value->lat,0,6); ?>,Long: <?php echo substr($value->lng,0,6) ?> , Accuracy: <?php echo $value->lat_lng_capcure_accuracy ?> ";
    	var marker = createMarker(point, name, address, type, map,title,interest,division);
	<?php }?>
	<?php foreach ($retRec as $key => $value) { ?>
		var name = "<?php echo addslashes(str_replace(array('\r\n', '\r', '\n'), '', $value->name)); ?>";
		var address = "<?php echo addslashes(str_replace(array('\r\n', '\r', '\n'), '', $value->address)); ?>";
		var type = "retailer";
    var interest = "<?php echo addslashes(str_replace(array('\r\n', '\r', '\n'), '', $value->display_outlet)); ?>";
		var division = "<?php echo $value->division_id; ?>";
    var point = new google.maps.LatLng(<?php echo $value->lat;?>,<?php echo $value->lng;?>);
    	var html = "<b>" + name + "</b> <br/>" + address;
    	var title="Name: <?php echo strip_tags(str_replace(array('\r\n', '\r', '\n'), '', $value->name)); ?>, Lat: <?php echo substr($value->lat,0,6); ?>,Long: <?php echo substr($value->lng,0,6) ?> , Accuracy: <?php echo $value->lat_lng_capcure_accuracy ?> ";
    	var marker = createMarker(point, name, address, type, map,title,interest,division);
	<?php }?>
	<?php foreach ($custRec as $key => $value) { ?>
		var name = "<?php echo addslashes(str_replace(array('\r\n', '\r', '\n'), '', $value->name)); ?>";
		var address = "<?php echo addslashes(str_replace(array('\r\n', '\r', '\n'), '', $value->address)); ?>";
		var type = "customer";
    var interest = "<?php echo addslashes(str_replace(array('\r\n', '\r', '\n'), '', $value->display_outlet)); ?>";
		var division = "<?php echo $value->division_id; ?>";
    var point = new google.maps.LatLng(<?php echo $value->lat;?>,<?php echo $value->lng;?>);
    	var html = "<b>" + name + "</b> <br/>" + address;
    	var title="Name: <?php echo strip_tags(str_replace(array('\r\n', '\r', '\n'), '', $value->name)); ?>, Lat: <?php echo substr($value->lat,0,6); ?>,Long: <?php echo substr($value->lng,0,6) ?> , Accuracy: <?php echo $value->lat_lng_capcure_accuracy ?> ";
    	var marker = createMarker(point, name, address, type, map,title,interest,division);
	<?php }?>
    Array.prototype.push.apply(markerGroups['all'],markerGroups.retailer);
    Array.prototype.push.apply(markerGroups['all'],markerGroups.distributor);
    Array.prototype.push.apply(markerGroups['all'],markerGroups.customer);
    Array.prototype.push.apply(markerGroups['allInterest'],markerGroups.retailerInterest);
    Array.prototype.push.apply(markerGroups['allInterest'],markerGroups.distributorInterest);
    Array.prototype.push.apply(markerGroups['allInterest'],markerGroups.customerInterest);
    Array.prototype.push.apply(markerGroups['allDivision'],markerGroups.retailerDivision);
    Array.prototype.push.apply(markerGroups['allDivision'],markerGroups.distributorDivision);
    Array.prototype.push.apply(markerGroups['allDivision'],markerGroups.customerDivision);
    //console.log(markerGroups.customer);
    bindInfoWindow(marker, map, infoWindow, html);
  }
function createMarker(point, name, address, type, map,title,interest,division) {
  var icon = customIcons[type] || {};
  var marker = new google.maps.Marker({
    map: map,
    position: point,
    icon: icon.icon,
    title:title,
    // shadow: icon.shadow,
    type: type
  });
  if (!markerGroups[type]) markerGroups[type] = [];
  markerGroups[type].push(marker);
  if(interest == ""){
    markerGroups[type+"Interest"].push("");
  } else {
    markerGroups[type+"Interest"].push(interest);
  }
  if(division == ""){
    markerGroups[type+"Division"].push("");
  } else {
    markerGroups[type+"Division"].push(division);
  }
  var html = "<b>" + name + "</b> <br/>" + address;
  bindInfoWindow(marker, map, infoWindow, html);
  return marker;
}

function toggleGroup(type,interest,division) {
    var count = 0;
    var counter = 0;
    if(markerGroups[type].length > 0 ){
        for (var i = 0; i < markerGroups[type].length; i++) {
            var marker = markerGroups[type][i];
            var markerInterest = markerGroups[type+"Interest"][i];
            var markerDivision = markerGroups[type+"Division"][i];

            if(division == "all"){
                if(interest == "all"){
                    if(type == "all"){
                        toggleDisable('all',count);
                        marker.setVisible(true);
                        count++;
                        counter++;
                    } else if(type == "retailer"){
                        toggleDisable('all',count);
                        marker.setVisible(true);
                        count++;
                        counter++;
                    } else if(type == "distributor"){
                        toggleDisable('all',count);
                        marker.setVisible(true);
                        count++;
                        counter++;
                    } else if(type == "customer"){
                        toggleDisable('all',count);
                        marker.setVisible(true);
                        count++;
                        counter++;
                    } else {
                        toggleDisable('all',count);
                    }
                } else if(interest == "hot"){
                    if(type == "all" && interest == markerInterest){
                        toggleDisable('all',count);
                        marker.setVisible(true);
                        count++;
                        counter++;
                    } else if(type == "retailer" && interest == markerInterest){
                        toggleDisable('all',count);
                        marker.setVisible(true);
                        count++;
                        counter++;
                    } else if(type == "distributor" && interest == markerInterest){
                        toggleDisable('all',count);
                        marker.setVisible(true);
                        count++;
                        counter++;
                    } else if(type == "customer" && interest == markerInterest){
                        toggleDisable('all',count);
                        marker.setVisible(true);
                        count++;
                        counter++;
                    } else {
                        toggleDisable('all',count);
                    }
                } else if(interest == "cold"){
                    if(type == "all" && interest == markerInterest){
                        toggleDisable('all',count);
                        marker.setVisible(true);
                        count++;
                        counter++;
                    } else if(type == "retailer" && interest == markerInterest){
                        toggleDisable('all',count);
                        marker.setVisible(true);
                        count++;
                        counter++;
                    } else if(type == "distributor" && interest == markerInterest){
                        toggleDisable('all',count);
                        marker.setVisible(true);
                        count++;
                        counter++;
                    } else if(type == "customer" && interest == markerInterest){
                        toggleDisable('all',count);
                        marker.setVisible(true);
                        count++;
                        counter++;
                    } else {
                        toggleDisable('all',count);
                    }
                } else {
                    count = 0;
                }
            } else if(division != "0") {
                if(interest == "all"){
                    if(type == "all" && division == markerDivision){
                        toggleDisable('all',count);
                        marker.setVisible(true);
                        count++;
                        counter++;
                    } else if(type == "retailer" && division == markerDivision){
                        toggleDisable('all',count);
                        marker.setVisible(true);
                        count++;
                        counter++;
                    } else if(type == "distributor" && division == markerDivision){
                        toggleDisable('all',count);
                        marker.setVisible(true);
                        count++;
                        counter++;
                    } else if(type == "customer" && division == markerDivision){
                        toggleDisable('all',count);
                        marker.setVisible(true);
                        count++;
                        counter++;
                    } else {
                        toggleDisable('all',count);
                    }
                } else if(interest == "hot"){
                    if(type == "all" && interest == markerInterest && division == markerDivision){
                        toggleDisable('all',count);
                        marker.setVisible(true);
                        count++;
                        counter++;
                    } else if(type == "retailer" && interest == markerInterest && division == markerDivision){
                        toggleDisable('all',count);
                        marker.setVisible(true);
                        count++;
                        counter++;
                    } else if(type == "distributor" && interest == markerInterest && division == markerDivision){
                        toggleDisable('all',count);
                        marker.setVisible(true);
                        count++;
                        counter++;
                    } else if(type == "customer" && interest == markerInterest && division == markerDivision){
                        toggleDisable('all',count);
                        marker.setVisible(true);
                        count++;
                        counter++;
                    } else {
                        toggleDisable('all',count);
                    }
                } else if(interest == "cold"){
                    if(type == "all" && interest == markerInterest && division == markerDivision){
                        toggleDisable('all',count);
                        marker.setVisible(true);
                        count++;
                        counter++;
                    } else if(type == "retailer" && interest == markerInterest && division == markerDivision){
                        toggleDisable('all',count);
                        marker.setVisible(true);
                        count++;
                        counter++;
                    } else if(type == "distributor" && interest == markerInterest && division == markerDivision){
                        toggleDisable('all',count);
                        marker.setVisible(true);
                        count++;
                        counter++;
                    } else if(type == "customer" && interest == markerInterest && division == markerDivision){
                        toggleDisable('all',count);
                        marker.setVisible(true);
                        count++;
                        counter++;
                    } else {
                        toggleDisable('all',count);
                    }
                } else {
                    count = 0;
                }
            } else {
                toggleDisable('all',count);
            }
        }
    } else {
        toggleDisable('all',count);
    }
    $("#count").text(counter);
}

function toggleDisable(type,count){
  if(count == 0){
    for (var i = 0; i < markerGroups[type].length; i++) {
      var marker = markerGroups[type][i];
      if(type == "all"){
        marker.setVisible(false);
      }
    }
  }
}

function bindInfoWindow(marker, map, infoWindow, html) {
  google.maps.event.addListener(marker, 'click', function() {
    infoWindow.setContent(html);
    infoWindow.open(map, marker);

  });
}

function downloadUrl(url, callback) {
  var request = window.ActiveXObject ? new ActiveXObject('Microsoft.XMLHTTP') : new XMLHttpRequest();

  request.onreadystatechange = function() {
    if (request.readyState == 4) {
      request.onreadystatechange = doNothing;
      callback(request, request.status);
    }
  };

  request.open('GET', url, true);
  request.send(null);
}

function doNothing() {}
google.maps.event.addDomListener(window, 'load', load);


function xmlParse(str) {
  if (typeof ActiveXObject != 'undefined' && typeof GetObject != 'undefined') {
    var doc = new ActiveXObject('Microsoft.XMLDOM');
    doc.loadXML(str);
    return doc;
  }

  if (typeof DOMParser != 'undefined') {
    return (new DOMParser()).parseFromString(str, 'text/xml');
  }

  return createElement('div', null);
}


$(function(){
  $(document).on("change","#customer",function(){
    var customer = $(this).val();
    var interest = $("#interest").val();
    var division = $("#division").val();
    toggleGroup(customer,interest,division);
  });
  $(document).on("change","#interest",function(){
    var interest = $(this).val();
    var customer = $("#customer").val();
    var division = $("#division").val();
    toggleGroup(customer,interest,division);
  });
  $(document).on("change","#division",function(){
    var interest = $("#interest").val();
    var customer = $("#customer").val();
    var division = $(this).val();
    toggleGroup(customer,interest,division);
  });
});

</script>
<body>
