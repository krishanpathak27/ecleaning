<?php
/*
* Created Date 21 jan 2016
* By Abhishek Jaiswal
*/
$divisionRec=$_objAdmin->_getSelectList2('table_division',"division_name as division,division_id",'',$condition1);

$stateRec=$_objAdmin->_getSelectList2('state',"state_name,state_id",'','status="A"');



if(isset($_POST['showReport']) && $_POST['showReport'] == 'View') {    

    // Customer Type    
    if($_POST['customer']!=''){
        $_SESSION['findCust'] = $_POST['customer'];
    }

    if($_POST['interest'] !=''){
        $_SESSION['findIntr'] = $_POST['interest'];
    }
    if($_POST['division'] !=''){
        $_SESSION['findDivision'] = $_POST['division'];
    }
    if($_POST['customerclass'] !=''){
        $_SESSION['customerClassRec'] = $_POST['customerclass'];
    }
    if($_POST['active'] !=''){
        $_SESSION['activeCust'] = $_POST['active'];
    }
    if($_POST['state'] !=''){
        $_SESSION['stateCust'] = $_POST['state'];
    }
    if($_POST['district'] !=''){
        $_SESSION['districtCust'] = $_POST['district'];
    }
    if($_POST['tehsil'] !=''){
        $_SESSION['tehsilCust'] = $_POST['tehsil'];
    }
     if($_POST['find'] !=''){
        $_SESSION['search'] = $_POST['find'];
    }
}



 function getDistance2($latitude1, $longitude1, $latitude2, $longitude2) {  
    // echo $latitude1.'-'.$longitude1.'-'.$latitude2.'-'.$longitude2;
    // echo "<br>";

    $earth_radius = 6371;  
                          
    $dLat = deg2rad($latitude2 - $latitude1);  
    $dLon = deg2rad($longitude2 - $longitude1);  
                          
    $a = sin($dLat/2) * sin($dLat/2) + cos(deg2rad($latitude1)) * cos(deg2rad($latitude2)) * sin($dLon/2) * sin($dLon/2);  
    $c = 2 * asin(sqrt($a));  
    $d = $earth_radius * $c;  
    return $d;  
  }  


function distance($lat1, $lon1, $lat2, $lon2, $unit) {

  $theta = $lon1 - $lon2;
  $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
  $dist = acos($dist);
  $dist = rad2deg($dist);
  $miles = $dist * 60 * 1.1515;
  $unit = strtoupper($unit);

  if ($unit == "K") {
      return ($miles * 1.609344);
  } else if ($unit == "N") {
      return ($miles * 0.8684);
  } else {
      return $miles;
  }
}

/*
echo "GGG";
echo getDistance2('28.6139391', '77.2090212', '28.499484999999996', '77.08453833333333');

echo "GGG";
exit;*/








// if($_SESSION['findCust']!='' && $_SESSION['findCust']!='all'){
//     if($_SESSION['findCust'] =="retailer"){
//         $custCond=" ta.activity_type='5' AND ";
//         if($_SESSION['findDivision']=="all"){
//             $divisionrCond = " AND tr.division_id IN(".$divisionIdString.")";
//         } else {
//             $divisionrCond = " AND tr.division_id IN(".$_SESSION['findDivision'].")";
//         }
//     }
//     else if($_SESSION['findCust'] =="distributor"){
//         $custCond=" ta.activity_type='24' AND ";
//         if($_SESSION['findDivision']=="all"){
//             $divisiondCond = " AND td.division_id IN(".$divisionIdString.")";
//         } else {
//             $divisiondCond = " AND td.division_id IN(".$_SESSION['findDivision'].")";
//         }
//     }
//     else if($_SESSION['findCust'] =="customer"){
//         $custCond=" ta.activity_type='25' AND ";
//         if($_SESSION['findDivision']!="all"){
//             $divisioncCond = " AND tc.customer_id < 0";
//         } else {
//             $divisioncCond="";
//         }
//     }
// } else {
//     $custCond = " ";

// }













// select R.retailer_id, R.retailer_name, R.lat, R.lng, S1.lat AS survey_lat, S1.lng AS survey_lng from table_retailer AS R LEFT JOIN table_survey AS S1 ON R.retailer_id = S1.retailer_id LEFT JOIN table_survey AS S2 ON S2.retailer_id = S1.retailer_id AND S2.survey_id > S1.survey_id WHERE R.account_id=1 and S2.survey_id IS NULL AND S1.lat IS NOT NULL

//print_r($_SESSION   );


// Conditions 

$intrestedCondition = "";
$classCondition = "";
$retailerActiveInactiveCond="";
$distributorActiveInactiveCond="";
$customerActiveInactiveCond="";
$divisionCondition = "";
$stateCondition = "";
$distributorCondition = "";
$talukaCondition = "";
$searchCondition = "";


if(isset($_SESSION['findIntr']) && $_SESSION['findIntr']!="all"){
    $intrestedCondition =" AND display_outlet='".$_SESSION['findIntr']."'";
}

if(isset($_SESSION['customerClassRec']) && $_SESSION['customerClassRec']!='all'){
    $classCondition =" AND rr.relationship_id='".$_SESSION['customerClassRec']."'";
} 

if(isset($_SESSION['activeCust']) && $_SESSION['activeCust']!='all'){
    $retailerActiveInactiveCond =" AND R.status='".$_SESSION['activeCust']."'";
    $distributorActiveInactiveCond=" AND D.status='".$_SESSION['activeCust']."'";
    $customerActiveInactiveCond =" AND C.status='".$_SESSION['activeCust']."'";
}


if($_SESSION['userLoginType'] == 5 && !isset($_SESSION['findDivision'])){
    $divisionIdString = implode(",", $divisionList);
    $divisionCondition = " AND dV.division_id IN ($divisionIdString) ";
} 

if(isset($_SESSION['findDivision']) && $_SESSION['findDivision']!="all"){
   $divisionCondition = " AND dV.division_id IN(".$_SESSION['findDivision'].")";
}


if(isset($_SESSION['stateCust']) && $_SESSION['stateCust']!="all"){
    $stateCondition = " AND ST.state_id IN(".$_SESSION['stateCust'].")";
}

if(isset($_SESSION['districtCust']) && $_SESSION['districtCust']!="all"){
    $cityCondition = " AND CT.city_id IN(".$_SESSION['districtCust'].")";
}

if(isset($_SESSION['tehsilCust']) && $_SESSION['tehsilCust']!="all"){
    $talukaCondition = " AND TL.taluka_id IN(".$_SESSION['tehsilCust'].")";
}

if(isset($_SESSION['search'])){

$address=$_POST['find'];
$lat1 = '';
$lon1 = '';


 // Get lat and long by address         
        //$address = $dlocation; // Google HQ
        //$prepAddr = str_replace(' ','+',$address);
        $Address = urlencode($address);
        $geocode=file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address='.$Address.'&sensor=false');
        $output= json_decode($geocode);
        $lat1 = $output->results[0]->geometry->location->lat;
        $lon1 = $output->results[0]->geometry->location->lng;

//$geocode_stats = file_get_contents("http://maps.googleapis.com/maps/api/geocode/json?address=".$address."&sensor=false");

//$output_deals = json_decode($geocode_stats);


//$latLng = $output_deals->results[0]->geometry->location;

   //$lat = $latLng->lat;
   //$lng = $latLng->lng;    


$Data1 = $_objAdmin->_getSelectList("viewRetDisCusSurvey",'survey_id, retailer_id, distributor_id,customer_id','',"" );
// echo "<pre>";
// print_r($Data1);
// exit;

$vars1 = array();
$vars2 = array();
$vars3 = array();

 $values1 = '';
 $values2 = '';
 $values3 = '';

 $retailerINCircle = "";
 $distributorINCircle = "";
 $customerINCircle = "";


foreach ($Data1 as $key => $value) {
    # code...

    //print_r($value);
    //exit;
    $surveyData = array();
    $row        = 0;
    $lat2       = '';
    $lon2       = '';


    $surveyData = $_objAdmin->_getSelectList("table_survey",'`lat`, `lng`',''," survey_id='".$value->survey_id."' AND lat>0 AND lng>0 " );
    // echo "<pre>";
    // print_r($surveyData);

    if(sizeof($surveyData)>0) {

        $lat2 = $surveyData[0]->lat;
        $lon2 = $surveyData[0]->lng;

        if($value->retailer_id>0) {
            $row = getDistance2($lat1, $lon1, $lat2, $lon2);
           // echo "Retailer ".$value->retailer_id.' Radius='.$row.'<br>';
            if($row <  10)
                 $vars1[]  = $value->retailer_id;

              
        } else if($value->distributor_id>0) {
            $row = getDistance2($lat1, $lon1, $lat2, $lon2);
            //echo "Distributor ".$value->distributor_id.' Radius='.$row.'<br>';
            if($row <  10)
                 $vars2[]  = $value->distributor_id;
        } else if($value->customer_id>0) {
            $row = getDistance2($lat1, $lon1, $lat2, $lon2);
            //echo "Customer ".$value->customer_id.' Radius='.$row.'<br>';
            if($row <  10)
                 $vars3[]  = $value->customer_id;
        }
       
    }
}
    //echo "<pre>";
    // print_r($vars1);
    // print_r($vars2);
    // print_r($vars3); 
    //exit;   
    // echo "<br>";
    // echo sizeof($vars1);
    //  echo sizeof($vars2);
    //   echo sizeof($vars3);

    if(sizeof($vars1)>0) {
        $values1 = implode(',',$vars1);
        $retailerINCircle = ' AND R.retailer_id IN ('.$values1.')';
    } else {
        $retailerINCircle = ' AND R.retailer_id IN (0)';
    }

    if(sizeof($vars2)>0) {
        $values2 = implode(',',$vars2);
        $distributorINCircle = ' AND D.distributor_id IN ('.$values2.')';
    } else {
        $distributorINCircle = ' AND D.distributor_id IN (0)';
    }

    if(sizeof($vars3)>0) {
        $values3 = implode(',',$vars3);
        $customerINCircle = ' AND C.customer_id IN ('.$values3.')';
    } else {
        $customerINCircle = ' AND C.customer_id IN (0)';
    }


//exit;


//     $Data1 = mysql_query("SELECT retailer_id,(3959 * acos( cos( radians($lat) ) * cos( radians( lat ) ) * 
// cos( radians( lng ) - radians($lng) ) + sin( radians($lat) ) * 
// sin( radians( lat ) ) ) ) AS distance FROM viewRetDisCusSurvey HAVING
// distance < 25 AND retailer_id > 0 ORDER BY distance ASC ");

//     $vars1 = array();
//     while($res1=mysql_fetch_array($Data1))
//     { 
//         $vars1[].=$res1['retailer_id'];   
         

//     } 
//     $values1= implode(',',$vars1);
   

//      $Data2 = mysql_query("SELECT distributor_id,(3959 * acos( cos( radians($lat) ) * cos( radians( lat ) ) * 
//     cos( radians( lng ) - radians($lng) ) + sin( radians($lat) ) * 
//     sin( radians( lat ) ) ) ) AS distance FROM viewRetDisCusSurvey HAVING
//     distance < 25 ORDER BY distance ASC  ");


//    $vars2 = array();
//     while($res2=mysql_fetch_array($Data2))
//     { 


//        $vars2[] .=$res2['distributor_id'];   
         

//     } 
//     $values2= implode(',',$vars2);
    

//      $Data3 = mysql_query("SELECT customer_id,(3959 * acos( cos( radians($lat) ) * cos( radians( lat ) ) * 
//     cos( radians( lng ) - radians($lng) ) + sin( radians($lat) ) * 
//     sin( radians( lat ) ) ) ) AS distance FROM viewRetDisCusSurvey HAVING
//     distance < 25 ORDER BY distance ASC ");


//    $vars3 = array();
//     while($res3=mysql_fetch_array($Data3))
//     { 


//        $vars3[] .=$res3['customer_id'];   
         

//     } 
//  $values3= implode(',',$vars3);
 

   
 //$searchCondition1=" AND S1.retailer_id IN('".$values1."')";
 //$searchCondition2=" AND S1.distributor_id IN('".$values2."')";
 //$searchCondition3=" AND S1.customer_id IN('".$values3."')";


 //$searchCondition1=" AND S1.lat='".$lat."' AND S1.lng = '".$lng."'";
 //$searchCondition2=" AND S1.distributor_id IN('".$values2."')";
 //$searchCondition3=" AND S1.customer_id IN('".$values3."')";


    //$searchCondition1= "AND ST.state_name = '".$_SESSION['search']."' OR CT.city_name = '".$_SESSION['search']."' OR TL.taluka_name = '".$_SESSION['search']."' OR R.retailer_address LIKE '%".$_SESSION['search']."%'";

   //$searchCondition2= "AND ST.state_name = '".$_SESSION['search']."' OR CT.city_name = '".$_SESSION['search']."' OR TL.taluka_name = '".$_SESSION['search']."' OR D.distributor_address LIKE '%".$_SESSION['search']."%'";

   // $searchCondition3= "AND ST.state_name = '".$_SESSION['search']."' OR CT.city_name = '".$_SESSION['search']."' OR TL.taluka_name = '".$_SESSION['search']."' OR C.customer_address LIKE '%".$_SESSION['search']."%'";

}

// Conditions 

if(isset($_REQUEST['reset']) && $_REQUEST['reset']=='yes'){
    unset($_SESSION['findCust']);  
    unset($_SESSION['findIntr']);    
    unset($_SESSION['findDivision']); 
    unset($_SESSION['customerClassRec']); 
    unset($_SESSION['activeCust']); 
    unset($_SESSION['stateCust']); 
    unset($_SESSION['districtCust']); 
    unset($_SESSION['tehsilCust']);
    unset($_SESSION['search']); 
    header("Location: customer_geo_tagging.php");
}






if( !isset($_SESSION['findCust']) || $_SESSION['findCust'] == 'all' || $_SESSION['findCust'] == 'R' ) {

$retailerData = $_objAdmin->_getSelectList("table_retailer AS R
    LEFT JOIN viewRetDisCusSurvey AS S1 ON S1.retailer_id = R.retailer_id 
    LEFT JOIN table_relationship as rr on rr.relationship_id=R.relationship_id 
    left join table_division AS dV ON dV.division_id = R.division_id 
    left join state as ST on ST.state_id=R.state 
    left join city as CT on CT.city_id=R.city 
    left join table_taluka as TL on TL.taluka_id=R.taluka_id",
    'R.retailer_id AS id, R.retailer_name AS name, R.retailer_address AS address, R.display_outlet AS outlet, R.lat_lng_capcure_accuracy AS accuracy, R.division_id, R.lat, R.lng,S1.survey_id,S1.lat AS survey_lat, S1.lng AS survey_lng, "Retailer" AS customer_type,dV.division_name,rr.relationship_code,ST.state_name,CT.city_name,TL.taluka_name,R.status ',''," R.status!='D' AND (S1.lat !='') $intrestedCondition $classCondition $retailerActiveInactiveCond $divisionCondition $stateCondition $cityCondition $talukaCondition  $searchCondition1 $retailerINCircle" );

} else {
    $retailerData  = array();
}


if( !isset($_SESSION['findCust']) || $_SESSION['findCust'] == 'all' || $_SESSION['findCust'] == 'D' ) {

$distributorData = $_objAdmin->_getSelectList("table_distributors AS D
    LEFT JOIN viewRetDisCusSurvey AS S1 ON S1.distributor_id = D.distributor_id 
     LEFT JOIN table_relationship as rr on rr.relationship_id=D.relationship_id 
    left join table_division AS dV ON dV.division_id = D.division_id 
     left join state as ST on ST.state_id=D.state 
    left join city as CT on CT.city_id=D.city 
    left join table_taluka as TL on TL.taluka_id=D.taluka_id",
    'D.distributor_id AS id, D.distributor_name AS name, D.distributor_address AS address, D.display_outlet AS outlet,D.lat_lng_capcure_accuracy AS accuracy, D.division_id, D.lat, D.lng,S1.survey_id, S1.lat AS survey_lat, S1.lng AS survey_lng, "Distributor" AS customer_type,dV.division_name,rr.relationship_code,ST.state_name,CT.city_name,TL.taluka_name,D.status ',''," D.status!='D' AND (S1.lat !='') $intrestedCondition $classCondition $distributorActiveInactiveCond  $divisionCondition  $stateCondition $cityCondition $talukaCondition $searchCondition2 $distributorINCircle");

} else {
    $distributorData  = array();
}

if( !isset($_SESSION['findCust']) || $_SESSION['findCust'] == 'all' || !in_array($_SESSION['findCust'], array('R','D')) ) {
$typeCondition = "";
//echo $_SESSION['findCust'];
if( isset($_SESSION['findCust']) AND !in_array($_SESSION['findCust'], array('R','D', 'all'))) {
  $typeCondition = " AND C.customer_type='".$_SESSION['findCust']."'";  
}


$customerData = $_objAdmin->_getSelectList("table_customer AS C
    LEFT JOIN viewRetDisCusSurvey AS S1 ON S1.customer_id = C.customer_id 
    left join table_division AS dV ON dV.division_id = C.division_id 
     left join state as ST on ST.state_id=C.state 
    left join city as CT on CT.city_id=C.city 
    left join table_taluka as TL on TL.taluka_id=C.taluka_id",
    'C.customer_id AS id, C.customer_name AS name, C.customer_address AS address, C.display_outlet AS outlet, C.lat_lng_capcure_accuracy AS accuracy, C.division_id, C.lat, C.lng, S1.lat AS survey_lat,S1.survey_id, S1.lng AS survey_lng, C.customer_type,dV.division_name,"-" as relationship_code,ST.state_name,CT.city_name,TL.taluka_name,C.status ','',"  C.status!='D' AND (S1.lat !='' || C.lat !='') $intrestedCondition  $customerActiveInactiveCond  $divisionCondition  $stateCondition $cityCondition $talukaCondition $typeCondition $searchCondition3 $customerINCircle");
} else {
    $customerData  = array();
}

if(sizeof($retailerData)>0 && sizeof($distributorData)>0 && sizeof($customerData)>0) {
    $custData = array_merge($retailerData, $distributorData, $customerData);
} else if(sizeof($retailerData)> 0 && sizeof($distributorData)>0 && sizeof($customerData)==0) {
    $custData = array_merge($retailerData, $distributorData);
} else if(sizeof($retailerData)> 0 && sizeof($distributorData)==0 && sizeof($customerData)>0) {
    $custData = array_merge($retailerData, $customerData);
} else if(sizeof($retailerData)== 0 && sizeof($distributorData)>0 && sizeof($customerData)>0) {
    $custData = array_merge($distributorData, $customerData);
} else if(sizeof($retailerData)> 0 && sizeof($distributorData)==0 && sizeof($customerData)==0) {
    $custData = $retailerData;
} else if(sizeof($retailerData)== 0 && sizeof($distributorData)>0 && sizeof($customerData)==0) {
    $custData = $distributorData;
} else if(sizeof($retailerData)== 0 && sizeof($distributorData)==0 && sizeof($customerData)>0) {
    $custData = $customerData;
}

     //echo "<pre>";
    // print_r($retailerData);
    // echo "Distributor";
    // print_r($distributorData);
    // echo "Customer";
    // print_r($customerData);
    // echo "All Data";
    // print_r($custData);
    // exit;


// $custData = $_objAdmin->_getSelectList2("table_activity as ta
//                 LEFT JOIN table_retailer as tr ON ta.ref_id=tr.retailer_id AND ta.ref_type='5' $custrCond $divisionrCond $staterCond $cityrCond $talukarCond $acticerCond $classrCond
//                 LEFT JOIN table_distributors as td ON ta.ref_id=td.distributor_id AND ta.ref_type='24' $custdCond $divisiondCond $statedCond $citydCond $talukadCond $acticedCond $classdCond
//                 LEFT JOIN table_customer as tc ON ta.ref_id=tc.customer_id AND ta.ref_type='25' $custcCond $divisioncCond $statecCond $citycCond $talukacCond $acticecCond
//                 ",'ta.activity_type,
//                 CASE WHEN ta.activity_type="5" THEN tr.retailer_id WHEN ta.activity_type="24" THEN td.distributor_id WHEN ta.activity_type="25" THEN tc.customer_id END AS customer_id,
//                 CASE WHEN ta.activity_type="5" THEN tr.retailer_name WHEN ta.activity_type="24" THEN td.distributor_name WHEN ta.activity_type="25" THEN tc.customer_name END AS customer_name,
//                 CASE WHEN ta.activity_type="5" THEN tr.retailer_code WHEN ta.activity_type="24" THEN td.distributor_code WHEN ta.activity_type="25" THEN tc.customer_code END AS customer_code,
//                 CASE WHEN ta.activity_type="5" THEN "Retailer" WHEN ta.activity_type="24" THEN "Distributor" WHEN ta.activity_type="25" THEN "Shakti-Partner" END AS customer_type,
//                 CASE WHEN ta.activity_type="5" THEN tr.display_outlet WHEN ta.activity_type="24" THEN td.display_outlet WHEN ta.activity_type="25" THEN tc.display_outlet END AS outlet,
//                 CASE WHEN ta.activity_type="5" THEN tr.retailer_address WHEN ta.activity_type="24" THEN td.distributor_address WHEN ta.activity_type="25" THEN tc.customer_address END AS address,
//                 CASE WHEN ta.activity_type="5" THEN tr.lat WHEN ta.activity_type="24" THEN td.lat WHEN ta.activity_type="25" THEN tc.lat END AS lat,
//                 CASE WHEN ta.activity_type="5" THEN tr.lng WHEN ta.activity_type="24" THEN td.lng WHEN ta.activity_type="25" THEN tc.lng END AS lng',
//                 '',"$custCond $intrCond ta.activity_type IN (5,24,25)");
/*$custData = $_objAdmin->_getSelectList2("table_activity as ta
                LEFT JOIN table_retailer as tr ON ta.ref_id=tr.retailer_id AND ta.ref_type='5' $custrCond $divisionrCond $staterCond $cityrCond $talukarCond $acticerCond $classrCond
                LEFT JOIN table_distributors as td ON ta.ref_id=td.distributor_id AND ta.ref_type='24' $custdCond $divisiondCond $statedCond $citydCond $talukadCond $acticedCond $classdCond
                LEFT JOIN table_customer as tc ON ta.ref_id=tc.customer_id AND ta.ref_type='25' $custcCond $divisioncCond $statecCond $citycCond $talukacCond $acticecCond
                LEFT JOIN table_survey as ts ON ta.ref_id=ts.retailer_id AND ts.retailer_id > 0 AND ta.ref_type='5'
                LEFT JOIN table_survey as ts1 ON ta.ref_id=ts1.distributor_id AND ts1.retailer_id=0 AND ta.ref_type='24'",'ta.activity_type,
                CASE WHEN ta.activity_type="5" THEN tr.retailer_id WHEN ta.activity_type="24" THEN td.distributor_id WHEN ta.activity_type="25" THEN tc.customer_id END AS customer_id,
                CASE WHEN ta.activity_type="5" THEN tr.retailer_name WHEN ta.activity_type="24" THEN td.distributor_name WHEN ta.activity_type="25" THEN tc.customer_name END AS customer_name,
                CASE WHEN ta.activity_type="5" THEN tr.retailer_code WHEN ta.activity_type="24" THEN td.distributor_code WHEN ta.activity_type="25" THEN tc.customer_code END AS customer_code,
                CASE WHEN ta.activity_type="5" THEN "Retailer" WHEN ta.activity_type="24" THEN "Distributor" WHEN ta.activity_type="25" THEN "Shakti-Partner" END AS customer_type,
                CASE WHEN ta.activity_type="5" THEN tr.display_outlet WHEN ta.activity_type="24" THEN td.display_outlet WHEN ta.activity_type="25" THEN tc.display_outlet END AS outlet,
                CASE WHEN ta.activity_type="5" THEN tr.retailer_address WHEN ta.activity_type="24" THEN td.distributor_address WHEN ta.activity_type="25" THEN tc.customer_address END AS address,
                CASE WHEN ta.activity_type="5" THEN ts.lat WHEN ta.activity_type="24" THEN ts1.lat WHEN ta.activity_type="25" THEN tc.lat END AS lat,
                CASE WHEN ta.activity_type="5" THEN ts.lng WHEN ta.activity_type="24" THEN ts1.lng WHEN ta.activity_type="25" THEN tc.lng END AS lng',
                '',"$custCond $intrCond ta.activity_type IN (5,24,25)");
*/
?>

<style type="text/css">
    html { height: 100% }
    body { height: 100%; margin: 0; padding: 0 }
    #map_canvas { height: 500px; }
    .tooltip {
        position:absolute;
        width: 150px;
        height: 150px;
        padding: 5px;
        margin:350px,120px,0px,100px;
        border: 1px solid gray;
        font-size: 9pt;
        font-family: Verdana;
        color: #000;
    }
</style>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>
<script type="text/javascript" src="javascripts/custom_map_tooltip.js"></script>
<script src="javascripts/jquery.min.1.10.2.js"></script>
<script type="text/javascript">
var infoWindow = new google.maps.InfoWindow();
    var customIcons = {
    Retailer: {
    icon: 'images/retailerM.png'
    },
    Distributor: {
    icon: 'images/distributorM.png'
    },
    C: { // C- Shakti Partner
    icon: 'images/shakti_partnerM.png'
    },
    P: { // S- pumps installar
    icon: 'images/paleblue_MarkerS.png'
    },
    S: { // S- solar pumps installar
    icon: 'images/orange_MarkerS.png'
    }
    };

    var markerGroups = {
    "all":[],
    "retailer": [],
    "distributor": [],
    "customer": [],
    };

function load() {
  var map = new google.maps.Map(document.getElementById("map_canvas"), {
    //center: new google.maps.LatLng('21.0000', '78.0000'),
    center: new google.maps.LatLng("<?php if(isset($_SESSION['search'])){ echo $lat1;} else { echo '21.0000';}?>", "<?php if(isset($_SESSION['search'])){ echo $lon1;} else{ echo '78.0000';}?>"),
    zoom: <?php if(isset($_SESSION['search'])){ echo '15';} else { echo '5';} ?>,
    zoom: 5,
    mapTypeId: 'roadmap'
  });
  var infoWindow = new google.maps.InfoWindow();

  <?php if(isset($_SESSION['search'])){?>
            var name = "<?PHP ECHO $_SESSION['search'];?>";
            var address = "<?php echo addslashes(str_replace(array('\r\n', '\r', '\n'), '', $_SESSION['search'])); ?>";
            var type = "<?php echo addslashes(str_replace(array('\r\n', '\r', '\n'), '', '')); ?>";
            var interest = "<?php echo addslashes(str_replace(array('\r\n', '\r', '\n'), '', '')); ?>";
            var division = "";
            var point = new google.maps.LatLng(<?php echo $lat1;?>,<?php echo $lon1;?>);
            var html = "<b>" + name + "</b> <br/>" + "<?PHP ECHO $_SESSION['search'];?>";
            var title = "Name: <?php echo strip_tags(str_replace(array('\r\n', '\r', '\n'), '', $_SESSION['search'])); ?>, Lat: <?php echo substr($lat1,0,6); ?>,Long: <?php echo substr($lon1,0,6) ?>";
            var marker = createMarker(point, name, address, type, map,title,interest,division);

<?php }?>



  map.set('styles', [{
    zoomControl: false
  }, {
    featureType: "road.highway",
    elementType: "geometry.fill",
    stylers: [{
      color: "#ffd986"
    }]
  }, {
    featureType: "road.arterial",
    elementType: "geometry.fill",
    stylers: [{
      color: "#9e574f"
    }]
  }, {
    featureType: "road.local",
    elementType: "geometry.fill",
    stylers: [{
        color: "#d0cbc0"
      }, {
        weight: 1.1
      }

    ]
  }, {
    featureType: 'road',
    elementType: 'labels',
    stylers: [{
      saturation: -100
    }]
  }, {
    featureType: 'landscape',
    elementType: 'geometry',
    stylers: [{
      hue: '#ffff00'
    }, {
      gamma: 1.4
    }, {
      saturation: 82
    }, {
      lightness: 96
    }]
  }, {
    featureType: 'poi.school',
    elementType: 'geometry',
    stylers: [{
      hue: '#fff700'
    }, {
      lightness: -15
    }, {
      saturation: 99
    }]
  }]);
  var count=0;

  //         downloadUrl("markers.xml", function (data) {
    <?php foreach ($custData as $key => $value) {
      //  $latittude  = 0;
      //  $lognitutde = 0;

        $surveyLatLng = $_objAdmin->_getSelectList("table_survey AS S1 ",'S1.lat AS survey_lat, S1.lng AS survey_lng',''," S1.survey_id='".$value->survey_id."'");

        foreach ($surveyLatLng as $key => $surveyDts) {
           $latittude  = "";
            $lognitutde = "";

            if($surveyDts->survey_lat !="" && $surveyDts->survey_lng !="" && $surveyDts->survey_lat !="0" && $surveyDts->survey_lng !="0"){
                $latittude  = $surveyDts->survey_lat;
                $lognitutde = $surveyDts->survey_lng;
            }

        }



        // } else if($value->lat !="" && $value->lng !="" && $value->lat !="0" && $value->lng !="0"){
        //     $latittude  = $value->lat;
        //     $lognitutde = $value->lng;
        // }


        if(sizeof($surveyLatLng)==0) {
            
            if($value->lat !="" && $value->lng !="" && $value->lat !="0" && $value->lng !="0"){
                $latittude  = $value->lat;
                $lognitutde = $value->lng;
            }


        }

            if($value->survey_lat !="" && $value->survey_lng !=""){
            $latittude  = $value->survey_lat;
            $lognitutde = $value->survey_lng;
        } else if($value->lat !="" && $value->lng !=""){
           // $latittude  = $value->lat;
            //$lognitutde = $value->lng;
        }

        if($value->customer_type == 'C') {
            $customer_type = 'Mechanics';
        } else if($value->customer_type == 'P') {
            $customer_type = 'Pump Installer';
        } else if($value->customer_type == 'S') {
            $customer_type = 'Solar Pumps Installer';
        } else {
            $customer_type = $value->customer_type;
        }


        if($latittude !="" &&  $lognitutde !=""){?>

            var name = "<?php echo addslashes(str_replace(array('\r\n', '\r', '\n'), '', $value->name)); ?>";
            var address = "<?php echo addslashes(str_replace(array('\r\n', '\r', '\n'), '', $value->address)); ?>";
            var type = "<?php echo addslashes(str_replace(array('\r\n', '\r', '\n'), '', $value->customer_type)); ?>";
            var interest = "<?php echo addslashes(str_replace(array('\r\n', '\r', '\n'), '', $value->outlet)); ?>";
            var division = "<?php echo $value->division_id; ?>";
            var point = new google.maps.LatLng(<?php echo $latittude;?>,<?php echo $lognitutde;?>);
            var html = "<b>" + name + "</b> <br/>" + address;
            var title = "Name: <?php echo strip_tags(str_replace(array('\r\n', '\r', '\n'), '', $value->name)); ?>, Lat: <?php echo substr($latittude,0,6); ?>,Long: <?php echo substr($lognitutde,0,6) ?> , Accuracy: <?php echo $value->lat_lng_capcure_accuracy ?>, type: <?php echo $customer_type ?>";
            var marker = createMarker(point, name, address, type, map,title,interest,division);
            count++;
    <?php } 
    }?>
    $("#count").text(count);
}

function createMarker(point, name, address, type, map,title,interest,division) {
    // if(type=="Mechanics"){
    //     type = "Mechanic";
    // }
  var icon = customIcons[type] || {};
  var marker = new google.maps.Marker({
    map: map,
    position: point,
    icon: icon.icon,
    title:title,
    type: type
  });
  var html = "<b>AA" + name + "</b> <br/>" + address;
  bindInfoWindow(marker, map, infoWindow, html);
  return marker;
}

function bindInfoWindow(marker, map, infoWindow, html) {
  google.maps.event.addListener(marker, 'click', function() {
    infoWindow.setContent(html);
    infoWindow.open(map, marker);

  });
}

function downloadUrl(url, callback) {
  var request = window.ActiveXObject ? new ActiveXObject('Microsoft.XMLHTTP') : new XMLHttpRequest();

  request.onreadystatechange = function() {
    if (request.readyState == 4) {
      request.onreadystatechange = doNothing;
      callback(request, request.status);
    }
  };

  request.open('GET', url, true);
  request.send(null);
}

function doNothing() {}
google.maps.event.addDomListener(window, 'load', load);


function xmlParse(str) {
  if (typeof ActiveXObject != 'undefined' && typeof GetObject != 'undefined') {
    var doc = new ActiveXObject('Microsoft.XMLDOM');
    doc.loadXML(str);
    return doc;
  }

  if (typeof DOMParser != 'undefined') {
    return (new DOMParser()).parseFromString(str, 'text/xml');
  }

  return createElement('div', null);
}


</script>
<body>
