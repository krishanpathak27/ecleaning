<?php
/*
* Created Date 21 jan 2016
* By Abhishek Jaiswal
*/

  /*All Conditions
  */

    //for Retailers
    $stateCondiRet='';
    $cityCondiRet='';
    $talukaCondiRet='';
    $marketCondiRet='';
    $hotColdCondiRet='';
    $activeCondiRet='';
    $divisionCondiRet='';
    $classCondiRet='';
    $pincodeCondiRet='';
    //$PinBlackCondi='';
     

    if(isset($_SESSION['AllStates']) && !empty($_SESSION['AllStates'])){
      if(isset($_SESSION['AllStates']) && !empty($_SESSION['AllStates'])){
        $stateCondiRet=" AND R.state IN(".$_SESSION['AllStates'].")";        
      }

      if(isset($_SESSION['AllCity']) && !empty($_SESSION['AllCity'])){
        $cityCondiRet=" AND R.city IN(".$_SESSION['AllCity'].")";
      }

      if(isset($_SESSION['AllTaluka']) && !empty($_SESSION['AllTaluka'])){
        $talukaCondiRet=" AND R.taluka_id IN(".$_SESSION['AllTaluka'].")";
      }

      if(isset($_SESSION['AllMarket']) && !empty($_SESSION['AllMarket'])){
        $marketCondiRet = " AND R.market_id IN(".$_SESSION['AllMarket'].")";
      }

      if(isset($_SESSION['AllDivision']) && !empty($_SESSION['AllDivision'])){
        $divisionCondiRet = " AND R.division_id IN(".$_SESSION['AllDivision'].")";
      }else{
        $divisionCondiRet = " AND (R.division_id ='' || R.division_id ='0')";
      }

      if(isset($_SESSION['AllClass']) && !empty($_SESSION['AllClass'])){
        $classCondiRet = " AND R.relationship_id IN(".$_SESSION['AllClass'].")";     
      }else{
        $classCondiRet = " AND (R.relationship_id ='' || R.relationship_id ='0')";
      }

      if(isset($_SESSION['AllHotCold']) && !empty($_SESSION['AllHotCold'])){       
        $hotColdCondiRet = " AND R.display_outlet IN(".$_SESSION['AllHotCold'].")";
      }else{
        $hotColdCondiRet = " AND R.display_outlet =''";
      }

      if(isset($_SESSION['AllActivStatus']) && !empty($_SESSION['AllActivStatus'])){      
        $activeCondiRet = " AND R.status IN(".$_SESSION['AllActivStatus'].")";
      }else{
        $activeCondiRet = " AND R.status =''";
      }

       /*if(isset($_SESSION['AllPincode']) && !empty($_SESSION['AllPincode'])){      
        $pincodeCondiRet = " AND R.zipcode IN(".$_SESSION['AllPincode'].")";
        $expldArray= explode(',', $_SESSION['AllPincode']);
        if(in_array(000000, $expldArray)){
          $PinBlackCondi = " AND R.zipcode=''";
        }
      }*/
      if(isset($_SESSION['AllPincode']) && !empty($_SESSION['AllPincode'])){ //all PinCode
               
            $expldArray= explode(',', $_SESSION['AllPincode']);
            if(in_array(000000, $expldArray)){              
              unset($expldArray[0]);
              if(sizeof($expldArray)>0){
                $Pincode = implode(',', $expldArray);  
              $pincodeCondiRet = " AND (R.zipcode IN(".$Pincode.") || R.zipcode='' || R.zipcode='0')";
              }else{
                $pincodeCondiRet = " AND (R.zipcode='' || R.zipcode='0')";
              }
            }else{ 
               $pincodeCondiRet = " AND (R.zipcode IN(".$_SESSION['AllPincode']."))";
            }
      }

    }
    else if(!empty($_REQUEST['id'])){
      if(isset($auRec[0]->state_id) && !empty($auRec[0]->state_id)){
        $stateCondiRet=" AND R.state IN(".$auRec[0]->state_id.")";
      }

      if(isset($auRec[0]->city_id) && !empty($auRec[0]->city_id)){
        $cityCondiRet=" AND R.city IN(".$auRec[0]->city_id.")";
      }

      if(isset($auRec[0]->taluka_id) && !empty($auRec[0]->taluka_id)){
        $talukaCondiRet=" AND R.taluka_id IN(".$auRec[0]->taluka_id.")";
      }

      if(isset($auRec[0]->market_id) && !empty($auRec[0]->market_id)){
        $marketCondiRet = " AND R.market_id IN(".$auRec[0]->market_id.")";
      }

      if(isset($auRec[0]->division_id) && !empty($auRec[0]->division_id)){
        $divisionCondiRet = " AND R.division_id IN(".$auRec[0]->division_id.")";
      }else{
        $divisionCondiRet = " AND R.division_id =''";
      }

      if(isset($auRec[0]->relationship_id) && !empty($auRec[0]->relationship_id)){
        $classCondiRet = " AND R.relationship_id IN(".$auRec[0]->relationship_id.")";     
      }else{
        $classCondiRet = " AND R.relationship_id =''";
      }

      if(isset($hotListString) && !empty($hotListString)){       
        $hotColdCondiRet = " AND R.display_outlet IN(".$hotListString.")";
      }else{
        $hotColdCondiRet = " AND R.display_outlet =''";
      }

      if(isset($activeListString) && !empty($activeListString)){      
        $activeCondiRet = " AND R.status IN(".$activeListString.")";
      }else{
        $activeCondiRet = " AND R.status =''";
      }

      /*if(isset($auRec[0]->pin_code) && !empty($auRec[0]->pin_code)){
        $pincodeCondiRet = " AND R.zipcode IN(".$auRec[0]->pin_code.")";
        $expldArray= explode(',', $auRec[0]->pin_code);
        if(in_array(000000, $expldArray)){
          $PinBlackCondi = " AND R.zipcode=''";
        }     
      }*/

      if(isset($auRec[0]->pin_code) && !empty($auRec[0]->pin_code)){
        $expldArray= explode(',', $auRec[0]->pin_code);
            if(in_array(000000, $expldArray)){
              $key=array_search(000000, $expldArray);              
              unset($expldArray[$key]);
              if(sizeof($expldArray)>0){
                $Pincode = implode(',', $expldArray);  
              $pincodeCondiRet = " AND (R.zipcode IN(".$Pincode.") || R.zipcode='' || R.zipcode='0')";
              }else{
                $pincodeCondiRet = " AND (R.zipcode='' || R.zipcode='0')";
              }
            }else{ 
               $pincodeCondiRet = " AND (R.zipcode IN(".$auRec[0]->pin_code."))";
            }            
      }
      
    }    



    $custData = array();
    $retailerData=array();
    $distributorData=array();
    $customerData=array();

    if((isset($_SESSION['AllStates']) && !empty($_SESSION['AllStates'])) || (isset($_REQUEST['id']) && !empty($_REQUEST['id']))){
      $retailerData=$_objAdmin->_getSelectList("table_retailer AS R
      LEFT JOIN table_relationship as rr on rr.relationship_id=R.relationship_id 
      LEFT join table_division AS dV ON dV.division_id = R.division_id 
      LEFT join state as ST on ST.state_id=R.state 
      LEFT join city as CT on CT.city_id=R.city 
      LEFT join table_taluka as TL on TL.taluka_id=R.taluka_id",'R.retailer_id AS id, R.retailer_name AS name, R.retailer_address AS address, R.display_outlet AS outlet, R.lat_lng_capcure_accuracy AS accuracy, R.division_id, R.lat AS latitude, R.lng AS longitude, "Retailer" AS customer_type,dV.division_name,rr.relationship_code,R.state,ST.state_name,CT.city_name,TL.taluka_name,R.status,R.zipcode ','',"R.status!='D' AND (R.lat > 0 || R.lat < 0) AND (R.lng > 0 || R.lng < 0) $stateCondiRet $cityCondiRet $talukaCondiRet $marketCondiRet $hotColdCondiRet $activeCondiRet $divisionCondiRet $classCondiRet $pincodeCondiRet $PinBlackCondi ");

      $distributorData=$_objAdmin->_getSelectList("table_distributors AS R
      LEFT JOIN table_relationship as rr on rr.relationship_id=R.relationship_id 
      LEFT join table_division AS dV ON dV.division_id = R.division_id 
      LEFT join state as ST on ST.state_id=R.state 
      LEFT join city as CT on CT.city_id=R.city 
      LEFT join table_taluka as TL on TL.taluka_id=R.taluka_id",'R.distributor_id AS id, R.distributor_name AS name, R.distributor_address AS address, R.display_outlet AS outlet,R.lat_lng_capcure_accuracy AS accuracy, R.division_id, R.lat AS latitude, R.lng AS longitude, "Distributor" AS customer_type,dV.division_name,rr.relationship_code,R.state,ST.state_name,CT.city_name,TL.taluka_name,R.status,R.zipcode','',"R.status!='R' AND (R.lat > 0 || R.lat < 0) AND (R.lng > 0 || R.lng < 0) $stateCondiRet $cityCondiRet $talukaCondiRet $marketCondiRet $hotColdCondiRet $activeCondiRet $divisionCondiRet $classCondiRet $pincodeCondiRet $PinBlackCondi ");

      $customerData=$_objAdmin->_getSelectList("table_customer AS R 
      LEFT join table_division AS dV ON dV.division_id = R.division_id 
      LEFT join state as ST on ST.state_id=R.state 
      LEFT join city as CT on CT.city_id=R.city 
      LEFT join table_taluka as TL on TL.taluka_id=R.taluka_id",'R.customer_id AS id, R.customer_name AS name, R.customer_address AS address, R.display_outlet AS outlet, R.lat_lng_capcure_accuracy AS accuracy, R.division_id,R.lat AS latitude, R.lng AS longitude, R.customer_type,dV.division_name,"-" as relationship_code,R.state,ST.state_name,CT.city_name,TL.taluka_name,R.status,R.zipcode ','',"R.status!='D' AND (R.lat > 0 || R.lat < 0) AND (R.lng > 0 || R.lng < 0) $stateCondiRet $cityCondiRet $talukaCondiRet $marketCondiRet $hotColdCondiRet $activeCondiRet $divisionCondiRet $pincodeCondiRet $PinBlackCondi ");     

    }
    
   /* echo "<pre>";
   print_r($retailerData);
   
    echo "<pre>";
   print_r($distributorData);
   
    echo "<pre>";
   print_r($customerData);
   exit;*/


    if(sizeof($retailerData)>0 && sizeof($distributorData)>0 && sizeof($customerData)>0) {
    $custData = array_merge($retailerData, $distributorData, $customerData);
    } else if(sizeof($retailerData)> 0 && sizeof($distributorData)>0 && sizeof($customerData)==0) {
        $custData = array_merge($retailerData, $distributorData);
    } else if(sizeof($retailerData)> 0 && sizeof($distributorData)==0 && sizeof($customerData)>0) {
        $custData = array_merge($retailerData, $customerData);
    } else if(sizeof($retailerData)== 0 && sizeof($distributorData)>0 && sizeof($customerData)>0) {
        $custData = array_merge($distributorData, $customerData);
    } else if(sizeof($retailerData)> 0 && sizeof($distributorData)==0 && sizeof($customerData)==0) {
        $custData = $retailerData;
    } else if(sizeof($retailerData)== 0 && sizeof($distributorData)>0 && sizeof($customerData)==0) {
        $custData = $distributorData;
    } else if(sizeof($retailerData)== 0 && sizeof($distributorData)==0 && sizeof($customerData)>0) {
        $custData = $customerData;
    }

    /*echo count($custData);
    echo "<br/>";
    foreach($custData as $key=>$value){

     echo "CustomerType= ".$value->customer_type;
     echo "<br/>";

     echo "ZipCode     = ".$value->zipcode;
     echo "<br/>";
     
     echo "TalukaName  = ".$value->taluka_name;
     echo "<br/>";
     
     echo "Division    = ".$value->division_name;
     echo "<br/>";
     
     echo "Class       = ".$value->relationship_code;
     echo "<br/>";
     
     echo "Status      = ".$value->status;
     echo "<br/>";
     
     echo "HotCold     = ".$value->outlet;
     echo "<br/>";

     echo "HotCold     = ".$value->latitude;
     echo "<br/>";

     echo "HotCold     = ".$value->longitude;
     echo "<br/>";
     echo "******************************************";
     echo "<br/>";

    }  */    


     /*echo "<pre>";
   print_r($custData);*/
   //exit;
    


?>

<style type="text/css">
    html { height: 100% }
    body { height: 100%; margin: 0; padding: 0 }
    #map_canvas { height: 500px; }
    .tooltip {
        position:absolute;
        width: 150px;
        height: 150px;
        padding: 5px;
        margin:350px,120px,0px,100px;
        border: 1px solid gray;
        font-size: 9pt;
        font-family: Verdana;
        color: #000;
    }
</style>
<style>
.sortable   { padding:0; }
.sortable li  { padding:4px 8px; color:#000; cursor:move; list-style:none; width:350px; background:#ddd; margin:10px 0; border:1px solid #999; }
#message-box    { background:#fffea1; border:2px solid #fc0; padding:4px 8px; margin:0 0 14px 0; width:350px; }
</style>
<!-- <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false"></script> -->

<script src="https://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyAuAzZRDkMuaLSPt1I-MPNlAOlzbJjbLlU"></script>
<!-- <script type="text/javascript" src="javascripts/custom_map_tooltip.js"></script>
<script src="javascripts/jquery.min.1.10.2.js"></script> -->
<script src="javascripts/jquery.sortable.js"></script>
<script>
  $(function() { 
    //initialize();
    //$('#message-box').hide(); 
    $('.sortable').sortable(); 
  }); 
 
 </script>
  <!-- create center lat long-->
  <?php 
    if(sizeof($custData)>0 && $custData[0]->latitude !='' && $custData[0]->longitude !=''){
        $centerLat = $custData[0]->latitude;
        $centerLong = $custData[0]->longitude;
    }else{
        $centerLat = "21.0000";
        $centerLong = "78.0000";
    }
   ?>

<script type="text/javascript">
var infoWindow = new google.maps.InfoWindow();
    var customIcons = {
    Retailer: {
    icon: 'images/retailerM.png'
    },
    Distributor: {
    icon: 'images/distributorM.png'
    },
    C: { // C- Shakti Partner
    icon: 'images/shakti_partnerM.png'
    },
    P: { // S- pumps installar
    icon: 'images/paleblue_MarkerS.png'
    },
    S: { // S- solar pumps installar
    icon: 'images/orange_MarkerS.png'
    }
    };

    var markerGroups = {
    "all":[],
    "retailer": [],
    "distributor": [],
    "customer": [],
    };
 
 

function load() {
  var map = new google.maps.Map(document.getElementById("map_canvas"), {
    center: new google.maps.LatLng('<?php echo $centerLat;?>','<?php echo $centerLong;?>'),
    zoom: 8,
    mapTypeId: 'roadmap'
  });
  var infoWindow = new google.maps.InfoWindow();

   /*<?php if(isset($_SESSION['search'])){?>
            var name = "<?PHP echo $_SESSION['search'];?>";
            var address = "<?php echo addslashes(str_replace(array('\r\n', '\r', '\n'), '', $_SESSION['search'])); ?>";
            var type = "<?php echo addslashes(str_replace(array('\r\n', '\r', '\n'), '', '')); ?>";
            var interest = "<?php echo addslashes(str_replace(array('\r\n', '\r', '\n'), '', '')); ?>";
            var division = "";
            var point = new google.maps.LatLng(<?php echo $lat1;?>,<?php echo $lon1;?>);
            var html = "<b>" + name + "</b> <br/>" + "<?PHP echo $_SESSION['search'];?>";
            var title = "Name: <?php echo strip_tags(str_replace(array('\r\n', '\r', '\n'), '', $_SESSION['search'])); ?>, Lat: <?php echo substr($lat1,0,6); ?>,Long: <?php echo substr($lon1,0,6) ?>";
            var marker = createMarker(point, name, address, type, map,title,interest,division);

<?php }?>*/



  map.set('styles', [{
    zoomControl: false
  }, {
    featureType: "road.highway",
    elementType: "geometry.fill",
    stylers: [{
      color: "#ffd986"
    }]
  }, {
    featureType: "road.arterial",
    elementType: "geometry.fill",
    stylers: [{
      color: "#9e574f"
    }]
  }, {
    featureType: "road.local",
    elementType: "geometry.fill",
    stylers: [{
        color: "#d0cbc0"
      }, {
        weight: 1.1
      }

    ]
  }, {
    featureType: 'road',
    elementType: 'labels',
    stylers: [{
      saturation: -100
    }]
  }, {
    featureType: 'landscape',
    elementType: 'geometry',
    stylers: [{
      hue: '#ffff00'
    }, {
      gamma: 1.4
    }, {
      saturation: 82
    }, {
      lightness: 96
    }]
  }, {
    featureType: 'poi.school',
    elementType: 'geometry',
    stylers: [{
      hue: '#fff700'
    }, {
      lightness: -15
    }, {
      saturation: 99
    }]
  }]);
  var count=0;

  //         downloadUrl("markers.xml", function (data) {
    <?php foreach ($custData as $key => $value) {


      /*  $surveyLatLng = $_objAdmin->_getSelectList("table_survey AS S1 ",'S1.lat AS survey_lat, S1.lng AS survey_lng',''," S1.survey_id='".$value->survey_id."'");

        foreach ($surveyLatLng as $key => $surveyDts) {
           $latittude  = "";
            $lognitutde = "";

            if($surveyDts->survey_lat !="" && $surveyDts->survey_lng !="" && $surveyDts->survey_lat !="0" && $surveyDts->survey_lng !="0"){
                $latittude  = $surveyDts->survey_lat;
                $lognitutde = $surveyDts->survey_lng;
            }

        }
*/


        // } else if($value->lat !="" && $value->lng !="" && $value->lat !="0" && $value->lng !="0"){
        //     $latittude  = $value->lat;
        //     $lognitutde = $value->lng;
        // }


       /* if(sizeof($surveyLatLng)==0) {
            
            if($value->lat !="" && $value->lng !="" && $value->lat !="0" && $value->lng !="0"){
                $latittude  = $value->lat;
                $lognitutde = $value->lng;
            }


        }*/
        

        if($value->customer_type == 'C') {
            $customer_type = 'Mechanics';
        } else if($value->customer_type == 'P') {
            $customer_type = 'Pump Installer';
        } else if($value->customer_type == 'S') {
            $customer_type = 'Solar Pumps Installer';
        } else {
            $customer_type = $value->customer_type;
        }

        $latitude  = $value->latitude;
        $longitude = $value->longitude;


        if($latitude !="" &&  $longitude !="" && in_array($value->state,$stateArr)){?>

            var id = "<?php echo addslashes(str_replace(array('\r\n', '\r', '\n'), '', $value->id)); ?>";
            var name = "<?php echo addslashes(str_replace(array('\r\n', '\r', '\n'), '', $value->name)); ?>";
            var address = "<?php echo addslashes(str_replace(array('\r\n', '\r', '\n'), '', $value->address)); ?>";
            var type = "<?php echo addslashes(str_replace(array('\r\n', '\r', '\n'), '', $value->customer_type)); ?>";
            var interest = "<?php echo addslashes(str_replace(array('\r\n', '\r', '\n'), '', $value->outlet)); ?>";
            var division = "<?php echo $value->division_id; ?>";
            var point = new google.maps.LatLng(<?php echo $latitude;?>,<?php echo $longitude;?>);
            var html = "<b>" + name + "</b> <br/>" + address;
            var title = "Name: <?php echo strip_tags(str_replace(array('\r\n', '\r', '\n'), '', $value->name)); ?>, Lat: <?php echo substr($latitude,0,6); ?>,Long: <?php echo substr($longitude,0,6) ?> , Accuracy: <?php echo $value->lat_lng_capcure_accuracy ?>, type: <?php echo $customer_type ?>";
            var marker1 = createMarker(point, name, address, type, map,title,interest,division,id);
            count++;
    <?php } 
    }?>
   /* $("#count").text(count);*/
   //initialize();
}

function createMarker(point, name, address, type, map,title,interest,division,id) {

  //alert(id+type+name);
    // if(type=="Mechanics"){
    //     type = "Mechanic";
    // }
  var icon = customIcons[type] || {};
  var markr = new google.maps.Marker({
    map: map,
    position: point,
    icon: icon.icon,
    title:title,
    type: type
  });
  var html = "<b>AA" + name + "</b> <br/>" + address;
  bindInfoWindow(markr, map, infoWindow, html,id,type,name);
  return markr;
}

function bindInfoWindow(markr, map, infoWindow, html, id, type, name) {
  google.maps.event.addListener(markr, 'click', function() {
    addtoList(id, type, name);
    //infoWindow.setContent(html);
    //infoWindow.open(map, markr);    

  });
}

function addtoList(id, type, name){
    if(type == "Retailer"){
        type1 = "R"; 
        var color="red"; 
    }else if(type == "Distributor"){ 
        type1 = "D"; 
        var color="green";
    }else{
        var color="blue";
        type1 = "C";
        type = "Customer"; 
    }
    var nm = "retailer_id[]";
    var dftype = "  <strong style='color:"+color+";'>("+type1+")</strong>";
    var fullName= name+dftype;
    var CustType = type1+"_"+id;
    if($('.sortable input[class="' + CustType + '"]').length == 0){

        var liNumber = $('.sortable li').length;
        liNumber= (liNumber+1)+". ";
        $(".sortable").append("<li title='"+id+"'><input type='checkbox' name='"+nm+"' value='"+id+"' checked='checked' onclick='this.checked=true;' style='opacity:0;' /><input type='hidden' name='retailer_type[]' value='"+type1+"' class='"+CustType+"'/><label for='MultiRetID' style='margin-left:-20px;'>"+liNumber+fullName+"</label><a id='"+id+"' style='cursor:pointer;float:right;' onclick='removeRow(this.id);'><img src='images/cross.png'/></a></li>");
    }else{
      alert(type+" already added!");
    }
}

/*function addtoList(id, type, name){
    if(type == "Retailer"){
      //check if an Retailer already added
      if($('#11RetailerDiv input[value="' + id + '"]').length == 0 ){
        $("#11RetailerDiv").append("<div class='newcheckRet'><input type='checkbox' name='retailer_id[]' value='"+id+"' checked='checked' onclick='this.checked=true;' /><label for='MultiRetID'>"+name+"</label><a id='"+id+"' style='cursor:pointer;float:right;' onclick='removeRow(this.id);'><img src='images/cross.png'/></a></div>");
      }else{
        alert("Retailer already added!");
      }      

    }else if(type == "Distributor"){
      if($('#11DistributorDiv input[value="' + id + '"]').length == 0 ){

          $("#11DistributorDiv").append("<div class='newcheckDist'><input type='checkbox' name='distributor_id[]' value='"+id+"' checked='checked' onclick='this.checked=true;' /><label for='MultiDistID'>"+name+"</label><a id='"+id+"' style='cursor:pointer;float:right;' onclick='removeRow(this.id);'><img src='images/cross.png'/></a></div>");

       }else{
        alert("Distributor already added!");
      }
    }else{

      if($('#11CustomerDiv input[value="' + id + '"]').length == 0 ){ 

       $("#11CustomerDiv").append("<div class='newcheckCust'><input type='checkbox' name='customer_id[]' value='"+id+"' checked='checked' onclick='this.checked=true;' /><label for='MultiCustID'>"+name+"</label><a id='"+id+"' style='cursor:pointer;float:right;' onclick='removeRow(this.id);'><img src='images/cross.png'/></a></div>");
      }else{
        alert("Customer already added!");
      }
    }
}*/


 function removeRow(thisId){
  //alert(thisId);
       $("#"+thisId).closest('li').remove();
    
 }

function downloadUrl(url, callback) {
  var request = window.ActiveXObject ? new ActiveXObject('Microsoft.XMLHTTP') : new XMLHttpRequest();

  request.onreadystatechange = function() {
    if (request.readyState == 4) {
      request.onreadystatechange = doNothing;
      callback(request, request.status);
    }
  };

  request.open('GET', url, true);
  request.send(null);
}

function doNothing() {}
google.maps.event.addDomListener(window, 'load', load);


function xmlParse(str) {
  if (typeof ActiveXObject != 'undefined' && typeof GetObject != 'undefined') {
    var doc = new ActiveXObject('Microsoft.XMLDOM');
    doc.loadXML(str);
    return doc;
  }

  if (typeof DOMParser != 'undefined') {
    return (new DOMParser()).parseFromString(str, 'text/xml');
  }

  return createElement('div', null);
}




</script>

