<!-- start content-outer -->
<!-- <script src="https://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyAuAzZRDkMuaLSPt1I-MPNlAOlzbJjbLlU"></script> -->
     
     <script>
     var customIcons2 = {
    R: {
    iconM: 'images/retailerM.png'
    },
    D: {
    iconM: 'images/distributorM.png'
    },
    C: { // C- Shakti Partner
    iconM: 'images/shakti_partnerM.png'
    },
    P: { // S- pumps installar
    iconM: 'images/paleblue_MarkerS.png'
    },
    S: { // S- solar pumps installar
    iconM: 'images/orange_MarkerS.png'
    }
    };
// This example creates a simple polygon representing the Bermuda Triangle.

function initialize() {
  //alert('Body onload');

  var mapOptions2 = { zoom: 11, center: new google.maps.LatLng(<?php echo $lat.",".$lng;?>), mapTypeId: google.maps.MapTypeId.ROADMAP };

  var bermudaTriangle;

  var map2 = new google.maps.Map(document.getElementById('map'), mapOptions2);


  // Create Start Point
    var myLatLng2 = new google.maps.LatLng(<?php echo $lat;?>,<?php echo $lng;?>);
    var marker2 = new google.maps.Marker({ 
      position: myLatLng2, 
      map: map2, 
      icon: 'https://chart.googleapis.com/chart?chst=d_map_pin_letter&chld=S|FF776B|000000|FFFFFF',
      title: '<?php echo $retailername;?>' });

    var infowindowa = new google.maps.InfoWindow({ content: '<?php echo $retailername;?>'  });
      google.maps.event.addListener(marker2, 'click', function() {
      infowindowa.open(map2,marker2);
      });

  // Create all mid points
  <?php 
  
  if($retailerCount>1){
  
  for($h = 1; $h < $retailerCount-1; $h++)  {?>

     var retname = "<?php echo addslashes(str_replace(array('\r\n', '\r', '\n'), '', $auRecRet[$h]->retailer_name)); ?>";
     var rtype = "<?php echo addslashes(str_replace(array('\r\n', '\r', '\n'), '', $auRecRet[$h]->status)); ?>";
    var myLatLng3 = new google.maps.LatLng(<?php echo $auRecRet[$h]->lat;?>,<?php echo $auRecRet[$h]->lng;?>);
    var title1 = "Name: <?php echo strip_tags(str_replace(array('\r\n', '\r', '\n'), '', $auRecRet[$h]->retailer_name)); ?>, Lat: <?php echo substr($auRecRet[$h]->lat,0,6); ?>,Long: <?php echo substr($auRecRet[$h]->lng,0,6) ?> , type: <?php echo $auRecRet[$h]->status; ?>";
    var iconN= 'https://chart.googleapis.com/chart?chst=d_map_pin_letter&chld='+<?php echo $h+1;?>+'|FF776B|000000';

    var infowindow2 = new google.maps.InfoWindow();
      var marker11 = createMarker2(myLatLng3, retname, map2, rtype, title1, infowindow2,iconN); 
    
  <?php } ?>

  function createMarker2(point2, retname, map2, rtype,title1, infowindow2, iconN) {

  var icon2 = customIcons2[rtype] || {};
  var markr2 = new google.maps.Marker({
    map: map2,
    position: point2,
    icon: iconN,
    title:title1,
    type: rtype
  });  
  bindInfoWindow2(markr2, map2, infowindow2,rtype,retname);
  return markr2;
}

function bindInfoWindow2(markr2, map2, infowindow2, rtype, retname) {
  google.maps.event.addListener(markr2, 'click', function() {    
    infowindow2.setContent(retname);
    infowindow2.open(map2, markr2);    

  });
}

  
  //Created last or end points
            
    <?php $lastIndexof = $retailerCount-1;?>
    
     var myLatLng = new google.maps.LatLng(<?php echo $auRecRet[$lastIndexof]->lat;?>,<?php echo $auRecRet[$lastIndexof]->lng;?>);

     var marker5 = new google.maps.Marker({ position: myLatLng, map: map2, icon: 'https://chart.googleapis.com/chart?chst=d_map_pin_letter&chld=E|FF776B|000000',title: '<?php echo htmlspecialchars($auRecRet[$lastIndexof]->retailer_name, ENT_QUOTES);?>' });

     var infowindowa2 = new google.maps.InfoWindow({ content: '<?php echo $auRecRet[$lastIndexof]->retailer_name;?>'  });
      google.maps.event.addListener(marker5, 'click', function() {
      infowindowa2.open(map2,marker5);
      });

    
    // Create polygon
    <?php }
    $k=0;

    for($j=0;$j<$retailerCount-1;$j++) { $k=$j+1; ?>
      var flightPlanCoordinates2 = [
      new google.maps.LatLng(<?php echo $auRecRet[$j]->lat.",".$auRecRet[$j]->lng; ?> ),
      new google.maps.LatLng(<?php echo $auRecRet[$k]->lat.",".$auRecRet[$k]->lng; ?> )
      ];

      var lineSymbol2 = {
          path: google.maps.SymbolPath.FORWARD_CLOSED_ARROW
      };
    var arrow2 = {
      icon: lineSymbol2,
      offset: '50%',
      strokeColor: "#980000 ",
      strokeOpacity :1,
    };
    var flightPath2 = new google.maps.Polyline({
            path: flightPlanCoordinates2,
            strokeColor: "#980000 ",
            strokeOpacity: 1.0,
            geodesic: true,
            strokeWeight: 2,
            icons : [arrow2],
            });
    flightPath2.setMap(map2);
    <?php }  ?>
  }

  google.maps.event.addDomListener(window, 'load', initialize);

    </script> 
