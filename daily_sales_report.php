<?php
include("includes/config.inc.php");
include("includes/function.php");
$page_name="Daily Sales Report";
$_objAdmin = new Admin();
$_DB = new Db_Action();

$retailer_name = '-'; 
$percentage = array();


if( $_SESSION['userLoginType']==3){
	$disLogCond="distributor_id='".$_SESSION['userLoginType']."' and ";
	$disLogCond1="tod.distributor_id='".$_SESSION['userLoginType']."' and ";
	$list=$_objAdmin->getDistributorWiseSalesmanDetails($_SESSION['distributorId']);
	if(sizeof($list)>0){
	foreach($list as $val){
			$listArr[]=$val;
		}
	$sal=implode(',',$listArr);
	$salCond=" salesman_id IN(".$sal.") and";
}
}

if(isset($_POST['submit']) && $_POST['showReport'] == 'yes')
{
	$date=date('Y-m-d', strtotime($_POST['from']));
	$day=date('D', strtotime($date));
	$where = "$disLogCond date_of_order='".$date."' AND salesman_id='".$_POST['sname']."' AND (retailer_id IS NOT NULL OR retailer_id<>'') group by retailer_id"; 
	$auRetailer = $_objAdmin->_getSelectList('salereldailywiseitemreport','*',''," $where "); 
	$salesman_name=$auRetailer[0]->salesman_name;
	
	// for route name.
	$cond="salesman_id='".$_POST['sname']."' and '$date'>from_date and '$date'< to_date ";
    $tblroute = $_objAdmin->_getSelectList('table_route_schedule','*','',"$cond"); 
	//print_r($auRetailer);
	//echo  count($auRetailer);	
} else {
	$_POST['from'] =date('j M Y');

}
if(isset($_REQUEST['reset']) && $_REQUEST['reset'] == 'yes')
{
header("Location: daily_sales_report.php");
}

?>

<?php include("header.inc.php") ?>
<script>
    $(function() {
        $( "#from" ).datepicker({
			dateFormat: "d M yy",
            defaultDate: "w",
            changeMonth: true,
            numberOfMonths: 1,
            onSelect: function( selectedDate ) {
                $( "#to" ).datepicker( "option", "minDate", selectedDate );
            }
        });
        $( "#to" ).datepicker({
			dateFormat: "d M yy",
            defaultDate: "-w",
            changeMonth: true,
            numberOfMonths: 1,
            onSelect: function( selectedDate ) {
                $( "#from" ).datepicker( "option", "maxDate", selectedDate );
            }
        });
    });
</script>

<script src="javascripts/dateNextPrev.js" type="text/javascript"></script>
<script src="javascripts/jquery-1.8.2.js" type="text/javascript"></script>
<script src="javascripts/jquery-ui.js"></script>
<script type="text/javascript" src="javascripts/validate.js"></script>
<script type='text/javascript'>//<![CDATA[ 
 function PrintElem(elem)
    {
        Popup($(elem).html());
    }

    function Popup(data) 
    {
		
        var mywindow = window.open('', 'Report');
		
        mywindow.document.write('<html><head><title>Daily Sales Report</title>');
		mywindow.document.write('<table><tr><td><b>Salesman Name:</b> <?php echo $salesman_name; ?></td><td><b>From Date:</b> <?php echo $date; ?></td></tr></table>');
        /*optional stylesheet*/ //mywindow.document.write('<link rel="stylesheet" href="main.css" type="text/css" />');
        mywindow.document.write('</head><body >');
        mywindow.document.write(data);
        mywindow.document.write('</body></html>');

        mywindow.print();
        mywindow.close();
        return true;
    }

$(document).ready(function() {
    
   <?php 
   if($_POST['submit']=='Export to Excel'){?>
   tableToExcel('report_export', 'DSR Report', 'DSR Report.xls');
   <?php } ?> 

	
});



var tableToExcel = (function () {
 
        var uri = 'data:application/vnd.ms-excel;base64,'
        , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table><tr><td></td></tr></table><table>{table}</table></body></html>'
        , base64 = function (s) { return window.btoa(unescape(encodeURIComponent(s))) }
        , format = function (s, c) { return s.replace(/{(\w+)}/g, function (m, p) { return c[p]; }) }
        return function (table, name, filename) {
            if (!table.nodeType) table = document.getElementById(table)
            var ctx = { worksheet: name || 'Worksheet', table: table.innerHTML }

            document.getElementById("dlink").href = uri + base64(format(template, ctx));
            document.getElementById("dlink").download = filename;
            document.getElementById("dlink").click();

        }
    })()

//]]>  
</script>
<div id="content-outer">
<!-- start content -->
<div id="content">
<div id="page-heading"><h1><span style="color: #d74343; font-family: Tahoma; font-weight: bold;">Daily Sales Report</span></h1></div>
<table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">

<tr>
	<!--<td id="tbl-border-left"></td>-->
	<td>
	<!--  start content-table-inner -->
	
	<div id="content-table-inner">

	<form name="frmPre" id="frmPre" method="post" action="" enctype="multipart/form-data" >
	<table border="0"  cellpadding="0" cellspacing="0">
	<tr>
		<td><h3>Salesman: </h3>
		<h6><select name="sname" id="sname" class="styledselect_form_3 required">
		<option value="" >Select Salesman</option>
		<?php $aSalesman=$_objAdmin->_getSelectList('table_salesman','salesman_id,salesman_name',''," $salCond salesman_name!='' ORDER BY salesman_name"); 
		if(is_array($aSalesman)){
		for($i=0;$i<count($aSalesman);$i++){
		?>
		
		<option value="<?php echo $aSalesman[$i]->salesman_id;?>" <?php if ($aSalesman[$i]->salesman_id==$_POST['sname']){ ?> selected <?php } ?>><?php echo $aSalesman[$i]->salesman_name;?></option>
		<?php } }?>
		</select></h6></td>
		
		<td><h3> &nbsp;&nbsp;&nbsp; Date:</h3><h6>&nbsp;&nbsp;<img src="css/images/prev.png" height="18" width="18" onclick="dateFromPrev();">  <input type="text" id="from" name="from" class="date" value="<?php  echo $_POST['from'];?>"  readonly /> <img src="css/images/next.png" height="18" width="18" onclick="dateFromNext();"></h6></td>
		
		<td><h3></h3>
		<input type="button" value="Reset!" class="form-reset" onclick="location.href='daily_sales_report.php?reset=yes';" />	
		</td>
		
		<td><h3></h3>
		<input name="submit" class="result-submit" type="submit" id="submit" value="View Details"  onclick="return validation();"/>
		</td>
		
		<td><h3></h3><input name="showReport" type="hidden" value="yes" />
		<input type="button" value="Print" class="result-submit" onclick="PrintElem('#Report')" /></td>
	
		<td><h3></h3><a id="dlink"  style="display:none;"></a>
	<input type="submit" name="submit" value="Export to Excel" class="result-submit"  />



	</td>
	</tr>
	</table>
	</form>
		<div style="width:1000px;overflow:auto; height:auto; margin-top:20px;" >
		<div id="Report">
<?php 
foreach($tblroute as $value){
   
       $cond="route_schedule_id='".$value->route_schedule_id."' and $day!='' ";
        $tblrouteassign = $_objAdmin->_getSelectList('table_route_schedule_by_day',"".$day."",'',"$cond");
		
		$retailerID = $_DB->returnObjectArrayToIndexArray($auRetailer, 'retailer_id');
		$retailerIDwithcomma = implode(',', $retailerID);
        if($retailerIDwithcomma==""){
		
		$retailerIDwithcomma=0;
		}
		else
		{$retailerIDwithcomma;}
		
		
		$routeID = $_DB->returnObjectArrayToIndexArray($tblrouteassign, $day);
	    $routeIDwithcomma = implode(',', $routeID);
		if($routeIDwithcomma==""){$routeIDwithcomma=0;} else{ $routeIDwithcomma; }
		
		$rtID = $_objAdmin->_getSelectList('table_route_retailer',"DISTINCT(route_id)",''," route_id IN (".$routeIDwithcomma.") AND retailer_id IN (".$retailerIDwithcomma.")");
		
		$rtfID = $_DB->returnObjectArrayToIndexArray($rtID, 'route_id');
		$routeIDfinal = implode(',',$rtfID);
		if($routeIDfinal==""){$routeIDfinal=0;}else {$routeIDfinal;}
		
		$tblroutefinal = $_objAdmin->_getSelectList('table_route','*','',"route_id IN(".$routeIDfinal.")");
		}
 ?>		
<table width="100%" border="1">
  <tr>
    <td style="padding:10px;" width="20%">&nbsp;</td>
    <td style="padding:10px;" width="20%">Route Name:</td>
    <td style="padding:10px;" colspan="2">
	  <?php $j=0; 
		for($i=0;$i<count($tblroutefinal);$i++)
		{ 
		  
		  if($tblroutefinal[$i]->route_name!=""){ 
		  if($j>0){ echo ','.'&nbsp;' ;} 
		  echo $tblroutefinal[$i]->route_name; 
		 
		  $j++; 
		  }
		 
	    }?>
		</td>
    
  </tr>
</table>
		
<table border="0" width="100%" cellpadding="0" cellspacing="0" id="report_export" name="report_export">
	
	<tr>
		
		<td>
		<table  border="1" width="100%" cellpadding="0" cellspacing="0" id="lists">
		<tr id="candidateNameTD">
				<td style="padding:10px;">S.no</td>
			 	 <td style="padding:10px;">Retailer Name</td>
				 <td>
				 <table border="1">
				 	<tr id="candidateNameTD">
					
				 <?php 
				 	if(isset($_POST['sname'])){
				 	 $ItemArray2 = $_objAdmin->_getSelectList('salereldailywiseitemreport  
 as tod left join table_item as ti on tod.item_id=ti.item_id','DISTINCT(ti.item_code),ti.item_name,ti.item_id,tod.price',''," $disLogCond1 tod.date_of_order ='".$date."' AND (tod.retailer_id!='' OR tod.retailer_id IS NOT NULL) AND tod.salesman_id =".$_POST['sname']." ORDER BY ti.item_id ASC");
 				//echo "<pre>";
 				//echo count($ItemArray2);
		           		if(is_array($ItemArray2))
						{
				           foreach($ItemArray2 as $itm){ ?>
				 			<td style="padding:5px;" align="center"><div style="width:60px;"><?php echo $itm->item_name;?></div></td>
				 			<?php }
						}
					
					//print_r($ItemArray2);
					$final = $_DB->returnObjectArrayToIndexArray($ItemArray2, 'item_id');
					//print_r($final);
				    $itemID = implode(',', $final);
					}
					?>
				
			      </tr>
				 </table>
				 </td>
 
					<td style="padding:10px;">Value</td>
			
			</tr>
			
			
		<?php if(count($auRetailer)>0){	?>
			

			<?php $counter = 0; 
			      $MultiDimArr = array();
				  
				  foreach($auRetailer as $key=>$values){  ?>	

			<tr class="maintr"  style="border-bottom:2px solid #6E6E6E;">
			<td style="padding:10px;" width="20%"><?php echo $key+1; ?></td>
			<td style="padding:10px;" width="20%"><?php echo $values->retailer_name;?></td>
			<td><table border="1">
				 	<tr id="candidateNameTD">
					
				 <?php 
				 
				 	 //$ItemArray = $_objAdmin->_getSelectList2('table_order_detail as tod left join table_item as ti on tod.item_id=ti.item_id','tod.order_detail_id, tod.order_id, tod.item_id, tod.quantity, tod.type, tod.discount_id, tod.order_detail_status,ti.item_code','',"tod.order_id =".$values->order_id.""); 
					 //$ItemArray = $_objAdmin->_getSelectList2('salereldailywiseitemreportas tod left join table_item as ti on tod.item_id=ti.item_id','tod.item_id, tod.totalSaleUnit,ti.item_code','',"tod.retailer_id =".$values->retailer_id." AND tod.date_of_order ='".$date."' AND (tod.retailer_id!='' OR tod.retailer_id IS NOT NULL) AND tod.salesman_id =".$_POST['sname']); 
          
		  $ItemArray = $_objAdmin->_getSelectList2('table_item AS ti LEFT OUTER JOIN salereldailywiseitemreport as tod on tod.item_id=ti.item_id','tod.item_id, tod.totalSaleUnit,ti.item_code,tod.price','',"$disLogCond1 tod.retailer_id =".$values->retailer_id." AND tod.date_of_order ='".$date."' AND (tod.retailer_id!='' OR tod.retailer_id IS NOT NULL) AND ti.item_id IN (".$itemID.") AND  tod.salesman_id =".$_POST['sname']);   
		  //echo "<pre>";
		 // print_r( $ItemArray);           
						
		           		if(is_array($ItemArray))
						{
						  //echo count($ItemArray);
						  $unitsale=0;
						  $ValArr = array();
						  $ValArr2=array();
				           foreach($ItemArray as $key=>$itm){
						  $ValArr[$itm->item_id] = $itm->totalSaleUnit;
						  $ValArr2[$itm->item_id] = $itm->price;
						  
						 }
						//print_r($ValArr);
						$count = array();
						$rowtotal_price=0;
						for($i=0;$i<count($final);$i++){
						$count[$i] = 1;
						//echo "<pre>";
						//print_r($final);
						
						?>
							<td style="padding:5px; width:60px;" align="center">
							
							<?php 
									if($ValArr[$final[$i]]!='') {
										 //echo $ValArr2[$final[$i]].'('.$final[$i].')';
										echo '<b>'.$ValArr[$final[$i]].'</b>';
										
										$MultiDimArr[$final[$i]]['qty'] = $MultiDimArr[$final[$i]]['qty'] + $ValArr[$final[$i]];
										$MultiDimArr[$final[$i]]['count'] = $MultiDimArr[$final[$i]]['count'] + $count[$i];
										$counter++;
									} else {
									
										echo "-";
									}
									
										$rowtotal_price=$rowtotal_price+($ValArr[$final[$i]]*$ValArr2[$final[$i]]);	
									
									
								
								//}?>
							</td>
						<? }
						
						
						
						
					//endforeach;?>
				
			      </tr>
				 </table>
				</td>			
			 <td style="padding:10px;" width="20%" align="center"><b><?php echo $rowtotal_price; ?></b></td>	
			</tr>
			<?php  $finalval=$finalval+$rowtotal_price;} } ?>
			
			<tr>
	      	 	<td>&nbsp;</td>
				<td><table border="1">
							<tr><td style="padding:5px; width:60px;">RD TOTAL</td></tr>
							<tr><td style="padding:5px; width:60px;">LINE TOTAL</td></tr>
						</table>
						</td>
				<td>
					<table border="1">
					<tr>
					<?php 
					    $tls=0;
						for($i=0;$i<count($final);$i++){?>
						<td style="padding:5px; width:60px;">
						<table border="1">
							<tr><td style="padding:5px; width:60px;" align="center"><?php echo '<b>'.$MultiDimArr[$final[$i]]['qty'].'</b>';?></td></tr>
							<tr><td style="padding:5px; width:60px;" align="center"><?php echo '<b>'.$MultiDimArr[$final[$i]]['count'].'</b>';?></td></tr>
						</table>
						</td>
					<? 
                    $tls=$tls+$MultiDimArr[$final[$i]]['count'];
					}?>
					</tr>
					</table>
				</td>
				<td align="center"><b><?php echo $finalval; ?></b></td>
		   </tr>
		   
		   
		   <tr>
		   <td>&nbsp;</td><td>PC-  <b><?php echo count($auRetailer); ?></b></td><td>&nbsp;</td><td>&nbsp;</td>
	      </tr>
		  <tr>
		   <td>&nbsp;</td><td>TLS-  <b><?php echo $tls; ?></b></td><td>&nbsp;</td><td>&nbsp;</td>
	      </tr>
		  
		  <tr>
		   <td>&nbsp;</td><td>TOT VAL-  <b><?php echo $finalval; ?></b></td><td>&nbsp;</td><td></td>
	      </tr>
			
			
			<?php 	//echo $counter;?>	

		
		<? } else {?>  
		<table  border="1" width="100%" cellpadding="0" cellspacing="0" id="lists">
			<tr  style="border-bottom:2px solid #6E6E6E;" align="center" >
			 <td style="padding:10px;" width="100%">Report Not Available</td>
			</tr>
			</table>
		
		<? }?>
		</table>
		</td>
	
	</tr>
	
	</table>
						
		</div>
		</div>
<div class="clear"></div>
<!-- Graph code starts here -->
</div>

<!--  end content-table-inner  -->
</td>
<td id="tbl-border-right"></td>
</tr>
</table>
<div class="clear">&nbsp;</div>
</div>
<!--  end content -->
<div class="clear">&nbsp;</div>
</div>
	

	

<?php include("footer.php") ?>
</body>
</html>
