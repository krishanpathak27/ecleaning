<?php include("includes/config.inc.php");
      include("includes/function.php");
	  $_objAdmin = new Admin();
	  
	  
	// Get the list of today sms scheduled scheme between Monday - Saturday
			
	$scheduleData = $_objAdmin->_getSelectList('sms_schedule AS S 
		LEFT JOIN table_discount AS D ON S.discount_id = D.discount_id
		INNER JOIN table_discount_detail AS DD ON S.discount_id = DD.discount_id',
		'D.party_type, schedule_id, discount_desc, S.discount_id, state_id, city_id, retailer_id, from_date, to_date, repeats, 
		sent_nature, weekday, S.status ',
		'',' WHERE to_date>=CURDATE() AND sent_nature =1  AND S.status = "A" ORDER BY D.party_type DESC');
		
		


		if(!empty($scheduleData)){
		
			
			foreach($scheduleData as $key=>$svalue):
			
				$excludeStateArray = array();
				$excludeCityArray = array();
				$excludeRetailerArray = array();
				$PartyTypeData = '';
				$finalData = '';
				$state = '';
				$city = '';
				$retailer = '';

			
				//echo "<pre>";
				//print_r($scheduleData[$key]);
				$excludeStateArray 	  = explode(',',$svalue->state_id);
				$excludeCityArray 	  =	explode(',',$svalue->city_id);
				$excludeRetailerArray = explode(',',$svalue->retailer_id);
				
				//print_r($excludeStateArray);
				//print_r($excludeCityArray);
				//print_r($excludeRetailerArray);

			
			if(!empty($excludeStateArray[0]))	$state 	=  "AND state NOT IN (".$svalue->state_id.")"; else $state = "";
			if(!empty($excludeCityArray[0])) 	$city 	=  "AND city NOT IN (".$svalue->city_id.")";	 else $city = "";
			if(!empty($excludeRetailerArray[0]))$retailer= "AND retailer_id NOT IN (".$svalue->retailer_id.")"; else $retailer="";
				
				
				$a = $svalue->party_type;
			 	switch ($a) 
				 {
				 		// Party type All
					case 1: 
					//echo "</pre>";
						$PartyTypeData = '';	
						$finalData = array('all');
				
					break;
				case 2: 
						// Party type state
						$PartyTypeData = $_objAdmin->_getSelectList('table_discount_party AS DP ','state_id','',
							' WHERE discount_id='.$svalue->discount_id);	

						$stateIDarray = array_map(function ($object) { return $object->state_id; }, $PartyTypeData);
						
						$finalData = array_diff($stateIDarray,$excludeStateArray);

						if(!empty($finalData))
						{ 
							$excludeStateArrayList	=	implode(',',$finalData);
							$state = "AND state IN (".$excludeStateArrayList.")";
						} else { $state =""; }
						
					break;
				case 3: 
						// Party type city
						$PartyTypeData = $_objAdmin->_getSelectList('table_discount_party AS DP ','city_id','',
							' WHERE discount_id='.$svalue->discount_id);	
						$cityIDarray = array_map(function ($object) { return $object->city_id; }, $PartyTypeData);
						
						$finalData = array_diff($cityIDarray,$excludeCityArray);
						
						
						if(!empty($finalData))
						{ 
							$excludeCityArrayList  =	implode(',',$finalData);
							$city = "AND city IN (".$excludeCityArrayList.")";
						} else {
							$city ="";
						}

					break;
				case 4: 
						// Party type Retailer
						$PartyTypeData = $_objAdmin->_getSelectList('table_discount_party AS DP ','retailer_id','',
							' WHERE discount_id='.$svalue->discount_id);	
						$retIDarray = array_map(function ($object) { return $object->retailer_id; }, $PartyTypeData);
						
						$finalData = array_diff($retIDarray,$excludeRetailerArray);

						if(!empty($finalData)) {
							$excludeRetailerArrayList  =	implode(',',$finalData);
							$retailer = "AND retailer_id IN (".$excludeRetailerArrayList.")";
						}
						
						
					break;
				default:
					break;
				}
				
				/* echo $state;
				echo $city;
				echo $retailer;
				print_r($finalData); */
		//exit;
				// Fetch all the retailers list with phone number or name.
				if(!empty($finalData)){
				$data = $_objAdmin->_getSelectList('table_retailer',
					'contact_number','',
					" WHERE status = 'A' AND contact_number REGEXP '^[0-9]+$' AND contact_number REGEXP '^.{10}$' ".$state." ".$city." ".$retailer." ORDER BY retailer_name");
				//echo "</pre>";
				//print_r($data);
				
				//exit;
			    if(!empty($data))
				{
				
					foreach($data as $key=>$Rvalue):
					
						//Retailer SMS
						$smslist = $svalue->discount_desc;
						$sms = explode("||||",wordwrap($smslist,153,"||||"));
						$num = count($sms);
						$i = 1; 
						
						foreach ($sms as $value) 
						{
							if ($num > 1) 
							{
								$mobile=SMS_MOBILE;
								$pass=SMS_PASS;
								$senderid=SMS_SENDERID; 
								$to=$Rvalue->contact_number;
								$msg=urlencode("($i/$num) $value");
								//$sendsms = file("http://trans.smsndata4india.com/sendsms.aspx?mobile=$mobile&pass=$pass&senderid=$senderid&to=$to&msg=$msg");
									$i++;
							} 
							else 
							{
								$mobile=SMS_MOBILE;
								$pass=SMS_PASS;
								$senderid=SMS_SENDERID; 
								$to=$Rvalue->contact_number;
								$msg=urlencode($value);
								//$sendsms = file("http://trans.smsndata4india.com/sendsms.aspx?mobile=$mobile&pass=$pass&senderid=$senderid&to=$to&msg=$msg");
							}
						} 
															
					
					endforeach;
					//
				}
				}
				
		endforeach;
			
	}
	echo  "<script type='text/javascript'>";
	echo "javascript:window.close()";
	echo "</script>";
?>