<?php
include("includes/config.inc.php");
include("includes/function.php");
include("includes/globalarraylist.php");

	$d1=0; $d2=0; $d3=0; $d4=0; $d5=0; $d6=0; $d7=0; $d8=0; $d9=0; $d10=0;
	$d11=0; $d12=0; $d13=0; $d14=0; $d15=0; $d16=0; $d17=0; $d18=0; $d19=0; $d20=0;
	$d21=0; $d22=0; $d23=0; $d24=0; $d25=0; $d26=0; $d27=0; $d28=0; $d29=0; $d30=0; $d31=0;
	//echo $_REQUEST['y']." ".$_REQUEST['disID']." ".$_REQUEST['m'];
	if($_REQUEST['y']!='' && $_REQUEST['disID']!='' && $_REQUEST['m']!='')
	{
		$auRet=$_objAdmin->_getSelectList('disretdailywiseitemreport ',' sum(totalSaleUnit) as quantity, day ',''," distributor_id = ".$_REQUEST['disID']." and monthname( date_of_order) ='".$_REQUEST['m']."' and year='".$_REQUEST['y']."' group by `day`");
		for($i=0; $i < count($auRet); $i++)
		{
			if($auRet[$i]->day=='1') { $d1 = $d1 + $auRet[$i]->quantity; }
			if($auRet[$i]->day=='2') { $d2 = $d2 + $auRet[$i]->quantity; }
			if($auRet[$i]->day=='3') { $d3 = $d3 + $auRet[$i]->quantity; }
			if($auRet[$i]->day=='4') { $d4 = $d4 + $auRet[$i]->quantity; }
			if($auRet[$i]->day=='5') { $d5 = $d5 + $auRet[$i]->quantity; }
			if($auRet[$i]->day=='6') { $d6 = $d6 + $auRet[$i]->quantity; }
			if($auRet[$i]->day=='7') { $d7 = $d7 + $auRet[$i]->quantity; }
			if($auRet[$i]->day=='8') { $d8 = $d8 + $auRet[$i]->quantity; }
			if($auRet[$i]->day=='9') { $d9 = $d9 + $auRet[$i]->quantity; }
			if($auRet[$i]->day=='10') { $d10 = $d10 + $auRet[$i]->quantity; }
			if($auRet[$i]->day=='11') { $d11 = $d11 + $auRet[$i]->quantity; }
			if($auRet[$i]->day=='12') { $d12 = $d12 + $auRet[$i]->quantity; }
			if($auRet[$i]->day=='13') { $d13 = $d13 + $auRet[$i]->quantity; }
			if($auRet[$i]->day=='14') { $d14 = $d14 + $auRet[$i]->quantity; }
			if($auRet[$i]->day=='15') { $d15 = $d15 + $auRet[$i]->quantity; }
			if($auRet[$i]->day=='16') { $d16 = $d16 + $auRet[$i]->quantity; }
			if($auRet[$i]->day=='17') { $d17 = $d17 + $auRet[$i]->quantity; }
			if($auRet[$i]->day=='18') { $d18 = $d18 + $auRet[$i]->quantity; }
			if($auRet[$i]->day=='19') { $d19 = $d19 + $auRet[$i]->quantity; }
			if($auRet[$i]->day=='20') { $d20 = $d20 + $auRet[$i]->quantity; }
			if($auRet[$i]->day=='21') { $d21 = $d21 + $auRet[$i]->quantity; }
			if($auRet[$i]->day=='22') { $d22 = $d22 + $auRet[$i]->quantity; }
			if($auRet[$i]->day=='23') { $d23 = $d23 + $auRet[$i]->quantity; }
			if($auRet[$i]->day=='24') { $d24 = $d24 + $auRet[$i]->quantity; }
			if($auRet[$i]->day=='25') { $d25 = $d25 + $auRet[$i]->quantity; }
			if($auRet[$i]->day=='26') { $d26 = $d26 + $auRet[$i]->quantity; }
			if($auRet[$i]->day=='27') { $d27 = $d27 + $auRet[$i]->quantity; }
			if($auRet[$i]->day=='28') { $d28 = $d28 + $auRet[$i]->quantity; }
			if($auRet[$i]->day=='29') { $d29 = $d29 + $auRet[$i]->quantity; }
			if($auRet[$i]->day=='30') { $d30 = $d30 + $auRet[$i]->quantity; }
			if($auRet[$i]->day=='31') { $d31 = $d31 + $auRet[$i]->quantity; }
		}
	}		
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta charset="utf-8">
<title>Quytech PepUpSales - Distributor Monthly Sale Report Graph</title>
<?php include_once('graph/header-files.php');?>

    <script type="text/javascript">
      google.load('visualization', '1', {packages: ['corechart']});
	 
    </script>
    <script type="text/javascript">
      function drawVisualization() {
        // Some raw data (not necessarily accurate)
        var data = google.visualization.arrayToDataTable([
           ['Month', 'Quantity' ],
           ['1',  <?php echo $d1; ?> ],
          ['2',  <?php echo $d2; ?> ],
          ['3',  <?php echo $d3; ?> ],
          ['4',  <?php echo $d4; ?> ],
		  ['5',  <?php echo $d5; ?> ],
		  ['6',  <?php echo $d6; ?> ],
		  ['7',  <?php echo $d7; ?> ],
		  ['8',  <?php echo $d8; ?> ],
		  ['9',  <?php echo $d9; ?> ],
		  ['10',  <?php echo $d10; ?> ],
		  ['11',  <?php echo $d11; ?> ],
		  ['12',  <?php echo $d12; ?> ],
		  ['13',  <?php echo $d13; ?> ],
		  ['14',  <?php echo $d14; ?> ],
		  ['15',  <?php echo $d15; ?> ],
		  ['16',  <?php echo $d16; ?> ],
		  ['17',  <?php echo $d17; ?> ],
		  ['18',  <?php echo $d18; ?> ],
		  ['19',  <?php echo $d19; ?> ],
          ['20',  <?php echo $d20; ?> ],
		  ['21',  <?php echo $d21; ?> ],
		  ['22',  <?php echo $d22; ?> ],
		  ['23',  <?php echo $d23; ?> ],
		  ['24',  <?php echo $d24; ?> ],
		  ['25',  <?php echo $d25; ?> ],
		  ['26',  <?php echo $d26; ?> ],
		  ['27',  <?php echo $d27; ?> ],
		  ['28',  <?php echo $d28; ?> ],
		  ['29',  <?php echo $d29; ?> ],
          ['30',  <?php echo $d30; ?> ],
          ['31',  <?php echo $d31; ?> ]
        ]);

        var options = {
          title : 'Daily total number of item sales by a distributer',
          vAxis: {title: "Total item sales" , gridlines: { count: 8  }, minValue:0},
          hAxis: {title: "<?php echo $_REQUEST['m']; ?>"},
          seriesType: "bars",
          series: {5: {type: "line"}}
        };

        var chart = new google.visualization.ComboChart(document.getElementById('chart_div'));
        chart.draw(data, options);
      }
      google.setOnLoadCallback(drawVisualization);
    </script>
</head>
<body> 
<!-- Start: page-top-outer -->

<?php include_once('graph/header.php');?>
<!-- End: page-top-outer -->
	
<div id="content-outer">
<div id="content">
<div id="page-heading"><h1>Montly Sale of Items Report Graph</h1></div>
<div id="container">
<form name="frmPre" id="frmPre" method="post" action="<?php echo $_SERVER['PHP_SELF'];?>" enctype="multipart/form-data" >
<table width="100%" border="0">
  <tr>
  	<td width="12%" valign="bottom"><h3>Distributor :</h3></td>
	<td width="14%" align="left">
	<select name="disID" id="disID" class="styledselect_form_5" >
					<option value="">Please Select</option>
						<?php $aSal=$_objAdmin->_getSelectList('table_distributors AS d','distributor_id, distributor_name','',
										"".$Discondition." ORDER BY distributor_name"); 
								if(is_array($aSal)){
									for($i=0;$i<count($aSal);$i++){?>
										<option value="<?php echo $aSal[$i]->distributor_id;?>" <?php if ($aSal[$i]->distributor_id==$_REQUEST['disID']){ ?> selected <?php } ?> ><?php echo $aSal[$i]->distributor_name;?></option>		<?php } }?>
			  </select>
	</td>

	<td width="8%" valign="bottom" align="right"><h3>Month :</h3></td>
	<td width="14%" align="left">
	<select name="m" id="m" onchange="newMonth(this.value)" class="styledselect_form_5" >
		<?php echo $months = $_objArrayList->getMonthList2($_REQUEST['m']);?>
		</select>
	</td>
			
	<td width="7%" valign="bottom"><h3>Year :</h3></td>
    <td width="13%" align="left">
	<select name="y" id="y"  class="styledselect_form_5" >
		<?php echo $year = $_objArrayList->getYearList2($_REQUEST['y']);?>
	</select>
	</td>	
	
	<td width="32%">
		<input name="submit" class="result-submit" type="submit" id="submit" value="Show graph" />
	</td>
	</tr>
	
	<tr>
		<td colspan="7"><div id="chart_div" style="width: 100%; height: 450px;"></div></td>
	  </tr>
	
	
		</table>
	</form>

<div class="clear">&nbsp;</div>
	
</div>
</div>
</div>
	<!-- Graph code ends here -->
	<?php include("footer.php") ?>
</html>