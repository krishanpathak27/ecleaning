<?php
include("includes/config.inc.php");
include("includes/function.php");
include("includes/globalarraylist.php");

	$jan = 0; $feb = 0; $march = 0; $april = 0; $may = 0; $june = 0; $july = 0; $aug = 0; $sep = 0; $oct = 0; $nov = 0; $dec = 0;
	if($_REQUEST['y']!='' && $_REQUEST['disID']!='')
	{
		$graph=$_objAdmin->_getSelectList('disretdailywiseitemreport ',' sum(totalSaleUnit) as quantity, monthname(date_of_order) as month ',''," distributor_id = ".$_REQUEST['disID']." and year='".$_REQUEST['y']."' group by `month`");
		for($j=0; $j<count($graph); $j++)
		{
			if($graph[$j]->month == 'January')	{ $jan += $graph[$j]->quantity;		}
			if($graph[$j]->month == 'February')	{ $feb += $graph[$j]->quantity;		}
			if($graph[$j]->month == 'March')	{ $march += $graph[$j]->quantity;		}
			if($graph[$j]->month == 'April')	{ $april += $graph[$j]->quantity;		}
			if($graph[$j]->month == 'May')		{ $may += $graph[$j]->quantity;		}
			if($graph[$j]->month == 'June')		{ $june += $graph[$j]->quantity;		}
			if($graph[$j]->month == 'July')		{ $july += $graph[$j]->quantity;		}
			if($graph[$j]->month == 'August')	{ $aug += $graph[$j]->quantity;		}
			if($graph[$j]->month == 'September'){ $sep += $graph[$j]->quantity;		}
			if($graph[$j]->month == 'October')	{ $oct += $graph[$j]->quantity;		}
			if($graph[$j]->month == 'November')	{ $nov += $graph[$j]->quantity;		}
			if($graph[$j]->month == 'December')	{ $dec += $graph[$j]->quantity;		}
		}
	}		
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta charset="utf-8">
<title>Quytech PepUpSales - Distributor Yearly Sale Report Graph</title>
<?php include_once('graph/header-files.php');?>

    <script type="text/javascript">
      google.load('visualization', '1', {packages: ['corechart']});
	 
    </script>
    <script type="text/javascript">
      function drawVisualization() {
        // Some raw data (not necessarily accurate)
        var data = google.visualization.arrayToDataTable([
           ['Month', 'Quantity' ],
          ['January',  <?php echo $jan; ?>],
          ['February',  <?php echo $feb; ?>],
          ['March',  <?php echo $march; ?>],
          ['April',  <?php echo $april; ?>],
		  ['May',  <?php echo $may; ?>],
          ['June',  <?php echo $june; ?>],
          ['July',  <?php echo $july; ?>],
          ['August',  <?php echo $aug; ?>],
		  ['September',  <?php echo $sep; ?>],
		  ['October',  <?php echo $oct; ?>],
		  ['November',  <?php echo $nov; ?>],
		  ['December',  <?php echo $dec; ?>]
        ]);

        var options = {
          title : 'Monthly total number of item sales by a distributer',
          vAxis: {title: "Total item sales" , gridlines: { count: 8  }, minValue:0},
          hAxis: {title: "<?php echo $_REQUEST['y']; ?>"},
          seriesType: "bars",
          series: {5: {type: "line"}}
        };

        var chart = new google.visualization.ComboChart(document.getElementById('chart_div'));
		/* click handler ends */
		 function selectHandler() {
          var selectedItem = chart.getSelection()[0];
          if (selectedItem) {
            var topping = data.getValue(selectedItem.row, 0);
			<?php if($_REQUEST['disID'] != '' ) { ?>
            var url = 'dis_montly_basis_sale_month_graph.php?m='+topping+'&y='+<?php echo $_REQUEST['y']; ?>+'&disID='+<?php echo $_REQUEST['disID']; ?>;
			OpenInNewTab(url);
			<?php } ?>
          }
        }

        google.visualization.events.addListener(chart, 'click', selectHandler);   
		
		/* click handler ends */
        chart.draw(data, options);
      }
      google.setOnLoadCallback(drawVisualization);
    </script>
	</head>
<body> 
<!-- Start: page-top-outer -->

<?php include_once('graph/header.php');?>
<!-- End: page-top-outer -->
	
<div id="content-outer">
<div id="content">
<div id="page-heading"><h1>Yearly Sale of Items Report Graph</h1></div>
<div id="container">

<form name="frmPre" id="frmPre" method="post" action="<?php echo $_SERVER['PHP_SELF'];?>" enctype="multipart/form-data" >
<table width="100%" border="0">
  <tr>
  
	<td width="11%" valign="bottom"><h3>Distributor :</h3></td>
    <td width="15%" align="left">
	<select name="disID" id="disID" class="styledselect_form_5" >
		<option value="">Please Select</option>
			<?php $aSal=$_objAdmin->_getSelectList('table_distributors AS d','distributor_id, distributor_name','',
							"".$Discondition." ORDER BY distributor_name"); 
					if(is_array($aSal)){
						for($i=0;$i<count($aSal);$i++){?>
							<option value="<?php echo $aSal[$i]->distributor_id;?>" <?php if ($aSal[$i]->distributor_id==$_REQUEST['disID']){ ?> selected <?php } ?> ><?php echo $aSal[$i]->distributor_name;?></option>		<?php } }?>
	</select>
	</td>
  
  
  	<td width="6%" valign="bottom"><h3>Year :</h3></td>
    <td width="17%" align="left">
	<select name="y" id="y"  class="styledselect_form_5" >
		<?php echo $year = $_objArrayList->getYearList2($_REQUEST['y']);?>
	</select>
	</td>
	
	<td width="51%" valign="bottom"><input name="submit" class="result-submit" type="submit" id="submit" value="Show graph" /></td>
  </tr>
  
  
  
  
  <tr>
    <td colspan="5"><div id="chart_div" style="width: 100%; height: 450px;"></div></td>
  </tr>
</table>
</form>

<div class="clear">&nbsp;</div>
	
</div>
</div>
</div>
	<!-- Graph code ends here -->
	<?php include("footer.php") ?>
	
	<script>
	function OpenInNewTab(url )
		{
		  var win=window.open(url, '_blank');
		  win.focus();
		}
		</script>
</body>
</html>