<?php
include("includes/config.inc.php");
include("includes/function.php");
$_objAdmin = new Admin();
$day= date("D");
$auOrd=$_objAdmin->_getSelectList('table_order as o left join table_retailer as r on o.retailer_id=r.retailer_id left join table_salesman as s on s.salesman_id=o.salesman_id',"o.*,r.retailer_name,r.retailer_address,r.retailer_location,s.salesman_name",'',"o.order_id=".$_SESSION['disorderdetails']." ");

?>
<?php include("header.inc.php") ?>
<!-- start content-outer -->
<div id="content-outer">
<!-- start content -->
<div id="content">
<div id="page-heading"><h1><span style="color: #d74343; font-family: Tahoma; font-weight: bold;">Order List</span></h1></div>
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" id="content-table">
<tr>
	<th rowspan="3" class="sized"><img src="images/side_shadowleft.jpg" width="20" height="300" alt="" /></th>
	<th class="topleft"></th>
	<td id="tbl-border-top">&nbsp;</td>
	<th class="topright"></th>
	<th rowspan="3" class="sized"><img src="images/side_shadowright.jpg" width="20" height="300" alt="" /></th>
</tr>
<tr>
	<td id="tbl-border-left"></td>
	<td align="center" valign="middle">
	<!--  start content-table-inner -->
	<div id="content-table-inner">
	
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
	<tr valign="top">
	<td>
		
		<!-- start id-form -->
		
		<table border="0" width="100%" cellpadding="0" cellspacing="0" id="id-form">
			<tr>
				<td><b>Salesman Name:</b> <?php echo $auOrd[0]->salesman_name;?></td>
				<td align="right" style="padding-right: 50px;"><b>Order Number:</b> <?php echo $auOrd[0]->order_id;?></td>
			</tr>
			<!-- <tr>
				<td><b>Retailer Name:</b> <?php echo $auOrd[0]->retailer_name;?></td>
				<td align="right" style="padding-right: 50px;"><b>Date:</b> <?php echo date("d-M-Y", strtotime($auOrd[0]->date_of_order)); ?></td>
			</tr>
			<tr>
				<td><b>Retailer Mobile-No:</b> <?php echo $auOrd[0]->retailer_phone_no;?></td>
				<td align="right" style="padding-right: 50px;"><b>Time:</b> <?php echo $auOrd[0]->time_of_order;?></td>
			</tr>
			<tr>
				<td><b>Retailer Market:</b> <?php echo $auOrd[0]->retailer_location;?></td>
				<td align="right" style="padding-right: 50px;"><?php if($auOrd[0]->bill_no !=''){ ?><b>Bill Date:</b> <?php echo $auOrd[0]->bill_date; }?></td>
				
			</tr>
			<tr>
				<td><b>Retailer Address:</b> <?php echo $auOrd[0]->retailer_address;?></td>
				<td align="right" style="padding-right: 50px;"><?php if($auOrd[0]->bill_no !=''){ ?><b>Bill No:</b> <?php echo $auOrd[0]->bill_no; }?></td>
			</tr>
			<tr>
				<td colspan="2" align="center"><img src="images/accept.jpg" width="15" height="15" alt="" /> <b>Accepted</b> &nbsp;&nbsp;&nbsp;&nbsp;<img src="images/reject.jpg" width="15" height="15" alt="" /> <b>Rejected</b></td>
			</tr> -->
		
		</table>
		<form name="frmPre" id="frmPre" method="post" action="dis_order_processed_list.php" enctype="multipart/form-data" >
		<table  border="0" width="100%" cellpadding="0" cellspacing="0" id="id-form">
		<tr bgcolor="#6E6E6E" style="color: #fff;font-weight: bold;">
				<td style="padding:10px;" width="10%">Status</td>
				<td style="padding:10px;" width="30%">Particulars</td>
				<td style="padding:10px;" width="10%">Item Code</td>
				<td style="padding:10px;" width="10%">Item Color</td>
				<td style="padding:10px;" width="10%">Order Quantity</td>
				<td style="padding:10px;" width="10%">Accept Quantity</td>
				<td style="padding:10px;" width="5%">Price</td>
				<td style="padding:10px;" width="5%" align="right">Total</td>
			</tr>
		<?php
		$total=array();
		$condi=" where t.order_id=".$auOrd[0]->order_id." and order_detail_status!=5";
		$Rec = mysql_query("SELECT t.*,i.item_name,i.item_code,c.color_code,g.tag_description FROM table_order_detail as t left join table_item as i on t.item_id=i.item_id left join table_color as c on t.color_id=c.color_id left join table_tags as g on g.tag_id=t.tag_id".$condi);
		while ($auRec = mysql_fetch_array($Rec)){
		?>
		<tr <?php if($auRec['order_detail_status']==2){ ?> style="background-color:#82FA58;border-bottom:2px solid #6E6E6E;" <?php } ?> <?php if($auRec['order_detail_status']==3){ ?> style="background-color:#d74343;color:#ffffff;border-bottom:2px solid #6E6E6E;" <?php } ?>style="border-bottom:2px solid #6E6E6E;" >
			<?php if($auRec['type']==2){ ?>
			<?php if($auRec['discount_type']==1) {?>
			<td style="padding:10px;">Scheme</td>
			<td style="padding:10px;" colspan="6">
			<font color="red"><?php echo $auRec['discount_desc'];?></font>
			</td>
			<td style="padding:10px;" width="5%" align="right"><font color="red">-<?php echo $auRec['acc_total'];?></font></td>
			<?php } ?>
			<?php if($auRec['discount_type']==2) {?>
			<td style="padding:10px;" >Scheme</td>
			<td style="padding:10px;" colspan="6">
			<font color="red"><?php echo $auRec['discount_desc'];?></font>
			</td>
			<td style="padding:10px;" width="5%" align="right"><font color="red">-<?php echo $auRec['acc_total'];?></font></td>
			<?php } ?>
			<?php if($auRec['discount_type']==3) {?>
			<td style="padding:10px;" >Scheme</td>
			<td style="padding:10px;" colspan="3">
			<font color="red"><?php echo $auRec['discount_desc'];?></font>
			</td>
			<td style="padding:10px;" ><font color="red"><?php echo $auRec['total_free_quantity'];?></font></td>
			<td style="padding:10px;" ><font color="red"><?php echo $auRec['acc_free_item_qty'];?></font></td>
			<td style="padding:10px;" ></td>
			<td style="padding:10px;" width="5%" align="right"><font color="red"><?php echo $auRec['acc_total'];?></font></td>
			<?php } ?>
			<?php } else { 
			if($auRec['tag_id']!=0){
			?>
			<td style="padding:10px;" width="10%"><?php echo $auRec['tag_description'] ?></td>
			<?php } else { ?>
			<td style="padding:10px;" width="10%"></td>
			<?php } ?>
			<td style="padding:10px;" width="25%"><?php echo $auRec['item_name'];?></td>
			<td style="padding:10px;" width="8%"><?php echo $auRec['item_code'];?></td>
			<?php if($auRec['color_type']==3) { ?>
			<td style="padding:10px;" ><?php echo $auRec['color_code'];?></td>
			<?php } ?>
			<?php if($auRec['color_type']==2) { ?>
			<td style="padding:10px;" >Assorted</td>
			<?php } ?>
			<?php if($auRec['color_type']==1 || $auRec['color_type']=='') { ?>
			<td style="padding:10px;" ></td>
			<?php } ?>
			<td style="padding:10px;" width="10%"><?php echo $auRec['quantity'];?></td>
			<td style="padding:10px;" width="15%">
			<?php echo $auRec['acc_quantity'];?>
			</td>
			<td style="padding:10px;" width="5%"><?php echo $auRec['price'];?></td>
			<td style="padding:10px;" width="5%" align="right"><?php echo $auRec['acc_total']; $total[]=$auRec['acc_total'];?></td>
			<?php } ?>
		</tr>
		<?php } // close order detail ?>
		<?php
		//combo discount list
		$comboRec = mysql_query("SELECT dc.order_combo_detail_id,dc.acc_discount_desc,dc.acc_discount_amount,dc.discount_type,dc.acc_free_item_qty,dc.free_item_qty,i.item_code FROM table_order_combo_detail as dc left join table_item as i on i.item_id=dc.acc_free_item_id where dc.order_id=".$auOrd[0]->order_id." and dc.status!='5'");
		while ($comboListRec = mysql_fetch_array($comboRec)){
		?>
		<tr  style="border-bottom:2px solid #6E6E6E;">
			<?php if($comboListRec['discount_type']==1) {?>
			<td style="padding:10px;">Scheme</td>
			<td style="padding:10px;" colspan="6" >
			<font color="red"><?php echo $comboListRec['acc_discount_desc'];?></font>
			</td>
			<td align="right" style="padding:10px;" width="5%"><font color="red">-<?php echo $comboListRec['acc_discount_amount'];?></font></td>
			<?php } ?>
			<?php if($comboListRec['discount_type']==2) {?>
			<td style="padding:10px;" >Scheme</td>
			<td style="padding:10px;" colspan="6">
			<font color="red"><?php echo $comboListRec['acc_discount_desc'];?></font>
			</td>
			<td align="right" style="padding:10px;" width="5%"><font color="red">-<?php echo $comboListRec['acc_discount_amount'];?></font></td>
			<?php } ?>
			<?php if($comboListRec['discount_type']==3) {?>
			<td style="padding:10px;" >Scheme</td>
			<td style="padding:10px;" colspan="1" >
			<font color="red"><?php echo $comboListRec['acc_discount_desc'];?></font>
			</td>
			<td style="padding:10px;" ><font color="red"><?php echo $comboListRec['item_code'];?></font></td>
			<td style="padding:10px;" ></td>
			<td style="padding:10px;" ><font color="red"><?php echo $comboListRec['free_item_qty'];?></font></td>
			<td style="padding:10px;" ><font color="red"><?php echo $comboListRec['acc_free_item_qty'];?></font></td>
			<td style="padding:10px;" ></td>
			<td style="padding:10px;" width="5%" align="right"><font color="red"><?php echo $comboListRec['acc_discount_amount'];?></font></td>
			<?php } ?>
		</tr>
		<?php } ?>
		<?php
		$net = mysql_query("SELECT o.*,t.tag_description FROM table_order as o left join table_tags as t on t.tag_id=o.tag_id where o.order_id=".$auOrd[0]->order_id." " );
		$netTotal = mysql_fetch_array($net);
		if($netTotal['acc_discount_id']!=0){
		?>
		<tr style="border-bottom:2px solid #6E6E6E; border-top:2px solid #6E6E6E;" >
			<td style="padding:10px" align="right" colspan="7">
			<b>Total</b></td>
			<td align="right" style="padding:10px"><b><?php echo number_format(array_sum($total), 2, '.', ',');?></b></td>
		</tr>
		<tr  >
			<td style="padding:10px;" colspan="5">
			<font color="red"><b>Scheme(<?php echo $netTotal['discount_desc'];?>)</b></font></td>
			<td style="padding:10px;"><font color="red"><b><?php echo $netTotal['acc_free_item_qty'];?>
			<td style="padding:10px;"><font color="red"><b><?php //echo $netTotal['discount_amount'];?>
			<td style="padding:10px;" align="right"><font color="red"><b><?php if($netTotal['acc_discount_amount']!=0.00){ echo "-".$netTotal['acc_discount_amount'] ; }?></b></font></td>
		</tr>
		<?php } ?>
		<tr style="border-bottom:2px solid #6E6E6E; border-top:2px solid #6E6E6E;" >
			<td style="padding:10px;" align="right" colspan="7">
			<b>Net Total</b></td>
			<td style="padding:10px;" align="right"><b><?php echo $netTotal['acc_total_invoice_amount'];?></b></td>
		</tr>
		<tr align="center">
			<td style="padding:10px;" colspan="8">
			<input type="button" value="Back" class="form-cen" onclick="location.href='distributors_order_list.php';" /></td>
		</tr>
		
	</table>
	</form>
	</td>
	</tr>
	<tr valign="top">
	<td>
		<div id="div1"></div>
	</td>
	</tr>
	</div>
	</td>
</tr>
</table>
<div class="clear"></div>
</div>
<!--  end content-table-inner  -->
</td>
<td id="tbl-border-right"></td>
</tr>
<tr>
	<th class="sized bottomleft"></th>
	<td id="tbl-border-bottom">&nbsp;</td>
	<th class="sized bottomright"></th>
</tr>
</table>
<div class="clear">&nbsp;</div>
</div>
<!--  end content -->
<div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->
<div class="clear">&nbsp;</div> 
<!-- start footer -->         
<?php include("footer.php") ?>
<!-- end footer -->
<script src="javascripts/jquery-1.8.2.js" type="text/javascript"></script>
<script src="javascripts/jquery-ui.js"></script>
<script type="text/javascript" src="javascripts/validate.js"></script>
<script type="text/javascript">
// Popup window code
function newPopup(url) {
	popupWindow = window.open(
		url,'popUpWindow','height=500,width=600,left=200,top=100,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no,status=yes')
}
</script>
<script>
    $(function() {
        $( "#from" ).datepicker({
			dateFormat: "d M yy",
            defaultDate: "w",
            changeMonth: true,
            numberOfMonths: 1,
            onSelect: function( selectedDate ) {
                $( "#to" ).datepicker( "option", "minDate", selectedDate );
            }
        });
        $( "#to" ).datepicker({
			dateFormat: "d M yy",
            defaultDate: "-w",
            changeMonth: true,
            numberOfMonths: 1,
            onSelect: function( selectedDate ) {
                $( "#from" ).datepicker( "option", "maxDate", selectedDate );
            }
        });
    });
</script>
</body>
</html>