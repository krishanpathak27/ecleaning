
<script>
	$(function() {
		var availableState = [
			<?php
			$auCol=$_objAdmin->_getSelectList2('state',"state_name",''," 1=1 ORDER by state_name");
			for($i=0;$i<count($auCol);$i++){
				if(count($auCol)-1!=$i)
				{
					echo '"'.$auCol[$i]->state_name.'",';
				} else {
					echo '"'.$auCol[$i]->state_name.'"';
				}
			}
			?>
		];
		function split( val ) {
			return val.split( /,\s*/ );
		}
		function extractLast( term ) {
			return split( term ).pop();
		}

		$( "#state_list" )
			// don't navigate away from the field on tab when selecting an item
			.bind( "keydown", function( event ) {
				if ( event.keyCode === $.ui.keyCode.TAB &&
						$( this ).data( "autocomplete" ).menu.active ) {
					event.preventDefault();
				}
			})
			.autocomplete({
				minLength: 0,
				source: function( request, response ) {
					// delegate back to autocomplete, but extract the last term
					response( $.ui.autocomplete.filter(
						availableState, extractLast( request.term ) ) );
				},
				focus: function() {
					// prevent value inserted on focus
					return false;
				},
				select: function( event, ui ) {
					var terms = split( this.value );
					// remove the current input
					terms.pop();
					// add the selected item
					terms.push( ui.item.value );
					// add placeholder to get the comma-and-space at the end
					terms.push( "" );
					//this.value = terms.join( "," );
 					//return false;
					this.value = terms.join( "," );
					//alert(ui.item.value);
					//alert(availableState);
					
					availableState.splice($.inArray(ui.item.value, availableState), 1);
					return false;
					
				}
			});
	});

	$(function() {
		var availableCity = [
			<?php
			$auCol=$_objAdmin->_getSelectList2('city',"city_name",''," ORDER by city_name");
			for($i=0;$i<count($auCol);$i++){
				if(count($auCol)-1!=$i)
				{
					echo '"'.$auCol[$i]->city_name.'",';
				} else {
					echo '"'.$auCol[$i]->city_name.'"';
				}
			}
			?>
		];
		function split( val ) {
			return val.split( /,\s*/ );
		}
		function extractLast( term ) {
			return split( term ).pop();
		}

		$( "#city_list" )
			// don't navigate away from the field on tab when selecting an item
			.bind( "keydown", function( event ) {
				if ( event.keyCode === $.ui.keyCode.TAB &&
						$( this ).data( "autocomplete" ).menu.active ) {
					event.preventDefault();
				}
			})
			.autocomplete({
				minLength: 0,
				source: function( request, response ) {
					// delegate back to autocomplete, but extract the last term
					response( $.ui.autocomplete.filter(
						availableCity, extractLast( request.term ) ) );
				},
				focus: function() {
					// prevent value inserted on focus
					return false;
				},
				select: function( event, ui ) {
					var terms = split( this.value );
					// remove the current input
					terms.pop();
					// add the selected item
					terms.push( ui.item.value );
					// add placeholder to get the comma-and-space at the end
					terms.push( "" );
					this.value = terms.join( "," );
					return false;
				}
			});
	});

	$(function() {
		var availableRet = [
			<?php
			$auCol=$_objAdmin->_getSelectList('table_retailer',"retailer_name",''," division_id = '".$_GET['division_id']."' AND status!='D'  ORDER by retailer_name");
			for($i=0;$i<count($auCol);$i++){
				if(count($auCol)-1!=$i)
				{
					$remove = array("&");
					echo '"'.htmlspecialchars(str_replace($remove, 'And',$auCol[$i]->retailer_name)).'",';
				} else {
					echo '"'.htmlspecialchars(str_replace($remove, 'And ',$auCol[$i]->retailer_name)).'"';
				}
			}
			?>
		];
		function split( val ) {
			return val.split( /,\s*/ );
		}
		function extractLast( term ) {
			return split( term ).pop();
		}

		$( "#ret_list" )
			// don't navigate away from the field on tab when selecting an item
			.bind( "keydown", function( event ) {
				if ( event.keyCode === $.ui.keyCode.TAB &&
						$( this ).data( "autocomplete" ).menu.active ) {
					event.preventDefault();
				}
			})
			.autocomplete({
				minLength: 0,
				source: function( request, response ) {
					// delegate back to autocomplete, but extract the last term
					response( $.ui.autocomplete.filter(
						availableRet, extractLast( request.term ) ) );
				},
				focus: function() {
					// prevent value inserted on focus
					return false;
				},
				select: function( event, ui ) {
					var terms = split( this.value );
					// remove the current input
					terms.pop();
					// add the selected item
					terms.push( ui.item.value );
					// add placeholder to get the comma-and-space at the end
					terms.push( "" );
					this.value = terms.join( "," );
					return false;
				}
			});
	});






	$(function() {
		// var brand_id = $("#brand").val();
		// alert(brand_id);
		var availableDis = [
			<?php
			$auCol=$_objAdmin->_getSelectList('table_distributors',"distributor_code, distributor_name",''," division_id = '".$_GET['division_id']."' AND status!='D' ORDER by distributor_name");
			for($i=0;$i<count($auCol);$i++){
				if(count($auCol)-1!=$i)
				{
					$remove = array("&");
					echo '"'.htmlspecialchars(str_replace($remove, 'And',$auCol[$i]->distributor_name)).'",';
				} else {
					echo '"'.htmlspecialchars(str_replace($remove, 'And ',$auCol[$i]->distributor_name)).'"';
				}
			}
			?>
		];
		function split( val ) {
			return val.split( /,\s*/ );
		}
		function extractLast( term ) {
			return split( term ).pop();
		}

		$( "#dis_list" )
			// don't navigate away from the field on tab when selecting an item
			.bind( "keydown", function( event ) {
				if ( event.keyCode === $.ui.keyCode.TAB &&
						$( this ).data( "autocomplete" ).menu.active ) {
					event.preventDefault();
				}
			})
			.autocomplete({
				minLength: 0,
				source: function( request, response ) {
					// delegate back to autocomplete, but extract the last term
					response( $.ui.autocomplete.filter(
						availableDis, extractLast( request.term ) ) );
				},
				focus: function() {
					// prevent value inserted on focus
					return false;
				},
				select: function( event, ui ) {
					var terms = split( this.value );
					// remove the current input
					terms.pop();
					// add the selected item
					terms.push( ui.item.value );
					// add placeholder to get the comma-and-space at the end
					terms.push( "" );
					this.value = terms.join( "," );
					return false;
				}
			});
	});









	
	$(function() {
		var availableItem = [
			<?php
			$auCol=$_objAdmin->_getSelectList('table_item',"item_code",''," 1=1 ORDER by item_code");
			for($i=0;$i<count($auCol);$i++){
				if(count($auCol)-1!=$i)
				{
					echo '"'.$auCol[$i]->item_code.'",';
				} else {
					echo '"'.$auCol[$i]->item_code.'"';
				}
			}
			?>
		];
		function split( val ) {
			return val.split( /,\s*/ );
		}
		function extractLast( term ) {
			return split( term ).pop();
		}

		$( "#item_list" )
			// don't navigate away from the field on tab when selecting an item
			.bind( "keydown", function( event ) {
				if ( event.keyCode === $.ui.keyCode.TAB &&
						$( this ).data( "autocomplete" ).menu.active ) {
					event.preventDefault();
				}
			})
			.autocomplete({
				minLength: 0,
				source: function( request, response ) {
					// delegate back to autocomplete, but extract the last term
					response( $.ui.autocomplete.filter(
						availableItem, extractLast( request.term ) ) );
				},
				focus: function() {
					// prevent value inserted on focus
					return false;
				},
				select: function( event, ui ) {
					var terms = split( this.value );
					// remove the current input
					terms.pop();
					// add the selected item
					terms.push( ui.item.value );
					// add placeholder to get the comma-and-space at the end
					terms.push( "" );
					this.value = terms.join( "," );
					return false;
				}
			});
	});
	</script>
