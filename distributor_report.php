<?php include("includes/config.inc.php");
      include("includes/function.php");
	  include("includes/globalarraylist.php");
	  
		$page_name="Distributor Wise Report";


		$retailer_name = '-'; 
		$percentage = array();
		

if(isset($_POST['showReport']) && $_POST['showReport'] == 'yes')
{	
	if($_POST['salname']!="") 
	{
	$_SESSION['SalnameList']=$_POST['salname'];	
	} else {
	unset($_SESSION['SalnameList']);
	}
	
	
	if($_POST['filterby']!=""){
		
			$_SESSION['disfilterBY']=$_POST['filterby'];	
		
		}
		
	
	if($_POST['from']!="") 
	{
	$_SESSION['FromDisList']=$_objAdmin->_changeDate($_POST['from']);	
	}
	if($_POST['to']!="") 
	{
	$_SESSION['ToDisList']=$_objAdmin->_changeDate($_POST['to']);	
	}
	if($_POST['dist']!="") 
	{
	$_SESSION['DistList']=$_POST['dist'];
	}
	$_SESSION['order_by']="desc";
	} else {
	$_SESSION['FromDisList']= $_objAdmin->_changeDate(date("Y-m-d"));
	$_SESSION['ToDisList']= $_objAdmin->_changeDate(date("Y-m-d"));
	$_SESSION['order_by']="desc";

}
if($_SESSION['disfilterBY']==''){
		
			$_SESSION['disfilterBY']=1;	
		
		}
	
if($_SESSION['userLoginType']==3){
	$_SESSION['DistList']=$_SESSION['distributorId'];
}

if(isset($_REQUEST['reset']) && $_REQUEST['reset'] == 'yes')
{
	unset($_SESSION['DistList']);	
	unset($_SESSION['SalnameList']);
	header("Location: distributor_report.php");
}

if($_SESSION['DistList']!=''){
$DistName=$_objAdmin->_getSelectList('table_distributors','distributor_name',''," distributor_id='".$_SESSION['DistList']."'"); 
$dist=$DistName[0]->distributor_name;
}
if($_SESSION['SalnameList']!=''){
//$salesman="AND o.salesman_id='".$_SESSION['SalnameList']."'";
		$salArrList=$_SESSION['SalnameList'];	
		$filterby=$_SESSION['disfilterBY'];
		$salesman = $_objArrayList->getSalesmanConsolidatedList($salArrList,$filterby);
}


if($_SESSION['DistList']!=''){
$list="d.distributor_id='".$_SESSION['DistList']."'";
}
else{
$list="'1'";
}
if($_REQUEST['order_by']!=''){
if($_REQUEST['order_by']=='desc'){
$_SESSION['order_by']="asc";
} else {
$_SESSION['order_by']="desc";
}
//$_SESSION['order_by']=$_REQUEST['order_by'];
$_SESSION['FromDisList']=$_REQUEST['from'];
$_SESSION['ToDisList']=$_REQUEST['to'];
} else {
if($_POST['dist']==""){
unset($_SESSION['DistList']);
}
if($_POST['salname']==""){
unset($_SESSION['SalnameList']);
}
}	


?>

<?php include("header.inc.php") ?>


<script type="text/javascript">

    function PrintElem(elem)
    {
        Popup($(elem).html());
    }

    function Popup(data) 
    {
		
        var mywindow = window.open('', 'Report');
		
        mywindow.document.write('<html><head><title>Distributor Wise Report</title>');
		mywindow.document.write('<table><tr><td><b>City Name:</b> <?php echo $city; ?></td><td><b>Market Name:</b> <?php echo $_SESSION['MarketList']; ?></td><td><b>From Date:</b> <?php echo $_SESSION['FromDisList']; ?></td><td><b>To Date:</b> <?php echo $_SESSION['ToDisList']; ?></td></tr></table>');
        /*optional stylesheet*/ //mywindow.document.write('<link rel="stylesheet" href="main.css" type="text/css" />');
        mywindow.document.write('</head><body >');
        mywindow.document.write(data);
        mywindow.document.write('</body></html>');

        mywindow.print();
        mywindow.close();
        return true;
    }
	
	

	$(document).ready(function(){
	
		$('.maintr').click(function() {
			//alert("Hello");
			//$(this).css('background', '#E3E3E3');
			$('#lists tr').removeClass('trbgcolor');
			
			$(this).addClass('trbgcolor');
		});
		//$('#lists').find('tr').keydown(function(e){
		//$('#lists tr').keydown(function(e) {
		
		
	});	
	
	
	
	$(document).keydown(function(e) {
		//alert(e);
		if(e.keyCode==113){
			var id = $('.trbgcolor input').val();
			var from = document.getElementById('from').value;
			var to = document.getElementById('to').value;
			var sal = document.getElementById('salname').value;
			if(typeof id!='undefined')
			//alert(to); 
			window.open("retailer_wise_sales_of_distributor_report.php?id="+id+"&from="+from+"&to="+to+"&sal="+sal, '_blank');
			else 
			alert('Please Select One Record');
			}
		});
</script>
<script src="javascripts/dateNextPrev.js" type="text/javascript"></script>
<!-- start content-outer -->
<div id="content-outer">
<!-- start content -->
<div id="content">
<div id="page-heading"><h1><span style="color: #d74343; font-family: Tahoma; font-weight: bold;">Distributor Wise Report</span></h1></div>
<table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">
<tr>
	<td>
	<!--  start content-table-inner -->
	
	<div id="content-table-inner">
	
	<div id="page-heading" align="left" >
		<form name="frmPre" id="frmPre" method="post" action="" enctype="multipart/form-data" >
		<table border="0" width="100%" cellpadding="0" cellspacing="0">
		<tr>
			
			<td ><h3>Distributor: </h3> <h6> 
			<select name="dist" id="dist" class="" style="border: solid 1px #BDBDBD; color: #393939;font-family: Arial;font-size: 12px;border-radius:5px ;-moz-border-radius:5px;-o-border-radius:5px;-ms-border-radius:5px;-webkit-border-radius:5px;width: 125px;height: 30px;" onChange="showMarket(this.value)" >
			<option value="" >All Distributor</option>
			<?php if($_SESSION['userLoginType']==3){
			$aDist=$_objAdmin->_getSelectList('table_distributors AS d','distributor_id, distributor_name',''," distributor_id='".$_SESSION['distributorId']."' ORDER BY distributor_name");
		if(is_array($aDist)){
		?>
		<option value="<?php echo $aDist[0]->distributor_id;?>" <?php if ($aDist[0]->distributor_id==$_SESSION['DistList']){ ?> selected <?php } ?>><?php echo $aDist[0]->distributor_name;?></option>
		<?php }}



		if( $_SESSION['userLoginType']==5){
	    $DivisionId=$_objAdmin->_getSelectList2('table_salesman','division_id',''," salesman_id='".$_SESSION['salesmanId']."'");
	
		if($DivisionId->division_id==6)
		{
			$aDist=$_objAdmin->_getSelectList2('table_distributors','distributor_id, distributor_name',''," division_id=6");
			 //print_r($DistrictName); die;
		}
		else
		{
			$aDist=$_objAdmin->_getSelectList2('table_distributors','distributor_id, distributor_name',''," division_id!=6");

		}
		if(is_array($aDist)){
			for($i=0;$i<count($aDist);$i++){
				?>
				<option value="<?php echo $aDist[$i]->distributor_id;?>" <?php if ($aDist[$i]->distributor_id==$_SESSION['DistList']){ ?> selected <?php } ?>><?php echo $aDist[$i]->distributor_name;?></option>
				<?php }}


			//print_r($_SESSION['DistrictName']);
		}

       else { ?>
			<option value="" >All Distributor</option>
			<?php $aDist=$_objAdmin->_getSelectList('table_distributors','distributor_id,distributor_name',''," distributor_name!='' ORDER BY distributor_name"); 
			if(is_array($aDist)){
			for($i=0;$i<count($aDist);$i++){
			?>
			<option value="<?php echo $aDist[$i]->distributor_id;?>" <?php if ($aDist[$i]->distributor_id==$_SESSION['DistList']){ ?> selected <?php } ?>><?php echo $aDist[$i]->distributor_name;?></option>
			<?php }} }?>
			</select></h6></td>
			
			
			
			<td ><h3>Salesman: </h3><h6>
			<select name="salname" id="salname" class="menulist">
				<?php echo $salesmanOptionList = $_objArrayList->GetSalesmenMenu($salsList, $_SESSION['SalnameList']);?>
			</select></h6></td>
			
			
			
			
			<td><h3>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;From Date: </h3><h6> <img src="css/images/prev.png" height="18" width="18" onclick="dateFromPrev();">  <input type="text" id="from" name="from" class="date" style="width:100px" value="<?php  echo $_SESSION['FromDisList'];?>"  readonly /> <img src="css/images/next.png" height="18" width="18" onclick="dateFromNext();"></h6></td>
			<td><h3>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;To Date: </h3><h6>  <img src="css/images/prev.png" height="18" width="18" onclick="dateToPrev();">  <input type="text" id="to" name="to" class="date" style="width:100px" value="<?php echo $_SESSION['ToDisList']; ?>"  readonly /> <img src="css/images/next.png" height="18" width="18" onclick="dateToNext();"></h6></td>
			<td><h3></h3>
			<input name="submit" class="result-submit" type="submit" id="submit" value="View Details" />
	<input type="button" value="Reset!" class="form-reset" onclick="location.href='distributor_report.php?reset=yes';" />
			</td>
			<td colspan="3"></td>
			</tr>
			<tr class="consolidatedReport">
			<td colspan="3" ><h2>View Report For:&nbsp;&nbsp;
			<input type="radio" name="filterby" value="1" <?php if($_SESSION['disfilterBY']==1){ ?> checked="checked" <?php } ?>   />&nbsp;Individual
					<input type="radio" name="filterby" value="2" <?php if($_SESSION['disfilterBY']==2){ ?> checked="checked" <?php } ?>  />&nbsp;Hierarchy</h2>
					</td>
					</tr>
			<tr>
			<td colspan="6"><input name="showReport" type="hidden" value="yes" />
			
			<input type="button" value="Print" class="result-submit" onclick="PrintElem('#Report')" />
			<input type="submit" name="submit" value="Export to Excel" class="result-submit"  />
			<!-- <a href="distributor_report.php#chart_div"><input name="graph" class="result-submit" type="button" id="graph" value="Show Graph" /></a> -->
			
			</td>
			
		</tr>
		</table>
		</form>
	</div>
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
	<?php
	//if($_SESSION['DistList']==""){
	?>
	<tr valign="top">		
		<td>
		<div style="padding:10px; font-size:15px; font-weight:bold;" >Press F2 to Check Dealer Wise Sales</div>
		<div style="width:1000px;overflow:auto; height:auto;overflow:auto;" >
		<div id="Report">
		
			<?php
			//echo $list;exit;
			$auRet=$_objAdmin->_getSelectList('table_order as o left join table_distributors as d on o.distributor_id=d.distributor_id LEFT JOIN table_salesman AS s ON s.salesman_id = o.salesman_id',"COUNT(o.distributor_id) as total",''," $list and (o.date_of_order BETWEEN '".date('Y-m-d', strtotime($_SESSION['FromDisList']))."' AND '".date('Y-m-d', strtotime($_SESSION['ToDisList']))."') $salesman AND o.order_type!='No' GROUP BY o.distributor_id ORDER BY total desc LIMIT 1");
			if(is_array($auRet)){
			?>
			<table  border="1"  cellpadding="0" cellspacing="0" id="lists">
			<tr  bgcolor="#6E6E6E" style="color: #fff;font-weight: bold;" id="candidateNameTD">
				<td style="padding:10px;" width="20%"><div style="width: 100px;">Distributor Name</div></td>
				<td style="padding:10px;" width="20%"><div style="width: 100px;">Distributor Code</div></td>
				<!--<td style="padding:10px;" width="10%"><div style="width: 100px;">Market</div></td>-->
				<form name="submitForm" method="POST" action="">
				<input type="hidden" name="order_by" value="<?php echo $_SESSION['order_by']; ?>">
				<input type="hidden" name="from" value="<?php echo $_SESSION['FromDisList']; ?>">
				<input type="hidden" name="to" value="<?php echo $_SESSION['ToDisList'];  ?>">
				<!--<td style="padding:10px;" width="10%"><div style="width: 65px;">Total &nbsp;&nbsp;&nbsp;&nbsp;<a href="retailer_report.php?order_by=<?php echo $_SESSION['order_by']; ?>&from=<?php echo $_SESSION['FromDisList']; ?>&to=<?php echo $_SESSION['ToDisList'];  ?>" ><img src="images/arrow-up.png" width="13" height="13" /></a></div></td>-->
				<td style="padding:10px;" width="10%"><div style="width: 65px;">Total &nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:document.submitForm.submit()">
				<?php if($_SESSION['order_by']=='desc'){ ?>
				<img src="images/arrow-up.png" width="13" height="13" />
				<?php } else { ?>
				<img src="images/arrow-down.png" width="13" height="13" />
				<?php } ?>
				</a></div></td>
				</form>
				<td style="padding:10px;" width="10%"><div style="width: 65px;" align="center">% Share</div></td>
				<?php
				$row_total=$auRet[0]->total;
				 for($a=0;$a<$auRet[0]->total;$a++){ ?>
				<td style="padding:10px; " width="20%"><div style="width: 65px;" align="center">Date <?php echo $a+1; ?></div></td>
				<td style="padding:10px;" width="10%">Salesman</td>
				<td style="padding:10px;" width="10%">Salesman Code</td>
				<td style="padding:10px;" width="20%"><div style="width: 85px;">Order Value <?php echo $a+1; ?></div></td>
				
				<?php } ?>
				
			</tr>
			
			<?php
			if(isset($_POST['ord'])) { $ordby = $_POST['ord']; } else { $ordby = 'desc';} 
			$orderList=$_objAdmin->_getSelectList('table_order as o left join table_distributors as d on o.distributor_id=d.distributor_id LEFT JOIN table_salesman AS s ON s.salesman_id = o.salesman_id',"sum(o.acc_total_invoice_amount) as total_amt,o.order_id,o.distributor_id,d.distributor_name,d.distributor_code",''," $list and (o.date_of_order BETWEEN '".date('Y-m-d', strtotime($_SESSION['FromDisList']))."' AND '".date('Y-m-d', strtotime($_SESSION['ToDisList']))."') $salesman AND o.order_type!='No' GROUP BY o.distributor_id  Order by total_amt ".$_SESSION['order_by']."");
			if(is_array($orderList)){
			//echo count($orderList);
				for($i=0;$i<count($orderList);$i++)
				{
			?>
			<tr class="maintr"  style="border-bottom:2px solid #6E6E6E;">
			<td style="padding:10px;" width="20%"><input type="hidden" id="<?php echo $i+1; ?>" value="<?php echo $orderList[$i]->distributor_id;?>"> <?php echo $distributor_name = $orderList[$i]->distributor_name;?></td>
			<td style="padding:10px;" width="20%"><?php echo  $orderList[$i]->distributor_code;?> </td>
			<!--<td style="padding:10px;" width="20%"><?php echo $orderList[$i]->retailer_location;?> </td>-->
			<td style="padding:10px;" width="20%"><?php echo  $orderList[$i]->total_amt;?> </td>
			<?php
			$orderTotal=$_objAdmin->_getSelectList('table_order as o left join table_distributors as d on o.distributor_id=d.distributor_id LEFT JOIN table_salesman AS s ON s.salesman_id = o.salesman_id',"count(o.distributor_id) as total_cal",''," o.distributor_id='".$orderList[$i]->distributor_id."' and (o.date_of_order BETWEEN '".date('Y-m-d', strtotime($_SESSION['FromDisList']))."' AND '".date('Y-m-d', strtotime($_SESSION['ToDisList']))."') $salesman AND o.order_type!='No'");
			?>
		
			
			<?php
			$Netorder=$_objAdmin->_getSelectList('table_order as o left join table_distributors as d on o.distributor_id=d.distributor_id LEFT JOIN table_salesman AS s ON s.salesman_id = o.salesman_id',"sum(o.acc_total_invoice_amount) as net_amt",''," $list and (o.date_of_order BETWEEN '".date('Y-m-d', strtotime($_SESSION['FromDisList']))."' AND '".date('Y-m-d', strtotime($_SESSION['ToDisList']))."') $salesman AND o.order_type!='No' ");
			?>
			<td style="padding:10px;" width="20%" align="center"><?php echo round($orderList[$i]->total_amt/$Netorder[0]->net_amt*100,2) ."%";
			$percentage[$distributor_name] = round($orderList[$i]->total_amt/$Netorder[0]->net_amt*100,2); ?> </td>
			<?php
			$orderDet=$_objAdmin->_getSelectList('table_order as o left join table_distributors as d on o.distributor_id=d.distributor_id LEFT JOIN table_salesman AS s ON s.salesman_id = o.salesman_id',"o.order_id,o.date_of_order,o.acc_total_invoice_amount,o.order_type,s.salesman_name,s.salesman_code",''," o.distributor_id='".$orderList[$i]->distributor_id."' and (o.date_of_order BETWEEN '".date('Y-m-d', strtotime($_SESSION['FromDisList']))."' AND '".date('Y-m-d', strtotime($_SESSION['ToDisList']))."') $salesman AND o.order_type!='No' Order by o.date_of_order asc ");
				for($b=0;$b<count($orderDet);$b++)
				{
			?>
			<td style="padding:5px;" bgcolor="#98FB98" width="20%"><?php echo $_objAdmin->_changeDate($orderDet[$b]->date_of_order);?> </td>
			<td style="padding:10px;" width="20%"><?php echo $orderDet[$b]->salesman_name;?> </td>
			<td style="padding:10px;" width="20%"><?php echo $orderDet[$b]->salesman_code
			;?> </td>
			<td style="padding:10px;" width="20%">
			<?php if($orderDet[$b]->order_type!='No'){
			echo "<a href=\"order_list.php?act_id=".base64_encode($orderDet[$b]->order_id)."\" target='_blank' ><b>".$orderDet[$b]->acc_total_invoice_amount."</b></a>";
			} else {
			echo $orderDet[$b]->acc_total_invoice_amount;
			}
			?> 
			</td>
			<?php } ?>
			<?php
				for($c=0;$c<$row_total-count($orderDet);$c++){
			?>
			<td style="padding:10px;" bgcolor="#98FB98" width="20%" align="center">-</td>
			<td style="padding:10px;" width="20%" align="center">-</td>
			<td style="padding:10px;" width="20%" align="center">-</td>
			<?php } ?>
			</tr>
			<?php } } ?>
			</table>
		<?php } else { ?>
		<table  border="1" width="100%" cellpadding="0" cellspacing="0" id="lists">
		<tr  style="border-bottom:2px solid #6E6E6E;" align="center" >
		 <td style="padding:10px;" width="100%">Report Not Available</td>
		</tr>
		</table>	
		<?php } ?>	
		
		</div>
		</div>
		</td>
	</tr>
	<tr>
		<td><img src="images/blank.gif" width="695" height="1" alt="blank" /></td>
		<td></td>
	</tr>
	</table>
<div class="clear"></div>
<!-- Graph code starts here -->

    
    <!-- Graph code starts here -->
<?php 
arsort($percentage);
$d_name = array(); 
$p_value = array();
$j=0; 
$p_other = 0;
if(count($percentage)>10)
{
	foreach($percentage as $x=>$x_value)
    {
		if($j<10)
		{
			$d_name[]= $x;
			$p_value[]= $x_value;
			$j=$j+1;
		}
		else
		{
			$d_other = 'Others';
			$p_other = $p_other+$x_value;
		}		
	}
}
else
{
	foreach($percentage as $x=>$x_value)
    {
		$d_name[]= $x;
		$p_value[]= $x_value;
	}
	for($i=count($percentage); $i<10; $i++)
	{
		$d_name[]= '';
		$p_value[]= 0;
	}
	$r_other = 'Others';
	$p_other = 0;
}
 ?>
 
 <!--<script type="text/javascript" src="javascripts/jsapi"></script>
    <script type="text/javascript">
      google.load("visualization", "1", {packages:["corechart"]});
      google.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Distributor', 'Percentage'],
          ['<?php echo $d_name[0]; ?>',     <?php echo $p_value[0]; ?>],
          ['<?php echo $d_name[1]; ?>',     <?php echo $p_value[1]; ?>],
          ['<?php echo $d_name[2]; ?>',  	<?php echo $p_value[2]; ?>],
          ['<?php echo $d_name[3]; ?>', 	<?php echo $p_value[3]; ?>],
		  ['<?php echo $d_name[4]; ?>', 	<?php echo $p_value[4]; ?>],
		  ['<?php echo $d_name[5]; ?>', 	<?php echo $p_value[5]; ?>],
		  ['<?php echo $d_name[6]; ?>', 	<?php echo $p_value[6]; ?>],
		  ['<?php echo $d_name[7]; ?>', 	<?php echo $p_value[7]; ?>],
		  ['<?php echo $d_name[8]; ?>', 	<?php echo $p_value[8]; ?>],
		  ['<?php echo $d_name[9]; ?>', 	<?php echo $p_value[9]; ?>],
          ['<?php echo $d_other; ?>',    <?php echo $p_other; ?>]
        ]);		
		
		var dName= "<?php echo $d_name[0]; ?>";
		if(dName==''){
		var dTitle= '';
		}
	else{var dTitle= '<?php if($_SESSION['userLoginType']!=3) {?>Top 10 Distributors<?php } ?>';}
        var options = {
          title: dTitle
        };

        var chart = new google.visualization.PieChart(document.getElementById('chart_div'));
        chart.draw(data, options);
      }
    </script>-->
    <div id="chart_div" style="width: 100%; height: 500px;"></div>
	<!-- Graph code ends here -->
</div>

<!--  end content-table-inner  -->
</td>
<td id="tbl-border-right"></td>
</tr>
<!--<tr>
	<th class="sized bottomleft"></th>
	<td id="tbl-border-bottom">&nbsp;</td>
	<th class="sized bottomright"></th>
</tr>-->
</table>
<div class="clear">&nbsp;</div>
</div>
<!--  end content -->
<div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->
<div class="clear">&nbsp;</div> 
<!-- start footer -->        
<?php include("footer.php") ?>

<!-- end footer -->
<script src="javascripts/jquery-1.8.2.js" type="text/javascript"></script>
<script src="javascripts/jquery-ui.js"></script>
<script type="text/javascript" src="javascripts/validate.js"></script>
<script type="text/javascript">
// Popup window code
function newPopup(url) {
	popupWindow = window.open(
		url,'popUpWindow','height=500,width=600,left=200,top=100,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no,status=yes')
}
</script>
<script>
    $(function() {
        $( "#from" ).datepicker({
			dateFormat: "d M yy",
            defaultDate: "w",
            changeMonth: true,
            numberOfMonths: 1,
            onSelect: function( selectedDate ) {
                $( "#to" ).datepicker( "option", "minDate", selectedDate );
            }
        });
        $( "#to" ).datepicker({
			dateFormat: "d M yy",
            defaultDate: "-w",
            changeMonth: true,
            numberOfMonths: 1,
            onSelect: function( selectedDate ) {
                $( "#from" ).datepicker( "option", "maxDate", selectedDate );
            }
        });
    });
</script>
<script>
$(document).ready(function() {
 
   <?php 
   if($_POST['submit']=='Export to Excel'){?>
   window.location.assign("export.inc.php?export_dist_vise_report");
   <?php } ?> 

	
});
</script>
</body>
</html>