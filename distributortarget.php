<?php
include("includes/config.inc.php");
include("includes/function.php");
include("includes/globalarraylist.php");



function checkUserType($sessionValueUserType) {

	$type = "";

	if(isset($sessionValueUserType) && !empty($sessionValueUserType)){

		switch ($sessionValueUserType) {
			
			case 1 : 
				//echo $type;
				$type = "account/";

			break;

			case 3 : 
				//echo $type;
				$type = "distributor/";

			break;

			case 4 :

				$type = "retailer/";

			break;

			case 5 :

				$type = "salesman/";

			break;

		}


	}

	return $type;

}
//echo $type;


$page_name="Distributor Target List";
//$checkPromotionStatus = true;

if(isset($_REQUEST['show']) && $_REQUEST['show']=="yes"){
	$_objAdmin->showDistributorTarget();
	die;
}


/*  Description : On submit value distributor target added. */

     if(isset($_POST['submit']) && $_POST['submit']=='Submit')
     {
		 
		$auRetRec=$_objAdmin->_getSelectList('table_distributors_target',"*",''," target_month='".ltrim($_REQUEST['month'],0)."' and target_year='".$_REQUEST['year']."' and distributor_id='".$_REQUEST['distributor_id']."' and target_type='".$_REQUEST['target_type']."' ");
		
		if(sizeof($auRetRec)>0){
			
				$disTargetErr ="Target already exist for the distributor for selected month.";
			
			}
	else{
		 $_SESSION['dismonth']=$_POST['month'];
		 $_SESSION['disCyear']=$_POST['year'];
		 $_SESSION['TargetdisId']=$_POST['distributor_id'];
     	$disTargetSucc=$_objAdmin->distributorTargetAdd();
     	if(!empty($disTargetSucc)){$disTargetSucc ="Target successfully added."; 
			unset($_SESSION['dismonth']);
			unset($_SESSION['disCyear']);
			unset($_SESSION['TargetdisId']);
			
			}
	}
     }
     
     if(isset($_POST['submitupdate']) && $_POST['submitupdate']=='Submit')
     {
			$disTargetSucc=$_objAdmin->distributorTargetUpdate();
			if(!empty($disTargetSucc)){$disTargetSucc ="Target successfully updated."; }
	 
	 }


//Delete Salesman
if(isset($_REQUEST['stid']) && isset($_REQUEST['value']) && $_REQUEST['stid']!=""){
	
		if($_REQUEST['value'] == 'Active')
		$id=$_objAdmin->_dbUpdate(array("last_update_date"=>date('Y-m-d'),"last_update_status"=>'Update',"status"=>'I'),'table_distributors_target', " distributor_target_id='".$_REQUEST['stid']."'");
		else 
		$id=$_objAdmin->_dbUpdate(array("last_update_date"=>date('Y-m-d'),"last_update_status"=>'Update',"status"=>'A'),'table_distributors_target', " distributor_target_id='".$_REQUEST['stid']."'");

		$sus="Status updated successfully";
		header ("location: distributortarget.php");
}
include("import.inc.php");
include("header.inc.php");
?>
    <!-- Bootstrap core CSS -->
    <link href="bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Bootstrap theme -->
    <link href="bootstrap/dist/css/bootstrap-theme.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <!--<link href="bootstrap/css/theme.css" rel="stylesheet">-->

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <!--<script src="bootstrap/assets/js/ie-emulation-modes-warning.js"></script>-->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
<!-- start content-outer -->



    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
   <?php if($_REQUEST['action']!='') {?> 
   <script src="javascripts/jquery-1.11.2.min.js"></script>
   <?php } ?>
   <!--<script src="bootstrap/dist/js/bootstrap.min.js"></script>-->
     <!--<script src="bootstrap/assets/js/docs.min.js"></script>-->
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
   <!--<script src="bootstrap/assets/js/ie10-viewport-bug-workaround.js"></script>-->



 
<input name="pagename" type="hidden"  id="pagename" value="order.php" />
<div id="content-outer">
<!-- start content -->
<div id="content">
<div id="page-heading"><h1><span style="color: #d74343; font-family: Tahoma; font-weight: bold;">Distributor Target</span></h1></div>
<?php if($_REQUEST['err']!=''){?>
	<div id="message-red">
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
	<tr>
		<td class="red-left">Error. <?php echo "You have exceeded the maximum limit of active users"; ?></td>
		<td class="red-right"><a class="close-red"><img src="images/icon_close_red.gif"   alt="" /></a></td>
	</tr>
	</table>
	</div>
<?php } ?>
<?php if($disTargetSucc!=''){?>
	<div class="alert alert-success" role="alert">
        <strong><?php echo $disTargetSucc; ?></strong> 
      </div>
<?php } ?>
<?php if($disTargetErr!=''){?>
	<div id="message-red">
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
	<tr>
		<td class="red-left"><?php echo $disTargetErr; ?></td>
		<td class="red-right"><a class="close-red"><img src="images/icon_close_red.gif"   alt="" /></a></td>
	</tr>
	</table>
	</div>
<?php } ?>
<table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">
<tr>
	<td>
	<!--  start content-table-inner -->
	<div id="content-table-inner">
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
	<tr valign="top">
	<td>
		<!-- start id-form -->
		<?php
		if(basename($_SERVER['PHP_SELF']) == "distributortarget.php") { 
			$userType = checkUserType($_SESSION['userLoginType']);
			if(isset($_REQUEST['action'])) {

				

				

				//echo $userType;
				//echo "order/".$userType."action/add.php";

				switch ($_REQUEST['action']) {
					case add : 
						include("distributortarget/".$userType."action/add.php");
					break;

					case edit :
						include("distributortarget/".$userType."action/edit.php");
					break;
					
					case import:
						include("distributortarget/".$userType."action/import_dis_target.php");
					break;
					
					case export:
						include("distributortarget/".$userType."action/index.php");
					break;
					
					case delete:
						include("distributortarget/".$userType."action/index.php");
					break;

					default :
						include("distributortarget/".$userType."action/index.php");
					break;
				} 
			} else {	
				//echo $userType;			
				//echo "distributortarget/".$userType."action/index.php";
				include("distributortarget/".$userType."action/index.php");
			}
			
		} ?>
		<!-- end id-form  -->
	</td>
	<td>
	<!-- right bar-->
	<?php if($_REQUEST['action']==''){include("distributortarget/distributortargetbar.php");} ?>
	</td>
	</tr>
<tr>
</tr>
</table>
<div class="clear"></div>
</div>
<!--  end content-table-inner  -->
</td>
<td id="tbl-border-right"></td>
</tr>
</table>
<div class="clear">&nbsp;</div>
</div>
<!--  end content -->
<div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->
<div class="clear">&nbsp;</div> 
<!-- start footer -->         
<?php include("footer.php");?>
<!-- end footer -->
<!--<script src="javascripts/jquery-1.8.2.js" type="text/javascript"></script>
<script src="javascripts/jquery-ui.js"></script>
<script type="text/javascript" src="javascripts/validate.js"></script>-->
</body>
</html>
