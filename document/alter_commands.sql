ALTER TABLE `table_favorite_products` ADD `customer_id` INT( 11 ) NOT NULL AFTER `favorite_id` ,
ADD INDEX ( `customer_id` ) ;

ALTER TABLE `table_merchant_customer_relationship` ADD `created_date` DATETIME NOT NULL AFTER `customer_id` ;


--
-- Table structure for table `table_custom_order`
--

CREATE TABLE IF NOT EXISTS `table_custom_order` (
  `request_id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) NOT NULL,
  `merchant_id` int(11) NOT NULL,
  `request_desc` text COLLATE utf8_unicode_ci NOT NULL,
  `app_date` date NOT NULL,
  `app_time` time NOT NULL,
  `created_date` datetime NOT NULL,
  `status` char(2) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`request_id`),
  KEY `request_id` (`request_id`,`customer_id`,`merchant_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;



ALTER TABLE `table_favorite_products` ADD `catalogue_id` INT( 11 ) NOT NULL AFTER `merchant_id` ,
ADD INDEX ( `catalogue_id` ) ;

ALTER TABLE `table_favorite_products` ADD `favorite_type` CHAR( 2 ) NOT NULL COMMENT 'S-Store, C=Catalogue, P-Products' AFTER `datetime` ,
ADD INDEX ( `favorite_type` ) ;

ALTER TABLE `table_customer_profile` ADD `alternate_phone_no` VARCHAR( 30 ) NOT NULL AFTER `phone_number` ;













ALTER TABLE `table_merchants` ADD `password` VARCHAR( 255 ) NOT NULL AFTER `merchant_phone` ;
ALTER TABLE `table_merchants` ADD `business_tagline` VARCHAR( 255 ) NOT NULL AFTER `business_desc` ;
ALTER TABLE `table_merchants` ADD `business_in_years` INT( 11 ) NOT NULL AFTER `business_tagline` ;

ALTER TABLE `table_merchants` ADD `facebook_profile` VARCHAR( 255 ) NOT NULL AFTER `business_url` ,
ADD `google_profile` VARCHAR( 255 ) NOT NULL AFTER `facebook_profile` ,
ADD `twitter_profile` VARCHAR( 255 ) NOT NULL AFTER `google_profile` ,
ADD `linkedin_profile` VARCHAR( 255 ) NOT NULL AFTER `twitter_profile` ,
ADD `pinterest_profile` VARCHAR( 255 ) NOT NULL AFTER `linkedin_profile` ;

ALTER TABLE `table_offers` ADD `offer_code` VARCHAR( 255 ) NOT NULL AFTER `offer_title` ;









# Added by AJAY@2016-05-05 used for offline orders.
ALTER TABLE `table_order` ADD `customer_name` VARCHAR( 200 ) NOT NULL AFTER `customer_id` ,
ADD `customer_number` VARCHAR( 20 ) NOT NULL AFTER `customer_name` ;



#by Maninder on 26 May 2016
ALTER TABLE `table_merchants` ADD `banner_img` VARCHAR( 255 ) NOT NULL AFTER `app_logo` ;

#by Maninder kumar on 30th May 2016
ALTER TABLE  `table_order_details` ADD  `order_desc` TEXT NOT NULL AFTER  `actual_price_total` ,
ADD  `order_image` VARCHAR( 255 ) NOT NULL AFTER  `order_desc` ;




#by Maninder kumar on 5th July 2016
ALTER TABLE  `table_customer` ADD  `password` VARCHAR( 100 ) NOT NULL AFTER  `username` ;

ALTER TABLE  `table_offers` ADD  `offer_image2` VARCHAR( 255 ) NOT NULL AFTER  `offer_image` ,
ADD  `offer_image3` VARCHAR( 255 ) NOT NULL AFTER  `offer_image2` ;

ALTER TABLE  `table_manage_contacts` ADD  `user_status` VARCHAR( 255 ) NOT NULL AFTER  `app_user` ;

ALTER TABLE  `table_manage_contacts` ADD  `customer_id` INT NOT NULL AFTER  `merchant_id` ,
ADD INDEX (  `customer_id` ) ;

#by Vijay Sharma on 14th July 2016
ALTER TABLE  `table_offers` ADD  `offer_discount` VARCHAR( 200 ) NOT NULL ;
ALTER TABLE  `table_order_details` ADD  `offer_discount` VARCHAR( 200 ) NOT NULL ;
ALTER TABLE  `table_order` ADD  `offer_discount` VARCHAR( 200 ) NOT NULL ;


# by vijay sharma on 19th july,2016
ALTER TABLE  `table_offers` ADD  `catalogue_id` INT( 20 ) NOT NULL ;