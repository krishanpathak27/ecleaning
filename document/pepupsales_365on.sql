-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 17, 2016 at 05:13 PM
-- Server version: 5.5.44-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `pepupsales_365on`
--

-- --------------------------------------------------------

--
-- Table structure for table `table_banner`
--

CREATE TABLE IF NOT EXISTS `table_banner` (
  `banner_id` int(11) NOT NULL AUTO_INCREMENT,
  `merchant_id` int(11) NOT NULL,
  `offer_id` int(11) NOT NULL,
  `banner_text` varchar(255) NOT NULL,
  `banner_desc` varchar(255) NOT NULL,
  `banner_image` varchar(150) NOT NULL,
  `put_on_home` int(11) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `view_order` int(11) NOT NULL,
  `user_type` varchar(10) NOT NULL,
  `created_date` datetime NOT NULL,
  `last_update_date` datetime NOT NULL,
  `status` char(2) NOT NULL,
  PRIMARY KEY (`banner_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `table_catalogue`
--

CREATE TABLE IF NOT EXISTS `table_catalogue` (
  `catalogue_id` int(11) NOT NULL AUTO_INCREMENT,
  `merchant_id` int(11) NOT NULL,
  `catalogue_name` varchar(150) NOT NULL,
  `catalogue_desc` varchar(255) NOT NULL,
  `catalogue_image` varchar(150) NOT NULL,
  `created_date` datetime NOT NULL,
  `last_update_date` datetime NOT NULL,
  `status` char(2) NOT NULL,
  PRIMARY KEY (`catalogue_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `table_category`
--

CREATE TABLE IF NOT EXISTS `table_category` (
  `category_id` int(11) NOT NULL AUTO_INCREMENT,
  `catalouge_id` int(11) NOT NULL,
  `category_name` varchar(255) NOT NULL,
  `category_code` varchar(100) NOT NULL,
  `created_date` datetime NOT NULL,
  `last_update_date` datetime NOT NULL,
  `status` char(2) NOT NULL,
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `table_customer`
--

CREATE TABLE IF NOT EXISTS `table_customer` (
  `customer_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(100) NOT NULL,
  `register_date` date NOT NULL,
  `register_time` time NOT NULL,
  `register_lat` varchar(80) NOT NULL,
  `register_lng` varchar(80) NOT NULL,
  `auth_token` varchar(255) NOT NULL,
  `gcm_registration_id` text NOT NULL,
  `auth_otp` varchar(20) NOT NULL,
  `created_date` datetime NOT NULL,
  `last_update_date` datetime NOT NULL,
  `status` char(2) NOT NULL,
  PRIMARY KEY (`customer_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `table_customer_profile`
--

CREATE TABLE IF NOT EXISTS `table_customer_profile` (
  `profile_id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) NOT NULL,
  `display_name` varchar(150) NOT NULL,
  `provider_uid` int(11) NOT NULL,
  `first_name` varchar(150) NOT NULL,
  `last_name` varchar(150) NOT NULL,
  `email_address` varchar(255) NOT NULL,
  `phone_number` varchar(30) NOT NULL,
  `landline` varchar(20) NOT NULL,
  `social_profile_id` varchar(255) NOT NULL,
  `profile_url` varchar(150) NOT NULL,
  `dob` date NOT NULL,
  `gender` varchar(10) NOT NULL,
  `country_id` int(11) NOT NULL,
  `state_id` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  `address` varchar(255) NOT NULL,
  `address2` varchar(255) NOT NULL,
  `zipcode` varchar(20) NOT NULL,
  `photo_url` varchar(150) NOT NULL,
  `provider` varchar(50) NOT NULL,
  `status` char(2) NOT NULL,
  PRIMARY KEY (`profile_id`),
  KEY `customer_id` (`customer_id`,`country_id`,`state_id`,`city_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `table_expenses_revenue`
--

CREATE TABLE IF NOT EXISTS `table_expenses_revenue` (
  `financial_id` int(11) NOT NULL AUTO_INCREMENT,
  `merchant_id` int(11) NOT NULL,
  `financial_desc` varchar(255) NOT NULL,
  `amount` float(10,7) NOT NULL,
  `app_date` date NOT NULL,
  `app_time` time NOT NULL,
  `lat` int(150) NOT NULL,
  `lng` varchar(150) NOT NULL,
  `financial_type` varchar(20) NOT NULL,
  `created_date` datetime NOT NULL,
  `last_update_date` datetime NOT NULL,
  `status` char(2) NOT NULL,
  PRIMARY KEY (`financial_id`),
  KEY `merchant_id` (`merchant_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `table_favorite_products`
--

CREATE TABLE IF NOT EXISTS `table_favorite_products` (
  `favorite_id` int(11) NOT NULL AUTO_INCREMENT,
  `merchant_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `interested` varchar(10) NOT NULL,
  `app_date` date NOT NULL,
  `app_time` time NOT NULL,
  `datetime` datetime NOT NULL,
  `status` char(2) NOT NULL,
  PRIMARY KEY (`favorite_id`),
  KEY `merchant_id` (`merchant_id`,`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `table_image`
--

CREATE TABLE IF NOT EXISTS `table_image` (
  `image_id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) NOT NULL,
  `ref_id` int(11) NOT NULL,
  `ref_type` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL,
  `image_url` varchar(150) NOT NULL,
  `lat` varchar(100) NOT NULL,
  `lng` varchar(100) NOT NULL,
  `status` char(2) NOT NULL,
  PRIMARY KEY (`image_id`),
  KEY `customer_id` (`customer_id`,`tag_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `table_merchants`
--

CREATE TABLE IF NOT EXISTS `table_merchants` (
  `merchant_id` int(11) NOT NULL AUTO_INCREMENT,
  `merchant_name` varchar(255) NOT NULL,
  `merchant_email` varchar(100) NOT NULL,
  `username` varchar(50) NOT NULL,
  `business_logo` varchar(50) NOT NULL,
  `app_logo` varchar(50) NOT NULL,
  `business_type` varchar(50) NOT NULL,
  `business_name` varchar(150) NOT NULL,
  `business_desc` varchar(255) NOT NULL,
  `country_id` int(11) NOT NULL,
  `state_id` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  `business_zipcode` varchar(20) NOT NULL,
  `business_address` varchar(255) NOT NULL,
  `business_phone` varchar(20) NOT NULL,
  `business_landline` varchar(20) NOT NULL,
  `business_email` varchar(100) NOT NULL,
  `business_url` varchar(255) NOT NULL,
  `register_date` date NOT NULL,
  `register_time` time NOT NULL,
  `register_lat` varchar(20) NOT NULL,
  `register_lng` varchar(20) NOT NULL,
  `auth_token` varchar(255) NOT NULL,
  `gcm_registration_id` text NOT NULL,
  `auth_otp` varchar(20) NOT NULL,
  `created_date` datetime NOT NULL,
  `last_update_date` datetime NOT NULL,
  `status` char(2) NOT NULL,
  PRIMARY KEY (`merchant_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `table_merchant_customer_relationship`
--

CREATE TABLE IF NOT EXISTS `table_merchant_customer_relationship` (
  `relation_id` int(11) NOT NULL AUTO_INCREMENT,
  `merchant_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `status` char(2) NOT NULL,
  PRIMARY KEY (`relation_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `table_merchant_ref_images`
--

CREATE TABLE IF NOT EXISTS `table_merchant_ref_images` (
  `ref_image_id` int(11) NOT NULL AUTO_INCREMENT,
  `merchant_id` int(11) NOT NULL,
  `image_title` varchar(150) NOT NULL,
  `image_desc` varchar(255) NOT NULL,
  `image_url` varchar(100) NOT NULL,
  `last_update_date` datetime NOT NULL,
  `status` char(2) NOT NULL,
  PRIMARY KEY (`ref_image_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `table_offers`
--

CREATE TABLE IF NOT EXISTS `table_offers` (
  `offer_id` int(11) NOT NULL AUTO_INCREMENT,
  `merchant_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `offer_title` varchar(255) NOT NULL,
  `offer_desc` varchar(255) NOT NULL,
  `offer_thumb` varchar(150) NOT NULL,
  `offer_image` varchar(150) NOT NULL,
  `offer_start_date` date NOT NULL,
  `offer_end_date` date NOT NULL,
  `offer_action` varchar(20) NOT NULL,
  `created_date` datetime NOT NULL,
  `last_update_date` datetime NOT NULL,
  `status` char(2) NOT NULL,
  PRIMARY KEY (`offer_id`),
  KEY `merchant_id` (`merchant_id`,`product_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `table_order`
--

CREATE TABLE IF NOT EXISTS `table_order` (
  `order_id` int(11) NOT NULL AUTO_INCREMENT,
  `merchant_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `date_of_order` date NOT NULL,
  `time_of_order` time NOT NULL,
  `expected_delivery_date` date NOT NULL,
  `actual_delivery_date` date NOT NULL,
  `order_lat` varchar(50) NOT NULL,
  `order_lng` varchar(50) NOT NULL,
  `network_type` varchar(50) NOT NULL,
  `created_date` datetime NOT NULL,
  `last_update_date` datetime NOT NULL,
  `order_status_id` int(11) NOT NULL,
  `status` char(2) NOT NULL,
  PRIMARY KEY (`order_id`),
  KEY `merchant_id` (`merchant_id`,`customer_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `table_order_details`
--

CREATE TABLE IF NOT EXISTS `table_order_details` (
  `order_detail_id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_qty` int(11) NOT NULL,
  `product_actual_qty` int(11) NOT NULL,
  `product_price` float(10,5) NOT NULL,
  `price_total` float(10,8) NOT NULL,
  `actual_price_total` float(10,5) NOT NULL,
  `offer_id` int(11) NOT NULL,
  `offer_title` varchar(255) NOT NULL,
  `last_update_date` date NOT NULL,
  `order_detail_status` char(2) NOT NULL,
  `status` char(2) NOT NULL,
  PRIMARY KEY (`order_detail_id`),
  KEY `order_id` (`order_id`,`product_id`,`offer_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `table_order_status`
--

CREATE TABLE IF NOT EXISTS `table_order_status` (
  `status_id` int(11) NOT NULL AUTO_INCREMENT,
  `order_status_id` int(11) NOT NULL,
  `status_title` varchar(255) NOT NULL,
  `status_desc` varchar(255) NOT NULL,
  `status_image` varchar(255) NOT NULL,
  `status` char(2) NOT NULL,
  PRIMARY KEY (`status_id`),
  KEY `order_status_id` (`order_status_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `table_product`
--

CREATE TABLE IF NOT EXISTS `table_product` (
  `product_id` int(11) NOT NULL AUTO_INCREMENT,
  `catalogue_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `product_code` varchar(150) NOT NULL,
  `product_desc` varchar(255) NOT NULL,
  `product_price` float(10,2) NOT NULL,
  `created_date` datetime NOT NULL,
  `last_update_date` datetime NOT NULL,
  `status` char(2) NOT NULL,
  PRIMARY KEY (`product_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `table_product_images`
--

CREATE TABLE IF NOT EXISTS `table_product_images` (
  `product_img_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `product_thumb` varchar(255) NOT NULL,
  `product_image` varchar(255) NOT NULL,
  `created_date` datetime NOT NULL,
  `last_update_date` datetime NOT NULL,
  `status` char(2) NOT NULL,
  PRIMARY KEY (`product_img_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
