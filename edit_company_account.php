<?php
include("includes/config.inc.php");
include("includes/function.php");
$page_name="Company Account";
$_objAdmin = new Admin();
$_objItem = new Item();

if(isset($_POST['add']) && $_POST['add'] == 'yes')
{
	if($_REQUEST['id']!="") 
		{
			$no_of_users = $_POST['no_of_employees'];
			$existing_users = $_objAdmin->getTotalUsersOfCompany($_REQUEST['id']);
			
			if($no_of_users < $existing_users)
				{ 
				 $_objAdmin->updateCompanyAccount($_REQUEST['id']); 
				 $userstoinactive = $existing_users - $no_of_users; 
  				  
				 $auRec=$_objAdmin->_getSelectList2('table_web_users',"web_user_id AS wid,user_type AS utype, distributor_id AS did, retailer_id AS rid, salesman_id AS sid",''," status = 'A' and user_type NOT IN (0,1) and account_id=".$_REQUEST['id']." LIMIT ".$userstoinactive."");
				 
				 $data['status'] = 'I';
				 for($i=0; $i<count($auRec); $i++){
					$_objAdmin->_dbUpdate2($data,'table_web_users'," web_user_id =".$auRec[$i]->wid."");
					switch ($auRec[$i]->utype) {
						case 3:
							$data['status'] = 'I';
							$_objAdmin->_dbUpdate2($data,'table_distributors'," distributor_id =".$auRec[$i]->did."");
							break;
						case 4:
							$data['status'] = 'I';
							$_objAdmin->_dbUpdate2($data,'table_retailer'," retailer_id =".$auRec[$i]->rid."");
							break;
						case 5:
							$data['status'] = 'I';
							$_objAdmin->_dbUpdate2($data,'table_salesman'," salesman_id =".$auRec[$i]->sid."");
							break;
						}
				 }
		
				  header("Location: edit_company_account.php?id=".$_REQUEST['id']."&sus=sus");
				}
			else{
					$id=$_objAdmin->updateCompanyAccount($_REQUEST['id']);
					header("Location: edit_company_account.php?id=".$_REQUEST['id']."&sus=sus");
				}
		}
	
}
if($_REQUEST['sus']=='sus')
{
	$sus="Account has been updated successfully.";
	header("Location: company_login_account.php?id=".$_REQUEST['id']."");
}


if(isset($_POST['comp_login']) && $_POST['comp_login']=='yes'){
	if($_POST['web_id']!="") {
	$condi=	" username='".$_POST['username']."' and web_user_id<>'".$_POST['web_id']."'";
	}else{
	$condi=	" username='".$_POST['username']."'";
	}
	$auRec=$_objAdmin->_getSelectList('table_web_users',"*",'',$condi);
	if(is_array($auRec)){
		$login_err="User name already exists in the system.";
		$_SESSION['CompId']=$_POST['comp_acc_id'];
		$auRec[0]=(object)$_POST;						
		}else{
			if($_POST['web_id']!='') {
				
			     $condition = "account_id='".$_POST['account_id']."'";
			    $companydata=$_objAdmin->_getSelectList2('table_account','*','',$condition);
				
				$start_date=$companydata[0]->start_date;
				$end_date=$companydata[0]->end_date;	
			$_objAdmin->UpdateLogin($_POST["web_id"],$start_date,$end_date);
			$login_sus="Account has been Updated successfully.";
			$_SESSION['CompId']=$_POST['comp_acc_id'];
			} else {
			if(count($user_exit)>0)
				{
				$_SESSION['user_exit']="User Already Exits";
				header('location:company_login_account.php?id='.$_REQUEST['id']);	
				}
				else{
				$condition = "account_id='".$_POST['account_id']."'";
			    $companydata=$_objAdmin->_getSelectList2('table_account','*','',$condition);
				$start_date=$companydata[0]->start_date;
				$end_date=$companydata[0]->end_date;
				
			    $web_id=$_objAdmin->addLogin($start_date,$end_date);
				if($web_id!=''){
				$login_sus="Account has been Added successfully.";
				
				}
			}
		  }
		}
	}


if(isset($_REQUEST['id']) && $_REQUEST['id']!="")
{
	$auRec=$_objAdmin->_getSelectList2('table_account',"*",''," account_id='".$_REQUEST['id']."'");
	
	if(count($auRec)<=0) header("Location: manage_account.php");
}
?>

<?php include("header.inc.php") ?>
<link rel="stylesheet" href="./base/jquery.ui.all.css">
<script src="./Autocomplete/jquery-1.5.1.js"></script>
<script src="./Autocomplete/jquery.ui.core.js"></script>
<script src="./Autocomplete/jquery.ui.widget.js"></script>
<script src="./Autocomplete/jquery.ui.button.js"></script>
<script src="./Autocomplete/jquery.ui.position.js"></script>
<script src="./Autocomplete/jquery.ui.menu.js"></script>
<script src="./Autocomplete/jquery.ui.autocomplete.js"></script>
<script src="./Autocomplete/jquery.ui.tooltip.js"></script>
<script src="./Autocomplete/jquery.ui.datepicker.js"></script>
<link rel="stylesheet" href="./base/demos.css">
<script type="text/javascript" src="./javascripts/validate.js"></script>


<script>
    $(function() {
        $( "#from" ).datepicker({
			dateFormat: "d M yy",
            defaultDate: "+1w",
            changeMonth: true,
            numberOfMonths: 3,
            onSelect: function( selectedDate ) {
                $( "#to" ).datepicker( "option", "minDate", selectedDate );
            }
        });
        $( "#to" ).datepicker({
			dateFormat: "d M yy",
            defaultDate: "+1w",
            changeMonth: true,
            numberOfMonths: 3,
            onSelect: function( selectedDate ) {
                $( "#from" ).datepicker( "option", "maxDate", selectedDate );
            }
        });
    });
	
	
	function showStateCity(str,id)
		{
		/*alert(str);
		alert(id);*/
			$.ajax({
				'type': 'POST',
				'url': 'dropdown_city.php',
				'data': 's='+str+'&c='+id,
				'success' : function(mystring) {
				document.getElementById("outputcity").innerHTML = mystring;
				$('#selectCity').show();
				}
			});
		}
</script>

	


<div class="clear"></div>
<!-- start content-outer -->
<div id="content-outer">
<!-- start content -->
	<div id="content">
	<div id="page-heading"><h1><span style="color: #d74343; font-weight: bold;">Company Account</span></h1></div>
		<table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">
		<tr>
			<td>
			<!--  start content-table-inner -->
			<div id="content-table-inner">
				<table border="0" width="100%" cellpadding="0" cellspacing="0">
				<tr valign="top">
					<td>
					<!--  start message-red -->
					<?php if($login_err!=''){?>
					<div id="message-red">
						<table border="0" width="100%" cellpadding="0" cellspacing="0">
						<tr>
							<td class="red-left"> <?php echo $login_err; ?></td>
							<td class="red-right"><a class="close-red"><img src="images/icon_close_red.gif"   alt="" /></a></td>
						</tr>
						</table>
					</div>
					<?php } ?>
					<!--  end message-red -->
					<?php if($login_sus!=''){?>

					<!--  start message-green -->
					<div id="message-green">
						<table border="0" width="100%" cellpadding="0" cellspacing="0">
						<tr>
							<td class="green-left"><?php echo $login_sus; ?></td>
							<td class="green-right"><a class="close-green"><img src="images/icon_close_green.gif"   alt="" /></a></td>
						</tr>
						</table>
					</div>
					<?php } ?>
					<!--  end message-green -->
					
			<!-- start id-form -->
		<form name="frmPre" id="frmPre" method="post" action="<?php $_SERVER['PHP_SELF'] ?>" enctype="multipart/form-data" >
			<table border="0" cellpadding="0" cellspacing="0" width="800" id="id-form">
			<tr>
				<th valign="top" >Company Name:</th>
				<td width="200px"><input type="text" name="company_name" id="company_name"  class="required" value="<?php echo $auRec[0]->company_name; ?>" maxlength="255" placeholder="Company Name"/></td>
				<td width="500px"></td>
			</tr>
			<tr>
				<th valign="top">Company Address:</th>
				<td colspan="2">
			<input type="text" style="width:590px;" name="company_address" id="company_address" class="text" value="<?php echo $auRec[0]->company_address; ?>" placeholder="Address Line"/>
			</td>
			</tr>
			<tr>
				<th valign="top" >Company Phone No.</th>
				<td >
					<input type="text" name="company_phone_no" id="company_phone_no" class="required number" maxlength="15" value="<?php echo $auRec[0]->company_phone_no; ?>" placeholder="Phone Number"/>
				</td>
				<td ></td>
			</tr>
			<tr>
				<th valign="top" >No. Of Users</th>
				<td >
					<input type="text" name="no_of_employees" id="no_of_employees" class="required number" maxlength="5" value="<?php echo $auRec[0]->no_of_employees; ?>" placeholder="No. Of Users"/>
				</td>
				<td ></td>
			</tr>
			
			
		<tr>
		<?php 
			$pageHierarchyID=$_objAdmin->_getSelectList2('table_module_enable',"*",''," account_id='".$_REQUEST['id']."' and feature_id='1'");
				$Hierarchy_enable=$pageHierarchyID[0]->status;		
		?>
			
			<th valign="top">Sales Hierarchy :</th>
			<td><input type="radio" name="hierarchy_enable" value="1" <?php if($Hierarchy_enable=='1'){ ?> checked <?php } ?> > Yes</td>
			<td><input type="radio" name="hierarchy_enable" value="0" <?php if($Hierarchy_enable=='0'){ ?> checked <?php } ?> > No</td>
		</tr>
		<tr>
		<?php 
				$pageAccessID=$_objAdmin->_getSelectList2('table_module_enable',"*",''," account_id='".$_REQUEST['id']."' and feature_id='2'");
				$access_enable=$pageAccessID[0]->status;	
			
			?>
			<th valign="top">Access Management</th>
			<td><input type="radio" name="access_enable" value="1" <?php if($access_enable=='1'){ ?> checked <?php } ?> > Yes</td>
			<td><input type="radio" name="access_enable" value="0" <?php if($access_enable=='0'){ ?> checked <?php } ?> > No</td>
		</tr>
		<tr>
			<th valign="top">Country:</th>
			<td>
			<select name="country" id="country" class="styledselect_form_3">
			<option value="india">India</option>
			</select>
			</td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<th valign="top">Select State:</th>
			<td>
			<select name="state" id="<?php echo $auRec[0]->city; ?>" class="styledselect_form_3" onChange="showStateCity(this.value,id)" >
			<?php 
			$auUsr=$_objAdmin->_getSelectList2('state','state_id,state_name',''," ORDER BY state_name"); 
			if(is_array($auUsr)){ ?>
			<?php
			for($i=0;$i<count($auUsr);$i++){?>
			<option value="<?php echo $auUsr[$i]->state_id;?>" <?php if ($auUsr[$i]->state_id==$auRec[0]->state){ ?> selected <?php } ?> ><?php echo $auUsr[$i]->state_name;?></option>
			<?php }}?>
			</select>
			</td>
		</tr>
		<tr id="selectCity" style="display:none;">
			<th valign="top">Select City:</th>
			<td>
				<div id="outputcity"><div>
			</td>
		</tr>
			<tr>
				<th valign="top">Start Date:</th>
				<td><input type="text" name="from" id="from" class="date required" value="<?php echo $_objAdmin->_changeDate($auRec[0]->start_date); ?>" readonly /></td>
				<td></td>
			</tr>
			<tr>
				<th valign="top">End Date:</th>
				<td><input type="text" id="to" name="to" value="<?php echo $_objAdmin->_changeDate($auRec[0]->end_date); ?>" class="date required" readonly /></td>
				<td></td>
			</tr>
			<tr>
				<th valign="top">Status:</th>
				<td>
				<select name="status" id="status" class="styledselect_form_3">
				<option value="A" <?php if($auRec[0]->status=='A') echo "selected";?> >Active</option>
				<option value="I" <?php if($auRec[0]->status=='I') echo "selected";?> >Inactive</option>
				</select>
				</td>
				<td></td>
			</tr>
				
			<tr>
				<th>&nbsp;</th>
				<td valign="top">
					<!--<input type="hidden" name="target_incentive_type" value="1"  />
					<input type="hidden" name="qualifiers_type" value="1"  />-->
					<input name="add" type="hidden" value="yes" />
					<input type="reset" value="Reset!" class="form-reset">
					<input type="button" value="Back" class="form-reset" onclick="location.href='manage_account.php';" />
					<input name="submit" class="form-submit" type="submit" id="submit" value="Save"  />
				</td>
			</tr>
			</table>
		</form>
			<!-- end id-form  -->
			</td>
			<td>
			<!-- right bar-->
			<?php //include("rightbar/item_bar.php") ?>
			<div class="clear"></div>
		</div>
<!-- end related-act-bottom -->
</div>
<!-- end related-activities -->
</td>
</tr>
<tr>
<td><img src="images/blank.gif" width="695" height="1" alt="blank" /></td>
<td></td>
</tr>
</table>
<div class="clear"></div>
</div>
<!--  end content-table-inner  -->
</td>
<td id="tbl-border-right"></td>
</tr>
<!--<tr>
	<th class="sized bottomleft"></th>
	<td id="tbl-border-bottom">&nbsp;</td>
	<th class="sized bottomright"></th>
</tr>-->
</table>
<div class="clear">&nbsp;</div>

</div>
<!--  end content -->
<div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->
<div class="clear">&nbsp;</div>
    
<!-- start footer --> 
<?php include("footer.php") ?>  
<!-- end footer -->
</body>
</html>