<?php
include("includes/config.inc.php");
include("includes/function.php");
$page_name="Edit Secondary Scheme";
$_objAdmin = new Admin();
$objArrayList= new ArrayList();


if(isset($_POST['add']) && $_POST['add'] == 'yes')
{

	if($_REQUEST['id']!="") 
		{

			// echo "<pre>";
		// print_r($_REQUEST);
		// exit;

			$id=$_objAdmin->updateScheme($_REQUEST['id']);
			
			header("Location: edit_secondary_scheme.php?id=".$_REQUEST['id']."&sus=sus");
			
		}
}
if($_REQUEST['sus']=='sus'){
			$sus="Secondary Scheme has been updated successfully.";
			}

if(isset($_REQUEST['id']) && $_REQUEST['id']!="")
{
	$auRec=$_objAdmin->_getSelectList('table_discount as d 
		LEFT JOIN table_discount_detail as dd on d.discount_id=dd.discount_id 
		LEFT JOIN table_discount_item as dt on dt.discount_id=d.discount_id
		LEFT JOIN table_discount_party AS tdp ON tdp.discount_id = d.discount_id 
		LEFT JOIN table_category as c on dt.category_id=c.category_id 
		LEFT JOIN table_item as t on t.item_id=dt.item_id 
		LEFT JOIN table_brands AS B ON B.brand_id = dt.brand_id',"d.discount_id,d.discount,d.is_open,d.is_dis_discount,d.mode,d.item_type,d.party_type,d.start_date,d.end_date,d.status,dd.discount_desc,dd.discount_type,dd.foc_id,dd.minimum_quantity,dd.discount_percentage,dd.discount_amount,dd.minimum_amount,c.category_name,t.item_name,dt.item_id, B.brand_name, d.slabwise_scheme, d.division_id, GROUP_CONCAT(tdp.retailer_id) AS all_retailerId",''," d.discount_id=".$_REQUEST['id']." GROUP BY discount_id");

	if(count($auRec)<=0) {

		
		header("Location: secondary_scheme.php");
	}else{
		 $retId	=$auRec[0]->all_retailerId; 
		 $retailer_id = explode(",", $retId);
	}	
}

// echo "<pre>";
// print_r($auRec);
// exit;



?>
<?php include("header.inc.php");
$pageAccess=1;
$check=$objArrayList->checkAccess($pageAccess, basename($_SERVER['PHP_SELF']));
if($check == false){
header('Location: ' . $_SERVER['HTTP_REFERER']);}
 ?>
 <script src="javascripts/jquery-1.8.2.js" type="text/javascript"></script>
<script src="javascripts/jquery-ui.js"></script>
<script src="javascripts/discount.js"></script>
<script src="javascripts/addrow.js"></script>

<!--  end content-outer -->
<div class="clear">&nbsp;</div>

<script type="text/javascript">
	function checkall(el){
		var ip = document.getElementsByTagName('input'), i = ip.length - 1;
		for (i; i > -1; --i){
			if(ip[i].type && ip[i].type.toLowerCase() === 'checkbox'){
				ip[i].checked = el.checked;
			}
		}
	}
</script>

<script>

   $(function() {
        $( "#from" ).datepicker({
			dateFormat: "d M yy",
            defaultDate: "w",
            changeMonth: true,
            numberOfMonths: 1,
            onSelect: function( selectedDate ) {
                $( "#to" ).datepicker( "option", "minDate", selectedDate );
            }
        });
        $( "#to" ).datepicker({
			dateFormat: "d M yy",
            defaultDate: "-w",
            changeMonth: true,
            numberOfMonths: 1,
            onSelect: function( selectedDate ) {
                $( "#from" ).datepicker( "option", "maxDate", selectedDate );
            }
        });
    });      
</script>
	<style>
	.ui-button {
		margin-left: -1px;
	}
	.ui-button-icon-only .ui-button-text {
		padding: 0.35em;
	} 
	.ui-autocomplete-input {
		margin: 0;
		padding: 0.4em 0 0.4em 0.45em;
	}
	</style>

<?php // include("discount_list.php") ?>

<script type="text/javascript">
$(document).ready(function() {
	
	var v = $("#frmPre").validate({
			rules: {
				fileToUpload: {
				  required: true,
				  accept: "csv"
				}
			  },
			submitHandler: function(form) {
				document.frmPre.submit();				
			}
		});
});

</script>
<script type="text/javascript">
$(document).ready(function(){

	$('#loader').show();
	$('#loader').html('<div id="loader" align="center"><img src="images/ajax-loader.gif" /><br/>Please Wait...</div>');
    $.ajax({
		'type': 'POST',
		'url': 'discount_date.php',
		'data': '',
		'success' : function(mystring) {
			//alert(mystring);
		//document.getElementById("div1").innerHTML = mystring;
		$('#div1').html(mystring);
		$('#loader').hide();
		}
	});
	
});
</script>
<style type="text/css">
#StateDiv, #CityDiv, #ClassDiv {
    clear: none;
    height: 170px;
    margin: 0;
    overflow: scroll;
    padding: 0;
    width: 160px;
}

#MarketDiv, #RetailerDiv, #DistributorDiv{
    clear: none;
    height: 170px;
    margin: 0;
    overflow: scroll;
    padding: 0;
    width: 200px;
}

label {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	font-weight: normal;
}

.optlegend {
    background: none repeat scroll 0 0 #6E6E6E;
    border-radius: 0.2em 0.2em 0.2em 0.2em;
    color: #FFFFFF;
    font-family: arial;
    font-size: 17px;
    font-weight: normal;
    padding: 3px;

}
</style>
<div class="clear"></div>
<!-- start content-outer -->
<div id="content-outer">
<!-- start content -->
	<div id="content">
	<div id="page-heading"><h1><span style="color: #d74343; font-family: Tahoma; font-weight: bold;">Edit Secondary Scheme</span></h1></div>
		<table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">
		<tr>
			<th rowspan="3" class="sized"><img src="images/side_shadowleft.jpg" width="20" height="300" alt="" /></th>
			<th class="topleft"></th>
			<td id="tbl-border-top">&nbsp;</td>
			<th class="topright"></th>
			<th rowspan="3" class="sized"><img src="images/side_shadowright.jpg" width="20" height="300" alt="" /></th>
		</tr>
		<tr>
			<!--<td id="tbl-border-left"></td>-->
			<td>
			<!--  start content-table-inner -->
			<div id="content-table-inner">
				<table border="0" width="100%" cellpadding="0" cellspacing="0">
				<tr valign="top">
					<td>
					<!--  start message-red -->
					<?php if($err!=''){?>
					<div id="message-red">
						<table border="0" width="100%" cellpadding="0" cellspacing="0">
						<tr>
							<td class="red-left"> <?php echo $err; ?></td>
							<td class="red-right"><a class="close-red"><img src="images/icon_close_red.gif"   alt="" /></a></td>
						</tr>
						</table>
					</div>
					<?php } ?>
					<!--  end message-red -->
					<?php if($sus!=''){?>
					<!--  start message-green -->
					<div id="message-green">
						<table border="0" width="100%" cellpadding="0" cellspacing="0">
						<tr>
							<td class="green-left"><?php echo $sus; ?></td>
							<td class="green-right"><a class="close-green"><img src="images/icon_close_green.gif"   alt="" /></a></td>
						</tr>
						</table>
					</div>
					<?php } ?>
					<!--  end message-green -->
					
			<!-- start id-form -->
		<form name="frmPre" id="frmPre" method="post" action="edit_secondary_scheme.php" enctype="multipart/form-data" >
			<table border="0" cellpadding="0" cellspacing="0"  id="id-form">
			<tr>
				<th valign="top">Scheme Description:</th>
				<td><input type="text" name="dis_desc" id="dis_desc" class="required" value="<?php echo $auRec[0]->discount_desc; ?>" maxlength="255"/></td>
				<td></td>
			</tr>			
			<tr>
				<th valign="top">Scheme:</th>
				<td>
				<?php if($auRec[0]->discount==2) {?>
				
				<input type="radio" name="discount" value="2" checked="checked"> Non Exclusive &nbsp;
				<?php } ?>
				</td>
				<td></td>
			</tr>
			<tr>
				<th valign="top">Primary Scheme:</th>
				<td>
				<?php if($auRec[0]->is_dis_discount==2) {?>
				
				<input type="radio" name="dis_discount" value="2" checked="checked"> No &nbsp;
				<?php } ?>
				</td>
				<td></td>
			</tr>


			<tr>
				<th valign="top">Scheme Mode:</th>
				<td>
				<select name="" id="" class="styledselect_form_3" >
					<option value="<?php echo $auRec[0]->mode; ?>"><?php if($auRec[0]->mode==1){ echo "Quantity";} ?></option>
				</select>	
				</td>
				<td></td>
			</tr>


			<?php if($auRec[0]->mode==1){ ?>
			<tr>
				<th valign="top">Item Type:</th>
				<td>
				<select name="" id="" class="styledselect_form_3" >
					<option value="<?php echo $auRec[0]->item_type; ?>">
					<?php 
					if($auRec[0]->item_type==1){ echo "All";}
					if($auRec[0]->item_type==2){ echo "Category";}
					if($auRec[0]->item_type==3){ echo "Items";}
					
					?>
					</option>
				</select>	
				</td>
			</tr>
			<?php if($auRec[0]->item_type==2){ ?>
			<tr>
				<th valign="top">Select Category:</th>
				<td><select name="" id="" class="styledselect_form_3" >
					<option value="<?php echo $auRec[0]->category_name; ?>"><?php echo $auRec[0]->category_name; ?></option>
					</select>
				</td>
				<td></td>
			</tr>
			<?php } ?>
			<?php if($auRec[0]->item_type==3){ ?>
			<tr>
				<th valign="top">Select Item:</th>
				<td><select name="" id="" class="styledselect_form_3" >
					<option value="<?php echo $auRec[0]->item_name; ?>"><?php echo $auRec[0]->item_name; ?></option>
					</select>
				</td>
				<td></td>
			</tr>
			<?php } ?>



			<tr ><th>Scheme Details:</th>
				<th valign="top">Minimum Quantity:<br><input type="text" name="min_qty" id="min_qty" class="text number" maxlength="10" value="<?php echo $auRec[0]->minimum_quantity; ?>"  /></th>

				<th valign="top">Scheme Type:
				<select name="" id="" class="styledselect_form_3" >
					<option value="<?php echo $auRec[0]->discount_type; ?>">
					<?php 
					if($auRec[0]->discount_type==1){ echo "Percentage";}
					if($auRec[0]->discount_type==2){ echo "Amount";}
					
					
					?>
					</option>
				</select>		
				</th>


				<?php if($auRec[0]->discount_type==1){ ?>
					<th valign="top">Scheme Percentage:<br><input type="text" name="dis_per" id="dis_per" class="text number" value="<?php echo $auRec[0]->discount_percentage; ?>" maxlength="2"  />
					</th>
				<?php } ?>


				<?php  if($auRec[0]->discount_type==2){ ?>

					<th valign="top">Scheme Amount:<input type="text" name="dis_amt" id="dis_amt" class="text number" value="<?php echo $auRec[0]->discount_amount; ?>"  />
					</th>

				<?php  } ?>

			</tr>

			<?php }?>


			<tr>
				<th valign="top">Scheme Start Date:</th>
				<td><input type="text" name="start_date" id="start_date" class="text" value="<?php echo $_objAdmin->_changeDate($auRec[0]->start_date); ?>" readonly /></td>
				<td></td>
			</tr>
			<tr>
				<th valign="top">Scheme End Date:</th>
				<td><input type="text" id="to" name="to" class="date required " value="<?php echo $_objAdmin->_changeDate($auRec[0]->end_date); ?>" readonly /></td>
				<td></td>
			</tr>

			<tr>
				<th valign="top">Dealer:</th>
				<td>
					<div style="width:200px; height:100px;overflow:auto;" >
					<table border="0" cellpadding="0" cellspacing="0"  id="id-form">
					<tr>
						<th valign="top"></th>
					</tr>
					<input type="checkbox" name="" onclick="checkall(this);" /> All Dealer
					<?php
					$auCol=$_objAdmin->_getSelectList('table_retailer AS r',"retailer_id,retailer_name",''," status='A' AND distributor_id = '".$_SESSION['distributorId']."' ORDER BY  retailer_name");


					for($i=0;$i<count($auCol);$i++){
							$checked = '';
						if(in_array($auCol[$i]->retailer_id, $retailer_id))
							$checked = 'checked';

					?>
					<tr>
						<td><input type="checkbox" name="retailer_id[]" value="<?php echo $auCol[$i]->retailer_id;?>" <?php echo $checked; ?> /> <?php echo $auCol[$i]->retailer_name; ?>
						</td>
					</tr>
					<?php } ?>
					</table>
					</div>
				</td>
				<td></td>
			</tr>

			<tr>
				<th valign="top">Status:</th>
				<td>
				<select name="status" id="status" class="styledselect_form_3">
				<option value="A" <?php if($auRec[0]->status=='A') echo "selected";?> >Active</option>
				<option value="I" <?php if($auRec[0]->status=='I') echo "selected";?> >Inactive</option>
				</td>
				<td></td>
			</tr>
			<tr>
				<th>&nbsp;</th>
				<td valign="top">
					<input name="account_id" type="hidden" value="<?php echo $_SESSION['accountId']; ?>" />
					<input name="id" type="hidden" value="<?php echo $auRec[0]->discount_id; ?>" />
					<input name="add" type="hidden" value="yes" />
					<input type="reset" value="Reset!" class="form-reset">
					<input type="button" value="Back" class="form-reset" onclick="location.href='secondary_scheme.php';" />
					<input name="submit" class="form-submit" type="submit" id="submit" value="Save" />
				</td>
			</tr>
			</table>
		</form>
			<!-- end id-form  -->
			</td>
			<td>
			<!-- right bar-->
			<?php //include("rightbar/item_bar.php") ?>
			<div class="clear"></div>
		</div>
<!-- end related-act-bottom -->
</div>
<!-- end related-activities -->
</td>
</tr>
<tr>
<td><img src="images/blank.gif" width="695" height="1" alt="blank" /></td>
<td></td>
</tr>
</table>
<div class="clear"></div>
</div>
<!--  end content-table-inner  -->
</td>
<td id="tbl-border-right"></td>
</tr>
<!--<tr>
	<th class="sized bottomleft"></th>
	<td id="tbl-border-bottom">&nbsp;</td>
	<th class="sized bottomright"></th>
</tr>-->
</table>
<div class="clear">&nbsp;</div>



      <link rel="stylesheet" href="docsupport/prism.css">
  <link rel="stylesheet" href="docsupport/chosen.css">
  <style type="text/css" media="all">
    /* fix rtl for demo */
    .chosen-rtl .chosen-drop { left: -9000px; }
	
	#smsform label.error {
	margin-left: 10px;
	width: auto;
	display: inline;
	}
	.error {
		color: #FF0000;
		font:normal 12px tahoma;
	}

		input, textarea {
		width:auto;
		}

  </style>

		  <script src="docsupport/chosen.jquery.js" type="text/javascript"></script>

		  <script type="text/javascript">
			var config = {
			  '.chosen-select'           : {},
			  '.chosen-select-deselect'  : {allow_single_deselect:true},
			  '.chosen-select-no-single' : {disable_search_threshold:10},
			  '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
			  '.chosen-select-width'     : {width:"100%"}
			}
			for (var selector in config) {
			  $(selector).chosen(config[selector]);
			}

			function ChangeDropdowns(){
			if ($("#valnostatus").is(":checked")) {
			    $("#validation_reason").removeAttr("disabled");
			}
			if ($("#valyesstatus").is(":checked")) {
			    $("#validation_reason").attr("disabled","disabled");
			}

			}
			</script>



</div>
<!--  end content -->
<div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->
<div class="clear">&nbsp;</div>
    
<!-- start footer --> 
<?php include("footer.php") ?>  
<!-- end footer -->
</body>
</html>