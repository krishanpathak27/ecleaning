<?php
include("includes/config.inc.php");
include("includes/function.php");
$page_name="Edit Target";
$_objAdmin = new Admin();
$_objItem = new Item();
$objArrayList= new ArrayList();

if($_REQUEST['id']!="") 
{
	$auEndDate=$_objAdmin->_getSelectList('table_target_incentive',"end_date",'',' target_incentive_id='.$_REQUEST['id'].'');
	$endDate=$auEndDate[0]->end_date;

	if($date>$endDate)
		{
			header("Location: target.php?exp=exp");
		}

}


if(isset($_POST['add']) && $_POST['add'] == 'yes')
{
	if($_REQUEST['id']!="") 
		{
			
			$id=$_objAdmin->updateIncentive($_REQUEST['id']);
			
			if($_POST['duration']==1 || $_POST['duration']==2 || $_POST['duration']==4 || $_POST['duration']==5|| $_POST['duration']==6 || $_POST['duration']==7){
				$id=$_objAdmin->updateIncentiveDuration($_REQUEST['id']);
			}
			if($_POST['salesman_id']!=''){
				$id=$_objAdmin->updateIncentiveSalesman($_REQUEST['id']);
			}
				//$new=$_POST['salesman_id'];
			//print_r($new);exit;
			header("Location: edit_target.php?id=".$_REQUEST['id']."&sus=sus");
		}
	
}
if($_REQUEST['sus']=='sus')
{
	$sus="Target has been updated successfully.";
}


if(isset($_REQUEST['id']) && $_REQUEST['id']!="")
{
	$auRec=$_objAdmin->_getSelectList('table_target_incentive as ti left join table_target_incentive_duration as tid on tid.target_incentive_id =ti.target_incentive_id left join table_target_incentive_salesman as tis on tis.duration_id=tid.duration_id left join table_target_incentive_type as tit on tit.target_incentive_id=ti.target_incentive_id 
		left join table_segment as seg on tit.ref_id = seg.segment_id 
		left join table_category as tc on tit.ref_id=tc.category_id',"ti.*,tis.salesman_id,tc.category_code,seg.segment_name,count(distinct tid.duration_number) as total_duration",''," ti.target_incentive_id='".$_REQUEST['id']."'");
	//echo $auRec[0]->status;
	// print_r($auRec);
	if(count($auRec)<=0) header("Location: target.php");
}
?>


<?php
 include("header.inc.php");
 $pageAccess=1;
$check=$objArrayList->checkAccess($pageAccess, basename($_SERVER['PHP_SELF']));
if($check == false){
header('Location: ' . $_SERVER['HTTP_REFERER']);}
  ?>
<link rel="stylesheet" href="./base/jquery.ui.all.css">
<script src="./Autocomplete/jquery-1.5.1.js"></script>
<script src="./Autocomplete/jquery.ui.core.js"></script>
<script src="./Autocomplete/jquery.ui.widget.js"></script>
<script src="./Autocomplete/jquery.ui.button.js"></script>
<script src="./Autocomplete/jquery.ui.position.js"></script>
<script src="./Autocomplete/jquery.ui.menu.js"></script>
<script src="./Autocomplete/jquery.ui.autocomplete.js"></script>
<script src="./Autocomplete/jquery.ui.tooltip.js"></script>
<script src="./Autocomplete/jquery.ui.datepicker.js"></script>
<link rel="stylesheet" href="./base/demos.css">
<script type="text/javascript" src="./javascripts/validate.js"></script>
<script type="text/javascript">
// Popup window code
function newPopup(fieldname) {
	var type = $('#type').val();
	var incentive = $('#incentive').val();
	//alert(incentive);
	popupWindow = window.open(
		'update_qualifire.php?id='+type+"&inc="+incentive,'popUpWindow','height=300,width=700,left=250,top=225,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no,status=yes')
}
</script>
 <script type="text/javascript">
function checkall(el){
	var ip = document.getElementsByTagName('input'), i = ip.length - 1;
	for (i; i > -1; --i){
		if(ip[i].type && ip[i].type.toLowerCase() === 'checkbox'){
			ip[i].checked = el.checked;
		}
	}
}
</script>
<script type="text/javascript">
window.setInterval(function(){
showUpdateQualifire()
{
//alert(str);
//alert(id);
	var incentive = $('#incentive').val();
	//alert(incentive);
	$.ajax({
		'type': 'POST',
		'url': 'update_qualifire_list.php',
		'data': 'i_id='+incentive,
		'success' : function(mystring) {
			//alert(mystring);
		document.getElementById("QuaList").innerHTML = mystring;
		}
	});
}
}, 1000);
//window.onload = showQualifire();
function showUpdateQualifire()
{
//alert(str);
//alert(id);
	var incentive = $('#incentive').val();
	//alert(incentive);
	$.ajax({
		'type': 'POST',
		'url': 'update_qualifire_list.php',
		'data': 'i_id='+incentive,
		'success' : function(mystring) {
			//alert(mystring);
		document.getElementById("QuaList").innerHTML = mystring;
		}
	});
} 
</script>
<script type="text/javascript">
function deleteQualifire(id)
{
//alert(str);
//alert(id);
	var result = confirm("Are you sure, you want to delete selected Qualifier?");
	if (result==true) {
		$.ajax({
			'type': 'POST',
			'url': 'update_qualifire_list.php',
			'data': 'id='+id,
			'success' : function(mystring) {
				//alert(mystring);
			//document.getElementById("disList").innerHTML = mystring;
			}
		});
	}
}
</script>


<script>
$(document).ready(function() {

var $rows = $('.srchSal tr');
$('#srch_sal').live('input',function() {
    var val = $.trim($(this).val()).replace(/ +/g, ' ').toLowerCase();

    $rows.show().filter(function() {
        var text = $(this).text().replace(/\s+/g, ' ').toLowerCase();
        return !~text.indexOf(val);
    }).hide();
});

});
</script>
	<style>
	.ui-button {
		margin-left: -1px;
	}
	.ui-button-icon-only .ui-button-text {
		padding: 0.35em;
	} 
	.ui-autocomplete-input {
		margin: 0;
		padding: 0.4em 0 0.4em 0.45em;
	}
	</style>
<?php include("incentive_list.php") ?>

<div class="clear"></div>
<!-- start content-outer -->
<div id="content-outer">
<!-- start content -->
	<div id="content">
	<div id="page-heading"><h1><span style="color: #d74343; font-family: Tahoma; font-weight: bold;">Edit Target</span></h1></div>
		<table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">
		<tr>
			<th rowspan="3" class="sized"><img src="images/side_shadowleft.jpg" width="20" height="300" alt="" /></th>
			<th class="topleft"></th>
			<td id="tbl-border-top">&nbsp;</td>
			<th class="topright"></th>
			<th rowspan="3" class="sized"><img src="images/side_shadowright.jpg" width="20" height="300" alt="" /></th>
		</tr>
		<tr>
			<!--<td id="tbl-border-left"></td>-->
			<td>
			<!--  start content-table-inner -->
			<div id="content-table-inner">
				<table border="0" width="100%" cellpadding="0" cellspacing="0">
				<tr valign="top">
					<td>
					<!--  start message-red -->
					<?php if($err!=''){?>
					<div id="message-red">
						<table border="0" width="100%" cellpadding="0" cellspacing="0">
						<tr>
							<td class="red-left"> <?php echo $err; ?></td>
							<td class="red-right"><a class="close-red"><img src="images/icon_close_red.gif"   alt="" /></a></td>
						</tr>
						</table>
					</div>
					<?php } ?>
					<!--  end message-red -->
					<?php if($sus!=''){?>
					<!--  start message-green -->
					<div id="message-green">
						<table border="0" width="100%" cellpadding="0" cellspacing="0">
						<tr>
							<td class="green-left"><?php echo $sus; ?></td>
							<td class="green-right"><a class="close-green"><img src="images/icon_close_green.gif"   alt="" /></a></td>
						</tr>
						</table>
					</div>
					<?php } ?>
					<!--  end message-green -->
					
			<!-- start id-form -->
			
		<form name="frmPre" id="frmPre" method="post" action="<?php $_SERVER['PHP_SELF'] ?>" enctype="multipart/form-data" >
			<table border="0" cellpadding="0" cellspacing="0" width="800" id="id-form">
			<tr>
				<th valign="top" >Target Description:</th>
				<td width="200px"><input type="text" name="description" id="description"  class="required" value="<?php echo $auRec[0]->description; ?>" maxlength="255"/></td>
				<td width="500px"><input type="hidden" name="incentive" id="incentive" value="<?php echo $_REQUEST['id']; ?>" /></td>
			</tr>
			<!-- <tr>
				<th valign="top">Target Start Date:</th>
				<td><input type="text" name="from" id="from" class="text" value="<?php echo $_objAdmin->_changeDate($auRec[0]->start_date); ?>" readonly  /></td>
				<td></td>
			</tr> -->
			<!-- <tr>
				<th valign="top" >Target Duration:</th>
				<td >
					<select name="duration" id="duration" class="required styledselect_form_3" onchange="shownumber(this.value);" >
						<option value="<?php echo $auRec[0]->dur_id; ?>">
						<?php 
						$condi=" dur_id='".$auRec[0]->dur_id."' AND status='A'";
						$auDuration=$_objAdmin->_getSelectList2('table_duration',"dur_description",'',$condi);
						 echo $auDuration[0]->dur_description;
						
						?></option>
					</select>
				</td>
				<td ></td>
			</tr> -->
			<tr>
				<th valign="top">Target Year:</th>
				<td><input type="text" name="year" id="year"  class="text" value="<?php echo date('Y',strtotime($auRec[0]->start_date)); ?>" readonly /></td>
				<td></td>
			        
			    </td>
		    </tr> 
     
			<tr> 
				<th valign="top">Target Month:</th>
				<td><input type="text" name="month" id="month"  class="text" value="<?php echo date('F',strtotime($auRec[0]->start_date)); ?>" readonly /></td>
			</tr>
			<?php if($auRec[0]->dur_id==1){ ?>
			<tr>
				<th valign="top" >No of Days to Run:</th>
				<td><input type="text" name="day_list" id="day_list"  class="text" value="<?php echo $auRec[0]->total_duration; ?>" readonly /></td>
				<td></td>
			</tr>
			<?php } else if($auRec[0]->dur_id==2){ ?>
			<!-- <tr>
				<th valign="top" >No of Weeks to Run:</th>
				<td><input type="text" name="week_list" id="week_list"  class="text" value="<?php echo $auRec[0]->total_duration; ?>" readonly /></td>
				<td></td>
			</tr> -->

			<?php } else if($auRec[0]->dur_id==4){ ?>
			<tr>
				<th valign="top" >No of Months to Run:</th>
				<td><input type="text" name="month_list" id="month_list"  class="text" value="<?php echo $auRec[0]->total_duration; ?>" readonly /></td>
				<td></td>
			</tr>
			<?php } else if($auRec[0]->dur_id==5){ ?>
			<tr>
				<th valign="top" >No of Quarter to Run:</th>
				<td><input type="text" name="quarterly_list" id="quarterly_list"  class="text" value="<?php echo $auRec[0]->total_duration; ?>" readonly /></td>
				<td></td>
			</tr>
			<?php } else if($auRec[0]->dur_id==6){ ?>
			<tr>
				<th valign="top" >No of Half Year to Run:</th>
				<td><input type="text" name="halfyearly_list" id="halfyearly_list"  class="text" value="<?php echo $auRec[0]->total_duration; ?>" readonly /></td>
				<td></td>
			</tr>
			<?php } else if($auRec[0]->dur_id==7){ ?>
			<tr>
				<th valign="top" >No of Year to Run:</th>
				<td><input type="text" name="yearly_list" id="yearly_list"  class="text" value="<?php echo $auRec[0]->total_duration; ?>" readonly /></td>
				<td></td>
			</tr>
			<?php } ?>
			<!-- <tr>
				<th valign="top">Target End Date:</th>
				<td><input type="text" name="end_date" id="end_date" value="<?php echo $_objAdmin->_changeDate($auRec[0]->end_date); ?>"  class="text" readonly /></td>
				<td></td>
			</tr> -->
			
			<tr>
				<th valign="top">Salesman:</th>
				<td>
					<div style="width:200px; height:200px;overflow:auto;" >
					<table class="srchSal" border="0" cellpadding="0" cellspacing="0"  id="id-form">

					<input class="text srch_sal" type="text" style="width: 179px; margin-bottom: 10px;" id="srch_sal" placeholder="Search Salesman">
					<tr>
						<th valign="top"></th>
					</tr>
					<input type="checkbox" name="" onclick="checkall(this);" /> All Salesman
					<?php
					$auCol=$_objAdmin->_getSelectList('table_salesman',"salesman_id,salesman_name",''," status='A' ORDER BY  salesman_name");
					for($i=0;$i<count($auCol);$i++){
					?>
					<tr>
						<td><input type="checkbox" name="salesman_id[]" <?php 
						if($auRec[0]->salesman_id!=''){
							$auchk=$_objAdmin->_getSelectList('table_target_incentive as ti left join table_target_incentive_duration as tid on tid.target_incentive_id =ti.target_incentive_id left join table_target_incentive_salesman as tis on tis.duration_id=tid.duration_id',"*",''," ti.target_incentive_id=".$auRec[0]->target_incentive_id." and tis.salesman_id=".$auCol[$i]->salesman_id." and tis.status='A' and tid.to_date >='".$date."'");
							
							if(is_array($auchk)){
							echo "checked";
						}
					}
					?> value="<?php echo $auCol[$i]->salesman_id;?>" /> <?php echo $auCol[$i]->salesman_name;?>
						</td>
					</tr>
					<?php } ?>
					</table>
					</div>
				</td>
				<td></td>
			</tr>
			
			<tr>
			<tr>
				<th valign="top">Target Type:</th>
				<td>
				<select name="type" id="type" class="required styledselect_form_3" onchange="inctype(this.value);">
					<option value="<?php echo $auRec[0]->type_id; ?>">
					<?php 
					$auType=$_objAdmin->_getSelectList2('table_type',"type_description",''," type_id='".$auRec[0]->type_id."' ");
					echo $auType[0]->type_description;
					?>
					</option>
				</select>	
				</td>
				<td></td>
			</tr>
			<tr>
				<th valign="top">Name</th>
					<td width="200px"><input type="text" class="required" value="<?php echo $auRec[0]->segment_name; ?>" maxlength="255" readonly/></td>
				<td></td>
			</tr>
			<?php if($auRec[0]->type_id==1){?>
			<tr>
				<th valign="top">Category Code:</th>
				<td>
				<input readonly id="category_list" style="width: 500px;" class="text" name="category_list" value="<?php $catRec=$_objAdmin->_getSelectList2('table_target_incentive_type as tit left join table_category as c on tit.ref_id=c.category_id',"c.category_code",''," tit.target_incentive_id='".$auRec[0]->target_incentive_id."'");
					for($i=0;$i<count($catRec);$i++){
					echo $catRec[$i]->category_code.",";
					}?>" />
				</td>
				<td></td>
			</tr>
			<?php } else if($auRec[0]->type_id==2){?>
			<tr>
				<th valign="top">Item Code:</th>
				<td>
				<input readonly id="item_list" style="width: 500px;" class="text" value="<?php $itemRec=$_objAdmin->_getSelectList2('table_target_incentive_type as tit left join table_item as i on tit.ref_id=i.item_id',"i.item_code",''," tit.target_incentive_id='".$auRec[0]->target_incentive_id."'");
					for($i=0;$i<count($itemRec);$i++){
					echo $itemRec[$i]->item_code.",";
					}?>" name="item_list"/>
				</td>
				<td></td>
			</tr>
			<?php } else if($auRec[0]->type_id==9){?>
			<tr>
				<th valign="top">Item Code:</th>
				<td>
				<input readonly id="focus_item_list" style="width: 500px;" class="text" name="focus_item_list" value="<?php $itemRec=$_objAdmin->_getSelectList2('table_target_incentive_type as tit left join table_item as i on tit.ref_id=i.item_id',"i.item_code",''," tit.target_incentive_id='".$auRec[0]->target_incentive_id."'");
					for($i=0;$i<count($itemRec);$i++){
					echo $itemRec[$i]->item_code.",";
					}?>"/>
				</td>
				<td></td>
			</tr>
			<?php }?>
			
			<tr>
				<th valign="top" >
				<?php 
				if($auRec[0]->type_id==1){ echo "Minimum Quantity:";}
				else if($auRec[0]->type_id==2){ echo "Minimum Quantity:";} 
				else if($auRec[0]->type_id==3){ echo "Minimum Items:";} 
				else if($auRec[0]->type_id==4){ echo "Minimum Orders:";} 
				else if($auRec[0]->type_id==5){ echo "Minimum Order Value:";} 
				else if($auRec[0]->type_id==6){ echo "Minimum Schemes:";} 
				else if($auRec[0]->type_id==7){ echo "Minimum Retailers:";} 
				else if($auRec[0]->type_id==8){ echo "Retailers To Be Added:";} 
				else if($auRec[0]->type_id==9){ echo "Minimum Quantity:";} 
				else if($auRec[0]->type_id==10){ echo "Retailers To Be Added:";} 
				else if($auRec[0]->type_id==11){ echo "Minimum Total Call:";} 
				else if($auRec[0]->type_id==12){ echo "Minimum Amount:";}
				else if($auRec[0]->type_id==14){ echo "Minimum Quantity:";} 
				?>				
				</th>
				<td><input type="text" name="primary_values" id="primary_values"  value="<?php echo $auRec[0]->primary_values; ?>" class="required numberDE"/></td>
				<td></td>
			</tr>
			<?php if($auRec[0]->secondary_values !='' ){ ?>
			<tr> 
				<th valign="top">
				<?php
				if($auRec[0]->type_id==3){ echo "Minimum Order:";}
				else if($auRec[0]->type_id==5){ echo "Minimum Order:";} 
				else if($auRec[0]->type_id==9){ echo "Minimum Retailers:";} 
				else if($auRec[0]->type_id==11){ echo "% of Productive Calls:";} 
				?>
				</th>
				<td>
				<input type="text" id="secondary_value" value="<?php echo $auRec[0]->secondary_values; ?>" class="required numberDE" name="secondary_value" />
				</td>
				<td></td>
			</tr>
			<?php }	 ?>
			<?php if($auRec[0]->incentive_reward_type==2){ ?>
			<tr >
				<th valign="top" >Average Amount:</th>
				<td><input type="text" name="average_amount" id="average_amount" class="text" value="<?php echo $auRec[0]->average_amount; ?>" readonly /></td>
				<td></td>
			</tr>
			<tr >
				<th valign="top" >Reward Slab:</th>
				<td colspan="2">
					<table   border="0"  >
						<tr >
							<td><div style="font-weight:bold; width:210px" >Description</div></td>
							<td><div style="font-weight:bold; width:120px" >Criteria</div></td>
							<td><div style="font-weight:bold;">Incentive Amount on Turn Over</div></td>
							
						</tr>
						<tr >
							<td colspan="6">
								<table id="dataTableQuestion" cellspacing="10" cellpadding="10" border="0">
									<?php
									$slabRec=$_objAdmin->_getSelectList2('table_target_incentive_slab',"*",''," target_incentive_id='".$auRec[0]->target_incentive_id."'");
									for($i=0;$i<count($slabRec);$i++){
									?>
									<tr >
										<td><div style="font-weight:bold; width:235px" ><input name="slab_description[]" class="text" type="text" id="slab_description" value="<?php echo $slabRec[$i]->slab_description; ?>" maxlength="250" style="width:200px" readonly /></div></td>
										<td><div style="font-weight:bold; width:145px" ><input name="percentage_of_criteria[]" class="text" type="text" id="percentage_of_criteria" value="<?php echo $slabRec[$i]->percentage_of_criteria; ?>" maxlength="3" style="width:25px" readonly /><b> &#37; &amp; Above</b></div></td>
										<td ><div style="font-weight:bold; width:220px"  ><input name="percentage_of_reward_amount[]" class="text" type="text" id="percentage_of_reward_amount" value="<?php echo $slabRec[$i]->percentage_of_reward_amount; ?>" maxlength="5" style="width:25px" readonly /><span style="padding-bottom:5px; border:1px;"><b> &#37;</b></span></div></td>
									</tr>
									<?php } ?>
								</table>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<?php } else { ?>
			<!-- <tr>
				<th valign="top" >Reward Amount:</th>
				<td><input type="text" name="reward_amount" id="reward_amount" class="text numberDE" value="<?php if ($auRec[0]->incentive_reward_amount!=0) { echo intval($auRec[0]->incentive_reward_amount); }?>" /></td>
				<td></td>
			</tr> -->
			<?php } ?>
			<tr>
				<th valign="top">Party Type:</th>
				<td valign="top">
				<select name="party_type" id="party_type" class="styledselect_form_3" onchange="partype(this.value);">
				<option value="<?php echo $auRec[0]->party_type_id; ?>">
					<?php 
						$condi=" party_type_id='".$auRec[0]->party_type_id."' AND status='A'";
						$auPartyType=$_objAdmin->_getSelectList2('table_party_type',"party_description",'',$condi);
						 echo $auPartyType[0]->party_description;
						
						?>
					</option>
				</select>	
				</td>
				<td></td>
			</tr>
			<?php if($auRec[0]->party_type_id==2){ ?>
			<tr>
				<th valign="top">State Name:</th>
				<td>
				<input readonly id="state_list" style="width: 500px;" class="text" name="state_list" value="<?php $stateRec=$_objAdmin->_getSelectList2('table_target_incentive_party as tip left join state as s on tip.state_id=s.state_id',"s.state_name",''," tip.target_incentive_Id='".$auRec[0]->target_incentive_id."'");
					for($i=0;$i<count($stateRec);$i++){
					echo $stateRec[$i]->state_name.",";
					}?>" />
				</td>
				<td></td>
			</tr>
			<?php } else if($auRec[0]->party_type_id==3){ ?>
			<tr>
				<th valign="top">City Name:</th>
				<td>
				<input readonly id="city_list" style="width: 500px;" class="text" name="city_list" value="<?php $cityRec=$_objAdmin->_getSelectList2('table_target_incentive_party as tip left join city as c on tip.city_Id=c.city_id',"c.city_name",''," tip.target_incentive_Id='".$auRec[0]->target_incentive_id."'");
					for($i=0;$i<count($cityRec);$i++){
					echo $cityRec[$i]->city_name.",";
					}?>" />
				</td>
				<td></td>
			</tr>
			<?php }?>
			<tr>
				<th valign="top">Target Status:</th>
				<td>
				<select name="status" id="status" class="styledselect_form_3">
					<option value="A"  <?php if($auRec[0]->status=='A') echo "selected";?> >Active</option>
					<option value="I" <?php if($auRec[0]->status=='I') echo "selected";?>>Inactive</option>
				</select>
				</td>
				<td></td>
			</tr>
			<tr>
			<td id="QuaList" colspan="3" >
			</td>
			<!--<div id="disList" ></div>-->
			</tr>
			<tr>
				<th>&nbsp;</th>
				<td><input type="button" onclick="return newPopup()" value="Add Qualifier" class="form-reset"  /> <!--<input type="button" onclick="showQualifire()" value="test" class="form-reset"  />--></td>
			</tr>
			<tr>
				<th>&nbsp;</th>
				<td valign="top">
					<input name="add" type="hidden" value="yes" />
					<input type="hidden" name="duration" value="2"  />
					<input type="reset" value="Reset!" class="form-reset">
					<input type="button" value="Back" class="form-reset" onclick="location.href='target.php';" />
					<input name="submit" class="form-submit" type="submit" id="submit" value="Save"  />
				</td>
			</tr>
			</table>
		</form>
			<!-- end id-form  -->
			</td>
			<td>
			<!-- right bar-->
			<?php //include("rightbar/item_bar.php") ?>
			<div class="clear"></div>
		</div>
<!-- end related-act-bottom -->
</div>
<!-- end related-activities -->
</td>
</tr>
<tr>
<td><img src="images/blank.gif" width="695" height="1" alt="blank" /></td>
<td></td>
</tr>
</table>
<div class="clear"></div>
</div>
<!--  end content-table-inner  -->
</td>
<td id="tbl-border-right"></td>
</tr>
<!--<tr>
	<th class="sized bottomleft"></th>
	<td id="tbl-border-bottom">&nbsp;</td>
	<th class="sized bottomright"></th>
</tr>-->
</table>
<div class="clear">&nbsp;</div>

</div>
<!--  end content -->
<div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->
<div class="clear">&nbsp;</div>
    
<!-- start footer --> 
<?php include("footer.php") ?>  
<!-- end footer -->

</body>
</html>