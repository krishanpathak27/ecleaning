<?php
include("includes/config.inc.php");
include("includes/function.php");
$page_name="Enquiries";
$_objAdmin = new Admin();

if(isset($_POST['showEnquiry']) && $_POST['showEnquiry'] == 'yes'){ 
    if($_POST['from']!=""){
        $_SESSION['FromEnquiryList']=date('Y-m-d',strtotime($_POST['from']));  
    }

    if($_POST['to']!=""){
        $_SESSION['ToEnquiryList']=date('Y-m-d',strtotime($_POST['to']));
    }

}

if($_SESSION['FromEnquiryList']==''){
  $_SESSION['FromEnquiryList']= '';
}
//echo $_SESSION['FromBookingList'];die;

if($_SESSION['ToEnquiryList']==''){
  $_SESSION['ToEnquiryList']= '';
}

if(isset($_POST['export']) && $_POST['export']!=""){


    header("Location:export.inc.php?export_customer_enquiries");
    exit;
}
if(isset($_REQUEST['reset']) && $_REQUEST['reset'] == 'yes'){ 

  $_SESSION['FromEnquiryList']='';
  $_SESSION['ToEnquiryList']= '';
  header("Location: enquiries.php");
}


?>

<?php include("header.inc.php") ?>
<div class="m-grid__item m-grid__item--fluid m-wrapper" style="background-color:">
    <!-- BEGIN: Subheader -->
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="#" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__separator">
                        -
                    </li>
                    <li class="m-nav__item">
                        <a href="#" class="m-nav__link">
                            <span class="m-nav__link-text">
                                Report
                            </span>
                        </a>
                    </li>
                    <li class="m-nav__separator">
                        -
                    </li>
                    <li class="m-nav__item">
                        <a href="" class="m-nav__link">
                            <span class="m-nav__link-text">
                                Enquiry
                            </span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <!-- END: Subheader -->
    <div class="m-content">
        
        <div class="m-portlet m-portlet--mobile">
            
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            Enquiry List
                        </h3>
                    </div>
                </div>
            </div>
            <div class="m-portlet__body">
                <!--begin: Search Form -->
                <form class="m-form m-form--fit m-form--label-align-right" name="frmPre" id="frmPre" method="post" action="" enctype="multipart/form-data">
                    <div class="form-group m-form__group row">
                        <div class="col-lg-2">                                                   
                            <label>
                                Date Picker
                            </label>                                                     
                        </div>
                        <div class="col-lg-6" style="padding-bottom: 10px">
                            <div class="m-input-icon m-input-icon--right">
                                <div class="input-daterange input-group" id="m_datepicker_5">
                                    <input type="text" class="form-control m-input" name="from" id="from" value="<?php if(!empty($_SESSION['FromEnquiryList'])){echo date('m/d/Y',strtotime($_SESSION['FromEnquiryList']));}?>">
                                    <span class="input-group-addon">
                                        <i class="la la-ellipsis-h"></i>
                                    </span>
                                    <input type="text" class="form-control" name="to" id="to" value="<?php if(!empty($_SESSION['ToEnquiryList'])){echo date('m/d/Y',strtotime($_SESSION['ToEnquiryList']));}?>">
                                </div>

                            </div>
                        </div>
                        <div class="col-lg-4 text-right"> 
                            <div class="m-input-icon m-input-icon--right">
                                <button type="submit" class="btn btn-success">
                                    Submit
                                </button>
                                <button type="submit"  name="export" id="export" value="Export" class="btn btn-secondary">
                                    Export
                                </button>
                                <button type="reset" class="btn btn-secondary" onclick="location.href='enquiries.php?reset=yes';">
                                    Reset
                                </button>
                            </div> 
                        </div> 
                    </div>
                    <input name="showEnquiry" type="hidden" value="yes"/>
                </form>
                <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
                    <div class="row align-items-center">
                        <div class="col-xl-8 order-2 order-xl-1">
                            <div class="form-group m-form__group row align-items-center">
                                <div class="col-md-4">
                                    <div class="m-input-icon m-input-icon--left">
                                        <input type="text" class="form-control m-input" placeholder="Search..." id="generalSearch">
                                        <span class="m-input-icon__icon m-input-icon__icon--left">
                                            <span>
                                                <i class="la la-search"></i>
                                            </span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-4 order-1 order-xl-2 m--align-right add">
                            <!-- <a href="add_cleaner.php" class="btn btn-warning m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                                <span>
                                    <i class="la la-cart-plus"></i>
                                    <span>
                                        Add Cleaner
                                    </span>
                                </span>
                            </a> -->
                            <div class="m-separator m-separator--dashed d-xl-none "></div>
                        </div>
                    </div>
                </div>
                <!--end: Search Form -->
                <!--begin: Datatable -->
                <div class="m_datatable" id="enquiry_list"></div>
                <!--end: Datatable -->
            </div>
        </div>
    </div>
</div>
</div>
</div>
<!-- start footer -->         
<?php include("footer.php") ?>
<script type="text/javascript" >
    var DefaultDatatableDemo = function () {
        //== Private functions

        // basic demo
        var demo = function () {
            var datatable = $('.m_datatable').mDatatable({
                data: {
                    type: 'remote',
                    source: {
                        read: {
                            url: '<?php echo $ajaxUrl . '?pageType=enquiryList' ?>'
                        }
                    },
                    pageSize: 20,
                    serverPaging: true,
                    serverFiltering: true,
                    serverSorting: true
                },
                layout: {
                    theme: 'default',
                    class: '',
                    
                    scroll: true,
                    footer: false
                },
                sortable: true,
                filterable: false,
                pagination: true,
                search: {
                    input: $('#generalSearch')
                },
                columns: [{
                        field: "message",
                        title: "Message",
                        width:100,
                        sortable: 'asc'
                    }, {
                        field: "query_type_text",
                        title: "Query Type",
                        responsive: {visible: 'lg'}
                    }, {
                        field: "customer_name",
                        title: "Customer Name",
                        responsive: {visible: 'lg'}
                    }, {
                        field: "customer_number",
                        title: "Customer Number",
                        responsive: {visible: 'lg'}
                    }, {
                        field: "customer_email",
                        title: "Customer Email",
                        width:150,
                        responsive: {visible: 'lg'}
                    },{
                        field: "created_date",
                        title: "Date",
                        responsive: {visible: 'lg'},
                    },{
                        field: "Actions",
                        title: "Actions",
                        sortable: false,
                        locked: {right: 'xl'},
                        overflow: 'visible',
                        template: function (row, index, datatable) {
                            var dropup = (datatable.getPageSize() - index) <= 4 ? 'dropup' : '';
                             return '\<a href="enquiries_detail.php?id='+row.contact_query_id+'" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only edit m-btn--pill" title="View Details">\
                                                        <i class="la la-eye"></i>\
                                                </a>';
                        }
                    }]
            });
        };

        return {
            // public functions
            init: function () {
                demo();
            }
        };
    }();

    jQuery(document).ready(function () {
        DefaultDatatableDemo.init();
    });



</script>


