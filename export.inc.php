<?php
ini_set('memory_limit','-1');
include("includes/config.inc.php");
include("includes/function.php");
//include("includes/globalarraylist.php");
$_objAdmin = new Admin();
$_objArrayList = new ArrayList();
ob_clean();

//------------------------------------------------

/******************************************** Start Export State List ******************************************************************/			
if(isset($_GET['export_state_new'])){		
	/*$data="State Name\t Status\n";*/ //added by anurag@8-02-2017
	$data="State Name\t Region \t Zone \t Country \t State Status\n";
	$condi=	"s.status!="."'D'";
	/*$auRec=$_objAdmin->_getSelectList2('state as s',"s.*",'',$condi);*/
	//added by Anurag
	$auRec=$_objAdmin->_getSelectList2('state AS s left join table_zone as z on s.zone_id=z.zone_id left join table_region as r on z.region_id=r.region_id left join country as cou on r.country_id=cou.country_id',"s.state_name,z.zone_name,r.region_name,cou.country_name,s.status",'',$condi);
	
	if(is_array($auRec)){
		for($i=0;$i<count($auRec);$i++){
		$data.="".$auRec[$i]->state_name."\t".$auRec[$i]->region_name."\t".$auRec[$i]->zone_name."\t".$auRec[$i]->country_name."\t".$auRec[$i]->status."\n";
		}
						
	}
	header("Content-type: application/octet-stream");
	header("Content-Disposition: attachment; filename=\"states_list.xls\"");		
	echo $data;
	exit;
	}
/******************************************** End Export State List ******************************************************************/	

/******************************************** Start Export Distric/City List ******************************************************************/			
if(isset($_GET['export_distric_new'])){		
	$data="Distric Name \t Distric Code\t State Name\t Zone \t Region \t Country\t Status\n";
	/*$condi=	"s.status!="."'D' order by s.state_name";*/
	//added by anurag@8-02-2017
	$condi=	"C.status!="."'D' order by C.city_name";
	
	/*$auRec=$_objAdmin->_getSelectList2('state as s',"s.*",'',$condi);*/
	//added by anurag@8-02-2017
	$auRec=$_objAdmin->_getSelectList2('city AS C Left JOIN state AS S ON S.state_id = C.state_id left join table_zone AS z on S.zone_id=z.zone_id left join table_region as r on z.region_id=r.region_id left join country as coun on r.country_id=coun.country_id ',"C.city_id,C.city_name,C.city_code,S.state_name,z.zone_name,r.region_name,coun.country_name,C.status ",'',$condi);
	
	if(is_array($auRec))
	{
		for($i=0;$i<count($auRec);$i++)
		{     
			
			/*$condi1=	"c.state_id= '".$auRec[$i]->state_id."' and c.status!="."'D' order by c.city_name";*/
			//added by anurag@8-02-2017
			$data.="".$auRec[$i]->city_name."\t".$auRec[$i]->city_code."\t".$auRec[$i]->state_name." \t".$auRec[$i]->zone_name."\t".$auRec[$i]->region_name."\t".$auRec[$i]->country_name."\t".$auRec[$i]->status."\n";

			/*$auRec1=$_objAdmin->_getSelectList2('city as c',"c.*",'',$condi1);*/
			/*if(is_array($auRec1))
			{
				for($j=0;$j<count($auRec1);$j++)
				{
					$data.="".$auRec1[$j]->city_name."\t".$auRec1[$j]->city_code."\t".$auRec[$i]->state_name."\t".$auRec1[$j]->status."\n";
				}
			}*/
			
			
				
		}						
	}
	header("Content-type: application/octet-stream");
	header("Content-Disposition: attachment; filename=\"distric_list.xls\"");		
	echo $data;
	exit;
	}
/******************************************** End Export Distric/City List ******************************************************************/	


/************************* Start Export Area List 21 Feb 2017 ********************************************/	

if(isset($_GET['export_area_new'])){		
	$data=" Area Name \t Area Code \t State \t City \t Status\n";
	
	$condi = " M.status!='D' ORDER BY M.market_name ";

	$auRec = $_objAdmin->_getSelectList("table_markets AS M 
		Left JOIN city AS C ON C.city_id = M.city_id AND C.status!='D' 
		Left JOIN state AS S ON S.state_id = C.state_id  AND S.status!='D'",
		" M.market_id, M.market_name,M.market_code, M.status,C.city_name,S.state_name ",'',$condi);
	//print_r($auRec); exit;

	if(is_array($auRec)){
		for($i=0;$i<count($auRec);$i++){

							$sts=($auRec[$i]->status=='A')?"Active":"Inactive";

							$remove = array("\n", "\r\n", "\r");

							$data.='"'.ucwords($auRec[$i]->market_name).'"';
							$data.="\t";

							
							$data.='"'.ucwords($auRec[$i]->market_code).'"';
							$data.="\t";
							
							// State Name
							$data.='"'.ucwords($auRec[$i]->state_name).'"';
							$data.="\t";

							// City
							$data.='"'.ucwords($auRec[$i]->city_name).'"';
							$data.="\t";

							// Status
							$data.='"'.$sts.'"';
							$data.="\t";
							$data.="\n";

		}	

	}					
						
	header("Content-type: application/octet-stream");
	header("Content-Disposition: attachment; filename=\"area_list.xls\"");		
	echo $data;
	exit;
	}

/******************************************** End Export Area List ******************************************************************/	



/******************************************** Start Export Taluka List ******************************************************************/			
if(isset($_GET['export_taluka_new'])){		
	$data=" Taluka Name \t Taluka Code \t Distric Name \t State Name\t Taluka Status\n";
	$condi=	"s.status!="."'D' order by s.state_name";
	$auRec=$_objAdmin->_getSelectList2('state as s',"s.*",'',$condi);
	
	if(is_array($auRec))
	{
		for($i=0;$i<count($auRec);$i++)
		{
			
			$condi1=	"c.state_id= '".$auRec[$i]->state_id."' and c.status!="."'D' order by c.city_name";
			$auRec1=$_objAdmin->_getSelectList2('city as c',"c.*",'',$condi1);
			if(is_array($auRec1))
			{
				for($j=0;$j<count($auRec1);$j++)
				{
					
					$condi2 = "t.city_id= '".$auRec1[$j]->city_id."' and t.status!="."'D' order by t.taluka_name";
					$auRec2 = $_objAdmin->_getSelectList2('table_taluka as t',"t.*",'',$condi2);
					if(is_array($auRec2))
					{
						for($k=0;$k<count($auRec2);$k++)
						{
							$data.="".$auRec2[$k]->taluka_name."\t".$auRec2[$k]->taluka_code."\t".$auRec1[$j]->city_name."\t" .$auRec[$i]->state_name."\t".$auRec2[$k]->status."\n";
						}
					}
					
				}
			}
				
		}						
	}
	header("Content-type: application/octet-stream");
	header("Content-Disposition: attachment; filename=\"taluka_list.xls\"");		
	echo $data;
	exit;
	}
/******************************************** End Export Taluka List ******************************************************************/	


/******************************************** Start Export Market List ******************************************************************/			
if(isset($_GET['export_market_new'])){		
	$data="Market Name \t Market Code \t Taluka Name \t Distric Name \t State Name\t Status\n";
	$condi=	"s.status!="."'D' order by s.state_name";
	$auRec=$_objAdmin->_getSelectList2('state as s',"s.*",'',$condi);
	
	if(is_array($auRec))
	{
		for($i=0;$i<count($auRec);$i++)
		{
			
			$condi1=	"c.state_id= '".$auRec[$i]->state_id."' and c.status!="."'D' order by c.city_name";
			$auRec1=$_objAdmin->_getSelectList2('city as c',"c.*",'',$condi1);
			if(is_array($auRec1))
			{
				for($j=0;$j<count($auRec1);$j++)
				{
					
					$condi2 = "t.city_id= '".$auRec1[$j]->city_id."' and t.status!="."'D' order by t.taluka_name";
					$auRec2 = $_objAdmin->_getSelectList2('table_taluka as t',"t.*",'',$condi2);
					if(is_array($auRec2))
					{
						for($k=0;$k<count($auRec2);$k++)
						{
							
							$condi3 = "m.taluka_id= '".$auRec2[$k]->taluka_id."' and m.status!="."'D' order by m.market_name";
							$auRec3 = $_objAdmin->_getSelectList2('table_markets as m',"m.*",'',$condi3);
							if(is_array($auRec3))
							{
								for($l=0;$l<count($auRec3);$l++)
								{
									$data.="".$auRec3[$l]->market_name."\t".$auRec3[$l]->market_code."\t".$auRec2[$k]->taluka_name."\t".$auRec1[$j]->city_name."\t".$auRec[$i]->state_name."\t".$auRec3[$l]->status."\n";
								}
							}
							
						}
					}
					
				}
			}
				
		}						
	}
	header("Content-type: application/octet-stream");
	header("Content-Disposition: attachment; filename=\"city_list.xls\"");		
	echo $data;
	exit;
	}
/******************************************** End Export Market List ******************************************************************/	











//-------------------------------------------------
/******************************************** Start Export City List ******************************************************************/			
if(isset($_GET['export_city'])){		
	$data="State Name\t District\n";
	$condi=	" 1=1";
	$auRec=$_objAdmin->_getSelectList2('city as c left join state as s on c.state_id=s.state_id',"c.city_name,s.state_name",'',$condi);
	if(is_array($auRec)){
		for($i=0;$i<count($auRec);$i++){
		$data.="".$auRec[$i]->state_name."\t".$auRec[$i]->city_name."\n";
		}						
	}
	header("Content-type: application/octet-stream");
	header("Content-Disposition: attachment; filename=\"district_list.xls\"");		
	echo $data;
	exit;
	}
/******************************************** End Export City List ******************************************************************/				
/******************************************** Start Export Category List ******************************************************************/			
if(isset($_GET['export_category'])){		
	$data="Category Name\t Category Code\t Status\n";
	$condi=	" account_id='".$_SESSION['accountId']."' ORDER BY category_name";
	$auRec=$_objAdmin->_getSelectList('table_category',"*",'',$condi);
	if(is_array($auRec)){
		for($i=0;$i<count($auRec);$i++){
		if($auRec[$i]->status=='A'){
		$status="Active";
		} else {
		$status="Inactive";
		}
		$data.="".$auRec[$i]->category_name."\t".$auRec[$i]->category_code."\t".$status."\n";
		}						
	}
	header("Content-type: application/octet-stream");
	header("Content-Disposition: attachment; filename=\"category_list.xls\"");		
	echo $data;
	exit;
	}
/******************************************** End Export Category List ******************************************************************/			
/******************************************** Start Export Item Cases Size  for MED Project ******************************************************************/		
if(isset($_GET['export_cases'])){		
	$data="Cases Size\t Case Description\t Status\n";
	$condi=	" account_id='".$_SESSION['accountId']."' ORDER BY case_size";
	$auRec=$_objAdmin->_getSelectList('table_cases',"*",'',$condi);
	if(is_array($auRec)){
		for($i=0;$i<count($auRec);$i++){
		if($auRec[$i]->status=='A'){
		$status="Active";
		} else {
		$status="Inactive";
		}
		$data.="".$auRec[$i]->case_size."\t".$auRec[$i]->case_description."\t".$status."\n";
		}						
	}
	header("Content-type: application/octet-stream");
	header("Content-Disposition: attachment; filename=\"case_list.xls\"");		
	echo $data;
	exit;
	}
	
/******************************************** End Export Item Color List ******************************************************************/		
/******************************************** Start Export Item List ******************************************************************/	
if(isset($_GET['export_item'])){		
	$data="Item Name\t Weight\t Item Code\t Product Code Old\t Product Code New\t Category\t Item Division\t Item Segment\t MRP\t DP\t Model Code\t Warranty\t Prodata\t Grace\t  Status\n";
	$condi=	" i.account_id='".$_SESSION['accountId']."' ORDER BY item_name";
	$auRec=$_objAdmin->_getSelectList('table_item as i left join table_price as p on i.item_id = p.item_id left join table_category as c on i.category_id = c.category_id left join table_brands as b on b.brand_id=i.brand_id left join table_offer as o on o.offer_id=i.offer_id left join table_item_case_relationship re on re.item_id=i.item_id left join table_division divi on divi.division_id=i.item_division left join table_cases as cas on cas.case_id=re.case_id left join table_segment as seg on seg.segment_id=i.item_segment
			left join table_variant as va on va.variant_id=i.variant_id left join table_sku as sk on sk.sku_id=i.sku_id',"i.*,p.item_mrp,p.item_dp,cas.case_size,b.brand_name,divi.division_name,seg.segment_name,va.variant_name,sk.sku_name,o.offer_name,c.category_name",'',$condi);
	if(is_array($auRec)){
		for($i=0;$i<count($auRec);$i++){
		if($auRec[$i]->status=='A'){
		$status="Active";
		} else {
		$status="Inactive";
		}
		
		
		$data.="".$auRec[$i]->item_name."\t".$auRec[$i]->item_size."\t".$auRec[$i]->item_code."\t".$auRec[$i]->item_erp_code_old."\t".$auRec[$i]->item_erp_code."\t".$auRec[$i]->category_name."\t".$auRec[$i]->division_name."\t".$auRec[$i]->segment_name."\t".$auRec[$i]->item_mrp."\t".$auRec[$i]->item_dp."\t".$auRec[$i]->model_code."\t".$auRec[$i]->item_warranty."\t".$auRec[$i]->item_prodata."\t".$auRec[$i]->item_grace."\t".$status."\n";
		}						
	}
	header("Content-type: application/octet-stream");
	header("Content-Disposition: attachment; filename=\"item_list.xls\"");		
	echo $data;
	exit;
	}
/******************************************** End Export Item List ******************************************************************/	
/******************************************** Start Export State List ******************************************************************/	
if(isset($_GET['export_state'])){		
	$data="State Name\n";
	$condi=	" 1=1";
	$auRec=$_objAdmin->_getSelectList2('state',"*",'',$condi);
	if(is_array($auRec)){
		for($i=0;$i<count($auRec);$i++){
		$data.="".$auRec[$i]->state_name."\n";
		}						
	}
	header("Content-type: application/octet-stream");
	header("Content-Disposition: attachment; filename=\"state_list.xls\"");		
	echo $data;
	exit;
	}	
/******************************************** End Export State List ******************************************************************/		
/******************************************** Start Export Distributor ******************************************************************/	
	
if(isset($_GET['export_distributors_list'])){		
	// $data="Distributor Name \t Distributor Code \t Division \t   State \t District \t District Code \t Taluka \t Taluka Code \t City \t City Code \t Address1 \t Address2 \t Zipcode \t SMS Mobile Number \t Phone Number1 \t Phone Number2 \t Phone Number3 \t Landline Number \t Contact Person1 \t Contact Person Number1 \t Contact Person2 \t Contact Person Number2 \t Contact Person3 \t Contact Person Number3 \t Email ID1 \t Email ID2 \t Email ID3\t Distributor Class\t Distributor Region \t Aadhar Number \t Pan Number  \n";

	$data = "Distributor Name \t Distributor Code \t Salesman \t Division \t Brand\t Country \t Region \t Zone \t State \t City \t Area \t Zipcode/Pincode \t Address 1 \t Address 2 \t Send SMS Mobile No \t Phone Number 1 \t Phone Number 2 \t Phone Number 3 \t Contact Person 1 \t Contact Number 1 \t Contact Person 2 \t Contact Number 2 \t Contact Person 3 \t Contact Number 3 \t Email ID 1 \t Email ID 2 \t Email ID 3 \t Date of Birth \t Distributor Status \t Aadhar Number \t Pan Number \t Tin Number \t Credit Limit \n";


		/*// Added by AJAY@2015-11-20
		$salsList = $_objArrayList->SalesmanArrayList(); // For Admin and Salesman Login
		$divisionList = $_objArrayList->getAllDivisonOfSelectedSalesmen($salsList);
		$divisionFilters = $_objAdmin->getDivisionCondition($divisionList, 'd');*/

		$divisionFilters = "";
		
		$salDiv = $_objAdmin->_getSelectList('table_salesman'," division_id",''," AND salesman_id='".$_SESSION['salesmanId']."'");
		    if(sizeof($salDiv) > 0)
		   	 $divisionFilters = " AND d.division_id =".$salDiv[0]->division_id;



	$condi=	" d.new='' and d.status='A' $divisionFilters and d.account_id='".$_SESSION['accountId']."' ORDER BY d.distributor_name";

	// $auRec=$_objAdmin->_getSelectList('table_distributors as d  
	// 	left join table_division AS dV ON dV.division_id = d.division_id  
	// 	left join state as s on d.state=s.state_id 
	// 	left join table_web_users as w on w.distributor_id=d.distributor_id 
	// 	left join city as c on d.city=c.city_id 
	// 	left join table_relationship as tr on tr.relationship_id=d.relationship_id 
	// 	left join table_region as trr on trr.region_id=d.region_id 
	// 	left join table_markets as m on m.market_id=d.market_id 
	// 	left join table_taluka as t on t.taluka_id=d.taluka_id',"d.*,s.state_name,w.email_id,c.city_name,c.city_code,dV.division_name, tr.relationship_code,trr.region_name, m.market_name,m.market_code,t.taluka_name, t.taluka_code",'',$condi);

	$auRec=$_objAdmin->_getSelectList('table_distributors as d  
		left join table_division AS dV ON dV.division_id = d.division_id  
		left join table_account as a on a.account_id=d.account_id 
		left join table_web_users as w on w.distributor_id=d.distributor_id 
		left JOIN table_salesman AS sal ON sal.salesman_id = d.salesman_id
		left join state as s on s.state_id=d.state 
		left join city as c on c.city_id=d.city 
		left join table_relationship as tr on tr.relationship_id=d.relationship_id 
		left join table_region as rg on rg.region_id=d.region_id 
		left join table_zone as zn on zn.zone_id = d.zone
		left join country as cntry on cntry.country_id = d.country
		left join table_markets as m on m.market_id=d.market_id
		left join table_brands as brd on brd.brand_id = d.brand_id', 
		"d.*,sal.salesman_name,sal.salesman_code,w.username,w.email_id,w.web_user_id,w.status as loginStatus,s.state_name,c.city_name,dV.division_name,tr.relationship_code,rg.region_name,d.taluka_id,m.market_name,zn.zone_name,cntry.country_name,brd.brand_name",'',$condi);



	if(is_array($auRec)){
		for($i=0;$i<count($auRec);$i++){
		//$data.="".$auRec[$i]->distributor_name."\t".$auRec[$i]->distributor_code."\t".$auRec[$i]->state_name."\t".$auRec[$i]->city_name."\t".$auRec[$i]->city_code."\t".$auRec[$i]->taluka_name."\t".$auRec[$i]->taluka_code."\t".$auRec[$i]->market_name."\t".$auRec[$i]->market_code."\t".$auRec[$i]->distributor_address."\t".$auRec[$i]->distributor_address2."\t".$auRec[$i]->zipcode."\t".$auRec[$i]->sms_number."\t".$auRec[$i]->distributor_phone_no."\t".$auRec[$i]->distributor_phone_no2."\t".$auRec[$i]->distributor_phone_no3."\t".$auRec[$i]->distributor_leadline_no."\t".$auRec[$i]->contact_person."\t".$auRec[$i]->contact_number."\t".$auRec[$i]->contact_person2."\t".$auRec[$i]->contact_number2."\t".$auRec[$i]->contact_person3."\t".$auRec[$i]->contact_number3."\t".$auRec[$i]->distributor_email."\t".$auRec[$i]->distributor_email2."\t".$auRec[$i]->distributor_email3."\t".$auRec[$i]->relationship_code."\t".$auRec[$i]->region_name."\t".$auRec[$i]->aadhar_no."\t".$auRec[$i]->pan_no."\n";


			$start_date==$_objAdmin->_changeDate($auRec[$i]->start_date);

			if($auRec[$i]->distributor_dob=="0000-00-00"){
				$distributor_dob="-";
			}
			else{
				$distributor_dob==$_objAdmin->_changeDate($auRec[$i]->distributor_dob);
			}
			
			//end date
			if($auRec[$i]->status=='I' ){
				$end_date==$_objAdmin->_changeDate($auRec[$i]->end_date);
			} else {
				$end_date=' ';
			}
			//end date
			
			if($auRec[$i]->status=='A'){
				$sts = "Active";
			}else{
				$sts = "Inactive";
			}
			//login status
			if($auRec[$i]->web_user_id!=''){
				if($auRec[$i]->status=='A'){
					$LogSts=($auRec[$i]->loginStatus=='A')?"Active":"Inactive";
				} else {
					$LogSts='Inactive';
				}
			} else {
				$LogSts=' ';
			}
			
			// $survey="View Photo";

			$remove = array("\n", "\r\n", "\r");

			$data.='"'.ucwords($auRec[$i]->distributor_name).'"';
			$data.="\t";

			$data.='"'.ucwords($auRec[$i]->distributor_code).'"';
			$data.="\t";

			$data.='"'.ucwords($auRec[$i]->salesman_name).'"';
			$data.="\t";

			// $data.='"'.ucwords($auRec[$i]->salesman_code).'"';
			// $data.="\t";

			$data.='"'.ucwords($auRec[$i]->division_name).'"';
			$data.="\t";

			$data.='"'.ucwords($auRec[$i]->brand_name).'"';
			$data.="\t";

			// $data.='"'.ucwords($auRec[$i]->relationship_code).'"';
			// $data.="\t";

			$data.='"'.ucwords($auRec[$i]->country_name).'"';
			$data.="\t";

			$data.='"'.ucwords($auRec[$i]->region_name).'"';
			$data.="\t";

			$data.='"'.ucwords($auRec[$i]->zone_name).'"';
			$data.="\t";

			$data.='"'.ucwords($auRec[$i]->state_name).'"';
			$data.="\t";

			$data.='"'.ucwords($auRec[$i]->city_name).'"';
			$data.="\t";

			$data.='"'.ucwords(str_replace($remove, ' ',$auRec[$i]->market_name)).'"';
			$data.="\t";
			

			// $data.='"'.ucwords(str_replace($remove, ' ',$auRec[$i]->taluka_name)).'"';
			// $data.="\t";

			

			$data.='"'.ucwords(str_replace($remove, ' ',$auRec[$i]->zipcode)).'"';
			$data.="\t";

			$data.='"'.ucwords(str_replace($remove, ' ',$auRec[$i]->distributor_address)).'"';
			$data.="\t";

			$data.='"'.ucwords(str_replace($remove, ' ',$auRec[$i]->distributor_address2)).'"';
			$data.="\t";

			$data.='"'.$auRec[$i]->sms_number.'"';
			$data.="\t";

			// $data.='"'.$survey.'"';
			// $data.="\t";

			$data.='"'.$auRec[$i]->distributor_phone_no.'"';
			$data.="\t";

			$data.='"'.$auRec[$i]->distributor_phone_no2.'"';
			$data.="\t";

			$data.='"'.$auRec[$i]->distributor_phone_no3.'"';
			$data.="\t";

			// $data.='"'.$auRec[$i]->distributor_leadline_no.'"';
			// $data.="\t";

			$data.='"'.ucwords($auRec[$i]->contact_person).'"';
			$data.="\t";

			$data.='"'.str_replace($remove, ' ',$auRec[$i]->contact_number).'"';
			$data.="\t";

			$data.='"'.ucwords($auRec[$i]->contact_person2).'"';
			$data.="\t";

			$data.='"'.$auRec[$i]->contact_number2.'"';
			$data.="\t";

			$data.='"'.ucwords($auRec[$i]->contact_person3).'"';
			$data.="\t";

			$data.='"'.$auRec[$i]->contact_number3.'"';
			$data.="\t";

			// $data.='"'.$auRec[$i]->username.'"';
			// $data.="\t";

			$data.='"'.$auRec[$i]->distributor_email.'"';
			$data.="\t";

			$data.='"'.$auRec[$i]->distributor_email2.'"';
			$data.="\t";

			$data.='"'.$auRec[$i]->distributor_email3.'"';
			$data.="\t";

			$data.='"'.$distributor_dob.'"';
			$data.="\t";

			// $data.='"'.$start_date.'"';
			// $data.="\t";

			// $data.='"'.$end_date.'"';
			// $data.="\t";

			$data.='"'.$sts.'"';	
			$data.="\t";

			$data.='"'.$auRec[$i]->aadhar_no.'"';
			$data.="\t";

			$data.='"'.$auRec[$i]->pan_no.'"';
			$data.="\t";

			$data.='"'.$auRec[$i]->tin_number.'"';
			$data.="\t";

			$data.='"'.$auRec[$i]->credit_limit.'"';
			$data.="\n";

		}						
	}
	header("Content-type: application/octet-stream");
	header("Content-Disposition: attachment; filename=\"distributors_list.xls\"");		
	echo $data;
	exit;
	}
	
/******************************************** End Export Distributor ******************************************************************/	
/******************************************** Start Export Retailor ******************************************************************/	
	
	
if(isset($_GET['export_relailer_list'])){		
	///$data="Retailer Name \t Retailer Class \t Division \t Retailer Type \t Retailer Channel \t Distributor \t Interested \t State \t District \t District Code \t Taluka \t Taluka Code \t City \t City Code \t Zipcode \t Address1 \t Address2 \t Phone Number1 \t Phone Number2 \t Landline Number \t Contact Person1 \t Contact Person Number1 \t Contact Person2 \t Contact Person Number2 \t Email ID1 \t Email ID2\t Aadhar Number \t Pan Number \t Retailer Code\n";

	/*$data="Dealer Name \t Division \t Brand \t Dealer Code \t Distributor \t App Version \t State \t District \t Pincode/Zipcode \t Map \t Photo \t Address 1 \t Address 2 \t Phone Number 1 \t Phone Number 2 \t Landline Number \t Aadhar Number \t Pan Number \t Contact Person 1 \t Contact Number 1 \t Contact Person 2 \t Contact Number 2 \t Username \t Email ID 1 \t Email ID 2 \t Date of Birth \t Start Date \t End Date \t Dealer Status \t Login Status\n";*/ //added by anurag@13-02-2017


	if($_SESSION['userLoginType'] == 3){
		$data="Dealer Name \t Division \t Brand \t Dealer Code \t Country\t Region\t Zone \t State \t District \t Area \t Pincode \t Address 1 \t Address 2 \t Phone Number 1 \t Phone Number 2 \t Aadhar Number \t Pan Number \t Contact Person 1 \t Contact Number 1 \t Contact Person 2 \t Contact Number 2 \t Email ID 1 \t Email ID 2 \t Dealer Status \t Date of Birth \n";
	}else{
		$data="Dealer Name \t Dealer Code \t Distributor \t Salesman \t Division \t Brand \t Country\t Region\t Zone \t State \t District \t Area \t Pincode \t Address 1 \t Address 2 \t Phone Number 1 \t Phone Number 2 \t Aadhar Number \t Pan Number \t Party Strength \t Contact Person 1 \t Contact Number 1 \t Contact Person 2 \t Contact Number 2 \t Email ID 1 \t Email ID 2 \t Dealer Status \t Date of Birth \n";
	}

	/*$salsList = $_objArrayList->SalesmanArrayList(); // For Admin and Salesman Login
	$divisionList = $_objArrayList->getAllDivisonOfSelectedSalesmen($salsList);
	$divisionFilters = $_objAdmin->getDivisionCondition($divisionList, 'r');*/

	$divisionFilters = "";
		
		if($_SESSION['userLoginType'] == 5) {
			$salDiv = $_objAdmin->_getSelectList('table_salesman'," division_id",''," AND salesman_id='".$_SESSION['salesmanId']."'");
		    if(sizeof($salDiv) > 0)
		   	 $divisionFilters = " AND r.division_id =".$salDiv[0]->division_id;
		}

	    if($_SESSION['userLoginType'] == 3){
	        $disDiv = $_objAdmin->_getSelectList('table_distributors'," division_id",''," AND distributor_id='".$_SESSION['distributorId']."'");
	        if(sizeof($disDiv) > 0)	
	            $divisionFilters = " AND r.division_id =".$disDiv[0]->division_id;

	           	// Find retailerId
	           	$retRec = $_objAdmin->_getSelectList('table_retailer',"GROUP_CONCAT( retailer_id ) AS retailer_id ",''," AND distributor_id='".$_SESSION['distributorId']."'");
	          
	          	$retId = $retRec[0]->retailer_id;
	         // 	echo $retId; exit;
	          	
           		$retailerCondition = " AND r.retailer_id IN($retId)";
        }


	// $condi=	" r.new='' and r.status!='D' $divisionFilters and r.account_id='".$_SESSION['accountId']."' ORDER BY r.retailer_name";

        $condi=	" r.new='' AND r.status!='D' $divisionFilters $retailerCondition AND r.account_id='".$_SESSION['accountId']."' ORDER BY r.retailer_name";

	
	/*$auRec=$_objAdmin->_getSelectList('table_retailer as r 
		left join table_division AS dV ON dV.division_id = r.division_id
		left join table_brands AS tb ON tb.brand_id = r.brand_id
		left join table_retailer_app_user AS AP ON AP.retailer_id = r.retailer_id   
		LEFT JOIN table_relationship as rr on rr.relationship_id=r.relationship_id 
		left join table_account as a on a.account_id=r.account_id 
		left join table_web_users as w on w.retailer_id=r.retailer_id 
		left join state as s on s.state_id=r.state 
		left join city as c on c.city_id=r.city 
		left join table_retailer_channel_master as cm on r.channel_id=cm.channel_id 
		left join table_retailer_type_master as tm on tm.type_id=r.type_id 
		left JOIN table_salesman AS sal ON sal.salesman_id = r.salesman_id
		left join table_distributors as d on d.distributor_id=r.distributor_id 
		left join table_markets as m on m.market_id=r.market_id 
		left join table_taluka as t on t.taluka_id=r.taluka_id',"r.*,w.username,cm.channel_name,d.distributor_name,w.email_id,w.web_user_id,sal.salesman_name,sal.salesman_code,rr.relationship_code,tm.type_name, w.status as loginStatus,s.state_name,c.city_name,dV.division_name, t.taluka_name,m.market_name,AP.app_version,tb.brand_name",'',$condi);*/ // added by anurag@13-02-2017

	$auRec=$_objAdmin->_getSelectList('table_retailer as r 
		left join table_division AS dV ON dV.division_id = r.division_id
		left join table_brands AS tb ON tb.brand_id = r.brand_id
		left join table_retailer_app_user AS AP ON AP.retailer_id = r.retailer_id   
		LEFT JOIN table_relationship as rr on rr.relationship_id=r.relationship_id 
		left join table_account as a on a.account_id=r.account_id 
		left join table_web_users as w on w.retailer_id=r.retailer_id 
		left join state as s on s.state_id=r.state 
		left join city as c on c.city_id=r.city 
		left join table_retailer_channel_master as cm on r.channel_id=cm.channel_id 
		left join table_retailer_type_master as tm on tm.type_id=r.type_id 
		left JOIN table_salesman AS sal ON sal.salesman_id = r.salesman_id
		left join table_distributors as d on d.distributor_id=r.distributor_id 
		left join table_markets as m on m.market_id=r.market_id 
		left join country as cn on r.country = cn.country_id
		left join table_region as tr on r.region = tr.region_id
		left join table_zone as tz on r.zone = tz.zone_id', "r.*,w.username,cm.channel_name,d.distributor_name,w.email_id,w.web_user_id,sal.salesman_name,sal.salesman_code,rr.relationship_code,tm.type_name, w.status as loginStatus,s.state_name,c.city_name,dV.division_name, m.market_name,cn.country_name,tr.region_name,tz.zone_name,AP.app_version,tb.brand_name",'',$condi);


	if(is_array($auRec)){
		for($i=0;$i<count($auRec);$i++){

			$start_date=$_objAdmin->_changeDate($auRec[$i]->start_date);
			if($auRec[$i]->retailer_dob=="0000-00-00"){
				$retailer_dob="-";
			}
			else{$retailer_dob=$_objAdmin->_changeDate($auRec[$i]->retailer_dob);}
		
			//end date
			if($auRec[$i]->status=='I' ){
				$end_date=$_objAdmin->_changeDate($auRec[$i]->end_date);
			} else {
				$end_date=' ';
			}

			$sts=($auRec[$i]->status=='A')?"Active":"Inactive";

			//login status
			if($auRec[$i]->web_user_id!=''){
				if($auRec[$i]->status=='A'){
					$LogStatus=($auRec[$i]->loginStatus=='A')?"Active":"Inactive";
				} else {
					$LogStatus='Inactive';
				}
			} else {
				$LogStatus=' ';
			}		

			$remove = array("\n", "\r\n", "\r");

		//$data.="".$auRec[$i]->retailer_name."\t".$auRec[$i]->relationship_code."\t".$auRec[$i]->type_name."\t".$auRec[$i]->channel_name."\t".$auRec[$i]->distributor_name."\t".strtoupper($auRec[$i]->display_outlet)."\t".$auRec[$i]->state_name."\t".$auRec[$i]->city_name."\t".$auRec[$i]->city_code."\t".$auRec[$i]->taluka_name."\t".$auRec[$i]->taluka_code."\t".$auRec[$i]->market_name."\t".$auRec[$i]->market_code."\t".$auRec[$i]->zipcode."\t".$auRec[$i]->retailer_address."\t".$auRec[$i]->retailer_address2."\t".$auRec[$i]->retailer_phone_no."\t".$auRec[$i]->retailer_phone_no2."\t".$auRec[$i]->retailer_leadline_no."\t".$auRec[$i]->contact_person."\t".$auRec[$i]->contact_number."\t".$auRec[$i]->contact_person2."\t".$auRec[$i]->contact_number2."\t".$auRec[$i]->email_id."\t".$auRec[$i]->email_id2."\t".$auRec[$i]->aadhar_no."\t".$auRec[$i]->pan_no."\t".$auRec[$i]->retailer_code."\n";

			// Retailer Name
			$data.='"'.$auRec[$i]->retailer_name.'"';
			$data.="\t";

			//  Retailer Code
			$data.='"'.$auRec[$i]->retailer_code.'"';
			$data.="\t";

			// Distributor
			if($_SESSION['userLoginType'] != 3){
				$data.='"'.$auRec[$i]->distributor_name.'"';
				$data.="\t";

				$data.='"'.$auRec[$i]->salesman_name.'"';
				$data.="\t";
			}

			// Retailer Class
			// $data.='"'.$auRec[$i]->relationship_code.'"';
			// $data.="\t";

			// Division
			$data.='"'.$auRec[$i]->division_name.'"';
			$data.="\t";

			// Brands
			$data.='"'.$auRec[$i]->brand_name.'"';
			$data.="\t";

			

			// Retailer Type
			// $data.='"'.$auRec[$i]->type_name.'"';
			// $data.="\t";

			// Retailer Channel
			// $data.='"'.$auRec[$i]->channel_name.'"';
			// $data.="\t";

			// Salesman Name 
			// $data.='"'.$auRec[$i]->salesman_name.'"';
			// $data.="\t";

			// Salesman Code 
			// $data.='"'.$auRec[$i]->salesman_code.'"';
			// $data.="\t";



			// Interested
			/*$data.='"'.strtoupper($auRec[$i]->display_outlet).'"';
			$data.="\t";*/

			//App Version
			// $data .= ",'".addslashes($auRec[$i]->app_version)."'";
			// $data.="\t";
			// $data.='"'.$auRec[$i]->app_version.'"';
			// $data.="\t";

			// Country Name
			$data.='"'.$auRec[$i]->country_name.'"';
			$data.="\t";
			// Region Name
			$data.='"'.$auRec[$i]->region_name.'"';
			$data.="\t";
			// Zone Name
			$data.='"'.$auRec[$i]->zone_name.'"';
			$data.="\t";	

			// State
			$data.='"'.$auRec[$i]->state_name.'"';
			$data.="\t";

			// District
			$data.='"'.$auRec[$i]->city_name.'"';
			$data.="\t";

			// Taluka
			/*$data.='"'.$auRec[$i]->taluka_name.'"';
			$data.="\t";*/


			// Area
			$data.='"'.$auRec[$i]->market_name.'"';
			$data.="\t";

			// Pincode
			$data.='"'.$auRec[$i]->zipcode.'"';
			$data.="\t";

			// Map
			// $data.='"'.$map.'"';
			// $data.="\t";

			// // Photo
			// $data.='"'.$photo.'"';
			// $data.="\t";

			// Address 1
			$data.='"'.$auRec[$i]->retailer_address.'"';
			$data.="\t";

			// Address 2
			$data.='"'.$auRec[$i]->retailer_address2.'"';
			$data.="\t";

			// Phone Number 1
			$data.='"'.$auRec[$i]->retailer_phone_no.'"';
			$data.="\t";

			// Phone Number 2
			$data.='"'.$auRec[$i]->retailer_phone_no2.'"';
			$data.="\t";

			// Landline Number
			// $data.='"'.$auRec[$i]->retailer_leadline_no.'"';
			// $data.="\t";

			// Aadhar Number
			$data.='"'.$auRec[$i]->aadhar_no.'"';
			$data.="\t";

			// Pan Number
			$data.='"'.$auRec[$i]->pan_no.'"';		
			$data.="\t";

			// Party Strength
			if($_SESSION['userLoginType'] != 3){
			$data.='"'.$auRec[$i]->party_strength.'"';
			$data.="\t";
			}

			// Contact Person 1
			$data.='"'.$auRec[$i]->contact_person.'"';
			$data.="\t";

			// Contact Number 1
			$data.='"'.$auRec[$i]->contact_number.'"';
			$data.="\t";

			// Contact Person 2
			$data.='"'.$auRec[$i]->contact_person2.'"';
			$data.="\t";

			// Contact Number 2
			$data.='"'.$auRec[$i]->contact_number2.'"';
			$data.="\t";

			// Username
			// $data.='"'.$auRec[$i]->username.'"';
			// $data.="\t";

			// Email ID 1
			//$data.='"'.$auRec[$i]->email_id.'"';
			$data.='"'.$auRec[$i]->retailer_email.'"';
			$data.="\t";

			// Email ID 2
			//$data.='"'.$auRec[$i]->email_id2.'"';
			$data.='"'.$auRec[$i]->retailer_email2.'"';
			$data.="\t";

			
			
			// Start Date
			// $data.='"'.str_replace($remove, ' ',$start_date).'"';
			// $data.="\t";

			// End Date
			// $data.='"'.str_replace($remove, ' ',$end_date).'"';
			// $data.="\t";

			// Retailer Status
			$data.='"'.$sts.'"';
			$data.="\t";

			// // Login Status
			// $data.='"'.$LogStatus.'"';
			// $data.="\t";
							
			// Date of Birth
			$data.='"'.str_replace($remove, ' ',$retailer_dob).'"';
			$data.="\n";

		}						
	}
	header("Content-type: application/octet-stream");
	header("Content-Disposition: attachment; filename=\"dealer_list.xls\"");		
	echo $data;
	exit;
	}
	
/******************************************** End Export Retailor ******************************************************************/
/******************************************** Start Export Salesman ******************************************************************/	
	
if(isset($_GET['export_salesman_list'])){		
	//$data="Salesman Name \t State \t City \t City Code \t Address \t Email ID \t Phone Number\t UserName\t Division\n";
	/*$data ="Salesman Name \t User Level \t Reporting to \t Reporting Person \t Username \t Salesman Code \t Segment \t Email ID \t Phone Number \t State \t City \t City Code \t Address \t Start Date \t End Date \t Salesman Status \t Login Status\n";*/

	$data ="Salesman Name \t User Level \t Reporting to \t Reporting Person \t Username \t Salesman Code \t Segment \t Email ID \t Phone Number \t State \t City \t City Code \t Address \t Total no. of Distributors \t Total no. of Dealers \t Start Date \t End Date \t Salesman Status \t Login Status\n";

	// $condi=	" sl.status='A' and sl.account_id='".$_SESSION['accountId']."'";
	// $auRec=$_objAdmin->_getSelectList('table_salesman as sl LEFT JOIN table_division AS TD ON TD.division_id = sl.division_id left join state as s on sl.state=s.state_id left join table_web_users as w on w.salesman_id=sl.salesman_id left join city as c on sl.city=c.city_id',"sl.*,s.state_name,w.email_id,w.username,c.city_name, TD.division_name ",'',$condi);

	$salsList = $_objArrayList->SalesmanArrayList(); // For Admin and Salesman Login
	$salesman = $_objArrayList->getSalesCondition($salsList);
	// $divisionList = $_objArrayList->getAllDivisonOfSelectedSalesmen($salsList);
	// $divisionFilters = $_objAdmin->getDivisionCondition($divisionList, 'r');

	// $condi=	" s.status='A' $salesman and s.account_id='".$_SESSION['accountId']."'";

	// $auRec=$_objAdmin->_getSelectList('table_salesman as s 
	// 	LEFT JOIN table_division AS TD ON TD.division_id = s.division_id 
	// 	left join state as ST on ST.state_id = s.state 
	// 	left join table_web_users as w on w.salesman_id = s.salesman_id 
	// 	left join city as c on c.city_id = s.city',
	// 	"s.*,ST.state_name,w.email_id,w.username,c.city_name, TD.division_name ",'',$condi);




	/*$auRec =$_objAdmin->_getSelectList('table_salesman AS s
		LEFT JOIN table_web_users AS w ON w.salesman_id = s.salesman_id 
		LEFT JOIN table_salesman_hierarchy_relationship AS SH ON SH.salesman_id = s.salesman_id 
		LEFT JOIN table_salesman_hierarchy AS H ON H.hierarchy_id = SH.hierarchy_id	
		LEFT JOIN table_salesman_hierarchy AS H2 ON H2.hierarchy_id = SH.rpt_hierarchy_id 
		LEFT JOIN table_salesman AS S2 ON S2.salesman_id = SH.rpt_user_id 
		LEFT JOIN table_division as D ON D.division_id=s.division_id 
		left join state as ST on ST.state_id = s.state 
		left join city as c on c.city_id = s.city',"s.*,w.username,w.email_id,w.web_user_id,w.status as loginStatus, H.description AS des1, H2.description AS des2, S2.salesman_name AS rptPerson,D.division_name,ST.state_name,c.city_name",'',$condi);*/

	$condi=	" s.status!='D' $salesman ";

	$group_by = " GROUP BY s.salesman_id";

	$sort = " ORDER BY salesman_name ASC";

	$auRec =$_objAdmin->_getSelectList('table_salesman AS s
		LEFT JOIN table_web_users AS w ON w.salesman_id = s.salesman_id 
		LEFT JOIN table_salesman_hierarchy_relationship AS SH ON SH.salesman_id = s.salesman_id 
		LEFT JOIN table_salesman_hierarchy AS H ON H.hierarchy_id = SH.hierarchy_id	
		LEFT JOIN table_salesman_hierarchy AS H2 ON H2.hierarchy_id = SH.rpt_hierarchy_id 
		LEFT JOIN table_salesman AS S2 ON S2.salesman_id = SH.rpt_user_id 
		LEFT JOIN table_division as D ON D.division_id=s.division_id 
		left join state as ST on ST.state_id = s.state 
		left join city as c on c.city_id = s.city
		LEFT JOIN table_distributors AS td ON td.salesman_id = s.salesman_id
		LEFT JOIN table_retailer AS r ON r.salesman_id = s.salesman_id', "s.*,w.username,w.email_id,w.web_user_id,w.status as loginStatus, H.description AS des1, H2.description AS des2, S2.salesman_name AS rptPerson,D.division_name,ST.state_name,c.city_name, count(distinct td.distributor_id ) AS ttl_distributors, count(distinct r.retailer_id ) AS ttl_dealers",'',$condi.$group_by.$sort);

	if(is_array($auRec)){

		for($i=0;$i<count($auRec);$i++){

			$start_date=$_objAdmin->_changeDate($auRec[$i]->start_date);	

			//end date
			if($auRec[$i]->status=='I' ){
				$end_date=$_objAdmin->_changeDate($auRec[$i]->end_date);
			} else {
				$end_date=' ';
			}
			//end date

			$sts=($auRec[$i]->status=='A')?"Active":"Inactive";
			
			//login status
			if($auRec[$i]->web_user_id!=''){
				if($auRec[$i]->status=='A'){
					$LogSts=($auRec[$i]->loginStatus=='A')?"Active":"Inactive";
				} else {
					$LogSts='Inactive';
				}
			} else {
				$LogSts=' ';
			}

			$remove = array("\n", "\r\n", "\r");

			$data.='"'.$auRec[$i]->salesman_name.'"';
			$data.="\t";

			$data.='"'.$auRec[$i]->des1.'"';
			$data.="\t";

			$data.='"'.$auRec[$i]->des2.'"';
			$data.="\t";

			$data.='"'.$auRec[$i]->rptPerson.'"';
			$data.="\t";

			$data.='"'.$auRec[$i]->username.'"';
			$data.="\t";

			$data.='"'.$auRec[$i]->salesman_code.'"';
			$data.="\t";

			$data.='"'.$auRec[$i]->division_name.'"';
			$data.="\t";

			$data.='"'.$auRec[$i]->email_id.'"';
			$data.="\t";

			$data.='"'.$auRec[$i]->salesman_phome_no.'"';
			$data.="\t";

			$data.='"'.$auRec[$i]->state_name.'"';
			$data.="\t";

			$data.='"'.$auRec[$i]->city_name.'"';
			$data.="\t";

			$data.='"'.$auRec[$i]->city_code.'"';
			$data.="\t";

			$data.='"'.ucwords(str_replace($remove, ' ',$auRec[$i]->salesman_address)).'"';
			$data.="\t";

			$data.='"'.$auRec[$i]->ttl_distributors.'"';
			$data.="\t";

			$data.='"'.$auRec[$i]->ttl_dealers.'"';
			$data.="\t";

			$data.='"'.$start_dates.'"';
			$data.="\t";

			$data.='"'.$end_date.'"';
			$data.="\t";

			$data.='"'.$sts.'"';
			$data.="\t";		
			
			
			$data.='"'.$LogSts.'"';
			$data.="\n";

		}						
	}
	header("Content-type: application/octet-stream");
	header("Content-Disposition: attachment; filename=\"salesman_list.xls\"");		
	echo $data;
	exit;
	}
	
/******************************************** End Export Salesman ******************************************************************/	
/******************************************** Start Export Route ******************************************************************/
if(isset($_GET['export_route'])){		
	$data="Route Name\n";
	$condi=	" status='A' ";
	$auRec=$_objAdmin->_getSelectList('table_route',"*",'',$condi);
	if(is_array($auRec)){
		for($i=0;$i<count($auRec);$i++){
		$data.="".$auRec[$i]->route_name."\n";
		}						
	}
	header("Content-type: application/octet-stream");
	header("Content-Disposition: attachment; filename=\"route_list.xls\"");		
	echo $data;
	exit;
	}
	
/******************************************** End Export Route ******************************************************************/
/******************************************** Start Export Manage User ******************************************************************/

if(isset($_GET['export_manageuser_list'])){		
	$data="Name \t User Name \t Email ID \t Phone Number\t Manage Items \t Manage Distributors \t Manage Retailer \t Manage Salesman \t Manage Route\t Manage Retailer Relationship\n";
	$condi=	" a.status='A' and a.account_id='".$_SESSION['accountId']."' and view_activity_report!='Yes'";
	$auRec=$_objAdmin->_getSelectList('table_account_admin as a left join table_web_users as w on w.operator_id=a.operator_id',"a.*,w.email_id,w.username",'',$condi);
	if(is_array($auRec)){
		for($i=0;$i<count($auRec);$i++){
		$data.="".$auRec[$i]->operator_name."\t".$auRec[$i]->username."\t".$auRec[$i]->email_id."\t".$auRec[$i]->operator_phone_number."\t".$auRec[$i]->manage_items."\t".$auRec[$i]->manage_distributors."\t".$auRec[$i]->manage_retailer."\t".$auRec[$i]->manage_salesman."\t".$auRec[$i]->manage_route."\t".$auRec[$i]->manage_retailer_relationship."\n";
		}						
	}
	header("Content-type: application/octet-stream");
	header("Content-Disposition: attachment; filename=\"manage_users_list.xls\"");		
	echo $data;
	exit;
	}
	
/******************************************** End Export Manage User ******************************************************************/
/******************************************** Start Export Message ******************************************************************/
	
	
if(isset($_GET['export_message'])){		
	$data="Salesman Name \t Subject \t Message\t Reply\t Send Date\n";
	$condi=	" m.status='A' and s.account_id=".$_SESSION['accountId']." order by m.message_id desc ";
	$auRec=$_objAdmin->_getSelectList2('table_message as m left join table_salesman as s on s.salesman_id=m.salesman_id',"m.*,s.salesman_name ",'',$condi);
	if(is_array($auRec)){
		for($i=0;$i<count($auRec);$i++){
		$remove = array("\n", "\r\n", "\r");
		$data.="".$auRec[$i]->salesman_name."\t".$auRec[$i]->subject."\t".str_replace($remove, ' ',$auRec[$i]->message)."\t".str_replace($remove, ' ',$auRec[$i]->reply)."\t".$_objAdmin->_changeDate($auRec[$i]->send_date)."\n";
		}						
	}
	header("Content-type: application/octet-stream");
	header("Content-Disposition: attachment; filename=\"message_list.xls\"");		
	echo $data;
	exit;
	}
/******************************************** End Export Message ******************************************************************/
/******************************************** Start Export Salesman Activity ******************************************************************/
	
if(isset($_GET['export_act'])){	
	$data="Salesman Name: ".$_SESSION['SalNameAct']."\t From Date: ".$_SESSION['FromAct']."\t To Date: ".$_SESSION['ToAct']."\n";
	$data.="Date\t Day\t Market\t Total Calls\t Total Productive Calls\t Total No. of New Retailer\t Total Amount of Order\n";
	$auRet=$_objAdmin->_getSelectList('table_order',"date_of_order",''," salesman_id='".$_SESSION['SalAct']."' and (date_of_order BETWEEN '".date('Y-m-d', strtotime($_SESSION['FromAct']))."' AND '".date('Y-m-d', strtotime($_SESSION['ToAct']))."') Group by date_of_order ORDER BY date_of_order asc ");
	if(is_array($auRet)){
		for($i=0;$i<count($auRet);$i++)
		{
		$td1=$_objAdmin->_getSelectList('table_order as o left join table_retailer as r on o.retailer_id=r.retailer_id','count(order_id) as total_call, sum( o.total_invoice_amount ) as total_amt ',''," o.date_of_order='".$auRet[$i]->date_of_order."' and r.new='' and o.salesman_id='".$_SESSION['SalAct']."'"); 
		$td2=$_objAdmin->_getSelectList('table_order as o left join table_retailer as r on o.retailer_id=r.retailer_id','count(order_id) as productive_call',''," o.date_of_order='".$auRet[$i]->date_of_order."' and o.order_type != 'No' and r.new='' and o.salesman_id='".$_SESSION['SalAct']."'");		
		$td3=$_objAdmin->_getSelectList('table_retailer','count(retailer_id) as total_retailer',''," start_date='".$auRet[$i]->date_of_order."' and salesman_id='".$_SESSION['SalAct']."'");		
		$td4=$_objAdmin->_getSelectList('table_retailer','DISTINCT(retailer_location)',''," retailer_id in(Select DISTINCT(retailer_id) from table_order where salesman_id='".$_SESSION['SalAct']."' and date_of_order='".$auRet[$i]->date_of_order."') Order by retailer_location asc");
		for($a=0;$a<count($td4);$a++)
		{
		$market.=$comma.$td4[$a]->retailer_location;
		$comma=", ";
		}
		$data.="".$_objAdmin->_changeDate($auRet[$i]->date_of_order)."\t".date("l",strtotime($auRet[$i]->date_of_order))."\t".strtoupper($market)."\t".$td1[0]->total_call."\t".$td2[0]->productive_call."\t".$td3[0]->total_retailer."\t".$td1[0]->total_amt."\n";
		unset($market);
		unset($comma);
		}
	} else {
		$data.="Report Not Available";
	}
	header("Content-type: application/octet-stream");
	header("Content-Disposition: attachment; filename=\"Activity Report.xls\"");		
	echo $data;
	exit;
	}
	
/******************************************** End Export Salesman Activity ******************************************************************/		
/******************************************** Start Export Retailer Vise Report ******************************************************************/	
	
if(isset($_GET['export_ret_vise_report'])){	
	$CityName=$_objAdmin->_getSelectList2('city as c left join table_retailer as r on r.city=c.city_id','city_name',''," r.city='".$_SESSION['CityList']."'"); 
	if($_SESSION['MarketList']!=''){
	$list="r.retailer_location='".$_SESSION['MarketList']."'";
	$market=$_SESSION['MarketList'];
	} else {
	$list="r.city='".$_SESSION['CityList']."'";
	$market="All Market";
	}
	if($_SESSION['SnameList']!=''){
		$sNameId="AND o.salesman_id='".$_SESSION['SnameList']."'";
		}
	$data="City Name: ".$CityName[0]->city_name."\t Market Name: ".$market."\t From Date: ".$_SESSION['FromRetList']."\t To Date: ".$_SESSION['ToRetList']."\n";
	$auRet=$_objAdmin->_getSelectList('table_order as o left join table_retailer as r on o.retailer_id=r.retailer_id',"COUNT(o.retailer_id) as total",''," $list and (o.date_of_order BETWEEN '".date('Y-m-d', strtotime($_SESSION['FromRetList']))."' AND '".date('Y-m-d', strtotime($_SESSION['ToRetList']))."') $sNameId AND r.new='' GROUP BY o.retailer_id ORDER BY total desc LIMIT 1");
		if(is_array($auRet)){
		$data1="Retailer Name\t Market\t Total\t Pro / Tot\t % Share\t";
		$row_total=$auRet[0]->total;
			for($a=0;$a<$auRet[0]->total;$a++){	 
			$no=$a+1;
			$data1.="Date ".$no."\t Salesman\t Order Value ".$no."\t";
		}
		$data1.=" \n";
	$data.=$data1;
	$orderList=$_objAdmin->_getSelectList('table_order as o left join table_retailer as r on o.retailer_id=r.retailer_id',"sum(o.acc_total_invoice_amount) as total_amt,o.order_id,o.retailer_id,r.retailer_name,r.retailer_location",''," $list and (o.date_of_order BETWEEN '".date('Y-m-d', strtotime($_SESSION['FromRetList']))."' AND '".date('Y-m-d', strtotime($_SESSION['ToRetList']))."') $sNameId AND r.new='' GROUP BY o.retailer_id  Order by total_amt ".$_SESSION['order_by']."");
		if(is_array($orderList)){
			for($i=0;$i<count($orderList);$i++)
			{
			$data2=$orderList[$i]->retailer_name."\t ";
			$data2.=$orderList[$i]->retailer_location."\t ";
			$data2.=$orderList[$i]->total_amt."\t ";
			$orderTotal=$_objAdmin->_getSelectList('table_order as o left join table_retailer as r on o.retailer_id=r.retailer_id',"count(o.retailer_id) as total_cal",''," o.retailer_id='".$orderList[$i]->retailer_id."' and (o.date_of_order BETWEEN '".date('Y-m-d', strtotime($_SESSION['FromRetList']))."' AND '".date('Y-m-d', strtotime($_SESSION['ToRetList']))."') $sNameId AND r.new='' ");
			
			$orderTotalPro=$_objAdmin->_getSelectList('table_order as o left join table_retailer as r on o.retailer_id=r.retailer_id',"count(o.retailer_id) as total_cal_pro",''," o.retailer_id='".$orderList[$i]->retailer_id."' and (o.date_of_order BETWEEN '".date('Y-m-d', strtotime($_SESSION['FromRetList']))."' AND '".date('Y-m-d', strtotime($_SESSION['ToRetList']))."') $sNameId AND r.new='' AND order_type!='No' ");
			
			$data2.=$orderTotalPro[0]->total_cal_pro."/".$orderTotal[0]->total_cal."\t ";
			
			$Netorder=$_objAdmin->_getSelectList('table_order as o left join table_retailer as r on o.retailer_id=r.retailer_id',"sum(o.acc_total_invoice_amount) as net_amt",''," $list and (o.date_of_order BETWEEN '".date('Y-m-d', strtotime($_SESSION['FromRetList']))."' AND '".date('Y-m-d', strtotime($_SESSION['ToRetList']))."') $sNameId AND r.new='' ");
			$data2.=round($orderList[$i]->total_amt/$Netorder[0]->net_amt*100,2) ."% \t ";
			
			$orderDet=$_objAdmin->_getSelectList('table_order as o left join table_retailer as r on o.retailer_id=r.retailer_id left join table_salesman as s on o.salesman_id=s.salesman_id',"o.order_id,o.date_of_order,o.acc_total_invoice_amount,o.order_type,s.salesman_name",''," o.retailer_id='".$orderList[$i]->retailer_id."' and (o.date_of_order BETWEEN '".date('Y-m-d', strtotime($_SESSION['FromRetList']))."' AND '".date('Y-m-d', strtotime($_SESSION['ToRetList']))."') $sNameId AND r.new='' Order by o.date_of_order asc ");
				for($b=0;$b<count($orderDet);$b++)
				{
				$data2.=$_objAdmin->_changeDate($orderDet[$b]->date_of_order)."\t ";
				$data2.=$orderDet[$b]->salesman_name."\t ";
					if($orderDet[$b]->order_type!='No'){
					$data2.=$orderDet[$b]->acc_total_invoice_amount."\t ";
					} else {
					$data2.=$orderDet[$b]->acc_total_invoice_amount."\t ";
					}
				}  
				for($c=0;$c<$row_total-count($orderDet);$c++){
					$data2.="-\t ";
					$data2.="-\t ";
					$data2.="-\t ";
					}
					$orderAmount=$_objAdmin->_getSelectList('table_order as o left join table_retailer as r on o.retailer_id=r.retailer_id',"sum(o.acc_total_invoice_amount) as total_amt",''," o.retailer_id='".$orderList[$i]->retailer_id."' and (o.date_of_order BETWEEN '".date('Y-m-d', strtotime($_SESSION['FromRetList']))."' AND '".date('Y-m-d', strtotime($_SESSION['ToRetList']))."') $sNameId AND r.new='' ");
					$data2.="\n"; 
					
				$data.=$data2;
			}
		}
		
	
	} else {
	$data.="Report Not Available /n";
	}
	header("Content-type: application/octet-stream");
	header("Content-Disposition: attachment; filename=\"Retailer Wise Report.xls\"");		
	echo $data;
	exit;
	}

/******************************************** End Export Retailer Vise Report ******************************************************************/	


/******************************************** Start Export Distributor Vise Report ******************************************************************/	
	
if(isset($_GET['export_dist_vise_report'])){	
	$DistName=$_objAdmin->_getSelectList('table_distributors','distributor_name',''," distributor_id='".$_SESSION['DistList']."'"); 
	if($_SESSION['SalnameList']!=''){
		$sNameId="AND o.salesman_id='".$_SESSION['SalnameList']."'";
		}
		if($_SESSION['DistList']!=''){
			$list="d.distributor_id='".$_SESSION['DistList']."'";
			$dist=$DistName[0]->distributor_name;

		}
		else{
		$list="'1'";
		$dist="All Distributors";

		}
	$data="Distributor: ".$dist."\t From Date: ".$_SESSION['ToDisList']."\t To Date: ".$_SESSION['ToDisList']."\n";
	$auRet=$_objAdmin->_getSelectList('table_order as o left join table_distributors as d on o.distributor_id=d.distributor_id',"COUNT(o.distributor_id) as total",''," $list and (o.date_of_order BETWEEN '".date('Y-m-d', strtotime($_SESSION['FromDisList']))."' AND '".date('Y-m-d', strtotime($_SESSION['ToDisList']))."') $sNameId AND o.order_type!='No' GROUP BY o.distributor_id ORDER BY total desc LIMIT 1");
		if(is_array($auRet)){
		$data1="Distributor Name\t Total\t % Share\t";
		$row_total=$auRet[0]->total;
			for($a=0;$a<$auRet[0]->total;$a++){	 
			$no=$a+1;
			$data1.="Date ".$no."\t Salesman\t Order Value ".$no."\t";
		}
		$data1.=" \n";
	$data.=$data1;
	$orderList=$_objAdmin->_getSelectList('table_order as o left join table_distributors as d on o.distributor_id=d.distributor_id',"sum(o.acc_total_invoice_amount) as total_amt,o.order_id,o.distributor_id,d.distributor_name",''," $list and (o.date_of_order BETWEEN '".date('Y-m-d', strtotime($_SESSION['FromDisList']))."' AND '".date('Y-m-d', strtotime($_SESSION['ToDisList']))."') $sNameId AND o.order_type!='No' GROUP BY o.distributor_id  Order by total_amt ".$_SESSION['order_by']."");
		if(is_array($orderList)){
			for($i=0;$i<count($orderList);$i++)
			{
			$data2=$orderList[$i]->distributor_name."\t ";
			$data2.=$orderList[$i]->total_amt."\t ";
			$orderTotal=$_objAdmin->_getSelectList('table_order as o left join table_distributors as d on o.distributor_id=d.distributor_id',"count(o.distributor_id) as total_cal",''," o.distributor_id='".$orderList[$i]->distributor_id."' and (o.date_of_order BETWEEN '".date('Y-m-d', strtotime($_SESSION['FromDisList']))."' AND '".date('Y-m-d', strtotime($_SESSION['ToDisList']))."') $sNameId AND o.order_type!='No'");
			
			/*$orderTotalPro=$_objAdmin->_getSelectList('table_order as o left join table_retailer as r on o.retailer_id=r.retailer_id',"count(o.retailer_id) as total_cal_pro",''," o.retailer_id='".$orderList[$i]->retailer_id."' and (o.date_of_order BETWEEN '".date('Y-m-d', strtotime($_SESSION['FromRetList']))."' AND '".date('Y-m-d', strtotime($_SESSION['ToRetList']))."') $sNameId AND r.new='' AND order_type!='No' ");
			
			$data2.=$orderTotalPro[0]->total_cal_pro."/".$orderTotal[0]->total_cal."\t ";*/
			
			$Netorder=$_objAdmin->_getSelectList('table_order as o left join table_distributors as d on o.distributor_id=d.distributor_id',"sum(o.acc_total_invoice_amount) as net_amt",''," $list and (o.date_of_order BETWEEN '".date('Y-m-d', strtotime($_SESSION['FromDisList']))."' AND '".date('Y-m-d', strtotime($_SESSION['ToDisList']))."') $sNameId AND o.order_type!='No' ");
			$data2.=round($orderList[$i]->total_amt/$Netorder[0]->net_amt*100,2) ."% \t ";
			
			$orderDet=$_objAdmin->_getSelectList('table_order as o left join table_distributors as d on o.distributor_id=d.distributor_id left join table_salesman as s on o.salesman_id=s.salesman_id',"o.order_id,o.date_of_order,o.acc_total_invoice_amount,o.order_type,s.salesman_name",''," o.distributor_id='".$orderList[$i]->distributor_id."' and (o.date_of_order BETWEEN '".date('Y-m-d', strtotime($_SESSION['FromDisList']))."' AND '".date('Y-m-d', strtotime($_SESSION['ToDisList']))."') $sNameId AND o.order_type!='No' Order by o.date_of_order asc ");
				for($b=0;$b<count($orderDet);$b++)
				{
				$data2.=$_objAdmin->_changeDate($orderDet[$b]->date_of_order)."\t ";
				$data2.=$orderDet[$b]->salesman_name."\t ";
					if($orderDet[$b]->order_type!='No'){
					$data2.=$orderDet[$b]->acc_total_invoice_amount."\t ";
					} else {
					$data2.=$orderDet[$b]->acc_total_invoice_amount."\t ";
					}
				}  
				for($c=0;$c<$row_total-count($orderDet);$c++){
					$data2.="-\t ";
					$data2.="-\t ";
					$data2.="-\t ";
					}
					$orderAmount=$_objAdmin->_getSelectList('table_order as o left join table_distributors as d on o.distributor_id=d.distributor_id',"sum(o.acc_total_invoice_amount) as total_amt",''," o.distributor_id='".$orderList[$i]->distributor_id."' and (o.date_of_order BETWEEN '".date('Y-m-d', strtotime($_SESSION['FromDisList']))."' AND '".date('Y-m-d', strtotime($_SESSION['ToDisList']))."') $sNameId AND o.order_type!='No' ");
					$data2.="\n"; 
					
				$data.=$data2;
			}
		}
		
	
	} else {
	$data.="Report Not Available /n";
	}
	header("Content-type: application/octet-stream");
	header("Content-Disposition: attachment; filename=\"Distributor Wise Report.xls\"");		
	echo $data;
	exit;
	}

/******************************************** End Export Retailer Vise Report ******************************************************************/	

	
/******************************************** Start Export Item Vise Report ******************************************************************/	
	
	
if(isset($_GET['export_ret_item_vise_report'])){	
	$data="Dealer Name: ".$_SESSION['ItemRetName']."\n";
	$data.="Market: ".$_SESSION['ItemRetMarket']."\n";
	$data.="City: ".$_SESSION['ItemRetCity']."\n";
	$data.="From Date: ".$_SESSION['FromRetList']."\t To Date: ".$_SESSION['ToRetList']."\n";
	$auRet=$_objAdmin->_getSelectList('table_order as o left join table_salesman as s on o.salesman_id=s.salesman_id',"o.order_id,o.date_of_order,s.salesman_name",''," o.retailer_id='".$_SESSION['ItemRetID']."' and (o.date_of_order BETWEEN '".date('Y-m-d', strtotime($_SESSION['FromRetList']))."' AND '".date('Y-m-d', strtotime($_SESSION['ToRetList']))."') AND o.order_type!='No' order by o.date_of_order asc");
		if(is_array($auRet)){
		$data1="SNO. \t Item Code\t ";
		for($a=0;$a<count($auRet);$a++){
			$no=$a+1;
		$data1.="Visit ".$no.", Date:".$_objAdmin->_changeDate($auRet[$a]->date_of_order).",".$auRet[$a]->salesman_name."\t ";
		}
		$data1.="Total Quantity \t Total Amount\t % Share\n";
		$data.=$data1;
		$itemList=$_objAdmin->_getSelectList2('table_order_detail as d left join table_order as o on o.order_id=d.order_id left join table_item as i on i.item_id=d.item_id',"sum(acc_quantity) as total_qty,sum(d.acc_total) as amt_total,i.item_code,i.item_id",''," d.order_id in (SELECT order_id FROM table_order where o.retailer_id='".$_SESSION['ItemRetID']."' and (o.date_of_order BETWEEN '".date('Y-m-d', strtotime($_SESSION['FromRetList']))."' AND '".date('Y-m-d', strtotime($_SESSION['ToRetList']))."') ) GROUP BY d.item_id ORDER BY ".$_SESSION['List']."");
		
		$net_total_item=$_objAdmin->_getSelectList2('table_order_detail as d left join table_order as o on o.order_id=d.order_id',"sum(d.acc_quantity) as totalitem, sum(d.acc_total) as amt_total",''," o.retailer_id='".$_SESSION['ItemRetID']."' and (o.date_of_order BETWEEN '".date('Y-m-d', strtotime($_SESSION['FromRetList']))."' AND '".date('Y-m-d', strtotime($_SESSION['ToRetList']))."')");
		
			for($i=0;$i<count($itemList);$i++)
			{
			$num=$i+1;
			$data2=$num."\t".$itemList[$i]->item_code."\t";
				for($a=0;$a<count($auRet);$a++){
				$item=$_objAdmin->_getSelectList2('table_order_detail',"sum(acc_quantity) as total_qty",''," item_id='".$itemList[$i]->item_id."' and order_id='".$auRet[$a]->order_id."'");
				if(is_array($item)){
					$data2.=$item[0]->total_qty."\t";
				//echo $item[0]->acc_quantity;
					} else {
					$data2.="0 \t";
					}
				}
				$data2.=$itemList[$i]->total_qty."\t".$itemList[$i]->amt_total."\t".round($itemList[$i]->amt_total/$net_total_item[0]->amt_total*100,2)."% \n";
				$data.=$data2;
			}
			for($z=0;$z<count($auRet)+1;$z++){
			$data3.="\t";
			}
			$net_total_item=$_objAdmin->_getSelectList2('table_order_detail as d left join table_order as o on o.order_id=d.order_id',"sum(d.acc_quantity) as totalitem, sum(d.acc_total) as amt_total",''," o.retailer_id='".$_SESSION['ItemRetID']."' and (o.date_of_order BETWEEN '".date('Y-m-d', strtotime($_SESSION['FromRetList']))."' AND '".date('Y-m-d', strtotime($_SESSION['ToRetList']))."')");
			$data3.="Total \t".$net_total_item[0]->totalitem."\t".$net_total_item[0]->amt_total."\n";
			$data.=$data3;
		
	} else {
	$data.="Report Not Available";
	}
	header("Content-type: application/octet-stream");
	header("Content-Disposition: attachment; filename=\"Retailer Item Wise Report.xls\"");		
	echo $data;
	exit;
	}

if(isset($_GET['export_ret_his_report'])){	
	
	if($_SESSION['RetId']!=''){
	$list="r.retailer_id='".$_SESSION['RetId']."'";
	}
	//$Name=$_objAdmin->_getSelectList('table_retailer','retailer_name',''," retailer_id='".$_SESSION['RetId']."'"); 
	$data="From Date: ".$_SESSION['FromRetList']."\t To Date: ".$_SESSION['ToRetList']."\n";
	$auRet=$_objAdmin->_getSelectList('table_order as o left join table_retailer as r on o.retailer_id=r.retailer_id',"COUNT(o.retailer_id) as total",''," $list and (o.date_of_order BETWEEN '".date('Y-m-d', strtotime($_SESSION['FromRetList']))."' AND '".date('Y-m-d', strtotime($_SESSION['ToRetList']))."') AND r.new='' GROUP BY o.retailer_id ORDER BY total desc LIMIT 1");
		if(is_array($auRet)){
		$data1="Dealer Name\t Market\t ";
		$row_total=$auRet[0]->total;
			for($a=0;$a<$auRet[0]->total;$a++){	 
			$no=$a+1;
			$data1.="Date ".$no."\t Salesman\t Order Value ".$no."\t";
		}
		$data1.="Total\t Pro / Tot\n";
	$data.=$data1;
	$pro=array();
	$tot=array();
	$orderList=$_objAdmin->_getSelectList('table_order as o left join table_retailer as r on o.retailer_id=r.retailer_id',"o.order_id,o.retailer_id,r.retailer_name,r.retailer_location",''," $list and (o.date_of_order BETWEEN '".date('Y-m-d', strtotime($_SESSION['FromRetList']))."' AND '".date('Y-m-d', strtotime($_SESSION['ToRetList']))."') AND r.new='' GROUP BY o.retailer_id Order by retailer_location asc ");
		if(is_array($orderList)){
			for($i=0;$i<count($orderList);$i++)
			{
			$data2=$orderList[$i]->retailer_name."\t ";
			$data2.=$orderList[$i]->retailer_location."\t ";
			$orderDet=$_objAdmin->_getSelectList('table_order as o left join table_retailer as r on o.retailer_id=r.retailer_id left join table_salesman as s on o.salesman_id=s.salesman_id',"o.order_id,o.date_of_order,o.acc_total_invoice_amount,o.order_type,s.salesman_name",''," o.retailer_id='".$orderList[$i]->retailer_id."' and (o.date_of_order BETWEEN '".date('Y-m-d', strtotime($_SESSION['FromRetList']))."' AND '".date('Y-m-d', strtotime($_SESSION['ToRetList']))."') AND r.new='' Order by o.date_of_order asc ");
				for($b=0;$b<count($orderDet);$b++)
				{
				$tot[]=$b;
				$data2.=$_objAdmin->_changeDate($orderDet[$b]->date_of_order)."\t ";
				$data2.=$orderDet[$b]->salesman_name."\t ";
					if($orderDet[$b]->order_type!='No'){
					$pro[]=$orderDet[$b]->order_id;
					$data2.=$orderDet[$b]->acc_total_invoice_amount."\t ";
					} else {
					$data2.=$orderDet[$b]->acc_total_invoice_amount."\t ";
					}
				}  
				for($c=0;$c<$row_total-count($orderDet);$c++){
					$data2.="-\t ";
					$data2.="-\t ";
					$data2.="-\t ";
					}
					$orderAmount=$_objAdmin->_getSelectList('table_order as o left join table_retailer as r on o.retailer_id=r.retailer_id',"sum(o.acc_total_invoice_amount) as total_amt",''," o.retailer_id='".$orderList[$i]->retailer_id."' and (o.date_of_order BETWEEN '".date('Y-m-d', strtotime($_SESSION['FromRetList']))."' AND '".date('Y-m-d', strtotime($_SESSION['ToRetList']))."') AND r.new='' ");
					$tatal_pro=count($pro);
					$tatal_tot=count($tot);
					$data2.=$orderAmount[0]->total_amt."\t".$tatal_pro."/".$tatal_tot."\n"; 
					unset($pro); 
					unset($tot);
				$data.=$data2;
			}
		}
		
	
	} else {
	$data.="Report Not Available /n";
	}
	header("Content-type: application/octet-stream");
	header("Content-Disposition: attachment; filename=\"Retailer Wise Report.xls\"");		
	echo $data;
	exit;
	}

/******************************************** End Export Item Vise Report ******************************************************************/	
/******************************************** Start Export Retailer PHOTO ******************************************************************/

if(isset($_GET['export_relailer_photo'])){  
 $data="Dealer Name \t Number of Photos \t Address \t Market \t State \t City \t Contact Person \t Contact Person Number\n";
 $condi= " r.status='A' and r.account_id='".$_SESSION['accountId']."' and s.state_name!='' and c.city_name!='' ORDER BY s.state_name, c.city_name, r.retailer_name";
 $auRec=$_objAdmin->_getSelectList('table_retailer as r left join state as s on r.state=s.state_id left join city as c on r.city=c.city_id',"r.retailer_id,r.retailer_name,r.retailer_address,r.retailer_location,r.contact_person,r.contact_number,s.state_name,c.city_name",'',$condi);
 if(is_array($auRec)){
  for($i=0;$i<count($auRec);$i++){
  $photo_list=$_objAdmin->_getSelectList('table_image as i left join table_survey as s on s.survey_id=i.ref_id','count(i.image_url) as total_img',''," s.retailer_id='".$auRec[$i]->retailer_id."'  and i.image_type=3 ");
  if($photo_list[0]->total_img!=''){
  $photo=$photo_list[0]->total_img;
  } else {
  $photo=NULL;
  }
  $data.="".$auRec[$i]->retailer_name."\t";
  $data.="".$photo."\t";
  $data.="".$auRec[$i]->retailer_address."\t".$auRec[$i]->retailer_location."\t".$auRec[$i]->state_name."\t".$auRec[$i]->city_name."\t".$auRec[$i]->contact_person."\t".$auRec[$i]->contact_number."\n";
  }      
 }
 header("Content-type: application/octet-stream");
 header("Content-Disposition: attachment; filename=\"dealer_photo_list.xls\"");  
 echo $data;
 exit;
 }

/******************************************** End Export Retailer Report ******************************************************************/	
/************************************************************ Start Export Price Details ****************************************************/	
	
	
if(isset($_GET['export_price_list'])){		
	$data="State \t City \t Price Applicable\n";
	$condi=	" pca.account_id='".$_SESSION['accountId']."' ORDER BY s.state_name,cs.city_name";
	$auRec=$_objAdmin->_getSelectList('price_city_applicable as pca left join state as s on pca.state_id=s.state_id left join city as cs on pca.city_id=cs.city_id',"s.state_name,cs.city_name,pca.*",'',$condi);
	if(is_array($auRec)){
		for($i=0;$i<count($auRec);$i++){
		if($auRec[$i]->price_type=="1"){$price_type="MRP";} else if ($auRec[$i]->price_type=="2"){$price_type="DP";} else {$price_type="PTR";}
		$data.="".$auRec[$i]->state_name."\t".$auRec[$i]->city_name."\t".$price_type."\n";
		}						
	}
	header("Content-type: application/octet-stream");
	header("Content-Disposition: attachment; filename=\"price_details_list.xls\"");		
	echo $data;
	exit;
	}
	
	if(isset($_GET['trouble_tickets'])){		
	$data="Date \t Salesman Name \t Message\t Reply \t Reply Date \t Status\n";
	if($_SESSION['SalTroubleList']!=''){
		$salesman=" and t.salesman_id='".$_SESSION['SalTroubleList']."'";
	}
	if($_SESSION['FromTroubleList']!=''){
		$fromdate=" t.trouble_tickets_date >= '".$_SESSION['FromTroubleList']."'";
	}
	else
	{
		$fromdate=" t.trouble_tickets_date >= '".date("Y-m-d",strtotime("-1 day"))."'";
	}
	if($_SESSION['ToTroubleList']!=''){
		$todate=" and t.trouble_tickets_date <= '".$_SESSION['ToTroubleList']."'";
	}
	else
	{
		$todate=" and t.trouble_tickets_date <= '".date("Y-m-d",strtotime("-1 day"))."'";
	}
	$condi=	" $fromdate $todate $salesman and s.account_id=".$_SESSION['accountId']." Order by trouble_tickets_date desc";
	$auRec=$_objAdmin->_getSelectList2('table_trouble_tickets as t left join table_salesman as s on s.salesman_id=t.salesman_id',"t.*,s.salesman_name",'',$condi);
	if(is_array($auRec)){
		for($i=0;$i<count($auRec);$i++){
		/*if($auRec[$i]->reply_date!='0000-00-00'){
			$reply_date=date('Y-m-d', strtotime($auRec[$i]->reply_date));
			} else {
			$reply_date="";
			}*/
		$remove = array("\n", "\r\n", "\r");
		$data.="".$_objAdmin->_changeDate($auRec[$i]->trouble_tickets_date)."\t".$auRec[$i]->salesman_name."\t".str_replace($remove, ' ',$auRec[$i]->message)."\t".str_replace($remove, ' ',$auRec[$i]->reply)."\t".$_objAdmin->_changeDate($auRec[$i]->reply_date)."\t".$auRec[$i]->status."\n";
		//$data.="<br>";
		}						
	} else {
	$data.="Trouble Tickets List Not Available /n";
	}
	header("Content-type: application/octet-stream");
	header("Content-Disposition: attachment; filename=\"trouble_tickets_list.xls\"");		
	echo $data;
	exit;
	}
	
	
	
	// Export Quality Checker List
if(isset($_GET['export_dispatched_order_list'])){		
	$data="Distributor Name\t Category Name\t Item Name\t Item Code\t Item Quantity\t Date Of Dispatch\n";
	$condi=	" 1=1";
	$auRec=$_objAdmin->_getSelectList('table_dispatch_order as tdo left join table_distributors as d on d.distributor_id=tdo.distributor_id left join table_category as c on c.category_id=tdo.category_id left join table_item as i on i.item_id=tdo.item_id',"tdo.*,d.distributor_name,c.category_name,i.item_name,i.item_code,tdo.item_quantity,tdo.date_of_dispatch",'',$condi);
	if(is_array($auRec)){
		for($i=0;$i<count($auRec);$i++){
		$data.="".$auRec[$i]->distributor_name."\t".$auRec[$i]->category_name."\t".$auRec[$i]->item_name."\t".$auRec[$i]->item_code."\t".$auRec[$i]->item_quantity."\t".$auRec[$i]->date_of_dispatch."\n";
		}						
	}
	header("Content-type: application/octet-stream");
	header("Content-Disposition: attachment; filename=\"dispatch_order_list.xls\"");		
	echo $data;
	exit;
	}
	
	
	if(isset($_GET['export_admin_ret_his_report'])){	
	
	if($_SESSION['RetHistoryId']!=''){
	$list="r.retailer_id='".$_SESSION['RetHistoryId']."'";
	}
	//$Name=$_objAdmin->_getSelectList('table_retailer','retailer_name',''," retailer_id='".$_SESSION['RetId']."'"); 
	$data="From Date: ".$_SESSION['FromRetOrderList']."\t To Date: ".$_SESSION['ToRetOrderList']."\n";
	$auRet=$_objAdmin->_getSelectList('table_order as o left join table_retailer as r on o.retailer_id=r.retailer_id',"COUNT(o.retailer_id) as total",''," $list and (o.date_of_order BETWEEN '".date('Y-m-d', strtotime($_SESSION['FromRetOrderList']))."' AND '".date('Y-m-d', strtotime($_SESSION['ToRetOrderList']))."') AND r.new='' GROUP BY o.retailer_id ORDER BY total desc LIMIT 1");
		if(is_array($auRet)){
		$data1="Retailer Name\t Market\t ";
		$row_total=$auRet[0]->total;
			for($a=0;$a<$auRet[0]->total;$a++){	 
			$no=$a+1;
			$data1.="Date ".$no."\t Salesman\t Order Value ".$no."\t";
		}
		$data1.="Total\t Pro / Tot\n";
	$data.=$data1;
	$pro=array();
	$tot=array();
	$orderList=$_objAdmin->_getSelectList('table_order as o left join table_retailer as r on o.retailer_id=r.retailer_id',"o.order_id,o.retailer_id,r.retailer_name,r.retailer_location",''," $list and (o.date_of_order BETWEEN '".date('Y-m-d', strtotime($_SESSION['FromRetOrderList']))."' AND '".date('Y-m-d', strtotime($_SESSION['ToRetOrderList']))."') AND r.new='' GROUP BY o.retailer_id Order by retailer_location asc ");
		if(is_array($orderList)){
			for($i=0;$i<count($orderList);$i++)
			{
			$data2=$orderList[$i]->retailer_name."\t ";
			$data2.=$orderList[$i]->retailer_location."\t ";
			$orderDet=$_objAdmin->_getSelectList('table_order as o left join table_retailer as r on o.retailer_id=r.retailer_id left join table_salesman as s on o.salesman_id=s.salesman_id',"o.order_id,o.date_of_order,o.acc_total_invoice_amount,o.order_type,s.salesman_name",''," o.retailer_id='".$orderList[$i]->retailer_id."' and (o.date_of_order BETWEEN '".date('Y-m-d', strtotime($_SESSION['FromRetOrderList']))."' AND '".date('Y-m-d', strtotime($_SESSION['ToRetOrderList']))."') AND r.new='' Order by o.date_of_order asc ");
				for($b=0;$b<count($orderDet);$b++)
				{
				$tot[]=$b;
				$data2.=$_objAdmin->_changeDate($orderDet[$b]->date_of_order)."\t ";
				$data2.=$orderDet[$b]->salesman_name."\t ";
					if($orderDet[$b]->order_type!='No'){
					$pro[]=$orderDet[$b]->order_id;
					$data2.=$orderDet[$b]->acc_total_invoice_amount."\t ";
					} else {
					$data2.=$orderDet[$b]->acc_total_invoice_amount."\t ";
					}
				}  
				for($c=0;$c<$row_total-count($orderDet);$c++){
					$data2.="-\t ";
					$data2.="-\t ";
					$data2.="-\t ";
					}
					$orderAmount=$_objAdmin->_getSelectList('table_order as o left join table_retailer as r on o.retailer_id=r.retailer_id',"sum(o.acc_total_invoice_amount) as total_amt",''," o.retailer_id='".$orderList[$i]->retailer_id."' and (o.date_of_order BETWEEN '".date('Y-m-d', strtotime($_SESSION['FromRetOrderList']))."' AND '".date('Y-m-d', strtotime($_SESSION['ToRetOrderList']))."') AND r.new='' ");
					$tatal_pro=count($pro);
					$tatal_tot=count($tot);
					$data2.=$orderAmount[0]->total_amt."\t".$tatal_pro."/".$tatal_tot."\n"; 
					unset($pro); 
					unset($tot);
				$data.=$data2;
			}
		}
		
	
	} else {
	$data.="Report Not Available /n";
	}
	header("Content-type: application/octet-stream");
	header("Content-Disposition: attachment; filename=\"Retailer Wise Report.xls\"");		
	echo $data;
	exit;
	}

	
	
	
	if(isset($_GET['export_distributor_monthly_sale'])){	
	//echo hello;exit;
	
	$data=" Retailer\t Retailer Code \t Distributor \t Distributor Code \t Item\t Item Code\t Month\t Year\t Day1\t  Day2\t Day3\t Day4\t Day5\t Day6\t Day7\t Day8\t Day9\t Day10\t Day11\t Day12\t Day13\t Day14\t Day15\t Day16\t Day17\t Day18\t Day19\t Day20\t Day21\t Day22\t Day23\t Day24\t Day25\t Day26\t Day27\t Day28\t Day29\t Day30\t Day31\n";
	
	//echo $_SESSION['distributorIDSale'].$_SESSION['itemID'].$_SESSION['dismonth'].$_SESSION['disCyear'];
	
	if($_SESSION['distributorIDSale']!=''){
			$distributor=" and D.distributor_id ='".$_SESSION['distributorIDSale']."'";
		}
		
		if($_SESSION['itemID']!=''){
			$Item=" and V.item_id ='".$_SESSION['itemID']."'";
		}
		
		if($_SESSION['dismonth']!=''){
			$fromdate=" and V.month = '".$_SESSION['dismonth']."'";
		} else {
			$fromdate = "and V.month = ".date('m');
		}
		
		if($_SESSION['disCyear']!=''){
			$todate=" and V.year = '".$_SESSION['disCyear']."'";
		} else {
			$todate=" and V.year = ".date('Y');
		}
		
		$sortname = 'month';
		$sortorder = 'ASC';
		$sort = " ORDER BY $sortname $sortorder";
		
		$where = "  $distributor $Item $fromdate $todate  AND R.new=''";
		$groupby = " GROUP BY R.retailer_id , item_id, month";
		
		$auRec=$_objAdmin->_getSelectList('disretdailywiseitemreport AS V 
					LEFT JOIN table_retailer AS R ON V.retailer_id = R.retailer_id 
					LEFT JOIN table_distributors AS D ON V.distributor_id = D.distributor_id	
					LEFT JOIN table_item AS i ON V.item_id = i.item_id',	
			"V.retailer_id AS RID,
			 DATE_FORMAT(V.date_of_order, '%b') AS DOODR,
			 V.Day AS Day,
			 V.item_name AS item_name,
			 i.item_code AS item_code,
			 V.item_id AS item_id,
			 V.month AS month,
			 V.year AS year,
			 R.retailer_name AS retailer_name,
			 D.distributor_name As distributor_name,
			 D.distributor_code,
			 R.retailer_address,
			 R.retailer_code,
			 R.retailer_address2",'',$where.$groupby.$sort,'');
			 for($i=0;$i<count($auRec);$i++){

	$auRec2=$_objAdmin->_getSelectList('disretdailywiseitemreport AS V',"totalSaleUnit,Day,retailer_id,item_name",''," retailer_id = ".$auRec[$i]->RID." AND item_id = ".$auRec[$i]->item_id." AND month = ".$auRec[$i]->month."");

			$row1  = '-';
			$row2  = '-';
			$row3  = '-';
			$row4  = '-';
			$row5  = '-';
			$row6  = '-';
			$row7  = '-';
			$row8  = '-';
			$row9  = '-';
			$row10 = '-';
			$row11 = '-';
			$row12 = '-';
			$row13 = '-';
			$row14 = '-';
			$row15 = '-';
			$row16 = '-';
			$row17 = '-';
			$row18 = '-';
			$row19 = '-';
			$row20 = '-';
			$row21 = '-';
			$row22 = '-';
			$row23 = '-';
			$row24 = '-';
			$row25 = '-';
			$row26 = '-';
			$row27 = '-';
			$row28 = '-';
			$row29 = '-';
			$row30 = '-';
			$row31 = '-';

			for($j=0;$j<count($auRec2);$j++){
			
				${'row'.$auRec2[$j]->Day} = $auRec2[$j]->totalSaleUnit;

			}
			
		$data.="".$auRec[$i]->retailer_name. "\t".$auRec[$i]->retailer_code. "\t".$auRec[$i]->distributor_name."\t".$auRec[$i]->distributor_code."\t".$auRec[$i]->item_name."\t".$auRec[$i]->item_code."\t".$auRec[$i]->DOODR."\t".$auRec[$i]->year."\t".$row1."\t".$row2."\t".$row3."\t".$row4."\t".$row5."\t".$row6."\t".$row7."\t".$row8."\t".$row9."\t".$row10."\t".$row11."\t".$row12."\t".$row13."\t".$row14."\t".$row15."\t".$row16."\t".$row17."\t".$row18."\t".$row19."\t".$row20."\t".$row21."\t".$row22."\t".$row23."\t".$row24."\t".$row25."\t".$row26."\t".$row27."\t".$row28."\t".$row29."\t".$row30."\t".$row31."\n";
		}
			 
	header("Content-type: application/octet-stream");
	header("Content-Disposition: attachment; filename=\"Distributor Monthly Sale.xls\"");		
	echo $data;
	exit;
	}
	

	/*******
	* Edited By : Abhishek Jaiswal
	* Date 17-02-16
	* Desc : want to export same as grid report
	******/
	if(isset($_GET['export_order_list'])){
		$data = "Salesman Name\t Reporting To\t Order Status\t No-Order Reason\t Comments\t Total Invoice Amount\t Total No Of Items\t Quantity\t Dealer Name\t Dealer Pincode \t Order Number\t Date of Order\t Time of Order\t Salesperson Code\t Distributor Code\t Distributor Name\t Distributor Pincode \t Dealer Code\t Dealer Address\t State\tCity";


		$sort = " ORDER BY date_of_order desc,time_of_order desc";
		if($_SESSION['FromOrderList']!=''){
			$fromdate=" and o.date_of_order >= '".date('Y-m-d', strtotime($_SESSION['FromOrderList']))."'";
		} else {
			$fromdate=" and o.date_of_order >= '".date('Y-m-d')."'";
		}
		//for customer type
		/*if($_SESSION['customerType']!=''){
			if($_SESSION['customerType']==1){ // retailer
				$cust_type=" AND o.retailer_id!=0";
			} else if($_SESSION['customerType']==2){ // distributor
				$cust_type=" AND o.retailer_id=0";
			} else {
				$cust_type = "";
			}
		} else {
			$cust_type = "";
		}*/
		//for customer type  by: Maninder on 06th April 2016
		$divisionCondition = "";
		if($_SESSION['customerType']!=''){
		   if($_SESSION['customerType']==1){ // retailer
		   	$cust_type=" AND o.retailer_id!=0";
		   	if($_SESSION['customerClass']!='' && $_SESSION['customerClass']!='all'){
		   		$class_type = " AND r.relationship_id='".$_SESSION['customerClass']."'";
		   	}else{
		   		$class_type = "";
		   	}
		   	if($_SESSION['divisionCond']!='' && $_SESSION['divisionCond']!='all'){
		   		$divisionCondition = " AND r.division_id='".$_SESSION['divisionCond']."'";
		   	}else{
		   		$divisionCondition = "";
		   	}
		   	
		   }
		   else if($_SESSION['customerType']==2){ // distributor
		   	$cust_type=" AND o.retailer_id=0";
		   	if($_SESSION['customerClass']!='' && $_SESSION['customerClass']!='all'){
		   		$class_type = " AND d.relationship_id='".$_SESSION['customerClass']."'";
		   	}else{
		   		$class_type = "";
		   	}
		   	if($_SESSION['divisionCond']!='' && $_SESSION['divisionCond']!='all'){
		   		$divisionCondition = " AND d.division_id='".$_SESSION['divisionCond']."'";
		   	}else{
		   		$divisionCondition = "";
		   	}

		   } else {
		   	$cust_type = "";
		   	$class_type = "";
		   	$divisionCondition = "";
		   }
		}
		else
		{
			$cust_type = "";
			$class_type = "";
			$divisionCondition = "";
		}
		//for order
		if($_SESSION['orderStatus']!=''){
			if($_SESSION['orderStatus']==1){
				$odr_type=" AND o.order_type='Yes' AND o.order_status='A' ";
			} else if($_SESSION['orderStatus']==2){
				$odr_type=" AND o.order_type='No' AND o.order_status='A' ";
			} else if($_SESSION['orderStatus']==3){
				$odr_type=" AND o.order_status='I' ";
			} else if($_SESSION['orderStatus']==4){
				$odr_type=" AND o.order_status='D' ";
			} else if($_SESSION['orderStatus']==5){
				$odr_type=" AND o.order_type='Adhoc' AND o.order_status='A' ";
			} else {
				$odr_type = "";	
			}
		} else {
			$odr_type = "";
		}

		if($_SESSION['ToOrderList']!=''){
			$todate=" and o.date_of_order <= '".date('Y-m-d', strtotime($_SESSION['ToOrderList']))."'";
		} else {
			$todate=" and o.date_of_order <= '".date('Y-m-d')."'";
		}
		
		if($_SESSION['OrderBy']!=''){
			if($_SESSION['OrderBy']==1){
				$orderby="";
			}
			if($_SESSION['OrderBy']==2){
				$orderby=" and o.order_type='Adhoc' ";
			}
			if($_SESSION['OrderBy']==3){
				$orderby=" and o.order_type!='Adhoc' ";
			}
			if($_SESSION['OrderBy']==4){     //this condition added by maninder on 17-10-2016 for show only retailers order 
				$orderby=" and o.salesman_id = 0 AND o.retailer_id > 0"; 
			} 

		} else {
			$orderby="";
		}
		if( $_SESSION['userLoginType']==3){
			$disLogCond="o.distributor_id='".$_SESSION['distributorId']."' ";
		}
		if( $_SESSION['userLoginType']==4){ 
			$disLogCond="o.retailer_id='".$_SESSION['retailerId']."' "; 
		}

// If salesman login
		// if( $_SESSION['userLoginType']==5){
		// 	$salLogCond=" and o.salesman_id='".$_SESSION['salesmanId']."' ";
		// }


		// AJAY@2016-04-08
		  /*if($_SESSION['SalOrderList']!=''){
			   $salesman=" and s.salesman_id='".$_SESSION['SalOrderList']."'";
			  } else  if($_SESSION['userLoginType']==1){
				 $salesman = "";
				}*/ 

				if(isset($_SESSION['SalOrderList']) && $_SESSION['SalOrderList']!='' && $_SESSION['orderfilterBY'] == 1)
				{
					
					$salesman=" and s.salesman_id='".$_SESSION['SalOrderList']."'"; 
					
				}else if(isset($_SESSION['SalOrderList']) && $_SESSION['SalOrderList']!='' && $_SESSION['orderfilterBY'] == 2)
				{
					
					$salArrList=$_SESSION['SalOrderList'];	
					$filterby=$_SESSION['orderfilterBY'];
					$salesman = $_objArrayList->getSalesmanConsolidatedList($salArrList,$filterby);
					
				}else if($_SESSION['userLoginType']==1){
					$salesman = "";
				}
				else
				{
					$salesman = "";
				}


				$stateCondition = "";
				$cityCondition = "";
				$talukaCondition = "";

				if(isset($_SESSION['stateCust']) && $_SESSION['stateCust']!="all"){
					$stateCondition = " AND (r.state IN(".$_SESSION['stateCust'].") OR d.state IN(".$_SESSION['stateCust']."))";
				}

				if(isset($_SESSION['districtCust']) && $_SESSION['districtCust']!="all"){
					$cityCondition = " AND (r.city IN(".$_SESSION['districtCust'].") OR d.city IN(".$_SESSION['districtCust']."))";
				}


				if(isset($_SESSION['tehsilCust']) && $_SESSION['tehsilCust']!="all"){
					$talukaCondition = " AND ( r.taluka_id IN(".$_SESSION['tehsilCust'].") OR d.taluka_id IN(".$_SESSION['tehsilCust']."))";
				}

				if(isset($_SESSION['divisionCond']) && $_SESSION['divisionCond']!="all" && $_SESSION['customerType'] == 0){
					$divisionCondition = " AND ( r.division_id IN(".$_SESSION['divisionCond'].") OR d.division_id IN(".$_SESSION['divisionCond']."))";
		}//fileter added by Maninder@2016-08-25
		//$where = " 1=1 ";
		
		// $where = " AND (r.new = '' ||  d.new = '') $disLogCond $salLogCond $cust_type $class_type $stateCondition $cityCondition $talukaCondition $odr_type $salesman $fromdate $todate $orderby and o.account_id =".$_SESSION['accountId'];
		$getHierarchy = $_objAdmin->_getSelectList2('table_account_admin','hierarchy_id','',' operator_id = "'.$_SESSION['operatorId'].'"');  
		if($_SESSION['userLoginType'] == 2 && $getHierarchy[0]->hierarchy_id!=14 && $getHierarchy[0]->hierarchy_id!=9)
		{
		      //	echo"usertow";
		          // $where = " AND (r.new = '' ||  d.new = '') $disLogCond $cust_type $class_type $stateCondition
		          //  $cityCondition $talukaCondition $divisionCondition $odr_type $salesman $fromdate $todate $orderby
		          //   and o.account_id =".$_SESSION['accountId']." AND o.salesman_order_status='A' ";


		            // $where = " AND (r.new = '' ||  d.new = '') $disLogCond $cust_type $class_type $stateCondition
		            // $cityCondition $talukaCondition $divisionCondition  $fromdate $todate $orderby
		            // and o.account_id =".$_SESSION['accountId']." AND ((o.salesman_order_status='A')
		            // 	OR(o.salesman_order_status='U' And o.distributor_id!=0)) ";

			// $where = " AND (r.new = '' ||  d.new = '') $disLogCond $cust_type $class_type $stateCondition
			// $cityCondition $talukaCondition $divisionCondition  $fromdate $todate $orderby
			// AND o.salesman_order_status='A' ";  

			$where = " AND (r.new = '' ||  d.new = '') $disLogCond $cust_type $class_type $stateCondition
			$cityCondition $talukaCondition $divisionCondition  $fromdate $todate $orderby
			AND (case when o.salesman_id >0 then o.salesman_order_status = 'A' when o.salesman_id = 0 and o.retailer_id=0 and o.distributor_id>0 then o.salesman_order_status != 'A'  end) ";

		}
		else if($_SESSION['userLoginType'] == 4)
		{
			$where = " AND (r.new = '' ||  d.new = '')AND $disLogCond $cust_type $class_type $stateCondition $cityCondition $talukaCondition $divisionCondition $fromdate $todate $orderby
			AND o.salesman_order_status='U' "; 

		}
		else if($_SESSION['userLoginType'] == 3)
		{
			$where = " AND (r.new = '' ||  d.new = '')AND $disLogCond $cust_type $class_type $stateCondition $cityCondition $talukaCondition $divisionCondition $fromdate $todate $orderby
			AND o.salesman_order_status='U' "; 

		}
		else if($_SESSION['userLoginType'] == 7){

			$wearhouse=$_objAdmin->_getSelectList2('table_order_invoice_assign_wh AS wh 
				INNER JOIN table_warehouse AS w ON w.warehouse_id = wh.warhouse_id' ,
				'wh.warhouse_id as wid ,wh.order_id as oid',''," wh.warhouse_id =".$_SESSION['warehouseId']); 



			$ord_id=array();
			$wear_id=array();
			foreach($wearhouse as $wearhouse_details)
			{
				$wear_id=$wearhouse_details->id;
				$ord_id[]=$wearhouse_details->oid;

			}
			$matches = implode(',', $ord_id);

		           // if(!empty($matches) && $matches!="")
		           // {


			$where = " AND (r.new = '' ||  d.new = '') $disLogCond $cust_type $class_type $stateCondition
			$cityCondition $talukaCondition $divisionCondition $odr_type $salesman $fromdate $todate $orderby AND o.invoice_commercial_approval_status='A' AND o.order_id IN (".$matches.")"; 
		      //print_R($where);
		      //}
		}
		else if($_SESSION['userLoginType']==5){

			$where = "$stateCondition $cityCondition $talukaCondition $divisionCondition $odr_type $salesman $fromdate $todate AND ((o.salesman_id > 0 AND o.distributor_id > 0 AND o.retailer_id=0) OR (o.salesman_id > 0 AND o.distributor_id = 0 AND o.retailer_id>0)) $orderby "; 

		}
		else{
		  //$where = " 1=1 "; 

			$where = " AND (r.new = '' ||  d.new = '') $disLogCond $cust_type $class_type $stateCondition $cityCondition $talukaCondition $divisionCondition $odr_type $salesman $fromdate $todate $orderby"; 
		}

		//$sort = "date_of_order desc, time_of_order desc";

		//$group_by = "GROUP BY o.order_id";

		if($_SESSION['userLoginType']==1){
			$auRec=$_objAdmin->_getSelectList('table_order as o 
	  left join table_retailer as r on o.retailer_id=r.retailer_id  
	  left join table_salesman as s on o.salesman_id=s.salesman_id 
	  left join table_distributors as d on o.distributor_id=d.distributor_id AND o.so_commercial_approval_status="A" AND o.retailer_id=0  
	  left join table_retailer_channel_master as cm on r.channel_id=cm.channel_id 
	  LEFT JOIN table_relationship AS REL ON REL.relationship_id = r.relationship_id 
	  LEFT JOIN table_division AS dv ON dv.division_id = r.division_id 
	  LEFT JOIN table_relationship AS RELD ON RELD.relationship_id = d.relationship_id 
	  LEFT JOIN table_salesman_hierarchy_relationship as sr ON sr.salesman_id=s.salesman_id
	  LEFT JOIN table_salesman_hierarchy as shr ON shr.hierarchy_id=sr.hierarchy_id
	  left join table_markets as m on m.market_id=r.market_id',
	  "o.*,r.retailer_name,r.state as r_state,r.city as r_city,r.taluka_id as r_taluka,r.retailer_code,r.zipcode AS 
	  retailer_pincode,r.retailer_address,r.retailer_location,r.lat as retlat,r.survey_status as r_survey_status,
	  cm.channel_name,r.display_outlet,r.lng as retlng,dv.division_name,sr.hierarchy_id as hierarchy_id,
	  shr.sort_order AS level,s.salesman_name,s.salesman_code,d.distributor_id,d.distributor_name,d.distributor_code,d.zipcode AS distributor_pincode,d.state as d_state,d.city as d_city,d.taluka_id as d_taluka,d.survey_status as d_survey_status,m.market_name,REL.relationship_desc AS retailer_class, RELD.relationship_desc AS distributor_class,o.comments as orderComments,o.tag_description as noOrderReason",$rp, $where.$sort,''); 



	$auRecCount=$_objAdmin->_getSelectList('table_order as o 
	  left join table_retailer as r on o.retailer_id=r.retailer_id  
	  left join table_salesman as s on o.salesman_id=s.salesman_id 
	  left join table_distributors as d on o.distributor_id=d.distributor_id AND o.so_commercial_approval_status="A" AND o.retailer_id=0 
	  left join table_retailer_channel_master as cm on r.channel_id=cm.channel_id 
	  left join table_markets as m on m.market_id=r.market_id', 
	  'count(*) as total','',$where);

		}else{

			$auRec=$_objAdmin->_getSelectList('table_order as o 
				left join table_retailer as r on o.retailer_id=r.retailer_id and r.new=""  
				left join table_salesman as s on o.salesman_id=s.salesman_id 
				left join table_distributors as d on o.distributor_id=d.distributor_id and d.new=""  
				left join table_retailer_channel_master as cm on r.channel_id=cm.channel_id 
				LEFT JOIN table_relationship AS REL ON REL.relationship_id = r.relationship_id 
				LEFT JOIN table_division AS dv ON dv.division_id = r.division_id 
				LEFT JOIN table_relationship AS RELD ON RELD.relationship_id = d.relationship_id 
				LEFT JOIN table_salesman_hierarchy_relationship as sr ON sr.salesman_id=s.salesman_id
				LEFT JOIN table_salesman_hierarchy as shr ON shr.hierarchy_id=sr.hierarchy_id
				left join table_markets as m on m.market_id=r.market_id', 
				"o.*,r.retailer_name,r.state as r_state,r.city as r_city,r.taluka_id as r_taluka,r.retailer_code,r.zipcode AS 
				retailer_pincode,r.retailer_address,r.retailer_location,r.lat as retlat,r.survey_status as r_survey_status,
				cm.channel_name,r.display_outlet,r.lng as retlng,dv.division_name,sr.hierarchy_id as hierarchy_id,
				shr.sort_order AS level,s.salesman_name,s.salesman_code,d.distributor_id,d.distributor_name,d.distributor_code,d.zipcode AS distributor_pincode,d.state as d_state,d.city as d_city,d.taluka_id as d_taluka,d.survey_status as d_survey_status,m.market_name,REL.relationship_desc AS retailer_class, RELD.relationship_desc AS distributor_class,o.comments as orderComments,o.tag_description as noOrderReason",$rp, $where.$sort,''); 



			$auRecCount=$_objAdmin->_getSelectList('table_order as o 
				left join table_retailer as r on o.retailer_id=r.retailer_id and r.new="" 
				left join table_salesman as s on o.salesman_id=s.salesman_id 
				left join table_distributors as d on o.distributor_id=d.distributor_id and d.new="" 
				left join table_retailer_channel_master as cm on r.channel_id=cm.channel_id 
				left join table_markets as m on m.market_id=r.market_id', 
				'count(*) as total','',$where); 

		}

		$data.="\n";
		for($i=0;$i<count($auRec);$i++){
			if($auRec[$i]->order_type=='Yes'){
				if($auRec[$i]->order_status=='A'){
					$status="New Order";
				}
				if($auRec[$i]->order_status=='I'){
					$status="Processed";
				}
				if($auRec[$i]->order_status=='D'){
					$status="Dispatched";
				}
				//$color=($auRec[$i]->order_status=='A')?"A":"P";
			}
			if($auRec[$i]->order_type=='No'){   
   				$status=($auRec[$i]->order_status=='A')?"No Order":"Processed";
   				//$color=($auRec[$i]->order_status=='A')?"I":"P";
			}
			if($auRec[$i]->order_type=='Adhoc'){
				if($auRec[$i]->order_status=='A'){
					//$status="New Adhoc Order";
					$status="Unplanned Order";
				}
				if($auRec[$i]->order_status=='I'){
					$status="Processed";
				}
				if($auRec[$i]->order_status=='D'){
					$status="Dispatched";
				}
			}
			if(strtolower($auRec[$i]->order_type)== 'cs'){
			   if($auRec[$i]->order_status=='D'){
			    $status="Counter Sale";
			   }
			}
   			$auRec2=$_objAdmin->_getSelectList2('table_order_detail',"count( distinct(item_id)) as total_item,SUM(quantity) total_quantity1",''," order_id = ".$auRec[$i]->order_id." and type = 1");
   			$uType = "";
   			$stateN = "";
   			$cityN = "";
			if($auRec[$i]->retailer_id == 0 || $auRec[$i]->retailer_id== "") {
				$uType = "Distributor ";
				$ret_name= $auRec[$i]->distributor_name;
				$auMarker=$_objAdmin->_getSelectList2('table_survey'," lat as rlat,lng as rlng,accuracy_level,network_mode",''," distributor_id='".$auRec[$i]->distributor_id."' order by survey_date desc,survey_time desc limit 0,1");
				$survey_status = ($auRec[$i]->d_survey_status=='I')?"Not Done":"Done";


				if($auRec[$i]->d_state > 0){
					$stateN = $_objAdmin->_getSelectList2('state',"state_name",'','state_id='.$auRec[$i]->d_state);
				}

				if($auRec[$i]->d_city > 0){
					$cityN = $_objAdmin->_getSelectList2('city',"city_name",'',"city_id='".$auRec[$i]->d_city."'"); 
				}
						 		 
		 		
			} else {
				$uType  = "Retailer ";
				$ret_name= $auRec[$i]->retailer_name;
				$auMarker=$_objAdmin->_getSelectList2('table_survey ',"lat as rlat,lng as rlng,accuracy_level,network_mode",''," retailer_id='".$auRec[$i]->retailer_id."' order by survey_date desc,survey_time desc limit 0,1");
				$survey_status = ($auRec[$i]->r_survey_status=='I')?"Not Done":"Done";

				if($auRec[$i]->r_state > 0){
					$stateN = $_objAdmin->_getSelectList2('state',"state_name",'','state_id='.$auRec[$i]->r_state);
				}

				if($auRec[$i]->r_city > 0){
					$cityN = $_objAdmin->_getSelectList2('city',"city_name",'',"city_id='".$auRec[$i]->r_city."'"); 
				}
				 
		 		
			}
			$center_lat ="";	
			$center_lng ="";
			$accuracy_level = "";
			$network_mode ="";
			$radius = 0.05;
			if($auMarker[0]->rlat>0){
				$center_lat =$auMarker[0]->rlat;
				$center_lng =$auMarker[0]->rlng;
				$accuracy_level=$auMarker[0]->accuracy_level;
				$network_mode=$auMarker[0]->network_mode;
				$query = sprintf("SELECT order_id, lat, lng, ( 6371 * acos( cos( radians('%s') ) * cos( radians( lat ) ) * cos( radians( lng ) - radians('%s') ) + sin( radians('%s') ) * sin( radians( lat ) ) ) ) AS distance FROM table_order where order_id='".$auRec[$i]->order_id."'", mysql_real_escape_string($center_lat), mysql_real_escape_string($center_lng), mysql_real_escape_string($center_lat), mysql_real_escape_string($radius));
				$result = mysql_query($query);
				$row = @mysql_fetch_assoc($result);
				if($row['distance'] <= $radius){
					$visit_status = "Visited";
				} else {
					$visit_status = "Not Visited";
				}
			} else {
				$visit_status = "Not Visited";
			} 
   			$date_of_order=$_objAdmin->_changeDate($auRec[$i]->date_of_order);   
			$salDetails = $_objAdmin->getSalesmanFullDetails($auRec[$i]->salesman_id);
			$remove = array("\n","\r","\t");
			$remove1 = array(",","-",".");
			$data.=""
			.$auRec[$i]->salesman_name.
			"\t".$salDetails[0]->rpt_to. 
			"\t".$status.
			"\t".$auRec[$i]->noOrderReason.
			"\t".$auRec[$i]->orderComments. 
			"\t".$auRec[$i]->acc_total_invoice_amount. 
			"\t".$auRec2[0]->total_item.
			"\t".$auRec2[0]->total_quantity1. 
			//"\t".$survey_status. 
			//"\t".$visit_status.
			//"\t".$auRec[$i]->tag_description. 
			"\t".$auRec[$i]->retailer_name.
			//"\t".$auRec[$i]->retailer_class. 
			"\t".$auRec[$i]->retailer_pincode.
			"\t".$auRec[$i]->order_id. 
			"\t".$date_of_order. 
			"\t".$auRec[$i]->time_of_order. 
			"\t".$auRec[$i]->salesman_code. 
			//"\t".$salDetails[0]->rpt_person_code. 
			"\t".$auRec[$i]->distributor_code. 
			"\t".$auRec[$i]->distributor_name.
			//"\t".$auRec[$i]->distributor_class. 
			"\t".$auRec[$i]->distributor_pincode.
			"\t".$auRec[$i]->retailer_code. 
			//"\t".str_replace($remove, ' ',$auRec[$i]->market_name). 
			//"\t".$auRec[$i]->channel_name. 
			"\t".str_replace($remove1, ' ',addslashes($auRec[$i]->retailer_address)). 
			//"\t".str_replace($remove, ' ',addslashes($auRec[$i]->comments)).
			"\t".$stateN[0]->state_name. 
			"\t".$cityN[0]->city_name. 
			"\n";
		}
	/*$data=" Distributor Name\t Distributor Code \t Salesman \t Salesman Code \t  Retailer \t  Retailer Code \t Retailer Market\t Retailer Channel\t Display Outlet\t Date\t Time\t Total Invoice Ammount\t Total Number Of Item\t   Order Status\n";
	
	
	$sort = " ORDER BY date_of_order desc,time_of_order desc";
	
 if($_SESSION['FromOrderList']!=''){
   $fromdate=" and o.date_of_order >= '".date('Y-m-d', strtotime($_SESSION['FromOrderList']))."'";
  }
  else
  {
   $fromdate=" and o.date_of_order >= '".date('Y-m-d')."'";
  }
  if($_SESSION['ToOrderList']!=''){
   $todate=" and o.date_of_order <= '".date('Y-m-d', strtotime($_SESSION['ToOrderList']))."'";
  }
  else
  {
   $todate=" and o.date_of_order <= '".date('Y-m-d')."'";
  }
  if($_SESSION['OrderBy']!=''){
   if($_SESSION['OrderBy']==1){
   $orderby="";
   }
   if($_SESSION['OrderBy']==2){
   $orderby=" and o.order_type='Adhoc' ";
   }
   if($_SESSION['OrderBy']==3){
   $orderby=" and o.order_type!='Adhoc' ";
   }
  }
  else
  {
   $orderby="";
  }
  //$where = " 1=1 ";
   $salesman=$_GET['id'];
  $where = " $salesman $fromdate $todate and r.new='' $orderby and o.account_id =".$_SESSION['accountId'];
  
   $auRec=$_objAdmin->_getSelectList('table_order as o left join table_retailer as r on o.retailer_id=r.retailer_id left join table_salesman as s on o.salesman_id=s.salesman_id left join table_distributors as d on o.distributor_id=d.distributor_id
   	   left join table_retailer_channel_master as cm on cm.channel_id=r.channel_id',"o.*,r.retailer_name,r.retailer_code,r.retailer_address,r.retailer_location,cm.channel_name,r.display_outlet,r.lat as retlat,r.lng as retlng,s.salesman_name,s.salesman_code,d.distributor_name,d.distributor_code",'',$where.$sort,'');
	
	 for($i=0;$i<count($auRec);$i++){
	 
	 
	 if($auRec[$i]->order_type=='Yes'){
    if($auRec[$i]->order_status=='A'){
    $status="New Order";
    }
    if($auRec[$i]->order_status=='I'){
    $status="Processed";
    }
    if($auRec[$i]->order_status=='D'){
    $status="Dispatched";
    }
   $color=($auRec[$i]->order_status=='A')?"A":"P";
   } 
   if($auRec[$i]->order_type=='No'){
   
   $status=($auRec[$i]->order_status=='A')?"No Order":"Processed";
   $color=($auRec[$i]->order_status=='A')?"I":"P";
   }
   if($auRec[$i]->order_type=='Adhoc'){
   if($auRec[$i]->order_status=='A'){
    $status="New Adhoc Order";
    }
    if($auRec[$i]->order_status=='I'){
    $status="Processed";
    }
    if($auRec[$i]->order_status=='D'){
    $status="Dispatched";
    }
   //$status=($auRec[$i]->order_status=='A')?"New Adhoc Order":"Processed";
   $color=($auRec[$i]->order_status=='A')?"O":"P";
   }

   // display oulet
   if($auRec[$i]->display_outlet=='Y'){ $outlet='Yes';} elseif($auRec[$i]->display_outlet=='N'){$outlet='No';}
   
    $auRec2=$_objAdmin->_getSelectList2('table_order_detail',"count( distinct(item_id)) as total_item",''," order_id = ".$auRec[$i]->order_id." and type = 1");
	
    	$date_of_order=$_objAdmin->_changeDate($auRec[$i]->date_of_order);   
		$remove = array("\n","\r");
		$data.="".$auRec[$i]->distributor_name."\t".$auRec[$i]->distributor_code. "\t".$auRec[$i]->salesman_name. "\t".$auRec[$i]->salesman_code. "\t".$auRec[$i]->retailer_name. "\t".$auRec[$i]->retailer_code. "\t".str_replace($remove, ' ',$auRec[$i]->retailer_location)."\t".$auRec[$i]->channel_name."\t".$outlet."\t".$date_of_order."\t".$auRec[$i]->time_of_order."\t".$auRec[$i]->acc_total_invoice_amount."\t".$auRec2[0]->total_item."\t".$status."\n";
		}*/
			 
	header("Content-type: application/octet-stream");
	header("Content-Disposition: attachment; filename=\"Order List.xls\"");		
	echo $data;
	exit;
	}
	
	
	
	
	/*if(isset($_GET['export_salesman_monthly_sale'])){	
	//echo hello;exit;
	
	$data=" Salesman\t Dealer\t Item\t Item Code\t Month\t Year\t Day1\t  Day2\t Day3\t Day4\t Day5\t Day6\t Day7\t Day8\t Day9\t Day10\t Day11\t Day12\t Day13\t Day14\t Day15\t Day16\t Day17\t Day18\t Day19\t Day20\t Day21\t Day22\t Day23\t Day24\t Day25\t Day26\t Day27\t Day28\t Day29\t Day30\t Day31\n";
	
	//echo $_SESSION['distributorIDSale'].$_SESSION['itemID'].$_SESSION['dismonth'].$_SESSION['disCyear'];
	
	if($_SESSION['itemID']!=''){
			$Item=" and V.item_id ='".$_SESSION['itemID']."'";
		}
		
		if($_SESSION['dismonth']!=''){
			$fromdate=" and V.month = '".$_SESSION['dismonth']."'";
		} else {
			$fromdate = "and V.month = ".date('m');
		}
		
		if($_SESSION['disCyear']!=''){
			$todate=" and V.year = '".$_SESSION['disCyear']."'";
		} else {
			$todate="and V.year = ".date('Y');
		}
		
		if($_SESSION['salesmanID']!=''){
			$salesman =" and V.salesman_id ='".$_SESSION['salesmanID']."'";
		}

		//$sortname = 'month';
		//$sortorder = 'ASC';
		$sort = " ORDER BY month ASC";
		
		$where = " $salesman $Item $fromdate $todate AND R.new=''";
		$groupby = ' GROUP BY R.retailer_id, s.salesman_id, item_id, month';
		
		$auRec=$_objAdmin->_getSelectList('salereldailywiseitemreport AS V 
					LEFT JOIN table_retailer AS R ON V.retailer_id = R.retailer_id 
					LEFT JOIN table_salesman AS s ON V.salesman_id = s.salesman_id
					LEFT JOIN table_item AS i ON V.item_id = i.item_id',		
			"V.retailer_id AS RID,
			V.salesman_id AS SID,
			 DATE_FORMAT(V.date_of_order, '%b') AS DOODR,
			 V.Day AS Day,
			 V.item_name AS item_name,
			 i.item_code AS item_code,
			 s.salesman_name AS s_name,
			 V.item_id AS item_id,
			 V.month AS month,
			 V.year AS year,
			 R.retailer_name AS retailer_name,
			 R.retailer_address,
			 R.retailer_address2",'',$where.$groupby.$sort,'');
			 for($i=0;$i<count($auRec);$i++){
		
		if(isset($auRec[$i]->item_id)){
	$auRec2=$_objAdmin->_getSelectList('salereldailywiseitemreport AS V',"totalSaleUnit,Day,retailer_id,salesman_id,item_name",''," retailer_id = ".$auRec[$i]->RID." AND salesman_id = ".$auRec[$i]->SID." AND item_id = ".$auRec[$i]->item_id." AND month = ".$auRec[$i]->month."");

			$row1  = '-';
			$row2  = '-';
			$row3  = '-';
			$row4  = '-';
			$row5  = '-';
			$row6  = '-';
			$row7  = '-';
			$row8  = '-';
			$row9  = '-';
			$row10 = '-';
			$row11 = '-';
			$row12 = '-';
			$row13 = '-';
			$row14 = '-';
			$row15 = '-';
			$row16 = '-';
			$row17 = '-';
			$row18 = '-';
			$row19 = '-';
			$row20 = '-';
			$row21 = '-';
			$row22 = '-';
			$row23 = '-';
			$row24 = '-';
			$row25 = '-';
			$row26 = '-';
			$row27 = '-';
			$row28 = '-';
			$row29 = '-';
			$row30 = '-';
			$row31 = '-';

			for($j=0;$j<count($auRec2);$j++){
			
				${'row'.$auRec2[$j]->Day} = $auRec2[$j]->totalSaleUnit;
			
			}
			
		$data.="".$auRec[$i]->s_name."\t".$auRec[$i]->retailer_name."\t".$auRec[$i]->item_name."\t".$auRec[$i]->item_code."\t".$auRec[$i]->DOODR."\t".$auRec[$i]->year."\t".$row1."\t".$row2."\t".$row3."\t".$row4."\t".$row5."\t".$row6."\t".$row7."\t".$row8."\t".$row9."\t".$row10."\t".$row11."\t".$row12."\t".$row13."\t".$row14."\t".$row15."\t".$row16."\t".$row17."\t".$row18."\t".$row19."\t".$row20."\t".$row21."\t".$row22."\t".$row23."\t".$row24."\t".$row25."\t".$row26."\t".$row27."\t".$row28."\t".$row29."\t".$row30."\t".$row31."\n";
		}
		}
			 
	header("Content-type: application/octet-stream");
	header("Content-Disposition: attachment; filename=\"Salesman Monthly Sale.xls\"");		
	echo $data;
	exit;
	}*/
	
	
	if(isset($_GET['export_salesman_monthly_sale'])){	

	$datasetString = array();
	$datasetString=" Salesman\t Salesman Code \t Dealer \t Dealer Code \t City \t District \t State \t Distributor Name \t Distributor Code \t Item\t Item Code\t Brand Name \t Month\t Year\t Day1\t  Day2\t Day3\t Day4\t Day5\t Day6\t Day7\t Day8\t Day9\t Day10\t Day11\t Day12\t Day13\t Day14\t Day15\t Day16\t Day17\t Day18\t Day19\t Day20\t Day21\t Day22\t Day23\t Day24\t Day25\t Day26\t Day27\t Day28\t Day29\t Day30\t Day31 \t Total\n";

	$monthtext = date('M');
	$yeartext  = date('Y');
	
	if($_SESSION['itemID']!=''){
			$Item=" and V.item_id ='".$_SESSION['itemID']."'";
		}
		
		if($_SESSION['dismonth']!=''){
			$fromdate=" and V.month = '".$_SESSION['dismonth']."'";
		} else {
			$fromdate = "and V.month = ".date('m');
		}
		
		if($_SESSION['disCyear']!=''){
			$todate=" and V.year = '".$_SESSION['disCyear']."'";
		} else {
			$todate="and V.year = ".date('Y');
		}
		
		if($_SESSION['monthSalesmanID']!=''){
			$salesman =" and V.salesman_id ='".$_SESSION['monthSalesmanID']."'";
		} else {
			$salsList = $_objArrayList->SalesmanArrayList(); // For Admin and Salesman Login
			$salesman = $_objArrayList->getSalesCondition($salsList); 
		}

		$sort = " ORDER BY month ASC";

		$data = array ();

		
		
		for ($i = 1; $i <=31; $i++ ) {
		
		$where = " $salesman $Item $fromdate $todate AND V.Day = ".$i." AND R.new=''";
		$groupby = ' GROUP BY V.retailer_id, V.salesman_id, V.item_id';
		
			$auRec = $_objAdmin->_getSelectList('salereldailywiseitemreport AS V 
					LEFT JOIN table_retailer AS R ON V.retailer_id = R.retailer_id 
					LEFT JOIN table_salesman AS s ON V.salesman_id = s.salesman_id
					LEFT JOIN table_item AS i ON V.item_id = i.item_id 
					LEFT JOIN state as st on st.state_id = R.state
					LEFT JOIN city as ct on ct.city_id=R.city
					LEFT JOIN table_markets as mk on mk.market_id = R.market_id
					LEFT JOIN table_distributors as D on V.distributor_id = D.distributor_id 
					LEFT JOIN table_brands  AS B ON B.brand_id=i.brand_id',		
			"V.retailer_id AS RID,
			V.salesman_id AS SID,
			 DATE_FORMAT(V.date_of_order, '%b') AS DOODR,
			 V.Day AS Day,
			 SUM(V.totalSaleUnit) AS quantity,
			 V.item_name AS item_name,
			 i.item_code AS item_code,
			 s.salesman_name AS s_name,
			 s.salesman_code,
			 V.item_id AS item_id,
			 V.month AS month,
			 V.year AS year,
			 R.retailer_name AS retailer_name,
			 R.retailer_code,
			 R.retailer_address,
			 R.retailer_address2,st.state_name,ct.city_name,mk.market_name,D.distributor_name,D.distributor_code,B.brand_name",'',$where.$groupby.$sort,'');

		
			foreach ( $auRec as $key=>$value ) :
			
			$monthtext = $value->DOODR;
			$yeartext = $value->year;
			$data[$value->SID]['s_name'] = $value->s_name; 
			$data[$value->SID]['s_code'] = $value->salesman_code;
			$data[$value->SID]['market_name'] = $value->market_name; 
			$data[$value->SID]['city_name'] = $value->city_name;
			$data[$value->SID]['state_name'] = $value->state_name; 
			$data[$value->SID]['brand_name'] = $value->brand_name;
			$data[$value->SID]['distributor_name'] = $value->distributor_name; 
			$data[$value->SID]['distributor_code'] = $value->distributor_code;
			
			$data[$value->SID]['retailers'][$value->RID]['items'][$value->item_id]['days'][$value->Day] = array('retailer_name'=>$value->retailer_name,'retailer_code'=>$value->retailer_code, 'item_name'=>$value->item_name, 'item_code'=>$value->item_code, 'qty'=>$value->quantity);

			endforeach;
		
		
		}
		
		
		foreach ($data as $skey=>$svalue ) :
		
			$s_name = "";
		
			 $s_name = $svalue['s_name'];
			 $s_code = $svalue['s_code'];
			$market_name = $svalue['market_name']; 
			$city_name = $svalue['city_name'];
			$state_name = $svalue['state_name']; 
			$brand_name = $svalue['brand_name'];
			$distributor_name = $svalue['distributor_name']; 
			$distributor_code = $svalue['distributor_code'];

		
			foreach ($svalue['retailers'] as $rkey=>$rvalue ) :
			
				$retailer_name = "";
				$retailer_code = "";
				
				foreach ($rvalue['items'] as $ikey=>$ivalue ) :
				
					$item_name = "";
					$item_code = "";
					$row1  = '-';
					$row2  = '-';
					$row3  = '-';
					$row4  = '-';
					$row5  = '-';
					$row6  = '-';
					$row7  = '-';
					$row8  = '-';
					$row9  = '-';
					$row10 = '-';
					$row11 = '-';
					$row12 = '-';
					$row13 = '-';
					$row14 = '-';
					$row15 = '-';
					$row16 = '-';
					$row17 = '-';
					$row18 = '-';
					$row19 = '-';
					$row20 = '-';
					$row21 = '-';
					$row22 = '-';
					$row23 = '-';
					$row24 = '-';
					$row25 = '-';
					$row26 = '-';
					$row27 = '-';
					$row28 = '-';
					$row29 = '-';
					$row30 = '-';
					$row31 = '-';
		
					$total = '-';
					
				

					foreach ($ivalue['days'] as $dkey=>$dvalue ) :
						//echo $dvalue['qty'];
						${'row'.$dkey} = $dvalue['qty'];

					    $total += $dvalue['qty'];
								
						$retailer_name 	= $dvalue['retailer_name'];	
						$retailer_code 	= $dvalue['retailer_code'];	
						$item_name 		= $dvalue['item_name'];
						$item_code		= $dvalue['item_code'];

					endforeach ;
					
				$datasetString.="".$s_name."\t".$s_code."\t".$retailer_name."\t".$retailer_code."\t".$market_name."\t".$city_name."\t".$state_name."\t".$distributor_name."\t".$distributor_code."\t".$item_name."\t".$item_code."\t".$brand_name."\t".$monthtext."\t".$yeartext."\t".$row1."\t".$row2."\t".$row3."\t".$row4."\t".$row5."\t".$row6."\t".$row7."\t".$row8."\t".$row9."\t".$row10."\t".$row11."\t".$row12."\t".$row13."\t".$row14."\t".$row15."\t".$row16."\t".$row17."\t".$row18."\t".$row19."\t".$row20."\t".$row21."\t".$row22."\t".$row23."\t".$row24."\t".$row25."\t".$row26."\t".$row27."\t".$row28."\t".$row29."\t".$row30."\t".$row31."\t".$total."\n";
					
				endforeach ;
			
			endforeach ;
			

			
		endforeach ;
			 
	header("Content-type: application/octet-stream");
	header("Content-Disposition: attachment; filename=\"Salesman Monthly Sale.xls\"");		
	echo $datasetString;
	exit;

	}
	
	

	
	
	if(isset($_GET['export_retailer_opening_stock'])){	
	//echo hello;exit;
	
	$data=" Salesman \t Salesman Code \t Retailer \t Retailer Code \t Category\t Item\t Stock Value\t Date\t Time\n";
	
	 $sort=" ORDER BY date_of_order desc,time_of_order desc";
	
  if($_SESSION['RetailerID']!=''){
   $retailer=" AND r.retailer_id='".$_SESSION['RetailerID']."'";
  }
    		
	if(isset($_SESSION['userLoginType']) && in_array($_SESSION['userLoginType'],array(6,7,8,9)))
	{
			
		$condwhere = $_objAdmin->getSalesMenID();
	}
  //$where = " 1=1 ";
  if($_SESSION['FromOS']!=''){
   $fromdate=" and ot.date_of_order >= '".date('Y-m-d', strtotime($_SESSION['FromOS']))."'";
  }
  else
  {
   $fromdate=" and ot.date_of_order >= '".date('Y-m-d')."'";
  }
  
  if($_SESSION['ToOS']!=''){
   $todate=" and ot.date_of_order <= '".date('Y-m-d', strtotime($_SESSION['ToOS']))."'";
  }
  else
  {
   $todate=" and ot.date_of_order <= '".date('Y-m-d')."'";
  }


  $where = " ot.ostype='R' $retailer $fromdate $todate "; 
  
   $auRec=$_objAdmin->_getSelectList('table_order_os AS ot 
  	LEFT JOIN table_order_detail_os AS tod ON tod.os_id=ot.os_id 
	LEFT JOIN table_retailer AS r ON r.retailer_id=ot.retailer_id 
	LEFT JOIN table_salesman AS s ON s.salesman_id=ot.salesman_id 
	LEFT JOIN table_item AS i ON i.item_id=tod.item_id 
	LEFT JOIN table_category AS c ON c.category_id=i.category_id',
	"ot.*,tod.*,
	r.retailer_name,
	r.retailer_code,
	s.salesman_name,
	s.salesman_code,
	i.item_name,
	c.category_name",'',$where.$condwhere.$sort,'');
	
	 for($i=0;$i<count($auRec);$i++){
	 
		//$data.="".$auRec[$i]->salesman_name."\t".$auRec[$i]->salesman_code."\t".$auRec[$i]->retailer_name."\t".$auRec[$i]->retailer_code."\t".$auRec[$i]->category_name."\t".$auRec[$i]->item_name."\t".$auRec[$i]->quantity."\t".$_objAdmin->_changeDate($auRec[$i]->date_of_order)."\t".$auRec[$i]->time_of_order."\n";

			$data.='"'.$auRec[$i]->salesman_name.'"';
			$data.="\t";

			$data.='"'.$auRec[$i]->salesman_code.'"';
			$data.="\t";

			$data.='"'.$auRec[$i]->retailer_name.'"';
			$data.="\t";

			$data.='"'.$auRec[$i]->retailer_code.'"';
			$data.="\t";

			$data.='"'.$auRec[$i]->category_name.'"';
			$data.="\t";

			$data.='"'.$auRec[$i]->item_name.'"';
			$data.="\t";

			$data.='"'.$auRec[$i]->quantity.'"';
			$data.="\t";

			$data.='"'.$_objAdmin->_changeDate($auRec[$i]->date_of_order).'"';
			$data.="\t";

			$data.='"'.$auRec[$i]->time_of_order.'"';
			$data.="\n";
		}
			 
	header("Content-type: application/octet-stream");
	header("Content-Disposition: attachment; filename=\"Retailer Opening Stock .xls\"");		
	echo $data;
	exit;
	}
	
	
	
	
	if(isset($_GET['export_product_wise_report'])){	
	//echo hello;exit;
	
	$data=" Category \t Salesman \t Salesman Code \t Item\t Item Code\t Brand\t Offer Type\t Erp Code\t Number Of Order\t Quantity\t Price\t Total\n";
	
	$sort = " ORDER BY date_of_order desc,time_of_order desc";
	
 if($_SESSION['categoryID']!='' && $_SESSION['categoryID']!='all'){
		
			$condition .=" c.category_id = ".$_SESSION['categoryID']." AND ";
		} 
		
		if($_SESSION['itemID']!='' && $_SESSION['itemID']!='all'){
		
			$condition .=" od.item_id = ".$_SESSION['itemID']." AND ";
		}  
		
		
		if($_SESSION['FromProductList']!=''){
			$fromdate="  o.date_of_order  >= '".date('Y-m-d', strtotime($_SESSION['FromProductList']))."' AND ";
		}
		else
		{
			$fromdate=" o.date_of_order  >= '".date('Y-m-d')."' AND ";
		}
		
		
		
		if($_SESSION['ToProductList']!=''){
			$todate=" o.date_of_order <= '".date('Y-m-d', strtotime($_SESSION['ToProductList']))."' ";
		}
		else
		{
			$todate="  o.date_of_order <= '".date('Y-m-d')."'";
		}
		
		//$groupby  = " GROUP BY table_order_detail.item_id having SUM(table_order_detail.quantity)!=0";
		//$groupby  = " AND o.order_id in (select order_id from table_order where ostype in ('D','R') AND order_type = 'Yes' AND order_status='A' AND $fromdate $todate ) group by o.item_id having SUM(o.quantity)!=0 ";
		$groupby  = " GROUP BY od.item_id having od.item_id!=''";
		$where = " od.type=1 and c.account_id='".$_SESSION['accountId']."' and $condition $fromdate $todate ";		

		
		$auRec=$_objAdmin->_getSelectList2('table_order AS o
					LEFT JOIN table_order_detail AS od ON od.order_id = o.order_id
					LEFT JOIN table_salesman as s ON s.salesman_id=o.salesman_id
					LEFT JOIN table_item AS i ON i.item_id = od.item_id
					LEFT JOIN table_category AS c ON c.category_id = i.category_id 
					left join table_price as tp on tp.item_id= od.item_id',
					"COUNT(od.item_id) AS total, sum(od.quantity) AS Quantity, SUM(od.quantity * tp.item_mrp) AS ttlprice, category_name, od.item_id, item_name, item_code,s.salesman_name,s.salesman_code ,price,date_of_order",
					'',$where.$groupby.$sort,'');
	
	 for($i=0;$i<count($auRec);$i++){
	 
		$data.="".$auRec[$i]->category_name. "\t".$auRec[$i]->salesman_name. "\t".$auRec[$i]->salesman_code. "\t".$auRec[$i]->item_name."\t".$auRec[$i]->item_code."\t".$auRec[$i]->total."\t".$auRec[$i]->Quantity."\t".$auRec[$i]->price."\t".$auRec[$i]->ttlprice."\n";
		}
			 
	header("Content-type: application/octet-stream");
	header("Content-Disposition: attachment; filename=\"Product Wise Report .xls\"");		
	echo $data;
	exit;
	}
	// Distributor Order List	



	//product export

	if(isset($_GET['export_product_report'])){	
	//echo hello;exit;
	
	$data=" Distributor Name \t  Distributor Code \t  Retailer Name \t  Retailer Code \t Salesman Name \t Salesman Code \t Category Name\t Item Name\t Item Code\t price\t Quantity\t Total\t Date of Order\t Retailer Type\n";

	
	$sort = " ORDER BY date_of_order desc,time_of_order desc";
	
 if($_SESSION['categoryID']!='' && $_SESSION['categoryID']!='all'){
		
			$condition .=" c.category_id = ".$_SESSION['categoryID']." AND ";
		} 
		
		if($_SESSION['itemID']!='' && $_SESSION['itemID']!='all'){
		
			$condition .=" D.item_id = ".$_SESSION['itemID']." AND ";
		}  
		
		
		if($_SESSION['FromProductList']!=''){
			$fromdate="  O.date_of_order  >= '".date('Y-m-d', strtotime($_SESSION['FromProductList']))."' AND ";
		}
		else
		{
			$fromdate=" O.date_of_order  >= '".date('Y-m-d')."' AND ";
		}
		
		
		
		if($_SESSION['ToProductList']!=''){
			$todate=" O.date_of_order <= '".date('Y-m-d', strtotime($_SESSION['ToProductList']))."' ";
		}
		else
		{
			$todate="  O.date_of_order <= '".date('Y-m-d')."'";
		}
		
		//$groupby  = " GROUP BY table_order_detail.item_id having SUM(table_order_detail.quantity)!=0";
		//$groupby  = " AND o.order_id in (select order_id from table_order where ostype in ('D','R') AND order_type = 'Yes' AND order_status='A' AND $fromdate $todate ) group by o.item_id having SUM(o.quantity)!=0 ";
		$groupby  = " GROUP BY D.item_id having D.item_id!=''";
		$where = " D.type=1 and O.account_id='".$_SESSION['accountId']."' and $condition $fromdate $todate ";		

		
		$auRec=$_objAdmin->_getSelectList2('table_order_detail AS D 
		LEFT JOIN table_order AS O ON D.order_id = O.order_id
        LEFT JOIN table_item AS I ON D.item_id = I.item_id
		LEFT JOIN table_category AS c ON c.category_id = I.category_id 
		LEFT JOIN table_price AS P ON D.item_id = P.item_id
		LEFT JOIN table_salesman AS S ON O.salesman_id = S.salesman_id
		LEFT JOIN table_retailer AS R ON O.retailer_id = R.retailer_id
		LEFT JOIN table_distributors AS Dis ON O.distributor_id = Dis.distributor_id',
	'D.price, D.quantity, D.total, I.item_name, I.item_code, P.item_mrp, O.order_id, S.salesman_name, S.salesman_code, Dis.distributor_name,Dis.distributor_code,R.retailer_name,R.retailer_code,R.new,c.category_name, O.date_of_order, O.time_of_order',$rp,$where.$sort,'');
		
	
	 for($i=0;$i<count($auRec);$i++){
	 if($auRec[$i]->new=="1"){$retailer_type='New Retailer';} else{$retailer_type='Existing Retailer';}
		$data.="".$auRec[$i]->distributor_name. "\t".$auRec[$i]->distributor_code. "\t".$auRec[$i]->retailer_name. "\t".$auRec[$i]->retailer_code. "\t".$auRec[$i]->salesman_name."\t" .$auRec[$i]->salesman_code. "\t".$auRec[$i]->category_name."\t".$auRec[$i]->item_name."\t".$auRec[$i]->item_code."\t".$auRec[$i]->price."\t".$auRec[$i]->quantity."\t".$auRec[$i]->total."\t".$_objAdmin->_changeDate($auRec[$i]->date_of_order)."\t".$retailer_type."\n";
		}
			 
	header("Content-type: application/octet-stream");
	header("Content-Disposition: attachment; filename=\"Product Report.xls\"");		
	echo $data;
	exit;
	}


	//Ends product export
	
	
if(isset($_GET['export_distributor_order_list'])){	
	//echo hello;exit;
	
	//$data="Date\t Time\t Order Number\t Total Invoice Ammount\t Distributor Name\t Distributor Code \t Salesman\t  Salesman Code \t Retailer \t Retailer Code \t Retailer Channel\t Display Outlet\t Market\t Address\t Order Status\n";

	$data="Date\t Time\t Order Number\t Total Invoice Ammount\t Distributor Name\t Distributor Code \t Salesman\t  Salesman Code \t Order Status\n";
	
	$sort = " ORDER BY date_of_order desc,time_of_order desc";
	
       
		if($_SESSION['FromDisList']!=''){
			$fromdate=" and o.date_of_order >= '".date('Y-m-d', strtotime($_SESSION['FromDisList']))."'";
		}
		else
		{
			$fromdate=" and o.date_of_order >= '".date('Y-m-d')."'";
		}
		if($_SESSION['ToDisList']!=''){
			$todate=" and o.date_of_order <= '".date('Y-m-d', strtotime($_SESSION['ToDisList']))."'";
		}
		else
		{
			$todate=" and o.date_of_order <= '".date('Y-m-d')."'";
		}
		
		
		if(isset($_SESSION['distributorStatus']) && $_SESSION['distributorStatus']!="A"){
			
			$disList=" AND o.distributor_id=".$_SESSION['distributorStatus']." ";
		}
		else{
			$disList="";
		}


			if($_SESSION['OrderBy']!=''){
				if($_SESSION['OrderBy']==1){
					$orderby="";
				}
				if($_SESSION['OrderBy']==2){
					$orderby=" and o.order_status='A' ";
				}
				if($_SESSION['OrderBy']==3){
					$orderby=" and o.order_status='I' ";
				}
				if($_SESSION['OrderBy']==4){
					$orderby=" and o.order_status='D' ";
				}
				if($_SESSION['OrderBy']==5){
					$orderby=" and (o.order_status='D' OR o.order_status='I') and od.order_detail_status ='3'";
				}
			}
			else
			{
				$orderby="";
			}


			  // AJAY@2016-04-11
			    if($_SESSION['SalDisList']!=''){
					$salesman=" and s.salesman_id='".$_SESSION['SalDisList']."'";
			    } else if( $_SESSION['userLoginType']==1 || $_SESSION['userLoginType'] == 3){
				 	$salesman = "";
			    } 

			    if($_SESSION['userLoginType'] == 3 ) {

					//$where=" o.distributor_id='".$_SESSION['distributorId']."' $salesman $disList $fromdate $todate $orderby and o.account_id ='".$_SESSION['accountId']."'";

			    	$where=" o.distributor_id > 0 AND o.distributor_id='".$_SESSION['distributorId']."' AND o.retailer_id=0 $salesman $disList $fromdate $todate $orderby ";
			    }
			    else
			    {
		//$where = " 1=1 ";
			    	$where = "  (r.new='' || d.new='') and o.distributor_id!='' $salesman $disList $fromdate $todate $orderby and o.account_id =".$_SESSION['accountId'];
			    }

		//if ($query) $where .= " AND $qtype LIKE '%$query%' ";

			    $auRec=$_objAdmin->_getSelectList('table_order as o
			    	LEFT JOIN table_order_detail AS od ON od.order_id = o.order_id AND od.order_detail_status = 3 
			    	left join table_retailer as r on o.retailer_id=r.retailer_id 
			    	left join table_salesman as s on o.salesman_id=s.salesman_id 
			    	left join table_distributors as d on d.distributor_id=o.distributor_id 
			    	left join table_retailer_channel_master as cm on cm.channel_id=r.channel_id',
			    	"o.*, od.order_detail_status AS od_sts, r.retailer_name,r.retailer_code,r.retailer_address,r.retailer_location,cm.channel_name,r.display_outlet,s.salesman_name,s.salesman_code,d.distributor_name,d.distributor_code",$rp,$where.$sort,'');
	
			    for($i=0;$i<count($auRec);$i++){
			    	if($auRec[$i]->order_status=='A'){
			    		$status="New Order";				
			    	} else  if($auRec[$i]->order_status=='I'){
			    		$status="Processed";					
			    	} else if($auRec[$i]->order_status=='D'){
			    		$status="Dispatched (Counter Sale)";					
			    	}
			    	if($auRec[$i]->display_outlet=='Y'){$outlet='Yes';} else if($auRec[$i]->display_outlet=='N'){ $outlet='No';} else{ $outlet=''; }
			//$status=($auRec[$i]->order_status=='A')?"New Adhoc Order":"Processed";
			//$color=($auRec[$i]->order_status=='A')?"O":"P";
			//$status=($auRec[$i]->order_status=='A')?"New Order":"Processed";
			    	$date_of_order=$_objAdmin->_changeDate($auRec[$i]->date_of_order);
			    	$remove = array("\n","\r");
			    	$remove = array(",", "-", "_");
			    	$data.="".
			    	$date_of_order.
			    	"\t".$auRec[$i]->time_of_order.
			    	"\t".$auRec[$i]->order_id.
			    	"\t".$auRec[$i]->acc_total_invoice_amount.
			    	"\t".str_replace($remove, ' ',addslashes($auRec[$i]->distributor_name)).
			    	"\t".$auRec[$i]->distributor_code. 
			    	"\t".$auRec[$i]->salesman_name .
			    	"\t".$auRec[$i]->salesman_code.
			    	// "\t".str_replace($remove, ' ',addslashes($auRec[$i]->retailer_name)).
			    	// "\t".$auRec[$i]->retailer_code.
			    	// "\t".$auRec[$i]->channel_name.
			    	// "\t".$outlet.
			    	// "\t".str_replace($remove, ' ',addslashes($auRec[$i]->retailer_location)).
			    	// "\t".str_replace($remove, ' ',addslashes($auRec[$i]->retailer_address)).
			    	"\t".$status."\n";
			    }
			 
			    header("Content-type: application/octet-stream");
			    header("Content-Disposition: attachment; filename=\"Distributor Order List.xls\"");		
			    echo $data;
			    exit;
	}

	/******************
	*Desc:Export Disctributor Counter sales
	 By: Maninder KUmar
	 On:12th April 2016
	 ***************************/

	 if(isset($_GET['export_distributor_counter_sales'])){	
	//echo hello;exit;
	
	$data="Date\t Time\t Order No\t Total Invoice Ammount\t Distributor Name\t Distributor Code\t Distributor Division\t  Display Outlet\t Market\t Address\t Order Status\n";
	
	   $sort = " ORDER BY ord.date_of_order desc,ord.time_of_order desc";
	
		
		if($_SESSION['FromDisList']!=''){
			$fromdate=" and ord.date_of_order >= '".date('Y-m-d', strtotime($_SESSION['FromDisList']))."'";
		} else {
			$fromdate=" and ord.date_of_order >= '".date('Y-m-d')."'";
		}

		if($_SESSION['ToDisList']!=''){
			$todate=" and ord.date_of_order <= '".date('Y-m-d', strtotime($_SESSION['ToDisList']))."'";
		} else {
			$todate=" and ord.date_of_order <= '".date('Y-m-d')."'";
		}
		
		$disList="";

		if(isset($_SESSION['selectedDistributorId']) && $_SESSION['selectedDistributorId']!="A"){
			$disList=" AND ord.distributor_id=".$_SESSION['selectedDistributorId']." ";
		}

		$disCondition = "";
	  if($_SESSION['userLoginType']==5)  {
	  	 $DivisionId=$_objAdmin->_getSelectList2('table_salesman','division_id',''," salesman_id='".$_SESSION['salesmanId']."'");
	  	  $disCondition =  " AND d.division_id IN (".$DivisionId[0]->division_id.")";
	  	  //$disCondition =  " d.division_id IN (".$divisionIdString.")";
	  } else  if($_SESSION['userLoginType']==3)  {
	  	$disCondition = " d.distributor_id='".$_SESSION['selectedDistributorId']."'";
	  }

	       $where = "  ord.order_type='cs' AND ord.distributor_id!='' $disCondition $disList  $fromdate $todate";  
		
		$auRec=$_objAdmin->_getSelectList('table_order as ord 
			LEFT JOIN table_order_detail AS od ON od.order_id = ord.order_id  
		left join table_distributors as d on d.distributor_id=ord.distributor_id  
		LEFT JOIN table_division AS divi ON divi.division_id = d.division_id ',
		"ord.*, od.order_detail_status AS od_sts,d.distributor_name,d.distributor_code,d.display_outlet, d.distributor_address, d.distributor_location, divi.division_name ",'',$where.$sort,'');
		
	
	     for($i=0;$i<count($auRec);$i++){
			if($auRec[$i]->order_status=='A')
			{
			    $status="New Order";				
			}elseif($auRec[$i]->order_status=='I')
			{
			    $status="Processed";
			}elseif($auRec[$i]->order_status=='D')
			{
				$status="Dispatched (Counter Sale)";					
			}
				if($auRec[$i]->display_outlet=='Y'){$outlet='Yes';} else if($auRec[$i]->display_outlet=='N'){ $outlet='No';} else{ $outlet=''; }
				$remove = array("\n", "\r\n", "\r");
			//$status=($auRec[$i]->order_status=='A')?"New Adhoc Order":"Processed";
			//$color=($auRec[$i]->order_status=='A')?"O":"P";
			//$status=($auRec[$i]->order_status=='A')?"New Order":"Processed";
			
			$date_of_order=$_objAdmin->_changeDate($auRec[$i]->date_of_order);
	    $remove = array(",", "-", "_");
		$data.="".
		$date_of_order.
		"\t".$auRec[$i]->time_of_order.
		"\t".$auRec[$i]->order_id.
		"\t".$auRec[$i]->acc_total_invoice_amount.
		"\t".str_replace($remove, ' ',addslashes($auRec[$i]->distributor_name)).
		"\t".$auRec[$i]->distributor_code.
		"\t".$auRec[$i]->division_name.
		"\t".$outlet.
		"\t".str_replace($remove, ' ',addslashes($auRec[$i]->distributor_location)).
		"\t".str_replace($remove, ' ',addslashes($auRec[$i]->distributor_address)).
		"\t".$status."\n";
		}
			 
	header("Content-type: application/octet-stream");
	header("Content-Disposition: attachment; filename=\"Distributor Counter Sales.xls\"");		
	echo $data;
	exit;
	}
	 /**********End export distributor counter sales***********/




	   /******************
	*Desc:Export distributor Actual Stock report 
	 By: Maninder Kumar
	 On:29th April 2016
	 ***************************/

	 if(isset($_GET['export_distributor_stock_report'])){	
	//echo hello;exit;
	
	$data=" Distributor Code\t Distributor Name\t Category Name\t Item Code\t Item Name\t BSN\t Last Update Date\t Status\n";	 
		
		$where = "";
		$dis="";
		$divisionCondition = "";

		/***Filter added by Maninder on 29th April 2016**********************************************/
			if(isset($_SESSION['distributorIDSale'])){ 

		$querAccount= mysql_query("SELECT session_id, division_id FROM table_distributors WHERE distributor_id='".$_SESSION['distributorIDSale']."'");
				$stockRec = mysql_fetch_array($querAccount);
				
				// if($stockRec['division_id']>0){
				// 	$divisionCondition .= " AND IDR.division_id = '".$stockRec['division_id']."' ";
				// }
				// else {
				// 	$divisionCondition .= " AND IDR.division_id = -1 "; 
				// }

				// AJAY@2016-06-01
				$divisionJoin = "";
				$divisionCondition = "";
				if($stockRec['division_id']>0) {
					$divisionJoin = " LEFT JOIN table_item_division_relationship AS IDR ON IDR.item_id = i.item_id ";
					$divisionCondition = " AND IDR.division_id = '".$stockRec['division_id']."' ";
				}



				$where .= " AND s.dis_stock_value!=0 AND s.distributor_id='".$_SESSION['distributorIDSale']."' $divisionCondition ";	      
			}

			if(isset($_SESSION['itemDisId'])){
	          $where .= " AND s.dis_stock_value!=0 AND s.item_id='".$_SESSION['itemDisId']."' ";
			}

			$where .= " AND s.dis_stock_value!=0 ";
			
		/**************************************************/
		
		if($_SESSION['distributorId']!="" && $_SESSION['distributorId']!="0")
		{
		 $dis .= " and d.distributor_id='".$_SESSION['distributorId']."'";
		}

		$auRec=$_objAdmin->_getSelectList('table_item_distributor_stock AS s 
		LEFT JOIN table_item_bsn AS BSN ON BSN.bsn_id = s.bsn_id
		LEFT JOIN table_category AS c ON c.category_id = s.category_id 
		LEFT JOIN table_item AS i ON i.item_id = s.item_id 
		LEFT JOIN table_distributors AS d ON d.distributor_id = s.distributor_id '.$divisionJoin,"d.distributor_code,d.distributor_name, c.category_name,  i.item_name, i.item_code, s.dis_stock_value, s.status, s.last_update_datetime,s.dis_stk_id, BSN.bsn",$rp,$where,'');

	
	     for($i=0;$i<count($auRec);$i++){			
				
				$remove = array("\n", "\r\n", "\r");
				$last_update_datetime=$_objAdmin->_changeDate($auRec[$i]->last_update_datetime);
				$remove = array(",", "-", "_");
				$data.=""
				.$auRec[$i]->distributor_code.
				"\t".str_replace(',',' ',addslashes($auRec[$i]->distributor_name)).
				"\t".str_replace(',',' ',addslashes($auRec[$i]->category_name)).
				"\t".$auRec[$i]->item_code.
				"\t".str_replace(',',' ',addslashes($auRec[$i]->item_name)).
				"\t".$auRec[$i]->bsn.
				"\t".$auRec[$i]->last_update_datetime.				
				"\t".$auRec[$i]->status."\n";				
		}
			 
	header("Content-type: application/octet-stream");
	header("Content-Disposition: attachment; filename=\"Distributor Stock Report.xls\"");		
	echo $data;
	exit;
	}
	 /**********End export distributor Actual Stock report by Maninder on 29th April 2016***********/
	
	
	if(isset($_GET['export_sales_return'])){	
	
		$data="Salesman \t Salesman Code \t Distributor \t Distributor Code \t Retailer \t Retailer Code \t Number Of Item\t Date\t  Time\n";
		$sort = " ORDER BY date_of_order desc,time_of_order desc";
		if($_SESSION['FromSalesOrderList']!=''){
			$fromdate=" and o.date_of_order >= '".date('Y-m-d', strtotime($_SESSION['FromSalesOrderList']))."'";
		}
		else
		{
			$fromdate=" and o.date_of_order >= '".date('Y-m-d')."'";
		}
		if($_SESSION['ToSalesOrderList']!=''){
			$todate=" and o.date_of_order <= '".date('Y-m-d', strtotime($_SESSION['ToSalesOrderList']))."'";
		}
		else
		{
			$todate=" and o.date_of_order <= '".date('Y-m-d')."'";
		}

		$where = " o.account_id =".$_SESSION['accountId']."  AND o.order_status='A' AND ostype IN ('Q') $salesman $fromdate $todate";
		
		
		
		$auRec=$_objAdmin->_getSelectList('table_sales_return AS o 
			LEFT JOIN table_retailer AS r ON o.retailer_id=r.retailer_id 
			LEFT JOIN table_salesman AS s ON o.salesman_id=s.salesman_id 
			LEFT JOIN table_distributors AS d ON o.distributor_id=d.distributor_id',
			"o.*,
			r.retailer_name,
			r.retailer_code,
			r.retailer_address,
			r.retailer_location,
			r.lat AS retlat,
			r.lng AS retlng,
			s.salesman_name,
			s.salesman_code,
			d.distributor_code,
			d.distributor_name",'',$where.$sort,'');
			
			for($i=0;$i<count($auRec);$i++){
		
		if(ucwords(strtolower($auRec[$i]->order_type))=='Yes'){
			if($auRec[$i]->order_status=='A'){
				$status="New Sales Return";
			}
			
			if($auRec[$i]->order_status=='I')
			{
				$status="Processed";
			}
			
			if($auRec[$i]->order_status=='D')
			{
				$status="Dispatched";
			}
		} 
			
$auRec2=$_objAdmin->_getSelectList2('table_sales_return_detail INNER JOIN table_item ON table_item.item_id = table_sales_return_detail.item_id INNER JOIN table_price on table_sales_return_detail.item_id=table_price.item_id',"count(table_sales_return_detail.item_id) AS total_item, SUM(quantity * item_mrp) AS Total ",''," sales_order_id = ".$auRec[$i]->sales_order_id."");
	
	
$data.="".$auRec[$i]->salesman_name."\t".$auRec[$i]->salesman_code. "\t".$auRec[$i]->distributor_name. "\t".$auRec[$i]->distributor_code. "\t".$auRec[$i]->retailer_name. "\t".$auRec[$i]->retailer_code. "\t".$auRec2[0]->total_item."\t".$_objAdmin->_changeDate($auRec[$i]->date_of_order)."\t".$auRec[$i]->time_of_order."\n";
		}
	
	
	header("Content-type: application/octet-stream");
	header("Content-Disposition: attachment; filename=\"Sales Return.xls\"");		
	echo $data;
	exit;
	}
	
	
	if(isset($_GET['export_transaction_list'])){	
	
		$data="Salesman\t Salesman Code\t Distributor Name\t Dealer\t Dealer Code\t District\t State\t Transaction Type\t Bank\t Amount\t Outstanding Amount\t Date\t Time\n";
		$sort = " ORDER BY transaction_date desc,transaction_time desc";

		if($_SESSION['transsalesmanID']!=''){
		
			$salesman=" AND s.salesman_id = '".$_SESSION['transsalesmanID']."'";

		}
		
		if($_SESSION['retailerID']!=''){
			$retailer=" AND r.retailer_id = '".$_SESSION['retailerID']."'";
			
		} 
		
		
		if($_SESSION['FromTransList']!=''){
			$fromdate=" and td.transaction_date >= '".date('Y-m-d', strtotime($_SESSION['FromTransList']))."'";
		}
		else
		{
			$fromdate=" and td.transaction_date >= '".date('Y-m-d')."'";
		}
		if($_SESSION['ToTransList']!=''){
			$todate=" and td.transaction_date <= '".date('Y-m-d', strtotime($_SESSION['ToTransList']))."'";
		}
		else
		{
			$todate=" and td.transaction_date <= '".date('Y-m-d')."'";
		}

		$where = " $salesman $retailer $fromdate $todate";
		
			$auRec=$_objAdmin->_getSelectList('table_transaction_details as td left join table_retailer as r on td.retailer_id=r.retailer_id and td.ret_type ="R" 
				left join state as st on r.state = st.state_id
			    left join city as ct on ct.city_id = r.city 
				left join table_distributors as dl on td.retailer_id=dl.distributor_id and td.ret_type ="D"
			    left join table_salesman as s on td.salesman_id=s.salesman_id',"td.*,s.salesman_name,s.salesman_code,r.retailer_name,r.retailer_location,r.retailer_address,r.retailer_code,dl.distributor_name,dl.distributor_code,st.state_name,ct.city_name",'',$where.$sort,'');	
			
			for($i=0;$i<count($auRec);$i++){
				
				if($auRec[$i]->transaction_type==1){
					$type="Cash";
				}else if($auRec[$i]->transaction_type==2){
					$type="Cheque";
				}else{
					$type="Netbanking";
				}
	
$data.="".$auRec[$i]->salesman_name."\t".$auRec[$i]->salesman_code."\t".$auRec[$i]->distributor_name."\t".$auRec[$i]->retailer_name."\t".$auRec[$i]->retailer_code."\t".$auRec[$i]->city_name."\t".$auRec[$i]->state_name."\t".$type."\t".$auRec[$i]->issuing_bank."\t".round($auRec[$i]->total_sale_amount)."\t".$auRec[$i]->outstanding_amount."\t".$_objAdmin->_changeDate($auRec[$i]->transaction_date)."\t".$auRec[$i]->transaction_time."\n";
		}
	
	header("Content-type: application/octet-stream");
	header("Content-Disposition: attachment; filename=\"Transaction Details.xls\"");		
	echo $data;
	exit;
	}

/************************************** 
**	Export consolidated_activity_report  
**  by Gyanendra
**  Date: 15th jan 2015 
***************************************/

 if(isset($_GET['export_activity_report'])){	

 	$from_date = $_REQUEST['fdate'];
	$to_date   = $_REQUEST['tdate'];

	
	$data="Start Date: ".$_objAdmin->_changeDate($from_date)."\t\t End Date: ".$_objAdmin->_changeDate($to_date)."\n";
	$data.="Ind :- Individual\t\t HL :- Hierarchy Level\n";
	$data.="Level\t Reporting To\t Total Calls(Ind)\t Productive Calls(Ind)\t New Retailer Added(Ind)\t Amount of Order(Ind)\t Total Calls(HL)\t Productive Calls(HL)\t New Retailer Added(HL)\t Amount of Order(HL)\n\n";
	echo $data;
	$from_date = $_REQUEST['fdate'];
	$to_date   = $_REQUEST['tdate'];

	$total_call   	 = "";
	$total_amount	 = "";
	$productive_call = "";
	$total_retailer  = "";
 
 if(isset($_SESSION['rptSal']) && $_SESSION['rptSal']!="All")
	{
	
		$qrySet="select SH.sort_order from table_salesman_hierarchy_relationship as SHR LEFT JOIN table_salesman_hierarchy as SH ON SHR.hierarchy_id=SH.hierarchy_id WHERE SH.account_id=".$_SESSION['accountId']." AND SHR.salesman_id='".$_SESSION['rptSal']."'";
		
		 $resultSet=mysql_query($qrySet);
		 $row = mysql_fetch_assoc($resultSet);		 
		 $curLevelOrder=$row['sort_order']; 
		 $salID=array($_SESSION['rptSal']);

		 $getList=$_objArrayList->getSalesbottomhierarchy($salID, $curLevelOrder);

		 if(!empty($getList)) {
					 $getList=implode(',',$getList);
				$salesman = " AND SHR.salesman_id IN ($getList)";
		 }
		 else {
			 $salesman = " AND SHR.salesman_id ='".$_SESSION['rptSal']."'";
		 
		 }
				
	} 

  $qry="select s.salesman_id, s.salesman_name, SHR.rpt_user_id AS parent_id, SH.description AS SalLevel,SH.sort_order, S2.salesman_name AS rptPerson, H2.description AS rptLevel
  	  from table_salesman_hierarchy_relationship AS SHR 
      LEFT JOIN table_salesman AS s ON s.salesman_id = SHR.salesman_id 
  

		LEFT JOIN table_salesman_hierarchy AS SH ON SH.hierarchy_id = SHR.hierarchy_id
		LEFT JOIN table_salesman_hierarchy AS H2 ON H2.hierarchy_id = SHR.rpt_hierarchy_id
		LEFT JOIN table_salesman AS S2 ON S2.salesman_id = SHR.rpt_user_id


  WHERE SH.account_id=".$_SESSION['accountId']." $salesman ORDER BY SH.sort_order";																																																																		

 $result=mysql_query($qry);

 $salesmanHirearchySet = array();

 while($row = mysql_fetch_assoc($result)){ 
	
		$td1 = $_objAdmin->_getSelectList('table_order as o left join table_retailer as r on o.retailer_id=r.retailer_id','count(order_id) as total_call, sum( o.total_invoice_amount ) as total_amt ',''," (o.date_of_order BETWEEN '".date('Y-m-d', strtotime($from_date))."' AND '".date('Y-m-d', strtotime($to_date))."') and r.new='' and o.salesman_id='".$row['salesman_id']."'"); 
 
		$td2 = $_objAdmin->_getSelectList('table_order as o left join table_retailer as r on o.retailer_id=r.retailer_id','count(order_id) as productive_call',''," (o.date_of_order BETWEEN '".date('Y-m-d', strtotime($from_date))."' AND '".date('Y-m-d', strtotime($to_date))."') and o.order_type != 'No' and r.new='' and o.salesman_id='".$row['salesman_id']."'");

		$td3 = $_objAdmin->_getSelectList2('table_activity','count(activity_id) as total_retailer','',"(activity_date BETWEEN '".date('Y-m-d', strtotime($from_date))."' AND '".date('Y-m-d', strtotime($to_date))."') and salesman_id='".$row['salesman_id']."' and activity_type = '5'");		

		$salesmanHirearchySet[$row['salesman_id']] = array("parent_id" => $row['parent_id'], "rptPerson" =>$row['rptPerson'], 'rptLevel'=>$row['rptLevel'],"salesman_name" => $row['salesman_name'],'level'=>$row['SalLevel'],'salesman_id'=>$row['salesman_id'],'sort_order'=>$row['sort_order'],'total_call'=>$td1[0]->total_call,'total_amount'=>$td1[0]->total_amt,'productive_call'=>$td2[0]->productive_call,'total_retailer'=>$td3[0]->total_retailer);   
	
  }

 function createTree($array, $currentParent, $currLevel = 0, $prevLevel = -1, $sort_order=0) {
		
foreach ($array as $key => $value) {
	
if ($currentParent == $value['parent_id'] && $sort_order < $value['sort_order']) 
{  
		$salID              = array($value['salesman_id']);
		$sortOrder          = $value['sort_order'];	
		$objArrayList	    = new ArrayList();	
		$_objAdmin          = new Admin();  
		$getList            = $objArrayList->getSalesbottomhierarchy($salID, $sortOrder);
		
		$levelTotalSalCount = sizeof($getList);
		$getList            = implode(',',$getList);
		
		$level_call_amount = $_objAdmin->_getSelectList('table_order as o left join table_retailer as r on o.retailer_id=r.retailer_id','count(order_id) as level_total_call, sum( o.total_invoice_amount ) as level_total_amt ',''," (o.date_of_order BETWEEN '".$_SESSION['rptForm']."' AND '".$_SESSION['rptTo']."') and r.new='' and o.salesman_id IN ($getList)"); 

		$productive_call = $_objAdmin->_getSelectList('table_order as o left join table_retailer as r on o.retailer_id=r.retailer_id','count(order_id) as level_productive_call',''," (o.date_of_order BETWEEN '".$_SESSION['rptForm']."' AND '".$_SESSION['rptTo']."') and o.order_type != 'No' and r.new='' and o.salesman_id IN ($getList)");

		$retailer = $_objAdmin->_getSelectList2('table_activity','count(activity_id) as level_total_retailer','',"(activity_date BETWEEN '".$_SESSION['rptForm']."' AND '".$_SESSION['rptTo']."') and salesman_id IN ($getList) and activity_type = '5'");
		
        $level_total_call      = $level_call_amount[0]->level_total_call;
		$level_total_amount    = $level_call_amount[0]->level_total_amt;
		$level_productive_call = $productive_call[0]->level_productive_call;
		$level_total_retailer  = $retailer[0]->level_total_retailer;

	$total_call 	 = $value['total_call']?$value['total_call']:0;
	$productive_call = $value['productive_call']?$value['productive_call']:0;
	$total_retailer  = $value['total_retailer']?$value['total_retailer']:0;
	$total_amount 	 = $value['total_amount']?$value['total_amount']:'0.00';
		
	$level_total_call 	   = $level_total_call?$level_total_call:0;
	$level_productive_call = $level_productive_call?$level_productive_call:0;
	$level_total_retailer  = $level_total_retailer?$level_total_retailer:0;
	$level_total_amount    = $level_total_amount?$level_total_amount:'0.00';
	
	$exportFormatArray['salesman_name'] = $value['salesman_name'];	
	$exportFormatArray['level']         = $value['level'];
	if($value['parent_id'] == 0){
		$value['rptLevel'] = 'N/A';
	}
	header("Content-type: application/octet-stream");	

     $data = "".$value['salesman_name']." (".$value['level'].")\t".$value['rptPerson']." (".$value['rptLevel'].")\t".$total_call."\t".$productive_call."\t".$total_retailer."\t".$total_amount."\t".$level_total_call."\t".$level_productive_call."\t".$level_total_retailer."\t".$level_total_amount."\n";
    echo $data;
    if ($currLevel > $prevLevel) { $prevLevel = $currLevel; } 
    $currLevel++; 
		
    createTree ($array, $key, $currLevel, $prevLevel,$value['sort_order']);	
    $currLevel--;  
    }  

} 
}
if(mysql_num_rows($result)!=0)
 {
	if($_SESSION['userLoginType']=='5' || $_SESSION['rptSal']!=''){
	
	
	if(isset($_SESSION['userLoginType'])){ $salesmanID=$_SESSION['salesmanId']; }
	if(isset($_SESSION['rptSal'])){ $salesmanID=$_SESSION['rptSal']; }

 $qrySetResult="select rpt_user_id
  from table_salesman_hierarchy_relationship 
  WHERE account_id=".$_SESSION['accountId']." AND salesman_id='".$salesmanID."'";
 $resultSetquery=mysql_query($qrySetResult);
 	$row = mysql_fetch_assoc($resultSetquery);
	$curLevel=$row['rpt_user_id']; 

 if($row['rpt_user_id']=='')
 	{
		$curLevel=0;
	}
 } 
 else { 
 	$curLevel=0;
 } 
 createTree($salesmanHirearchySet, $curLevel); 
}
	header("Content-Disposition: attachment; filename=\"Consolidated Activity.xls\"");		
	exit;
}

/****************************** 
**  Export consolidated_report  
**  by Gyanendra
**  Date: 16th jan 2015 
********************************/

 if(isset($_GET['export_consolidated_report'])){	

 	$from_date = $_REQUEST['fdate'];
	$to_date   = $_REQUEST['tdate'];

	
	$data="Start Date: ".$_objAdmin->_changeDate($from_date)."\t\t End Date: ".$_objAdmin->_changeDate($to_date)."\n";
	$data.="Hierarchy Level\t Reporting to\t Individual Total Quantity\t Individual Total Amount\t Hierarchy Total Quantity\t Hierarchy Total Amount\n\n";
	echo $data;
	 if(isset($_SESSION['rptSal']) && $_SESSION['rptSal']!="All")
	{
	
		 $qrySet="select SH.sort_order from table_salesman_hierarchy_relationship as SHR LEFT JOIN table_salesman_hierarchy as SH ON SHR.hierarchy_id=SH.hierarchy_id WHERE SH.account_id=".$_SESSION['accountId']." AND SHR.salesman_id='".$_SESSION['rptSal']."'";
		
		 $resultSet=mysql_query($qrySet);
		 $row = mysql_fetch_assoc($resultSet);
		 $curLevelOrder=$row['sort_order']; 
		 $salID=array($_SESSION['rptSal']);
		 $getList=$_objArrayList->getSalesbottomhierarchy($salID, $curLevelOrder);
		 if(!empty($getList)) {
					 $getList=implode(',',$getList);
					 $salesman = " AND SHR.salesman_id IN ($getList)";
		 }
		 else {
		 	$salesman = " AND SHR.salesman_id ='".$_SESSION['rptSal']."'";
		 
		 }
				
	} 

  $qry="select s.salesman_id, s.salesman_name, SHR.rpt_user_id AS parent_id, SH.description AS SalLevel,SH.sort_order, S2. 	salesman_name AS rptPerson, H2.description AS rptLevel
 	    from table_salesman_hierarchy_relationship AS SHR 
        LEFT JOIN table_salesman AS s ON s.salesman_id = SHR.salesman_id 
        LEFT JOIN table_salesman_hierarchy AS SH ON SH.hierarchy_id = SHR.hierarchy_id
        LEFT JOIN table_salesman_hierarchy AS H2 ON H2.hierarchy_id = SHR.rpt_hierarchy_id
		LEFT JOIN table_salesman AS S2 ON S2.salesman_id = SHR.rpt_user_id

  WHERE SH.account_id=".$_SESSION['accountId']." $salesman ORDER BY SH.sort_order";
  
 $result=mysql_query($qry);


 $salesmanHirearchySet = array();

 while($row = mysql_fetch_assoc($result)){ 

	 $salesmanHirearchySet[$row['salesman_id']] = array("parent_id" => $row['parent_id'], "rptPerson" =>$row['rptPerson'], 'rptLevel'=>$row['rptLevel'], "salesman_name" => $row['salesman_name'],'level'=>$row['SalLevel'],'salesman_id'=>$row['salesman_id'],'sort_order'=>$row['sort_order']);
  }

 function createTree($array, $currentParent, $currLevel = 0, $prevLevel = -1, $sort_order=0) {
 
 		
foreach ($array as $key => $value) {
	
if ($currentParent == $value['parent_id'] && $sort_order < $value['sort_order']) 
{ 
	
		/*$qrySelAmount="SELECT SUM(O.acc_total_invoice_amount) as total
		FROM table_order as O
		left join table_retailer as R on O.retailer_id=R.retailer_id
		left join table_salesman as s on O.salesman_id=s.salesman_id 
		left join table_distributors as d on O.distributor_id=d.distributor_id
		WHERE O.salesman_id='".$value['salesman_id']."' and R.new='' AND O.date_of_order >='".$_SESSION['rptForm']."' AND O.date_of_order <='".$_SESSION['rptTo']."'";*/

		$qrySelAmount="SELECT SUM(O.acc_total_invoice_amount) as total
		FROM table_order as O
		left join table_retailer as R on O.retailer_id=R.retailer_id
		left join table_salesman as s on O.salesman_id=s.salesman_id 
		left join table_distributors as d on O.distributor_id=d.distributor_id
		WHERE O.salesman_id='".$value['salesman_id']."' and O.date_of_order >='".$_SESSION['rptForm']."' AND O.date_of_order <='".$_SESSION['rptTo']."'";
		
		
		/*$qrySelQuantity="SELECT SUM(OD.acc_quantity) as totalQuantity
		FROM table_order as O
		left join table_order_detail as OD on O.order_id=OD.order_id 
		left join table_retailer as R on O.retailer_id=R.retailer_id
		left join table_salesman as s on O.salesman_id=s.salesman_id 
		left join table_distributors as d on O.distributor_id=d.distributor_id
		WHERE O.salesman_id='".$value['salesman_id']."' and R.new='' and OD.total_free_quantity='' AND O.date_of_order >='".$_SESSION['rptForm']."' AND O.date_of_order <='".$_SESSION['rptTo']."'";*/

		$qrySelQuantity="SELECT SUM(OD.acc_quantity) as totalQuantity
		FROM table_order as O
		left join table_order_detail as OD on O.order_id=OD.order_id 
		left join table_retailer as R on O.retailer_id=R.retailer_id
		left join table_salesman as s on O.salesman_id=s.salesman_id 
		left join table_distributors as d on O.distributor_id=d.distributor_id
		WHERE O.salesman_id='".$value['salesman_id']."' and OD.total_free_quantity='' AND O.date_of_order >='".$_SESSION['rptForm']."' AND O.date_of_order <='".$_SESSION['rptTo']."'";
		
		
 	$resultSelAmount=mysql_query($qrySelAmount);
	$rowSelAmount = mysql_fetch_assoc($resultSelAmount);
	
	$resultSelQuantity=mysql_query($qrySelQuantity);
	$rowSelQuantity = mysql_fetch_assoc($resultSelQuantity);
	
	$salesmanTotal=$rowSelAmount['total'];
	$salesmanTotalQuantity=$rowSelQuantity['totalQuantity'];
	
	$salID=array($value['salesman_id']);
	$sortOrder=$value['sort_order'];
	
		$objArrayList= new ArrayList();
		$getList=$objArrayList->getSalesbottomhierarchy($salID, $sortOrder);

		$getList=implode(',',$getList);
		
		/*$levelWiseAmountSum="SELECT SUM(O.acc_total_invoice_amount) as total
		FROM table_order as O		
		left join table_retailer as R ON O.retailer_id=R.retailer_id
		left join table_salesman as s on O.salesman_id=s.salesman_id 
		left join table_distributors as d on O.distributor_id=d.distributor_id
		WHERE O.salesman_id IN ($getList) AND R.new='' AND O.date_of_order >='".$_SESSION['rptForm']."' AND O.date_of_order <='".$_SESSION['rptTo']."'";*/

		$levelWiseAmountSum="SELECT SUM(O.acc_total_invoice_amount) as total
		FROM table_order as O		
		left join table_retailer as R ON O.retailer_id=R.retailer_id
		left join table_salesman as s on O.salesman_id=s.salesman_id 
		left join table_distributors as d on O.distributor_id=d.distributor_id
		WHERE O.salesman_id IN ($getList) AND O.date_of_order >='".$_SESSION['rptForm']."' AND O.date_of_order <='".$_SESSION['rptTo']."'";
		
		/*$levelWiseQuantitySum="SELECT SUM(OD.acc_quantity) as totalQuantity
		FROM table_order as O
		left join table_order_detail as OD on O.order_id=OD.order_id
		left join table_retailer as R ON O.retailer_id=R.retailer_id
		left join table_salesman as s on O.salesman_id=s.salesman_id 
		left join table_distributors as d on O.distributor_id=d.distributor_id
		WHERE O.salesman_id IN ($getList) AND R.new='' AND OD.total_free_quantity='' AND O.date_of_order >='".$_SESSION['rptForm']."' AND O.date_of_order <='".$_SESSION['rptTo']."'";*/

		$levelWiseQuantitySum="SELECT SUM(OD.acc_quantity) as totalQuantity
		FROM table_order as O
		left join table_order_detail as OD on O.order_id=OD.order_id
		left join table_retailer as R ON O.retailer_id=R.retailer_id
		left join table_salesman as s on O.salesman_id=s.salesman_id 
		left join table_distributors as d on O.distributor_id=d.distributor_id
		WHERE O.salesman_id IN ($getList) AND OD.total_free_quantity='' AND O.date_of_order >='".$_SESSION['rptForm']."' AND O.date_of_order <='".$_SESSION['rptTo']."'";
		
		
		$resultAmountSum=mysql_query($levelWiseAmountSum);
		$salAmountSum = mysql_fetch_assoc($resultAmountSum);
		
		$resultQuantitySum=mysql_query($levelWiseQuantitySum);
		$salQuantitySum = mysql_fetch_assoc($resultQuantitySum);
		
		$levelTotal=$salAmountSum['total'];
		$levelTotalQuantity=$salQuantitySum['totalQuantity']; 


	
	$selfTotalQuantity  = $salesmanTotalQuantity ? $salesmanTotalQuantity : 0;
	$selfTotalAmount    = $salesmanTotal ? $salesmanTotal : '0.00';
	$levelTotalQuantity = $levelTotalQuantity ? $levelTotalQuantity : 0;
	$levelTotalAmount   = $levelTotal ? $levelTotal : '0.00';
	if($value['parent_id'] == 0){
		$value['rptLevel'] = 'N/A';
	}
	 header("Content-type: application/octet-stream");	

     $data = "".$value['salesman_name']." (".$value['level'].")\t".$value['rptPerson']." (".$value['rptLevel'].")\t".$selfTotalQuantity."\t".$selfTotalAmount."\t".$levelTotalQuantity."\t".$levelTotalAmount."\n";
    echo $data;
	     
    if ($currLevel > $prevLevel) { $prevLevel = $currLevel; } 
    $currLevel++; 
			
    createTree ($array, $key, $currLevel, $prevLevel,$value['sort_order']);
	
    $currLevel--;      

    }  

}

}
 if(mysql_num_rows($result)!=0)
 {
	if($_SESSION['userLoginType']=='5' || $_SESSION['rptSal']!=''){
	
	
	if(isset($_SESSION['userLoginType'])){ $salesmanID=$_SESSION['salesmanId']; }
	if(isset($_SESSION['rptSal'])){ $salesmanID=$_SESSION['rptSal']; }
 $qrySetResult="select rpt_user_id
  from table_salesman_hierarchy_relationship 
  WHERE account_id=".$_SESSION['accountId']." AND salesman_id='".$salesmanID."'";
 $resultSetquery=mysql_query($qrySetResult);
 	$row = mysql_fetch_assoc($resultSetquery);
	$curLevel=$row['rpt_user_id']; 
 if($row['rpt_user_id']=='')
 	{
		$curLevel=0;
	}
 }
 else {
 	$curLevel=0;
 }
 createTree($salesmanHirearchySet, $curLevel);
}
	header("Content-Disposition: attachment; filename=\"Consolidated Sales Report.xls\"");		
	exit;
}


/**************************************** 
**  Export consolidated_attendance_report  
**  by Gyanendra
**  Date: 16th jan 2015 
*****************************************/

 if(isset($_GET['export_consolidated_attendance_report'])){	

 	$from_date = $_REQUEST['fdate'];
	$to_date   = $_REQUEST['tdate'];

	
	$data="Start Date: ".$_objAdmin->_changeDate($from_date)."\t\t End Date: ".$_objAdmin->_changeDate($to_date)."\n";
	$data.="Ind :- Individual\t\t HL :- Hierarchy Level\n";
	//$data.="Hierarchy Level\t Reporting to\t Working Hours (Ind)\t Missing Hours (Ind)\t Present (Ind)\t Absent (Ind)\t Working Hours (HL)\t Missing Hours (HL)\t Present (HL)\t Absent (HL)\n";
	$data.="Hierarchy Level\t Reporting to\t Present (Ind)\t Absent (Ind)\t Present (HL)\t Absent (HL)\n\n";
	echo $data;

	 /******************************* get bottom level from hierarchy *************************/
 	  $horder = "select TSH.sort_order AS sort_order from table_salesman_hierarchy AS TSH WHERE TSH.account_id=".$_SESSION['accountId']." ORDER BY TSH.sort_order DESC LIMIT 1";
 	    
 		$lastLevel = mysql_query($horder);
 			while($lastLevelOrder = mysql_fetch_assoc($lastLevel)){ 
 					$bottomLevel = $lastLevelOrder['sort_order'];
 			}

 if(isset($_SESSION['rptSal']) && $_SESSION['rptSal']!="All")
	{
		$appUserActivityID = "";

		 $qrySet="select SH.sort_order from table_salesman_hierarchy_relationship as SHR LEFT JOIN table_salesman_hierarchy as SH ON SHR.hierarchy_id=SH.hierarchy_id WHERE SH.account_id=".$_SESSION['accountId']." AND SHR.salesman_id='".$_SESSION['rptSal']."'";
		
		 $resultSet 	= mysql_query($qrySet);
		 $row 			= mysql_fetch_assoc($resultSet);
		 $curLevelOrder = $row['sort_order']; 
		 $salID         = array($_SESSION['rptSal']);

		 $getList = $_objArrayList->getSalesbottomhierarchy($salID, $curLevelOrder);

		 if(!empty($getList)) {
					 $getList=implode(',',$getList);
					 $salesman = " AND SHR.salesman_id IN ($getList)";
		 } else {
		 	$salesman = " AND SHR.salesman_id ='".$_SESSION['rptSal']."'";		 
		 }
				
	} 
	
	function DateCheck($start_date,$end_date){
				$start = strtotime($start_date);
				$end = strtotime($end_date);
				$new_date=array($start_date);
				$date_arr=array();
				$days_count = ceil(abs($end - $start) / 86400)+1;
				for($i=0;$i<$days_count;$i++)
					{
						$date = strtotime ('+1 days', strtotime($start_date)) ;
						$start_date = date ('Y-m-d' , $date);
						$date_arr[$i] = $start_date;
						$week_last_date=$start_date;
						array_push($new_date,$week_last_date);
						$get_newdate[]=array($new_date[$i],$new_date[$i+1]);
					}
					return $get_newdate;
			}	
			
		$dayArray=array();
			
		$d1="".$_SESSION['rptForm']."";
		$d2="".$_SESSION['rptTo']."";
		$duration_dates = DateCheck($d1,$d2);
		foreach($duration_dates as $value)
		{
			$duration_start_date=$value[0];
			$dayArray[]= date('D',strtotime($duration_start_date));
			
		}
		
		$dayCount=count($dayArray);
		
function convertToHoursMins($time, $format = '%d:%d') {
    settype($time, 'integer');
    if ($time < 1) {
        return;
    }
    $hours = floor($time/60);
    $minutes = $time%60;
    return sprintf($format, $hours, $minutes);
}

  $qry="select s.salesman_id, s.salesman_name, SHR.rpt_user_id AS parent_id, SH.description AS SalLevel,SH.sort_order, S2.salesman_name AS rptPerson, H2.description AS rptLevel
 	    from table_salesman_hierarchy_relationship AS SHR 
        LEFT JOIN table_salesman AS s ON s.salesman_id = SHR.salesman_id 
        LEFT JOIN table_salesman_hierarchy AS SH ON SH.hierarchy_id = SHR.hierarchy_id
        LEFT JOIN table_salesman_hierarchy AS H2 ON H2.hierarchy_id = SHR.rpt_hierarchy_id
		LEFT JOIN table_salesman AS S2 ON S2.salesman_id = SHR.rpt_user_id
  WHERE SH.account_id=".$_SESSION['accountId']." $salesman ORDER BY SH.sort_order";

 $result=mysql_query($qry);
 $salesmanHirearchySet = array();

 while($row = mysql_fetch_assoc($result)){ 
 
 			/********************** Start Calculation For Individual Salesman **************************/
 
 	 $getTotalWorkingHours=mysql_query("SELECT SEC_TO_TIME(SUM(TIME_TO_SEC(diffe))) as timeSum FROM salesman_working_time WHERE salesman_id='".$row['salesman_id']."' AND activity_date >='".$_SESSION['rptForm']."' AND activity_date <='".$_SESSION['rptTo']."'");
		 $individualWorkinghours = mysql_fetch_assoc($getTotalWorkingHours);		 
		 
		 if($individualWorkinghours['timeSum']>0 && !empty($individualWorkinghours['timeSum'])){		 
		 	
				$salIndiWorkingTime= date("H:i:s",strtotime("-20 minutes",strtotime($individualWorkinghours['timeSum'])));
					 
			 }	
			 else {
			 		$salIndiWorkingTime="NA";
			 }
			 
			 
			 
	 $getTotalPresent=mysql_query("SELECT count(activity_id) as totalPresent FROM act_in WHERE salesman_id='".$row['salesman_id']."' AND activity_date >='".$_SESSION['rptForm']."' AND activity_date <='".$_SESSION['rptTo']."'");
	 $individualPresent = mysql_fetch_assoc($getTotalPresent);
	 
	 if($individualPresent['totalPresent']>0 && !empty($individualPresent['totalPresent'])){		 
		 		$salIndiPresent=$individualPresent['totalPresent']; 		 
			 }	
			 else {
			 		$salIndiPresent="NA";
			 }
 		
		$salIndividualAbsent=$dayCount-$salIndiPresent;

		$getIndividualGap=mysql_query("SELECT salesman_id,ref_type,ref_id,activity_date,end_time,activity_id,Sec_to_time(@diff) AS starttime,end_time,IF(@diff = 0, 0,Time_to_sec(end_time) - @diff)/60 AS diff,@diff := Time_to_sec(end_time) FROM table_activity,(SELECT @diff := 0) AS x WHERE salesman_id='".$row['salesman_id']."' and activity_date>='".$_SESSION['rptForm']."' and activity_date<='".$_SESSION['rptTo']."' and activity_type in (3,4,5,10,11,12) ORDER  BY table_activity.activity_date asc, table_activity.end_time asc");
	
		while($resultIndividualGap= mysql_fetch_assoc($getIndividualGap)){		

		if($resultIndividualGap['diff']>$_SESSION['GapTime'] ){ 
		
				if($resultIndividualGap['diff']>60){
								$individualMissingHours = convertToHoursMins(floor($resultIndividualGap['diff']), '%d hours %d minutes');
									//echo floor($row['diff'])." minutes";
							   } else {
									$individualMissingHours = floor($resultIndividualGap['diff'])." minutes";
							}
		}
		else {
		
			$individualMissingHours= "NA";
		}
		}
		
	/************************************ End Calculation For Individual Salesman **************************************/

	/******************** Start Calculation For Hirearchy Of Salesman ****************************/
	
		$salID        = array($row['salesman_id']);
		$sortOrder    = $row['sort_order'];
		$objArrayList = new ArrayList();
		$getList      = $objArrayList->getSalesbottomhierarchy($salID, $sortOrder);
		$getHierList  = $getList;
		

		/******************** find out whether user level is app user or not *******************************/
		unset($appUser);
		unset($appUserSalID);
		for($i=0; $i<count($getHierList); $i++){
			
			$getActivityId = mysql_query("SELECT activity_id as activity_id FROM table_activity WHERE salesman_id =".$getHierList[$i]." AND activity_date >='".$_SESSION['rptForm']."' AND activity_date <='".$_SESSION['rptTo']."' AND activity_type in (3,4,5,10,11,12)");
			
			$activityID = mysql_fetch_assoc($getActivityId);  
			if($activityID['activity_id']>0){
				 $appUser[] 	 = $activityID['activity_id'];
				 $appUserSalID[] = $getHierList[$i];
			}
		}

		$appUserCount = count($appUser); 
		$getList      = implode(',',$appUserSalID);
		/************************************ Calculate Hierarchy Working Hours ********************************/
		$getTotalWorkingHoursHierarchy=mysql_query("SELECT SEC_TO_TIME(SUM(TIME_TO_SEC(diffe))) as timeSumHier FROM salesman_working_time WHERE salesman_id in (".$getList.") AND activity_date >='".$_SESSION['rptForm']."' AND activity_date <='".$_SESSION['rptTo']."'");
		
		$hierarchyWorkinghours = mysql_fetch_assoc($getTotalWorkingHoursHierarchy);
		
		 if($hierarchyWorkinghours['timeSumHier']>0 && !empty($hierarchyWorkinghours['timeSumHier'])){				
				$salHierWorkingTime = $hierarchyWorkinghours['timeSumHier']; 
				$salHierWorkingTime = date("H:i:s",strtotime("-20 minutes",strtotime($hierarchyWorkinghours['timeSumHier'])));
		}
		else {
				$salHierWorkingTime="NA"; 
		}
		
		
		$getTotalHirearchyPresent=mysql_query("SELECT count(activity_id) as totalhierPresent FROM act_in WHERE salesman_id in (".$getList.") AND activity_date >='".$_SESSION['rptForm']."' AND activity_date <='".$_SESSION['rptTo']."'");
	 $hierarchyPresent = mysql_fetch_assoc($getTotalHirearchyPresent);
	
	 if($hierarchyPresent['totalhierPresent']>0 && !empty($hierarchyPresent['totalhierPresent'])){		 
		 		$salhiearchyPresent=$hierarchyPresent['totalhierPresent']; 		 
			 } else {
			 	$salhiearchyPresent="NA";
			 }	
		
		/************* Calculation of total Absent of Hirearchy ****************/

			$salHierarchyAbsent = $dayCount*$appUserCount-$salhiearchyPresent;
		
		$hierarchyMissingHours = "";
		$getHierarchyGap=mysql_query("SELECT Sec_to_time(@diff) AS starttime,end_time,IF(@diff = 0, 0,Time_to_sec(end_time) - @diff)/60 AS diff,@diff := Time_to_sec(end_time) FROM table_activity,(SELECT @diff := 0) AS x WHERE salesman_id IN (".$getList.") and activity_date>='".$_SESSION['rptForm']."' and activity_date<='".$_SESSION['rptTo']."' and activity_type in (3,4,5,10,11,12) ORDER  BY table_activity.activity_date asc, table_activity.end_time asc");
		
		while($resultHierarchyGap= mysql_fetch_assoc($getHierarchyGap)){
		
					if($resultHierarchyGap['diff']>$_SESSION['GapTime'] ){ 				
						if($resultHierarchyGap['diff']>60){
							$hierarchyMissingHours = convertToHoursMins(floor($resultHierarchyGap['diff']), '%d hours %d minutes');
						} else {
							$hierarchyMissingHours = floor($resultHierarchyGap['diff'])." minutes";
						}
					} else {		
						$hierarchyMissingHours = "NA";
					}
			}

 	/************************** End Calculation For Hirearchy Of Salesman ******************************/

	 $salesmanHirearchySet[$row['salesman_id']] = array("parent_id" => $row['parent_id'], "rptPerson" =>$row['rptPerson'], 'rptLevel'=>$row['rptLevel'], "salesman_name" => $row['salesman_name'],'level'=>$row['SalLevel'],'salesman_id'=>$row['salesman_id'],'sort_order'=>$row['sort_order'],'individualWorking'=>$salIndiWorkingTime,'hierarchyWorking'=>$salHierWorkingTime,'individualTotalPresent'=>$salIndiPresent,'hierarchyTotalPresent'=>$salhiearchyPresent,'individualAbsent'=>$salIndividualAbsent,'hierarchyAbsent'=>$salHierarchyAbsent,'individualMissingWorkingTime'=>$individualMissingHours,'hierarchyMissingWorkingTime'=>$hierarchyMissingHours, 'bottomLevel'=>$bottomLevel, 'appSalId'=>$appUserSalID);   
 	 }

 function createTree($array, $currentParent, $currLevel = 0, $prevLevel = -1, $sort_order=0) {
		
foreach ($array as $key => $value) {
	
if ($currentParent == $value['parent_id'] && $sort_order < $value['sort_order']) 
{ 
		
		$salesmanWorkingTime                 = $value['individualWorking'];
		$salesmanHierarchyWorkingTime        = $value['hierarchyWorking'];
		$salesmanPresentTotal                = $value['individualTotalPresent'];
		$salesmanHierarchyPresentTotal       = $value['hierarchyTotalPresent'];
		$salesmanAbsentTotal                 = $value['individualAbsent'];
		$salesmanHierarchyAbsentTotal        = $value['hierarchyAbsent'];
		$salesmanMissingWorkingTime          = $value['individualMissingWorkingTime'];
		$salesmanHierarchyMissingWorkingTime = $value['hierarchyMissingWorkingTime'];
		
		if($value['parent_id'] == 0){
			$value['rptLevel'] = 'N/A';
		}
		
	header("Content-type: application/octet-stream");	

  		 $data = "".$value['salesman_name']." (".$value['level'].")\t".$value['rptPerson']." (".$value['rptLevel'].")\t";
  		if(in_array($value['salesman_id'], $value['appSalId'])){	
  		 //$data.=$salesmanWorkingTime."\t".$salesmanMissingWorkingTime."\t".$salesmanPresentTotal."\t".$salesmanAbsentTotal."\t";
  			$data.=$salesmanPresentTotal."\t".$salesmanAbsentTotal."\t";
  		} else  $data.='-'."\t".'-'."\t";
  		//else  $data.='-'."\t".'-'."\t".'-'."\t".'-'."\t";
  		if($value['sort_order'] != $value['bottomLevel']){
  		 //$data.=$salesmanHierarchyWorkingTime."\t".$salesmanHierarchyMissingWorkingTime."\t".$salesmanHierarchyPresentTotal."\t".$salesmanHierarchyAbsentTotal."\n";
  		 $data.=$salesmanHierarchyPresentTotal."\t".$salesmanHierarchyAbsentTotal."\n";
  		}else $data.='-'."\t".'-'."\t\n";
  		//$data.='-'."\t".'-'."\t".'-'."\t".'-'."\t\n";
   		 echo $data;
	     
      
    if ($currLevel > $prevLevel) { $prevLevel = $currLevel; } 
    $currLevel++; 
			
    createTree ($array, $key, $currLevel, $prevLevel,$value['sort_order']);
	
    $currLevel--;      

    }  

}
}
 if(mysql_num_rows($result)!=0)
 {

	if($_SESSION['userLoginType']=='5' || $_SESSION['rptSal']!=''){
	
	
	if(isset($_SESSION['userLoginType'])){ $salesmanID=$_SESSION['salesmanId']; }
	if(isset($_SESSION['rptSal'])){ $salesmanID=$_SESSION['rptSal']; }
	//echo $salesmanID;
 $qrySetResult="select rpt_user_id
  from table_salesman_hierarchy_relationship 
  WHERE account_id=".$_SESSION['accountId']." AND salesman_id='".$salesmanID."'";
 $resultSetquery=mysql_query($qrySetResult);
 	$row = mysql_fetch_assoc($resultSetquery);
	$curLevel=$row['rpt_user_id']; 
 if($row['rpt_user_id']=='')
 	{
		$curLevel=0;
	}
 }
 else {
 	$curLevel=0;
 }
 createTree($salesmanHirearchySet, $curLevel); 

}

	header("Content-Disposition: attachment; filename=\"Consolidated Attendance Report.xls\"");		
	exit;
}


/********************************** Export Distributor Target Vs Achievement Gaurav (08 June 2015)***************************/

if(isset($_GET['export_target_achivement_list'])){		
	
	if($_SESSION['targetType']!='' && $_SESSION['targetType']==1){
	$data="Distributor Name\t Distributor Code\t Target Type\t Item Code\t Brand\t Offer\t Item Erp\t Case Size\t Target(In No. Of Cases)\t Achievement(In No. Of Cases)\t Month\t Year\n";
}

else if($_SESSION['targetType']!='' && $_SESSION['targetType']==2){
	$data="Distributor Name\t Distributor Code\t Target Type\t Item Code\t Brand\t Offer\t Item Erp\t Target(In No. Of Cases)\t Achievement(In Quantity)\t Month\t Year\n";
	
$groupCond="GROUP BY dt.distributor_id,dti.item_id";
}
	
	if($_SESSION['targetType']!=''){$target_type=$_SESSION['targetType'];} else { $target_type="1";}

$targetMonth=$_SESSION['dismonth'];
$targetYear=$_SESSION['disCyear'];

$itemId='null';
$caseId='null';

if($_SESSION['distributorIDSale']!=''){	$disCond="and dt.distributor_id='".$_SESSION['distributorIDSale']."'";}
if($_SESSION['itemID']!=''){	$itemCond="and dti.item_id='".$_SESSION['itemID']."'";}
//echo $disCond;
//exit;
$getDistributorDetails=$_objAdmin->_getSelectList('table_distributors_target AS dt 
LEFT JOIN table_distributors_target_item  AS dti ON dt.distributor_target_id = dti.distributor_target_id 
LEFT JOIN table_distributors as d on d.distributor_id=dt.distributor_id
',"dti.no_of_cases,dti.total_quantity,dti.item_id,dti.case_id,d.distributor_id,d.distributor_name,distributor_code,dt.target_type,dt.distributor_target_id",''," dt.target_month='".$targetMonth."' and dt.target_year='".$targetYear."' and dt.target_type='".$target_type."' $disCond $itemCond $groupCond",'');	
if(sizeof($getDistributorDetails)>0){
foreach($getDistributorDetails as $disKey=>$disValue) { 
	$getBrandList=$_objAdmin->_getSelectList('table_item as i left join table_brands as b on b.brand_id=i.item_id left join table_offer as o on o.offer_id=i.offer_id',"o.offer_name,b.brand_name,i.item_erp_code,i.item_name,i.item_code",''," i.item_id='".$disValue->item_id."'",'');
	
	$disTargetType =($disValue->target_type==1)?" Primary":" Secondary"; 
	
	$getCaseList=$_objAdmin->_getSelectList2('table_item_case_relationship as c left join table_cases as ca on c.case_id=ca.case_id',"*",''," c.item_id='".$disValue->item_id."' and ca.case_id='".$disValue->case_id."'",'');	
	
	$getAchivementsByItemCase=$_objAdmin->getDistributorsAchivements($disValue->distributor_id,$targetMonth,$targetYear,$target_type,$disValue->item_id,$disValue->case_id);
	
	
	if($_SESSION['targetType']!='' && $_SESSION['targetType']==1){
	$data.="".$disValue->distributor_name."\t".$disValue->distributor_code."\t".$disTargetType."\t".$getBrandList[0]->item_code."\t".$getBrandList[0]->brand_name."\t".$getBrandList[0]->offer_name."\t".$getBrandList[0]->item_erp_code."\t".$getCaseList[0]->case_size."\t".$disValue->no_of_cases."\t".$getAchivementsByItemCase."\t".$targetMonth."\t".$targetYear."\n";
}

else if($_SESSION['targetType']!='' && $_SESSION['targetType']==2){
	$data.="".$disValue->distributor_name."\t".$disValue->distributor_code."\t".$disTargetType."\t".$getBrandList[0]->item_code."\t".$getBrandList[0]->brand_name."\t".$getBrandList[0]->offer_name."\t".$getBrandList[0]->item_erp_code."\t".$disValue->total_quantity."\t".$getAchivementsByItemCase."\t".$targetMonth."\t".$targetYear."\n";
}
	
	
	
}
}
	header("Content-type: application/octet-stream");
	header("Content-Disposition: attachment; filename=\"Target Vs Acievement.xls\"");		
	echo $data;
	exit;
	}
/********************************** Export Distributor Target Vs Achievement Gaurav (08 June 2015)***************************/


/********************************** Export Distributor Target Vs Achievement Gaurav (08 June 2015)***************************/

if(isset($_GET['export_achivement_list'])){		
	
	if($_SESSION['targetType']!='' && $_SESSION['targetType']==1){
	$data="Distributor Name\t Distributor Code\t Item Code\t Brand\t Offer\t Item Erp\t Case Size\t Achievement(In No. Of Cases)\n";
}

else if($_SESSION['targetType']!='' && $_SESSION['targetType']==2){
	$data="Distributor Name\t Distributor Code\t Item Code\t Brand\t Offer\t Item Erp\t Achievement(In quantity)\n";
	
$groupCond="GROUP BY od.distributor_id,dti.item_id";
}
	
	if($_SESSION['targetType']!=''){$target_type=$_SESSION['targetType'];} else { $target_type="1";}

$itemId='null';
$caseId='null';


//echo $disCond;
//exit;
if($_SESSION['FromOrderList']!='') { $frmNew = $_SESSION['FromOrderList']; } else { $frmNew = date('Y-m-d'); }
if($_SESSION['ToOrderList']!='') { $toNew = $_SESSION['ToOrderList']; } else { $toNew = date('Y-m-d'); }

if($target_type==1){
if($_SESSION['distributorIDSale']!=''){	$disCond="and dt.distributor_id='".$_SESSION['distributorIDSale']."'";}
if($_SESSION['itemID']!=''){	$itemCond="and dt.item_id='".$_SESSION['itemID']."'";}
$getDistributorDetails=$_objAdmin->_getSelectList('table_item_dis_stk_inprocess AS dt LEFT JOIN table_distributors as d on d.distributor_id=dt.distributor_id',"dt.acpt_stock_value,dt.item_id,dt.attribute_value_id,dt.item_id,dt.dis_stk_inpro_id,d.distributor_id,d.distributor_name,d.distributor_code",'',"  DATE(dt.last_update_datetime) >= '".date('Y-m-d', strtotime(mysql_escape_string($frmNew)))."' and DATE(dt.last_update_datetime) <= '".date('Y-m-d', strtotime(mysql_escape_string($toNew)))."' $disCond $itemCond and dt.status='A'",'');	}

else {
if($_SESSION['distributorIDSale']!=''){	$disCond="and o.distributor_id='".$_SESSION['distributorIDSale']."'";}
if($_SESSION['itemID']!=''){	$itemCond="and od.item_id='".$_SESSION['itemID']."'";}
	$getDistributorDetails=$_objAdmin->_getSelectList('table_order as o left join table_order_detail as od on o.order_id=od.order_id LEFT JOIN table_distributors as d on d.distributor_id=o.distributor_id',"acc_quantity,d.distributor_id,d.distributor_name,d.distributor_code,od.item_id",'',"  DATE(o.date_of_order) >= '".date('Y-m-d', strtotime(mysql_escape_string($frmNew)))."' and DATE(o.date_of_order) <= '".date('Y-m-d', strtotime(mysql_escape_string($toNew)))."' $disCond $itemCond GROUP BY o.distributor_id,od.item_id",'');
	
	}

//echo "<pre>";
//print_r($getDistributorDetails);
//exit;
if(sizeof($getDistributorDetails)>0){
foreach($getDistributorDetails as $disKey=>$disValue) { 
	$getBrandList=$_objAdmin->_getSelectList('table_item as i left join table_brands as b on b.brand_id=i.item_id left join table_offer as o on o.offer_id=i.offer_id',"o.offer_name,b.brand_name,i.item_erp_code,i.item_name,i.item_code",''," i.item_id='".$disValue->item_id."'",'');
	
	$disTargetType =($disValue->target_type==1)?" Primary":" Secondary"; 
	
	$getCaseList=$_objAdmin->_getSelectList2('table_item_case_relationship as c left join table_cases as ca on c.case_id=ca.case_id',"*",''," c.item_id='".$disValue->item_id."' and ca.case_id='".$disValue->attribute_value_id."'",'');
	
	if($target_type==2){ $caseId='null';} else {$caseId=$disValue->attribute_value_id;}	
	
	$getAchivementsByItemCase=$_objAdmin->getDistributorsAchivementsByDate($disValue->distributor_id,$frmNew,$toNew,$target_type,$disValue->item_id,$caseId);
	
	
	if($_SESSION['targetType']!='' && $_SESSION['targetType']==1){
	$data.="".$disValue->distributor_name."\t".$disValue->distributor_code."\t".$getBrandList[0]->item_code."\t".$getBrandList[0]->brand_name."\t".$getBrandList[0]->offer_name."\t".$getBrandList[0]->item_erp_code."\t".$getCaseList[0]->case_size."\t".$getAchivementsByItemCase."\n";
}

else if($_SESSION['targetType']!='' && $_SESSION['targetType']==2){
	$data.="".$disValue->distributor_name."\t".$disValue->distributor_code."\t".$getBrandList[0]->item_code."\t".$getBrandList[0]->brand_name."\t".$getBrandList[0]->offer_name."\t".$getBrandList[0]->item_erp_code."\t".$getAchivementsByItemCase."\n";
}
	
	
	
}
}
	header("Content-type: application/octet-stream");
	header("Content-Disposition: attachment; filename=\"Acievement.xls\"");		
	echo $data;
	exit;
	}
/********************************** Export Distributor Target Vs Achievement Gaurav (08 June 2015)***************************/




/********************************** Export Order Vs Supply Gaurav (29 Sep 2015)***************************/

if(isset($_GET['export_order_vs_supply'])){		
	
	$data="Salesman\t Distributor\t retailer\t Item \t Order Number\t Order Quantity\t Order Value\t Dispatched Quantity\t Dispatched Value\n";
	
	
	
if(isset($_SESSION['OrderDisId']) && $_SESSION['OrderDisId']!="A" ) 
	{
		$disList=" and o.distributor_id=".$_SESSION['OrderDisId']." ";
	}
	else{
	
	$disList="";
	
	}
	

if(isset($_SESSION['retDisId']) && $_SESSION['retDisId']!="A") 
	{
		$retList=" and o.retailer_id=".$_SESSION['retDisId']."";
	}
	else{
		
		$retList="";
	
	}


if(isset($_SESSION['SalDisList']) && $_SESSION['SalDisList']!="") 
	{
		$salList=" and o.salesman_id=".$_SESSION['SalDisList']."";
	}
	else{
	$salList="";
	
	}



if(isset($_SESSION['itemDisId']) && $_SESSION['itemDisId']!="A") 
	{
		$itemList=" and od.item_id=".$_SESSION['itemDisId']."";
	}
	else{
		$itemList="";
	
	}




$getSalList=$_objAdmin->_getSelectList('table_salesman as sal left join table_order as o on sal.salesman_id=o.salesman_id','DISTINCT(sal.salesman_id),sal.salesman_name',''," $salList $disList $retList  and sal.status!='D' and o.order_status='D' and o.date_of_order >= '".date('Y-m-d', strtotime(mysql_escape_string($_SESSION['FromDisList'])))."' and o.date_of_order <= '".date('Y-m-d', strtotime(mysql_escape_string($_SESSION['ToDisList'])))."'  ORDER BY sal.salesman_name");

if(sizeof($getSalList)>0){

foreach($getSalList as $salKey=>$salValue)
	{
	
	$salesman_id=$salValue->salesman_id;//echo "-";
    $salesman_name=$salValue->salesman_name; //echo "<br>";
	
	$getSalesmanWiseOrder=$_objAdmin->_getSelectList('table_order as o','o.order_id',''," o.salesman_id='".$salesman_id."' $disList $retList and o.date_of_order >= '".date('Y-m-d', strtotime(mysql_escape_string($_SESSION['FromDisList'])))."' and o.date_of_order <= '".date('Y-m-d', strtotime(mysql_escape_string($_SESSION['ToDisList'])))."' and o.order_status='D'");   
	
	
	foreach($getSalesmanWiseOrder as $orderKey=>$orderValue)
		{
		
			$getSalesmanWiseOrderDetail=$_objAdmin->_getSelectList('table_order as o left join table_order_detail as od on od.order_id=o.order_id left join table_item as i on i.item_id=od.item_id left join table_retailer as r on r.retailer_id=o.retailer_id left join table_distributors as d on d.distributor_id=o.distributor_id ','i.item_name,od.quantity,od.acc_quantity,od.total,od.acc_total,r.retailer_name,d.distributor_name,o.order_id',''," $itemList and o.order_id='".$orderValue->order_id."' "); 
		
			if(sizeof($getSalesmanWiseOrderDetail)>0){
			foreach($getSalesmanWiseOrderDetail as $orderDetailKey=>$orderDetailValue)
			{
				
				$data.="".$salesman_name."\t".$orderDetailValue->distributor_name."\t".$orderDetailValue->retailer_name."\t".$orderDetailValue->item_name."\t".$orderDetailValue->order_id."\t".$orderDetailValue->quantity."\t".$orderDetailValue->total."\t".$orderDetailValue->acc_quantity."\t".$orderDetailValue->acc_total."\n";
			}
		}
		else {
				$data.="Dispatch list not available";
			}
	}
}
}
else {
				$data.="Dispatch list not available";
	}

	header("Content-type: application/octet-stream");
	header("Content-Disposition: attachment; filename=\"OrderVsSupply.xls\"");		
	echo $data;
	exit;
	}
/********************************** Export Order Vs Supply Gaurav (29 Sep 2015)***************************/
/********************************** Export Timeline Report Gaurav (05 Oct 2015)***************************/

if(isset($_GET['export_salesman_timeline'])){
	
	$sal_time=$_GET['sal'];
	$date_time=$_GET['date'];	
	$salesman=base64_decode($sal_time);
	$aSal=$_objAdmin->_getSelectList('table_salesman AS s','*',''," status='A' and salesman_id='".$salesman."'"); 
	$name=$aSal[0]->salesman_name;
	$data="Salesman: ";
	$data.="$name\t";
	$data.="Date: ";
	$data.="$date_time\n";
	$data.="Time\t Action\n";

	if(date("Y-m-d",strtotime("-0 day")) < date('Y-m-d', strtotime($date_time))){
	$data.="Data Not Available\n";
	 } else {
	 
	 		$activity_id = array();
			$time_start = array();
			$time_end = array();
			$min = array();
			$per = array();
			$action = array();
			$lat = array();
			$lng = array();
			$ref_id = array();
			$ref_type = array();
			$retailer = array();
			$distance = array();
			$total = 0;
		
	   $query="select * from table_activity where salesman_id=".base64_decode($sal_time)." and activity_date ='".date('Y-m-d', strtotime($date_time))."' and activity_type NOT IN(1,6) order by start_time asc";
		
		$result=mysql_query($query) or die("error". mysql_error());
		while($row = mysql_fetch_array($result)) 
		{ 
			$activity_id[] = $row['activity_id'];
			$retailer[] = $row['retailer_name'];
			$action[] = $row['activity'];
			$lat[] = $row['lat'];
			$lng[] = $row['lng'];
			$ref_id[] = $row['ref_id'];
			$ref_type[] = $row['ref_type'];
			$activity_type[]=$row['activity_type'];
			$time_start[] = date('H:i',STRTOTIME($row['start_time']));
			$time_end[] = date('H:i',STRTOTIME($row['end_time']));
			$distance[] =$row['distance_travel'];
			
			$seconds = STRTOTIME($row['end_time']) - STRTOTIME($row['start_time']);
			$days    = floor($seconds / 86400);
			$hours   = floor(($seconds - ($days * 86400)) / 3600);
			$minutes = floor(($seconds - ($days * 86400) - ($hours * 3600))/60);
			$min[] = ($hours*60)+$minutes;
			
			$total = $total + (($hours*60)+$minutes);	// Sum total of minutes to get percentage
		}
		for($i = 0; $i < count($time_start); $i++)
		{
			$per[] = ($min[$i]/$total)*100; 			// convert minut in percentagr
		}
		if(count($per) > 0 ) {	
		
		for($i = 0; $i < count($per); $i++)
			{
				
				if(ceil($per[$i]) <= 0 )
				{
					$ResrTime=$time_start[$i];
					
					
				}
				else
				{
					$ResrTime=$time_start[$i]." - ".$time_end[$i];
					
				}
				
				if($retailer[$i]!='')
				{
					$Ret_loc=$_objAdmin->_getSelectList('table_retailer','retailer_location',''," retailer_name='".mysql_escape_string($retailer[$i])."' ");
					$market=$Ret_loc[0]->retailer_location;
					$Resaction= $action[$i]." (".$retailer[$i].", Market: ".$market.")";
				}
				else
				{
						$Resaction= $action[$i];
				}
				/*if($ref_id[$i]!='' && $ref_type[$i]==1){
				
				$details=
				
				}
				if($ref_id[$i]!='' && $ref_type[$i]==2){
					$details=
				}*/
	 
	 
	 		//$data.="".$ResrTime."\t".$Resaction."\t".$distance[$i]."\n";

	 		$data.='"'.$ResrTime.'"'."\t".'"'.$Resaction.'"'."\n";
	 
	 
	 }
	
	}
	}
	
	header("Content-type: application/octet-stream");
	header("Content-Disposition: attachment; filename=\"Timeline Details.xls\"");		
	echo $data;
	exit;	
	}
/********************************** Export Timeline Report Gaurav (05 Oct 2015)***************************/



/******************************************** Start Export Route List Gaurav (15 Oct 2015)******************************************************************/			
if(isset($_GET['export_dealer_master'])){		
	$data="Dsitributor Name\t Distributor Code\t State Name\t District\t Taluka Name\t Market\t Route\n";

	$salsList = $_objArrayList->SalesmanArrayList(); // For Admin and Salesman Login
	$salesman = $_objArrayList->getSalesCondition($salsList);

	$getRoute =$_objAdmin->_getSelectList('table_route AS r LEFT JOIN table_user_relationships AS ur ON ur.route_id = r.route_id LEFT JOIN table_salesman AS s ON s.salesman_id = ur.salesman_id LEFT JOIN table_route_retailer as rr on r.route_id=rr.route_id'," r.route_id,r.route_name,rr.retailer_id,rr.status as type",''," $salesman order by r.route_name");
	
	
	if(sizeof($getRoute)>0)
{	
	
	foreach($getRoute as $key=>$value)
	{			
		if($value->type=='D')
			{
				$getCustomerList=$_objAdmin->_getSelectList('table_distributors as d LEFT JOIN state as s on s.state_id=d.state LEFT JOIN city as c on c.city_id=d.city LEFT JOIN table_taluka as t on t.taluka_id=d.taluka_id LEFT JOIN table_markets as m on m.market_id=d.market_id'," d.distributor_name,d.distributor_code,s.state_name,c.city_name,t.taluka_name,m.market_name",''," d.distributor_id='".$value->retailer_id."'");
			
				foreach($getCustomerList as $key=>$custValue)
				{	
					$data.="".$custValue->distributor_name."\t".$custValue->distributor_code."\t".$custValue->state_name."\t".$custValue->city_name."\t".$custValue->taluka_name."\t".$custValue->market_name."\t".$value->route_name."\n";
				}
			}
	}
}
else 
{
	$data.="Data Not Exist";
}
		
	header("Content-type: application/octet-stream");
	header("Content-Disposition: attachment; filename=\"dealer_route_master.xls\"");		
	echo $data;
	exit;
}
	
	
if(isset($_GET['export_retailer_master'])){		
	$data="Dealer Name\t State Name\t District\t Taluka Name\t Market\t Dsitributor Name\t Distributor Code\t Route\n";

	$salsList = $_objArrayList->SalesmanArrayList(); // For Admin and Salesman Login
	$salesman = $_objArrayList->getSalesCondition($salsList);


	$getRoute =$_objAdmin->_getSelectList('table_route AS r LEFT JOIN table_user_relationships AS ur ON ur.route_id = r.route_id LEFT JOIN table_salesman AS s ON s.salesman_id = ur.salesman_id LEFT JOIN table_route_retailer as rr on r.route_id=rr.route_id'," r.route_id,r.route_name,rr.retailer_id,rr.status as type",''," $salesman order by r.route_name");
	
	
	if(sizeof($getRoute)>0)
{	
	
	foreach($getRoute as $key=>$value)
	{			
		if($value->type=='R'){
				$getCustomerList=$_objAdmin->_getSelectList('table_retailer as r LEFT JOIN state as s on s.state_id=r.state LEFT JOIN city as c on c.city_id=r.city LEFT JOIN table_taluka as t on t.taluka_id=r.taluka_id LEFT JOIN table_markets as m on m.market_id=r.market_id
				LEFT JOIN table_distributors as d on d.distributor_id=r.distributor_id'," r.retailer_name,d.distributor_name,d.distributor_code,s.state_name,c.city_name,t.taluka_name,m.market_name",''," r.retailer_id='".$value->retailer_id."'");
				
			
				foreach($getCustomerList as $key=>$custValue)
				{	
					$data.="".$custValue->retailer_name."\t".$custValue->state_name."\t".$custValue->city_name."\t".$custValue->taluka_name."\t".$custValue->market_name."\t".$custValue->distributor_name."\t".$custValue->distributor_code."\t".$value->route_name."\n";
				}
			}
	}
}
else 
{
	$data.="Data Not Exist";
}
		
	header("Content-type: application/octet-stream");
	header("Content-Disposition: attachment; filename=\"retailer_route_master.xls\"");		
	echo $data;
	exit;
	}
/******************************************** Start Export Route List Gaurav (15 Oct 2015) **************************************************/	



/******************************************** Start Export Assigned Route List (15 Oct 2015) *********************************************/

	
if(isset($_GET['export_salesman_planned_route'])){		
	$data="Salesman Name\t Designation\t Month\t Year\t Date\t Route\t Dsitributor Name\t Retailer Name\n";
	//$condi=	" 1=1";
	$route_schedule_id=$_GET['id'];
	$getRouteDate =$_objAdmin->_getSelectList('table_route_scheduled as rs LEFT JOIN table_salesman as s on rs.salesman_id=s.salesman_id LEFT JOIN table_route_schedule_details as rd on rd.route_schedule_id=rs.route_schedule_id LEFT JOIN table_salesman_hierarchy_relationship AS SH ON SH.salesman_id = s.salesman_id LEFT JOIN table_salesman_hierarchy AS H ON H.hierarchy_id = SH.hierarchy_id LEFT JOIN table_salesman_hierarchy AS H2 ON H2.hierarchy_id = SH.rpt_hierarchy_id'," s.salesman_name,s.salesman_id,rd.route_id,H.description AS des1,rs.month,rs.year,rd.route_schedule_id,rd.assign_day",''," rd.route_schedule_id='".$route_schedule_id."'");

	if(sizeof($getRouteDate)>0)
	{	
	foreach($getRouteDate as $key=>$value)
		{
			
			
			$getRoute =$_objAdmin->_getSelectList('table_route as r LEFT JOIN table_route_retailer as rr on r.route_id=rr.route_id'," r.route_id,r.route_name,rr.retailer_id,rr.status as type",''," r.route_id='".$value->route_id."' order by r.route_name");
		
		//echo "<pre>";
		//print_r($getRoute);
		foreach($getRoute as $routeKey=>$routeValue)
		{
			
			if($routeValue->type=='R'){
				$getCustomerList=$_objAdmin->_getSelectList('table_retailer as r LEFT JOIN state as s on s.state_id=r.state LEFT JOIN city as c on c.city_id=r.city LEFT JOIN table_taluka as t on t.taluka_id=r.taluka_id LEFT JOIN table_markets as m on m.market_id=r.market_id
				'," r.retailer_name,s.state_name,c.city_name,t.taluka_name,m.market_name",''," r.retailer_id='".$routeValue->retailer_id."'");
				}
			
		    if($routeValue->type=='D'){
				$getCustomerList=$_objAdmin->_getSelectList('table_distributors as d LEFT JOIN state as s on s.state_id=d.state LEFT JOIN city as c on c.city_id=d.city LEFT JOIN table_taluka as t on t.taluka_id=d.taluka_id LEFT JOIN table_markets as m on m.market_id=d.market_id'," d.distributor_name,d.distributor_code,s.state_name,c.city_name,t.taluka_name,m.market_name",''," d.distributor_id='".$routeValue->retailer_id."'");
			}
			
			
			
			//echo $routeValue->route_id."-".$routeValue->route_name."-".$routeValue->type."<br>";
			//print_r($getCustomerList);
			
			foreach($getCustomerList as $listKey=>$listValue)
			{
				$dateNew=$value->year."-".$value->month."-".$value->assign_day;
				//echo $value->salesman_name."-".$value->month."-".$value->year."-".$_objAdmin->_changeDate($dateNew)."-".$routeValue->route_name."-".$listValue->retailer_name."-".$listValue->distributor_name."<br>";
				
		$data.="".$value->salesman_name."\t".$value->des1."\t".$value->month."\t".$value->year."\t".$_objAdmin->_changeDate($dateNew)."\t".$routeValue->route_name."\t".$listValue->distributor_name."\t".$listValue->retailer_name."\n";
				
				
			}
			
			}
		}
	}
else 
{
	$data.="Data Not Exist";
}
		
	header("Content-type: application/octet-stream");
	header("Content-Disposition: attachment; filename=\"salesman_planned_route.xls\"");		
	echo $data;
	exit;
	}

/**************************** End Export Assigned Route List (15 Oct 2015) *****************************/


/**************************** Export Salesman Planned route by Nizam (29 Oct 2015)*******************/


if(isset($_GET['salesman_route_summery'])){

    
    $data="Salesman Name \t Salesman Code\t Salesman Division\t Salesman Designation\t Date \t Route Name \t Number Of Dealer \t Number Of Dealer Visited \t Number Of Distributor \t Number Of Distributor Visited \n";

	if($_GET['sal']=='All') {	
	$salsList = $_objArrayList->SalesmanArrayList(); // For Admin and Salesman Login
	$salesman = $_objArrayList->getSalesCondition($salsList); 
	} else { $salesman=' and s.salesman_id='.$_GET['sal'].' ';}

	if(isset($_SESSION['findDivision']) && $_SESSION['findDivision']!="all"){
	   $divisionCondition = " AND ( s.division_id IN(".$_SESSION['findDivision'].") )";

	}

	if(isset($_SESSION['findDesignation']) && $_SESSION['findDesignation']!="all"){
	 $designationCondition = " AND ( SR.hierarchy_id IN(".$_SESSION['findDesignation'].") )";

	}

	if($_GET['route_id']=='0'){ $routeVal='';} else{ $routeVal=" and rsd.route_id='".$_GET['route_id']."' ";}
    
    if($_GET['month']!='')
    	$monthVal=" and rs.month=".$_GET['month']."";

    if($_GET['year']!="")
    	$yearVal =" and rs.year=".$_GET['year']."";
    
    $Newdate = $_SESSION['PVsVYear'].'-'.$_SESSION['PVsVMonth'];
	$startDate = date('Y-m-d', strtotime($Newdate));
	$endDate = date('Y-m-t', strtotime($Newdate));

	


	//$condi=	" rs.status='A' and s.status='A'  ";
	//$auRec=$_objAdmin->_getSelectList2('table_message as m left join table_salesman as s on s.salesman_id=m.salesman_id',"m.*,s.salesman_name ",'',$condi);
	$auRec=$_objAdmin->_getSelectList('table_route_scheduled as rs left join table_salesman as s on rs.salesman_id=s.salesman_id left join table_route_schedule_details as rsd on rsd.route_schedule_id=rs.route_schedule_id left join table_route as ro on ro.route_id=rsd.route_id left join table_division as td on td.division_id=s.division_id LEFT JOIN table_salesman_hierarchy_relationship as SR on SR.salesman_id=s.salesman_id 
    	LEFT JOIN table_salesman_hierarchy as SH on SH.hierarchy_id=SR.hierarchy_id','rs.month,rs.year,s.salesman_name,s.salesman_id,s.salesman_code,SH.description,td.division_name,rsd.assign_day,ro.route_name,ro.route_id',''," $salesman $divisionCondition $designationCondition  $routeVal  $monthVal $yearVal ORDER BY rsd.assign_day ASC"); 

	$RResult1 = $_objAdmin->_getSelectList("table_order",'COUNT(DISTINCT retailer_id) as total_orders,salesman_id,date_of_order','',"retailer_id>0 AND date_of_order BETWEEN ('".date('Y-m-d', strtotime($startDate))."') AND ('".date('Y-m-d', strtotime($endDate))."') GROUP BY salesman_id,date_of_order " );


	$DResult1 = $_objAdmin->_getSelectList("table_order",'COUNT(DISTINCT distributor_id) as total_orders,salesman_id,date_of_order','',"distributor_id >0 AND customer_id=0 AND retailer_id=0 AND date_of_order BETWEEN ('".date('Y-m-d', strtotime($startDate))."') AND ('".date('Y-m-d', strtotime($endDate))."')  GROUP BY salesman_id,date_of_order" );

	$salData = array();

	foreach ($RResult1 as $key => $value) {

		$salData[$value->salesman_id][$value->date_of_order] = $value;
	}

	$disData = array();
	foreach ($DResult1 as $key => $value) {
		$disData[$value->salesman_id][$value->date_of_order] = $value;
	}


	if(is_array($auRec)){
		$retailer=0;
		$distributor=0;
		$visited_dealer=0;
		$visited_distributor=0;
		

		
		foreach ($auRec as $key => $value) {
			$remove = array("\n", "\r\n", "\r");
			
			$days = date('Y-m-d', strtotime($value->year.'-'.$value->month.'-'.$value->assign_day));
		    $date=$_objAdmin->_changeDate($days);

			$aRec=$_objAdmin->_getSelectList('table_route_retailer','route_id, COUNT(*) AS TOTAL, COUNT(IF(status="R",1,null)) as retailer,
    COUNT(IF(status="D",1,null)) as distributor',''," route_id='".$value->route_id."' and status!='' ");


			if( isset($salData[$value->salesman_id][$days])){

				 $visited_dealer = $salData[$value->salesman_id][$days]->total_orders; 
			}else{
				$visited_dealer =  0;
			}	

			if( isset($disData[$value->salesman_id][$days])){

				$visited_distributor = $disData[$value->salesman_id][$days]->total_orders;
			}else{
				$visited_distributor = 0;
			}	
							
							
			$data.="".$value->salesman_name."\t".str_replace($remove, ' ',$value->salesman_code)."\t".str_replace($remove, ' ',$value->division_name)."\t".str_replace($remove, ' ',$value->description)."\t" .$date."\t".str_replace($remove, ' ',$value->route_name)."\t".str_replace($remove, ' ',$aRec[0]->retailer)."\t".str_replace($remove, ' ',$visited_dealer)."\t".str_replace($remove, ' ',$aRec[0]->distributor)."\t".str_replace($remove, ' ',$visited_distributor)."\n";
			
			$retailer=$retailer+$aRec[0]->retailer;
			$distributor=$distributor+$aRec[0]->distributor;

		}
		//$data.=" ".to."\t".ff."\t".$retailer."\t".$distributor."";						
	}
	header("Content-type: application/octet-stream");
	header("Content-Disposition: attachment; filename=\"salesman_route_summery.xls\"");		
	echo $data;
	exit;
	}





/******************************************** Start Export Customer ***********************************/	
	
	
if(isset($_GET['export_customer_list'])){		
	$data="Customer Name \t Customer Code \t Customer Type \t Customer region \t Division \t Interested \t State \t District \t Taluka \t City \t Zipcode \t Address1 \t Address2 \t Phone Number1 \t Phone Number2 \t Landline Number \t Contact Person1 \t Contact Person Number1 \t Contact Person2 \t Contact Person Number2 \t Email ID1 \t Email ID2\t Aadhar Number \t Pan Number \n";
	$divisionFilters = "";
		
		$salDiv = $_objAdmin->_getSelectList('table_salesman'," division_id",''," AND salesman_id='".$_SESSION['salesmanId']."'");
		    if(sizeof($salDiv) > 0)
		   	 $divisionFilters = " AND cus.division_id =".$salDiv[0]->division_id;
	$condi=	" cus.status='A' $divisionFilters and cus.account_id='".$_SESSION['accountId']."' ORDER BY s.state_name,c.city_name,cus.customer_name";
	$auRec=$_objAdmin->_getSelectList('table_customer as cus LEFT JOIN table_customer_type AS CT ON CT.type_code = cus.customer_type  LEFT JOIN table_taluka as t on t.taluka_id=cus.taluka_id left join state as s on cus.state=s.state_id left join city as c on cus.city=c.city_id  left join table_markets as m on m.market_id=cus.market_id left join table_region as r on r.region_id=cus.region_id left join table_division AS d ON d.division_id = cus.division_id ',"cus.*,d.division_name,r.region_name,s.state_name, CT.type_name, c.city_name,m.market_name,t.taluka_name",'',$condi);
	if(is_array($auRec)){
		for($i=0;$i<count($auRec);$i++){
					
			
		$data.="".$auRec[$i]->customer_name."\t".$auRec[$i]->customer_code."\t".$auRec[$i]->type_name."\t".$auRec[$i]->region_name."\t".$auRec[$i]->division_name."\t".strtoupper($auRec[$i]->display_outlet)."\t".$auRec[$i]->state_name."\t".$auRec[$i]->city_name."\t".$auRec[$i]->taluka_name."\t".$auRec[$i]->market_name."\t".$auRec[$i]->zipcode."\t".$auRec[$i]->market_name."\t".$auRec[$i]->zipcode."\t".$auRec[$i]->customer_address."\t".$auRec[$i]->retailer_address2."\t".$auRec[$i]->retailer_phone_no."\t".$auRec[$i]->retailer_phone_no2."\t".$auRec[$i]->retailer_leadline_no."\t".$auRec[$i]->contact_person."\t".$auRec[$i]->contact_number."\t".$auRec[$i]->contact_person2."\t".$auRec[$i]->contact_number2."\t".$auRec[$i]->email_id."\t".$auRec[$i]->email_id2."\t".$auRec[$i]->aadhar_no."\t".$auRec[$i]->pan_no."\n";
		}						
	}
	header("Content-type: application/octet-stream");
	header("Content-Disposition: attachment; filename=\"customer_list.xls\"");		
	echo $data;
	exit;
	}



/***************************************************************************
	* DESC : Export serial number scan by customer report
	* Author : Abhishek
	* Created : 2016-02-24
	*
	**/

if(isset($_GET['export_serial_no_scan'])){		
	$data="ID \t Shakti-Partner Name \t Shakti-Partner Code \t Employee Name \t Employee Code \t Customer Name \t Customer Mobile Number \t Serial No \t Date \t Time \t Status \n";
	$auRec=$_objAdmin->_getSelectList2('table_serial_number_scan as serial LEFT JOIN table_customer as tc ON serial.customer_id=tc.customer_id LEFT JOIN table_retailer as tr ON serial.retailer_id=tr.retailer_id LEFT JOIN table_salesman as sal ON serial.salesman_id=sal.salesman_id',"serial.serial_id,tc.customer_name AS shakti_partner_name,tc.customer_code,sal.salesman_name as employee_name,sal.salesman_code as employee_code ,serial.customer_name,serial.customer_mobile_number,serial.serial_number,serial.date,serial.time,serial.serial_number_status",'','');
	if(is_array($auRec)){
		for($i=0;$i<count($auRec);$i++){
					
			
		$data.="".$auRec[$i]->serial_id."\t".$auRec[$i]->shakti_partner_name."\t".$auRec[$i]->customer_code."\t".$auRec[$i]->employee_name."\t".$auRec[$i]->employee_code."\t".$auRec[$i]->customer_name."\t".$auRec[$i]->customer_mobile_number."\t".$auRec[$i]->serial_number."\t".$auRec[$i]->date."\t".$auRec[$i]->time."\t".$auRec[$i]->serial_number_status."\n";
		}
	}
	header("Content-type: application/octet-stream");
	header("Content-Disposition: attachment; filename=\"Scan Serial no List.xls\"");
	echo $data;
	exit;
}


/******************************************************************/
/******************************************** Start Export Customer Geo Tagging Report ******************************************************************/

if(isset($_GET['export_geo_tagging_list'])){ 		
	$data="Customer Name \t Customer Code \t Customer Address \t Customer Class \t Customer Status\t Customer Type \t Customer Mobile No. \t Latitude \t Longitude \t Division \t State \t State Code \t District \t District Code \t Tehsil \t Taluka Code\t Pin Code \t Interest\n";

	$divisionRec=$_objAdmin->_getSelectList2('table_division',"division_name as division,division_id",'',$condition1);
     $stateRec=$_objAdmin->_getSelectList2('state',"state_name,state_id",'','status="A"');

		// Conditions 

		$intrestedCondition = "";
		$classCondition = "";
		$retailerActiveInactiveCond="";
		$distributorActiveInactiveCond="";
		$customerActiveInactiveCond="";
		$divisionCondition = "";
		$stateCondition = "";
		$distributorCondition = "";
		$talukaCondition = "";


		if(isset($_SESSION['findIntr']) && $_SESSION['findIntr']!="all"){
		    $intrestedCondition =" AND display_outlet='".$_SESSION['findIntr']."'";
		}
		if(isset($_SESSION['customerClassRec']) && $_SESSION['customerClassRec']!='all'){
		    $classCondition =" AND rr.relationship_id='".$_SESSION['customerClassRec']."'";
		} 
		if(isset($_SESSION['activeCust']) && $_SESSION['activeCust']!='all'){
		    $retailerActiveInactiveCond =" AND R.status='".$_SESSION['activeCust']."'";
		    $distributorActiveInactiveCond=" AND D.status='".$_SESSION['activeCust']."'";
		    $customerActiveInactiveCond =" AND C.status='".$_SESSION['activeCust']."'";
		}
		if($_SESSION['userLoginType'] == 5 && !isset($_SESSION['findDivision'])){
		    $divisionIdString = implode(",", $divisionList);
		    $divisionCondition = " AND dV.division_id IN ($divisionIdString) ";
		}
		if(isset($_SESSION['findDivision']) && $_SESSION['findDivision']!="all"){
		   $divisionCondition = " AND dV.division_id IN(".$_SESSION['findDivision'].")";
		}
		if(isset($_SESSION['stateCust']) && $_SESSION['stateCust']!="all"){
		    $stateCondition = " AND ST.state_id IN(".$_SESSION['stateCust'].")";
		}
		if(isset($_SESSION['districtCust']) && $_SESSION['districtCust']!="all"){
		    $cityCondition = " AND CT.city_id IN(".$_SESSION['districtCust'].")";
		}
		if(isset($_SESSION['tehsilCust']) && $_SESSION['tehsilCust']!="all"){
		    $talukaCondition = " AND TL.taluka_id IN(".$_SESSION['tehsilCust'].")";
		}

		if( !isset($_SESSION['findCust']) || $_SESSION['findCust'] == 'all' || $_SESSION['findCust'] == 'R' ) {

		$retailerData = $_objAdmin->_getSelectList("table_retailer AS R
		    LEFT JOIN viewRetDisCusSurvey AS S1 ON S1.retailer_id = R.retailer_id 
		    LEFT JOIN table_relationship as rr on rr.relationship_id=R.relationship_id 
		    left join table_division AS dV ON dV.division_id = R.division_id 
		    left join state as ST on ST.state_id=R.state 
		    left join city as CT on CT.city_id=R.city 
		    left join table_taluka as TL on TL.taluka_id=R.taluka_id",
		    'R.retailer_id AS id,R.retailer_code as customer_code, R.retailer_name AS name, R.retailer_address AS address, R.display_outlet AS outlet, R.lat_lng_capcure_accuracy AS accuracy, R.division_id, R.lat, R.lng, S1.lat AS survey_lat, S1.lng AS survey_lng, "Retailer" AS customer_type,R.retailer_phone_no AS customer_mobile_number,dV.division_name,rr.relationship_code,ST.state_name,ST.state_code,CT.city_name,CT.city_code,TL.taluka_name,TL.taluka_code,R.status,R.zipcode AS pincode ',''," R.status!='D' AND (S1.lat !='') $intrestedCondition $classCondition $retailerActiveInactiveCond $divisionCondition $stateCondition $cityCondition $talukaCondition " );

		} else {
		    $retailerData  = array();
		}


		if( !isset($_SESSION['findCust']) || $_SESSION['findCust'] == 'all' || $_SESSION['findCust'] == 'D' ) {

		$distributorData = $_objAdmin->_getSelectList("table_distributors AS D
		    LEFT JOIN viewRetDisCusSurvey AS S1 ON S1.distributor_id = D.distributor_id 
		     LEFT JOIN table_relationship as rr on rr.relationship_id=D.relationship_id 
		    left join table_division AS dV ON dV.division_id = D.division_id 
		     left join state as ST on ST.state_id=D.state 
		    left join city as CT on CT.city_id=D.city 
		    left join table_taluka as TL on TL.taluka_id=D.taluka_id",
		    'D.distributor_id AS id,D.distributor_code as customer_code, D.distributor_name AS name, D.distributor_address AS address, D.display_outlet AS outlet,D.lat_lng_capcure_accuracy AS accuracy, D.division_id, D.lat, D.lng, S1.lat AS survey_lat, S1.lng AS survey_lng, "Distributor" AS customer_type,D.distributor_phone_no AS customer_mobile_number,dV.division_name,rr.relationship_code,ST.state_name,ST.state_code,CT.city_name,CT.city_code,TL.taluka_name,TL.taluka_code,D.status,D.zipcode AS pincode ',''," D.status!='D' AND (S1.lat !='') $intrestedCondition $classCondition $distributorActiveInactiveCond  $divisionCondition  $stateCondition $cityCondition $talukaCondition");

		} else {
		    $distributorData  = array();
		}

		if( !isset($_SESSION['findCust']) || $_SESSION['findCust'] == 'all' || !in_array($_SESSION['findCust'], array('R','D')) ) {
		$typeCondition = "";
		//echo $_SESSION['findCust'];
		if( isset($_SESSION['findCust']) AND !in_array($_SESSION['findCust'], array('R','D', 'all'))) {
		  $typeCondition = " AND C.customer_type='".$_SESSION['findCust']."'";  
		}


		$customerData = $_objAdmin->_getSelectList("table_customer AS C
		    LEFT JOIN viewRetDisCusSurvey AS S1 ON S1.customer_id = C.customer_id 
		    left join table_division AS dV ON dV.division_id = C.division_id 
		     left join state as ST on ST.state_id=C.state 
		    left join city as CT on CT.city_id=C.city 
		    left join table_taluka as TL on TL.taluka_id=C.taluka_id",
		    'C.customer_id AS id,C.customer_code, C.customer_name AS name, C.customer_address AS address, C.display_outlet AS outlet, C.lat_lng_capcure_accuracy AS accuracy, C.division_id, C.lat, C.lng, S1.lat AS survey_lat, S1.lng AS survey_lng, C.customer_type,C.customer_phone_no AS customer_mobile_number,dV.division_name,"-" as relationship_code,ST.state_name,ST.state_code,CT.city_name,CT.city_code,TL.taluka_name,TL.taluka_code,C.status,C.zipcode AS pincode ','',"  C.status!='D' AND (S1.lat !='' || C.lat !='') $intrestedCondition  $customerActiveInactiveCond  $divisionCondition  $stateCondition $cityCondition $talukaCondition $typeCondition ");
		} else {
		    $customerData  = array();
		}

		if(sizeof($retailerData)>0 && sizeof($distributorData)>0 && sizeof($customerData)>0) {
		    $custData = array_merge($retailerData, $distributorData, $customerData);
		} else if(sizeof($retailerData)> 0 && sizeof($distributorData)>0 && sizeof($customerData)==0) {
		    $custData = array_merge($retailerData, $distributorData);
		} else if(sizeof($retailerData)> 0 && sizeof($distributorData)==0 && sizeof($customerData)>0) {
		    $custData = array_merge($retailerData, $customerData);
		} else if(sizeof($retailerData)== 0 && sizeof($distributorData)>0 && sizeof($customerData)>0) {
		    $custData = array_merge($distributorData, $customerData);
		} else if(sizeof($retailerData)> 0 && sizeof($distributorData)==0 && sizeof($customerData)==0) {
		    $custData = $retailerData;
		} else if(sizeof($retailerData)== 0 && sizeof($distributorData)>0 && sizeof($customerData)==0) {
		    $custData = $distributorData;
		} else if(sizeof($retailerData)== 0 && sizeof($distributorData)==0 && sizeof($customerData)>0) {
		    $custData = $customerData;
		}


	if(sizeof($custData)>0){
		foreach ($custData as $key => $value) {

			       if($value->status=="I"){ $status="Inactive";}else{ $status="Active";}
                	if($value->customer_type=="C"){ 
                		$customer_type="Shakti-Partner";
            		}elseif ($value->customer_type=="P") {
            			$customer_type="Pump Installer";
            		}elseif ($value->customer_type=="S"){
            			$customer_type="Solar Pumps Installer";
            		}else{
            			$customer_type=$value->customer_type;
            		}

            $remove = array(",", "-", "/","_",":",";","`","!","?");

		$data.="".$value->name."\t".$value->customer_code."\t".str_replace($remove,' ',$value->address)."\t".$value->relationship_code."\t".$status."\t".$customer_type."\t".$value->customer_mobile_number."\t".$value->survey_lat."\t".$value->survey_lng."\t".$value->division_name."\t".$value->state_name."\t".$value->state_code."\t".$value->city_name."\t".$value->city_code."\t".str_replace($remove,' ',$value->taluka_name)."\t".$value->taluka_code."\t".$value->pincode."\t".$value->outlet."\n";
		}						
	}
	header("Content-type: application/octet-stream");
	header("Content-Disposition: attachment; filename=\"customer geo tagging report.xls\"");		
	echo $data;
	exit;
	}
	
/******************************************** End Start Export Customer Geo Tagging Report******************************************************************/



/***************************************************************************
	* DESC : Export Total Assigned Retailers
	* Author : Sohan
	* Created : 2016-09-20
	*
	**/

if(isset($_GET['export_assigned_retailer'])){		
	$data="Salesman Name\t Salesman Designation \t Retailer Name \t Class \t State \t District \t City \t Taluka \t Address \t Route Date \t Route Name \n";
	//From date  
	if(isset($_SESSION['FROM_DATE']) && $_SESSION['FROM_DATE'] != ''){  
		 $start_date = $_SESSION['FROM_DATE']; 

	}else
	{
		$start_date = date('Y-m-01');
	}

	//To date
	if(isset($_SESSION['TO_DATE']) && $_SESSION['TO_DATE'] != ''){
		$end_date = $_SESSION['TO_DATE'];			
	}else
	{
		$end_date = date('Y-m-d');
	}


	// Added this condition only when user logged in except admin
	if(isset($_SESSION['userLoginType']) && $_SESSION['userLoginType'] > 1) {

		$_objArrayList = new ArrayList();
		$salesmanList = $_objArrayList->SalesmanArrayList();

	}


	if(sizeof($salesmanList)>0) {  
			$salList = implode(",", $salesmanList); 
			$salesmanCondition = ' AND RET.salesman_id IN ('.$salList.') AND S.status = "A"';


		} 

	 $yearFrom = date('Y',strtotime($start_date));
	 $yearTo = date('Y',strtotime($end_date));
	 $monthFrom = date('n',strtotime($start_date));
	 $monthTo = date('n',strtotime($end_date));
	 $dayFrom = date('j',strtotime($start_date));
	 $dayTo = date('j',strtotime($end_date));
	 $allMonths=0;
	 $allMonthsFromYear=0;
	 $allMonthsToYear=0;
	 if($yearFrom == $yearTo){
	 		 	
 		while($monthFrom <= $monthTo){
 			$monthsIn[]=$monthFrom;
 		  $monthFrom++; 		  	 		
 	     }
	 	$allMonths=implode(',', $monthsIn);
	 }else if($yearFrom < $yearTo){
	 		 	
 		while($monthFrom <= 12){
 			$monthsInFromYear[]=$monthFrom;
 		  $monthFrom++; 		  		 		
 	     }

 	     $mn=1;
 	     while($mn <= $monthTo){
 			$monthsInToYear[]=$mn;
 		  $mn++; 		  		 		
 	     }

	 	$allMonthsFromYear=implode(',', $monthsInFromYear);
	 	$allMonthsToYear=implode(',', $monthsInToYear);
	 }else{
	 	$allMonths=0;
	 	$allMonthsFromYear=0;
	 	$allMonthsToYear=0;
	 }
       $allMon = $allMonthsFromYear.','.$allMonthsToYear;


    $auRec =$_objAdmin->_getSelectList('`table_route_scheduled` AS RS 
			LEFT JOIN table_route_schedule_details AS RSD ON RSD.`route_schedule_id` = RS.`route_schedule_id`

			LEFT JOIN table_route AS R ON R.route_id = RSD.route_id

			LEFT JOIN table_route_retailer AS RR ON RR.route_id = RSD.route_id AND RR.status IN ("R")
			
			INNER JOIN table_retailer AS RET ON RET.retailer_id = RR.retailer_id AND RR.status = "R" AND DATE_FORMAT(RET.start_date, "%Y-%m-%d") < DATE_FORMAT(CONCAT(RS.year,"-",RS.month,"-",RSD.assign_day),"%Y-%m-%d") AND RET.status = "A" 

			

			LEFT JOIN table_salesman as S on RS.salesman_id=S.salesman_id 
	    	LEFT JOIN table_salesman_hierarchy_relationship as SR on SR.salesman_id=S.salesman_id 
	    	LEFT JOIN table_salesman_hierarchy as SH on SH.hierarchy_id=SR.hierarchy_id		
			LEFT JOIN table_relationship AS RELD ON RELD.relationship_id = RET.relationship_id 
			LEFT JOIN table_division AS DI ON DI.division_id = RET.division_id 
			LEFT JOIN state AS ST ON ST.state_id = RET.state 
			LEFT JOIN city AS CT ON CT.city_id = RET.city 
			LEFT JOIN table_taluka AS TL ON TL.taluka_id = RET.taluka_id',


			"S.salesman_name,SH.description,RET.retailer_id,RET.retailer_name,RET.display_outlet,RET.retailer_address,RET.retailer_location as city_name,RELD.relationship_desc AS retailer_class,DI.division_name,ST.state_name,CT.city_name as district,TL.taluka_name,R.route_name,RS.created_date as route_date",'',
			"CASE WHEN '".$yearFrom."' = '".$yearTo."' THEN RS.month IN($allMonths) AND RS.year BETWEEN '".$yearFrom."' AND '".$yearTo."' AND RSD.assign_day BETWEEN '".$dayFrom."' AND '".$dayTo."' 
			ELSE 
				CASE 
			        WHEN '".$yearFrom."' < '".$yearTo."' THEN RS.month IN($allMon) AND RS.year BETWEEN '".$yearFrom."' AND '".$yearTo."' AND RSD.assign_day BETWEEN '".$dayFrom."' AND '".$dayTo."' 					
			    END 
			    
			END $salesmanCondition  ORDER BY RSD.assign_day ASC"); //GROUP BY RS.created_date

    	// echo "<pre>";
    	// print_r($anaResult);exit;
        $err = '';

		if(count($auRec) < 1){
			 $err='Data does not exist!';
		} 
	if(is_array($auRec)){
		for($i=0;$i<count($auRec);$i++){
					
			
		$data.="".$auRec[$i]->salesman_name."\t".$auRec[$i]->description."\t".$auRec[$i]->retailer_name."\t".$auRec[$i]->retailer_class."\t".$auRec[$i]->state_name."\t".$auRec[$i]->district."\t".$auRec[$i]->city_name."\t".$auRec[$i]->taluka_name."\t".$auRec[$i]->retailer_address."\t".$auRec[$i]->route_date."\t".$auRec[$i]->route_name."\n";
		}
	}
	header("Content-type: application/octet-stream");
	header("Content-Disposition: attachment; filename=\"Total_Assigned_Retailers.xls\"");
	echo $data;
	exit;
}

/******************************************************************/

/******************************************************************/
/******************************************** Start Export Customer Geo Tagging Report ******************************************************************/

if(isset($_GET['export_sal_checkin_checkout'])){ 		
	$data="Salesman Name \t Customer Name \t Customer Status\t Customer Type \t Latitude \t Longitude \t Division \t State \t City \t Login Time\t Logout Time \t Date\n";

	/*$divisionRec=$_objAdmin->_getSelectList2('table_division',"division_name as division,division_id",'',$condition1);
     $stateRec=$_objAdmin->_getSelectList2('state',"state_name,state_id",'','status="A"');*/

		// Conditions 



		$intrestedCondition = "";
		$classCondition = "";
		$retailerActiveInactiveCond="";
		$distributorActiveInactiveCond="";
		$customerActiveInactiveCond="";
		$divisionCondition = "";
		$stateCondition = "";
		$distributorCondition = "";
		$talukaCondition = "";
		$salCondition = "";
		$dateCondition = "";


		if(isset($_SESSION['findIntr']) && $_SESSION['findIntr']!="all"){
		    $intrestedCondition =" AND display_outlet='".$_SESSION['findIntr']."'";
		}

		if(isset($_SESSION['customerClassRec']) && $_SESSION['customerClassRec']!='all'){
		    $classCondition =" AND rr.relationship_id='".$_SESSION['customerClassRec']."'";
		} 

		if(isset($_SESSION['activeCust']) && $_SESSION['activeCust']!='all'){
		    $retailerActiveInactiveCond =" AND R.status='".$_SESSION['activeCust']."'";
		    $distributorActiveInactiveCond=" AND D.status='".$_SESSION['activeCust']."'";
		    $customerActiveInactiveCond =" AND C.status='".$_SESSION['activeCust']."'";
		}


		if($_SESSION['userLoginType'] == 5 && !isset($_SESSION['findDivision'])){
		    $divisionIdString = implode(",", $divisionList);
		    $divisionCondition = " AND dV.division_id IN ($divisionIdString) ";
		} 

		if(isset($_SESSION['findDivision']) && $_SESSION['findDivision']!="all"){
		   $divisionCondition = " AND dV.division_id IN(".$_SESSION['findDivision'].")";
		}


		if(isset($_SESSION['stateCust']) && $_SESSION['stateCust']!="all"){
		    $stateCondition = " AND ST.state_id IN(".$_SESSION['stateCust'].")";
		}

		if(isset($_SESSION['districtCust']) && $_SESSION['districtCust']!="all"){
		    $cityCondition = " AND CT.city_id IN(".$_SESSION['districtCust'].")";
		}

		if(isset($_SESSION['tehsilCust']) && $_SESSION['tehsilCust']!="all"){
		    $talukaCondition = " AND TL.taluka_id IN(".$_SESSION['tehsilCust'].")";
		}

		if(isset($_SESSION['salesName']) && $_SESSION['salesName']!="all" && $_SESSION['salesName']!=""){
		    $salCondition = " AND A.salesman_id='".$_SESSION['salesName']."'";
		}

		if(isset($_SESSION['fromDate']) && isset($_SESSION['toDate'])){
		    $dateCondition = " AND A.activity_date BETWEEN '".date('Y-m-d',strtotime($_SESSION['fromDate']))."' AND '".date('Y-m-d',strtotime($_SESSION['toDate']))."'";
		}		

		if( !isset($_SESSION['findCust']) || $_SESSION['findCust'] == 'all' || $_SESSION['findCust'] == 'R' ) { 

		$retailerData = $_objAdmin->_getSelectList2("view_salesmanCheckIn AS A
    LEFT JOIN table_retailer AS R ON A.ref_id = R.retailer_id 
    LEFT JOIN view_salesmanCheckIn AS A1 ON A.activity_date=A1.activity_date AND A1.activity_type = 29 AND A1.ref_id = A.ref_id AND A1.ref_type = A.ref_type
    LEFT JOIN table_salesman AS S ON A.salesman_id = S.salesman_id 
    LEFT JOIN table_relationship as rr on rr.relationship_id=R.relationship_id 
    left join table_division AS dV ON dV.division_id = R.division_id 
    left join state as ST on ST.state_id=R.state 
    left join city as CT on CT.city_id=R.city 
    left join table_taluka as TL on TL.taluka_id=R.taluka_id",
    'S.salesman_name,A.activity_date,A.activity_type,A.start_time,A.ref_id,A.ref_type,A.salesman_id,R.retailer_id AS id, R.retailer_name AS name, R.retailer_address AS address, R.display_outlet AS outlet, R.lat_lng_capcure_accuracy AS accuracy, R.division_id, R.lat, R.lng,   A.lat AS survey_lat, A.lng AS survey_lng, "Retailer" AS customer_type,dV.division_name,rr.relationship_code,ST.state_name,CT.city_name,TL.taluka_name,R.status,A1.start_time as "checkout_time" ',''," R.status!='D' AND (A.lat !='') AND A.activity_type IN(28,29) AND A.ref_type = 1 $intrestedCondition $classCondition $retailerActiveInactiveCond $divisionCondition $stateCondition $cityCondition $talukaCondition $salCondition $dateCondition GROUP BY A.salesman_id,A.activity_date,A.ref_id ORDER BY S.salesman_name" );

		/*echo "<pre>";
    print_r($retailerData);
    exit;*/

		} else {
		    $retailerData  = array();
		}


		if( !isset($_SESSION['findCust']) || $_SESSION['findCust'] == 'all' || $_SESSION['findCust'] == 'D' ) {

		$distributorData = $_objAdmin->_getSelectList2("view_salesmanCheckIn AS A
    LEFT JOIN table_distributors AS D ON A.ref_id = D.distributor_id 
    LEFT JOIN view_salesmanCheckIn AS A1 ON A.activity_date=A1.activity_date AND A1.activity_type = 29 AND A1.ref_id = A.ref_id AND A1.ref_type = A.ref_type
    LEFT JOIN table_salesman AS S ON A.salesman_id = S.salesman_id 
     LEFT JOIN table_relationship as rr on rr.relationship_id=D.relationship_id 
    left join table_division AS dV ON dV.division_id = D.division_id 
     left join state as ST on ST.state_id=D.state 
    left join city as CT on CT.city_id=D.city 
    left join table_taluka as TL on TL.taluka_id=D.taluka_id",
    'S.salesman_name,A.activity_date,A.activity_type,A.start_time,A.ref_id,A.ref_type,A.salesman_id,D.distributor_id AS id, D.distributor_name AS name, D.distributor_address AS address, D.display_outlet AS outlet,D.lat_lng_capcure_accuracy AS accuracy, D.division_id, D.lat, D.lng, A.lat AS survey_lat, A.lng AS survey_lng, "Distributor" AS customer_type,dV.division_name,rr.relationship_code,ST.state_name,CT.city_name,TL.taluka_name,D.status,A1.start_time as "checkout_time" ',''," D.status!='D' AND (A.lat !='') AND A.activity_type IN(28,29) AND A.ref_type = 2  $intrestedCondition $classCondition $distributorActiveInactiveCond  $divisionCondition  $stateCondition $cityCondition $talukaCondition $salCondition $dateCondition GROUP BY A.salesman_id,A.activity_date,A.ref_id ORDER BY S.salesman_name");

		} else {
		    $distributorData  = array();
		}

		if( !isset($_SESSION['findCust']) || $_SESSION['findCust'] == 'all' || !in_array($_SESSION['findCust'], array('R','D')) ) {
		$typeCondition = "";
		//echo $_SESSION['findCust'];
		if( isset($_SESSION['findCust']) AND !in_array($_SESSION['findCust'], array('R','D', 'all'))) {
		  $typeCondition = " AND C.customer_type='".$_SESSION['findCust']."'";  
		}


		$customerData = $_objAdmin->_getSelectList2("view_salesmanCheckIn AS A
    LEFT JOIN table_customer AS C ON A.ref_id = C.customer_id 
    LEFT JOIN view_salesmanCheckIn AS A1 ON A.activity_date=A1.activity_date AND A1.activity_type = 29 AND A1.ref_id = A.ref_id AND A1.ref_type = A.ref_type
    LEFT JOIN table_salesman AS S ON A.salesman_id = S.salesman_id 
    left join table_division AS dV ON dV.division_id = C.division_id 
     left join state as ST on ST.state_id=C.state 
    left join city as CT on CT.city_id=C.city 
    left join table_taluka as TL on TL.taluka_id=C.taluka_id",
    'S.salesman_name,A.activity_date,A.activity_type,A.start_time,A.ref_id,A.ref_type,A.salesman_id,C.customer_id AS id, C.customer_name AS name, C.customer_address AS address, C.display_outlet AS outlet, C.lat_lng_capcure_accuracy AS accuracy, C.division_id, C.lat, C.lng, A.lat AS survey_lat, A.lng AS survey_lng, C.customer_type,dV.division_name,"-" as relationship_code,ST.state_name,CT.city_name,TL.taluka_name,C.status,A1.start_time as "checkout_time" ','',"  C.status!='D' AND (A.lat !='' || C.lat !='') AND A.activity_type IN(28,29) AND A.ref_type IN(3,4,5)  $intrestedCondition  $customerActiveInactiveCond  $divisionCondition  $stateCondition $cityCondition $talukaCondition $typeCondition $salCondition $dateCondition GROUP BY A.salesman_id,A.activity_date,A.ref_id ORDER BY S.salesman_name");
		} else {
		    $customerData  = array();
		}

		if(sizeof($retailerData)>0 && sizeof($distributorData)>0 && sizeof($customerData)>0) {
		    $custData = array_merge($retailerData, $distributorData, $customerData);
		} else if(sizeof($retailerData)> 0 && sizeof($distributorData)>0 && sizeof($customerData)==0) {
		    $custData = array_merge($retailerData, $distributorData);
		} else if(sizeof($retailerData)> 0 && sizeof($distributorData)==0 && sizeof($customerData)>0) {
		    $custData = array_merge($retailerData, $customerData);
		} else if(sizeof($retailerData)== 0 && sizeof($distributorData)>0 && sizeof($customerData)>0) {
		    $custData = array_merge($distributorData, $customerData);
		} else if(sizeof($retailerData)> 0 && sizeof($distributorData)==0 && sizeof($customerData)==0) {
		    $custData = $retailerData;
		} else if(sizeof($retailerData)== 0 && sizeof($distributorData)>0 && sizeof($customerData)==0) {
		    $custData = $distributorData;
		} else if(sizeof($retailerData)== 0 && sizeof($distributorData)==0 && sizeof($customerData)>0) {
		    $custData = $customerData;
		}

		/* echo "<pre>";
    print_r($custData);
    exit;*/

	if(sizeof($custData)>0){
		foreach ($custData as $key => $value) {

			       if($value->status=="I"){ $status="Inactive";}else{ $status="Active";}
                	if($value->customer_type=="C"){ 
                		$customer_type="Shakti-Partner";
            		}elseif ($value->customer_type=="P") {
            			$customer_type="Pump Installer";
            		}elseif ($value->customer_type=="S"){
            			$customer_type="Solar Pumps Installer";
            		}else{
            			$customer_type=$value->customer_type;
            		}

            $remove = array(",", "-", "/","_",":",";","`","!","?");


            


		$data.="".$value->salesman_name."\t".str_replace($remove,' ',$value->name)."\t".$status."\t".$customer_type."\t".$value->survey_lat."\t".$value->survey_lng."\t".$value->division_name."\t".$value->state_name."\t".$value->city_name."\t".$value->start_time."\t".$value->checkout_time."\t".$value->activity_date."\n";
		}						
	}
	header("Content-type: application/octet-stream");
	header("Content-Disposition: attachment; filename=\"salesman chekcin-checkout report.xls\"");		
	echo $data;
	exit;
	}
	
/******************************************** End Export Salesman Checkin Checkout Report******************************************************************/


/******************************************** Start Export Distributor ******************************************************************/	
	
if(isset($_GET['export_new_distributors_list'])){		
	// $data="Distributor Name \t Distributor Code \t Division \t   State \t District \t District Code \t Taluka \t Taluka Code \t City \t City Code \t Address1 \t Address2 \t Zipcode \t SMS Mobile Number \t Phone Number1 \t Phone Number2 \t Phone Number3 \t Landline Number \t Contact Person1 \t Contact Person Number1 \t Contact Person2 \t Contact Person Number2 \t Contact Person3 \t Contact Person Number3 \t Email ID1 \t Email ID2 \t Email ID3\t Distributor Class\t Distributor Region \t Aadhar Number \t Pan Number  \n";

	$data = "Distributor Name \t Distributor Code \t Division \t Distributor Class \t Distributor Region \t State \t District \t City \t Zipcode \t Address 1 \t Address 2 \t Send SMS Mobile No \t Photo \t Phone Number 1 \t Phone Number 2 \t Phone Number 3 \t Landline Number \t Contact Person 1 \t Contact Number 1 \t Contact Person 2 \t Contact Number 2 \t Contact Person 3 \t Contact Number 3 \t Username \t Email ID 1 \t Email ID 2 \t Email ID 3 \t Date of Birth \t Start Date \t End Date \t Distributor Status \t Login Status \n";


		/*// Added by AJAY@2015-11-20
		$salsList = $_objArrayList->SalesmanArrayList(); // For Admin and Salesman Login
		$divisionList = $_objArrayList->getAllDivisonOfSelectedSalesmen($salsList);
		$divisionFilters = $_objAdmin->getDivisionCondition($divisionList, 'd');*/

		$divisionFilters = "";
		
		$salDiv = $_objAdmin->_getSelectList('table_salesman'," division_id",''," AND salesman_id='".$_SESSION['salesmanId']."'");
		    if(sizeof($salDiv) > 0)
		   	 $divisionFilters = " AND d.division_id =".$salDiv[0]->division_id;



	$condi=	" d.new!='' and d.status!='D' $divisionFilters and d.account_id='".$_SESSION['accountId']."' ORDER BY d.distributor_name";

	// $auRec=$_objAdmin->_getSelectList('table_distributors as d  
	// 	left join table_division AS dV ON dV.division_id = d.division_id  
	// 	left join state as s on d.state=s.state_id 
	// 	left join table_web_users as w on w.distributor_id=d.distributor_id 
	// 	left join city as c on d.city=c.city_id 
	// 	left join table_relationship as tr on tr.relationship_id=d.relationship_id 
	// 	left join table_region as trr on trr.region_id=d.region_id 
	// 	left join table_markets as m on m.market_id=d.market_id 
	// 	left join table_taluka as t on t.taluka_id=d.taluka_id',"d.*,s.state_name,w.email_id,c.city_name,c.city_code,dV.division_name, tr.relationship_code,trr.region_name, m.market_name,m.market_code,t.taluka_name, t.taluka_code",'',$condi);

	$auRec=$_objAdmin->_getSelectList('table_distributors as d  
		left join table_division AS dV ON dV.division_id = d.division_id  
		left join table_account as a on a.account_id=d.account_id 
		left join table_web_users as w on w.distributor_id=d.distributor_id 
		left join table_salesman as sal on sal.salesman_id=d.salesman_id
		left join state as s on s.state_id=d.state 
		left join city as c on c.city_id=d.city 
		left join table_relationship as tr on tr.relationship_id=d.relationship_id 
		left join table_region as rg on rg.region_id=d.region_id 
		left join table_markets as m on m.market_id=d.market_id 
		left join table_taluka as t on t.taluka_id=d.taluka_id',"d.*,sal.salesman_name,sal.salesman_code,w.username,w.email_id,w.web_user_id,w.status as loginStatus,s.state_name,c.city_name,dV.division_name,tr.relationship_code,rg.region_name,d.taluka_id,m.market_name,t.taluka_name",'',$condi);



	if(is_array($auRec)){
		for($i=0;$i<count($auRec);$i++){
		//$data.="".$auRec[$i]->distributor_name."\t".$auRec[$i]->distributor_code."\t".$auRec[$i]->state_name."\t".$auRec[$i]->city_name."\t".$auRec[$i]->city_code."\t".$auRec[$i]->taluka_name."\t".$auRec[$i]->taluka_code."\t".$auRec[$i]->market_name."\t".$auRec[$i]->market_code."\t".$auRec[$i]->distributor_address."\t".$auRec[$i]->distributor_address2."\t".$auRec[$i]->zipcode."\t".$auRec[$i]->sms_number."\t".$auRec[$i]->distributor_phone_no."\t".$auRec[$i]->distributor_phone_no2."\t".$auRec[$i]->distributor_phone_no3."\t".$auRec[$i]->distributor_leadline_no."\t".$auRec[$i]->contact_person."\t".$auRec[$i]->contact_number."\t".$auRec[$i]->contact_person2."\t".$auRec[$i]->contact_number2."\t".$auRec[$i]->contact_person3."\t".$auRec[$i]->contact_number3."\t".$auRec[$i]->distributor_email."\t".$auRec[$i]->distributor_email2."\t".$auRec[$i]->distributor_email3."\t".$auRec[$i]->relationship_code."\t".$auRec[$i]->region_name."\t".$auRec[$i]->aadhar_no."\t".$auRec[$i]->pan_no."\n";


			$start_date==$_objAdmin->_changeDate($auRec[$i]->start_date);

			if($auRec[$i]->distributor_dob=="0000-00-00"){
				$distributor_dob="-";
			}
			else{
				$distributor_dob==$_objAdmin->_changeDate($auRec[$i]->distributor_dob);
			}
			
			//end date
			if($auRec[$i]->status=='I' ){
				$end_date==$_objAdmin->_changeDate($auRec[$i]->end_date);
			} else {
				$end_date=' ';
			}
			//end date
			
			if($auRec[$i]->status=='A'){
				$sts = "Active";
			}else{
				$sts = "Inactive";
			}
			//login status
			if($auRec[$i]->web_user_id!=''){
				if($auRec[$i]->status=='A'){
					$LogSts=($auRec[$i]->loginStatus=='A')?"Active":"Inactive";
				} else {
					$LogSts='Inactive';
				}
			} else {
				$LogSts=' ';
			}
			
			$survey="View Photo";

			$remove = array("\n", "\r\n", "\r");

			$data.='"'.ucwords($auRec[$i]->distributor_name).'"';
			$data.="\t";

			$data.='"'.ucwords($auRec[$i]->distributor_code).'"';
			$data.="\t";

			$data.='"'.ucwords($auRec[$i]->division_name).'"';
			$data.="\t";

			$data.='"'.ucwords($auRec[$i]->relationship_code).'"';
			$data.="\t";

			// $data.='"'.ucwords($auRec[$i]->salesman_name).'"';
			// $data.="\t";

			// $data.='"'.ucwords($auRec[$i]->salesman_code).'"';
			// $data.="\t";

			$data.='"'.ucwords($auRec[$i]->region_name).'"';
			$data.="\t";

			$data.='"'.ucwords($auRec[$i]->state_name).'"';
			$data.="\t";

			$data.='"'.ucwords($auRec[$i]->city_name).'"';
			$data.="\t";

			// $data.='"'.ucwords(str_replace($remove, ' ',$auRec[$i]->taluka_name)).'"';
			// $data.="\t";

			$data.='"'.ucwords(str_replace($remove, ' ',$auRec[$i]->market_name)).'"';
			$data.="\t";

			$data.='"'.ucwords(str_replace($remove, ' ',$auRec[$i]->zipcode)).'"';
			$data.="\t";

			$data.='"'.ucwords(str_replace($remove, ' ',$auRec[$i]->distributor_address)).'"';
			$data.="\t";

			$data.='"'.ucwords(str_replace($remove, ' ',$auRec[$i]->distributor_address2)).'"';
			$data.="\t";
			$data.='"'.$auRec[$i]->sms_number.'"';
			$data.="\t";

			$data.='"'.$survey.'"';
			$data.="\t";

			$data.='"'.$auRec[$i]->distributor_phone_no.'"';
			$data.="\t";

			$data.='"'.$auRec[$i]->distributor_phone_no2.'"';
			$data.="\t";

			$data.='"'.$auRec[$i]->distributor_phone_no3.'"';
			$data.="\t";

			$data.='"'.$auRec[$i]->distributor_leadline_no.'"';
			$data.="\t";

			$data.='"'.ucwords($auRec[$i]->contact_person).'"';
			$data.="\t";

			$data.='"'.str_replace($remove, ' ',$auRec[$i]->contact_number).'"';
			$data.="\t";

			$data.='"'.ucwords($auRec[$i]->contact_person2).'"';
			$data.="\t";

			$data.='"'.$auRec[$i]->contact_number2.'"';
			$data.="\t";

			$data.='"'.ucwords($auRec[$i]->contact_person3).'"';
			$data.="\t";

			$data.='"'.$auRec[$i]->contact_number3.'"';
			$data.="\t";

			$data.='"'.$auRec[$i]->username.'"';
			$data.="\t";

			$data.='"'.$auRec[$i]->distributor_email.'"';
			$data.="\t";

			$data.='"'.$auRec[$i]->distributor_email2.'"';
			$data.="\t";

			$data.='"'.$auRec[$i]->distributor_email3.'"';
			$data.="\t";

			$data.='"'.$distributor_dob.'"';
			$data.="\t";

			$data.='"'.$start_date.'"';
			$data.="\t";

			$data.='"'.$end_date.'"';
			$data.="\t";

			$data.='"'.$sts.'"';	
			$data.="\t";

			$data.='"'.$LogSts.'"';		
			$data.="\n";

		}						
	}
	header("Content-type: application/octet-stream");
	header("Content-Disposition: attachment; filename=\"new_distributors_list.xls\"");		
	echo $data;
	exit;
	}
	
/******************************************** End Export New Distributor ******************************************************************/


/******************************************** Start Export New Retailor ******************************************************************/	
	
	
if(isset($_GET['export_new_retailers_list'])){		
	///$data="Retailer Name \t Retailer Class \t Division \t Retailer Type \t Retailer Channel \t Distributor \t Interested \t State \t District \t District Code \t Taluka \t Taluka Code \t City \t City Code \t Zipcode \t Address1 \t Address2 \t Phone Number1 \t Phone Number2 \t Landline Number \t Contact Person1 \t Contact Person Number1 \t Contact Person2 \t Contact Person Number2 \t Email ID1 \t Email ID2\t Aadhar Number \t Pan Number \t Retailer Code\n";

	/*$data="Dealer Name \t Dealer Class \t Division \t Dealer Code \t Dealer Type \t Dealer Channel \t Distributor \t Interested \t State \t District \t City \t Pincode \t Map \t Photo \t Address 1 \t Address 2 \t Phone Number 1 \t Phone Number 2 \t Landline Number \t Aadhar Number \t Pan Number \t Contact Person 1 \t Contact Number 1 \t Contact Person 2 \t Contact Number 2 \t Username \t Email ID 1 \t Email ID 2 \t Date of Birth \t Start Date \t End Date \t Dealer Status \t Login Status\n";
*/ // added by anurag@13-02-2017


$data="Dealer Name \t Dealer Class \t Division \t Dealer Code \t Dealer Type \t Dealer Channel \t Distributor \t Interested \t State \t District \t City \t Pincode \t Map \t Photo \t Address 1 \t Address 2 \t Phone Number 1 \t Phone Number 2 \t Landline Number \t Aadhar Number \t Pan Number \t Contact Person 1 \t Contact Number 1 \t Contact Person 2 \t Contact Number 2 \t Username \t Email ID 1 \t Email ID 2 \t Date of Birth \t Start Date \t End Date \t Dealer Status \t Country\t Region\t Zone\t Login Status\n";

	/*$salsList = $_objArrayList->SalesmanArrayList(); // For Admin and Salesman Login
	$divisionList = $_objArrayList->getAllDivisonOfSelectedSalesmen($salsList);
	$divisionFilters = $_objAdmin->getDivisionCondition($divisionList, 'r');*/

	$divisionFilters = "";
		
		$salDiv = $_objAdmin->_getSelectList('table_salesman'," division_id",''," AND salesman_id='".$_SESSION['salesmanId']."'");
		    if(sizeof($salDiv) > 0)
		   	 $divisionFilters = " AND r.division_id =".$salDiv[0]->division_id;


	$condi=	" r.new!='' AND r.status!='D' $divisionFilters and r.account_id='".$_SESSION['accountId']."' ORDER BY r.retailer_name";

	// $auRec=$_objAdmin->_getSelectList('table_retailer as r 

	// 	left join table_division AS dV ON dV.division_id = r.division_id  

	// 	LEFT JOIN table_relationship as rr on rr.relationship_id=r.relationship_id 

	// 	left join table_retailer_channel_master as cm on r.channel_id=cm.channel_id 

	// 	left join state as s on r.state=s.state_id 

	// 	left join table_web_users as w on w.retailer_id=r.retailer_id 


	// 	left join city as c on r.city=c.city_id 

	// 	left join table_retailer_type_master as tm on tm.type_id=r.type_id 

	// 	left join table_distributors as d on d.distributor_id=r.distributor_id 
	// 	left join table_markets as m on m.market_id=r.market_id 
	// 	left join table_taluka as t on t.taluka_id=r.taluka_id',"r.*,rr.relationship_code,s.state_name,w.email_id,tm.type_name,d.distributor_name,c.city_name,c.city_code,cm.channel_name,m.market_name,m.market_code, dV.division_name, t.taluka_name, t.taluka_code",'',$condi);



	/*$auRec=$_objAdmin->_getSelectList('table_retailer as r 
		left join table_division AS dV ON dV.division_id = r.division_id  
		LEFT JOIN table_relationship as rr on rr.relationship_id=r.relationship_id 
		left join table_account as a on a.account_id=r.account_id 
		left join table_web_users as w on w.retailer_id=r.retailer_id 
		left join state as s on s.state_id=r.state 
		left join city as c on c.city_id=r.city 
		left join table_retailer_channel_master as cm on r.channel_id=cm.channel_id 
		left join table_retailer_type_master as tm on tm.type_id=r.type_id 
		left join table_distributors as d on d.distributor_id=r.distributor_id 
		left join table_salesman as sal on sal.salesman_id=r.salesman_id
		left join table_markets as m on m.market_id=r.market_id 
		left join table_taluka as t on t.taluka_id=r.taluka_id',"r.*,sal.salesman_name,sal.salesman_code,w.username,cm.channel_name,d.distributor_name,w.email_id,w.web_user_id,rr.relationship_code,tm.type_name, w.status as loginStatus,s.state_name,c.city_name,dV.division_name, t.taluka_name,m.market_name",'',$condi);*/ //added by anurag@13-02-2017

$auRec=$_objAdmin->_getSelectList('table_retailer as r 
		left join table_division AS dV ON dV.division_id = r.division_id  
		LEFT JOIN table_relationship as rr on rr.relationship_id=r.relationship_id 
		left join table_account as a on a.account_id=r.account_id 
		left join table_web_users as w on w.retailer_id=r.retailer_id 
		left join state as s on s.state_id=r.state 
		left join city as c on c.city_id=r.city 
		left join table_retailer_channel_master as cm on r.channel_id=cm.channel_id 
		left join table_retailer_type_master as tm on tm.type_id=r.type_id 
		left join table_distributors as d on d.distributor_id=r.distributor_id 
		left join table_salesman as sal on sal.salesman_id=r.salesman_id
		left join table_markets as m on m.market_id=r.market_id 
		left join table_taluka as t on t.taluka_id=r.taluka_id
		left join country as cn on r.country = cn.country_id
		     left join table_region as tr on r.region = tr.region_id
		    left join table_zone as tz on r.zone = tz.zone_id
			',"r.*,sal.salesman_name,sal.salesman_code,w.username,cm.channel_name,d.distributor_name,w.email_id,w.web_user_id,rr.relationship_code,tm.type_name, w.status as loginStatus,s.state_name,c.city_name,dV.division_name,cn.country_name,tr.region_name,tz.zone_name, t.taluka_name,m.market_name",'',$condi);
	

	if(is_array($auRec)){
		for($i=0;$i<count($auRec);$i++){

			$start_date=$_objAdmin->_changeDate($auRec[$i]->start_date);
			if($auRec[$i]->retailer_dob=="0000-00-00"){
				$retailer_dob="-";
			}
			else{$retailer_dob=$_objAdmin->_changeDate($auRec[$i]->retailer_dob);}
		
			//end date
			if($auRec[$i]->status=='I' ){
				$end_date=$_objAdmin->_changeDate($auRec[$i]->end_date);
			} else {
				$end_date=' ';
			}

			$sts=($auRec[$i]->status=='A')?"Active":"Inactive";

			//login status
			if($auRec[$i]->web_user_id!=''){
				if($auRec[$i]->status=='A'){
					$LogStatus=($auRec[$i]->loginStatus=='A')?"Active":"Inactive";
				} else {
					$LogStatus='Inactive';
				}
			} else {
				$LogStatus=' ';
			}		

			$remove = array("\n", "\r\n", "\r");

		//$data.="".$auRec[$i]->retailer_name."\t".$auRec[$i]->relationship_code."\t".$auRec[$i]->type_name."\t".$auRec[$i]->channel_name."\t".$auRec[$i]->distributor_name."\t".strtoupper($auRec[$i]->display_outlet)."\t".$auRec[$i]->state_name."\t".$auRec[$i]->city_name."\t".$auRec[$i]->city_code."\t".$auRec[$i]->taluka_name."\t".$auRec[$i]->taluka_code."\t".$auRec[$i]->market_name."\t".$auRec[$i]->market_code."\t".$auRec[$i]->zipcode."\t".$auRec[$i]->retailer_address."\t".$auRec[$i]->retailer_address2."\t".$auRec[$i]->retailer_phone_no."\t".$auRec[$i]->retailer_phone_no2."\t".$auRec[$i]->retailer_leadline_no."\t".$auRec[$i]->contact_person."\t".$auRec[$i]->contact_number."\t".$auRec[$i]->contact_person2."\t".$auRec[$i]->contact_number2."\t".$auRec[$i]->email_id."\t".$auRec[$i]->email_id2."\t".$auRec[$i]->aadhar_no."\t".$auRec[$i]->pan_no."\t".$auRec[$i]->retailer_code."\n";

			// Retailer Name
			$data.='"'.$auRec[$i]->retailer_name.'"';
			$data.="\t";

			// Retailer Class
			$data.='"'.$auRec[$i]->relationship_code.'"';
			$data.="\t";

			// Division
			$data.='"'.$auRec[$i]->division_name.'"';
			$data.="\t";

			//  Retailer Code
			$data.='"'.$auRec[$i]->retailer_code.'"';
			$data.="\t";

			// Retailer Type
			$data.='"'.$auRec[$i]->type_name.'"';
			$data.="\t";

			// Retailer Channel
			$data.='"'.$auRec[$i]->channel_name.'"';
			$data.="\t";

			// Distributor
			$data.='"'.$auRec[$i]->distributor_name.'"';
			$data.="\t";

			// Interested
			$data.='"'.strtoupper($auRec[$i]->display_outlet).'"';
			$data.="\t";

			// Salesman Name
			// $data.='"'.strtoupper($auRec[$i]->salesman_name).'"';
			// $data.="\t";


			// Salesman Code
			// $data.='"'.strtoupper($auRec[$i]->salesman_code).'"';
			// $data.="\t";

			// State
			$data.='"'.$auRec[$i]->state_name.'"';
			$data.="\t";

			// District
			$data.='"'.$auRec[$i]->city_name.'"';
			$data.="\t";

			// Taluka
			// $data.='"'.$auRec[$i]->taluka_name.'"';
			// $data.="\t";


			// City
			$data.='"'.$auRec[$i]->market_name.'"';
			$data.="\t";

			// Pincode
			$data.='"'.$auRec[$i]->zipcode.'"';
			$data.="\t";

			// Map
			$data.='"'.$map.'"';
			$data.="\t";

			// Photo
			$data.='"'.$photo.'"';
			$data.="\t";

			// Address 1
			$data.='"'.$auRec[$i]->retailer_address.'"';
			$data.="\t";

			// Address 2
			$data.='"'.$auRec[$i]->retailer_address2.'"';
			$data.="\t";

			// Phone Number 1
			$data.='"'.$auRec[$i]->retailer_phone_no.'"';
			$data.="\t";

			// Phone Number 2
			$data.='"'.$auRec[$i]->retailer_phone_no2.'"';
			$data.="\t";

			// Landline Number
			$data.='"'.$auRec[$i]->retailer_leadline_no.'"';
			$data.="\t";

			// Aadhar Number
			$data.='"'.$auRec[$i]->aadhar_no.'"';
			$data.="\t";

			// Pan Number
			$data.='"'.$auRec[$i]->pan_no.'"';		
			$data.="\t";

			// Contact Person 1
			$data.='"'.$auRec[$i]->contact_person.'"';
			$data.="\t";

			// Contact Number 1
			$data.='"'.$auRec[$i]->contact_number.'"';
			$data.="\t";

			// Contact Person 2
			$data.='"'.$auRec[$i]->contact_person2.'"';
			$data.="\t";

			// Contact Number 2
			$data.='"'.$auRec[$i]->contact_number2.'"';
			$data.="\t";

			// Username
			$data.='"'.$auRec[$i]->username.'"';
			$data.="\t";

			// Email ID 1
			$data.='"'.$auRec[$i]->email_id.'"';
			$data.="\t";

			// Email ID 2
			$data.='"'.$auRec[$i]->email_id2.'"';
			$data.="\t";

			// Date of Birth
			$data.='"'.str_replace($remove, ' ',$retailer_dob).'"';
			$data.="\t";
			
			// Start Date
			$data.='"'.str_replace($remove, ' ',$start_date).'"';
			$data.="\t";

			// End Date
			$data.='"'.str_replace($remove, ' ',$end_date).'"';
			$data.="\t";

			// Retailer Status
			$data.='"'.$sts.'"';
			$data.="\t";
			$data.='"'.$auRec[$i]->country_name.'"';
			$data.="\t";
			$data.='"'.$auRec[$i]->region_name.'"';
			$data.="\t";
			$data.='"'.$auRec[$i]->zone_name.'"';
			$data.="\t";
			// Login Status
			$data.='"'.$LogStatus.'"';
			$data.="\t";
								
			$data.="\n";

		}						
	}
	header("Content-type: application/octet-stream");
	header("Content-Disposition: attachment; filename=\"new_retailer_list.xls\"");		
	echo $data;
	exit;
	}

	
/******************************************** End Export New Retailor ******************************************************************/

/************************* Start Export Warehouse ****************************************/	
	
	
if(isset($_GET['export_warehouse_list'])){		

	$data = "Warehouse Prefix\t Warehouse Name\t Warehouse Code\t Warehouse Phone No\t Warehouse Address\t Country\t Region\t Zone\t State\t City\t Area\t Pincode/Zipcode\t Warehouse Incharge Phone No1\t Warehouse Incharge Phone No2\t Warehouse Incharge Phone No3\t Warehouse Incharge Email-ID1\t Warehouse Incharge Email-ID2\t Warehouse Incharge Email-ID3\t Warehouse Incharge Name\t Username\t Email ID\t Start Date\t End Date\t Status\n ";



	$condi=	"w.status = 'A' and w.account_id = '".$_SESSION['accountId']."' ORDER BY w.warehouse_name";

	
	// $auRec=$_objAdmin->_getSelectList('table_warehouse as w
	// 	left join table_division as divi ON divi.division_id = w.division_id
	// 	left join table_account as a on a.account_id=w.account_id 
	// 	left join table_web_users as web on web.warehouse_id=w.warehouse_id  
	// 	left join country as c on c.country_id=w.country_id
	// 	left join table_region as reg on reg.region_id=w.region_id
	// 	left join table_zone as zn on zn.zone_id=w.zone_id 
	// 	left join state as st on st.state_id=w.state_id 
	// 	left join city as ct on ct.city_id=w.city_id',"w.*,st.state_name,ct.city_name,divi.division_name,reg.region_name,c.country_name,zn.zone_name,web.username,web.email_id,web.web_user_id,web.status as loginStatus",'',$condi);


	$auRec=$_objAdmin->_getSelectList('table_warehouse as w
		left join table_account as a on a.account_id=w.account_id 
		left join table_web_users as web on web.warehouse_id=w.warehouse_id  
		left join country as c on c.country_id=w.country_id
		left join table_region as reg on reg.region_id=w.region_id
		left join table_zone as zn on zn.zone_id=w.zone_id 
		left join state as st on st.state_id=w.state_id 
		left join city as ct on ct.city_id=w.city_id',"w.*,st.state_name,ct.city_name,reg.region_name,c.country_name,zn.zone_name,web.username,web.email_id,web.web_user_id,web.status as loginStatus",'',$condi);
		//print_r($auRec); exit;

	if(is_array($auRec)){
		for($i=0;$i<count($auRec);$i++){

			$start_date=$_objAdmin->_changeDate($auRec[$i]->start_date);

			
			
			//end date
			if($auRec[$i]->status=='I' ){
			$end_date=$_objAdmin->_changeDate($auRec[$i]->end_date);
			} else {
			$end_date=' ';
			}
			//end date

			$sts=($auRec[$i]->status=='A')?"Active":"Inactive";

			//login status
			if($auRec[$i]->web_user_id!=''){
				if($auRec[$i]->status=='A'){
					$LogStatus=($auRec[$i]->loginStatus=='A')?"Active":"Inactive";
				} else {
					$LogStatus='Inactive';
				}
			} else {
				$LogStatus=' ';
			}		

			$remove = array("\n", "\r\n", "\r");

		
			// Warehouse Prefix
			$data.='"'.ucwords(addslashes(str_replace($remove, ' ',$auRec[$i]->warehouse_prefix))).'"';
			$data.="\t";

			// Warehouse Name
			$data.='"'.ucwords(addslashes(str_replace($remove, ' ',$auRec[$i]->warehouse_name))).'"';
			$data.="\t";

			// Warehouse Code
			 $data.='"'.ucwords(addslashes($auRec[$i]->warehouse_code)).'"';
			 $data.="\t";

			 // Warehouse Phone No
			$data.='"'.addslashes($auRec[$i]->warehouse_phone_no).'"';
			$data.="\t";

			 // Warehouse Address
			 $data.='"'.ucwords(str_replace($remove, ' ',addslashes($auRec[$i]->warehouse_address))).'"';
			 $data.="\t";

			

			// Division
			// $data.='"'.ucwords(addslashes($auRec[$i]->division_name)).'"';
			// $data.="\t";

			// Country Name
			$data.='"'.ucwords(addslashes(str_replace($remove, ' ',$auRec[$i]->country_name))).'"';
			$data.="\t";

			//  Region Name
			$data.='"'.ucwords(addslashes(str_replace($remove, ' ',$auRec[$i]->region_name))).'"';
			$data.="\t";

			// Zone Name
			 $data.='"'.ucwords(addslashes(str_replace($remove, ' ',$auRec[$i]->zone_name))).'"';
			$data.="\t";

			// State Name
			 $data.='"'.ucwords(addslashes(str_replace($remove, ' ',$auRec[$i]->state_name))).'"';
			$data.="\t";

			// City
			$data.='"'.ucwords(addslashes(str_replace($remove, ' ',$auRec[$i]->city_name))).'"';
			$data.="\t";

			// Area
			$data.='"'.ucwords(addslashes(str_replace($remove, ' ',$auRec[$i]->market_name))).'"';
			$data.="\t";

			// Pincode/Zipcode
			$data.='"'.ucwords(str_replace($remove, ' ',$auRec[$i]->zipcode)).'"';
			$data.="\t";

			// Warehouse Incharge Person Phone No1
			$data.='"'.addslashes($auRec[$i]->warehouse_incharge_person_phone_no).'"';
			$data.="\t";

			// Warehouse Incharge Person Phone No2
			$data.='"'.addslashes($auRec[$i]->warehouse_incharge_person_phone_no2).'"';
			$data.="\t";

			// Warehouse Incharge Person Phone No3
			$data.='"'.addslashes($auRec[$i]->warehouse_incharge_person_phone_no3).'"';
			$data.="\t";

			// Warehouse Incharge Person Email Id1
			$data.='"'.addslashes(str_replace($remove, ' ',$auRec[$i]->warehouse_incharge_person_email)).'"';
			$data.="\t";

			// Warehouse Incharge Person Email Id2
			$data.='"'.addslashes(str_replace($remove, ' ',$auRec[$i]->warehouse_incharge_person_email2)).'"';
			$data.="\t";

			// Warehouse Incharge Person Email Id3
			$data.='"'.addslashes(str_replace($remove, ' ',$auRec[$i]->warehouse_incharge_person_email3)).'"';
			$data.="\t";

			// Warehouse Incharge Person
			$data.='"'.ucwords(addslashes(str_replace($remove, ' ',$auRec[$i]->warehouse_incharge_person))).'"';
			$data.="\t";

			// User Name
			$data.='"'.ucwords(addslashes(str_replace($remove, ' ',$auRec[$i]->username))).'"';
			$data.="\t";

			// Email Id
			$data.='"'.addslashes(str_replace($remove, ' ',$auRec[$i]->email_id)).'"';
			$data.="\t";

			// Start Date
			$data.='"'.addslashes($start_date).'"';
			$data.="\t";

			// End Date
			$data.='"'.addslashes($end_date).'"';
			$data.="\t";

			// Status
			$data.='"'.$sts.'"';
			$data.="\t";

			// Login Status
			// $data.='"'.$LogSts.'"';
			// $data.="\t";

			// App Version
			// $data.='"'.$app_version.'"';
			// $data.="\t";	

			$data.="\n";
								
			

		}	

	}

	header("Content-type: application/octet-stream");
	header("Content-Disposition: attachment; filename=\"warehouses_list.xls\"");		
	echo $data;
	exit;
}
	
/******************************************** End Export Warehouse***************/ 


/************************ Start Export Service Person's List 21 Feb 2017 By Pooja ****************************/	
	
	
if(isset($_GET['export_service_persons_list'])){		

	$data="Service Personnel Name\t Service Personnel Code\t Division\t Warehouse\t Phone Number\t Address\t Country\t Region\t Zone\t State\t City\t Area\t Pincode/Zipcode\t Username\t Email ID\t Start Date\t End Date\t Status\n";

	$condi=	"sp.status = 'A' and sp.account_id = '".$_SESSION['accountId']."' ORDER BY sp.sp_name desc";

	
	$auRec=$_objAdmin->_getSelectList('table_service_personnel as sp  
		left join table_division AS divi ON divi.division_id = sp.division_id
		left join table_account as a on a.account_id=sp.account_id 
		left join table_warehouse as w on w.warehouse_id=sp.warehouse_id 
		left join table_web_users as web on web.service_personnel_id=sp.service_personnel_id 
		left join country as cnt on cnt.country_id=sp.country_id 
		left join table_region as reg on reg.region_id=sp.region_id 
		left join table_zone as zn on zn.zone_id=sp.zone_id 
		left join state as st on st.state_id=sp.state_id 
		left join city as ct on ct.city_id=sp.city_id',"sp.*,web.username,web.email_id,web.web_user_id,web.status as loginStatus,st.state_name,ct.city_name,divi.division_name,reg.region_name,cnt.country_name,zn.zone_name,w.warehouse_name",'',$condi);
//print_r($auRec); exit;
	if(is_array($auRec)){
		for($i=0;$i<count($auRec);$i++){

			// start date
			$start_date=$_objAdmin->_changeDate($auRec[$i]->start_date);

			// end date
			if($auRec[$i]->status=='I' ){
			$end_date=$_objAdmin->_changeDate($auRec[$i]->end_date);
			} else {
			$end_date=' ';
			}
			
			// status
			$sts=($auRec[$i]->status=='A')?"Active":"Inactive";

			// login status
			if($auRec[$i]->web_user_id!=''){
				if($auRec[$i]->status=='A'){
					$LogStatus=($auRec[$i]->loginStatus=='A')?"Active":"Inactive";
				} else {
					$LogStatus='Inactive';
				}
			} else {
				$LogStatus=' ';
			}		

			$remove = array("\n", "\r\n", "\r");

		

			// Service Person Name
			$data.='"'.ucwords(addslashes(str_replace($remove, ' ',$auRec[$i]->sp_name))).'"';
			$data.="\t";

			// Service Person Code
			 $data.='"'.ucwords(addslashes($auRec[$i]->sp_code)).'"';
			 $data.="\t";

			// Division
			$data.='"'.ucwords(addslashes($auRec[$i]->division_name)).'"';
			$data.="\t";

			//warehouse_name                         
			$data.='"'.ucwords(addslashes(str_replace($remove, ' ',$auRec[$i]->warehouse_name))).'"';
			$data.="\t";

			// Service Person Phone No
			$data.='"'.addslashes($auRec[$i]->sp_phone_no).'"';
			$data.="\t";
			
			// Service Person Address
			 $data.='"'.ucwords(str_replace($remove, ' ',addslashes($auRec[$i]->sp_address))).'"';
			 $data.="\t";

			// Country Name
			$data.='"'.ucwords(addslashes(str_replace($remove, ' ',$auRec[$i]->country_name))).'"';
			$data.="\t";

			//  Region Name
			$data.='"'.ucwords(addslashes(str_replace($remove, ' ',$auRec[$i]->region_name))).'"';
			$data.="\t";

			// Zone Name
			$data.='"'.ucwords(addslashes(str_replace($remove, ' ',$auRec[$i]->zone_name))).'"';
			$data.="\t";

			// State Name
			$data.='"'.ucwords(addslashes(str_replace($remove, ' ',$auRec[$i]->state_name))).'"';
			$data.="\t";

			// City
			$data.='"'.ucwords(addslashes(str_replace($remove, ' ',$auRec[$i]->city_name))).'"';
			$data.="\t";

			// Area
			$data.='"'.ucwords(addslashes(str_replace($remove, ' ',$auRec[$i]->market_name))).'"';
			$data.="\t";

			// Pincode/Zipcode
			$data.='"'.ucwords(str_replace($remove, ' ',$auRec[$i]->zipcode)).'"';
			$data.="\t";

			// User Name
			 $data.='"'.ucwords(addslashes(str_replace($remove, ' ',$auRec[$i]->username))).'"';
			 $data.="\t";

			// Email Id
			$data.='"'.addslashes(str_replace($remove, ' ',$auRec[$i]->email_id)).'"';
			$data.="\t";

			// Start Date
			$data.='"'.addslashes($start_date).'"';
			$data.="\t";

			// End Date
			$data.='"'.addslashes($end_date).'"';
			$data.="\t";

			// Status
			$data.='"'.$sts.'"';
			$data.="\t";

			// Login Status
			// $data.='"'.$LogSts.'"';
			// $data.="\t";

			// App Version
			// $data.='"'.$app_version.'"';
			// $data.="\t";	
							
			$data.="\n";
								
			

		}	

	}

	header("Content-type: application/octet-stream");
	header("Content-Disposition: attachment; filename=\"service_persons_list.xls\"");		
	echo $data;
	exit;
	}
	
/******************************************** End Export Service Person's List***************/ 









/*******************************************************
* DESC: Export Warehouse Stock
* Author: AJAY@2017-02-27
*
*
*
********************************************************/	


	 if(isset($_GET['export_warehouse_stock_report'])){	
	//echo hello;exit;
	
	//$data=" Warehouse Name\t Warehouse Code\t Status \t Battery Ageing(In Days)\t Recharged Date\t BSN\t Distributor Name\t Distributor Code\t Item Code\t Item Name\t Category Name\t Manufacture Date\t Warranty Period\t ProRata\t Grace Period\t Last Update Date\n";	 

	
	$data=" Warehouse Name\t Warehouse Code\t Status \t Recharged Date\t BSN\t Distributor Name\t Distributor Code\t Item Code\t Item Name\t Category Name\t Manufacture Date\t Warranty Period\t ProRata\t Grace Period\t Transfer Type\t Last Update Date\n";	

		
		$where = "";
		$dis="";
		$divisionCondition = "";



		//AJAY@2017-09-08 (If Warehouse logged in the system export data on the based of user login)

		if(isset($_SESSION['warehouseId']) && $_SESSION['userLoginType'] == 7) {
			$_SESSION['warehouseIDSale']  = $_SESSION['warehouseId'];
		} 


		

		/***Filter added by Maninder on 29th April 2016**********************************************/
			if(isset($_SESSION['warehouseIDSale'])){ 

				$querAccount= mysql_query("SELECT session_id, warehouse_id FROM table_warehouse WHERE warehouse_id=".$_SESSION['warehouseIDSale']."");
				$stockRec = mysql_fetch_array($querAccount);
				
				$divisionJoin = "";
				$divisionCondition = "";

				$where .= " AND WS.stock_value!=0 AND WS.warehouse_id='".$_SESSION['warehouseIDSale']."' $divisionCondition ";		      
			}

			if(isset($_SESSION['itemWarehouseId'])){
	          $where .= " AND WS.stock_value!=0 AND WS.item_id='".$_SESSION['itemWarehouseId']."' ";
			}

			$where .= " AND WS.stock_value!=0 ";
			
		/**************************************************/
		
		if($_SESSION['warehouse_id']!="" && $_SESSION['warehouse_id']!="0"){ $dis=" and WS.warehouse_id='".$_SESSION['warehouse_id']."'";}

		// $auRec=$_objAdmin->_getSelectList('table_item_distributor_stock AS s 
		// LEFT JOIN table_cases AS clr ON clr.case_id = s.attribute_value_id 
		// LEFT JOIN table_category AS c ON c.category_id = s.category_id 
		// LEFT JOIN table_item AS i ON i.item_id = s.item_id 
		// LEFT JOIN table_distributors AS d ON d.distributor_id = s.distributor_id '.$divisionJoin,"d.distributor_code,d.distributor_name, c.category_name, clr.case_size, i.item_name, i.item_code, s.dis_stock_value, s.status, s.last_update_datetime,s.dis_stk_id",$rp,$where,'');



		$auRec=$_objAdmin->_getSelectList('table_warehouse_stock AS WS 
		INNER JOIN table_item_bsn AS BSN ON BSN.bsn_id = WS.bsn_id
		LEFT JOIN table_category AS c ON c.category_id = BSN.category_id
		LEFT JOIN table_item AS i ON i.item_id = BSN.item_id
		LEFT JOIN table_warehouse AS W ON W.warehouse_id = WS.warehouse_id 
		LEFT JOIN table_item_dis_stk_inprocess as STKI on STKI.bsn_id = WS.bsn_id and WS.status="D"
		LEFT JOIN table_distributors as D on D.distributor_id = STKI.distributor_id'.$divisionJoin," W.warehouse_code, W.warehouse_name, c.category_name,i.item_name, i.item_code, BSN.bsn, BSN.manufacture_date, BSN.warranty_period, BSN.warranty_prorata, BSN.warranty_grace_period, WS.stock_value, WS.status AS warehouseSts, BSN.status AS bsnsts, WS.last_update_datetime, WS.stk_recharged_date, BSN.recharged_date, BSN.distributor_dispatch_date, WS.warehouse_stock_id,D.distributor_name,D.distributor_code,WS.transfer_type",$rp,$where,'');

	
	     for($i=0;$i<count($auRec);$i++){			
				
				$remove = array("\n", "\r\n", "\r");
				$last_update_datetime=$_objAdmin->_changeDate($auRec[$i]->last_update_datetime);
				$remove = array(",", "-", "_");

			$rechargedDate =($auRec[$i]->stk_recharged_date=='0000-00-00')?"-":$auRec[$i]->stk_recharged_date;

			$stockObj = new StockClass();
			$batteryAge = $stockObj->getBatteryAgeing($auRec[$i]->manufacture_date, $auRec[$i]->stk_recharged_date, $auRec[$i]->distributor_dispatch_date);
			$status = $stockObj->getBatteryStockStatus($auRec[$i]->warehouseSts);


			$data.='"'.ucwords($auRec[$i]->warehouse_name).'"';
			$data.="\t";

			$data.='"'.ucwords($auRec[$i]->warehouse_code).'"';
			$data.="\t";

			$data.='"'.$status.'"';
			$data.="\t";


			// $data.='"'.$batteryAge.'"';
			// $data.="\t";



			$data.='"'.$rechargedDate.'"';
			$data.="\t";



			$data.='"'.$auRec[$i]->bsn.'"';
			$data.="\t";

			$data.='"'.$auRec[$i]->distributor_name.'"';
			$data.="\t";

			$data.='"'.$auRec[$i]->distributor_code.'"';
			$data.="\t";

			$data.='"'.ucwords($auRec[$i]->item_code).'"';
			$data.="\t";

			$data.='"'.ucwords($auRec[$i]->item_name).'"';
			$data.="\t";

			$data.='"'.ucwords($auRec[$i]->category_name).'"';
			$data.="\t";


			$data.='"'.$auRec[$i]->manufacture_date.'"';
			$data.="\t";

			$data.='"'.$auRec[$i]->warranty_period.'"';
			$data.="\t";

			$data.='"'.$auRec[$i]->warranty_prorata.'"';
			$data.="\t";

			$data.='"'.$auRec[$i]->warranty_grace_period.'"';
			$data.="\t";

			if($auRec[$i]->transfer_type=='F')
			{
				$data.='"Factory"';
				$data.="\t";	
			}
			else if($auRec[$i]->transfer_type=='W')
			{
				$data.='"Warehouse"';
				$data.="\t";	
			}
			else
			{
				$data.='"Not Availbale"';
				$data.="\t";
			}
			

			$data.='"'.ucwords($auRec[$i]->last_update_datetime).'"';
			$data.="\n";

		}
			 
	header("Content-type: application/octet-stream");
	header("Content-Disposition: attachment; filename=\"Warehouse Stock Report.xls\"");		
	echo $data;
	exit;
	}



/*******************************************************
* DESC: End of Export Warehouse Stock
* Author: AJAY@2017-02-27
*
*
*
********************************************************/	










/*******************************************************
* DESC: Export Warehouse FIFO BSN DETAIL
* Author: AJAY@2017-04-12
*
*
*
********************************************************/	


	 if(isset($_GET['action']) && $_GET['action'] == 'export_warehouse_fifo_stock_report' && isset($_SESSION['sid']) && $_SESSION['sid']>0)   {	
	//echo hello;exit;
	
	$data=" Warehouse Name\t Warehouse Code\t BSN Status \t FIFO Status \t Manufacture Ageing \t At WH Ageing \t Recharged Date\t BSN\t Item Code\t Item Name\t Category Name\t Manufacture Date\t Warranty Period\t ProRata\t Grace Period\t Factory Dispatched Date\n";	 
		
	$itemCondition = "";
		if(isset($_SESSION['itemDisId']) && $_SESSION['itemDisId']>0) {
			$itemCondition = " AND item_id='".$_SESSION['itemDisId']."'";
		}

		$where = " warehouse_id='".$_SESSION['sid']."' $itemCondition";
		$dis="";

		$auRec=$_objAdmin->_getSelectList2('view_wh_bsn_fifo_status',"*",$rp,$where,'');

	
	     for($i=0;$i<count($auRec);$i++){			
				
				$remove = array("\n", "\r\n", "\r");
				//$last_update_datetime=$_objAdmin->_changeDate($auRec[$i]->last_update_datetime);
				$remove = array(",", "-", "_");

			$rechargedDate =($auRec[$i]->recharged_date=='0000-00-00')?"":$auRec[$i]->recharged_date;

			$stockObj = new StockClass();
			//$batteryAge = $stockObj->getBatteryAgeing($auRec[$i]->manufacture_date, $auRec[$i]->recharged_date, $auRec[$i]->distributor_dispatch_date);
			$status = $stockObj->getBatteryStockStatus($auRec[$i]->status);


			$data.='"'.ucwords($auRec[$i]->warehouse_name).'"';
			$data.="\t";

			$data.='"'.ucwords($auRec[$i]->warehouse_code).'"';
			$data.="\t";

			$data.='"'.$status.'"';
			$data.="\t";


			$data.='"'.$auRec[$i]->FIFO_STATUS.'"';
			$data.="\t";

			$data.='"'.$auRec[$i]->Ageing.'"';
			$data.="\t";

			$data.='"'.$auRec[$i]->WhStockAgeing.'"';
			$data.="\t";


			$data.='"'.$rechargedDate.'"';
			$data.="\t";



			$data.='"'.$auRec[$i]->bsn.'"';
			$data.="\t";

			$data.='"'.ucwords($auRec[$i]->item_code).'"';
			$data.="\t";

			$data.='"'.ucwords($auRec[$i]->item_name).'"';
			$data.="\t";

			$data.='"'.ucwords($auRec[$i]->category_name).'"';
			$data.="\t";


			$data.='"'.$auRec[$i]->manufacture_date.'"';
			$data.="\t";

			$data.='"'.$auRec[$i]->warranty_period.'"';
			$data.="\t";

			$data.='"'.$auRec[$i]->warranty_prorata.'"';
			$data.="\t";

			$data.='"'.$auRec[$i]->warranty_grace_period.'"';
			$data.="\t";

			$data.='"'.ucwords($auRec[$i]->dispatch_date).'"';
			$data.="\n";

		}
			 
	header("Content-type: application/octet-stream");
	header("Content-Disposition: attachment; filename=\"Warehouse FIFO STOCK Report.xls\"");		
	echo $data;
	exit;
	}




































/*******************************************************
* DESC: Export Warehouse FIFO BSN DETAIL
* Author: AJAY@2017-02-27
*
*
*
********************************************************/	


/**************************************************************** 
* Desc:  Export Service Personnel CheckIn CheckOut Report
* By: Pooja@21Apr2017 
******************************************************************/

if(isset($_GET['export_service_personnel_check_inout'])){ 		
	$data="Service Personnel Name \t Customer Name \t Customer Status\t Customer Type \t Latitude \t Longitude \t Division \t State \t City \t Login Time\t Logout Time \t Date\n";

	/*$divisionRec=$_objAdmin->_getSelectList2('table_division',"division_name as division,division_id",'',$condition1);
     $stateRec=$_objAdmin->_getSelectList2('state',"state_name,state_id",'','status="A"');*/

		// Conditions 



		$intrestedCondition = "";
		$classCondition = "";
		$retailerActiveInactiveCond="";
		$distributorActiveInactiveCond="";
		$customerActiveInactiveCond="";
		$divisionCondition = "";
		$stateCondition = "";
		$distributorCondition = "";
		$talukaCondition = "";
		$salCondition = "";
		$dateCondition = "";


		if(isset($_SESSION['findIntr']) && $_SESSION['findIntr']!="all"){
		    $intrestedCondition =" AND display_outlet='".$_SESSION['findIntr']."'";
		}

		if(isset($_SESSION['customerClassRec']) && $_SESSION['customerClassRec']!='all'){
		    $classCondition =" AND rr.relationship_id='".$_SESSION['customerClassRec']."'";
		} 

		if(isset($_SESSION['activeCust']) && $_SESSION['activeCust']!='all'){
		    $retailerActiveInactiveCond =" AND R.status='".$_SESSION['activeCust']."'";
		    $distributorActiveInactiveCond=" AND D.status='".$_SESSION['activeCust']."'";
		    $customerActiveInactiveCond =" AND C.status='".$_SESSION['activeCust']."'";
		}

// $_SESSION['userLoginType'] == 6 for service personnel
		if($_SESSION['userLoginType'] == 6 && !isset($_SESSION['findDivision'])){
		    $divisionIdString = implode(",", $divisionList);
		    $divisionCondition = " AND dV.division_id IN ($divisionIdString) ";
		} 

		if(isset($_SESSION['findDivision']) && $_SESSION['findDivision']!="all"){
		   $divisionCondition = " AND dV.division_id IN(".$_SESSION['findDivision'].")";
		}


		if(isset($_SESSION['stateCust']) && $_SESSION['stateCust']!="all"){
		    $stateCondition = " AND ST.state_id IN(".$_SESSION['stateCust'].")";
		}

		if(isset($_SESSION['districtCust']) && $_SESSION['districtCust']!="all"){
		    $cityCondition = " AND CT.city_id IN(".$_SESSION['districtCust'].")";
		}

		if(isset($_SESSION['tehsilCust']) && $_SESSION['tehsilCust']!="all"){
		    $talukaCondition = " AND TL.taluka_id IN(".$_SESSION['tehsilCust'].")";
		}

		if(isset($_SESSION['sp_id']) && $_SESSION['sp_id']!="all" && $_SESSION['sp_id']!=""){
		    $salCondition = " AND A.service_personnel_id='".$_SESSION['sp_id']."'";
		}

		if(isset($_SESSION['fromDate']) && isset($_SESSION['toDate'])){
		    $dateCondition = " AND A.activity_date BETWEEN '".date('Y-m-d',strtotime($_SESSION['fromDate']))."' AND '".date('Y-m-d',strtotime($_SESSION['toDate']))."'";
		}		

		if( !isset($_SESSION['findCust']) || $_SESSION['findCust'] == 'all' || $_SESSION['findCust'] == 'R' ) { 

		$retailerData = $_objAdmin->_getSelectList2("view_service_personnel AS A
    LEFT JOIN table_retailer AS R ON A.ref_id = R.retailer_id 
    LEFT JOIN view_service_personnel AS A1 ON A.activity_date=A1.activity_date AND A1.activity_type = 29 AND A1.ref_id = A.ref_id AND A1.ref_type = A.ref_type
    LEFT JOIN table_service_personnel AS SP ON A.service_personnel_id = SP.service_personnel_id 
    LEFT JOIN table_relationship as rr on rr.relationship_id=R.relationship_id 
    left join table_division AS dV ON dV.division_id = R.division_id 
    left join state as ST on ST.state_id=R.state 
    left join city as CT on CT.city_id=R.city 
    left join table_taluka as TL on TL.taluka_id=R.taluka_id",
    'SP.sp_name,A.activity_date,A.activity_type,A.start_time,A.ref_id,A.ref_type,A.service_personnel_id,R.retailer_id AS id, R.retailer_name AS name, R.retailer_address AS address, R.display_outlet AS outlet, R.lat_lng_capcure_accuracy AS accuracy, R.division_id, R.lat, R.lng,   A.lat AS survey_lat, A.lng AS survey_lng, "Retailer" AS customer_type,dV.division_name,rr.relationship_code,ST.state_name,CT.city_name,TL.taluka_name,R.status,A1.start_time as "checkout_time" ',''," R.status!='D' AND (A.lat !='') AND A.activity_type IN(28,29) AND A.ref_type = 1 $intrestedCondition $classCondition $retailerActiveInactiveCond $divisionCondition $stateCondition $cityCondition $talukaCondition $salCondition $dateCondition GROUP BY A.service_personnel_id,A.activity_date,A.ref_id ORDER BY SP.sp_name" );

		/*echo "<pre>";
    print_r($retailerData);
    exit;*/

		} else {
		    $retailerData  = array();
		}


		if( !isset($_SESSION['findCust']) || $_SESSION['findCust'] == 'all' || $_SESSION['findCust'] == 'D' ) {

		$distributorData = $_objAdmin->_getSelectList2("view_service_personnel AS A
    LEFT JOIN table_distributors AS D ON A.ref_id = D.distributor_id 
    LEFT JOIN view_service_personnel AS A1 ON A.activity_date=A1.activity_date AND A1.activity_type = 29 AND A1.ref_id = A.ref_id AND A1.ref_type = A.ref_type
    LEFT JOIN table_service_personnel AS SP ON A.service_personnel_id = SP.service_personnel_id 
     LEFT JOIN table_relationship as rr on rr.relationship_id=D.relationship_id 
    left join table_division AS dV ON dV.division_id = D.division_id 
     left join state as ST on ST.state_id=D.state 
    left join city as CT on CT.city_id=D.city 
    left join table_taluka as TL on TL.taluka_id=D.taluka_id",
    'SP.sp_name,A.activity_date,A.activity_type,A.start_time,A.ref_id,A.ref_type,A.service_personnel_id,D.distributor_id AS id, D.distributor_name AS name, D.distributor_address AS address, D.display_outlet AS outlet,D.lat_lng_capcure_accuracy AS accuracy, D.division_id, D.lat, D.lng, A.lat AS survey_lat, A.lng AS survey_lng, "Distributor" AS customer_type,dV.division_name,rr.relationship_code,ST.state_name,CT.city_name,TL.taluka_name,D.status,A1.start_time as "checkout_time" ',''," D.status!='D' AND (A.lat !='') AND A.activity_type IN(28,29) AND A.ref_type = 2  $intrestedCondition $classCondition $distributorActiveInactiveCond  $divisionCondition  $stateCondition $cityCondition $talukaCondition $salCondition $dateCondition GROUP BY A.service_personnel_id,A.activity_date,A.ref_id ORDER BY SP.sp_name");

		} else {
		    $distributorData  = array();
		}

		if( !isset($_SESSION['findCust']) || $_SESSION['findCust'] == 'all' || !in_array($_SESSION['findCust'], array('R','D')) ) {
		$typeCondition = "";
		//echo $_SESSION['findCust'];
		if( isset($_SESSION['findCust']) AND !in_array($_SESSION['findCust'], array('R','D', 'all'))) {
		  $typeCondition = " AND C.customer_type='".$_SESSION['findCust']."'";  
		}


		$customerData = $_objAdmin->_getSelectList2("view_service_personnel AS A
    LEFT JOIN table_customer AS C ON A.ref_id = C.customer_id 
    LEFT JOIN view_service_personnel AS A1 ON A.activity_date=A1.activity_date AND A1.activity_type = 29 AND A1.ref_id = A.ref_id AND A1.ref_type = A.ref_type
    LEFT JOIN table_service_personnel AS SP ON A.service_personnel_id = SP.service_personnel_id 
    left join table_division AS dV ON dV.division_id = C.division_id 
     left join state as ST on ST.state_id=C.state 
    left join city as CT on CT.city_id=C.city 
    left join table_taluka as TL on TL.taluka_id=C.taluka_id",
    'SP.sp_name,A.activity_date,A.activity_type,A.start_time,A.ref_id,A.ref_type,A.service_personnel_id,C.customer_id AS id, C.customer_name AS name, C.customer_address AS address, C.display_outlet AS outlet, C.lat_lng_capcure_accuracy AS accuracy, C.division_id, C.lat, C.lng, A.lat AS survey_lat, A.lng AS survey_lng, C.customer_type,dV.division_name,"-" as relationship_code,ST.state_name,CT.city_name,TL.taluka_name,C.status,A1.start_time as "checkout_time" ','',"  C.status!='D' AND (A.lat !='' || C.lat !='') AND A.activity_type IN(28,29) AND A.ref_type IN(3,4,5)  $intrestedCondition  $customerActiveInactiveCond  $divisionCondition  $stateCondition $cityCondition $talukaCondition $typeCondition $salCondition $dateCondition GROUP BY A.service_personnel_id,A.activity_date,A.ref_id ORDER BY SP.sp_name");
		} else {
		    $customerData  = array();
		}

		if(sizeof($retailerData)>0 && sizeof($distributorData)>0 && sizeof($customerData)>0) {
		    $custData = array_merge($retailerData, $distributorData, $customerData);
		} else if(sizeof($retailerData)> 0 && sizeof($distributorData)>0 && sizeof($customerData)==0) {
		    $custData = array_merge($retailerData, $distributorData);
		} else if(sizeof($retailerData)> 0 && sizeof($distributorData)==0 && sizeof($customerData)>0) {
		    $custData = array_merge($retailerData, $customerData);
		} else if(sizeof($retailerData)== 0 && sizeof($distributorData)>0 && sizeof($customerData)>0) {
		    $custData = array_merge($distributorData, $customerData);
		} else if(sizeof($retailerData)> 0 && sizeof($distributorData)==0 && sizeof($customerData)==0) {
		    $custData = $retailerData;
		} else if(sizeof($retailerData)== 0 && sizeof($distributorData)>0 && sizeof($customerData)==0) {
		    $custData = $distributorData;
		} else if(sizeof($retailerData)== 0 && sizeof($distributorData)==0 && sizeof($customerData)>0) {
		    $custData = $customerData;
		}

		/* echo "<pre>";
    print_r($custData);
    exit;*/

	if(sizeof($custData)>0){
		foreach ($custData as $key => $value) {

			       if($value->status=="I"){ $status="Inactive";}else{ $status="Active";}
                	if($value->customer_type=="C"){ 
                		$customer_type="Shakti-Partner";
            		}elseif ($value->customer_type=="P") {
            			$customer_type="Pump Installer";
            		}elseif ($value->customer_type=="S"){
            			$customer_type="Solar Pumps Installer";
            		}else{
            			$customer_type=$value->customer_type;
            		}

            $remove = array(",", "-", "/","_",":",";","`","!","?");


            


		$data.="".$value->sp_name."\t".str_replace($remove,' ',$value->name)."\t".$status."\t".$customer_type."\t".$value->survey_lat."\t".$value->survey_lng."\t".$value->division_name."\t".$value->state_name."\t".$value->city_name."\t".$value->start_time."\t".$value->checkout_time."\t".$value->activity_date."\n";
		}						
	}
	header("Content-type: application/octet-stream");
	header("Content-Disposition: attachment; filename=\"service personnel checkin-checkout report.xls\"");		
	echo $data;
	exit;
	}

/************************End Export Service Personnel CheckIn CheckOut Report*****************************/




/*******************************************************
* DESC: Export Warehouse Availbale BSN DETAIL
* Author: AJAY@2017-04-26
*
*
*
********************************************************/	


	 if(isset($_GET['action']) && $_GET['action'] == 'export_warehouse_available_stock_report' && isset($_GET['sid']) && $_GET['sid']>0)   {	
	//echo hello;exit;
	
	$data=" Warehouse Name\t Warehouse Code\t Wh BSN Status \t Recharged Date\t BSN\t Item Code\t Item Name\t Category Name\t Manufacture Date\t Warranty Period\t ProRata\t Grace Period\t Factory Dispatched Date\n";	 
		
		$itemCondition = "";
		if(isset($_SESSION['itemDisId']) && $_SESSION['itemDisId']>0) {
			$itemCondition = " AND WS.item_id='".$_SESSION['itemDisId']."'";
		}

		$where = " WS.warehouse_id='".$_GET['sid']."' AND WS.status='A' $itemCondition";
		$dis="";

		$auRec=$_objAdmin->_getSelectList2('table_warehouse_stock AS WS LEFT JOIN table_warehouse AS W ON W.warehouse_id = WS.warehouse_id LEFT JOIN table_item_bsn AS BSN ON BSN.bsn_id = WS.bsn_id LEFT JOIN table_item AS I ON I.item_id = WS.item_id LEFT JOIN table_category AS C ON C.category_id = I.category_id',"WS.*, W.warehouse_name, W.warehouse_code, I.item_code, I.item_name, C.category_name, BSN.bsn, BSN.manufacture_date, BSN.warranty_period, BSN.warranty_prorata, BSN.warranty_grace_period",$rp,$where,'');
		
		$stockObj = new StockClass();
	
	     for($i=0;$i<count($auRec);$i++){			
			
			$remove = array("\n", "\r\n", "\r");
			$remove = array(",", "-", "_");

			$rechargedDate =($auRec[$i]->stk_recharged_date=='0000-00-00')?"":$auRec[$i]->stk_recharged_date;
			$status = $stockObj->getBatteryStockStatus($auRec[$i]->status);


			$data.='"'.ucwords($auRec[$i]->warehouse_name).'"';
			$data.="\t";

			$data.='"'.ucwords($auRec[$i]->warehouse_code).'"';
			$data.="\t";

			$data.='"'.$status.'"';
			$data.="\t";

			$data.='"'.$rechargedDate.'"';
			$data.="\t";

			$data.='"'.$auRec[$i]->bsn.'"';
			$data.="\t";

			$data.='"'.ucwords($auRec[$i]->item_code).'"';
			$data.="\t";

			$data.='"'.ucwords($auRec[$i]->item_name).'"';
			$data.="\t";

			$data.='"'.ucwords($auRec[$i]->category_name).'"';
			$data.="\t";


			$data.='"'.$auRec[$i]->manufacture_date.'"';
			$data.="\t";

			$data.='"'.$auRec[$i]->warranty_period.'"';
			$data.="\t";

			$data.='"'.$auRec[$i]->warranty_prorata.'"';
			$data.="\t";

			$data.='"'.$auRec[$i]->warranty_grace_period.'"';
			$data.="\t";

			$data.='"'.ucwords($auRec[$i]->dispatch_date).'"';
			$data.="\n";

		}
			 
	header("Content-type: application/octet-stream");
	header("Content-Disposition: attachment; filename=\"Warehouse_available_stock.xls\"");		
	echo $data;
	exit;
	}



/*******************************************************
* DESC: Export Warehouse Availbale BSN DETAIL
* Author: AJAY@2017-04-26
*
*
*
********************************************************/	






/*********************************************************
* DESC: Export Service DIstributor List
* Author: AJAY@2017-04-26
*
*********************************************************/

	

	
if(isset($_GET['export_service_distributor_list'])){		

	$data = "Service Distributor Name\t Service Distributor Code\t Country\t Region\t Zone\t State\t City\t Address\t Phone Number\t Pincode\t User Name\t Email Id\t Start Date\t End Date\t Status\n ";



	$condi=	" SD.status = 'A' and SD.account_id = '".$_SESSION['accountId']."' ORDER BY SD.service_distributor_name";

	
	$auRec=$_objAdmin->_getSelectList('table_service_distributor as SD
		
		left join table_account as a on a.account_id=SD.account_id 
		left join table_web_users as web on web.service_distributor_id=SD.service_distributor_id  
		left join country as c on c.country_id=SD.country_id
		left join table_region as reg on reg.region_id=SD.region_id
		left join table_zone as zn on zn.zone_id=SD.zone_id 
		left join state as st on st.state_id=SD.state_id 
		left join city as ct on ct.city_id=SD.city_id',"SD.*,st.state_name,ct.city_name,reg.region_name,c.country_name,zn.zone_name,web.username,web.email_id,web.web_user_id,web.status as loginStatus",'',$condi);

	if(is_array($auRec)){

		for($i=0;$i<count($auRec);$i++){

			$start_date=$_objAdmin->_changeDate($auRec[$i]->start_date);
		
			if($auRec[$i]->status=='I' ){
			$end_date=$_objAdmin->_changeDate($auRec[$i]->end_date);
			} else {
			$end_date=' ';
			}


			$sts=($auRec[$i]->status=='A')?"Active":"Inactive";

			//login status
			if($auRec[$i]->web_user_id!=''){
				if($auRec[$i]->status=='A'){
					$LogStatus=($auRec[$i]->loginStatus=='A')?"Active":"Inactive";
				} else {
					$LogStatus='Inactive';
				}
			} else {
				$LogStatus=' ';
			}		

			$remove = array("\n", "\r\n", "\r");

		

			// Service Distributor Name
			$data.='"'.ucwords(addslashes(str_replace($remove, ' ',$auRec[$i]->service_distributor_name))).'"';
			$data.="\t";

			// Service Distributor Code
			 $data.='"'.ucwords(addslashes($auRec[$i]->service_distributor_code)).'"';
			 $data.="\t";

			

			// Service Distributor Country
			$data.='"'.ucwords(addslashes(str_replace($remove, ' ',$auRec[$i]->country_name))).'"';
			$data.="\t";

			// Service Distributor Region
			$data.='"'.ucwords(addslashes(str_replace($remove, ' ',$auRec[$i]->region_name))).'"';
			$data.="\t";

			// Service Distributor Zone 
			 $data.='"'.ucwords(addslashes(str_replace($remove, ' ',$auRec[$i]->zone_name))).'"';
			$data.="\t";

			// Service Distributor State
			 $data.='"'.ucwords(addslashes(str_replace($remove, ' ',$auRec[$i]->state_name))).'"';
			$data.="\t";

			// Service Distributor City
			$data.='"'.ucwords(addslashes(str_replace($remove, ' ',$auRec[$i]->city_name))).'"';
			$data.="\t";

			// Service Distributor Address
			 $data.='"'.ucwords(str_replace($remove, ' ',addslashes($auRec[$i]->service_distributor_address))).'"';
			 $data.="\t";

			// Service Distributor Phone No
			$data.='"'.addslashes($auRec[$i]->service_distributor_phone_no).'"';
			$data.="\t";

			// Service Distributor Pincode/Zipcode
			$data.='"'.ucwords(str_replace($remove, ' ',$auRec[$i]->zipcode)).'"';
			$data.="\t";

			// Service Distributor User Name
			 $data.='"'.ucwords(addslashes(str_replace($remove, ' ',$auRec[$i]->username))).'"';
			 $data.="\t";

			// Service Distributor Email Id
			$data.='"'.addslashes(str_replace($remove, ' ',$auRec[$i]->email_id)).'"';
			$data.="\t";

			// Service Distributor Start Date
			$data.='"'.addslashes($start_date).'"';
			$data.="\t";

			// Service Distributor End Date
			$data.='"'.addslashes($end_date).'"';
			$data.="\t";

			// Service Distributor Status
			$data.='"'.$sts.'"';
			$data.="\t";

			// Login Status
			// $data.='"'.$LogSts.'"';
			// $data.="\t";

			// App Version
			// $data.='"'.$app_version.'"';
			// $data.="\t";	

			$data.="\n";
								
			

		}	

	}

	header("Content-type: application/octet-stream");
	header("Content-Disposition: attachment; filename=\"service_distributor_list.xls\"");		
	echo $data;
	exit;
}




/*********************************************************
* DESC: Export Service DIstributor List
* Author: AJAY@2017-04-26
*
*********************************************************/






/*********************************************************
* DESC: Export Service DIstributor SP List
* Author: AJAY@2017-04-27
*
*********************************************************/

	

	
if(isset($_GET['export_service_distributor_sp_list'])){		

	$data = "Service Distributor SP Name\t Service Distributor SP Code\t  Service Distributor Name\t Service Distributor Code\t Country\t Region\t Zone\t State\t City\t Area\t Address\t Phone Number\t Pincode\t User Name\t Email Id\t Start Date\t End Date\t Status\n ";




			//AJAY@2017-04-28
		if($_SESSION['userLoginType']==9){
			$condi2 = " AND SD.service_distributor_id =".$_SESSION['serviceDistributorId'];
		}
		//AJAY@2017-04-28
	$condi=	" SD.status = 'A' $condi2 and SD.account_id = '".$_SESSION['accountId']."' ORDER BY SD.service_distributor_sp_name";


	
	$auRec=$_objAdmin->_getSelectList('table_service_distributor_sp as SD
		
		left join table_service_distributor AS TSD ON TSD.service_distributor_id = SD.service_distributor_id
		left join table_account as a on a.account_id=SD.account_id 
		left join table_web_users as web on web.service_distributor_sp_id=SD.service_distributor_sp_id  
		left join country as c on c.country_id=SD.country_id
		left join table_region as reg on reg.region_id=SD.region_id
		left join table_zone as zn on zn.zone_id=SD.zone_id 
		left join state as st on st.state_id=SD.state_id 
		left join city as ct on ct.city_id=SD.city_id',"SD.*,TSD.service_distributor_name, TSD.service_distributor_code, st.state_name,ct.city_name,reg.region_name,c.country_name,zn.zone_name,web.username,web.email_id,web.web_user_id,web.status as loginStatus",'',$condi);

	if(is_array($auRec)){

		for($i=0;$i<count($auRec);$i++){

			$start_date=$_objAdmin->_changeDate($auRec[$i]->start_date);
		
			$end_date=$_objAdmin->_changeDate($auRec[$i]->end_date);



			$sts=($auRec[$i]->status=='A')?"Active":"Inactive";

			//login status
			if($auRec[$i]->web_user_id!=''){
				if($auRec[$i]->status=='A'){
					$LogStatus=($auRec[$i]->loginStatus=='A')?"Active":"Inactive";
				} else {
					$LogStatus='Inactive';
				}
			} else {
				$LogStatus=' ';
			}		

			$remove = array("\n", "\r\n", "\r");

		

			// Warehouse Name
			$data.='"'.ucwords(addslashes(str_replace($remove, ' ',$auRec[$i]->service_distributor_sp_name))).'"';
			$data.="\t";

			// Warehouse Code
			 $data.='"'.ucwords(addslashes($auRec[$i]->service_distributor_sp_code)).'"';
			 $data.="\t";


			// Warehouse Name
			$data.='"'.ucwords(addslashes(str_replace($remove, ' ',$auRec[$i]->service_distributor_name))).'"';
			$data.="\t";

			// Warehouse Code
			 $data.='"'.ucwords(addslashes($auRec[$i]->service_distributor_code)).'"';
			 $data.="\t";


			// // Division
			// $data.='"'.ucwords(addslashes($auRec[$i]->division_name)).'"';
			// $data.="\t";

			// Country Name
			$data.='"'.ucwords(addslashes(str_replace($remove, ' ',$auRec[$i]->country_name))).'"';
			$data.="\t";

			//  Region Name
			$data.='"'.ucwords(addslashes(str_replace($remove, ' ',$auRec[$i]->region_name))).'"';
			$data.="\t";

			// Zone Name
			 $data.='"'.ucwords(addslashes(str_replace($remove, ' ',$auRec[$i]->zone_name))).'"';
			$data.="\t";

			// State Name
			 $data.='"'.ucwords(addslashes(str_replace($remove, ' ',$auRec[$i]->state_name))).'"';
			$data.="\t";

			// City
			$data.='"'.ucwords(addslashes(str_replace($remove, ' ',$auRec[$i]->city_name))).'"';
			$data.="\t";

			//Area
			$data.='"'.ucwords(str_replace($remove, ' ',$auRec[$i]->market_name)).'"';
			$data.="\t";

			// Warehouse Address
			 $data.='"'.ucwords(str_replace($remove, ' ',addslashes($auRec[$i]->service_distributor_sp_address))).'"';
			 $data.="\t";

			// Warehouse Phone No
			$data.='"'.addslashes($auRec[$i]->service_distributor_sp_phone_no).'"';
			$data.="\t";

			// Pincode/Zipcode
			$data.='"'.ucwords(str_replace($remove, ' ',$auRec[$i]->zipcode)).'"';
			$data.="\t";

			// User Name
			 $data.='"'.ucwords(addslashes(str_replace($remove, ' ',$auRec[$i]->username))).'"';
			 $data.="\t";

			// Email Id
			$data.='"'.addslashes(str_replace($remove, ' ',$auRec[$i]->email_id)).'"';
			$data.="\t";

			// Start Date
			$data.='"'.addslashes($start_date).'"';
			$data.="\t";

			// End Date
			$data.='"'.addslashes($end_date).'"';
			$data.="\t";

			// Status
			$data.='"'.$sts.'"';
			$data.="\t";

			// Login Status
			// $data.='"'.$LogSts.'"';
			// $data.="\t";

			// App Version
			// $data.='"'.$app_version.'"';
			// $data.="\t";
								
			$data.="\n";
								
			

		}	

	}

	header("Content-type: application/octet-stream");
	header("Content-Disposition: attachment; filename=\"service_distributor_sp_list.xls\"");		
	echo $data;
	exit;
}




/*********************************************************
* DESC: Export Service DIstributor SP List
* Author: AJAY@2017-04-27
*
*********************************************************/



/*********************************************************
* DESC: Export Call Center Complaints
* Author: Chirag@2017-06-06
*
*********************************************************/

	

	
if(isset($_GET['export_call_center_complaints'])){	

	$data = "Complaint Number\t Operator Name\t Complaint Date\t  Service Distributor \t Service Distributor Code \t Warehouse Name \t RSI Name \t  State\t District\t Customer Name\t Customer Phone \t Customer Address\t Customer Email\t Battery Serial No\t Battery Model\t Battery Segment\t Sale Date\t Service Distributor SP\t Service Distributor SP Phone\t Action Taken\t Failure Mode \t Auth Code\t Complaint Status\t Call Closed Time\t In-Process TAT (in hours)\t TAT1\t TAT2\t Approver Comment \t Call login Date-Time\t Initial Reading Date-Time\t Allocation Date-Time\t Amount\t Comment\n ";

			
	$condi=	" COMPLAINT.account_id = '".$_SESSION['accountId']."' and COMPLAINT.call_center_id !=0 ORDER BY COMPLAINT.created_date ";

	
	$auRec=$_objAdmin->_getSelectList('table_complaint as COMPLAINT
		left join table_service_distributor as SD on SD.service_distributor_id = COMPLAINT.service_distributor_id
		left join table_customer as CUST on CUST.customer_id = COMPLAINT.customer_id
		left join table_item_bsn as BSN on BSN.bsn_id = COMPLAINT.bsn_id
		left join table_service_distributor_sp as SP on SP.service_distributor_sp_id = COMPLAINT.service_distributor_sp_id
		left join state as ST on ST.state_id = CUST.state
		left join table_service_distributor_sp_bcf as BCF on BCF.complaint_id = COMPLAINT.complaint_id
		left join table_failure_mode as FM on FM.failure_id = BCF.failure_mode
		left join table_item as ITEM on ITEM.item_id = BSN.item_id
		left join table_segment as SEGMENT on SEGMENT.segment_id = ITEM.item_segment
		left join city as CT on CUST.city = CT.city_id
		left join table_account_admin as ADMIN on ADMIN.operator_id = COMPLAINT.call_center_id
		LEFT Join table_warehouse as WHS on WHS.warehouse_id = SD.warehouse_id',' COMPLAINT.*, SD.service_distributor_name,SD.service_distributor_code,CUST.customer_name,CUST.customer_phone_no,CUST.customer_email,CUST.customer_address,BSN.date_of_sale,SP.service_distributor_sp_name,SP.service_distributor_sp_phone_no,ST.state_name,BCF.action_taken,BCF.eapi_auth_code,BCF.under_process,BCF.status as BCFST,BCF.approver_comments,BCF.created_datetime,BCF.initial_reading_save_time ,COMPLAINT.created_date as complaint_date,COMPLAINT.assigned_sp_time,BCF.call_close_datetime,ADMIN.operator_name,FM.failure_desc,FM.failure_code,SEGMENT.segment_name,COMPLAINT.allocation_date as allocation_date ,ITEM.item_code,CT.city_name,BCF.asd_basic_payout AS asd_basic_payout,BCF.asd_tat_incentive AS asd_tat_incentive ,BCF.asd_payout_revival AS asd_payout_revival,WHS.warehouse_name,WHS.warehouse_id as WHSID','',$condi);

	if(is_array($auRec)){

		for($i=0;$i<count($auRec);$i++){

			$Sale_date=$_objAdmin->_changeDate($auRec[$i]->date_of_sale);
		
			$Complaint_date=$_objAdmin->_changeDate($auRec[$i]->complaint_date);


			if($auRec[$i]->complain_status=='R')
			{
				$sts = "Rejected";	
			}
			else if($auRec[$i]->complain_status=='I')
			{
				$sts = "In-Process";	
			}

			else if($auRec[$i]->complain_status=='P')
			{
				$sts = "Pending";	
			}
			else if($auRec[$i]->complain_status=='C')
			{
				$sts = "Complete";	
			}
			

			/****************** Calculating BCF Status ****************************************/
			if(($auRec[$i]->under_process=='U' && $auRec[$i]->BCFST=='R'))
			{
				$bcf_status = "Closed (U)";
			}
			else if($auRec[$i]->under_process == 'T' && $auRec[$i]->BCFST=='A') 
			{
				$bcf_status = "Closed (T)";
			}
			else if($auRec[$i]->under_process=='C' && $auRec[$i]->BCFST=='C')
			{
				$bcf_status = "Closed (C)";
			}
			else if($auRec[$i]->BCFST=='P' && $auRec[$i]->under_process=='F')
			{
				$bcf_status = "Final Inspection Complete (F)";
			}
			else if($auRec[$i]->under_process=='I' && $auRec[$i]->BCFST=='P')
			{
				$bcf_status = "Initial Inspection Complete (I)";
			}
			else if (($auRec[$i]->assigned_sp_time !='0000-00-00 00:00:00') && ($auRec[$i]->service_distributor_sp_bcf_id == NULL))
			{
				$bcf_status = "Pending at SE";
			}
			else
			{
				$bcf_status = "New (N)";
			}

			/****************** Calculating BCF Status ****************************************/


			/****************** Calculating IN-PROCESS TAT ****************************************/
			if($auRec[$i]->cctat1=='')
			{
				$calculated_pending='';
				$current_time = date("Y-m-d H:i:s");
				$calculated_pending = round((strtotime($current_time) - strtotime($auRec[$i]->complaint_date)) /3600);
			}
			else 
			{
				$calculated_pending ='-';	
			}
 $AMOUNT=$auRec[$i]->asd_basic_payout+ $auRec[$i]->asd_tat_incentive + $auRec[$i]->asd_payout_revival;
			/****************** Calculating IN-PROCESS TAT ****************************************/


			// Complaint No
			$data.='"'.ucwords(addslashes($auRec[$i]->complaint_no)).'"';
			$data.="\t";

			// Operator Name
			 $data.='"'.ucwords(addslashes($auRec[$i]->operator_name)).'"';
			 $data.="\t";

			// Complaint Date
			 $data.='"'.ucwords(addslashes($Complaint_date)).'"';
			 $data.="\t";


			// Service Distributor
			$data.='"'.ucwords(addslashes($auRec[$i]->service_distributor_name)).'"';
			$data.="\t";

			// Service Distributor Code
			 $data.='"'.ucwords(addslashes($auRec[$i]->service_distributor_code)).'"';
			 $data.="\t";


			 // Warehouse Name
			 $data.='"'.ucwords(addslashes($auRec[$i]->warehouse_name)).'"';
			 $data.="\t";

			 $RSINAME = $_objAdmin->_getSelectList2('table_service_personnel_warehouse_mapping as TSPWM left join table_service_personnel as SPNL on SPNL.service_personnel_id = TSPWM.service_personnel_id',' SPNL.sp_name','',' TSPWM.warehouse_id = "'.$auRec[$i]->WHSID.'"');

			 // RSI Name
			 $data.='"'.ucwords(addslashes($RSINAME[0]->sp_name)).'"';
			 $data.="\t";

			

			 // Customer State
			$data.='"'.ucwords(addslashes($auRec[$i]->state_name)).'"';
			$data.="\t";

			 // Customer District
			$data.='"'.ucwords(addslashes($auRec[$i]->city_name)).'"';
			$data.="\t";

			// Customer Name
			$data.='"'.ucwords(addslashes($auRec[$i]->customer_name)).'"';
			$data.="\t";

			//  Customer Phone
			$data.='"'.ucwords(addslashes($auRec[$i]->customer_phone_no)).'"';
			$data.="\t";

			// Customer Address
			 $data.='"'.ucwords(addslashes($auRec[$i]->customer_address)).'"';
			$data.="\t";

			// Customer Email
			 $data.='"'.ucwords(addslashes($auRec[$i]->customer_email)).'"';
			$data.="\t";

			// BSN
			$data.='"'.ucwords(addslashes($auRec[$i]->product_serial_no)).'"';
			$data.="\t";

			// Battery Model
			$data.='"'.ucwords(addslashes($auRec[$i]->item_code)).'"';
			$data.="\t";

			// Battery Segment
			$data.='"'.ucwords(addslashes($auRec[$i]->segment_name)).'"';
			$data.="\t";

			// Sale Date
			$data.='"'.ucwords($auRec[$i]->date_of_sale).'"';
			$data.="\t";

			// Service Personnel Name
			 $data.='"'.ucwords(addslashes($auRec[$i]->service_distributor_sp_name)).'"';
			 $data.="\t";

			// Service Personnel Phone
			$data.='"'.addslashes($auRec[$i]->service_distributor_sp_phone_no).'"';
			$data.="\t";

			// Action Taken
			$data.='"'.addslashes($auRec[$i]->action_taken).'"';
			$data.="\t";

			// Failure Mode 
			$data.='"'.addslashes($auRec[$i]->failure_desc).'-'.addslashes($auRec[$i]->failure_code).'"';
			$data.="\t";

			// Auth Code
			$data.='"'.addslashes($auRec[$i]->eapi_auth_code).'"';
			$data.="\t";

			// Complaint Status
			$data.='"'.$bcf_status.'"';
			$data.="\t";

			// Call Closed Datetime
			$data.='"'.addslashes($auRec[$i]->call_close_datetime).'"';
			$data.="\t";

			// In-Process TAT
			 $data.='"'.addslashes($calculated_pending).'"';
			 $data.="\t";

			// TAT-1
			 $data.='"'.addslashes($auRec[$i]->cctat1).'"';
			 $data.="\t";

			// TAT-2
			$data.='"'.addslashes($auRec[$i]->cctat2).'"';
			$data.="\t";

			// Approver Comments
			$data.='"'.addslashes($auRec[$i]->approver_comments).'"';
			$data.="\t";

			// Created Date/Time
			$data.='"'.addslashes($auRec[$i]->created_datetime).'"';
			$data.="\t";

			// Initial Date/Time
			$data.='"'.addslashes($auRec[$i]->initial_reading_save_time).'"';
			$data.="\t";

			// allocation Date/Time
			$data.='"'.addslashes($auRec[$i]->allocation_date).'"';
			$data.="\t";

			// Amount
			$data.='"'.addslashes($AMOUNT).'"';
			$data.="\t";



			// Comment 
			$data.='"'.addslashes($auRec[$i]->remark).'"';
			$data.="\t";

			
			

			$data.="\n";
								
			

		}	

	}

	header("Content-type: application/octet-stream");
	header("Content-Disposition: attachment; filename=\"complaint_list.xls\"");		
	echo $data;
	exit;
}




/*********************************************************
* DESC: Export Call Center Complaints
* Author: Chirag@2017-06-06
*
*********************************************************/




/*********************************************************
* DESC: Export Complaints of ASD
* Author: Chirag@2017-06-13
*
*********************************************************/

	

	
if(isset($_GET['export_complaint_history_asd'])){		

	$data = "Complaint Number\t Operator Name\t Complaint Date\t State\t Customer Name\t Customer Phone \t Customer Address\t Customer Email\t Battery Serial No\t Battery Segment \t Sale Date\t Service Distributor SP\t Service Distributor SP Phone\t Warehouse Name \t RSI Name\t Failure Mode \tAuth Code\t Action Taken\t Complaint Status\t Call Closed Datetime\t In-Process TAT (in hours)\t TAT1\t TAT2\t Approver Comments\t Comment\n ";

			
	$condi=	" COMPLAINT.account_id = '".$_SESSION['accountId']."' and COMPLAINT.service_distributor_id = '".$_SESSION['serviceDistributorId']."' ORDER BY COMPLAINT.created_date";


	
	$auRec=$_objAdmin->_getSelectList('table_complaint as COMPLAINT
		left join table_service_distributor as SD on SD.service_distributor_id = COMPLAINT.service_distributor_id
		left join table_customer as CUST on CUST.customer_id = COMPLAINT.customer_id
		left join table_item_bsn as BSN on BSN.bsn_id = COMPLAINT.bsn_id
		left join table_service_distributor_sp as SP on SP.service_distributor_sp_id = COMPLAINT.service_distributor_sp_id
		left join state as ST on ST.state_id = CUST.state
		left join table_service_distributor_sp_bcf as BCF on BCF.complaint_id = COMPLAINT.complaint_id
		left join table_failure_mode as FM on FM.failure_id = BCF.failure_mode
		left join table_item as ITEM on ITEM.item_id = BSN.item_id
		left join table_segment as SEGMENT on SEGMENT.segment_id = ITEM.item_segment
		left join table_account_admin as ADMIN on ADMIN.operator_id = COMPLAINT.call_center_id
		Left Join table_warehouse as WHS on WHS.warehouse_id = SD.warehouse_id',' COMPLAINT.*, SD.service_distributor_name,SD.service_distributor_code,CUST.customer_name,CUST.customer_phone_no,CUST.customer_email,CUST.customer_address,BSN.date_of_sale,SP.service_distributor_sp_name,SP.service_distributor_sp_phone_no,ST.state_name,BCF.action_taken,BCF.eapi_auth_code,BCF.under_process,BCF.status as BCFST,COMPLAINT.created_date as complaint_date,COMPLAINT.assigned_sp_time,BCF.call_close_datetime,ADMIN.operator_name,FM.failure_code,FM.failure_desc,BCF.approver_comments,SEGMENT.segment_name,WHS.warehouse_name,WHS.warehouse_id as WHSID','',$condi);

	if(is_array($auRec)){

		for($i=0;$i<count($auRec);$i++){

			$Sale_date=$_objAdmin->_changeDate($auRec[$i]->date_of_sale);
		
			$Complaint_date=$_objAdmin->_changeDate($auRec[$i]->complaint_date);


			if($auRec[$i]->complain_status=='R')
			{
				$sts = "Rejected";	
			}
			else if($auRec[$i]->complain_status=='I')
			{
				$sts = "In-Process";	
			}

			else if($auRec[$i]->complain_status=='P')
			{
				$sts = "Pending";	
			}
			else if($auRec[$i]->complain_status=='C')
			{
				$sts = "Complete";	
			}
			
			
			/****************** Calculating BCF Status ****************************************/
			if(($auRec[$i]->under_process=='U' && $auRec[$i]->BCFST=='R'))
			{
				$bcf_status = "Closed (U)";
			}
			else if($auRec[$i]->under_process == 'T' && $auRec[$i]->BCFST=='A') 
			{
				$bcf_status = "Closed (T)";
			}
			else if($auRec[$i]->under_process=='C' && $auRec[$i]->BCFST=='C')
			{
				$bcf_status = "Closed (C)";
			}
			else if($auRec[$i]->BCFST=='P' && $auRec[$i]->under_process=='F')
			{
				$bcf_status = "Final Inspection Complete (F)";
			}
			else if($auRec[$i]->under_process=='I' && $auRec[$i]->BCFST=='P')
			{
				$bcf_status = "Initial Inspection Complete (I)";
			}
			else if (($auRec[$i]->assigned_sp_time !='0000-00-00 00:00:00') && ($auRec[$i]->service_distributor_sp_bcf_id == NULL))
			{
				$bcf_status = "Pending at SE";
			}
			else
			{
				$bcf_status = "New (N)";
			}

			/****************** Calculating BCF Status ****************************************/


			/****************** Calculating IN-PROCESS TAT ****************************************/
			if($auRec[$i]->cctat1=='')
			{
				$calculated_pending='';
				$current_time = date("Y-m-d H:i:s");
				$calculated_pending = round((strtotime($current_time) - strtotime($auRec[$i]->complaint_date)) /3600);
			}
			else 
			{
				$calculated_pending ='-';	
			}

			/****************** Calculating IN-PROCESS TAT ****************************************/
		

			// Complaint No
			$data.='"'.ucwords(addslashes($auRec[$i]->complaint_no)).'"';
			$data.="\t";

			// Operator Name
			$data.='"'.ucwords(addslashes($auRec[$i]->operator_name)).'"';
			$data.="\t";

			// Complaint Date
			 $data.='"'.ucwords(addslashes($Complaint_date)).'"';
			 $data.="\t";


			 // State
			 $data.='"'.ucwords(addslashes($auRec[$i]->state_name)).'"';
			 $data.="\t";

			// Customer Name
			$data.='"'.ucwords(addslashes($auRec[$i]->customer_name)).'"';
			$data.="\t";

			//  Customer Phone
			$data.='"'.ucwords(addslashes($auRec[$i]->customer_phone_no)).'"';
			$data.="\t";

			// Customer Address
			 $data.='"'.ucwords(addslashes($auRec[$i]->customer_address)).'"';
			$data.="\t";

			// Customer Email
			 $data.='"'.ucwords(addslashes($auRec[$i]->customer_email)).'"';
			$data.="\t";

			// BSN
			$data.='"'.ucwords(addslashes($auRec[$i]->product_serial_no)).'"';
			$data.="\t";

			// Segment Name
			$data.='"'.ucwords(addslashes($auRec[$i]->segment_name)).'"';
			$data.="\t";

			// Sale Date
			$data.='"'.ucwords($auRec[$i]->date_of_sale).'"';
			$data.="\t";

			// Service Personnel Name
			 $data.='"'.ucwords(addslashes($auRec[$i]->service_distributor_sp_name)).'"';
			 $data.="\t";

			// Service Personnel Phone
			$data.='"'.addslashes($auRec[$i]->service_distributor_sp_phone_no).'"';
			$data.="\t";


			// Warehouse Name
			 $data.='"'.ucwords(addslashes($auRec[$i]->warehouse_name)).'"';
			 $data.="\t";

			 $RSINAME = $_objAdmin->_getSelectList2('table_service_personnel_warehouse_mapping as TSPWM left join table_service_personnel as SPNL on SPNL.service_personnel_id = TSPWM.service_personnel_id',' SPNL.sp_name','',' TSPWM.warehouse_id = "'.$auRec[$i]->WHSID.'"');

			 // RSI Name
			 $data.='"'.ucwords(addslashes($RSINAME[0]->sp_name)).'"';
			 $data.="\t";

			// Failure Mode
			$data.='"'.addslashes($auRec[$i]->failure_desc).'-'.addslashes($auRec[$i]->failure_code).'"';
			$data.="\t";

			// Auth Code
			$data.='"'.addslashes($auRec[$i]->auth_token).'"';
			$data.="\t";


			// Action Taken
			$data.='"'.addslashes($auRec[$i]->action_taken).'"';
			$data.="\t";


			// Complaint Status
			$data.='"'.$bcf_status.'"';
			$data.="\t";


			// Call Close Datetime
			$data.='"'.addslashes($auRec[$i]->call_close_datetime).'"';
			$data.="\t";


			// In Process TAT
			$data.='"'.$calculated_pending.'"';
			$data.="\t";

			// TAT-1
			 $data.='"'.addslashes($auRec[$i]->cctat1).'"';
			 $data.="\t";

			// TAT-2
			$data.='"'.addslashes($auRec[$i]->cctat2).'"';
			$data.="\t";

			// Comment
			$data.='"'.addslashes($auRec[$i]->approver_comments).'"';
			$data.="\t";

			// Comment
			$data.='"'.addslashes($auRec[$i]->remark).'"';
			$data.="\t";

			$data.="\n";
								
			

		}	

	}

	header("Content-type: application/octet-stream");
	header("Content-Disposition: attachment; filename=\"complaint_history_asd.xls\"");		
	echo $data;
	exit;
}




/*********************************************************
* DESC: Export Complaints of ASD
* Author: Chirag@2017-06-13
*
*********************************************************/






/*********************************************************
* DESC: Export Running Complaints of ASD
* Author: Chirag@2017-06-27
*
*********************************************************/

	

	
if(isset($_GET['export_asd_complaints'])){		
if($_SESSION['serviceDistributorId']>0)
{
	$data = "Complaint Number\t Operator Name\t Complaint Date\t Battery Serial No\t Battery Segment\t Item Code\t Battery Used (in months)\t Warranty (in months)\t Prorata (in months)\t Date of Sale \t Service Distributor SE\t Service Distributor SE Phone\t Warehouse Name\t RSI Name \t Complaint Assigned Time\t Customer Name\t Customer Phone No\t Customer City \t Customer Address\t Approver Comments\t Comment\t Failure Mode\t Authentication Token \t Action Taken \t Initial Reading Save Time\t Complaint Status\t  Call Close Datetime\n";

			
	$condi=	" AND c.complain_status!='C' AND c.service_distributor_id ='".$_SESSION['serviceDistributorId']."'";


	
	$auRec=$_objAdmin->_getSelectList('table_complaint AS c 
			LEFT JOIN table_item_bsn AS BSN ON BSN.bsn_id = c.bsn_id
			LEFT JOIN table_item AS I ON I.item_id = BSN.item_id 
			LEFT JOIN table_distributors AS d ON d.distributor_id=c.distributor_id
			LEFT JOIN table_retailer AS R ON R.retailer_id=c.retailer_id
			LEFT JOIN table_service_personnel AS SP ON SP.service_personnel_id = c.service_personnel_id
			LEFT JOIN table_service_distributor AS SD ON SD.service_distributor_id = c.service_distributor_id
			LEFT JOIN table_service_distributor_sp AS SDSP ON SDSP.service_distributor_sp_id = c.service_distributor_sp_id
			LEFT JOIN table_service_distributor_sp_bcf as BCF on BCF.complaint_id = c.complaint_id
			LEFT JOIN table_warehouse AS tw ON tw.warehouse_id=c.warehouse_id 
			LEFT JOIN table_customer as CUST on CUST.customer_id = c.customer_id
			LEFT JOIN city as CTY on CTY.city_id = CUST.city
			LEFT JOIN table_failure_mode as FM on FM.failure_id = BCF.failure_mode
			LEFT Join table_segment as SEGMENT on I.item_segment = SEGMENT.segment_id 
			LEFT JOIN table_account_admin as ADMIN on ADMIN.operator_id = c.call_center_id
			LEFT JOIN table_warehouse as WHS on WHS.warehouse_id = SD.warehouse_id',' c.product_serial_no AS pid, c.auth_token, c.created_date AS complaint_date, d.state AS state, c.app_date AS app_date, c.remark AS remark, c.replace_battery_bsn AS replace_battery_bsn, c.complaint_id AS complaint_id, c.retailer_id AS retailer_id, d.distributor_name AS distributor_name, R.retailer_name as retailer_name, c.warranty_card_img_url as warranty_card_img_url , c.complain_status as complain_status, c.service_personnel_id as service_personnel_id, c.service_distributor_id, c.service_distributor_sp_id, c.complaint_no,c.assigned_sp_time, SDSP.service_distributor_sp_name, SDSP.service_distributor_sp_code,SDSP.service_distributor_sp_phone_no, SP.sp_name, SD.service_distributor_name, SD.service_distributor_code, BSN.item_id, BSN.category_id, BSN.warranty_period, BSN.warranty_prorata, BSN.date_of_sale, I.item_name, 
			 I.item_code,tw.warehouse_name as warehouse_name, BCF.initial_reading_save_time, CTY.city_name as CUST_city, BCF.under_process,BCF.status as BCFST,BCF.action_taken,CUST.customer_address as CUST_ADDRESS,CUST.customer_phone_no,CUST.customer_name,BCF.service_distributor_sp_bcf_id,BCF.action_taken,c.assigned_sp_time,BCF.call_close_datetime,ADMIN.operator_name,BCF.approver_comments,FM.failure_code,FM.failure_desc,SEGMENT.segment_name,WHS.warehouse_name, WHS.warehouse_id as WHSID','',$condi);

	if(is_array($auRec)){

		for($i=0;$i<count($auRec);$i++){

			$Sale_date=$_objAdmin->_changeDate($auRec[$i]->date_of_sale);
		
			$Complaint_date=$_objAdmin->_changeDate($auRec[$i]->complaint_date);


			// if($auRec[$i]->complain_status=='R')
			// {
			// 	$sts = "Rejected";	
			// }
			// else if($auRec[$i]->complain_status=='I')
			// {
			// 	$sts = "In-Process";	
			// }

			// else if($auRec[$i]->complain_status=='P')
			// {
			// 	$sts = "Pending";	
			// }
			// else if($auRec[$i]->complain_status=='C')
			// {
			// 	$sts = "Complete";	
			// }
			
			$objStock = new StockClass();
			/****************** Calculating BCF Status ****************************************/
			if(($auRec[$i]->under_process=='U' && $auRec[$i]->BCFST=='R'))
			{
				$bcf_status = "Closed (U)";
			}
			else if($auRec[$i]->under_process == 'T' && $auRec[$i]->BCFST=='A') 
			{
				$bcf_status = "Closed (T)";
			}
			else if($auRec[$i]->under_process=='C' && $auRec[$i]->BCFST=='C')
			{
				$bcf_status = "Closed (C)";
			}
			else if($auRec[$i]->BCFST=='P' && $auRec[$i]->under_process=='F')
			{
				$bcf_status = "Final Inspection Complete (F)";
			}
			else if($auRec[$i]->under_process=='I' && $auRec[$i]->BCFST=='P')
			{
				$bcf_status = "Initial Inspection Complete (I)";
			}
			else if (($auRec[$i]->assigned_sp_time !='0000-00-00 00:00:00') && ($auRec[$i]->service_distributor_sp_bcf_id == NULL))
			{
				$bcf_status = "Pending at SE";
			}
			else
			{
				$bcf_status = "New (N)";
			}

			/****************** Calculating BCF Status ****************************************/


			/****************** Calculating IN-PROCESS TAT ****************************************/
			if($auRec[$i]->cctat1=='')
			{
				$calculated_pending='';
				$current_time = date("Y-m-d H:i:s");
				$calculated_pending = round((strtotime($current_time) - strtotime($auRec[$i]->complaint_date)) /3600);
			}
			else 
			{
				$calculated_pending ='-';	
			}

			/****************** Calculating IN-PROCESS TAT ****************************************/
		


			/******************************* Calculating Battery Usage **************************/


			$usedBatteryInMonths = 0;
			$usedBatteryInDays   = 0;
			$date_of_sale        = "";
			$claimSlabWithDiscount = "";
			if($auRec[$i]->date_of_sale!='' && $auRec[$i]->date_of_sale!='1970-01-01' && $auRec[$i]->date_of_sale!='0000-00-00' ) {
			

				//$usedBatteryInMonths = strtotime(date('Y-m-d'))-strtotime($auRec[$i]->date_of_sale) / (60*60*24);	

				$datetime1 = date_create(date('Y-m-d'));

				$datetime2 = date_create($auRec[$i]->date_of_sale);

				$interval = date_diff($datetime1, $datetime2);

				//$usedBatteryInMonths = $interval->format('%m month');
				$usedBatteryInDays = $interval->format('%a');
				$usedBatteryInMonths = ceil($usedBatteryInDays  / 30)	;

				$date_of_sale = $objStock->dateFormatter($auRec[$i]->date_of_sale);

				//if($usedBatteryInMonths+1>$auRec[$i]->warranty_period && $usedBatteryInMonths+1<$auRec[$i]->warranty_prorata )
				//if($usedBatteryInMonths+1>$auRec[$i]->warranty_period && $usedBatteryInMonths+1<$auRec[$i]->warranty_prorata )
				$claimSlabWithDiscount = 	$objStock->getBatteryProRataCliamSlabDetail($auRec[$i]->item_id, $usedBatteryInMonths, $auRec[$i]->warranty_period);
				if(sizeof($claimSlabWithDiscount)>0) {
					$prorataMSG = $claimSlabWithDiscount['msg'];
					$prorataDiscount = $claimSlabWithDiscount['claimDiscount'];
				} else {
					$prorataMSG      = "";
					$prorataDiscount = "";
				}
				//	print_r($claimSlabWithDiscount);
				//	print_r($claimSlabWithDiscount);

			} 


			/******************** Calculating Battery Usage *******************************************/

			// Complaint No
			$data.='"'.ucwords(addslashes($auRec[$i]->complaint_no)).'"';
			$data.="\t";

			// Operator Name
			 $data.='"'.ucwords(addslashes($auRec[$i]->operator_name)).'"';
			 $data.="\t";

			// Complaint Date
			 $data.='"'.ucwords(addslashes($Complaint_date)).'"';
			 $data.="\t";

			 // BSN
			$data.='"'.ucwords(addslashes($auRec[$i]->pid)).'"';
			$data.="\t";

			// Battery Segment
			$data.='"'.ucwords(addslashes($auRec[$i]->segment_name)).'"';
			$data.="\t";

			// Item Code
			$data.='"'.ucwords(addslashes($auRec[$i]->item_code)).'"';
			$data.="\t";

			 // Battery Usage in Months
			 $data.='"'.ucwords(addslashes($usedBatteryInMonths.' Months/'.$usedBatteryInDays." Days")).'"';
			 $data.="\t";

			// Warranty in months
			$data.='"'.ucwords(addslashes($auRec[$i]->warranty_period)).'"';
			$data.="\t";			 

			// Prorata in months
			$data.='"'.ucwords(addslashes($auRec[$i]->warranty_prorata)).'"';
			$data.="\t";			 

			// Sale Date
			$data.='"'.ucwords(date('d-m-Y', strtotime(mysql_escape_string($auRec[$i]->date_of_sale)))).'"';
			$data.="\t";

			// Service Personnel Name
			 $data.='"'.ucwords(addslashes($auRec[$i]->service_distributor_sp_name)).'"';
			 $data.="\t";

			// Service Personnel Phone
			$data.='"'.addslashes($auRec[$i]->service_distributor_sp_phone_no).'"';
			$data.="\t";


			// Warehouse Name
			 $data.='"'.ucwords(addslashes($auRec[$i]->warehouse_name)).'"';
			 $data.="\t";

			 $RSINAME = $_objAdmin->_getSelectList2('table_service_personnel_warehouse_mapping as TSPWM left join table_service_personnel as SPNL on SPNL.service_personnel_id = TSPWM.service_personnel_id',' SPNL.sp_name','',' TSPWM.warehouse_id = "'.$auRec[$i]->WHSID.'"');

			 // RSI Name
			 $data.='"'.ucwords(addslashes($RSINAME[0]->sp_name)).'"';
			 $data.="\t";

			// Complaint Assigned Time
			$data.='"'.addslashes($auRec[$i]->assigned_sp_time).'"';
			$data.="\t";

			// Customer Name
			$data.='"'.ucwords(addslashes($auRec[$i]->customer_name)).'"';
			$data.="\t";

			//  Customer Phone
			$data.='"'.ucwords(addslashes($auRec[$i]->customer_phone_no)).'"';
			$data.="\t";

			// Customer City
			 $data.='"'.ucwords(addslashes($auRec[$i]->CUST_city)).'"';
			$data.="\t";

			// Customer Address
			 $data.='"'.ucwords(addslashes($auRec[$i]->CUST_ADDRESS)).'"';
			$data.="\t";			

			//Approver Comment
			$data.='"'.addslashes($auRec[$i]->approver_comments).'"';
			$data.="\t";

			// Comment
			$data.='"'.addslashes($auRec[$i]->remark).'"';
			$data.="\t";
			

			// Failure Mode
			$data.='"'.addslashes($auRec[$i]->failure_desc).'-'.addslashes($auRec[$i]->failure_code).'"';
			$data.="\t";

			// Auth Code
			$data.='"'.addslashes($auRec[$i]->auth_token).'"';
			$data.="\t";


			// Action Taken
			$data.='"'.addslashes($auRec[$i]->action_taken).'"';
			$data.="\t";

			// initial reading save time
			$data.='"'.addslashes($auRec[$i]->initial_reading_save_time).'"';
			$data.="\t";

			// Complaint Status
			$data.='"'.$bcf_status.'"';
			$data.="\t";
			

			// Call CLose Datetime
			$data.='"'.addslashes($auRec[$i]->call_close_datetime).'"';
			$data.="\t";

			$data.="\n";
								
			

		}	

	}

	header("Content-type: application/octet-stream");
	header("Content-Disposition: attachment; filename=\"asd_complaints.xls\"");		
	echo $data;
	exit;
}
else
{
	header("Location:distributor_wearhouse_comp.php");
	$_SESSION['sus'] = "Export is not Available";
}
}




/*********************************************************
* DESC: Export Running Complaints of ASD
* Author: Chirag@2017-06-27
*
*********************************************************/







/*********************************************************
* DESC: Export Completed Complaints from SP
* Author: Chirag@2017-06-13
*
*********************************************************/

	

	
if(isset($_GET['export_completed_complaintsSP'])){		

	$data = "Complaint Number\t Operator Name\t Complaint Date\t Service Distributor\t Service Distributor Code\t Warehouse Name \t RSI Name \t State\t Customer Name\t Customer Phone \t Customer Address\t Customer Email\t Battery Serial No\t Battery Segment\t Sale Date\t Service Distributor SP\t Service Distributor SP Phone\t Complaint Status\t Call Close Datetime\t Action Taken\t Failure Mode \t Auth Code\t In-Process TAT (in hours)\t TAT1\t TAT2\t Approver Comments \t Comment\n ";

			
	$condi=	" COMPLAINT.account_id = '".$_SESSION['accountId']."' and COMPLAINT.complain_status !='P' and COMPLAINT.assigned_sp_time !='0000-00-00 00:00:00' and BCF.service_distributor_sp_bcf_id IS NOT NULL ORDER BY COMPLAINT.created_date";


	
	$auRec=$_objAdmin->_getSelectList('table_complaint as COMPLAINT
		left join table_service_distributor as SD on SD.service_distributor_id = COMPLAINT.service_distributor_id
		left join table_customer as CUST on CUST.customer_id = COMPLAINT.customer_id
		left join table_item_bsn as BSN on BSN.bsn_id = COMPLAINT.bsn_id
		left join table_service_distributor_sp as SP on SP.service_distributor_sp_id = COMPLAINT.service_distributor_sp_id
		left join state as ST on ST.state_id = CUST.state
		left join table_service_distributor_sp_bcf as BCF on BCF.complaint_id = COMPLAINT.complaint_id
		left join table_failure_mode as FM on FM.failure_id = BCF.failure_mode
		left join table_item as ITEM on ITEM.item_id = BSN.item_id
		left join table_segment as SEGMENT on SEGMENT.segment_id = ITEM.item_segment
		left join table_account_admin as ADMIN on ADMIN.operator_id = COMPLAINT.call_center_id
		Left Join table_warehouse as WHS on WHS.warehouse_id = SD.warehouse_id',' COMPLAINT.*, SD.service_distributor_name,SD.service_distributor_code,CUST.customer_name,CUST.customer_phone_no,CUST.customer_email,CUST.customer_address,BSN.date_of_sale,SP.service_distributor_sp_name,SP.service_distributor_sp_phone_no,ST.state_name,BCF.action_taken,BCF.eapi_auth_code,BCF.under_process,BCF.status as BCFST,COMPLAINT.created_date as complaint_date,COMPLAINT.assigned_sp_time,BCF.call_close_datetime,ADMIN.operator_name,FM.failure_code,FM.failure_desc,BCF.approver_comments,SEGMENT.segment_name,WHS.warehouse_name, WHS.warehouse_id as WHSID','',$condi);

	if(is_array($auRec)){

		for($i=0;$i<count($auRec);$i++){

			$Sale_date=$_objAdmin->_changeDate($auRec[$i]->date_of_sale);
		
			$Complaint_date=$_objAdmin->_changeDate($auRec[$i]->complaint_date);


			if($auRec[$i]->complain_status=='R')
			{
				$sts = "Rejected";	
			}
			else if($auRec[$i]->complain_status=='I')
			{
				$sts = "In-Process";	
			}

			else if($auRec[$i]->complain_status=='P')
			{
				$sts = "Pending";	
			}
			else if($auRec[$i]->complain_status=='C')
			{
				$sts = "Complete";	
			}
			
			
			/****************** Calculating BCF Status ****************************************/
			if(($auRec[$i]->under_process=='U' && $auRec[$i]->BCFST=='R'))
			{
				$bcf_status = "Closed (U)";
			}
			else if($auRec[$i]->under_process == 'T' && $auRec[$i]->BCFST=='A') 
			{
				$bcf_status = "Closed (T)";
			}
			else if($auRec[$i]->under_process=='C' && $auRec[$i]->BCFST=='C')
			{
				$bcf_status = "Closed (C)";
			}
			else if($auRec[$i]->BCFST=='P' && $auRec[$i]->under_process=='F')
			{
				$bcf_status = "Final Inspection Complete (F)";
			}
			else if($auRec[$i]->under_process=='I' && $auRec[$i]->BCFST=='P')
			{
				$bcf_status = "Initial Inspection Complete (I)";
			}
			else if (($auRec[$i]->assigned_sp_time !='0000-00-00 00:00:00') && ($auRec[$i]->service_distributor_sp_bcf_id == NULL))
			{
				$bcf_status = "Pending at SE";
			}
			else
			{
				$bcf_status = "New (N)";
			}

			/****************** Calculating BCF Status ****************************************/


			/****************** Calculating IN-PROCESS TAT ****************************************/
			if($auRec[$i]->cctat1=='')
			{
				$calculated_pending='';
				$current_time = date("Y-m-d H:i:s");
				$calculated_pending = round((strtotime($current_time) - strtotime($auRec[$i]->complaint_date)) /3600);
			}
			else 
			{
				$calculated_pending ='-';	
			}

			/****************** Calculating IN-PROCESS TAT ****************************************/

		

			// Complaint No
			$data.='"'.ucwords(addslashes($auRec[$i]->complaint_no)).'"';
			$data.="\t";

			// Operator Name
			 $data.='"'.ucwords(addslashes($auRec[$i]->operator_name)).'"';
			 $data.="\t";

			// Complaint Date
			 $data.='"'.ucwords(addslashes($Complaint_date)).'"';
			 $data.="\t";


			 // Service Distributor
			 $data.='"'.ucwords(addslashes($auRec[$i]->service_distributor_name)).'"';
			 $data.="\t";


			 // Service Distributor Code
			 $data.='"'.ucwords(addslashes($auRec[$i]->service_distributor_code)).'"';
			 $data.="\t";


			 // Warehouse Name
			 $data.='"'.ucwords(addslashes($auRec[$i]->warehouse_name)).'"';
			 $data.="\t";

			 $RSINAME = $_objAdmin->_getSelectList2('table_service_personnel_warehouse_mapping as TSPWM left join table_service_personnel as SPNL on SPNL.service_personnel_id = TSPWM.service_personnel_id',' SPNL.sp_name','',' TSPWM.warehouse_id = "'.$auRec[$i]->WHSID.'"');

			 // RSI Name
			 $data.='"'.ucwords(addslashes($RSINAME[0]->sp_name)).'"';
			 $data.="\t";

			 // State
			$data.='"'.ucwords(addslashes($auRec[$i]->state_name)).'"';
			$data.="\t";

			// Customer Name
			$data.='"'.ucwords(addslashes($auRec[$i]->customer_name)).'"';
			$data.="\t";

			//  Customer Phone
			$data.='"'.ucwords(addslashes($auRec[$i]->customer_phone_no)).'"';
			$data.="\t";

			// Customer Address
			 $data.='"'.ucwords(addslashes($auRec[$i]->customer_address)).'"';
			$data.="\t";

			// Customer Email
			 $data.='"'.ucwords(addslashes($auRec[$i]->customer_email)).'"';
			$data.="\t";

			// BSN
			$data.='"'.ucwords(addslashes($auRec[$i]->product_serial_no)).'"';
			$data.="\t";

			// Battery Segment
			$data.='"'.ucwords(addslashes($auRec[$i]->segment_name)).'"';
			$data.="\t";

			// Sale Date
			$data.='"'.ucwords($auRec[$i]->date_of_sale).'"';
			$data.="\t";

			// Service Personnel Name
			 $data.='"'.ucwords(addslashes($auRec[$i]->service_distributor_sp_name)).'"';
			 $data.="\t";

			// Service Personnel Phone
			$data.='"'.addslashes($auRec[$i]->service_distributor_sp_phone_no).'"';
			$data.="\t";

			// BCF Status
			$data.='"'.addslashes($bcf_status).'"';
			$data.="\t";

			// Call Close Datetime
			$data.='"'.addslashes($auRec[$i]->call_close_datetime).'"';
			$data.="\t";

			// Action Taken
			$data.='"'.addslashes($auRec[$i]->action_taken).'"';
			$data.="\t";

			// Failure Mode
			$data.='"'.addslashes($auRec[$i]->failure_desc).'-'.addslashes($auRec[$i]->failure_code).'"';
			$data.="\t";

			// Auth Code
			$data.='"'.addslashes($auRec[$i]->eapi_auth_code).'"';
			$data.="\t";

			// In-Process TAT
			$data.='"'.addslashes($calculated_pending).'"';
			$data.="\t";

			// TAT-1
			 $data.='"'.addslashes($auRec[$i]->cctat1).'"';
			 $data.="\t";

			// TAT-2
			$data.='"'.addslashes($auRec[$i]->cctat2).'"';
			$data.="\t";

			//Approver Comment
			$data.='"'.addslashes($auRec[$i]->approver_comments).'"';
			$data.="\t";

			// Comment
			$data.='"'.addslashes($auRec[$i]->remark).'"';
			$data.="\t";

			$data.="\n";
								
			

		}	

	}

	header("Content-type: application/octet-stream");
	header("Content-Disposition: attachment; filename=\"completed_complaints.xls\"");		
	echo $data;
	exit;
}




/*********************************************************
* DESC: Export Completed Complaints from SP
* Author: Chirag@2017-06-13
*
*********************************************************/



/*********************************************************
* DESC: Export Completed Complaints By Approver
* Author: Chirag@2017-06-13
*
*********************************************************/

	

	
if(isset($_GET['export_completed_complaintsApprover'])){		

	$data = "Complaint Number\t Operator Name\t Complaint Date\t Service Distributor\t Service Distributor Code\t State\t Customer Name\t Customer Phone \t Customer Address\t Customer Email\t Battery Serial No\t Battery Segment \t Sale Date\t Service Distributor SP\t Service Distributor SP Phone\t Complaint Status\t Call Close Datetime\t Action Taken\t Failure Mode \t Auth Code\t In-Process TAT (in hours)\t TAT1\t TAT2\t Approver Comments\t Comment\n ";

			
	$condi=	" COMPLAINT.account_id = '".$_SESSION['accountId']."' and COMPLAINT.complain_status IN('I','R') and BCF.under_process IN ('T','U') and BCF.status IN ('A','R') ORDER BY COMPLAINT.created_date";


	
	$auRec=$_objAdmin->_getSelectList('table_complaint as COMPLAINT
		left join table_service_distributor as SD on SD.service_distributor_id = COMPLAINT.service_distributor_id
		left join table_customer as CUST on CUST.customer_id = COMPLAINT.customer_id
		left join table_item_bsn as BSN on BSN.bsn_id = COMPLAINT.bsn_id
		left join table_service_distributor_sp as SP on SP.service_distributor_sp_id = COMPLAINT.service_distributor_sp_id
		left join state as ST on ST.state_id = CUST.state
		left join table_service_distributor_sp_bcf as BCF on BCF.complaint_id = COMPLAINT.complaint_id
		left join table_failure_mode as FM on FM.failure_id = BCF.failure_mode
		left join table_item as ITEM on BSN.item_id = ITEM.item_id
		left join table_segment as SEGMENT on SEGMENT.segment_id = ITEM.item_segment
		left join table_account_admin as ADMIN on ADMIN.operator_id = COMPLAINT.call_center_id',' COMPLAINT.*, SD.service_distributor_name,SD.service_distributor_code,CUST.customer_name,CUST.customer_phone_no,CUST.customer_email,CUST.customer_address,BSN.date_of_sale,SP.service_distributor_sp_name,SP.service_distributor_sp_phone_no,ST.state_name,BCF.action_taken,BCF.eapi_auth_code,BCF.under_process,BCF.status as BCFST,COMPLAINT.created_date as complaint_date,COMPLAINT.assigned_sp_time,BCF.call_close_datetime,ADMIN.operator_name,FM.failure_code,FM.failure_desc,BCF.approver_comments,SEGMENT.segment_name','',$condi);

	if(is_array($auRec)){

		for($i=0;$i<count($auRec);$i++){

			$Sale_date=$_objAdmin->_changeDate($auRec[$i]->date_of_sale);
		
			$Complaint_date=$_objAdmin->_changeDate($auRec[$i]->complaint_date);


			if($auRec[$i]->complain_status=='R')
			{
				$sts = "Rejected";	
			}
			else if($auRec[$i]->complain_status=='I')
			{
				$sts = "In-Process";	
			}

			else if($auRec[$i]->complain_status=='P')
			{
				$sts = "Pending";	
			}
			else if($auRec[$i]->complain_status=='C')
			{
				$sts = "Complete";	
			}
			
			
			/****************** Calculating BCF Status ****************************************/
			if(($auRec[$i]->under_process=='U' && $auRec[$i]->BCFST=='R'))
			{
				$bcf_status = "Closed (U)";
			}
			else if($auRec[$i]->under_process == 'T' && $auRec[$i]->BCFST=='A') 
			{
				$bcf_status = "Closed (T)";
			}
			else if($auRec[$i]->under_process=='C' && $auRec[$i]->BCFST=='C')
			{
				$bcf_status = "Closed (C)";
			}
			else if($auRec[$i]->BCFST=='P' && $auRec[$i]->under_process=='F')
			{
				$bcf_status = "Final Inspection Complete (F)";
			}
			else if($auRec[$i]->under_process=='I' && $auRec[$i]->BCFST=='P')
			{
				$bcf_status = "Initial Inspection Complete (I)";
			}
			else if (($auRec[$i]->assigned_sp_time !='0000-00-00 00:00:00') && ($auRec[$i]->service_distributor_sp_bcf_id == NULL))
			{
				$bcf_status = "Pending at SE";
			}
			else
			{
				$bcf_status = "New (N)";
			}

			/****************** Calculating BCF Status ****************************************/


			/****************** Calculating IN-PROCESS TAT ****************************************/
			if($auRec[$i]->cctat1=='')
			{
				$calculated_pending='';
				$current_time = date("Y-m-d H:i:s");
				$calculated_pending = round((strtotime($current_time) - strtotime($auRec[$i]->complaint_date)) /3600);
			}
			else 
			{
				$calculated_pending ='-';	
			}

			/****************** Calculating IN-PROCESS TAT ****************************************/
		


			// Complaint No
			$data.='"'.ucwords(addslashes($auRec[$i]->complaint_no)).'"';
			$data.="\t";


			// Operator Name
			$data.='"'.ucwords(addslashes($auRec[$i]->operator_name)).'"';
			$data.="\t";

			// Complaint Date
			 $data.='"'.ucwords(addslashes($Complaint_date)).'"';
			 $data.="\t";


			 // Service Distributor
			 $data.='"'.ucwords(addslashes($auRec[$i]->service_distributor_name)).'"';
			 $data.="\t";


			 // Service Distributor Code
			 $data.='"'.ucwords(addslashes($auRec[$i]->service_distributor_code)).'"';
			 $data.="\t";

			 // State
			$data.='"'.ucwords(addslashes($auRec[$i]->state_name)).'"';
			$data.="\t";

			// Customer Name
			$data.='"'.ucwords(addslashes($auRec[$i]->customer_name)).'"';
			$data.="\t";

			//  Customer Phone
			$data.='"'.ucwords(addslashes($auRec[$i]->customer_phone_no)).'"';
			$data.="\t";

			// Customer Address
			 $data.='"'.ucwords(addslashes($auRec[$i]->customer_address)).'"';
			$data.="\t";

			// Customer Email
			 $data.='"'.ucwords(addslashes($auRec[$i]->customer_email)).'"';
			$data.="\t";

			// BSN
			$data.='"'.ucwords(addslashes($auRec[$i]->product_serial_no)).'"';
			$data.="\t";

			// Battery Segment
			$data.='"'.ucwords(addslashes($auRec[$i]->segment_name)).'"';
			$data.="\t";

			// Sale Date
			$data.='"'.ucwords($auRec[$i]->date_of_sale).'"';
			$data.="\t";

			// Service Personnel Name
			 $data.='"'.ucwords(addslashes($auRec[$i]->service_distributor_sp_name)).'"';
			 $data.="\t";

			// Service Personnel Phone
			$data.='"'.addslashes($auRec[$i]->service_distributor_sp_phone_no).'"';
			$data.="\t";

			// BCF Status
			$data.='"'.addslashes($bcf_status).'"';
			$data.="\t";


			// Call Close Datetime
			$data.='"'.addslashes($auRec[$i]->call_close_datetime).'"';
			$data.="\t";

			// Failure Mode
			$data.='"'.addslashes($auRec[$i]->failure_desc).'-'.addslashes($auRec[$i]->failure_code).'"';
			$data.="\t";

			// Action Taken
			$data.='"'.addslashes($auRec[$i]->action_taken).'"';
			$data.="\t";


			// Auth Code
			$data.='"'.addslashes($auRec[$i]->eapi_auth_code).'"';
			$data.="\t";

			// In-Process TAT
			$data.='"'.addslashes($calculated_pending).'"';
			$data.="\t";

			// TAT-1
			 $data.='"'.addslashes($auRec[$i]->cctat1).'"';
			 $data.="\t";

			// TAT-2
			$data.='"'.addslashes($auRec[$i]->cctat2).'"';
			$data.="\t";

			// Approver Comments
			$data.='"'.addslashes($auRec[$i]->approver_comments).'"';
			$data.="\t";

			// Comment
			$data.='"'.addslashes($auRec[$i]->remark).'"';
			$data.="\t";

			$data.="\n";
								
			

		}	

	}

	header("Content-type: application/octet-stream");
	header("Content-Disposition: attachment; filename=\"completed_complaints_approver.xls\"");		
	echo $data;
	exit;
}




/*********************************************************
* DESC: Export Completed Complaints By Approver
* Author: Chirag@2017-06-13
*
*********************************************************/

/*******
* Created By : Manidner kumar
* Date 13-06-17
* Desc : Export Order Sku Order report
******/
if(isset($_GET['export_sku_order_list'])){	
		$data = "Salesperson Code\t Salesman Name\t Reporting To\t Brand\t Category\t Segment \t Dealer Name\t Dealer Code\t Dealer Address\t  City\t District\t Dealer Pincode\t State\t  Distributor Code\t Distributor Name\t City\t Order Status\t No Order Reason\t No Order Comments\t Order Number\t  Date of Order\t Time of Order\t Sku Code\t Quantity \t Updated Quantity ";


		$sort = " ORDER BY date_of_order desc,time_of_order desc";
		if($_SESSION['FromOrderList']!=''){
			$fromdate=" and o.date_of_order >= '".date('Y-m-d', strtotime($_SESSION['FromOrderList']))."'";
		} else {
			$fromdate=" and o.date_of_order >= '".date('Y-m-d')."'";
		}
		
		$divisionCondition = "";
		  if($_SESSION['customerType']!=''){
		   if($_SESSION['customerType']==1){ // retailer
			$cust_type=" AND o.retailer_id!=0";
			if($_SESSION['customerClass']!='' && $_SESSION['customerClass']!='all'){
				$class_type = " AND r.relationship_id='".$_SESSION['customerClass']."'";
			}else{
				$class_type = "";
			}
			if($_SESSION['divisionCond']!='' && $_SESSION['divisionCond']!='all'){
				$divisionCondition = " AND r.division_id='".$_SESSION['divisionCond']."'";
			}else{
				$divisionCondition = "";
			}
			
		   }
		   else if($_SESSION['customerType']==2){ // distributor
		   	$cust_type=" AND o.retailer_id=0";
		   	if($_SESSION['customerClass']!='' && $_SESSION['customerClass']!='all'){
				$class_type = " AND d.relationship_id='".$_SESSION['customerClass']."'";
			}else{
				$class_type = "";
			}
			if($_SESSION['divisionCond']!='' && $_SESSION['divisionCond']!='all'){
				$divisionCondition = " AND d.division_id='".$_SESSION['divisionCond']."'";
			}else{
				$divisionCondition = "";
			}

		   } else {
		   	$cust_type = "";
		   	$class_type = "";
		   		$divisionCondition = "";
		   }
		  }
		  else
		  {
		   $cust_type = "";
		   $class_type = "";
		   	$divisionCondition = "";
		  }
		//for order
		if($_SESSION['orderStatus']!=''){
			if($_SESSION['orderStatus']==1){
				$odr_type=" AND o.order_type='Yes' AND o.order_status='A' ";
			} else if($_SESSION['orderStatus']==2){
				$odr_type=" AND o.order_type='No' AND o.order_status='A' ";
			} else if($_SESSION['orderStatus']==3){
				$odr_type=" AND o.order_status='I' ";
			} else if($_SESSION['orderStatus']==4){
				$odr_type=" AND o.order_status='D' ";
			} else if($_SESSION['orderStatus']==5){
				$odr_type=" AND o.order_type='Adhoc' AND o.order_status='A' ";
			} else {
				$odr_type = "";	
			}
		} else {
			$odr_type = "";
		}

		if($_SESSION['ToOrderList']!=''){
			$todate=" and o.date_of_order <= '".date('Y-m-d', strtotime($_SESSION['ToOrderList']))."'";
		} else {
			$todate=" and o.date_of_order <= '".date('Y-m-d')."'";
		}
		
		if($_SESSION['OrderBy']!=''){
			if($_SESSION['OrderBy']==1){
				$orderby="";
			}
			if($_SESSION['OrderBy']==2){
				$orderby=" and o.order_type='Adhoc' ";
			}
			if($_SESSION['OrderBy']==3){
				$orderby=" and o.order_type!='Adhoc' ";
			}
		} else {
			$orderby="";
		}
		if( $_SESSION['userLoginType']==3){
			$disLogCond="o.distributor_id='".$_SESSION['distributorId']."' ";
		}

		// AJAY@2016-04-08
		  /*if($_SESSION['SalOrderList']!=''){
			   $salesman=" and s.salesman_id='".$_SESSION['SalOrderList']."'";
			  } else  if($_SESSION['userLoginType']==1){
				 $salesman = "";
			  } */

		if(isset($_SESSION['SalOrderList']) && $_SESSION['SalOrderList']!='' && $_SESSION['orderfilterBY'] == 1)
	    {

		  	$salesman=" and s.salesman_id='".$_SESSION['SalOrderList']."'"; 

		}else if(isset($_SESSION['SalOrderList']) && $_SESSION['SalOrderList']!='' && $_SESSION['orderfilterBY'] == 2)
		{

		  	$salArrList=$_SESSION['SalOrderList'];	
		  	$filterby=$_SESSION['orderfilterBY'];
		  	$salesman = $_objArrayList->getSalesmanConsolidatedList($salArrList,$filterby);

		}else if($_SESSION['userLoginType']==1){
		  	$salesman = "";
		}


			  $stateCondition = "";
			  $cityCondition = "";
			  $talukaCondition = "";

			if(isset($_SESSION['stateCust']) && $_SESSION['stateCust']!="all"){
			    $stateCondition = " AND (r.state IN(".$_SESSION['stateCust'].") OR d.state IN(".$_SESSION['stateCust']."))";
			}

			if(isset($_SESSION['districtCust']) && $_SESSION['districtCust']!="all"){
			    $cityCondition = " AND (r.city IN(".$_SESSION['districtCust'].") OR d.city IN(".$_SESSION['districtCust']."))";
			}


			if(isset($_SESSION['tehsilCust']) && $_SESSION['tehsilCust']!="all"){
			    $talukaCondition = " AND ( r.taluka_id IN(".$_SESSION['tehsilCust'].") OR d.taluka_id IN(".$_SESSION['tehsilCust']."))";
		}

		if(isset($_SESSION['divisionCond']) && $_SESSION['divisionCond']!="all" && $_SESSION['customerType'] == 0){
    		$divisionCondition = " AND ( r.division_id IN(".$_SESSION['divisionCond'].") OR d.division_id IN(".$_SESSION['divisionCond']."))";
		}//fileter added by Maninder@2016-08-25
		//$where = " 1=1 ";
		//$where = " AND (r.new = '' ||  d.new = '') $disLogCond $cust_type $class_type $stateCondition $cityCondition $talukaCondition $odr_type $salesman $fromdate $todate $orderby and o.account_id =".$_SESSION['accountId'];

		if($_SESSION['userLoginType'] == 2)
		{
		      //	echo"usertow";
		          // $where = " AND (r.new = '' ||  d.new = '') $disLogCond $cust_type $class_type $stateCondition
		          //  $cityCondition $talukaCondition $divisionCondition $odr_type $salesman $fromdate $todate $orderby
		          //   and o.account_id =".$_SESSION['accountId']." AND o.salesman_order_status='A' ";


		            // $where = " AND (r.new = '' ||  d.new = '') $disLogCond $cust_type $class_type $stateCondition
		            // $cityCondition $talukaCondition $divisionCondition  $fromdate $todate $orderby
		            // and o.account_id =".$_SESSION['accountId']." AND ((o.salesman_order_status='A')
		            // 	OR(o.salesman_order_status='U' And o.distributor_id!=0)) ";

			$where = " AND (r.new = '' ||  d.new = '') $disLogCond $cust_type $class_type $stateCondition
			$cityCondition $talukaCondition $divisionCondition  $fromdate $todate $orderby
			AND o.salesman_order_status='A' ";  

		           //print_R($where);exit;

		}
		else if($_SESSION['userLoginType'] == 4)
		{
			$where = " AND (r.new = '' ||  d.new = '')AND $disLogCond $cust_type $class_type $stateCondition $cityCondition $talukaCondition $divisionCondition $fromdate $todate $orderby
			AND o.salesman_order_status='U' "; 

		}
		else if($_SESSION['userLoginType'] == 3)
		{
			$where = " AND (r.new = '' ||  d.new = '')AND $disLogCond $cust_type $class_type $stateCondition $cityCondition $talukaCondition $divisionCondition $fromdate $todate $orderby
			AND o.salesman_order_status='U' "; 

		}
		else if($_SESSION['userLoginType'] == 7){

			$wearhouse=$_objAdmin->_getSelectList2('table_order_invoice_assign_wh AS wh 
				INNER JOIN table_warehouse AS w ON w.warehouse_id = wh.warhouse_id' ,
				'wh.warhouse_id as wid ,wh.order_id as oid',''," wh.warhouse_id =".$_SESSION['warehouseId']); 



			$ord_id=array();
			$wear_id=array();
			foreach($wearhouse as $wearhouse_details)
			{
				$wear_id=$wearhouse_details->id;
				$ord_id[]=$wearhouse_details->oid;

			}
			$matches = implode(',', $ord_id);

		           // if(!empty($matches) && $matches!="")
		           // {


			$where = " AND (r.new = '' ||  d.new = '') $disLogCond $cust_type $class_type $stateCondition
			$cityCondition $talukaCondition $divisionCondition $odr_type $salesman $fromdate $todate $orderby AND o.invoice_commercial_approval_status='A' AND o.order_id IN (".$matches.")"; 
		      //print_R($where);
		      //}
		}
		else{
		  //$where = " 1=1 "; 

			$where = " AND (r.new = '' ||  d.new = '') $disLogCond $cust_type $class_type $stateCondition $cityCondition $talukaCondition $divisionCondition $odr_type $salesman $fromdate $todate $orderby"; 
		}

		if($_SESSION['userLoginType']==1){

			$auRec=$_objAdmin->_getSelectList('table_order as o 
			left join table_order_detail as od on o.order_id = od.order_id 
			left join table_item as i on i.item_id = od.item_id 
			left join table_brands as br on br.brand_id = i.brand_id 
			left join table_segment as SEG on SEG.segment_id = i.item_segment
			left join table_category as CAT on CAT.category_id = i.category_id
			left join table_sku as sku on sku.sku_id = i.sku_id  
			left join table_retailer as r on o.retailer_id=r.retailer_id 
			left join table_salesman as s on o.salesman_id=s.salesman_id 
			left join table_distributors as d on o.distributor_id=d.distributor_id AND o.so_commercial_approval_status="A" AND o.retailer_id=0  
			left join table_retailer_channel_master as cm on r.channel_id=cm.channel_id 
			LEFT JOIN table_relationship AS REL ON REL.relationship_id = r.relationship_id
	        LEFT JOIN table_relationship AS RELD ON RELD.relationship_id = d.relationship_id
			left join table_markets as m on m.market_id=r.market_id',
			"o.*,r.retailer_name,r.retailer_code,r.zipcode AS retailer_pincode,r.retailer_address,r.retailer_location,r.lat as retlat,r.survey_status as r_survey_status,cm.channel_name,r.display_outlet,r.lng as retlng,s.salesman_name,s.salesman_code,d.distributor_id,d.distributor_name,d.distributor_code,d.zipcode AS distributor_pincode,d.survey_status as d_survey_status,m.market_name,REL.relationship_desc AS retailer_class, RELD.relationship_desc AS distributor_class,r.state as r_state,d.state as d_state,r.city as r_city,d.city as d_city,sku.sku_name,br.brand_name,od.quantity,od.acc_quantity,r.taluka_id,i.item_code,CAT.category_name,SEG.segment_name",$rp, $where.$sort,'');

			$auRecCount=$_objAdmin->_getSelectList('table_order as o 
			left join table_retailer as r on o.retailer_id=r.retailer_id 
			left join table_salesman as s on o.salesman_id=s.salesman_id 
			left join table_distributors as d on o.distributor_id=d.distributor_id AND o.so_commercial_approval_status="A" AND o.retailer_id=0 
			left join table_retailer_channel_master as cm on r.channel_id=cm.channel_id 
			left join table_markets as m on m.market_id=r.market_id',
			'count(*) as total','',$where);

		}else{

			$auRec=$_objAdmin->_getSelectList('table_order as o 
			left join table_order_detail as od on o.order_id = od.order_id 
			left join table_item as i on i.item_id = od.item_id 
			left join table_brands as br on br.brand_id = i.brand_id 
			left join table_sku as sku on sku.sku_id = i.sku_id  
			left join table_retailer as r on o.retailer_id=r.retailer_id and r.new=""  
			left join table_salesman as s on o.salesman_id=s.salesman_id 
			left join table_distributors as d on o.distributor_id=d.distributor_id and d.new=""  
			left join table_retailer_channel_master as cm on r.channel_id=cm.channel_id 
			LEFT JOIN table_relationship AS REL ON REL.relationship_id = r.relationship_id
	        LEFT JOIN table_relationship AS RELD ON RELD.relationship_id = d.relationship_id
			left join table_markets as m on m.market_id=r.market_id',
			"o.*,r.retailer_name,r.retailer_code,r.zipcode AS retailer_pincode,r.retailer_address,r.retailer_location,r.lat as retlat,r.survey_status as r_survey_status,cm.channel_name,r.display_outlet,r.lng as retlng,s.salesman_name,s.salesman_code,d.distributor_id,d.distributor_name,d.distributor_code,d.zipcode AS distributor_pincode,d.survey_status as d_survey_status,m.market_name,REL.relationship_desc AS retailer_class, RELD.relationship_desc AS distributor_class,r.state as r_state,d.state as d_state,r.city as r_city,d.city as d_city,sku.sku_name,br.brand_name,od.quantity,od.acc_quantity,r.taluka_id,i.item_code",$rp, $where.$sort,'');

			$auRecCount=$_objAdmin->_getSelectList('table_order as o 
			left join table_retailer as r on o.retailer_id=r.retailer_id and r.new="" 
			left join table_salesman as s on o.salesman_id=s.salesman_id 
			left join table_distributors as d on o.distributor_id=d.distributor_id and d.new="" 
			left join table_retailer_channel_master as cm on r.channel_id=cm.channel_id 
			left join table_markets as m on m.market_id=r.market_id',
			'count(*) as total','',$where);
		}
		

		$data.="\n";
		for($i=0;$i<count($auRec);$i++){
			if($auRec[$i]->order_type=='Yes'){
				if($auRec[$i]->order_status=='A'){
					$status="New Order";
				}
				if($auRec[$i]->order_status=='I'){
					$status="Processed";
				}
				if($auRec[$i]->order_status=='D'){
					$status="Dispatched";
				}
				//$color=($auRec[$i]->order_status=='A')?"A":"P";
			}
			if($auRec[$i]->order_type=='No'){   
   				$status=($auRec[$i]->order_status=='A')?"No Order":"Processed";
   				//$color=($auRec[$i]->order_status=='A')?"I":"P";
			}
			if($auRec[$i]->order_type=='Adhoc'){
				if($auRec[$i]->order_status=='A'){
					//$status="New Adhoc Order";
					$status="Unplanned Order";
				}
				if($auRec[$i]->order_status=='I'){
					$status="Processed";
				}
				if($auRec[$i]->order_status=='D'){
					$status="Dispatched";
				}
			}
			if(strtolower($auRec[$i]->order_type)== 'cs'){
			   if($auRec[$i]->order_status=='D'){
			    $status="Counter Sale";
			   }
			}
   			$auRec2=$_objAdmin->_getSelectList2('table_order_detail',"count( distinct(item_id)) as total_item",''," order_id = ".$auRec[$i]->order_id." and type = 1");
   			$uType = "";
   			$stateN = "";
   			$cityN = "";
			if($auRec[$i]->retailer_id == 0 || $auRec[$i]->retailer_id== "") {
				$uType = "Distributor ";
				$ret_name= $auRec[$i]->distributor_name;
				$auMarker=$_objAdmin->_getSelectList2('table_survey'," lat as rlat,lng as rlng,accuracy_level,network_mode",''," distributor_id='".$auRec[$i]->distributor_id."' order by survey_date desc,survey_time desc limit 0,1");
				$survey_status = ($auRec[$i]->d_survey_status=='I')?"Not Done":"Done";


				if($auRec[$i]->d_state > 0){
					$stateN = $_objAdmin->_getSelectList2('state',"state_name",'','state_id='.$auRec[$i]->d_state);
				}

				if($auRec[$i]->d_city > 0){
					$cityN = $_objAdmin->_getSelectList2('city',"city_name as d_city",'',"city_id='".$auRec[$i]->d_city."'"); 
				}
						 		 
		 		
			} else {
				$uType  = "Retailer ";
				$ret_name= $auRec[$i]->retailer_name;
				$auMarker=$_objAdmin->_getSelectList2('table_survey ',"lat as rlat,lng as rlng,accuracy_level,network_mode",''," retailer_id='".$auRec[$i]->retailer_id."' order by survey_date desc,survey_time desc limit 0,1");
				$survey_status = ($auRec[$i]->r_survey_status=='I')?"Not Done":"Done";

				if($auRec[$i]->r_state > 0){
					$stateN = $_objAdmin->_getSelectList2('state',"state_name",'','state_id='.$auRec[$i]->r_state);
				}

				if($auRec[$i]->r_city > 0){
					$cityN = $_objAdmin->_getSelectList2('city',"city_name",'',"city_id='".$auRec[$i]->r_city."'"); 
				}
				if($auRec[$i]->r_taluka > 0){
					$talukaN = $_objAdmin->_getSelectList2('table_taluka',"taluka_name",'',"taluka_id='".$auRec[$i]->r_taluka."'"); 
				}
				 
		 		
			}
			$center_lat ="";	
			$center_lng ="";
			$accuracy_level = "";
			$network_mode ="";
			$radius = 0.05;
			if($auMarker[0]->rlat>0){
				$center_lat =$auMarker[0]->rlat;
				$center_lng =$auMarker[0]->rlng;
				$accuracy_level=$auMarker[0]->accuracy_level;
				$network_mode=$auMarker[0]->network_mode;
				$query = sprintf("SELECT order_id, lat, lng, ( 6371 * acos( cos( radians('%s') ) * cos( radians( lat ) ) * cos( radians( lng ) - radians('%s') ) + sin( radians('%s') ) * sin( radians( lat ) ) ) ) AS distance FROM table_order where order_id='".$auRec[$i]->order_id."'", mysql_real_escape_string($center_lat), mysql_real_escape_string($center_lng), mysql_real_escape_string($center_lat), mysql_real_escape_string($radius));
				$result = mysql_query($query);
				$row = @mysql_fetch_assoc($result);
				if($row['distance'] <= $radius){
					$visit_status = "Visited";
				} else {
					$visit_status = "Not Visited";
				}
			} else {
				$visit_status = "Not Visited";
			} 
   			$date_of_order=$_objAdmin->_changeDate($auRec[$i]->date_of_order);   
			$salDetails = $_objAdmin->getSalesmanFullDetails($auRec[$i]->salesman_id);
			$remove = array("\n","\r","\t");
			$remove1 = array(",","-",".");
			$data.=""
			.$auRec[$i]->salesman_code. 
			"\t".$auRec[$i]->salesman_name.
			"\t".$salDetails[0]->rpt_to. 
			
			"\t".$auRec[$i]->brand_name.
			"\t".$auRec[$i]->category_name.
			"\t".$auRec[$i]->segment_name.
			"\t".$auRec[$i]->retailer_name.
			"\t".$auRec[$i]->retailer_code. 
			"\t".str_replace($remove1, ' ',addslashes($auRec[$i]->retailer_address)). 
			"\t".$talukaN[0]->taluka_name.			 
			"\t".$cityN[0]->city_name.
			
			"\t".$auRec[$i]->retailer_pincode.
			"\t".$stateN[0]->state_name.
			"\t".$auRec[$i]->distributor_code. 
			"\t".$auRec[$i]->distributor_name. 
			"\t".$cityN[0]->d_city. 
			"\t".$status.
			"\t".$auRec[$i]->tag_description.
			"\t".$auRec[$i]->comments.
			"\t".$auRec[$i]->order_id. 
			"\t".$date_of_order. 
			"\t".$auRec[$i]->time_of_order.
			 
			"\t".$auRec[$i]->item_code.
			"\t".$auRec[$i]->quantity.
			"\t".$auRec[$i]->acc_quantity.
			 
			
			 
			"\n";
		}	
			 
	header("Content-type: application/octet-stream");
	header("Content-Disposition: attachment; filename=\"Order List Sku wise.xls\"");		
	echo $data;
	exit;
	}



/*******************************************************
* DESC: Export Destributor  DETAIL
* Author: Arvind@2017-06-15
*
*
*
********************************************************/	


	 if(isset($_GET['action']) && $_GET['action'] == 'export_distributor_stock_report' && isset($_GET['sid']) && $_GET['sid']>0)   {	
	//echo hello;exit;
	
	// $data=" Distributor Name\t Distributor Code\t BSN Status \t BSN\t Item Code\t Item Name\t Category Name\t Manufacture Date\t Warehouse Dispatched Date\n";	 
	 	$data=" Distributor Name\t Distributor Code\t BSN Status \t BSN\t Item Code\t Item Name\t Category Name\n "; 	 
		
	$itemCondition = "";
		if(isset($_SESSION['itemDisId']) && $_SESSION['itemDisId']>0) {
			$itemCondition = " AND ds.item_id='".$_SESSION['itemDisId']."' ";
		}
		

		$where = " ds.distributor_id='".$_GET['sid']."' $itemCondition";
		$where .="And ds.status='A'";
		$dis="";

		$auRec=$_objAdmin->_getSelectList2('table_item_distributor_stock AS ds 
				LEFT JOIN table_distributors AS d ON d.distributor_id = ds.distributor_id
				LEFT JOIN table_item_bsn AS i ON i.bsn_id = ds.bsn_id
				LEFT JOIN table_category AS c ON c.category_id = ds.category_id
				LEFT JOIN table_item AS item ON item.item_id = ds.item_id',"d.distributor_name as distributor_name ,d.distributor_code as distributor_code,ds.status as status,i.bsn as bsn,item.item_code as item_code,item.item_name as item_name,c.category_name as category_name",$rp,$where,'');



	     for($i=0;$i<count($auRec);$i++){			
				
				$remove = array("\n", "\r\n", "\r");
				//$last_update_datetime=$_objAdmin->_changeDate($auRec[$i]->last_update_datetime);
				$remove = array(",", "-", "_");

			$rechargedDate =($auRec[$i]->recharged_date=='0000-00-00')?"":$auRec[$i]->recharged_date;

			$stockObj = new StockClass();
			//$batteryAge = $stockObj->getBatteryAgeing($auRec[$i]->manufacture_date, $auRec[$i]->recharged_date, $auRec[$i]->distributor_dispatch_date);
			$status = $stockObj->getBatteryStockStatus($auRec[$i]->status);


			$data.='"'.ucwords($auRec[$i]->distributor_name).'"';
			$data.="\t";

			$data.='"'.ucwords($auRec[$i]->distributor_code).'"';
			$data.="\t";

			$data.='"'.$status.'"';
			$data.="\t";


		
			$data.='"'.$auRec[$i]->bsn.'"';
			$data.="\t";

			$data.='"'.ucwords($auRec[$i]->item_code).'"';
			$data.="\t";

			$data.='"'.ucwords($auRec[$i]->item_name).'"';
			$data.="\t";

			
			$data.='"'.ucwords($auRec[$i]->category_name).'"';
			$data.="\n";

		}
			 
	header("Content-type: application/octet-stream");
	header("Content-Disposition: attachment; filename=\"Distributor STOCK Report.xls\"");		
	echo $data;
	exit;
	}




/********************************** Export ASD Report Arvind (10 july 2017)***************************/


if(isset($_GET['export_ASD_timeline'])){

	$year=$_REQUEST['year'];
	$month=$_REQUEST['month'];

	$dis_service_id=$_REQUEST['sal'];
	$date_is=date('Y-m-d', strtotime($_REQUEST['date']));


	//print_R($date_is);
function latlongadd($address){

$geo = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?address='.urlencode($address).'&sensor=false');


$geo = json_decode($geo, true);

if ($geo['status'] == 'OK') {
  
  $latitude = $geo['results'][0]['geometry']['location']['lat'];
  $longitude = $geo['results'][0]['geometry']['location']['lng'];
  $returnadd=array($latitude,$longitude);
 
  return $returnadd;
}
}

function distance($lat1, $lon1, $lat2, $lon2, $unit) {

  $theta = $lon1 - $lon2;
  $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
  $dist = acos($dist);
  $dist = rad2deg($dist);
  $miles = $dist * 60 * 1.1515;
  $unit = strtoupper($unit);

  if ($unit == "K") {
    return ($miles * 1.6093);
  } else if ($unit == "N") {
      return ($miles * 0.8684);
    } else {
        return $miles;
      }
}

	
	$aSal=$_objAdmin->_getSelectList('table_service_distributor AS s','*',''," status='A' and service_distributor_name='".$dis_service_id."'"); 
	$name=$aSal[0]->service_distributor_name;



	$data="Eapl Complaint no \t call Allocation(date/time) \t Customer Location \t ASD Distance(km) \t ASD Basic Payout \t ASD TAT Incentive \t ASD Revival  \t Battery Model \t Battery serial no \t Action taken \t Tat1 \t Tat2 \n";

	// $data="Eapl Complaint no , call Allocation(date/time) , Customer Location , ASD Distance(km), ASD Basic Payout, ASD TAT Incentive, ASD Revival , Battery Model , Battery serial no , Action taken , Tat1 , Tat2 \r\n"; // For CSV Output
	//$data="Salesman Name \t Date \t Route Name \t Number Of Retailer \t Number Of Distributor\n";
	

	if(!empty($_REQUEST['sal'])) {	
	
	//$first_day_this_month = date('m-01-Y'); // hard-coded '01' for first day
   // $last_day_this_month  = date('m-t-Y');

  //$did=" AND date(c.created_date) >='".$first_day_this_month."' And date(c.created_date)<='". $last_day_this_month."' ";
               $did="  AND  year(c.created_date) ='".$_REQUEST['year']."' And MONTH(c.created_date)='". $_REQUEST['month']."' and c.service_distributor_id = '".$_REQUEST['sal']."'";



                      $auRec=$_objAdmin->_getSelectList('table_complaint as c 
                left join table_service_distributor_sp_bcf as sb on sb.complaint_id=c.complaint_id 
                left join table_customer as cust on cust.customer_id=c. customer_id
                left join table_service_distributor as s on s.service_distributor_id=c.service_distributor_id 
                ','c.complaint_id as complaint_id,c.*,cust.customer_address as customer_address,cust.customer_id as customer_id , cust.lat as clat,cust.lng as clng ,s.lat as lat,s.lng as lng,sb.action_taken as action_taken,sb.call_close_datetime as call_close_datetime,sb.initial_reading_save_time as initial_reading_save_time,cust.state as state,sb.asd_distance,sb.asd_basic_payout,sb.asd_tat_incentive,sb.asd_payout_revival',''," $did ORDER BY s.service_distributor_id ASC"); 
                   // print_R($auRec);


	if(is_array($auRec)){
		
		for($i=0;$i<count($auRec);$i++){

		  $customer_state=$auRec[$i]->state;
	      $statename=$_objAdmin->_getSelectList2('state',"state_name",''," state_id='".$customer_state."' ");
	
 
	        $lat=$auRec[$i]->lat;
		    $lng=$auRec[$i]->lng;
		    $lat2=$auRec[$i]->clat;
		    $lng2=$auRec[$i]->clng;
		 //    if(($lat2<=0 || $lat2=='')|| ($lng2<=0 || $lng2=='') || ($lat<=0 || $lat==0)|| $lng<=0)
			// {$asd_distance="--";}
		 //     else
		 //     {
		 //   	     // $asd_distance= distance($lat,$lng, $lat2, $lng2, "K") . " Km<br>";
   //         //       $asd_distance=round($asd_distance, 2);
		 //       }
				if($auRec[$i]->cctat1< 0 || $auRec[$i]->cctat1=='')
				{
					$cctat1=0;
				}
				else{$cctat1=$auRec[$i]->cctat1;}

					if($auRec[$i]->cctat2 =='' || $auRec[$i]->cctat2 < 0)
				{
					$cctat2=0;
				}else{$cctat2=$auRec[$i]->cctat2;}
		
		
			
		  $customer_address= str_replace(",", "", $auRec[$i]->customer_address);

	
  		  $data.="".$auRec[$i]->complaint_no."\t".$auRec[$i]->created_date."\t".$customer_address."\t".$auRec[$i]->asd_distance."\t".$auRec[$i]->asd_basic_payout."\t".$auRec[$i]->asd_tat_incentive."\t".$auRec[$i]->asd_payout_revival."\t".$auRec[$i]->battery_type."\t".$auRec[$i]->product_serial_no."\t".$auRec[$i]->action_taken."\t".$cctat1."\t".$cctat2."\n";


		  // $data.="".$auRec[$i]->complaint_no.",".$auRec[$i]->created_date.",".$customer_address.",".$asd_distance.",".$auRec[$i]->battery_type.",".$auRec[$i]->product_serial_no.",".$auRec[$i]->action_taken.",".$cctat1.",".$cctat2."\r\n"; // For CSV Output
		
		}
					
	}
}

/*********** FOR CSV Output  *****************/
	// print_r($data);
	// $fp = fopen('file.csv', 'w');

	// foreach ($data as $fields) {
	//     fputcsv($fp, $fields);
	// }

	// fclose($fp);
	// header( 'Content-Type: text/csv' );
 	// header( 'Content-Disposition: attachment;filename=file.csv');
/*********** FOR CSV Output  *****************/
	header("Content-type: application/octet-stream");
	header("Content-Disposition: attachment; filename=\"ASD_Payout_Report.xls\"");		
	echo $data;
	exit;
	
	}


	
//productivity_report for salesman 13-07-2107


	if(isset($_GET['export_productivity_report'])){

		if(isset($_SESSION['showPlannedVsVisited']) && $_SESSION['salesman']!='')
		{
			$salArrList=$_SESSION['salesman'];	
			$filterby=$_SESSION['orderfilterBY'];
			$salesman = $_objArrayList->getSalesmanConsolidatedList($salArrList,$filterby);
			
		} 

		// $year=$_REQUEST['year'];
		// $month=$_REQUEST['month'];
		//echo hello;exit;
	
	// $data=" Distributor Name\t Distributor Code\t BSN Status \t BSN\t Item Code\t Item Name\t Category Name\t Manufacture Date\t Warehouse Dispatched Date\n";	 
	 	$data=" Salesman Name\t Salesaman Code\t Reporting To \t Brand\t Dealer Name\t Dealer Contact No\t Dealer Address\t City\t State\t Distributor Name\t Actual Visit\t Segment\t Total Order Qty\t Total Invoice Amt\t Outstaning Amt Name\t Order Type \n ";	 
		





	 	if(isset($_SESSION['fromDate'])){
		 	
		 	$where.=" and o.date_of_order >= '".date('Y-m-d',strtotime($_SESSION['fromDate']))."' "; 	
		}
		if(isset($_SESSION['toDate'])){
	        
	        $where.=" and o.date_of_order <= '".date('Y-m-d',strtotime($_SESSION['toDate']))."'  "; 
		}

		if(isset($_SESSION['fromDate']) && isset($_SESSION['toDate']))
		{
			$where.=" and o.date_of_order BETWEEN '".date('Y-m-d',strtotime($_SESSION['fromDate']))."' AND '".date('Y-m-d',strtotime($_SESSION['toDate']))."'"; 	
		}
	
		if(isset($_SESSION['salesman'])){
 		 	$salesman=" and s.salesman_id =".$_SESSION['salesman']."   "; 
 		 	$where .= " and R.status='A' AND R.new='' AND D.status='A' AND D.new='' AND od.type=1 $salesman";
 		} else{
        	$salesman=$salesman;
         	$where .= " and R.status='A' AND R.new='' AND D.status='A' AND D.new='' AND od.type=1 $salesman  ";
 		}
		
	
		$groupby=" GROUP BY o.order_id, od.order_detail_id,o.distributor_id,o.retailer_id";		

	$auRec=$_objAdmin->_getSelectList('table_order AS o 
      LEFT JOIN table_salesman AS s ON s.salesman_id = o.salesman_id
      LEFT JOIN table_retailer AS R ON R.retailer_id = o.retailer_id
      LEFT JOIN table_order_detail as od on od.order_id = o.order_id
      LEFT JOIN table_distributors AS D ON D.distributor_id = o.distributor_id
      LEFT JOIN table_transaction_details AS t ON t.salesman_id = o.salesman_id

      LEFT JOIN table_salesman_hierarchy_relationship AS SH ON SH.salesman_id = s.salesman_id 
      LEFT JOIN table_item AS i ON i.item_id = od.item_id
      LEFT JOIN table_segment AS seg ON seg.segment_id = i.item_segment ',
      " R.*, s.salesman_name as salesman_name,s.salesman_id as salesid ,SH.rpt_user_id as salesman_hir ,s.salesman_code as salesman_code,MONTH(o.date_of_order) as month,i.brand_id as brand_id,D.distributor_name as distributor_name,od.acc_quantity as quantity, t.outstanding_amount as outstanding_amount,t.total_sale_amount as total_sale_amount,o.date_of_order as date_of_order,seg.segment_name as segment_name,o.order_id as order_id,o.acc_total_invoice_amount, o.order_type",'',$where.$groupby,'');













	
		
  //      $where.=" and year(o.date_of_order) =".$_SESSION['PVsVYear']."  "; 
  //      $where.=" and MONTH(o.date_of_order) =".$_SESSION['PVsVMonth']."  ";

  //  //      if(isset($_SESSION['salesman'])){
 	// 	//  	$salesman=" and s.salesman_id =".$_SESSION['salesman']."   "; 
 	// 	//  	$where .= " $salesman and o.account_id =".$_SESSION['accountId'];
 	// 	// } else{
  //  //      $salesman=$salesman;
  //  //       $where .= " $salesman and o.account_id =".$_SESSION['accountId'];
 	// 	// }



		// if(isset($_SESSION['salesman'])){
 	// 	 	$salesman=" and s.salesman_id =".$_SESSION['salesman']."   "; 
 	// 	 	$where .= " and R.status='A' AND R.new='' $salesman";
 	// 	} else{
  //       	$salesman=$salesman;
  //        	$where .= " and R.status='A' AND R.new='' $salesman  ";
 	// 	}
		

  //       $where.=" GROUP BY o.order_id,o.distributor_id,o.retailer_id";		

  //       $auRec=$_objAdmin->_getSelectList('table_order AS o 
  //         LEFT JOIN table_salesman AS s ON s.salesman_id = o.salesman_id
  //         LEFT JOIN table_retailer AS R ON R.retailer_id = o.retailer_id
  //         LEFT JOIN table_order_detail as od on od.order_id=o.order_id 
  //         LEFT JOIN table_distributors AS D ON D.distributor_id = o.distributor_id
  //         LEFT JOIN table_transaction_details AS t ON t.salesman_id = o.salesman_id
  //         LEFT JOIN table_salesman_hierarchy_relationship AS SH ON SH.salesman_id = s.salesman_id 
  //         LEFT JOIN table_item AS i ON i.item_id = od.item_id
  //         LEFT JOIN table_segment AS seg ON seg.segment_id = i.item_segment ',
  //        " R.*, s.salesman_name as salesman_name,SH.rpt_user_id as salesman_hir ,s.salesman_code as salesman_code,MONTH(o.date_of_order) as month,i.brand_id as brand_id,D.distributor_name as distributor_name,SUM(od.acc_quantity) as quantity, t.outstanding_amount as outstanding_amount,t.total_sale_amount as total_sale_amount,o.date_of_order as date_of_order,seg.segment_name as segment_name,o.acc_total_invoice_amount, o.order_type",$rp,$where,'');
	

	


	     for($i=0;$i<count($auRec);$i++){			
				
				
	    $auSal=$_objAdmin->_getSelectList('table_salesman',"salesman_name",''," salesman_id='".$auRec[$i]->salesman_hir."' ");
	    $brand_name=$_objAdmin->_getSelectList('table_brands',"brand_name",''," brand_id='".$auRec[$i]->brand_id."' ");
	    $state_name=$_objAdmin->_getSelectList2('state',"state_name",''," state_id='".$auRec[$i]->state."' ");
	
	     $city_name=$_objAdmin->_getSelectList2('city',"city_name",''," city_id='".$auRec[$i]->city."' ");
		
			if($auRec[$i]->order_type == 'Yes')
				$order_type = 'Yes';
			else if($auRec[$i]->order_type == 'No')
				$order_type = 'No';
			else if($auRec[$i]->order_type == 'Adhoc')
				$order_type = 'Unplanned';

			$data.='"'.ucwords($auRec[$i]->salesman_name).'"';
			$data.="\t";

			$data.='"'.ucwords($auRec[$i]->salesman_code).'"';
			$data.="\t";
			$data.='"'.ucwords($auSal[0]->salesman_name).'"';
			$data.="\t";
			$data.='"'.ucwords($brand_name[0]->brand_name).'"';
			$data.="\t";
			$data.='"'.ucwords($auRec[$i]->retailer_name).'"';
			$data.="\t";
			$data.='"'.ucwords($auRec[$i]->retailer_phone_no).'"';
			$data.="\t";
			$data.='"'.ucwords($auRec[$i]->retailer_address).'"';
			$data.="\t";
			$data.='"'.ucwords($city_name[0]->city_name).'"';
			$data.="\t";
			$data.='"'.ucwords($state_name[0]->state_name).'"';
			$data.="\t";
			$data.='"'.ucwords($auRec[$i]->distributor_name).'"';
			$data.="\t";
			$data.='"'.ucwords($auRec[$i]->date_of_order).'"';
			$data.="\t";
			$data.='"'.ucwords($auRec[$i]->segment_name).'"';
			$data.="\t";
			$data.='"'.ucwords($auRec[$i]->quantity).'"';
			$data.="\t";
			$data.='"'.$auRec[$i]->acc_total_invoice_amount.'"';
			$data.="\t";
			// $data.='"'.$auRec[$i]->total_sale_amount.'"';
			// $data.="\t";
			$data.='"'.$auRec[$i]->outstanding_amount.'"';
			$data.="\t";
			$data.='"'.$order_type.'"';
			$data.="\t";

			$data.="\n";


			
			

		}
			 
	header("Content-type: application/octet-stream");
	header("Content-Disposition: attachment; filename=\"Salesman Productivity Report.xls\"");		
	echo $data;
	exit;
	}



/*********************************************************
* DESC: Export Approver Complaints or Final Process Complaints
* Author: Chirag@2017-07-13
*
*********************************************************/

	

	
if(isset($_GET['export_final_process_complaints'])){	

	$data = "Complaint Number\t Operator Name\t Complaint Date\t  Service Distributor \t Service Distributor Code \t Warehouse Name \t RSI Name \t State\t Customer Name\t Customer Phone \t Customer Address\t Customer Email\t Battery Serial No\t Battery Segment\t Sale Date\t Service Distributor SP\t Service Distributor SP Phone\t Action Taken\t Auth Code\t Failure Mode \t Complaint Status\t In-Process TAT (in hours)\t TAT1\t TAT2\t Approver Comments\t Comment\n ";

			
	$condi=	" COMPLAINT.account_id = '".$_SESSION['accountId']."' and BCF.status!='D' and BCF.status!='C' and BCF.under_process='F'";


	
	$auRec=$_objAdmin->_getSelectList('table_service_distributor_sp_bcf as BCF
	 Left Join table_complaint as COMPLAINT on COMPLAINT.complaint_id = BCF.complaint_id
	 Left join table_customer as CUST on CUST.customer_id=COMPLAINT.customer_id
	 Left join country as COUNTRY on COUNTRY.country_id = CUST.country
	 Left join state as ST on ST.state_id = CUST.state
	 Left join city as CT on CT.city_id=CUST.city
	 Left join table_account_admin as ADMIN on ADMIN.operator_id = COMPLAINT.call_center_id
	 Left join table_service_distributor as SD on SD.service_distributor_id = COMPLAINT.service_distributor_id
	 Left join table_service_distributor_sp as SP on SP.service_distributor_sp_id = COMPLAINT.service_distributor_sp_id
	 Left join table_item_bsn as BSN on BSN.bsn_id = COMPLAINT.bsn_id
	 Left join table_item as ITEM on ITEM.item_id = BSN.item_id
	 LEFT join table_segment as SEGMENT on SEGMENT.segment_id = ITEM.item_segment
	 LEft join table_failure_mode as FM on FM.failure_id = BCF.failure_mode
	 LEFT Join table_warehouse as WHS on WHS.warehouse_id = SD.warehouse_id',
	 ' COMPLAINT.*, SD.service_distributor_name,SD.service_distributor_code,CUST.customer_name,CUST.customer_phone_no,CUST.customer_email,CUST.customer_address,BSN.date_of_sale,SP.service_distributor_sp_name,SP.service_distributor_sp_phone_no,ST.state_name,BCF.action_taken,BCF.eapi_auth_code,BCF.under_process,BCF.status as BCFST,COMPLAINT.created_date as complaint_date,COMPLAINT.assigned_sp_time,BCF.call_close_datetime,ADMIN.operator_name,SEGMENT.segment_name,FM.failure_code,FM.failure_desc,BCF.approver_comments,WHS.warehouse_id as WHSID, WHS.warehouse_name','',$condi);

	if(is_array($auRec)){

		for($i=0;$i<count($auRec);$i++){

			$Sale_date=$_objAdmin->_changeDate($auRec[$i]->date_of_sale);
		
			$Complaint_date=$_objAdmin->_changeDate($auRec[$i]->complaint_date);


			if($auRec[$i]->complain_status=='R')
			{
				$sts = "Rejected";	
			}
			else if($auRec[$i]->complain_status=='I')
			{
				$sts = "In-Process";	
			}

			else if($auRec[$i]->complain_status=='P')
			{
				$sts = "Pending";	
			}
			else if($auRec[$i]->complain_status=='C')
			{
				$sts = "Complete";	
			}
			

			/****************** Calculating BCF Status ****************************************/
			if(($auRec[$i]->under_process=='U' && $auRec[$i]->BCFST=='R'))
			{
				$bcf_status = "Closed (U)";
			}
			else if($auRec[$i]->under_process == 'T' && $auRec[$i]->BCFST=='A') 
			{
				$bcf_status = "Closed (T)";
			}
			else if($auRec[$i]->under_process=='C' && $auRec[$i]->BCFST=='C')
			{
				$bcf_status = "Closed (C)";
			}
			else if($auRec[$i]->BCFST=='P' && $auRec[$i]->under_process=='F')
			{
				$bcf_status = "Final Inspection Complete (F)";
			}
			else if($auRec[$i]->under_process=='I' && $auRec[$i]->BCFST=='P')
			{
				$bcf_status = "Initial Inspection Complete (I)";
			}
			else if (($auRec[$i]->assigned_sp_time !='0000-00-00 00:00:00') && ($auRec[$i]->service_distributor_sp_bcf_id == NULL))
			{
				$bcf_status = "Pending at SE";
			}
			else
			{
				$bcf_status = "New (N)";
			}

			/****************** Calculating BCF Status ****************************************/


			/****************** Calculating IN-PROCESS TAT ****************************************/
			if($auRec[$i]->cctat1=='')
			{
				$calculated_pending='';
				$current_time = date("Y-m-d H:i:s");
				$calculated_pending = round((strtotime($current_time) - strtotime($auRec[$i]->complaint_date)) /3600);
			}
			else 
			{
				$calculated_pending ='-';	
			}

			/****************** Calculating IN-PROCESS TAT ****************************************/


			// Complaint No
			$data.='"'.ucwords(addslashes($auRec[$i]->complaint_no)).'"';
			$data.="\t";

			// Operator Name
			 $data.='"'.ucwords(addslashes($auRec[$i]->operator_name)).'"';
			 $data.="\t";

			// Complaint Date
			 $data.='"'.ucwords(addslashes($Complaint_date)).'"';
			 $data.="\t";


			// Service Distributor
			$data.='"'.ucwords(addslashes($auRec[$i]->service_distributor_name)).'"';
			$data.="\t";

			// Service Distributor Code
			 $data.='"'.ucwords(addslashes($auRec[$i]->service_distributor_code)).'"';
			 $data.="\t";

			 // Warehouse Name
			 $data.='"'.ucwords(addslashes($auRec[$i]->warehouse_name)).'"';
			 $data.="\t";

			 $RSINAME = $_objAdmin->_getSelectList2('table_service_personnel_warehouse_mapping as TSPWM left join table_service_personnel as SPNL on SPNL.service_personnel_id = TSPWM.service_personnel_id',' SPNL.sp_name','',' TSPWM.warehouse_id = "'.$auRec[$i]->WHSID.'"');

			 // RSI Name
			 $data.='"'.ucwords(addslashes($RSINAME[0]->sp_name)).'"';
			 $data.="\t";



			 // Customer State
			$data.='"'.ucwords(addslashes($auRec[$i]->state_name)).'"';
			$data.="\t";

			// Customer Name
			$data.='"'.ucwords(addslashes($auRec[$i]->customer_name)).'"';
			$data.="\t";

			//  Customer Phone
			$data.='"'.ucwords(addslashes($auRec[$i]->customer_phone_no)).'"';
			$data.="\t";

			// Customer Address
			 $data.='"'.ucwords(addslashes($auRec[$i]->customer_address)).'"';
			$data.="\t";

			// Customer Email
			 $data.='"'.ucwords(addslashes($auRec[$i]->customer_email)).'"';
			$data.="\t";

			// BSN
			$data.='"'.ucwords(addslashes($auRec[$i]->product_serial_no)).'"';
			$data.="\t";

			// Battery Segment
			$data.='"'.ucwords(addslashes($auRec[$i]->segment_name)).'"';
			$data.="\t";

			// Sale Date
			$data.='"'.ucwords($auRec[$i]->date_of_sale).'"';
			$data.="\t";

			// Service Personnel Name
			 $data.='"'.ucwords(addslashes($auRec[$i]->service_distributor_sp_name)).'"';
			 $data.="\t";

			// Service Personnel Phone
			$data.='"'.addslashes($auRec[$i]->service_distributor_sp_phone_no).'"';
			$data.="\t";

			// Action Taken
			$data.='"'.addslashes($auRec[$i]->action_taken).'"';
			$data.="\t";

			// Auth Code
			$data.='"'.addslashes($auRec[$i]->eapi_auth_code).'"';
			$data.="\t";

			// Failure Mode
			$data.='"'.addslashes($auRec[$i]->failure_desc).'-'.addslashes($auRec[$i]->failure_code).'"';
			$data.="\t";

			// Complaint Status
			$data.='"'.$bcf_status.'"';
			$data.="\t";

			// // Call Closed Datetime
			// $data.='"'.addslashes($auRec[$i]->call_close_datetime).'"';
			// $data.="\t";

			// In-Process TAT
			 $data.='"'.addslashes($calculated_pending).'"';
			 $data.="\t";

			// TAT-1
			 $data.='"'.addslashes($auRec[$i]->cctat1).'"';
			 $data.="\t";

			// TAT-2
			$data.='"'.addslashes($auRec[$i]->cctat2).'"';
			$data.="\t";

			// Approver Comments
			$data.='"'.addslashes($auRec[$i]->approver_comments).'"';
			$data.="\t";

			// Comment
			$data.='"'.addslashes($auRec[$i]->remark).'"';
			$data.="\t";

			$data.="\n";
								
			

		}	

	}

	header("Content-type: application/octet-stream");
	header("Content-Disposition: attachment; filename=\"approver_complaints.xls\"");		
	echo $data;
	exit;
}


/**************************************** Start Export New Dealers List****************************************/	
	
if(isset($_GET['export_new_dealers_report'])){		
	
	$data = "Dealer Name \t Dealer Code \t Salesman\t Division\t Added Date\t Country\t Region\t Zone\t State\t District\t Address\t Phone Number\t Contact Person\t Contact Number\t \n";

/*
		$divisionFilters = "";
		
		$salDiv = $_objAdmin->_getSelectList('table_salesman'," division_id",''," AND salesman_id='".$_SESSION['salesmanId']."'");
		    if(sizeof($salDiv) > 0)
		   	 $divisionFilters = " AND d.division_id =".$salDiv[0]->division_id;



	$condi=	" d.new!='' and d.status!='D' $divisionFilters and d.account_id='".$_SESSION['accountId']."' ORDER BY d.distributor_name";*/

	if($_SESSION['salesman_id'])
	$salesman_id=" AND sal.salesman_id='".$_SESSION['salesman_id']."'";

	if($_SESSION['distributor_id'])
		$distributor = " AND d.distributor_id='".$_SESSION['distributor_id']."'";

	if($_SESSION['state'])
		$state="and r.state='".$_SESSION['state']."'";

	


	$salDiv = $_objAdmin->_getSelectList('table_salesman'," division_id",''," AND salesman_id='".$_SESSION['salesmanId']."'");
		    if(sizeof($salDiv) > 0)
				$divisionFilters = " AND r.division_id =".$salDiv[0]->division_id;


	$where = " r.new='' $divisionFilters and r.status!='D'";

	

	$auRec=$_objAdmin->_getSelectList('table_retailer as r 
		left join table_division AS dV ON dV.division_id = r.division_id
		left join table_brands AS tb ON tb.brand_id = r.brand_id  
		left join table_retailer_app_user AS AP ON AP.retailer_id = r.retailer_id  
		LEFT JOIN table_relationship as rr on rr.relationship_id=r.relationship_id 
		left join table_account as a on a.account_id=r.account_id 
		left join table_web_users as w on w.retailer_id=r.retailer_id 
		left join state as s on s.state_id=r.state 
		left join city as c on c.city_id=r.city 
		left join table_retailer_channel_master as cm on r.channel_id=cm.channel_id 
		left join table_retailer_type_master as tm on tm.type_id=r.type_id 
		left JOIN table_salesman AS sal ON sal.salesman_id = r.salesman_id
		left join table_distributors as d on d.distributor_id=r.distributor_id 
		left join table_markets as m on m.market_id=r.market_id 
		left join country as cn on r.country = cn.country_id
		left join table_region as tr on r.region = tr.region_id
		left join table_zone as tz on r.zone = tz.zone_id',
				"r.*,w.username,cm.channel_name,d.distributor_name,w.email_id,w.web_user_id,rr.relationship_code,tm.type_name, w.status as loginStatus,sal.salesman_name,sal.salesman_code,s.state_name,c.city_name,dV.division_name,m.market_name,AP.app_version,tb.brand_name,cn.country_name,tr.region_name,tz.zone_name",''," $where $salesman_id $distributor $state and (r.start_date BETWEEN '".date('Y-m-d', strtotime($_SESSION['FromDate']))."' AND '".date('Y-m-d', strtotime($_SESSION['ToDate']))."')  ORDER BY r.start_date desc, c.city_name asc");




	if(is_array($auRec)){
		for($i=0;$i<count($auRec);$i++){
		


			$added_date = $_objAdmin->_changeDate($auRec[$i]->start_date);

			$remove = array("\n", "\r\n", "\r");

			$data.='"'.ucwords($auRec[$i]->retailer_name).'"';
			$data.="\t";

			$data.='"'.ucwords($auRec[$i]->retailer_code).'"';
			$data.="\t";

			$data.='"'.ucwords($auRec[$i]->salesman_name).'"';
			$data.="\t";

			$data.='"'.ucwords($auRec[$i]->division_name).'"';
			$data.="\t";

			$data.='"'.$added_date.'"';
			$data.="\t";

			$data.='"'.ucwords($auRec[$i]->country_name).'"';
			$data.="\t";

			$data.='"'.ucwords($auRec[$i]->region_name).'"';
			$data.="\t";

			$data.='"'.ucwords($auRec[$i]->zone_name).'"';
			$data.="\t";

			$data.='"'.ucwords($auRec[$i]->state_name).'"';
			$data.="\t";

			$data.='"'.ucwords($auRec[$i]->city_name).'"';
			$data.="\t";

		    $data.='"'.ucwords(str_replace($remove, ' ',$auRec[$i]->retailer_address)).'"';
			$data.="\t";

			$data.='"'.$auRec[$i]->retailer_phone_no.'"';
			$data.="\t";

			$data.='"'.ucwords($auRec[$i]->contact_person).'"';
			$data.="\t";

			$data.='"'.$auRec[$i]->contact_number.'"';
			$data.="\t";

			//$data.='"'.$end_date.'"';
			//$data.="\t";

			//$data.='"'.$sts.'"';	
			//$data.="\t";

			//$data.='"'.$LogSts.'"';	

			$data.="\n";

		}						
	}
	header("Content-type: application/octet-stream");
	header("Content-Disposition: attachment; filename=\"new_dealers_list.xls\"");		
	echo $data;
	exit;
	}
	
/********************************End Export New Dealers List*****************************************/


/*******************************************************
* DESC: Export Warehouse Stock Commercial
* Author: Chirag@2017-08-05
*
*
*
********************************************************/	


	 if(isset($_GET['export_warehouse_available_stock_commercial'])){	
	//echo hello;exit;
	
	$data=" Warehouse Name\t Warehouse Code\t Status \t Battery Ageing(In Days)\t Recharged Date\t BSN\t Item Code\t Item Name\t Category Name\t Manufacture Date\t Warranty Period\t ProRata\t Grace Period\t Last Update Date\n";	 
		
		$where = "";
		$dis="";
		$divisionCondition = "";

		/***Filter added by Maninder on 29th April 2016**********************************************/
			if(isset($_SESSION['warehouseIDSale'])){ 

				$querAccount= mysql_query("SELECT session_id, warehouse_id FROM table_warehouse WHERE warehouse_id=".$_SESSION['warehouseIDSale']."");
				$stockRec = mysql_fetch_array($querAccount);
				
				$divisionJoin = "";
				$divisionCondition = "";

				$where .= " AND WS.stock_value!=0 AND WS.warehouse_id='".$_SESSION['warehouseIDSale']."' $divisionCondition ";		      
			}

			if(isset($_SESSION['itemWarehouseId'])){
	          $where .= " AND WS.stock_value!=0 AND WS.item_id='".$_SESSION['itemWarehouseId']."' ";
			}

			$where .= " AND WS.stock_value!=0 ";
			if(isset($_SESSION['warehouseIdSel']) && $_SESSION['warehouseIdSel']!=0)
	           {
	           		$warehouseFilter = " AND WS.warehouse_id =".$_SESSION['warehouseIdSel'];
	           }
		/**************************************************/
		
		if($_SESSION['warehouse_id']!="" && $_SESSION['warehouse_id']!="0"){ $dis=" and WS.warehouse_id='".$_SESSION['warehouse_id']."'";}

		// $auRec=$_objAdmin->_getSelectList('table_item_distributor_stock AS s 
		// LEFT JOIN table_cases AS clr ON clr.case_id = s.attribute_value_id 
		// LEFT JOIN table_category AS c ON c.category_id = s.category_id 
		// LEFT JOIN table_item AS i ON i.item_id = s.item_id 
		// LEFT JOIN table_distributors AS d ON d.distributor_id = s.distributor_id '.$divisionJoin,"d.distributor_code,d.distributor_name, c.category_name, clr.case_size, i.item_name, i.item_code, s.dis_stock_value, s.status, s.last_update_datetime,s.dis_stk_id",$rp,$where,'');



		$auRec=$_objAdmin->_getSelectList('table_warehouse_stock AS WS 
		INNER JOIN table_item_bsn AS BSN ON BSN.bsn_id = WS.bsn_id
		LEFT JOIN table_category AS c ON c.category_id = BSN.category_id
		LEFT JOIN table_item AS i ON i.item_id = BSN.item_id
		LEFT JOIN table_warehouse AS W ON W.warehouse_id = WS.warehouse_id '.$divisionJoin," W.warehouse_code, W.warehouse_name, c.category_name,i.item_name, i.item_code, BSN.bsn, BSN.manufacture_date, BSN.warranty_period, BSN.warranty_prorata, BSN.warranty_grace_period, WS.stock_value, WS.status AS warehouseSts, BSN.status AS bsnsts, WS.last_update_datetime, WS.stk_recharged_date, BSN.recharged_date, BSN.distributor_dispatch_date, WS.warehouse_stock_id",$rp,$where.$warehouseFilter,'');

	
	     for($i=0;$i<count($auRec);$i++){			
				
				$remove = array("\n", "\r\n", "\r");
				$last_update_datetime=$_objAdmin->_changeDate($auRec[$i]->last_update_datetime);
				$remove = array(",", "-", "_");

			$rechargedDate =($auRec[$i]->stk_recharged_date=='0000-00-00')?"-":$auRec[$i]->stk_recharged_date;

			$stockObj = new StockClass();
			$batteryAge = $stockObj->getBatteryAgeing($auRec[$i]->manufacture_date, $auRec[$i]->stk_recharged_date, $auRec[$i]->distributor_dispatch_date);
			$status = $stockObj->getBatteryStockStatus($auRec[$i]->warehouseSts);


			$data.='"'.ucwords($auRec[$i]->warehouse_name).'"';
			$data.="\t";

			$data.='"'.ucwords($auRec[$i]->warehouse_code).'"';
			$data.="\t";

			$data.='"'.$status.'"';
			$data.="\t";


			$data.='"'.$batteryAge.'"';
			$data.="\t";



			$data.='"'.$rechargedDate.'"';
			$data.="\t";



			$data.='"'.$auRec[$i]->bsn.'"';
			$data.="\t";

			$data.='"'.ucwords($auRec[$i]->item_code).'"';
			$data.="\t";

			$data.='"'.ucwords($auRec[$i]->item_name).'"';
			$data.="\t";

			$data.='"'.ucwords($auRec[$i]->category_name).'"';
			$data.="\t";


			$data.='"'.$auRec[$i]->manufacture_date.'"';
			$data.="\t";

			$data.='"'.$auRec[$i]->warranty_period.'"';
			$data.="\t";

			$data.='"'.$auRec[$i]->warranty_prorata.'"';
			$data.="\t";

			$data.='"'.$auRec[$i]->warranty_grace_period.'"';
			$data.="\t";

			$data.='"'.ucwords($auRec[$i]->last_update_datetime).'"';
			$data.="\n";

		}
			 
	header("Content-type: application/octet-stream");
	header("Content-Disposition: attachment; filename=\"Warehouse Stock Report.xls\"");		
	echo $data;
	exit;
	}



/*******************************************************
* DESC: End of Export Warehouse Stock Commercial
* Author: Chirag@2017-08-05
*
*
*
********************************************************/

/*********************************************************
* DESC: Export Call Center Complaints BCF Dump
* Author: Chirag@2017-10-10
*
*********************************************************/

	

	
if(isset($_GET['export_call_center_complaints_bcf_dump'])){	

	$data = "Complaint Number\t Operator Name\t Complaint Date\t  Service Distributor \t Service Distributor Code \t State\t District\t Customer Name\t Customer Phone \t Customer Address\t Customer Email\t Battery Serial No\t Battery Model\t Battery Segment\t Sale Date\t Service Distributor SP\t Service Distributor SP Phone\t Action Taken\t Failure Mode \t Auth Code\t Complaint Status\t Call Closed Time\t In-Process TAT (in hours)\t TAT1\t TAT2\t Approver Comment \t Call login Date-Time\t Initial Reading Date-Time\t Allocation Date-Time\t Amount\t Actual Problem\t Visual Problem\t Documents Checked\t OCV\t C1\t C2\t C3\t C4\t C5\t C6\t TOC\t C1\t C2\t C3\t C4\t C5\t C6\t total-charging-time\t Back-up-load \t Back-up-time\t Load-used\t Comment\n ";

			
	$condi=	" COMPLAINT.account_id = '".$_SESSION['accountId']."' and COMPLAINT.call_center_id !=0 ORDER BY COMPLAINT.created_date";

	
	$auRec=$_objAdmin->_getSelectList('table_complaint as COMPLAINT
		left join table_service_distributor as SD on SD.service_distributor_id = COMPLAINT.service_distributor_id
		left join table_customer as CUST on CUST.customer_id = COMPLAINT.customer_id
		left join table_item_bsn as BSN on BSN.bsn_id = COMPLAINT.bsn_id
		left join table_service_distributor_sp as SP on SP.service_distributor_sp_id = COMPLAINT.service_distributor_sp_id
		left join state as ST on ST.state_id = CUST.state
		left join table_service_distributor_sp_bcf as BCF on BCF.complaint_id = COMPLAINT.complaint_id
		left join table_failure_mode as FM on FM.failure_id = BCF.failure_mode
		left join table_item as ITEM on ITEM.item_id = BSN.item_id
		left join table_segment as SEGMENT on SEGMENT.segment_id = ITEM.item_segment
		left join city as CT on SD.city_id = CT.city_id
		left join table_account_admin as ADMIN on ADMIN.operator_id = COMPLAINT.call_center_id',' COMPLAINT.*, SD.service_distributor_name,SD.service_distributor_code,CUST.customer_name,CUST.customer_phone_no,CUST.customer_email,CUST.customer_address,BSN.date_of_sale,SP.service_distributor_sp_name,SP.service_distributor_sp_phone_no,ST.state_name,BCF.action_taken,BCF.eapi_auth_code,BCF.under_process,BCF.status as BCFST,BCF.approver_comments,BCF.created_datetime,BCF.initial_reading_save_time ,COMPLAINT.created_date as complaint_date,COMPLAINT.assigned_sp_time,BCF.call_close_datetime,ADMIN.operator_name,FM.failure_desc,FM.failure_code,SEGMENT.segment_name,COMPLAINT.allocation_date as allocation_date ,ITEM.item_code,CT.city_name,BCF.asd_basic_payout AS asd_basic_payout,BCF.asd_tat_incentive AS asd_tat_incentive ,BCF.asd_payout_revival AS asd_payout_revival,BCF.actual_problem, BCF.visual_observation,BCF.documents_checked,BCF.ocv,BCF.inital_reading_c1,BCF.inital_reading_c2,BCF.inital_reading_c3,BCF.inital_reading_c4,BCF.inital_reading_c5,BCF.inital_reading_c6,BCF.toc,BCF.final_reading_c1,BCF.final_reading_c2,BCF.final_reading_c3,BCF.final_reading_c4,BCF.final_reading_c5,BCF.final_reading_c6,BCF.charging_time_in_hrs,BCF.min_backup,BCF.backup_load,BCF.load_used','',$condi);

	if(is_array($auRec)){

		for($i=0;$i<count($auRec);$i++){

			$Sale_date=$_objAdmin->_changeDate($auRec[$i]->date_of_sale);
		
			$Complaint_date=$_objAdmin->_changeDate($auRec[$i]->complaint_date);


			if($auRec[$i]->complain_status=='R')
			{
				$sts = "Rejected";	
			}
			else if($auRec[$i]->complain_status=='I')
			{
				$sts = "In-Process";	
			}

			else if($auRec[$i]->complain_status=='P')
			{
				$sts = "Pending";	
			}
			else if($auRec[$i]->complain_status=='C')
			{
				$sts = "Complete";	
			}
			

			/****************** Calculating BCF Status ****************************************/
			if(($auRec[$i]->under_process=='U' && $auRec[$i]->BCFST=='R'))
			{
				$bcf_status = "Closed (U)";
			}
			else if($auRec[$i]->under_process == 'T' && $auRec[$i]->BCFST=='A') 
			{
				$bcf_status = "Closed (T)";
			}
			else if($auRec[$i]->under_process=='C' && $auRec[$i]->BCFST=='C')
			{
				$bcf_status = "Closed (C)";
			}
			else if($auRec[$i]->BCFST=='P' && $auRec[$i]->under_process=='F')
			{
				$bcf_status = "Final Inspection Complete (F)";
			}
			else if($auRec[$i]->under_process=='I' && $auRec[$i]->BCFST=='P')
			{
				$bcf_status = "Initial Inspection Complete (I)";
			}
			else if (($auRec[$i]->assigned_sp_time !='0000-00-00 00:00:00') && ($auRec[$i]->service_distributor_sp_bcf_id == NULL))
			{
				$bcf_status = "Pending at SE";
			}
			else
			{
				$bcf_status = "New (N)";
			}

			/****************** Calculating BCF Status ****************************************/


			/****************** Calculating IN-PROCESS TAT ****************************************/
			if($auRec[$i]->cctat1=='')
			{
				$calculated_pending='';
				$current_time = date("Y-m-d H:i:s");
				$calculated_pending = round((strtotime($current_time) - strtotime($auRec[$i]->complaint_date)) /3600);
			}
			else 
			{
				$calculated_pending ='-';	
			}
 $AMOUNT=$auRec[$i]->asd_basic_payout+ $auRec[$i]->asd_tat_incentive + $auRec[$i]->asd_payout_revival;
			/****************** Calculating IN-PROCESS TAT ****************************************/


			// Complaint No
			$data.='"'.ucwords(addslashes($auRec[$i]->complaint_no)).'"';
			$data.="\t";

			// Operator Name
			 $data.='"'.ucwords(addslashes($auRec[$i]->operator_name)).'"';
			 $data.="\t";

			// Complaint Date
			 $data.='"'.ucwords(addslashes($Complaint_date)).'"';
			 $data.="\t";


			// Service Distributor
			$data.='"'.ucwords(addslashes($auRec[$i]->service_distributor_name)).'"';
			$data.="\t";

			// Service Distributor Code
			 $data.='"'.ucwords(addslashes($auRec[$i]->service_distributor_code)).'"';
			 $data.="\t";

			 // Customer State
			$data.='"'.ucwords(addslashes($auRec[$i]->state_name)).'"';
			$data.="\t";

			 // Customer District
			$data.='"'.ucwords(addslashes($auRec[$i]->city_name)).'"';
			$data.="\t";

			// Customer Name
			$data.='"'.ucwords(addslashes($auRec[$i]->customer_name)).'"';
			$data.="\t";

			//  Customer Phone
			$data.='"'.ucwords(addslashes($auRec[$i]->customer_phone_no)).'"';
			$data.="\t";

			// Customer Address
			 $data.='"'.ucwords(addslashes($auRec[$i]->customer_address)).'"';
			$data.="\t";

			// Customer Email
			 $data.='"'.ucwords(addslashes($auRec[$i]->customer_email)).'"';
			$data.="\t";

			// BSN
			$data.='"'.ucwords(addslashes($auRec[$i]->product_serial_no)).'"';
			$data.="\t";

			// Battery Model
			$data.='"'.ucwords(addslashes($auRec[$i]->item_code)).'"';
			$data.="\t";

			// Battery Segment
			$data.='"'.ucwords(addslashes($auRec[$i]->segment_name)).'"';
			$data.="\t";

			// Sale Date
			$data.='"'.ucwords($auRec[$i]->date_of_sale).'"';
			$data.="\t";

			// Service Personnel Name
			 $data.='"'.ucwords(addslashes($auRec[$i]->service_distributor_sp_name)).'"';
			 $data.="\t";

			// Service Personnel Phone
			$data.='"'.addslashes($auRec[$i]->service_distributor_sp_phone_no).'"';
			$data.="\t";

			// Action Taken
			$data.='"'.addslashes($auRec[$i]->action_taken).'"';
			$data.="\t";

			// Failure Mode 
			$data.='"'.addslashes($auRec[$i]->failure_desc).'-'.addslashes($auRec[$i]->failure_code).'"';
			$data.="\t";

			// Auth Code
			$data.='"'.addslashes($auRec[$i]->eapi_auth_code).'"';
			$data.="\t";

			// Complaint Status
			$data.='"'.$bcf_status.'"';
			$data.="\t";

			// Call Closed Datetime
			$data.='"'.addslashes($auRec[$i]->call_close_datetime).'"';
			$data.="\t";

			// In-Process TAT
			 $data.='"'.addslashes($calculated_pending).'"';
			 $data.="\t";

			// TAT-1
			 $data.='"'.addslashes($auRec[$i]->cctat1).'"';
			 $data.="\t";

			// TAT-2
			$data.='"'.addslashes($auRec[$i]->cctat2).'"';
			$data.="\t";

			// Approver Comments
			$data.='"'.addslashes($auRec[$i]->approver_comments).'"';
			$data.="\t";

			// Created Date/Time
			$data.='"'.addslashes($auRec[$i]->created_datetime).'"';
			$data.="\t";

			// Initial Date/Time
			$data.='"'.addslashes($auRec[$i]->initial_reading_save_time).'"';
			$data.="\t";

			// allocation Date/Time
			$data.='"'.addslashes($auRec[$i]->allocation_date).'"';
			$data.="\t";

			// Amount
			$data.='"'.addslashes($AMOUNT).'"';
			$data.="\t";


			// Actual Problem
			$data.='"'.addslashes($auRec[$i]->actual_problem).'"';
			$data.="\t";


			// Visual Observation
			$data.='"'.addslashes($auRec[$i]->visual_observation).'"';
			$data.="\t";


			// Documents Checked
			$data.='"'.addslashes($auRec[$i]->documents_checked).'"';
			$data.="\t";


			// OCV
			$data.='"'.addslashes($auRec[$i]->ocv).'"';
			$data.="\t";

			// Initial Reading C1
			$data.='"'.addslashes($auRec[$i]->inital_reading_c1).'"';
			$data.="\t";


			// Initial Reading C2
			$data.='"'.addslashes($auRec[$i]->inital_reading_c2).'"';
			$data.="\t";


			// Initial Reading C3
			$data.='"'.addslashes($auRec[$i]->inital_reading_c3).'"';
			$data.="\t";


			// Initial Reading C4
			$data.='"'.addslashes($auRec[$i]->inital_reading_c4).'"';
			$data.="\t";


			// Initial Reading C5
			$data.='"'.addslashes($auRec[$i]->inital_reading_c5).'"';
			$data.="\t";


			// Initial Reading C6
			$data.='"'.addslashes($auRec[$i]->inital_reading_c6).'"';
			$data.="\t";


			// TOC
			$data.='"'.addslashes($auRec[$i]->toc).'"';
			$data.="\t";


			// Final Reading C1
			$data.='"'.addslashes($auRec[$i]->final_reading_c1).'"';
			$data.="\t";


			// Final Reading C2
			$data.='"'.addslashes($auRec[$i]->final_reading_c2).'"';
			$data.="\t";


			// Final Reading C3
			$data.='"'.addslashes($auRec[$i]->final_reading_c3).'"';
			$data.="\t";


			// Final Reading C4
			$data.='"'.addslashes($auRec[$i]->final_reading_c4).'"';
			$data.="\t";


			// Final Reading C5
			$data.='"'.addslashes($auRec[$i]->final_reading_c5).'"';
			$data.="\t";


			// Final Reading C6
			$data.='"'.addslashes($auRec[$i]->final_reading_c6).'"';
			$data.="\t";


			// Charging Time in Hours
			$data.='"'.addslashes($auRec[$i]->charging_time_in_hrs).'"';
			$data.="\t";


			// Min Backup
			$data.='"'.addslashes($auRec[$i]->min_backup).'"';
			$data.="\t";


			// Back up Load
			$data.='"'.addslashes($auRec[$i]->backup_load).'"';
			$data.="\t";


			// Load Used
			$data.='"'.addslashes($auRec[$i]->load_used).'"';
			$data.="\t";

			// Comment 
			$data.='"'.addslashes($auRec[$i]->remark).'"';
			$data.="\t";

			
			

			$data.="\n";
								
			

		}	

	}

	header("Content-type: application/octet-stream");
	header("Content-Disposition: attachment; filename=\"complaint_list.xls\"");		
	echo $data;
	exit;
}




/*********************************************************
* DESC: Export Call Center Complaints BCF Dump
* Author: Chirag@2017-10-10
*
*********************************************************/

/*********************************************************
* DESC: Export Transferred Stock to Distributor List
* Author: Chirag@2017-11-06
*
*********************************************************/

	

	
if(isset($_GET['export_partywise_transfer_list'])){	

	$objStock = new StockClass();
	$data = "Warehouse Code\t Warehouse Name\t Total BSN \t  Distributor Name \t Distributor Code \t Invoice Date \t Invoice No \t Delivery Challan Date \t LR Number \t Transfer Date \n ";

	
	 if($_SESSION['userLoginType'] == 7) { // Warehouse Login
           		$warehouseFilter = " s.warehouse_id =".$_SESSION['warehouseId'];
     }		
	$condi=	" $warehouseFilter group by s.distributor_id,s.bill_no,s.bill_date";

	
	$auRec=$_objAdmin->_getSelectList("table_item_dis_stk_inprocess AS s 
			LEFT JOIN table_warehouse AS WH ON WH.warehouse_id = s.warehouse_id
			LEFT JOIN table_category AS c ON c.category_id = s.category_id
			LEFT JOIN table_item AS i ON i.item_id = s.item_id
			LEFT JOIN table_distributors AS d ON d.distributor_id = s.distributor_id 
			LEFT JOIN table_delivery_challan as DLC on DLC.distributor_id = s.distributor_id and DLC.warehouse_id = s.warehouse_id and DATE_FORMAT(s.created_datetime,'%Y-%m%d') = DATE_FORMAT(DLC.created_datetime,'%Y-%m-%d') ",' COUNT(*) AS TotalBSN, d.distributor_code,d.distributor_name, c.category_name, s.bill_date, s.bill_no, s.status, s.delivery_challan_no, s.delivery_challan_date, s.last_update_datetime,s.dis_stk_inpro_id, WH.warehouse_code,WH.warehouse_name,s.delivery_challan_no,s.distributor_id,DLC.delivery_challan_number','',$condi);
	if(is_array($auRec)){

		for($i=0;$i<count($auRec);$i++){

			$bill_date = $objStock->dateFormatter($auRec[$i]->bill_date);
			$delivery_challan_date = $objStock->dateFormatter($auRec[$i]->delivery_challan_date);
			$billl_date = date('Y-m-d',strtotime($bill_date));


			


			// Warehouse Code
			$data.='"'.ucwords(addslashes($auRec[$i]->warehouse_code)).'"';
			$data.="\t";

			// Warehouse Name
			 $data.='"'.ucwords(addslashes($auRec[$i]->warehouse_name)).'"';
			 $data.="\t";

			// Total BSN
			 $data.='"'.ucwords(addslashes($auRec[$i]->TotalBSN)).'"';
			 $data.="\t";


			// Distributor Name
			$data.='"'.ucwords(addslashes($auRec[$i]->distributor_name)).'"';
			$data.="\t";

			// Distributor Code
			 $data.='"'.ucwords(addslashes($auRec[$i]->distributor_code)).'"';
			 $data.="\t";

			 // Bill Date
			$data.='"'.ucwords(addslashes(date('Y-m-d',strtotime($bill_date)))).'"';
			$data.="\t";

			 // Bill No
			$data.='"'.ucwords(addslashes($auRec[$i]->bill_no)).'"';
			$data.="\t";

			// Delivery Challan Date
			$data.='"'.ucwords(addslashes($delivery_challan_date)).'"';
			$data.="\t";

			//  LR Number
			$data.='"'.ucwords(addslashes($auRec[$i]->delivery_challan_no)).'"';
			$data.="\t";

			//  Transfer Date
			$data.='"'.ucwords(addslashes(date('Y-m-d',strtotime($auRec[$i]->last_update_datetime)))).'"';
			// $data.="\t";
			// $BSNstr = '';
			// $auOrd=$_objAdmin->_getSelectList('table_item_dis_stk_inprocess AS DisStk LEFT JOIN table_item AS I ON I.item_id = DisStk.item_id LEFT JOIN table_warehouse AS W ON W.warehouse_id = DisStk.warehouse_id left join table_item_bsn as BSN on BSN.bsn_id = DisStk.bsn_id left join table_distributors as DIST on DIST.distributor_id = DisStk.distributor_id'," DisStk.status, DisStk.delivery_challan_date, DisStk.bill_no, DisStk.bill_date, W.warehouse_name, DisStk.item_id, DisStk.bsn_id, I.item_code, I.item_name,BSN.bsn, DIST.distributor_name,DisStk.delivery_challan_date",'',"DisStk.bill_no='".$auRec[$i]->bill_no."' and  DisStk.bill_date = '".date('Y-m-d',strtotime($bill_date))."' and DisStk.distributor_id = '".$auRec[$i]->distributor_id."' ");
			
			// if(count($auOrd)>0 && is_array($auOrd))
			// {
			// 	for($j=0;$j<count($auOrd);$j++)
			// 	{
			// 		$BSNstr = $BSNstr.$auOrd[$j]->bsn.",";
			// 	}
			// }
			// // BSNs
			// $data.='"'.ucwords(addslashes($BSNstr)).'"';

			$data.="\n";
								
			

		}	

	}

	header("Content-type: application/octet-stream");
	header("Content-Disposition: attachment; filename=\"partywise_transfer_list.xls\"");		
	echo $data;
	exit;
}




/*********************************************************
* DESC: Export Transferred Stock to Distributor List
* Author: Chirag@2017-11-06
*
*********************************************************/


/*********************************************************
* DESC: Export Transferred Stock to Warehouse
* Author: Chirag@2017-11-09
*
*********************************************************/


if(isset($_GET['export_warehouse_transfer_list'])){	

	$objStock = new StockClass();
	$data = "From Warehouse\t To Warehouse\t Total BSN \t  Dispatched No \t Dispatched Date \t Transfer Time \n ";

	
	 if($_SESSION['userLoginType'] == 7) { // Warehouse Login
           		$warehouseFilter = " AND WHST.from_warehouse_id =".$_SESSION['warehouseId'];
           }

	$condi=	" $warehouseFilter GROUP BY WHSTD.transfer_id ";

	$auRec=$_objAdmin->_getSelectList("table_warehouse_stk_trns AS WHST LEFT JOIN table_warehouse_stk_trn_dts AS WHSTD ON WHSTD.transfer_id = WHST.transfer_id LEFT JOIN table_warehouse AS W ON W.warehouse_id = WHST.from_warehouse_id LEFT JOIN table_warehouse AS W2 ON W2.warehouse_id = WHST.to_warehouse_id ",' WHST.status, WHST.transfer_id, WHST.transfer_datetime, COUNT(*) AS ttlbsntransfer, WHST.bill_no, WHST.bill_date, W.warehouse_name AS from_warehouse_name, W2.warehouse_name AS to_warehouse_name','',$condi);


	if(is_array($auRec)){

		for($i=0;$i<count($auRec);$i++){

			$bill_date = $objStock->dateFormatter($auRec[$i]->bill_date);
			// From Warehouse
			$data.='"'.ucwords(addslashes($auRec[$i]->from_warehouse_name)).'"';
			$data.="\t";

			// To Warehouse
			 $data.='"'.ucwords(addslashes($auRec[$i]->to_warehouse_name)).'"';
			 $data.="\t";

			// Total BSN
			 $data.='"'.ucwords(addslashes($auRec[$i]->ttlbsntransfer)).'"';
			 $data.="\t";


			// Bill No
			$data.='"'.ucwords(addslashes($auRec[$i]->bill_no)).'"';
			$data.="\t";


			// Bill Date
			$data.='"'.ucwords(addslashes($auRec[$i]->bill_date)).'"';
			$data.="\t";

			// Transfer Datetime
			 $data.='"'.ucwords(addslashes($auRec[$i]->transfer_datetime)).'"';
			 $data.="\t";

			

			$data.="\n";
								
			

		}	

	}

	header("Content-type: application/octet-stream");
	header("Content-Disposition: attachment; filename=\"warehouse_stock_transfer_list.xls\"");		
	echo $data;
	exit;
}




/*********************************************************
* DESC: Export Transferred Stock to Warehouse
* Author: Chirag@2017-11-09
*
*********************************************************/



/*********************************************************
* DESC: Export Transferred Stock to Warehouse
* Author: Chirag@2017-11-11
*
*********************************************************/


if(isset($_GET['export_warehouse_dispatches_report'])){	


	$frmDate = $_REQUEST['fdate'];
	$toDate = $_REQUEST['tdate'];
	$whId = $_REQUEST['whid'];	

	$objStock = new StockClass();
	$data = "From Warehouse\t To Warehouse\t Item Name \t Item Code \t Total BSN \t  Dispatched No \t Dispatched Date \t Transfer Time \n ";

	
	$reportFilter = "";
	if($_SESSION['userLoginType'] == 7) { // Warehouse Login
       		$warehouseFilter = " AND WHST.from_warehouse_id =".$_SESSION['warehouseId'];
       }
    else if($whId == 'all')
    {
    	$reportFilter = "";
    }
    else if($whId>=0)
    {
    	$reportFilter = " AND WHST.from_warehouse_id = '".$whId."'";
    }
	$condi=	" transfer_datetime >='".date('Y-m-d H:i:s',strtotime($frmDate))."' AND transfer_datetime <='".date('Y-m-d H:i:s',strtotime($toDate))."' $warehouseFilter $reportFilter group by WHSTD.item_id,WHST.transfer_id,WHST.bill_no";
	
	$auRec=$_objAdmin->_getSelectList("table_warehouse_stk_trns AS WHST LEFT JOIN table_warehouse_stk_trn_dts AS WHSTD ON WHSTD.transfer_id = WHST.transfer_id LEFT JOIN table_warehouse AS W ON W.warehouse_id = WHST.from_warehouse_id LEFT JOIN table_warehouse AS W2 ON W2.warehouse_id = WHST.to_warehouse_id left join table_item as ITEM on ITEM.item_id = WHSTD.item_id",' WHST.status, WHST.transfer_id, WHST.transfer_datetime, COUNT(*) AS ttlbsntransfer, WHST.bill_no, WHST.bill_date, W.warehouse_name AS from_warehouse_name, W2.warehouse_name AS to_warehouse_name,ITEM.item_code,ITEM.item_name','',$condi);
	
	if(is_array($auRec)){

		for($i=0;$i<count($auRec);$i++){

			$bill_date = $objStock->dateFormatter($auRec[$i]->bill_date);
			// From Warehouse
			$data.='"'.ucwords(addslashes($auRec[$i]->from_warehouse_name)).'"';
			$data.="\t";

			// To Warehouse
			 $data.='"'.ucwords(addslashes($auRec[$i]->to_warehouse_name)).'"';
			 $data.="\t";

			// Item Name
			 $data.='"'.ucwords(addslashes($auRec[$i]->item_name)).'"';
			 $data.="\t";			 

			 // Item Code
			 $data.='"'.ucwords(addslashes($auRec[$i]->item_code)).'"';
			 $data.="\t";

			// Total BSN
			 $data.='"'.ucwords(addslashes($auRec[$i]->ttlbsntransfer)).'"';
			 $data.="\t";


			// Bill No
			$data.='"'.ucwords(addslashes($auRec[$i]->bill_no)).'"';
			$data.="\t";


			// Bill Date
			$data.='"'.ucwords(addslashes($auRec[$i]->bill_date)).'"';
			$data.="\t";

			// Transfer Datetime
			 $data.='"'.ucwords(addslashes($auRec[$i]->transfer_datetime)).'"';
			 $data.="\t";

			

			$data.="\n";
								
			

		}	

	}

	header("Content-type: application/octet-stream");
	header("Content-Disposition: attachment; filename=\"warehouse_stock_dispatch_list.xls\"");		
	echo $data;
	exit;
}




/*********************************************************
* DESC: Export Transferred Stock to Warehouse
* Author: Chirag@2017-11-11
*
*********************************************************/


/*********************************************************
* DESC: Export Actual Stock at Warehouse FIFO
* Author: Chirag@2017-11-13
*
*********************************************************/


if(isset($_GET['export_warehouse_actual_stock_status'])){	

	$whId = $_REQUEST['whid'];	

	$objStock = new StockClass();
	$data = "Warehouse Name\t Warehouse Code\t  Country\t State \t City \t Item Name \t Item Code \t  Quantity \t Category \t FIFO Status\n ";

	
	$reportFilter = "";
	if($_SESSION['userLoginType'] == 7) { // Warehouse Login
       		$warehouseFilter = " AND WS.warehouse_id =".$_SESSION['warehouseId'];
       }
    else if($whId == 'all')
    {
    	$reportFilter = "";
    }
    else if($whId>=0)
    {
    	$reportFilter = " AND WS.warehouse_id = '".$whId."'";
    }
	$whereItemWise = " WS.status = 'A' $warehouseFilter $reportFilter GROUP BY FIFO_STATUS, W.warehouse_id, i.item_id  ";
	
	$auRec=$_objAdmin->_getSelectList("table_warehouse_stock AS WS 
		INNER JOIN table_item_bsn AS BSN ON BSN.bsn_id = WS.bsn_id 
		LEFT JOIN table_category AS c ON c.category_id = BSN.category_id 
		LEFT JOIN table_item AS i ON i.item_id = BSN.item_id 
		LEFT JOIN table_warehouse AS W ON W.warehouse_id = WS.warehouse_id
		LEFT JOIN country as CNTRY on CNTRY.country_id = W.country_id
		LEFT JOIN state as ST on ST.state_id = W.state_id
		LEFT JOIN city as CT on CT.city_id = W.city_id"," W.warehouse_code, W.warehouse_name,W.warehouse_address, CNTRY.country_name,CT.city_name,ST.state_name, c.category_name,i.item_name, i.item_code, i.item_grace AS warranty_grace_period, i.item_prodata AS warranty_prorata, i.item_warranty AS warranty_period, datediff(NOW(),WS.created_datetime) AS Ageing, COUNT(*) AS ttlStock, 
					CASE  
				 		WHEN datediff(NOW(),WS.dispatch_date) > 90 
				 		THEN 
							CASE 
							WHEN   datediff(NOW(),WS.stk_recharged_date) < 90 
				                THEN  'Brown'
				                ELSE  'Recharge'  
				            END
						 WHEN  (datediff(NOW(),WS.dispatch_date)>=60  AND datediff(NOW(),WS.dispatch_date)<90) 
						 THEN 'Green'
						 WHEN  (datediff(NOW(),WS.dispatch_date)>=30 AND datediff(NOW(),WS.dispatch_date)<60) 
						 THEN 'Yellow'
						 WHEN  (datediff(NOW(),WS.dispatch_date)>=0 AND datediff(NOW(),WS.dispatch_date)<30) 
						 THEN 'Red'
						 END AS FIFO_STATUS   ",'',$whereItemWise);
	
	if(is_array($auRec)){

		foreach ($auRec as $key => $value)
		{
			switch ($value->FIFO_STATUS)
 			{
				case 'Red':
					# code...
					$className = "bg-danger text-white"; 
					$status = "Fresh Stock"; 
					$hideTheCheckbox = "disabled"; 
					$textColor       ="color:white; font-weight:bold;";

					break;

				case 'Yellow':
					# code...
					$className = "bg-warning text-white"; 
					$status = "Maintain FIFO"; 
					$hideTheCheckbox = "disabled"; 
					$textColor       ="color:white; font-weight:bold;";
					break;

				case 'Green':
					# code...
					$className = "bg-success text-white"; 
					$status = "Need to be dispatch "; 
					$hideTheCheckbox = "disabled"; 
					$textColor       ="color:white; font-weight:bold;";
					break;

				case 'Brown':
					# code...
					$className = " bg-brown text-white"; 
					$status = "Recharged(Dispatch Firstly)"; 
					$hideTheCheckbox = "disabled"; 
					$textColor       ="color:white; font-weight:bold;";
					break;

				case 'Recharge':
					# code...
					$className = " bg-primary text-white"; 
					$status = "Need to be recharge"; 
					$hideTheCheckbox = "disabled"; 
					$textColor       ="color:white; font-weight:bold;";
					break;


				
				default:
					# code...
					$className = ""; 
					$status = ""; 
					$hideTheCheckbox = ""; 
					break;
			}
			
			// Warehouse Name
			$data.='"'.ucwords(addslashes($value->warehouse_name)).'"';
			$data.="\t";

			// Warehouse Code
			 $data.='"'.ucwords(addslashes($value->warehouse_code)).'"';
			 $data.="\t";
	 

			 // Country
			 $data.='"'.ucwords(addslashes($value->country_name)).'"';
			 $data.="\t";

			// State
			 $data.='"'.ucwords(addslashes($value->state_name)).'"';
			 $data.="\t";


			// City
			$data.='"'.ucwords(addslashes($value->city_name)).'"';
			$data.="\t";


			// Item Name
			$data.='"'.ucwords(addslashes($value->item_name)).'"';
			$data.="\t";


			// Item Code
			$data.='"'.ucwords(addslashes($value->item_code)).'"';
			$data.="\t";

			// Total BSN
			$data.='"'.ucwords(addslashes($value->ttlStock)).'"';
			$data.="\t";

			// Category
			$data.='"'.ucwords(addslashes($value->category_name)).'"';
			$data.="\t";

			// FIFO Status
			 $data.='"'.ucwords(addslashes($status)).'"';
			 $data.="\t";

			

			$data.="\n";
								
			

		}	

	}

	header("Content-type: application/octet-stream");
	header("Content-Disposition: attachment; filename=\"warehouse_actual_stock_list.xls\"");		
	echo $data;
	exit;
}




/*********************************************************
* DESC: Export Actual Stock at Warehouse FIFO
* Author: Chirag@2017-11-13
*
*********************************************************/



/************************ Start Export Cleaner Feedback By Chirag@29-12-2017  *******************************/			
if(isset($_GET['export_cleaner_feedback'])){		
	
	$data="Feedback\t Booking Number \t Cleaner Code \t Cleaner Name \t Cleaner Contact No.\t Cleaner Email\t Rating \t Date\n";

	if($_SESSION['FromCleanerFeedback']!=''){
        $fromdate=" tsf.created_date >= '".date('Y-m-d', strtotime($_SESSION['FromCleanerFeedback']))."'";
        $where="$fromdate ";
    }
    if($_SESSION['ToCleanerFeedback']!=''){
        $todate=" and  tsf.created_date <= '".date('Y-m-d', strtotime($_SESSION['ToCleanerFeedback']))."'";
        $where.="$todate AND ";
    }
	
	$where .= " tsf.feedback_type='cleaner' AND tsf.cleaner_id!=0";

	if($_SESSION['userLoginType'] == 5 && isset($_SESSION['supervisor_id']) && !empty($_SESSION['supervisor_id']))
	{
        $where .= " and tc.supervisor_id = '".$_SESSION['supervisor_id']."'";
    }
	
	$auRec=$_objAdmin->_getSelectList2('table_service_feedback as tsf left join table_booking_register as tbr on tbr.booking_id=tsf.booking_id left join table_cleaner as tc on tc.cleaner_id=tsf.cleaner_id',"tc.cleaner_id,tc.cleaner_code,tc.cleaner_name,tc.cleaner_phone_no,tc.cleaner_email,tsf.*,tbr.booking_id",$rp,$where." order by tsf.feedback_id desc",'');
	
	if(is_array($auRec)){
		for($i=0;$i<count($auRec);$i++){
		$data.="".$auRec[$i]->comments."\t".$auRec[$i]->booking_id."\t".$auRec[$i]->cleaner_code."\t".$auRec[$i]->cleaner_name."\t".$auRec[$i]->cleaner_phone_no."\t".$auRec[$i]->cleaner_email."\t".$auRec[$i]->star_rating."\t".date('d M Y',strtotime($auRec[$i]->created_date))."\n";
		}
						
	}
	header("Content-type: application/octet-stream");
	header("Content-Disposition: attachment; filename=\"cleaner_feedbacks.xls\"");		
	echo $data;
	exit;
	}
/************************ Start Export Cleaner Feedback By Chirag@29-12-2017  *******************************/	



/************************ Start Export Customer Feedback By Chirag@29-12-2017  *******************************/			
if(isset($_GET['export_customer_feedback'])){		
	
	$data="Feedback\t Booking Number \t Customer Name \t Customer Contact No.\t Customer Email\t Rating \t Date\n";
	if($_SESSION['FromCustomerFeedback']!=''){
        $fromdate=" tsf.created_date >= '".date('Y-m-d', strtotime($_SESSION['FromCustomerFeedback']))."'";
         $where ="$fromdate ";
    }
    if($_SESSION['ToCustomerFeedback']!=''){
        $todate=" and  tsf.created_date <= '".date('Y-m-d', strtotime($_SESSION['ToCustomerFeedback']))."'";
        $where .="$todate AND ";
    }
    
	$where .= " tsf.feedback_type='customer' AND tsf.customer_id!=0";
	
	$auRec=$_objAdmin->_getSelectList2('table_service_feedback as tsf left join table_booking_register as tbr on tbr.booking_id=tsf.booking_id left join table_customer as tc on tc.customer_id=tsf.customer_id',"tc.customer_id,tc.customer_name,tc.customer_number,tc.customer_email,tsf.*,tbr.booking_id",$rp,$where." order by tsf.feedback_id desc",'');
	
	if(is_array($auRec)){
		for($i=0;$i<count($auRec);$i++){
		$data.="".$auRec[$i]->comments."\t".$auRec[$i]->booking_id."\t".$auRec[$i]->customer_name."\t".$auRec[$i]->customer_number."\t".$auRec[$i]->customer_email."\t".$auRec[$i]->star_rating."\t".date('d M Y',strtotime($auRec[$i]->created_date))."\n";
		}
						
	}
	header("Content-type: application/octet-stream");
	header("Content-Disposition: attachment; filename=\"customer_feedbacks.xls\"");		
	echo $data;
	exit;
	}
/************************ Start Export Customer Feedback By Chirag@29-12-2017  *******************************/	


/************************ Start Export Customer Payment History By Chirag@29-12-2017  **********************/			
if(isset($_GET['export_customer_payment_history'])){		
	
	$data="Payment Mode\t Booking No \t Customer Name \t Customer Contact No.\t Customer Email\t Amount \t Date\n";
	 $where='';
	 if($_SESSION['FromPaymentHistory']!=''){
        $fromdate=" tph.created_date >= '".date('Y-m-d', strtotime($_SESSION['FromPaymentHistory']))."'";
        $where.=" $fromdate ";
    }
    if($_SESSION['ToPaymentHistory']!=''){
        $todate="and  tph.created_date <= '".date('Y-m-d', strtotime($_SESSION['ToPaymentHistory']))."'";
        $where.=" $todate AND";
    }
	$where.=" tph.status='A'";
	
	$auRec=$_objAdmin->_getSelectList2('table_payment_history as tph left join table_customer as CUST on CUST.customer_id = tph.customer_id left join table_booking_register as REGISTER on REGISTER.payment_id = tph.payment_id',"tph.payment_id,tph.customer_id,tph.payment_mode,tph.created_date,tph.payment_mode,tph.	transaction_status,tph.amount,CUST.customer_name,CUST.customer_number,CUST.customer_email,REGISTER.booking_id",$rp,$where.$sort,'');
	
	if(is_array($auRec)){
		for($i=0;$i<count($auRec);$i++){
		$data.="".$auRec[$i]->payment_mode."\t".$auRec[$i]->booking_id."\t".$auRec[$i]->customer_name."\t".$auRec[$i]->customer_number."\t".$auRec[$i]->customer_email."\t".$auRec[$i]->amount."\t".date('d M Y',strtotime($auRec[$i]->created_date))."\n";
		}
						
	}
	header("Content-type: application/octet-stream");
	header("Content-Disposition: attachment; filename=\"customer_payment_history.xls\"");		
	echo $data;
	exit;
	}
/************************ Start Export Customer Payment History By Chirag@29-12-2017 



/************************ Start Export Customer Customer Enquiries By Chirag@29-12-2017  **********************/			
if(isset($_GET['export_customer_enquiries'])){		
	
	$data="Message\t Query Type \t Customer Name \t Customer Contact No.\t Customer Email\t Customer Address \t Date\n";

	 $where='';
    if($_SESSION['FromEnquiryList']!=''){
        $fromdate=" QUERY.created_date >= '".date('Y-m-d', strtotime($_SESSION['FromEnquiryList']))."'";
        $where .="$fromdate ";
    }
    if($_SESSION['ToEnquiryList']!=''){
        $todate=" and QUERY.created_date <= '".date('Y-m-d', strtotime($_SESSION['ToEnquiryList']))."'";
        $where .="$todate AND ";
    }
	$where .= " QUERY.status !='D' AND AddR.default_address=1";
	
	$auRec=$_objAdmin->_getSelectList2('table_contact_query as QUERY left join table_contact_type_master as MASTER on MASTER.query_id = QUERY.query_id left join table_customer as CUST on CUST.customer_id = QUERY.customer_id left join table_customer_address as AddR on AddR.customer_id=CUST.customer_id left join table_buildings as tb on tb.building_id=AddR.building_id left join table_area as ta on ta.area_id=tb.area_id left join city as c on c.city_id=ta.city_id left join city as c2 on c2.city_id=AddR.city_id left join table_area as ta2 on ta2.area_id=AddR.area_id',"QUERY.*,MASTER.query_type_text,CUST.customer_name,CUST.customer_email,CUST.customer_email,CUST.customer_number,AddR.*,tb.building_name,ta.area_name,c.city_name,c2.city_name as city2,ta2.area_name as area_name2",$rp,$where,'');
	if($auRec[$i]->building_id==0){
							
		if($auRec[$i]->area_id==0){
			//custom address
			$address=$auRec[$i]->customer_address.','.$auRec[$i]->city2;


		}else{
			//Villa address
			$address=$auRec[$i]->customer_address.','.$auRec[$i]->area_name2.','.$auRec[$i]->city2;

		}
	}else{
		//Building address

		$address=$auRec[$i]->customer_address.','.$auRec[$i]->building_name.','.$auRec[$i]->area_name.','.$auRec[$i]->city_name;
	}
	if(is_array($auRec)){
		for($i=0;$i<count($auRec);$i++){
		$data.="".$auRec[$i]->message."\t".$auRec[$i]->query_type_text."\t".$auRec[$i]->customer_name."\t".$auRec[$i]->customer_number."\t".$auRec[$i]->customer_email."\t".str_replace(',', '', $auRec[$i]->address)."\t".date('d M Y',strtotime($auRec[$i]->created_date))."\n";
		}
						
	}
	header("Content-type: application/octet-stream");
	header("Content-Disposition: attachment; filename=\"customer_enquiries.xls\"");		
	echo $data;
	exit;
	}
/************************ Start Export Customer Customer Enquiries By Chirag@29-12-2017  **********************/


/*********************************************************

Booking export
*********************************************************/


if(isset($_GET['export_bookings'])){	
	

	$data = "Booking No\t Customer Name\t Cleaning Actual Cost \t  Total Cost Paid \t Cleaning Type\t Unit Type \t Building Name\t Customer Address\t Package Start Date \t Package End Date\t Room Detail \t Status\n ";

	$where = ""; 
	if($_SESSION['bookingStatus']!=NULL){
    if($_SESSION['bookingStatus']=='U'){//UNpaid check
        $bookingStatus="ba.booking_status!='CB' AND tph.payment_mode='cash' ";
    }else if($_SESSION['bookingStatus']=='P'){//Paid Check
        $bookingStatus="ba.booking_status='CB' OR tph.payment_mode!='cash' ";
    }else{
        $bookingStatus="ba.booking_status='".$_SESSION['bookingStatus']."' ";
    }
    }else{
        $bookingStatus='';
    }
    if(isset($bookingStatus) && $bookingStatus != ""){
        $where.= "$bookingStatus"; 
    }
   
    

     if($_SESSION['FromBookingList']!=''){
        $fromdate=" AND br.packg_start_date >= '".date('Y-m-d', strtotime($_SESSION['FromBookingList']))."'";
        $where.="$fromdate ";
    }
    if($_SESSION['ToBookingList']!=''){
        $todate=" and br.packg_start_date <= '".date('Y-m-d', strtotime($_SESSION['ToBookingList']))."'";
        $where.="$todate ";
    } 
    if(isset($_SESSION['booking_search']) && !empty($_SESSION['booking_search'])){
    	$where .= " and (br.booking_id like '%" . $search . "%' or cus.customer_name like '%" . $search . "%'". "or cus.customer_number like '%" . $search . "%' or br.total_cost_paid like '%" . $search . "%'". "or b.building_name like '%" . $search . "%')";
    }
    if($_SESSION['userLoginType'] == 5 && isset($_SESSION['supervisor_id']) && !empty($_SESSION['supervisor_id'])){
        $auRec=$this->_getSelectList2('table_supervisor',"*",'',"supervisor_id = '".$_SESSION['supervisor_id']."'",'');
        $where .= " and b.building_id = '".$auRec[0]->building_id."'";
    }
    if(isset($bookingStatus) && $bookingStatus == ""){
        $where .=" Group by br.booking_id";
    } 
	$select = "table_booking_register AS br "
                ."LEFT JOIN table_booking_details AS bd ON br.booking_id = bd.booking_id "
                ."JOIN "
                ."("
                ."SELECT GROUP_CONCAT( CONCAT(brt.counter,' ',tct.room_category)) AS room_detail,brt.booking_id,ut.`unit_name` "
                ."FROM   table_booking_room_type_mapping AS brt "
                ."LEFT JOIN table_category_type AS tct ON tct.room_category_id=brt.room_category_id "
                ."LEFT JOIN table_unit_type AS ut ON ut.unit_type_id = tct.unit_type_id "
                ."GROUP BY brt.booking_id ) xx ON bd.booking_id = xx.booking_id "
                ."LEFT JOIN table_booking_allotment AS ba ON bd.booking_detail_id = ba.booking_detail_id "
                ."LEFT JOIN  table_calender_slots AS cs ON cs.slot_id = bd.slot_id "
               
                ."LEFT JOIN table_cleaner AS c ON c.cleaner_id = bd.cleaner_id "
                ."LEFT JOIN table_supervisor AS s ON s.supervisor_id = c.supervisor_id "
                ."LEFT JOIN table_customer AS cus ON cus.customer_id = br.customer_id "
                ."LEFT JOIN table_cleaning_type AS ct ON ct.cleaning_type_id = br.cleaning_type_id "
                ."LEFT JOIN table_customer_address AS ca ON ca.customer_address_id = br.customer_address_id "
                 ."LEFT JOIN table_buildings AS b ON b.building_id = ca.building_id "
                ."LEFT JOIN table_calender_slots AS cst ON cst.slot_id = bd.slot_id "
                ."LEFT JOIN table_reject_reason AS rs ON  rs.reject_reason_id = ba.reject_reason_id "
		."LEFT JOIN table_payment_history AS tph ON  tph.payment_id=br.payment_id ";

    $fields = " br.booking_id, bd.booking_detail_id,cus.customer_name,br.cleaning_actual_cost,br.total_cost_paid,br.packg_start_date,br.packg_end_date,ca.customer_address,"
               ."b.building_name,ct.cleaning_type,cst.slot_end_time,cst.slot_start_time,bd.cleaning_date,c.cleaner_name,"
               ."s.supervisor_name,ba.booking_alloted,ba.booking_alloted_date,ba.not_alloted_reason,ba.booking_accepted,"
               ."ba.booking_accepted_date,ba.other_rejection_reason,rs.reject_reason,ba.booking_status,br.status,"
               ."xx.room_detail,xx.unit_name";

	$auRec=$_objAdmin->_getSelectList($select,$fields,'',$where,'');

	if(is_array($auRec)){
		"Booking No\t Customer Name\t Cleaning Actual Cost \t  Total Cost Paid \t Cleaning Type\t Unit Type \t Building Name\t Customer Address\t Package Start Date \t Package End Date\t Room Detail \t Status\n ";

		for($i=0;$i<count($auRec);$i++){
			$status=($auRec[$i]->status=='A')?"Active":"Inactive";
			
			$data.='"'.ucwords(addslashes($auRec[$i]->booking_id)).'"';
			$data.="\t";

			
			 $data.='"'.ucwords(addslashes($auRec[$i]->customer_name)).'"';
			 $data.="\t";

			
			 $data.='"'.ucwords(addslashes($auRec[$i]->cleaning_actual_cost)).'"';
			 $data.="\t";


			
			$data.='"'.ucwords(addslashes($auRec[$i]->total_cost_paid)).'"';
			$data.="\t";


		
			$data.='"'.ucwords(addslashes($auRec[$i]->cleaning_type)).'"';
			$data.="\t";

			
			 $data.='"'.ucwords(addslashes($auRec[$i]->unit_name)).'"';
			 $data.="\t";

			 
			 $data.='"'.ucwords(addslashes($auRec[$i]->building_name)).'"';
			 $data.="\t";

			 
			 $data.='"'.ucwords(addslashes($auRec[$i]->customer_address)).'"';
			 $data.="\t";

			
			 $data.='"'.ucwords(addslashes($auRec[$i]->packg_start_date)).'"';
			 $data.="\t";

			
			 $data.='"'.ucwords(addslashes($auRec[$i]->packg_end_date)).'"';
			 $data.="\t";

			 $data.='"'.ucwords(addslashes($auRec[$i]->room_detail)).'"';
			 $data.="\t";

			 
			 $data.='"'.ucwords(addslashes($status)).'"';
			 $data.="\t";

			$data.="\n";
								
			

		}	

	}

	header("Content-type: application/octet-stream");
	header("Content-Disposition: attachment; filename=\"Booking.xls\"");		
	echo $data;
	exit;
}
?>
