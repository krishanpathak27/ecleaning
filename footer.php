<script src="assets/vendors/base/vendors.bundle.js" type="text/javascript"></script>
		<script src="assets/demo/default/base/scripts.bundle.js" type="text/javascript"></script>
		<!--end::Base Scripts -->   
        <!--begin::Page Vendors -->
		<!--<script src="assets/vendors/custom/fullcalendar/fullcalendar.bundle.js" type="text/javascript"></script>-->
        <script src="assets/vendors/custom/fullcalendar/moment.min.js" type="text/javascript"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.9.0/fullcalendar.min.js" type="text/javascript"></script>
        <script src="assets/vendors/custom/fullcalendar/scheduler.min.js" type="text/javascript"></script>
        
		<!--end::Page Vendors -->  
       
      
        <!--begin::Page Resources -->
		<script src="assets/demo/default/custom/components/forms/widgets/bootstrap-datepicker.js" type="text/javascript"></script>
        <script src="assets/demo/default/custom/components/forms/widgets/bootstrap-datetimepicker.js" type="text/javascript"></script>
        <script src="assets/demo/default/custom/components/forms/widgets/bootstrap-timepicker.js" type="text/javascript"></script>
        
          <!--begin::Page Snippets -->
		<script src="assets/app/js/dashboard.js" type="text/javascript"></script>
       
       
        <script src="assets/app/js/components-date-time-pickers.min.js" type="text/javascript"></script>
		<!--end::Page Snippets -->  
<?php 
  
   $pageName=basename($_SERVER['PHP_SELF']); 
   $obj_arraylist =new ArrayList;
   $obj_getselect=new Db_Action;
   $_objAdmin = new Admin();
   $accountid="account_id=".$_SESSION['accountId']."";
   //$useraccess=$obj_getselect->_getselectList('table_page_access','*','',$accountid);
  $pageAccessID=$_objAdmin->_getselectList('table_module_enable','status',''," feature_id='2'");		
  $access_enable=$pageAccessID[0]->status;
   if($access_enable=="1")
   {
  
   $userTypeSalesTeamID = $obj_arraylist->getsaleteamID();  
   //print_r($userTypeSalesTeamID);

   if($_SESSION['userLoginType'] == 5) {
	 $hierarchyIDofloggedUser = $obj_arraylist->getHierarchyIDoFSalesTeam($userTypeSalesTeamID[0]);  
	 $hierarchyCondition = " and hierarchy_id='".$hierarchyIDofloggedUser."'";
   }
  
  //print_r($hierarchyIDofloggedUser);
   $pageID=$obj_getselect->_getselectList2('table_cms','id',''," page_name='".$pageName."' and status!='D'"); 
   //print_r($pageID); 
   //$pageAccessID=$obj_getselect->_getselectList('table_module_enable','status',''," feature_id='2'"); 
   //$access_enable=$pageAccessID[0]->status;
    
   $userType = " user_type='".$_SESSION['userLoginType']."' $hierarchyCondition AND  page_privilege_id = '".$pageID[0]->id."'";
   $pageGroup=$obj_getselect->_getselectList('table_page_access AS A INNER JOIN table_cms AS C ON C.id = A.page_privilege_id','page_privilege_id,permisson_id, C.page_name, C.id',''," $userType");  
   
   //for edit page not access.

     $add=0;
	 $edit=0;
	 $del=0;
	 $imp=0;
  
        if($pageGroup[0]->permisson_id!=""){
     
	     $allowedAccess = explode( ',',$pageGroup[0]->permisson_id);
         // for add only.


		// AJAY@2017-04-03
		// Commercial Login
	    $getHierarchy = $_objAdmin->_getSelectList2('table_account_admin','hierarchy_id','',' operator_id = "'.$_SESSION['operatorId'].'"');
		if($_SESSION['userLoginType'] == 2 && $getHierarchy[0]->hierarchy_id==9) {
		    $pageaccess = array(1,2,3,4);
		}
		else if($_SESSION['userLoginType'] == 2 && $getHierarchy[0]->hierarchy_id!=9) {
		    $pageaccess = array(2,3,4);
		}
		 else {
			 $pageaccess = array(1,2,3,4);
		}



		// $pageaccess = array(1,2,3,4);
         $result = array_intersect($pageaccess,$allowedAccess);
		
		 $data = array_values($result);
		  //print_r($data);
		 for($i=0; $i<count($data); $i++)
		 {
		 if($data[$i]==1)
		 {
		 $add=1;
		 }
		 
		 
		 elseif($data[$i]==2)
		 {
		 $edit=1;
		 	 
		 }
		 
		 
		 elseif($data[$i]==3)
		 {
		 $del=1;	 
		 }
		
		 
		 
		 elseif($data[$i]==4)
		 {
		 $imp=1;	 
		 }	 
		
	 
		 }
		
		
		 
	 }
	 
	    //echo $add; 
		//echo $edit; 
		//echo $del;
    	//echo $imp; 
	
				
 // return 1 is access, 0 is not access	
 
 	
?>
<script type="text/javascript">
<?php if($_SESSION['userLoginType']!='0') { ?>
	 $(document).ready(function () {
        ButtonmenuAccess(add=<?php echo $add; ?>,edit=<?php echo $edit; ?>,del=<?php echo $del; ?>,importt=<?php echo $imp; ?>);
    });
function ButtonmenuAccess(add,edit,del,importt){
	//alert(importt);
	if(add==0)
	{
   $(".add").remove();
   $(".btnseparator:nth-child(2)").remove();
   }
  else
  {
  $(".add").show();
  $(".btnseparator:nth-child(2)").show();	  
  }
  if(edit==0)
  {
 $(".edit").remove();
 $(".btnseparator:nth-child(2)").remove();
 $(document).keydown(function(e){
   var code = e.keyCode ? e.keyCode : e.which;
   if(code==13){
    return false;
   }
});	
  
  }
  else
  { 
  $(".edit").show();
  $(".btnseparator:nth-child(3)").show();	  
  }
   
 if(del==0)
 {
$(".delete").remove();
$(".btnseparator:nth-child(3)").remove();	 
 }
 else
 {
 $(".delete").show();
 $(".btnseparator:nth-child(3)").show();	 	 
 }
 
 if(importt==0)
 {
$("#related-activities").remove();
}
else
{
$("#related-activities").show();
}
 
 
  
 }
function closeMenu() {
    $('#page-top').removeClass('m-aside-left--on');
    $('#m_aside_left').removeClass('m-aside-left--on');
    $('.m-aside-left-overlay').hide();
 }			
 </script>
<?php } } ?>
    
			<footer class="m-grid__item	m-footer ">
				<div class="m-container m-container--fluid m-container--full-height m-page__container">
					<div class="m-stack m-stack--flex-tablet-and-mobile m-stack--ver m-stack--desktop">
						<div class="m-stack__item m-stack__item--left m-stack__item--middle m-stack__item--last">
							<span class="m-footer__copyright">
								2018 &copy; Pepupsales by
								<a href="http://www.quytech.com/" class="m-link" target = "_blank">
									Quytech
								</a>
							</span>
						</div> 
					</div>
				</div>
			</footer>
			<!-- end::Footer -->
		</div>
		<!-- end:: Page --> 	    
	    <!-- begin::Scroll Top -->
		<div class="m-scroll-top m-scroll-top--skin-top" data-toggle="m-scroll-top" data-scroll-offset="500" data-scroll-speed="300">
			<i class="la la-arrow-up"></i>
		</div>
		<!-- end::Scroll Top -->		    <!-- begin::Quick Nav -->
		 
		<!-- begin::Quick Nav -->	
    	<!--begin::Base Scripts -->
		
		<!--end::Page Resources -->
	</body>
	<!-- end::Body -->
</html>
