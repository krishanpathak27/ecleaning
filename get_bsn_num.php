<?php 
	include("includes/config.inc.php");
	include("includes/function.php");
	$_objAdmin = new Admin();
	$_objItem = new Item();
	$objArrayList= new ArrayList();
	$objStock  = new StockClass;
	// header('Content-Type: application/json');
	if($_REQUEST['type']=='A' && isset($_REQUEST['selected_JCN']) && !empty($_REQUEST['selected_JCN']))
	{

		$result=$_objAdmin->_getSelectList2('table_wh_mrn_bcf_jobcard as JC 
		left join table_wh_mrn_bcs as BCS on BCS.jobcard_id = JC.jobcard_id 
		left join table_category as cat on cat.category_id = BCS.rpl_battery_category_id 
		left join table_brands as brand on BCS.rpl_battery_brand_id = brand.brand_id 
		left join table_wh_mrn_bcf as BCF on JC.bcf_id=BCF.bcf_id 
		LEFT JOIN table_item_bsn AS BSN ON BSN.bsn_id = BCF.bsn_id
		LEFT JOIN table_item as I on BSN.item_id = I.item_id
		LEFT JOIN table_distributors AS D ON D.distributor_id = BCF.service_distributor_id
		LEFT JOIN table_wh_mrn_detail AS MRND ON MRND.mrn_detail_id = BCF.mrn_detail_id
		LEFT JOIN table_wh_mrn AS MRN ON MRN.mrn_id = MRND.mrn_id
		LEFT JOIN table_complaint as COMPLAINT on COMPLAINT.product_serial_no = BSN.bsn
		LEFT JOIN table_item_bsn as BSND on BSND.bsn = BCS.rpl_battery_distributor
		LEFT JOIN table_item as DI on DI.item_id = BSND.item_id', "BCS.bcs_number, BCS.bcs_date, brand.brand_name,cat.category_name, BCF.bcf_id,MRN.mrn_id,BCF.bsn_id, MRN.mrn_no, MRN.mrn_date, BCF.bcf_no, BCF.item_id, BCF.service_distributor_id, D.distributor_name, BCF.bcf_date, JC.jobcard_no, JC.jobcard_date, JC.is_goodwill, JC.goodwill_approve, BCF.bsn, BSN.item_id, BSN.date_of_sale, JC.battery_sts,BSN.warranty_period,JC.test_report_action,COMPLAINT.replace_battery_bsn,BCS.rpl_battery_distributor,DI.model_code as battery_model_distributor,I.model_code as battery_model",''," JC.jobcard_id=".$_REQUEST['selected_JCN']);

		//print_r($result);

			foreach ($result as $key => $fetch) {
				# code...

			if($fetch->bcf_date == '1970-01-01'  || $fetch->bcf_date == '0000-00-00' || $fetch->bcf_date == '')
					$bcf_date = "";
				else 
					$bcf_date = $objStock->dateFormatter($fetch->bcf_date);


				if($fetch->mrn_date == '1970-01-01'  || $fetch->mrn_date == '0000-00-00' || $fetch->mrn_date == '')
					$mrn_date = "";
				else 
					$mrn_date = $objStock->dateFormatter($fetch->mrn_date);

			if($fetch->jobcard_date == '1970-01-01'  || $fetch->jobcard_date == '0000-00-00' || $fetch->jobcard_date == '')
					$jobcard_date = "";
				else 
					$jobcard_date = $objStock->dateFormatter($fetch->jobcard_date);


			// Claim Slab Calculation AJAY@2017-04-13



			 $date_of_sale =  $fetch->date_of_sale;
			 $item_id      =  $fetch->item_id;
			 $warranty_period = $fetch->warranty_period;


			if($date_of_sale!='' && $date_of_sale!='1970-01-01' && $date_of_sale!='0000-00-00' && $item_id>0) {
				// echo "hello";
				// exit;

				//$usedBatteryInMonths = strtotime(date('Y-m-d'))-strtotime($auRec[$i]->date_of_sale) / (60*60*24);	

				$datetime1 = date_create(date('Y-m-d'));

				$datetime2 = date_create($date_of_sale);

				$interval = date_diff($datetime1, $datetime2);

				//$usedBatteryInMonths = $interval->format('%m month');
				$usedBatteryInDays = $interval->format('%a');
				$usedBatteryInMonths = ceil($usedBatteryInDays  / 30)	;

				$date_of_sale = $objStock->dateFormatter($date_of_sale);

				$objStock = new StockClass;
				$prorataMSG      = "";
				$prorataDiscount = "";

				if($fetch->is_goodwill!='Y' || $fetch->goodwill_approve!='Y') {

					//if($usedBatteryInMonths+1>$auRec[$i]->warranty_period && $usedBatteryInMonths+1<$auRec[$i]->warranty_prorata )
					$claimSlabWithDiscount = 	$objStock->getBatteryProRataCliamSlabDetail($item_id, $usedBatteryInMonths, $warranty_period);
					if(sizeof($claimSlabWithDiscount)>0) {
						$prorataMSG = $claimSlabWithDiscount['msg'];
						$prorataDiscount = $claimSlabWithDiscount['claimDiscount'];
					}
					//	print_r($claimSlabWithDiscount);
				}
				//	print_r($claimSlabWithDiscount);

			} 

			// Claim Slab Calculation AJAY@2017-04-13


			// Prorata check AJAY@2017-04-14
			$is_goodwill = "";
			 if(strtolower($fetch->is_goodwill) =='y' && strtolower($fetch->goodwill_approve)=='y') {

			 	$is_goodwill = 'Y';
					
			 }

			 // Prorata check AJAY@2017-04-14


				echo json_encode(array('bcf_no'=>$fetch->bcf_no, 'bsn_id'=>$fetch->bsn_id, 'item_id'=>$fetch->item_id, 'bcf_date'=>$bcf_date, 'jobcard_no'=>$fetch->jobcard_no, 'jobcard_date'=>$jobcard_date, 'mrn_no'=>$fetch->mrn_no, 'service_distributor_id'=>$fetch->service_distributor_id, 'service_distributor_name'=>htmlspecialchars($fetch->distributor_name, ENT_QUOTES), 'mrn_date'=>$mrn_date, 'defective_battery_bsn'=>$fetch->bsn, 'prorata'=>$prorataMSG, 'prorataDiscount'=>$prorataDiscount, 'is_goodwill'=>$is_goodwill,'rpl_battery_action'=>$fetch->test_report_action,'rpl_battery_bsn'=>$fetch->replace_battery_bsn,'replacement_type'=>$fetch->battery_sts,'rpl_battery_distributor'=>$fetch->rpl_battery_distributor,'battery_model_distributor'=>$fetch->battery_model_distributor,'battery_model'=>$fetch->battery_model));
				break;
			}
	}


	if($_POST['type']=='C')
	{
		//$result = mysql_query("select  from table_item_bsn AS BSN LEFT JOIN table_item AS I ON I.item_id = BSN.item_id where BSN.bsn='".$_POST['filled_bsn']."'");

		$model_code = substr($_REQUEST['filled_bsn'],8,4);
		$result=$_objAdmin->_getSelectList2('table_item_bsn AS BSN 
		 LEFT JOIN table_item AS I ON I.item_id = BSN.item_id and I.model_code="'.$model_code.'"', "I.category_id, I.brand_id,I.model_code",''," BSN.bsn='".$_REQUEST['filled_bsn']."'");

		
		if(sizeof($result) <= 0)
		{
			
			echo 'false';
			
		}
		else
		{
			//echo 'true';
			foreach ($result as $key => $fetch) {
			echo json_encode(array('rpl_battery_category_id'=>$fetch->category_id, 'rpl_battery_color_id'=>$fetch->color_id, 'rpl_battery_brand_id'=>$fetch->brand_id,'battery_model'=>$fetch->model_code));
			}

		}
	}

	if($_POST['type']=='available')
	{
		if(isset($_POST['itemID']))
		{
			$itemCondition = " AND BSN.item_id ='".$_POST['itemID']."'";	
		}
		
		$aDis=$_objAdmin->_getSelectList('table_warehouse_stock AS WS 
		LEFT JOIN table_item_bsn AS BSN ON BSN.bsn_id = WS.bsn_id ',' WS.bsn_id, BSN.item_id, BSN.bsn, WS.status ',''," WS.warehouse_id='".$_SESSION['warehouseId']."' AND WS.status='A' $itemCondition ");
		foreach ($aDis as $key => $value) {
			echo json_encode(array('bsn'=>$value->bsn,'bsn_id'=>$value->bsn_id));	
		}
		
	}
?>