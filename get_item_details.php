<?php 
	include("includes/config.inc.php");
	include("includes/function.php");
	$_objAdmin = new Admin();
	$_objItem = new Item();
	$objArrayList= new ArrayList();
	$objStock = new StockClass();
	// header('Content-Type: application/json');

	$result = mysql_query("Select item.item_name,cat.category_name,brnd.brand_name,clr.color_desc,twmb.bsn, twmb.bcf_no, twmb.service_personnel_id, twmb.	service_distributor_id, bcf_date, MRN.mrn_no, MRN.mrn_date, SP.sp_name, SP.sp_code, D.distributor_name from table_wh_mrn_bcf as 
		twmb 
		LEFT JOIN table_wh_mrn_bcf_jobcard AS JC ON JC.bcf_id = twmb.bcf_id
		LEFT JOIN table_distributors AS D ON D.distributor_id = twmb.service_distributor_id
		LEFT JOIN table_service_personnel AS SP ON SP.service_personnel_id = twmb.service_personnel_id
		left join table_item as item on item.item_id = twmb.item_id 
		LEFT JOIN table_wh_mrn_detail AS MRND ON MRND.mrn_detail_id = twmb.mrn_detail_id
		LEFT JOIN table_wh_mrn AS MRN ON MRN.mrn_id = MRND.mrn_id
		left join table_category as cat on cat.category_id = item.category_id 
		left join table_brands as brnd on brnd.brand_id = item.brand_id 
		left join table_color as clr on clr.color_id = item.item_color 
		where twmb.bcf_id = '".$_POST['selected_bcf']."'");

	
	if(mysql_num_rows($result) > 0)
	{
		while($fetch = mysql_fetch_object($result))
		{
				//print_r($fetch);
		if($fetch->bcf_date == '1970-01-01'  || $fetch->bcf_date == '0000-00-00' || $fetch->bcf_date == '')
				$bcf_date = "";
			else 
				$bcf_date = $objStock->dateFormatter($fetch->bcf_date);


			if($fetch->mrn_date == '1970-01-01'  || $fetch->mrn_date == '0000-00-00' || $fetch->mrn_date == '')
				$mrn_date = "";
			else 
				$mrn_date = $objStock->dateFormatter($fetch->mrn_date);

			if($fetch->SPID!="" && $fetch->SPID>0)
				$service_personnel_id = $fetch->SPID;
			else 
				$service_personnel_id = $fetch->service_personnel_id;
				$service_distributor_id = $fetch->service_distributor_id;
				$sp_name = htmlspecialchars($fetch->sp_name, ENT_QUOTES);
				$distributor_name = htmlspecialchars($fetch->distributor_name, ENT_QUOTES);


			$goodwill = $_objAdmin->salesGoodwillCalc($_POST['selected_bcf']);
			

			echo json_encode(array('item_name' => $fetch->item_name, 'bcf_no'=>$fetch->bcf_no, 'bcf_date'=>$bcf_date, 'mrn_no'=>$fetch->mrn_no, 'mrn_date'=>$mrn_date, 'service_personnel_id'=>$service_personnel_id,  'sp_name'=>$sp_name, 'service_distributor_id'=>$service_distributor_id, 'distributor_name'=>$distributor_name, 'category_name'=> $fetch->category_name,'brand_name'=>$fetch->brand_name,'color_desc'=>$fetch->color_desc,'bsn'=>$fetch->bsn,'is_goodwill'=>$goodwill['is_goodwill'],'goodwill_msg' =>$goodwill['goodwill_msg']));
			break;
		}
	 }

	 if($_POST['type']=='distributor_details')
	 {
	 	$distributor_details = $_objAdmin->_getSelectList2('table_distributors as D left join state as ST on D.state = ST.state_id left join city as CT on CT.city_id = D.city','D.distributor_phone_no,D.distributor_code,ST.state_name,CT.city_name','',' D.distributor_id = "'.$_POST['selected_distributor'].'"');
	 	echo json_encode(array('distributor_code' => $distributor_details[0]->distributor_code, 'state'=>$distributor_details[0]->state_name, 'city'=>$distributor_details[0]->city_name, 'mobile'=>$distributor_details[0]->distributor_phone_no));
	 }
	 if($_POST['type']=='service_distributor_details')
	 {
	 	$distributor_details = $_objAdmin->_getSelectList2('table_distributors as D left join state as ST on D.state = ST.state_id left join city as CT on CT.city_id = D.city','D.distributor_phone_no,D.distributor_code,ST.state_name,CT.city_name','',' D.distributor_id = "'.$_POST['selected_distributor'].'"');
	 	echo json_encode(array('service_distributor_code' => $distributor_details[0]->distributor_code, 'service_state'=>$distributor_details[0]->state_name, 'service_city'=>$distributor_details[0]->city_name, 'service_mobile'=>$distributor_details[0]->distributor_phone_no));
	 }
?>