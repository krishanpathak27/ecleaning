<script type="text/javascript">
      google.load("visualization", "1", {packages:["corechart"]});
      google.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Month', 'Total Orders', 'Accepted Order', 'Rejected Order'],
          ['January',	2000,	1500,	500],
          ['February',	2400,	2000,	400],
          ['March',	3000,	2000,	1000],
          ['April',	2500,	2200,	300],
		  ['May',	2135,	2000,	135],
          ['June',	2170,	1500,	570],
          ['July',	2660,	2300,	360],
          ['August',	3030,	2500,	030],
		  ['September',	2135,	2135,	000],
          ['October',	3170,	3000,	170],
          ['November',	3660,	3120,	540],
          ['December',	000,	000,	000]
        ]);

        var options = {
          title: '<?php echo $auRet[0]->retailer_name; ?> Order Performance',
          hAxis: {title: 'Months', titleTextStyle: {color: '#d74343'}},
		  vAxis: {title: 'Order in RS.', titleTextStyle: {color: '#d74343'}}
        };

        var chart = new google.visualization.ColumnChart(document.getElementById('chart_div'));
        chart.draw(data, options);
      }
 </script>
 <div id="chart_div" style="width: 900px; height: 400px;"></div>