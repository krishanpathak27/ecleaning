<script type="text/javascript">
      google.load("visualization", "1", {packages:["corechart"]});
      google.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Distributor Name', 'Total Orders', 'Accepted Order', 'Rejected Order'],
          ['Amit Enterprises',	2135,	2000,	135],
          ['Akhil Infotech Pvt Ltd',	2000,	1800,	200],
          ['ABC Enterprises',	1995,	1800,	195]
        ]);

        var options = {
          title: 'Top 10 Distributor',
          hAxis: {title: 'Distributor Name', titleTextStyle: {color: '#d74343'}},
		  vAxis: {title: 'Order in RS.', titleTextStyle: {color: '#d74343'}}
        };

        var chart = new google.visualization.ColumnChart(document.getElementById('chart_div'));
        chart.draw(data, options);
      }
 </script>
 <div id="chart_div" style="width: 900px; height: 400px;"></div>