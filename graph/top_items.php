<script type="text/javascript">
      google.load("visualization", "1", {packages:["corechart"]});
      google.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Items Name', 'Total Units on sale'],
          ['1100W 2.0 cu. ft. OTR Microwave',	2500],
          ['Stainless Steel OTR Speed Oven',	2250],
          ['Samsung Galaxy Camera (AT&T 4G Connected)',	2000],
          ['Samsung Galaxy Tab 2 10.1 (Wi-Fi) 16GB',	1750],
		  ['Samsung Galaxy Note 16GB (Wi-Fi) 10.1',	1700],
          ['4GB microSD Card MB-MS4GA High Speed Series',	1690],
          ['SCX-3405FW Multi-Function Printer',	1600],
          ['25.6 cu. ft. French Door Refrigerator',	1590],
		  ['WB150F 14 Megapixel Samsung SMART Camera',	1550],
          ['32" Class (31.5" Diag.) LED 5000 Series TV',	1500]
        ]);

        var options = {
          title: 'Top 10 Items',
          hAxis: {title: 'Item Name', titleTextStyle: {color: '#d74343'}},
		  vAxis: {title: 'Total Units on sale', titleTextStyle: {color: '#d74343'}}
        };

        var chart = new google.visualization.ColumnChart(document.getElementById('chart_div'));
        chart.draw(data, options);
      }
 </script>
 <div id="chart_div" style="width: 900px; height: 400px;"></div>