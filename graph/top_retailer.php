<script type="text/javascript">
      google.load("visualization", "1", {packages:["corechart"]});
      google.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Retailer Name', 'Total Orders', 'Accepted Order', 'Rejected Order'],
          ['Laptop Bazar',	3500,	3000,	500],
          ['Singh Enterprises',	3400,	3000,	400],
          ['Kumar Enterprises',	3200,	3000,	200],
          ['Apna Bazar',	3000,	2700,	300],
		  ['Moolchand Bazar',	2800,	2500,	300],
          ['Z Enterprises',	2750,	2500,	250],
          ['Chandni Chowk Bazar',	2460,	2300,	160],
          ['Ajmere Gate Bazar',	2250,	2000,	250],
		  ['Shakarpur Bazar',	2150,	1900,	250],
          ['Vaishali Bazar',	2000,	1800,	200]
        ]);

        var options = {
          title: 'Top 10 Retailer',
          hAxis: {title: 'Retailer Name', titleTextStyle: {color: '#d74343'}},
		  vAxis: {title: 'Order in RS.', titleTextStyle: {color: '#d74343'}}
        };

        var chart = new google.visualization.ColumnChart(document.getElementById('chart_div'));
        chart.draw(data, options);
      }
 </script>
 <div id="chart_div" style="width: 900px; height: 400px;"></div>