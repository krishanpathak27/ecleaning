<script type="text/javascript">
      google.load("visualization", "1", {packages:["corechart"]});
      google.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Salesman Name', 'Total Orders', 'Accepted Order', 'Rejected Order'],
          ['Akhilesh',	3000,	2500,	500],
          ['Deepak',	2900,	2500,	400],
          ['Amit',	2800,	2500,	300]
        ]);

        var options = {
          title: 'Top 10 Salesman',
          hAxis: {title: 'Salesman Name', titleTextStyle: {color: '#d74343'}},
		  vAxis: {title: 'Order in RS.', titleTextStyle: {color: '#d74343'}}
        };

        var chart = new google.visualization.ColumnChart(document.getElementById('chart_div'));
        chart.draw(data, options);
      }
 </script>
 <div id="chart_div" style="width: 900px; height: 400px;"></div>