<?php require_once('../includes/config.inc.php');

 class chartClass  {
		
		public $_allow = array();
		public $_content_type = "application/json";
		public $_request = array();
		
		private $_method = "";		
		private $_code = 200;
		private $graphObj;
		private $salesmanCondition = "";


	public function __construct(){
		$this->graphObj = new GraphsClass();
	}








	/* * Dynmically call the method based on the query string  */
	public function processApi()
	{
		if(isset($_REQUEST['action']))
		{
			$func = strtolower(trim(str_replace("/","",$_REQUEST['action'])));
		if((int)method_exists($this,$func) > 0)
				$this->$func();
			else
				$error = array('status' => "Failed", "msg" => "Request Blank");
				$this->response($this->json($error),201); // If the request is blank.
		}
			else 
				{
					$error = array('status' => "Failed", "msg" => "Request Blank");
					$this->response($this->json($error),201); // If the request is blank.
				}
	}


	/* *	Encode array into JSON */

	private function json($data) {
		if(is_array($data)) {
			return json_encode($data);
		}
	}

		public function get_referer(){
			return $_SERVER['HTTP_REFERER'];
		}
		
		public function response($data,$status){
			$this->_code = ($status)?$status:200;
			$this->set_headers();
			echo $data;
			exit;
		}
		// For a list of http codes checkout http://en.wikipedia.org/wiki/List_of_HTTP_status_codes
		private function get_status_message(){
			$status = array(
						200 => 'Success',
						201 => 'Request Blank',  
						202 => 'Database Not Connected',  
						203 => 'Invalid Username',
						207 => 'Invalid User ID OR Authentication Key',  
						209 => 'Data Does Not Exist',  
						210 => 'Json Data Is Blank', 
						212 => 'Detail Already Exist In The System', 
						214 => 'Fields Can Not Be Empty', 
						404 => 'Not Found',  
						406 => 'Not Acceptable');
						
			return ($status[$this->_code])?$status[$this->_code]:$status[500];
		}
		
		public function get_request_method(){
			return $_SERVER['REQUEST_METHOD'];
		}
		
		private function inputs(){
			switch($this->get_request_method()){
				case "POST":
					$this->_request = $this->cleanInputs($_POST);
					break;
				case "GET":
				case "DELETE":
					$this->_request = $this->cleanInputs($_GET);
					break;
				case "PUT":
					parse_str(file_get_contents("php://input"),$this->_request);
					$this->_request = $this->cleanInputs($this->_request);
					break;
				default:
					$this->response('',406);
					break;
			}
		}		
		
		private function cleanInputs($data){
			$clean_input = array();
			if(is_array($data)){
				foreach($data as $k => $v){
					$clean_input[$k] = $this->cleanInputs($v);
				}
			}else{
				if(get_magic_quotes_gpc()){
					$data = trim(stripslashes($data));
				}
				$data = strip_tags($data);
				$clean_input = trim($data);
			}
			return $clean_input;
		}		
		
		private function set_headers(){
			header("HTTP/1.1 ".$this->_code." ".$this->get_status_message());
			header("Content-Type:".$this->_content_type);
			header("Access-Control-Allow-Origin: *");
		}


   // Salesman Attendance Maninder @2016-07-21

	public function salesmanAttendance() {

		$result = $this->graphObj->salesmanAttandance();
		//$moderateResult = (obj) array('ttlInvoiceAmt'=>'');

		if(is_array($result) && sizeof($result)>0) {

			$success = array('status' => "Success", "msg" => "", "data" => $result);
			$this->response($this->json($success), 200);
		} else {
			$error = array('status' => "Failed", "msg" => "Data Does Not Exist");
			$this->response($this->json($error),209);	
		}


	}



	// top 10 retailer AJAY@2016-03-01

	public function top10Retailers() {

		$result = $this->graphObj->topRetailers();
		//$moderateResult = (obj) array('ttlInvoiceAmt'=>'');

		if(is_array($result) && sizeof($result)>0) {

			// Set the top 10 retailer data inputs of graph
			// if(isset($result['data']['ttlAmnt']) &&  sizeof($result['data']['ttlAmnt'])>0) {
			// 	$moderateResult['ttlInvoiceAmt'] = explode(",", $result['data']['ttlAmnt']);
			// }

			$success = array('status' => "Success", "msg" => "", "data" => $result);
			$this->response($this->json($success), 200);
		} else {
			$error = array('status' => "Failed", "msg" => "Data Does Not Exist");
			$this->response($this->json($error),209);	
		}


	}

	// top 10 Distributors AJAY@2016-03-01

	public function top10Distributors() {

		$result = $this->graphObj->topDistributors();
		//$moderateResult = (obj) array('ttlInvoiceAmt'=>'');

		if(is_array($result) && sizeof($result)>0) {

			// Set the top 10 retailer data inputs of graph
			// if(isset($result['data']['ttlAmnt']) &&  sizeof($result['data']['ttlAmnt'])>0) {
			// 	$moderateResult['ttlInvoiceAmt'] = explode(",", $result['data']['ttlAmnt']);
			// }

			$success = array('status' => "Success", "msg" => "", "data" => $result);
			$this->response($this->json($success), 200);
		} else {
			$error = array('status' => "Failed", "msg" => "Data Does Not Exist");
			$this->response($this->json($error),209);	
		}


	}





	// dashBoard Analytic(Total Calls, Productive Calls, Total Added retailers & Total Amount Order) values AJAY@2016-03-02

	private function dashBoardTilesValues () {

		$result = $this->graphObj->dashBoardSummary();
		$success = array('status' => "Success", "msg" => "", "data" => $result);
		$this->response($this->json($success), 200);


	} // End of function




	// Order performance of this year

	private function orderPerformance () {

		$result = $this->graphObj->orderPerfomanceThisYear();

		if(is_array($result) && sizeof($result)>0) {
			$success = array('status' => "Success", "msg" => "", "data" => $result);
			$this->response($this->json($success), 200);
		} else {
			$error = array('status' => "Failed", "msg" => "Data Does Not Exist");
			$this->response($this->json($error),209);	
		}

	} // End of function

	// Order performance of this year

	private function orderPerformanceRet () {

		$result = $this->graphObj->orderPerfomanceThisYearRet();

		if(is_array($result) && sizeof($result)>0) {
			$success = array('status' => "Success", "msg" => "", "data" => $result);
			$this->response($this->json($success), 200);
		} else {
			$error = array('status' => "Failed", "msg" => "Data Does Not Exist");
			$this->response($this->json($error),209);	
		}

	} // End of function



	// distributor share of this year

	private function distributorsMarketShare () {

		 $result = $this->graphObj->distributorsMarketSharePerformance();

		if(is_array($result) && sizeof($result)>0) {
			$success = array('status' => "Success", "msg" => "", "data" => $result);
			$this->response($this->json($success), 200);
		} else {
			$error = array('status' => "Failed", "msg" => "Data Does Not Exist");
			$this->response($this->json($error),209);	
		}

	} // End of function





    //  Top 5 salesman of this Year 

	private function top5SalesmenDistributors () {

		 $result = $this->graphObj->top5SalesmenDistributors();

		if(is_array($result) && sizeof($result)>0) {
			$success = array('status' => "Success", "msg" => "", "data" => $result);
			$this->response($this->json($success), 200);
		} else {
			$error = array('status' => "Failed", "msg" => "Data Does Not Exist");
			$this->response($this->json($error),209);	
		}

	} // End of function


	 //  Top 5 salesman of this Year 

	private function top5SalesmenRetailers () {

		 $result = $this->graphObj->top5SalesmenRetailers();

		if(is_array($result) && sizeof($result)>0) {
			$success = array('status' => "Success", "msg" => "", "data" => $result);
			$this->response($this->json($success), 200);
		} else {
			$error = array('status' => "Failed", "msg" => "Data Does Not Exist");
			$this->response($this->json($error),209);	
		}

	} // End of function




    //  Top 10 products of this Year Distributors

	private function topProductsDistributors () {

		 $result = $this->graphObj->top10ProductsDistributors();

		if(is_array($result) && sizeof($result)>0) {
			$success = array('status' => "Success", "msg" => "", "data" => $result);
			$this->response($this->json($success), 200);
		} else {
			$error = array('status' => "Failed", "msg" => "Data Does Not Exist");
			$this->response($this->json($error),209);	
		}

	} // End of function

	//  Top 10 products of this Year  Retailers

	private function topProductsRetailers () {

		 $result = $this->graphObj->top10ProductsRetailers();

		if(is_array($result) && sizeof($result)>0) {
			$success = array('status' => "Success", "msg" => "", "data" => $result);
			$this->response($this->json($success), 200);
		} else {
			$error = array('status' => "Failed", "msg" => "Data Does Not Exist");
			$this->response($this->json($error),209);	
		}

	} // End of function







    //  Top 5 Cities of this Year 

	private function topCitiesDistributors () {

		 $result = $this->graphObj->topCitiesDistributors();

		if(is_array($result) && sizeof($result)>0) {
			$success = array('status' => "Success", "msg" => "", "data" => $result);
			$this->response($this->json($success), 200);
		} else {
			$error = array('status' => "Failed", "msg" => "Data Does Not Exist");
			$this->response($this->json($error),209);	
		}

	} // End of function


	 //  Top 5 Cities of this Year 

	private function topCitiesRetailers () {

		 $result = $this->graphObj->topCitiesRetailers();

		if(is_array($result) && sizeof($result)>0) {
			$success = array('status' => "Success", "msg" => "", "data" => $result);
			$this->response($this->json($success), 200);
		} else {
			$error = array('status' => "Failed", "msg" => "Data Does Not Exist");
			$this->response($this->json($error),209);	
		}

	} // End of function


	//  Top 5 products of  Distributors

	private function top5ProductsDist () {

		 $result = $this->graphObj->top5ProductsDistributors();

		if(is_array($result) && sizeof($result)>0) {
			$success = array('status' => "Success", "msg" => "", "data" => $result);
			$this->response($this->json($success), 200);
		} else {
			$error = array('status' => "Failed", "msg" => "Data Does Not Exist");
			$this->response($this->json($error),209);	
		}

	} // End of function

	//  Top 5 products of  Retailers
	private function top5ProductsRet () {

		 $result = $this->graphObj->top5ProductsRetailers();

		if(is_array($result) && sizeof($result)>0) {
			$success = array('status' => "Success", "msg" => "", "data" => $result);
			$this->response($this->json($success), 200);
		} else {
			$error = array('status' => "Failed", "msg" => "Data Does Not Exist");
			$this->response($this->json($error),209);	
		}

	} // End of function

	//  No Order reason wise Distributors
	private function NoOrderDistributors () {

		 $result = $this->graphObj->NoOrderDistributors();

		if(is_array($result) && sizeof($result)>0) {
			$success = array('status' => "Success", "msg" => "", "data" => $result);
			$this->response($this->json($success), 200);
		} else {
			$error = array('status' => "Failed", "msg" => "Data Does Not Exist");
			$this->response($this->json($error),209);	
		}

	} // End of function

	//  No Order reason wise Retailers
	private function NoOrderRetailers () {

		 $result = $this->graphObj->NoOrderRetailers();

		if(is_array($result) && sizeof($result)>0) {
			$success = array('status' => "Success", "msg" => "", "data" => $result);
			$this->response($this->json($success), 200);
		} else {
			$error = array('status' => "Failed", "msg" => "Data Does Not Exist");
			$this->response($this->json($error),209);	
		}

	} // End of function


	//  Order, No Order Retailers and Distributors
	private function orderNoOrder () {

		 $result = $this->graphObj->orderNoOrder();

		if(is_array($result) && sizeof($result)>0) {
			$success = array('status' => "Success", "msg" => "", "data" => $result);
			$this->response($this->json($success), 200);
		} else {
			$error = array('status' => "Failed", "msg" => "Data Does Not Exist");
			$this->response($this->json($error),209);	
		}

	} // End of function

	//  Hot, Cold Retailers and Distributors
	private function HotColdRetDist () {

		 $result = $this->graphObj->HotColdRetDist();

		if(is_array($result) && sizeof($result)>0) {
			$success = array('status' => "Success", "msg" => "", "data" => $result);
			$this->response($this->json($success), 200);
		} else {
			$error = array('status' => "Failed", "msg" => "Data Does Not Exist");
			$this->response($this->json($error),209);	
		}

	} // End of function

	//  No order reason wise Retailers and Distributors
	private function NoOrderReasonWise () {

		 $result = $this->graphObj->NoOrderReasonWise();

		if(is_array($result) && sizeof($result)>0) {
			$success = array('status' => "Success", "msg" => "", "data" => $result);
			$this->response($this->json($success), 200);
		} else {
			$error = array('status' => "Failed", "msg" => "Data Does Not Exist");
			$this->response($this->json($error),209);	
		}

	} // End of function


	 //Top 10 product by values
	private function top10ProdductbyValues () {

		 $result = $this->graphObj->top10ProdductbyValues();

		if(is_array($result) && sizeof($result)>0) {
			$success = array('status' => "Success", "msg" => "", "data" => $result);
			$this->response($this->json($success), 200);
		} else {
			$error = array('status' => "Failed", "msg" => "Data Does Not Exist");
			$this->response($this->json($error),209);	
		}

	} // End of function



	//Total summary of Assigned retailers and distributors
	private function assignedSummary () {

		 $result = $this->graphObj->assignedSummary();

		if(is_array($result) && sizeof($result)>0) {
			$success = array('status' => "Success", "msg" => "", "data" => $result);
			$this->response($this->json($success), 200);
		} else {
			$error = array('status' => "Failed", "msg" => "Data Does Not Exist");
			$this->response($this->json($error),209);	
		}

	} // End of function


	//distributorComplaint
	private function distributorComplaint() {

		$result = $this->graphObj->distributorComplaint();

		if(is_array($result) && sizeof($result)>0) {
			$success = array('status' => "Success", "msg" => "", "data" => $result);
			$this->response($this->json($success), 200);
		} else {
			$error = array('status' => "Failed", "msg" => "Data Does Not Exist");
			$this->response($this->json($error),209);	
		}

	} // End of function


	//dealerComplaint
	private function dealersComplaint() {

		$result = $this->graphObj->dealersComplaint();

		if(is_array($result) && sizeof($result)>0) {
			$success = array('status' => "Success", "msg" => "", "data" => $result);
			$this->response($this->json($success), 200);
		} else {
			$error = array('status' => "Failed", "msg" => "Data Does Not Exist");
			$this->response($this->json($error),209);	
		}

	} // End of function


//CallCenterComplaint
	private function callCenterComplaint() {

		$result = $this->graphObj->callCenterComplaint();

		if(is_array($result) && sizeof($result)>0) {
			$success = array('status' => "Success", "msg" => "", "data" => $result);
			$this->response($this->json($success), 200);
		} else {
			$error = array('status' => "Failed", "msg" => "Data Does Not Exist");
			$this->response($this->json($error),209);	
		}

	} // End of function



 //callcenter tat current month currentmonthtat
private function currentmonthtat() {
	
		$result = $this->graphObj->currentmonthtat();
		//print_R($result);
		//exit;

		if(is_array($result) && sizeof($result)>0) {
			$success = array('status' => "Success", "msg" => "", "data" => $result);
			$this->response($this->json($success), 200);
		} else {
			$error = array('status' => "Failed", "msg" => "Data Does Not Exist");
			$this->response($this->json($error),209);	
		}

	} // End












// Complaint Analytic(Total Complaint, Total Pending Complaint, Total In-Process Complaint, Total Rejected Complaint, Total Completed Complaint)
	
	private function complaintResult () {

		$result = $this->graphObj->complaintAnalytic();
		$success = array('status' => "Success", "msg" => "", "data" => $result);
		$this->response($this->json($success), 200);


	} // End of function







} // end of class




// Initiiate Library
$chartClass = new chartClass;
$chartClass->processApi();
?>