


    function dashBoardSummary () {

        $.ajax({
          url: "graphs/chartClass.php?action=dashBoardTilesValues",
          data: { },          
          success: function( result ) {        
            if(typeof result.data!= 'undefined' && result.status == 'Success') {

                $('#ttlCallsRet h3').html(result.data.retailer.ttlCalls);
                //$('#ttlCallsRet h4').html(result.data.retailer.ttlCalls);
                $('#ttlCallsRet h5').html(result.data.retailer.ttlnewCalls);
                $('#ttlCallsRet h6').html(result.data.retailer.nottlCalls);
                $('#ttlProductCallsRet h3').html(result.data.retailer.productiveCalls);
                //$('#ttlProductCallsRet h4').html(result.data.retailer.productiveCalls);
                $('#ttlProductCallsRet h5').html(result.data.retailer.totalRetailers);
                $('#ttlAmntsRet h3').html(result.data.retailer.ttlAmnts);
                $('#newlyAddedRet h3').html(result.data.retailer.newlyAddedRet);
                //$('#newlyAddedRet h4').html(result.data.retailer.newlyAddedRet);
                $('#newlyAddedRet h5').html(result.data.retailer.newlyAddedRethot);
                $('#newlyAddedRet h6').html(result.data.retailer.newlyAddedRetcold);

                $('#ttlCallsDist h3').html(result.data.distributor.ttlCalls);
                //$('#ttlCallsDist h4').html(result.data.distributor.ttlCalls);
                $('#ttlCallsDist h5').html(result.data.distributor.ttlnewCalls);
                $('#ttlCallsDist h6').html(result.data.distributor.nottlCalls);
                $('#ttlProductCallsDist h3').html(result.data.distributor.productiveCalls);
                //$('#ttlProductCallsDist h4').html(result.data.distributor.productiveCalls);
                $('#ttlProductCallsDist h5').html(result.data.distributor.totalDistributors);
                $('#ttlAmntsDist h3').html(result.data.distributor.ttlAmnts);
                $('#newlyAddedDist h3').html(result.data.distributor.newlyAddedRet);
                //$('#newlyAddedDist h4').html(result.data.distributor.newlyAddedRet);
                $('#newlyAddedDist h5').html(result.data.distributor.newlyAddedRethot);
                $('#newlyAddedDist h6').html(result.data.distributor.newlyAddedRetcold);



                  
            } else {
                $('#ttlCallsRet h3').html(0);
                $('#ttlCallsRet h5').html(0);
                $('#ttlCallsRet h6').html(0);
                $('#ttlProductCallsRet h3').html(0);
                $('#ttlProductCallsRet h5').html(0);
                $('#ttlAmntsRet h3').html('0.0');
                $('#newlyAddedRet h3').html(0);
                $('#newlyAddedRet h5').html(0);
                $('#newlyAddedRet h6').html(0);


                $('#ttlCallsDist h3').html(0);
                $('#ttlCallsDist h5').html(0);
                $('#ttlCallsDist h6').html(0);
                $('#ttlProductCallsDist h3').html(0);
                $('#ttlProductCallsDist h5').html(0);
                $('#ttlAmntsDist h3').html('0.0');
                $('#newlyAddedDist h3').html(0);
                $('#newlyAddedDist h5').html(0);
                $('#newlyAddedDist h6').html(0);
            }

             
             assignedSummary();
             salesmanAttendance(); 

          },
          error: function(error) {
            console.log(error);
          }
        });
    }

    // Call Center Complaints Analytic created by pooja@9Jun2017
     function complaintSummary () {

        $.ajax({
          url: "graphs/chartClass.php?action=complaintResult",
          data: { },          
          success: function( result ) {        
            if(typeof result.data!= 'undefined' && result.status == 'Success') {
              
                $('#ttlComplt h3').html(result.data.T);
                $('#ttlPComplt h3').html(result.data.P);
                $('#ttlIComplt h3').html(result.data.I);
                $('#ttlRComplt h3').html(result.data.R);
                $('#ttlCComplt h3').html(result.data.C);
                
            } else {
                $('#ttlComplt h3').html(0);
                $('#ttlPComplt h3').html(0);
                $('#ttlIComplt h3').html(0);
                $('#ttlRComplt h3').html(0);
                $('#ttlCComplt h3').html(0);
            }
             
             
            
             callCenterComplaint();


          },
          error: function(error) {
            console.log(error);
          }
        });
    }

    //Salesman Attendance 
    function salesmanAttendance () {

            $.ajax({
              url: "graphs/chartClass.php?action=salesmanAttendance",
              data: { },
              success: function( result ) {
                //console.log(result.data);
                if(typeof result.data!= 'undefined' && result.status == 'Success') { 
                    salesmanAttendanceGraphChart (result.data);
                } else {
                    $('#top10RetailerLoader').html(result.msg);
                }

                 topDistributors();  // top 10 distributoier
              },
              error: function(error) {
               console.log(error)
              }
            });
        }

    function salesmanAttendanceGraphChart (result) {  
    
          var seriesListAttend = [];
         for (var i = 0; i < result.Marked.length; i++) {
           seriesListAttend.push([result.Marked[i]]);
         } 

         var seriesListNotAttend = [];
         for (var i = 0; i < result.notMarked.length; i++) {
         
           seriesListNotAttend.push([result.notMarked[i]]);
         } 

         var divisAttend = [];  
         divisAttend = result.division.join(",");


          var salesman_attendence=new Highcharts.Chart({
            chart: {
                 renderTo: 'salesman_attendence',
                type: 'column'
            },
             title: {
                text: ''
            },
            xAxis: {
                 categories: result.division
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Salesman Daily Activity'
                },
                stackLabels: {
                    enabled: true,
                    style: {
                        fontWeight: 'bold',
                        color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                    }
                }
            },
            legend: {
                align: 'right',
                x: -30,
                verticalAlign: 'top',
                y: 25,
                floating: true,
                backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
                borderColor: '#CCC',
                borderWidth: 1,
                shadow: false
            },
            tooltip: {
                headerFormat: '<b>{point.x}</b><br/>',
                pointFormat: '{series.name}: {point.y}<br/>Total: {point.stackTotal}'
            },
            plotOptions: {
                column: {
                    stacking: 'normal',
                    dataLabels: {
                        enabled: true,
                        color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white',
                        style: {
                            textShadow: '0 0 3px black'
                        }
                    }
                }
            },
            exporting: { enabled: false },
            credits: { enabled: false },
            series: [{
                name: 'Attendance Not Marked',
                data: seriesListNotAttend
            }, {
                name: 'Attendance Marked',
               color: 'Green',
                data: seriesListAttend
            }]
        });
       
      }

    //Top 10 Distributors
    function topDistributors () {

        $.ajax({
          url: "graphs/chartClass.php?action=top10Distributors",
          data: { },
          success: function( result ) {
            //console.log(result.data);
            if(typeof result.data!= 'undefined' && result.status == 'Success') {
                topDistrubotorsGraphChart (result.data);
            } else {
                $('#top10DistributorsLoader').html(result.msg);
            }

             topRetailers(); // order performance 
          },
          error: function(error) {
           console.log(error)
          }
        });
    }


    function topDistrubotorsGraphChart(result) {
      var chart;      
      var ttlAmnt = 0;
      if(result.ttlAmnt.length>0)
       ttlAmnt = result.ttlAmnt.join(",");     
      var seriesList = [];
       for (var i = 0; i < result.data.length; i++) {
         seriesList.push({name:result.data[i].retailer_name, data: [ parseFloat(result.data[i].ttlSale) ] });
       }       

      var data = [ttlAmnt];   
        Array.max = function (array) {
            return Math.max.apply(Math, array);
        };
        Array.min = function (array) {
            return Math.min.apply(Math, array);
        };

        var min = Array.min(data);
        var max = Array.max(data);

      var chart = new Highcharts.Chart({ 
        chart: {
          renderTo: 'top10Distributors',
              events: {
              load: function(event) {
                        //When is chart ready?
                        $(document).resize(); 
                    }
                },         
          type: 'column',
        },
        title: { text: '' },
         
        xAxis: {
              categories: [result.month]
          },
          yAxis: {
          //tickInterval: min,
           labels: {
          formatter: function () {
              return this.value;
          }
        }
      
       },
       credits: { enabled: false },
            tooltip: {
              headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
              pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                  '<td style="padding:0"><b>{point.y:.1f} Rs</b></td></tr>',
              footerFormat: '</table>',
              shared: true,
              useHTML: true
          },
          plotOptions: {
              column: {
                  pointPadding: 0.2,
                  borderWidth: 0
              }
          },
         exporting: { enabled: false },
         series: seriesList
        });  

    }

    //Top 10 Retailers
    function topRetailers () {

        $.ajax({
          url: "graphs/chartClass.php?action=top10Retailers",
          data: { },
          success: function( result ) {
            //console.log(result.data);
            if(typeof result.data!= 'undefined' && result.status == 'Success') {
                topRetailersGraphChart (result.data);
            } else {
                $('#top10RetailersLoader').html(result.msg);
            }

             orderPerformanceDistributors(); // order performance 
          },
          error: function(error) {
           console.log(error)
          }
        });
    }


    function topRetailersGraphChart(result) {
        var chart;      
        var ttlAmnt = 0;
        if(result.ttlAmnt.length>0)
         ttlAmnt = result.ttlAmnt.join(","); 

        var seriesList = [];
         for (var i = 0; i < result.data.length; i++) {
           seriesList.push({name:result.data[i].retailer_name, data: [ parseFloat(result.data[i].ttlSale) ] });
         }        

        var data = [ttlAmnt];   
          Array.max = function (array) {
              return Math.max.apply(Math, array);
          };
          Array.min = function (array) {
              return Math.min.apply(Math, array);
          };

          var min = Array.min(data);
          var max = Array.max(data);

          var chart = new Highcharts.Chart({ 
            chart: {
              renderTo: 'top10Retailers',
                  events: {
                  load: function(event) {
                            //When is chart ready?
                            $(document).resize(); 
                        }
                    },         
              type: 'column',
            },
            title: { text: '' },
             
            xAxis: {
                  categories: [result.month]
              },
              yAxis: {
              //tickInterval: min,
               labels: {
              formatter: function () {
                  return this.value;
              }
            }
          
           },
           credits: { enabled: false },
                tooltip: {
                  headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                  pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                      '<td style="padding:0"><b>{point.y:.1f} Rs</b></td></tr>',
                  footerFormat: '</table>',
                  shared: true,
                  useHTML: true
              },
              plotOptions: {
                  column: {
                      pointPadding: 0.2,
                      borderWidth: 0
                  }
              },
             exporting: { enabled: false },
             series: seriesList
        });  

    }


    // Order Performance of Distributors 
    function orderPerformanceDistributors () {

        $.ajax({
          url: "graphs/chartClass.php?action=orderPerformance",
          data: { },
          success: function( result ) {
            //console.log(result.data);
            if(typeof result.data!= 'undefined' && result.status == 'Success') {
                orderPerformanceGraphChart (result.data);
               // distributorsMarketShare(); // distributor share market
            } else {
                $('#distributor_order_performanceLoader').html(result.msg);
            }

            orderPerformanceRetailers(); // distributor share market
          },
          error: function(error) {
           console.log(error)
          }
        });
    }


    function orderPerformanceGraphChart(result) {    

        var fullDate = new Date();
        var thisYear = fullDate.getFullYear();
        var seriesList =[];
        for( var i in result ) {
          if (result.hasOwnProperty(i)){
          seriesList.push(parseFloat(result[i]));
          }
        } 

        var chart = new Highcharts.Chart({
          chart: {
            renderTo: 'distributor_order_performance',
              events: {
              load: function(event) {
                        //When is chart ready?
                        $(document).resize(); 
                    }
                }, 
            type: 'line',
            height: 400
          },
          title: {
            text: '',
          },
         
          xAxis: {
              categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun','Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
          },
          yAxis: {
            minorTickInterval: 'auto',
            min:0,
            title: {
              text: 'Order Amount (Rs)'
            },
            labels: {
              formatter: function () {
                return this.value;
              }
            }
          },
          credits: {
            enabled: false
          },

          exporting: { enabled: false },
          tooltip: {
            valueSuffix: ' (Rs)'
          },
          legend: {
            layout: 'horizontal',
            align: 'center',
            verticalAlign: 'bottom',
            borderWidth: 0
          },
          series: [{name: thisYear, data: seriesList}]
        });

    } // End of function


    //Order Performance of Retailers
    function orderPerformanceRetailers () {

        $.ajax({
          url: "graphs/chartClass.php?action=orderPerformanceRet",
          data: { },
          success: function( result ) {
            //console.log(result.data);
            if(typeof result.data!= 'undefined' && result.status == 'Success') {
                orderPerformanceGraphChartRet (result.data);
               // distributorsMarketShare(); // distributor share market
            } else {
                $('#order_performance_for_retailersLoader').html(result.msg);
            }

            topProductsDistributors(); // distributor share market
          },
          error: function(error) {
           console.log(error)
          }
        });
    }


    function orderPerformanceGraphChartRet(result) {    

        var fullDate = new Date();
        var thisYear = fullDate.getFullYear();
        var seriesList =[];
        for( var i in result ) {
          if (result.hasOwnProperty(i)){
            seriesList.push(parseFloat(result[i]));
          }
        }    
   

          var chart = new Highcharts.Chart({
            chart: {
              renderTo: 'order_performance_for_retailers',
                events: {
                load: function(event) {
                          //When is chart ready?
                          $(document).resize(); 
                      }
                  }, 
              type: 'line',
              height: 400
            },
            title: {
              text: '',
            },
           
            xAxis: {
                categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun','Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
            },
            yAxis: {
              minorTickInterval: 'auto',
              min:0,
              title: {
                text: 'Order Amount (Rs)'
              },
              labels: {
                formatter: function () {
                  return this.value;
                }
              }
            },
            credits: {
              enabled: false
            },

            exporting: { enabled: false },
            tooltip: {
              valueSuffix: ' (Rs)'
            },
            legend: {
              layout: 'horizontal',
              align: 'center',
              verticalAlign: 'bottom',
              borderWidth: 0
            },
            series: [{name: thisYear, data: seriesList}]
        });

    } // End of function


   //  Top 10 products of this Year Distributors
    function topProductsDistributors () {

        $.ajax({
          url: "graphs/chartClass.php?action=topProductsDistributors",
          data: { },
          success: function( result ) {
            if(typeof result.data!= 'undefined' && result.status == 'Success') {
                topProductsGraphChart (result);
            } else {
                $('#top10_products_distributorLoader').html(result.msg);
            }

            topProductsRetailers(); // Top 5 Cities of this Year 
          },
          error: function(error) {
            console.log(error)
          }
        });
    }



    function topProductsGraphChart(result) {    

        var currentDate = new Date();
        var cur_month   = currentDate.toLocaleString('en-us', { month: "long" });

        var seriesList = [];
        for (var i = 0; i < result.data.length; i++) {
        seriesList.push({name:result.data[i].item_name, data: [ parseInt(result.data[i].ttlQty) ] });
        }     

        var chart = new Highcharts.Chart({
          chart: {
            renderTo: 'top10_products_distributor',
              events: {
              load: function(event) {
                        //When is chart ready?
                        $(document).resize(); 
                    }
                },           
            type:'column'
          },
          title: {
            text: ''
          },
          xAxis: {
            categories: [cur_month]
          },
          yAxis: {
          //tickInterval: min,
          labels: {
            formatter: function () {
              return this.value;
            }
          }
        },
        tooltip: {
              headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
              pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                  '<td style="padding:0"><b>{point.y} Qty</b></td></tr>',
              footerFormat: '</table>',
              shared: true,
              useHTML: true
          },
          plotOptions: {
              column: {
                  pointPadding: 0.2,
                  borderWidth: 0
              }
          },
         exporting: { enabled: false },
         credits: {
        enabled: false
       },
       series: seriesList 
     });

  } // End of function    



//  Top 10 products of this Year for Retailers
    function topProductsRetailers () {

        $.ajax({
          url: "graphs/chartClass.php?action=topProductsRetailers",
          data: { },
          success: function( result ) {
            if(typeof result.data!= 'undefined' && result.status == 'Success') {
                topProductsGraphChartRetailers (result);
            } else {
               $('#top10_products_retailersLoader').html(result.msg);
            }

            topCitiesDistributors(); // Top 5 Cities of this Year 
          },
          error: function(error) {
            console.log(error)
          }
        });
    }

    function topProductsGraphChartRetailers(result) {
    
        var currentDate = new Date();
        var cur_month   = currentDate.toLocaleString('en-us', { month: "long" });

        var seriesList = [];
        for (var i = 0; i < result.data.length; i++) {
        seriesList.push({name:result.data[i].item_name, data: [ parseInt(result.data[i].ttlQty) ] });
        }      

        var chart = new Highcharts.Chart({
          chart: {
            renderTo: 'top10_products_retailers',
              events: {
              load: function(event) {
                        //When is chart ready?
                        $(document).resize(); 
                    }
                },           
            type:'column'
          },
          title: {
            text: ''
          },
          xAxis: {
            categories: [cur_month]
          },
          yAxis: {
          //tickInterval: min,
          labels: {
            formatter: function () {
              return this.value;
            }
          }
        },
        tooltip: {
              headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
              pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                  '<td style="padding:0"><b>{point.y} Qty</b></td></tr>',
              footerFormat: '</table>',
              shared: true,
              useHTML: true
          },
          plotOptions: {
              column: {
                  pointPadding: 0.2,
                  borderWidth: 0
              }
          },
         exporting: { enabled: false },
         credits: {
        enabled: false
       },
       series: seriesList 
     });

  } // End of function  

    //  Top 5 Cities of this Year Distributors
    function topCitiesDistributors () {

        $.ajax({
          url: "graphs/chartClass.php?action=topCitiesDistributors",
          data: { },
          success: function( result ) {
            if(typeof result.data!= 'undefined' && result.status == 'Success') {
                topCitiesGraphChartDistributors (result);
            } else {
                $('#top5cities_distributorLoader').html(result.msg);
            }

            topCitiesRetailers();
          },
          error: function(error) {
            console.log(error)
          }
        });
    }



    function topCitiesGraphChartDistributors(result) {     

        var currentDate = new Date();
        var cur_month   = currentDate.toLocaleString('en-us', { month: "long" });

        var seriesList = [];
        for (var i = 0; i < result.data.length; i++) {
        seriesList.push([result.data[i].city_name, parseFloat(result.data[i].ttlSale)]);
        }      

         var chart = new Highcharts.Chart({
            chart: {
                renderTo: 'top5cities_distributor',
                events: {
                load: function(event) {
                          //When is chart ready?
                          $(document).resize(); 
                      }
                  }, 
                   type: 'pie',
                    options3d: {
                        enabled: true,
                        alpha: 45
                    }
              },
              title: {
                  text: ''
                },
               
                plotOptions: {
                    pie: {
                  tickInterval: 100000,
                        innerSize: 100,
                        depth: 45
                    }
                },
            exporting: { enabled: false },
                credits: {
              enabled: false
          },
             tooltip: {
                    formatter: function() {
                        return  '<b>' + this.point.name + '</b><br />Rs.: ' + Highcharts.numberFormat(this.y, 2);
                    }
                },
            plotOptions: {
                pie: {
                    dataLabels: {
                        formatter: function() {
                            return '<b>' + this.point.name + '</b>: '+ Highcharts.numberFormat(this.y, 2)+" <b>Rs</b>";
                        }
                    }
                }
            },        
            
                   series: [{
                    name: 'Selling amount',
                    data: seriesList
                }]
         
        });

    } // End of function  


     //  Top 5 Cities of this Year Retailers
    function topCitiesRetailers () {

        $.ajax({
          url: "graphs/chartClass.php?action=topCitiesRetailers",
          data: { },
          success: function( result ) {
            if(typeof result.data!= 'undefined' && result.status == 'Success') {
                topCitiesGraphChartRetailers (result);
            } else {
                $('#top5cities_retailerLoader').html(result.msg);
            }

            top5SalesmenDistributors();
          },
          error: function(error) {
            console.log(error)
          }
        });
    }



    function topCitiesGraphChartRetailers(result) {
     
        var currentDate = new Date();
        var cur_month   = currentDate.toLocaleString('en-us', { month: "long" });

        var seriesList = [];
        for (var i = 0; i < result.data.length; i++) {
        seriesList.push([result.data[i].city_name, parseFloat(result.data[i].ttlSale)]);
        }      

         var chart = new Highcharts.Chart({
            chart: {
                renderTo: 'top5cities_retailer',
                events: {
                load: function(event) {
                          //When is chart ready?
                          $(document).resize(); 
                      }
                  }, 
                   type: 'pie',
                    options3d: {
                        enabled: true,
                        alpha: 45
                    }
              },
              title: {
                  text: ''
                },
               
                plotOptions: {
                    pie: {
                  tickInterval: 100000,
                        innerSize: 100,
                        depth: 45
                    }
                },
            exporting: { enabled: false },
                credits: {
              enabled: false
          },
             tooltip: {
                    formatter: function() {
                        return  '<b>' + this.point.name + '</b><br />Rs.: ' + Highcharts.numberFormat(this.y, 2);
                    }
                },
            plotOptions: {
                pie: {
                    dataLabels: {
                        formatter: function() {
                            return '<b>' + this.point.name + '</b>: '+ Highcharts.numberFormat(this.y, 2)+" <b>Rs</b>";
                        }
                    }
                }
            },
            
            
                   series: [{
                    name: 'Selling amount',
                    data: seriesList
                }]
         
        });

    } // End of function  



     //  Top 5 salesman of this Year Distributors
    function top5SalesmenDistributors () {

        $.ajax({
          url: "graphs/chartClass.php?action=top5SalesmenDistributors",
          data: { },
          success: function( result ) {
            if(typeof result.data!= 'undefined' && result.status == 'Success') {
                topSalesmenGraphChartDistributors (result);
               // topProducts(); // Top 5 products of this Year 
            } else {
                $('#top5DisSalesmanLoader').html(result.msg);
            }

            top5SalesmenRetailers(); // Top 5 Salesman for Retailers
          },
          error: function(error) {
            console.log(error)
          }
        });
    }



    function topSalesmenGraphChartDistributors(result) {

        var currentDate = new Date();
        var cur_month   = currentDate.toLocaleString('en-us', { month: "long" });

        var seriesList = [];
        for (var i = 0; i < result.data.length; i++) {
        seriesList.push({name:result.data[i].salesman_name, data: [ parseInt(result.data[i].ttlSale) ] });
        }       

        var chart = new Highcharts.Chart({
          chart: {
            renderTo: 'top5DisSalesman',
              events: {
              load: function(event) {
                        //When is chart ready?
                        $(document).resize(); 
                    }
                }, 
            type: 'column',
          },
          title: {
            text: ''
          },
          xAxis: {
            categories: [cur_month]
          },
          yAxis: {
          //tickInterval: min,
          labels: {
            formatter: function () {
              return this.value;
            }
          }
        },
        tooltip: {
              headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
              pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                  '<td style="padding:0"><b>{point.y:.1f} Rs</b></td></tr>',
              footerFormat: '</table>',
              shared: true,
              useHTML: true
          },
          plotOptions: {
              column: {
                  pointPadding: 0.2,
                  borderWidth: 0
              }
            },
           exporting: { enabled: false },
          credits: {
          enabled: false
        },
        series: seriesList
      });  
    

   } // End of function   


     //  Top 5 salesman of this Year Retailers
    function top5SalesmenRetailers () {

        $.ajax({
          url: "graphs/chartClass.php?action=top5SalesmenRetailers",
          data: { },
          success: function( result ) {
            if(typeof result.data!= 'undefined' && result.status == 'Success') {
                topSalesmenGraphChartRetailers (result);
               // topProducts(); // Top 5 products of this Year 
            } else {
                $('#top5RetSalesmanLoader').html(result.msg);
            }

            top5ProductsDist(); // Top 5 products of this Year
          },
          error: function(error) {
            console.log(error)
          }
        });
    }



    function topSalesmenGraphChartRetailers(result1) {
        var currentDate = new Date();
        var cur_month   = currentDate.toLocaleString('en-us', { month: "long" });

        var seriesList = [];
        for (var i = 0; i < result1.data.length; i++) {
        seriesList.push({name:result1.data[i].salesman_name, data: [ parseInt(result1.data[i].ttlSale) ] });
        } 

        var chart = new Highcharts.Chart({
          chart: {
            renderTo: 'top5RetSalesman',
              events: {
              load: function(event) {
                        //When is chart ready?
                        $(document).resize(); 
                    }
                }, 
            type: 'column',
          },
          title: {
            text: ''
          },
          xAxis: {
            categories: [cur_month]
          },
          yAxis: {
          //tickInterval: min,
          labels: {
            formatter: function () {
              return this.value;
            }
          }
        },
        tooltip: {
              headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
              pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                  '<td style="padding:0"><b>{point.y:.1f} Rs</b></td></tr>',
              footerFormat: '</table>',
              shared: true,
              useHTML: true
          },
          plotOptions: {
              column: {
                  pointPadding: 0.2,
                  borderWidth: 0
              }
            },
           exporting: { enabled: false },
          credits: {
          enabled: false
        },
        series: seriesList
      }); 

  } // End of function   




//  Top 5 products of this Year Distributors
    function top5ProductsDist () {

        $.ajax({
          url: "graphs/chartClass.php?action=top5ProductsDist",
          data: { },
          success: function( result ) {
            if(typeof result.data!= 'undefined' && result.status == 'Success') {
                top5ProductsChartDist (result);
            } else {
                $('#top5_products_distributorLoader').html(result.msg);
            }

            top5ProductsRet(); // Top 5 Cities of this Year 
          },
          error: function(error) {
            console.log(error)
          }
        });
    }



    function top5ProductsChartDist(result) {    

        var currentDate = new Date();
        var cur_month   = currentDate.toLocaleString('en-us', { month: "long" });

        var seriesList = [];
        for (var i = 0; i < result.data.length; i++) {
        seriesList.push({name:result.data[i].item_name, data: [ parseInt(result.data[i].ttlQty) ] });
        }      

         var chart = new Highcharts.Chart({
          chart: {
            renderTo: 'top5_products_distributor',
              events: {
              load: function(event) {
                        //When is chart ready?
                        $(document).resize(); 
                    }
                },           
            type:'column'
          },
          title: {
            text: ''
          },
          xAxis: {
            categories: [cur_month]
          },
          yAxis: {
          //tickInterval: min,
          labels: {
            formatter: function () {
              return this.value;
            }
          }
        },
        tooltip: {
              headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
              pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                  '<td style="padding:0"><b>{point.y} Qty</b></td></tr>',
              footerFormat: '</table>',
              shared: true,
              useHTML: true
          },
          plotOptions: {
              column: {
                  pointPadding: 0.2,
                  borderWidth: 0
              }
          },
         exporting: { enabled: false },
         credits: {
        enabled: false
       },
       series: seriesList 
     });

 } // End of function    



//  Top 5 products of this Year for Retailers
    function top5ProductsRet () {

      $.ajax({
        url: "graphs/chartClass.php?action=top5ProductsRet",
        data: { },
        success: function( result ) {
          if(typeof result.data!= 'undefined' && result.status == 'Success') {
              top5ProductChartRetailers (result);
          } else {
             $('#top5_products_retailerLoader').html(result.msg);
          }

          NoOrderDistributors(); // Top 5 Cities of this Year 
        },
        error: function(error) {
          console.log(error)
        }
      });
  }

    function top5ProductChartRetailers(result) {       

          var currentDate = new Date();
          var cur_month   = currentDate.toLocaleString('en-us', { month: "long" });

          var seriesList = [];
          for (var i = 0; i < result.data.length; i++) {
          seriesList.push({name:result.data[i].item_name, data: [ parseInt(result.data[i].ttlQty) ] });
          }         

          var chart = new Highcharts.Chart({
            chart: {
              renderTo: 'top5_products_retailer',
                events: {
                load: function(event) {
                          //When is chart ready?
                          $(document).resize(); 
                      }
                  },           
              type:'column'
            },
            title: {
              text: ''
            },
            xAxis: {
              categories: [cur_month]
            },
            yAxis: {
            //tickInterval: min,
            labels: {
              formatter: function () {
                return this.value;
              }
            }
          },
          tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y} Qty</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
           exporting: { enabled: false },
           credits: {
          enabled: false
         },
         series: seriesList 
       });

    } // End of function  


    //No Order Reason wise Distributors
    function NoOrderDistributors () {

      $.ajax({
        url: "graphs/chartClass.php?action=NoOrderDistributors",
        data: { },
        success: function( result ) {
          if(typeof result.data!= 'undefined' && result.status == 'Success') {
              noOrderGraphDistributor (result);
          } else {
             $('#no_order_reason_distributorLoader').html(result.msg);
          }

          NoOrderRetailers(); //No Order Reason wise Retailers
        },
        error: function(error) {
          console.log(error)
        }
      });
  }

    function noOrderGraphDistributor(result) {


      var seriesList = [];
        for (var i = 0; i < result.data.length; i++) {
        seriesList.push([result.data[i].discription, parseFloat(result.data[i].noOrderCount)]);
        } 
      
      var chart=new Highcharts.Chart({
          chart: {
              renderTo: 'no_order_reason_distributor',
              type: 'pie',
              options3d: {
                  enabled: true,
                  alpha: 45
              }
          },
          title: {
              text: ''
          },
          plotOptions: {
              pie: {
                  tickInterval: 100000,
                  innerSize: 100,
                  depth: 45
              }
          },
          exporting: { enabled: false },
          credits: {
              enabled: false
          },
          tooltip: {
              formatter: function() {
                  return  '<b>' + this.point.name + '</b><br />' + this.y;
              }
          },
          plotOptions: {
              pie: {
                  dataLabels: {
                      formatter: function() {
                          return '<b>' + this.point.name + '</b>: '+ this.y;
                      }
                  }
              }
          },
          
          series: [{
              name: 'No order reason',
              data: seriesList
          }]
      });

    } // End of function  


    //No Order Reason wise Retailers
    function NoOrderRetailers () {

      $.ajax({
        url: "graphs/chartClass.php?action=NoOrderRetailers",
        data: { },
        success: function( result ) {
          if(typeof result.data!= 'undefined' && result.status == 'Success') {
              noOrderGraphRetailers (result);
          } else {
              $('#no_order_reason_retailerLoader').html(result.msg);
          }

          distributorsMarketShare(); // Distributor Share Market
        },
        error: function(error) {
          console.log(error)
        }
      });
  }

    function noOrderGraphRetailers(result) {


      var seriesList = [];
        for (var i = 0; i < result.data.length; i++) {
        seriesList.push([result.data[i].discription, parseFloat(result.data[i].noOrderCount)]);
        } 
      
      var chart=new Highcharts.Chart({
          chart: {
              renderTo: 'no_order_reason_retailer',
              type: 'pie',
              options3d: {
                  enabled: true,
                  alpha: 45
              }
          },
          title: {
              text: ''
          },
          plotOptions: {
              pie: {
                  tickInterval: 100000,
                  innerSize: 100,
                  depth: 45
              }
          },
          exporting: { enabled: false },
          credits: {
              enabled: false
          },
          tooltip: {
              formatter: function() {
                  return  '<b>' + this.point.name + '</b><br />' + this.y;
              }
          },
          plotOptions: {
              pie: {
                  dataLabels: {
                      formatter: function() {
                          return '<b>' + this.point.name + '</b>: '+ this.y;
                      }
                  }
              }
          },
          
          series: [{
              name: 'No order reason',
              data: seriesList
          }]
      });

    } // End of function  

     //  Distributor market share of this Year 
    function distributorsMarketShare () {

        $.ajax({
          url: "graphs/chartClass.php?action=distributorsMarketShare",
          data: { },
          success: function( result ) {           
            if(typeof result.data!= 'undefined' && result.status == 'Success') {
                distributorMarketShareGraphChart (result);               
            } else {
                $('#distributors_shareLoader').html(result.msg);              
            }
            orderNoOrder(); // Order No order

          },
          error: function(error) {
            console.log(error)
          }
        });
    }

    function distributorMarketShareGraphChart(result) {    

      var seriesList = [];
       for (var i = 0; i < result.data.length; i++) {
         seriesList.push([result.data[i].distributor_name, parseFloat(result.data[i].value)]);
       } 

      var chart = new Highcharts.Chart({
        chart: {
          renderTo: 'distributors_share',
          events: {
            load: function(event) {
                      //When is chart ready?
                      $(document).resize(); 
                  }
              }, 
          plotBackgroundColor: null,
          plotBorderWidth: null,
          plotShadow: false
        },
        title: {
          text: ''
        },
        tooltip: {
          pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        exporting: { enabled: false },
        plotOptions: {
          pie: {
            dataLabels: {
              formatter: function() {
                return '<b>' + this.point.name + '</b>: ' + this.percentage.toFixed(2) + ' %';
              }
            }
          }
        },
        credits: {
          enabled: false
        },
        series: [{
          type: 'pie',
          name: 'Distributor Share',
          data: seriesList
        }]
      });


    } // End of function



    //  Order and No order graph for Dealers and Distributors
    function orderNoOrder () {

        $.ajax({
          url: "graphs/chartClass.php?action=orderNoOrder",
          data: { },
          success: function( result ) {
            if(typeof result.data!= 'undefined' && result.status == 'Success') {
                orderNoOrderGraph (result);
            } else {
                $('#no_new_orderLoader').html(result.msg);
            }

            //HotColdRetDist(); // Top 5 Cities of this Year 
            NoOrderReasonWise();
          },
          error: function(error) {
            console.log(error)
          }
        });
    }



    function orderNoOrderGraph(result) {

         var OrderList = [];
         for (var i = 0; i < result.data.Order.length; i++) {
           OrderList.push(parseInt(result.data.Order[i]));
         } 

          var NoOrderList = [];
         for (var i = 0; i < result.data.NoOrder.length; i++) {
           NoOrderList.push(parseInt(result.data.NoOrder[i]));
         }
        
          var chart=new Highcharts.Chart({
          chart: {
              renderTo: 'no_new_order',
              type: 'column'
          },
          title: {
              text: ''
          },
          xAxis: {
              categories: ['Distributor','Dealers']
          },
          yAxis: {
              min: 0,
              title: {
                  text: 'New Order/No order'
              },
              stackLabels: {
                  enabled: true,
                  style: {
                      fontWeight: 'bold',
                      color: (Highcharts.theme && Highcharts.theme.textColor) || 'black'
                  }
              }
          },
          legend: {
              align: 'right',
              x: -10,
              verticalAlign: 'top',
              y: -10,
              floating: true,
              backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
              borderColor: '#CCC',
              borderWidth: 1,
              shadow: false
          },
          tooltip: {
              headerFormat: '<b>{point.x}</b><br/>',
              pointFormat: '{series.name}: {point.y}<br/>Total: {point.stackTotal}'
          },
          plotOptions: {
              column: {
                  stacking: 'normal',
                  dataLabels: {
                      enabled: true,
                      color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'black',
                      style: {
                          //textShadow: '0 0 3px black'
                      }
                  }
              }
          },
          exporting: { enabled: false },
          credits: { enabled: false },
          series: [{
              name: 'New Order',
              color:'#3ADF00',
              data: OrderList
          },{
              name: 'No Order',
              color:'#FFFF00',
              data: NoOrderList
          }]
      });

} // End of function   


 //  Order and No order graph for Dealers and Distributors
    function HotColdRetDist () {

        $.ajax({
          url: "graphs/chartClass.php?action=HotColdRetDist",
          data: { },
          success: function( result ) {
            if(typeof result.data!= 'undefined' && result.status == 'Success') {
                HotColdRetDistGraph (result);
            } else {
                $('#hot_coldLoader').html(result.msg);
            }

            NoOrderReasonWise(); // No order
          },
          error: function(error) {
            console.log(error)
          }
        });
    }



    function HotColdRetDistGraph(result) {

         var HotList = [];
         for (var i = 0; i < result.data.Hot.length; i++) {
           HotList.push(parseInt(result.data.Hot[i]));
         } 

          var ColdList = [];
         for (var i = 0; i < result.data.Cold.length; i++) {
           ColdList.push(parseInt(result.data.Cold[i]));
         }

          var chart=new Highcharts.Chart({
              chart: {
                  renderTo: 'hot_cold',
                  type: 'column'
              },
              title: {
                  text: ''
              },
              xAxis: {
                  categories: ['Distributor','Dealers']
              },
              yAxis: {
                  min: 0,
                  title: {
                      text: 'Hot and Cold'
                  },
                  stackLabels: {
                      enabled: true,
                      style: {
                          fontWeight: 'bold',
                          color: (Highcharts.theme && Highcharts.theme.textColor) || 'black'
                      }
                  }
              },
              legend: {
                  align: 'right',
                  x: -30,
                  verticalAlign: 'top',
                  y: -10,
                  floating: true,
                  backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
                  borderColor: '#CCC',
                  borderWidth: 1,
                  shadow: false
              },
              tooltip: {
                  headerFormat: '<b>{point.x}</b><br/>',
                  pointFormat: '{series.name}: {point.y}<br/>Total: {point.stackTotal}'
              },
              plotOptions: {
                  column: {
                      stacking: 'normal',
                      dataLabels: {
                          enabled: true,
                          color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'black',
                          style: {
                              //textShadow: '0 0 3px black'
                          }
                      }
                  }
              },
              exporting: { enabled: false },
              credits: { enabled: false },
              series: [{
                  name: 'HOT',
                  color:'rgb(240, 111, 111)',
                  data: HotList 
              },{
                  name: 'COLD',
                  color:'rgb(65, 152, 65)',
                  data: ColdList
              }]
          });

          
} // End of function


//No Order Reason wise Distributors and Retailers
    function NoOrderReasonWise () {

      $.ajax({
        url: "graphs/chartClass.php?action=NoOrderReasonWise",
        data: { },
        success: function( result ) {
          if(typeof result.data!= 'undefined' && result.status == 'Success') {
              NoOrderReasonWiseGraph (result);
          } else {
              $('#no_order_reasonLoader').html(result.msg);
          }

          //assignedSummary(); //No Order Reason wise Retailers

          distributorComplaint();
        },
        error: function(error) {
          console.log(error)
        }
      });
  }

    function NoOrderReasonWiseGraph(result) {

      var seriesList = [];
        for (var i = 0; i < result.data.length; i++) {
        seriesList.push([result.data[i].discription, parseFloat(result.data[i].noOrderCount)]);
        } 
      
      var chart=new Highcharts.Chart({
          chart: {
              renderTo: 'no_order_reason',
              type: 'pie',
              options3d: {
                  enabled: true,
                  alpha: 45
              }
          },
          title: {
              text: ''
          },
          plotOptions: {
              pie: {
                  tickInterval: 100000,
                  innerSize: 100,
                  depth: 45
              }
          },
          exporting: { enabled: false },
          credits: {
              enabled: false
          },
          tooltip: {
              formatter: function() {
                  return  '<b>' + this.point.name + '</b><br />' + this.y;
              }
          },
          plotOptions: {
              pie: {
                  dataLabels: {
                      formatter: function() {
                          return '<b>' + this.point.name + '</b>: '+ this.y;
                      }
                  }
              }
          },
          
          series: [{
              name: 'No order reason',
              data: seriesList
          }]
      });

    } // End of function  

    //Top 10 product by values
   /* function top10ProdductbyValues () {

      $.ajax({
        url: "graphs/chartClass.php?action=top10ProdductbyValues",
        data: { },
        success: function( result ) {
          if(typeof result.data!= 'undefined' && result.status == 'Success') {
              top10ProdductbyValuesGraph (result);
          } else {
             // $('#topProductsLoader').html(result.msg);
          }

          //NoOrderRetailers(); //No Order Reason wise Retailers
        },
        error: function(error) {
          console.log(error)
        }
      });
  }

    function top10ProdductbyValuesGraph(result) {


        var AmountArr = [];
         for (var i = 0; i < result.data.ttlAmnt.length; i++) {
           AmountArr.push(parseFloat(result.data.ttlAmnt[i]));
         } 


          var ItemArr = [];

         for (var i = 0; i < result.data.data.length; i++) {
           ItemArr.push(result.data.data[i]['item_name']);
         } 


         var ItemArr1 = [];
            ItemArr1=    ItemArr.join();     

          var  top10_products=new Highcharts.Chart({
         
        
        chart: {
            renderTo: 'top10_products',
               type: 'column',
                },
          title: {
              text: ''
            },
           
          xAxis: {
                categories: [result.data.month]
            },
            yAxis: {
             title: {
                      text: 'Values'
                  },
             labels: {
            formatter: function () {
                return this.value;
            }
          }
      
       },
         credits: { enabled: false },
          tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0;">{series.name}: </td>' +
                    '<td style="padding:0;text-align:right;"><b>{point.y:.1f} Rs</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
           exporting: { enabled: false },
        
          series: [{
                  name: ItemArr1,
                  data: AmountArr
              }]
     
          });  

     
    } // End of function */ 


    function assignedSummary () {

        $.ajax({
          url: "graphs/chartClass.php?action=assignedSummary",
          data: { },          
          success: function( result ) {        
            if(typeof result.data!= 'undefined' && result.status == 'Success') {

                $('#ttlAssignedRet h3').html(result.data.retailers);
                $('#ttlAssignedDist h3').html(result.data.distributors);
                  
            } else {
                $('#ttlAssignedRet h3').html(0);
                $('#ttlAssignedDist h3').html(0);
                
            }              

          },
          error: function(error) {
            console.log(error);
          }
        });
    }


    //Added DistributorComplaint
    function distributorComplaint () {

        $.ajax({
          url: "graphs/chartClass.php?action=distributorComplaint",
          data: { },
          success: function( result ) {
            //console.log(result.data);
            if(typeof result.data!= 'undefined' && result.status == 'Success') {
                distributorComplaintGraphChart (result.data);
            } else {
                $('#distributorComplaint').html(result.msg);
            }

            dealerComplaintOfCurrentYear();
          },
          error: function(error) {
           console.log(error)
          }
        });
    }


    function distributorComplaintGraphChart(result) {
     // alert('See');
     //   console.log(result);
      //  alert('Hide');
        var t = [];
        var p = [];
        var ip = [];
        var r = [];
        var c = [];

        if(typeof result!='undefined'){
         $.each(result, function(key, value){
         // console.log(value.p);
          t.push(parseInt(value.t));
          p.push(parseInt(value.p));
          ip.push(parseInt(value.ip));
          r.push(parseInt(value.r));
          c.push(parseInt(value.c));

         });
        }

          // console.log(p);
          //   console.log(ip);
          //     console.log(r);
          //       console.log(c);

       var chart = new Highcharts.Chart({
        chart: {
          renderTo: 'distributorComplaint',
          zoomType: 'xy'
        },
        title: {
          text: ''
        },
        subtitle: {
          text: ''
        },
        xAxis: [{
          categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
          'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
          crosshair: true
        }],
      yAxis: [{ // Primary yAxis
      labels: {
        format: '{value}',
        style: {
          color: Highcharts.getOptions().colors[2]
        }
      },
      title: {
        text: '',
        style: {
          color: Highcharts.getOptions().colors[2]
        }
      },
      opposite: true

    }, { // Secondary yAxis
      gridLineWidth: 0,
      title: {
        text: '',
        style: {
          color: Highcharts.getOptions().colors[0]
        }
      },
      labels: {
        format: '{value}',
        style: {
          color: Highcharts.getOptions().colors[0]
        }
      }

    },
    { // Tertiary yAxis
      gridLineWidth: 0,
      title: {
        text: ' ',
        style: {
          color: Highcharts.getOptions().colors[1]
        }
      },
      labels: {
        format: '{value}',
        style: {
          color: Highcharts.getOptions().colors[1]
        }
      },

    },
    { 
      gridLineWidth: 0,
      title: {
        text: ' ',
        style: {
          color: Highcharts.getOptions().colors[3]
        }
      },
      labels: {
        format: '{value}',
        style: {
          color: Highcharts.getOptions().colors[3]
        }
      },

    },

    { 
      gridLineWidth: 0,
      title: {
        text: ' ',
        style: {
          color: Highcharts.getOptions().colors[4]
        }
      },
      labels: {
        format: '{value}',
        style: {
          color: Highcharts.getOptions().colors[4]
        }
      },
      opposite: true
    }],
    tooltip: {
      shared: true
    },

    // legend: {
    //   layout: 'vertical',
    //   align: 'left',
    //   x: 80,
    //   verticalAlign: 'top',
    //   y: 55,
    //   floating: true,
    //   backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
    // },
    credits: { enabled: false },
    exporting: { enabled: false },

    series: [{
      name: 'Total Complaints',
      type: 'spline',
     //   yAxis: 1,
    // data: [49.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.1, 95.6, 54.4],
     data:t,

     tooltip: {
           // valueSuffix: ' mm'
         }

       }, {
        name: 'Total Pending Complaints',
        type: 'spline',
       // yAxis: 2,
      // data: [30, 1, 90, 50, 10, 15, 1, 12, 0, 16, 8, 7],
       data:p,
       marker: {
        enabled: true
      },
      //  dashStyle: 'shortdot',
      tooltip: {
          //  valueSuffix: ' mb'
        }

      }, 
      {
        name: 'Total In-Process Complaints',
        type: 'spline',
       // yAxis: 3,
       //data: [20, 10, 18, 4, 13, 29, 7, 12, 40, 90, 4, 15],
       data:ip,
       marker: {
        enabled: true
      },
    //    dashStyle: 'shortdot',
    tooltip: {
          //  valueSuffix: ' mb'
        }

      },
      {
        name: 'Total Rejected Complaints',
        type: 'spline',
       // yAxis: 4,
      // data: [6, 1, 5, 2, 3, 9, 6, 2, 1, 0, 2, 8],
      data:r,
       marker: {
        enabled: true
      },
      //  dashStyle: 'shortdot',
      tooltip: {
          //  valueSuffix: ' mb'
        }

      },
      {
        name: 'Total Completed Complaints',
        type: 'spline',
       // data: [7.0, 6.9, 9.5, 14.5, 18.2, 21.5, 25.2, 26.5, 23.3, 18.3, 13.9, 9.6],
       data:c,
        tooltip: {
           // valueSuffix: ' °C'
         }
       }]
     });


    }



    // Monthly DealerComplaint Of Current Year
    function dealerComplaintOfCurrentYear() {

        $.ajax({
          url: "graphs/chartClass.php?action=dealersComplaint",
          data: { },
          success: function( result ) {
            //console.log(result.data);
            if(typeof result.data!= 'undefined' && result.status == 'Success') {
                
                dealersComplaintGraphChart (result.data);
            
            } else {
                $('#dealers_complaint_loader').html(result.msg);
            }

             
             callCenterComplaint();
          },
          error: function(error) {
           console.log(error)
          }
        });
    }


    function dealersComplaintGraphChart(result) {
        //alert('See');
       // console.log(result);
       // alert('Hide');
        var t = [];
        var p = [];
        var ip = [];
        var r = [];
        var c = [];

        if(typeof result!='undefined'){
         $.each(result, function(key, value){
         // console.log(value.p);
          t.push(parseInt(value.t));
          p.push(parseInt(value.p));
          ip.push(parseInt(value.ip));
          r.push(parseInt(value.r));
          c.push(parseInt(value.c));

         });
        }

          // console.log(p);
          //   console.log(ip);
          //     console.log(r);
          //       console.log(c);

       var chart = new Highcharts.Chart({
        
        chart: {
          renderTo: 'dealersComplaint',
          zoomType: 'xy'
        },
        title: {
          text: ''
        },
        subtitle: {
          text: ''
        },
        xAxis: [{
          categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
          'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
          crosshair: true
        }],
      yAxis: [{ // Primary yAxis
      labels: {
        format: '{value}',
        style: {
          color: Highcharts.getOptions().colors[2]
        }
      },
      title: {
        text: '',
        style: {
          color: Highcharts.getOptions().colors[2]
        }
      },
      opposite: true

    }, { // Secondary yAxis
      gridLineWidth: 0,
      title: {
        text: '',
        style: {
          color: Highcharts.getOptions().colors[0]
        }
      },
      labels: {
        format: '{value}',
        style: {
          color: Highcharts.getOptions().colors[0]
        }
      }

    },
    { // Tertiary yAxis
      gridLineWidth: 0,
      title: {
        text: ' ',
        style: {
          color: Highcharts.getOptions().colors[1]
        }
      },
      labels: {
        format: '{value}',
        style: {
          color: Highcharts.getOptions().colors[1]
        }
      },

    },
    { 
      gridLineWidth: 0,
      title: {
        text: ' ',
        style: {
          color: Highcharts.getOptions().colors[3]
        }
      },
      labels: {
        format: '{value}',
        style: {
          color: Highcharts.getOptions().colors[3]
        }
      },

    },

    { 
      gridLineWidth: 0,
      title: {
        text: ' ',
        style: {
          color: Highcharts.getOptions().colors[4]
        }
      },
      labels: {
        format: '{value}',
        style: {
          color: Highcharts.getOptions().colors[4]
        }
      },
      opposite: true
    }],
    tooltip: {
      shared: true
    },

    // legend: {
    //   layout: 'vertical',
    //   align: 'left',
    //   x: 80,
    //   verticalAlign: 'top',
    //   y: 55,
    //   floating: true,
    //   backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
    // },
    credits: { enabled: false },
    exporting: { enabled: false },

    series: [{
      name: 'Total Complaints',
      type: 'spline',
     //   yAxis: 1,
    // data: [49.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.1, 95.6, 54.4],
     data:t,

     tooltip: {
           // valueSuffix: ' mm'
         }

       }, {
        name: 'Total Pending Complaints',
        type: 'spline',
       // yAxis: 2,
      // data: [30, 1, 90, 50, 10, 15, 1, 12, 0, 16, 8, 7],
       data:p,
       marker: {
        enabled: true
      },
      //  dashStyle: 'shortdot',
      tooltip: {
          //  valueSuffix: ' mb'
        }

      }, 
      {
        name: 'Total In-Process Complaints',
        type: 'spline',
       // yAxis: 3,
       //data: [20, 10, 18, 4, 13, 29, 7, 12, 40, 90, 4, 15],
       data:ip,
       marker: {
        enabled: true
      },
    //    dashStyle: 'shortdot',
    tooltip: {
          //  valueSuffix: ' mb'
        }

      },
      {
        name: 'Total Rejected Complaints',
        type: 'spline',
       // yAxis: 4,
      // data: [6, 1, 5, 2, 3, 9, 6, 2, 1, 0, 2, 8],
      data:r,
       marker: {
        enabled: true
      },
      //  dashStyle: 'shortdot',
      tooltip: {
          //  valueSuffix: ' mb'
        }

      },
      {
        name: 'Total Completed Complaints',
        type: 'spline',
       // data: [7.0, 6.9, 9.5, 14.5, 18.2, 21.5, 25.2, 26.5, 23.3, 18.3, 13.9, 9.6],
       data:c,
        tooltip: {
           // valueSuffix: ' °C'
         }
       }]
     });


    }

//CallCenterComplaint
    function callCenterComplaint () {

        $.ajax({
          url: "graphs/chartClass.php?action=callCenterComplaint",
          data: { },
          success: function( result ) {
            //console.log(result.data);
            if(typeof result.data!= 'undefined' && result.status == 'Success') {
                callCenterComplaintGraphChart (result.data);
            } else {
                $('#call_center_complaint_loader').html(result.msg);
            }

          currentmonthtat();
          },
          error: function(error) {
           console.log(error)
          }
        });
    }


    function callCenterComplaintGraphChart(result) {
     // alert('See');
     //   console.log(result);
      //  alert('Hide');
        var t = [];
        var p = [];
        var ip = [];
        var r = [];
        var c = [];

        if(typeof result!='undefined'){
         $.each(result, function(key, value){
         // console.log(value.p);
          t.push(parseInt(value.t));
          p.push(parseInt(value.p));
          ip.push(parseInt(value.ip));
          r.push(parseInt(value.r));
          c.push(parseInt(value.c));

         });
        }

          // console.log(p);
          //   console.log(ip);
          //     console.log(r);
          //       console.log(c);

       var chart = new Highcharts.Chart({
        chart: {
          renderTo: 'call_center_complaint',
          zoomType: 'xy'
        },
        title: {
          text: ''
        },
        subtitle: {
          text: ''
        },
        xAxis: [{
          categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
          'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
          crosshair: true
        }],
      yAxis: [{ // Primary yAxis
      labels: {
        format: '{value}',
        style: {
          color: Highcharts.getOptions().colors[2]
        }
      },
      title: {
        text: '',
        style: {
          color: Highcharts.getOptions().colors[2]
        }
      },
      opposite: true

    }, { // Secondary yAxis
      gridLineWidth: 0,
      title: {
        text: '',
        style: {
          color: Highcharts.getOptions().colors[0]
        }
      },
      labels: {
        format: '{value}',
        style: {
          color: Highcharts.getOptions().colors[0]
        }
      }

    },
    { // Tertiary yAxis
      gridLineWidth: 0,
      title: {
        text: ' ',
        style: {
          color: Highcharts.getOptions().colors[1]
        }
      },
      labels: {
        format: '{value}',
        style: {
          color: Highcharts.getOptions().colors[1]
        }
      },

    },
    { 
      gridLineWidth: 0,
      title: {
        text: ' ',
        style: {
          color: Highcharts.getOptions().colors[3]
        }
      },
      labels: {
        format: '{value}',
        style: {
          color: Highcharts.getOptions().colors[3]
        }
      },

    },

    { 
      gridLineWidth: 0,
      title: {
        text: ' ',
        style: {
          color: Highcharts.getOptions().colors[4]
        }
      },
      labels: {
        format: '{value}',
        style: {
          color: Highcharts.getOptions().colors[4]
        }
      },
      opposite: true
    }],
    tooltip: {
      shared: true
    },

    // legend: {
    //   layout: 'vertical',
    //   align: 'left',
    //   x: 80,
    //   verticalAlign: 'top',
    //   y: 55,
    //   floating: true,
    //   backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
    // },
    credits: { enabled: false },
    exporting: { enabled: false },

    series: [{
      name: 'Total Complaints',
      type: 'spline',
     //   yAxis: 1,
    // data: [49.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.1, 95.6, 54.4],
     data:t,

     tooltip: {
           // valueSuffix: ' mm'
         }

       }, {
        name: 'Total Pending Complaints',
        type: 'spline',
       // yAxis: 2,
      // data: [30, 1, 90, 50, 10, 15, 1, 12, 0, 16, 8, 7],
       data:p,
       marker: {
        enabled: true
      },
      //  dashStyle: 'shortdot',
      tooltip: {
          //  valueSuffix: ' mb'
        }

      }, 
      {
        name: 'Total In-Process Complaints',
        type: 'spline',
       // yAxis: 3,
       //data: [20, 10, 18, 4, 13, 29, 7, 12, 40, 90, 4, 15],
       data:ip,
       marker: {
        enabled: true
      },
    //    dashStyle: 'shortdot',
    tooltip: {
          //  valueSuffix: ' mb'
        }

      },
      {
        name: 'Total Rejected Complaints',
        type: 'spline',
       // yAxis: 4,
      // data: [6, 1, 5, 2, 3, 9, 6, 2, 1, 0, 2, 8],
      data:r,
       marker: {
        enabled: true
      },
      //  dashStyle: 'shortdot',
      tooltip: {
          //  valueSuffix: ' mb'
        }

      },
      {
        name: 'Total Completed Complaints',
        type: 'spline',
       // data: [7.0, 6.9, 9.5, 14.5, 18.2, 21.5, 25.2, 26.5, 23.3, 18.3, 13.9, 9.6],
       data:c,
        tooltip: {
           // valueSuffix: ' °C'
         }
       }]
     });


    }



//call center Tat

 function currentmonthtat () {

        $.ajax({
          url: "graphs/chartClass.php?action=currentmonthtat",
          data: { },
          success: function( result ) {
            //console.log(result.data);
            if(typeof result.data!= 'undefined' && result.status == 'Success') {
                callcentertatgraphchart (result.data);
            } else {
                $('#call_center_complaint_tat_loader').html(result.msg);
            }

             
          },
          error: function(error) {
           console.log(error)
          }
        });
    }


    function callcentertatgraphchart(result) {
      
      var tat1 = [];
      var tat2 = [];
       for (var i = 1; i < result.length+1; i++) {
         // seriesList.push({name:result[i].month, data: [ parseFloat(result[i].tat1) ] });

         // seriesList.push({name:'Tat1', data: [ result.tat1 ] }, {name:'Tat2', data: [ result.tat2 ] });     
       }  

      tat1.push([result.tat1]); 
      tat2.push([result.tat2]);      

      // seriesListAttend.push([result.Marked[i]]);
     // seriesList.push({name:'Tat1', data: [ result.tat1 ] }, {name:'Tat2', data: [ result.tat2 ] });     
       //console.log(seriesList);

      // var data = [ttlAmnt];   
      //   Array.max = function (array) {
      //       return Math.max.apply(Math, array);
      //   };
      //   Array.min = function (array) {
      //       return Math.min.apply(Math, array);
      //   };

      //   var min = Array.min(data);
      //   var max = Array.max(data);

      var chart = new Highcharts.Chart({ 
        chart: {
          renderTo: 'currentmonthtatcallcenter',
              events: {
              load: function(event) {
                        //When is chart ready?
                        $(document).resize(); 
                    }
                },         
          type: 'column',
        },
        title: { text: '' },
         
        xAxis: {
              categories: [result.month]
          },
          yAxis: {
          //tickInterval: min,
           labels: {
          formatter: function () {
              return this.value;
          }
        }
      
       },
       credits: { enabled: false },
            tooltip: {
                headerFormat: '<b>{point.x}</b><br/>',
                pointFormat: '{series.name}: {point.y}<br/>'
            },
          plotOptions: {
              column: {
                  pointPadding: 0.2,
                  borderWidth: 0
              }
          },
         exporting: { enabled: false },
         series:[{
                name: 'Tat1',
                data: tat1
            }, {
                name: 'Tat2',
                color: 'Green',
                data: tat2
            }]
        });  

    }
























  



    




   






    