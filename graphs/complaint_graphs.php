 <style type="text/css">
  .loader { text-align: center;  }
  /*#ttlCallsDist h4{margin-top: -18px;
margin-left: 65px; font-size: 15px;}*/
#ttlCallsDist h5{margin-top: -15px;
margin-left: 65px; font-size: 15px;}
#ttlCallsDist h6{margin-top: -15px;
margin-left: 90px; font-size: 15px;}
/*#ttlCallsRet h4{margin-top: -18px;
margin-left: 65px; font-size: 15px;}*/
#ttlCallsRet h5{margin-top: -15px;
margin-left: 65px;font-size: 15px;}
#ttlCallsRet h6{margin-top: -15px;
margin-left: 90px; font-size: 15px;}
/*#ttlProductCallsDist h4{margin-top: -18px;
margin-left: 65px; font-size: 15px;}*/
#ttlProductCallsDist h5{margin-top: -15px;
margin-left: 200px; font-size: 15px;}
/*#ttlProductCallsRet h4{margin-top: -18px;
margin-left: 65px; font-size: 15px;}*/
#ttlProductCallsRet h5{margin-top: -15px;
margin-left: 180px; font-size: 15px;}
/*#newlyAddedDist h4{margin-top: -15px;
margin-left: 35px; font-size: 15px;}*/
#newlyAddedDist h5{margin-top: -15px;
margin-left: 35px; font-size: 15px;}
#newlyAddedDist h6{margin-top: -15px;
margin-left: 35px; font-size: 15px;}
/*#newlyAddedRet h4{margin-top: -15px;
margin-left: 35px; font-size: 15px;}*/
#newlyAddedRet h5{margin-top: -15px;
margin-left: 35px;font-size: 15px;}
#newlyAddedRet h6{margin-top: -15px;
margin-left: 35px; font-size: 15px;}

</style>














<div id="content-outer">




<!-- start content -->
<div id="content">


<div id="page-heading" align="left" >
  
  <form name="frmPre" id="frmPre" method="post" action="" enctype="multipart/form-data" >
  <table border="0" width="90%" cellpadding="0" cellspacing="0">
  <tr style="">
  
     <td><div style="color: #423B3B;font-size: 16px;font-weight: bold;line-height: 18px;margin-bottom: 7px">&nbsp;&nbsp;&nbsp;From Date: </div><h6><img src="css/images/prev.png" height="18" width="18" onclick="dateFromPrev();"> <input type="text" id="from" name="from" class="date" style="padding-right: 17px;height: 30px;padding: 6px 6px 0 6px;width: 187px;border-radius: 5px;-moz-border-radius: 5px;-o-border-radius: 5px;-ms-border-radius: 5px;-webkit-border-radius: 5px;" value="<?php echo date('d M Y', strtotime($_SESSION['FROM_DATE']));?>"  readonly /> <img src="css/images/next.png" height="18" width="18" onclick="dateFromNext();"></h6></td>
    
    <td><div style="color: #423B3B;font-size: 16px;font-weight: bold;line-height: 18px;margin-bottom: 7px">&nbsp;&nbsp;&nbsp;To Date: </div><h6><img src="css/images/prev.png" height="18" width="18" onclick="dateToPrev();"> <input type="text" id="to" name="to" class="date" style="padding-right: 17px;height: 30px;padding: 6px 6px 0 6px;width: 187px;border-radius: 5px;-moz-border-radius: 5px;-o-border-radius: 5px;-ms-border-radius: 5px;-webkit-border-radius: 5px;" value="<?php echo date('d M Y', strtotime($_SESSION['TO_DATE'])); ?>"  readonly /> <img src="css/images/next.png" height="18" width="18" onclick="dateToNext();"></h6></td>
    
    
     <td style="padding:5px;" width="300px;"><h3></h3>
        <input name="submit" class="result-submit" type="submit" id="submit" value="View Details" />
        <input type="button" value="Reset!" class="form-reset" onclick="location.href='index.php?reset=yes';" /> 
     </td>





     
    
   </tr>
  </table>
  </form>
  </div>




<aside class="right-side"> 
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1> Dashboard <small></small> </h1>
    <ol class="breadcrumb">
      <li><a href="#"> Dashboard</a></li>
    </ol>
  </section>
    
  <section class="content">  
  <div class="row">
    <h4 style="margin-left:22%;"> Call Center Complaints </h4>
    

      <div class="col-lg-3 col-xs-6"> 
        
        <div class="small-box bg-aqua">
          <div class="inner" id="ttlComplt">
            <h3><center><image src="images/ajax-loader.gif" /></center></h3>
            <p>Total Complaints </p>
           <!--  Total Complaint:<h4></h4> -->
           
          </div>
          <div class="icons"> <i class="fa fa-building-o"></i> </div>
          <!-- <span class="small-box-footer">
          <a href="total_calls_dist.php" target="_blank" class="btn btn-danger btn-sm">More Info<i style="padding-left:5px;" class="fa fa-arrow-circle-o-right"></i> </a> 
          </span> -->

           </div>
      </div>
      
      <div class="col-lg-3 col-xs-6"> 
        
        <div class="small-box bg-blue">
          <div class="inner" id="ttlPComplt">
            <h3><center><image src="images/ajax-loader.gif" /></center></h3>
            <p>Total Pending Complaints </p>
           <!--  Total Pending Complaint:<h4></h4> -->
           
            
          </div>
          <div class="icons"> <i class="fa fa-users"></i> </div>
          <!-- <span class="small-box-footer">
          <a href="total_productive_calls_dist.php" target="_blank" class="btn btn-danger btn-sm">More Info<i style="padding-left:5px;" class="fa fa-arrow-circle-o-right"></i> </a> 
          </span> -->
          </div>
      </div> 
       
      <div class="col-lg-3 col-xs-6"> 
        
        <div class="small-box bg-lime">
          <div class="inner" id="ttlIComplt">
            <h3><center><image src="images/ajax-loader.gif" /></center></h3>
            <p>Total In-Process Complaints </p>
            <!-- Total In-Process Complaint:<h4></h4> -->
           
          </div>
          <div class="icons"> <i class="fa fa-building-o"></i> </div>
         <!--  <span class="small-box-footer">
          <a href="total_calls_Ret.php" target="_blank" class="btn btn-danger btn-sm">More Info<i style="padding-left:5px;" class="fa fa-arrow-circle-o-right"></i> </a> 
          </span> -->
           </div>
      </div>
      
      <div class="col-lg-3 col-xs-6"> 
        
        <div class="small-box bg-orange">
          <div class="inner" id="ttlRComplt">
            <h3><center><image src="images/ajax-loader.gif" /></center></h3>
            <p>Total Rejected Complaints </p>
            <!-- Total Rejected Complaint:<h4></h4> -->
           
          </div>
          <div class="icons"> <i class="fa fa-users"></i> </div>
          <!-- <span class="small-box-footer">
          <a href="total_productive_calls_Ret.php" target="_blank" class="btn btn-danger btn-sm">More Info<i style="padding-left:5px;" class="fa fa-arrow-circle-o-right"></i> </a> 
          </span> --> </div>
      </div>
          
    </div>

    <div class="row"> 
      
      <div class="col-lg-3 col-xs-6"> 
        
        <div class="small-box bg-red">
          <div class="inner" id="ttlCComplt">
            <h3><center><image src="images/ajax-loader.gif" /></center></h3>
            <p>Total Completed Complaints </p>
            <!-- Total Completed Complaint:<h4></h4> -->
           <!--  New Order:<h5></h5>
            No Order Count:<h6></h6> -->
          </div>
          <div class="icons"> <i class="fa fa-building-o"></i> </div>
          <!-- <span class="small-box-footer">
          <a href="total_calls_dist.php" target="_blank" class="btn btn-danger btn-sm">More Info<i style="padding-left:5px;" class="fa fa-arrow-circle-o-right"></i> </a> 
          </span> -->
        </div>

      </div>
      
    </div> 


   
<!--  CallCenterComplaintComplaint Of Current Year -->
  <section class="col-lg-12 connectedSortable"> 

    <div class="box box-success">
      <div class="box-header"> <i class="fa fa-signal"></i>

        <h3 class="box-title"> Call Center Complaints </h3>
        <div class="pull-right box-tools">
          <button class="btn btn-success btn-xs" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse"><i class="fa fa-minus"></i></button>
        </div>
      </div>
      <div class="box-body" style="display: block;">
      <div id="call_center_complaint" style="height: 420px;" data-highcharts-chart="21">
          <div class="loader" id="call_center_complaint_loader"><center><image src="images/ajax-loader.gif" /></center></div>
        </div>
      </div>
    </div>
  </section>

  

   
<!--  CallCenter Tat DispLay -->
  <section class="col-lg-12 connectedSortable"> 

    <div class="box box-success">
      <div class="box-header"> <i class="fa fa-signal"></i>

        <h3 class="box-title"> Call Center Tat </h3>
        <div class="pull-right box-tools">
          <button class="btn btn-success btn-xs" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse"><i class="fa fa-minus"></i></button>
        </div>
      </div>
      <div class="box-body" style="display: block;">
      <div id="currentmonthtatcallcenter" style="height: 320px;" data-highcharts-chart="22">
          <div class="loader" id="call_center_complaint_tat_loader">
            <center><image src="images/ajax-loader.gif" /></center></div>
        </div>
      </div>
    </div>
  </section>
 

  </aside>
</div>

</div>

<script>
    $(function() {
        $( "#from" ).datepicker({
      dateFormat: "d M yy",
            defaultDate: "w",
            changeMonth: true,
            numberOfMonths: 1,
            onSelect: function( selectedDate ) {
                $( "#to" ).datepicker( "option", "minDate", selectedDate );
            }
        });
        $( "#to" ).datepicker({
      dateFormat: "d M yy",
            defaultDate: "-w",
            changeMonth: true,
            numberOfMonths: 1,
            onSelect: function( selectedDate ) {
                $( "#from" ).datepicker( "option", "maxDate", selectedDate );
            }
        });

         $( "#from1" ).datepicker({
      dateFormat: "d M yy",
            defaultDate: "-w",
            changeMonth: true,
            numberOfMonths: 1,
            onSelect: function( selectedDate ) {
                $( "#to1" ).datepicker( "option", "maxDate", selectedDate );
            }
        });
    });
</script>