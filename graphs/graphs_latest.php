 <style type="text/css">
  .loader { text-align: center;  }
  /*#ttlCallsDist h4{margin-top: -18px;
margin-left: 65px; font-size: 15px;}*/
#ttlCallsDist h5{margin-top: -15px;
margin-left: 65px; font-size: 15px;}
#ttlCallsDist h6{margin-top: -15px;
margin-left: 90px; font-size: 15px;}
/*#ttlCallsRet h4{margin-top: -18px;
margin-left: 65px; font-size: 15px;}*/
#ttlCallsRet h5{margin-top: -15px;
margin-left: 65px;font-size: 15px;}
#ttlCallsRet h6{margin-top: -15px;
margin-left: 90px; font-size: 15px;}
/*#ttlProductCallsDist h4{margin-top: -18px;
margin-left: 65px; font-size: 15px;}*/
#ttlProductCallsDist h5{margin-top: -15px;
margin-left: 200px; font-size: 15px;}
/*#ttlProductCallsRet h4{margin-top: -18px;
margin-left: 65px; font-size: 15px;}*/
#ttlProductCallsRet h5{margin-top: -15px;
margin-left: 180px; font-size: 15px;}
/*#newlyAddedDist h4{margin-top: -15px;
margin-left: 35px; font-size: 15px;}*/
#newlyAddedDist h5{margin-top: -15px;
margin-left: 35px; font-size: 15px;}
#newlyAddedDist h6{margin-top: -15px;
margin-left: 35px; font-size: 15px;}
/*#newlyAddedRet h4{margin-top: -15px;
margin-left: 35px; font-size: 15px;}*/
#newlyAddedRet h5{margin-top: -15px;
margin-left: 35px;font-size: 15px;}
#newlyAddedRet h6{margin-top: -15px;
margin-left: 35px; font-size: 15px;}

</style>














<div id="content-outer">




<!-- start content -->
<div id="content">


<div id="page-heading" align="left" >
  
  <form name="frmPre" id="frmPre" method="post" action="" enctype="multipart/form-data" >
  <table border="0" width="90%" cellpadding="0" cellspacing="0">
  <tr style="">
  
     <td><div style="color: #423B3B;font-size: 16px;font-weight: bold;line-height: 18px;margin-bottom: 7px">&nbsp;&nbsp;&nbsp;From Date: </div><h6><img src="css/images/prev.png" height="18" width="18" onclick="dateFromPrev();"> <input type="text" id="from" name="from" class="date" style="padding-right: 17px;height: 30px;padding: 6px 6px 0 6px;width: 187px;border-radius: 5px;-moz-border-radius: 5px;-o-border-radius: 5px;-ms-border-radius: 5px;-webkit-border-radius: 5px;" value="<?php echo date('d M Y', strtotime($_SESSION['FROM_DATE']));?>"  readonly /> <img src="css/images/next.png" height="18" width="18" onclick="dateFromNext();"></h6></td>
    
    <td><div style="color: #423B3B;font-size: 16px;font-weight: bold;line-height: 18px;margin-bottom: 7px">&nbsp;&nbsp;&nbsp;To Date: </div><h6><img src="css/images/prev.png" height="18" width="18" onclick="dateToPrev();"> <input type="text" id="to" name="to" class="date" style="padding-right: 17px;height: 30px;padding: 6px 6px 0 6px;width: 187px;border-radius: 5px;-moz-border-radius: 5px;-o-border-radius: 5px;-ms-border-radius: 5px;-webkit-border-radius: 5px;" value="<?php echo date('d M Y', strtotime($_SESSION['TO_DATE'])); ?>"  readonly /> <img src="css/images/next.png" height="18" width="18" onclick="dateToNext();"></h6></td>
    
    
     <td style="padding:5px;" width="300px;"><h3></h3>
        <input name="submit" class="result-submit" type="submit" id="submit" value="View Details" />
        <input type="button" value="Reset!" class="form-reset" onclick="location.href='index.php?reset=yes';" /> 
     </td>





     
    
   </tr>
  </table>
  </form>
  </div>




<aside class="right-side"> 
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1> Dashboard(Current Month) <small></small> </h1>
    <ol class="breadcrumb">
      <li><a href="#"> Dashboard (Current Month)</a></li>
    </ol>
  </section>
    
  <section class="content">  
  <div class="row">
    <h4 style="margin-left:22%;"> Distributors </h4>
    <h4 style="margin-left: 72%; margin-top: -2.2%;"> Dealers </h4>

      <div class="col-lg-3 col-xs-6"> 
        
        <div class="small-box bg-light-blue">
          <div class="inner" id="ttlCallsDist">
            <h3><center><image src="images/ajax-loader.gif" /></center></h3>
            <p>Distributors Total Calls </p>
            <!-- Total Call:<h4></h4> -->
            New Order:<h5></h5>
            No Order Count:<h6></h6>
          </div>
          <div class="icons"> <i class="fa fa-building-o"></i> </div>
          <!-- <span class="small-box-footer">
          <a href="total_calls_dist.php" target="_blank" class="btn btn-danger btn-sm">More Info<i style="padding-left:5px;" class="fa fa-arrow-circle-o-right"></i> </a> 
          </span> -->

           </div>
      </div>
      
      <div class="col-lg-3 col-xs-6"> 
        
        <div class="small-box bg-orange">
          <div class="inner" id="ttlProductCallsDist">
            <h3><center><image src="images/ajax-loader.gif" /></center></h3>
            <p> Distributors Productive Calls </p>
            <!--  Total Call:<h4></h4> -->
            Order Taken from no. of distributors:<h5></h5>
            
          </div>
          <div class="icons"> <i class="fa fa-users"></i> </div>
          <!-- <span class="small-box-footer">
          <a href="total_productive_calls_dist.php" target="_blank" class="btn btn-danger btn-sm">More Info<i style="padding-left:5px;" class="fa fa-arrow-circle-o-right"></i> </a> 
          </span> -->
          </div>
      </div> 
       
      <div class="col-lg-3 col-xs-6"> 
        
        <div class="small-box bg-light-blue">
          <div class="inner" id="ttlCallsRet">
            <h3><center><image src="images/ajax-loader.gif" /></center></h3>
            <p>Dealers Total Calls </p>
            <!-- Total Call:<h4></h4> -->
            New Order:<h5></h5>
            No Order Count:<h6></h6>
          </div>
          <div class="icons"> <i class="fa fa-building-o"></i> </div>
         <!--  <span class="small-box-footer">
          <a href="total_calls_Ret.php" target="_blank" class="btn btn-danger btn-sm">More Info<i style="padding-left:5px;" class="fa fa-arrow-circle-o-right"></i> </a> 
          </span> -->
           </div>
      </div>
      
      <div class="col-lg-3 col-xs-6"> 
        
        <div class="small-box bg-orange">
          <div class="inner" id="ttlProductCallsRet">
            <h3><center><image src="images/ajax-loader.gif" /></center></h3>
            <p>Dealers Productive Calls </p>
            <!-- Total Call:<h4></h4> -->
            Order Taken from no. of Dealers:<h5></h5>
          </div>
          <div class="icons"> <i class="fa fa-users"></i> </div>
          <!-- <span class="small-box-footer">
          <a href="total_productive_calls_Ret.php" target="_blank" class="btn btn-danger btn-sm">More Info<i style="padding-left:5px;" class="fa fa-arrow-circle-o-right"></i> </a> 
          </span> --> </div>
      </div>
          
    </div>






  <div class="row"> 
      
      <div class="col-lg-3 col-xs-6"> 
        
        <div class="small-box bg-grass">
          <div class="inner" id="ttlAmntsDist">
            <h3><center><image src="images/ajax-loader.gif" /></center> <i class="fa fa-inr"></i></h3>
            <p>Distributors Total amount of order  </p>
          </div>
          <div class="icons"> <i class="fa fa-user"></i> </div>
          <!-- <span class="small-box-footer">
          <a href="total_order_ammount_dist.php"  target="_blank" class="btn btn-danger btn-sm">More Info<i style="padding-left:5px;" class="fa fa-arrow-circle-o-right"></i> </a> 
          </span> --> </div>
      </div>
      
      <div class="col-lg-3 col-xs-6"> 
        
        <div class="small-box bg-wood">
          <div class="inner" id="newlyAddedDist">
            <h3><center><image src="images/ajax-loader.gif" /></center></h3>
            <p>Total New Distributors </p>
             <!-- Total:<h4> </h4> -->
            <!--  Hot:<h5></h5>
            Cold:<h6></h6> -->
          </div>
          <div class="icons"> <i class="fa fa-desktop"></i> </div>
         <!--  <span class="small-box-footer">
          <a href="total_newAdded_dist.php" target="_blank" class="btn btn-danger btn-sm">More Info<i style="padding-left:5px;" class="fa fa-arrow-circle-o-right"></i> </a> 
          </span> --> </div>
      </div>
     
      <div class="col-lg-3 col-xs-6"> 
        
        <div class="small-box bg-grass">
          <div class="inner" id="ttlAmntsRet">
            <h3><center><image src="images/ajax-loader.gif" /></center> <i class="fa fa-inr"></i></h3>
            <p>Dealers Total amount of order  </p>
          </div>
          <div class="icons"> <i class="fa fa-user"></i> </div>
          <!-- <span class="small-box-footer">
          <a href="total_order_ammount_ret.php" target="_blank" class="btn btn-danger btn-sm">More Info<i style="padding-left:5px;" class="fa fa-arrow-circle-o-right"></i> </a> 
          </span> --> </div>
      </div>
      
      <div class="col-lg-3 col-xs-6"> 
        
        <div class="small-box bg-wood">
          <div class="inner" id="newlyAddedRet">
            <h3><center><image src="images/ajax-loader.gif" /></center></h3>
            <p>Total New Dealers </p>
            <!-- Total:<h4> </h4> -->
          <!--  Hot:<h5></h5>
            Cold:<h6></h6> -->
          </div>
          <div class="icons"> <i class="fa fa-desktop"></i> </div>
          <!-- <span class="small-box-footer">
          <a href="total_newAdded_ret.php" target="_blank" class="btn btn-danger btn-sm">More Info<i style="padding-left:5px;" class="fa fa-arrow-circle-o-right"></i> </a> 
          </span> --> </div>
      </div>
      
    </div> 


     <div class="row">   
      
      <div class="col-lg-6 col-xs-6"> 
        
        <div class="small-box bg-red">
          <div class="inner" id="ttlAssignedDist">
             <p>Total Assigned Distributors </p>
            <h3><center><image src="images/ajax-loader.gif" /></center></h3>
          </div>
          <div class="icons"> <i class="fa fa-users"></i> </div>
          <!-- <span class="small-box-footer">
          <a href="total_assigned_Dist.php" target="_blank" class="btn btn-primary btn-sm">More Info<i style="padding-left:5px;" class="fa fa-arrow-circle-o-right"></i> </a> 
          </span> --> 
        </div>
      </div>

      <div class="col-lg-6 col-xs-6"> 
        
        <div class="small-box bg-red">
          <div class="inner" id="ttlAssignedRet">
             <p>Total Assigned Dealers </p>             
            <h3><center><image src="images/ajax-loader.gif" /></center></h3>                       
          </div>
          <div class="icons"> <i class="fa fa-users"></i> </div>
          <!-- <span class="small-box-footer">
          <a href="total_assigned_Ret.php" target="_blank" class="btn btn-primary btn-sm">More Info<i style="padding-left:5px;" class="fa fa-arrow-circle-o-right"></i> </a> 
          </span> -->
           </div>
      </div>
  </div> 


<div class="row">
   <div class="box box-success">
          <div class="box-header"> <i class="fa fa-rocket"></i>
            <h4 class="box-title" width="200px;">Salesman Attendance</h4>&nbsp;&nbsp;&nbsp;&nbsp;
          <form name="salesman_attendance" method="post">  
           <div  align="center"><img src="css/images/prev.png" height="18" width="18" onclick="dateFromPrev();"> <input type="text" id="from1" name="from1" class="date" style="padding-right: 17px;height: 30px;padding: 6px 6px 0 6px;width: 187px;border-radius: 5px;-moz-border-radius: 5px;-o-border-radius: 5px;-ms-border-radius: 5px;-webkit-border-radius: 5px;" value="<?php  echo date('d M Y', strtotime($_SESSION['DATE_ATTENDANCE']));?>"  readonly /> <img src="css/images/next.png" height="18" width="18" onclick="dateFromNext();">
             <input name="submit" class="result-submit" type="submit" id="submit" value="Refresh" />
             <input type="hidden" name="attendance" value="attendance">
            </div>
            </form>
      <div class="pull-right box-tools">
        <button class="btn btn-success btn-xs" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse"><i class="fa fa-minus"></i></button>
      </div><!-- /. tools -->
          </div>
          <div class="box-body" style="display: block;">
            <div id="salesman_attendence"  data-highcharts-chart="1" style="min-width: 310px; height: 400px; margin: 0 auto">
            <center><image src="images/ajax-loader.gif" /></center></div>
          </div>
        </div>
</div>




<section class="col-lg-6 connectedSortable"> 
        <!-- interactive chart -->
        <div class="box box-success">
          <div class="box-header"> <i class="fa fa-signal"></i>
      <div style="display: none;" id="count_start">1170</div>
            <h3 class="box-title">Top 10 Distributors </h3>
      <div class="pull-right box-tools">
        <button class="btn btn-success btn-xs" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse"><i class="fa fa-minus"></i></button>
      </div>
          </div>
          <div class="box-body" style="display: block;">
            <div id="top10Distributors" style="height: 420px;" data-highcharts-chart="2">
            <div class="loader" id="top10DistributorsLoader"><center><image src="images/ajax-loader.gif" /></center></div>
            </div>
          </div>
        </div>
      </section>

<section class="col-lg-6 connectedSortable"> 
        <!-- interactive chart -->
        <div class="box box-info">
          <div class="box-header"> <i class="fa fa-signal"></i>
      <div style="display: none;" id="count_start">1170</div>
            <h3 class="box-title">Top 10 Dealers </h3>
      <div class="pull-right box-tools">
        <button class="btn btn-info btn-xs" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse"><i class="fa fa-minus"></i></button>
      </div>
          </div>
          <div class="box-body" style="display: block;">
            <div id="top10Retailers" style="height: 420px;" data-highcharts-chart="3">
            <div class="loader" id="top10RetailersLoader"><center><image src="images/ajax-loader.gif" /></center></div>
            </div>
          </div>
        </div>
      </section>

 <!--  detailed analysis  / -->
    <section class="col-lg-6 connectedSortable"> 
      <!-- Box (with bar chart) -->
      <div class="box box-success">
        <div class="box-header">
        <i class="fa fa-line-chart"></i>
                <h3 class="box-title">Order Performance For Distributors</h3>
        <div class="pull-right box-tools">
        <button class="btn btn-success btn-xs" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse"><i class="fa fa-minus"></i></button>
      </div>
        </div>
        <div class="box-body no-padding" style="display: block;">
          <div class="box-body" style="display:block;">
              <div class="chart" id="distributor_order_performance" style="height: 420px;" data-highcharts-chart="4">
                <div class="loader" id="distributor_order_performanceLoader"><center><image src="images/ajax-loader.gif" /></center></div>
              </div>
            </div>
          
        </div>
      </div>

    </section>  
    <!--  detailed analysis  / -->
    <section class="col-lg-6 connectedSortable"> 
      <!-- Box (with bar chart) -->
      <div class="box box-info">
        <div class="box-header">
        <i class="fa fa-line-chart"></i>
                <h3 class="box-title">Order Performance For Dealers</h3>
        <div class="pull-right box-tools">
        <button class="btn btn-info btn-xs" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse"><i class="fa fa-minus"></i></button>
      </div>
        </div>
        <div class="box-body no-padding" style="display: block;">
          <div class="box-body" style="display:block;">
              <div class="chart" id="order_performance_for_retailers" style="height: 420px;" data-highcharts-chart="5">
               <div class="loader" id="order_performance_for_retailersLoader"><center><image src="images/ajax-loader.gif" /></center></div>
              </div>
            </div>
          
        </div>
      </div>

    </section>

    <!-- Top 10 products by values Distributors-->
        <section class="col-lg-6 connectedSortable"> 
        <!-- interactive chart -->
        <div class="box box-success">
        <div class="box-header"><i class="fa fa-rocket"></i>
        <h3 class="box-title">Top 10 Product- Distributors </h3>
        <div class="pull-right box-tools">
        <button class="btn btn-success btn-xs" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse"><i class="fa fa-minus"></i></button>
        </div>
        </div>
        <div class="box-body" style="display: block;">
        <div id="top10_products_distributor" style="height: 420px;" data-highcharts-chart="6">
          <div class="loader" id="top10_products_distributorLoader"><center><image src="images/ajax-loader.gif" /></center></div>          
        </div>
        </div>
        </div>
        </section>

        <!-- Top 10 products by values Dealers-->
        <section class="col-lg-6 connectedSortable"> 
        <!-- interactive chart -->
        <div class="box box-info">
        <div class="box-header"><i class="fa fa-rocket"></i>
        <h3 class="box-title">Top 10 Dealers Product </h3>
        <div class="pull-right box-tools">
        <button class="btn btn-info btn-xs" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse"><i class="fa fa-minus"></i></button>
        </div>
        </div>
        <div class="box-body" style="display: block;">
        <div id="top10_products_retailers" style="height: 420px;" data-highcharts-chart="7">
          <div class="loader" id="top10_products_retailersLoader"><center><image src="images/ajax-loader.gif" /></center></div>
        </div>
        </div>
        </div>
        </section>



         <!-- top 5 distributors city -->
        <section class="col-lg-6 connectedSortable">  
        <!-- auth response line  -->
        <div class="box box-success">
        <div class="box-header"> <i class="fa fa-rocket"></i>
        <h4 class="box-title">Top 5 City- Distributors</h4>
        <div class="pull-right box-tools">
        <button class="btn btn-success btn-xs" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse"><i class="fa fa-minus"></i></button>
        </div><!-- /. tools -->
        </div>
        <div class="box-body" style="display: block;">
        <div id="top5cities_distributor" style="height:420px;" data-highcharts-chart="8"><div class="loader" id="top5cities_distributorLoader"><center><image src="images/ajax-loader.gif" /></center></div></div>
        </div>
        </div>
        </section>


         <!-- top 5 Dealers city -->
        <section class="col-lg-6 connectedSortable">  
        <!-- auth response line  -->
        <div class="box box-info">
        <div class="box-header"> <i class="fa fa-rocket"></i>
        <h4 class="box-title">Top 5 City- Dealers</h4>
        <div class="pull-right box-tools">
        <button class="btn btn-info btn-xs" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse"><i class="fa fa-minus"></i></button>
        </div><!-- /. tools -->
        </div>
        <div class="box-body" style="display: block;">
        <div id="top5cities_retailer" style="height:420px;" data-highcharts-chart="9"><div class="loader" id="top5cities_retailerLoader"><center><image src="images/ajax-loader.gif" /></center></div></div>
        </div>
        </div>
        </section>



        <!--   For Top 10 Distributors & Order Performance --> 

    

      <section class="col-lg-6 connectedSortable">  
        <!-- auth response line  -->
        <div class="box box-success">
          <div class="box-header"> <i class="fa fa-rocket"></i>
            <h4 class="box-title">Top 5 Salesmen For Distributors</h4>
      <div class="pull-right box-tools">
        <button class="btn btn-success btn-xs" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse"><i class="fa fa-minus"></i></button>
      </div><!-- /. tools -->
          </div>
          <div class="box-body" style="display: block;">
            <div id="top5DisSalesman" style="height:420px;" data-highcharts-chart="10"><div class="loader" id="top5DisSalesmanLoader"><center><image src="images/ajax-loader.gif" /></center></div></div>
          </div>
        </div>
      </section>

       <section class="col-lg-6 connectedSortable">  
        <!-- auth response line  -->
        <div class="box box-info">
          <div class="box-header"> <i class="fa fa-rocket"></i>
            <h4 class="box-title">Top 5 Salesmen For Dealers</h4>
      <div class="pull-right box-tools">
        <button class="btn btn-info btn-xs" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse"><i class="fa fa-minus"></i></button>
      </div><!-- /. tools -->
          </div>
          <div class="box-body" style="display: block;">
            <div id="top5RetSalesman" style="height:420px;" data-highcharts-chart="11"><div class="loader" id="top5RetSalesmanLoader"><center><image src="images/ajax-loader.gif" /></center></div></div>
          </div>
        </div>
      </section>

      <!-- top 5 distributors products -->
         <section class="col-lg-6 connectedSortable">  
        <!-- auth response line  -->
        <div class="box box-success">
        <div class="box-header"> <i class="fa fa-rocket"></i>
        <h4 class="box-title">Top 5 Products- Distributors </h4>
        <div class="pull-right box-tools">
        <button class="btn btn-success btn-xs" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse"><i class="fa fa-minus"></i></button>
        </div><!-- /. tools -->
        </div>
        <div class="box-body" style="display: block;">
        <div id="top5_products_distributor" style="height:420px;" data-highcharts-chart="12"><div class="loader" id="top5_products_distributorLoader"><center><image src="images/ajax-loader.gif" /></center></div></div>
        </div>
        </div>
        </section>

         <!-- top 5 Dealers products -->
        <section class="col-lg-6 connectedSortable">  
        <!-- auth response line  -->
        <div class="box box-info">
        <div class="box-header"> <i class="fa fa-rocket"></i>
        <h4 class="box-title">Top 5 Products- Dealers </h4>
        <div class="pull-right box-tools">
        <button class="btn btn-info btn-xs" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse"><i class="fa fa-minus"></i></button>
        </div><!-- /. tools -->
        </div>
        <div class="box-body" style="display: block;">
        <div id="top5_products_retailer" style="height:420px;" data-highcharts-chart="13"><div class="loader" id="top5_products_retailerLoader"><center><image src="images/ajax-loader.gif" /></center></div></div>
        </div>
        </div>
        </section>



        <!-- no order reason wise graph distributors-->
        <section class="col-lg-6 connectedSortable">  
        <!-- auth response line  -->
        <div class="box box-success">
        <div class="box-header"> <i class="fa fa-thumbs-down"></i>
        <h4 class="box-title">No Order Reason Wise- Distributors</h4>
        <div class="pull-right box-tools">
        <button class="btn btn-success btn-xs" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse"><i class="fa fa-minus"></i></button>
        </div><!-- /. tools -->
        </div>
        <div class="box-body" style="display: block;">
        <div id="no_order_reason_distributor" style="height:420px;" data-highcharts-chart="14"><div class="loader" id="no_order_reason_distributorLoader"><center><image src="images/ajax-loader.gif" /></center></div></div>
        </div>
        </div>
        </section>

        <!-- no order reason wise graph Retailers-->
        <section class="col-lg-6 connectedSortable">  
        <!-- auth response line  -->
        <div class="box box-info">
        <div class="box-header"> <i class="fa fa-thumbs-down"></i>
        <h4 class="box-title">No Order Reason Wise- Dealers</h4>
        <div class="pull-right box-tools">
        <button class="btn btn-info btn-xs" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse"><i class="fa fa-minus"></i></button>
        </div><!-- /. tools -->
        </div>
        <div class="box-body" style="display: block;">
        <div id="no_order_reason_retailer" style="height:420px;" data-highcharts-chart="15"><div class="loader" id="no_order_reason_retailerLoader"><center><image src="images/ajax-loader.gif" /></center></div></div>
        </div>
        </div>
        </section>

        <section class="col-lg-6 connectedSortable"> 
        <!-- employee registration bar graph -->
        <div class="box box-success">
          <div class="box-header"> <i class="fa fa-clock-o"></i>
            <h3 class="box-title">Distributors Share Market</h3>
        <div class="pull-right box-tools">
        <button class="btn btn-success btn-xs" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse"><i class="fa fa-minus"></i></button>
         </div><!-- /. tools -->
          </div>
          <div class="box-body" style="display: block;">
            <div id="distributors_share" style="height:420px;" data-highcharts-chart="16"><div class="loader" id="distributors_shareLoader"><center><image src="images/ajax-loader.gif" /></center></div></div>
          </div>
        </div>
        </section>


         <!-- New order/no order Graph(Distributor/Dealers)-->
      <section class="col-lg-6 connectedSortable">  
        <!-- auth response line  -->
        <div class="box box-success">
          <div class="box-header"> <i class="fa fa-line-chart"></i>
            <h4 class="box-title">New order / No order </h4>
      <div class="pull-right box-tools">
        <button class="btn btn-success btn-xs" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse"><i class="fa fa-minus"></i></button>
      </div><!-- /. tools -->
          </div>
          <div class="box-body" style="display: block;">
            <div id="no_new_order" style="height:420px;" data-highcharts-chart="17"><div class="loader" id="no_new_orderLoader"><center><image src="images/ajax-loader.gif" /></center></div></div>
          </div>
        </div>
      </section>


       <!-- Hot/Cold Graph(Distributor/Dealers)-->
      <!-- <section class="col-lg-6 connectedSortable">
        <div class="box box-success">
          <div class="box-header"> <i class="fa fa-rocket"></i>
            <h4 class="box-title">Hot / Cold </h4>
      <div class="pull-right box-tools">
        <button class="btn btn-success btn-xs" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse"><i class="fa fa-minus"></i></button>
      </div>
          </div>
          <div class="box-body" style="display: block;">
            <div id="hot_cold" style="height:420px;" data-highcharts-chart="18"><div class="loader" id="hot_coldLoader"><center><image src="images/ajax-loader.gif" /></center></div></div>
          </div>
        </div>
      </section> -->


      <!-- no order reason wise graph -->
    <section class="col-lg-6 connectedSortable">  
        <!-- auth response line  -->
        <div class="box box-success">
            <div class="box-header"> <i class="fa fa-thumbs-down"></i>
                <h4 class="box-title">No Order Reason Wise</h4>
                <div class="pull-right box-tools">
                    <button class="btn btn-success btn-xs" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse"><i class="fa fa-minus"></i></button>
                </div><!-- /. tools -->
            </div>
            <div class="box-body" style="display: block;">
                <div id="no_order_reason" style="height:420px;" data-highcharts-chart="19"><div class="loader" id="no_order_reasonLoader"><center><image src="images/ajax-loader.gif" /></center></div></div>
            </div>
        </div>
    </section>

<!-- Top 10 products by values -->

 <!-- <section class="col-lg-6 connectedSortable">
        <div class="box box-info">
          <div class="box-header"><i class="fa fa-rocket"></i>
            <h3 class="box-title">Top 10 Products </h3>
      <div class="pull-right box-tools">
        <button class="btn btn-info btn-xs" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse"><i class="fa fa-minus"></i></button>
      </div>
          </div>
          <div class="box-body" style="display: block;">
            <div id="top10_products" style="height: 420px;" data-highcharts-chart="20"></div>
          </div>
        </div>
  </section> -->


    <section class="col-lg-12 connectedSortable"> 

      <div class="box box-success">
        <div class="box-header"> <i class="fa fa-signal"></i>

          <h3 class="box-title"> Distributor Complaints </h3>
          <div class="pull-right box-tools">
            <button class="btn btn-success btn-xs" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse"><i class="fa fa-minus"></i></button>
          </div>
        </div>
        <div class="box-body" style="display: block;">
          <div id="distributorComplaint" style="height: 420px;" data-highcharts-chart="21">
            <div class="loader" id="distributorComplaint"><center><image src="images/ajax-loader.gif" /></center></div>
          </div>
        </div>
      </div>
    </section> 


    <!-- Monthly DealerComplaint Of Current Year -->
  <section class="col-lg-12 connectedSortable"> 

    <div class="box box-success">
      <div class="box-header"> <i class="fa fa-signal"></i>

        <h3 class="box-title"> Dealers Complaints </h3>
        <div class="pull-right box-tools">
          <button class="btn btn-success btn-xs" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse"><i class="fa fa-minus"></i></button>
        </div>
      </div>
      <div class="box-body" style="display: block;">
      <div id="dealersComplaint" style="height: 420px;" data-highcharts-chart="22">
          <div class="loader" id="dealers_complaint_loader"><center><image src="images/ajax-loader.gif" /></center></div>
        </div>
      </div>
    </div>
  </section>

  <!--  CallCenterComplaintComplaint Of Current Year -->
  <section class="col-lg-12 connectedSortable"> 

    <div class="box box-success">
      <div class="box-header"> <i class="fa fa-signal"></i>

        <h3 class="box-title"> Call Center Complaints </h3>
        <div class="pull-right box-tools">
          <button class="btn btn-success btn-xs" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse"><i class="fa fa-minus"></i></button>
        </div>
      </div>
      <div class="box-body" style="display: block;">
      <div id="call_center_complaint" style="height: 420px;" data-highcharts-chart="21">
          <div class="loader" id="call_center_complaint_loader"><center><image src="images/ajax-loader.gif" /></center></div>
        </div>
      </div>
    </div>
  </section>



  </aside>
</div>

</div>

<script>
    $(function() {
        $( "#from" ).datepicker({
      dateFormat: "d M yy",
            defaultDate: "w",
            changeMonth: true,
            numberOfMonths: 1,
            onSelect: function( selectedDate ) {
                $( "#to" ).datepicker( "option", "minDate", selectedDate );
            }
        });
        $( "#to" ).datepicker({
      dateFormat: "d M yy",
            defaultDate: "-w",
            changeMonth: true,
            numberOfMonths: 1,
            onSelect: function( selectedDate ) {
                $( "#from" ).datepicker( "option", "maxDate", selectedDate );
            }
        });

         $( "#from1" ).datepicker({
      dateFormat: "d M yy",
            defaultDate: "-w",
            changeMonth: true,
            numberOfMonths: 1,
            onSelect: function( selectedDate ) {
                $( "#to1" ).datepicker( "option", "maxDate", selectedDate );
            }
        });
    });
</script>