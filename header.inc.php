
<?php
//include("includes/config.inc.php");
//include("includes/function.php");
$ajaxUrl = "ajaxFunctions.php";
$_objArrayList = new ArrayList();
$_objAdmin = new Admin();
$userTypeSalesTeamID = $_objArrayList->getsaleteamID();
$hierarchyIDofloggedUser = $_objArrayList->getHierarchyIDoFSalesTeam($userTypeSalesTeamID[0]);
$_objModuleClass = new ModuleClass();
$fetaurearray = $_objModuleClass->getEnableModuleList();
?>


<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8" />
        <?php
        if ($page_name != '') {
            ?>
            <title><?php echo $page_name; ?></title>
        <?php } else { ?>
            <title>WhiteSpot Admin - Quy Technologies</title>
        <?php } ?>
        <meta name="description" content="Latest updates and statistic charts">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!--begin::Web font -->
        <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
        <script>
            WebFont.load({
                google: {"families": ["Poppins:300,400,500,600,700", "Roboto:300,400,500,600,700"]},
                active: function () {
                    sessionStorage.fonts = true;
                }
            });
        </script>


        <link href="assets/vendors/custom/fullcalendar/fullcalendar.bundle.css" rel="stylesheet" type="text/css" />
        <link href="assets/vendors/custom/fullcalendar/scheduler.min.css" rel="stylesheet" type="text/css" />
        <!--end::Page Vendors -->
        <link href="assets/vendors/base/vendors.bundle.css" rel="stylesheet" type="text/css" />
        <link href="assets/demo/default/base/style.bundle.css" rel="stylesheet" type="text/css" />
        <!--end::Base Styles -->
        <link rel="shortcut icon" href="assets/demo/default/media/img/logo/favicon.ico" />
        <link href="css/newStyle.css" rel="stylesheet" type="text/css"/>
       
        <script type="text/javascript">

        function route_load(month, contailerId, year, salesman, route)
        {
            //alert(month);
            //alert(route);
            //alert(salesman);
            var year = $('#year').val();
            $.ajax({
                'type': 'GET',
                'url': 'ajaxrouteedit.php',
                'data': 'year=' + year + '&month=' + month + '&contailerId=' + contailerId + '&salesman=' + salesman + '&route_schedule_id=' + route,
                'success': function (callday) {
                    //alert(callday);
                    //console.log(callday);
                    $('#' + contailerId).html(callday);
                    $('#items').show();
                    $('#addmore').show();
                    $('#save').show();
                }
            });

        }
    </script>
    </head>
    <!-- Tooltips -->
    
<?php
if ($_SESSION['userLoginType'] == 2) {
    $opRec = $_objAdmin->_getSelectList('table_account_admin', "*", '', " operator_id=" . $_SESSION['operatorId']);
    $opRec[0]->manage_items;
}
?>
<body id="page-top" class=" fixed-nav sticky-footer bg-dark m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default">
    <!-- begin:: Page -->
    <div class="m-grid m-grid--hor m-grid--root m-page">
        <!-- BEGIN: Header -->
        <header class="m-grid__item    m-header "  data-minimize-offset="200" data-minimize-mobile-offset="200" >
            <div class="m-container m-container--fluid m-container--full-height">
                <div class="m-stack m-stack--ver m-stack--desktop">
                    <!-- BEGIN: Brand -->
                    <div class="m-stack__item m-brand  m-brand--skin-dark ">
                        <div class="m-stack m-stack--ver m-stack--general">
                            <div class="m-stack__item m-stack__item--middle m-brand__logo">
                                <a href="index.php" class="m-brand__logo-wrapper">
                                    <img alt="" src="assets/demo/default/media/img/logo/WS_logo.png"/>
                                </a>
                            </div>
                            <div class="m-stack__item m-stack__item--middle m-brand__tools">
                                <!-- BEGIN: Left Aside Minimize Toggle -->
                                <a href="javascript:;" id="m_aside_left_minimize_toggle" class="m-brand__icon m-brand__toggler m-brand__toggler--left m--visible-desktop-inline-block 
                                   ">
                                    <span></span>
                                </a>
                                <!-- END -->
                                <!-- BEGIN: Responsive Aside Left Menu Toggler -->
                                <a href="javascript:;" id="m_aside_left_offcanvas_toggle" class="m-brand__icon m-brand__toggler m-brand__toggler--left m--visible-tablet-and-mobile-inline-block">
                                    <span></span>
                                </a>
                                <!-- END -->
                                <!-- BEGIN: Responsive Header Menu Toggler -->
                                
                                <!-- END -->
                                <!-- BEGIN: Topbar Toggler -->
                                <a id="m_aside_header_topbar_mobile_toggle" href="javascript:;" class="m-brand__icon m--visible-tablet-and-mobile-inline-block">
                                    <i class="flaticon-more"></i>
                                </a>
                                <!-- BEGIN: Topbar Toggler -->
                            </div>
                        </div>
                    </div>
                    <!-- END: Brand -->
                    <div class="m-stack__item m-stack__item--fluid m-header-head" id="m_header_nav" style="background-color: #fff !important;">
                        <!-- BEGIN: Horizontal Menu -->
                        <button class="m-aside-header-menu-mobile-close  m-aside-header-menu-mobile-close--skin-dark " id="m_aside_header_menu_mobile_close_btn">
                            <i class="la la-close"></i>
                        </button>
                        <div id="m_header_menu" class="m-header-menu m-aside-header-menu-mobile m-aside-header-menu-mobile--offcanvas  m-header-menu--skin-light m-header-menu--submenu-skin-light m-aside-header-menu-mobile--skin-dark m-aside-header-menu-mobile--submenu-skin-dark "  >

                        </div>
                        <!-- END: Horizontal Menu -->								<!-- BEGIN: Topbar -->
                        <div id="m_header_topbar" class="m-topbar  m-stack m-stack--ver m-stack--general">
                            <div class="m-stack__item m-topbar__nav-wrapper">
                                <ul class="m-topbar__nav m-nav m-nav--inline"> 
                                    <li class="m-nav__item m-topbar__user-profile m-topbar__user-profile--img  m-dropdown m-dropdown--medium m-dropdown--arrow m-dropdown--header-bg-fill m-dropdown--align-right m-dropdown--mobile-full-width m-dropdown--skin-light" data-dropdown-toggle="click">
                                        <a href="#" class="m-nav__link m-dropdown__toggle">
                                            <span class="m-topbar__userpic">
                                                <img src="images/user.png" class="m--img-rounded m--marginless m--img-centered" alt=""/>
                                            </span>
                                            <span class="m-topbar__username m--hide">
                                                Nick
                                            </span>
                                        </a>
                                        <div class="m-dropdown__wrapper">
                                            <span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>
                                            <div class="m-dropdown__inner">
                                                <div class="m-dropdown__header m--align-center" style="background: url(assets/app/media/img/misc/user_profile_bg.jpg); background-size: cover;">
                                                    <div class="m-card-user m-card-user--skin-dark">
                                                        <div class="m-card-user__pic">
                                                            <img src="images/user.png" class="m--img-rounded m--marginless" alt=""/>
                                                        </div>
                                                        <div class="m-card-user__details">
                                                            <span class="m-card-user__name m--font-weight-500">
                                                                <?php echo $_SESSION['comusername']; ?>
                                                            </span>
                                                            <a href="" class="m-card-user__email m--font-weight-300 m-link">
                                                               <?php echo $_SESSION['email_id']; ?>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="m-dropdown__body">
                                                    <div class="m-dropdown__content">
                                                        <ul class="m-nav m-nav--skin-light">
                                                            <li class="m-nav__section m--hide">
                                                                <span class="m-nav__section-text">
                                                                    Section
                                                                </span>
                                                            </li>
                                                            <li class="m-nav__item">
                                                                <a href="profile.php" class="m-nav__link">
                                                                    <i class="m-nav__link-icon flaticon-profile-1"></i>
                                                                    <span class="m-nav__link-title">
                                                                        <span class="m-nav__link-wrap">
                                                                            <span class="m-nav__link-text">
                                                                                My Profile
                                                                            </span>
                                                                        </span>
                                                                    </span>
                                                                </a>
                                                            </li>  
                                                            <li class="m-nav__separator m-nav__separator--fit"></li>
                                                            <li class="m-nav__item">
                                                                <a style=" cursor: pointer" class="nav-link" data-toggle="modal" data-target="#m_modal_1">
                                                                    <i class="fa fa-fw fa-sign-out"></i>Logout
                                                                </a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li> 
                                </ul>
                            </div>
                        </div>
                        <!-- END: Topbar -->
                    </div>
                </div>
            </div>
        </header>
        <div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
            <!-- BEGIN: Left Aside -->
            <button class="m-aside-left-close  m-aside-left-close--skin-dark " onclick="closeMenu()" id="m_aside_left_close_btn">
                <i class="la la-close"></i>
            </button>
            <!-- Navigation-->
               <!-- BEGIN: Left Aside -->
                <button class="m-aside-left-close  m-aside-left-close--skin-dark " onclick="closeMenu()" id="m_aside_left_close_btn">
                    <i class="la la-close"></i>
                </button>
                <div id="m_aside_left" class="m-grid__item	m-aside-left  m-aside-left--skin-dark ">
                    <!-- BEGIN: Aside Menu -->
                    <div 
                        id="m_ver_menu" 
                        class="m-aside-menu  m-aside-menu--skin-dark m-aside-menu--submenu-skin-dark " 
                        data-menu-vertical="true"
                        data-menu-scrollable="false" data-menu-dropdown-timeout="500"  
                        >
                        <ul class="m-menu__nav  m-menu__nav--dropdown-submenu-arrow ">
                            <?php if ($_SESSION['userLoginType'] == 'admin') { ?>
                                <?php
                            } else {
                                if ($hierarchyIDofloggedUser != '') {
                                    $hid = " and hierarchy_id='" . $hierarchyIDofloggedUser . "'";
                                }
                                /*                                 * ****************** Code for different Company Users By Chirag Gupta on 26th April 2017 *************************** */
                                if ($_SESSION['userLoginType'] == 2) {
                                    $query = $_objAdmin->_getSelectList2('table_account_admin', 'hierarchy_id', '', ' operator_id="' . $_SESSION['operatorId'] . '"');

                                    $hid = ' and hierarchy_id = "' . $query[0]->hierarchy_id . '"';
                                } else if ($_SESSION['userLoginType'] == 8) {
                                    $query = $_objAdmin->_getSelectList2('table_service_personnel_hierarchy_relationship', 'hierarchy_id', '', ' service_personnel_id="' . $_SESSION['servicePersonnelId'] . '"');

                                    $hid = ' and hierarchy_id = "' . $query[0]->hierarchy_id . '"';
                                }
                                /*                                 * ****************** Code for different Company Users By Chirag Gupta on 26th April 2017 *************************** */
                                $userType = " user_type='" . $_SESSION['userLoginType'] . "'";
                                $pageGroup = $_objAdmin->_getselectList('table_navigation_privilege', '*', '', " user_id='" . $_SESSION['web_user_id'] . "' and $userType $hid ");

                                if ($pageGroup) {

                                    $pageID = $pageGroup[0]->page_privileges;
                                    if ($pageID != '') {
                                        $checkPageID = "id IN ($pageID) AND";
                                    }
                                } else {
                                    $pageGroup = $_objAdmin->_getselectList('table_navigation_privilege', '*', '', " $userType $hid ");
                                    if ($pageGroup) {

                                        $pageID = $pageGroup[0]->page_privileges;
                                        if ($pageID != '') {
                                            $checkPageID = "id IN ($pageID) AND";
                                        }
                                    } else {

                                        //echo "hello";
                                        //exit;
                                        if ($_SESSION['userLoginType'] != 1) {
                                            $checkPageID = "id IN (1,7) AND";
                                        }
                                        //echo "hello";
                                        //exit;
                                        //$checkPageID="id IN (1,7) AND";
                                    }
                                }


                                $pages = $_objAdmin->_getSelectList2('table_cms', 'id, page_name, parentPage, page_alias_name', '', " $checkPageID status = 'A' AND (parentPage IS NULL OR parentPage = 0)  AND ordID <>'' ORDER BY ordID ASC LIMIT 0,10");

                                if (is_array($pages)) {
                                    $i = 1;
                                    foreach ($pages as $value) {
                                        ?>
                                        <?php
                                        $subpages = $_objAdmin->_getSelectList2('table_cms', 'id,ordID,page_name, parentPage, page_alias_name', '', "  $checkPageID
									 parentPage = '" . $value->id . "' AND status = 'A' " . $condition . " ORDER BY ordID ASC");
                                        if (is_array($subpages)) {
                                            ?> 
                                            <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true"  data-menu-submenu-toggle="hover">
                                                <a  href="#" class="m-menu__link m-menu__toggle">
                                                    <i class="m-menu__link-icon flaticon-network"></i>
                                                    <span class="m-menu__link-text">
                                                        <?php echo $value->page_alias_name; ?>
                                                    </span>
                                                    <i class="m-menu__ver-arrow la la-angle-right"></i>
                                                </a>
                                                <div class="m-menu__submenu ">
                                                    <span class="m-menu__arrow"></span>
                                                    <ul class="m-menu__subnav">
                                                        <li class="m-menu__item  m-menu__item--parent" aria-haspopup="true" >
                                                            <span class="m-menu__link">
                                                                <span class="m-menu__link-text">
                                                                    <?php echo $value->page_alias_name; ?>
                                                                </span>
                                                            </span>
                                                        </li>


                                                        <?php
                                                        $j = 0;
                                                        foreach ($subpages as $value) {
                                                            $suborderpages = $_objAdmin->_getSelectList2('table_cms', 'id,page_name, parentPage, page_alias_name', '', "  $checkPageID parentPage = " . $value->id . " and entity_name!='Schedule' AND status = 'A' " . $condition . " ORDER BY ordID ASC");
                                                            if (is_array($suborderpages)) {
                                                                ?>

                                                                <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true"  data-menu-submenu-toggle="hover">
                                                                    <a  href="#" class="m-menu__link m-menu__toggle">
                                                                        <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                                                            <span></span>
                                                                        </i>
                                                                        <span class="m-menu__link-text">
                                                                            <?php echo $value->page_alias_name; ?>
                                                                        </span>
                                                                        <i class="m-menu__ver-arrow la la-angle-right"></i>
                                                                    </a>
                                                                    <div class="m-menu__submenu ">
                                                                        <span class="m-menu__arrow"></span>
                                                                        <ul class="m-menu__subnav">
                                                                    <?php foreach($suborderpages as $valuenew) { ?>
                                                                            <li class="m-menu__item " aria-haspopup="true" >
                                                                                <a  href="<?php echo  $valuenew->page_name; ?>" class="m-menu__link ">
                                                                                    <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                                                                        <span></span>
                                                                                    </i>
                                                                                    <span class="m-menu__link-text">
                                                                                        <?php echo $valuenew->page_alias_name;?>
                                                                                    </span>
                                                                                </a>
                                                                            </li>
                                                                    <?php } ?>
                                                                        </ul>
                                                                    </div>
                                                                </li>
                                                                <?php
                                                            } else {
                                                                ?>

                                                               <li class="m-menu__item " aria-haspopup="true" >
                                                                <?php if($value->page_alias_name=="My Profile"){?>
                                                                  <a  href="profile.php" class="m-menu__link ">
                                                                        <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                                                            <span></span>
                                                                        </i>
                                                                        <span class="m-menu__link-text">
                        <?php echo $value->page_alias_name; ?>
                                                                        </span>
                                                                    </a>
                                                                <?php }else{?>
                                                                    <a  href="<?php echo $value->page_name; ?>" class="m-menu__link ">
                                                                        <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                                                            <span></span>
                                                                        </i>
                                                                        <span class="m-menu__link-text">
                        <?php echo $value->page_alias_name; ?>
                                                                        </span>
                                                                    </a>
                                                                    <?php }?>
                                                                </li>
                                                                <?php
                                                            }
                                                            $j++;
                                                        }
                                                        ?>
                                                    </ul>
                                                </div>
                                            </li>
                                            <?php
                                        } else {
                                            ?>
                                            <li class="m-menu__item " aria-haspopup="true" >
                                                <a  href="<?php echo $value->page_name; ?>" class="m-menu__link ">
                                                    <i class="m-menu__link-icon flaticon-line-graph"></i>
                                                    <span class="m-menu__link-title">
                                                        <span class="m-menu__link-wrap">
                                                            <span class="m-menu__link-text">
                <?php echo $value->page_alias_name; ?>
                                                            </span> 
                                                        </span>
                                                    </span>
                                                </a>
                                            </li>
                                            <?php
                                        }
                                    }
                                }
                            }
                            ?>
                        </ul>
                    </div>
                </div>
                
                <div id="errorAccess" style="display:none; width:920px; margin-left:200px; margin-top:18px;">
                    <div id="messageaccess">
                        <table border="0" width="100%" cellpadding="0" cellspacing="0">
                            <tr>
                                <td class="red-left">You are not authorized to access this page.</td>
                                <td class="red-right"><a class="close-redaccess"><img src="images/icon_close_red.gif"   alt="" /></a></td>
                            </tr>
                        </table>
                    </div>
                </div>
               
               <div class="modal fade" id="m_modal_1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
							<div class="modal-dialog" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="exampleModalLabel">
											Ready to Leave?
										</h5>
										<button type="button" class="close" data-dismiss="modal" aria-label="Close">
											<span aria-hidden="true">
												&times;
											</span>
										</button>
									</div>
									<div class="modal-body">
										<p>
											Are you sure,You want to logout.
										</p>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-secondary" data-dismiss="modal">
											Close
										</button>
										<a href="logout.php" class="btn btn-primary">
											Logout
                                                                                </a>
									</div>
								</div>
							</div>
						</div>
