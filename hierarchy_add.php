<?php
include("includes/config.inc.php");
include("includes/function.php");
$_objAdmin = new Admin();
$objArrayList= new ArrayList();


if(isset($_POST['add']) && $_POST['add'] == 'yes')
{
	if($_REQUEST['id']!="") 
	{
		$condi=	"  LOWER(description)='".strtolower($_POST['description'])."' and hierarchy_id<>'".$_REQUEST['id']."'";
	}
	else
	{
		$condi=	" LOWER(description)='".strtolower($_POST['description'])."'";
	}
	
	$auRec=$_objAdmin->_getSelectList('table_salesman_hierarchy',"*",'',$condi);
	
	if(is_array($auRec))
	{
		$err="This hierarchy already exists in the system.";	
		$auRec[0]=(object)$_POST;						
	}
	else
	{
		if($_REQUEST['id']!="") 
		{
			$cid=$_objAdmin->updateHierarchy($_REQUEST['id']);
			$sus="hierarchy has been updated successfully.";
		}
		else 
		{
			$cid=$_objAdmin->addHierarchy();
			$sus="hierarchy added successfully.";
		}
	}
}


if(isset($_REQUEST['id']) && $_REQUEST['id']!="")
{
	$auRec=$_objAdmin->_getSelectList('table_salesman_hierarchy',"*",''," hierarchy_id=".$_REQUEST['id']);
	$cond = " AND hierarchy_id NOT IN (".$_REQUEST['id'].")";
	if(count($auRec)<=0) header("Location: hierarchy.php");
} 


	$auRec2=$_objAdmin->_getSelectList('table_salesman_hierarchy',"*",'',"  status = 'A' ORDER BY sort_order ASC");




?>

<?php include("header.inc.php");
$pageAccess=1;
$check=$objArrayList->checkAccess($pageAccess, basename($_SERVER['PHP_SELF']));
//echo $check;
if($check == false){
header('Location: ' . $_SERVER['HTTP_REFERER']);
}

 ?>

 <style>
.sortable		{ padding:0; }
.sortable li	{ padding:4px 8px; color:#000; cursor:move; list-style:none; width:350px; background:#ddd; margin:10px 0; border:1px solid #999; }
#message-box		{ background:#fffea1; border:2px solid #fc0; padding:4px 8px; margin:0 0 14px 0; width:350px; }
</style>
<div class="clear"></div>
<!-- start content-outer -->
<div id="content-outer">
<!-- start content -->
	<div id="content">
	<div id="page-heading"><h1><span style="color: #d74343; font-family: Tahoma; font-weight: bold;"><?php if(isset($_GET['id'])){?> Edit <?php } else {?> Add <?php }?> Hierarchy</span></h1></div>
		<table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">
		<tr>
			<td id="tbl-border-left"></td>
			<td>
			<!--  start content-table-inner -->
			<div id="content-table-inner">
				<table border="0" width="100%" cellpadding="0" cellspacing="0" style="margin-left:10px;">
				<tr valign="top">
					<td>
					<!--  start message-red -->
					<?php if($err!=''){?>
					<div id="message-red">
						<table border="0" width="100%" cellpadding="0" cellspacing="0">
						<tr>
							<td class="red-left"> <?php echo $err; ?></td>
							<td class="red-right"><a class="close-red"><img src="images/icon_close_red.gif"   alt="" /></a></td>
						</tr>
						</table>
					</div>
					<?php }?>
					<!--  end message-red -->
					<?php if($sus!=''){?>
					<!--  start message-green -->
					<div id="message-green">
						<table border="0" width="100%" cellpadding="0" cellspacing="0">
						<tr>
							<td class="green-left"><?php echo $sus; ?></td>
							<td class="green-right"><a class="close-green"><img src="images/icon_close_green.gif"   alt="" /></a></td>
						</tr>
						</table>
					</div>
					<?php } ?>
					<!--  end message-green -->
					
			<!-- start id-form -->
			<form name="frmPre" id="frmPre" method="post" action="<?php $_SERVER['PHP_SELF'];?>" enctype="multipart/form-data" >
			
				<table border="0" cellpadding="0" cellspacing="0"  id="id-form">
					<tr>
						<th valign="top">Hierarchy Position:</th>
						<td><input type="text" name="description" id="description" class="required" value="<?php echo $auRec[0]->description; ?>" maxlength="100" /></td>
						<td></td>
					</tr>
					
					
			<tr>
						<th valign="top">Status:</th>
						<td>
						<select name="status" id="status" class="styledselect_form_3">
						<option value="A" <?php if($auRec[0]->status=='A') echo "selected";?> >Active</option>
						<option value="I" <?php if($auRec[0]->status=='I') echo "selected";?> >Inactive</option>
						</select>
						</td>
						<td></td>
					</tr>
					
					
					<tr>
						<th>&nbsp;</th>
						<td valign="top">
							<input name="hierarchy_id" type="hidden" value="<?php echo $auRec[0]->hierarchy_id; ?>" />
							<input name="account_id" type="hidden" value="<?php echo $_SESSION['accountId']; ?>" />
							<input name="add" type="hidden" value="yes" />
							<input type="reset" value="Reset!" class="form-reset">
							<input type="button" value="Back" class="form-reset" onclick="location.href='hierarchy.php';" />
							<input name="submit" class="form-submit" type="submit" id="submit" value="Save" />
						</td>
					</tr>
					</table>
				</form>
			<!-- end id-form  -->
			</td>
			
			<td><?php /*function create_pyramid($limit) {
					for($row = 1; $row < $limit; $row ++) {
						$stars = str_repeat('*', ($row - 1) * 2 + 1);
						$space = str_repeat(' ', $limit - $row);
						echo $space . $stars . '<br/>';
					}
				}
					echo "<pre>" ;
				create_pyramid(10);*/?>
				<form id="dd-form" action="#" method="post">
				<!--<div style="height:400px; width:430px; overflow:scroll;">-->
				<h1><span style="color: #d74343; font-family: Tahoma; font-weight: bold;"> Hierarchy Level</span></h1>
				<?php if(sizeof($auRec2)>=1) {?>
					<ul class="sortable">
					<?php foreach($auRec2 as $key=>$value):?>
						  	<li><?php echo $key+1 ;?>.&nbsp;<?php echo ucwords(strtolower($value->description));?></li>
							<?php endforeach; ?>
					</ul>
				
					
				<?php } else { ?>
				
				<p>Sorry!  There are no hierarchy level in the system.</p>
				
			<?php } ?>

			</form>
			<!--</div>-->
				
				
				</td>
			</tr>
			</table>
			<!-- right bar-->
			
			<div class="clear"></div>
			</div>
			</td>
		</tr>
	</table>
<div class="clear"></div>
</div>
<!--  end content-table-inner  -->
<div class="clear">&nbsp;</div>

</div>
<!--  end content-outer -->
<div class="clear">&nbsp;</div>
    
<!-- start footer --> 
<?php include("footer.php") ?>  
<!-- end footer -->
</body>
</html>