<?php
include("includes/config.inc.php");
include("includes/function.php");
$_objAdmin = new Admin();

if(isset($_REQUEST['id']) && $_REQUEST['value']!="")
{
	if($_REQUEST['value']=="Active")
	{	
		$status='I';
	}
	else
	{
		$status='A';
	}
	$cid=$_objAdmin->_dbUpdate(array("status"=>$status),'table_service_personnel_hierarchy', " hierarchy_id='".$_REQUEST['id']."'");
	$Typeid=$_objAdmin->_dbUpdate(array("status"=>$status),'table_user_type', " hierarchy_id='".$_REQUEST['id']."'");
	header("Location: hierarchy_service_person.php");
}	

if(isset($_REQUEST['show']) && $_REQUEST['show']=="yes")
{
	$_objAdmin->showServicehierarchy();
	die;
}	

?>

<?php include("header.inc.php") ?>
 
 <style>
.sortable		{ padding:0; }
.sortable li	{ padding:4px 8px; color:#000; cursor:move; list-style:none; width:350px; background:#ddd; margin:10px 0; border:1px solid #999; }
#message-box		{ background:#fffea1; border:2px solid #fc0; padding:4px 8px; margin:0 0 14px 0; width:350px; }
</style>
<!-- start content-outer -->
<div id="content-outer">
<!-- start content -->
<div id="content">
<div id="page-heading"><h1><span style="color: #d74343; font-family: Tahoma; font-weight: bold;">Service Personnel Hierarchy</span></h1></div>
<table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">
<!--<tr>
	<th rowspan="3" class="sized"><img src="images/side_shadowleft.jpg" width="20" height="300" alt="" /></th>
	<th class="topleft"></th>
	<td id="tbl-border-top">&nbsp;</td>
	<th class="topright"></th>
	<th rowspan="3" class="sized"><img src="images/side_shadowright.jpg" width="20" height="300" alt="" /></th>
</tr>-->
<tr>
	<td id="tbl-border-left"></td>
	<td>
	<!--  start content-table-inner -->
	<div id="content-table-inner">
	
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
	<tr valign="top">
	<td>
		<!-- start id-form -->
		<table  border="0" cellspacing="0" cellpadding="0"  align="center" width="100%">
				<tr>
				<td align="lift" valign="top"  >
				<table id="flex1" style="display:none"></table>
					<script type="text/javascript">showServicehierarchy();</script>
				</td>
				</tr>
				</table>
	<!-- end id-form  -->

	</td>
	<td>

	<!-- right bar-->
</td>
</tr>
<tr>
<td></td>
<td></td>
</tr>
</table>
<div class="clear"></div>
</div>
<!--  end content-table-inner  -->
</td>
<td id="tbl-border-right"></td>
</tr>
<tr>
	<th class="sized bottomleft"></th>
	<td id="tbl-border-bottom">&nbsp;</td>
	<th class="sized bottomright"></th>
</tr>
</table>
<div class="clear">&nbsp;</div>
</div>
<!--  end content -->
<div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->
<div class="clear">&nbsp;</div> 
<!-- start footer -->         
<?php include("footer.php") ?>
<!-- end footer -->
 
</body>
</html>