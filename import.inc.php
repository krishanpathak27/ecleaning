<?php
ob_clean();
//$_objAdmin = new Admin();

/************************************* CSV For Category ***************************************/

if(isset($_POST['category_import_csv']) && $_POST['category_import_csv']=='yes'){		
	$data="Category Name*,Category Code\n";					
	header("Content-type: application/octet-stream");
	header("Content-Disposition: attachment; filename=\"category_sheet.csv\"");		
	echo $data;	
	die;
	}
	
if(isset($_POST['category_import']) && $_POST['category_import']=='yes'){
	if(isset($_FILES['fileToUpload']['tmp_name']) && $_FILES['fileToUpload']['tmp_name']!=""){	
		
		$ret = $_objItem->uploadCategoryFile();
		if($ret=='') {																	
			$msg= "File has been successfully imported.";								
			}elseif($ret=='no') {
			$error= "Empty file";
			}else {
			$error=  implode(",", $ret);
			//print($error);													
			}
			if($msg!=''){
			$cat_sus=$msg;
			}else{
			$data="Row Number(Please Delete The Row When Import),Category Name*,Category Code\n";	
			$data .="$error \n";					
			header("Content-type: application/octet-stream");
			header("Content-Disposition: attachment; filename=\"error.csv\"");		
			echo $data;	
			die;
			
			}		
		}
	}

/************************************* CSV For Category ***************************************/
/************************************* CSV For Color ***************************************/


if(isset($_POST['color_import_csv']) && $_POST['color_import_csv']=='yes'){		
	$data="Color Description*,Color Code*\n";					
	header("Content-type: application/octet-stream");
	header("Content-Disposition: attachment; filename=\"color_sheet.csv\"");		
	echo $data;	
	die;
	}
	
if(isset($_POST['color_import']) && $_POST['color_import']=='yes'){
	if(isset($_FILES['fileToUpload']['tmp_name']) && $_FILES['fileToUpload']['tmp_name']!=""){	
		//$_objItem->mysql_query("delete from table_color");
		$ret = $_objItem->uploadColorFile();
		if($ret=='') {																	
			$msg= "File has been successfully imported.";								
			}elseif($ret=='no') {
			$error= "Empty file";
			}else {
			$error=  implode(",", $ret);													
			}
			if($msg!=''){
			$cat_sus=$msg;
			}else{
			$data="Row Number(Please Delete The Row When Import),Color Description*,Color Code*\n";	
			$data .="$error \n";
			header("Content-type: application/octet-stream");
			header("Content-Disposition: attachment; filename=\"error.csv\"");		
			echo $data;	
			die;
			}		
		}
	}
/************************************* CSV For Color ***************************************/

/************************************* CSV For Items ***************************************/

if(isset($_POST['item_import_csv']) && $_POST['item_import_csv']=='yes'){		
	//$data="Category Name*,Item Code*,Item Description*,Size,D.P*,M R P*,Colors Code, Item Chain\n";					
	$data="Category Name*,Item Code*,Item Description*,Weight,D.P*,M R P*,Brand*,Old Product Code,New Product Code,Model Code,Division Name*,Item Segment*,Color,Capacity,Volt,CCA,Item Availability*,Warranty*,Prodata*,Grace*\n";					
	header("Content-type: application/octet-stream");
	header("Content-Disposition: attachment; filename=\"item_sheet.csv\"");		
	echo $data;	
	die;
	}
	
if(isset($_POST['item_import']) && $_POST['item_import']=='yes'){
	if(isset($_FILES['fileToUpload']['tmp_name']) && $_FILES['fileToUpload']['tmp_name']!=""){	
		
		$ret = $_objItem->uploadItemListFile();
		if($ret=='') {																	
			$msg= "File has been successfully imported.";								
			}elseif($ret=='no') {
			$error= "Empty file";
			}else {
				$error=  implode(",", $ret);															
			}
			if($msg!=''){
			$cat_sus=$msg;
			}else{
			$data="Row Number(Please Delete The Row When Import),Category Name*,Item Code*,Item Description*,Weight,D.P*,M R P*,Segment,Division,Capacity,Volt,CCA\n";	
			$data .="$error \n";
			header("Content-type: application/octet-stream");
			header("Content-Disposition: attachment; filename=\"error.csv\"");		
			echo $data;	
			die;
			
			}		
		}
	}
/************************************* CSV For Items ***************************************/

/************************************* CSV For Items PTR Files ***************************************/

	if(isset($_REQUEST['items_prt_csv'])&& $_REQUEST['items_prt_csv']=='yes'){
	$data="State Name*,City Name*,Item Code*,PTR Price* \n";  
	header("Content-type: application/octet-stream");
	header("Content-Disposition: attachment; filename=\"Items_PTR_sheet.csv\"");		
	echo $data;	
	die;
	}
	
	
	if(isset($_POST['import_item_ptr_file']) && $_POST['import_item_ptr_file'] == 'yes')
{
	if(isset($_FILES['fileToUpload']['tmp_name']) && $_FILES['fileToUpload']['tmp_name']!=""){			 
		$ret = $_objItem->importItemsPTRFile();
		if($ret=='') {																	
			$sus= "Items PTR has been added successfully.";								
			}elseif($ret=='no') {
			$error= "Empty file";
			}else {
			$error= implode(",", $ret);															
			}
			if($sus!=''){
			$sus=$sus;
			}else{
			$data="Row Number(Please Delete The Row When Import),State Name*,City Name*,Item Code*,PTR Price* \n";	
			$data .="$error \n";
			header("Content-type: application/octet-stream");
			header("Content-Disposition: attachment; filename=\"error.csv\"");		
			echo $data;	
			die;
			
			}		
		}
		//$sus="Item price has been added successfully.";
	}
	
	


/************************************* CSV For Items PTR Files ***************************************/




/************************************* Customer Import ***************************************/

if(isset($_POST['customer_import_csv']) && $_POST['customer_import_csv']=='yes'){	 
	$data="Customer Name*,Phone No1*,Phone No2,Phone No3,Customer Address,Customer City*,State*,District*,Taluka Name*,zipcode,Contact Person1*,contact Phone No1*,Contact Person2,contact Phone No2,Contact Person3,contact Phone No3,Email-ID1*,Email-ID2,Email-ID3,Number To Send SMS*,Customer Class,Customer Region,Customer Code*,Aadhar No,Pan No,Interested\n";  
	header("Content-type: application/octet-stream");
	header("Content-Disposition: attachment; filename=\"customer_sheet.csv\"");		
	echo $data;	
	die;
	}
	
if(isset($_POST['customer_import']) && $_POST['customer_import']=='yes'){
	if(isset($_FILES['fileToUpload']['tmp_name']) && $_FILES['fileToUpload']['tmp_name']!=""){			 
		$ret = $_objItem->uploadCustomerFile();
		if($ret=='') {																	
			$msg= "File has been successfully imported.";								
			}elseif($ret=='no') {
			$error= "Empty file";
			}else {
			$error= implode(",", $ret);														
			}
			if($msg!=''){
			$dis_sus=$msg;
			}else{
			$data="Row Number(Please Delete The Row When Import),Customer Name*,Phone No1*,Phone No2,Phone No3,Customer Address,Customer City*,State*,District*,Taluka Name*,zipcode,Contact Person1*,contact Phone No1*,Contact Person2,contact Phone No2,Contact Person3,contact Phone No3,Email-ID1*,Email-ID2,Email-ID3,Number To Send SMS*,Customer Class,Customer Region,Customer Code*,Aadhar No,Pan No,Interested\n";	
			$data .="$error \n";
			header("Content-type: application/octet-stream");
			header("Content-Disposition: attachment; filename=\"error.csv\"");		
			echo $data;	
			die;
			}		
		}
	}
/************************************* Ends Customer Imports ***************************************/

if(isset($_POST['distributor_import_csv']) && $_POST['distributor_import_csv']=='yes'){	 
	//$data="Distributor Name*,Phone No1*,Phone No2,Phone No3,Distributor Address,Distributor City*,State*,District*,Taluka Name*,zipcode,Contact Person1*,contact Phone No1*,Contact Person2,contact Phone No2,Contact Person3,contact Phone No3,Email-ID1*,Email-ID2,Email-ID3,Number To Send SMS*,Distributor Class,Distributor Region,Distributor Code*,Aadhar No,Pan No, Division\n"; 

	$data="Distributor Name*,Phone No1*,Phone No2,Phone No3,Distributor Address,State*,City*,Pincode/Zipcode*,Contact Person1*,contact Phone No1*,Contact Person2,contact Phone No2,Contact Person3,contact Phone No3,Email-ID1*,Email-ID2,Email-ID3,Number To Send SMS*, Aadhar No*, Pan No*, Division*, Brand*, Country*, Region*, Zone*, Area, Credit Limit*, Tin Number*\n";

	header("Content-type: application/octet-stream");
	header("Content-Disposition: attachment; filename=\"distributor_sheet.csv\"");		
	echo $data;	
	die;
}
	
if(isset($_POST['distributor_import']) && $_POST['distributor_import']=='yes'){
	if(isset($_FILES['fileToUpload']['tmp_name']) && $_FILES['fileToUpload']['tmp_name']!=""){			 
		$ret = $_objItem->uploadDistributorFile();
		if($ret=='') {																	
			$msg= "File has been successfully imported.";								
			}elseif($ret=='no') {
			$error= "Empty file";
			}else {
			$error= implode(",", $ret);														
			}
			if($msg!=''){
			$dis_sus=$msg;
			}else{
			//$data="Row Number(Please Delete The Row When Import),Distributor Name*,Phone No1*,Phone No2,Phone No3,Distributor Address,Distributor City*,State*,District*,Taluka Name*,zipcode,Contact Person1*,contact Phone No1*,Contact Person2,contact Phone No2,Contact Person3,contact Phone No3,Email-ID1*,Email-ID2,Email-ID3,Number To Send SMS*,Distributor Class,Distributor Region,Distributor Code*,Aadhar No,Pan No, Division\n";	

			$data="Row Number(Please Delete The Row When Import),Distributor Name*,Phone No1*,Phone No2,Phone No3,Distributor Address,State*,City*,Zipcode*,Contact Person1*,contact Phone No1*,Contact Person2,contact Phone No2,Contact Person3,contact Phone No3,Email-ID1*,Email-ID2,Email-ID3,Number To Send SMS*, Aadhar No*, Pan No*, Division*, Brand*, Country*, Region*, Zone*, Area, Credit Limit*, Tin Number*\n";

			$data .="$error \n";
			header("Content-type: application/octet-stream");
			header("Content-Disposition: attachment; filename=\"error.csv\"");		
			echo $data;	
			die;
			}		
		}
}


/************************************* CSV For Distributor ***************************************/

/************************************* CSV For Retailer ***************************************/



if(isset($_POST['retailer_import_csv']) && $_POST['retailer_import_csv']=='yes'){	 
	// $data="Retailer Name*,Phone No1*,Phone No2,Retailer Address,City*,State*,District*,Taluka Name*,Zipcode,Contact Person1*,contact Phone No1* ,Contact Person2,Contact Phone No2,Email-ID1*,Email-ID2,Retailer Class, Route Name,Retailer Channel,Distributor Code,Interested ,Retailer Type,Aadhar No,Pan No,Retailer Code*, Division\n";  

	$data="Dealer Name*,Phone No1*,Phone No2,Dealer Address,State*,City*,Pincode/Zipcode*,Contact Person1*,contact Phone No1* ,Contact Person2,Contact Phone No2,Email-ID1*,Email-ID2,Route Name,Distributor Code*,Aadhar No,Pan No,Dealer Code*, Division*, Brand*, Country*, Region*, Zone*, Area\n";   

	header("Content-type: application/octet-stream");
	header("Content-Disposition: attachment; filename=\"dealer_sheet.csv\"");		
	echo $data;	
	die;
	}
	
if(isset($_POST['retailer_import']) && $_POST['retailer_import']=='yes'){
	if(isset($_FILES['fileToUpload']['tmp_name']) && $_FILES['fileToUpload']['tmp_name']!=""){			 
		$ret = $_objItem->uploadRetailerFile();
		if($ret=='') {																	
			$msg= "File has been successfully imported.";								
			}elseif($ret=='no') {
			$error= "Empty file";
			}else {
			$error= implode(",", $ret);														
			}
			if($msg!=''){
			$ret_sus=$msg;
			}else{
			//$data="Row Number(Please Delete The Row When Import),Retailer Name*,Phone No1*,Phone No2,Retailer Address,City*,State*,District*,Taluka Name*,Zipcode,Contact Person1*,contact Phone No1* ,Contact Person2,Contact Phone No2,Email-ID1*,Email-ID2,Retailer Class, Route Name,Retailer Channel,Distributor Code,Interested ,Retailer Type,Aadhar No,Pan No,Retailer Code*, Division\n";	

			$data="Row Number(Please Delete The Row When Import),Dealer Name*,Phone No1*,Phone No2,Dealer Address,State*,City*,Pincode/Zipcode*,Contact Person1*,contact Phone No1* ,Contact Person2,Contact Phone No2,Email-ID1*,Email-ID2,Route Name,Distributor Code*,Aadhar No,Pan No,Dealer Code*, Division*, Brand*, Country*, Region*, Zone*, Area\n";	
			
			$data .="$error \n";
			header("Content-type: application/octet-stream");
			header("Content-Disposition: attachment; filename=\"error.csv\"");		
			echo $data;	
			die;
			}		
		}
	}

/************************************* CSV For Retailer ***************************************/
/************************************* CSV For Salesman ***************************************/

/*
if(isset($_POST['salesman_import_csv']) && $_POST['salesman_import_csv']=='yes'){	 
	$data="Salesman Name*,State*,City*,Address,Phone No*\n";  
	header("Content-type: application/octet-stream");
	header("Content-Disposition: attachment; filename=\"salesman_sheet.csv\"");		
	echo $data;	
	die;
	}
	
if(isset($_POST['salesman_import']) && $_POST['salesman_import']=='yes'){
	if(isset($_FILES['fileToUpload']['tmp_name']) && $_FILES['fileToUpload']['tmp_name']!=""){			 
		$ret = $_objItem->uploadSalesmanFile();
		if($ret=='') {																	
			$msg= "File has been successfully imported.";								
			}elseif($ret=='no') {
			$error= "Empty file";
			}else {
			$error= implode(",", $ret);													
			}
			if($msg!=''){
			$sal_sus=$msg;
			}else{
			$data="Row Number(Please Delete The Row When Import),Salesman Name*,State*,City*,Address,Phone No*,Category Name \n";	
			$data .="$error \n";
			header("Content-type: application/octet-stream");
			header("Content-Disposition: attachment; filename=\"error.csv\"");		
			echo $data;	
			die;
			}		
		}
	}*/
	
	
if(isset($_POST['salesman_import_csv']) && $_POST['salesman_import_csv']=='yes'){	 
	//$data="Salesman Name*,State*,City*,Address,Phone No*,Category Name\n";  
	$data="Salesman Name*,State*,City*,Address,Phone No*,Username,Password,Category Name,Salesman Designation,Reporting Person, Division,Country*,Region*,Zone*\n";  
	header("Content-type: application/octet-stream");
	header("Content-Disposition: attachment; filename=\"salesman_sheet.csv\"");		
	echo $data;	
	die;
	}
	
if(isset($_POST['salesman_import']) && $_POST['salesman_import']=='yes'){
	if(isset($_FILES['fileToUpload']['tmp_name']) && $_FILES['fileToUpload']['tmp_name']!=""){			 
		$ret = $_objItem->uploadSalesmanFile();
		if($ret=='') {																	
			$msg= "File has been successfully imported.";								
			}elseif($ret=='no') {
			$error= "Empty file";
			}else {
			$error= implode(",", $ret);													
			}
			if($msg!=''){
			$sal_sus=$msg;
			}else{
			$data="Row Number(Please Delete The Row When Import),Salesman Name*,State*,City*,Address,Phone No*,Username,Password,Category Name,Salesman Designation,Reporting Person, Division,Country*,Region*,Zone*\n";	
			$data .="$error \n";
			header("Content-type: application/octet-stream");
			header("Content-Disposition: attachment; filename=\"error.csv\"");		
			echo $data;	
			die;
			}		
		}
	}
/************************************* CSV For Salesman ***************************************/

/************************************* CSV For City ***************************************/
if(isset($_REQUEST['city_state_import_csv'])&& $_REQUEST['city_state_import_csv']=='yes'){
	$data="State Name*, District Name*,District Code*\n";  
	header("Content-type: application/octet-stream");
	header("Content-Disposition: attachment; filename=\"district_sheet.csv\"");		
	echo $data;	
	die;
}


/************************************* CSV For City ***************************************/
if(isset($_POST['city_import']) && $_POST['city_import']=='yes'){
	if(isset($_FILES['fileToUpload']['tmp_name']) && $_FILES['fileToUpload']['tmp_name']!=""){	
		$ret = $_objItem->uploadstatecity();
		if($ret=='') {																	
			$msg= "File has been successfully imported.";								
			}elseif($ret=='no') {
			$error= "Empty file";
			}else {
				$error= implode(",", $ret);													
			}
			if($msg!=''){
			$cat_sus=$msg;
			}else{
			$data="Row Number(Please Delete The Row When Import),State Name*, District Name* ,District Code*\n";	
			$data .="$error \n";
			header("Content-type: application/octet-stream");
			header("Content-Disposition: attachment; filename=\"error.csv\"");		
			echo $data;	
			die;
			}		
		}
	}


/************************************* CSV For Stock ***************************************/


if(isset($_POST['stock_import_csv']) && $_POST['stock_import_csv']=='yes')
{		
	$data="Category Name*, Item Code*, Cases Size*, Number Of Cases*\n";					
	header("Content-type: application/octet-stream");
	header("Content-Disposition: attachment; filename=\"item_stock_sheet.csv\"");		
	echo $data;	
	die;
}
	
if(isset($_POST['stock_import']) && $_POST['stock_import']=='yes'){
	if(isset($_FILES['fileToUpload']['tmp_name']) && $_FILES['fileToUpload']['tmp_name']!=""){	
		$ret = $_objItem->uploadstockDetails();
		if($ret=='') {																	
			$msg= "File has been successfully imported.";								
			}elseif($ret=='no') {
			$error= "Empty file";
			}else {
				$error= implode(",", $ret);														
			}
			if($msg!=''){
			$sus=$msg;
			}else{
			$data="Row Number(Please Delete The Row When Import),Category Name*, Item Code*, Color Code*, Stock Value* \n";	
			$data .="$error \n";
			header("Content-type: application/octet-stream");
			header("Content-Disposition: attachment; filename=\"error.csv\"");		
			echo $data;	
			die;		
			}		
		}
	}
	
/************************************* CSV For Stock ***************************************/

/********************************* DISTRIBUTOR DISPATCHED STOCK IMPORT SECTION ****************************/


if(isset($_POST['distributor_stock_import_csv']) && $_POST['distributor_stock_import_csv']=='yes')
{		
	$data="Distributor Code*, Item Code*, Bill Date, Bill No, Stock Value*\n";					
	header("Content-type: application/octet-stream");
	header("Content-Disposition: attachment; filename=\"distributor_item_stock_sheet.csv\"");		
	echo $data;	
	die;
}
	
if(isset($_POST['distributor_stock_import']) && $_POST['distributor_stock_import']=='yes'){
	if(isset($_FILES['fileToUpload']['tmp_name']) && $_FILES['fileToUpload']['tmp_name']!=""){	
		$ret = $_objItem->uploadDistributorDispatchstock();
		if($ret=='') {																	
			$msg= "File has been successfully imported.";								
			}elseif($ret=='no') {
			$error= "Empty file";
			}else {
				$error= implode(",", $ret);												
			}
			if($msg!=''){
			$sus=$msg;
			}else{
			$data="Row Number(Please Delete The Row When Import),Distributor Code*, Item Code*, Bill Date, Bill No., Stock Value* \n";	
			$data .="$error \n";
			header("Content-type: application/octet-stream");
			header("Content-Disposition: attachment; filename=\"error.csv\"");		
			echo $data;	
			die;	
			}		
		}
	}
	
/********************************* DISTRIBUTOR DISPATCHED STOCK IMPORT SECTION ****************************/

/********************************* Retailer Dues ****************************/


if(isset($_REQUEST['retailer_dues_import_csv'])&& $_REQUEST['retailer_dues_import_csv']=='yes'){

	$aDisRec = $_objAdmin->_getSelectList('table_retailer AS R LEFT JOIN state ON state.state_id = R.state LEFT JOIN city ON city.city_id = R.city ','retailer_id,retailer_name, state_name, city_name ','','');
	$data="Retailer ID*,Retailer Name*,Due Amount*, State, City\n"; 
	for($i=0;$i<count($aDisRec);$i++){
		$retId=$aDisRec[$i]->retailer_id;
		$retName=$aDisRec[$i]->retailer_name;
		$amt = "";
		$stateName=$aDisRec[$i]->state_name;
		$cityName=$aDisRec[$i]->city_name;
	$data.="$retId,$retName,$amt, $stateName,$cityName\n"; 
	} 
	header("Content-type: application/octet-stream");
	header("Content-Disposition: attachment; filename=\"retailer_due_sheet.csv\"");		
	echo $data;	
	die;
}


if(isset($_POST['retailer_due_import']) && $_POST['retailer_due_import']=='yes'){
	if(isset($_FILES['fileToUpload']['tmp_name']) && $_FILES['fileToUpload']['tmp_name']!=""){	
		
		$ret = $_objItem->uploadRetailerDueFile();
		if($ret=='') {																	
			$msg= "File has been successfully imported.";								
			}elseif($ret=='no') {
			$error= "Empty file";
			}else {
				$error= implode(",", $ret);														
			}
			if($msg!=''){
			$cat_sus=$msg;
			}else{
			$data="Row Number(Please Delete The Row When Import),Retailer ID*,Retailer Name*,Due Amount* \n";	
			$data .="$error \n";
			header("Content-type: application/octet-stream");
			header("Content-Disposition: attachment; filename=\"error.csv\"");		
			echo $data;	
			die;
			}		
		}
	}
/********************************* Retailer Dues ****************************/






/********************************* DISTRIBUTOR ACTUAL STOCK IMPORT SECTION(10 Sep 2014) ****************************/


if(isset($_POST['distributor_actual_stock_import_csv']) && $_POST['distributor_actual_stock_import_csv']=='yes')
{		
	$data="Distributor Code*, Category Name*, Item Code*, Cases Size, Number Of Cases*\n";					
	header("Content-type: application/octet-stream");
	header("Content-Disposition: attachment; filename=\"distributor_actual_item_stock_sheet.csv\"");		
	echo $data;	
	die;
}
	
if(isset($_POST['distributor_actual_stock_import']) && $_POST['distributor_actual_stock_import']=='yes'){
	if(isset($_FILES['fileToUpload']['tmp_name']) && $_FILES['fileToUpload']['tmp_name']!=""){	
		$ret = $_objItem->uploadDistributorActualStock();
		if($ret=='') {																	
			$msg= "File has been successfully imported.";								
			}elseif($ret=='no') {
			$error= "Empty file";
			}else {
				$error= implode(",", $ret);												
			}
			if($msg!=''){
			$sus=$msg;
			}else{
			$data="Row Number(Please Delete The Row When Import),Distributor Code*, Category Name*, Item Code*, Cases Sizes, Number Of Cases* \n";	
			$data .="$error \n";
			header("Content-type: application/octet-stream");
			header("Content-Disposition: attachment; filename=\"error.csv\"");		
			echo $data;	
			die;	
			}		
		}
	}
	
/********************************* DISTRIBUTOR ACTUAL STOCK IMPORT SECTION(10 Sep 2014) ****************************/



	
/******* Route Import *********************/


if(isset($_POST['route_import_csv']) && $_POST['route_import_csv']=='yes')
{	
	
	if($_SESSION['RouteStateSession']!=''){$state="r.state='".$_SESSION['RouteStateSession']."' ";}
	if($_SESSION['RouteCitySession']!=''){$city="and r.city='".$_SESSION['RouteCitySession']."' ";}
	if($_SESSION['RouteMarketSession']!=''){$market="and r.retailer_location='".$_SESSION['RouteMarketSession']."' ";}
	
	
	$auRec=$_objAdmin->_getSelectList('table_retailer as r left join state as s on s.state_id=r.state left join city as c on c.city_id=r.city',"r.retailer_id,r.retailer_name,r.retailer_location,s.state_name,c.city_name",''," $state $city $market ");
	//echo "<pre>";
	//print_r($auRec);
	//exit;
		
	$data="ID*, Retailer*, State*, City*, Market*, Route Name*\n";
	foreach($auRec as $val){
	$data.="".$val->retailer_id.",".$val->retailer_name.",".$val->state_name.",".$val->city_name.",".$val->retailer_location.",\n";
}					
	header("Content-type: application/octet-stream");
	header("Content-Disposition: attachment; filename=\"route_list_sheet.csv\"");		
	echo $data;	
	die;
}
	
if(isset($_POST['route_import']) && $_POST['route_import']=='yes'){
	if(isset($_FILES['fileToUpload']['tmp_name']) && $_FILES['fileToUpload']['tmp_name']!=""){	
		$ret = $_objItem->uploadRouteList();
		if($ret=='') {																	
			$msg= "File has been successfully imported.";								
			}elseif($ret=='no') {
			$error= "Empty file";
			}else {
				$error= implode(",", $ret);												
			}
			if($msg!=''){
			$sus=$msg;
			}else{
			$data="Row Number(Please Delete The Row When Import),ID*, Retailer*, State*, City*, Market*, Route Name* \n";	
			$data .="$error \n";
			header("Content-type: application/octet-stream");
			header("Content-Disposition: attachment; filename=\"error.csv\"");		
			echo $data;	
			die;	
			}		
		}
	}
	

/************* Route Import ***************/ 

/************* Distributor Target Import 2nd June 2015 (Gaurav)  ***************/ 


if(isset($_POST['distributor_target_import_csv']) && $_POST['distributor_target_import_csv']=='yes')
{		
	$data="Distributor Code*, Item Code*, Cases Size*, Number Of Cases*, Target Type*\n";					
	header("Content-type: application/octet-stream");
	header("Content-Disposition: attachment; filename=\"distributor_target_sheet.csv\"");		
	echo $data;	
	die;
}
	
if(isset($_POST['distributor_target_import_import']) && $_POST['distributor_target_import_import']=='yes'){
	if(isset($_FILES['fileToUpload']['tmp_name']) && $_FILES['fileToUpload']['tmp_name']!=""){	
		$ret = $_objItem->uploadDistributorTraget();
		if($ret=='') {																	
			$msg= "File has been successfully imported.";								
			}elseif($ret=='no') {
			$error= "Empty file";
			}else {
				$error= implode(",", $ret);												
			}
			if($msg!=''){
			$sus=$msg;
			}else{
			$data="Row Number(Please Delete The Row When Import),Distributor Code*, Item Code*, Cases Size*, Number Of Cases*,Target Type* \n";	
			$data .="$error \n";
			header("Content-type: application/octet-stream");
			header("Content-Disposition: attachment; filename=\"error.csv\"");		
			echo $data;	
			die;	
			}		
		}
	}

/************* Distributor Target Import 2nd June 2015 (Gaurav)  ***************/ 





/**********************************************************************************
* DESC : Import salesman order by admin
* Author : AJAY
* created : 29th July 2015
*
**/



if(isset($_POST['order_import_csv']) && $_POST['order_import_csv']=='yes'){	 
	$data="Salesman Name*, Salemsan Phone*, Distributor Code*, Retailer Name*, Retailer Phone*, Bill Date, Bill No, Date of Order*, Item Code*, Qty* \n";  
	header("Content-type: application/octet-stream");
	header("Content-Disposition: attachment; filename=\"order_sheet.csv\"");		
	echo $data;	
	die;
	}
	
if(isset($_POST['order_import']) && $_POST['order_import']=='yes'){
	if(isset($_FILES['fileToUpload']['tmp_name']) && $_FILES['fileToUpload']['tmp_name']!=""){			 
		$ret = $_objItem->uploadOrdersFile();
		if($ret=='') {																	
			$msg= "File has been successfully imported.";								
			}elseif($ret=='no') {
			$error= "Empty file";
			}else {
			$error= implode(",", $ret);														
			}
			if($msg!=''){
			$ret_sus=$msg;
			}else{
			$data="Row Number(Please Delete The Row When Import),Salesman Name*, Salemsan Phone*, Distributor Code*, Retailer Name*, Retailer Phone*, Bill Date, Bill No, Date of Order*, Item Code*, Qty* \n";	
			$data .="$error \n";
			header("Content-type: application/octet-stream");
			header("Content-Disposition: attachment; filename=\"order_error.csv\"");		
			echo $data;	
			die;
			}		
		}
	}




	// Added by ajay@2015-10-07






if(isset($_REQUEST['taluka_import_csv'])&& $_REQUEST['taluka_import_csv']=='yes'){
	$data="Taluka Name*, District Name*,Taluka Code*\n";  
	header("Content-type: application/octet-stream");
	header("Content-Disposition: attachment; filename=\"taluka_sheet.csv\"");		
	echo $data;	
	die;
}


/************************************* CSV For Taluka ***************************************/
if(isset($_POST['taluka_import']) && $_POST['taluka_import']=='yes'){
	if(isset($_FILES['fileToUpload']['tmp_name']) && $_FILES['fileToUpload']['tmp_name']!=""){	
		$ret = $_objItem->uploadDistrictTalukas();
		if($ret=='') {																	
			$msg= "File has been successfully imported.";								
			}elseif($ret=='no') {
			$error= "Empty file";
			}else {
				$error= implode(",", $ret);													
			}
			if($msg!=''){
			$cat_sus=$msg;
			}else{
			$data="Row Number(Please Delete The Row When Import),Taluka Name*, District Name* ,Taluka Code*\n";	
			$data .="$error \n";
			header("Content-type: application/octet-stream");
			header("Content-Disposition: attachment; filename=\"error.csv\"");		
			echo $data;	
			die;
			}		
		}
	}








if(isset($_REQUEST['market_import_csv'])&& $_REQUEST['market_import_csv']=='yes'){
	$data="City Name*, Taluka Name*, Market code*\n";  
	header("Content-type: application/octet-stream");
	header("Content-Disposition: attachment; filename=\"citymarket_sheet.csv\"");		
	echo $data;	
	die;
}


/************************************* CSV For Taluka ***************************************/
if(isset($_POST['market_import']) && $_POST['market_import']=='yes'){
	if(isset($_FILES['fileToUpload']['tmp_name']) && $_FILES['fileToUpload']['tmp_name']!=""){	
		$ret = $_objItem->uploadTalukaMarkets();
		if($ret=='') {																	
			$msg= "File has been successfully imported.";								
			}elseif($ret=='no') {
			$error= "Empty file";
			}else {
				$error= implode(",", $ret);													
			}
			if($msg!=''){
			$cat_sus=$msg;
			}else{
			$data="Row Number(Please Delete The Row When Import),City Name*, Taluka Name*,Market code* \n";	
			$data .="$error \n";
			header("Content-type: application/octet-stream");
			header("Content-Disposition: attachment; filename=\"error.csv\"");		
			echo $data;	
			die;
			}		
		}
	}



/*********************** Import Monthly Route Assignment ***********************/


	if(isset($_POST['route_import_monthly']) && $_POST['route_import_monthly']=='yes'){
	if(isset($_FILES['fileToUpload']['tmp_name']) && $_FILES['fileToUpload']['tmp_name']!=""){	
		$ret = $_objItem->uploadMonthlyRouteList();
		if($ret=='') {																	
			$msg= "File has been successfully imported.";								
			}elseif($ret=='no') {
			$error= "Empty file";
			}else {
				$error= implode(",", $return);												
			}
			if($msg!=''){
			$sus=$msg;
			}else{
			$data="Row Number(Please Delete The Row When Import),Salesman Name*, Route Name*, Year* ,Month*, \n";	
			$data .="$error \n";
			header("Content-type: application/octet-stream");
			header("Content-Disposition: attachment; filename=\"error.csv\"");		
			echo $data;	
			die;	
			}		
		}
	}
	


/****************************** For Route Assignment (Date: 29sep 2015)*********************/

if(isset($_POST['route_assignment_import_csv']) && $_POST['route_assignment_import_csv']=='yes')
{	
	
	if($_SESSION['MonthSession']!=""){  $month=$_SESSION['MonthSession'];}
	if($_SESSION['YearSession']!=""){  $year=$_SESSION['YearSession'];}

	    $days            =cal_days_in_month(CAL_GREGORIAN, $month, $year);
	    $current_day_Val = date('d');
	    $current_month   =date('n');
	    $passing_month   =$month;
	    // if current day and month are current then download with current day
	    if($current_month==$passing_month){ $current_day_Val;} else{ $current_day_Val=1;}

	    $monthName       = date("F", mktime(0, 0, 0, $month, 10));
	    for($i=$current_day_Val;$i<=$days;$i++) {
		if($i<$days){ $comma=',';} else{ $comma='';}
		$dayValue.='day'.$i.$comma;
		}
	//echo $days; exit;	
	$data="Salesman Name*, Route Name*, Year*, Month*,".$dayValue." \n";
	$data.=",,".$year.",".$monthName." ";
					
	header("Content-type: application/octet-stream");
	header("Content-Disposition: attachment; filename=\"route_list_sheet.csv\"");		
	echo $data;	
	die;
}



/********************************** Division List 16 Oct 2015 By Nizam *************************/
if(isset($_POST['division_import_csv']) && $_POST['division_import_csv']=='yes'){		
	$data="Division Name*\n";					
	header("Content-type: application/octet-stream");
	header("Content-Disposition: attachment; filename=\"division_sheet.csv\"");		
	echo $data;	
	die;
	}
	
if(isset($_POST['division_import']) && $_POST['division_import']=='yes'){
	if(isset($_FILES['fileToUpload']['tmp_name']) && $_FILES['fileToUpload']['tmp_name']!=""){	
		
		$ret = $_objItem->uploadDivisionFile();
		if($ret=='') {																	
			$msg= "File has been successfully import.";								
			}elseif($ret=='no') {
			$error= "Empty file";
			}else {
			$error= "Following record has not been inserted.<br>".$ret;														
			}
			if($msg!=''){
			$cat_sus=$msg;
			}else{
			$cat_err=$error;
			}		
		}
	}
/********************************** Division List 16 Oct 2015 Nizam *************************/



/********************************** Serial no scan List 24 feb 2016 By Abhishek *************************/
if(isset($_POST['serial_import']) && $_POST['serial_import']=='yes'){
	if(isset($_FILES['fileToUpload']['tmp_name']) && $_FILES['fileToUpload']['tmp_name']!=""){	
		$ret = $_objItem->uploadserialscan();
		if($ret=='') {
			$msg= "File has been successfully imported.";
			header("Location: serial_no_scan_report.php");
		} elseif($ret=='no') {
			$error= "Empty file";
		}else {
				$error= implode(",", $ret);
		}
		if($msg!=''){
			$cat_sus=$msg;
		}else{
			$data="Row Number(Please Delete The Row When Import),ID*, Status*\n";	
			$data .="$error \n";
			header("Content-type: application/octet-stream");
			header("Content-Disposition: attachment; filename=\"error.csv\"");		
			echo $data;	
			die;
		}		
	}
}
/********************* Serial no scan List 24 feb 2016 By Abhishek*************************/


/************************************* CSV For Warehouse  20 Feb 2017 By Pooja ***************************************/

if(isset($_POST['warehouse_import_csv']) && $_POST['warehouse_import_csv']=='yes'){	 
	
	//	$data="Warehouse Prefix*,Warehouse Name*,Warehouse Code*,Tally Warehouse ID*,Division*,Warehouse Phone No*,Warehouse Address, Country,Region,Zone,State,City,Pincode/Zipcode\n";

	$data="Warehouse Prefix*, Warehouse Name*, Warehouse Code*, Warehouse Phone No*, Warehouse Address, Country*, Region*, Zone*, State*, City*, Area, Warehouse Incharge Phone No1*, Warehouse Incharge Phone No2, Warehouse Incharge Phone No3, Warehouse Incharge Email-ID1*, Warehouse Incharge Email-ID2, Warehouse Incharge Email-ID3, Warehouse Incharge Name*, Pincode/Zipcode\n";

	header("Content-type: application/octet-stream");
	header("Content-Disposition: attachment; filename=\"warehouse_sheet.csv\"");		
	echo $data;	
	die;
	}
	
if(isset($_POST['warehouse_import']) && $_POST['warehouse_import']=='yes'){
	if(isset($_FILES['fileToUpload']['tmp_name']) && $_FILES['fileToUpload']['tmp_name']!=""){			 
		$ret = $_objItem->uploadWarehouseFile();
		if($ret=='') {																	
			$msg= "File has been successfully imported.";								
			}elseif($ret=='no') {
			$error= "Empty file";
			}else {
			$error= implode(",", $ret);														
			}
			if($msg!=''){
			$dis_sus=$msg;
			}else{
				
				// $data="Row Number(Please Delete The Row When Import),Warehouse Prefix*,Warehouse Name*,Warehouse Code*,Tally Warehouse ID*,Division*,Warehouse Phone No*,Warehouse Address,Country,Region,Zone,State,City,Pincode/Zipcode\n";

				$data="Row Number(Please Delete The Row When Import),Warehouse Prefix*, Warehouse Name*, Warehouse Code*, Warehouse Phone No*, Warehouse Address, Country*, Region*, Zone*, State*, City*, Area, Warehouse Incharge Phone No1*, Warehouse Incharge Phone No2, Warehouse Incharge Phone No3, Warehouse Incharge Email-ID1*, Warehouse Incharge Email-ID2, Warehouse Incharge Email-ID3, Warehouse Incharge Name*, Pincode/Zipcode\n";

			$data .="$error \n";
			header("Content-type: application/octet-stream");
			header("Content-Disposition: attachment; filename=\"error.csv\"");		
			echo $data;	
			die;
			}		
		}
	}

/************************************* CSV For Warehouse ***************************************/


/********************************* CSV For Service Persons ************************************/

if(isset($_POST['service_person_import_csv']) && $_POST['service_person_import_csv']=='yes'){	 
	
	$data="Service Personnel Name*,Service Personnel Code*,Division*,Warehouse, Phone Number*, Address,Country*,Region*,Zone*,State*,City*,Area,Pincode/Zipcode\n";

	header("Content-type: application/octet-stream");
	header("Content-Disposition: attachment; filename=\"service_person_sheet.csv\"");		
	echo $data;	
	die;
}
	
if(isset($_POST['service_person_import']) && $_POST['service_person_import']=='yes'){
	if(isset($_FILES['fileToUpload']['tmp_name']) && $_FILES['fileToUpload']['tmp_name']!=""){			 
		$ret = $_objItem->uploadServicePersonFile();
		if($ret=='') {																	
			$msg= "File has been successfully imported.";								
			}elseif($ret=='no') {
			$error= "Empty file";
			}else {
			$error= implode(",", $ret);														
			}
			if($msg!=''){
			$dis_sus=$msg;
			}else{
				

				$data="Row Number(Please Delete The Row When Import),Service Personnel Name*,Service Personnel Code*,Division*,Warehouse, Phone Number*, Address,Country*,Region*,Zone*,State*,City*,Pincode/Zipcode\n";

				$data .="$error \n";
				header("Content-type: application/octet-stream");
				header("Content-Disposition: attachment; filename=\"error.csv\"");		
				echo $data;	
				die;
			}		
		}
	}

/************************************* CSV For Service Persons ***************************************/






	
/*******************************************************
* DESC: End of Import Warehose Stock From Company
* Author: AJAY@2017-02-24
*
*
*
********************************************************/

/**************************** Code Commented as we have to show dispatch summary too ************************************/

// if(isset($_POST['warehouse_stock_import_csv']) && $_POST['warehouse_stock_import_csv']=='yes')
// {		
// 	//$data="Warehouse Code*, Category Name*, Item Code*, BSN*, Batch No, Manufacture Date*, Warranty Period(In Months)*, ProRata(In Months)*, Grace Period(In Months)* \n";	
// 	$data="BSN*\n";							
// 	header("Content-type: application/octet-stream");
// 	header("Content-Disposition: attachment; filename=\"warehouse_stock_sheet.txt\"");		
// 	echo $data;	
// 	die;
// }
	
// if(isset($_POST['warehouse_stock_import']) && $_POST['warehouse_stock_import']=='yes'){
// 	if(isset($_FILES['fileToUpload']['tmp_name']) && $_FILES['fileToUpload']['tmp_name']!=""){	
// 		$ret = $_objItem->uploadWarehouseActualStock();
// 		if($ret=='') {																	
// 			$msg= "File has been successfully imported.";								
// 			}elseif($ret=='no') {
// 			$error= "Empty file";
// 			}else {
// 				$error= implode(",", $ret);												
// 			}
// 			if($msg!=''){
// 			$sus=$msg;
// 			}else{
// 			$data="Row Number(Please Delete The Row When Import),Item Code*, BSN*, Batch No, Manufacture Date*, Dispatch Date*  \n";	
// 			$data .="$error \n";
// 			header("Content-type: application/octet-stream");
// 			header("Content-Disposition: attachment; filename=\"error.csv\"");		
// 			echo $data;	
// 			die;	
// 			}		
// 		}
// 	}

/**************************** Code Commented as we have to show dispatch summary too ************************************/
	
if(isset($_POST['warehouse_stock_import_csv']) && $_POST['warehouse_stock_import_csv']=='yes')
{		
	//$data="Warehouse Code*, Category Name*, Item Code*, BSN*, Batch No, Manufacture Date*, Warranty Period(In Months)*, ProRata(In Months)*, Grace Period(In Months)* \n";	
	$data="BSN*\n";							
	header("Content-type: application/octet-stream");
	header("Content-Disposition: attachment; filename=\"warehouse_stock_sheet.txt\"");		
	echo $data;	
	die;
}
	
if(isset($_POST['warehouse_stock_import']) && $_POST['warehouse_stock_import']=='yes'){
	if(isset($_SESSION['dispatch_wrhs_info']) && $_SESSION['dispatch_wrhs_info']!=""){	
		//$ret = $_objItem->uploadWarehouseActualStock();// Commented due to showing of Dispatch Summary @Chirag 2017-09-04
		$ret = $_objItem->uploadWarehouseActualStockDispatchSummary();
		if($ret=='') {																	
			$msg= "File has been successfully imported.";								
			}elseif($ret=='no') {
			$error= "Empty file";
			}else {
				$error= implode(",", $ret);												
			}
			if($msg!=''){
			$sus=$msg;
			}else{
			$data="Row Number(Please Delete The Row When Import),Item Code*, BSN*, Batch No, Manufacture Date*, Dispatch Date*  \n";	
			$data .="$error \n";
			header("Content-type: application/octet-stream");
			header("Content-Disposition: attachment; filename=\"error.csv\"");		
			echo $data;	
			die;	
			}		
		}
	}
/*******************************************************
* DESC: End of Import Warehose Stock From Company
* Author: AJAY@2017-02-24
*
*
*
********************************************************/







/*******************************************************
* DESC: Download preformatted service distributor list
* Author: AJAY@2017-04-27
*
*******************************************************/


if(isset($_POST['service_distributor_import_csv']) && $_POST['service_distributor_import_csv']=='yes'){	 

	$data=" Service Distributor Name*, Service Distributor Code*, Phone No*, Address, Country*,Region*,Zone*,State*,City*,Area,Pincode/Zipcode\n";

	header("Content-type: application/octet-stream");
	header("Content-Disposition: attachment; filename=\"service_distributor_sheet.csv\"");		
	echo $data;	
	die;

}
	
if(isset($_POST['service_distributor_import']) && $_POST['service_distributor_import']=='yes'){
	if(isset($_FILES['fileToUpload']['tmp_name']) && $_FILES['fileToUpload']['tmp_name']!=""){			 
	//	$ret = $_objItem->uploadWarehouseFile();
		$ret = $_objItem->uploadServiceDistributorFile();
		if($ret=='') {																	
			$msg= "File has been successfully imported.";								
			}elseif($ret=='no') {
			$error= "Empty file";
			}else {
			$error= implode(",", $ret);														
			}
			if($msg!=''){
			$dis_sus=$msg;
			}else{
				
			$data="Row Number(Please Delete The Row When Import), Service Distributor Name*, Service Distributor Code*, Phone No*, Address, Country*,Region*,Zone*,State*,City*,Area,Pincode/Zipcode\n";

			$data .="$error \n";
			header("Content-type: application/octet-stream");
			header("Content-Disposition: attachment; filename=\"error.csv\"");		
			echo $data;	
			die;
			}		
		}
	}



/*******************************************************
* DESC: Download preformatted service distributor list
* Author: AJAY@2017-04-27
*
*******************************************************/

// commeted by arvind @4-05-2017 import area

/************************************* CSV For Area ***************************************/
if(isset($_REQUEST['city_area_import_csv'])&& $_REQUEST['city_area_import_csv']=='yes'){
	
	$data="City Name*,Area Name*,Area Code*\n";  
	header("Content-type: application/octet-stream");
	header("Content-Disposition: attachment; filename=\"area_sheet.csv\"");		
	echo $data;	
	die;
}




/************************************* CSV For area ***************************************/
if(isset($_POST['area_import']) && $_POST['area_import']=='yes'){
	if(isset($_FILES['fileToUpload']['tmp_name']) && $_FILES['fileToUpload']['tmp_name']!=""){	
		$ret = $_objItem->uploadcityarea();
		if($ret=='') {																	
			$msg= "File has been successfully imported.";								
			}elseif($ret=='no') {
			$error= "Empty file";
			}else {
				$error= implode(",", $ret);													
			}
			if($msg!=''){
			$cat_sus=$msg;
			}else{
			$data="Row Number(Please Delete The Row When Import),City Name*, Area Name* ,Area Code*\n";	
			$data .="$error \n";
			header("Content-type: application/octet-stream");
			header("Content-Disposition: attachment; filename=\"error.csv\"");		
			echo $data;	
			die;
			}		
		}
	}



/************************************************ CSV for importing BSN May 15, 2017 ******************************************/

if(isset($_POST['bsn_import_csv']) && $_POST['bsn_import_csv']=='yes'){		
	$data="BSN*,Date of Sale*,Batch Number,Status*,Recharge Date, Factory Dispatched Date\n";					
	header("Content-type: application/octet-stream");
	header("Content-Disposition: attachment; filename=\"bsn_sheet.csv\"");		
	echo $data;	
	die;
	}
	
if(isset($_POST['bsn_import']) && $_POST['bsn_import']=='yes'){
	if(isset($_FILES['fileToUpload']['tmp_name']) && $_FILES['fileToUpload']['tmp_name']!=""){	
		
		$ret = $_objItem->uploadBsnListFile();
		if($ret=='') {																	
			$msg= "File has been successfully imported.";								
			}elseif($ret=='no') {
			$error= "Empty file";
			}else {
				$error=  implode(",", $ret);															
			}
			if($msg!=''){
			$cat_sus=$msg;
			}else{
			$data="Row Number(Please Delete The Row When Import),BSN*,Date of Sale*,Status*\n";	
			$data .="$error \n";
			header("Content-type: application/octet-stream");
			header("Content-Disposition: attachment; filename=\"error.csv\"");		
			echo $data;	
			die;
			
			}		
		}
	}

/************************************************ CSV for importing BSN May 15, 2017 ******************************************/


/************************************************************************
* DESC: Download preformatted service distributor service personnel list
* Created By: Pooja@16May2017
*
**************************************************************************/


if(isset($_POST['service_distributor_sp_import_csv']) && $_POST['service_distributor_sp_import_csv']=='yes'){	 

	$data=" Service Distributor SP Name*, Service Distributor SP Code*, Service Distributor Name*, Phone No*, Address, Country*,Region*,Zone*,State*,City*,Area,Pincode/Zipcode\n";

	header("Content-type: application/octet-stream");
	header("Content-Disposition: attachment; filename=\"service_distributor_sp_sheet.csv\"");		
	echo $data;	
	die;

}
	
if(isset($_POST['service_distributor_sp_import']) && $_POST['service_distributor_sp_import']=='yes'){
	if(isset($_FILES['fileToUpload']['tmp_name']) && $_FILES['fileToUpload']['tmp_name']!=""){			 
		$ret = $_objItem->uploadServiceDistributorSpCsv();
		if($ret=='') {																	
			$msg= "File has been successfully imported.";								
			}elseif($ret=='no') {
			$error= "Empty file";
			}else {
			$error= implode(",", $ret);														
			}
			if($msg!=''){
			$dis_sus=$msg;
			}else{
				
			$data="Row Number(Please Delete The Row When Import), Service Distributor SP Name*, Service Distributor SP Code*, Service Distributor Name*, Phone No*, Address, Country*,Region*,Zone*,State*,City*,Area,Pincode/Zipcode\n";

			$data .="$error \n";
			header("Content-type: application/octet-stream");
			header("Content-Disposition: attachment; filename=\"error.csv\"");		
			echo $data;	
			die;
			}		
		}
	}



/******************************************************************
* DESC: Download preformatted service distributor service personnel
* 
*
*******************************************************************/

	

/******************************************************************
* DESC: Download preformatted dealers
* Created By: Pooja@3Jul2017
*
*******************************************************************/


if(isset($_POST['dealers_list_import_csv']) && $_POST['dealers_list_import_csv']=='yes'){	 
	// $data="Retailer Name*,Phone No1*,Phone No2,Retailer Address,City*,State*,District*,Taluka Name*,Zipcode,Contact Person1*,contact Phone No1* ,Contact Person2,Contact Phone No2,Email-ID1*,Email-ID2,Retailer Class, Route Name,Retailer Channel,Distributor Code,Interested ,Retailer Type,Aadhar No,Pan No,Retailer Code*, Division\n";  

	$data="Dealer Name*,Phone No1*,Phone No2,Dealer Address,State*,City*,Pincode/Zipcode*,Contact Person1*,contact Phone No1* ,Contact Person2,Contact Phone No2,Email-ID1*,Email-ID2,Aadhar No,Pan No,Dealer Code*, Division*, Brand*, Country*, Region*, Zone*, Area\n";   

	header("Content-type: application/octet-stream");
	header("Content-Disposition: attachment; filename=\"dealer_sheet.csv\"");		
	echo $data;	
	die;
}


/******************************************************************
* DESC: Upload dealers csv from distributor panel
* Created By: Pooja@3Jul2017
*
*******************************************************************/

if(isset($_POST['dealers_list_import']) && $_POST['dealers_list_import']=='yes'){
	if(isset($_FILES['fileToUpload']['tmp_name']) && $_FILES['fileToUpload']['tmp_name']!=""){			 
		$ret = $_objItem->uploadDealersListFile();
		if($ret=='') {																	
			$msg= "File has been successfully imported.";								
			}elseif($ret=='no') {
			$error= "Empty file";
			}else {
			$error= implode(",", $ret);														
			}
			if($msg!=''){
			$ret_sus=$msg;
			}else{
			//$data="Row Number(Please Delete The Row When Import),Retailer Name*,Phone No1*,Phone No2,Retailer Address,City*,State*,District*,Taluka Name*,Zipcode,Contact Person1*,contact Phone No1* ,Contact Person2,Contact Phone No2,Email-ID1*,Email-ID2,Retailer Class, Route Name,Retailer Channel,Distributor Code,Interested ,Retailer Type,Aadhar No,Pan No,Retailer Code*, Division\n";	

			$data="Row Number(Please Delete The Row When Import),Dealer Name*,Phone No1*,Phone No2,Dealer Address,State*,City*,Pincode/Zipcode*,Contact Person1*,contact Phone No1* ,Contact Person2,Contact Phone No2,Email-ID1*,Email-ID2,Aadhar No,Pan No,Dealer Code*, Division*, Brand*, Country*, Region*, Zone*, Area\n";	
			
			$data .="$error \n";
			header("Content-type: application/octet-stream");
			header("Content-Disposition: attachment; filename=\"error.csv\"");		
			echo $data;	
			die;
			}		
		}
}

/************************************* CSV For dealers ***************************************/


?>