<?php
include("includes/config.inc.php");
include("includes/function.php");
$_objAdmin = new Admin();
$_objItem = new Item();
$objArrayList= new ArrayList();
include("import.inc.php");


?>

<?php include("header.inc.php");
$pageAccess=2;
$check=$objArrayList->checkAccess($pageAccess, basename($_SERVER['PHP_SELF']));
if($check == false){
header('Location: ' . $_SERVER['HTTP_REFERER']);}
 ?>
<div class="clear"></div>
<!-- start content-outer -->
<div id="content-outer">
<!-- start content -->
	<div id="content">
	<div id="page-heading"><h1><span style="color: #d74343; font-family: Tahoma; font-weight: bold;">Category</span></h1></div>
<table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">
	<tr>
		<th rowspan="3" class="sized"><img src="images/side_shadowleft.jpg" width="20" height="300" alt="" /></th>
		<th class="topleft"></th>
		<td id="tbl-border-top">&nbsp;</td>
		<th class="topright"></th>
		<th rowspan="3" class="sized"><img src="images/side_shadowright.jpg" width="20" height="300" alt="" /></th>
	</tr>
	<tr>
		<!--<td id="tbl-border-left"></td>-->
		<td>
		<!--  start content-table-inner -->
		<div id="content-table-inner">
		<table border="0" width="100%" cellpadding="0" cellspacing="0">
		<tr valign="top">
		<td>
			<!--  start message-red -->
			<?php if($cat_err!=''){?>
			<div id="message-red">
				<table border="0" width="100%" cellpadding="0" cellspacing="0">
				<tr>
					<td class="red-left">Error. <?php echo $cat_err; ?></td>
					<td class="red-right"><a class="close-red"><img src="images/icon_close_red.gif"   alt="" /></a></td>
				</tr>
				</table>
			</div>
			<?php } ?>
			<!--  end message-red -->
			<?php if($cat_sus!=''){?>
			<!--  start message-green -->
			<div id="message-green">
				<table border="0" width="100%" cellpadding="0" cellspacing="0">
				<tr>
					<td class="green-left"><?php echo $cat_sus; ?></td>
					<td class="green-right"><a class="close-green"><img src="images/icon_close_green.gif"   alt="" /></a></td>
				</tr>
				</table>
			</div>
			<?php } ?>
			<!--  end message-green -->
			<!-- start id-form -->
			<form name="frmPre" id="frmPre" method="post" action="import_category.php" enctype="multipart/form-data" >
			<table border="0" cellpadding="0" cellspacing="0"  id="id-form">
			<tr>
				<td valign="top">Pre-formated Spreadsheet to enter multiple Categoryes</td>
			</tr>
			<tr>
			<td valign="top">
				<input name="account_id" type="hidden" value="<?php echo $_SESSION['accountId']; ?>" />
				<input name="category_import_csv" type="hidden" value="yes" />
				<input name="submit" class="form-submit" type="submit" id="submit" value="Click to Downlaod" />
			</td>
			</tr>
			</table>
			</form>
			<form name="form" id="form" method="post" action="import_category.php" enctype="multipart/form-data" >
			<table border="0" cellpadding="0" cellspacing="0"  id="id-form">
			<tr>
				<th valign="top">Import Item Category</th>
			</tr>
			<tr>
				<td valign="top">Select Spreadsheet File in which your entered category</td>
			</tr>
			<tr>
				<td><input type="file" name="fileToUpload" id="file_1" class="required" ></td>
			</tr>
			<tr>
				<td valign="top">Click Import button Now </td>
			</tr>
			<tr>
			<td valign="top">
				<input name="account_id" type="hidden" value="<?php echo $_SESSION['accountId']; ?>" />
				<input name="category_import" type="hidden" value="yes" />
				<input type="button" value="Back" class="form-reset" onclick="location.href='category.php';" />
				<input name="submit" class="form-submit" type="submit" id="submit" value="Import" />
				<div id="divLoad1" style="display:none;"><img src="images/load.gif" /> Please wait... </div>
			</td>
			</tr>
			</table>
			</form>
			<!-- end id-form  -->
			</td>
			<td>
			<!-- right bar-->
			<?php include("rightbar/category_bar.php") ?>
			<div class="clear"></div>
		</div>
<!-- end related-act-bottom -->
</div>
<!-- end related-activities -->
</td>
</tr>
<tr>
<td><img src="images/blank.gif" width="695" height="1" alt="blank" /></td>
<td></td>
</tr>
</table>
<div class="clear"></div>
</div>
<!--  end content-table-inner  -->
</td>
<td id="tbl-border-right"></td>
</tr>
<!--<tr>
	<th class="sized bottomleft"></th>
	<td id="tbl-border-bottom">&nbsp;</td>
	<th class="sized bottomright"></th>
</tr>-->
</table>