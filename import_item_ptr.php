<?php
include("includes/config.inc.php");
include("includes/function.php");
$_objAdmin = new Admin();
$_objItem = new Item();
include("import.inc.php");

if(isset($_POST['add']) && $_POST['add'] == 'yes')
{
	if(isset($_FILES['fileToUpload']['tmp_name']) && $_FILES['fileToUpload']['tmp_name']!=""){			 
		$ret = $_objItem->importItemsPTRFile();
		if($ret=='') {																	
			$sus= "Items PTR has been added successfully.";								
			}elseif($ret=='no') {
			$error= "Empty file";
			}else {
			$error= "Following record has not been inserted.<br>".$ret;														
			}
			if($sus!=''){
			$sus=$sus;
			}else{
			$err=$error;
			}		
		}
		//$sus="Item price has been added successfully.";
	}


?>

<?php include("header.inc.php") ?>

<script src="javascripts/jquery-1.8.2.js" type="text/javascript"></script>
<script src="javascripts/jquery-ui.js"></script>
<script type="text/javascript" src="javascripts/validate.js"></script>
<script>
    $(function() {
        $( "#from" ).datepicker({
			dateFormat: "yy-mm-dd",
            defaultDate: "+1w",
            changeMonth: true,
            numberOfMonths: 3,
            onSelect: function( selectedDate ) {
                $( "#to" ).datepicker( "option", "minDate", selectedDate );
            }
        });
        $( "#to" ).datepicker({
			dateFormat: "yy-mm-dd",
            defaultDate: "+1w",
            changeMonth: true,
            numberOfMonths: 3,
            onSelect: function( selectedDate ) {
                $( "#from" ).datepicker( "option", "maxDate", selectedDate );
            }
        });
    });
</script>

<div class="clear"></div>
<!-- start content-outer -->
<div id="content-outer">
<!-- start content -->
	<div id="content">
	<div id="page-heading"><h1><span style="color: #d74343; font-family: Tahoma; font-weight: bold;">Item PTR Price</span></h1></div>
		<table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">
		<tr>
			<th rowspan="3" class="sized"><img src="images/side_shadowleft.jpg" width="20" height="300" alt="" /></th>
			<th class="topleft"></th>
			<td id="tbl-border-top">&nbsp;</td>
			<th class="topright"></th>
			<th rowspan="3" class="sized"><img src="images/side_shadowright.jpg" width="20" height="300" alt="" /></th>
		</tr>
		<tr>
			<!--<td id="tbl-border-left"></td>-->
			<td>
			<!--  start content-table-inner -->
			<div id="content-table-inner">
				<table border="0" width="100%" cellpadding="0" cellspacing="0">
				<tr valign="top">
					<td>
					<!--  start message-red -->
					<?php if($err!=''){?>
					<div id="message-red">
						<table border="0" width="100%" cellpadding="0" cellspacing="0">
						<tr>
							<td class="red-left"> <?php echo $err; ?></td>
							<td class="red-right"><a class="close-red"><img src="images/icon_close_red.gif"   alt="" /></a></td>
						</tr>
						</table>
					</div>
					<?php } ?>
					<!--  end message-red -->
					<?php if($sus!=''){?>
					<!--  start message-green -->
					<div id="message-green">
						<table border="0" width="100%" cellpadding="0" cellspacing="0">
						<tr>
							<td class="green-left"><?php echo $sus; ?></td>
							<td class="green-right"><a class="close-green"><img src="images/icon_close_green.gif"   alt="" /></a></td>
						</tr>
						</table>
					</div>
					<?php } ?>
					<!--  end message-green -->
					
			<!-- start id-form -->
		<form name="frmPre" id="frmPre" method="post" action="import_item_ptr.php" enctype="multipart/form-data" >
			<table border="0" cellpadding="0" cellspacing="0"  id="id-form">
			<tr>
				<th valign="top">Item PTR <small>(Import)</small> :</th>
				<td><input type="file" name="fileToUpload" id="file_1" class="text" ></td>
				<td><input name="submit" class="form-submit" type="button" id="submit" value="Downlaod Item PTR CSV Sheet" onclick="location.href='import.inc.php?items_prt_csv=yes';"/></td>
			</tr>
			<tr>
				<th>&nbsp;</th>
				<td valign="top">
					<input name="account_id" type="hidden" value="<?php echo $_SESSION['accountId']; ?>" />
					<input name="add" type="hidden" value="yes" />
					<input type="reset" value="Reset!" class="form-reset">
					<input type="button" value="Back" class="form-reset" onclick="location.href='item.php';" />
					<input name="submit" class="form-submit" type="submit" id="submit" value="Save" />
				</td>
			</tr>
			</table>
		</form>
			<!-- end id-form  -->
			</td>
			<td>
			<!-- right bar-->
			<?php include("rightbar/item_bar.php") ?>
			<div class="clear"></div>
		</div>
<!-- end related-act-bottom -->
</div>
<!-- end related-activities -->
</td>
</tr>
<tr>
<td><img src="images/blank.gif" width="695" height="1" alt="blank" /></td>
<td></td>
</tr>
</table>
<div class="clear"></div>
</div>
<!--  end content-table-inner  -->
</td>
<td id="tbl-border-right"></td>
</tr>
<!--<tr>
	<th class="sized bottomleft"></th>
	<td id="tbl-border-bottom">&nbsp;</td>
	<th class="sized bottomright"></th>
</tr>-->
</table>
<div class="clear">&nbsp;</div>

</div>
<!--  end content -->
<div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->
<div class="clear">&nbsp;</div>
    
<!-- start footer --> 
<?php include("footer.php") ?>  
<!-- end footer -->
</body>
</html>
