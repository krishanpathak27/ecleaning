<?php include("includes/config.inc.php");
      include("includes/function.php");
	  
	  $_objAdmin = new Admin();
	  $_objItem = new Item();
	  
	  
	  include("import.inc.php");
	  include("header.inc.php");?>
	  
	  
<div id="content-outer">
	<!-- start content -->
	<div id="content">
	<div id="page-heading"><h1><span style="color: #d74343; font-family: Tahoma; font-weight: bold;">Import Stock</span></h1></div>

	<?php if($cat_err!=''){?>
		<div id="message-red">
			<table border="0" width="100%" cellpadding="0" cellspacing="0">
			<tr>
				<td class="red-left">Error. <?php echo $cat_err; ?></td>
				<td class="red-right"><a class="close-red"><img src="images/icon_close_red.gif"   alt="" /></a></td>
			</tr>
			</table>
		</div>
	<?php } ?>
	<!--  end message-red -->

	<!--  start message-green -->
	<?php if($sus!=''){?>
	<div id="message-green">
		<table border="0" width="100%" cellpadding="0" cellspacing="0">
		<tr>
			<td class="green-left"><?php echo $sus;?></td>
			<td class="green-right"><a class="close-green"><img src="images/icon_close_green.gif"   alt="" /></a></td>
		</tr>
		</table>
	</div>
	<?php } ?>
	
	
	
	
	<table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">
	<tr>
		<td>
		<!-- start content-table-inner -->
		<div id="content-table-inner">
	
		<table border="0" width="100%" cellpadding="0" cellspacing="0">
		<tr valign="top">
		<td>	
	
	<!-- start id-form -->
	<form name="frmPre" id="frmPre" method="post" action="import_stock.php" enctype="multipart/form-data" >
	<table border="0" cellpadding="0" cellspacing="0"  id="id-form">
	<tr>
		<td valign="top">Pre-formated Spreadsheet to enter multiple Stock</td>
		</tr>
	<tr>
		<td valign="top">
		<input name="account_id" type="hidden" value="<?php echo $_SESSION['accountId']; ?>" />
		<input name="stock_import_csv" type="hidden" value="yes" />
		<input name="submit" class="form-submit" type="submit" id="submit" value="Click to Downlaod" />
		</td>
	</tr>
	</table>
	</form>
		<form name="form" id="form" method="post" action="import_stock.php" enctype="multipart/form-data" >
		<table border="0" cellpadding="0" cellspacing="0"  id="id-form">
		<tr>
			<th valign="top">Import Stock</th>
		</tr>
		<tr>
			<td valign="top">Select Spreadsheet File in which your entered Stock</td>
		</tr>
		<tr>
			<td><input type="file" name="fileToUpload" id="file_1" class="required" ></td>
		</tr>
		<tr>
			<td valign="top">Click Import button Now </td>
		</tr>
		<tr>
			<td valign="top">
			<input name="account_id" type="hidden" value="<?php echo $_SESSION['accountId']; ?>" />
			<input name="stock_import" type="hidden" value="yes" />
			<input type="button" value="Back" class="form-reset" onclick="location.href='stock.php';" />
			<input name="submit" class="form-submit" type="submit" id="submit" value="Import" />
			<div id="divLoad1" style="display:none;"><img src="images/load.gif" /> Please wait... </div>
			</td>
		</tr>
		</table>
		</form>
	<!-- end id-form  -->
	</td>
	<td>
	<!-- right bar-->
	</td>
	</tr>
<tr>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
</table>
<div class="clear"></div>
</div>
<!--  end content-table-inner  -->
</td>
<td id="tbl-border-right"></td>
</tr>
</table>	
<div class="clear">&nbsp;</div>
</div>
<!--  end content -->
<div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->
<div class="clear">&nbsp;</div> 
<!-- start footer -->         
<?php include("footer.php");?>
<!-- end footer -->
</body>
</html>	