
<script>
	$(function() {
		var availableState = [
			<?php
			$auCol=$_objAdmin->_getSelectList2('state',"state_name",''," state_name!='' ORDER by state_name");
			for($i=0;$i<count($auCol);$i++){
				if(count($auCol)-1!=$i)
				{
					echo '"'.$auCol[$i]->state_name.'",';
				} else {
					echo '"'.$auCol[$i]->state_name.'"';
				}
			}
			?>
		];
		function split( val ) {
			return val.split( /,\s*/ );
		}
		function extractLast( term ) {
			return split( term ).pop();
		}

		$( "#state_list" )
			// don't navigate away from the field on tab when selecting an item
			.bind( "keydown", function( event ) {
				if ( event.keyCode === $.ui.keyCode.TAB &&
						$( this ).data( "autocomplete" ).menu.active ) {
					event.preventDefault();
				}
			})
			.autocomplete({
				minLength: 0,
				source: function( request, response ) {
					// delegate back to autocomplete, but extract the last term
					response( $.ui.autocomplete.filter(
						availableState, extractLast( request.term ) ) );
				},
				focus: function() {
					// prevent value inserted on focus
					return false;
				},
				select: function( event, ui ) {
					var terms = split( this.value );
					// remove the current input
					terms.pop();
					// add the selected item
					terms.push( ui.item.value );
					// add placeholder to get the comma-and-space at the end
					terms.push( "" );
					this.value = terms.join( "," );
					availableState.splice($.inArray(ui.item.value, availableState), 1);
					return false;
				}
			});
	});

	$(function() {
		var availableCity = [
			<?php
			$auCol=$_objAdmin->_getSelectList2('city',"city_name",''," city_name!='' and city_name!='City*' ORDER by city_name");
			for($i=0;$i<count($auCol);$i++){
				if(count($auCol)-1!=$i)
				{
					echo '"'.$auCol[$i]->city_name.'",';
				} else {
					echo '"'.$auCol[$i]->city_name.'"';
				}
			}
			?>
		];
		function split( val ) {
			return val.split( /,\s*/ );
		}
		function extractLast( term ) {
			return split( term ).pop();
		}

		$( "#city_list" )
			// don't navigate away from the field on tab when selecting an item
			.bind( "keydown", function( event ) {
				if ( event.keyCode === $.ui.keyCode.TAB &&
						$( this ).data( "autocomplete" ).menu.active ) {
					event.preventDefault();
				}
			})
			.autocomplete({
				minLength: 0,
				source: function( request, response ) {
					// delegate back to autocomplete, but extract the last term
					response( $.ui.autocomplete.filter(
						availableCity, extractLast( request.term ) ) );
				},
				focus: function() {
					// prevent value inserted on focus
					return false;
				},
				select: function( event, ui ) {
					var terms = split( this.value );
					// remove the current input
					terms.pop();
					// add the selected item
					terms.push( ui.item.value );
					// add placeholder to get the comma-and-space at the end
					terms.push( "" );
					this.value = terms.join( "," );
					availableCity.splice($.inArray(ui.item.value, availableCity), 1);
					return false;
				}
			});
	});
	
	$(function() {
		var availableCategory = [
			<?php
			$auCol=$_objAdmin->_getSelectList('table_category',"category_code",''," 1=1 ORDER by category_code");
			for($i=0;$i<count($auCol);$i++){
				if(count($auCol)-1!=$i)
				{
					echo '"'.$auCol[$i]->category_code.'",';
				} else {
					echo '"'.$auCol[$i]->category_code.'"';
				}
			}
			?>
		];
		function split( val ) {
			return val.split( /,\s*/ );
		}
		function extractLast( term ) {
			return split( term ).pop();
		}

		$( "#category_list" )
			// don't navigate away from the field on tab when selecting an item
			.bind( "keydown", function( event ) {
				if ( event.keyCode === $.ui.keyCode.TAB &&
						$( this ).data( "autocomplete" ).menu.active ) {
					event.preventDefault();
				}
			})
			.autocomplete({
				minLength: 0,
				source: function( request, response ) {
					// delegate back to autocomplete, but extract the last term
					response( $.ui.autocomplete.filter(
						availableCategory, extractLast( request.term ) ) );
				},
				focus: function() {
					// prevent value inserted on focus
					return false;
				},
				select: function( event, ui ) {
					var terms = split( this.value );
					terms.pop();
					terms.push( ui.item.value );
					terms.push( "" );
					this.value = terms.join( "," );
					availableCategory.splice($.inArray(ui.item.value, availableCategory), 1);
					return false;
				}
			});
	});


	$(function() {
		var availableSegments = [
			<?php
			$auCol=$_objAdmin->_getSelectList('table_segment',"segment_name",''," 1=1 ORDER by segment_name");
			for($i=0;$i<count($auCol);$i++){
				if(count($auCol)-1!=$i)
				{
					echo '"'.$auCol[$i]->segment_name.'",';
				} else {
					echo '"'.$auCol[$i]->segment_name.'"';
				}
			}
			?>
		];
		function split( val ) {
			return val.split( /,\s*/ );
		}
		function extractLast( term ) {
			return split( term ).pop();
		}

		$( "#segment_list" )
			// don't navigate away from the field on tab when selecting an item
			.bind( "keydown", function( event ) {
				if ( event.keyCode === $.ui.keyCode.TAB &&
						$( this ).data( "autocomplete" ).menu.active ) {
					event.preventDefault();
				}
			})
			.autocomplete({
				minLength: 0,
				source: function( request, response ) {
					// delegate back to autocomplete, but extract the last term
					response( $.ui.autocomplete.filter(
						availableSegments, extractLast( request.term ) ) );
				},
				focus: function() {
					// prevent value inserted on focus
					return false;
				},
				select: function( event, ui ) {
					var terms = split( this.value );
					terms.pop();
					terms.push( ui.item.value );
					terms.push( "" );
					this.value = terms.join( "," );
					availableSegments.splice($.inArray(ui.item.value, availableSegments), 1);
					return false;
				}
			});
	});
	
	$(function() {
		var availableItem = [
			<?php
			$auCol=$_objAdmin->_getSelectList('table_item',"item_code",''," 1=1 ORDER by item_code");
			for($i=0;$i<count($auCol);$i++){
				if(count($auCol)-1!=$i)
				{
					echo '"'.$auCol[$i]->item_code.'",';
				} else {
					echo '"'.$auCol[$i]->item_code.'"';
				}
			}
			?>
		];
		function split( val ) {
			return val.split( /,\s*/ );
		}
		function extractLast( term ) {
			return split( term ).pop();
		}

		$( "#item_list" )
			// don't navigate away from the field on tab when selecting an item
			.bind( "keydown", function( event ) {
				if ( event.keyCode === $.ui.keyCode.TAB &&
						$( this ).data( "autocomplete" ).menu.active ) {
					event.preventDefault();
				}
			})
			.autocomplete({
				minLength: 0,
				source: function( request, response ) {
					// delegate back to autocomplete, but extract the last term
					response( $.ui.autocomplete.filter(
						availableItem, extractLast( request.term ) ) );
				},
				focus: function() {
					// prevent value inserted on focus
					return false;
				},
				select: function( event, ui ) {
					var terms = split( this.value );
					// remove the current input
					terms.pop();
					// add the selected item
					terms.push( ui.item.value );
					// add placeholder to get the comma-and-space at the end
					terms.push( "" );
					this.value = terms.join( "," );
					availableItem.splice($.inArray(ui.item.value, availableItem), 1);
					return false;
				}
			});
	});
	
	
	
	
	$(function() {
		var availableItem1 = [
			<?php
			$auCol=$_objAdmin->_getSelectList('table_item',"item_code",''," 1=1 ORDER by item_code");
			for($i=0;$i<count($auCol);$i++){
				if(count($auCol)-1!=$i)
				{
					echo '"'.$auCol[$i]->item_code.'",';
				} else {
					echo '"'.$auCol[$i]->item_code.'"';
				}
			}
			?>
		];
		function split( val ) {
			return val.split( /,\s*/ );
		}
		function extractLast( term ) {
			return split( term ).pop();
		}

		$( "#item_list1" )
			// don't navigate away from the field on tab when selecting an item
			.bind( "keydown", function( event ) {
				if ( event.keyCode === $.ui.keyCode.TAB &&
						$( this ).data( "autocomplete" ).menu.active ) {
					event.preventDefault();
				}
			})
			.autocomplete({
				minLength: 0,
				source: function( request, response ) {
					// delegate back to autocomplete, but extract the last term
					response( $.ui.autocomplete.filter(
						availableItem1, extractLast( request.term ) ) );
				},
				focus: function() {
					// prevent value inserted on focus
					return false;
				},
				select: function( event, ui ) {
					var terms = split( this.value );
					// remove the current input
					terms.pop();
					// add the selected item
					terms.push( ui.item.value );
					// add placeholder to get the comma-and-space at the end
					terms.push( "" );
					this.value = terms.join( "," );
					availableItem1.splice($.inArray(ui.item.value, availableItem1), 1);
					return false;
				}
			});
	});
	
	
	
	$(function() {
		var availableFocusItem = [
			<?php
			$auCol=$_objAdmin->_getSelectList('table_item',"item_code",''," 1=1 ORDER by item_code");
			for($i=0;$i<count($auCol);$i++){
				if(count($auCol)-1!=$i)
				{
					echo '"'.$auCol[$i]->item_code.'",';
				} else {
					echo '"'.$auCol[$i]->item_code.'"';
				}
			}
			?>
		];
		function split( val ) {
			return val.split( /,\s*/ );
		}
		function extractLast( term ) {
			return split( term ).pop();
		}

		$( "#focus_item_list" )
			// don't navigate away from the field on tab when selecting an item
			.bind( "keydown", function( event ) {
				if ( event.keyCode === $.ui.keyCode.TAB &&
						$( this ).data( "autocomplete" ).menu.active ) {
					event.preventDefault();
				}
			})
			.autocomplete({
				minLength: 0,
				source: function( request, response ) {
					// delegate back to autocomplete, but extract the last term
					response( $.ui.autocomplete.filter(
						availableFocusItem, extractLast( request.term ) ) );
				},
				focus: function() {
					// prevent value inserted on focus
					return false;
				},
				select: function( event, ui ) {
					var terms = split( this.value );
					// remove the current input
					terms.pop();
					// add the selected item
					terms.push( ui.item.value );
					// add placeholder to get the comma-and-space at the end
					terms.push( "" );
					this.value = terms.join( "," );
					availableFocusItem.splice($.inArray(ui.item.value, availableFocusItem), 1);
					return false;
				}
			});
	});
	
	</script>
