<?php
include("includes/config.inc.php");
include("includes/function.php");
include("includes/globalarraylist.php");

//$_objAdmin = new Admin();
$page_name="Incentive Report";
if(isset($_POST['showReport']) && $_POST['showReport'] == 'yes')
{	
	if($_POST['sal']!="") 
	{
	$sal_id=$_POST['sal'];
		//$salesman=" AND s.salesman_id='".$_POST['sal']."' ";
		$salArrList=$_POST['sal'];	
		$filterby=$_POST['filterby'];
		$salesman = $_objArrayList->getSalesmanConsolidatedList($salArrList,$filterby);
	}
	if($_POST['from']!="") 
	{
	$from_date=$_objAdmin->_changeDate($_POST['from']);	
	}
	if($_POST['to']!="") 
	{
	$to_date=$_objAdmin->_changeDate($_POST['to']);	
	}
} else {
$from_date= $_objAdmin->_changeDate(date("Y-m-d"));
$to_date= $_objAdmin->_changeDate(date("Y-m-d"));	
}

if($_REQUEST['filterby']==''){
		
			$_REQUEST['filterby']=1;	
		
		}

if(isset($_REQUEST['reset']) && $_REQUEST['reset'] == 'yes')
{
	header("Location: incentive_report.php");
}
if($sal_id!=''){
$SalName=$_objAdmin->_getSelectList('table_salesman','salesman_name',''," salesman_id='".$sal_id."'"); 
$sal_name=$SalName[0]->salesman_name;
} else {
$sal_name="All Salesman";
}
?>

<?php include("header.inc.php") ?>
<script type="text/javascript">
	function showloader()
	{
		$('#Report').hide();
		$('#loader').show();
	}
</script>
<script>
$(document).ready(function(){
	$('#loader').hide();
	$('#loader').html('<div id="loader" align="center"><img src="images/ajax-loader.gif" /><br/>Please Wait...</div>');
	$('#Report').show();
});
</script>
<script type="text/javascript">

    function PrintElem(elem)
    {
        Popup($(elem).html());
    }

    function Popup(data) 
    {
		
        var mywindow = window.open('', 'Report');
		
        mywindow.document.write('<html><head><title>Incentive Report</title>');
		mywindow.document.write('<table><tr><td><b>Salesman Name:</b> <?php echo $sal_name; ?></td><td><b>Report Date:</b> <?php echo $from_date; ?></td></tr></table>');
        /*optional stylesheet*/ //mywindow.document.write('<link rel="stylesheet" href="main.css" type="text/css" />');
        mywindow.document.write('</head><body >');
        mywindow.document.write(data);
        mywindow.document.write('</body></html>');

        mywindow.print();
        mywindow.close();
        return true;
    }

</script>
<script src="javascripts/dateNextPrev.js" type="text/javascript"></script>
<!-- start content-outer -->
<input name="pagename" type="hidden"  id="pagename" value="admin_order_list.php" />
<div id="content-outer">
<!-- start content -->
<div id="content">
<div id="loader" style="position:absolute; margin-left:40%; margin-top:10%;"></div>
<div id="page-heading"><h1><span style="color: #d74343; font-family: Tahoma; font-weight: bold;">Incentive Report</span></h1></div>
<table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">

<tr>
	
	<td>
	<!--  start content-table-inner -->
	<div id="content-table-inner">
	
	<div id="page-heading" align="left" >
	<form name="frmPre" id="frmPre" method="post" action="incentive_report.php" enctype="multipart/form-data" >
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
	<tr>
		<td ><h3>Salesman Name:</h3><h6> 
		
		 <select name="sal" id="sal" class="styledselect_form_5" style="" >
			<?php echo $rtndata = $_objArrayList->GetSalesmenMenu($salsList, $_REQUEST['sal']);?>
		</select>
		</h6></td>

		
		<td><h3>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;From Date: </h3><h6><img src="css/images/prev.png" height="18" width="18" onclick="dateFromPrev();"> <input type="text" id="from" name="from" class="date" style="width:150px" value="<?php  echo $from_date;?>"  readonly /> <img src="css/images/next.png" height="18" width="18" onclick="dateFromNext();"></h6></td>
		<td><h3>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;To Date: </h3> <h6><img src="css/images/prev.png" height="18" width="18" onclick="dateToPrev();"> <input type="text" id="to" name="to" class="date" style="width:150px" value="<?php echo $to_date; ?>"  readonly /><img src="css/images/next.png" height="18" width="18" onclick="dateToNext();"></h6></td>
		<td><h3></h3>
		<input name="showReport" type="hidden" value="yes" />
		<input name="submit" class="result-submit" type="submit" id="submit" value="View Details" onclick="showloader()";/>		 		
		<input type="button" value="Reset!" class="form-reset" onclick="location.href='incentive_report.php?reset=yes';" />
		</td>
		<td colspan="2"></td>
		</tr>
		<tr class="consolidatedReport">
					<td colspan="3" ><h2>View Report For:&nbsp;&nbsp;
					<input type="radio" name="filterby" value="1" <?php if($_REQUEST['filterby']==1){ ?> checked="checked" <?php } ?>   />&nbsp;Individual
					<input type="radio" name="filterby" value="2" <?php if($_REQUEST['filterby']==2){ ?> checked="checked" <?php } ?>  />&nbsp;Hierarchy</h2>
					</td>
					</tr>
	</table>
	</form>
	</div>
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
	<tr valign="top">
		<td>
		<div id="Report" style="width:1000px";>
		<table  border="0" width="100%" cellpadding="0" cellspacing="0" id="report_export" name="report_export" >
			<tr  bgcolor="#6E6E6E" style="color: #fff;font-weight: bold;" >
				<td style="padding:10px;" width="10%">Sales person</td>
				<td style="padding:10px;" width="30%">Discription</td>
				<td style="padding:10px;" width="10%">Duration</td>
				<td style="padding:10px;" width="10%">Start Date</td>
				<td style="padding:10px;" width="10%">End Date</td>
				<td style="padding:10px;" width="10%">Minimum Value</td>
				<td style="padding:10px;" width="10%">Achieved Value</td>
				<td style="padding:10px;" width="10%">Reward Amount</td>
			</tr>
			<?php
			$salRec=$_objAdmin->_getSelectList('table_salesman as s','s.salesman_id,s.salesman_name',''," s.status='A' $salesman ORDER BY s.salesman_name");
			for($z=0;$z<count($salRec);$z++){
			$auRec = mysql_query("SELECT i.target_incentive_id, i.description, i.target_incentive_type, i.incentive_reward_amount, i.type_id,i.incentive_reward_type, d.from_date, d.to_date, t.type_description,  i.party_type_id, du.dur_description, p.party_description, d.duration_id, s.salesman_id, d.primary_values, d.secondary_values FROM  table_target_incentive as i inner join table_target_incentive_duration as d on i.target_incentive_id=d.target_incentive_id inner join table_target_incentive_salesman as s on d.duration_id=s.duration_id left join table_type as t on t.type_id=i.type_id left join table_duration as du on du.dur_id=i.dur_id left join table_party_type as p on p.party_type_id=i.party_type_id where s.salesman_id='".$salRec[$z]->salesman_id."' AND d.from_date <='".date('Y-m-d', strtotime($from_date))."' AND d.to_date>='".date('Y-m-d', strtotime($from_date))."' AND i.target_incentive_type=2 AND s.status='A' ");
			if(mysql_num_rows($auRec)> 0){
				$report=1;
				while ($TIRec = mysql_fetch_array($auRec)){
			?>
				<tr  style="border-bottom:2px solid #6E6E6E;" >
					<td style="padding:10px;"><?php echo $salRec[$z]->salesman_name;?></td>
					<td style="padding:10px;">
					<?php
					if($TIRec['target_incentive_type']==1)
						{
						$type="Target";
						$type_value="achieved";
						} else {
						$type="Incentive";
						$type_value="availed";
						}
						//Start Check Qualifiers Target and Incentive
						$qua_desc="";
						$check=mysql_query("SELECT * FROM table_target_incentive where applied_incentive_id='".$TIRec['target_incentive_id']."' and qualifiers_type=2 and status='A'");
						$checknum=mysql_num_rows($check);
						if($checknum > 0){
							$qua_desc.="</br>The above ".$type." can only be ".$type_value." if the conditions below are met:-</br>";
							$DescList="";
							$a=0;
							while ($check_id = mysql_fetch_array($check)){
							$a++;
								if($check_id['type_id']==1){
								$cat=mysql_query("SELECT c.category_name,c.category_code FROM table_target_incentive_type as t left join table_category as c on c.category_id=t.ref_id WHERE t.target_incentive_id='".$check_id['target_incentive_id']."'");
								$i=0;
									while ($catRec = mysql_fetch_array($cat)){
									$i++;
									$cat_list.="     ".$i.". ".$catRec['category_name']."(".$catRec['category_code'].")</br>";
									}
									$DescList.=$a.". For selling specific categories of products </br>".$cat_list."Minimum quantity:-".$TIRec['primary_values'].". </br>";
									unset($cat_list);
								}
								if($check_id['type_id']==2){
									$item=mysql_query("SELECT i.item_name,i.item_code FROM table_target_incentive_type as t left join table_item as i on i.item_id=t.ref_id WHERE t.target_incentive_id='".$check_id['target_incentive_id']."'");
									$i=0;
									while ($itemRec = mysql_fetch_array($item)){
									$i++;
									$item_list.="     ".$i.". ".$itemRec['item_name']."(".$itemRec['item_code'].")</br>";
									}
									$DescList.=$a.". For selling specific items </br>".$item_list."Minimum quantity:-".$check_id['primary_values'].". </br>";
									unset($item_list);
								}
								if($check_id['type_id']==3){
									$DescList.=$a.". For Minimum ".$check_id['primary_values']." Item(s) in each Order. For atleast ".$check_id['secondary_values']." Order(s). </br>";
								}
								if($check_id['type_id']==4){
									$DescList.=$a.". Atleast Minimum ".$check_id['primary_values']." Order(s) are taken. </br>";
								}
								if($check_id['type_id']==5){
									$DescList.=$a.". Minimum value of each order should be Rs. ".$check_id['primary_values']." for atleast ".$check_id['secondary_values']." order(s). </br>";
								}
								if($check_id['type_id']==6){
									$DescList.=$a.". Atleast ".$check_id['primary_values']." schemes are present.</br>";
								}
								if($check_id['type_id']==7){
									$DescList.=$a.". Atleast visitng minimum ".$check_id['primary_values']." Number Of Retailer(s).  </br>";
								}
								if($check_id['type_id']==9){
									$item=mysql_query("SELECT i.item_name,i.item_code FROM table_target_incentive_type as t left join table_item as i on i.item_id=t.ref_id WHERE t.target_incentive_id='".$check_id['target_incentive_id']."'");
									$i=0;
									while ($itemRec = mysql_fetch_array($item)){
									$i++;
									$item_list.=$i.". ".$itemRec['item_name']."(".$itemRec['item_code'].")</br>";
									}
									$DescList.=$a.". For taking Orders of Focused Product(s) from atleast ".$check_id['secondary_values']." Retailer(s). </br>".$item_list."Minimum quantity:-".$check_id['primary_values'].". </br>";
									unset($item_list);
								}
								if($check_id['type_id']==10){
									$DescList.=$a.". Atleast ".$check_id['primary_values']." new retailers are added. </br>";
								}
								if($check_id['type_id']==11){
									$DescList.=$a.". Atleast ".$check_id['primary_values']." calls. Min ".$check_id['secondary_values']."% of calls should be productive. </br>";
								}
								if($check_id['type_id']==12){
									$DescList.=$a.". Total order(s) value of atleast Rs. ".$check_id['primary_values'].".</br>";
								}
							}
							$qua_desc.=$DescList;
							unset($DescList);
						}
							
						
						//End Check Qualifiers Target and Incentive
						
						//Start Party Type
						if($TIRec['party_type_id']==1)
						{
						 $party_type="</br>Applicable to all Retailers";
						}
						
						if($TIRec['party_type_id']==2)
						{
							$state=mysql_query("SELECT s.state_name FROM table_target_incentive_party as p left join state as s on s.state_id=p.state_id WHERE p.target_incentive_Id='".$TIRec['target_incentive_id']."'");
							$party_type="</br>Applicable to all Retailers in </br>";
							$i=0;
							while ($stateRec = mysql_fetch_array($state)){
							$i++;
							$party_type.=$i.". ".$stateRec['state_name']."</br>";
							}
						}
						if($TIRec['party_type_id']==3)
						{
							$city=mysql_query("SELECT c.city_name FROM table_target_incentive_party as p left join city as c on c.city_id=p.city_id WHERE p.target_incentive_Id='".$TIRec['target_incentive_id']."'");
							$party_type="</br>Applicable to all Retailers in </br>";
							$i=0;
							while ($cityRec = mysql_fetch_array($city)){
							$i++;
							$party_type.=$i.". ".$cityRec['city_name']."</br>";
							}
							//$party_type=" And Party City(".$city_list.")";
						}
						
						//End Party Type 
						
						//Start slab
						if($TIRec['incentive_reward_type']==2)
						{
						 $slab=mysql_query("SELECT percentage_of_criteria,percentage_of_reward_amount FROM table_target_incentive_slab WHERE target_incentive_Id='".$TIRec['target_incentive_id']."'");
							$slab_list="Slabs:- </br>";
							$i=0;
							while ($slabRec = mysql_fetch_array($slab)){
							$i++;
							$slab_list.=$i.". ".$slabRec['percentage_of_criteria']."% & Above:- ".$slabRec['percentage_of_reward_amount']."% </br>";
							}
						}
						//End slab
						
						//Start Long description Incentive Type
						if($TIRec['type_id']==1){
							$cat=mysql_query("SELECT c.category_name,c.category_code FROM table_target_incentive_type as t left join table_category as c on c.category_id=t.ref_id WHERE t.target_incentive_id='".$TIRec['target_incentive_id']."'");
							$i=0;
							while ($catRec = mysql_fetch_array($cat)){
							$i++;
							$cat_list.=$i.". ".$catRec['category_name']."(".$catRec['category_code'].")</br>";
							}
							$LogDesc=$type." for selling specific categories of products </br>".$cat_list."Minimum quantity:-".$TIRec['primary_values'].". </br>";
							unset($cat_list);
							}
						
						if($TIRec['type_id']==2){
							$item=mysql_query("SELECT i.item_name,i.item_code FROM table_target_incentive_type as t left join table_item as i on i.item_id=t.ref_id WHERE t.target_incentive_id='".$TIRec['target_incentive_id']."'");
							$i=0;
							while ($itemRec = mysql_fetch_array($item)){
							$i++;
							$item_list.=$i.". ".$itemRec['item_name']."(".$itemRec['item_code'].")</br>";
							}
							$LogDesc=$type." for selling specific item(s) </br>".$item_list."Minimum quantity:-".$TIRec['primary_values'].". </br>";
							unset($item_list);
						}
						
						if($TIRec['type_id']==3){
							$LogDesc=$type." can be ".$type_value." for Minimum ".$TIRec['primary_values']." Item(s) in each Order. For atleast ".$TIRec['secondary_values']." Order(s). </br>";
						}
						
						if($TIRec['type_id']==4){
							$LogDesc=$type." can be ".$type_value." for Minimum ".$TIRec['primary_values']." Order(s) are taken. </br>";
						}
						if($TIRec['type_id']==5){
							$LogDesc=$type." can be ".$type_value." if Minimum value of each order should be Rs. ".$TIRec['primary_values']." for atleast ".$TIRec['secondary_values']." order(s). </br>";
						}
						if($TIRec['type_id']==6){
							$LogDesc=$type." can be ".$type_value." if atleast ".$TIRec['primary_values']." schemes are present.</br>";
						}
						if($TIRec['type_id']==7){
							$LogDesc=$type." can be ".$type_value." on visitng minimum ".$TIRec['primary_values']." Number Of Retailer(s).  </br>";
						}
						if($TIRec['type_id']==9){
							$item=mysql_query("SELECT i.item_name,i.item_code FROM table_target_incentive_type as t left join table_item as i on i.item_id=t.ref_id WHERE t.target_incentive_id='".$TIRec['target_incentive_id']."'");
							$i=0;
							while ($itemRec = mysql_fetch_array($item)){
							$i++;
							$item_list.=$i.". ".$itemRec['item_name']."(".$itemRec['item_code'].")</br>";
							}
							$LogDesc=$type." can be ".$type_value." for taking Orders of Focused Product(s) from atleast ".$TIRec['secondary_values']." Retailer(s). </br>".$item_list."Minimum quantity:-".$TIRec['primary_values'].". </br>";
							unset($item_list);
						}
						if($TIRec['type_id']==10){
							$LogDesc=$type." can be ".$type_value." if ".$TIRec['primary_values']." new retailers are added. </br>";
						}
						if($TIRec['type_id']==11){
							$LogDesc=$type." can be ".$type_value." for atleast ".$TIRec['primary_values']." calls. Min ".$TIRec['secondary_values']."% of calls should be productive. </br>";
						}
						if($TIRec['type_id']==12){
							$LogDesc=$type." can be ".$type_value." for total order(s) value of atleast Rs. ".$TIRec['primary_values'].".</br>";
						}
						//End Long description Incentive Type
						//$Description=$LogDesc.$qua_desc.$party_type;
						$Description=$LogDesc.$qua_desc.$slab_list;
						unset($LogDesc);
						unset($party_type);
						unset($slab_list);
					echo "<b>".$TIRec['description']."</b></br></br>";
					echo $Description; 
					?></td>
					<td style="padding:10px;"><?php echo $TIRec['dur_description']; ?></td>
					<!--<td style="padding:10px;"><?php echo $TIRec['type_description']; ?></td>-->
					<td style="padding:10px;"><?php echo $_objAdmin->_changeDate($TIRec['from_date']); ?></td>
					<td style="padding:10px;"><?php echo $_objAdmin->_changeDate($TIRec['to_date']); ?></td>
					<td style="padding:10px;" align="center">
						<?php 
							if($TIRec['type_id']==3 || $TIRec['type_id']==5 || $TIRec['type_id']==9){
							echo $TIRec['secondary_values'];
							} else {
							echo $TIRec['primary_values'];
							}
						?>
					</td>
					<?php
					//Start Achieved Value Calculation
					$flag=true;
					//Check Qualifiers Target and Incentive
					$check=mysql_query("SELECT * FROM table_target_incentive where applied_incentive_id='".$TIRec['target_incentive_id']."' and qualifiers_type=2 and status='A'");
					$checknum=mysql_num_rows($check);
					if($checknum > 0){
						while ($check_id = mysql_fetch_array($check)){
							
							//Qualifiers Category
							if($check_id['type_id']==1){
								if($check_id['party_type_id']==1){
								$party="";
								}
								if($check_id['party_type_id']==2){
								$state=mysql_query("SELECT state_id FROM table_target_incentive_party WHERE target_incentive_Id='".$TIRec['target_incentive_id']."'");
								$state_list="";
								$i=0;
								while ($stateRec = mysql_fetch_array($state)){
									$i++;
									if($i>1){
									$state_list.=",";
									}
									$state_list.=$stateRec['state_id'];
								}
								$party=" and r.state IN ($state_list)";
								}
								unset($state_list);
								if($check_id['party_type_id']==3){
								$city=mysql_query("SELECT city_id FROM table_target_incentive_party WHERE target_incentive_Id='".$TIRec['target_incentive_id']."'");
								$city_list="";
								$i=0;
								while ($cityRec = mysql_fetch_array($city)){
									$i++;
									if($i>1){
									$city_list.=",";
									}
									$city_list.=$cityRec['city_id'];
								}
								$party=" and r.city IN ($city_list)";
								}
								unset($city_list);
								$QualifiersRec = mysql_query("SELECT SUM(d.acc_quantity) as total_qty FROM table_order as o left join table_order_detail as d on o.order_id=d.order_id left join table_item as i on d.item_id=i.item_id left join table_retailer as r on o.retailer_id=r.retailer_id inner join table_target_incentive_type as t on i.category_id=t.ref_id WHERE o.salesman_id='".$TIRec['salesman_id']."' and (o.date_of_order BETWEEN '".$TIRec['from_date']."' AND '".$TIRec['to_date']."') and o.order_type != 'No' and r.new='' and d.type=1 $party and t.target_incentive_id='".$check_id['target_incentive_id']."' ");
								$QualiffersNum=mysql_num_rows($QualifiersRec);
								if($QualiffersNum > 0){
								$QualiAchRec = mysql_fetch_array($QualifiersRec);
									if($QualiAchRec['total_qty']<$check_id['primary_values']){
										$flag =false;
										}
									} else {
									$flag =false;
								}
								unset($QualifiersRec);
							}
							//Qualifiers Items
							if($check_id['type_id']==2){
								if($check_id['party_type_id']==1){
								$party="";
								}
								if($check_id['party_type_id']==2){
								$state=mysql_query("SELECT state_id FROM table_target_incentive_party WHERE target_incentive_Id='".$TIRec['target_incentive_id']."'");
								$state_list="";
								$i=0;
								while ($stateRec = mysql_fetch_array($state)){
									$i++;
									if($i>1){
									$state_list.=",";
									}
									$state_list.=$stateRec['state_id'];
								}
								$party=" and r.state IN ($state_list)";
								}
								unset($state_list);
								if($check_id['party_type_id']==3){
								$city=mysql_query("SELECT city_id FROM table_target_incentive_party WHERE target_incentive_Id='".$TIRec['target_incentive_id']."'");
								$city_list="";
								$i=0;
								while ($cityRec = mysql_fetch_array($city)){
									$i++;
									if($i>1){
									$city_list.=",";
									}
									$city_list.=$cityRec['city_id'];
								}
								$party=" and r.city IN ($city_list)";
								}
								unset($city_list);
								$QualifiersRec = mysql_query("SELECT SUM(d.acc_quantity) as total_qty FROM table_order as o left join table_order_detail as d on o.order_id=d.order_id left join table_item as i on d.item_id=i.item_id left join table_retailer as r on o.retailer_id=r.retailer_id inner join table_target_incentive_type as t on i.item_id=t.ref_id WHERE o.salesman_id='".$TIRec['salesman_id']."' and (o.date_of_order BETWEEN '".$TIRec['from_date']."' AND '".$TIRec['to_date']."') and o.order_type != 'No' and r.new='' and d.type=1 $party and t.target_incentive_id='".$check_id['target_incentive_id']."'");
								$QualiffersNum=mysql_num_rows($QualifiersRec);
								if($QualiffersNum > 0){
									$QualiAchRec = mysql_fetch_array($QualifiersRec);
									if($QualiAchRec['total_qty']<$check_id['primary_values']){
										$flag =false;
										}
									} else {
									$flag =false;
								}
								unset($QualifiersRec);
							}
							//Qualifiers Items Per Order
							if($check_id['type_id']==3){
								if($check_id['party_type_id']==1){
								$party="";
								}
								if($check_id['party_type_id']==2){
								$state=mysql_query("SELECT state_id FROM table_target_incentive_party WHERE target_incentive_Id='".$TIRec['target_incentive_id']."'");
								$state_list="";
								$i=0;
								while ($stateRec = mysql_fetch_array($state)){
									$i++;
									if($i>1){
									$state_list.=",";
									}
									$state_list.=$stateRec['state_id'];
								}
								$party=" and r.state IN ($state_list)";
								}
								unset($state_list);
								if($check_id['party_type_id']==3){
								$city=mysql_query("SELECT city_id FROM table_target_incentive_party WHERE target_incentive_Id='".$TIRec['target_incentive_id']."'");
								$city_list="";
								$i=0;
								while ($cityRec = mysql_fetch_array($city)){
									$i++;
									if($i>1){
									$city_list.=",";
									}
									$city_list.=$cityRec['city_id'];
								}
								$party=" and r.city IN ($city_list)";
								}
								unset($city_list);
								$QualifiersRec = mysql_query("SELECT Count(distinct o.order_id) as total_order FROM table_order as o left join table_order_detail as d on o.order_id=d.order_id left join table_retailer as r on o.retailer_id=r.retailer_id WHERE o.salesman_id='".$TIRec['salesman_id']."' and (o.date_of_order BETWEEN '".$TIRec['from_date']."' AND '".$TIRec['to_date']."') and o.order_type != 'No' and r.new='' and d.type=1 $party Group by o.order_id HAVING COUNT(d.item_id) >= '".$check_id['primary_values']."'");
								$QualiffersNum=mysql_num_rows($QualifiersRec);
								if($QualiffersNum > 0){
									$achive_value_qua=array();
									while ($QualiAchRec = mysql_fetch_array($QualifiersRec)){
									$achive_value_qua[]=$QualiAchRec['total_order'];
									} 
									if(array_sum($achive_value_qua)<$check_id['secondary_values']){
										$flag =false;
										}
									} else {
									$flag =false;
								}
								unset($achive_value_qua);
								unset($QualifiersRec);
							}
							//Qualifiers Number of Order
							if($check_id['type_id']==4){
								if($check_id['party_type_id']==1){
								$party="";
								}
								if($check_id['party_type_id']==2){
								$state=mysql_query("SELECT state_id FROM table_target_incentive_party WHERE target_incentive_Id='".$TIRec['target_incentive_id']."'");
								$state_list="";
								$i=0;
								while ($stateRec = mysql_fetch_array($state)){
									$i++;
									if($i>1){
									$state_list.=",";
									}
									$state_list.=$stateRec['state_id'];
								}
								$party=" and r.state IN ($state_list)";
								}
								unset($state_list);
								if($check_id['party_type_id']==3){
								$city=mysql_query("SELECT city_id FROM table_target_incentive_party WHERE target_incentive_Id='".$TIRec['target_incentive_id']."'");
								$city_list="";
								$i=0;
								while ($cityRec = mysql_fetch_array($city)){
									$i++;
									if($i>1){
									$city_list.=",";
									}
									$city_list.=$cityRec['city_id'];
								}
								$party=" and r.city IN ($city_list)";
								}
								unset($city_list);
								$QualifiersRec = mysql_query("SELECT Count(o.order_id) as total_order FROM table_order as o left join table_retailer as r on o.retailer_id=r.retailer_id WHERE o.salesman_id='".$TIRec['salesman_id']."' and (o.date_of_order BETWEEN '".$TIRec['from_date']."' AND '".$TIRec['to_date']."') and o.order_type = 'Yes' and r.new='' $party ");
								$QualiffersNum=mysql_num_rows($QualifiersRec);
								if($QualiffersNum > 0){
								$QualiAchRec = mysql_fetch_array($QualifiersRec);
									if($QualiAchRec['total_order']<$check_id['primary_values']){
										$flag =false;
										}
									} else {
									$flag =false;
								}
								unset($QualifiersRec);
							}
							//Qualifiers Order Value
							if($check_id['type_id']==5){
								if($check_id['party_type_id']==1){
								$party="";
								}
								if($check_id['party_type_id']==2){
								$state=mysql_query("SELECT state_id FROM table_target_incentive_party WHERE target_incentive_Id='".$TIRec['target_incentive_id']."'");
								$state_list="";
								$i=0;
								while ($stateRec = mysql_fetch_array($state)){
									$i++;
									if($i>1){
									$state_list.=",";
									}
									$state_list.=$stateRec['state_id'];
								}
								$party=" and r.state IN ($state_list)";
								}
								unset($state_list);
								if($check_id['party_type_id']==3){
								$city=mysql_query("SELECT city_id FROM table_target_incentive_party WHERE target_incentive_Id='".$TIRec['target_incentive_id']."'");
								$city_list="";
								$i=0;
								while ($cityRec = mysql_fetch_array($city)){
									$i++;
									if($i>1){
									$city_list.=",";
									}
									$city_list.=$cityRec['city_id'];
								}
								$party=" and r.city IN ($city_list)";
								}
								unset($city_list);
								$QualifiersRec = mysql_query("SELECT Count(o.order_id) as total_order FROM table_order as o left join table_retailer as r on o.retailer_id=r.retailer_id WHERE o.salesman_id='".$TIRec['salesman_id']."' and (o.date_of_order BETWEEN '".$TIRec['from_date']."' AND '".$TIRec['to_date']."') and o.order_type != 'No' and r.new='' $party and o.acc_total_invoice_amount>='".$check_id['primary_values']."'");
								$QualiffersNum=mysql_num_rows($QualifiersRec);
								if($QualiffersNum > 0){
								$QualiAchRec = mysql_fetch_array($QualifiersRec);
									if($QualiAchRec['total_order']<$check_id['secondary_values']){
										$flag =false;
										}
									} else {
									$flag =false;
								}
								unset($QualifiersRec);
							}
							//Qualifiers Scheme
							if($check_id['type_id']==6){		
								if($check_id['party_type_id']==1){
								$party="";
								}
								if($check_id['party_type_id']==2){
								$state=mysql_query("SELECT state_id FROM table_target_incentive_party WHERE target_incentive_Id='".$TIRec['target_incentive_id']."'");
								$state_list="";
								$i=0;
								while ($stateRec = mysql_fetch_array($state)){
									$i++;
									if($i>1){
									$state_list.=",";
									}
									$state_list.=$stateRec['state_id'];
								}
								$party=" and r.state IN ($state_list)";
								}
								unset($state_list);
								if($check_id['party_type_id']==3){
								$city=mysql_query("SELECT city_id FROM table_target_incentive_party WHERE target_incentive_Id='".$TIRec['target_incentive_id']."'");
								$city_list="";
								$i=0;
								while ($cityRec = mysql_fetch_array($city)){
									$i++;
									if($i>1){
									$city_list.=",";
									}
									$city_list.=$cityRec['city_id'];
								}
								$party=" and r.city IN ($city_list)";
								}
								unset($city_list);
								$Recqua = mysql_query("SELECT Count(distinct total.order_id) as total_order,sum(total.qty) as total_qty,sum(total.amt) as total_amt, Count(distinct total.retailer_id) as total_ret, total.dis, d.discount_desc, d.discount_type, d.discount_amount, f.free_qty from (SELECT o.order_id, o.acc_discount_id as dis,o.acc_free_item_qty as qty, o.acc_discount_amount as amt, o.salesman_id, o.retailer_id FROM table_order as o left join table_retailer as r on o.retailer_id=r.retailer_id WHERE o.order_type != 'No' and r.new='' and o.acc_discount_id!='' and (o.date_of_order BETWEEN '".$TIRec['from_date']."' AND '".$TIRec['to_date']."') AND o.salesman_id='".$TIRec['salesman_id']."' $party Group by o.order_id union all SELECT o.order_id, d.acc_discount_id as dis,d.acc_free_item_qty as qty, d.acc_discount_amount as amt, o.salesman_id, o.retailer_id FROM table_order as o inner join table_order_detail as d on o.order_id=d.order_id left join table_retailer as r on o.retailer_id=r.retailer_id WHERE  o.order_type != 'No' and r.new='' and d.acc_discount_id!='' and (o.date_of_order BETWEEN '".$TIRec['from_date']."' AND '".$TIRec['to_date']."') AND o.salesman_id='".$TIRec['salesman_id']."' $party union all SELECT o.order_id, c.acc_discount_id as dis, c.acc_free_item_qty as qty, c.acc_discount_amount as amt, o.salesman_id, o.retailer_id FROM table_order as o inner join table_order_combo_detail as c on c.order_id=o.order_id left join table_retailer as r on o.retailer_id=r.retailer_id WHERE c.acc_discount_id!='' and (o.date_of_order BETWEEN '".$TIRec['from_date']."' AND '".$TIRec['to_date']."') and o.order_type != 'No' and r.new='' AND o.salesman_id='".$TIRec['salesman_id']."' $party ) as total left join table_discount_detail as d on d.discount_id=total.dis left join table_foc_detail as f on f.foc_id=d.foc_id Group by total.dis ORDER BY total_order desc");
								$QualiffersNum=mysql_num_rows($Recqua);
								if($QualiffersNum > 0){
								$total_dis_qua=array();
									while ($auRecQuq = mysql_fetch_array($Recqua)){
										if($auRecQuq['discount_type']==1){
										$total_discount_qua=$auRecQuq['total_order'];
										}
										if($auRecQuq['discount_type']==2){
										$total_discount_qua=round($auRecQuq['total_amt']/$auRecQuq['discount_amount'],1);
										}
										if($auRecQuq['discount_type']==3){
										$total_discount_qua= round($auRecQuq['total_qty']/$auRecQuq['free_qty'],1);
										}
										$total_dis_qua[]=$total_discount_qua;
										unset($total_discount_qua);
									}
									$net_dis_qua=array_sum($total_dis_qua);
									if($net_dis_qua<$check_id['primary_values']){	
										$flag =false;
										}
								} else {
								$flag =false;
								}
								unset($total_dis_qua);
								unset($net_dis_qua);
								unset($Recqua);
							}
							
							//Qualifiers Total Calls
							if($check_id['type_id']==11){
								if($check_id['party_type_id']==1){
								$party="";
								}
								if($check_id['party_type_id']==2){
								$state=mysql_query("SELECT state_id FROM table_target_incentive_party WHERE target_incentive_Id='".$TIRec['target_incentive_id']."'");
								$state_list="";
								$i=0;
								while ($stateRec = mysql_fetch_array($state)){
									$i++;
									if($i>1){
									$state_list.=",";
									}
									$state_list.=$stateRec['state_id'];
								}
								$party=" and r.state IN ($state_list)";
								}
								unset($state_list);
								if($check_id['party_type_id']==3){
								$city=mysql_query("SELECT city_id FROM table_target_incentive_party WHERE target_incentive_Id='".$TIRec['target_incentive_id']."'");
								$city_list="";
								$i=0;
								while ($cityRec = mysql_fetch_array($city)){
									$i++;
									if($i>1){
									$city_list.=",";
									}
									$city_list.=$cityRec['city_id'];
								}
								$party=" and r.city IN ($city_list)";
								}
								unset($city_list);
								$TotalRecQua = mysql_query("SELECT Count(o.order_id) as total_order FROM table_order as o left join table_retailer as r on o.retailer_id=r.retailer_id WHERE o.salesman_id='".$TIRec['salesman_id']."' and (o.date_of_order BETWEEN '".$TIRec['from_date']."' AND '".$TIRec['to_date']."') and o.order_type != 'Adhoc' and r.new='' $party  ");
								$totalnum=mysql_num_rows($TotalRecQua);
								if($totalnum > 0){
									$TotalOrderRecQua = mysql_fetch_array($TotalRecQua);
									$TotalProRecQua = mysql_query("SELECT Count(o.order_id) as total_pro_order FROM table_order as o left join table_retailer as r on o.retailer_id=r.retailer_id WHERE o.salesman_id='".$TIRec['salesman_id']."' and (o.date_of_order BETWEEN '".$TIRec['from_date']."' AND '".$TIRec['to_date']."') and o.order_type = 'Yes' and r.new='' $party ");
									$TotalProOrderRecQua = mysql_fetch_array($TotalProRecQua);
									$per_call_qua=$check_id['primary_values']*$check_id['secondary_values']/100;
									if($TotalProOrderRecQua['total_pro_order'] >= $per_call_qua){
										if($TotalOrderRecQua['total_order']<$check_id['primary_values']){
												$flag =false;
											}
										} else {
										$flag =false;
										} 
								} else {
								$flag =false;
								}
								unset($TotalRecQua);
								unset($TotalProRecQua);
								unset($per_call_qua);
							}
							//Qualifiers Total Order Amount
							if($check_id['type_id']==12){
								if($check_id['party_type_id']==1){
								$party="";
								}
								if($check_id['party_type_id']==2){
								$state=mysql_query("SELECT state_id FROM table_target_incentive_party WHERE target_incentive_Id='".$TIRec['target_incentive_id']."'");
								$state_list="";
								$i=0;
								while ($stateRec = mysql_fetch_array($state)){
									$i++;
									if($i>1){
									$state_list.=",";
									}
									$state_list.=$stateRec['state_id'];
								}
								$party=" and r.state IN ($state_list)";
								}
								unset($state_list);
								if($check_id['party_type_id']==3){
								$city=mysql_query("SELECT city_id FROM table_target_incentive_party WHERE target_incentive_Id='".$TIRec['target_incentive_id']."'");
								$city_list="";
								$i=0;
								while ($cityRec = mysql_fetch_array($city)){
									$i++;
									if($i>1){
									$city_list.=",";
									}
									$city_list.=$cityRec['city_id'];
								}
								$party=" and r.city IN ($city_list)";
								}
								unset($city_list);
								$QualifiersRec = mysql_query("SELECT SUM(o.acc_total_invoice_amount) as total_order FROM table_order as o left join table_retailer as r on o.retailer_id=r.retailer_id WHERE o.salesman_id='".$TIRec['salesman_id']."' and (o.date_of_order BETWEEN '".$TIRec['from_date']."' AND '".$TIRec['to_date']."') and o.order_type != 'No' and r.new='' $party ");
								$QualiffersNum=mysql_num_rows($QualifiersRec);
								if($QualiffersNum > 0){
								$QualiAchRec = mysql_fetch_array($QualifiersRec);
									if($QualiAchRec['total_order']<$check_id['primary_values']){
										$flag =false;
										}
									} else {
									$flag =false;
								}
								unset($QualifiersRec);
							}
							//Qualifiers Focus Product of Retailer
							if($check_id['type_id']==9){
								if($check_id['party_type_id']==1){
								$party="";
								}
								if($check_id['party_type_id']==2){
								$state=mysql_query("SELECT state_id FROM table_target_incentive_party WHERE target_incentive_Id='".$TIRec['target_incentive_id']."'");
								$state_list="";
								$i=0;
								while ($stateRec = mysql_fetch_array($state)){
									$i++;
									if($i>1){
									$state_list.=",";
									}
									$state_list.=$stateRec['state_id'];
								}
								$party=" and r.state IN ($state_list)";
								}
								unset($state_list);
								if($check_id['party_type_id']==3){
								$city=mysql_query("SELECT city_id FROM table_target_incentive_party WHERE target_incentive_Id='".$TIRec['target_incentive_id']."'");
								$city_list="";
								$i=0;
								while ($cityRec = mysql_fetch_array($city)){
									$i++;
									if($i>1){
									$city_list.=",";
									}
									$city_list.=$cityRec['city_id'];
								}
								$party=" and r.city IN ($city_list)";
								}
								unset($city_list);
								$QualifiersRec = mysql_query("SELECT Count(distinct o.retailer_id) as total_ret FROM table_order as o left join table_order_detail as d on o.order_id=d.order_id left join table_item as i on d.item_id=i.item_id left join table_retailer as r on o.retailer_id=r.retailer_id left join table_target_incentive_type as t on i.item_id=t.ref_id WHERE o.salesman_id='".$TIRec['salesman_id']."' and (o.date_of_order BETWEEN '".$TIRec['from_date']."' AND '".$TIRec['to_date']."') and o.order_type != 'No' and r.new='' and d.type=1 $party and t.target_incentive_id='".$check_id['target_incentive_id']."' HAVING SUM(d.acc_quantity) >= '".$check_id['primary_values']."'");
								
								$QualiffersNum=mysql_num_rows($QualifiersRec);
								if($QualiffersNum > 0){
								$achive_ret_qua=array();
								while ($QualiAchRec = mysql_fetch_array($QualifiersRec)){
								$achive_ret_qua[]=$QualiAchRec['total_ret'];
								} 
									if(array_sum($achive_ret_qua)<$check_id['secondary_values']){
										$flag =false;
										}
									} else {
									$flag =false;
								}
								unset($achive_ret_qua);
								unset($QualifiersRec);
							}
							//Qualifiers Number of Visits Retailers
							if($check_id['type_id']==7){
								if($check_id['party_type_id']==1){
								$party="";
								}
								if($check_id['party_type_id']==2){
								$state=mysql_query("SELECT state_id FROM table_target_incentive_party WHERE target_incentive_id='".$TIRec['target_incentive_id']."'");
								$state_list="";
								$i=0;
								while ($stateRec = mysql_fetch_array($state)){
									$i++;
									if($i>1){
									$state_list.=",";
									}
									$state_list.=$stateRec['state_id'];
								}
								$party=" and r.state IN ($state_list)";
								}
								unset($state_list);
								if($check_id['party_type_id']==3){
								$city=mysql_query("SELECT city_id FROM table_target_incentive_party WHERE target_incentive_id='".$TIRec['target_incentive_id']."'");
								$city_list="";
								$i=0;
								while ($cityRec = mysql_fetch_array($city)){
									$i++;
									if($i>1){
									$city_list.=",";
									}
									$city_list.=$cityRec['city_id'];
								}
								$party=" and r.city IN ($city_list)";
								}
								unset($city_list);
								$RetOrdRec = mysql_query("SELECT Count(distinct o.retailer_id) as total_ret FROM table_order as o left join table_retailer as r on o.retailer_id=r.retailer_id WHERE o.salesman_id='".$TIRec['salesman_id']."' and (o.date_of_order BETWEEN '".$TIRec['from_date']."' AND '".$TIRec['to_date']."') and r.new='' and o.order_type!='Adhoc' $party ");
								$RetOrdnum=mysql_num_rows($RetOrdRec);
								if($RetOrdnum > 0){
								$QualiAchRec = mysql_fetch_array($RetOrdRec);
									if($QualiAchRec['total_ret']<$check_id['primary_values']){
										$flag =false;
										}
									} else {
									$flag =false;
								}
								unset($RetOrdRec);
							}
							//Qualifiers number of added retailers
							if($check_id['type_id']==10){
								if($check_id['party_type_id']==1){
								$party="";
								}
								if($check_id['party_type_id']==2){
								$state=mysql_query("SELECT state_id FROM table_target_incentive_party WHERE target_incentive_id='".$TIRec['target_incentive_id']."'");
								$state_list="";
								$i=0;
								while ($stateRec = mysql_fetch_array($state)){
									$i++;
									if($i>1){
									$state_list.=",";
									}
									$state_list.=$stateRec['state_id'];
								}
								$party=" and r.state IN ($state_list)";
								}
								unset($state_list);
								if($check_id['party_type_id']==3){
								$city=mysql_query("SELECT city_id FROM table_target_incentive_party WHERE target_incentive_id='".$TIRec['target_incentive_id']."'");
								$city_list="";
								$i=0;
								while ($cityRec = mysql_fetch_array($city)){
									$i++;
									if($i>1){
									$city_list.=",";
									}
									$city_list.=$cityRec['city_id'];
								}
								$party=" and r.city IN ($city_list)";
								}
								unset($city_list);
								$RetOrdRec = mysql_query("SELECT Count(r.retailer_id) as total_ret FROM table_retailer as r WHERE r.salesman_id='".$TIRec['salesman_id']."' and (r.start_date BETWEEN '".$TIRec['from_date']."' AND '".$TIRec['to_date']."') and r.new='' $party ");
								$RetOrdnum=mysql_num_rows($RetOrdRec);
								if($RetOrdnum > 0){
								$QualiAchRec = mysql_fetch_array($RetOrdRec);
									if($QualiAchRec['total_ret']<$check_id['primary_values']){
										$flag =false;
										}
									} else {
									$flag =false;
								}
							}
							unset($RetOrdRec);
						}
					}
					//End Check Qualifiers Target and Incentive
					//Category
					if($TIRec['type_id']==1){
						
						if($TIRec['party_type_id']==1){
						$party="";
						}
						if($TIRec['party_type_id']==2){
						$state=mysql_query("SELECT state_id FROM table_target_incentive_party WHERE target_incentive_Id='".$TIRec['target_incentive_id']."'");
						$state_list="";
						$i=0;
						while ($stateRec = mysql_fetch_array($state)){
							$i++;
							if($i>1){
							$state_list.=",";
							}
							$state_list.=$stateRec['state_id'];
						}
						$party=" and r.state IN ($state_list)";
						}
						unset($state_list);
						if($TIRec['party_type_id']==3){
						$city=mysql_query("SELECT city_id FROM table_target_incentive_party WHERE target_incentive_Id='".$TIRec['target_incentive_id']."'");
						$city_list="";
						$i=0;
						while ($cityRec = mysql_fetch_array($city)){
							$i++;
							if($i>1){
							$city_list.=",";
							}
							$city_list.=$cityRec['city_id'];
						}
						$party=" and r.city IN ($city_list)";
						}
						unset($city_list);
						$orderRec = mysql_query("SELECT SUM(d.acc_quantity) as total_qty, SUM(d.acc_total) as total_value FROM table_order as o left join table_order_detail as d on o.order_id=d.order_id left join table_item as i on d.item_id=i.item_id left join table_retailer as r on o.retailer_id=r.retailer_id inner join table_target_incentive_type as t on i.category_id=t.ref_id WHERE o.salesman_id='".$TIRec['salesman_id']."' and (o.date_of_order BETWEEN '".$TIRec['from_date']."' AND '".$TIRec['to_date']."') and o.order_type != 'No' and r.new='' and d.type=1 $party and t.target_incentive_id='".$TIRec['target_incentive_id']."' ");
						$ordernum=mysql_num_rows($orderRec);
						if($ordernum > 0){
							if($flag){
								$achievedRec = mysql_fetch_array($orderRec);
								if($achievedRec['total_qty']>=1){
									//echo $TIRec['incentive_reward_type'];
									$achieved_value=$achievedRec['total_qty'];
									$price_val=$achievedRec['total_value'];
									if($TIRec['incentive_reward_type']==2)
									{
									$val=$achievedRec['total_qty'];
									//echo $price_val=$achievedRec['total_value'];
									$per_of_achval=$val*100/$TIRec['primary_values'];
									$criteria=mysql_query("SELECT percentage_of_reward_amount FROM table_target_incentive_slab WHERE target_incentive_Id='".$TIRec['target_incentive_id']."' AND percentage_of_criteria <='".$per_of_achval."' Order By percentage_of_criteria desc LIMIT 1");
										$criterianum=mysql_num_rows($criteria);
										if($criterianum > 0){
										$criteriaRec = mysql_fetch_array($criteria);
										$reward_amount=number_format((float)$price_val*$criteriaRec['percentage_of_reward_amount']/100,2,'.','');
										
										} else {
										$reward_amount="NA";
										}
									}
								}
							}
						}
						unset($orderRec);
					}
					//Items
					if($TIRec['type_id']==2){
						
						if($TIRec['party_type_id']==1){
						$party="";
						}
						if($TIRec['party_type_id']==2){
						$state=mysql_query("SELECT state_id FROM table_target_incentive_party WHERE target_incentive_Id='".$TIRec['target_incentive_id']."'");
						$state_list="";
						$i=0;
						while ($stateRec = mysql_fetch_array($state)){
							$i++;
							if($i>1){
							$state_list.=",";
							}
							$state_list.=$stateRec['state_id'];
						}
						$party=" and r.state IN ($state_list)";
						}
						unset($state_list);
						if($TIRec['party_type_id']==3){
						$city=mysql_query("SELECT city_id FROM table_target_incentive_party WHERE target_incentive_Id='".$TIRec['target_incentive_id']."'");
						$city_list="";
						$i=0;
						while ($cityRec = mysql_fetch_array($city)){
							$i++;
							if($i>1){
							$city_list.=",";
							}
							$city_list.=$cityRec['city_id'];
						}
						$party=" and r.city IN ($city_list)";
						}
						unset($city_list);
						$orderRec = mysql_query("SELECT SUM(d.acc_quantity) as total_qty FROM table_order as o left join table_order_detail as d on o.order_id=d.order_id left join table_item as i on d.item_id=i.item_id left join table_retailer as r on o.retailer_id=r.retailer_id inner join table_target_incentive_type as t on i.item_id=t.ref_id WHERE o.salesman_id='".$TIRec['salesman_id']."' and (o.date_of_order BETWEEN '".$TIRec['from_date']."' AND '".$TIRec['to_date']."') and o.order_type != 'No' and r.new='' and d.type=1 $party and t.target_incentive_id='".$TIRec['target_incentive_id']."'");
						$ordernum=mysql_num_rows($orderRec);
						if($ordernum > 0){
							if($flag){
								$achievedRec = mysql_fetch_array($orderRec);
								if($achievedRec['total_qty']>=1){
									$achieved_value=$achievedRec['total_qty'];
								}
							}
						}
						unset($orderRec);
					}
					//Items Per Order
					if($TIRec['type_id']==3){
						if($TIRec['party_type_id']==1){
						$party="";
						}
						if($TIRec['party_type_id']==2){
						$state=mysql_query("SELECT state_id FROM table_target_incentive_party WHERE target_incentive_Id='".$TIRec['target_incentive_id']."'");
						$state_list="";
						$i=0;
						while ($stateRec = mysql_fetch_array($state)){
							$i++;
							if($i>1){
							$state_list.=",";
							}
							$state_list.=$stateRec['state_id'];
						}
						$party=" and r.state IN ($state_list)";
						}
						unset($state_list);
						if($TIRec['party_type_id']==3){
						$city=mysql_query("SELECT city_id FROM table_target_incentive_party WHERE target_incentive_Id='".$TIRec['target_incentive_id']."'");
						$city_list="";
						$i=0;
						while ($cityRec = mysql_fetch_array($city)){
							$i++;
							if($i>1){
							$city_list.=",";
							}
							$city_list.=$cityRec['city_id'];
						}
						$party=" and r.city IN ($city_list)";
						}
						unset($city_list);
						$orderRec = mysql_query("SELECT Count(distinct o.order_id) as total_order FROM table_order as o left join table_order_detail as d on o.order_id=d.order_id left join table_retailer as r on o.retailer_id=r.retailer_id WHERE o.salesman_id='".$TIRec['salesman_id']."' and (o.date_of_order BETWEEN '".$TIRec['from_date']."' AND '".$TIRec['to_date']."') and o.order_type != 'No' and r.new='' and d.type=1 $party Group by o.order_id HAVING COUNT(d.item_id) >= '".$TIRec['primary_values']."'");
						$ordernum=mysql_num_rows($orderRec);
						if($ordernum > 0){
							if($flag){
								$achive_value=array();
								while ($achievedRec = mysql_fetch_array($orderRec)){
								$achive_value[]=$achievedRec['total_order'];
								} 
								if(array_sum($achive_value)>=1){
								
								$achieved_value=array_sum($achive_value);
								unset($achive_value);
								}
							}
						}
						unset($orderRec);
					}
					//Number of Order
					if($TIRec['type_id']==4){
						if($TIRec['party_type_id']==1){
						$party="";
						}
						if($TIRec['party_type_id']==2){
						$state=mysql_query("SELECT state_id FROM table_target_incentive_party WHERE target_incentive_Id='".$TIRec['target_incentive_id']."'");
						$state_list="";
						$i=0;
						while ($stateRec = mysql_fetch_array($state)){
							$i++;
							if($i>1){
							$state_list.=",";
							}
							$state_list.=$stateRec['state_id'];
						}
						$party=" and r.state IN ($state_list)";
						}
						unset($state_list);
						if($TIRec['party_type_id']==3){
						$city=mysql_query("SELECT city_id FROM table_target_incentive_party WHERE target_incentive_Id='".$TIRec['target_incentive_id']."'");
						$city_list="";
						$i=0;
						while ($cityRec = mysql_fetch_array($city)){
							$i++;
							if($i>1){
							$city_list.=",";
							}
							$city_list.=$cityRec['city_id'];
						}
						$party=" and r.city IN ($city_list)";
						}
						unset($city_list);
						$orderRec = mysql_query("SELECT Count(o.order_id) as total_order FROM table_order as o left join table_retailer as r on o.retailer_id=r.retailer_id WHERE o.salesman_id='".$TIRec['salesman_id']."' and (o.date_of_order BETWEEN '".$TIRec['from_date']."' AND '".$TIRec['to_date']."') and o.order_type != 'No' and r.new='' $party ");
						$ordernum=mysql_num_rows($orderRec);
						if($ordernum > 0){
							if($flag){
								$achievedRec = mysql_fetch_array($orderRec);
								if($achievedRec['total_order']>=1){
									$achieved_value=$achievedRec['total_order'];
								}
							}
						}
						unset($orderRec);
					}
					//Order Value
					if($TIRec['type_id']==5){
						if($TIRec['party_type_id']==1){
						$party="";
						}
						if($TIRec['party_type_id']==2){
						$state=mysql_query("SELECT state_id FROM table_target_incentive_party WHERE target_incentive_Id='".$TIRec['target_incentive_id']."'");
						$state_list="";
						$i=0;
						while ($stateRec = mysql_fetch_array($state)){
							$i++;
							if($i>1){
							$state_list.=",";
							}
							$state_list.=$stateRec['state_id'];
						}
						$party=" and r.state IN ($state_list)";
						}
						unset($state_list);
						if($TIRec['party_type_id']==3){
						$city=mysql_query("SELECT city_id FROM table_target_incentive_party WHERE target_incentive_Id='".$TIRec['target_incentive_id']."'");
						$city_list="";
						$i=0;
						while ($cityRec = mysql_fetch_array($city)){
							$i++;
							if($i>1){
							$city_list.=",";
							}
							$city_list.=$cityRec['city_id'];
						}
						$party=" and r.city IN ($city_list)";
						}
						unset($city_list);
						$orderRec = mysql_query("SELECT Count(o.order_id) as total_order FROM table_order as o left join table_retailer as r on o.retailer_id=r.retailer_id WHERE o.salesman_id='".$TIRec['salesman_id']."' and (o.date_of_order BETWEEN '".$TIRec['from_date']."' AND '".$TIRec['to_date']."') and o.order_type = 'Yes' and r.new='' $party and o.acc_total_invoice_amount>='".$TIRec['primary_values']."'");
						$ordernum=mysql_num_rows($orderRec);
						if($ordernum > 0){
							if($flag){
								$achievedRec = mysql_fetch_array($orderRec);
								if($achievedRec['total_order']>=1){
									$achieved_value=$achievedRec['total_order'];
								}
							}
						}
						unset($orderRec);
					}
					//Scheme
					if($TIRec['type_id']==6){		
						if($TIRec['party_type_id']==1){
						$party="";
						}
						if($TIRec['party_type_id']==2){
						$state=mysql_query("SELECT state_id FROM table_target_incentive_party WHERE target_incentive_Id='".$TIRec['target_incentive_id']."'");
						$state_list="";
						$i=0;
						while ($stateRec = mysql_fetch_array($state)){
							$i++;
							if($i>1){
							$state_list.=",";
							}
							$state_list.=$stateRec['state_id'];
						}
						$party=" and r.state IN ($state_list)";
						}
						unset($state_list);
						if($TIRec['party_type_id']==3){
						$city=mysql_query("SELECT city_id FROM table_target_incentive_party WHERE target_incentive_Id='".$TIRec['target_incentive_id']."'");
						$city_list="";
						$i=0;
						while ($cityRec = mysql_fetch_array($city)){
							$i++;
							if($i>1){
							$city_list.=",";
							}
							$city_list.=$cityRec['city_id'];
						}
						$party=" and r.city IN ($city_list)";
						}
						unset($city_list);
						$Rec = mysql_query("SELECT Count(distinct total.order_id) as total_order,sum(total.qty) as total_qty,sum(total.amt) as total_amt, Count(distinct total.retailer_id) as total_ret, total.dis, d.discount_desc, d.discount_type, d.discount_amount, f.free_qty from (SELECT o.order_id, o.acc_discount_id as dis,o.acc_free_item_qty as qty, o.acc_discount_amount as amt, o.salesman_id, o.retailer_id FROM table_order as o left join table_retailer as r on o.retailer_id=r.retailer_id WHERE o.order_type != 'No' and r.new='' and o.acc_discount_id!='' and (o.date_of_order BETWEEN '".$TIRec['from_date']."' AND '".$TIRec['to_date']."') AND o.salesman_id='".$TIRec['salesman_id']."' $party Group by o.order_id union all SELECT o.order_id, d.acc_discount_id as dis,d.acc_free_item_qty as qty, d.acc_discount_amount as amt, o.salesman_id, o.retailer_id FROM table_order as o inner join table_order_detail as d on o.order_id=d.order_id left join table_retailer as r on o.retailer_id=r.retailer_id WHERE  o.order_type != 'No' and r.new='' and d.acc_discount_id!='' and (o.date_of_order BETWEEN '".$TIRec['from_date']."' AND '".$TIRec['to_date']."') AND o.salesman_id='".$TIRec['salesman_id']."' $party union all SELECT o.order_id, c.acc_discount_id as dis, c.acc_free_item_qty as qty, c.acc_discount_amount as amt, o.salesman_id, o.retailer_id FROM table_order as o inner join table_order_combo_detail as c on c.order_id=o.order_id left join table_retailer as r on o.retailer_id=r.retailer_id WHERE c.acc_discount_id!='' and (o.date_of_order BETWEEN '".$TIRec['from_date']."' AND '".$TIRec['to_date']."') and o.order_type != 'No' and r.new='' AND o.salesman_id='".$TIRec['salesman_id']."' $party ) as total left join table_discount_detail as d on d.discount_id=total.dis left join table_foc_detail as f on f.foc_id=d.foc_id Group by total.dis ORDER BY total_order desc");
						$ordernum=mysql_num_rows($Rec);
						if($ordernum > 0){
							
							if($flag){
								$total_dis=array();
								while ($QuaRec = mysql_fetch_array($Rec)){
								//$achievedRec = mysql_fetch_array($orderRec);
									if($QuaRec['discount_type']==1){
									$total_discount=$QuaRec['total_order'];
									}
									if($QuaRec['discount_type']==2){
									$total_discount=round($QuaRec['total_amt']/$QuaRec['discount_amount'],1);
									}
									if($QuaRec['discount_type']==3){
									$total_discount= round($QuaRec['total_qty']/$QuaRec['free_qty'],1);
									}
									$total_dis[]=$total_discount;
									unset($total_discount);
								}
								$net_dis=array_sum($total_dis);
								if($net_dis>=1){
									$achieved_value=$net_dis;
								unset($total_dis);
								unset($Rec);
								unset($net_dis);
								}
							}
						}
						
					}
					//Total Calls
					if($TIRec['type_id']==11){
						if($TIRec['party_type_id']==1){
						$party="";
						}
						if($TIRec['party_type_id']==2){
						$state=mysql_query("SELECT state_id FROM table_target_incentive_party WHERE target_incentive_Id='".$TIRec['target_incentive_id']."'");
						$state_list="";
						$i=0;
						while ($stateRec = mysql_fetch_array($state)){
							$i++;
							if($i>1){
							$state_list.=",";
							}
							$state_list.=$stateRec['state_id'];
						}
						$party=" and r.state IN ($state_list)";
						}
						unset($state_list);
						if($TIRec['party_type_id']==3){
						$city=mysql_query("SELECT city_id FROM table_target_incentive_party WHERE target_incentive_Id='".$TIRec['target_incentive_id']."'");
						$city_list="";
						$i=0;
						while ($cityRec = mysql_fetch_array($city)){
							$i++;
							if($i>1){
							$city_list.=",";
							}
							$city_list.=$cityRec['city_id'];
						}
						$party=" and r.city IN ($city_list)";
						}
						unset($city_list);
						$TotalRec = mysql_query("SELECT Count(o.order_id) as total_order FROM table_order as o left join table_retailer as r on o.retailer_id=r.retailer_id WHERE o.salesman_id='".$TIRec['salesman_id']."' and (o.date_of_order BETWEEN '".$TIRec['from_date']."' AND '".$TIRec['to_date']."') and o.order_type != 'Adhoc' and r.new='' $party  ");
						$totalnum=mysql_num_rows($TotalRec);
						if($flag){
							if($totalnum > 0){
								$TotalOrderRec = mysql_fetch_array($TotalRec);
								$TotalProRec = mysql_query("SELECT Count(o.order_id) as total_pro_order FROM table_order as o left join table_retailer as r on o.retailer_id=r.retailer_id WHERE o.salesman_id='".$TIRec['salesman_id']."' and (o.date_of_order BETWEEN '".$TIRec['from_date']."' AND '".$TIRec['to_date']."') and o.order_type = 'Yes' and r.new='' $party ");
								$TotalProOrderRec = mysql_fetch_array($TotalProRec);
								$per_call=$TIRec['secondary_values']*$TIRec['primary_values']/100;
								if($TotalProOrderRec['total_pro_order'] >= $per_call)
								{
									$achieved_value=$TotalOrderRec['total_order'];
								unset($TotalRec);
								unset($per_call);
								}
								
							}
							
						}
					}
					//Total Order Amount
					if($TIRec['type_id']==12){
						if($TIRec['party_type_id']==1){
						$party="";
						}
						if($TIRec['party_type_id']==2){
						$state=mysql_query("SELECT state_id FROM table_target_incentive_party WHERE target_incentive_Id='".$TIRec['target_incentive_id']."'");
						$state_list="";
						$i=0;
						while ($stateRec = mysql_fetch_array($state)){
							$i++;
							if($i>1){
							$state_list.=",";
							}
							$state_list.=$stateRec['state_id'];
						}
						$party=" and r.state IN ($state_list)";
						}
						unset($state_list);
						if($TIRec['party_type_id']==3){
						$city=mysql_query("SELECT city_id FROM table_target_incentive_party WHERE target_incentive_Id='".$TIRec['target_incentive_id']."'");
						$city_list="";
						$i=0;
						while ($cityRec = mysql_fetch_array($city)){
							$i++;
							if($i>1){
							$city_list.=",";
							}
							$city_list.=$cityRec['city_id'];
						}
						$party=" and r.city IN ($city_list)";
						}
						unset($city_list);
						$orderRec = mysql_query("SELECT SUM(o.acc_total_invoice_amount) as total_order FROM table_order as o left join table_retailer as r on o.retailer_id=r.retailer_id WHERE o.salesman_id='".$TIRec['salesman_id']."' and (o.date_of_order BETWEEN '".$TIRec['from_date']."' AND '".$TIRec['to_date']."') and o.order_type != 'No' and r.new='' $party ");
						$ordernum=mysql_num_rows($orderRec);
						if($ordernum > 0){
							if($flag){
								$achievedRec = mysql_fetch_array($orderRec);
								if($achievedRec['total_order']>=1){
									$achieved_value=$achievedRec['total_order'];
								}
							}
						}
						unset($orderRec);
					}
					//Focus Product of Retailer
					if($TIRec['type_id']==9){
						if($TIRec['party_type_id']==1){
						$party="";
						}
						if($TIRec['party_type_id']==2){
						$state=mysql_query("SELECT state_id FROM table_target_incentive_party WHERE target_incentive_Id='".$TIRec['target_incentive_id']."'");
						$state_list="";
						$i=0;
						while ($stateRec = mysql_fetch_array($state)){
							$i++;
							if($i>1){
							$state_list.=",";
							}
							$state_list.=$stateRec['state_id'];
						}
						$party=" and r.state IN ($state_list)";
						}
						unset($state_list);
						if($TIRec['party_type_id']==3){
						$city=mysql_query("SELECT city_id FROM table_target_incentive_party WHERE target_incentive_Id='".$TIRec['target_incentive_id']."'");
						$city_list="";
						$i=0;
						while ($cityRec = mysql_fetch_array($city)){
							$i++;
							if($i>1){
							$city_list.=",";
							}
							$city_list.=$cityRec['city_id'];
						}
						$party=" and r.city IN ($city_list)";
						}
						unset($city_list);
						$orderRec = mysql_query("SELECT Count(distinct o.retailer_id) as total_ret FROM table_order as o left join table_order_detail as d on o.order_id=d.order_id left join table_item as i on d.item_id=i.item_id left join table_retailer as r on o.retailer_id=r.retailer_id left join table_target_incentive_type as t on i.item_id=t.ref_id WHERE o.salesman_id='".$TIRec['salesman_id']."' and (o.date_of_order BETWEEN '".$TIRec['from_date']."' AND '".$TIRec['to_date']."') and o.order_type != 'No' and r.new='' and d.type=1 $party and t.target_incentive_id='".$TIRec['target_incentive_id']."' HAVING SUM(d.acc_quantity) >= '".$TIRec['primary_values']."'");
						$ordernum=mysql_num_rows($orderRec);
						if($ordernum > 0){
							if($flag){
								$achive_ret=array();
								while ($achievedRec = mysql_fetch_array($orderRec)){
								$achive_ret[]=$achievedRec['total_ret'];
								} 
								if($achive_ret>=1){
									$achieved_value=array_sum($achive_ret);
								}
							}
						}
						unset($orderRec);
						unset($achive_ret);
					}
					//Number of Visits Retailers
					if($TIRec['type_id']==7){
						if($TIRec['party_type_id']==1){
						$party="";
						}
						if($TIRec['party_type_id']==2){
						$state=mysql_query("SELECT state_id FROM table_target_incentive_party WHERE target_incentive_id='".$TIRec['target_incentive_id']."'");
						$state_list="";
						$i=0;
						while ($stateRec = mysql_fetch_array($state)){
							$i++;
							if($i>1){
							$state_list.=",";
							}
							$state_list.=$stateRec['state_id'];
						}
						$party=" and r.state IN ($state_list)";
						}
						unset($state_list);
						if($TIRec['party_type_id']==3){
						$city=mysql_query("SELECT city_id FROM table_target_incentive_party WHERE target_incentive_id='".$TIRec['target_incentive_id']."'");
						$city_list="";
						$i=0;
						while ($cityRec = mysql_fetch_array($city)){
							$i++;
							if($i>1){
							$city_list.=",";
							}
							$city_list.=$cityRec['city_id'];
						}
						$party=" and r.city IN ($city_list)";
						}
						unset($city_list);
						$RetOrdRec = mysql_query("SELECT Count(distinct o.retailer_id) as total_ret FROM table_order as o left join table_retailer as r on o.retailer_id=r.retailer_id WHERE o.salesman_id='".$TIRec['salesman_id']."' and (o.date_of_order BETWEEN '".$TIRec['from_date']."' AND '".$TIRec['to_date']."') and r.new='' and o.order_type!='Adhoc' $party ");
						$RetOrdnum=mysql_num_rows($RetOrdRec);
						if($RetOrdnum > 0){
							if($flag){
								$achievedRec = mysql_fetch_array($RetOrdRec);
								if($achievedRec['total_ret']>=1){
									$achieved_value=$achievedRec['total_ret'];
								}
							}
						}
						unset($RetOrdRec);
					}
					//number of added retailers
					if($TIRec['type_id']==10){
						if($TIRec['party_type_id']==1){
						$party="";
						}
						if($TIRec['party_type_id']==2){
						$state=mysql_query("SELECT state_id FROM table_target_incentive_party WHERE target_incentive_id='".$TIRec['target_incentive_id']."'");
						$state_list="";
						$i=0;
						while ($stateRec = mysql_fetch_array($state)){
							$i++;
							if($i>1){
							$state_list.=",";
							}
							$state_list.=$stateRec['state_id'];
						}
						$party=" and r.state IN ($state_list)";
						}
						unset($state_list);
						if($TIRec['party_type_id']==3){
						$city=mysql_query("SELECT city_id FROM table_target_incentive_party WHERE target_incentive_id='".$TIRec['target_incentive_id']."'");
						$city_list="";
						$i=0;
						while ($cityRec = mysql_fetch_array($city)){
							$i++;
							if($i>1){
							$city_list.=",";
							}
							$city_list.=$cityRec['city_id'];
						}
						$party=" and r.city IN ($city_list)";
						}
						unset($city_list);
						$RetOrdRec = mysql_query("SELECT Count(r.retailer_id) as total_ret FROM table_retailer as r WHERE r.salesman_id='".$TIRec['salesman_id']."' and (r.start_date BETWEEN '".$TIRec['from_date']."' AND '".$TIRec['to_date']."') and r.new='' $party ");
						$RetOrdnum=mysql_num_rows($RetOrdRec);
						if($RetOrdnum > 0){
							if($flag){
								$achievedRec = mysql_fetch_array($RetOrdRec);
								if($achievedRec['total_ret']>=1){
									$achieved_value=$achievedRec['total_ret'];
								}
							}
						}
						unset($RetOrdRec);
					}
					
					
					?>
					<td style="padding:10px;" align="center">
					<?php 
					if ($achieved_value!='' && $achieved_value_quy=='' ){
						echo $achieved_value; 
					} else if($achieved_value=='' && $achieved_value_quy!=''){
						echo $achieved_value_quy; 
					}
					else{
					echo "NA";
					}
					?> 
					</td>
					<td style="padding:10px;" align="center"><?php echo $price_val; ?></td>
					<td style="padding:10px;" align="center">
					<?php 
					if($TIRec['incentive_reward_type']==2)
					{
						if($reward_amount!='' || $reward_amount='NA'){ echo $reward_amount;} else{ echo "-";}
					
					} else {
					echo "NA"; 
					}
					?>
					</td>
					<?php 
					unset($price_val); 
					unset($achieved_value); 
					unset($reward_amount); 
					?>
				</tr>
			<?php } } 
			unset($auRec);
			} ?> 
			<?php
			if($report==''){
			?>
			<tr  style="border-bottom:2px solid #6E6E6E;" align="center" >
				<td style="padding:10px;" colspan="8">Report Not Available</td>
			</tr>
			<?php }  ?>
		</table>
		</div>
		</td>
	</tr>
	<tr>
		<td><img src="images/blank.gif" width="695" height="1" alt="blank" /></td>
		<td></td>
	</tr>
	</table>
<div class="clear"></div>
</div>
<!--  end content-table-inner  -->
</td>
<td id="tbl-border-right"></td>
</tr>
<tr>
	<th class="sized bottomleft"></th>
	<td id="tbl-border-bottom">&nbsp;</td>
	<th class="sized bottomright"></th>
</tr>
</table>
<div class="clear">&nbsp;</div>
</div>
<!--  end content -->
<div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->
<div class="clear">&nbsp;</div> 
<!-- start footer -->         
<?php include("footer.php") ?>
<?php unset($_SESSION['SalAttList']); ?>
<!-- end footer -->
<script src="javascripts/jquery-1.8.2.js" type="text/javascript"></script>
<script src="javascripts/jquery-ui.js"></script>
<script type="text/javascript" src="javascripts/validate.js"></script>
<script type="text/javascript">
// Popup window code
function newPopup(url) {
	popupWindow = window.open(
		url,'popUpWindow','height=500,width=600,left=200,top=100,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no,status=yes')
}
</script>
<script>
    $(function() {
        $( "#from" ).datepicker({
			dateFormat: "d M yy",
            defaultDate: "w",
            changeMonth: true,
			changeYear: true,
			yearRange: '2010:2020',
            numberOfMonths: 1,
            onSelect: function( selectedDate ) {
                $( "#to" ).datepicker( "option", "minDate", selectedDate );
            }
        });
        $( "#to" ).datepicker({
			dateFormat: "d M yy",
            defaultDate: "-w",
            changeMonth: true,
			changeYear: true,
			yearRange: '2010:2020',
            numberOfMonths: 1,
            onSelect: function( selectedDate ) {
                $( "#from" ).datepicker( "option", "maxDate", selectedDate );
            }
        });
    });
</script>
<script type='text/javascript'>//<![CDATA[ 
var tableToExcel = (function () {
        var uri = 'data:application/vnd.ms-excel;base64,'
        , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table><tr><td><b>Salesman Name:</b> <?php echo $sal_name; ?></td><td><b>Report Date:</b> <?php echo $from_date; ?></td></tr></table><table>{table}</table></body></html>'
        , base64 = function (s) { return window.btoa(unescape(encodeURIComponent(s))) }
        , format = function (s, c) { return s.replace(/{(\w+)}/g, function (m, p) { return c[p]; }) }
        return function (table, name, filename) {
            if (!table.nodeType) table = document.getElementById(table)
            var ctx = { worksheet: name || 'Worksheet', table: table.innerHTML }

            document.getElementById("dlink").href = uri + base64(format(template, ctx));
            document.getElementById("dlink").download = filename;
            document.getElementById("dlink").click();

        }
    })()

//]]>  
</script>
</body>
</html>