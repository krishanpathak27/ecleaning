<?php
	CheckUser();
	$ARR_MONTHS = array('01'=>'Jan','02'=>'Feb','03'=>'Mar','04'=>'Apr','05'=>'May','06'=>'Jun','07'=>'Jul','08'=>'Aug','09'=>'Sep','10'=>'Oct','11'=>'Nov','12'=>'Dec');
	
	function checkFromdate($sessionDate) { 
	  if(isset($sessionDate) && $sessionDate!='') 
	   return date('Y', strtotime($sessionDate)); 
	  else 
	   return date('Y');
	 }		
	 

 /* 
 Description: getStatus used globally for returning  Active and Inactive.
 date: 20 May 2015
 By: Nizam

*/
 
  function getStatus($status)
       {
       		$Aselected = "";
	        $Iselected = "";
	 
	 if(isset($status)){
		if($status == 'A'){ $Aselected = "selected"; } else{ $Aselected = ""; }
		if($status == 'I'){ $Iselected = "selected"; } else{ $Iselected = ""; }
	  }
		return	 "<option value='A' $Aselected >Active</option>
				  <option value='I' $Iselected >Inactive</option>";				
	}

	function getBreadCrum($pageName,$parentPageId){

		static $bread_result=array();
		$con=mysqli_connect(MYSQL_HOST_NAME,MYSQL_USER_NAME,MYSQL_PASSWORD,MYSQL_DB_NAME);
		if(!$con){
			return $bread_result;
		}
		if($parentPageId==-1){

			$result=mysqli_query($con,"select parentPage,page_alias_name from table_cms where page_name="."'".$pageName."'");
			//print_r($result);die;
			if(mysqli_num_rows($result) > 0){
				$row = mysqli_fetch_assoc($result);
				
				$pageName=$row['page_alias_name'];
				$parentPageId=$row['parentPage'];
				
				array_push($bread_result,ucwords($pageName));

			}else{
				return($bread_result);
			}
		}else{
			$result=mysqli_query($con,"select parentPage,page_alias_name from table_cms where id=".$parentPageId);

			if(mysqli_num_rows($result) > 0){
				$row = mysqli_fetch_assoc($result);
			//	print_r($row);die;
				$pageName=$row['page_alias_name'];
				$parentPageId=$row['parentPage'];
				array_push($bread_result,ucwords($pageName));

			}else{

				return($bread_result);
			}

		}
		if($parentPageId!=0){
			getBreadCrum($pageName,$parentPageId);

		}
		//$bread_result=array_reverse($bread_result);
		return $bread_result;
		


	}


?>
