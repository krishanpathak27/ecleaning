<?php
$optionsPos_name = "users_type";

$optionsUserType[1] = "Account";
$optionsUserType[2] = "Company User";
$optionsUserType[3] = "Distributor";
$optionsUserType[4] = "Retailer";
$optionsUserType[5] = "Salesman";
$optionsUserType[6] = "Quality Auditor";
$optionsUserType[7] = "Warehouse";
$optionsUserType[8] = "Service Personnel";
$optionsUserType[9] = "Service Distributor";
$optionsUserType[10] = "Service Distributor Service Personnel";

?>