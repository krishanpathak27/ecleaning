<?php
include("includes/config.inc.php");
include("includes/function.php");
$_objAdmin = new Admin();
?>

<?php
$data = $_objAdmin->getAdminDashboardData();
?>



<?php include("header.inc.php") ?>
<?php //error_reporting(E_ALL) ?>
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <!-- BEGIN: Subheader -->
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title ">
                    Dashboard
                </h3>
            </div>

        </div>
    </div>
    <!-- END: Subheader -->
    <div class="m-content">
        <!--begin::Form-->
        <div class="m-portlet"> 
            <form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" method="post" action="index.php" enctype="multipart/form-data" id="filterForm">
                <div class="m-portlet__body">

                    <div class="form-group m-form__group row">
                        <div class="col-lg-2">													 
                            <label>
                                Date Picker
                            </label> 													 
                        </div>
                        <div class="col-lg-6" style="padding-bottom: 10px">
                            <div class="m-input-icon m-input-icon--right">
                                <div class="input-daterange input-group" id="m_datepicker_5">
                                    <input  id="from" type="text" class="form-control m-input" name="from" placeholder="From"> 
                                    <span class="input-group-addon">
                                        <i class="la la-ellipsis-h"></i>
                                    </span>
                                    <input id="to" type="text" class="form-control" name="to" placeholder="To">
                                </div>

                            </div>
                        </div>
                        <div class="col-lg-4 text-right"> 
                            <div class="m-input-icon m-input-icon--right">
                                <button type="reset" class="btn btn-success" onclick="dataChange();">
                                    Submit
                                </button>
                                <button type="reset" class="btn btn-secondary" onclick="dataChange();">
                                    Reset
                                </button>
                            </div> 
                        </div>
                    </div> 
                </div>

            </form>
        </div>
        <!--end::Form-->

        <!--begin:: Widgets/Stats-->

        
        <div class="m-portlet ">
            <div class="m-portlet__body  m-portlet__body--no-padding">
                <div style="height:500px;" class="row m-row--no-padding m-row--col-separator-xl" id="m_gchart_1" >

                </div>
            </div>
        </div>

        <div class="m-portlet ">
            <div class="m-portlet__body  m-portlet__body--no-padding">
                <div style="height:500px;" class="row m-row--no-padding m-row--col-separator-xl" id="curve_chart" >

                </div>
            </div>
        </div>
        <div class="m-portlet ">
            <div class="m-portlet__body  m-portlet__body--no-padding">
                <div class="row m-row--no-padding m-row--col-separator-xl">
                    <div class="col-md-12 col-lg-6 col-xl-3">
                        <!--begin::Total Profit-->
                        <div class="m-widget24">
                            <div class="m-widget24__item">
                                <h4 class="m-widget24__title">
                                    Total Bookings
                                </h4> <br> 
                                <span id="bookings" class="m-widget24__stats m--font-brand">
                                    <?php echo $data['bookings']; ?>
                                </span> 
                                <div class="">
                                    &nbsp;
                                </div> 
                                <span class="m-widget24__number">
                                    <a href="booking.php">View Details</a>
                                </span>
                            </div>
                        </div>
                        <!--end::Total Profit-->
                    </div>
                    <div class="col-md-12 col-lg-6 col-xl-3">
                        <!--begin::New Feedbacks-->
                        <div class="m-widget24">
                            <div class="m-widget24__item">
                                <h4 class="m-widget24__title">
                                    Completed Bookings
                                </h4> <br> 
                                <span id="completeBookings" class="m-widget24__stats m--font-info">
                                    <?php echo $data['completeBookings']; ?>
                                </span> 
                                <div class="">
                                    &nbsp;
                                </div>  
                                <span class="m-widget24__number">
                                    <a href="booking.php">View Details</a>
                                </span>
                            </div>
                        </div>
                        <!--end::New Feedbacks-->
                    </div>
                    <div class="col-md-12 col-lg-6 col-xl-3">
                        <!--begin::New Orders-->
                        <div class="m-widget24">
                            <div class="m-widget24__item">
                                <h4 class="m-widget24__title">
                                    Cancelled Booking
                                </h4> <br> 
                                <span id="cancelBookings" class="m-widget24__stats m--font-danger">
                                    <?php echo $data['cancelBookings']; ?>
                                </span> 
                                <div class="">
                                    &nbsp;
                                </div> 
                                <span class="m-widget24__number">
                                    <a href="booking.php">View Details</a>
                                </span>
                            </div>
                        </div>
                        <!--end::New Orders-->
                    </div>
                    <div class="col-md-12 col-lg-6 col-xl-3">
                        <!--begin::New Users-->
                        <div class="m-widget24">
                            <div class="m-widget24__item">
                                <h4 class="m-widget24__title">
                                    Amount
                                </h4> <br> 
                                <span id="totalCost" class="m-widget24__stats m--font-success">
                                    <?php echo $data['totalCost']; ?>
                                </span> 
                                <div class="">
                                    &nbsp;
                                </div> 
                                <span class="m-widget24__number">
                                    <a href="booking.php">View Details</a>
                                </span>
                            </div>
                        </div>
                        <!--end::New Users-->
                    </div>
                </div>
            </div>
        </div>
        <!--end:: Widgets/Stats-->  

        <!--begin:: Widgets/Stats-->
        <?php if ($_SESSION['userLoginType'] == 1) { ?>
            <div class="m-portlet ">
                <div class="m-portlet__body  m-portlet__body--no-padding">
                    <div class="row m-row--no-padding m-row--col-separator-xl">
                        <div class="col-md-12 col-lg-6 col-xl-3">
                            <!--begin::Total Profit-->
                            <div class="m-widget24">
                                <div class="m-widget24__item">
                                    <h4 class="m-widget24__title">
                                        Active Customers
                                    </h4>
                                    <br> 
                                    <span class="m-widget24__stats m--font-focus">
                                        <?php echo $data['customers']; ?>
                                    </span> 
                                    <div class="">
                                    &nbsp;
                                </div> 
                                    <span class="m-widget24__number">
                                        <a href="customers.php">View Details</a>
                                    </span>
                                </div>
                            </div>
                            <!--end::Total Profit-->
                        </div>
                        <div class="col-md-12 col-lg-6 col-xl-3">
                            <!--begin::New Feedbacks-->
                            <div class="m-widget24">
                                <div class="m-widget24__item">
                                    <h4 class="m-widget24__title">
                                        Active Supervisors
                                    </h4>
                                    <br> 
                                    <span class="m-widget24__stats m--font-warning">
                                        <?php echo $data['supervisor']; ?>
                                    </span> 
                                    <div class="">
                                    &nbsp;
                                </div> 
                                    <span class="m-widget24__number">
                                        <a href="supervisor.php">View Details</a>
                                    </span>
                                </div>
                            </div>
                            <!--end::New Feedbacks-->
                        </div>
                        <div class="col-md-12 col-lg-6 col-xl-3">
                            <!--begin::New Orders-->
                            <div class="m-widget24">
                                <div class="m-widget24__item">
                                    <h4 class="m-widget24__title">
                                        Active Cleaners
                                    </h4>
                                    <br> 
                                    <span class="m-widget24__stats m--font-info">
                                        <?php echo $data['cleaner']; ?>
                                    </span> 
                                    <div class="">
                                    &nbsp;
                                </div> 
                                    <span class="m-widget24__number">
                                        <a href="cleaner.php">View Details</a>
                                    </span>
                                </div>
                            </div>
                            <!--end::New Orders-->
                        </div>
                        <div class="col-md-12 col-lg-6 col-xl-3">
                            <!--begin::New Users-->
                            <div class="m-widget24">
                                <div class="m-widget24__item">
                                    <h4 class="m-widget24__title">
                                        Active Packages
                                    </h4>
                                    <br> 
                                    <span class="m-widget24__stats m--font-danger">
                                        <?php echo $data['packages']; ?>
                                    </span> 
                                    <div class="">
                                    &nbsp;
                                </div> 
                                    <span class="m-widget24__number">
                                        <a href="room_service_packages.php">View Details</a>
                                    </span>
                                </div>
                            </div>
                            <!--end::New Users-->
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>

        <!--end:: Widgets/Stats-->   
    </div>
</div>
</div>
</div>

<!-- graph section --> 
<!--  end content -->

<!-- start footer -->         
<?php include("footer.php") ?>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">
                                    function dataChange() {
                                        var data1 = $("#filterForm").serialize();
                                        $.ajax({
                                            type: "POST",
                                            url: "showDashBoardData.php",
                                            data: data1, // serializes the form's elements.
                                            success: function (data)
                                            {
                                                var parsedJson = $.parseJSON(data);
                                                $('#bookings').html(parsedJson.bookings);
                                                $('#completeBookings').html(parsedJson.completeBookings);
                                                $('#cancelBookings').html(parsedJson.cancelBookings);
                                                $('#totalCost').html(parsedJson.totalCost);
                                                // show response from the php script.
                                            }
                                        });
                                        drawVisualization(data1);
                                        drawChart(data1)
                                    }
                                    ;
</script>
<script type="text/javascript">
    google.charts.load('current', {'packages': ['corechart']});
    google.charts.setOnLoadCallback(drawVisualization);
    google.charts.setOnLoadCallback(drawChart);

    function drawVisualization(d) {
        // Some raw data (not necessarily accurate)

        $.ajax({
            type: "POST",
            url: "showDashBoardGraphData.php",
            data: d, // serializes the form's elements.
            success: function (data)
            {

                /*
                 var data = google.visualization.arrayToDataTable([
                 ['Month', 'Bolivia', 'Ecuador', 'Madagascar', 'Papua New Guinea', 'Rwanda', 'Average'],
                 ['2004/05',  165,      938,         522,             998,           450,      614.6],
                 ['2005/06',  135,      1120,        599,             1268,          288,      682],
                 ['2006/07',  157,      1167,        587,             807,           397,      623],
                 ['2007/08',  139,      1110,        615,             968,           215,      609.4],
                 ['2008/09',  136,      691,         629,             1026,          366,      569.6]
                 ]);
                 */
                var data = google.visualization.arrayToDataTable($.parseJSON(data));
                var options = {
                    title: 'Weekly Booking Status',
                    vAxis: {title: 'Total Booking'},
                    hAxis: {title: 'Date'},
                    seriesType: 'bars'
                };

                var chart = new google.visualization.ComboChart(document.getElementById('m_gchart_1'));
                chart.draw(data, options);
                // show response from the php script.
            }
        });



    }
    function drawChart(d) {
        $.ajax({
            type: "POST",
            url: "showDashBoardGraphData2.php",
            data: d, // serializes the form's elements.
            success: function (data)
            {
                var data = google.visualization.arrayToDataTable($.parseJSON(data));
                var options = {
                    title: 'Revenue Report',
                    curveType: 'function',
                    legend: {position: 'bottom'},
                    axes: {
                        x: {
                          0: {side: 'top'}
                        }
                    } 

                };

                var chart = new google.visualization.LineChart(document.getElementById('curve_chart'));

                chart.draw(data, options);
                // show response from the php script.
            }
        });



    }
</script>







































