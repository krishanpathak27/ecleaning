<?php
include("includes/config.inc.php");
include("includes/function.php");
$_objAdmin = new Admin(); 

?>

<?php

if(isset($_POST['submit']) && $_POST['submit']=='View Details' && $_POST['attendance']=='attendance')
   { 
  
  $from_date = date('Y-m-01');
  $_SESSION['FROM_DATE'] = date('Y-m-d', strtotime($from_date));

  $to_date = date('Y-m-t');
  $_SESSION['TO_DATE'] = date('Y-m-d', strtotime($to_date));
  
  $put_date = $_POST['from1'];
  $_SESSION['DATE_ATTENDANCE'] = date('Y-m-d', strtotime($put_date));   

   }
   else if(isset($_POST['submit']) && $_POST['submit']=='View Details')
   {   
   
    if($_POST['from']!="") 
    {
    $from_date=$_POST['from'];
    $_SESSION['FROM_DATE']  = date('Y-m-d', strtotime($from_date));  
    }
    if($_POST['to']!="") 
    {
    $to_date=$_POST['to'];
    $_SESSION['TO_DATE'] = date('Y-m-d', strtotime($to_date));     
    }
     $_SESSION['DATE_ATTENDANCE'] = date('Y-m-d');
   }

  else
   {
  $from_date = date('Y-m-01');
  $_SESSION['FROM_DATE'] = date('Y-m-d', strtotime($from_date));  
  $to_date = date('Y-m-t');
  $_SESSION['TO_DATE'] = date('Y-m-d', strtotime($to_date));
  $_SESSION['DATE_ATTENDANCE'] = date('Y-m-d');
   }

 /*echo $_SESSION['FROM_DATE']."<br/>";
 echo $_SESSION['TO_DATE']."<br/>";
 echo $_SESSION['DATE_ATTENDANCE']; exit; */
if(isset($_SESSION['warehouseId']) && $_SESSION['warehouseId']>0)
{
  $_SESSION['sid'] = $_SESSION['warehouseId'];
}
 ?>



<?php include("header.inc.php") ?>



<!-- <script src="javascripts/charts.js"></script> -->
<link rel="stylesheet" href="css/font-awesome.min.css" type="text/css" />
<link rel="stylesheet" href="css/style2.css" type="text/css" />
<link rel="stylesheet" href="css/bootstrap.min.css" type="text/css" />
<script src="javascripts/dateNextPrev.js" type="text/javascript"></script>

<?php 
    if($_SESSION['userLoginType']==2)
    {
        $getHierarchy = $_objAdmin->_getSelectList2('table_account_admin','hierarchy_id','',' operator_id = "'.$_SESSION['operatorId'].'"');  
    }
    
?>
<?php if($_SESSION['userLoginType'] == 1 || ($_SESSION['userLoginType']==2 && ($getHierarchy[0]->hierarchy_id == 14 || $getHierarchy[0]->hierarchy_id==5))) {
  //include("graphs/graphs_latest.php");?>
<script>
//$(window).load(function() {
  /*$(document).ready(function() {
    // executes when complete page is fully loaded, including all frames, objects and images
      $( "#container" ).load( "graphs/graphs_latest.php" );
  });*/
</script>


<script type="text/javascript">
  
 // call attendace graph chart
 $(document).ready(function() {

      dashBoardSummary(); // call dashboard summary
     // topRetailers();     // top 10 retailers
      // orderPerformance(); // order performance 
      // distributorsMarketShare(); // distributor share market
      // topSalesmen(); // Top 5 salesman of this Year 
      // topProducts(); // Top 5 products of this Year 
      // topCities(); // Top 5 Cities of this Year 

 });

</script>
<?php } elseif($_SESSION['userLoginType'] == 2 && ($getHierarchy[0]->hierarchy_id!=5 || $getHierarchy[0]->hierarchy_id!=14)) {
  include("graphs/complaint_graphs.php");?>
<script>
//$(window).load(function() {
  /*$(document).ready(function() {
    // executes when complete page is fully loaded, including all frames, objects and images
      $( "#container" ).load( "graphs/graphs_latest.php" );
  });*/
</script>


<script type="text/javascript">
  
 // call attendace graph chart
 $(document).ready(function() {

      complaintSummary(); // call complaint summary
     

 });

</script>
<?php }else {?>
<script type="text/javascript"> 
$(document).ready(function() {
$('#content2').html('<div id="content"><div id="page-heading"><h1><span style="font-weight: bold;  color:#d74343;">Dashboard</span></h1></div><table width="100%" cellpadding="0" cellspacing="0" id="content-table"><tr><td><div id="content-table-inner"><table border="0" cellpadding="0" cellspacing="0"  id="id-form" style="height:250px;"><tr><td><h4><?php echo $_SESSION["companyName"]; ?></h4><br/>You are <b><?php if($_SESSION['userLoginType']==3){echo 'a Distributor';}elseif($_SESSION['userLoginType']==4){echo 'a Dealer';}elseif($_SESSION['userLoginType']==5){echo 'a Salesman';}elseif($_SESSION['userLoginType']==6){echo 'a Quality Auditor';}elseif($_SESSION['userLoginType']==7){echo 'a Warehouse';}elseif($_SESSION['userLoginType']==8){echo 'a Service Personnel';}?></b> in this organization<br/><br/><i><font color="grey">Today Date &amp; time: <?php echo date("d M Y h:i:s"); ?></font></i></td><td></td></tr></table><div class="clear"></div></div></td><td id="tbl-border-right"></td></tr></table></div>');
});
</script>

<?php }?>



<!-- start content-outer -->
<div id="content2">
<div id="content-outer">
<div id="loader"></div>
  <!-- start content -->
   <div id="container"></div>
  <!-- graph section --> 
<!--  end content -->
<div class="clear">&nbsp;</div>
</div>
<div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->
<div class="clear">&nbsp;</div> 
<!-- start footer -->         
<?php include("footer.php") ?>
<!-- end footer -->
<script src="javascripts/jquery-1.7.1.min.js" type="text/javascript"></script>
<script src="javascripts/validate.js"></script>
<script src="javascripts/highcharts.js"></script>
<script src="javascripts/exporting.js"></script>
<script src="graphs/charts.js"></script>
<script src="javascripts/jquery-ui.js"></script>

</body>
</html>