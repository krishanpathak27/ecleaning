<?php
include("includes/config.inc.php");
include("includes/function.php");
$page_name="Retailer Wise Breakup of (Items) Sold";
$_objAdmin = new Admin();

if($_REQUEST['id']!=''){
$item_name=$_objAdmin->_getSelectList('table_item','item_name,item_code',''," item_id='".$_REQUEST['id']."'"); 
}

if($_REQUEST['sal']!=''){
$sal_name=$_objAdmin->_getSelectList('table_salesman','salesman_name',''," salesman_id='".$_REQUEST['sal']."'"); 
$salesman_name=$sal_name[0]->salesman_name;
$sal_request="and s.salesman_id=".$_REQUEST['sal'];
$sal_id=$_REQUEST['sal'];
} else {
$salesman_name="All Salesman";
$sal_request="";
$sal_id="";
}

if($_REQUEST['city']!=''){
$city_name=$_objAdmin->_getSelectList2('city','city_name',''," city_id='".$_REQUEST['city']."'"); 
$city_name_list=$city_name[0]->city_name;
$city_request="and r.city=".$_REQUEST['city'];
} else {
$city_name_list="All City";
$city_request="";
}

$order_by_amt="desc";
$order_by_qty="desc";
$List= "amt_total ".$order_by_amt;

if(isset($_POST['order_qty']) && $_POST['order_qty'] == 'yes')
{
	if($_REQUEST['order_by_qty']=='desc'){
	$order_by_qty="asc";
	$order_by_amt="asc";
	$List="total_qty ".$order_by_qty;
	} else {
	$order_by_qty="desc";
	$order_by_amt="desc";
	$List="total_qty ".$order_by_qty;
	}
}

if(isset($_POST['order_amt']) && $_POST['order_amt'] == 'yes')
{
	if($_REQUEST['order_by_amt']=='desc'){
	$order_by_amt="asc";
	$order_by_qty="asc";
	$List= "amt_total ".$order_by_amt;
	} else {
	$order_by_amt="desc";
	$order_by_qty="desc";
	$List= "amt_total ".$order_by_amt;
	}	
}
?>

<?php include("header.inc.php") ?>

<script type="text/javascript">

    function PrintElem(elem)
    {
        Popup($(elem).html());
    }

    function Popup(data) 
    {
		
        var mywindow = window.open('', 'Report');
		
        mywindow.document.write('<html><head><title>Retailer Wise Breakup of (Items) Sold</title>');
		mywindow.document.write('<table><tr><td><b>Item Name:</b> <?php echo $item_name[0]->item_name; ?></td><td><b>Item Code:</b> <?php echo $item_name[0]->item_code; ?></td><td><b>Salesman Name:</b> <?php echo $salesman_name; ?></td><td><b>City Name:</b> <?php echo $city_name_list; ?></td><td><b>From Date:</b> <?php echo $_REQUEST['from']; ?></td><td><b>To Date:</b> <?php echo $_REQUEST['to']; ?></td></tr></table>');
        /*optional stylesheet*/ //mywindow.document.write('<link rel="stylesheet" href="main.css" type="text/css" />');
        mywindow.document.write('</head><body >');
        mywindow.document.write(data);
        mywindow.document.write('</body></html>');

        mywindow.print();
        mywindow.close();
        return true;
    }
	
	$(document).ready(function(){
		$('.maintr').click(function() {
			//alert("Hello");
			//$(this).css('background', '#E3E3E3');
			$('#report_export tr').removeClass('trbgcolor');
			
			$(this).addClass('trbgcolor');
		});
		//$('#lists').find('tr').keydown(function(e){
		//$('#lists tr').keydown(function(e) {
		
		
	});
</script>

<script src="javascripts/dateNextPrev.js" type="text/javascript"></script>
<!-- start content-outer -->
<input name="pagename" type="hidden"  id="pagename" value="admin_order_list.php" />
<div id="content-outer">
<!-- start content -->
<div id="content">
<div id="page-heading"><h1><span style="color: #d74343; font-family: Tahoma; font-weight: bold;">Retailer Wise Breakup of (Items) Sold</span></h1></div>
<table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">

<tr>
	
	<td>
	<!--  start content-table-inner -->
	<div id="content-table-inner">
	<div style="width: 100%;" align="right">
				
		</div>
	<table border="0" width="100%" cellpadding="0" cellspacing="0" >	
		<td>
		<table border="0" cellpadding="0" cellspacing="0"  >
			<tr>
				<th valign="top" style="width: 100px; line-height:14px;;"  align="left">Item Name:</th>
				<td align="left" style="min-width: 150px;"><?php echo $item_name[0]->item_name; ?></td>
			</tr>
			<tr>
				<th valign="top" style="width: 100px;line-height: 28px;"  align="left">Item Code:</th>
				<td align="left" style="min-width: 150px;"><b><?php echo $item_name[0]->item_code; ?></b></td>
			</tr>
			<tr>
				<th valign="top" style="width: 100px;line-height: 28px;"  align="left">Salesman:</th>
				<td align="left" style="min-width: 150px;"><?php echo $salesman_name; ?></td>
			</tr>
			<tr>
				<th valign="top" style="width: 100px;line-height: 28px;"  align="left">City Name:</th>
				<td align="left" style="min-width: 150px;"><?php echo $city_name_list; ?></td>
			</tr>
			<tr>
				<th valign="top" style="width: 100px;line-height: 28px;"  align="left">From Date:</th>
				<td valign="center" style="min-width: 150px;"><?php echo $_REQUEST['from']; ?></td>
				<th valign="top" style="width: 100px;line-height: 28px;"  align="left">To Date:</th>
				<td valign="center" style="min-width: 150px;"><?php echo $_REQUEST['to']; ?></td>
			</tr>
			<tr>
				
			</tr>
		</table>
		<div style="width:1000px;overflow:auto; height:auto;overflow:auto;" >
		<div id="Report">
		<table border="0" width="100%" cellpadding="0" cellspacing="0" id="report_export" name="report_export">
			<?php
			$auRet=$_objAdmin->_getSelectList2('table_order_detail as d left join table_order as o on d.order_id=o.order_id left join table_salesman as s on o.salesman_id=s.salesman_id left join table_retailer as r on o.retailer_id=r.retailer_id',"COUNT(Distinct o.order_id) as total",''," d.item_id='".$_REQUEST['id']."' and d.type!=2 and (o.date_of_order BETWEEN '".date('Y-m-d', strtotime($_REQUEST['from']))."' AND '".date('Y-m-d', strtotime($_REQUEST['to']))."') $sal_request $city_request AND r.new='' AND o.order_type!='No' GROUP BY o.retailer_id ORDER BY total desc LIMIT 1");
			if(is_array($auRet)){
			?>
			<tr  bgcolor="#6E6E6E" style="color: #fff;font-weight: bold;" id="candidateNameTD">
				<td rowspan="2" scope="col" style="padding:10px; width:10px;">SNO.</td>
				<td rowspan="2" scope="col" style="padding:10px;">Retailer Name</td>
				<td rowspan="2" scope="col" style="padding:10px;">% Share</td>
				<form name="submitAmt" method="POST" action="">
				<input type="hidden" name="order_by_amt" value="<?php echo $order_by_amt; ?>">
				<input type="hidden" name="order_amt" value="yes">
				<td rowspan="2" scope="col" style="padding:10px;" ><div style="width: 110px;" align="center">Total Amount &nbsp;&nbsp;<a href="javascript:document.submitAmt.submit()">
				<?php if($order_by_amt=='desc'){ ?>
				<img src="images/arrow-up.png" width="13" height="13" />
				<?php } else { ?>
				<img src="images/arrow-down.png" width="13" height="13" />
				<?php } ?>
				</a></div></td>
				</form>
				<form name="submitQty" method="POST" action="">
				<input type="hidden" name="order_by_qty" value="<?php echo $order_by_qty; ?>">
				<input type="hidden" name="order_qty" value="yes">
				<td rowspan="2" scope="col" style="padding:10px;"><div style="width: 110px;" align="center">Total Quantity &nbsp;&nbsp;<a href="javascript:document.submitQty.submit()">
				<?php if($order_by_qty=='desc'){ ?>
				<img src="images/arrow-up.png" width="13" height="13" />
				<?php } else { ?>
				<img src="images/arrow-down.png" width="13" height="13" />
				<?php } ?>
				</a></div></td>
				</form>
				<!--<td rowspan="2" scope="col" style="padding:10px;">Total Quantity</td>-->
				<?php
				 for($a=0;$a<$auRet[0]->total;$a++){ ?> 
				<!--<td style="padding:10px; " width="20%">Date<?php echo $a+1; ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>-->
				<td colspan="3" scope="col" style="padding:10px;" align="center">Visit <?php echo $a+1; ?>
				</td>
				<?php } ?>
			</tr>
			<tr bgcolor="#6E6E6E" style="color: #fff;font-weight: bold;">
			<?php
				 for($a=0;$a<$auRet[0]->total;$a++){ ?> 
					<td style="padding:10px;"><div style="width: 100px;" align="center">Date of Order</div></td>
					<td style="padding:10px;"align="center" >Salesman</td>
					<td style="padding:10px;" align="center">Quantity</td>
				  
				<?php } ?>
			</tr>
			<?php
			$net_total_item=$_objAdmin->_getSelectList2('table_order_detail as d left join table_order as o on d.order_id=o.order_id left join table_salesman as s on o.salesman_id=s.salesman_id left join table_retailer as r on o.retailer_id=r.retailer_id',"sum(d.acc_quantity) as totalitem, sum(d.acc_total) as amt_total",''," d.item_id='".$_REQUEST['id']."' and d.type=1 and (o.date_of_order BETWEEN '".date('Y-m-d', strtotime($_REQUEST['from']))."' AND '".date('Y-m-d', strtotime($_REQUEST['to']))."') $sal_request $city_request AND r.new='' ");
			
			$itemList=$_objAdmin->_getSelectList2('table_order_detail as d left join table_order as o on d.order_id=o.order_id left join table_salesman as s on o.salesman_id=s.salesman_id left join table_retailer as r on o.retailer_id=r.retailer_id',"o.order_id, r.retailer_name, r.retailer_id, sum(d.acc_quantity) as total_qty, sum(d.acc_total) as amt_total ",''," d.item_id='".$_REQUEST['id']."' and d.type=1 and (o.date_of_order BETWEEN '".date('Y-m-d', strtotime($_REQUEST['from']))."' AND '".date('Y-m-d', strtotime($_REQUEST['to']))."') $sal_request $city_request AND r.new='' GROUP BY o.retailer_id order by $List");
			for($i=0;$i<count($itemList);$i++)
				{
			?>
			<tr class="maintr"  style="border-bottom:2px solid #6E6E6E;">
				<td style="padding:10px;"><?php echo $i+1 ?></td>
				<td style="padding:10px;"><?php echo $itemList[$i]->retailer_name; ?></td>
				<td style="padding:10px;" ><div style="width: 50px;" align="center"><?php echo round($itemList[$i]->amt_total/$net_total_item[0]->amt_total*100,2)."%" ?></div></td>
				<td style="padding:10px;" ><div style="width: 110px;" align="center"><?php echo $itemList[$i]->amt_total; ?></div></td>
				<td style="padding:10px;" ><div style="width: 110px;" align="center"><?php echo $itemList[$i]->total_qty;?></div></td>
				<?php
				$item=$_objAdmin->_getSelectList2('table_order_detail as d left join table_order as o on d.order_id=o.order_id left join table_salesman as s on o.salesman_id=s.salesman_id left join table_retailer as r on o.retailer_id=r.retailer_id',"o.order_id,o.date_of_order,sum(d.acc_quantity) as total_order,s.salesman_name",''," d.item_id='".$_REQUEST['id']."' and d.type=1 and (o.date_of_order BETWEEN '".date('Y-m-d', strtotime($_REQUEST['from']))."' AND '".date('Y-m-d', strtotime($_REQUEST['to']))."') $sal_request $city_request AND o.retailer_id='".$itemList[$i]->retailer_id."' group by o.order_id");
				if(is_array($item)){
				for($b=0;$b<count($item);$b++){
				/* $q=($b&1)?1:0;
				echo $m= ($q==1)?"<td style='padding:10px;' bgcolor='#98FB98' align='center'>":"<td style='padding:10px;' align='center'>";
				echo $item[$b]->date_of_order;
				echo "</td>";
				echo $m= ($q==1)?"<td style='padding:10px;' bgcolor='#98FB98' align='center'>":"<td style='padding:10px;' align='center'>";
				echo $item[$b]->salesman_name;
				echo "</td>";
				echo $m= ($q==1)?"<td style='padding:10px;' bgcolor='#98FB98' align='center'>":"<td style='padding:10px;' align='center'>";
				echo $item[$b]->acc_quantity;
				echo "</td>"; */
				?>
				<td style="padding:10px;" bgcolor="#98FB98"  align='center' ><?php echo $_objAdmin->_changeDate($item[$b]->date_of_order);?> </td>
				<td style="padding:10px;"  align='center' ><?php echo $item[$b]->salesman_name;?> </td>
				<td style="padding:10px;"  align='center'><?php echo $item[$b]->total_order;?> </td>
				<?php
				}
			$row_total=$auRet[0]->total;
				for($c=0;$c<$row_total-count($item);$c++){
			?>
			<td style="padding:10px;" bgcolor="#98FB98" align="center">-</td>
			<td style="padding:10px;" align="center">-</td>
			<td style="padding:10px;" align="center">-</td>
			<?php } 
			} 
			
			?>
			</tr>
			<?php } ?>
			<tr  bgcolor="#6E6E6E" style="color: #fff;font-weight: bold;" id="candidateNameTD">
				<td style="padding:10px; width:10px;" colspan="3" align="right">Total</td>
				<td style="padding:10px;" ><div style="width: 110px;" align="center"><?php echo $net_total_item[0]->amt_total; ?></div></td>
				<td style="padding:10px;"><div style="width: 110px;" align="center"><?php echo $net_total_item[0]->totalitem; ?></div></td>
				<td style="padding:10px; width:10px;" colspan="<?php echo $auRet[0]->total*3+5; ?>" align="right"></td>
			</tr>
		</table>
		
		<?php } else {?>
		<table border="0" width="100%" cellpadding="0" cellspacing="0">
			<tr  style="border-bottom:2px solid #6E6E6E;border-top:2px solid #6E6E6E;" align="center">
				<td style="padding:10px;" colspan="6">Report Not Available</td>
			</tr>
		</table>
		<?php } ?>
		</div>
		</div>
		<table border="0" width="25%" cellpadding="0" cellspacing="0">
		<tr  >
			<td style="padding:10px;"><div style="width: 2px;" align="left"><input type="button" value="Close" class="form-cen" onclick="javascript:window.close();" /></div></td>
			<td ><div style="width: 20px;" align="center"><input type="button" value="Print" class="result-submit" onclick="PrintElem('#Report')" /></div></td>
			<td ><div style="width: 20px;" align="center">
			<a id="dlink"  style="display:none;"></a>
			<input input type="button" value="Export to Excel" class="result-submit"  onclick="tableToExcel('report_export', 'Retailer Wise Breakup of (Items) Sold Report', 'Retailer Wise Breakup of (Items) Sold Report.xls')"></div>
			</td>
		</tr>
		</table>
		
		</td>
	
	</tr>
	<tr>
		<td><img src="images/blank.gif" width="695" height="1" alt="blank" /></td>
		<td></td>
	</tr>
	</table>
<div class="clear"></div>
</div>
<!--  end content-table-inner  -->
</td>
<td id="tbl-border-right"></td>
</tr>

</table>
<div class="clear">&nbsp;</div>
</div>
<!--  end content -->
<div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->
<div class="clear">&nbsp;</div> 
<!-- start footer -->         
<?php include("footer.php") ?>
<?php unset($_SESSION['SalAttList']); ?>
<!-- end footer -->
<script src="javascripts/jquery-1.8.2.js" type="text/javascript"></script>
<script src="javascripts/jquery-ui.js"></script>
<script type="text/javascript" src="javascripts/validate.js"></script>
<script type="text/javascript">
// Popup window code
function newPopup(url) {
	popupWindow = window.open(
		url,'popUpWindow','height=500,width=600,left=200,top=100,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no,status=yes')
}
</script>
<script>
    $(function() {
        $( "#from" ).datepicker({
			dateFormat: "yy-mm-dd",
            defaultDate: "w",
            changeMonth: true,
			changeYear: true,
			yearRange: '2010:2020',
            numberOfMonths: 1,
            onSelect: function( selectedDate ) {
                $( "#to" ).datepicker( "option", "minDate", selectedDate );
            }
        });
        $( "#to" ).datepicker({
			dateFormat: "yy-mm-dd",
            defaultDate: "-w",
            changeMonth: true,
			changeYear: true,
			yearRange: '2010:2020',
            numberOfMonths: 1,
            onSelect: function( selectedDate ) {
                $( "#from" ).datepicker( "option", "maxDate", selectedDate );
            }
        });
    });
</script>
<script type='text/javascript'>//<![CDATA[ 
var tableToExcel = (function () {
        var uri = 'data:application/vnd.ms-excel;base64,'
        , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table><tr><td><b>Item Name:</b> <?php echo $item_name[0]->item_name; ?></td><td><b>Item Code:</b> <?php echo $item_name[0]->item_code; ?></td><td><b>Salesman Name:</b> <?php echo $salesman_name; ?></td><td><b>City Name:</b> <?php echo $city_name_list; ?></td><td><b>From Date:</b> <?php echo $_REQUEST['from']; ?></td><td><b>To Date:</b> <?php echo  $_REQUEST['to']; ?></td></tr></table><table>{table}</table></body></html>'
        , base64 = function (s) { return window.btoa(unescape(encodeURIComponent(s))) }
        , format = function (s, c) { return s.replace(/{(\w+)}/g, function (m, p) { return c[p]; }) }
        return function (table, name, filename) {
            if (!table.nodeType) table = document.getElementById(table)
            var ctx = { worksheet: name || 'Worksheet', table: table.innerHTML }

            document.getElementById("dlink").href = uri + base64(format(template, ctx));
            document.getElementById("dlink").download = filename;
            document.getElementById("dlink").click();

        }
    })()

//]]>  
</script>
</body>
</html>