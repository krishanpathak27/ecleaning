<?php
include("includes/config.inc.php");
include("includes/function.php");
$page_name="Items Wise Report";
$_objAdmin = new Admin();

if(isset($_POST['showReport']) && $_POST['showReport'] == 'yes')
{	
	if($_POST['sal']!="") 
	{
	$sal_list=$_POST['sal'];
	$sal_query=" AND s.salesman_id='".$sal_list."' ";
	} 
	if($_POST['city']!="") 
	{
	$city_list=$_POST['city'];
	$city_query=" AND r.city='".$city_list."' ";
	} 
	if($_POST['items']==2) 
	{
		$item=$_POST['items'];
		$item_list_1=mysql_escape_string($_POST['item_list']);
		$item_name = explode(",", $item_list_1);
		$item_data="";
		$item_id_1=array();
		for($i=0;$i<count($item_name);$i++){
			if($item_name[$i]!=''){
			$item_name[$i];
			//$auItemRec=$this->_getSelectList('table_item',"item_id",'',"where LOWER(item_code)='".strtolower($item_name[$i])."'");
			$auItemRec=$_objAdmin->_getSelectList('table_item',"item_id",'',"where LOWER(item_code)='".strtolower($item_name[$i])."'"); 
				
				if(is_array($auItemRec)){
				$item_id_1[]=$auItemRec[0]->item_id;
				$item_data.=$coma.$auItemRec[0]->item_id;
				$item_data_name.=$coma.$item_name[$i];
				$coma=",";
				$item_query=" AND i.item_id in (".$item_data.") ";
				}
			}
			
		}
		unset($coma);
	//$item_list=$_POST['item'];	
	//$item_query=" AND d.item_id='".$item_list."' ";
	} 
	if($_POST['from']!="") 
	{
	$from_date=$_POST['from'];	
	}
	if($_POST['to']!="") 
	{
	$to_date=$_POST['to'];	
	}
} else {
$from_date= date("Y-m-d");
$to_date= date("Y-m-d");
}
if($_REQUEST['sal']!=''){
$SalName=$_objAdmin->_getSelectList('table_salesman','salesman_name','',"where salesman_id='".$_REQUEST['sal']."'"); 
$sal_name=$SalName[0]->salesman_name;
} else {
$sal_name="All Salesman";
}
if($_REQUEST['city']!=''){
$aCity=$_objAdmin->_getSelectList('city','city_name','',"where city_id='".$_REQUEST['city']."'"); 
$city_name=$aCity[0]->city_name;
} else {
$city_name="All City";
}
if($item_data_name!=''){
$item_name_list=$item_data_name;
} else {
$item_name_list="All Items";
}
?>

<?php include("header.inc.php") ?>
<link rel="stylesheet" href="./base/jquery.ui.all.css">
<script src="./Autocomplete/jquery-1.5.1.js"></script>
<script src="./Autocomplete/jquery.ui.core.js"></script>
<script src="./Autocomplete/jquery.ui.widget.js"></script>
<script src="./Autocomplete/jquery.ui.button.js"></script>
<script src="./Autocomplete/jquery.ui.position.js"></script>
<script src="./Autocomplete/jquery.ui.menu.js"></script>
<script src="./Autocomplete/jquery.ui.autocomplete.js"></script>
<script src="./Autocomplete/jquery.ui.tooltip.js"></script>
<script src="./Autocomplete/jquery.ui.datepicker.js"></script>
<link rel="stylesheet" href="./base/demos.css">
<script type="text/javascript" src="javascripts/validate.js"></script>
<script src="javascripts/discount.js"></script>
<script src="javascripts/addrow.js"></script>
<script>
    $(function() {
        $( "#from" ).datepicker({
			dateFormat: "yy-mm-dd",
            defaultDate: "+1w",
            changeMonth: true,
            numberOfMonths: 1,
            onSelect: function( selectedDate ) {
                $( "#to" ).datepicker( "option", "minDate", selectedDate );
            }
        });
        $( "#to" ).datepicker({
			dateFormat: "yy-mm-dd",
            defaultDate: "+1w",
            changeMonth: true,
            numberOfMonths: 1,
            onSelect: function( selectedDate ) {
                $( "#from" ).datepicker( "option", "maxDate", selectedDate );
            }
        });
    });

</script>
<script>
$(function() {
		var availableState = [
			<?php
			$auCol=$_objAdmin->_getSelectList('table_item',"item_code",''," where 1=1 ORDER by item_code");
			for($i=0;$i<count($auCol);$i++){
				if(count($auCol)-1!=$i)
				{
					echo '"'.$auCol[$i]->item_code.'",';
				} else {
					echo '"'.$auCol[$i]->item_code.'"';
				}
			}
			?>
		];
		function split( val ) {
			return val.split( /,\s*/ );
		}
		function extractLast( term ) {
			return split( term ).pop();
		}

		$( "#item_list" )
			// don't navigate away from the field on tab when selecting an item
			.bind( "keydown", function( event ) {
				if ( event.keyCode === $.ui.keyCode.TAB &&
						$( this ).data( "autocomplete" ).menu.active ) {
					event.preventDefault();
				}
			})
			.autocomplete({
				minLength: 0,
				source: function( request, response ) {
					// delegate back to autocomplete, but extract the last term
					response( $.ui.autocomplete.filter(
						availableState, extractLast( request.term ) ) );
				},
				focus: function() {
					// prevent value inserted on focus
					return false;
				},
				select: function( event, ui ) {
					var terms = split( this.value );
					// remove the current input
					terms.pop();
					// add the selected item
					terms.push( ui.item.value );
					// add placeholder to get the comma-and-space at the end
					terms.push( "" );
					this.value = terms.join( "," );
					return false;
				}
			});
	});
</script>
<script type="text/javascript">

    function PrintElem(elem)
    {
        Popup($(elem).html());
    }

    function Popup(data) 
    {
		
        var mywindow = window.open('', 'Report');
		
        mywindow.document.write('<html><head><title>Items Wise Report</title>');
		mywindow.document.write('<table><tr><td><b>Salesman Name:</b> <?php echo $sal_name; ?></td><td><b>City Name:</b> <?php echo $city_name; ?></td><td><b>Items:</b> <?php echo $item_name_list; ?></td><td><b>From Date:</b> <?php echo $from_date; ?></td><td><b>To Date:</b> <?php echo $to_date; ?></td></tr></table>');
        /*optional stylesheet*/ //mywindow.document.write('<link rel="stylesheet" href="main.css" type="text/css" />');
        mywindow.document.write('</head><body >');
        mywindow.document.write(data);
        mywindow.document.write('</body></html>');

        mywindow.print();
        mywindow.close();
        return true;
    }
</script>
<?php if ($item!=2){ ?>
<script>
$(document).ready(function() {
$("#item_list").attr('disabled', 'disabled');
});
</script>
<?php } ?>
<script>
function itemtype(value)
{
	if(value==2)
	{
		$("#item_list").removeAttr('disabled');
		$("#itemshow").show();
	}
	else 
	{
		$("#item_list").attr('disabled', 'disabled');
		$("#itemshow").hide();
	}
}
</script>
<script src="javascripts/dateNextPrev.js" type="text/javascript"></script>
<!-- start content-outer -->
<div id="content-outer">
<!-- start content -->
<div id="content">
<div id="page-heading"><h1><span style="color: #d74343; font-family: Tahoma; font-weight: bold;">Items Wise Report</span></h1></div>
<table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">
<tr>
	<th rowspan="3" class="sized"><img src="images/side_shadowleft.jpg" width="20" height="300" alt="" /></th>
	<th class="topleft"></th>
	<td id="tbl-border-top">&nbsp;</td>
	<th class="topright"></th>
	<th rowspan="3" class="sized"><img src="images/side_shadowright.jpg" width="20" height="300" alt="" /></th>
</tr>
<tr>
	<td id="tbl-border-left"></td>
	<td>
	<!--  start content-table-inner -->
	
	<div id="content-table-inner">
	
	<div id="page-heading" align="left" >
	<form name="frmPre" id="frmPre" method="post" action="" enctype="multipart/form-data" >
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
	<tr  align="right">
		<td></td>
		<td></td>
		<td></td>
		<td><table border="0" width="60%" ><tr><td> <img src="css/images/prev.png" onclick="dateFromPrev();"> </td> <td align="right"> <img src="css/images/next.png" onclick="dateFromNext();"> </td><td>&nbsp;&nbsp; </td> </tr></table></td>
		<td><table border="0" width="65%"><tr><td> <img src="css/images/prev.png" onclick="dateToPrev();"> </td> <td align="right"> <img src="css/images/next.png" onclick="dateToNext();"> </td><td>&nbsp;&nbsp; </td> </tr></table></td>
		<td>
		<a id="dlink"  style="display:none;"></a>
		<input input type="button" value="Export to Excel" class="form-reset"  onclick="tableToExcel('report_export', 'item wise report', 'item wise report.xls')">
		</td>
		<td><input type="button" value="Print" class="form-reset" onclick="PrintElem('#Report')" /></td>
	</tr>
	<tr>
		<td ><h3>Salesman: 
		<select name="sal" id="sal" class="" style="border: solid 1px #BDBDBD; color: #393939;font-family: Arial;font-size: 12px;border-radius:5px ;-moz-border-radius:5px;-o-border-radius:5px;-ms-border-radius:5px;-webkit-border-radius:5px;width: 150px;height: 30px;" >
		<option value="" >All Salesman</option>
		<?php $aSal=$_objAdmin->_getSelectList('table_salesman','*','',"where account_id='".$_SESSION['accountId']."' and status='A' ORDER BY salesman_name"); 
		if(is_array($aSal)){
		for($i=0;$i<count($aSal);$i++){
		?>
		<option value="<?php echo $aSal[$i]->salesman_id;?>" <?php if ($aSal[$i]->salesman_id==$sal_list){ ?> selected <?php } ?>><?php echo $aSal[$i]->salesman_name;?></option>
		<?php } }?>
		</select></h3></td>
		<td><h3>City: 
		<select name="city" id="city" class="" style="border: solid 1px #BDBDBD; color: #393939;font-family: Arial;font-size: 12px;border-radius:5px ;-moz-border-radius:5px;-o-border-radius:5px;-ms-border-radius:5px;-webkit-border-radius:5px;width: 125px;height: 30px;" >
		<option value="" >All City</option>
		<?php $aCity=$_objAdmin->_getSelectList('city','city_id,city_name','',"where city_name!='' ORDER BY city_name"); 
		if(is_array($aCity)){
		for($i=0;$i<count($aCity);$i++){
		?>
		<option value="<?php echo $aCity[$i]->city_id;?>" <?php if ($aCity[$i]->city_id==$city_list){ ?> selected <?php } ?>><?php echo $aCity[$i]->city_name;?></option>
		<?php } }?>
		</select></h3></td>
		<td><h2><input type="radio" name="items" value="1" checked="checked" onchange="itemtype(this.value);"> All Items <input type="radio" name="items" value="2"  onchange="itemtype(this.value);" <?php if ($item==2){ ?> checked="checked" <?php } ?>> Group  
		</h2></td>
		<td><h3>From Date: <input type="text" id="from" name="from" class="date" style="width:100px" value="<?php  echo $from_date;?>"  readonly /></h3></td>
		<td><h3>To Date: <input type="text" id="to" name="to" class="date" style="width:100px" value="<?php echo $to_date; ?>"  readonly /></h3></td>
		<td><input name="showReport" type="hidden" value="yes" />
		<input name="submit" class="result-submit" type="submit" id="submit" value="Show Report" /></td>
		<td><input type="button" value="Reset!" class="form-reset" onclick="location.href='items_report.php';" /></td>
		<!--<td><input type="button" value="Reset!" class="form-reset" onclick="location.href='retailer_report.php?reset=yes';" /></td>
		<td><input type="button" value="Print" class="form-reset" onclick="PrintElem('#Report')" /></td>
		<td><input type="button" value="Export to Excel" class="form-reset" onclick="location.href='export.inc.php?export_ret_vise_report';" /></td>-->
	</tr>
	<tr  align="center" id="itemshow" <?php if ($item!=2){ ?> style="display:none;" <?php } ?>>
		<td colspan="5">
		<input id="item_list" style="width: 500px; height: 20px;" class="ui-autocomplete-input required" autocomplete="off" role="textbox" aria-autocomplete="list" aria-haspopup="true" name="item_list" placeholder="Search Item Code" value="<?php echo $item_data_name; ?>">
		</td>
		<td></td>
	</tr>
	</table>
	</form>
	</div>
	<table border="0" width="100%" cellpadding="0" cellspacing="0" id="report_export" name="report_export">
		<td>
		<div style="width:1100px;overflow:auto; height:400px;overflow:auto;" >
		<div id="Report">
		<table  border="1"  width="100%" cellpadding="0" cellspacing="0" >
			<?php
			$Total_Items=$_objAdmin->_getSelectList('table_order_detail as d left join table_order as o on o.order_id=d.order_id left join table_item as i on i.item_id=d.item_id left join table_retailer as r on o.retailer_id=r.retailer_id left join table_salesman as s on s.salesman_id=o.salesman_id',"SUM(d.acc_total) as net_total",''," where r.new='' AND (o.date_of_order BETWEEN '".$from_date."' AND '".$to_date."') AND o.order_type!='No' $sal_query $city_query $item_query ");
			$net_amoumt=$Total_Items[0]->net_total;
			
			$auItem=$_objAdmin->_getSelectList('table_item as i left join table_order_detail as d on i.item_id=d.item_id left join table_order as o on o.order_id=d.order_id left join table_retailer as r on o.retailer_id=r.retailer_id left join table_salesman as s on s.salesman_id=o.salesman_id',"SUM(d.acc_quantity) as total_qty,SUM(d.acc_total) as total_amt,COUNT(Distinct o.order_id) as total_order,COUNT(Distinct o.retailer_id) As total_ret, i.item_code, d.item_id",''," where r.new='' AND (o.date_of_order BETWEEN '".$from_date."' AND '".$to_date."') AND o.order_type!='No' $sal_query $city_query $item_query GROUP BY i.item_id ORDER BY total_amt desc ");
			$item_id_2=array();
			if(is_array($auItem)){
			?>
			
			<tr  bgcolor="#6E6E6E" style="color: #fff;font-weight: bold;" >
				<td style="padding:10px;" width="10%">Items</td>
				<td style="padding:10px;" width="10%">% Share</td>
				<td style="padding:10px;" width="10%">Total Amount</td>
				<td style="padding:10px;" width="10%">Total quantity</td>
				<td style="padding:10px;" width="10%">Sold to</td>
				<td style="padding:10px;" width="10%">In.</td>
			</tr>
			<?php 
			for($i=0;$i<count($auItem);$i++)	{ 
			$item_id_2[]= $auItem[$i]->item_id;
			?>
			<tr  style="border-bottom:2px solid #6E6E6E;" >
				<td style="padding:10px;" width="10%"><?php echo $auItem[$i]->item_code;?> </td>
				<td style="padding:10px;" width="10%"><?php echo  round($auItem[$i]->total_amt/$net_amoumt*100,2) ."%";?> </td>
				<td style="padding:10px;" width="10%"><?php echo $auItem[$i]->total_amt;?> </td>
				<td style="padding:10px;" width="10%"><?php echo $auItem[$i]->total_qty;?> </td>
				<td style="padding:10px;" width="10%"><?php echo $auItem[$i]->total_ret;?> </td>
				<td style="padding:10px;" width="10%"><?php echo $auItem[$i]->total_order." Order";?> </td>
			</tr>
			<?php } ?>
			
		<?php
		$Item_not_exit = array_diff($item_id_1, $item_id_2);
		foreach($Item_not_exit as $a=>$a_value)
		  {
		  $item_id_not.=$coma.$a_value;
		  $coma=",";
		  }
		unset($coma);
		//echo $item_id_not;
		//print_r($Item_not_exit);
		if($item_id_not!=''){
		$auItem_not=$_objAdmin->_getSelectList('table_item',"item_code",''," where item_id in ($item_id_not) ");
			if(is_array($auItem_not)){
				for($i=0;$i<count($auItem_not);$i++)	{ 
				?>
				<tr  style="border-bottom:2px solid #6E6E6E;" >
				<td style="padding:10px;" width="10%"><?php echo $auItem_not[$i]->item_code;?> </td>
				<td style="padding:10px;" width="10%">-</td>
				<td style="padding:10px;" width="10%">-</td>
				<td style="padding:10px;" width="10%">-</td>
				<td style="padding:10px;" width="10%">-</td>
				<td style="padding:10px;" width="10%">-</td>
			</tr>
			<?php
				}
			}
		}
			?>
			
		<?php
		}
		else { 
		if($item_query!=''){
		$auItem_not=$_objAdmin->_getSelectList('table_item',"item_code",''," where item_id in ($item_data) ");
			if(is_array($auItem_not)){
				?>
				<tr  bgcolor="#6E6E6E" style="color: #fff;font-weight: bold;" >
					<td style="padding:10px;" width="10%">Items</td>
					<td style="padding:10px;" width="10%">% Share</td>
					<td style="padding:10px;" width="10%">Total Amount</td>
					<td style="padding:10px;" width="10%">Total quantity</td>
					<td style="padding:10px;" width="10%">Sold to</td>
					<td style="padding:10px;" width="10%">In.</td>
				</tr>
				<?php
				for($i=0;$i<count($auItem_not);$i++)	{ 
				?>
				<tr  style="border-bottom:2px solid #6E6E6E;" >
				<td style="padding:10px;" width="10%"><?php echo $auItem_not[$i]->item_code;?> </td>
				<td style="padding:10px;" width="10%">-</td>
				<td style="padding:10px;" width="10%">-</td>
				<td style="padding:10px;" width="10%">-</td>
				<td style="padding:10px;" width="10%">-</td>
				<td style="padding:10px;" width="10%">-</td>
			</tr>
			<?php
				}
			}
			?>
		<?php } else { ?>
		<tr  style="border-bottom:2px solid #6E6E6E; border-top:2px solid #6E6E6E;" align="center" >
		 <td style="padding:10px;" width="100%">Report Not Available</td>
		</tr>
		<?php }  } ?>
		</table>
		</div>
		</div>
		</td>
	</tr>
	<tr>
		<td><img src="images/blank.gif" width="695" height="1" alt="blank" /></td>
		<td></td>
	</tr>
	</table>
<div class="clear"></div>
</div>

<!--  end content-table-inner  -->
</td>
<td id="tbl-border-right"></td>
</tr>
<tr>
	<th class="sized bottomleft"></th>
	<td id="tbl-border-bottom">&nbsp;</td>
	<th class="sized bottomright"></th>
</tr>
</table>
<div class="clear">&nbsp;</div>
</div>
<!--  end content -->
<div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->
<div class="clear">&nbsp;</div> 
<!-- start footer -->        
<?php include("footer.php") ?>

<!-- end footer -->
<script type='text/javascript'>//<![CDATA[ 
var tableToExcel = (function () {
        var uri = 'data:application/vnd.ms-excel;base64,'
        , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table><tr><td><b>Salesman Name:</b> <?php echo $sal_name; ?></td><td><b>City Name:</b> <?php echo $city_name; ?></td><td><b>Items:</b> <?php echo $item_name_list; ?></td><td><b>From Date:</b> <?php echo $from_date; ?></td><td><b>To Date:</b> <?php echo $to_date; ?></td></tr></table><table>{table}</table></body></html>'
        , base64 = function (s) { return window.btoa(unescape(encodeURIComponent(s))) }
        , format = function (s, c) { return s.replace(/{(\w+)}/g, function (m, p) { return c[p]; }) }
        return function (table, name, filename) {
            if (!table.nodeType) table = document.getElementById(table)
            var ctx = { worksheet: name || 'Worksheet', table: table.innerHTML }

            document.getElementById("dlink").href = uri + base64(format(template, ctx));
            document.getElementById("dlink").download = filename;
            document.getElementById("dlink").click();

        }
    })()

//]]>  
</script>


</body>
</html>