function chkDiscount()
{
	if(document.getElementById("item").value == "")
	{
	alert("Please Select Item.");
	document.getElementById("item").focus();
	return false;
	}
	if(document.getElementById("discount_type").value == "")
	{
	alert("Please Select Discount Type.");
	document.getElementById("discount_type").focus();
	return false;
	}
}
function addRow(tableID,flag,value,qty,dqty,i,id) {
 			//alert(id);
			
            var table = document.getElementById(tableID);
			if (i==1 && flag==2)
 				table.deleteRow(0);
            var rowCount = table.rows.length;
            var row = table.insertRow(rowCount);
 			row.id = rowCount + 1;
            var colCount = table.rows[0].cells.length;
 
            for(var i=0; i<colCount; i++) {
 				
                var newcell = row.insertCell(i);
 				
                newcell.innerHTML = table.rows[0].cells[i].innerHTML;
				//alert(newcell.childNodes);
                switch(newcell.childNodes[0].type) {
					case "label":
							newcell.childNodes[0].innerHTML = "4";
                            break;
					case "text":
					//alert(i);
							if (flag == 2)
							{
								if (i == 1)
									newcell.childNodes[0].value = qty;
								else if (i ==2)
									newcell.childNodes[0].value = dqty;
								else if (i ==3)
									newcell.childNodes[0].value = id;
							}
							else
							{ 	newcell.childNodes[0].value = ""; }
                            break;
                    case "checkbox":
                            newcell.childNodes[0].checked = false;
                            break;
                    case "select-one":
							if (flag == 2)
								newcell.childNodes[0].value = value;
							else
                            	newcell.childNodes[0].selectedIndex = 0;
                            break;
					case "button":
							
                            newcell.onclick = function() {
								table.deleteRow(row); }
                            break;
					
                }
            }
        }
 
        function deleteRow(tableID) {
			//alert(tableID);
            try {
            var table = document.getElementById(tableID);
            var rowCount = table.rows.length;
 
            for(var i=0; i<rowCount; i++) {
                var row = table.rows[i];
                var chkbox = row.cells[0].childNodes[0];
                if(null != chkbox && true == chkbox.checked) {
                    if(rowCount <= 1) {
                        alert("Cannot delete all the rows.");
                        break;
                    }
                    table.deleteRow(i);
                    rowCount--;
                    i--;
                }
 
            }
            }catch(e) {
                alert(e);
            }
        }