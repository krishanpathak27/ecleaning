// JavaScript Document
function showCategory(){
$("#flex1").flexigrid({
	url: 'category.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Category Name', name : 'category_name', width : 300, sortable : true, align: 'left'},
		{display: 'Category Code', name : 'category_code', width : 200, sortable : true, align: 'left'},
		{display: 'Status', name : 'status', width : 100, sortable : true, align: 'left'}
		],
	buttons : [
		{name: 'Add', bclass: 'add', onpress : eventHandlerCategory},
		{separator: true},		
		{name: 'Edit', bclass: 'edit', onpress : eventHandlerCategory},
		{separator: true}		
		],
	searchitems : [
		{display: 'Category Name', name : 'category_name', isdefault: true},
		{display: 'Category Code', name : 'category_code'}
		
		],
	sortname: "category_name",
	sortorder: "asc",
	usepager: true,
	title: 'Category',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 700,
	height: 'auto'
});  
}
function eventHandlerCategory(com,grid)
{
	if (com=='Add')	{
		location.href="addCategory.php";		
	}else{
		if( $('.trSelected',grid).length<=0 ||  $('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=$('.trSelected',grid);		
		if (com=='Edit'){
			location.href="addCategory.php?id="+items[0].id.substr(3);			
		}
		if (com=='Delete'){
			if(confirm("Are you sure, you want to delete selected Category?")){
					
				$.post("category.php", {"delete":"yes","id":items[0].id.substr(3)},function(data)
				{		 
				  if(data!="") {					 
					 $('#flex1').flexOptions().flexReload(); 
				  }else {
					  alert("Record doesn't exists.")
				  }
				  
				});		
			}			
		}	
	}
}

function showRelationship(){
$("#flex1").flexigrid({
	url: 'relationship.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Customer Class', name : 'relationship_code', width : 200, sortable : true, align: 'left'},
		{display: 'Customer Class Description', name : 'relationship_desc', width : 200, sortable : true, align: 'left'},
		{display: 'Customer Type', name : 'relationship_type', width : 200, sortable : true, align: 'left'},
		{display: 'Status', name : 'status', width : 100, sortable : true, align: 'left'}
		],
	buttons : [
		{name: 'Add', bclass: 'add', onpress : eventHandlerRelationship},
		{separator: true},		
		{name: 'Edit', bclass: 'edit', onpress : eventHandlerRelationship},
		{separator: true}
		
		],
	searchitems : [
		{display: 'Customer Class', name : 'relationship_code', isdefault: true},
		{display: 'Customer Class Description', name : 'relationship_desc'},
		{display: 'Customer Type', name : 'relationship_type'}
		
		],
	sortname: "relationship_code",
	sortorder: "asc",
	usepager: true,
	title: 'Customer Class',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 700,
	height: 'auto'
});  
}
function eventHandlerRelationship(com,grid)
{
	if (com=='Add')	{
		location.href="addrelationship.php";		
	}else{
		if( $('.trSelected',grid).length<=0 ||  $('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=$('.trSelected',grid);		
		if (com=='Edit'){
			location.href="addrelationship.php?id="+items[0].id.substr(3);			
		}
		if (com=='Delete'){
			if(confirm("Are you sure, you want to delete selected Category?")){
					
				$.post("addrelationship.php", {"delete":"yes","id":items[0].id.substr(3)},function(data)
				{		 
				  if(data!="") {					 
					 $('#flex1').flexOptions().flexReload(); 
				  }else {
					  alert("Record doesn't exists.")
				  }
				  
				});		
			}			
		}	
	}
}

function showItemColor(){
$("#flex1").flexigrid({
	url: 'item_color.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Color Code', name : 'color_code', width : 250, sortable : true, align: 'left'},
		{display: 'Color Description', name : 'status', width : 250, sortable : true, align: 'left'},
		{display: 'Status', name : 'status', width : 100, sortable : true, align: 'left'}
		],
	buttons : [
		{name: 'Add', bclass: 'add', onpress : eventHandlerItemColor},
		{separator: true},		
		{name: 'Edit', bclass: 'edit', onpress : eventHandlerItemColor},
		{separator: true}
		
		],
	searchitems : [
		{display: 'Color Code', name : 'color_code', isdefault: true},
		{display: 'Color Description', name : 'category_code'}
		
		],
	sortname: "color_code",
	sortorder: "asc",
	usepager: true,
	title: 'Item Colors',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 700,
	height: 'auto'
});  
}
function eventHandlerItemColor(com,grid)
{
	if (com=='Add')	{
		location.href="add_color.php";		
	}else{
		if( $('.trSelected',grid).length<=0 ||  $('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=$('.trSelected',grid);		
		if (com=='Edit'){
			location.href="add_color.php?id="+items[0].id.substr(3);			
		}
		if (com=='Delete'){
			if(confirm("Are you sure, you want to delete selected Category?")){
					
				$.post("category.php", {"delete":"yes","id":items[0].id.substr(3)},function(data)
				{		 
				  if(data!="") {					 
					 $('#flex1').flexOptions().flexReload(); 
				  }else {
					  alert("Record doesn't exists.")
				  }
				  
				});		
			}			
		}	
	}
}


function showItem(){
$("#flex1").flexigrid({
	url: 'item.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Item Name', name : 'item_name', width : 150, sortable : true, align: 'left'},
		{display: 'Brand Name', name : 'brand_name', width : 80, sortable : true, align: 'left'},		
		{display: 'Category', name : 'category_name', width : 80, sortable : true, align: 'left'},		
		{display: 'Weight', name : 'item_size', width : 40, sortable : true, align: 'left'},
		{display: 'Item Code', name : 'item_code', width : 100, sortable : true, align: 'left'},
		{display: 'Division', name : 'division_name', width : 100, sortable : true, align: 'left'},
		{display: 'Segment', name : 'segment_name', width : 100, sortable : true, align: 'left'},
		{display: 'Warranty', name : 'item_warranty', width : 100, sortable : true, align: 'left'},
		{display: 'Prodata', name : 'item_prodata', width : 100, sortable : true, align: 'left'},
		{display: 'Grace', name : 'item_grace', width : 100, sortable : true, align: 'left'},
		{display: 'Offer Type', name : 'offer_name', width : 100, sortable : true, align: 'left'},
		{display: 'MRP/DP Price Detail', name : 'price_detail', width : 100, sortable : true, align: 'left'},
		{display: 'PTR Price Detail', name : 'price_detail', width : 100, sortable : true, align: 'left'},
		{display: 'Status', name : 'status', width : 225, sortable : true, align: 'left'}
		],
	buttons : [
		{name: 'Add', bclass: 'add', onpress : eventHandlerItem},
		{separator: true},		
		{name: 'Edit', bclass: 'edit', onpress : eventHandlerItem},
		{separator: true},		
		{name: 'Import PTR Price Detail', bclass: 'import', onpress : eventHandlerItem},
		{separator: true}
		
		],
	searchitems : [
		{display: 'Item Name', name : 'item_name', isdefault: true},
		{display: 'Item Code', name : 'item_code'},
		{display: 'Division', name : 'division_name'},
		{display: 'Segment', name : 'segment_name'},
		{display: 'Brand Name', name : 'brand_name'},
		{display: 'Category', name : 'category_name'}
		
		],
	sortname: "item_name",
	sortorder: "asc",
	usepager: true,
	title: 'Item',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 700,
	height: 'auto'
});  
}
function eventHandlerItem(com,grid)
{
	if (com=='Import PTR Price Detail')	{
		location.href="import_item_ptr.php";	
		//location.href="#";	
	}
	else if (com=='Export')	{
		location.href="export.inc.php?export_item";		
	}
	else if (com=='Add')	{
		location.href="addItem.php";		
	}else{
		if( $('.trSelected',grid).length<=0 ||  $('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=$('.trSelected',grid);		
		if (com=='Edit'){
			location.href="addItem.php?id="+items[0].id.substr(3);			
		}
		if (com=='Delete'){
			if(confirm("Are you sure, you want to delete selected item?")){
					
				$.post("item.php", {"delete":"yes","id":items[0].id.substr(3)},function(data)
				{		 
				  if(data!="") {					 
					 $('#flex1').flexOptions().flexReload(); 
				  }else {
					  alert("Record doesn't exists.")
				  }
				  
				});		
			}			
		}	
	}
}
function showDiscount(){
$("#flex1").flexigrid({
	url: 'discount.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Description', name : 'discount_desc', width : 150, sortable : true, align: 'left'},
		{display: 'Scheme Mode', name : 'mode', width : 100, sortable : true, align: 'left'},
		{display: 'Scheme', name : 'discount', width : 100, sortable : true, align: 'left'},
		{display: 'Primary/Secondary', name : 'mode', width : 100, sortable : true, align: 'left'},
		{display: 'Item Type', name : 'item_type', width : 100, sortable : true, align: 'left'},
		{display: 'Party Type', name : 'price_detail', width : 100, sortable : true, align: 'left'},
		{display: 'Scheme Type', name : 'discount_type', width : 100, sortable : true, align: 'left'},
		//{display: 'Minimum Amount', name : 'minimum_amount', width : 100, sortable : true, align: 'left'},
		//{display: 'Minimum Quantity', name : 'minimum_quantity', width : 100, sortable : true, align: 'left'},
		{display: 'Start Date', name : 'start_date', width : 100, sortable : true, align: 'left'},
		{display: 'End Date', name : 'end_date', width : 100, sortable : true, align: 'left'},
		{display: 'Status', name : 'status', width : 50, sortable : true, align: 'left'}
		],
	buttons : [
		{name: 'Add', bclass: 'add', onpress : eventHandlerDiscount},
		{separator: true},		
		{name: 'Edit', bclass: 'edit', onpress : eventHandlerDiscount},
		{separator: true},
		{name: 'Delete', bclass: 'delete', onpress : eventHandlerDiscount},
		{separator: true}
		/*,
		{name: 'Sms Schedule', bclass: 'sms', onpress : eventHandlerDiscount},
		{separator: true}*/
		
		],
	searchitems : [
		{display: 'Description', name : 'discount_desc', isdefault: true}
		
		],
	sortname: "discount_id",
	sortorder: "Desc",
	usepager: true,
	title: 'Scheme',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 1000,
	height: 'auto'
});  
}
function eventHandlerDiscount(com,grid)
{
	if (com=='Import')	{
		//location.href="import_item.php";	
		location.href="#";	
	}
	else if (com=='Export')	{
		location.href="export.inc.php?export_item";		
	}
	else if (com=='Add')	{
		location.href="adddiscount.php";		
	}else{
		if( $('.trSelected',grid).length<=0 ||  $('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=$('.trSelected',grid);	
		if (com=='Sms Schedule')	{
			var id = items[0].id.substr(3);
			//alert(id);
			var ids = btoa(id);
			//alert(atob(ids));
			window.open("sms_schedule.php?sid="+ids,'_newtab');
			//location.href="sms_schedule.php?sid="+ids;	
			} 
		if (com=='Edit'){
			location.href="editdiscount.php?id="+items[0].id.substr(3);			
		}
		if (com=='Delete'){
			if(confirm("Are you sure, you want to delete selected Scheme?")){
					
				$.post("discount.php", {"delete":"yes","id":items[0].id.substr(3)},function(data)
				{		 
				  if(data!="") {					 
					 $('#flex1').flexOptions().flexReload(); 
				  }else {
					  alert("Record doesn't exists.")
				  }
				  
				});		
			}			
		}	
	}
}

function showItemPrice(){
$("#flex1").flexigrid({
	url: 'item_price.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Item Name', name : 'item_name', width : 150, sortable : true, align: 'left'},
		{display: 'Item Code', name : 'item_code', width : 100, sortable : true, align: 'left'},
		{display: 'Category', name : 'category_name', width : 100, sortable : true, align: 'left'},
		{display: 'MRP', name : 'item_mrp', width : 80, sortable : true, align: 'left'},
		{display: 'DP', name : 'item_dp', width : 80, sortable : true, align: 'left'},
		{display: 'Start Date', name : 'start_date', width : 80, sortable : true, align: 'left'},
		{display: 'End Date', name : 'end_date', width : 80, sortable : true, align: 'left'},
		{display: 'Status', name : 'status', width : 50, sortable : true, align: 'left'}
		],
	buttons : [
		{name: 'Add', bclass: 'additemprice', onpress : eventHandlerItemPrice},
		{separator: true},		
		{name: 'Edit', bclass: 'edititemprice', onpress : eventHandlerItemPrice},
		{separator: true},
		{name: 'Back', bclass: 'back', onpress : eventHandlerItemPrice},
		{separator: true}
		],
	searchitems : [
		{display: 'Name', name : 'item_name', isdefault: true}
		
		],
	sortname: "start_date",
	sortorder: "asc",
	usepager: true,
	title: 'Item Price Detail',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 700,
	height: 'auto'
});  
}
function eventHandlerItemPrice(com,grid)
{
	if (com=='Add')	{
		location.href="addPrice.php";		
	}else
	if (com=='Back')	{
		location.href="item.php";		
	}else{
		if( $('.trSelected',grid).length<=0 ||  $('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=$('.trSelected',grid);		
		if (com=='Edit'){
			location.href="addPrice.php?id="+items[0].id.substr(3);			
		}
		if (com=='Delete'){
			if(confirm("Are you sure, you want to delete selected Price?")){
					
				$.post("item_price.php", {"delete":"yes","id":items[0].id.substr(3)},function(data)
				{		 
				  if(data!="") {					 
					 $('#flex1').flexOptions().flexReload(); 
				  }else {
					  alert("Record doesn't exists.")
				  }
				  
				});		
			}			
		}	
	}
}
function showItemPTR(){
$("#flex1").flexigrid({
	url: 'item_ptr.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Item Name', name : 'item_name', width : 150, sortable : true, align: 'left'},
		{display: 'Item Code', name : 'item_code', width : 80, sortable : true, align: 'left'},
		{display: 'Category', name : 'category_name', width : 80, sortable : true, align: 'left'},
		{display: 'PTR Price', name : 'item_mrp', width : 80, sortable : true, align: 'left'},
		{display: 'State', name : 'item_dp', width : 80, sortable : true, align: 'left'},
		{display: 'City', name : 'item_dp', width : 80, sortable : true, align: 'left'},
		{display: 'Start Date', name : 'start_date', width : 80, sortable : true, align: 'left'},
		{display: 'End Date', name : 'end_date', width : 80, sortable : true, align: 'left'},
		{display: 'Status', name : 'status', width : 50, sortable : true, align: 'left'}
		],
	buttons : [
		{name: 'Add', bclass: 'additemptr', onpress : eventHandlerItemPTR},
		{separator: true},		
		{name: 'Edit', bclass: 'edititemptr', onpress : eventHandlerItemPTR},
		{separator: true},
		{name: 'Back', bclass: 'backitemptr', onpress : eventHandlerItemPTR},
		{separator: true}
		],
	searchitems : [
		{display: 'Name', name : 'item_name', isdefault: true}
		
		],
	sortname: "start_date",
	sortorder: "asc",
	usepager: true,
	title: 'Item PTR Price Detail',
	useRp: true,
	rp: 15,
	showTableToggleBtn: true,
	width: 700,
	height: 375
});  
}
function eventHandlerItemPTR(com,grid)
{
	if (com=='Add')	{
		location.href="add_ptr.php";		
	}else
	if (com=='Back')	{
		location.href="item.php";		
	}else{
		if( $('.trSelected',grid).length<=0 ||  $('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=$('.trSelected',grid);		
		if (com=='Edit'){
			location.href="add_ptr.php?id="+items[0].id.substr(3);			
		}
		if (com=='Delete'){
			if(confirm("Are you sure, you want to delete selected Price?")){
					
				$.post("item_price.php", {"delete":"yes","id":items[0].id.substr(3)},function(data)
				{		 
				  if(data!="") {					 
					 $('#flex1').flexOptions().flexReload(); 
				  }else {
					  alert("Record doesn't exists.")
				  }
				  
				});		
			}			
		}	
	}
}

function showDistributors(){
$("#flex1").flexigrid({
	url: 'distributors.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Name', name : 'distributor_name', width : 80, sortable : true, align: 'left'},
		{display: 'Distributor Code', name : 'distributor_code', width : 80, sortable : true, align: 'left'},
		{display: 'Division', name : 'division_name', width : 80, sortable : true, align: 'left'},
		{display: 'Distributor Class', name : 'relationship_code', width : 80, sortable : true, align: 'left'},
		{display: 'Added by(Salesman)', name : 'salesman_name', width : 80, sortable : true, align: 'left'},
		{display: 'Salesman Code', name : 'salesman_code', width : 80, sortable : true, align: 'left'},
		{display: 'Distributor Region', name : 'region_name', width : 92, sortable : true, align: 'left'},
		{display: 'State', name : 'state_name', width : 80, sortable : true, align: 'left'},
		{display: 'District', name : 'city_name', width : 80, sortable : true, align: 'left'},
		{display: 'Taluka', name : 'taluka_name', width : 80, sortable : true, align: 'left'},
		{display: 'City', name : 'distributor_location', width : 100, sortable : true, align: 'left'},
		{display: 'Pincode', name : 'pin_code', width : 100, sortable : true, align: 'left'},
		{display: 'Address 1', name : 'distributor_address', width : 100, sortable : true, align: 'left'},
		{display: 'Address 2', name : 'distributor_address2', width : 100, sortable : true, align: 'left'},
		{display: 'Send SMS Mobile No', name : 'sms_number', width : 100, sortable : true, align: 'left'},
		{display: 'Photo', name : '', width : 100, sortable : true, align: 'left'},
		{display: 'Phone Number 1', name : 'distributor_phone_no', width : 80, sortable : true, align: 'left'},
		{display: 'Phone Number 2', name : 'distributor_phone_no2', width : 80, sortable : true, align: 'left'},
		{display: 'Phone Number 3', name : 'distributor_phone_no3', width : 80, sortable : true, align: 'left'},
		{display: 'Landline Number', name : 'distributor_leadline_no', width : 80, sortable : true, align: 'left'},
		{display: 'Contact Person 1', name : 'contact_person', width : 80, sortable : true, align: 'left'},
		{display: 'Contact Number 1', name : 'contact_number', width : 80, sortable : true, align: 'left'},
		{display: 'Contact Person 2', name : 'contact_person2', width : 80, sortable : true, align: 'left'},
		{display: 'Contact Number 2', name : 'contact_number2', width : 80, sortable : true, align: 'left'},
		{display: 'Contact Person 3', name : 'contact_person3', width : 80, sortable : true, align: 'left'},
		{display: 'Contact Number 3', name : 'contact_number3', width : 80, sortable : true, align: 'left'},
		{display: 'Username', name : 'username', width : 80, sortable : true, align: 'left'},
		{display: 'Email ID 1', name : 'distributor_email', width : 100, sortable : true, align: 'left'},
		{display: 'Email ID 2', name : 'distributor_email2', width : 100, sortable : true, align: 'left'},
		{display: 'Email ID 3', name : 'distributor_email3', width : 100, sortable : true, align: 'left'},
		{display: 'Date of Birth', name : 'distributor_dob', width : 80, sortable : true, align: 'left'},
		{display: 'Start Date', name : 'start_date', width : 80, sortable : true, align: 'left'},
		{display: 'End Date', name : 'end_date', width : 80, sortable : true, align: 'left'},
		{display: 'Distributor Status', name : 'status', width : 80, sortable : true, align: 'center'},
		{display: 'Login Status', name : 'loginStatus', width : 80, sortable : true, align: 'center'},
		{display: 'App Version', name : 'appStatus', width : 80, sortable : true, align: 'center'}
			
		],
	buttons : [
		{name: 'Add', bclass: 'add', onpress : eventHandlerDisAdminUser},
		{separator: true},		
		{name: 'Edit', bclass: 'edit', onpress : eventHandlerDisAdminUser},
		{separator: true},
		{name: 'Delete', bclass: 'delete', onpress : eventHandlerDisAdminUser},
		{separator: true}
		],
	searchitems : [
		{display: 'Name', name : 'distributor_name', isdefault: true},
		{display: 'Division', name : 'division_name'},
		{display: 'Username', name : 'username'},
		{display: 'Division', name : 'division'},
		{display: 'Email ID', name : 'distributor_email'},
		{display: 'Send SMS Mobile No', name : 'sms_number'},
		{display: 'Phone Number', name : 'distributor_phone_no'},
		{display: 'State', name : 'state_name'},
		{display: 'District', name : 'city_name'},
		{display: 'Taluka', name : 'taluka_name'},
		{display: 'City', name : 'market_name'},
		{display: 'Address', name : 'distributor_address'},
		{display: 'City', name : 'distributor_location'},
		{display: 'Contact Person', name : 'contact_person'},
		{display: 'Contact Number', name : 'contact_number'}
		],
	sortname: "distributor_name",
	sortorder: "asc",
	usepager: true,
	title: 'Distributors',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 700,
	height: 'auto'
});  
}
/*function eventHandlerDisAdminUser(com,grid)
{
	if (com=='Add')	{
		location.href="distributors.php?add";	
	}else if (com=='Import')	{
		location.href="distributors.php?import";	
	}else if (com=='Export')	{
		location.href="export.inc.php?export_distributors_list";	
	}
	else{
		if( $('.trSelected',grid).length<=0 ||  $('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=$('.trSelected',grid);		
		if (com=='Edit'){
			location.href="distributors.php?id="+items[0].id.substr(3);			
		}else if (com=='Create Login')	{
		location.href="#.php?login&id="+items[0].id.substr(3);
	}
			
	}
}*/


function showRetailer(){
$("#flex1").flexigrid({
	url: 'retailer.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Retailer Name', name : 'retailer_name', width : 80, sortable : true, align: 'left'},
		{display: 'Retailer Class', name : 'relationship_code', width : 80, sortable : true, align: 'left'},
		{display: 'Division', name : 'division_name', width : 80, sortable : true, align: 'left'},
		{display: 'Retailer Code', name : 'retailer_code', width : 80, sortable : true, align: 'left'},
		{display: 'Retailer Type', name : 'type_name', width : 80, sortable : true, align: 'left'},
		{display: 'Retailer Channel', name : 'channel_name', width : 80, sortable : true, align: 'left'},
		{display: 'Added by(Salesman)', name : 'salesman_name', width : 80, sortable : true, align: 'left'},
		{display: 'Salesman Code', name : 'salesman_code', width : 80, sortable : true, align: 'left'},
		{display: 'Distributor', name : 'distributor_id', width : 80, sortable : true, align: 'left'},
		{display: 'Interested', name : 'display_outlet', width : 80, sortable : true, align: 'left'},
		{display: 'App Version', name : 'app_version', width : 80, sortable : true, align: 'left'},
		{display: 'State', name : 'state_name', width : 80, sortable : true, align: 'left'},
		{display: 'District', name : 'city_name', width : 80, sortable : true, align: 'left'},
		
		{display: 'City', name : 'market_name', width : 100, sortable : true, align: 'left'},
		{display: 'Pincode', name : 'pin_code', width : 100, sortable : true, align: 'left'},
		{display: 'Map', name : 'retailer_location', width : 100, sortable : true, align: 'left'},
		{display: 'Photo', name : '', width : 100, sortable : true, align: 'left'},
		{display: 'Address 1', name : 'retailer_address', width : 100, sortable : true, align: 'left'},
		{display: 'Address 2', name : 'retailer_address2', width : 100, sortable : true, align: 'left'},
		{display: 'Phone Number 1', name : 'retailer_phone_no', width : 80, sortable : true, align: 'left'},
		{display: 'Phone Number 2', name : 'retailer_phone_no2', width : 80, sortable : true, align: 'left'},
		{display: 'Leadline Number', name : 'retailer_leadline_no', width : 80, sortable : true, align: 'left'},
		{display: 'Aadhar Number', name : 'aadhar_no', width : 80, sortable : true, align: 'left'},
		{display: 'Pan Number', name : 'pan_no', width : 80, sortable : true, align: 'left'},

		{display: 'Contact Person 1', name : 'contact_person', width : 80, sortable : true, align: 'left'},
		{display: 'Contact Number 1', name : 'contact_number', width : 80, sortable : true, align: 'left'},
		{display: 'Contact Person 2', name : 'contact_person2', width : 80, sortable : true, align: 'left'},
		{display: 'Contact Number 2', name : 'contact_number2', width : 80, sortable : true, align: 'left'},
		{display: 'Username', name : 'username', width : 80, sortable : true, align: 'left'},
		{display: 'Email ID 1', name : 'retailer_email', width : 100, sortable : true, align: 'left'},
		{display: 'Email ID 2', name : 'retailer_email2', width : 100, sortable : true, align: 'left'},
		{display: 'Date of Birth', name : 'retailer_dob', width : 80, sortable : true, align: 'left'},
		{display: 'Start Date', name : 'start_date', width : 80, sortable : true, align: 'left'},
		{display: 'End Date', name : 'end_date', width : 80, sortable : true, align: 'left'},
		{display: 'Retailer Status', name : 'status', width : 80, sortable : true, align: 'center'},
		{display: 'Login Status', name : 'loginStatus', width : 80, sortable : true, align: 'center'},
		{display: 'Validation Status', name : 'validation_status', width : 80, sortable : true, align: 'center'},
		{display: 'Not Validation Reason', name : 'validation_reason', width : 80, sortable : true, align: 'center'},
		{display: 'Remark', name : 'remark', width : 80, sortable : true, align: 'left'}
			
		],
	buttons : [
		{name: 'Add', bclass: 'add', onpress : eventHandlerRetAdminUser},
		{separator: true},		
		{name: 'Edit', bclass: 'edit', onpress : eventHandlerRetAdminUser},
		{separator: true},
		{name: 'Delete', bclass: 'delete', onpress : eventHandlerRetAdminUser},
		{separator: true},
		{name: 'Export Number of Photos', bclass: 'showpic', onpress : eventHandlerRetAdminUser},
		{separator: true}	
		],
	searchitems : [
		{display: 'Retailer Name', name : 'retailer_name', isdefault: true},
		{display: 'Division', name : 'd.division_name'},
		{display: 'Address', name : 'retailer_address'},
		//{display: 'Division', name : 'division'},
		{display: 'Retailer Class', name : 'relationship_code'},
		{display: 'Retailer Type', name : 'tm.type_name'},
		{display: 'Retailer Channel', name : 'channel_name'},
		{display: 'Distributor', name : 'distributor_name'},
		{display: 'City', name : 'market_name'},
		{display: 'Taluka', name : 'taluka_name'},
		{display: 'District', name : 'city_name'},
		{display: 'State', name : 'state_name'},
		{display: 'Username', name : 'username'},
		{display: 'Email ID', name : 'email_id'},
		{display: 'Phone Number', name : 'retailer_phone_no'},
		{display: 'Contact Person', name : 'contact_person'},
		{display: 'Contact Number', name : 'contact_number'}
		],
	sortname: "retailer_name",
	sortorder: "asc",
	usepager: true,
	title: 'Retailer',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 700,
	height: 'auto'
});  
}
function eventHandlerRetAdminUser(com,grid)
{
	var searchParam_1 = $('input[name=q]').val();
	var searchParam_2 = $('select[name=qtype]').val();
	var searchParam_3 = $(".pcontrol").find("input").val();
	if (com=='Add')	{
		location.href="retailer.php?add=''"+"&searchParam_1="+searchParam_1+"&searchParam_2="+searchParam_2+"&searchParam_3="+searchParam_3;	
	}else if (com=='Export Number of Photos')	{
		location.href="export.inc.php?export_relailer_photo";	
	}else if (com=='Export')	{
		location.href="export.inc.php?export_relailer_list";	
	}
	else{
		if( $('.trSelected',grid).length<=0 ||  $('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=$('.trSelected',grid);		
		if (com=='Edit'){
			location.href="retailer.php?id="+items[0].id.substr(3)+"&searchParam_1="+searchParam_1+"&searchParam_2="+searchParam_2+"&searchParam_3="+searchParam_3;			
		}else if (com=='Create Login')	{
		location.href="#.php?login&id="+items[0].id.substr(3);
	}
	else if (com=='Delete')	{
		if(confirm("Are you sure, you want to delete selected Retailer?")){
					
				$.post("retailer.php", {"delete":"yes","id":items[0].id.substr(3)},function(data)
				{
					$("#message-green").hide();
				  if(data!="") {					 
					 $('#flex1').flexOptions().flexReload(); 
				  }else {
					  alert("Record doesn't exists.")
				  }
				  
				});		
			}
	}
			
	}
}
function showNewRetailer(){
$("#flex1").flexigrid({
	url: 'new_retailer.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Retailer Name', name : 'retailer_name', width : 80, sortable : true, align: 'left'},
		{display: 'Division', name : 'division_name', width : 80, sortable : true, align: 'left'},
		{display: 'Added by(Salesman)', name : 'salesman_name', width : 80, sortable : true, align: 'left'},
		{display: 'Salesman Code', name : 'salesman_code', width : 80, sortable : true, align: 'left'},
		{display: 'Added Date', name : 'start_date', width : 80, sortable : true, align: 'left'},
		{display: 'Order', name : 'order', width : 80, sortable : true, align: 'left'},
		{display: 'State', name : 'state_name', width : 80, sortable : true, align: 'left'},
		{display: 'City', name : 'city_name', width : 80, sortable : true, align: 'left'},
		{display: 'Taluka', name : 'taluka_name', width : 80, sortable : true, align: 'left'},
		{display: 'Market', name : 'market_name', width : 100, sortable : true, align: 'left'},

		{display: 'Aadhar Number', name : 'aadhar_no', width : 100, sortable : true, align: 'left'},
		{display: 'Pan Number', name : 'pan_no', width : 100, sortable : true, align: 'left'},
		
		{display: 'Map', name : 'retailer_location', width : 100, sortable : true, align: 'left'},
		{display: 'Photo', name : '', width : 100, sortable : true, align: 'left'},
		{display: 'Address', name : 'retailer_address', width : 100, sortable : true, align: 'left'},
		{display: 'Phone Number', name : 'retailer_phone_no', width : 80, sortable : true, align: 'left'},
		{display: 'Contact Person', name : 'contact_person', width : 80, sortable : true, align: 'left'},
		{display: 'Contact Number', name : 'contact_number', width : 80, sortable : true, align: 'left'},
		{display: 'Date of Birth', name : 'retailer_dob', width : 80, sortable : true, align: 'left'},
		{display: 'Retailer Status', name : 'status', width : 80, sortable : true, align: 'center'}
			
		],
	buttons : [	
		{name: 'Edit', bclass: 'edit', onpress : eventHandlerRetNewAdminUser},
		{separator: true},
		{name: 'Delete', bclass: 'delete', onpress : eventHandlerRetNewAdminUser},
		{separator: true}
		],
	searchitems : [
		{display: 'Retailer Name', name : 'retailer_name', isdefault: true},
		{display: 'Division', name : 'division_name'},
		{display: 'Added By', name : 'salesman_name'},
		{display: 'Added Date', name : 'r.start_date'},
		{display: 'Address', name : 'retailer_address'},
		{display: 'City', name : 'city_name'},
		{display: 'Market', name : 'market_name'},
		{display: 'Taluka', name : 'taluka_name'},
		{display: 'State', name : 'state_name'},
		/*{display: 'Username', name : 'username'},
		{display: 'Email ID', name : 'email_id'},*/
		{display: 'Phone Number', name : 'retailer_phone_no'},
		{display: 'Contact Person', name : 'contact_person'},
		{display: 'Contact Number', name : 'contact_number'}
		],
	sortname: "start_date",
	sortorder: "desc",
	usepager: true,
	title: 'New Retailer',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 950,
	height: 'auto'
});  
}
function eventHandlerRetNewAdminUser(com,grid)
{
	var searchParam_1 = $('input[name=q]').val();
	var searchParam_2 = $('select[name=qtype]').val();
	var searchParam_3 = $(".pcontrol").find("input").val();
	if (com=='Add')	{
		location.href="retailer.php?add";	
	}else if (com=='Import')	{
		location.href="retailer.php?import";	
	}else if (com=='Export')	{
		location.href="export.inc.php?export_relailer_list";	
	}
	else{
		if( $('.trSelected',grid).length<=0 ||  $('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=$('.trSelected',grid);		
		if (com=='Edit'){
			location.href="retailer.php?ap=1&id="+items[0].id.substr(3)+"&searchParam_1="+searchParam_1+"&searchParam_2="+searchParam_2+"&searchParam_3="+searchParam_3;		
		}else if (com=='Delete')	{
		if(confirm("Are you sure, you want to delete selected Retailer?")){
					
				$.post("new_retailer.php", {"delete":"yes","id":items[0].id.substr(3)},function(data)
				{		 
				  if(data!="") {					 
					 $('#flex1').flexOptions().flexReload(); 
				  }else {
					  alert("Record doesn't exists.")
				  }
				  
				});		
			}
	}
			
	}
}


function showSalsman(checkhierarchyenable){
	
//alert(checkhierarchyenable);

if(checkhierarchyenable == true){
var colModel01 = [ 		
		{display: 'Name', name : 'salesman_name', width : 120, sortable : true, align: 'left'},
		{display: 'User Level', name : 'des1', width : 50, sortable : true, align: 'left'},
		{display: 'Reporting to', name : 'des2', width : 80, sortable : true, align: 'left'},
		{display: 'Reporting Person', name : 'username', width : 120, sortable : true, align: 'left'},
		{display: 'Username', name : 'username', width : 120, sortable : true, align: 'left'},
		{display: 'Salesman Code', name : 'salesman_code', width : 120, sortable : true, align: 'left'},
		{display: 'Segment', name : 'division_id', width : 120, sortable : true, align: 'left'},
		{display: 'Email ID', name : 'email_id', width : 120, sortable : true, align: 'left'},
		{display: 'Phone Number', name : 'salesman_phome_no', width : 120, sortable : true, align: 'left'},
		{display: 'Address', name : 'salesman_address', width : 120, sortable : true, align: 'left'},
		{display: 'Start Date', name : 'start_date', width : 80, sortable : true, align: 'left'},
		{display: 'End Date', name : 'end_date', width : 80, sortable : true, align: 'left'},
		{display: 'Salesman Status', name : 'status', width : 80, sortable : true, align: 'center'},
		{display: 'Login Status', name : 'loginStatus', width : 80, sortable : true, align: 'center'}
			
		];
} else {
var colModel01 = [ 		
		{display: 'Name', name : 'salesman_name', width : 120, sortable : true, align: 'left'},
		{display: 'Username', name : 'username', width : 120, sortable : true, align: 'left'},
		{display: 'Email ID', name : 'email_id', width : 120, sortable : true, align: 'left'},
		{display: 'Phone Number', name : 'salesman_phome_no', width : 120, sortable : true, align: 'left'},
		{display: 'Address', name : 'salesman_address', width : 120, sortable : true, align: 'left'},
		{display: 'Start Date', name : 'start_date', width : 80, sortable : true, align: 'left'},
		{display: 'End Date', name : 'end_date', width : 80, sortable : true, align: 'left'},
		{display: 'Salesman Status', name : 'status', width : 80, sortable : true, align: 'center'},
		{display: 'Login Status', name : 'loginStatus', width : 80, sortable : true, align: 'center'}
			
		];	
	
}

	
$("#flex1").flexigrid({
	url: 'salesman.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel    : colModel01, 
	buttons : [
		{name: 'Add', bclass: 'add', onpress : eventHandlerSalAdminUser},
		{separator: true},		
		{name: 'Edit', bclass: 'edit', onpress : eventHandlerSalAdminUser},
		{separator: true},
		{name: 'Delete', bclass: 'delete', onpress : eventHandlerSalAdminUser},
		{separator: true},
		{name: 'View Salesman Schedule', bclass: 'schedule', onpress : eventHandlerSalAdminUser},
		{separator: true}
		],
	searchitems : [
		{display: 'Name', name : 's.salesman_name', isdefault: true},
		{display: 'Username', name : 'w.username'},
		{display: 'Email ID', name : 'w.email_id'},
		{display: 'Phone Number', name : 's.salesman_phome_no'},
		{display: 'Address', name : 's.salesman_address'}
		],
	sortname: "s.salesman_name",
	sortorder: "asc",
	usepager: true,
	title: 'Salesman',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 700,
	height: 'auto'
});  
}
function eventHandlerSalAdminUser(com,grid)
{
	if (com=='Add')	{
		location.href="salesman.php?add";	
	}else if (com=='Import')	{
		location.href="salesman.php?import";	
	}else if (com=='Export')	{
		location.href="export.inc.php?export_salesman_list";	
	}
	else{
		if( $('.trSelected',grid).length<=0 ||  $('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=$('.trSelected',grid);		
		if (com=='Edit'){
			location.href="salesman.php?id="+items[0].id.substr(3);			
		}else {
			if(com=='View Salesman Schedule'){
			location.href="salesman.php?schid="+items[0].id.substr(3);	
			}
		}
		if (com=='Delete'){
			if(confirm("Are you sure, you want to delete selected Salesman?")){
					
				$.post("salesman.php", {"delete":"yes","id":items[0].id.substr(3)},function(data)
				{	
				   $('#message-green').hide();
				  if(data!="") {					 
					 $('#flex1').flexOptions().flexReload(); 
				  }else {
					  alert("Record doesn't exists.")
				  }
				  
				});		
			}			
		}
	}			
}

/*function showSalsman(){
$("#flex1").flexigrid({
	url: 'salesman.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Name', name : 'salesman_name', width : 120, sortable : true, align: 'left'},
		{display: 'Username', name : 'username', width : 120, sortable : true, align: 'left'},
		{display: 'Email ID', name : 'email_id', width : 120, sortable : true, align: 'left'},
		{display: 'Phone Number', name : 'salesman_phome_no', width : 120, sortable : true, align: 'left'},
		{display: 'Address', name : 'salesman_address', width : 120, sortable : true, align: 'left'},
		{display: 'Start Date', name : 'start_date', width : 80, sortable : true, align: 'left'},
		{display: 'End Date', name : 'end_date', width : 80, sortable : true, align: 'left'},
		{display: 'Salesman Status', name : 'status', width : 80, sortable : true, align: 'center'},
		{display: 'Login Status', name : 'loginStatus', width : 80, sortable : true, align: 'center'}
			
		],
	buttons : [
		{name: 'Add', bclass: 'add', onpress : eventHandlerSalAdminUser},
		{separator: true},		
		{name: 'Edit', bclass: 'edit', onpress : eventHandlerSalAdminUser},
		{separator: true},
		{name: 'Delete', bclass: 'delete', onpress : eventHandlerSalAdminUser},
		{separator: true},
		{name: 'View Salesman Schedule', bclass: 'edit', onpress : eventHandlerSalAdminUser},
		{separator: true}
		],
	searchitems : [
		{display: 'Name', name : 'salesman_name', isdefault: true},
		{display: 'Username', name : 'username'},
		{display: 'Email ID', name : 'email_id'},
		{display: 'Phone Number', name : 'salesman_phome_no'},
		{display: 'Address', name : 'salesman_address'}
		],
	sortname: "salesman_name",
	sortorder: "asc",
	usepager: true,
	title: 'Salesman',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 700,
	height: 'auto'
});  
}
function eventHandlerSalAdminUser(com,grid)
{
	if (com=='Add')	{
		location.href="salesman.php?add";	
	}else if (com=='Import')	{
		location.href="salesman.php?import";	
	}else if (com=='Export')	{
		location.href="export.inc.php?export_salesman_list";	
	}
	else{
		if( $('.trSelected',grid).length<=0 ||  $('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=$('.trSelected',grid);		
		if (com=='Edit'){
			location.href="salesman.php?id="+items[0].id.substr(3);			
		}else {
			if(com=='View Salesman Schedule'){
			location.href="salesman.php?schid="+items[0].id.substr(3);	
			}
		}
		if (com=='Delete'){
			if(confirm("Are you sure, you want to delete selected Salesman?")){
					
				$.post("salesman.php", {"delete":"yes","id":items[0].id.substr(3)},function(data)
				{	
				   $('#message-green').hide();
				  if(data!="") {					 
					 $('#flex1').flexOptions().flexReload(); 
				  }else {
					  alert("Record doesn't exists.")
				  }
				  
				});		
			}			
		}
	}			
}*/

function showManageUser(){
$("#flex1").flexigrid({
	url: 'manage_user.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Name', name : 'operator_name', width : 100, sortable : true, align: 'left'},
		{display: 'Username', name : 'username', width : 100, sortable : true, align: 'left'},
		{display: 'Email ID', name : 'email_id', width : 100, sortable : true, align: 'left'},
		{display: 'Phone Number', name : 'operator_phone_number', width : 100, sortable : true, align: 'left'},
		{display: 'Manage Items', name : 'manage_items', width : 100, sortable : true, align: 'left'},
		{display: 'Manage Distributors', name : 'manage_distributors', width : 100, sortable : true, align: 'left'},
		{display: 'Manage Retailer', name : 'manage_retailer', width : 100, sortable : true, align: 'left'},
		{display: 'Manage Salesman', name : 'manage_salesman', width : 100, sortable : true, align: 'left'},
		{display: 'Manage Route', name : 'manage_route', width : 100, sortable : true, align: 'left'},
		{display: 'Manage Retailer Relationship', name : 'manage_route', width : 100, sortable : true, align: 'left'},
		{display: 'Start Date', name : 'start_date', width : 80, sortable : true, align: 'left'},
		{display: 'End Date', name : 'end_date', width : 80, sortable : true, align: 'left'},
		{display: 'Users Status', name : 'status', width : 80, sortable : true, align: 'center'},
		{display: 'Login Status', name : 'loginStatus', width : 80, sortable : true, align: 'center'}
			
		],
	buttons : [
		{name: 'Add', bclass: 'add', onpress : eventHandlerManageUser},
		{separator: true},		
		{name: 'Edit', bclass: 'edit', onpress : eventHandlerManageUser},
		{separator: true},
		{name: 'Export', bclass: 'export', onpress : eventHandlerManageUser},
		{separator: true}
		],
	searchitems : [
		{display: 'Name', name : 'operator_name', isdefault: true},
		{display: 'Username', name : 'username'},
		{display: 'Email ID', name : 'email_id'},
		{display: 'Manage Items', name : 'manage_items'},
		{display: 'Manage Distributors', name : 'manage_distributors'},
		{display: 'Manage Retailer', name : 'manage_retailer'},
		{display: 'Manage Salesman', name : 'manage_salesman'},
		{display: 'Manage Route', name : 'manage_route'}
		],
	sortname: "operator_name",
	sortorder: "asc",
	usepager: true,
	title: 'Company Users',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 750,
	height: 'auto'
});  
}
function eventHandlerManageUser(com,grid)
{

	if (com=='Add')	{
		location.href="manage_user.php?add";	
	}else if (com=='Import')	{
		location.href="salesman.php?import";	
	}else if (com=='Export')	{
		location.href="export.inc.php?export_manageuser_list";	
	}
	else{
		if( $('.trSelected',grid).length<=0 ||  $('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=$('.trSelected',grid);		
		if (com=='Edit'){
			location.href="manage_user.php?id="+items[0].id.substr(3);			
		}else if (com=='Create Login')	{
		location.href="#.php?login&id="+items[0].id.substr(3);
	}
			
	}
}
function showUserList(){
$("#flex1").flexigrid({
	url: 'users_list.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Name', name : 'name', width : 140, sortable : true, align: 'left'},
		{display: 'Username', name : 'username', width : 140, sortable : true, align: 'left'},
		{display: 'Email ID', name : 'email_id', width : 140, sortable : true, align: 'left'},
		{display: 'Phone Number', name : 'salesman_phome_no', width : 140, sortable : true, align: 'left'},
		{display: 'User Type', name : 'user_type', width : 140, sortable : true, align: 'left'},
		{display: 'Login Status', name : 'status', width : 80, sortable : true, align: 'center'}
			
		],
	buttons : [
		
		],
	searchitems : [
		{display: 'Username', name : 'username', isdefault: true},
		{display: 'Email ID', name : 'email_id'},
		{display: 'Phone Number', name : 'salesman_phome_no'},
		{display: 'User Type', name : 'user_type'}
		],
	sortname: "web_user_id",
	sortorder: "asc",
	usepager: true,
	title: 'Users',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 850,
	height: 'auto'
});  
}
function eventHandlerUserListAdminUser(com,grid)
{
	if (com=='Add')	{
		location.href="#.php?add";	
	}
	else{
		if( $('.trSelected',grid).length<=0 ||  $('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=$('.trSelected',grid);		
		if (com=='Edit'){
			location.href="users_list.php?id="+items[0].id.substr(3);			
		}	
	}
}
function showRoute(){
$("#flex1").flexigrid({
	url: 'route.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Name', name : 'route_name', width : 250, sortable : true, align: 'left'},
		{display: 'Retailer Counts', name : 'retailers', width : 100, sortable : true, align: 'left'},
		{display: 'Distributor Counts', name : 'distributors', width : 100, sortable : true, align: 'left'},
		/*{display: 'Shakti-Partner Counts', name : 'customers', width : 120, sortable : true, align: 'left'},*/
		{display: 'Option', name : 'option', width : 60, sortable : true, align: 'left'},
		{display: 'Status', name : 'status', width : 100, sortable : true, align: 'left'}
		],
	buttons : [
		{name: 'Add', bclass: 'add', onpress : eventHandlerRoute},
		{separator: true},		
		{name: 'Edit', bclass: 'edit', onpress : eventHandlerRoute},
		{separator: true},			
		{name: 'Delete', bclass: 'delete', onpress : eventHandlerRoute},
		{separator: true},
  		{name: 'Sort Retailers', bclass: 'Sort', onpress : eventHandlerRoute},
  		{separator: true}
		
		],
	searchitems : [
		{display: 'Name', name : 'route_name', isdefault: true}
		
		],
	sortname: "route_name",
	sortorder: "asc",
	usepager: true,
	title: 'Route',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 750,
	height: 'auto'
});  
}
function eventHandlerRoute(com,grid)
{
	if (com=='Import')	{
		location.href="route.php?import";		
	}
	else if (com=='Export')	{
		location.href="#.php?export_route";		
	}
	else if (com=='Add')	{
		location.href="add_route.php";		
	}else{
		if( $('.trSelected',grid).length<=0 ||  $('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=$('.trSelected',grid);		
		if (com=='Edit'){
			location.href="add_route.php?id="+items[0].id.substr(3);			
		}
		if (com=='Delete'){
			if(confirm("Are you sure, you want to delete selected Route?")){
					
				$.post("route.php", {"delete":"yes","id":items[0].id.substr(3)},function(data)
				{		 
				  if(data!="") {					 
					 $('#flex1').flexOptions().flexReload(); 
				  }else {
					  alert("Record doesn't exists.")
				  }
				  
				});		
			}			
		}
		if (com=='Sort Retailers'){   
   				location.href="retailer_ordering.php?rid="+items[0].id.substr(3);   
	  }
	}
}
function showRouteSchedule(){
$("#flex1").flexigrid({
	url: 'route_schedule.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Salesperson Name', name : 'salesman_name', width : 300, sortable : true, align: 'left'},
		{display: 'Month', name : 'month', width : 200, sortable : true, align: 'left'},
		{display: 'Year', name : 'year', width : 200, sortable : true, align: 'left'},
		{display: 'Status', name : 'status', width : 100, sortable : true, align: 'left'}
		],
	buttons : [
		{name: 'Add', bclass: 'add', onpress : eventHandlerRouteSchedule},
		{separator: true},		
		{name: 'Edit', bclass: 'edit', onpress : eventHandlerRouteSchedule},
		{separator: true}
		/*,
		{name: 'View Salesman Schedule', bclass: 'salschedule', onpress : eventHandlerRouteSchedule},
		{separator: true},
		{name: 'Export', bclass: 'export', onpress : eventHandlerRouteSchedule},
		{separator: true}*/
		
		],
	searchitems : [
		{display: 'Salesperson Name', name : 'salesman_name', isdefault: true},
		{display: 'Month', name : 'month', isdefault: true},
		{display: 'Year', name : 'year', isdefault: true}
		
		],
	sortname: "salesman_name",
	sortorder: "asc",
	usepager: true,
	title: 'Route Schedule',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 700,
	height: 'auto'
});  
}
function eventHandlerRouteSchedule(com,grid)
{
	if (com=='Export')	{
		location.href="#.php?export_route";		
	}
	else if (com=='Add')	{
		location.href="add_route_schedule.php";		
	}else{
		if( $('.trSelected',grid).length<=0 ||  $('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=$('.trSelected',grid);		
		if (com=='Edit'){
			location.href="add_route_schedule_edit.php?id="+items[0].id.substr(3);			
		}
		if (com=='View Salesman Schedule'){
			location.href="route_schedule.php?schid="+items[0].id.substr(3);			
		}
		if (com=='Delete'){
			if(confirm("Are you sure, you want to delete selected Category?")){
					
				$.post("category.php", {"delete":"yes","id":items[0].id.substr(3)},function(data)
				{		 
				  if(data!="") {					 
					 $('#flex1').flexOptions().flexReload(); 
				  }else {
					  alert("Record doesn't exists.")
				  }
				  
				});		
			}			
		}	
	}
}
function showOrderList(){
$("#flex1").flexigrid({
	url: 'admin_order_list.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [
		{display: 'Salesperson Name', name : 'salesman_name', width : 100, sortable : true, align: 'left'},
		{display: 'Reporting To', name : 'rpt_to', width : 100, sortable : true, align: 'left'},
		{display: 'Order Status', name : 'order_status', width : 100, sortable : true, align: 'left'},
		{display: 'Total Invoice Amount', name : 'acc_total_invoice_amount', width : 100, sortable : true, align: 'left'},
		{display: 'Total no of items', name : 'total_item', width : 80, sortable : true, align: 'left'},
		{display: 'Map', name : 'lat', width : 80, sortable : true, align: 'left'},
		{display: 'Retailer Photo', name : 'lat', width : 80, sortable : true, align: 'left'},
		{display: 'Survey Status', name : 'survey_status', width : 100, sortable : true, align: 'left'},
		{display: 'Visit Status', name : 'visit_status', width : 100, sortable : true, align: 'left'},
		{display: 'Reason', name : 'reason', width : 100, sortable : true, align: 'left'},
		{display: 'Retailer Name', name : 'retailer_name', width : 100, sortable : true, align: 'left'},
		{display: 'Retailer Class', name : 'retailer_class', width : 100, sortable : true, align: 'left'},
		{display: 'Retailer Division', name : 'retailer_division', width : 100, sortable : true, align: 'left'},
		{display: 'Retailer Pincode', name : 'rpin_code', width : 100, sortable : true, align: 'left'},
		{display: 'Order Number', name : 'order_id', width : 70, sortable : true, align: 'left'},
		{display: 'Date of Order', name : 'date_of_order', width : 100, sortable : true, align: 'left'},
		{display: 'Time of Order', name : 'time_of_order', width : 70, sortable : true, align: 'left'},
		{display: 'Salesperson Code', name : 'salesman_code', width : 100, sortable : true, align: 'left'},
		{display: 'Reporting Person Code', name : 'rpt_to_code', width : 100, sortable : true, align: 'left'},
		{display: 'Distributor Code', name : 'distributor_code', width : 100, sortable : true, align: 'left'},
		{display: 'Distributor Name', name : 'distributor_name', width : 100, sortable : true, align: 'left'},
		{display: 'Distributor Class', name : 'distributor_class', width : 100, sortable : true, align: 'left'},
		{display: 'Distributor Pincode', name : 'dpin_code', width : 100, sortable : true, align: 'left'},
		{display: 'Retailer Code', name : 'retailer_code', width : 100, sortable : true, align: 'left'},
		{display: 'Retailer Market', name : 'retailer_location', width : 100, sortable : true, align: 'left'},
		{display: 'Retailer Channel', name : 'channel_name', width : 100, sortable : true, align: 'left'},
		/*{display: 'Display Outlet', name : 'display_outlet', width : 100, sortable : true, align: 'left'},*/
		{display: 'Retailer Address', name : 'retailer_address', width : 150, sortable : true, align: 'left'},
		{display: 'State', name : 'retailer_state', width : 150, sortable : true, align: 'left'},
		{display: 'City', name : 'retailer_city', width : 150, sortable : true, align: 'left'},
		{display: 'Taluka', name : 'retailer_taluka', width : 150, sortable : true, align: 'left'},
		{display: 'Comments', name : 'comments', width : 150, sortable : true, align: 'left'}
		],
	buttons : [		
		{name: 'View Order Detail', bclass: 'orderdeails', onpress : eventHandlerOrderList},
		{separator: true},
		{name: 'Retailer Report (F2)', bclass: 'redit', onpress : eventHandlerOrderList},
		{separator: true},
		{name: 'Salesman Report (F4)', bclass: 'sedit', onpress : eventHandlerOrderList},
		{separator: true},
                {name: 'Export to Tally', bclass: 'export', onpress : eventHandlerOrderList},
		{separator: true}
		],
	searchitems : [
		{display: 'Order Number', name : 'order_id', isdefault: true},
		{display: 'Date of Order', name : 'Date of Order'},
		{display: 'Distributor Name', name : 'distributor_name'},
		{display: 'Salesperson Name', name : 'salesman_name'},
		{display: 'Retailer Name', name : 'retailer_name'},
		{display: 'Retailer Channel', name : 'channel_name'},
		{display: 'Display  Outlet', name : 'display_outlet'},
		{display: 'Retailer Market', name : 'retailer_location'},
		{display: 'Retailer Address', name : 'retailer_address'},
		{display: 'Order Taken', name : 'order_type'}
		],
	sortname: "date_of_order",
	sortorder: "desc",
	usepager: true,
	title: 'Order List',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 1000,
	height: 'auto'
});  
}
function eventHandlerOrderList(com,grid)
{
	
	if (com=='Add')	{
		location.href="#";	
	}
	else{
		if( $('.trSelected',grid).length<=0 ||  $('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=$('.trSelected',grid);		
		if (com=='View Order Detail'){
			var searchParam_1 = $('input[name=q]').val();
			var searchParam_2 = $('select[name=qtype]').val();
			var searchParam_3 = $(".pcontrol").find("input").val();
			location.href="admin_order_list.php?id="+items[0].id.substr(3)+"&searchParam_1="+searchParam_1+"&searchParam_2="+searchParam_2+"&searchParam_3="+searchParam_3;	
		}else if (com=='Retailer Report (F2)')	{
		window.open("retailer_his_report.php?ordid="+items[0].id.substr(3), '_blank');
		//location.href="retailer_items_report.php?id="+items[0].id.substr(3);
		}else if (com=='Salesman Report (F4)')	{
		location.href="admin_order_list.php?salid="+items[0].id.substr(3);	
		//location.href="retailer_items_report.php?id="+items[0].id.substr(3);
		}	
                 else if (com=='Export to Tally')	{
			var searchParam_1 = $('input[name=q]').val();
			var searchParam_2 = $('select[name=qtype]').val();
			var searchParam_3 = $(".pcontrol").find("input").val();
			location.href="download_tally_list.php?id="+items[0].id.substr(3)+"&searchParam_1="+searchParam_1+"&searchParam_2="+searchParam_2+"&searchParam_3="+searchParam_3;	
		
		//location.href="retailer_items_report.php?id="+items[0].id.substr(3);
		}	
	}
}
function showSalsmanOrderList(){
$("#flex1").flexigrid({
	url: 'salesman_order_list.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [
		{display: 'Date of Order', name : 'date_of_order', width : 100, sortable : true, align: 'left'},
		{display: 'Time of Order', name : 'time_of_order', width : 100, sortable : true, align: 'left'},
		{display: 'Retailer Name', name : 'retailer_name', width : 120, sortable : true, align: 'left'},
		{display: 'Retailer Market', name : 'retailer_location', width : 120, sortable : true, align: 'left'},
		{display: 'Retailer Address', name : 'retailer_address', width : 250, sortable : true, align: 'left'},
		{display: 'Comments', name : 'comments', width : 250, sortable : true, align: 'left'},
		{display: 'Order', name : 'order_type', width : 100, sortable : true, align: 'left'},
		{display: 'Status', name : 'order_status', width : 50, sortable : true, align: 'left'}
			
		],
	buttons : [		
		{name: 'View Order Detail', bclass: 'orderdeails', onpress : eventHandlerSalsmanOrderList},
		{separator: true}
		],
	searchitems : [
		{display: 'Date of Order', name : 'date_of_order', isdefault: true},
		{display: 'Retailer Name', name : 'retailer_name'},
		{display: 'Retailer Market', name : 'retailer_location'},
		{display: 'Retailer Address', name : 'retailer_address'},
		{display: 'Order Type', name : 'order_type'}
		],
	sortname: "date_of_order",
	sortorder: "desc",
	usepager: true,
	title: 'Order List',
	useRp: true,
	rp: 15,
	showTableToggleBtn: true,
	width: 'auto',
	height: 375
});  
}
function eventHandlerSalsmanOrderList(com,grid)
{
	if (com=='Add')	{
		location.href="salesman.php?add";	
	}
	else{
		if( $('.trSelected',grid).length<=0 ||  $('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=$('.trSelected',grid);		
		if (com=='View Order Detail'){
			location.href="salesman_order_list.php?order_id="+items[0].id.substr(3);			
		}else if (com=='Create Login')	{
		location.href="#.php?login&id="+items[0].id.substr(3);
	}
			
	}
}
function showDisributorOrderList(){
$("#flex1").flexigrid({
	url: 'distributors_order_list.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [
		{display: 'Date', name : 'date_of_order', width : 80, sortable : true, align: 'left'},
		{display: 'Time', name : 'time_of_order', width : 60, sortable : true, align: 'left'},
		{display: 'Order Number', name : 'order_id', width : 80, sortable : true, align: 'left'},
		{display: 'Invoice Amt', name : 'acc_total_invoice_amount', width : 80, sortable : true, align: 'left'},
		{display: 'Distributor', name : 'distributor_name', width : 80, sortable : true, align: 'left'},
		{display: 'Distributor Code', name : 'distributor_code', width : 80, sortable : true, align: 'left'},
		{display: 'Salesperson', name : 'salesman_name', width : 100, sortable : true, align: 'left'},
		{display: 'Salesperson Code', name : 'salesman_code', width : 100, sortable : true, align: 'left'},
		{display: 'Retailer', name :'retailer_name', width : 120, sortable : true, align: 'left'},
		{display: 'Retailer Code', name :'retailer_Code', width : 120, sortable : true, align: 'left'},
		{display: 'Retailer Channel', name :'channel_name', width : 120, sortable : true, align: 'left'},
		{display: 'Display Outlet', name : 'display_outlet', width : 120, sortable : true, align: 'left'},
		{display: 'Market', name : 'retailer_location', width : 120, sortable : true, align: 'left'},
		{display: 'Address', name : 'retailer_address', width : 150, sortable : true, align: 'left'},
		{display: 'Order Status', name : 'order_status', width : 150, sortable : true, align: 'left'}
			
		],
	buttons : [		
		{name: 'View Order Detail', bclass: 'orderdeails', onpress : eventHandlerDisributorOrderList},
		{separator: true}
		],
	searchitems : [
		{display: 'Order Number', name : 'order_id', isdefault: true},
		{display: 'Date of Order', name : 'Date of Order'},
		{display: 'Distributor', name : 'distributor_name'},
		{display: 'Salesperson', name : 'salesman_name'},
		{display: 'Retailer', name : 'retailer_name'},
		{display: 'Retailer Channel', name : 'channel_name'},
		{display: 'Display Outlet', name : 'display_outlet'},
		{display: 'Market', name : 'retailer_location'}
		],
	sortname: "date_of_order",
	sortorder: "desc",
	usepager: true,
	title: 'Order List',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 1000,
	height: 'auto'
});  
}
function eventHandlerDisributorOrderList(com,grid)
{
	if (com=='Add')	{
		location.href="#";	
	}
	else{
		if( $('.trSelected',grid).length<=0 ||  $('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=$('.trSelected',grid);		
		if (com=='View Order Detail'){
			location.href="distributors_order_list.php?id="+items[0].id.substr(3);			
		}else if (com=='Create Login')	{
		location.href="#.php?login&id="+items[0].id.substr(3);
	}
			
	}
}


	/**************************************************************************
	* DESC: Show Counter Sales
	* Author: AJAY
	* Created AT: 2016-04-11
	*
	**/

function showDisributorCounterSales(){
$("#flex1").flexigrid({
	url: 'distributors_counter_sales.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [
		{display: 'Date', name : 'date_of_order', width : 80, sortable : true, align: 'left'},
		{display: 'Time', name : 'time_of_order', width : 60, sortable : true, align: 'left'},
		{display: 'Order Number', name : 'order_id', width : 80, sortable : true, align: 'left'},
		{display: 'Invoice Amt', name : 'acc_total_invoice_amount', width : 80, sortable : true, align: 'left'},
		{display: 'Distributor', name : 'distributor_name', width : 80, sortable : true, align: 'left'},
		{display: 'Distributor Code', name : 'distributor_code', width : 80, sortable : true, align: 'left'},
		{display: 'Distributor Division', name : 'division_name', width : 100, sortable : true, align: 'left'},
		// {display: 'Salesperson Code', name : 'salesman_code', width : 100, sortable : true, align: 'left'},
		// {display: 'Retailer', name :'retailer_name', width : 120, sortable : true, align: 'left'},
		// {display: 'Retailer Code', name :'retailer_Code', width : 120, sortable : true, align: 'left'},
		// {display: 'Retailer Channel', name :'channel_name', width : 120, sortable : true, align: 'left'},
		{display: 'Display Outlet', name : 'display_outlet', width : 120, sortable : true, align: 'left'},
		{display: 'Market', name : 'retailer_location', width : 120, sortable : true, align: 'left'},
		{display: 'Address', name : 'retailer_address', width : 150, sortable : true, align: 'left'},
		{display: 'Order Status', name : 'order_status', width : 150, sortable : true, align: 'left'}
			
		],
	buttons : [		
		{name: 'View Order Detail', bclass: 'orderdeails', onpress : eventHandlerDisributorCounterSale},
		{separator: true}
		],
	searchitems : [
		{display: 'Order Number', name : 'order_id', isdefault: true},
		{display: 'Date of Order', name : 'Date of Order'},
		{display: 'Distributor', name : 'distributor_name'},
		{display: 'Division Name', name : 'division_name'},
		// {display: 'Retailer', name : 'retailer_name'},
		// {display: 'Retailer Channel', name : 'channel_name'},
		{display: 'Display Outlet', name : 'display_outlet'},
		{display: 'Market', name : 'distributor_location'}
		],
	sortname: "date_of_order",
	sortorder: "desc",
	usepager: true,
	title: 'Distributor Counter Sales',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 1000,
	height: 'auto'
});  
}


function eventHandlerDisributorCounterSale(com,grid)
{
	if (com=='Add')	{
		location.href="#";	
	}
	else{
		if( $('.trSelected',grid).length<=0 ||  $('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=$('.trSelected',grid);		
		if (com=='View Order Detail'){
			location.href="dis_cs_detail.php?id="+items[0].id.substr(3);			
		}else if (com=='Create Login')	{
		location.href="#.php?login&id="+items[0].id.substr(3);
	}
			
	}
}






function showRetailerOrderList(){
$("#flex1").flexigrid({
	url: 'retailer_order_list.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [
		{display: 'Date of Order', name : 'date_of_order', width : 120, sortable : true, align: 'left'},
		{display: 'Time of Order', name : 'time_of_order', width : 120, sortable : true, align: 'left'},
		{display: 'Salesperson Name', name : 'salesman_name', width : 120, sortable : true, align: 'left'},
		{display: 'Distributor Name', name : 'distributor_name', width : 120, sortable : true, align: 'left'},
		{display: 'Comments', name : 'comments', width : 250, sortable : true, align: 'left'},
		{display: 'Order Taken', name : 'order_type', width : 100, sortable : true, align: 'left'},
		{display: 'Order Status', name : 'order_status', width : 100, sortable : true, align: 'left'}
			
		],
	buttons : [		
		{name: 'View Order Detail', bclass: 'orderdeails', onpress : eventHandlerRetailerOrderList},
		{separator: true}
		],
	searchitems : [
		{display: 'Date of Order', name : 'Date of Order', isdefault: true},
		{display: 'Salesperson Name', name : 'salesman_name', isdefault: true},
		{display: 'Distributor Name', name : 'distributor_name'},
		{display: 'Order Taken', name : 'order_type'}
		],
	sortname: "date_of_order",
	sortorder: "desc",
	usepager: true,
	title: 'Order List',
	useRp: true,
	rp: 15,
	showTableToggleBtn: true,
	width: 'auto',
	height: 375
});  
}
function eventHandlerRetailerOrderList(com,grid)
{
	if (com=='Add')	{
		location.href="#";	
	}
	else{
		if( $('.trSelected',grid).length<=0 ||  $('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=$('.trSelected',grid);		
		if (com=='View Order Detail'){
			location.href="retailer_order_list.php?order_id="+items[0].id.substr(3);			
		}else if (com=='Create Login')	{
		location.href="#.php?login&id="+items[0].id.substr(3);
	}
			
	}
}
function showLoginSalsman(){
$("#flex1").flexigrid({
	url: 'salesman_list.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Salesman Name', name : 'salesman_name', width : 120, sortable : true, align: 'left'},
		{display: 'Username', name : 'username', width : 120, sortable : true, align: 'left'},
		{display: 'Email ID', name : 'email_id', width : 150, sortable : true, align: 'left'},
		{display: 'Phone Number', name : 'salesman_phome_no', width : 120, sortable : true, align: 'left'},
		{display: 'Login Status', name : '', width : 120, sortable : true, align: 'left'},
		{display: '', name : '', width : 120, sortable : true, align: 'left'}
			
		],
	buttons : [
		],
	searchitems : [
		{display: 'Salesman Name', name : 'salesman_name', isdefault: true},
		{display: 'Username', name : 'username'},
		{display: 'Email ID', name : 'email_id'},
		{display: 'Phone Number', name : 'salesman_phome_no'}
		],
	sortname: "salesman_name",
	sortorder: "asc",
	usepager: true,
	title: 'Salesman List',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 850,
	height: 'auto'
});  
}

function showTagList(){
$("#flex1").flexigrid({
	url: 'tag_list.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Description', name : 'tag_description', width : 500, sortable : true, align: 'left'},
		{display: 'status', name : 'status', width : 500, sortable : true, align: 'left'}	
		],
	buttons : [
		{name: 'Add', bclass: 'add', onpress : eventHandlerTagList},
		{separator: true},		
		{name: 'Edit', bclass: 'edit', onpress : eventHandlerTagList},
		{separator: true},
		{name: 'Delete', bclass: 'delete', onpress : eventHandlerTagList},
		{separator: true}
		],
	searchitems : [
		{display: 'Description', name : 'tag_description', isdefault: true}
		],
	sortname: "tag_type",
	sortorder: "asc",
	usepager: true,
	title: 'No Order Reason',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 850,
	height: 'auto'
});  
}
function eventHandlerTagList(com,grid)
{
	if (com=='Add')	{
		location.href="addtag.php";	
	}
	else{
		if( $('.trSelected',grid).length<=0 ||  $('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=$('.trSelected',grid);		
		if (com=='Edit'){
			location.href="addtag.php?id="+items[0].id.substr(3);			
		}
		if (com=='Delete'){
			if(confirm("Are you sure, you want to delete selected Tag?")){
					
				$.post("tag_list.php", {"delete":"yes","id":items[0].id.substr(3)},function(data)
				{		 
				  if(data!="") {					 
					 $('#flex1').flexOptions().flexReload(); 
				  }else {
					  alert("Record doesn't exists.")
				  }
				  
				});		
			}			
		}
	}
}

function showMessageCom(){
$("#flex1").flexigrid({
	url: 'message_communication.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Salesman Name', name : 'salesman_name', width : 100, sortable : true, align: 'left'},
		{display: 'Retailer Name', name : '', width : 180, sortable : true, align: 'left'},
		{display: 'Reporting to', name : 'salesman_name', width : 100, sortable : true, align: 'left'},
		{display: 'Salesman State', name : 'state_name', width : 100, sortable : true, align: 'left'},
		{display: 'Salesman City', name : 'city_name', width : 100, sortable : true, align: 'left'},
		{display: 'Message', name : 'comm_message', width : 300, sortable : true, align: 'left'},
		{display: 'Reason', name : 'reason', width : 300, sortable : true, align: 'left'},
		{display: 'Subreason', name : 'subreason', width : 300, sortable : true, align: 'left'},
		{display: 'Date', name : 'comm_date', width : 80, sortable : true, align: 'left'},
		{display: 'Time', name : 'comm_time', width : 80, sortable : true, align: 'left'}
		],
	buttons : [
		//{name: 'Send Message', bclass: 'sendmsg', onpress : eventHandlerMessageCom},
		//{separator: true},		
		{name: 'View Message', bclass: 'viewmsg', onpress : eventHandlerMessageCom},
		{separator: true}
		//{name: 'Delete', bclass: 'delete', onpress : eventHandlerMessageCom},
		//{separator: true}
		
		],
	searchitems : [
		{display: 'Salesman Name', name : 's.salesman_name', isdefault: true}
		//{display: 'Subject', name : 'subject'},
		//{display: 'Date', name : 'comm_date'}
		
		],
	sortname: "comm_id",
	sortorder: "desc",
	usepager: true,
	title: 'Complaint',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 950,
	height: 'auto'
});  
}
function eventHandlerMessageCom(com,grid)
{
	if (com=='Send Message')	{
		//location.href="send_message.php";		
	}else{
		if( $('.trSelected',grid).length<=0 ||  $('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=$('.trSelected',grid);		
		if (com=='View Message'){
			//location.href="view_message.php?id="+items[0].id.substr(3);	
			window.open("view_message_comm.php?id="+items[0].id.substr(3), '_blank');
		}
		/*if (com=='Delete'){
			if(confirm("Are you sure, you want to delete selected Message?")){
					
				$.post("message_communication.php", {"delete":"yes","id":items[0].id.substr(3)},function(data)
				{		 
				  if(data!="") {					 
					 $('#flex1').flexOptions().flexReload(); 
				  }else {
					  alert("Record doesn't exists.")
				  }
				  
				});		
			}			
		}*/	
	}
}
function showMessage(){
$("#flex1").flexigrid({
	url: 'message.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Salesman Name', name : 'salesman_name', width : 100, sortable : true, align: 'left'},
		{display: 'Subject', name : 'subject', width : 100, sortable : true, align: 'left'},
		{display: 'Message', name : 'message', width : 200, sortable : true, align: 'left'},
		{display: 'Reply', name : 'reply', width : 200, sortable : true, align: 'left'},
		{display: 'Photo', name : 'send_date', width : 60, sortable : true, align: 'left'},
		{display: 'Send Date', name : 'send_date', width : 60, sortable : true, align: 'left'},
		{display: 'Send Time', name : 'send_time', width : 60, sortable : true, align: 'left'}
		],
	buttons : [
		{name: 'Send Message', bclass: 'sendmsg', onpress : eventHandlerMessageOld},
		{separator: true},		
		{name: 'View', bclass: 'viewmsg', onpress : eventHandlerMessageOld},
		{separator: true},
		{name: 'Delete', bclass: 'delete', onpress : eventHandlerMessageOld},
		{separator: true}
		
		],
	searchitems : [
		{display: 'Salesman Name', name : 'salesman_name', isdefault: true},
		{display: 'Subject', name : 'subject'},
		{display: 'Send Date', name : 'send_date'}
		
		],
	sortname: "message_id",
	sortorder: "desc",
	usepager: true,
	title: 'Message Broadcast',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 700,
	height: 'auto'
});  
}
function eventHandlerMessageOld(com,grid)
{
	if (com=='Send Message')	{
		location.href="send_message.php";		
	}else{
		if( $('.trSelected',grid).length<=0 ||  $('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=$('.trSelected',grid);		
		if (com=='View'){
			//location.href="view_message.php?id="+items[0].id.substr(3);	
			window.open("view_message.php?id="+items[0].id.substr(3), '_blank');
		}
		if (com=='Delete'){
			if(confirm("Are you sure, you want to delete selected Message?")){
					
				$.post("message.php", {"delete":"yes","id":items[0].id.substr(3)},function(data)
				{		 
				  if(data!="") {					 
					 $('#flex1').flexOptions().flexReload(); 
				  }else {
					  alert("Record doesn't exists.")
				  }
				  
				});		
			}			
		}	
	}
}

function showTroubleTickets(){
$("#flex1").flexigrid({
	url: 'trouble_tickets.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [
		{display: 'Date', name : 'trouble_tickets_date', width : 100, sortable : true, align: 'left'},
		{display: 'Salesman Name', name : 'salesman_name', width : 120, sortable : true, align: 'left'},
		{display: 'Message', name : 'message', width : 200, sortable : true, align: 'left'},
		{display: 'Reply', name : 'reply', width : 200, sortable : true, align: 'left'},
		{display: 'Reply Date', name : 'reply_date', width : 70, sortable : true, align: 'left'},
		{display: 'Status', name : 'status', width : 70, sortable : true, align: 'left'}
		],
	buttons : [	
		{name: 'View', bclass: 'viewtickets', onpress : eventHandlerTroubleTickets},
		{separator: true}
		
		],
	searchitems : [
		{display: 'Salesman Name', name : 'salesman_name', isdefault: true},
		
		],
	sortname: "trouble_tickets_date",
	sortorder: "desc",
	usepager: true,
	title: 'Trouble Tickets',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 'auto',
	height: 'auto'
});  
}
function eventHandlerTroubleTickets(com,grid)
{
	var searchParam_1 = $('input[name=q]').val();
	var searchParam_2 = $('select[name=qtype]').val();
	var searchParam_3 = $(".pcontrol").find("input").val();
	if (com=='Send Message')	{
		location.href="send_message.php";		
	}else{
		if( $('.trSelected',grid).length<=0 ||  $('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=$('.trSelected',grid);		
		if (com=='View'){	
			location.href="view_trouble_tickets.php?id="+items[0].id.substr(3)+"&searchParam_1="+searchParam_1+"&searchParam_2="+searchParam_2+"&searchParam_3="+searchParam_3;				
			//window.open("view_trouble_tickets.php?id="+items[0].id.substr(3), '_blank');
		}
		if (com=='Delete'){
			if(confirm("Are you sure, you want to delete selected Message?")){
					
				$.post("message.php", {"delete":"yes","id":items[0].id.substr(3)},function(data)
				{		 
				  if(data!="") {					 
					 $('#flex1').flexOptions().flexReload(); 
				  }else {
					  alert("Record doesn't exists.")
				  }
				  
				});		
			}			
		}	
	}
}
//



function showIncentive(){
$("#flex1").flexigrid({
	url: 'incentive.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Description', name : 'description', width : 150, sortable : true, align: 'left'},
		{display: 'Incentive Type', name : 'type_description', width : 100, sortable : true, align: 'left'},
		{display: 'Party Type', name : 'party_description', width : 100, sortable : true, align: 'left'},
		{display: 'Duration', name : 'dur_description', width : 100, sortable : true, align: 'left'},
		{display: 'Value', name : 'value', width : 100, sortable : true, align: 'left'},
		{display: 'Reward Amount', name : 'incentive_reward_amount', width : 100, sortable : true, align: 'left'},		
		//{display: 'Salesman Name', name : 'discount_type', width : 100, sortable : true, align: 'left'},
		{display: 'Start Date', name : 'start_date', width : 100, sortable : true, align: 'left'},
		{display: 'End Date', name : 'end_date', width : 100, sortable : true, align: 'left'},
		{display: 'Incentive Status', name : 'status', width : 100, sortable : true, align: 'left'},
		//{display: 'Salesman Status', name : 'status', width : 100, sortable : true, align: 'left'}
		],
	buttons : [
		{name: 'Add', bclass: 'add', onpress : eventHandlerIncentive},
		{separator: true},		
		{name: 'Edit', bclass: 'edit', onpress : eventHandlerIncentive},
		{separator: true},
		{name: 'Delete', bclass: 'delete', onpress : eventHandlerIncentive},
		{separator: true}
		
		],
	searchitems : [
		{display: 'Description', name : 'description', isdefault: true},
		{display: 'Incentive Type', name : 'type_description'},
		{display: 'Party Type', name : 'party_description'},
		{display: 'Duration', name : 'dur_description'}
		],
	sortname: "target_incentive_id",
	sortorder: "asc",
	usepager: true,
	title: 'Incentive',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 'auto',
	height: 'auto'
});  
}
function eventHandlerIncentive(com,grid)
{
	if (com=='Import')	{
		//location.href="import_item.php";	
		location.href="#";	
	}
	else if (com=='Export')	{
		location.href="export.inc.php?export_item";		
	}
	else if (com=='Add')	{
		location.href="add_incentive.php";		
	}else{
		if( $('.trSelected',grid).length<=0 ||  $('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=$('.trSelected',grid);		
		if (com=='Edit'){
			//location.href="editincentive.php?id="+items[0].id.substr(3);
			location.href="editincentive.php?id="+items[0].id.substr(3);		
		}
		if (com=='Delete'){
			if(confirm("Are you sure, you want to delete selected Incentive?")){
					
				$.post("incentive.php", {"delete":"yes","id":items[0].id.substr(3)},function(data)
				{		 
				  if(data!="") {					 
					 $('#flex1').flexOptions().flexReload(); 
				  }else {
					  alert("Record doesn't exists.")
				  }
				  
				});		
			}			
		}	
	}
}
function showQualitychecker(){
$("#flex1").flexigrid({
	url: 'qualitychecker.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Name', name : 'qchecker_name', width : 120, sortable : true, align: 'left'},
		{display: 'Username', name : 'username', width : 120, sortable : true, align: 'left'},
		{display: 'Email ID', name : 'email_id', width : 120, sortable : true, align: 'left'},
		{display: 'Phone Number', name : 'qchecker_phone_no', width : 120, sortable : true, align: 'left'},
		{display: 'Address', name : 'qchecker_address', width : 120, sortable : true, align: 'left'},
		{display: 'Start Date', name : 'start_date', width : 80, sortable : true, align: 'left'},
		{display: 'End Date', name : 'end_date', width : 80, sortable : true, align: 'left'},
		{display: 'Quality Checker Status', name : 'status', width : 80, sortable : true, align: 'center'},
		{display: 'Login Status', name : 'loginStatus', width : 80, sortable : true, align: 'center'}
			
		],
	buttons : [
		{name: 'Add', bclass: 'add', onpress : eventHandlerQcheckAdminUser},
		{separator: true},		
		{name: 'Edit', bclass: 'edit', onpress : eventHandlerQcheckAdminUser},
		{separator: true}
		],
	searchitems : [
		{display: 'Name', name : 'qchecker_name', isdefault: true},
		{display: 'Username', name : 'username'},
		{display: 'Email ID', name : 'email_id'},
		{display: 'Phone Number', name : 'qchecker_phone_no'},
		{display: 'Address', name : 'qchecker_address'}
		],
	sortname: "qchecker_name",
	sortorder: "asc",
	usepager: true,
	title: 'Quality Auditor',
	useRp: true,
	rp: 15,
	showTableToggleBtn: true,
	width: 700,
	height: 375
});  
}

function eventHandlerQcheckAdminUser(com,grid)
{
	if (com=='Add')	{
		location.href="qualitychecker.php?add";	
	}else if (com=='Import')	{
		location.href="qualitychecker.php?import";	
	}else if (com=='Export')	{
		location.href="export.inc.php?export_qualitychecker_list";	
	}
	else{
		if( $('.trSelected',grid).length<=0 ||  $('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=$('.trSelected',grid);		
		if (com=='Edit'){
			location.href="qualitychecker.php?id="+items[0].id.substr(3);			
		}
	}			
}
function showLoginqualityAuditor(){
	$("#flex1").flexigrid({
		url: 'qauditor_list.php?show=yes',
		dataType: 'json',
		method : "GET", 
		singleSelect:true, 
		colModel : [		
			{display: 'Quality Auditor', name : 'qchecker_name', width : 130, sortable : true, align: 'left'},
			{display: 'Username', name : 'username', width : 100, sortable : true, align: 'left'},
			{display: 'Email ID', name : 'email_id', width : 150, sortable : true, align: 'left'},
			{display: 'Phone Number', name : 'qchecker_phone_no', width : 90, sortable : true, align: 'left'},
			{display: 'SESSIONID', name : 'session_id', width : 120, sortable : true, align: 'left'},
			{display: 'Login Status', name : '', width : 80, sortable : true, align: 'left'},
			{display: '', name : '', width : 50, sortable : true, align: 'left'}
				
			],
		searchitems : [
			{display: 'Quality Auditor', name : 'qchecker_name', isdefault: true},
			{display: 'Username', name : 'username'},
			{display: 'Email ID', name : 'email_id'},
			{display: 'Phone Number', name : 'qchecker_phone_no'}
			],
		sortname: "qchecker_name",
		sortorder: "asc",
		usepager: true,
		title: 'Quality Auditor List',
		useRp: true,
		rp: 15,
		showTableToggleBtn: true,
		width: 700,
		height: 375
	});  
	}
	
	
function showSalesReturn(){
$("#flex1").flexigrid({
	url: 'salesreturns.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [
		{display: 'Salesman', name : 'salesman_name', width : 100, sortable : true, align: 'left'},
		{display: 'Salesman Code', name : 'salesman_code', width : 100, sortable : true, align: 'left'},
		{display: 'Distributor Name', name : 'distributor_name', width : 100, sortable : true, align: 'left'},
		{display: 'Distributor Code', name : 'distributor_code', width : 100, sortable : true, align: 'left'},
		{display: 'Retailer Name', name : 'retailer_name', width : 100, sortable : true, align: 'left'},
		{display: 'Retailer Code', name : 'retailer_code', width : 100, sortable : true, align: 'left'},
		//{display: 'Order Value', name : 'order_id', width : 70, sortable : true, align: 'left'},
		{display: 'No. of items', name : 'total_item', width : 80, sortable : true, align: 'left'},		
		{display: 'Map', name : 'lat', width : 80, sortable : true, align: 'left'},
		{display: 'Date', name : 'date_of_order', width : 100, sortable : true, align: 'left'},
		{display: 'Time', name : 'time_of_order', width : 100, sortable : true, align: 'left'}
		//{display: 'Status', name : 'order_status', width : 70, sortable : true, align: 'left'},
		//{display: 'Comments', name : 'comments', width : 100, sortable : true, align: 'left'}
		],
	buttons : [		
		{name: 'View Details', bclass: 'viewdetails', onpress : eventHandlerSalesReturnList},
		{separator: true}
		],
	searchitems : [
		//{display: 'Date of Order', name : 'Date of Order', isdefault: true},
		{display: 'Salesman', name : 'salesman_name', isdefault: true},		
		{display: 'Distributor', name : 'distributor_name'},
		{display: 'Retailer', name : 'retailer_name'}
		],
	sortname: "date_of_order",
	sortorder: "desc",
	usepager: true,
	title: 'Sales Return List',
	useRp: true,
	rp: 20,
	showTableToggleBtn: true,
	width: 'auto',
	height: 'auto'
});  
}
function eventHandlerSalesReturnList(com,grid)
{	
	if (com=='Add')	{
		location.href="#";	
	}
	else{
		if( $('.trSelected',grid).length<=0 ||  $('.trSelected',grid).length>1){
			alert("Please.");
			return false;
		}
		var items=$('.trSelected',grid);			
		
		
		if (com=='View Details'){
			var searchParam_1 = $('input[name=q]').val();
			var searchParam_2 = $('select[name=qtype]').val();
			
          location.href="salesreturns.php?id="+items[0].id.substr(3)+"&searchParam_1="+searchParam_1+"&searchParam_2="+searchParam_2;	
			
		}
		
		else if (com=='Create Login')	{
		location.href="#.php?login&id="+items[0].id.substr(3);
	}
			
	}
}


function showTransaction(){
$("#flex1").flexigrid({
	url: 'transaction_list.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [
		{display: 'Salesman', name : 'salesman_name', width : 100, sortable : true, align: 'left'},
		{display: 'Salesman Code', name : 'salesman_code', width : 100, sortable : true, align: 'left'},
		{display: 'Retailer', name : 'retailer_name', width : 100, sortable : true, align: 'left'},
		{display: 'Retailer Code', name : 'retailer_code', width : 100, sortable : true, align: 'left'},
		{display: 'Transaction Type', name : 'transaction_type', width : 100, sortable : true, align: 'left'},
		{display: 'Bank', name : 'issuing_bank', width : 100, sortable : true, align: 'left'},
		//{display: 'Order Value', name : 'order_id', width : 70, sortable : true, align: 'left'},
		{display: 'Amount', name : 'total_sale_amount', width : 80, sortable : true, align: 'left'},		
		//{display: 'Map', name : 'lat', width : 80, sortable : true, align: 'left'},
		{display: 'Date', name : 'transaction_date', width : 100, sortable : true, align: 'left'},
		{display: 'Time', name : 'transaction_time', width : 100, sortable : true, align: 'left'}
		//{display: 'Status', name : 'order_status', width : 70, sortable : true, align: 'left'},
		//{display: 'Comments', name : 'comments', width : 100, sortable : true, align: 'left'}
		],
	buttons : [		
		{name: 'View Details', bclass: 'viewdetails', onpress : eventHandlerTransactionList},
		{separator: true}
		],
	searchitems : [
		//{display: 'Date of Order', name : 'Date of Order', isdefault: true},
		{display: 'Retailer', name : 'retailer_name', isdefault: true}	
		//{display: 'Distributor', name : 'distributor_name'},
		//{display: 'Retailer', name : 'retailer_name'}
		],
	sortname: "transaction_date",
	sortorder: "desc",
	usepager: true,
	title: 'Transaction List',
	useRp: true,
	rp: 20,
	showTableToggleBtn: true,
	width: 'auto',
	height: 'auto'
});  
}
function eventHandlerTransactionList(com,grid)
{

	
	if (com=='Add')	{
		location.href="#";	
	}
	else{
		if( $('.trSelected',grid).length<=0 ||  $('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=$('.trSelected',grid);	
				
		
		
		if (com=='View Details'){
			var searchParam_1 = $('input[name=q]').val();
			var searchParam_2 = $('select[name=qtype]').val();
			
          location.href="transaction_list.php?id="+items[0].id.substr(3)+"&searchParam_1="+searchParam_1+"&searchParam_2="+searchParam_2;	
			
		}else if (com=='Create Login')	{
		location.href="#.php?login&id="+items[0].id.substr(3);
	}
			
	}
}



// Show Product Wise Report List


function showproductwiselist(){
$("#flex1").flexigrid({
	url: 'productwise_report.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [
		{display: 'Category', name : 'category_name', width : 160, sortable : true, align: 'left'},
		{display: 'Salesman Name', name : 'salesman_name', width : 160, sortable : true, align: 'left'},
		{display: 'Salesman Code', name : 'salesman_code', width : 160, sortable : true, align: 'left'},
		{display: 'Item', name : 'item_name', width : 100, sortable : true, align: 'left'},
		{display: 'Item Code', name : 'item_code', width : 100, sortable : true, align: 'left'},
		{display: 'Brand', name : 'brand_name', width : 100, sortable : true, align: 'left'},
		{display: 'Offer Type', name : 'offer_name', width : 100, sortable : true, align: 'left'},
		{display: 'ERP Code', name : 'item_erp_code', width : 100, sortable : true, align: 'left'},
		{display: 'No. of orders', name : '', width : 70, sortable : true, align: 'left'},
		{display: 'Quantity', name : '', width : 70, sortable : true, align: 'left'},
		/*{display: 'Price', name : 'item_mrp', width : 100, sortable : true, align: 'left'},*/
		{display: 'Total', name : '', width : 100, sortable : true, align: 'left'},
		//{display: 'Date', name : 'date_of_order', width : 120, sortable : true, align: 'left'},
		//{display: 'Time', name : 'created', width : 120, sortable : true, align: 'left'}
		//{display: 'Order Status', name : 'order_status', width : 100, sortable : true, align: 'left'}
		//{display: 'Comments', name : 'comments', width : 80, sortable : true, align: 'left'}
		],
	buttons : [		
		{name: 'View Product Detail', bclass: 'viewprodetails', onpress : eventHandlerProductWiseOrderList},
		{separator: true}
		],
	searchitems : [
		{display: 'Item', name : 'item_name', isdefault: true},
		{display: 'Category', name : 'category_name'},
		{display: 'Brand', name : 'brand_name'},
		{display: 'Offer Type', name : 'offer_name'},
		{display: 'ERP Code', name : 'item_erp_code'}
		],
	sortname: "date_of_order",
	sortorder: "desc",
	usepager: true,
	title: 'Product List',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: '980',
	height: 'auto'
});  
}
function eventHandlerProductWiseOrderList(com,grid)
{

	
	if (com=='Add')	{
		location.href="#";	
	}
	else{
		if( $('.trSelected',grid).length<=0 ||  $('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=$('.trSelected',grid);		
		if (com=='View Product Detail'){
			var searchParam_1 = $('input[name=q]').val();
			var searchParam_2 = $('select[name=qtype]').val();			
			location.href="productwise_order_list.php?id="+items[0].id.substr(3)+"&searchParam_1="+searchParam_1+"&searchParam_2="+searchParam_2;	
			
		}else if (com=='Create Login')	{
		location.href="#.php?login&id="+items[0].id.substr(3);
	}
			
	}
}



// End of Product Wise Report List

// Show Product Report List


function showproductlist(){
$("#flex1").flexigrid({
	url: 'product_report.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [
		{display: 'Distributor Name', name : 'distributor_name', width : 80, sortable : true, align: 'left'},
		{display: 'Distributor Code', name : 'distributor_code', width : 80, sortable : true, align: 'left'},
		{display: 'Retailer Name', name : 'retailer_name', width : 70, sortable : true, align: 'left'},
		{display: 'Retailer Code', name : 'retailer_code', width : 70, sortable : true, align: 'left'},
		{display: 'State', name : 'state_name', width : 70, sortable : true, align: 'left'},
		{display: 'District', name : 'city_name', width : 70, sortable : true, align: 'left'},
		{display: 'Taluka', name : 'taluka_name', width : 70, sortable : true, align: 'left'},
		{display: 'Market', name : 'market_name', width : 70, sortable : true, align: 'left'},		
		{display: 'Salesman Name', name : 'salesman_name', width : 80, sortable : true, align: 'left'},
		{display: 'Salesman Code', name : 'salesman_code', width : 80, sortable : true, align: 'left'},
		{display: 'Division Name', name : 'division_name', width : 80, sortable : true, align: 'left'},
		{display: 'Designation', name : 'des1', width : 70, sortable : true, align: 'left'},
		{display: 'Category Name', name : 'category_name', width : 80, sortable : true, align: 'left'},
		{display: 'Item Name', name : 'item_name', width : 70, sortable : true, align: 'left'},
		{display: 'Item Code', name : 'item_code', width : 50, sortable : true, align: 'left'},
		{display: 'Brand Name', name : 'brand_name', width : 70, sortable : true, align: 'left'},
		{display: 'Offer', name : 'offer_name', width : 50, sortable : true, align: 'left'},
		{display: 'Item ERP Code', name : 'item_erp_code', width : 70, sortable : true, align: 'left'},
		{display: 'Price', name : 'price', width : 50, sortable : true, align: 'left'},
		{display: 'Quantity', name : 'quantity', width : 50, sortable : true, align: 'left'},
		{display: 'Total', name : 'ttlprice', width : 50, sortable : true, align: 'left'},
		{display: 'Date of Order', name : 'date_of_order', width : 70, sortable : true, align: 'left'},
		{display: 'Customer Type', name : 'new', width : 70, sortable : true, align: 'left'}
		
		],
	buttons : [		
		{separator: true}
		],
	searchitems : [
		{display: 'Item', name : 'item_name', isdefault: true},
		{display: 'Category', name : 'category_name'},
		{display: 'Retailer Name', name : 'retailer_name'},
		{display: 'Retailer Channel', name : 'channel_name'},
		{display: 'Brand', name : 'brand_name'},
		{display: 'Offer', name : 'offer_name'},
		{display: 'Item ERP Code', name : 'item_erp_code'}

		],
	sortname: "date_of_order",
	sortorder: "desc",
	usepager: true,
	title: 'Product List',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: '900',
	height: 'auto'
});  
}




// End of Product Report List







function showRetailerStockByDate(){
$("#flex1").flexigrid({
	url: 'RetOpeningStockByDate.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Salesmen', name : 'salesman_name', width : 120, sortable : true, align: 'left'},
		{display: 'Salesmen Code', name : 'salesman_code', width : 120, sortable : true, align: 'left'},
		{display: 'Retailer Name', name : 'retailer_name', width : 120, sortable : true, align: 'left'},
		{display: 'Retailer Code', name : 'retailer_code', width : 120, sortable : true, align: 'left'},
		{display: 'Retailer Channel', name : 'channel_name', width : 120, sortable : true, align: 'left'},
		{display: 'Retailer Display Outlet', name : 'display_outlet', width : 120, sortable : true, align: 'left'},
		{display: 'Category', name : 'category_name', width : 120, sortable : true, align: 'left'},
		{display: 'Item', name : 'item_name', width : 180, sortable : true, align: 'left'},
		{display: 'Item Code', name : 'item_code', width : 100, sortable : true, align: 'left'},
		{display: 'Stock Value', name : 'opening_stock_quantity', width : 70, sortable : true, align: 'left'},
		{display: 'Date', name : 'date_of_order', width : 80, sortable : true, align: 'left'},
		{display: 'Time', name : 'time_of_order', width :60, sortable : true, align: 'left'},
		{display: 'Previous Closing Stock', name : '', width :120, sortable : true, align: 'left'},
		{display: 'Sale', name : '', width :60, sortable : true, align: 'left'}
		],

	buttons : [		
		//{name: 'View Detail', bclass: 'edit', onpress : eventHandlerTotalSale},
		//{separator: true},
		//{name: 'Print', bclass: 'edit', onpress : printTable},
		//{separator: true},
		/*{name: 'Export to excel', bclass: 'exportexcel', onpress :toExcel },
		{separator: true}*/
		],
	searchitems : [
		{display: 'Salesman', name : 'salesman_name', isdefault: true},
		{display: 'Retailer', name : 'retailer_name'},
		{display: 'Retailer Channel', name : 'channel_name'},
		{display: 'Retailer Display Outlet', name : 'display_outlet'},
		{display: 'Item', name : 'item_name'},
		{display: 'Category', name : 'category_name'},
		{display: 'Date', name : 'date_of_order'}
		],
	sortname: "salesman_name",
	sortorder: "asc",
	usepager: true,
	title: 'Opening Stock For Retailers',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: '980',
	height: 'auto'
});  
}

function showDistibutorTotalSale(){
$("#flex1").flexigrid({
	url: 'dis_montly_basis_sale.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	
	colModel : [
		{display: 'Retailer', name : 'retailer_name', width : 100, sortable : true, align: 'left'},
		{display: 'Retailer Code', name : 'retailer_code', width : 100, sortable : true, align: 'left'},
		{display: 'Retailer Channel', name : 'channel_name', width : 100, sortable : true, align: 'left'},
		{display: 'Distributor', name : 'distributor_name', width : 100, sortable : true, align: 'left'},
		{display: 'Distributor Code', name : 'distributor_code', width : 100, sortable : true, align: 'left'},
		//{display: 'Address2', name : 'retailer_address2', width : 70, sortable : true, align: 'left'},
		{display: 'Item', name : 'item_name', width : 150, sortable : true, align: 'left'},
		{display: 'Item Code', name : 'item_code', width : 150, sortable : true, align: 'left'},
		{display: 'Brand', name : 'brand_name', width : 150, sortable : true, align: 'left'},
		{display: 'Offer Type', name : 'offer_name', width : 150, sortable : true, align: 'left'},
        {display: 'Month', name : 'month', width : 50, sortable : true, align: 'center'},
		{display: 'Year', name : 'year', width : 50, sortable : true, align: 'center'},
		{display: 'Day 1', name : '', width : 50, sortable : true, align: 'left'},
		{display: 'Day 2', name : '', width : 50, sortable : true, align: 'left'},
		{display: 'Day 3', name : '', width : 50, sortable : true, align: 'left'},
		{display: 'Day 4', name : '', width : 50, sortable : true, align: 'left'},
		{display: 'Day 5', name : '', width : 50, sortable : true, align: 'left'},
		{display: 'Day 6', name : '', width : 50, sortable : true, align: 'left'},
		{display: 'Day 7', name : '', width : 50, sortable : true, align: 'left'},
		{display: 'Day 8', name : '', width : 50, sortable : true, align: 'left'},
		{display: 'Day 9', name : '', width : 50, sortable : true, align: 'left'},
		{display: 'Day 10', name : '', width : 50, sortable : true, align: 'left'},
		{display: 'Day 11', name : '', width : 50, sortable : true, align: 'left'},
		{display: 'Day 12', name : '', width : 50, sortable : true, align: 'left'},
		{display: 'Day 13', name : '', width : 50, sortable : true, align: 'left'},
		{display: 'Day 14', name : '', width : 50, sortable : true, align: 'left'},
		{display: 'Day 15', name : '', width : 50, sortable : true, align: 'left'},
		{display: 'Day 16', name : '', width : 50, sortable : true, align: 'left'},
		{display: 'Day 17', name : '', width : 50, sortable : true, align: 'left'},
		{display: 'Day 18', name : '', width : 50, sortable : true, align: 'left'},
		{display: 'Day 19', name : '', width : 50, sortable : true, align: 'left'},
		{display: 'Day 20', name : '', width : 50, sortable : true, align: 'left'},
		{display: 'Day 21', name : '', width : 50, sortable : true, align: 'left'},
		{display: 'Day 22', name : '', width : 50, sortable : true, align: 'left'},
		{display: 'Day 23', name : '', width : 50, sortable : true, align: 'left'},
		{display: 'Day 24', name : '', width : 50, sortable : true, align: 'left'},
		{display: 'Day 25', name : '', width : 50, sortable : true, align: 'left'},
		{display: 'Day 26', name : '', width : 50, sortable : true, align: 'left'},
		{display: 'Day 27', name : '', width : 50, sortable : true, align: 'left'},
		{display: 'Day 28', name : '', width : 50, sortable : true, align: 'left'},
		{display: 'Day 29', name : '', width : 50, sortable : true, align: 'left'},
		{display: 'Day 30', name : '', width : 50, sortable : true, align: 'left'},
		{display: 'Day 31', name : '', width : 50, sortable : true, align: 'left'}
		],
	buttons : [		
		//{name: 'View Detail', bclass: 'edit', onpress : eventHandlerTotalSale},
		//{separator: true},
		//{name: 'Print', bclass: 'edit', onpress : printTable},
		//{separator: true},
		/*{name: 'Export to excel', bclass: 'exportexcel', onpress :toExcel },
		{separator: true}*/
		],
	searchitems : [
		{display: 'Retailer', name : 'retailer_name'},
		{display: 'Retailer Channel', name : 'channel_name'},
		{display: 'Item', name : 'item_name'},
		{display: 'Item Code', name : 'item_code'},
		{display: 'Brand', name : 'brand_name'},
		{display: 'Offer', name : 'offer_name'}

		],
	sortname: "month",
	sortorder: "ASC",
	usepager: true,
	title: 'Retailer Wise Item List',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 950,
	height: 'auto'
});  
}
function eventHandlerTotalSale(com,grid)
{

	
	if (com=='Add')	{
		location.href="#";	
	}
	else{
		if( $('.trSelected',grid).length<=0 ||  $('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=$('.trSelected',grid);	
		
		if (com=='View Target Detail'){
			var searchParam_1 = $('input[name=q]').val();
			var searchParam_2 = $('select[name=qtype]').val();
			location.href="salesmen_target_reports.php?id="+items[0].id.substr(3)+"&searchParam_1="+searchParam_1+"&searchParam_2="+searchParam_2;	
			
		}
			
	}
}



	


function showSalesmanTotalSale(){
$("#flex1").flexigrid({
	url: 'sale_montly_basis_sale.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	
	colModel : [
	    {display: 'Salesman', name : 's_name', width : 100, sortable : true, align: 'left'},
	    {display: 'Salesman Code', name : 'salesman_code', width : 100, sortable : true, align: 'left'},
		{display: 'Retailer', name : 'retailer_name', width : 100, sortable : true, align: 'left'},
		{display: 'Retailer Code', name : 'retailer_code', width : 100, sortable : true, align: 'left'},
		{display: 'Retailer Channel', name : 'channel_name', width : 100, sortable : true, align: 'left'},
		{display: 'Item', name :'item_name', width : 100, sortable : true, align: 'left'},
		{display: 'Brand', name : 'brand_name', width : 100, sortable : true, align: 'left'},
		{display: 'Offer', name : 'offer_name', width : 150, sortable : true, align: 'left'},
		{display: 'Month', name : 'month', width : 50, sortable : true, align: 'center'},
		{display: 'Year', name : 'year', width : 50, sortable : true, align: 'center'},
		{display: 'Day 1', name : '', width : 50, sortable : true, align: 'left'},
		{display: 'Day 2', name : '', width : 50, sortable : true, align: 'left'},
		{display: 'Day 3', name : '', width : 50, sortable : true, align: 'left'},
		{display: 'Day 4', name : '', width : 50, sortable : true, align: 'left'},
		{display: 'Day 5', name : '', width : 50, sortable : true, align: 'left'},
		{display: 'Day 6', name : '', width : 50, sortable : true, align: 'left'},
		{display: 'Day 7', name : '', width : 50, sortable : true, align: 'left'},
		{display: 'Day 8', name : '', width : 50, sortable : true, align: 'left'},
		{display: 'Day 9', name : '', width : 50, sortable : true, align: 'left'},
		{display: 'Day 10', name : '', width : 50, sortable : true, align: 'left'},
		{display: 'Day 11', name : '', width : 50, sortable : true, align: 'left'},
		{display: 'Day 12', name : '', width : 50, sortable : true, align: 'left'},
		{display: 'Day 13', name : '', width : 50, sortable : true, align: 'left'},
		{display: 'Day 14', name : '', width : 50, sortable : true, align: 'left'},
		{display: 'Day 15', name : '', width : 50, sortable : true, align: 'left'},
		{display: 'Day 16', name : '', width : 50, sortable : true, align: 'left'},
		{display: 'Day 17', name : '', width : 50, sortable : true, align: 'left'},
		{display: 'Day 18', name : '', width : 50, sortable : true, align: 'left'},
		{display: 'Day 19', name : '', width : 50, sortable : true, align: 'left'},
		{display: 'Day 20', name : '', width : 50, sortable : true, align: 'left'},
		{display: 'Day 21', name : '', width : 50, sortable : true, align: 'left'},
		{display: 'Day 22', name : '', width : 50, sortable : true, align: 'left'},
		{display: 'Day 23', name : '', width : 50, sortable : true, align: 'left'},
		{display: 'Day 24', name : '', width : 50, sortable : true, align: 'left'},
		{display: 'Day 25', name : '', width : 50, sortable : true, align: 'left'},
		{display: 'Day 26', name : '', width : 50, sortable : true, align: 'left'},
		{display: 'Day 27', name : '', width : 50, sortable : true, align: 'left'},
		{display: 'Day 28', name : '', width : 50, sortable : true, align: 'left'},
		{display: 'Day 29', name : '', width : 50, sortable : true, align: 'left'},
		{display: 'Day 30', name : '', width : 50, sortable : true, align: 'left'},
		{display: 'Day 31', name : '', width : 50, sortable : true, align: 'left'}
		],
	buttons : [		
		
		
		],
	searchitems : [
		{display: 'Salesman', name : 'V.salesman_name'},
		{display: 'Retailer', name : 'V.retailer_name'},
		{display: 'Retailer Channel', name : 'cm.channel_name'},
        {display: 'Item', name : 'i.item_name'},
        {display: 'Brand', name : 'B.brand_name'}
		],
	sortname: "month",
	sortorder: "ASC",
	usepager: true,
	title: 'Salesman Retailer Wise Item List',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 950,
	height: 'auto'
});  
}

// Show target details


function showTarget(){
$("#flex1").flexigrid({
	url: 'target.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Description', name : 'description', width : 150, sortable : true, align: 'left'},
		{display: 'Target Type', name : 'type_description', width : 100, sortable : true, align: 'left'},
		{display: 'Party Type', name : 'party_description', width : 100, sortable : true, align: 'left'},
		{display: 'Duration', name : 'dur_description', width : 100, sortable : true, align: 'left'},
		{display: 'Value', name : 'value', width : 100, sortable : true, align: 'left'},
		{display: 'Reward Amount', name : 'incentive_reward_amount', width : 100, sortable : true, align: 'left'},
		/*{display: 'Salesman Name', name : 'salesman_name', width : 150, sortable : true, align: 'left'},
		{display: 'Salesman Code', name : 'salesman_code', width : 100, sortable : true, align: 'left'},*/		
		//{display: 'Salesman Name', name : 'discount_type', width : 100, sortable : true, align: 'left'},
		{display: 'Start Date', name : 'start_date', width : 100, sortable : true, align: 'left'},
		{display: 'End Date', name : 'end_date', width : 100, sortable : true, align: 'left'},
		{display: 'Target Status', name : 'status', width : 100, sortable : true, align: 'left'},
		//{display: 'Salesman Status', name : 'status', width : 100, sortable : true, align: 'left'}
		],
	buttons : [
		{name: 'Add', bclass: 'add', onpress : eventHandlerTarget},
		{separator: true},		
		{name: 'Edit', bclass: 'edit', onpress : eventHandlerTarget},
		{separator: true},
		{name: 'Delete', bclass: 'delete', onpress : eventHandlerTarget},
		{separator: true}
		
		],
	searchitems : [
		{display: 'Description', name : 'description', isdefault: true},
		{display: 'Target Type', name : 'type_description'},
		{display: 'Party Type', name : 'party_description'},
		{display: 'Duration', name : 'dur_description'}
		],
	sortname: "target_incentive_id",
	sortorder: "asc",
	usepager: true,
	title: 'Target',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 'auto',
	height: 'auto'
});  
}
function eventHandlerTarget(com,grid)
{
	if (com=='Import')	{
		//location.href="import_item.php";	
		location.href="#";	
	}
	else if (com=='Export')	{
		location.href="export.inc.php?export_item";		
	}
	else if (com=='Add')	{
		location.href="add_target.php";		
	}else{
		if( $('.trSelected',grid).length<=0 ||  $('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=$('.trSelected',grid);		
		if (com=='Edit'){
			location.href="edit_target.php?id="+items[0].id.substr(3);		
		}
		if (com=='Delete'){
			if(confirm("Are you sure, you want to delete selected Target?")){
					
				$.post("target.php", {"delete":"yes","id":items[0].id.substr(3)},function(data)
				{		 
				  if(data!="") {					 
					 $('#flex1').flexOptions().flexReload(); 
				  }else {
					  alert("Record doesn't exists.")
				  }
				  
				});		
			}			
		}	
	}
}


function showSmsSchedule(){
$("#flex1").flexigrid({
	url: 'sms_schedule_list.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Description', name : 'discount_desc', width : 150, sortable : true, align: 'left'},
		{display: 'SMS Repeat', name : 'repeats', width : 100, sortable : true, align: 'left'},
		{display: 'SMS Sent', name : 'sent_nature', width : 100, sortable : true, align: 'left'},
		{display: 'Sent on', name : 'weekday', width : 100, sortable : true, align: 'left'},
		{display: 'Party Type', name : 'party_type', width : 100, sortable : true, align: 'left'},
		{display: 'Scheme Type', name : 'discount_type', width : 100, sortable : true, align: 'left'},
		{display: 'Start Date', name : 'from_date', width : 100, sortable : true, align: 'left'},
		{display: 'End Date', name : 'to_date', width : 100, sortable : true, align: 'left'},
		{display: 'Snooze', name : 'status', width : 50, sortable : true, align: 'left'}
		],
	buttons : [
		{name: 'Edit', bclass: 'edit', onpress : eventHandlerSmsSchedule},
		{separator: true},
		{name: 'Delete', bclass: 'delete', onpress : eventHandlerSmsSchedule},
		{separator: true}
		
		],
	searchitems : [
		{display: 'Description', name : 'discount_desc', isdefault: true}
		
		],
	sortname: "discount_id",
	sortorder: "asc",
	usepager: true,
	title: 'SMS Schedule',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 'auto',
	height: 'auto'
});  
}
function eventHandlerSmsSchedule(com,grid)
{
	
	if (com=='Export')	{
		location.href="export.inc.php?export_item";		
	}
	if (com=='Edit'){
			//alert(items[0].sid);
			var id = items[0].id.substr(3);
			//alert(id);
			var ids = btoa(id);
			//alert(atob(ids));

			location.href="sms_schedule.php?sid="+ids;			
		}
	
	if( $('.trSelected',grid).length<=0 ||  $('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=$('.trSelected',grid);		
		
		if (com=='Delete'){
			if(confirm("Are you sure, you want to delete selected Scheme?")){
					
				$.post("sms_schedule_list.php", {"delete":"yes","schedule_id":items[0].sid},function(data)
				{		 
				  if(data!="") {					 
					 $('#flex1').flexOptions().flexReload(); 
				  }else {
					  alert("Record doesn't exists.")
				  }
				  
				});		
			}			
		}	
}

/*************************** Access Management ********************************/

function showManageAccount(){
$("#flex1").flexigrid({
	url: 'manage_account.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Company Name', name : 'company_name', width : 150, sortable : true, align: 'left'},
		{display: 'Company Address', name : 'company_address', width : 100, sortable : true, align: 'left'},
		{display: 'Phone Number', name : 'company_phone_no', width : 100, sortable : true, align: 'left'},
		{display: 'Number Of Users', name : 'no_of_employees', width : 100, sortable : true, align: 'left'},
		{display: 'Start Date', name : 'start_date', width : 100, sortable : true, align: 'left'},
		{display: 'End Date', name : 'end_date', width : 100, sortable : true, align: 'left'},
		{display: 'Status', name : 'status', width : 50, sortable : true, align: 'left'}
		],
	buttons : [
		{name: 'Add', bclass: 'add', onpress : eventHandlerAccountManagement},
		{separator: true},
		{name: 'Edit', bclass: 'edit', onpress : eventHandlerAccountManagement},
		{separator: true},
		/*{name: 'Delete', bclass: 'delete', onpress : eventHandlerAccountManagement},
		{separator: true},*/
		{name: 'Manage Features', bclass: 'feature', onpress : eventHandlerAccountManagement},
		{separator: true}
		
		],
	searchitems : [
		{display: 'Company Name', name : 'company_name', isdefault: true}
		
		],
	sortname: "account_id",
	sortorder: "asc",
	usepager: true,
	title: 'Company Accounts',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 'auto',
	height: 'auto'
});  
}
function eventHandlerAccountManagement(com,grid)
{
	
 if (com=='Add')	{
		location.href="add_company_account.php";		
	}
	else{
	if( $('.trSelected',grid).length<=0 ||  $('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=$('.trSelected',grid);}
		if (com=='Edit'){
			//alert(items[0].sid);
			var id = items[0].id.substr(3);
			//alert(id);
			var ids = btoa(id);
			//alert(atob(ids));

			location.href="edit_company_account.php?id="+items[0].id.substr(3);				
		}
		if (com=='Manage Features'){
			var id = items[0].id.substr(3);
			var ids = btoa(id);
			location.href="account_navigation.php?id="+items[0].id.substr(3);				
		}
		
	 if (com=='Delete'){
			if(confirm("Are you sure, you want to delete selected Scheme?")){
					
				$.post("sms_schedule_list.php", {"delete":"yes","schedule_id":items[0].sid},function(data)
				{		 
				  if(data!="") {					 
					 $('#flex1').flexOptions().flexReload(); 
				  }else {
					  alert("Record doesn't exists.")
				  }
				  
				});		
			}			
		}
}
function showcms(){
$("#flex1").flexigrid({
	url: 'cms.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Name', name : 'page_alias_name', width : 200, sortable : true, align: 'left'},
		{display: 'Php Page', name : 'page_name', width : 200, sortable : true, align: 'left'},
		{display: 'Title', name : 'title', width : 180, sortable : true, align: 'left'},
		{display: 'Display Order', name : 'ordID', width : 80, sortable : true, align: 'left'},
		{display: 'Delete', name : 'del', width : 100, sortable : true, align: 'left'},
		{display: 'Status', name : 'status', width : 30, sortable : true, align: 'left'}
		],
	buttons : [
		{name: 'Add', bclass: 'add', onpress : eventHandlerCMS},
		{separator: true},		
		{name: 'Edit', bclass: 'edit', onpress : eventHandlerCMS},
		{separator: true}
		
		],
	searchitems : [
		{display: 'Name', name : 'page_alias_name', isdefault: true},
		{display: 'Status', name : 'status'}
		
		],
	sortname: "page_alias_name",
	sortorder: "asc",
	usepager: true,
	title: 'CMS',
	useRp: true,
	rp: 15,
	showTableToggleBtn: true,
	width: 'auto',
	height: 'auto'
});  
}
function eventHandlerCMS(com,grid)
{
	if (com=='Add')	{
		location.href="addcms.php";		
	}else{
		if( $('.trSelected',grid).length<=0 ||  $('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=$('.trSelected',grid);
		
		if (com=='Edit'){
			location.href="addcms.php?id="+items[0].id.substr(3);			
		}
		
		if (com=='Delete'){
			if(confirm("Are you sure, you want to delete selected Record?")){
					
				$.post("cms.php", {"delete":"yes","id":items[0].id.substr(3)},function(data)
				{		 
				  if(data!="") {					 
					 $('#flex1').flexOptions().flexReload(); 
				  }else {
					  alert("Record doesn't exists.")
				  }
				  
				});		
			}			
		}	
	}
}
function showNavigationList(){
$("#flex1").flexigrid({
	url: 'user_navigation_list.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Company Name', name : 'company_name', width : 150, sortable : true, align: 'left'},
		{display: 'User Type', name : 'type', width : 100, sortable : true, align: 'left'},
		{display: 'User Name', name : 'username', width : 100, sortable : true, align: 'left'},
		{display: 'Previlages', name : 'page_privileges', width : 100, sortable : true, align: 'left'}
		],
	buttons : [
		{name: 'Add', bclass: 'add', onpress : eventHandlerNavigationList},
		{separator: true},
		{name: 'Edit', bclass: 'edit', onpress : eventHandlerNavigationList},
		{separator: true}		
		],
	searchitems : [
		{display: 'Company Name', name : 'company_name', isdefault: true}
		
		],
	sortname: "nav_privilege_id",
	sortorder: "asc",
	usepager: true,
	title: 'User Navigation',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 'auto',
	height: 'auto'
});  
}
function eventHandlerNavigationList(com,grid)
{
	
 if (com=='Add')	{
		location.href="manage_navigation.php";		
	}
	else{
	if( $('.trSelected',grid).length<=0 ||  $('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=$('.trSelected',grid);}
		if (com=='Edit'){
			//alert(items[0].sid);
			var id = items[0].id.substr(3);
			//alert(id);
			var ids = btoa(id);
			//alert(atob(ids));

			location.href="manage_navigation.php?id="+items[0].id.substr(3);				
		}
}

/*************************** End Of Access Management ********************************/

/*********************************** Hierarchy Management ****************************************/

function showhierarchy(){
	
	$("#flex1").flexigrid({
		url: 'hierarchy.php?show=yes',
		dataType: 'json',
		method : "GET", 
		singleSelect:true, 
		colModel : [		
			{display: 'Hierarchy', name : 'a.description', width : 270, sortable : true, align: 'left'},
			//{display: 'Parent', name : 'parent_id', width : 240, sortable : true, align: 'left'},
			{display: 'Date', name : 'created', width : 120, sortable : true, align: 'left'},		
			{display: 'Status', name : 'status', width : 50, sortable : true, align: 'left'}
			],
		buttons : [
			{name: 'Add', bclass: 'add', onpress : eventHandlerhierarchy},
			{separator: true},		
			{name: 'Edit', bclass: 'edit', onpress : eventHandlerhierarchy},
			{separator: true},	
			{name: 'Redefine Level', bclass: 'level', onpress : eventHandlerhierarchy}
			
			],
		searchitems : [
			{display: 'hierarchy', name : 'a.description', isdefault: true}],
		sortname: "a.description",
		sortorder: "asc",
		usepager: true,
		title: 'Hierarchy',
		useRp: true,
		rp: 15,
		showTableToggleBtn: true,
		width: "900",
		height: "auto"
	});  
}



function eventHandlerhierarchy(com,grid) {
	
	if (com=='Import')	{
		//location.href="import_item.php";	
		location.href="#";	
	}
	
		if (com=='Redefine Level')
		{
			//alert('HEllo');
			//alert("addhierarchy.php?id="+items[0].id.substr(3));
			location.href="hierarchy_ordering.php";	
			
		}
	
	
	
	
	else if (com=='Export')	{
		location.href="export.inc.php?export_hierarchy";		
	}
	else if (com=='Add')	{
		location.href="hierarchy_add.php";		
	}else{
		if( $('.trSelected',grid).length<=0 ||  $('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=$('.trSelected',grid);		
		if (com=='Edit')
		{
			//alert('HEllo');
			//alert("addhierarchy.php?id="+items[0].id.substr(3));
			location.href="hierarchy_add.php?id="+items[0].id.substr(3);	
			
		}
		
		
		
		
		
		if (com=='Delete'){
			if(confirm("Are you sure, you want to delete selected data?")){
					
				$.post("hierarchy.php", {"delete":"yes","id":items[0].id.substr(3)},function(data)
				{		 
				  if(data!="") {					 
					 $('#flex1').flexOptions().flexReload(); 
				  }else {
					  alert("Record doesn't exists.")
				  }
				  
				});		
			}			
		}	
	}
}
	
/*********************************** End of Hierarchy Management ****************************************/






/************************************  CITY Section *****************************************************/


function showCity(){
$("#flex1").flexigrid({
	url: 'city.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'District', name : 'city_name', width : 280, sortable : true, align: 'left'},
		{display: 'District Code', name : 'city_code', width : 150, sortable : true, align: 'left'},
		{display: 'State Name', name : 'state_name', width : 200, sortable : true, align: 'left'},
		{display: 'District Status', name : 'status', width : 80, sortable : true, align: 'left'},
		],
	buttons : [
		{name: 'Add', bclass: 'add', onpress : eventHandlerCity},
		{separator: true},		
		{name: 'Edit', bclass: 'edit', onpress : eventHandlerCity},
		{separator: true},
		{name: 'Delete', bclass: 'delete', onpress : eventHandlerCity},
		{separator: true},			
		{name: 'Import', bclass: 'import', onpress : eventHandlerCity},
		{separator: true},
		{name: 'Export', bclass: 'export', onpress : eventHandlerCity},
		{separator: true}
		
		],
	searchitems : [
		{display: 'District', name : 'city_name', isdefault: true},
		{display: 'State', name : 'state_name'}
		
		],
	sortname: "city_name",
	sortorder: "asc",
	usepager: true,
	title: 'District',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 850,
	height: 'auto'
});  
}
function eventHandlerCity(com,grid)
{

	if (com=='Import')	{
		location.href="city_import.php";		
	}
	else if (com=='Export')	{
		location.href="export.inc.php?export_distric_new";		
	}
	else if (com=='Add')	{
		location.href="add_city.php";		
	}else{
		if( $('.trSelected',grid).length<=0 ||  $('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=$('.trSelected',grid);	
		
		
		if (com=='Edit'){
			location.href="add_city.php?id="+items[0].id.substr(3);			
		}
		if (com=='Delete'){
			if(confirm("Are you sure, you want to delete selected district?")){
					
				$.post("city.php", {"delete":"yes","id":items[0].id.substr(3)},function(data)
				{		 
				  var res  = data.split("-?");
					
				  if(res[0].trim()=="0") {					 
					 $('#flex1').flexOptions().flexReload(); 
					 alert('District deleted');
				  }else {
					  alert("District can not be deleted, it is mapped with "+res[0])
				  }
				  
				});		
			}			
		}
		
	}
}

/***************************************State**************************************************************/


function showState(){
$("#flex1").flexigrid({
	url: 'states.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'State', name : 'state_name', width : 280, sortable : true, align: 'left'},
		{display: 'State Code', name : 'state_code',width : 200,sortable : true, align: 'left'},
		{display: 'State Status', name : 'status',width : 100,sortable : true}
		
		],
	buttons : [
		{name: 'Add', bclass: 'add', onpress : eventHandlerState},
		{separator: true},		
		{name: 'Edit', bclass: 'edit', onpress : eventHandlerState},
		{separator: true},
		{name: 'Delete', bclass: 'delete', onpress : eventHandlerState},
		{separator: true},			
		{name: 'Import', bclass: 'import', onpress : eventHandlerState},
		{separator: true},
		{name: 'Export', bclass: 'export', onpress : eventHandlerState},
		{separator: true}
		],
	searchitems : [
		{display: 'State', name : 'state_name', isdefault: true},
		],
	sortname: "state_name",
	sortorder: "asc",
	usepager: true,
	title: 'State',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 850,
	height: 'auto'
});  
}


function eventHandlerState(com,grid)
{

	if (com=='Import')	{
		location.href="state_import.php";		
	}
	else if (com=='Export')	{
		location.href="export.inc.php?export_state_new";		
	}
	else if (com=='Add')	{
		location.href="add_state.php";		
	}else{
		if( $('.trSelected',grid).length<=0 ||  $('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=$('.trSelected',grid);	
		
		
		if (com=='Edit'){
			location.href="add_state.php?id="+items[0].id.substr(3);			
		}
		if (com=='Delete'){
			if(confirm("Are you sure, you want to delete selected state?")){
					
				$.post("states.php", {"delete":"yes","id":items[0].id.substr(3)},function(data)
				{		 
				  var res  = data.split("-?");
				  if(res[0].trim()=="0") {					 
					 $('#flex1').flexOptions().flexReload(); 
					 alert('State deleted');
				  }else {
					  alert("State can not be deleted, it is mapped with "+res[0])
				  }
				  
				});		
			}			
		}
		
	}
}

/************************************  CITY Section *****************************************************/


function showRetailerChannel(){
$("#flex1").flexigrid({
	url: 'channel.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Channel Name', name : 'channel_name', width : 280, sortable : true, align: 'left'},
		{display: 'Status', name : 'status', width : 200, sortable : true, align: 'left'}
		],
	buttons : [
		{name: 'Add', bclass: 'add', onpress : eventHandlerRetailerChainel},
		{separator: true},		
		{name: 'Edit', bclass: 'edit', onpress : eventHandlerRetailerChainel},
		{separator: true}			
		],
	searchitems : [
		{display: 'Channel name', name : 'channel_name', isdefault: true}
		
		],
	sortname: "channel_name",
	sortorder: "asc",
	usepager: true,
	title: 'Retailer Channel',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 850,
	height: 'auto'
});  
}
function eventHandlerRetailerChainel(com,grid)
{
   if (com=='Add')	{
		location.href="channel.php?action=add";		
	}else{
		if( $('.trSelected',grid).length<=0 ||  $('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=$('.trSelected',grid);	
		
		
		if (com=='Edit'){
			location.href="channel.php?action=edit&id="+items[0].id.substr(3);			
		}
		
		
	}
}











/************************************ END of city Section *******************************************/





/**************************** Distributor Login List 15 MAY 2014 ******************************************/

function showLoginDistributor(){
$("#flex1").flexigrid({
	url: 'distributor_login_list.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Distributor Name', name : 'distributor_name', width : 120, sortable : true, align: 'left'},
		{display: 'Username', name : 'username', width : 120, sortable : true, align: 'left'},
		{display: 'Email ID', name : 'email_id', width : 150, sortable : true, align: 'left'},
		{display: 'Phone Number', name : 'distributor_phone_no', width : 120, sortable : true, align: 'left'},
		{display: 'Login Status', name : '', width : 120, sortable : true, align: 'left'},
		{display: '', name : '', width : 120, sortable : true, align: 'left'}
			
		],
	searchitems : [
		{display: 'Distributor Name', name : 'distributor_name', isdefault: true},
		{display: 'Username', name : 'username'},
		{display: 'Email ID', name : 'email_id'},
		{display: 'Phone Number', name : 'distributor_phone_no'}
		],
	sortname: "distributor_name",
	sortorder: "asc",
	usepager: true,
	title: 'Distributor Login List',
	useRp: true,
	rp: 40,
	showTableToggleBtn: true,
	width: 850,
	height: 'auto'
});  
}


/**************************** Distributor Login List 15 MAY 2014 ******************************************/




/**************************** Master category Code 15 MAY 2014 ********************************************************/


function showMasterCategory(){
$("#flex1").flexigrid({
	url: 'master_category.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Category Name', name : 'category_name', width : 300, sortable : true, align: 'left'},
		{display: 'Category Code', name : 'category_code', width : 200, sortable : true, align: 'left'},
		{display: 'Margin Value', name : 'margin_value', width : 100, sortable : true, align: 'left'},
		{display: 'Status', name : 'status', width : 100, sortable : true, align: 'left'}
		],
	buttons : [
		{name: 'Add', bclass: 'add', onpress : eventHandlerMasterCategory},
		{separator: true},		
		{name: 'Edit', bclass: 'edit', onpress : eventHandlerMasterCategory},
		{separator: true},
		{name: 'Set Margin', bclass: 'margin', onpress : eventHandlerMasterCategory},
		{separator: true}
		],
	searchitems : [
		{display: 'Category Name', name : 'category_name', isdefault: true},
		{display: 'Category Code', name : 'category_code'}
		
		],
	sortname: "category_name",
	sortorder: "asc",
	usepager: true,
	title: 'Master Category',
	useRp: true,
	rp: 40,
	showTableToggleBtn: true,
	width: 800,
	height: 'auto'
});  
}
function eventHandlerMasterCategory(com,grid)
{
	if (com=='Add')	{
		location.href="add_master_category.php";		
	}else{
		if( $('.trSelected',grid).length<=0 ||  $('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=$('.trSelected',grid);	
		
		if (com=='Set Margin'){
			location.href="master_category_margin.php?id="+items[0].id.substr(3);			
		} else if (com=='Edit'){
			location.href="add_master_category.php?id="+items[0].id.substr(3);			
		} else if (com=='Delete'){
			if(confirm("Are you sure, you want to delete selected Category?")){
					
				$.post("master_category.php", {"delete":"yes","id":items[0].id.substr(3)},function(data)
				{		 
				  if(data!="") {					 
					 $('#flex1').flexOptions().flexReload(); 
				  }else {
					  alert("Record doesn't exists.")
				  }
				  
				});		
			}			
		}	
	}
}


/*********************************** END of Master Category Code *****************************************/





/**************************** NDC STOCK DETAIL 15 May 2014 ********************************************************/


function showStock(){
$("#flex1").flexigrid({
	url: 'stock.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Category Name', name : 'category_name', width : 200, sortable : true, align: 'left'},
		{display: 'Cases Size', name : 'case_size', width : 100, sortable : true, align: 'left'},
		{display: 'Item Code', name : 'item_code', width : 100, sortable : true, align: 'left'},
		{display: 'Stock Value', name : 'stock_value', width : 100, sortable : true, align: 'left'},
		{display: 'Last Update Date', name : 'last_updated_date', width : 100, sortable : true, align: 'left'}
		//{display: 'Status', name : 'status', width : 100, sortable : true, align: 'left'}
		],
	buttons : [
		/*{name: 'Add', bclass: 'add', onpress : eventHandlerMasterCategory},
		{separator: true},*/		
		{name: 'Edit', bclass: 'edit', onpress : eventHandlerCompanyStock},
		{separator: true}
		/*{name: 'Set Margin', bclass: 'margin', onpress : eventHandlerMasterCategory},
		{separator: true}*/
		],
	searchitems : [
		{display: 'Category Name', name : 'category_name', isdefault: true},
		{display: 'Color', name : 'color_desc'},
		{display: 'Item Code', name : 'item_code'}
		
		],
	sortname: "category_name",
	sortorder: "asc",
	usepager: true,
	title: 'Stock Details',
	useRp: true,
	rp: 40,
	showTableToggleBtn: true,
	width: 700,
	height: 'auto'
});  
}

function eventHandlerCompanyStock(com,grid)
{
	if (com=='Add')	{
		location.href="add_file_category.php";		
	}else{
		if( $('.trSelected',grid).length<=0 ||  $('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=$('.trSelected',grid);		
		if (com=='Edit'){
			location.href="stock.php?id="+items[0].id.substr(3);			
		}
		if (com=='Delete'){
			if(confirm("Are you sure, you want to delete selected Category?")){
					
				$.post("file_category.php", {"delete":"yes","id":items[0].id.substr(3)},function(data)
				{		 
				  if(data!="") {					 
					 $('#flex1').flexOptions().flexReload(); 
				  }else {
					  alert("Record doesn't exists.")
				  }
				  
				});		
			}			
		}	
	}
}

/**************************** NDC STOCK DETAIL 15 May 2014 ********************************************************/


function showFileCategory(){
$("#flex1").flexigrid({
	url: 'file_category.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: ' File Category Name', name : 'file_category_name', width : 300, sortable : true, align: 'left'},
		//{display: 'Category Code', name : 'category_code', width : 200, sortable : true, align: 'left'},
		{display: 'Status', name : 'status', width : 100, sortable : true, align: 'left'}
		],
	buttons : [
		{name: 'Add', class: 'add', onpress : eventHandlerFileCategory},
		{separator: true},		
		{name: 'Edit', class: 'edit', onpress : eventHandlerFileCategory},
		{separator: true}		
		],
	searchitems : [
		{display: 'Category Name', name : 'file_category_name', isdefault: true}
		//{display: 'Category Code', name : 'category_code'}
		
		],
	sortname: "file_category_name",
	sortorder: "asc",
	usepager: true,
	title: 'File Category',
	useRp: true,
	rp: 40,
	showTableToggleBtn: true,
	width: 1000,
	height: 'auto'
});  
}
function eventHandlerFileCategory(com,grid)
{
	if (com=='Add')	{
		location.href="add_file_category.php";		
	}else{
		if( $('.trSelected',grid).length<=0 ||  $('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=$('.trSelected',grid);		
		if (com=='Edit'){
			location.href="add_file_category.php?id="+items[0].id.substr(3);			
		}
		if (com=='Delete'){
			if(confirm("Are you sure, you want to delete selected Category?")){
					
				$.post("file_category.php", {"delete":"yes","id":items[0].id.substr(3)},function(data)
				{		 
				  if(data!="") {					 
					 $('#flex1').flexOptions().flexReload(); 
				  }else {
					  alert("Record doesn't exists.")
				  }
				  
				});		
			}			
		}	
	}
}



function showUploadFiles(){
$("#flex1").flexigrid({
	url: 'file_upload.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [
		{display: 'File Title', name : 'file_name', width : 200, sortable : true, align: 'left'},
		{display: 'File Category Name', name : 'file_category_name', width : 300, sortable : true, align: 'left'},
		{display: 'File Division', name : 'file_division_name', width : 200, align: 'left'},		
		{display: 'Date', name : 'last_update_date', width : 200, sortable : true, align: 'left'},
		{display: 'View', name : 'view', width : 100, sortable : true, align: 'left'}
		],
	buttons : [
		{name: 'Add', class: 'add', onpress : eventHandlerFileUpload},
		{separator: true},
		{name: 'Delete', class: 'delete', onpress : eventHandlerFileUpload},
		{separator: true}
		],
	searchitems : [
		{display: 'Category Name', name : 'file_category_name', isdefault: true}
		//{display: 'Category Code', name : 'category_code'}
		
		],
	sortname: "file_category_name",
	sortorder: "asc",
	usepager: true,
	title: 'File Details',
	useRp: true,
	rp: 40,
	showTableToggleBtn: true,
	width: 1000,
	height: 'auto'
});  
}
function eventHandlerFileUpload(com,grid)
{
	if (com=='Add')	{
		location.href="add_files.php";		
	}else{
		if( $('.trSelected',grid).length<=0 ||  $('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
	
		if (com=='Delete'){
			if(confirm("Are you sure, you want to delete selected Category File?")){
					
				$.post("file_upload.php", {"delete":"yes","id":items[0].id.substr(3)},function(data)
				{		 
				  if(data!="") {					 
					 $('#flex1').flexOptions().flexReload(); 
				  }else {
					  alert("Record doesn't exists.")
				  }
				  
				});		
			}			
		}	
	}
}




/**************************** DISTRIBUTOR IN PROCESS STOCK DETAIL 16 May 2014 ******************************************/


function showDistribuotrInProcessStock(){
$("#flex1").flexigrid({
	url: 'distributor_inpro_stock.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true,
	colModel : [		
		{display: 'Distributor Code', name : 'distributor_code', width : 75, sortable : true, align: 'left'},
		{display: 'Distributor Name', name : 'distributor_name', width : 170, sortable : true, align: 'left'},//1
		{display: 'Category Name', name : 'category_name', width : 72, sortable : true, align: 'left'},
		//{display: 'Cases', name : 'case_size', width : 100, sortable : true, align: 'left'},
		{display: 'Item Code', name : 'item_code', width : 65, sortable : true, align: 'left'},
		{display: 'Item Name', name : 'item_name', width : 200, sortable : true, align: 'left'},//2
		{display: 'Requested Quantity', name : 'rec_stock_value', width : 100, sortable : true, align: 'left'},
		{display: 'Accepted Quantity', name : 'acpt_stock_value', width : 90, sortable : true, align: 'left'},
		{display: 'Rejected Quantity', name : 'rejected_qty', width : 90, sortable : true, align: 'left'},//3
		{display: 'Rejected Reason', name : 'reject_reason', width : 120, sortable : true, align: 'left'},//4
		{display: 'Bill Number', name : 'bill_no', width : 60, sortable : true, align: 'left'},
		{display: 'Bill Date', name : 'bill_date', width : 65, sortable : true, align: 'left'},		
		{display: 'Last Update Date', name : 'last_updated_date', width : 100, sortable : true, align: 'left'},
		{display: 'Status', name : 'status', width : 55, sortable : true, align: 'left'}
		],
	buttons : [
		/*{name: 'Add', bclass: 'add', onpress : eventHandlerMasterCategory},
		{separator: true},		
		{name: 'Edit', bclass: 'edit', onpress : eventHandlerMasterCategory},
		{separator: true},
		{name: 'Set Margin', bclass: 'margin', onpress : eventHandlerMasterCategory},
		{separator: true}*/
		],
	searchitems : [
		{display: 'Category Name', name : 'category_name', isdefault: true},
		{display: 'Distributor Code', name : 'distributor_code'},
		{display: 'Distributor Name', name : 'distributor_name'},
		//{display: 'Case Size', name : 'color_code'},
		{display: 'Item Code', name : 'item_code'},
		{display: 'Itme Name', name : 'item_name'},
		{display: 'Bill Number', name : 'bill_no'}
		
		],
	sortname: "category_name",
	sortorder: "asc",
	usepager: true,
	title: 'Stock Details',
	useRp: true,
	rp: 40,
	showTableToggleBtn: true,
	width: 750,
	height: 'auto'
});  
}

/**************************** DISTRIBUTOR IN PROCESS STOCK DETAIL 16 May 2014 ******************************************/
/**************************** Salesman Allowance 20 May 2014 ******************************************/

function showSalesmanAllownce(checkhierarchyenable){
	
	if(checkhierarchyenable == true){
	var colModel01 = [ 		
		{display: 'Salesman', name : 'salesman_name', width : 100, sortable : true, align: 'left'},
		//{display: 'Salesman Level', name : 'des1', width : 100, sortable : true, align: 'left'},
		{display: 'Reporting to', name : 'rpt_to', width : 100, sortable : true, align: 'left'},
		{display: 'Salesman State', name : 'state_name', width : 100, sortable : true, align: 'left'},	
		{display: 'Salesman City', name : 'city_name', width : 100, sortable : true, align: 'left'},	
		{display: 'Allowance City', name : 'allwn_city', width : 100, sortable : true, align: 'left'},
		{display: 'Comment', name : 'cmt', width : 100, sortable : true, align: 'left'},
		{display: 'Date', name : 'app_date', width : 100, sortable : true, align: 'left'},
		{display: 'Time', name : 'app_time', width : 100, sortable : true, align: 'left'},	
		{display: 'Last updated on', name : 'last_update_date', width : 80, sortable : true, align: 'left'}		
			
		];
} else {
	var colModel01 = [ 		
		{display: 'Salesman', name : 'salesman_name', width : 100, sortable : true, align: 'left'},
		{display: 'Salesman State', name : 'state_name', width : 100, sortable : true, align: 'left'},	
		{display: 'Salesman City', name : 'city_name', width : 100, sortable : true, align: 'left'},	
		{display: 'Allowance City', name : 'allwn_city', width : 100, sortable : true, align: 'left'},
		{display: 'Comment', name : 'cmt', width : 100, sortable : true, align: 'left'},
		{display: 'Date', name : 'app_date', width : 100, sortable : true, align: 'left'},
		{display: 'Time', name : 'app_time', width : 100, sortable : true, align: 'left'},	
		{display: 'Last updated on', name : 'last_update_date', width : 80, sortable : true, align: 'left'}		
			
		];	
	
}
	
	
$("#flex1").flexigrid({
	url: 'salesman_allownce.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : colModel01,
	buttons : [		
		{name: 'Allowance Details', bclass: 'view', onpress : eventHandlerSalesmanAllownce},
		{separator: true}
		],
	searchitems : [
		
			{display: 'Salesman', name : 'S.salesman_name', isdefault: true},	
		{display: 'State', name : 'st.state_name', isdefault: true},	
		{display: 'City', name : 'c.city_name', isdefault: true}	
		
		],
	sortname: "sd.created",
	sortorder: "desc",
	usepager: true,
	title: 'Salesman Allowance List',
	useRp: true,
	rp: 20,
	showTableToggleBtn: true,
	width: 1000,
	height: 'auto'
});  
}



function eventHandlerSalesmanAllownce(com,grid)
{
	
		if( $('.trSelected',grid).length<=0 ||  $('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
	
		var items=$('.trSelected',grid);		
		if (com=='Allowance Details'){
			location.href="salesman_allowance_detail.php?id="+items[0].id.substr(3);			
		}
	
}

/**************************** Salesman Allowance 20 May 2014 ******************************************/
/**************************** Retailer Dues 20 May 2014 ******************************************/

function showRetailerDues(){
$("#flex1").flexigrid({
	url: 'retailer_due_list.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Retailer Name', name : 'retailer_name', width : 300, sortable : true, align: 'left'},
		{display: 'Due Amount', name : 'due_amt', width : 200, sortable : true, align: 'left'},
		{display: 'Date', name : 'last_update_date', width : 100, sortable : true, align: 'left'}
		],
	buttons : [
		
		
		],
	searchitems : [
		{display: 'Retailer', name : 'retailer_name', isdefault: true}		
		],
	sortname: "retailer_name",
	sortorder: "asc",
	usepager: true,
	title: 'Retailer Dues List',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 750,
	height: 'auto'
});  
}

/**************************** Retailer Dues 20 May 2014 ******************************************/


/***************************  Salesman Tags *******************************************/



function showSalesmanTags(){
$("#flex1").flexigrid({
	url: 'salesman_tag.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Tag Description', name : 'desc', width : 300, sortable : true, align: 'left'},
		//{display: 'Category Code', name : 'category_code', width : 200, sortable : true, align: 'left'},
		{display: 'Status', name : 'status', width : 100, sortable : true, align: 'left'}
		],
	buttons : [
		{name: 'Add', bclass: 'add', onpress : eventHandlerSalesmanTags},
		{separator: true},		
		{name: 'Edit', bclass: 'edit', onpress : eventHandlerSalesmanTags},
		{separator: true}		
		],
	searchitems : [
		{display: 'Tag Description', name : '`desc`', isdefault: true}
		//{display: 'Category Code', name : 'category_code'}
		
		],
	sortname: "`desc`",
	sortorder: "asc",
	usepager: true,
	title: 'Salesman Tags',
	useRp: true,
	rp: 40,
	showTableToggleBtn: true,
	width: 1000,
	height: 'auto'
});  
}
function eventHandlerSalesmanTags(com,grid)
{
	if (com=='Add')	{
		location.href="add_salesman_tags.php";		
	}else{
		if( $('.trSelected',grid).length<=0 ||  $('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=$('.trSelected',grid);		
		if (com=='Edit'){
			//alert("Hello");
		location.href="add_salesman_tags.php?id="+items[0].id.substr(3);				
		}
			
	}
}

	/***************************  Salesman Tags *******************************************/
	
	
	
	
	
	
	
	/**************************** Retailer Margin 23 May 2014 ******************************************/

function showRetailerMargin(){
$("#flex1").flexigrid({
	url: 'retailer_margin_list.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Retailer', name : 'retailer_name', width : 200, sortable : true, align: 'left'},
		{display: 'Category', name : 'category_name', width : 150, sortable : true, align: 'left'},
		{display: 'Actual Margin', name : 'margin_value', width : 80, sortable : true, align: 'left'},
		{display: 'Retailer Margin', name : 'ret_mar_val', width : 80, sortable : true, align: 'left'},
		{display: 'Last updated on', name : 'last_update_date', width : 170, sortable : true, align: 'left'},
		{display: 'Status', name : 'status', width : 50, sortable : true, align: 'left'}
		],
	buttons : [
		],
	searchitems : [
		{display: 'Retailer', name : 'retailer_name', isdefault: true},
		{display: 'Category', name : 'category_name', isdefault: true}	
		],
	sortname: "r.retailer_name",
	sortorder: "asc",
	usepager: true,
	title: 'Retailer Margin List',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 800,
	height: 'auto'
});  
}

/**************************** Retailer Margin 23 May 2014 ******************************************/





/**************************** Requested Retailer Margin List 23 May 2014 ******************************************/

function showRequestedRetailerMargin(){
$("#flex1").flexigrid({
	url: 'retailer_margin_request.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Retailer', name : 'retailer_name', width : 200, sortable : true, align: 'left'},
		{display: 'Request by', name : 'salesman_name', width : 120, sortable : true, align: 'left'},
		{display: 'Category', name : 'category_name', width : 150, sortable : true, align: 'left'},
		{display: 'Actual Margin', name : 'cur_mar_val', width : 80, sortable : true, align: 'left'},
		{display: 'Requested Margin', name : 'ret_mar_val', width : 100, sortable : true, align: 'left'},
		{display: 'Last Updated', name : 'created_on', width : 150, sortable : true, align: 'left'},
		{display: 'Approved', name : 'status', width : 50, sortable : true, align: 'left'}
		],
	buttons : [
		/*{name: 'Approve Margin', bclass: 'confirm', onpress : eventHandlerApproveMargin},
		{separator: true},
		{name: 'Disapprove Margin', bclass: 'confirm', onpress : eventHandlerApproveMargin},
		{separator: true}*/
		],
	searchitems : [
		{display: 'Retailer', name : 'retailer_name', isdefault: true},
		{display: 'Category', name : 'category_name', isdefault: true}	
		],
	sortname: "r.retailer_name",
	sortorder: "asc",
	usepager: true,
	title: 'Retailer Requested Margins',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 950,
	height: 'auto'
});  
}
function eventHandlerApproveMargin(com,grid)
{
	if (com=='Add')	{
		location.href="add_files.php";		
	}else{
		if( $('.trSelected',grid).length<=0 ||  $('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=$('.trSelected',grid);	
		if (com=='Approve Margin'){
			if(confirm("Are you sure, you want to confirm this margin?")){
					
				$.post("retailer_margin_request.php", {"approve":"yes","id":items[0].id.substr(3),"cat_id":items[0].sid},function(data)
				{	
				//alert(data);
				  if(data!="") {					 
					 $('#flex1').flexOptions().flexReload(); 
				  }else {
					  alert("Record doesn't exists.")
				  }
				  
				});		
			}			
		}	
		
		if (com=='Disapprove Margin'){
			if(confirm("Are you sure, you want to disapprove this margin?")){
					
				$.post("retailer_margin_request.php", {"disapprove":"no","id":items[0].id.substr(3),"order_id":items[0].sid},function(data)
				{	
				//alert(data);
				  if(data!="") {					 
					 $('#flex1').flexOptions().flexReload(); 
				  }else {
					  alert("Record doesn't exists.")
				  }
				  
				});		
			}			
		}	
	}
}


/**************************** Requested Retailer Margin List 23 May 2014 ******************************************/
	
	
	
function advisory(){
$("#flex1").flexigrid({
	url: 'advisory.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [
		//{display: 'Salesman', name : 'salesman_name', width : 100, sortable : true, align: 'left'},
		//{display: 'Retailer', name : 'retailer_id', width : 100, sortable : true, align: 'left'},
		{display: 'Retailer name', name : 'retailer_name', width : 100, sortable : true, align: 'left'},
		{display: 'Order Interval', name : 'order_interval', width : 100, sortable : true, align: 'left'},
		//{display: 'Order Value', name : 'order_id', width : 70, sortable : true, align: 'left'},
		{display: 'Order Frequency', name : 'order_frequency', width : 80, sortable : true, align: 'left'}	
		//{display: 'Map', name : 'lat', width : 80, sortable : true, align: 'left'},
		//{display: 'Date', name : 'transaction_date', width : 100, sortable : true, align: 'left'},
		//{display: 'Time', name : 'transaction_time', width : 100, sortable : true, align: 'left'}
		//{display: 'Status', name : 'order_status', width : 70, sortable : true, align: 'left'},
		//{display: 'Comments', name : 'comments', width : 100, sortable : true, align: 'left'}
		],
	buttons : [		
		//{name: 'View Details', bclass: 'viewdetails', onpress : eventHandlerTransactionList},
		//{separator: true}
		],
	searchitems : [
		//{display: 'Date of Order', name : 'Date of Order', isdefault: true},
		{display: 'Retailer', name : 'retailer_id', isdefault: true}	
		//{display: 'Distributor', name : 'distributor_name'},
		//{display: 'Retailer', name : 'retailer_name'}
		],
	sortname: "retailer_id",
	sortorder: "desc",
	usepager: true,
	title: 'Advisory',
	useRp: true,
	rp: 20,
	showTableToggleBtn: true,
	width: 'auto',
	height: 'auto'
});  
}

	
	
	
	
/**************************** DISTRIBUTOR STOCK DETAIL 24 June 2014 ******************************************/


function showDistribuotrStock(){
$("#flex1").flexigrid({
	url: 'distributor_stock.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Distributor Code', name : 'distributor_code', width : 80, sortable : true, align: 'left'},
		{display: 'Distributor Name', name : 'distributor_name', width : 200, sortable : true, align: 'left'},
		{display: 'Category Name', name : 'category_name', width : 150, sortable : true, align: 'left'},
		//{display: 'Cases', name : 'case_size', width : 100, sortable : true, align: 'left'},
		{display: 'Item Code', name : 'item_code', width : 100, sortable : true, align: 'left'},
		{display: 'Item Name', name : 'item_name', width : 200, sortable : true, align: 'left'},
		{display: 'Stock Quantity', name : 'dis_stock_value', width : 100, sortable : true, align: 'left'},
		{display: 'Last Update Date', name : 'last_updated_date', width : 100, sortable : true, align: 'left'},
		{display: 'Status', name : 'status', width : 100, sortable : true, align: 'left'}
		],
	buttons : [
		/*{name: 'Add', bclass: 'add', onpress : eventHandlerMasterCategory},
		{separator: true},		
		{name: 'Edit', bclass: 'edit', onpress : eventHandlerMasterCategory},
		{separator: true},
		{name: 'Set Margin', bclass: 'margin', onpress : eventHandlerMasterCategory},
		{separator: true}*/
		],
	searchitems : [
		{display: 'Category Name', name : 'category_name', isdefault: true},
		{display: 'Distributor Code', name : 'distributor_code'},
		{display: 'Distributor Name', name : 'distributor_name'},
		{display: 'Item Code', name : 'item_code'},
		{display: 'Item Name', name : 'item_name'}
		
		],
	sortname: "category_name",
	sortorder: "asc",
	usepager: true,
	title: 'Distributor Stock',
	useRp: true,
	rp: 40,
	showTableToggleBtn: true,
	width: 730,
	height: 'auto'
});  
}


/**************************** DISTRIBUTOR STOCK DETAIL 24 June 2014 ******************************************/

	
	

/*********************************** Report Emailer Schedule ****************************************/

function showReportSchedule(){
	
	sessionStorage.removeItem("rpt_id");
	$("#flex1").flexigrid({
		url: 'report_schedule_list.php?show=yes',
		dataType: 'json',
		method : "GET", 
		singleSelect:true, 
		colModel : [		
			{display: 'User Type', name : 'type', width : 120, sortable : true, align: 'left'},
			{display: 'User Name', name : '', width : 200, sortable : true, align: 'left'},
			{display: 'Start Date', name : 'from_date', width : 80, sortable : true, align: 'left'},
			{display: 'End Date', name : 'to_date', width : 80, sortable : true, align: 'left'},
			{display: 'Interval', name : 'sent_nature', width : 50, sortable : true, align: 'left'},
			{display: 'Schedule Day', name : 'weekday', width : 70, sortable : true, align: 'left'},
			{display: 'Additional emails', name : 'additional_emails', width : 120, sortable : true, align: 'left'},
			{display: 'Created', name : 'created', width :100, sortable : true, align: 'left'},
			{display: 'Status', name : 'status', width : 50, sortable : true, align: 'left'}
			],
		buttons : [
			{name: 'Add', bclass: 'add', onpress : eventHandlerReportSchedule},
			{separator: true},		
			{name: 'Edit', bclass: 'edit', onpress : eventHandlerReportSchedule},
			{separator: true},		
			{name: 'Send Mail', bclass: 'mail', onpress : eventHandlerReportSchedule}
			],
		searchitems : [
			{display: 'User Type', name : 'type', isdefault: true}
			//{display: 'Schedule Day', name : 'weekday'}
			],
		sortname: "",
		sortorder: "asc",
		usepager: true,
		title: 'Scheduled Reports List',
		useRp: true,
		rp: 15,
		showTableToggleBtn: true,
		width: "960",
		height: "auto"
	});  
}



function eventHandlerReportSchedule(com,grid) {

	if (com=='Add')	{
		location.href="reports_schedule.php";		
	}
	else if (com=='Send Mail'){ 
		location.href="weeklysendreportscript.php"; 
	}
	 else if( $('.trSelected',grid).length<=0 ||  $('.trSelected',grid).length>1) {
			alert("Please Select One Record.");
			return false;
	}
		
	var items=$('.trSelected',grid);		
		
	if (com=='Edit'){ 
		var rpt_id = items[0].id.substr(3); 
		sessionStorage.setItem("rpt_id", rpt_id); 
		//location.href="reports_schedule.php?rpt_id="+items[0].id.substr(3);
		location.href="reports_schedule.php"; 
	}
		
	if (com=='Delete'){
		if(confirm("Are you sure, you want to delete selected data?")){
			$.post("report_schedule_list.php", {"delete":"yes","id":items[0].id.substr(3)},function(data){		 
				  if(data!="") {					 
					 $('#flex1').flexOptions().flexReload(); 
				  } else {
					  alert("Record doesn't exists.")
				  }
				 });		
			}			
		}	
	}


/*********************************** Report Emailer Schedule ****************************************/



/******** Not Visited Dealer List 14 JULY 2014 ************************/

	
	
function showNotVisitedDealerReport(){
$("#flex1").flexigrid({
	url: 'notvisitedDealerReport.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Retailer Name', name : 'retailer_name', width : 180, sortable : true, align: 'left'},
		{display: 'Retailer Class', name : 'relationship_code', width : 80, sortable : true, align: 'left'},
		{display: 'City', name : 'city', width : 100, sortable : true, align: 'left'},
		{display: 'State', name : 'item_name', width : 100, sortable : true, align: 'left'},
		{display: 'Retailer Address', name : 'retailer_address', width : 200, sortable : true, align: 'left'},
		{display: 'Retailer Phone No', name : 'retailer_phone_no', width : 100, sortable : true, align: 'left'},
		{display: 'Market', name : 'opening_stock_quantity', width :180, sortable : true, align: 'left'}
		//{display: 'Date', name : 'date_of_order', width : 80, sortable : true, align: 'left'},
		//{display: 'Time', name : 'time_of_order', width :60, sortable : true, align: 'left'}					
		],
	searchitems : [
		{display: 'Retailer Name', name : 'retailer_name', isdefault: true},
		{display: 'City', name : 'city'}
		],
	sortname: "retailer_name",
	sortorder: "asc",
	usepager: true,
	title: 'Not Vissited Dealers Report',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 980,
	height: 'auto'
});  
}	
	
	
/******** Not Visited Dealer List 14 JULY 2014 ************************/
	
	



/*******************************************************************
* desc : SKU based promotions data grid
* Created on : 05 January 2015
* Author : AV 
*/


function showPromotion(){
$("#flex1").flexigrid({
	url: 'promotion.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Chain(Group)', name : 'C.chain_name', width : 200, sortable : true, align: 'left'},
		{display: 'Item Code(SKU)', name : 'I.item_name', width : 220, sortable : true, align: 'left'},
		//{display: 'Item Code', name : 'I.item_code', width : 220, sortable : true, align: 'left'},
		{display: 'Item promotion', name : 'P.promo_dec', width : 200, sortable : true, align: 'left'},
		{display: 'Start Date', name : 'P.start_date', width : 65, sortable : true, align: 'left'},
		{display: 'End Date', name : 'P.end_date', width : 65, sortable : true, align: 'left'},
		{display: 'last updated', name : 'P.last_updated_on', width : 100, sortable : true, align: 'left'},
		{display: 'Status', name : 'P.status', width : 50, sortable : true, align: 'left'}
		],
	buttons : [
		{name: 'Add', bclass: 'add', onpress : eventHandlerPromotion},
		{separator: true},		
		{name: 'Edit', bclass: 'edit', onpress : eventHandlerPromotion},
		{separator: true},
		{name: 'Import', bclass: 'import', onpress : eventHandlerPromotion},
		{separator: true}
		
		],
	searchitems : [
		{display: 'Item Code', name : 'I.item_code', isdefault: true},
		{display: 'promotion', name : 'P.promo_desc'},
		{display: 'Chain Name', name : 'C.chain_name'}
		
		],
	sortname: "I.item_code",
	sortorder: "ASC",
	usepager: true,
	title: 'Promotion',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: "980",
	height: 'auto'
});  
}
function eventHandlerPromotion(com,grid)
{
	if (com=='Add')	{
		location.href="promotion.php?action=add";		
	} else if (com=='Import')	{
		location.href="promotion.php?action=import";		
	} else{
		if( $('.trSelected',grid).length<=0 ||  $('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=$('.trSelected',grid);		
		if (com=='Edit'){
			location.href="promotion.php?action=add&id="+items[0].id.substr(3);			
		}
		if (com=='Delete'){
			if(confirm("Are you sure, you want to delete selected Category?")){
					
				$.post("promotion.php", {"delete":"yes","id":items[0].id.substr(3)},function(data)
				{		 
				  if(data!="") {					 
					 $('#flex1').flexOptions().flexReload(); 
				  }else {
					  alert("Record doesn't exists.")
				  }
				  
				});		
			}			
		}	
	}
}




function showBrands(){
$("#flex1").flexigrid({
	url: 'brands.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Brands Name', name : 'brands_name', width : 280, sortable : true, align: 'left'},
		{display: 'Status', name : 'status', width : 200, sortable : true, align: 'left'}
		],
	buttons : [
		{name: 'Add', bclass: 'add', onpress : eventHandlerBrands},
		{separator: true},		
		{name: 'Edit', bclass: 'edit', onpress : eventHandlerBrands},
		{separator: true},			
		/*{name: 'Import', bclass: 'import', onpress : eventHandlerBrands},
		{separator: true}*/
		
		],
	searchitems : [
		{display: 'Brands name', name : 'brand_name', isdefault: true}
		
		],
	sortname: "brand_name",
	sortorder: "asc",
	usepager: true,
	title: 'Brands',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 850,
	height: 'auto'
});  
}
function eventHandlerBrands(com,grid)
{

	if (com=='Import')	{
		location.href="city_import.php";		
	}
	else if (com=='Export')	{
		location.href="#.php?export_route";		
	}
	else if (com=='Add')	{
		location.href="brands.php?action=add";		
	}else{
		if( $('.trSelected',grid).length<=0 ||  $('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=$('.trSelected',grid);	
		
		
		if (com=='Edit'){
			location.href="brands.php?action=edit&id="+items[0].id.substr(3);			
		}
		if (com=='Delete'){
			if(confirm("Are you sure, you want to delete selected city?")){
					
				$.post("city.php", {"delete":"yes","id":items[0].id.substr(3)},function(data)
				{		 
				  if(data!="") {					 
					 $('#flex1').flexOptions().flexReload(); 
					 alert('City deleted');
				  }else {
					  alert("Record doesn't exists.")
				  }
				  
				});		
			}			
		}
		
	}
}

/*---------------------------------------------
   Discription: show offer list in flexigrid
   date: 21 may
   by nizam
-----------------------------------------------*/

function showOffer(){
$("#flex1").flexigrid({
	url: 'offer.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Offer Name', name : 'offer_name', width : 280, sortable : true, align: 'left'},
		{display: 'Status', name : 'status', width : 200, sortable : true, align: 'left'}
		],
	buttons : [
		{name: 'Add', bclass: 'add', onpress : eventHandlerOffers},
		{separator: true},		
		{name: 'Edit', bclass: 'edit', onpress : eventHandlerOffers},
		{separator: true},			
		/*{name: 'Import', bclass: 'import', onpress : eventHandlerBrands},
		{separator: true}*/
		
		],
	searchitems : [
		{display: 'Offer name', name : 'offer_name', isdefault: true}
		
		],
	sortname: "offer_name",
	sortorder: "asc",
	usepager: true,
	title: 'Offers',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 850,
	height: 'auto'
});  
}
function eventHandlerOffers(com,grid)
{

	if (com=='Import')	{
		location.href="city_import.php";		
	}
	else if (com=='Export')	{
		location.href="#.php?export_route";		
	}
	else if (com=='Add')	{
		location.href="offer.php?action=add";		
	}else{
		if( $('.trSelected',grid).length<=0 ||  $('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=$('.trSelected',grid);	
		
		
		if (com=='Edit'){
			location.href="offer.php?action=edit&id="+items[0].id.substr(3);			
		}
		if (com=='Delete'){
			if(confirm("Are you sure, you want to delete selected city?")){
					
				$.post("city.php", {"delete":"yes","id":items[0].id.substr(3)},function(data)
				{		 
				  if(data!="") {					 
					 $('#flex1').flexOptions().flexReload(); 
					 alert('City deleted');
				  }else {
					  alert("Record doesn't exists.")
				  }
				  
				});		
			}			
		}
		
	}
}





/*---------------------------------------------
   Discription	: show cases list in flexigrid
   Date         : 21 may
   by           : nizam
-----------------------------------------------*/

function showCases(){
$("#flex1").flexigrid({
	url: 'cases.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Case Description', name : 'case_description', width : 280, sortable : true, align: 'left'},
		{display: 'Case Size', name : 'case_size', width : 280, sortable : true, align: 'left'},
		{display: 'Status', name : 'status', width : 200, sortable : true, align: 'left'}
		],
	buttons : [
		{name: 'Add', bclass: 'add', onpress : eventHandlerCases},
		{separator: true},		
		{name: 'Edit', bclass: 'edit', onpress : eventHandlerCases},
		{separator: true},			
		/*{name: 'Import', bclass: 'import', onpress : eventHandlerBrands},
		{separator: true}*/
		
		],
	searchitems : [
		{display: 'Case Description', name : 'case_description', isdefault: true}
		
		],
	sortname: "case_description",
	sortorder: "asc",
	usepager: true,
	title: 'Cases',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 850,
	height: 'auto'
});  
}
function eventHandlerCases(com,grid)
{

	if (com=='Add')	{
		location.href="cases.php?action=add";		
	}else{
		if( $('.trSelected',grid).length<=0 ||  $('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=$('.trSelected',grid);	
		
		
		if (com=='Edit'){
			location.href="cases.php?action=edit&id="+items[0].id.substr(3);			
		}
		if (com=='Delete'){
			if(confirm("Are you sure, you want to delete selected city?")){
					
				$.post("city.php", {"delete":"yes","id":items[0].id.substr(3)},function(data)
				{		 
				  if(data!="") {					 
					 $('#flex1').flexOptions().flexReload(); 
					 alert('City deleted');
				  }else {
					  alert("Record doesn't exists.")
				  }
				  
				});		
			}			
		}
		
	}
}





function showDistributorGrnStock(){
$("#flex1").flexigrid({
	url: 'distributor_grn.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Distributor Name', name : 'distributor_code', width : 200, sortable : true, align: 'left'},
		{display: 'Distributor Code', name : 'distributor_code', width : 200, sortable : true, align: 'left'},
		{display: 'Bill Date', name : 'bill_date', width : 200, sortable : true, align: 'left'},
		{display: 'Bill Number', name : 'bill_no', width : 200, sortable : true, align: 'left'},
		{display: 'Last Update Date', name : 'last_update_datetime', width : 200, sortable : true, align: 'left'}		
		],
	buttons : [
		/*{name: 'Add', bclass: 'add', onpress : eventDistributorGrnStock},
		{separator: true},		
		{name: 'Edit', bclass: 'edit', onpress : eventHandlerMasterCategory},
		{separator: true},*/
		{name: 'Accept GRN Transfer', bclass: 'transfer', onpress : eventDistributorGrnStock},
		{separator: true}
		],
	searchitems : [
		{display: 'Distributor Name', name : 'distributor_name', isdefault: true},
		{display: 'Distributor Code', name : 'distributor_code'},
		{display: 'Bill Date', name : 'bill_date'},
		{display: 'Bill Numuber', name : 'bill_no'}
		
		],
	sortname: "distributor_name",
	sortorder: "asc",
	usepager: true,
	title: 'Distributor GRN',
	useRp: true,
	rp: 40,
	showTableToggleBtn: true,
	width: 1000,
	height: 'auto'
});  
}

function eventDistributorGrnStock(com,grid)
{
  
	if (com=='Add')	{
		location.href="cases.php?action=add";		
	}else{
		if( $('.trSelected',grid).length<=0 ||  $('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=$('.trSelected',grid);	
		
		
		if (com=='Accept GRN Transfer'){
			location.href="stock_transfer.php?data="+items[0].id.substr(3);			
		}
		
		
	}
}

/*
   Description : Salesman return show in flexigrid.
   date        : 1 june
   by 		   : Nizam
*/


function showReturns(){

$("#flex1").flexigrid({
 url: 'return.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [
        {display: 'Distributor Name', name : 'distributor_name', width : 200, sortable : true, align: 'left'},
		{display: 'Distributor Code', name : 'distributor_code', width : 200, sortable : true, align: 'left'},
		{display: 'Return Date', name : 'date_of_order', width : 200, sortable : true, align: 'left'},
		{display: 'Return Time', name : 'time_of_order', width : 200, sortable : true, align: 'left'},
		{display: 'Last Update Date', name : 'last_update_date', width : 200, sortable : true, align: 'left'},
		
		],
	buttons : [
		{name: 'Add', bclass: 'add2', onpress : eventSalesReturn},
		{separator: true},		
		{name: 'View Primary Return', bclass: 'orderdeails', onpress : eventSalesReturn},
		{separator: true}
		],
	searchitems : [
		{display: 'Distributor Name', name : 'distributor_name',isdefault: true},
		{display: 'Distributor Code', name : 'distributor_code'}
		],
	sortname: "distributor_name",
	sortorder: "desc",
	usepager: true,
	title: 'Primary Return',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 1000,
	height: 'auto'
});  
}


function eventSalesReturn(com,grid)
{
	
	if (com=='Add')	{
		location.href="return.php?action=add";
	}
	else{
		if( $('.trSelected',grid).length<=0 ||  $('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=$('.trSelected',grid);		
		if (com=='View Primary Return'){
			var searchParam_1 = $('input[name=q]').val();
			var searchParam_2 = $('select[name=qtype]').val();
			var searchParam_3 = $(".pcontrol").find("input").val();
			location.href="return_list.php?id="+items[0].id.substr(3);	
		}	
	}
	
}


function showDistributorTarget(){

$("#flex1").flexigrid({
 url: 'distributortarget.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [
        {display: 'Distributor Name', name : 'distributor_name', width : 200, sortable : true, align: 'left'},
		{display: 'Distributor Code', name : 'distributor_code', width : 200, sortable : true, align: 'left'},
		{display: 'Target Type', name : 'target_type', width : 100, sortable : true, align: 'left'},
		{display: 'Month', name : 'target_month', width : 100, sortable : true, align: 'left'},
		{display: 'Year', name : 'target_year', width : 70, sortable : true, align: 'left'},		
		{display: 'Status', name : 'status', width : 70, sortable : true, align: 'left'},
		
		],
	buttons : [
		{name: 'Add', bclass: 'add2', onpress : eventDistributorTarget},
		{separator: true},		
		{name: 'Edit', bclass: 'orderdeails', onpress : eventDistributorTarget},
		{separator: true}
		],
	searchitems : [
		{display: 'Distributor Name', name : 'distributor_name',isdefault: true},
		{display: 'Distributor Code', name : 'distributor_code'},
		{display: 'Target Type', name : 'target_type'}
		],
	sortname: "distributor_name",
	sortorder: "desc",
	usepager: true,
	title: 'Distributor Target',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 750,
	height: 'auto'
});  
}


function eventDistributorTarget(com,grid)
{
	
	if (com=='Add')	{
		location.href="distributortarget.php?action=add";
	}
	else{
		if( $('.trSelected',grid).length<=0 ||  $('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=$('.trSelected',grid);		
		if (com=='Edit'){
			location.href="distributortarget.php?action=edit&id="+items[0].id.substr(3);
		}	
	}
	
}



/*  
Description : Show Retailer Type List Master
date: 10 jun
BY Nizam 

*/


function showRetailerType(){
$("#flex1").flexigrid({
	url: 'retailer_type.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Retailer Type', name : 'type_name', width : 280, sortable : true, align: 'left'},
		{display: 'Status', name : 'status', width : 200, sortable : true, align: 'left'}
		],
	buttons : [
		{name: 'Add', bclass: 'add', onpress : eventHandlerRetailerType},
		{separator: true},		
		{name: 'Edit', bclass: 'edit', onpress : eventHandlerRetailerType},
		{separator: true}
		
		],
	searchitems : [
		{display: 'Retailer Type', name : 'type_name', isdefault: true}
		
		],
	sortname: "type_name",
	sortorder: "asc",
	usepager: true,
	title: 'Retailer Type Name',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 850,
	height: 'auto'
});  
}
function eventHandlerRetailerType(com,grid)
{

	if (com=='Add')	{
		location.href="retailer_type.php?action=add";		
	}else{
		if( $('.trSelected',grid).length<=0 ||  $('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=$('.trSelected',grid);	
		
		
		if (com=='Edit'){
			location.href="retailer_type.php?action=edit&id="+items[0].id.substr(3);			
		}
		
		
	}
}












	/***************************************************************************************
	* DESC : Order list of retailer taken by distributor Panel
	* Author : AJAY
 	* Created : 9th June 2015
 	* 
 	**/



function showRetailerOrderByDistributor(){
$("#flex1").flexigrid({
	url: 'order.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [
		{display: 'Distributor Name', name : 'distributor_name', width : 150, sortable : true, align: 'left'},
		{display: 'Salesperson Name', name : 'salesman_name', width : 150, sortable : true, align: 'left'},
		{display: 'Retailer Name', name : 'retailer_name', width : 200, sortable : true, align: 'left'},
		//{display: 'Retailer Market', name : 'retailer_location', width : 100, sortable : true, align: 'left'},
		//{display: 'Retailer Address', name : 'retailer_address', width : 150, sortable : true, align: 'left'},
		{display: 'Total Invoice Amount', name : 'acc_total_invoice_amount', width : 150, sortable : true, align: 'left'},
		//{display: 'Total no of items', name : 'total_item', width : 80, sortable : true, align: 'left'},
		{display: 'Last Update Order', name : 'date_of_order', width : 150, sortable : true, align: 'left'},
		{display: 'Order Status', name : 'order_status', width : 130, sortable : true, align: 'left'}
		],
	buttons : [		
		{name: 'Add', bclass: 'add', onpress : eventHandlershowRetailerOrderByDistributor},
		{separator: true},
		{name: 'Edit', bclass: 'edit', onpress : eventHandlershowRetailerOrderByDistributor},
		{separator: true},
		{name: 'View & Confirm Order', bclass: 'confirm', onpress : eventHandlershowRetailerOrderByDistributor},
		{separator: true}
		],
	searchitems : [
		{display: 'Distributor Name', name : 'distributor_name', isdefault: true},
		{display: 'Salesperson Name', name : 'salesman_name'},
		{display: 'Retailer Name', name : 'retailer_name'}
		],
	sortname: "date_of_order",
	sortorder: "desc",
	usepager: true,
	title: 'Last Saved Orders List',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 1000,
	height: 'auto'
});  
}


function eventHandlershowRetailerOrderByDistributor(com,grid)

{
	if (com=='Add')	{
		location.href="order.php?action=add";		
	} else if (com=='Import')	{
		location.href="";		
	} else{
		if( $('.trSelected',grid).length<=0 ||  $('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=$('.trSelected',grid);		
		if (com=='Edit'){
			location.href="order.php?action=edit&id="+items[0].id.substr(3);			
		}

		if (com=='View & Confirm Order'){
			location.href="order.php?action=confirm&id="+items[0].id.substr(3);	
		}


		if (com=='Delete'){
			if(confirm("Are you sure, you want to delete selected Category?")){
					
				$.post("order.php", {"delete":"yes","id":items[0].id.substr(3)},function(data)
				{		 
				  if(data!="") {					 
					 $('#flex1').flexOptions().flexReload(); 
				  }else {
					  alert("Record doesn't exists.")
				  }
				  
				});		
			}			
		}	
	}
}



/* Description: Show Item varient
   Date : 11 jun
   By : Nizam
   
*/

function showItemVariant(){
$("#flex1").flexigrid({
	url: 'variant.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Variant Name', name : 'variant_name', width : 280, sortable : true, align: 'left'},
		{display: 'Status', name : 'status', width : 200, sortable : true, align: 'left'}
		],
	buttons : [
		{name: 'Add', bclass: 'add', onpress : eventHandlerVariant},
		{separator: true},		
		{name: 'Edit', bclass: 'edit', onpress : eventHandlerVariant},
		{separator: true},			
		
		],
	searchitems : [
		{display: 'Variant name', name : 'variant_name', isdefault: true}
		
		],
	sortname: "variant_name",
	sortorder: "asc",
	usepager: true,
	title: 'Variant Name',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 850,
	height: 'auto'
});  
}


function eventHandlerVariant(com,grid)
{
    if(com=='Add'){
		location.href="variant.php?action=add";		
	}else{
		if( $('.trSelected',grid).length<=0 ||  $('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=$('.trSelected',grid);	
		
		
		if (com=='Edit'){
			location.href="variant.php?action=edit&id="+items[0].id.substr(3);			
		}
		/*if (com=='Delete'){
			if(confirm("Are you sure, you want to delete selected city?")){
					
				$.post("city.php", {"delete":"yes","id":items[0].id.substr(3)},function(data)
				{		 
				  if(data!="") {					 
					 $('#flex1').flexOptions().flexReload(); 
					 alert('City deleted');
				  }else {
					  alert("Record doesn't exists.")
				  }
				  
				});		
			}			
		}*/
		
	}
}



/* Description: Show Sku
   Date : 11 jun
   By : Nizam
   
*/

function showSku(){
$("#flex1").flexigrid({
	url: 'sku.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Sku Name', name : 'sku_name', width : 280, sortable : true, align: 'left'},
		{display: 'Status', name : 'status', width : 200, sortable : true, align: 'left'}
		],
	buttons : [
		{name: 'Add', bclass: 'add', onpress : eventHandlerSku},
		{separator: true},		
		{name: 'Edit', bclass: 'edit', onpress : eventHandlerSku},
		{separator: true},			
		],
	searchitems : [
		{display: 'Sku name', name : 'sku_name', isdefault: true}
		
		],
	sortname: "sku_name",
	sortorder: "asc",
	usepager: true,
	title: 'Sku Name',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 850,
	height: 'auto'
});  
}


function eventHandlerSku(com,grid)
{
    if(com=='Add'){
		location.href="sku.php?action=add";		
	}else{
		if( $('.trSelected',grid).length<=0 ||  $('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=$('.trSelected',grid);	
		
		
		if (com=='Edit'){
			location.href="sku.php?action=edit&id="+items[0].id.substr(3);			
		}
		/*if (com=='Delete'){
			if(confirm("Are you sure, you want to delete selected city?")){
					
				$.post("city.php", {"delete":"yes","id":items[0].id.substr(3)},function(data)
				{		 
				  if(data!="") {					 
					 $('#flex1').flexOptions().flexReload(); 
					 alert('City deleted');
				  }else {
					  alert("Record doesn't exists.")
				  }
				  
				});		
			}			
		}*/
		
	}
}



/*---------------------------------------------
   Discription: show region list in flexigrid
   date: 21 may
   by nizam
-----------------------------------------------*/

function showRegion(){
$("#flex1").flexigrid({
	url: 'region.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Region Name', name : 'region_name', width : 280, sortable : true, align: 'left'},
		{display: 'Status', name : 'status', width : 200, sortable : true, align: 'left'}
		],
	buttons : [
		{name: 'Add', bclass: 'add', onpress : eventHandlerRegions},
		{separator: true},		
		{name: 'Edit', bclass: 'edit', onpress : eventHandlerRegions},
		{separator: true},			
		/*{name: 'Import', bclass: 'import', onpress : eventHandlerBrands},
		{separator: true}*/
		
		],
	searchitems : [
		{display: 'Region name', name : 'region_name', isdefault: true}
		],
	sortname: "region_name",
	sortorder: "asc",
	usepager: true,
	title: 'Regions',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 850,
	height: 'auto'
});  
}
function eventHandlerRegions(com,grid)
{
    if(com=='Add'){
		location.href="region.php?action=add";		
	 }else{
		if( $('.trSelected',grid).length<=0 ||  $('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=$('.trSelected',grid);	
		if (com=='Edit'){
			location.href="region.php?action=edit&id="+items[0].id.substr(3);			
		}
		
		
	}
}


/*----------------------------- function added by Gaurav on 30 July 15 ----------------------*/

function showMarketIntelligence(){
$("#flex1").flexigrid({
	url: 'market_intelligence.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Salesman Name', name : 'salesman_name', width : 100, sortable : true, align: 'left'},
		//{display: 'Salesman Level', name : 'description', width : 180, sortable : true, align: 'left'},
		{display: 'Company Name', name : 'company_name', width : 100, sortable : true, align: 'left'},
		{display: 'Category', name : 'category', width : 100, sortable : true, align: 'left'},
		{display: 'Subcategory', name : 'subcategory', width : 100, sortable : true, align: 'left'},
		{display: 'Description', name : 'description', width : 300, sortable : true, align: 'left'},
		{display: 'Date', name : 'app_date', width : 80, sortable : true, align: 'left'},
		{display: 'Time', name : 'app_time', width : 80, sortable : true, align: 'left'}
		],
	buttons : [
		{name: 'View', bclass: 'viewmsg', onpress : eventHandlerMarketIntelligence},
		{separator: true}
		],
	searchitems : [
		{display: 'Salesman Name', name : 's.salesman_name', isdefault: true}
		],
	sortname: "market_intelligence_id",
	sortorder: "desc",
	usepager: true,
	title: 'Market Intelligence',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 950,
	height: 'auto'
});  
}
function eventHandlerMarketIntelligence(com,grid)
{
	if (com=='Send Message')	{
		//location.href="send_message.php";		
	}else{
		if( $('.trSelected',grid).length<=0 ||  $('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=$('.trSelected',grid);		
		if (com=='View'){
			window.open("view_market_intll.php?id="+items[0].id.substr(3), '_blank');
		}
	}
}

//


function showPlannedVsVisited(){
$("#flex1").flexigrid({
	url: 'plannedvsvisited_report.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel: [
	   	{display: 'Salesman', name : 'salesman_name', width : 80, sortable : true, align: 'left'},
	   	{display: 'State', name : 'state_name', width : 80, sortable : true, align: 'left'},
	   	{display: 'Reporting To', name : 'rpt_to', width : 90, sortable : true, align: 'left'},
		{display: 'Total Planned Route', name : 'plannedAccount', width :130, sortable : true, align: 'left'},
		{display: 'Total Visited Route(Planned)', name : 'plannedVisitedAccount', width :150, sortable : true, align: 'left'},
		//{display: 'Total Visited Route(Unplanned)', name : 'unplannedVisitedAccount', width :150, sortable : true, align: 'left'},
		{display: 'Working Hours', name : 'start_time', width :100, sortable : true, align: 'left'}
		//{display: 'Total Sale', name : 'unplannedVisitedAccount', width :100, sortable : true, align: 'left'}
		],
	buttons : [			
			{name: 'View Detail', bclass: 'view', onpress : eventHandlerPLVsVS},
			{separator: true}
		],
	searchitems : [
		{display: 'Salesman', name : 'salesman_name', isdefault: true}		
		],
	sortname: "last_updated_on",
	sortorder: "desc",
	usepager: true,
	title: 'Beat Adherence Report',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 1000,
	height: 'auto'
});  
}
	
function eventHandlerPLVsVS(com,grid){

	if( $('.trSelected',grid).length<=0 ||  $('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
	}
		var items=$('.trSelected',grid);		
		if (com=='View Detail'){
			window.open("plannedvisitedtown_detail.php?id="+items[0].id.substr(3), '_blank');		
		}
}


/*
  Description: show Customer list 
  Date: 1 oct 2015
  By : Nizam 
*/

function showCustomers(){
	$("#flex1").flexigrid({
	url: 'customer.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Name', name : 'customer_name', width : 80, sortable : true, align: 'left'},
		{display: 'Customer Code', name : 'customer_code', width : 80, sortable : true, align: 'left'},
		{display: 'Customer Type', name : 'type_name', width : 80, sortable : true, align: 'left'},
		{display: 'Customer Class', name : 'customer_class', width : 80, sortable : true, align: 'left'},
		{display: 'Added by(Salesman)', name : 'salesman_name', width : 80, sortable : true, align: 'left'},
		{display: 'Salesman Code', name : 'salesman_code', width : 80, sortable : true, align: 'left'},
		{display: 'Division', name : 'division_name', width : 80, sortable : true, align: 'left'},
		{display: 'Customer Region', name : 'region_name', width : 92, sortable : true, align: 'left'},
		{display: 'Interested', name : 'display_outlet', width : 50, sortable : true, align: 'left'},
		{display: 'State', name : 'state_name', width : 80, sortable : true, align: 'left'},
		{display: 'District', name : 'city_name', width : 80, sortable : true, align: 'left'},
		{display: 'Taluka', name : 'taluka_name', width : 80, sortable : true, align: 'left'},
		{display: 'City', name : 'market_name', width : 100, sortable : true, align: 'left'},
		{display: 'Pincode', name : 'pin_code', width : 100, sortable : true, align: 'left'},
		{display: 'Address 1', name : 'customer_address', width : 100, sortable : true, align: 'left'},
		{display: 'Address 2', name : 'customer_address2', width : 100, sortable : true, align: 'left'},
		{display: 'Map', name : 'lat', width : 80, sortable : true, align: 'left'},
		//{display: 'Send SMS Mobile No', name : 'sms_number', width : 100, sortable : true, align: 'left'},
		{display: 'Phone Number 1', name : 'customer_phone_no', width : 80, sortable : true, align: 'left'},
		{display: 'Phone Number 2', name : 'customer_phone_no2', width : 80, sortable : true, align: 'left'},
		{display: 'Phone Number 3', name : 'customer_phone_no3', width : 80, sortable : true, align: 'left'},
		{display: 'Landline Number', name : 'customer_leadline_no', width : 80, sortable : true, align: 'left'},
		{display: 'Contact Person 1', name : 'contact_person', width : 80, sortable : true, align: 'left'},
		{display: 'Contact Number 1', name : 'contact_number', width : 80, sortable : true, align: 'left'},
		{display: 'Contact Person 2', name : 'contact_person2', width : 80, sortable : true, align: 'left'},
		{display: 'Contact Number 2', name : 'contact_number2', width : 80, sortable : true, align: 'left'},
		{display: 'Contact Person 3', name : 'contact_person3', width : 80, sortable : true, align: 'left'},
		{display: 'Contact Number 3', name : 'contact_number3', width : 80, sortable : true, align: 'left'},
		{display: 'Username', name : 'username', width : 80, sortable : true, align: 'left'},
		{display: 'Email ID 1', name : 'customer_email', width : 100, sortable : true, align: 'left'},
		{display: 'Email ID 2', name : 'customer_email2', width : 100, sortable : true, align: 'left'},
		{display: 'Email ID 3', name : 'customer_email3', width : 100, sortable : true, align: 'left'},
		{display: 'Date of Birth', name :'customer_dob', width : 80, sortable : true, align: 'left'},
		{display: 'Start Date', name : 'start_date', width : 80, sortable : true, align: 'left'},
		{display: 'End Date', name : 'end_date', width : 80, sortable : true, align: 'left'},
		{display: 'Validation Status', name : 'validation_status', width : 80, sortable : true, align: 'center'},
		{display: 'Not Validation Reason', name : 'validation_reason', width : 80, sortable : true, align: 'center'},
		{display: 'Remark', name : 'remark', width : 80, sortable : true, align: 'left'}
		/*{display: 'Distributor Status', name : 'status', width : 80, sortable : true, align: 'center'},
		{display: 'Login Status', name : 'loginStatus', width : 80, sortable : true, align: 'center'}*/
			
		],
	buttons : [
		{name: 'Add', bclass: 'add', onpress : eventHandlerCustomerAdminUser},
		{separator: true},		
		{name: 'Edit', bclass: 'edit', onpress : eventHandlerCustomerAdminUser},
		{separator: true},
		{name: 'Delete', bclass: 'delete', onpress : eventHandlerCustomerAdminUser},
		{separator: true}
		],
	searchitems : [
		{display: 'Name', name : 'customer_name', isdefault: true},
		{display: 'Username', name : 'username'},
		{display: 'Added by', name : 'salesman_name'},
		{display: 'Division', name : 'division_name'},
		{display: 'Email ID', name : 'customer_email'},
		{display: 'Send SMS Mobile No', name : 'sms_number'},
		{display: 'Phone Number', name : 'customer_phone_no'},
		{display: 'State', name : 'state_name'},
		{display: 'District', name : 'city_name'},
		{display: 'Address', name : 'customer_address'},
		{display: 'City', name : 'customer_location'},
		{display: 'Contact Person', name : 'contact_person'},
		{display: 'Customer Type', name : 'type_name'},
		{display: 'Contact Number', name : 'contact_number'}
		],
	sortname: "customer_name",
	sortorder: "asc",
	usepager: true,
	title: 'Customers',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 700,
	height: 'auto'
});  
}
function eventHandlerCustomerAdminUser(com,grid)
{
	if (com=='Add')	{
		location.href="customer.php?add";	
	}else if (com=='Import')	{
		location.href="customer.php?import";	
	}else if (com=='Export')	{
		location.href="export.inc.php?export_distributors_list";	
	}
	else{
		if( $('.trSelected',grid).length<=0 ||  $('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=$('.trSelected',grid);		
		if (com=='Edit'){
			location.href="customer.php?id="+items[0].id.substr(3);			
		}else if (com=='Create Login')	{
		location.href="#.php?login&id="+items[0].id.substr(3);
	}

	else if (com=='Delete')	{
		
		if(confirm("Are you sure, you want to delete selected Customer?")){
					
				$.post("customer.php", {"delete":"yes","id":items[0].id.substr(3)},function(data)
				{

					$("#message-green").hide();
				  if(data!="") {

					 $('#flex1').flexOptions().flexReload(); 
				  }else {
					  alert("Record doesn't exists.")
				  }
				  
				});		
			}
	}
			
	}
}



function newDistributors(){
$("#flex1").flexigrid({
	url: 'new_distributors.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Name', name : 'distributor_name', width : 80, sortable : true, align: 'left'},
		{display: 'Division', name : 'division_name', width : 80, sortable : true, align: 'left'},
		{display: 'Distributor Code', name : 'distributor_code', width : 80, sortable : true, align: 'left'},
		{display: 'Distributor Class', name : 'relationship_code', width : 80, sortable : true, align: 'left'},
		{display: 'Added by(Salesman)', name : 'salesman_name', width : 80, sortable : true, align: 'left'},
		{display: 'Salesman Code', name : 'salesman_code', width : 80, sortable : true, align: 'left'},
		{display: 'Distributor Region', name : 'region_name', width : 92, sortable : true, align: 'left'},
		{display: 'State', name : 'state_name', width : 80, sortable : true, align: 'left'},
		{display: 'District', name : 'city_name', width : 80, sortable : true, align: 'left'},
		{display: 'Taluka', name : 'taluka_name', width : 80, sortable : true, align: 'left'},
		{display: 'City', name : 'market_name', width : 100, sortable : true, align: 'left'},

		{display: 'Address 1', name : 'distributor_address', width : 100, sortable : true, align: 'left'},
		{display: 'Address 2', name : 'distributor_address2', width : 100, sortable : true, align: 'left'},
		{display: 'Send SMS Mobile No', name : 'sms_number', width : 100, sortable : true, align: 'left'},
		{display: 'Phone Number 1', name : 'distributor_phone_no', width : 80, sortable : true, align: 'left'},
		{display: 'Phone Number 2', name : 'distributor_phone_no2', width : 80, sortable : true, align: 'left'},
		{display: 'Phone Number 3', name : 'distributor_phone_no3', width : 80, sortable : true, align: 'left'},
		{display: 'Landline Number', name : 'distributor_leadline_no', width : 80, sortable : true, align: 'left'},
		{display: 'Contact Person 1', name : 'contact_person', width : 80, sortable : true, align: 'left'},
		{display: 'Contact Number 1', name : 'contact_number', width : 80, sortable : true, align: 'left'},
		{display: 'Contact Person 2', name : 'contact_person2', width : 80, sortable : true, align: 'left'},
		{display: 'Contact Number 2', name : 'contact_number2', width : 80, sortable : true, align: 'left'},
		{display: 'Contact Person 3', name : 'contact_person3', width : 80, sortable : true, align: 'left'},
		{display: 'Contact Number 3', name : 'contact_number3', width : 80, sortable : true, align: 'left'},
		{display: 'Username', name : 'username', width : 80, sortable : true, align: 'left'},
		{display: 'Email ID 1', name : 'distributor_email', width : 100, sortable : true, align: 'left'},
		{display: 'Email ID 2', name : 'distributor_email2', width : 100, sortable : true, align: 'left'},
		{display: 'Email ID 3', name : 'distributor_email3', width : 100, sortable : true, align: 'left'},
		{display: 'Date of Birth', name : 'distributor_dob', width : 80, sortable : true, align: 'left'},
		{display: 'Start Date', name : 'start_date', width : 80, sortable : true, align: 'left'},
		{display: 'End Date', name : 'end_date', width : 80, sortable : true, align: 'left'},
		{display: 'Distributor Status', name : 'status', width : 80, sortable : true, align: 'center'},
		{display: 'Login Status', name : 'loginStatus', width : 80, sortable : true, align: 'center'}
			
		],
	buttons : [
		/*{name: 'Add', bclass: 'add', onpress : eventHandlerDisAdminUser},
		{separator: true},*/		
		{name: 'Edit', bclass: 'edit', onpress : eventHandlerDisAdminUser},
		{separator: true}
		],
	searchitems : [
		{display: 'Name', name : 'distributor_name', isdefault: true},
		{display: 'Division', name : 'division_name'},
		{display: 'Username', name : 'username'},
		{display: 'Email ID', name : 'distributor_email'},
		{display: 'Send SMS Mobile No', name : 'sms_number'},
		{display: 'Phone Number', name : 'distributor_phone_no'},
		{display: 'State', name : 'state_name'},
		{display: 'District', name : 'city_name'},
		{display: 'Address', name : 'distributor_address'},
		{display: 'City', name : 'distributor_location'},
		{display: 'Contact Person', name : 'contact_person'},
		{display: 'Contact Number', name : 'contact_number'}
		],
	sortname: "distributor_name",
	sortorder: "asc",
	usepager: true,
	title: 'Distributors',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 1000,
	height: 'auto'
});  
}
function eventHandlerDisAdminUser(com,grid)
{
	if (com=='Add')	{
		location.href="distributors.php?add";	
	}else if (com=='Import')	{
		location.href="distributors.php?import";	
	}else if (com=='Export')	{
		location.href="export.inc.php?export_distributors_list";	
	}
	else{
		if( $('.trSelected',grid).length<=0 ||  $('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=$('.trSelected',grid);		
		if (com=='Edit'){
			location.href="distributors.php?id="+items[0].id.substr(3);			
		}else if (com=='Create Login')	{
		location.href="#.php?login&id="+items[0].id.substr(3);
	}
		else if (com=='Delete')	{
		if(confirm("Are you sure, you want to delete selected Distributor?")){
					
				$.post("distributors.php", {"delete":"yes","id":items[0].id.substr(3)},function(data)
				{
					$("#message-green").hide();
				  if(data!="") {
				  	//alert(data);
				  	if(data >0 )
				  	{				  		
				  		alert("This distributor cannot be deleted.");
				  	}
				  				 
					 $('#flex1').flexOptions().flexReload(); 
				  }else {
					  alert("Record doesn't exists.")
				  }
				  
				});		
			}
	}
			
	}
}










// Added taluka and market data list


function showTaluka(){
$("#flex1").flexigrid({
	url: 'taluka.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Taluka', name : 'taluka_name', width : 100, sortable : true, align: 'left'},
		{display: 'Taluka Code', name : 'taluka_code', width : 100, sortable : true, align: 'left'},
		{display: 'District', name : 'city_name', width : 200, sortable : true, align: 'left'},
		{display: 'State Name', name : 'state_name', width : 100, sortable : true, align: 'left'},
		{display: 'Status', name : 'status', width : 100, sortable : true, align: 'left'},
		
		],
	buttons : [
		{name: 'Add', bclass: 'add', onpress : eventHandlerTaluka},
		{separator: true},		
		{name: 'Edit', bclass: 'edit', onpress : eventHandlerTaluka},
		{separator: true},
		{name: 'Delete', bclass: 'delete', onpress : eventHandlerTaluka},
		{separator: true},			
		{name: 'Import', bclass: 'import', onpress : eventHandlerTaluka},
		{separator: true},
		{name: 'Export', bclass: 'export', onpress : eventHandlerTaluka},
		{separator: true}
		
		],
	searchitems : [
		{display: 'Taluka', name : 'taluka_name', isdefault: true},
		{display: 'Taluka Code', name : 'taluka_code', isdefault: true},
		{display: 'District', name : 'city_name'},
		{display: 'State', name : 'state_name'}
		
		],
	sortname: "taluka_name",
	sortorder: "asc",
	usepager: true,
	title: 'Taluka',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 850,
	height: 'auto'
});  
}
function eventHandlerTaluka(com,grid)
{

	if (com=='Import')	{
		location.href="taluka_import.php";		
	}
	else if (com=='Export')	{
		location.href="export.inc.php?export_taluka_new";		
	}
	else if (com=='Add')	{
		location.href="add_taluka.php";		
	}
	else if (com=='delete')	{
		location.href="taluka.php";	}
		else{
		if( $('.trSelected',grid).length<=0 ||  $('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=$('.trSelected',grid);	
		
		
		if (com=='Edit'){
			location.href="add_taluka.php?id="+items[0].id.substr(3);			
		}
		if (com=='Delete'){
			if(confirm("Are you sure, you want to delete selected Taluka?")){
					
				$.post("taluka.php", {"delete":"yes","id":items[0].id.substr(3)},function(data)
				{		 
				   var res  = data.split("-?");
					if(res[0].trim()=="0") {					 
					 $('#flex1').flexOptions().flexReload(); 
					 alert('Taluka deleted');
				  }else {
					  alert("Taluka can not be deleted, it is mapped with "+res[0])
				  }
				  
				});		
			}			
		}
		
	}
}











function showMarkets(){
$("#flex1").flexigrid({
	url: 'market.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'City', name : 'market_name', width : 150, sortable : true, align: 'left'},
		{display: 'City Code', name : 'market_code', width : 150, sortable : true, align: 'left'},
		{display: 'Taluka', name : 'taluka_name', width : 150, sortable : true, align: 'left'},
		{display: 'District Name', name : 'city_name', width : 150, sortable : true, align: 'left'},
		{display: 'State', name : 'state_name', width : 150, sortable : true, align: 'left'},
		{display: 'Status', name : 'status', width : 150, sortable : true, align: 'left'},
		],
	buttons : [
		{name: 'Add', bclass: 'add', onpress : eventHandlerMarket},
		{separator: true},		
		{name: 'Edit', bclass: 'edit', onpress : eventHandlerMarket},
		{separator: true},
		{name: 'Delete', bclass: 'delete', onpress : eventHandlerMarket},
		{separator: true},			
		{name: 'Import', bclass: 'import', onpress : eventHandlerMarket},
		{separator: true},
		{name: 'Export', bclass: 'export', onpress : eventHandlerMarket},
		{separator: true}
		
		],
	searchitems : [
		{display: 'City', name : 'market_name', isdefault: true},
		{display: 'Taluka', name : 'taluka_name'},
		{display: 'District', name : 'city_name'},
		{display: 'State', name : 'state_name'}
		
		],
	sortname: "market_name",
	sortorder: "asc",
	usepager: true,
	title: 'City',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 850,
	height: 'auto'
});  
}
function eventHandlerMarket(com,grid)
{

	if (com=='Import')	{
		location.href="market_import.php";		
	}
	else if (com=='Export')	{
		location.href="export.inc.php?export_market_new";		
	}
	else if (com=='Add')	{
		location.href="add_market.php";		
	}
	else if (com=='delete')	{
		location.href="market.php";	}
		else{
		if( $('.trSelected',grid).length<=0 ||  $('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=$('.trSelected',grid);	
		
		
		if (com=='Edit'){
			location.href="add_market.php?id="+items[0].id.substr(3);			
		}
		if (com=='Delete'){
			if(confirm("Are you sure, you want to delete selected City?")){
					
				$.post("market.php", {"delete":"yes","id":items[0].id.substr(3)},function(data)
				{		 
				  var res  = data.split("-?");
					if(res[0].trim()=="0") {					 
					 $('#flex1').flexOptions().flexReload(); 
					 alert('City deleted');
				  }else {
					  alert("City can not be deleted, it is mapped with "+res[0])
				  }
				  
				});		
			}			
		}
		
	}
}



// Added taluka and market data list



/********************************** Division List 16 Oct 2015 Nizam *************************/

function showDivision(){
$("#flex1").flexigrid({
	url: 'division.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Division Name', name : 'division_name', width : 300, sortable : true, align: 'left'},
		{display: 'Status', name : 'status', width : 100, sortable : true, align: 'left'}
		],
	buttons : [
		{name: 'Add', bclass: 'add', onpress : eventHandlerDivision},
		{separator: true},		
		{name: 'Edit', bclass: 'edit', onpress : eventHandlerDivision},
		{separator: true}		
		],
	searchitems : [
		{display: 'Division Name', name : 'division_name', isdefault: true}
		
		],
	sortname: "division_name",
	sortorder: "asc",
	usepager: true,
	title: 'Division',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 700,
	height: 'auto'
});  
}
function eventHandlerDivision(com,grid)
{
	if (com=='Add')	{
		location.href="addDivision.php";		
	}else{
		if( $('.trSelected',grid).length<=0 ||  $('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=$('.trSelected',grid);		
		if (com=='Edit'){
			location.href="addDivision.php?id="+items[0].id.substr(3);			
		}
		if (com=='Delete'){
			if(confirm("Are you sure, you want to delete selected Branch?")){
					
				$.post("division.php", {"delete":"yes","id":items[0].id.substr(3)},function(data)
				{		 
				  if(data!="") {					 
					 $('#flex1').flexOptions().flexReload(); 
				  }else {
					  alert("Record doesn't exists.")
				  }
				  
				});		
			}			
		}	
	}
}


/********************************** Division List 16 Oct 2015 Nizam *************************/



function showSalesmanPlannedRoute(){
$("#flex1").flexigrid({
	url: 'salesmanplanned_report.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel: [
	   	{display: 'Name of Employee', name : 'salesman_name', width : 80, sortable : true, align: 'left'},
	   	{display: 'Employee Code', name : 'salesman_code', width : 80, sortable : true, align: 'left'},
	   	{display: 'Designation', name : 'designation', width : 80, sortable : true, align: 'left'},
	   	{display: 'Year', name : 'year', width : 90, sortable : true, align: 'left'},
		{display: 'Month', name : 'month', width :130, sortable : true, align: 'left'},
		{display: 'Date', name : 'assign_day', width :150, sortable : true, align: 'left'},
		{display: 'Route Name', name :'route_name', width :100, sortable : true, align: 'left'},
		{display: 'Retailer Name', name : 'retailer_name', width :100, sortable : true, align: 'left'},
		{display: 'Retailer Code', name : 'retailer_code', width :100, sortable : true, align: 'left'},
		/*{display: 'Achived Yes/No', name : 'achived', width :100, sortable : true, align: 'left'},*/
		{display: 'Order Taken Yes/No', name : 'order_id', width :100, sortable : true, align: 'left'},
		{display: 'Dealer Name', name : 'distributor_name', width :100, sortable : true, align: 'left'},
		{display: 'Dealer Code', name : 'distributor_code', width :100, sortable : true, align: 'left'},
		/*{display: 'Achived Yes/No', name : 'yes', width :100, sortable : true, align: 'left'},*/
		{display: 'Order Taken Yes/No', name : 'k', width :100, sortable : true, align: 'left'}
		//{display: 'Total Sale', name : 'unplannedVisitedAccount', width :100, sortable : true, align: 'left'}
		],
	buttons : [			
			/*{name: 'View Detail', bclass: 'view', onpress : eventHandlerPLVsVS},*/
			//{separator: true}
		],
	searchitems : [
		{display: 'Salesman', name : 'salesman_name', isdefault: true}		
		],
	sortname: " RSD.assign_day ,RS.month",
	sortorder: "desc",
	usepager: true,
	title: 'Salesman Route Plan',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 1000,
	height: 'auto'
});  
}



	/***************************************************************************
	* DESC : get serial number scan by customer report
	* Author : Abhishek
	* Created : 2016-02-24
	*
	**/

function serialNoScan(){
$("#flex1").flexigrid({
	url: 'serial_no_scan_report.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'ID', name : 'id', width : 50, sortable : true, align: 'left'},
		{display: 'Shakti-Partner Name', name : 'customer_name', width : 150, sortable : true, align: 'left'},
		{display: 'Shakti-Partner Code', name : 'customer_code', width : 100, sortable : true, align: 'left'},
		{display: 'Employee Name', name : 'employee_name', width : 150, sortable : true, align: 'left'},
		{display: 'Employee Code', name : 'employee_code', width : 100, sortable : true, align: 'left'},
		{display: 'Customer Name', name : 'customer_name', width : 150, sortable : true, align: 'left'},
		{display: 'Customer Mobile Number', name : 'customer_mobile_number', width : 100, sortable : true, align: 'left'},
		{display: 'Serial Number', name : 'serial_no', width : 100, sortable : true, align: 'left'},
		{display: 'Date', name : 'date', width : 70, sortable : true, align: 'left'},
		{display: 'Time', name : 'time', width : 70, sortable : true, align: 'left'},
		{display: 'Status', name : 'status', width : 100, sortable : true, align: 'left'}
		],
	buttons : [
	    {name: 'Add', bclass: 'add', onpress : eventHandlerSerial},
		{separator: true},
	    {name: 'Update', bclass: 'update', onpress : eventHandlerSerial},
		{separator: true},
		{name: 'Import', bclass: 'import', onpress : eventHandlerSerial},
		{separator: true},
		{name: 'Export', bclass: 'export', onpress : eventHandlerSerial},
		{separator: true}
		
		],
	searchitems : [
		{display: 'Shakti-Partner Name', name : 'customer_name', isdefault: true},
		{display: 'Serial Number', name : 'serial_number'},
		{display: 'Shakti-Partner Code', name : 'customer_code'},
		{display: 'Status', name : 'serial_number_status'}
		
		],
	sortname: "serial_id",
	sortorder: "asc",
	usepager: true,
	title: 'Serial Number Scan Report',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 700,
	height: 'auto'
});  
}

function eventHandlerSerial(com,grid)
{
	if (com=='Import')	{
		location.href="scan_serial_import.php";
	}
	else if (com=='Export')	{
		location.href="export.inc.php?export_serial_no_scan";
	}
	else if (com=='Add')	{
		location.href="serial_number_scan.php?add";	
	}
	else{
		if( $('.trSelected',grid).length<=0 ||  $('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=$('.trSelected',grid);	
		if (com=='Update'){
			location.href="serial_number_scan.php?id="+items[0].id.substr(3);
	}
}
}
	
/*function eventHandlerPLVsVS(com,grid){

	if( $('.trSelected',grid).length<=0 ||  $('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
	}
		var items=$('.trSelected',grid);		
		if (com=='View Detail'){
			window.open("plannedvisitedtown_detail.php?id="+items[0].id.substr(3), '_blank');		
		}
}*/





