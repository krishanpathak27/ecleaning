// JavaScript Document
function showCategory(){
j("#flex1").flexigrid({
	url: 'category.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Category Name', name : 'category_name', width : 300, sortable : true, align: 'left'},
		{display: 'Category Code', name : 'category_code', width : 200, sortable : true, align: 'left'},
		{display: 'Status', name : 'status', width : 100, sortable : true, align: 'left'}
		],
	buttons : [
		{name: 'Add', bclass: 'add', onpress : eventHandlerCategory},
		{separator: true},		
		{name: 'Edit', bclass: 'edit', onpress : eventHandlerCategory},
		{separator: true}		
		],
	searchitems : [
		{display: 'Category Name', name : 'category_name', isdefault: true},
		{display: 'Category Code', name : 'category_code'}
		
		],
	sortname: "category_name",
	sortorder: "asc",
	usepager: true,
	title: 'Category',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 700,
	height: 'auto'
});  
}
function eventHandlerCategory(com,grid)
{
	if (com=='Add')	{
		location.href="addCategory.php";		
	}else{
		if( j('.trSelected',grid).length<=0 ||  j('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=j('.trSelected',grid);		
		if (com=='Edit'){
			location.href="addCategory.php?id="+items[0].id.substr(3);			
		}
		if (com=='Delete'){
			if(confirm("Are you sure, you want to delete selected Category?")){
					
				j.post("category.php", {"delete":"yes","id":items[0].id.substr(3)},function(data)
				{		 
				  if(data!="") {					 
					 j('#flex1').flexOptions().flexReload(); 
				  }else {
					  alert("Record doesn't exists.")
				  }
				  
				});		
			}			
		}	
	}
}

function showRelationship(){
j("#flex1").flexigrid({
	url: 'relationship.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Customer Class', name : 'relationship_code', width : 200, sortable : true, align: 'left'},
		{display: 'Customer Class Description', name : 'relationship_desc', width : 200, sortable : true, align: 'left'},
		{display: 'Customer Type', name : 'relationship_type', width : 200, sortable : true, align: 'left'},
		{display: 'Status', name : 'status', width : 100, sortable : true, align: 'left'}
		],
	buttons : [
		{name: 'Add', bclass: 'add', onpress : eventHandlerRelationship},
		{separator: true},		
		{name: 'Edit', bclass: 'edit', onpress : eventHandlerRelationship},
		{separator: true}
		
		],
	searchitems : [
		{display: 'Customer Class', name : 'relationship_code', isdefault: true},
		{display: 'Customer Class Description', name : 'relationship_desc'},
		{display: 'Customer Type', name : 'relationship_type'}
		
		],
	sortname: "relationship_code",
	sortorder: "asc",
	usepager: true,
	title: 'Customer Class',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 700,
	height: 'auto'
});  
}
function eventHandlerRelationship(com,grid)
{
	if (com=='Add')	{
		location.href="addrelationship.php";		
	}else{
		if( j('.trSelected',grid).length<=0 ||  j('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=j('.trSelected',grid);		
		if (com=='Edit'){
			location.href="addrelationship.php?id="+items[0].id.substr(3);			
		}
		if (com=='Delete'){
			if(confirm("Are you sure, you want to delete selected Category?")){
					
				j.post("addrelationship.php", {"delete":"yes","id":items[0].id.substr(3)},function(data)
				{		 
				  if(data!="") {					 
					 j('#flex1').flexOptions().flexReload(); 
				  }else {
					  alert("Record doesn't exists.")
				  }
				  
				});		
			}			
		}	
	}
}

function showItemColor(){
j("#flex1").flexigrid({
	url: 'item_color.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Color Code', name : 'color_code', width : 250, sortable : true, align: 'left'},
		{display: 'Color Description', name : 'status', width : 250, sortable : true, align: 'left'},
		{display: 'Status', name : 'status', width : 100, sortable : true, align: 'left'}
		],
	buttons : [
		{name: 'Add', bclass: 'add', onpress : eventHandlerItemColor},
		{separator: true},		
		{name: 'Edit', bclass: 'edit', onpress : eventHandlerItemColor},
		{separator: true}
		
		],
	searchitems : [
		{display: 'Color Code', name : 'color_code', isdefault: true},
		{display: 'Color Description', name : 'category_code'}
		
		],
	sortname: "color_code",
	sortorder: "asc",
	usepager: true,
	title: 'Item Colors',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 700,
	height: 'auto'
});  
}
function eventHandlerItemColor(com,grid)
{
	if (com=='Add')	{
		location.href="add_color.php";		
	}else{
		if( j('.trSelected',grid).length<=0 ||  j('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=j('.trSelected',grid);		
		if (com=='Edit'){
			location.href="add_color.php?id="+items[0].id.substr(3);			
		}
		if (com=='Delete'){
			if(confirm("Are you sure, you want to delete selected Category?")){
					
				j.post("category.php", {"delete":"yes","id":items[0].id.substr(3)},function(data)
				{		 
				  if(data!="") {					 
					 j('#flex1').flexOptions().flexReload(); 
				  }else {
					  alert("Record doesn't exists.")
				  }
				  
				});		
			}			
		}	
	}
}


function showItem(){
j("#flex1").flexigrid({
	url: 'item.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Item Name', name : 'item_name', width : 150, sortable : true, align: 'left'},
		{display: 'Brand Name', name : 'brand_name', width : 80, sortable : true, align: 'left'},		
		{display: 'Category', name : 'category_name', width : 80, sortable : true, align: 'left'},		
		{display: 'Weight', name : 'item_size', width : 40, sortable : true, align: 'left'},
		{display: 'Item Code', name : 'item_code', width : 100, sortable : true, align: 'left'},
		{display: 'Product Code Old', name : 'item_erp_code', width : 100, sortable : true, align: 'left'},
		{display: 'Product Code New', name : 'item_erp_code', width : 100, sortable : true, align: 'left'},
		{display: 'Model Code', name : 'model_code', width : 100, sortable : true, align: 'left'},
		{display: 'Division', name : 'division_name', width : 100, sortable : true, align: 'left'},
		{display: 'Segment', name : 'segment_name', width : 100, sortable : true, align: 'left'},
		{display: 'Warranty', name : 'item_warranty', width : 100, sortable : true, align: 'left'},
		{display: 'Prodata', name : 'item_prodata', width : 100, sortable : true, align: 'left'},
		{display: 'Grace', name : 'item_grace', width : 100, sortable : true, align: 'left'},
		{display: 'Offer Type', name : 'offer_name', width : 100, sortable : true, align: 'left'},
		{display: 'MRP/DP Price Detail', name : 'price_detail', width : 100, sortable : true, align: 'left'},
		{display: 'PTR Price Detail', name : 'price_detail', width : 100, sortable : true, align: 'left'},
		{display: 'Status', name : 'status', width : 225, sortable : true, align: 'left'}
		],
	buttons : [
		{name: 'Add', bclass: 'add', onpress : eventHandlerItem},
		{separator: true},		
		{name: 'Edit', bclass: 'edit', onpress : eventHandlerItem},
		{separator: true},		
		{name: 'Import PTR Price Detail', bclass: 'import', onpress : eventHandlerItem},
		{separator: true}
		
		],
	searchitems : [
		{display: 'Item Name', name : 'item_name', isdefault: true},
		{display: 'Item Code', name : 'item_code'},
		{display: 'Old Product Code', name : 'item_erp_code_old'},
		{display: 'New Product Code', name : 'item_erp_code'},
		{display: 'Division', name : 'division_name'},
		{display: 'Segment', name : 'segment_name'},
		{display: 'Brand Name', name : 'brand_name'},
		{display: 'Category', name : 'category_name'}
		
		],
	sortname: "item_name",
	sortorder: "asc",
	usepager: true,
	title: 'Item',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 700,
	height: 'auto'
});  
}
function eventHandlerItem(com,grid)
{
	if (com=='Import PTR Price Detail')	{
		location.href="import_item_ptr.php";	
		//location.href="#";	
	}
	else if (com=='Export')	{
		location.href="export.inc.php?export_item";		
	}
	else if (com=='Add')	{
		location.href="addItem.php";		
	}else{
		if( j('.trSelected',grid).length<=0 ||  j('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=j('.trSelected',grid);		
		if (com=='Edit'){
			location.href="addItem.php?id="+items[0].id.substr(3);			
		}
		if (com=='Delete'){
			if(confirm("Are you sure, you want to delete selected item?")){
					
				j.post("item.php", {"delete":"yes","id":items[0].id.substr(3)},function(data)
				{		 
				  if(data!="") {					 
					 j('#flex1').flexOptions().flexReload(); 
				  }else {
					  alert("Record doesn't exists.")
				  }
				  
				});		
			}			
		}	
	}
}
function showDiscount(){
j("#flex1").flexigrid({
	url: 'discount.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Description', name : 'discount_desc', width : 150, sortable : true, align: 'left'},
		{display: 'Scheme Mode', name : 'mode', width : 100, sortable : true, align: 'left'},
		{display: 'Division', name : 'division_id', width : 100, sortable : true, align: 'left'},
		{display: 'Scheme', name : 'discount', width : 100, sortable : true, align: 'left'},
		{display: 'Primary/Secondary', name : 'mode', width : 100, sortable : true, align: 'left'},
		{display: 'Item Type', name : 'item_type', width : 100, sortable : true, align: 'left'},
		{display: 'Party Type', name : 'price_detail', width : 100, sortable : true, align: 'left'},
		{display: 'Scheme Type', name : 'discount_type', width : 100, sortable : true, align: 'left'},
		//{display: 'Minimum Amount', name : 'minimum_amount', width : 100, sortable : true, align: 'left'},
		//{display: 'Minimum Quantity', name : 'minimum_quantity', width : 100, sortable : true, align: 'left'},
		{display: 'Start Date', name : 'start_date', width : 100, sortable : true, align: 'left'},
		{display: 'End Date', name : 'end_date', width : 100, sortable : true, align: 'left'},
		{display: 'Status', name : 'status', width : 50, sortable : true, align: 'left'}
		],
	buttons : [
		{name: 'Add', bclass: 'add', onpress : eventHandlerDiscount},
		{separator: true},		
		{name: 'Edit', bclass: 'edit', onpress : eventHandlerDiscount},
		{separator: true},
		{name: 'Delete', bclass: 'delete', onpress : eventHandlerDiscount},
		{separator: true}
		/*,
		{name: 'Sms Schedule', bclass: 'sms', onpress : eventHandlerDiscount},
		{separator: true}*/
		
		],
	searchitems : [
		{display: 'Description', name : 'discount_desc', isdefault: true}
		
		],
	sortname: "discount_id",
	sortorder: "Desc",
	usepager: true,
	title: 'Scheme',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 1000,
	height: 'auto'
});  
}
function eventHandlerDiscount(com,grid)
{
	if (com=='Import')	{
		//location.href="import_item.php";	
		location.href="#";	
	}
	else if (com=='Export')	{
		location.href="export.inc.php?export_item";		
	}
	else if (com=='Add')	{
		location.href="adddiscount.php";		
	}else{
		if( j('.trSelected',grid).length<=0 ||  j('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=j('.trSelected',grid);	
		if (com=='Sms Schedule')	{
			var id = items[0].id.substr(3);
			//alert(id);
			var ids = btoa(id);
			//alert(atob(ids));
			window.open("sms_schedule.php?sid="+ids,'_newtab');
			//location.href="sms_schedule.php?sid="+ids;	
			} 
		if (com=='Edit'){
			//location.href="editdiscount.php?id="+items[0].id.substr(3);	
			location.href="editdiscount.php?id="+items[0].id.substr(3)+"&division_id="+items[0].sid;			
		}
		if (com=='Delete'){
			if(confirm("Are you sure, you want to delete selected Scheme?")){
					
				j.post("discount.php", {"delete":"yes","id":items[0].id.substr(3)},function(data)
				{		 
				  if(data!="") {					 
					 j('#flex1').flexOptions().flexReload(); 
				  }else {
					  alert("Record doesn't exists.")
				  }
				  
				});		
			}			
		}	
	}
}

function showItemPrice(){
j("#flex1").flexigrid({
	url: 'item_price.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Item Name', name : 'item_name', width : 150, sortable : true, align: 'left'},
		{display: 'Item Code', name : 'item_code', width : 100, sortable : true, align: 'left'},
		{display: 'Category', name : 'category_name', width : 100, sortable : true, align: 'left'},
		{display: 'MRP', name : 'item_mrp', width : 80, sortable : true, align: 'left'},
		{display: 'DP', name : 'item_dp', width : 80, sortable : true, align: 'left'},
		{display: 'Start Date', name : 'start_date', width : 80, sortable : true, align: 'left'},
		{display: 'End Date', name : 'end_date', width : 80, sortable : true, align: 'left'},
		{display: 'Status', name : 'status', width : 50, sortable : true, align: 'left'}
		],
	buttons : [
		{name: 'Add', bclass: 'additemprice', onpress : eventHandlerItemPrice},
		{separator: true},		
		{name: 'Edit', bclass: 'edititemprice', onpress : eventHandlerItemPrice},
		{separator: true},
		{name: 'Back', bclass: 'back', onpress : eventHandlerItemPrice},
		{separator: true}
		],
	searchitems : [
		{display: 'Name', name : 'item_name', isdefault: true}
		
		],
	sortname: "start_date",
	sortorder: "asc",
	usepager: true,
	title: 'Item Price Detail',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 700,
	height: 'auto'
});  
}
function eventHandlerItemPrice(com,grid)
{
	if (com=='Add')	{
		location.href="addPrice.php";		
	}else
	if (com=='Back')	{
		location.href="item.php";		
	}else{
		if( j('.trSelected',grid).length<=0 ||  j('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=j('.trSelected',grid);		
		if (com=='Edit'){
			location.href="addPrice.php?id="+items[0].id.substr(3);			
		}
		if (com=='Delete'){
			if(confirm("Are you sure, you want to delete selected Price?")){
					
				j.post("item_price.php", {"delete":"yes","id":items[0].id.substr(3)},function(data)
				{		 
				  if(data!="") {					 
					 j('#flex1').flexOptions().flexReload(); 
				  }else {
					  alert("Record doesn't exists.")
				  }
				  
				});		
			}			
		}	
	}
}
function showItemPTR(){
j("#flex1").flexigrid({
	url: 'item_ptr.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Item Name', name : 'item_name', width : 150, sortable : true, align: 'left'},
		{display: 'Item Code', name : 'item_code', width : 80, sortable : true, align: 'left'},
		{display: 'Category', name : 'category_name', width : 80, sortable : true, align: 'left'},
		{display: 'PTR Price', name : 'item_mrp', width : 80, sortable : true, align: 'left'},
		{display: 'State', name : 'item_dp', width : 80, sortable : true, align: 'left'},
		{display: 'City', name : 'item_dp', width : 80, sortable : true, align: 'left'},
		{display: 'Start Date', name : 'start_date', width : 80, sortable : true, align: 'left'},
		{display: 'End Date', name : 'end_date', width : 80, sortable : true, align: 'left'},
		{display: 'Status', name : 'status', width : 50, sortable : true, align: 'left'}
		],
	buttons : [
		{name: 'Add', bclass: 'additemptr', onpress : eventHandlerItemPTR},
		{separator: true},		
		{name: 'Edit', bclass: 'edititemptr', onpress : eventHandlerItemPTR},
		{separator: true},
		{name: 'Back', bclass: 'backitemptr', onpress : eventHandlerItemPTR},
		{separator: true}
		],
	searchitems : [
		{display: 'Name', name : 'item_name', isdefault: true}
		
		],
	sortname: "start_date",
	sortorder: "asc",
	usepager: true,
	title: 'Item PTR Price Detail',
	useRp: true,
	rp: 15,
	showTableToggleBtn: true,
	width: 700,
	height: 375
});  
}
function eventHandlerItemPTR(com,grid)
{
	if (com=='Add')	{
		location.href="add_ptr.php";		
	}else
	if (com=='Back')	{
		location.href="item.php";		
	}else{
		if( j('.trSelected',grid).length<=0 ||  j('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=j('.trSelected',grid);		
		if (com=='Edit'){
			location.href="add_ptr.php?id="+items[0].id.substr(3);			
		}
		if (com=='Delete'){
			if(confirm("Are you sure, you want to delete selected Price?")){
					
				j.post("item_price.php", {"delete":"yes","id":items[0].id.substr(3)},function(data)
				{		 
				  if(data!="") {					 
					 j('#flex1').flexOptions().flexReload(); 
				  }else {
					  alert("Record doesn't exists.")
				  }
				  
				});		
			}			
		}	
	}
}

function showDistributors(){
j("#flex1").flexigrid({
	url: 'distributors.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Distributor Name', name : 'distributor_name', width : 250, sortable : true, align: 'left'},
		{display: 'Distributor Code', name : 'distributor_code', width : 100, sortable : true, align: 'left'},
		{display: 'Salesman', name : 'salesman_name', width : 250, sortable : true, align: 'left'},
		{display: 'Division', name : 'division_name', width : 80, sortable : true, align: 'left'},
		{display: 'Brand', name : 'brand_name', width : 80, sortable : true, align: 'left'},
		//{display: 'Distributor Class', name : 'relationship_code', width : 80, sortable : true, align: 'left'},
		// {display: 'Added by(Salesman)', name : 'salesman_name', width : 80, sortable : true, align: 'left'},
		// {display: 'Salesman Code', name : 'salesman_code', width : 80, sortable : true, align: 'left'},
		{display: 'Country', name : 'country_name', width : 80, sortable : true, align: 'left'},
		{display: 'Region', name : 'region_name', width : 80, sortable : true, align: 'left'},
		{display: 'Zone', name : 'zone_name', width : 80, sortable : true, align: 'left'},
		{display: 'State', name : 'state_name', width : 80, sortable : true, align: 'left'},
		{display: 'District', name : 'city_name', width : 80, sortable : true, align: 'left'},
		//{display: 'Taluka', name : 'taluka_name', width : 80, sortable : true, align: 'left'},
		{display: 'Area', name : 'distributor_location', width : 100, sortable : true, align: 'left'},
		{display: 'Pincode', name : 'pin_code', width : 100, sortable : true, align: 'left'},
		{display: 'Address 1', name : 'distributor_address', width : 100, sortable : true, align: 'left'},
		{display: 'Address 2', name : 'distributor_address2', width : 100, sortable : true, align: 'left'},
		{display: 'Send SMS Mobile No', name : 'sms_number', width : 100, sortable : true, align: 'left'},
		{display: 'Phone Number 1', name : 'distributor_phone_no', width : 80, sortable : true, align: 'left'},
		{display: 'Phone Number 2', name : 'distributor_phone_no2', width : 80, sortable : true, align: 'left'},
		{display: 'Phone Number 3', name : 'distributor_phone_no3', width : 80, sortable : true, align: 'left'},
		{display: 'Landline Number', name : 'distributor_leadline_no', width : 80, sortable : true, align: 'left'},
		{display: 'Contact Person 1', name : 'contact_person', width : 80, sortable : true, align: 'left'},
		{display: 'Contact Number 1', name : 'contact_number', width : 80, sortable : true, align: 'left'},
		{display: 'Contact Person 2', name : 'contact_person2', width : 80, sortable : true, align: 'left'},
		{display: 'Contact Number 2', name : 'contact_number2', width : 80, sortable : true, align: 'left'},
		{display: 'Contact Person 3', name : 'contact_person3', width : 80, sortable : true, align: 'left'},
		{display: 'Contact Number 3', name : 'contact_number3', width : 80, sortable : true, align: 'left'},
		{display: 'Username', name : 'username', width : 80, sortable : true, align: 'left'},
		{display: 'Email ID 1', name : 'distributor_email', width : 100, sortable : true, align: 'left'},
		{display: 'Email ID 2', name : 'distributor_email2', width : 100, sortable : true, align: 'left'},
		{display: 'Email ID 3', name : 'distributor_email3', width : 100, sortable : true, align: 'left'},
		{display: 'Date of Birth', name : 'distributor_dob', width : 80, sortable : true, align: 'left'},
		{display: 'Aadhar Number', name : 'aadhar_no', width : 80, sortable : true, align: 'left'},
		{display: 'Pan Number', name : 'pan_no', width : 80, sortable : true, align: 'left'},
		{display: 'GST Number', name : 'tin_number', width : 80, sortable : true, align: 'left'},
		{display: 'Credit Limit', name : 'credit_limit', width : 80, sortable : true, align: 'left'},
		{display: 'Start Date', name : 'start_date', width : 80, sortable : true, align: 'left'},
		{display: 'End Date', name : 'end_date', width : 80, sortable : true, align: 'left'},
		{display: 'Distributor Status', name : 'status', width : 80, sortable : true, align: 'center'},
		{display: 'Login Status', name : 'loginStatus', width : 80, sortable : true, align: 'center'},
		{display: 'App Version', name : 'appStatus', width : 80, sortable : true, align: 'center'}
			
		],
	buttons : [
		{name: 'Add', bclass: 'add', onpress : eventHandlerDisAdminUser},
		{separator: true},		
		{name: 'Edit', bclass: 'edit', onpress : eventHandlerDisAdminUser},
		{separator: true},
		{name: 'Delete', bclass: 'delete', onpress : eventHandlerDisAdminUser},
		{separator: true}
		],
	searchitems : [
		{display: 'Name', name : 'distributor_name', isdefault: true},
		{display: 'Division', name : 'dV.division_name'},
		{display: 'Brand', name : 'brand_name'},
		{display: 'Username', name : 'w.username'},
		//{display: 'Division', name : 'division'},
		{display: 'Email ID', name : 'distributor_email'},
		{display: 'Send SMS Mobile No', name : 'sms_number'},
		{display: 'Phone Number', name : 'distributor_phone_no'},
		{display: 'Country', name : 'country_name'},
		{display: 'Region', name : 'region_name'},
		{display: 'Zone', name : 'zone_name'},
		{display: 'State', name : 'state_name'},
		{display: 'District', name : 'city_name'},
	//	{display: 'Taluka', name : 'taluka_name'},
	//	{display: 'City', name : 'market_name'},
		{display: 'Address', name : 'distributor_address'},
	//	{display: 'City', name : 'distributor_location'},
		{display: 'Contact Person', name : 'contact_person'},
		{display: 'Contact Number', name : 'contact_number'}
		],
	sortname: "distributor_name",
	sortorder: "asc",
	usepager: true,
	title: 'Distributors',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 700,
	height: 'auto'
});  
}
/*function eventHandlerDisAdminUser(com,grid)
{
	if (com=='Add')	{
		location.href="distributors.php?add";	
	}else if (com=='Import')	{
		location.href="distributors.php?import";	
	}else if (com=='Export')	{
		location.href="export.inc.php?export_distributors_list";	
	}
	else{
		if( j('.trSelected',grid).length<=0 ||  j('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=j('.trSelected',grid);		
		if (com=='Edit'){
			location.href="distributors.php?id="+items[0].id.substr(3);			
		}else if (com=='Create Login')	{
		location.href="#.php?login&id="+items[0].id.substr(3);
	}
			
	}
}*/


function showWarehouses(){
j("#flex1").flexigrid({
	url: 'warehouses.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Warehouse Name', name : 'warehouse_name', width : 80, sortable : true, align: 'left'},
		{display: 'Warehouse Code', name : 'warehouse_code', width : 80, sortable : true, align: 'left'},
		/*{display: 'Division', name : 'division_name', width : 80, sortable : true, align: 'left'},*/

		{display: 'Warehouse Incharge Email ID 1', name : 'warehouse_incharge_person_email', width : 80, sortable : true, align: 'left'}, //added by anurag@23-02-2017

		{display: 'Email ID 2', name : 'warehouse_incharge_person_email2', width : 80, sortable : true, align: 'left'}, ////added by anurag@23-02-2017

		{display: 'Email ID 3', name : 'warehouse_incharge_person_email3', width : 80, sortable : true, align: 'left'}, //added by anurag@23-02-2017

		{display: 'Warehouse Incharge Person', name : 'warehouse_incharge_person', width : 80, sortable : true, align: 'left'},
		{display: 'Warehouse Incharge Phone No 1', name : 'warehouse_incharge_person_phone_no', width : 100, sortable : true, align: 'left'}, //added by anurag@23-02-2017

		{display: 'Phone No 2', name : 'warehouse_incharge_person_phone_no2', width : 100, sortable : true, align: 'left'}, //added by anurag@23-02-2017

		{display: 'Phone No 3', name : 'warehouse_incharge_person_phone_no3', width : 100, sortable : true, align: 'left'}, //added by anurag@23-02-2017

		{display: 'Country', name : 'country_name', width : 80, sortable : true, align: 'left'},
		{display: 'Region', name : 'region_name', width : 80, sortable : true, align: 'left'},
		{display: 'Zone', name : 'zone_name', width : 80, sortable : true, align: 'left'},
		{display: 'State', name : 'state_name', width : 80, sortable : true, align: 'left'},
		{display: 'City', name : 'city_name', width : 80, sortable : true, align: 'left'},
		{display: 'Address', name : 'warehouse_address', width : 100, sortable : true, align: 'left'},
		{display: 'Phone Number', name : 'warehouse_phone_no', width : 80, sortable : true, align: 'left'},
		{display: 'Pincode/Zipcode', name : 'zipcode', width : 100, sortable : true, align: 'left'},
		{display: 'Username', name : 'username', width : 80, sortable : true, align: 'left'},
		{display: 'Email ID', name : 'email_id', width : 100, sortable : true, align: 'left'},
		{display: 'Start Date', name : 'start_date', width : 80, sortable : true, align: 'left'},
		{display: 'End Date', name : 'end_date', width : 80, sortable : true, align: 'left'},
		{display: 'Status', name : 'status', width : 80, sortable : true, align: 'center'},
		{display: 'Login Status', name : 'loginStatus', width : 80, sortable : true, align: 'center'},
		{display: 'App Version', name : 'appStatus', width : 80, sortable : true, align: 'center'}
			
		],
	buttons : [
		{name: 'Add', bclass: 'add', onpress : eventHandlerWarAdminUser},
		{separator: true},		
		{name: 'Edit', bclass: 'edit', onpress : eventHandlerWarAdminUser},
		{separator: true},
		// {name: 'Delete', bclass: 'delete', onpress : eventHandlerWarAdminUser},
		{separator: true}
		],
	searchitems : [
		{display: 'Name', name : 'warehouse_name', isdefault: true},
		/*{display: 'Division', name : 'division_name'},*/
		{display: 'Username', name : 'username'},
		{display: 'Email ID', name : 'email_id'},
		{display: 'Phone Number', name : 'warehouse_phone_no'},
		{display: 'Country', name : 'country_name'},
		{display: 'Region', name : 'region_name'},
		{display: 'Zone', name : 'zone_name'},
		{display: 'State', name : 'state_name'},
		{display: 'City', name : 'city_name'},
		{display: 'Address', name : 'warehouse_address'}
		],
	sortname: "warehouse_name",
	sortorder: "asc",
	usepager: true,
	title: 'Warehouses',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 700,
	height: 'auto'
});  
}
function eventHandlerWarAdminUser(com,grid)
{
	if (com=='Add')	{
		location.href="warehouses.php?add";	
	}else if (com=='Import')	{
		location.href="warehouses.php?import";	
	}else if (com=='Export')	{
		location.href="export.inc.php?export_warehouses_list";	
	}
	else{
		if( j('.trSelected',grid).length<=0 ||  j('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=j('.trSelected',grid);		
		if (com=='Edit'){
			location.href="warehouses.php?id="+items[0].id.substr(3);			
		}else if (com=='Create Login')	{
		location.href="#.php?login&id="+items[0].id.substr(3);
	}
		else if (com=='Delete')	{
		if(confirm("Are you sure, you want to delete selected Warehouse?")){
					
				j.post("warehouses.php", {"delete":"yes","id":items[0].id.substr(3)},function(data)
				{
					j("#message-green").hide();
				  if(data!="") {
				  	//alert(data);
				  	if(data >0 )
				  	{				  		
				  		alert("This warehouse cannot be deleted.");
				  	}
				  				 
					 j('#flex1').flexOptions().flexReload(); 
				  }else {
					  alert("Record doesn't exists.")
				  }
				  
				});		
			}
	}
			
	}
}


function showServicePersons(){
j("#flex1").flexigrid({
	url: 'serviceperson.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Service Person Name', name : 'sp_name', width : 102, sortable : true, align: 'left'},
		{display: 'User Level', name : 'des1', width : 60, sortable : true, align: 'left'},
		{display: 'Reporting to', name : 'des2', width : 70, sortable : true, align: 'left'},
		{display: 'Reporting Person', name : 'rptPerson', width : 80, sortable : true, align: 'left'},
		{display: 'Service Person Code', name : 'sp_code', width : 100, sortable : true, align: 'left'},
		{display: 'Division', name : 'division_name', width : 80, sortable : true, align: 'left'},
		{display: 'Country', name : 'country_name', width : 80, sortable : true, align: 'left'},
		{display: 'Region', name : 'region_name', width : 80, sortable : true, align: 'left'},
		{display: 'Zone', name : 'zone_name', width : 80, sortable : true, align: 'left'},
		{display: 'State', name : 'state_name', width : 80, sortable : true, align: 'left'},
		{display: 'City', name : 'city_name', width : 80, sortable : true, align: 'left'},
		{display: 'Address', name : 'sp_address', width : 100, sortable : true, align: 'left'},
		{display: 'Phone Number', name : 'sp_phone_no', width : 80, sortable : true, align: 'left'},
		{display: 'Pincode/Zipcode', name : 'zipcode', width : 100, sortable : true, align: 'left'},
		{display: 'Username', name : 'username', width : 80, sortable : true, align: 'left'},
		{display: 'Email ID', name : 'email_id', width : 100, sortable : true, align: 'left'},
		{display: 'Start Date', name : 'start_date', width : 80, sortable : true, align: 'left'},
		{display: 'End Date', name : 'end_date', width : 80, sortable : true, align: 'left'},
		{display: 'Status', name : 'status', width : 80, sortable : true, align: 'center'},
		{display: 'Login Status', name : 'loginStatus', width : 80, sortable : true, align: 'center'},
		{display: 'App Version', name : 'appStatus', width : 80, sortable : true, align: 'center'}
			
		],
	buttons : [
		{name: 'Add', bclass: 'add', onpress : eventHandlerServicePersonnelUser},
		{separator: true},		
		{name: 'Edit', bclass: 'edit', onpress : eventHandlerServicePersonnelUser},
		{separator: true},
		// {name: 'Delete', bclass: 'delete', onpress : eventHandlerServicePersonnelUser},
		//{separator: true}
		],
	searchitems : [
		{display: 'Name', name : 'sp.sp_name', isdefault: true},
		{display: 'Division', name : 'division_name'},
		{display: 'Username', name : 'username'},
		{display: 'Email ID', name : 'email_id'},
		{display: 'Phone Number', name : 'sp_phone_no'},
		{display: 'Country', name : 'country_name'},
		{display: 'Region', name : 'region_name'},
		{display: 'Zone', name : 'zone_name'},
		{display: 'State', name : 'state_name'},
		{display: 'City', name : 'city_name'},
		{display: 'Address', name : 'sp_address'}
		],
	sortname: "sp.sp_name",
	sortorder: "asc",
	usepager: true,
	title: 'Service Personnel',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 700,
	height: 'auto'
});  
}
function eventHandlerServicePersonnelUser(com,grid)
{
	if (com=='Add')	{
		location.href="serviceperson.php?add";	
	}else if (com=='Import')	{
		location.href="serviceperson.php?import";	
	}else if (com=='Export')	{
		location.href="export.inc.php?export_service_person_list";	
	}
	else{
		if( j('.trSelected',grid).length<=0 ||  j('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=j('.trSelected',grid);		
		if (com=='Edit'){
			location.href="serviceperson.php?id="+items[0].id.substr(3);			
		}else if (com=='Create Login')	{
		location.href="#.php?login&id="+items[0].id.substr(3);
	}
		else if (com=='Delete')	{
		if(confirm("Are you sure, you want to delete selected Service Person?")){
					
				j.post("serviceperson.php", {"delete":"yes","id":items[0].id.substr(3)},function(data)
				{
					j("#message-green").hide();
				  if(data!="") {
				  	//alert(data);
				  	if(data >0 )
				  	{				  		
				  		alert("This Service Personnel cannot be deleted.");
				  	}
				  				 
					 j('#flex1').flexOptions().flexReload(); 
				  }else {
					  alert("Record doesn't exists.")
				  }
				  
				});		
			}
	}
			
	}
}


function showRetailer(){
j("#flex1").flexigrid({
	url: 'retailer.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Dealer Name', name : 'retailer_name', width : 80, sortable : true, align: 'left'},
		{display: 'Dealer Code', name : 'retailer_code', width : 80, sortable : true, align: 'left'},
		{display: 'Distributor', name : 'distributor_name', width : 220, sortable : true, align: 'left'},
		{display: 'Salesman', name : 'salesman_name', width : 250, sortable : true, align: 'left'},
		// {display: 'Salesman Code', name : 'salesman_code', width : 80, sortable : true, align: 'left'},
	//	{display: 'Retailer Class', name : 'relationship_code', width : 80, sortable : true, align: 'left'},
		{display: 'Division', name : 'division_name', width : 80, sortable : true, align: 'left'},
		{display: 'Brand', name : 'brand_name', width : 80, sortable : true, align: 'left'},
	//	{display: 'Mobile No', name : 'retailer_code', width : 80, sortable : true, align: 'left'},
	//	{display: 'Retailer Type', name : 'type_name', width : 80, sortable : true, align: 'left'},
	//	{display: 'Retailer Channel', name : 'channel_name', width : 80, sortable : true, align: 'left'},
		// {display: 'Added by(Salesman)', name : 'salesman_name', width : 80, sortable : true, align: 'left'},
		// {display: 'Salesman Code', name : 'salesman_code', width : 80, sortable : true, align: 'left'},
	//	{display: 'Distributor', name : 'distributor_id', width : 80, sortable : true, align: 'left'},
	//	{display: 'Interested', name : 'display_outlet', width : 80, sortable : true, align: 'left'},
		{display: 'App Version', name : 'app_version', width : 80, sortable : true, align: 'left'},
		{display: 'Country', name : 'country_name', width : 80, sortable : true, align: 'left'},
		{display: 'Region', name : 'region_name', width : 80, sortable : true, align: 'left'},
		{display: 'Zone', name : 'zone_name', width : 80, sortable : true, align: 'left'},
		{display: 'State', name : 'state_name', width : 80, sortable : true, align: 'left'},
		{display: 'District', name : 'city_name', width : 80, sortable : true, align: 'left'},
		{display: 'Area', name : 'market_name', width : 100, sortable : true, align: 'left'},
		{display: 'Pincode', name : 'pin_code', width : 100, sortable : true, align: 'left'},
		{display: 'Map', name : 'retailer_location', width : 100, sortable : true, align: 'left'},
	//	{display: 'Photo', name : '', width : 100, sortable : true, align: 'left'},
		{display: 'Address 1', name : 'retailer_address', width : 100, sortable : true, align: 'left'},
		{display: 'Address 2', name : 'retailer_address2', width : 100, sortable : true, align: 'left'},
		{display: 'Phone Number 1', name : 'retailer_phone_no', width : 80, sortable : true, align: 'left'},
		{display: 'Phone Number 2', name : 'retailer_phone_no2', width : 80, sortable : true, align: 'left'},
	//	{display: 'Leadline Number', name : 'retailer_leadline_no', width : 80, sortable : true, align: 'left'},
		{display: 'Aadhar Number', name : 'aadhar_no', width : 80, sortable : true, align: 'left'},
		{display: 'Pan Number', name : 'pan_no', width : 80, sortable : true, align: 'left'},
		{display: 'Party Strength', name : 'party_strength', width : 80, sortable : true, align: 'left'},
		{display: 'Contact Person 1', name : 'contact_person', width : 80, sortable : true, align: 'left'},
		{display: 'Contact Number 1', name : 'contact_number', width : 80, sortable : true, align: 'left'},
		{display: 'Contact Person 2', name : 'contact_person2', width : 80, sortable : true, align: 'left'},
		{display: 'Contact Number 2', name : 'contact_number2', width : 80, sortable : true, align: 'left'},
		{display: 'Username', name : 'username', width : 80, sortable : true, align: 'left'},
		{display: 'Email ID 1', name : 'retailer_email', width : 100, sortable : true, align: 'left'},
		{display: 'Email ID 2', name : 'retailer_email2', width : 100, sortable : true, align: 'left'},
		{display: 'Date of Birth', name : 'retailer_dob', width : 80, sortable : true, align: 'left'},
		{display: 'Start Date', name : 'start_date', width : 80, sortable : true, align: 'left'},
		{display: 'End Date', name : 'end_date', width : 80, sortable : true, align: 'left'},
		{display: 'Dealer Status', name : 'status', width : 80, sortable : true, align: 'center'},
		{display: 'Login Status', name : 'loginStatus', width : 80, sortable : true, align: 'center'},
		{display: 'Validation Status', name : 'validation_status', width : 80, sortable : true, align: 'center'},
		{display: 'Not Validation Reason', name : 'validation_reason', width : 80, sortable : true, align: 'center'},
		{display: 'Remark', name : 'remark', width : 80, sortable : true, align: 'left'}
			
		],
	buttons : [
		{name: 'Add', bclass: 'add', onpress : eventHandlerRetAdminUser},
		{separator: true},		
		{name: 'Edit', bclass: 'edit', onpress : eventHandlerRetAdminUser},
		{separator: true},
		{name: 'Delete', bclass: 'delete', onpress : eventHandlerRetAdminUser},
		{separator: true}
		// {name: 'Export Number of Photos', bclass: 'showpic', onpress : eventHandlerRetAdminUser},
		// {separator: true}	
		],
	searchitems : [
		{display: 'Dealer', name : 'retailer_name', isdefault: true},
		{display: 'Dealer Code', name : 'retailer_code'},
	    {display: 'Distributor', name : 'd.distributor_name'},
	    {display: 'Salesman', name : 's.salesman_name'},
		{display: 'Division', name : 'dV.division_name'},
	    {display: 'Brand', name : 'brand_name'},
	    {display: 'Country', name : 'country_name'},
		{display: 'Region', name : 'region_name'},
		{display: 'Zone', name : 'zone_name'},
		{display: 'State', name : 'state_name'},
		{display: 'District', name : 'city_name'},
		{display: 'Username', name : 'w.username'},
		{display: 'Email ID', name : 'w.email_id'},
		{display: 'Phone Number', name : 'retailer_phone_no'},
		{display: 'Contact Person', name : 'r.contact_person'},
		{display: 'Contact Number', name : 'r.contact_number'}
		],
	sortname: "retailer_name",
	sortorder: "asc",
	usepager: true,
	title: 'Dealer',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 700,
	height: 'auto'
});  
}





/*commeted by arvind 13-07-2017*/



function showSalesmanProductivityReport(){
j("#flex1").flexigrid({
	url: 'productivity_report.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel: [
	   
	   	{display: 'Salesman Code', name : 'salesman_code', width : 100, sortable : true, align: 'left'},
		{display: 'Salesman Name', name : 'salesman_name', width : 80, sortable : true, align: 'left'},
	   	{display: 'Reporting To', name : 'designation', width : 80, sortable : true, align: 'left'},
	   	{display: 'Brand', name : 'year', width : 90, sortable : true, align: 'left'},
		
		
		{display: 'Dealer Name', name : 'retailer_name', width :100, sortable : true, align: 'left'},
		{display: 'Dealer Contact', name : 'retailer_code', width :100, sortable : true, align: 'left'},
		{display: 'Dealer Address', name : 'achived', width :100, sortable : true, align: 'left'},
        {display: 'City', name : 'city', width :100, sortable : true, align: 'left'},
		{display: 'State', name : 'state', width :90, sortable : true, align: 'left'},
	      /*  {display: 'District', name : 'District', width :150, sortable : true, align: 'left'},
		{display: 'Order Taken Yes/No', name : 'order_id', width :100, sortable : true, align: 'left'},*/
		{display: 'Distributor Name', name : 'distributor_name', width :100, sortable : true, align: 'left'},
		/*{display: 'Planned Visit', name : 'planned_visit', width :100, sortable : true, align: 'left'},*/
        {display: 'Actual Visit', name : 'actual_visit', width :100, sortable : true, align: 'left'},
		{display: 'Segment', name : 'segment', width :100, sortable : true, align: 'left'},
		{display: 'Total Order Qty', name : 'Order_qty', width :80, sortable : true, align: 'left'},
		{display: 'Total Invoice Amount', name : 'collectionAccount', width :80, sortable : true, align: 'left'},
	//	{display: 'Collection Amount', name : 'collectionAccount', width :80, sortable : true, align: 'left'},
        {display: 'Outstanding Amount', name : 'outstandingAccount', width :80, sortable : true, align: 'left'}
     //   {display: 'Order Type', name : 'order_type', width :80, sortable : true, align: 'left'},
		],
	buttons : [			
			/*{name: 'View Detail', bclass: 'view', onpress : eventHandlerPLVsVS},*/
			//{separator: true}
		],
	searchitems : [
		{display: 'Salesman', name : 'salesman_name', isdefault: true}		
		],
	sortname: "o.order_id",
	sortorder: "desc",
	usepager: true,
	title: 'Productivity Report',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 1100,
	height: 'auto'
});  
}
























function eventHandlerRetAdminUser(com,grid)
{
	var searchParam_1 = j('input[name=q]').val();
	var searchParam_2 = j('select[name=qtype]').val();
	var searchParam_3 = j(".pcontrol").find("input").val();
	if (com=='Add')	{
		location.href="retailer.php?add=''"+"&searchParam_1="+searchParam_1+"&searchParam_2="+searchParam_2+"&searchParam_3="+searchParam_3;	
	}else if (com=='Export Number of Photos')	{
		location.href="export.inc.php?export_relailer_photo";	
	}else if (com=='Export')	{
		location.href="export.inc.php?export_relailer_list";	
	}
	else{
		if( j('.trSelected',grid).length<=0 ||  j('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=j('.trSelected',grid);		
		if (com=='Edit'){
			location.href="retailer.php?id="+items[0].id.substr(3)+"&searchParam_1="+searchParam_1+"&searchParam_2="+searchParam_2+"&searchParam_3="+searchParam_3;			
		}else if (com=='Create Login')	{
		location.href="#.php?login&id="+items[0].id.substr(3);
	}
	else if (com=='Delete')	{
		if(confirm("Are you sure, you want to delete selected Dealer?")){
					
				j.post("retailer.php", {"delete":"yes","id":items[0].id.substr(3)},function(data)
				{
					j("#message-green").hide();
				  if(data!="") {					 
					 j('#flex1').flexOptions().flexReload(); 
				  }else {
					  alert("Record doesn't exists.")
				  }
				  
				});		
			}
	}
			
	}
}
function showNewRetailer(){
j("#flex1").flexigrid({
	url: 'new_retailer.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Dealer Name', name : 'retailer_name', width : 80, sortable : true, align: 'left'},
		{display: 'Division', name : 'division_name', width : 80, sortable : true, align: 'left'},
		// {display: 'Added by(Salesman)', name : 'salesman_name', width : 80, sortable : true, align: 'left'},
		// {display: 'Salesman Code', name : 'salesman_code', width : 80, sortable : true, align: 'left'},
		{display: 'Added Date', name : 'start_date', width : 80, sortable : true, align: 'left'},
		{display: 'Order', name : 'order', width : 80, sortable : true, align: 'left'},
		{display: 'State', name : 'state_name', width : 80, sortable : true, align: 'left'},
		{display: 'City', name : 'city_name', width : 80, sortable : true, align: 'left'},
		// {display: 'Taluka', name : 'taluka_name', width : 80, sortable : true, align: 'left'},
		{display: 'Market', name : 'market_name', width : 100, sortable : true, align: 'left'},

		{display: 'Aadhar Number', name : 'aadhar_no', width : 100, sortable : true, align: 'left'},
		{display: 'Pan Number', name : 'pan_no', width : 100, sortable : true, align: 'left'},
		
		{display: 'Map', name : 'retailer_location', width : 100, sortable : true, align: 'left'},
		{display: 'Photo', name : '', width : 100, sortable : true, align: 'left'},
		{display: 'Address', name : 'retailer_address', width : 100, sortable : true, align: 'left'},
		{display: 'Phone Number', name : 'retailer_phone_no', width : 80, sortable : true, align: 'left'},
		{display: 'Contact Person', name : 'contact_person', width : 80, sortable : true, align: 'left'},
		{display: 'Contact Number', name : 'contact_number', width : 80, sortable : true, align: 'left'},
		{display: 'Date of Birth', name : 'retailer_dob', width : 80, sortable : true, align: 'left'},
		{display: 'Country', name : 'country_name', width : 80, sortable : true, align: 'left'}, // added by anurag@13-02-2017
		{display: 'Region', name : 'region_name', width : 80, sortable : true, align: 'left'}, // added by anurag@13-02-2017
		{display: 'Zone', name : 'zone_name', width : 80, sortable : true, align: 'left'}, //added by anurag@13-02-2017
		{display: 'Dealer Status', name : 'status', width : 80, sortable : true, align: 'center'}
			
		],
	buttons : [	
		{name: 'Edit', bclass: 'edit', onpress : eventHandlerRetNewAdminUser},
		{separator: true},
		{name: 'Delete', bclass: 'delete', onpress : eventHandlerRetNewAdminUser},
		{separator: true}
		],
	searchitems : [
		{display: 'Dealer Name', name : 'retailer_name', isdefault: true},
		{display: 'Division', name : 'division_name'},
		{display: 'Added By', name : 'salesman_name'},
		{display: 'Added Date', name : 'r.start_date'},
		{display: 'Address', name : 'retailer_address'},
		{display: 'City', name : 'city_name'},
		{display: 'Market', name : 'market_name'},
		{display: 'Taluka', name : 'taluka_name'},
		{display: 'State', name : 'state_name'},
		/*{display: 'Username', name : 'username'},
		{display: 'Email ID', name : 'email_id'},*/
		{display: 'Phone Number', name : 'retailer_phone_no'},
		{display: 'Contact Person', name : 'contact_person'},
		{display: 'Contact Number', name : 'contact_number'}
		],
	sortname: "start_date",
	sortorder: "desc",
	usepager: true,
	title: 'New Dealer',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 950,
	height: 'auto'
});  
}
function eventHandlerRetNewAdminUser(com,grid)
{
	var searchParam_1 = j('input[name=q]').val();
	var searchParam_2 = j('select[name=qtype]').val();
	var searchParam_3 = j(".pcontrol").find("input").val();
	if (com=='Add')	{
		location.href="retailer.php?add";	
	}else if (com=='Import')	{
		location.href="retailer.php?import";	
	}else if (com=='Export')	{
		location.href="export.inc.php?export_relailer_list";	
	}
	else{
		if( j('.trSelected',grid).length<=0 ||  j('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=j('.trSelected',grid);		
		if (com=='Edit'){
			location.href="retailer.php?ap=1&id="+items[0].id.substr(3)+"&searchParam_1="+searchParam_1+"&searchParam_2="+searchParam_2+"&searchParam_3="+searchParam_3;		
		}else if (com=='Delete')	{
		if(confirm("Are you sure, you want to delete selected Retailer?")){
					
				j.post("new_retailer.php", {"delete":"yes","id":items[0].id.substr(3)},function(data)
				{		 
				  if(data!="") {					 
					 j('#flex1').flexOptions().flexReload(); 
				  }else {
					  alert("Record doesn't exists.")
				  }
				  
				});		
			}
	}
			
	}
}


function showSalsman(checkhierarchyenable){
	
//alert(checkhierarchyenable);

if(checkhierarchyenable == true){
var colModel01 = [ 		
		{display: 'Name', name : 'salesman_name', width : 120, sortable : true, align: 'left'},
		{display: 'User Level', name : 'des1', width : 50, sortable : true, align: 'left'},
		{display: 'Reporting to', name : 'des2', width : 80, sortable : true, align: 'left'},
		{display: 'Reporting Person', name : 'username', width : 120, sortable : true, align: 'left'},
		{display: 'Username', name : 'username', width : 120, sortable : true, align: 'left'},
		{display: 'Salesman Code', name : 'salesman_code', width : 120, sortable : true, align: 'left'},
		{display: 'Division', name : 'division_id', width : 120, sortable : true, align: 'left'},
		{display: 'Email ID', name : 'email_id', width : 120, sortable : true, align: 'left'},
		{display: 'Phone Number', name : 'salesman_phome_no', width : 120, sortable : true, align: 'left'},
		{display: 'Address', name : 'salesman_address', width : 120, sortable : true, align: 'left'},
		{display: 'Total No. Of Distributors', name : 'salesman_phome_no', width : 120, sortable : true, align: 'left'},
		{display: 'Total No. Of Dealers', name : 'salesman_phome_no', width : 120, sortable : true, align: 'left'},
		{display: 'Start Date', name : 'start_date', width : 80, sortable : true, align: 'left'},
		{display: 'End Date', name : 'end_date', width : 80, sortable : true, align: 'left'},
		{display: 'Salesman Status', name : 'status', width : 80, sortable : true, align: 'center'},
		{display: 'Login Status', name : 'loginStatus', width : 80, sortable : true, align: 'center'}
			
		];
} else {
var colModel01 = [ 		
		{display: 'Name', name : 'salesman_name', width : 120, sortable : true, align: 'left'},
		{display: 'Username', name : 'username', width : 120, sortable : true, align: 'left'},
		{display: 'Email ID', name : 'email_id', width : 120, sortable : true, align: 'left'},
		{display: 'Phone Number', name : 'salesman_phome_no', width : 120, sortable : true, align: 'left'},
		{display: 'Address', name : 'salesman_address', width : 120, sortable : true, align: 'left'},
		{display: 'Start Date', name : 'start_date', width : 80, sortable : true, align: 'left'},
		{display: 'End Date', name : 'end_date', width : 80, sortable : true, align: 'left'},
		{display: 'Salesman Status', name : 'status', width : 80, sortable : true, align: 'center'},
		{display: 'Login Status', name : 'loginStatus', width : 80, sortable : true, align: 'center'}
			
		];	
	
}

	
j("#flex1").flexigrid({
	url: 'salesman.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel    : colModel01, 
	buttons : [
		{name: 'Add', bclass: 'add', onpress : eventHandlerSalAdminUser},
		{separator: true},		
		{name: 'Edit', bclass: 'edit', onpress : eventHandlerSalAdminUser},
		{separator: true},
		{name: 'Delete', bclass: 'delete', onpress : eventHandlerSalAdminUser},
		{separator: true}
		//{name: 'View Salesman Schedule', bclass: 'schedule', onpress : eventHandlerSalAdminUser},
		//{separator: true}
		],
	searchitems : [
		{display: 'Name', name : 's.salesman_name', isdefault: true},
		{display: 'Username', name : 'w.username'},
		{display: 'Email ID', name : 'w.email_id'},
		{display: 'Phone Number', name : 's.salesman_phome_no'},
		{display: 'Address', name : 's.salesman_address'}
		],
	sortname: "s.salesman_name",
	sortorder: "asc",
	usepager: true,
	title: 'Salesman',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 700,
	height: 'auto'
});  
}
function eventHandlerSalAdminUser(com,grid)
{
	if (com=='Add')	{
		location.href="salesman.php?add";	
	}else if (com=='Import')	{
		location.href="salesman.php?import";	
	}else if (com=='Export')	{
		location.href="export.inc.php?export_salesman_list";	
	}
	else{
		if( j('.trSelected',grid).length<=0 ||  j('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=j('.trSelected',grid);		
		if (com=='Edit'){
			location.href="salesman.php?id="+items[0].id.substr(3);			
		}else {
			if(com=='View Salesman Schedule'){
			location.href="salesman.php?schid="+items[0].id.substr(3);	
			}
		}
		if (com=='Delete'){
			if(confirm("Are you sure, you want to delete selected Salesman?")){
					
				j.post("salesman.php", {"delete":"yes","id":items[0].id.substr(3)},function(data)
				{	
				   j('#message-green').hide();
				  if(data!="") {					 
					 j('#flex1').flexOptions().flexReload(); 
				  }else {
					  alert("Record doesn't exists.")
				  }
				  
				});		
			}			
		}
	}			
}

/*function showSalsman(){
j("#flex1").flexigrid({
	url: 'salesman.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Name', name : 'salesman_name', width : 120, sortable : true, align: 'left'},
		{display: 'Username', name : 'username', width : 120, sortable : true, align: 'left'},
		{display: 'Email ID', name : 'email_id', width : 120, sortable : true, align: 'left'},
		{display: 'Phone Number', name : 'salesman_phome_no', width : 120, sortable : true, align: 'left'},
		{display: 'Address', name : 'salesman_address', width : 120, sortable : true, align: 'left'},
		{display: 'Start Date', name : 'start_date', width : 80, sortable : true, align: 'left'},
		{display: 'End Date', name : 'end_date', width : 80, sortable : true, align: 'left'},
		{display: 'Salesman Status', name : 'status', width : 80, sortable : true, align: 'center'},
		{display: 'Login Status', name : 'loginStatus', width : 80, sortable : true, align: 'center'}
			
		],
	buttons : [
		{name: 'Add', bclass: 'add', onpress : eventHandlerSalAdminUser},
		{separator: true},		
		{name: 'Edit', bclass: 'edit', onpress : eventHandlerSalAdminUser},
		{separator: true},
		{name: 'Delete', bclass: 'delete', onpress : eventHandlerSalAdminUser},
		{separator: true},
		{name: 'View Salesman Schedule', bclass: 'edit', onpress : eventHandlerSalAdminUser},
		{separator: true}
		],
	searchitems : [
		{display: 'Name', name : 'salesman_name', isdefault: true},
		{display: 'Username', name : 'username'},
		{display: 'Email ID', name : 'email_id'},
		{display: 'Phone Number', name : 'salesman_phome_no'},
		{display: 'Address', name : 'salesman_address'}
		],
	sortname: "salesman_name",
	sortorder: "asc",
	usepager: true,
	title: 'Salesman',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 700,
	height: 'auto'
});  
}
function eventHandlerSalAdminUser(com,grid)
{
	if (com=='Add')	{
		location.href="salesman.php?add";	
	}else if (com=='Import')	{
		location.href="salesman.php?import";	
	}else if (com=='Export')	{
		location.href="export.inc.php?export_salesman_list";	
	}
	else{
		if( j('.trSelected',grid).length<=0 ||  j('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=j('.trSelected',grid);		
		if (com=='Edit'){
			location.href="salesman.php?id="+items[0].id.substr(3);			
		}else {
			if(com=='View Salesman Schedule'){
			location.href="salesman.php?schid="+items[0].id.substr(3);	
			}
		}
		if (com=='Delete'){
			if(confirm("Are you sure, you want to delete selected Salesman?")){
					
				j.post("salesman.php", {"delete":"yes","id":items[0].id.substr(3)},function(data)
				{	
				   j('#message-green').hide();
				  if(data!="") {					 
					 j('#flex1').flexOptions().flexReload(); 
				  }else {
					  alert("Record doesn't exists.")
				  }
				  
				});		
			}			
		}
	}			
}*/

function showManageUser(){
j("#flex1").flexigrid({
	url: 'manage_user.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Name', name : 'operator_name', width : 100, sortable : true, align: 'left'},
		{display: 'Username', name : 'username', width : 100, sortable : true, align: 'left'},
		{display: 'Email ID', name : 'email_id', width : 100, sortable : true, align: 'left'},
		{display: 'Phone Number', name : 'operator_phone_number', width : 100, sortable : true, align: 'left'},
		{display: 'Manage Items', name : 'manage_items', width : 100, sortable : true, align: 'left'},
		{display: 'Manage Distributors', name : 'manage_distributors', width : 100, sortable : true, align: 'left'},
		{display: 'Manage Retailer', name : 'manage_retailer', width : 100, sortable : true, align: 'left'},
		{display: 'Manage Salesman', name : 'manage_salesman', width : 100, sortable : true, align: 'left'},
		{display: 'Manage Route', name : 'manage_route', width : 100, sortable : true, align: 'left'},
		{display: 'Manage Retailer Relationship', name : 'manage_route', width : 100, sortable : true, align: 'left'},
		{display: 'Start Date', name : 'start_date', width : 80, sortable : true, align: 'left'},
		{display: 'End Date', name : 'end_date', width : 80, sortable : true, align: 'left'},
		{display: 'Users Status', name : 'status', width : 80, sortable : true, align: 'center'},
		{display: 'Login Status', name : 'loginStatus', width : 80, sortable : true, align: 'center'}
			
		],
	buttons : [
		{name: 'Add', bclass: 'add', onpress : eventHandlerManageUser},
		{separator: true},		
		{name: 'Edit', bclass: 'edit', onpress : eventHandlerManageUser},
		{separator: true},
		{name: 'Export', bclass: 'export', onpress : eventHandlerManageUser},
		{separator: true}
		],
	searchitems : [
		{display: 'Name', name : 'operator_name', isdefault: true},
		{display: 'Username', name : 'username'},
		{display: 'Email ID', name : 'email_id'},
		{display: 'Manage Items', name : 'manage_items'},
		{display: 'Manage Distributors', name : 'manage_distributors'},
		{display: 'Manage Retailer', name : 'manage_retailer'},
		{display: 'Manage Salesman', name : 'manage_salesman'},
		{display: 'Manage Route', name : 'manage_route'}
		],
	sortname: "operator_name",
	sortorder: "asc",
	usepager: true,
	title: 'Company Users',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 750,
	height: 'auto'
});  
}
function eventHandlerManageUser(com,grid)
{

	if (com=='Add')	{
		location.href="manage_user.php?add";	
	}else if (com=='Import')	{
		location.href="salesman.php?import";	
	}else if (com=='Export')	{
		location.href="export.inc.php?export_manageuser_list";	
	}
	else{
		if( j('.trSelected',grid).length<=0 ||  j('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=j('.trSelected',grid);		
		if (com=='Edit'){
			location.href="manage_user.php?id="+items[0].id.substr(3);			
		}else if (com=='Create Login')	{
		location.href="#.php?login&id="+items[0].id.substr(3);
	}
			
	}
}
function showUserList(){
j("#flex1").flexigrid({
	url: 'users_list.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Name', name : 'name', width : 140, sortable : true, align: 'left'},
		{display: 'Username', name : 'username', width : 140, sortable : true, align: 'left'},
		{display: 'Email ID', name : 'email_id', width : 140, sortable : true, align: 'left'},
		{display: 'Phone Number', name : 'salesman_phome_no', width : 140, sortable : true, align: 'left'},
		{display: 'User Type', name : 'user_type', width : 140, sortable : true, align: 'left'},
		{display: 'Login Status', name : 'status', width : 80, sortable : true, align: 'center'}
			
		],
	buttons : [
		
		],
	searchitems : [
		{display: 'Username', name : 'username', isdefault: true},
		{display: 'Email ID', name : 'email_id'},
		{display: 'Phone Number', name : 'salesman_phome_no'},
		{display: 'User Type', name : 'user_type'}
		],
	sortname: "web_user_id",
	sortorder: "asc",
	usepager: true,
	title: 'Users',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 850,
	height: 'auto'
});  
}
function eventHandlerUserListAdminUser(com,grid)
{
	if (com=='Add')	{
		location.href="#.php?add";	
	}
	else{
		if( j('.trSelected',grid).length<=0 ||  j('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=j('.trSelected',grid);		
		if (com=='Edit'){
			location.href="users_list.php?id="+items[0].id.substr(3);			
		}	
	}
}
function showRoute(){
j("#flex1").flexigrid({
	url: 'route.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Name', name : 'route_name', width : 250, sortable : true, align: 'left'},
		{display: 'Dealer Counts', name : 'retailers', width : 100, sortable : true, align: 'left'},
		{display: 'Distributor Counts', name : 'distributors', width : 100, sortable : true, align: 'left'},
		/*{display: 'Shakti-Partner Counts', name : 'customers', width : 120, sortable : true, align: 'left'},*/
		// {display: 'Option', name : 'option', width : 60, sortable : true, align: 'left'},
		{display: 'Status', name : 'status', width : 100, sortable : true, align: 'left'}
		],
	buttons : [
		{name: 'Add', bclass: 'add', onpress : eventHandlerRoute},
		{separator: true},		
		{name: 'Edit', bclass: 'edit', onpress : eventHandlerRoute},
		{separator: true},			
		{name: 'Delete', bclass: 'delete', onpress : eventHandlerRoute},
		{separator: true},
  		{name: 'Sort Dealer', bclass: 'Sort', onpress : eventHandlerRoute},
  		{separator: true}
		
		],
	searchitems : [
		{display: 'Name', name : 'route_name', isdefault: true}
		
		],
	sortname: "route_name",
	sortorder: "asc",
	usepager: true,
	title: 'Route',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 750,
	height: 'auto'
});  
}
function eventHandlerRoute(com,grid)
{
	if (com=='Import')	{
		location.href="route.php?import";		
	}
	else if (com=='Export')	{
		location.href="#.php?export_route";		
	}
	else if (com=='Add')	{
		location.href="add_route.php";		
	}else{
		if( j('.trSelected',grid).length<=0 ||  j('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=j('.trSelected',grid);		
		if (com=='Edit'){
			location.href="add_route.php?id="+items[0].id.substr(3);			
		}
		if (com=='Delete'){
			if(confirm("Are you sure, you want to delete selected Route?")){
					
				j.post("route.php", {"delete":"yes","id":items[0].id.substr(3)},function(data)
				{		 
				  if(data!="") {					 
					 j('#flex1').flexOptions().flexReload(); 
				  }else {
					  alert("Record doesn't exists.")
				  }
				  
				});		
			}			
		}
		if (com=='Sort Retailers'){   
   				location.href="retailer_ordering.php?rid="+items[0].id.substr(3);   
	  }
	}
}






// function showRoute(){
// j("#flex1").flexigrid({
// 	url: 'route.php?show=yes',
// 	dataType: 'json',
// 	method : "GET", 
// 	singleSelect:true, 
// 	colModel : [		
// 		{display: 'Route Name', name : 'route_name', width : 100, sortable : true, align: 'left'},
// 		{display: 'Salesman Name', name : 'salesman_name', width : 100, sortable : true, align: 'left'},
// 		{display: 'Salesman Code', name : 'salesman_code', width : 100, sortable : true, align: 'left'},
// 		{display: 'Date of Route', name : '', width : 100, sortable : true, align: 'left'},
// 		{display: 'Dealer Counts', name : 'retailers', width : 100, sortable : true, align: 'left'},
// 		{display: 'Distributor Counts', name : 'distributors', width : 100, sortable : true, align: 'left'},
// 		/*{display: 'Shakti-Partner Counts', name : 'customers', width : 120, sortable : true, align: 'left'},*/
// 		// {display: 'Option', name : 'option', width : 60, sortable : true, align: 'left'},
// 		{display: 'Status', name : 'status', width : 70, sortable : true, align: 'left'}
// 		],
// 	buttons : [
// 		{name: 'Add', bclass: 'add', onpress : eventHandlerRoute},
// 		{separator: true},		
// 		{name: 'Edit', bclass: 'edit', onpress : eventHandlerRoute},
// 		{separator: true},			
// 		{name: 'Delete', bclass: 'delete', onpress : eventHandlerRoute},
// 		{separator: true},
//   		{name: 'Sort Dealer', bclass: 'Sort', onpress : eventHandlerRoute},
//   		{separator: true}
		
// 		],
// 	searchitems : [
// 		{display: 'Route Name', name : 'R.route_name', isdefault: true}
		
// 		],
// 	sortname: "R.route_name",
// 	sortorder: "asc",
// 	usepager: true,
// 	title: 'Route',
// 	useRp: true,
// 	rp: 50,
// 	showTableToggleBtn: true,
// 	width: 750,
// 	height: 'auto'
// });  
// }
// function eventHandlerRoute(com,grid)
// {
// 	if (com=='Import')	{
// 		location.href="route.php?import";		
// 	}
// 	else if (com=='Export')	{
// 		location.href="#.php?export_route";		
// 	}
// 	else if (com=='Add')	{
// 		location.href="add_route.php";		
// 	}else{
// 		if( j('.trSelected',grid).length<=0 ||  j('.trSelected',grid).length>1){
// 			alert("Please Select One Record.");
// 			return false;
// 		}
// 		var items=j('.trSelected',grid);		
// 		if (com=='Edit'){
// 			location.href="add_route.php?id="+items[0].id.substr(3)+'&route_schedule_detail_id='+items[0].sid;			
// 		}
// 		if (com=='Delete'){
// 			if(confirm("Are you sure, you want to delete selected Route?")){
					
// 				j.post("route.php", {"delete":"yes","id":items[0].id.substr(3),"route_schedule_detail_id":items[0].sid },function(data)
// 				{		 
// 				  if(data!="") {					 
// 					 j('#flex1').flexOptions().flexReload(); 
// 				  }else {
// 					  alert("Record doesn't exists.")
// 				  }
				  
// 				});		
// 			}			
// 		}
// 		if (com=='Sort Retailers'){   
//    				location.href="retailer_ordering.php?rid="+items[0].id.substr(3);   
// 	  }
// 	}
// }





function showRouteSchedule(){
j("#flex1").flexigrid({
	url: 'route_schedule.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Salesperson Name', name : 'salesman_name', width : 300, sortable : true, align: 'left'},
		{display: 'Month', name : 'month', width : 200, sortable : true, align: 'left'},
		{display: 'Year', name : 'year', width : 200, sortable : true, align: 'left'},
		{display: 'Status', name : 'status', width : 100, sortable : true, align: 'left'}
		],
	buttons : [
		{name: 'Add', bclass: 'add', onpress : eventHandlerRouteSchedule},
		{separator: true},		
		{name: 'Edit', bclass: 'edit', onpress : eventHandlerRouteSchedule},
		{separator: true}
		/*,
		{name: 'View Salesman Schedule', bclass: 'salschedule', onpress : eventHandlerRouteSchedule},
		{separator: true},
		{name: 'Export', bclass: 'export', onpress : eventHandlerRouteSchedule},
		{separator: true}*/
		
		],
	searchitems : [
		{display: 'Salesperson Name', name : 'salesman_name', isdefault: true},
		{display: 'Month', name : 'month', isdefault: true},
		{display: 'Year', name : 'year', isdefault: true}
		
		],
	sortname: "salesman_name",
	sortorder: "asc",
	usepager: true,
	title: 'Route Schedule',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 700,
	height: 'auto'
});  
}
function eventHandlerRouteSchedule(com,grid)
{
	if (com=='Export')	{
		location.href="#.php?export_route";		
	}
	else if (com=='Add')	{
		location.href="add_route_schedule.php";		
	}else{
		if( j('.trSelected',grid).length<=0 ||  j('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=j('.trSelected',grid);		
		if (com=='Edit'){
			location.href="add_route_schedule_edit.php?id="+items[0].id.substr(3);			
		}
		if (com=='View Salesman Schedule'){
			location.href="route_schedule.php?schid="+items[0].id.substr(3);			
		}
		if (com=='Delete'){
			if(confirm("Are you sure, you want to delete selected Category?")){
					
				j.post("category.php", {"delete":"yes","id":items[0].id.substr(3)},function(data)
				{		 
				  if(data!="") {					 
					 j('#flex1').flexOptions().flexReload(); 
				  }else {
					  alert("Record doesn't exists.")
				  }
				  
				});		
			}			
		}	
	}
}
function showOrderList(){
j("#flex1").flexigrid({
	url: 'admin_order_list.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [
		{display: 'Salesperson Name', name : 'salesman_name', width : 100, sortable : true, align: 'left'},
		{display: 'Tally MasterID', name : 'LASTVCHID', width : 100, sortable : true, align: 'left'},
		{display: 'Tally Order No', name : 'TALLYORDERNO', width : 100, sortable : true, align: 'left'},
		{display: 'Reporting To', name : 'rpt_to', width : 100, sortable : true, align: 'left'},
		{display: 'Order Status', name : 'order_status', width : 100, sortable : true, align: 'left'},
		{display: 'No Order Reason', name : 'noOrderReason', width : 200, sortable : true, align: 'left'},
		{display: 'Comments', name : 'orderComments', width : 150, sortable : true, align: 'left'},		
		{display: 'Total Invoice Amount', name : 'acc_total_invoice_amount', width : 100, sortable : true, align: 'left'},
		{display: 'Total no of items', name : 'total_item', width : 100, sortable : true, align: 'left'},
		{display: 'Quantity', name : 'total_quantity1', width : 100, sortable : true, align: 'left'},
		{display: 'Map', name : 'lat', width : 100, sortable : true, align: 'left'},
		//{display: 'Dealer Photo', name : 'lat', width : 80, sortable : true, align: 'left'},
		//{display: 'Survey Status', name : 'survey_status', width : 100, sortable : true, align: 'left'},
		//{display: 'Visit Status', name : 'visit_status', width : 100, sortable : true, align: 'left'},
		//{display: 'Reason', name : 'reason', width : 100, sortable : true, align: 'left'},
		{display: 'Dealer Name', name : 'retailer_name', width : 100, sortable : true, align: 'left'},
		//{display: 'Dealer Class', name : 'retailer_class', width : 100, sortable : true, align: 'left'},
		{display: 'Dealer Division', name : 'retailer_division', width : 100, sortable : true, align: 'left'},
		{display: 'Dealer Pincode', name : 'rpin_code', width : 100, sortable : true, align: 'left'},
		{display: 'Order Number', name : 'order_id', width : 100, sortable : true, align: 'left'},
		{display: 'Date of Order', name : 'date_of_order', width : 100, sortable : true, align: 'left'},
		{display: 'Time of Order', name : 'time_of_order', width : 100, sortable : true, align: 'left'},
		{display: 'Salesperson Code', name : 'salesman_code', width : 100, sortable : true, align: 'left'},
		//{display: 'Reporting Person Code', name : 'rpt_to_code', width : 100, sortable : true, align: 'left'},
		{display: 'Distributor Code', name : 'distributor_code', width : 100, sortable : true, align: 'left'},
		{display: 'Distributor Name', name : 'distributor_name', width : 100, sortable : true, align: 'left'},
		//{display: 'Distributor Class', name : 'distributor_class', width : 100, sortable : true, align: 'left'},
		{display: 'Distributor Pincode', name : 'dpin_code', width : 100, sortable : true, align: 'left'},
		{display: 'Dealer Code', name : 'retailer_code', width : 100, sortable : true, align: 'left'},
		//{display: 'Dealer Market', name : 'retailer_location', width : 100, sortable : true, align: 'left'},
		//{display: 'Dealer Channel', name : 'channel_name', width : 100, sortable : true, align: 'left'},
		/*{display: 'Display Outlet', name : 'display_outlet', width : 100, sortable : true, align: 'left'},*/
		{display: 'Dealer Address', name : 'retailer_address', width : 150, sortable : true, align: 'left'},
		{display: 'State', name : 'retailer_state', width : 100, sortable : true, align: 'left'},
		{display: 'City', name : 'retailer_city', width : 100, sortable : true, align: 'left'},
		//{display: 'Taluka', name : 'retailer_taluka', width : 150, sortable : true, align: 'left'},
		//{display: 'Comments', name : 'comments', width : 150, sortable : true, align: 'left'},
		{display: 'Approval Status', name : 'Approval_Status', width : 150, sortable : true, align: 'left'},
		{display: 'Invoice', name : 'Invoice', width : 150, sortable : true, align: 'left'}
		],
	buttons : [		
		{name: 'View Order Detail', bclass: 'orderdeails', onpress : eventHandlerOrderList},
		{separator: true},
		{name: 'Dealer Report (F2)', bclass: 'redit', onpress : eventHandlerOrderList},
		{separator: true},
		{name: 'Salesman Report (F4)', bclass: 'sedit', onpress : eventHandlerOrderList},
		{separator: true},
        //{name: 'Export to Tally', bclass: 'export', onpress : eventHandlerOrderList},
		//{separator: true},
		{name: 'Push To Tally', bclass: 'export_one', onpress : eventHandlerOrderList},
		{separator: true},
        {name: 'Pull From Tally', bclass: 'pull', onpress : eventHandlerOrderList},
		{separator: true}



		],
	searchitems : [
		{display: 'Order Number', name : 'order_id', isdefault: true},
		{display: 'Date of Order', name : 'date_of_order'},
		{display: 'Distributor Name', name : 'distributor_name'},
		{display: 'Salesperson Name', name : 'salesman_name'},
		{display: 'Dealer Name', name : 'retailer_name'},
		//{display: 'Dealer Channel', name : 'channel_name'},
		//{display: 'Display  Outlet', name : 'display_outlet'},
		//{display: 'Dealer Market', name : 'retailer_location'},
		{display: 'Dealer Address', name : 'retailer_address'}
		//{display: 'Order Taken', name : 'order_type'}
		],
	sortname: "date_of_order",
	sortorder: "desc",
	usepager: true,
	title: 'Order List',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 1000,
	height: 'auto'
});  
}
function eventHandlerOrderList(com,grid)
{
	
	if (com=='Add')	{
		location.href="#";	
	}

  //         if (com=='Push to Tally')	{
		// //location.href="http://localhost/voucher2.php";	
		// window.open("http://localhost/voucher2.php",'_blank');
		// }
         



	else{
		if( j('.trSelected',grid).length<=0 ||  j('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=j('.trSelected',grid);		
		if (com=='View Order Detail'){
			var searchParam_1 = j('input[name=q]').val();
			var searchParam_2 = j('select[name=qtype]').val();
			var searchParam_3 = j(".pcontrol").find("input").val();
			location.href="admin_order_list.php?id="+items[0].id.substr(3)+"&searchParam_1="+searchParam_1+"&searchParam_2="+searchParam_2+"&searchParam_3="+searchParam_3;	
		}else if (com=='Dealer Report (F2)')	{
		window.open("retailer_his_report.php?ordid="+items[0].id.substr(3), '_blank');
		//location.href="retailer_items_report.php?id="+items[0].id.substr(3);
		}else if (com=='Salesman Report (F4)')	{
		location.href="admin_order_list.php?salid="+items[0].id.substr(3);	
		//location.href="retailer_items_report.php?id="+items[0].id.substr(3);
		}	

                 else if (com=='Export to Tally')	{
			var searchParam_1 = j('input[name=q]').val();
			var searchParam_2 = j('select[name=qtype]').val();
			var searchParam_3 = j(".pcontrol").find("input").val();
			location.href="download_tally_list.php?id="+items[0].id.substr(3)+"&searchParam_1="+searchParam_1+"&searchParam_2="+searchParam_2+"&searchParam_3="+searchParam_3;	
		
		//location.href="retailer_items_report.php?id="+items[0].id.substr(3);
		}	




  //       else  if (com=='Push to Tally')	{
        	
		// 	var link = "download_tally_list.php?id="+items[0].id.substr(3);
		// 	window.open(link,'_blank');
		// }
         
        else  if (com=='Push To Tally')	{

        	j( "#dialog" ).html('<center><img src="images/ajax-loader.gif" /></center>');

        	j( function() { j( "#dialog" ).dialog(); } );

        	var link = "download_tally_list.php?id="+items[0].id.substr(3);
        	j.ajax({
			  method: "POST",
			  url: link,
			  data: {}
			})
			  .done(function( data ) {

			  var msg = JSON.parse(data);
			  //alert( "Data Saved: " + msg.status );

			   	if(msg.status == 'Success') {
			   		 j(".trSelected td:nth-child(2)").find('div').text(msg.id);
			   		j(".trSelected td:nth-child(3)").find('div').text(msg.TALLYORDERNO);
			   	}
			   	 

			   		
			      j( "#dialog" ).html(msg.data);
			  });
			        	
			
			//window.open(link,'_blank');
		}
         




         
        else  if (com=='Pull From Tally')	{
        	
			// var link = "export_tally.php?id="+items[0].id.substr(3);
			// window.open(link,'_blank');

        	j( "#dialog2" ).html('<center><img src="images/ajax-loader.gif" /></center>');

        	j( function() { 
        		j( "#dialog2" ).dialog({
        			maxWidth:800,
        			maxHeight: 700,
       				width: 800,
        			height: 700,
        			modal: true,
        			buttons: { 
			            "Print": function() { 
			                j("#dialog2").printArea(); 
			            },
			            "Close": function() { 
			               j(this).dialog("close");
			            }
			         }
        		}); 
        	} );

        	var link = "invoice_template.php?id="+items[0].id.substr(3);
        	j.ajax({
			  method: "POST",
			  url: link,
			  data: {}
			})
			  .done(function( data ) {

			  //var msg = JSON.parse(data);
			 // alert( "Data Saved: " + data );

			   	// if(msg.status == 'Success')
			   	//   j(".trSelected td:nth-child(2)").find('div').text(items[0].id.substr(3));
			     j( "#dialog2" ).html(data);


			  });
			        	
			
			//window.open(link,'_blank');



		}




	}
}
function showSalsmanOrderList(){
j("#flex1").flexigrid({
	url: 'salesman_order_list.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [
		{display: 'Date of Order', name : 'date_of_order', width : 100, sortable : true, align: 'left'},
		{display: 'Time of Order', name : 'time_of_order', width : 100, sortable : true, align: 'left'},
		{display: 'Retailer Name', name : 'retailer_name', width : 120, sortable : true, align: 'left'},
		{display: 'Retailer Market', name : 'retailer_location', width : 120, sortable : true, align: 'left'},
		{display: 'Retailer Address', name : 'retailer_address', width : 250, sortable : true, align: 'left'},
		{display: 'Comments', name : 'comments', width : 250, sortable : true, align: 'left'},
		{display: 'Order', name : 'order_type', width : 100, sortable : true, align: 'left'},
		{display: 'Status', name : 'order_status', width : 50, sortable : true, align: 'left'}
			
		],
	buttons : [		
		{name: 'View Order Detail', bclass: 'orderdeails', onpress : eventHandlerSalsmanOrderList},
		{separator: true}
		],
	searchitems : [
		{display: 'Date of Order', name : 'date_of_order', isdefault: true},
		{display: 'Retailer Name', name : 'retailer_name'},
		{display: 'Retailer Market', name : 'retailer_location'},
		{display: 'Retailer Address', name : 'retailer_address'},
		{display: 'Order Type', name : 'order_type'}
		],
	sortname: "date_of_order",
	sortorder: "desc",
	usepager: true,
	title: 'Order List',
	useRp: true,
	rp: 15,
	showTableToggleBtn: true,
	width: 'auto',
	height: 375
});  
}
function eventHandlerSalsmanOrderList(com,grid)
{
	if (com=='Add')	{
		location.href="salesman.php?add";	
	}
	else{
		if( j('.trSelected',grid).length<=0 ||  j('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=j('.trSelected',grid);		
		if (com=='View Order Detail'){
			location.href="salesman_order_list.php?order_id="+items[0].id.substr(3);			
		}else if (com=='Create Login')	{
		location.href="#.php?login&id="+items[0].id.substr(3);
	}
			
	}
}
function showDisributorOrderList(){
j("#flex1").flexigrid({
	url: 'distributors_order_list.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [
		{display: 'Date', name : 'date_of_order', width : 80, sortable : true, align: 'left'},
		{display: 'Time', name : 'time_of_order', width : 60, sortable : true, align: 'left'},
		{display: 'Order Number', name : 'order_id', width : 80, sortable : true, align: 'left'},
		{display: 'Invoice Amt', name : 'acc_total_invoice_amount', width : 80, sortable : true, align: 'left'},
		{display: 'Distributor', name : 'distributor_name', width : 180, sortable : true, align: 'left'},
		{display: 'Distributor Code', name : 'distributor_code', width : 120, sortable : true, align: 'left'},
		{display: 'Salesperson', name : 'salesman_name', width : 120, sortable : true, align: 'left'},
		{display: 'Salesperson Code', name : 'salesman_code', width : 120, sortable : true, align: 'left'},
		//{display: 'Dealer', name :'retailer_name', width : 120, sortable : true, align: 'left'},
		//{display: 'Dealer Code', name :'retailer_Code', width : 120, sortable : true, align: 'left'},
		//{display: 'Dealer Channel', name :'channel_name', width : 120, sortable : true, align: 'left'},
		//{display: 'Display Outlet', name : 'display_outlet', width : 120, sortable : true, align: 'left'},
		//{display: 'Market', name : 'retailer_location', width : 120, sortable : true, align: 'left'},
		//{display: 'Address', name : 'retailer_address', width : 150, sortable : true, align: 'left'},
		{display: 'Order Status', name : 'order_status', width : 150, sortable : true, align: 'left'}
			
		],
	buttons : [		
		{name: 'View Order Detail', bclass: 'orderdeails', onpress : eventHandlerDisributorOrderList},
		{separator: true}
		],
	searchitems : [
		{display: 'Order Number', name : 'o.order_id', isdefault: true},
		{display: 'Date of Order', name : 'date_of_order'},
		{display: 'Distributor', name : 'distributor_name'},
		{display: 'Salesperson', name : 'salesman_name'},
		{display: 'Dealer', name : 'retailer_name'}
		// {display: 'Dealer Channel', name : 'channel_name'},
		// {display: 'Display Outlet', name : 'display_outlet'},
		// {display: 'Market', name : 'retailer_location'}
		],
	sortname: "date_of_order",
	sortorder: "desc",
	usepager: true,
	title: 'Order List',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 1000,
	height: 'auto'
});  
}
function eventHandlerDisributorOrderList(com,grid)
{
	if (com=='Add')	{
		location.href="#";	
	}
	else{
		if( j('.trSelected',grid).length<=0 ||  j('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=j('.trSelected',grid);		
		if (com=='View Order Detail'){
			location.href="distributors_order_list.php?id="+items[0].id.substr(3);			
		}else if (com=='Create Login')	{
		location.href="#.php?login&id="+items[0].id.substr(3);
	}
			
	}
}


	/**************************************************************************
	* DESC: Show Counter Sales
	* Author: AJAY
	* Created AT: 2016-04-11
	*
	**/

function showDisributorCounterSales(){
j("#flex1").flexigrid({
	url: 'distributors_counter_sales.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [
		{display: 'Date', name : 'date_of_order', width : 80, sortable : true, align: 'left'},
		{display: 'Time', name : 'time_of_order', width : 60, sortable : true, align: 'left'},
		{display: 'Order Number', name : 'order_id', width : 80, sortable : true, align: 'left'},
		{display: 'Invoice Amt', name : 'acc_total_invoice_amount', width : 80, sortable : true, align: 'left'},
		{display: 'Distributor', name : 'distributor_name', width : 80, sortable : true, align: 'left'},
		{display: 'Distributor Code', name : 'distributor_code', width : 80, sortable : true, align: 'left'},
		{display: 'Distributor Division', name : 'division_name', width : 100, sortable : true, align: 'left'},
		// {display: 'Salesperson Code', name : 'salesman_code', width : 100, sortable : true, align: 'left'},
		// {display: 'Retailer', name :'retailer_name', width : 120, sortable : true, align: 'left'},
		// {display: 'Retailer Code', name :'retailer_Code', width : 120, sortable : true, align: 'left'},
		// {display: 'Retailer Channel', name :'channel_name', width : 120, sortable : true, align: 'left'},
		{display: 'Display Outlet', name : 'display_outlet', width : 120, sortable : true, align: 'left'},
		{display: 'Market', name : 'retailer_location', width : 120, sortable : true, align: 'left'},
		{display: 'Address', name : 'retailer_address', width : 150, sortable : true, align: 'left'},
		{display: 'Order Status', name : 'order_status', width : 150, sortable : true, align: 'left'}
			
		],
	buttons : [		
		{name: 'View Order Detail', bclass: 'orderdeails', onpress : eventHandlerDisributorCounterSale},
		{separator: true}
		],
	searchitems : [
		{display: 'Order Number', name : 'order_id', isdefault: true},
		{display: 'Date of Order', name : 'Date of Order'},
		{display: 'Distributor', name : 'distributor_name'},
		{display: 'Division Name', name : 'division_name'},
		// {display: 'Retailer', name : 'retailer_name'},
		// {display: 'Retailer Channel', name : 'channel_name'},
		{display: 'Display Outlet', name : 'display_outlet'},
		{display: 'Market', name : 'distributor_location'}
		],
	sortname: "date_of_order",
	sortorder: "desc",
	usepager: true,
	title: 'Distributor Counter Sales',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 1000,
	height: 'auto'
});  
}


function eventHandlerDisributorCounterSale(com,grid)
{
	if (com=='Add')	{
		location.href="#";	
	}
	else{
		if( j('.trSelected',grid).length<=0 ||  j('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=j('.trSelected',grid);		
		if (com=='View Order Detail'){
			location.href="dis_cs_detail.php?id="+items[0].id.substr(3);			
		}else if (com=='Create Login')	{
		location.href="#.php?login&id="+items[0].id.substr(3);
	}
			
	}
}






function showRetailerOrderList(){
j("#flex1").flexigrid({
	url: 'retailer_order_list.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [
		{display: 'Date of Order', name : 'date_of_order', width : 120, sortable : true, align: 'left'},
		{display: 'Time of Order', name : 'time_of_order', width : 120, sortable : true, align: 'left'},
		{display: 'Salesperson Name', name : 'salesman_name', width : 120, sortable : true, align: 'left'},
		{display: 'Distributor Name', name : 'distributor_name', width : 120, sortable : true, align: 'left'},
		{display: 'Comments', name : 'comments', width : 250, sortable : true, align: 'left'},
		{display: 'Order Taken', name : 'order_type', width : 100, sortable : true, align: 'left'},
		{display: 'Order Status', name : 'order_status', width : 100, sortable : true, align: 'left'}
			
		],
	buttons : [		
		{name: 'View Order Detail', bclass: 'orderdeails', onpress : eventHandlerRetailerOrderList},
		{separator: true}
		],
	searchitems : [
		{display: 'Date of Order', name : 'Date of Order', isdefault: true},
		{display: 'Salesperson Name', name : 'salesman_name', isdefault: true},
		{display: 'Distributor Name', name : 'distributor_name'},
		{display: 'Order Taken', name : 'order_type'}
		],
	sortname: "date_of_order",
	sortorder: "desc",
	usepager: true,
	title: 'Order List',
	useRp: true,
	rp: 15,
	showTableToggleBtn: true,
	width: 'auto',
	height: 375
});  
}
function eventHandlerRetailerOrderList(com,grid)
{
	if (com=='Add')	{
		location.href="#";	
	}
	else{
		if( j('.trSelected',grid).length<=0 ||  j('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=j('.trSelected',grid);		
		if (com=='View Order Detail'){
			location.href="retailer_order_list.php?order_id="+items[0].id.substr(3);			
		}else if (com=='Create Login')	{
		location.href="#.php?login&id="+items[0].id.substr(3);
	}
			
	}
}
function showLoginSalsman(){
j("#flex1").flexigrid({
	url: 'salesman_list.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Salesman Name', name : 'salesman_name', width : 120, sortable : true, align: 'left'},
		{display: 'Username', name : 'username', width : 120, sortable : true, align: 'left'},
		{display: 'Email ID', name : 'email_id', width : 150, sortable : true, align: 'left'},
		{display: 'Phone Number', name : 'salesman_phome_no', width : 120, sortable : true, align: 'left'},
		{display: 'Login Status', name : '', width : 120, sortable : true, align: 'left'},
		{display: '', name : '', width : 120, sortable : true, align: 'left'}
			
		],
	buttons : [
		],
	searchitems : [
		{display: 'Salesman Name', name : 'salesman_name', isdefault: true},
		{display: 'Username', name : 'username'},
		{display: 'Email ID', name : 'email_id'},
		{display: 'Phone Number', name : 'salesman_phome_no'}
		],
	sortname: "salesman_name",
	sortorder: "asc",
	usepager: true,
	title: 'Salesman List',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 850,
	height: 'auto'
});  
}
function showLoginServicePersonnel(){
j("#flex1").flexigrid({
	url: 'service_personnel_list.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Service Personnel Name', name : 'sp_name', width : 120, sortable : true, align: 'left'},
		{display: 'Username', name : 'username', width : 120, sortable : true, align: 'left'},
		{display: 'Email ID', name : 'email_id', width : 150, sortable : true, align: 'left'},
		{display: 'Phone Number', name : 'sp_phone_no', width : 120, sortable : true, align: 'left'},
		{display: 'Login Status', name : '', width : 120, sortable : true, align: 'left'},
		{display: '', name : '', width : 120, sortable : true, align: 'left'}
			
		],
	buttons : [
		],
	searchitems : [
		{display: 'Service Personnel Name', name : 'sp_name', isdefault: true},
		{display: 'Username', name : 'username'},
		{display: 'Email ID', name : 'email_id'},
		{display: 'Phone Number', name : 'sp_phone_no'}
		],
	sortname: "sp_name",
	sortorder: "asc",
	usepager: true,
	title: 'Service Personnel List',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 850,
	height: 'auto'
});  
}

function showLoginDistributors(){
j("#flex1").flexigrid({
	url: 'distributor_list.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Distributor Name', name : 'distributor_name', width : 120, sortable : true, align: 'left'},
		{display: 'Username', name : 'username', width : 120, sortable : true, align: 'left'},
		{display: 'Email ID', name : 'email_id', width : 150, sortable : true, align: 'left'},
		{display: 'Phone Number', name : 'distributor_phone_no', width : 120, sortable : true, align: 'left'},
		{display: 'Login Status', name : '', width : 120, sortable : true, align: 'left'},
		{display: '', name : '', width : 120, sortable : true, align: 'left'}
			
		],
	buttons : [
		],
	searchitems : [
		{display: 'Distributor Name', name : 'distributor_name', isdefault: true},
		{display: 'Username', name : 'username'},
		{display: 'Email ID', name : 'email_id'},
		{display: 'Phone Number', name : 'distributor_phone_no'}
		],
	sortname: "distributor_name",
	sortorder: "asc",
	usepager: true,
	title: 'Distributor List',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 850,
	height: 'auto'
});  
}


function showLoginRetailers(){
j("#flex1").flexigrid({
	url: 'retailer_list.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Retailer Name', name : 'retailer_name', width : 120, sortable : true, align: 'left'},
		{display: 'Username', name : 'username', width : 120, sortable : true, align: 'left'},
		{display: 'Email ID', name : 'email_id', width : 150, sortable : true, align: 'left'},
		{display: 'Phone Number', name : 'retailer_phone_no', width : 120, sortable : true, align: 'left'},
		{display: 'Login Status', name : '', width : 120, sortable : true, align: 'left'},
		{display: '', name : '', width : 120, sortable : true, align: 'left'}
			
		],
	buttons : [
		],
	searchitems : [
		{display: 'Retailer Name', name : 'retailer_name', isdefault: true},
		{display: 'Username', name : 'username'},
		{display: 'Email ID', name : 'email_id'},
		{display: 'Phone Number', name : 'retailer_phone_no'}
		],
	sortname: "retailer_name",
	sortorder: "asc",
	usepager: true,
	title: 'Dealers List',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 850,
	height: 'auto'
});  
}


function showLoggedInWarehouses(){
j("#flex1").flexigrid({
	url: 'warehouse_list.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Warehouse Name', name : 'warehouse_name', width : 120, sortable : true, align: 'left'},
		{display: 'Username', name : 'username', width : 120, sortable : true, align: 'left'},
		{display: 'Email ID', name : 'email_id', width : 150, sortable : true, align: 'left'},
		{display: 'Phone Number', name : 'warehouse_phone_no', width : 120, sortable : true, align: 'left'},
		{display: 'Login Status', name : '', width : 120, sortable : true, align: 'left'},
		{display: '', name : '', width : 120, sortable : true, align: 'left'}
			
		],
	buttons : [
		],
	searchitems : [
		{display: 'Warehouse Name', name : 'warehouse_name', isdefault: true},
		{display: 'Username', name : 'username'},
		{display: 'Email ID', name : 'email_id'},
		{display: 'Phone Number', name : 'warehouse_phone_no'}
		],
	sortname: "warehouse_name",
	sortorder: "asc",
	usepager: true,
	title: 'Warehouse List',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 850,
	height: 'auto'
});  
}


function showTagList(){
j("#flex1").flexigrid({
	url: 'tag_list.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Description', name : 'tag_description', width : 500, sortable : true, align: 'left'},
		{display: 'status', name : 'status', width : 500, sortable : true, align: 'left'}	
		],
	buttons : [
		{name: 'Add', bclass: 'add', onpress : eventHandlerTagList},
		{separator: true},		
		{name: 'Edit', bclass: 'edit', onpress : eventHandlerTagList},
		{separator: true},
		{name: 'Delete', bclass: 'delete', onpress : eventHandlerTagList},
		{separator: true}
		],
	searchitems : [
		{display: 'Description', name : 'tag_description', isdefault: true}
		],
	sortname: "tag_type",
	sortorder: "asc",
	usepager: true,
	title: 'No Order Reason',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 850,
	height: 'auto'
});  
}
function eventHandlerTagList(com,grid)
{
	if (com=='Add')	{
		location.href="addtag.php";	
	}
	else{
		if( j('.trSelected',grid).length<=0 ||  j('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=j('.trSelected',grid);		
		if (com=='Edit'){
			location.href="addtag.php?id="+items[0].id.substr(3);			
		}
		if (com=='Delete'){
			if(confirm("Are you sure, you want to delete selected Tag?")){
					
				j.post("tag_list.php", {"delete":"yes","id":items[0].id.substr(3)},function(data)
				{		 
				  if(data!="") {					 
					 j('#flex1').flexOptions().flexReload(); 
				  }else {
					  alert("Record doesn't exists.")
				  }
				  
				});		
			}			
		}
	}
}

function showMessageCom(){
j("#flex1").flexigrid({
	url: 'message_communication.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Salesman Name', name : 'salesman_name', width : 100, sortable : true, align: 'left'},
		{display: 'Retailer Name', name : '', width : 180, sortable : true, align: 'left'},
		{display: 'Reporting to', name : 'salesman_name', width : 100, sortable : true, align: 'left'},
		{display: 'Salesman State', name : 'state_name', width : 100, sortable : true, align: 'left'},
		{display: 'Salesman City', name : 'city_name', width : 100, sortable : true, align: 'left'},
		{display: 'Message', name : 'comm_message', width : 300, sortable : true, align: 'left'},
		{display: 'Reason', name : 'reason', width : 300, sortable : true, align: 'left'},
		{display: 'Subreason', name : 'subreason', width : 300, sortable : true, align: 'left'},
		{display: 'Date', name : 'comm_date', width : 80, sortable : true, align: 'left'},
		{display: 'Time', name : 'comm_time', width : 80, sortable : true, align: 'left'}
		],
	buttons : [
		//{name: 'Send Message', bclass: 'sendmsg', onpress : eventHandlerMessageCom},
		//{separator: true},		
		{name: 'View Message', bclass: 'viewmsg', onpress : eventHandlerMessageCom},
		{separator: true}
		//{name: 'Delete', bclass: 'delete', onpress : eventHandlerMessageCom},
		//{separator: true}
		
		],
	searchitems : [
		{display: 'Salesman Name', name : 's.salesman_name', isdefault: true}
		//{display: 'Subject', name : 'subject'},
		//{display: 'Date', name : 'comm_date'}
		
		],
	sortname: "comm_id",
	sortorder: "desc",
	usepager: true,
	title: 'Complaint',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 950,
	height: 'auto'
});  
}
function eventHandlerMessageCom(com,grid)
{
	if (com=='Send Message')	{
		//location.href="send_message.php";		
	}else{
		if( j('.trSelected',grid).length<=0 ||  j('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=j('.trSelected',grid);		
		if (com=='View Message'){
			//location.href="view_message.php?id="+items[0].id.substr(3);	
			window.open("view_message_comm.php?id="+items[0].id.substr(3), '_blank');
		}
		/*if (com=='Delete'){
			if(confirm("Are you sure, you want to delete selected Message?")){
					
				j.post("message_communication.php", {"delete":"yes","id":items[0].id.substr(3)},function(data)
				{		 
				  if(data!="") {					 
					 j('#flex1').flexOptions().flexReload(); 
				  }else {
					  alert("Record doesn't exists.")
				  }
				  
				});		
			}			
		}*/	
	}
}
function showMessage(){
j("#flex1").flexigrid({
	url: 'message.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Salesman Name', name : 'salesman_name', width : 100, sortable : true, align: 'left'},
		{display: 'Subject', name : 'subject', width : 100, sortable : true, align: 'left'},
		{display: 'Message', name : 'message', width : 200, sortable : true, align: 'left'},
		{display: 'Reply', name : 'reply', width : 200, sortable : true, align: 'left'},
		{display: 'Photo', name : 'send_date', width : 60, sortable : true, align: 'left'},
		{display: 'Send Date', name : 'send_date', width : 60, sortable : true, align: 'left'},
		{display: 'Send Time', name : 'send_time', width : 60, sortable : true, align: 'left'}
		],
	buttons : [
		{name: 'Send Message', bclass: 'sendmsg', onpress : eventHandlerMessageOld},
		{separator: true},		
		{name: 'View', bclass: 'viewmsg', onpress : eventHandlerMessageOld},
		{separator: true},
		{name: 'Delete', bclass: 'delete', onpress : eventHandlerMessageOld},
		{separator: true}
		
		],
	searchitems : [
		{display: 'Salesman Name', name : 'salesman_name', isdefault: true},
		{display: 'Subject', name : 'subject'},
		{display: 'Send Date', name : 'send_date'}
		
		],
	sortname: "message_id",
	sortorder: "desc",
	usepager: true,
	title: 'Message Broadcast',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 700,
	height: 'auto'
});  
}
function eventHandlerMessageOld(com,grid)
{
	if (com=='Send Message')	{
		location.href="send_message.php";		
	}else{
		if( j('.trSelected',grid).length<=0 ||  j('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=j('.trSelected',grid);		
		if (com=='View'){
			//location.href="view_message.php?id="+items[0].id.substr(3);	
			window.open("view_message.php?id="+items[0].id.substr(3), '_blank');
		}
		if (com=='Delete'){
			if(confirm("Are you sure, you want to delete selected Message?")){
					
				j.post("message.php", {"delete":"yes","id":items[0].id.substr(3)},function(data)
				{		 
				  if(data!="") {					 
					 j('#flex1').flexOptions().flexReload(); 
				  }else {
					  alert("Record doesn't exists.")
				  }
				  
				});		
			}			
		}	
	}
}

function showTroubleTickets(){
j("#flex1").flexigrid({
	url: 'trouble_tickets.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [
		{display: 'Date', name : 'trouble_tickets_date', width : 100, sortable : true, align: 'left'},
		{display: 'Salesman Name', name : 'salesman_name', width : 120, sortable : true, align: 'left'},
		{display: 'Message', name : 'message', width : 200, sortable : true, align: 'left'},
		{display: 'Reply', name : 'reply', width : 200, sortable : true, align: 'left'},
		{display: 'Reply Date', name : 'reply_date', width : 70, sortable : true, align: 'left'},
		{display: 'Status', name : 'status', width : 70, sortable : true, align: 'left'}
		],
	buttons : [	
		{name: 'View', bclass: 'viewtickets', onpress : eventHandlerTroubleTickets},
		{separator: true}
		
		],
	searchitems : [
		{display: 'Salesman Name', name : 'salesman_name', isdefault: true},
		
		],
	sortname: "trouble_tickets_date",
	sortorder: "desc",
	usepager: true,
	title: 'Trouble Tickets',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 'auto',
	height: 'auto'
});  
}
function eventHandlerTroubleTickets(com,grid)
{
	var searchParam_1 = j('input[name=q]').val();
	var searchParam_2 = j('select[name=qtype]').val();
	var searchParam_3 = j(".pcontrol").find("input").val();
	if (com=='Send Message')	{
		location.href="send_message.php";		
	}else{
		if( j('.trSelected',grid).length<=0 ||  j('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=j('.trSelected',grid);		
		if (com=='View'){	
			location.href="view_trouble_tickets.php?id="+items[0].id.substr(3)+"&searchParam_1="+searchParam_1+"&searchParam_2="+searchParam_2+"&searchParam_3="+searchParam_3;				
			//window.open("view_trouble_tickets.php?id="+items[0].id.substr(3), '_blank');
		}
		if (com=='Delete'){
			if(confirm("Are you sure, you want to delete selected Message?")){
					
				j.post("message.php", {"delete":"yes","id":items[0].id.substr(3)},function(data)
				{		 
				  if(data!="") {					 
					 j('#flex1').flexOptions().flexReload(); 
				  }else {
					  alert("Record doesn't exists.")
				  }
				  
				});		
			}			
		}	
	}
}
//



function showIncentive(){
j("#flex1").flexigrid({
	url: 'incentive.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Description', name : 'description', width : 150, sortable : true, align: 'left'},
		{display: 'Incentive Type', name : 'type_description', width : 100, sortable : true, align: 'left'},
		{display: 'Party Type', name : 'party_description', width : 100, sortable : true, align: 'left'},
		{display: 'Duration', name : 'dur_description', width : 100, sortable : true, align: 'left'},
		{display: 'Value', name : 'value', width : 100, sortable : true, align: 'left'},
		{display: 'Reward Amount', name : 'incentive_reward_amount', width : 100, sortable : true, align: 'left'},		
		//{display: 'Salesman Name', name : 'discount_type', width : 100, sortable : true, align: 'left'},
		{display: 'Start Date', name : 'start_date', width : 100, sortable : true, align: 'left'},
		{display: 'End Date', name : 'end_date', width : 100, sortable : true, align: 'left'},
		{display: 'Incentive Status', name : 'status', width : 100, sortable : true, align: 'left'},
		//{display: 'Salesman Status', name : 'status', width : 100, sortable : true, align: 'left'}
		],
	buttons : [
		{name: 'Add', bclass: 'add', onpress : eventHandlerIncentive},
		{separator: true},		
		{name: 'Edit', bclass: 'edit', onpress : eventHandlerIncentive},
		{separator: true},
		{name: 'Delete', bclass: 'delete', onpress : eventHandlerIncentive},
		{separator: true}
		
		],
	searchitems : [
		{display: 'Description', name : 'description', isdefault: true},
		{display: 'Incentive Type', name : 'type_description'},
		{display: 'Party Type', name : 'party_description'},
		{display: 'Duration', name : 'dur_description'}
		],
	sortname: "target_incentive_id",
	sortorder: "asc",
	usepager: true,
	title: 'Incentive',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 'auto',
	height: 'auto'
});  
}
function eventHandlerIncentive(com,grid)
{
	if (com=='Import')	{
		//location.href="import_item.php";	
		location.href="#";	
	}
	else if (com=='Export')	{
		location.href="export.inc.php?export_item";		
	}
	else if (com=='Add')	{
		location.href="add_incentive.php";		
	}else{
		if( j('.trSelected',grid).length<=0 ||  j('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=j('.trSelected',grid);		
		if (com=='Edit'){
			//location.href="editincentive.php?id="+items[0].id.substr(3);
			location.href="editincentive.php?id="+items[0].id.substr(3);		
		}
		if (com=='Delete'){
			if(confirm("Are you sure, you want to delete selected Incentive?")){
					
				j.post("incentive.php", {"delete":"yes","id":items[0].id.substr(3)},function(data)
				{		 
				  if(data!="") {					 
					 j('#flex1').flexOptions().flexReload(); 
				  }else {
					  alert("Record doesn't exists.")
				  }
				  
				});		
			}			
		}	
	}
}
function showQualitychecker(){
j("#flex1").flexigrid({
	url: 'qualitychecker.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Name', name : 'qchecker_name', width : 120, sortable : true, align: 'left'},
		{display: 'Username', name : 'username', width : 120, sortable : true, align: 'left'},
		{display: 'Email ID', name : 'email_id', width : 120, sortable : true, align: 'left'},
		{display: 'Phone Number', name : 'qchecker_phone_no', width : 120, sortable : true, align: 'left'},
		{display: 'Address', name : 'qchecker_address', width : 120, sortable : true, align: 'left'},
		{display: 'Start Date', name : 'start_date', width : 80, sortable : true, align: 'left'},
		{display: 'End Date', name : 'end_date', width : 80, sortable : true, align: 'left'},
		{display: 'Quality Checker Status', name : 'status', width : 80, sortable : true, align: 'center'},
		{display: 'Login Status', name : 'loginStatus', width : 80, sortable : true, align: 'center'}
			
		],
	buttons : [
		{name: 'Add', bclass: 'add', onpress : eventHandlerQcheckAdminUser},
		{separator: true},		
		{name: 'Edit', bclass: 'edit', onpress : eventHandlerQcheckAdminUser},
		{separator: true}
		],
	searchitems : [
		{display: 'Name', name : 'qchecker_name', isdefault: true},
		{display: 'Username', name : 'username'},
		{display: 'Email ID', name : 'email_id'},
		{display: 'Phone Number', name : 'qchecker_phone_no'},
		{display: 'Address', name : 'qchecker_address'}
		],
	sortname: "qchecker_name",
	sortorder: "asc",
	usepager: true,
	title: 'Quality Auditor',
	useRp: true,
	rp: 15,
	showTableToggleBtn: true,
	width: 700,
	height: 375
});  
}

function eventHandlerQcheckAdminUser(com,grid)
{
	if (com=='Add')	{
		location.href="qualitychecker.php?add";	
	}else if (com=='Import')	{
		location.href="qualitychecker.php?import";	
	}else if (com=='Export')	{
		location.href="export.inc.php?export_qualitychecker_list";	
	}
	else{
		if( j('.trSelected',grid).length<=0 ||  j('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=j('.trSelected',grid);		
		if (com=='Edit'){
			location.href="qualitychecker.php?id="+items[0].id.substr(3);			
		}
	}			
}
function showLoginqualityAuditor(){
	j("#flex1").flexigrid({
		url: 'qauditor_list.php?show=yes',
		dataType: 'json',
		method : "GET", 
		singleSelect:true, 
		colModel : [		
			{display: 'Quality Auditor', name : 'qchecker_name', width : 130, sortable : true, align: 'left'},
			{display: 'Username', name : 'username', width : 100, sortable : true, align: 'left'},
			{display: 'Email ID', name : 'email_id', width : 150, sortable : true, align: 'left'},
			{display: 'Phone Number', name : 'qchecker_phone_no', width : 90, sortable : true, align: 'left'},
			{display: 'SESSIONID', name : 'session_id', width : 120, sortable : true, align: 'left'},
			{display: 'Login Status', name : '', width : 80, sortable : true, align: 'left'},
			{display: '', name : '', width : 50, sortable : true, align: 'left'}
				
			],
		searchitems : [
			{display: 'Quality Auditor', name : 'qchecker_name', isdefault: true},
			{display: 'Username', name : 'username'},
			{display: 'Email ID', name : 'email_id'},
			{display: 'Phone Number', name : 'qchecker_phone_no'}
			],
		sortname: "qchecker_name",
		sortorder: "asc",
		usepager: true,
		title: 'Quality Auditor List',
		useRp: true,
		rp: 15,
		showTableToggleBtn: true,
		width: 700,
		height: 375
	});  
	}
	
	
function showSalesReturn(){
j("#flex1").flexigrid({
	url: 'salesreturns.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [
		{display: 'Salesman', name : 'salesman_name', width : 100, sortable : true, align: 'left'},
		{display: 'Salesman Code', name : 'salesman_code', width : 100, sortable : true, align: 'left'},
		{display: 'Distributor Name', name : 'distributor_name', width : 100, sortable : true, align: 'left'},
		{display: 'Distributor Code', name : 'distributor_code', width : 100, sortable : true, align: 'left'},
		{display: 'Dealer Name', name : 'retailer_name', width : 100, sortable : true, align: 'left'},
		{display: 'Dealer Code', name : 'retailer_code', width : 100, sortable : true, align: 'left'},
		//{display: 'Order Value', name : 'order_id', width : 70, sortable : true, align: 'left'},
		{display: 'No. of items', name : 'total_item', width : 80, sortable : true, align: 'left'},		
		{display: 'Map', name : 'lat', width : 80, sortable : true, align: 'left'},
		{display: 'Date', name : 'date_of_order', width : 100, sortable : true, align: 'left'},
		{display: 'Time', name : 'time_of_order', width : 100, sortable : true, align: 'left'}
		//{display: 'Status', name : 'order_status', width : 70, sortable : true, align: 'left'},
		//{display: 'Comments', name : 'comments', width : 100, sortable : true, align: 'left'}
		],
	buttons : [		
		{name: 'View Details', bclass: 'viewdetails', onpress : eventHandlerSalesReturnList},
		{separator: true}
		],
	searchitems : [
		//{display: 'Date of Order', name : 'Date of Order', isdefault: true},
		{display: 'Salesman', name : 'salesman_name', isdefault: true},		
		{display: 'Distributor', name : 'distributor_name'},
		{display: 'Dealer', name : 'retailer_name'}
		],
	sortname: "date_of_order",
	sortorder: "desc",
	usepager: true,
	title: 'Sales Return List',
	useRp: true,
	rp: 20,
	showTableToggleBtn: true,
	width: 'auto',
	height: 'auto'
});  
}
function eventHandlerSalesReturnList(com,grid)
{	
	if (com=='Add')	{
		location.href="#";	
	}
	else{
		if( j('.trSelected',grid).length<=0 ||  j('.trSelected',grid).length>1){
			alert("Please.");
			return false;
		}
		var items=j('.trSelected',grid);			
		
		
		if (com=='View Details'){
			var searchParam_1 = j('input[name=q]').val();
			var searchParam_2 = j('select[name=qtype]').val();
			
          location.href="salesreturns.php?id="+items[0].id.substr(3)+"&searchParam_1="+searchParam_1+"&searchParam_2="+searchParam_2;	
			
		}
		
		else if (com=='Create Login')	{
		location.href="#.php?login&id="+items[0].id.substr(3);
	}
			
	}
}


function showTransaction(){
j("#flex1").flexigrid({
	url: 'transaction_list.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [
		{display: 'Salesman', name : 'salesman_name', width : 100, sortable : true, align: 'left'},
		{display: 'Salesman Code', name : 'salesman_code', width : 100, sortable : true, align: 'left'},
		{display: 'Distributor Name', name : 'distributor_name', width : 100, sortable : true, align: 'left'},
		/*{display: 'Distributor Code', name : 'distributor_code', width : 100, sortable : true, align: 'left'},*/
		{display: 'Dealer Name', name : 'retailer_name', width : 100, sortable : true, align: 'left'},
		{display: 'Dealer Code', name : 'retailer_code', width : 100, sortable : true, align: 'left'},
		{display: 'District', name : 'city_name', width : 80, sortable : true, align: 'left'},
		{display: 'State', name : 'state_name', width : 80, sortable : true, align: 'left'},
		{display: 'Transaction Type', name : 'transaction_type', width : 100, sortable : true, align: 'left'},
		{display: 'Bank', name : 'issuing_bank', width : 100, sortable : true, align: 'left'},
		//{display: 'Order Value', name : 'order_id', width : 70, sortable : true, align: 'left'},
		{display: 'Amount', name : 'total_sale_amount', width : 80, sortable : true, align: 'left'},
		{display: 'Outstanding Amount', name : 'outstanding_amount', width : 80, sortable : true, align: 'left'},		
		//{display: 'Map', name : 'lat', width : 80, sortable : true, align: 'left'},
		{display: 'Date', name : 'transaction_date', width : 100, sortable : true, align: 'left'},
		{display: 'Time', name : 'transaction_time', width : 100, sortable : true, align: 'left'}
		//{display: 'Status', name : 'order_status', width : 70, sortable : true, align: 'left'},
		//{display: 'Comments', name : 'comments', width : 100, sortable : true, align: 'left'}
		],
	buttons : [		
		{name: 'View Details', bclass: 'viewdetails', onpress : eventHandlerTransactionList},
		{separator: true}
		],
	searchitems : [
		//{display: 'Date of Order', name : 'Date of Order', isdefault: true},
		{display: 'Dealer', name : 'retailer_name', isdefault: true}	
		//{display: 'Distributor', name : 'distributor_name'},
		//{display: 'Retailer', name : 'retailer_name'}
		],
	sortname: "transaction_date",
	sortorder: "desc",
	usepager: true,
	title: 'Transaction List',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: '900',
	height: 'auto'
});  
}
function eventHandlerTransactionList(com,grid)
{

	
	if (com=='Add')	{
		location.href="#";	
	}
	else{ 
		if( j('.trSelected',grid).length<=0 ||  j('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=j('.trSelected',grid);	
				
		
		
		if (com=='View Details'){
			var searchParam_1 = j('input[name=q]').val();
			var searchParam_2 = j('select[name=qtype]').val();
			
          location.href="transaction_list.php?id="+items[0].id.substr(3)+"&searchParam_1="+searchParam_1+"&searchParam_2="+searchParam_2;	
			
		}else if (com=='Create Login')	{
		location.href="#.php?login&id="+items[0].id.substr(3);
	}
			
	}
}



// Show Product Wise Report List


function showproductwiselist(){
j("#flex1").flexigrid({
	url: 'productwise_report.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [
		{display: 'Category', name : 'category_name', width : 160, sortable : true, align: 'left'},
		{display: 'Salesman Name', name : 'salesman_name', width : 160, sortable : true, align: 'left'},
		{display: 'Salesman Code', name : 'salesman_code', width : 160, sortable : true, align: 'left'},
		{display: 'Item', name : 'item_name', width : 100, sortable : true, align: 'left'},
		{display: 'Item Code', name : 'item_code', width : 100, sortable : true, align: 'left'},
		{display: 'Brand', name : 'brand_name', width : 100, sortable : true, align: 'left'},
		{display: 'Offer Type', name : 'offer_name', width : 100, sortable : true, align: 'left'},
		{display: 'ERP Code', name : 'item_erp_code', width : 100, sortable : true, align: 'left'},
		{display: 'No. of orders', name : '', width : 70, sortable : true, align: 'left'},
		{display: 'Quantity', name : '', width : 70, sortable : true, align: 'left'},
		/*{display: 'Price', name : 'item_mrp', width : 100, sortable : true, align: 'left'},*/
		{display: 'Total', name : '', width : 100, sortable : true, align: 'left'},
		//{display: 'Date', name : 'date_of_order', width : 120, sortable : true, align: 'left'},
		//{display: 'Time', name : 'created', width : 120, sortable : true, align: 'left'}
		//{display: 'Order Status', name : 'order_status', width : 100, sortable : true, align: 'left'}
		//{display: 'Comments', name : 'comments', width : 80, sortable : true, align: 'left'}
		],
	buttons : [		
		{name: 'View Product Detail', bclass: 'viewprodetails', onpress : eventHandlerProductWiseOrderList},
		{separator: true}
		],
	searchitems : [
		{display: 'Item', name : 'item_name', isdefault: true},
		{display: 'Category', name : 'category_name'},
		{display: 'Brand', name : 'brand_name'},
		{display: 'Offer Type', name : 'offer_name'},
		{display: 'ERP Code', name : 'item_erp_code'}
		],
	sortname: "date_of_order",
	sortorder: "desc",
	usepager: true,
	title: 'Product List',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: '980',
	height: 'auto'
});  
}
function eventHandlerProductWiseOrderList(com,grid)
{

	
	if (com=='Add')	{
		location.href="#";	
	}
	else{
		if( j('.trSelected',grid).length<=0 ||  j('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=j('.trSelected',grid);		
		if (com=='View Product Detail'){
			var searchParam_1 = j('input[name=q]').val();
			var searchParam_2 = j('select[name=qtype]').val();			
			location.href="productwise_order_list.php?id="+items[0].id.substr(3)+"&searchParam_1="+searchParam_1+"&searchParam_2="+searchParam_2;	
			
		}else if (com=='Create Login')	{
		location.href="#.php?login&id="+items[0].id.substr(3);
	}
			
	}
}



// End of Product Wise Report List

// Show Product Report List


function showproductlist(){
j("#flex1").flexigrid({
	url: 'product_report.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [
		{display: 'Distributor Name', name : 'distributor_name', width : 80, sortable : true, align: 'left'},
		{display: 'Distributor Code', name : 'distributor_code', width : 80, sortable : true, align: 'left'},
		{display: 'Dealer Name', name : 'retailer_name', width : 70, sortable : true, align: 'left'},
		{display: 'Dealer Code', name : 'retailer_code', width : 70, sortable : true, align: 'left'},
		{display: 'State', name : 'state_name', width : 70, sortable : true, align: 'left'},
		{display: 'District', name : 'city_name', width : 70, sortable : true, align: 'left'},
		{display: 'Taluka', name : 'taluka_name', width : 70, sortable : true, align: 'left'},
		{display: 'Market', name : 'market_name', width : 70, sortable : true, align: 'left'},		
		{display: 'Salesman Name', name : 'salesman_name', width : 80, sortable : true, align: 'left'},
		{display: 'Salesman Code', name : 'salesman_code', width : 80, sortable : true, align: 'left'},
		{display: 'Division Name', name : 'division_name', width : 80, sortable : true, align: 'left'},
		{display: 'Designation', name : 'des1', width : 70, sortable : true, align: 'left'},
		{display: 'Category Name', name : 'category_name', width : 80, sortable : true, align: 'left'},
		{display: 'Item Name', name : 'item_name', width : 70, sortable : true, align: 'left'},
		{display: 'Item Code', name : 'item_code', width : 50, sortable : true, align: 'left'},
		{display: 'Brand Name', name : 'brand_name', width : 70, sortable : true, align: 'left'},
		{display: 'Offer', name : 'offer_name', width : 50, sortable : true, align: 'left'},
		{display: 'Item ERP Code', name : 'item_erp_code', width : 70, sortable : true, align: 'left'},
		{display: 'Price', name : 'price', width : 50, sortable : true, align: 'left'},
		{display: 'Quantity', name : 'quantity', width : 50, sortable : true, align: 'left'},
		{display: 'Total', name : 'ttlprice', width : 50, sortable : true, align: 'left'},
		{display: 'Date of Order', name : 'date_of_order', width : 70, sortable : true, align: 'left'},
		{display: 'Customer Type', name : 'new', width : 70, sortable : true, align: 'left'}
		
		],
	buttons : [		
		{separator: true}
		],
	searchitems : [
		{display: 'Item', name : 'item_name', isdefault: true},
		{display: 'Category', name : 'category_name'},
		{display: 'Dealer Name', name : 'retailer_name'},
		{display: 'Dealer Channel', name : 'channel_name'},
		{display: 'Brand', name : 'brand_name'},
		{display: 'Offer', name : 'offer_name'},
		{display: 'Item ERP Code', name : 'item_erp_code'}

		],
	sortname: "date_of_order",
	sortorder: "desc",
	usepager: true,
	title: 'Product List',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: '900',
	height: 'auto'
});  
}




// End of Product Report List







function showRetailerStockByDate(){
j("#flex1").flexigrid({
	url: 'RetOpeningStockByDate.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Salesmen', name : 'salesman_name', width : 120, sortable : true, align: 'left'},
		{display: 'Salesmen Code', name : 'salesman_code', width : 120, sortable : true, align: 'left'},
		{display: 'Dealer Name', name : 'retailer_name', width : 120, sortable : true, align: 'left'},
		{display: 'Dealer Code', name : 'retailer_code', width : 120, sortable : true, align: 'left'},
		{display: 'Dealer Channel', name : 'channel_name', width : 120, sortable : true, align: 'left'},
		{display: 'Dealer Display Outlet', name : 'display_outlet', width : 120, sortable : true, align: 'left'},
		{display: 'Category', name : 'category_name', width : 120, sortable : true, align: 'left'},
		{display: 'Item', name : 'item_name', width : 180, sortable : true, align: 'left'},
		{display: 'Item Code', name : 'item_code', width : 100, sortable : true, align: 'left'},
		{display: 'Stock Value', name : 'opening_stock_quantity', width : 70, sortable : true, align: 'left'},
		{display: 'Date', name : 'date_of_order', width : 80, sortable : true, align: 'left'},
		{display: 'Time', name : 'time_of_order', width :60, sortable : true, align: 'left'},
		{display: 'Previous Closing Stock', name : '', width :120, sortable : true, align: 'left'},
		{display: 'Sale', name : '', width :60, sortable : true, align: 'left'}
		],

	buttons : [		
		//{name: 'View Detail', bclass: 'edit', onpress : eventHandlerTotalSale},
		//{separator: true},
		//{name: 'Print', bclass: 'edit', onpress : printTable},
		//{separator: true},
		/*{name: 'Export to excel', bclass: 'exportexcel', onpress :toExcel },
		{separator: true}*/
		],
	searchitems : [
		{display: 'Salesman', name : 'salesman_name', isdefault: true},
		{display: 'Dealer', name : 'retailer_name'},
		{display: 'Dealer Channel', name : 'channel_name'},
		{display: 'Dealer Display Outlet', name : 'display_outlet'},
		{display: 'Item', name : 'item_name'},
		{display: 'Category', name : 'category_name'},
		{display: 'Date', name : 'date_of_order'}
		],
	sortname: "salesman_name",
	sortorder: "asc",
	usepager: true,
	title: 'Opening Stock For Dealers',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: '980',
	height: 'auto'
});  
}

function showDistibutorTotalSale(){
j("#flex1").flexigrid({
	url: 'dis_montly_basis_sale.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	
	colModel : [
		{display: 'Dealer', name : 'retailer_name', width : 100, sortable : true, align: 'left'},
		{display: 'Dealer Code', name : 'retailer_code', width : 100, sortable : true, align: 'left'},
		{display: 'Dealer Channel', name : 'channel_name', width : 100, sortable : true, align: 'left'},
		{display: 'Distributor', name : 'distributor_name', width : 100, sortable : true, align: 'left'},
		{display: 'Distributor Code', name : 'distributor_code', width : 100, sortable : true, align: 'left'},
		//{display: 'Address2', name : 'retailer_address2', width : 70, sortable : true, align: 'left'},
		{display: 'Item', name : 'item_name', width : 150, sortable : true, align: 'left'},
		{display: 'Item Code', name : 'item_code', width : 150, sortable : true, align: 'left'},
		{display: 'Brand', name : 'brand_name', width : 150, sortable : true, align: 'left'},
		{display: 'Offer Type', name : 'offer_name', width : 150, sortable : true, align: 'left'},
        {display: 'Month', name : 'month', width : 50, sortable : true, align: 'center'},
		{display: 'Year', name : 'year', width : 50, sortable : true, align: 'center'},
		{display: 'Day 1', name : '', width : 50, sortable : true, align: 'left'},
		{display: 'Day 2', name : '', width : 50, sortable : true, align: 'left'},
		{display: 'Day 3', name : '', width : 50, sortable : true, align: 'left'},
		{display: 'Day 4', name : '', width : 50, sortable : true, align: 'left'},
		{display: 'Day 5', name : '', width : 50, sortable : true, align: 'left'},
		{display: 'Day 6', name : '', width : 50, sortable : true, align: 'left'},
		{display: 'Day 7', name : '', width : 50, sortable : true, align: 'left'},
		{display: 'Day 8', name : '', width : 50, sortable : true, align: 'left'},
		{display: 'Day 9', name : '', width : 50, sortable : true, align: 'left'},
		{display: 'Day 10', name : '', width : 50, sortable : true, align: 'left'},
		{display: 'Day 11', name : '', width : 50, sortable : true, align: 'left'},
		{display: 'Day 12', name : '', width : 50, sortable : true, align: 'left'},
		{display: 'Day 13', name : '', width : 50, sortable : true, align: 'left'},
		{display: 'Day 14', name : '', width : 50, sortable : true, align: 'left'},
		{display: 'Day 15', name : '', width : 50, sortable : true, align: 'left'},
		{display: 'Day 16', name : '', width : 50, sortable : true, align: 'left'},
		{display: 'Day 17', name : '', width : 50, sortable : true, align: 'left'},
		{display: 'Day 18', name : '', width : 50, sortable : true, align: 'left'},
		{display: 'Day 19', name : '', width : 50, sortable : true, align: 'left'},
		{display: 'Day 20', name : '', width : 50, sortable : true, align: 'left'},
		{display: 'Day 21', name : '', width : 50, sortable : true, align: 'left'},
		{display: 'Day 22', name : '', width : 50, sortable : true, align: 'left'},
		{display: 'Day 23', name : '', width : 50, sortable : true, align: 'left'},
		{display: 'Day 24', name : '', width : 50, sortable : true, align: 'left'},
		{display: 'Day 25', name : '', width : 50, sortable : true, align: 'left'},
		{display: 'Day 26', name : '', width : 50, sortable : true, align: 'left'},
		{display: 'Day 27', name : '', width : 50, sortable : true, align: 'left'},
		{display: 'Day 28', name : '', width : 50, sortable : true, align: 'left'},
		{display: 'Day 29', name : '', width : 50, sortable : true, align: 'left'},
		{display: 'Day 30', name : '', width : 50, sortable : true, align: 'left'},
		{display: 'Day 31', name : '', width : 50, sortable : true, align: 'left'}
		],
	buttons : [		
		//{name: 'View Detail', bclass: 'edit', onpress : eventHandlerTotalSale},
		//{separator: true},
		//{name: 'Print', bclass: 'edit', onpress : printTable},
		//{separator: true},
		/*{name: 'Export to excel', bclass: 'exportexcel', onpress :toExcel },
		{separator: true}*/
		],
	searchitems : [
		{display: 'Dealer', name : 'retailer_name'},
		{display: 'Dealer Channel', name : 'channel_name'},
		{display: 'Item', name : 'item_name'},
		{display: 'Item Code', name : 'item_code'},
		{display: 'Brand', name : 'brand_name'},
		{display: 'Offer', name : 'offer_name'}

		],
	sortname: "month",
	sortorder: "ASC",
	usepager: true,
	title: 'Dealer Wise Item List',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 950,
	height: 'auto'
});  
}
function eventHandlerTotalSale(com,grid)
{

	
	if (com=='Add')	{
		location.href="#";	
	}
	else{
		if( j('.trSelected',grid).length<=0 ||  j('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=j('.trSelected',grid);	
		
		if (com=='View Target Detail'){
			var searchParam_1 = j('input[name=q]').val();
			var searchParam_2 = j('select[name=qtype]').val();
			location.href="salesmen_target_reports.php?id="+items[0].id.substr(3)+"&searchParam_1="+searchParam_1+"&searchParam_2="+searchParam_2;	
			
		}
			
	}
}



	


function showSalesmanTotalSale(){
j("#flex1").flexigrid({
	url: 'sale_montly_basis_sale.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	
	colModel : [
	    {display: 'Salesman', name : 's_name', width : 100, sortable : true, align: 'left'},
	    {display: 'Salesman Code', name : 'salesman_code', width : 100, sortable : true, align: 'left'},
		{display: 'Dealer', name : 'retailer_name', width : 100, sortable : true, align: 'left'},
		{display: 'Dealer Code', name : 'retailer_code', width : 100, sortable : true, align: 'left'},
		{display: 'City', name : 'market_name', width : 100, sortable : true, align: 'left'},
		{display: 'District', name : 'city_name', width : 100, sortable : true, align: 'left'},
		{display: 'State', name : 'state_name', width : 100, sortable : true, align: 'left'},
		{display: 'Distributor Name', name : 'distributor_name', width : 100, sortable : true, align: 'left'},
		{display: 'Distributor Code', name : 'distributor_code', width : 100, sortable : true, align: 'left'},
		// {display: 'Dealer Channel', name : 'channel_name', width : 100, sortable : true, align: 'left'},
		{display: 'Item', name :'item_name', width : 100, sortable : true, align: 'left'},
		{display: 'Brand', name : 'brand_name', width : 100, sortable : true, align: 'left'},
		{display: 'Offer', name : 'offer_name', width : 150, sortable : true, align: 'left'},
		{display: 'Month', name : 'month', width : 50, sortable : true, align: 'center'},
		{display: 'Year', name : 'year', width : 50, sortable : true, align: 'center'},
		{display: 'Day 1', name : '', width : 50, sortable : true, align: 'left'},
		{display: 'Day 2', name : '', width : 50, sortable : true, align: 'left'},
		{display: 'Day 3', name : '', width : 50, sortable : true, align: 'left'},
		{display: 'Day 4', name : '', width : 50, sortable : true, align: 'left'},
		{display: 'Day 5', name : '', width : 50, sortable : true, align: 'left'},
		{display: 'Day 6', name : '', width : 50, sortable : true, align: 'left'},
		{display: 'Day 7', name : '', width : 50, sortable : true, align: 'left'},
		{display: 'Day 8', name : '', width : 50, sortable : true, align: 'left'},
		{display: 'Day 9', name : '', width : 50, sortable : true, align: 'left'},
		{display: 'Day 10', name : '', width : 50, sortable : true, align: 'left'},
		{display: 'Day 11', name : '', width : 50, sortable : true, align: 'left'},
		{display: 'Day 12', name : '', width : 50, sortable : true, align: 'left'},
		{display: 'Day 13', name : '', width : 50, sortable : true, align: 'left'},
		{display: 'Day 14', name : '', width : 50, sortable : true, align: 'left'},
		{display: 'Day 15', name : '', width : 50, sortable : true, align: 'left'},
		{display: 'Day 16', name : '', width : 50, sortable : true, align: 'left'},
		{display: 'Day 17', name : '', width : 50, sortable : true, align: 'left'},
		{display: 'Day 18', name : '', width : 50, sortable : true, align: 'left'},
		{display: 'Day 19', name : '', width : 50, sortable : true, align: 'left'},
		{display: 'Day 20', name : '', width : 50, sortable : true, align: 'left'},
		{display: 'Day 21', name : '', width : 50, sortable : true, align: 'left'},
		{display: 'Day 22', name : '', width : 50, sortable : true, align: 'left'},
		{display: 'Day 23', name : '', width : 50, sortable : true, align: 'left'},
		{display: 'Day 24', name : '', width : 50, sortable : true, align: 'left'},
		{display: 'Day 25', name : '', width : 50, sortable : true, align: 'left'},
		{display: 'Day 26', name : '', width : 50, sortable : true, align: 'left'},
		{display: 'Day 27', name : '', width : 50, sortable : true, align: 'left'},
		{display: 'Day 28', name : '', width : 50, sortable : true, align: 'left'},
		{display: 'Day 29', name : '', width : 50, sortable : true, align: 'left'},
		{display: 'Day 30', name : '', width : 50, sortable : true, align: 'left'},
		{display: 'Day 31', name : '', width : 50, sortable : true, align: 'left'},
		{display: 'Total', name : 'total', width : 50, sortable : true, align: 'left'}
		],
	buttons : [		
		
		
		],
	searchitems : [
		{display: 'Salesman', name : 'V.salesman_name'},
		{display: 'Dealer', name : 'V.retailer_name'},
		{display: 'Dealer Channel', name : 'cm.channel_name'},
        {display: 'Item', name : 'i.item_name'},
        {display: 'Brand', name : 'B.brand_name'}
		],
	sortname: "month",
	sortorder: "ASC",
	usepager: true,
	title: 'Salesman Dealer Wise Item List',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 950,
	height: 'auto'
});  
}

// Show target details


function showTarget(){
j("#flex1").flexigrid({
	url: 'target.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Description', name : 'description', width : 150, sortable : true, align: 'left'},
		{display: 'Target Type', name : 'type_description', width : 100, sortable : true, align: 'left'},
		{display: 'Party Type', name : 'party_description', width : 100, sortable : true, align: 'left'},
		{display: 'Duration', name : 'dur_description', width : 100, sortable : true, align: 'left'},
		{display: 'Value', name : 'value', width : 100, sortable : true, align: 'left'},
		//{display: 'Reward Amount', name : 'incentive_reward_amount', width : 100, sortable : true, align: 'left'},
		//{display: 'Salesman Name', name : 'salesman_name', width : 150, sortable : true, align: 'left'},
		//{display: 'Salesman Code', name : 'salesman_code', width : 100, sortable : true, align: 'left'},		
		//{display: 'Salesman Name', name : 'discount_type', width : 100, sortable : true, align: 'left'},
		{display: 'Start Date', name : 'start_date', width : 100, sortable : true, align: 'left'},
		{display: 'End Date', name : 'end_date', width : 100, sortable : true, align: 'left'},
		{display: 'Target Status', name : 'status', width : 100, sortable : true, align: 'left'},
		//{display: 'Salesman Status', name : 'status', width : 100, sortable : true, align: 'left'}
		],
	buttons : [
		{name: 'Add', bclass: 'add', onpress : eventHandlerTarget},
		{separator: true},		
		{name: 'Edit', bclass: 'edit', onpress : eventHandlerTarget},
		{separator: true},
		{name: 'Delete', bclass: 'delete', onpress : eventHandlerTarget},
		{separator: true}
		
		],
	searchitems : [
		{display: 'Description', name : 'description', isdefault: true},
		{display: 'Target Type', name : 'type_description'},
		{display: 'Party Type', name : 'party_description'},
		{display: 'Duration', name : 'dur_description'}
		],
	sortname: "target_incentive_id",
	sortorder: "asc",
	usepager: true,
	title: 'Target',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: '800',
	height: 'auto'
});  
}
function eventHandlerTarget(com,grid)
{
	if (com=='Import')	{
		//location.href="import_item.php";	
		location.href="#";	
	}
	else if (com=='Export')	{
		location.href="export.inc.php?export_item";		
	}
	else if (com=='Add')	{
		location.href="add_target.php";		
	}else{
		if( j('.trSelected',grid).length<=0 ||  j('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=j('.trSelected',grid);		
		if (com=='Edit'){
			location.href="edit_target.php?id="+items[0].id.substr(3);		
		}
		if (com=='Delete'){
			if(confirm("Are you sure, you want to delete selected Target?")){
					
				j.post("target.php", {"delete":"yes","id":items[0].id.substr(3)},function(data)
				{		 
				  if(data!="") {					 
					 j('#flex1').flexOptions().flexReload(); 
				  }else {
					  alert("Record doesn't exists.")
				  }
				  
				});		
			}			
		}	
	}
}


function showSmsSchedule(){
j("#flex1").flexigrid({
	url: 'sms_schedule_list.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Description', name : 'discount_desc', width : 150, sortable : true, align: 'left'},
		{display: 'SMS Repeat', name : 'repeats', width : 100, sortable : true, align: 'left'},
		{display: 'SMS Sent', name : 'sent_nature', width : 100, sortable : true, align: 'left'},
		{display: 'Sent on', name : 'weekday', width : 100, sortable : true, align: 'left'},
		{display: 'Party Type', name : 'party_type', width : 100, sortable : true, align: 'left'},
		{display: 'Scheme Type', name : 'discount_type', width : 100, sortable : true, align: 'left'},
		{display: 'Start Date', name : 'from_date', width : 100, sortable : true, align: 'left'},
		{display: 'End Date', name : 'to_date', width : 100, sortable : true, align: 'left'},
		{display: 'Snooze', name : 'status', width : 50, sortable : true, align: 'left'}
		],
	buttons : [
		{name: 'Edit', bclass: 'edit', onpress : eventHandlerSmsSchedule},
		{separator: true},
		{name: 'Delete', bclass: 'delete', onpress : eventHandlerSmsSchedule},
		{separator: true}
		
		],
	searchitems : [
		{display: 'Description', name : 'discount_desc', isdefault: true}
		
		],
	sortname: "discount_id",
	sortorder: "asc",
	usepager: true,
	title: 'SMS Schedule',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 'auto',
	height: 'auto'
});  
}
function eventHandlerSmsSchedule(com,grid)
{
	
	if (com=='Export')	{
		location.href="export.inc.php?export_item";		
	}
	if (com=='Edit'){
			//alert(items[0].sid);
			var id = items[0].id.substr(3);
			//alert(id);
			var ids = btoa(id);
			//alert(atob(ids));

			location.href="sms_schedule.php?sid="+ids;			
		}
	
	if( j('.trSelected',grid).length<=0 ||  j('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=j('.trSelected',grid);		
		
		if (com=='Delete'){
			if(confirm("Are you sure, you want to delete selected Scheme?")){
					
				j.post("sms_schedule_list.php", {"delete":"yes","schedule_id":items[0].sid},function(data)
				{		 
				  if(data!="") {					 
					 j('#flex1').flexOptions().flexReload(); 
				  }else {
					  alert("Record doesn't exists.")
				  }
				  
				});		
			}			
		}	
}

/*************************** Access Management ********************************/

function showManageAccount(){
j("#flex1").flexigrid({
	url: 'manage_account.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Company Name', name : 'company_name', width : 150, sortable : true, align: 'left'},
		{display: 'Company Address', name : 'company_address', width : 100, sortable : true, align: 'left'},
		{display: 'Phone Number', name : 'company_phone_no', width : 100, sortable : true, align: 'left'},
		{display: 'Number Of Users', name : 'no_of_employees', width : 100, sortable : true, align: 'left'},
		{display: 'Start Date', name : 'start_date', width : 100, sortable : true, align: 'left'},
		{display: 'End Date', name : 'end_date', width : 100, sortable : true, align: 'left'},
		{display: 'Status', name : 'status', width : 50, sortable : true, align: 'left'}
		],
	buttons : [
		{name: 'Add', bclass: 'add', onpress : eventHandlerAccountManagement},
		{separator: true},
		{name: 'Edit', bclass: 'edit', onpress : eventHandlerAccountManagement},
		{separator: true},
		/*{name: 'Delete', bclass: 'delete', onpress : eventHandlerAccountManagement},
		{separator: true},*/
		{name: 'Manage Features', bclass: 'feature', onpress : eventHandlerAccountManagement},
		{separator: true}
		
		],
	searchitems : [
		{display: 'Company Name', name : 'company_name', isdefault: true}
		
		],
	sortname: "account_id",
	sortorder: "asc",
	usepager: true,
	title: 'Company Accounts',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 'auto',
	height: 'auto'
});  
}
function eventHandlerAccountManagement(com,grid)
{
	
 if (com=='Add')	{
		location.href="add_company_account.php";		
	}
	else{
	if( j('.trSelected',grid).length<=0 ||  j('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=j('.trSelected',grid);}
		if (com=='Edit'){
			//alert(items[0].sid);
			var id = items[0].id.substr(3);
			//alert(id);
			var ids = btoa(id);
			//alert(atob(ids));

			location.href="edit_company_account.php?id="+items[0].id.substr(3);				
		}
		if (com=='Manage Features'){
			var id = items[0].id.substr(3);
			var ids = btoa(id);
			location.href="account_navigation.php?id="+items[0].id.substr(3);				
		}
		
	 if (com=='Delete'){
			if(confirm("Are you sure, you want to delete selected Scheme?")){
					
				j.post("sms_schedule_list.php", {"delete":"yes","schedule_id":items[0].sid},function(data)
				{		 
				  if(data!="") {					 
					 j('#flex1').flexOptions().flexReload(); 
				  }else {
					  alert("Record doesn't exists.")
				  }
				  
				});		
			}			
		}
}
function showcms(){
j("#flex1").flexigrid({
	url: 'cms.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Name', name : 'page_alias_name', width : 200, sortable : true, align: 'left'},
		{display: 'Php Page', name : 'page_name', width : 200, sortable : true, align: 'left'},
		{display: 'Title', name : 'title', width : 180, sortable : true, align: 'left'},
		{display: 'Display Order', name : 'ordID', width : 80, sortable : true, align: 'left'},
		{display: 'Delete', name : 'del', width : 100, sortable : true, align: 'left'},
		{display: 'Status', name : 'status', width : 30, sortable : true, align: 'left'}
		],
	buttons : [
		{name: 'Add', bclass: 'add', onpress : eventHandlerCMS},
		{separator: true},		
		{name: 'Edit', bclass: 'edit', onpress : eventHandlerCMS},
		{separator: true}
		
		],
	searchitems : [
		{display: 'Name', name : 'page_alias_name', isdefault: true},
		{display: 'Status', name : 'status'}
		
		],
	sortname: "page_alias_name",
	sortorder: "asc",
	usepager: true,
	title: 'CMS',
	useRp: true,
	rp: 15,
	showTableToggleBtn: true,
	width: 'auto',
	height: 'auto'
});  
}
function eventHandlerCMS(com,grid)
{
	if (com=='Add')	{
		location.href="addcms.php";		
	}else{
		if( j('.trSelected',grid).length<=0 ||  j('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=j('.trSelected',grid);
		
		if (com=='Edit'){
			location.href="addcms.php?id="+items[0].id.substr(3);			
		}
		
		if (com=='Delete'){
			if(confirm("Are you sure, you want to delete selected Record?")){
					
				j.post("cms.php", {"delete":"yes","id":items[0].id.substr(3)},function(data)
				{		 
				  if(data!="") {					 
					 j('#flex1').flexOptions().flexReload(); 
				  }else {
					  alert("Record doesn't exists.")
				  }
				  
				});		
			}			
		}	
	}
}
function showNavigationList(){
j("#flex1").flexigrid({
	url: 'user_navigation_list.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Company Name', name : 'company_name', width : 150, sortable : true, align: 'left'},
		{display: 'User Type', name : 'type', width : 100, sortable : true, align: 'left'},
		{display: 'User Name', name : 'username', width : 100, sortable : true, align: 'left'},
		{display: 'Previlages', name : 'page_privileges', width : 500, sortable : true, align: 'left'}
		],
	buttons : [
		{name: 'Add', bclass: 'add', onpress : eventHandlerNavigationList},
		{separator: true},
		{name: 'Edit', bclass: 'edit', onpress : eventHandlerNavigationList},
		{separator: true}		
		],
	searchitems : [
		{display: 'Company Name', name : 'company_name', isdefault: true}
		
		],
	sortname: "nav_privilege_id",
	sortorder: "asc",
	usepager: true,
	title: 'User Navigation',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 'auto',
	height: 'auto'
});  
}
function eventHandlerNavigationList(com,grid)
{
	
 if (com=='Add')	{
		location.href="manage_navigation.php";		
	}
	else{
	if( j('.trSelected',grid).length<=0 ||  j('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=j('.trSelected',grid);}
		if (com=='Edit'){
			//alert(items[0].sid);
			var id = items[0].id.substr(3);
			//alert(id);
			var ids = btoa(id);
			//alert(atob(ids));

			location.href="manage_navigation.php?id="+items[0].id.substr(3);				
		}
}

/*************************** End Of Access Management ********************************/

/*********************************** Hierarchy Management ****************************************/

function showhierarchy(){
	
	j("#flex1").flexigrid({
		url: 'hierarchy.php?show=yes',
		dataType: 'json',
		method : "GET", 
		singleSelect:true, 
		colModel : [		
			{display: 'Hierarchy', name : 'a.description', width : 270, sortable : true, align: 'left'},
			//{display: 'Parent', name : 'parent_id', width : 240, sortable : true, align: 'left'},
			{display: 'Date', name : 'created', width : 120, sortable : true, align: 'left'},		
			{display: 'Status', name : 'status', width : 50, sortable : true, align: 'left'}
			],
		buttons : [
			{name: 'Add', bclass: 'add', onpress : eventHandlerhierarchy},
			{separator: true},		
			{name: 'Edit', bclass: 'edit', onpress : eventHandlerhierarchy},
			{separator: true},	
			{name: 'Redefine Level', bclass: 'level', onpress : eventHandlerhierarchy}
			
			],
		searchitems : [
			{display: 'hierarchy', name : 'a.description', isdefault: true}],
		sortname: "a.description",
		sortorder: "asc",
		usepager: true,
		title: 'Hierarchy',
		useRp: true,
		rp: 15,
		showTableToggleBtn: true,
		width: "900",
		height: "auto"
	});  
}



function eventHandlerhierarchy(com,grid) {
	
	if (com=='Import')	{
		//location.href="import_item.php";	
		location.href="#";	
	}
	
		if (com=='Redefine Level')
		{
			//alert('HEllo');
			//alert("addhierarchy.php?id="+items[0].id.substr(3));
			location.href="hierarchy_ordering.php";	
			
		}
	
	
	
	
	else if (com=='Export')	{
		location.href="export.inc.php?export_hierarchy";		
	}
	else if (com=='Add')	{
		location.href="hierarchy_add.php";		
	}else{
		if( j('.trSelected',grid).length<=0 ||  j('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=j('.trSelected',grid);		
		if (com=='Edit')
		{
			//alert('HEllo');
			//alert("addhierarchy.php?id="+items[0].id.substr(3));
			location.href="hierarchy_add.php?id="+items[0].id.substr(3);	
			
		}
		
		
		
		
		
		if (com=='Delete'){
			if(confirm("Are you sure, you want to delete selected data?")){
					
				j.post("hierarchy.php", {"delete":"yes","id":items[0].id.substr(3)},function(data)
				{		 
				  if(data!="") {					 
					 j('#flex1').flexOptions().flexReload(); 
				  }else {
					  alert("Record doesn't exists.")
				  }
				  
				});		
			}			
		}	
	}
}
	
/*********************************** End of Hierarchy Management ****************************************/



/******************************* Service Personnel Hierarchy Management ***********************************/

function showServicehierarchy(){
	
	j("#flex1").flexigrid({
		url: 'hierarchy_service_person.php?show=yes',
		dataType: 'json',
		method : "GET", 
		singleSelect:true, 
		colModel : [		
			{display: 'Hierarchy', name : 'a.description', width : 270, sortable : true, align: 'left'},
			{display: 'Date', name : 'created', width : 120, sortable : true, align: 'left'},		
			{display: 'Status', name : 'status', width : 50, sortable : true, align: 'left'}
			],
		buttons : [
			{name: 'Add', bclass: 'add', onpress : eventHandlerServicehierarchy},
			{separator: true},		
			{name: 'Edit', bclass: 'edit', onpress : eventHandlerServicehierarchy},
			{separator: true},
			{name: 'Redefine Level', bclass: 'level', onpress : eventHandlerServicehierarchy}
			],
		searchitems : [
			{display: 'hierarchy', name : 'a.description', isdefault: true}],
		sortname: "a.description",
		sortorder: "asc",
		usepager: true,
		title: 'Service Personnel Hierarchy',
		useRp: true,
		rp: 15,
		showTableToggleBtn: true,
		width: "900",
		height: "auto"
	});  
}



function eventHandlerServicehierarchy(com,grid) {
	
	if (com=='Import')	{
		//location.href="import_item.php";	
		location.href="#";	
	}
	if (com=='Redefine Level')
	{
		location.href="service_hierarchy_ordering.php";				
	}	
	else if (com=='Export')	{
		location.href="export.inc.php?export_hierarchy";		
	}
	else if (com=='Add')	{
		location.href="hierarchy_service_personnel_add.php";		
	}else{
		if( j('.trSelected',grid).length<=0 ||  j('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=j('.trSelected',grid);		
		if (com=='Edit')
		{
			location.href="hierarchy_service_personnel_add.php?id="+items[0].id.substr(3);	
			
		}
		
		// if (com=='Delete'){
		// 	if(confirm("Are you sure, you want to delete selected data?")){
					
		// 		j.post("hierarchy_service_person.php", {"delete":"yes","id":items[0].id.substr(3)},function(data)
		// 		{		 
		// 		  if(data!="") {					 
		// 			 j('#flex1').flexOptions().flexReload(); 
		// 		  }else {
		// 			  alert("Record doesn't exists.")
		// 		  }
				  
		// 		});		
		// 	}			
		// }	
	}
}
	
/******************************* End of Service Personnel Hierarchy Management *********************************/


/************************************  CITY Section *****************************************************/


function showCity(){
j("#flex1").flexigrid({
	url: 'city.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'District', name : 'city_name', width : 280, sortable : true, align: 'left'},
		{display: 'District Code', name : 'city_code', width : 150, sortable : true, align: 'left'},
		{display: 'State Name', name : 'state_name', width : 200, sortable : true, align: 'left'},
		{display:  'Zone', name : 'zone_name', width :100, sortable : true,align: 'left'},
		{display:  'Region', name : 'region_name', width :100, sortable : true,align: 'left'},
		{display:  'Country', name : 'country_name', width :150, sortable : true,align: 'left'},
		{display: 'District Status', name : 'status', width : 80, sortable : true, align: 'left'},
		],
	buttons : [
		{name: 'Add', bclass: 'add', onpress : eventHandlerCity},
		{separator: true},		
		{name: 'Edit', bclass: 'edit', onpress : eventHandlerCity},
		{separator: true},
		{name: 'Delete', bclass: 'delete', onpress : eventHandlerCity},
		{separator: true},			
		{name: 'Import', bclass: 'import', onpress : eventHandlerCity},
		{separator: true},
		{name: 'Export', bclass: 'export', onpress : eventHandlerCity},
		{separator: true}
		
		],
	searchitems : [
		{display: 'District', name : 'city_name', isdefault: true},
		{display: 'State', name : 'state_name'}
		
		],
	sortname: "city_name",
	sortorder: "asc",
	usepager: true,
	title: 'District',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 850,
	height: 'auto'
});  
}
function eventHandlerCity(com,grid)
{

	if (com=='Import')	{
		location.href="city_import.php";		
	}
	else if (com=='Export')	{
		location.href="export.inc.php?export_distric_new";		
	}
	else if (com=='Add')	{
		location.href="add_city.php";		
	}else{
		if( j('.trSelected',grid).length<=0 ||  j('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=j('.trSelected',grid);	
		
		
		if (com=='Edit'){
			location.href="add_city.php?id="+items[0].id.substr(3);			
		}
		if (com=='Delete'){
			if(confirm("Are you sure, you want to delete selected district?")){
					
				j.post("city.php", {"delete":"yes","id":items[0].id.substr(3)},function(data)
				{		 
				  var res  = data.split("-?");
					
				  if(res[0].trim()=="0") {					 
					 j('#flex1').flexOptions().flexReload(); 
					 alert('District deleted');
				  }else {
					  alert("District can not be deleted, it is mapped with "+res[0])
				  }
				  
				});		
			}			
		}
		
	}
}

/***************************************State**************************************************************/


function showState(){
j("#flex1").flexigrid({
	url: 'states.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'State', name : 'state_name', width : 280, sortable : true, align: 'left'},
		{display: 'Region', name : 'region_name',width : 150,sortable : true, align: 'left'},
		{display: 'Zone', name : 'zone_name',width : 150,sortable : true, align: 'left'},
		{display: 'Country', name : 'country_name',width : 150,sortable : true, align: 'left'},
		/*{display: 'State Code', name : 'state_code',width : 200,sortable : true, align: 'left'},*/
		{display: 'State Status', name : 'status',width : 100,sortable : true}
		
		],
	buttons : [
		{name: 'Add', bclass: 'add', onpress : eventHandlerState},
		{separator: true},		
		{name: 'Edit', bclass: 'edit', onpress : eventHandlerState},
		{separator: true},
		{name: 'Delete', bclass: 'delete', onpress : eventHandlerState},
		{separator: true},			
		/*{name: 'Import', bclass: 'import', onpress : eventHandlerState},
		{separator: true},*/
		{name: 'Export', bclass: 'export', onpress : eventHandlerState},
		{separator: true}
		],
	searchitems : [
		{display: 'State', name : 'state_name', isdefault: true},
		],
	sortname: "state_name",
	sortorder: "asc",
	usepager: true,
	title: 'State',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 850,
	height: 'auto'
});  
}


function eventHandlerState(com,grid)
{

	if (com=='Import')	{
		location.href="state_import.php";		
	}
	else if (com=='Export')	{
		location.href="export.inc.php?export_state_new";		
	}
	else if (com=='Add')	{
		location.href="add_state.php";		
	}else{
		if( j('.trSelected',grid).length<=0 ||  j('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=j('.trSelected',grid);	
		
		
		if (com=='Edit'){
			location.href="add_state.php?id="+items[0].id.substr(3);			
		}
		if (com=='Delete'){
			if(confirm("Are you sure, you want to delete selected state?")){
					
				j.post("states.php", {"delete":"yes","id":items[0].id.substr(3)},function(data)
				{		 
				  var res  = data.split("-?");
				  if(res[0].trim()=="0") {					 
					 j('#flex1').flexOptions().flexReload(); 
					 alert('State deleted');
				  }else {
					  alert("State can not be deleted, it is mapped with "+res[0])
				  }
				  
				});		
			}			
		}
		
	}
}



/******************************************Zone***********************************************************/


function showZones(){
j("#flex1").flexigrid({
	url: 'zones.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Zone', name : 'zone_name', width : 280, sortable : true, align: 'left'},
		{display: 'Region', name : 'region_name',width : 200,sortable : true, align: 'left'},
		{display: 'Country', name : 'country_name',width : 200,sortable : true, align: 'left'},
		{display: 'Status', name : 'status',width : 100,sortable : true}
		
		],
	buttons : [
		{name: 'Add', bclass: 'add', onpress : eventHandlerZones},
		{separator: true},		
		{name: 'Edit', bclass: 'edit', onpress : eventHandlerZones},
		{separator: true}
		],
	searchitems : [
		{display: 'Zone', name : 'zone_name', isdefault: true},
		],
	sortname: "zone_name",
	sortorder: "asc",
	usepager: true,
	title: 'Zone',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 850,
	height: 'auto'
});  
}


function eventHandlerZones(com,grid)
{
	if (com=='Add')	{
		location.href="addZones.php";		
	}else{
		if( j('.trSelected',grid).length<=0 ||  j('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=j('.trSelected',grid);
		if (com=='Edit'){
			location.href="addZones.php?id="+items[0].id.substr(3);			
		}
	}
}
/******************************************Zone***********************************************************/



/******************************************Regions***********************************************************/


function showRegions(){
j("#flex1").flexigrid({
	url: 'regions_new.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Region', name : 'region_name', width : 280, sortable : true, align: 'left'},
		{display: 'Country', name : 'country_name',width : 200,sortable : true, align: 'left'},
		
		{display: 'Status', name : 'status',width : 100,sortable : true}
		
		],
	buttons : [
		{name: 'Add', bclass: 'add', onpress : eventHandlerRegionsNew},
		{separator: true},		
		{name: 'Edit', bclass: 'edit', onpress : eventHandlerRegionsNew},
		{separator: true}
		],
	searchitems : [
		{display: 'Region', name : 'region_name', isdefault: true},
		],
	sortname: "region_name",
	sortorder: "asc",
	usepager: true,
	title: 'Regions',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 850,
	height: 'auto'
});  
}


function eventHandlerRegionsNew(com,grid)
{
	if (com=='Add')	{
		location.href="addRegions_new.php";		
	}else{
		if( j('.trSelected',grid).length<=0 ||  j('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=j('.trSelected',grid);
		if (com=='Edit'){
			location.href="addRegions_new.php?id="+items[0].id.substr(3);			
		}
	}
}
/******************************************Regions***********************************************************/


/******************************************Countries by Chirag Gup***********************************************************/


function showCountries(){
j("#flex1").flexigrid({
	url: 'country.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Country Name', name : 'country_name',width : 200,sortable : true, align: 'left'},
		{display: 'Country Code', name : 'country_code',width : 200,sortable : true, align: 'left'},
		{display: 'Status', name : 'status',width : 100,sortable : true}
		
		],
	buttons : [
		{name: 'Add', bclass: 'add', onpress : eventHandlerCountriesNew},
		{separator: true},		
		{name: 'Edit', bclass: 'edit', onpress : eventHandlerCountriesNew},
		{separator: true}
		],
	searchitems : [
		{display: 'Country', name : 'country_name', isdefault: true},
		],
	sortname: "country_name",
	sortorder: "asc",
	usepager: true,
	title: 'Countries',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 850,
	height: 'auto'
});  
}


function eventHandlerCountriesNew(com,grid)
{
	if (com=='Add')	{
		location.href="addCountry.php";		
	}else{
		if( j('.trSelected',grid).length<=0 ||  j('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=j('.trSelected',grid);
		if (com=='Edit'){
			location.href="addCountry.php?id="+items[0].id.substr(3);			
		}
	}
}
/******************************************Regions***********************************************************/
/************************************  CITY Section *****************************************************/


function showRetailerChannel(){
j("#flex1").flexigrid({
	url: 'channel.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Channel Name', name : 'channel_name', width : 280, sortable : true, align: 'left'},
		{display: 'Status', name : 'status', width : 200, sortable : true, align: 'left'}
		],
	buttons : [
		{name: 'Add', bclass: 'add', onpress : eventHandlerRetailerChainel},
		{separator: true},		
		{name: 'Edit', bclass: 'edit', onpress : eventHandlerRetailerChainel},
		{separator: true}			
		],
	searchitems : [
		{display: 'Channel name', name : 'channel_name', isdefault: true}
		
		],
	sortname: "channel_name",
	sortorder: "asc",
	usepager: true,
	title: 'Retailer Channel',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 850,
	height: 'auto'
});  
}
function eventHandlerRetailerChainel(com,grid)
{
   if (com=='Add')	{
		location.href="channel.php?action=add";		
	}else{
		if( j('.trSelected',grid).length<=0 ||  j('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=j('.trSelected',grid);	
		
		
		if (com=='Edit'){
			location.href="channel.php?action=edit&id="+items[0].id.substr(3);			
		}
		
		
	}
}











/************************************ END of city Section *******************************************/





/**************************** Distributor Login List 15 MAY 2014 ******************************************/

function showLoginDistributor(){
j("#flex1").flexigrid({
	url: 'distributor_login_list.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Distributor Name', name : 'distributor_name', width : 120, sortable : true, align: 'left'},
		{display: 'Username', name : 'username', width : 120, sortable : true, align: 'left'},
		{display: 'Email ID', name : 'email_id', width : 150, sortable : true, align: 'left'},
		{display: 'Phone Number', name : 'distributor_phone_no', width : 120, sortable : true, align: 'left'},
		{display: 'Login Status', name : '', width : 120, sortable : true, align: 'left'},
		{display: '', name : '', width : 120, sortable : true, align: 'left'}
			
		],
	searchitems : [
		{display: 'Distributor Name', name : 'distributor_name', isdefault: true},
		{display: 'Username', name : 'username'},
		{display: 'Email ID', name : 'email_id'},
		{display: 'Phone Number', name : 'distributor_phone_no'}
		],
	sortname: "distributor_name",
	sortorder: "asc",
	usepager: true,
	title: 'Distributor Login List',
	useRp: true,
	rp: 40,
	showTableToggleBtn: true,
	width: 850,
	height: 'auto'
});  
}


/**************************** Distributor Login List 15 MAY 2014 ******************************************/




/**************************** Master category Code 15 MAY 2014 ********************************************************/


function showMasterCategory(){
j("#flex1").flexigrid({
	url: 'master_category.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Category Name', name : 'category_name', width : 300, sortable : true, align: 'left'},
		{display: 'Category Code', name : 'category_code', width : 200, sortable : true, align: 'left'},
		{display: 'Margin Value', name : 'margin_value', width : 100, sortable : true, align: 'left'},
		{display: 'Status', name : 'status', width : 100, sortable : true, align: 'left'}
		],
	buttons : [
		{name: 'Add', bclass: 'add', onpress : eventHandlerMasterCategory},
		{separator: true},		
		{name: 'Edit', bclass: 'edit', onpress : eventHandlerMasterCategory},
		{separator: true},
		{name: 'Set Margin', bclass: 'margin', onpress : eventHandlerMasterCategory},
		{separator: true}
		],
	searchitems : [
		{display: 'Category Name', name : 'category_name', isdefault: true},
		{display: 'Category Code', name : 'category_code'}
		
		],
	sortname: "category_name",
	sortorder: "asc",
	usepager: true,
	title: 'Master Category',
	useRp: true,
	rp: 40,
	showTableToggleBtn: true,
	width: 800,
	height: 'auto'
});  
}
function eventHandlerMasterCategory(com,grid)
{
	if (com=='Add')	{
		location.href="add_master_category.php";		
	}else{
		if( j('.trSelected',grid).length<=0 ||  j('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=j('.trSelected',grid);	
		
		if (com=='Set Margin'){
			location.href="master_category_margin.php?id="+items[0].id.substr(3);			
		} else if (com=='Edit'){
			location.href="add_master_category.php?id="+items[0].id.substr(3);			
		} else if (com=='Delete'){
			if(confirm("Are you sure, you want to delete selected Category?")){
					
				j.post("master_category.php", {"delete":"yes","id":items[0].id.substr(3)},function(data)
				{		 
				  if(data!="") {					 
					 j('#flex1').flexOptions().flexReload(); 
				  }else {
					  alert("Record doesn't exists.")
				  }
				  
				});		
			}			
		}	
	}
}


/*********************************** END of Master Category Code *****************************************/





/**************************** NDC STOCK DETAIL 15 May 2014 ********************************************************/


function showStock(){
j("#flex1").flexigrid({
	url: 'stock.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Category Name', name : 'category_name', width : 200, sortable : true, align: 'left'},
		{display: 'Cases Size', name : 'case_size', width : 100, sortable : true, align: 'left'},
		{display: 'Item Code', name : 'item_code', width : 100, sortable : true, align: 'left'},
		{display: 'Stock Value', name : 'stock_value', width : 100, sortable : true, align: 'left'},
		{display: 'Last Update Date', name : 'last_updated_date', width : 100, sortable : true, align: 'left'}
		//{display: 'Status', name : 'status', width : 100, sortable : true, align: 'left'}
		],
	buttons : [
		/*{name: 'Add', bclass: 'add', onpress : eventHandlerMasterCategory},
		{separator: true},*/		
		{name: 'Edit', bclass: 'edit', onpress : eventHandlerCompanyStock},
		{separator: true}
		/*{name: 'Set Margin', bclass: 'margin', onpress : eventHandlerMasterCategory},
		{separator: true}*/
		],
	searchitems : [
		{display: 'Category Name', name : 'category_name', isdefault: true},
		{display: 'Color', name : 'color_desc'},
		{display: 'Item Code', name : 'item_code'}
		
		],
	sortname: "category_name",
	sortorder: "asc",
	usepager: true,
	title: 'Stock Details',
	useRp: true,
	rp: 40,
	showTableToggleBtn: true,
	width: 700,
	height: 'auto'
});  
}

function eventHandlerCompanyStock(com,grid)
{
	if (com=='Add')	{
		location.href="add_file_category.php";		
	}else{
		if( j('.trSelected',grid).length<=0 ||  j('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=j('.trSelected',grid);		
		if (com=='Edit'){
			location.href="stock.php?id="+items[0].id.substr(3);			
		}
		if (com=='Delete'){
			if(confirm("Are you sure, you want to delete selected Category?")){
					
				j.post("file_category.php", {"delete":"yes","id":items[0].id.substr(3)},function(data)
				{		 
				  if(data!="") {					 
					 j('#flex1').flexOptions().flexReload(); 
				  }else {
					  alert("Record doesn't exists.")
				  }
				  
				});		
			}			
		}	
	}
}

/**************************** NDC STOCK DETAIL 15 May 2014 ********************************************************/


function showFileCategory(){
j("#flex1").flexigrid({
	url: 'file_category.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: ' File Category Name', name : 'file_category_name', width : 300, sortable : true, align: 'left'},
		//{display: 'Category Code', name : 'category_code', width : 200, sortable : true, align: 'left'},
		{display: 'Status', name : 'status', width : 100, sortable : true, align: 'left'}
		],
	buttons : [
		{name: 'Add', class: 'add', onpress : eventHandlerFileCategory},
		{separator: true},		
		{name: 'Edit', class: 'edit', onpress : eventHandlerFileCategory},
		{separator: true}		
		],
	searchitems : [
		{display: 'Category Name', name : 'file_category_name', isdefault: true}
		//{display: 'Category Code', name : 'category_code'}
		
		],
	sortname: "file_category_name",
	sortorder: "asc",
	usepager: true,
	title: 'File Category',
	useRp: true,
	rp: 40,
	showTableToggleBtn: true,
	width: 1000,
	height: 'auto'
});  
}
function eventHandlerFileCategory(com,grid)
{
	if (com=='Add')	{
		location.href="add_file_category.php";		
	}else{
		if( j('.trSelected',grid).length<=0 ||  j('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=j('.trSelected',grid);		
		if (com=='Edit'){
			location.href="add_file_category.php?id="+items[0].id.substr(3);			
		}
		if (com=='Delete'){
			if(confirm("Are you sure, you want to delete selected Category?")){
					
				j.post("file_category.php", {"delete":"yes","id":items[0].id.substr(3)},function(data)
				{		 
				  if(data!="") {					 
					 j('#flex1').flexOptions().flexReload(); 
				  }else {
					  alert("Record doesn't exists.")
				  }
				  
				});		
			}			
		}	
	}
}



function showUploadFiles(){
j("#flex1").flexigrid({
	url: 'file_upload.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [
		{display: 'File Title', name : 'file_name', width : 200, sortable : true, align: 'left'},
		{display: 'File Category Name', name : 'file_category_name', width : 300, sortable : true, align: 'left'},
		{display: 'File Division', name : 'file_division_name', width : 200, align: 'left'},		
		{display: 'Date', name : 'last_update_date', width : 200, sortable : true, align: 'left'},
		{display: 'View', name : 'view', width : 100, sortable : true, align: 'left'}
		],
	buttons : [
		{name: 'Add', class: 'add', onpress : eventHandlerFileUpload},
		{separator: true},
		{name: 'Delete', class: 'delete', onpress : eventHandlerFileUpload},
		{separator: true}
		],
	searchitems : [
		{display: 'Category Name', name : 'file_category_name', isdefault: true}
		//{display: 'Category Code', name : 'category_code'}
		
		],
	sortname: "file_category_name",
	sortorder: "asc",
	usepager: true,
	title: 'File Details',
	useRp: true,
	rp: 40,
	showTableToggleBtn: true,
	width: 1000,
	height: 'auto'
});  
}
function eventHandlerFileUpload(com,grid)
{
	if (com=='Add')	{
		location.href="add_files.php";		
	}else{
		if( j('.trSelected',grid).length<=0 ||  j('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
	
		if (com=='Delete'){
			if(confirm("Are you sure, you want to delete selected Category File?")){
					
				j.post("file_upload.php", {"delete":"yes","id":items[0].id.substr(3)},function(data)
				{		 
				  if(data!="") {					 
					 j('#flex1').flexOptions().flexReload(); 
				  }else {
					  alert("Record doesn't exists.")
				  }
				  
				});		
			}			
		}	
	}
}




/**************************** DISTRIBUTOR IN PROCESS STOCK DETAIL 16 May 2014 ******************************************/


function showDistribuotrInProcessStock(){
j("#flex1").flexigrid({
	url: 'distributor_inpro_stock.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true,
	colModel : [		
		{display: 'Distributor Code', name : 'distributor_code', width : 75, sortable : true, align: 'left'},
		{display: 'Distributor Name', name : 'distributor_name', width : 170, sortable : true, align: 'left'},//1
		{display: 'Category Name', name : 'category_name', width : 72, sortable : true, align: 'left'},
		//{display: 'Cases', name : 'case_size', width : 100, sortable : true, align: 'left'},
		{display: 'Item Code', name : 'item_code', width : 65, sortable : true, align: 'left'},
		{display: 'Item Name', name : 'item_name', width : 200, sortable : true, align: 'left'},//2
		{display: 'Requested Quantity', name : 'rec_stock_value', width : 100, sortable : true, align: 'left'},
		{display: 'Accepted Quantity', name : 'acpt_stock_value', width : 90, sortable : true, align: 'left'},
		{display: 'Rejected Quantity', name : 'rejected_qty', width : 90, sortable : true, align: 'left'},//3
		{display: 'Rejected Reason', name : 'reject_reason', width : 120, sortable : true, align: 'left'},//4
		{display: 'Bill Number', name : 'bill_no', width : 60, sortable : true, align: 'left'},
		{display: 'Bill Date', name : 'bill_date', width : 65, sortable : true, align: 'left'},		
		{display: 'Last Update Date', name : 'last_updated_date', width : 100, sortable : true, align: 'left'},
		{display: 'Status', name : 'status', width : 55, sortable : true, align: 'left'}
		],
	buttons : [
		/*{name: 'Add', bclass: 'add', onpress : eventHandlerMasterCategory},
		{separator: true},		
		{name: 'Edit', bclass: 'edit', onpress : eventHandlerMasterCategory},
		{separator: true},
		{name: 'Set Margin', bclass: 'margin', onpress : eventHandlerMasterCategory},
		{separator: true}*/
		],
	searchitems : [
		{display: 'Category Name', name : 'category_name', isdefault: true},
		{display: 'Distributor Code', name : 'distributor_code'},
		{display: 'Distributor Name', name : 'distributor_name'},
		//{display: 'Case Size', name : 'color_code'},
		{display: 'Item Code', name : 'item_code'},
		{display: 'Itme Name', name : 'item_name'},
		{display: 'Bill Number', name : 'bill_no'}
		
		],
	sortname: "category_name",
	sortorder: "asc",
	usepager: true,
	title: 'Stock Details',
	useRp: true,
	rp: 40,
	showTableToggleBtn: true,
	width: 750,
	height: 'auto'
});  
}

/**************************** DISTRIBUTOR IN PROCESS STOCK DETAIL 16 May 2014 ******************************************/
/**************************** Salesman Allowance 20 May 2014 ******************************************/

function showSalesmanAllownce(checkhierarchyenable){
	
	if(checkhierarchyenable == true){
	var colModel01 = [ 		
		{display: 'Salesman', name : 'salesman_name', width : 100, sortable : true, align: 'left'},
		//{display: 'Salesman Level', name : 'des1', width : 100, sortable : true, align: 'left'},
		{display: 'Reporting to', name : 'rpt_to', width : 100, sortable : true, align: 'left'},
		{display: 'Salesman State', name : 'state_name', width : 100, sortable : true, align: 'left'},	
		{display: 'Salesman City', name : 'city_name', width : 100, sortable : true, align: 'left'},	
		{display: 'Allowance City', name : 'allwn_city', width : 100, sortable : true, align: 'left'},
		{display: 'Comment', name : 'cmt', width : 100, sortable : true, align: 'left'},
		{display: 'Date', name : 'app_date', width : 100, sortable : true, align: 'left'},
		{display: 'Time', name : 'app_time', width : 100, sortable : true, align: 'left'},	
		{display: 'Last updated on', name : 'last_update_date', width : 80, sortable : true, align: 'left'}		
			
		];
} else {
	var colModel01 = [ 		
		{display: 'Salesman', name : 'salesman_name', width : 100, sortable : true, align: 'left'},
		{display: 'Salesman State', name : 'state_name', width : 100, sortable : true, align: 'left'},	
		{display: 'Salesman City', name : 'city_name', width : 100, sortable : true, align: 'left'},	
		{display: 'Allowance City', name : 'allwn_city', width : 100, sortable : true, align: 'left'},
		{display: 'Comment', name : 'cmt', width : 100, sortable : true, align: 'left'},
		{display: 'Date', name : 'app_date', width : 100, sortable : true, align: 'left'},
		{display: 'Time', name : 'app_time', width : 100, sortable : true, align: 'left'},	
		{display: 'Last updated on', name : 'last_update_date', width : 80, sortable : true, align: 'left'}		
			
		];	
	
}
	
	
j("#flex1").flexigrid({
	url: 'salesman_allownce.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : colModel01,
	buttons : [		
		{name: 'Allowance Details', bclass: 'view', onpress : eventHandlerSalesmanAllownce},
		{separator: true}
		],
	searchitems : [
		
			{display: 'Salesman', name : 'S.salesman_name', isdefault: true},	
		{display: 'State', name : 'st.state_name', isdefault: true},	
		{display: 'City', name : 'c.city_name', isdefault: true}	
		
		],
	sortname: "sd.created",
	sortorder: "desc",
	usepager: true,
	title: 'Salesman Allowance List',
	useRp: true,
	rp: 20,
	showTableToggleBtn: true,
	width: 1000,
	height: 'auto'
});  
}



function eventHandlerSalesmanAllownce(com,grid)
{
	
		if( j('.trSelected',grid).length<=0 ||  j('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
	
		var items=j('.trSelected',grid);		
		if (com=='Allowance Details'){
			location.href="salesman_allowance_detail.php?id="+items[0].id.substr(3);			
		}
	
}

/**************************** Salesman Allowance 20 May 2014 ******************************************/
/**************************** Retailer Dues 20 May 2014 ******************************************/

function showRetailerDues(){
j("#flex1").flexigrid({
	url: 'retailer_due_list.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Retailer Name', name : 'retailer_name', width : 300, sortable : true, align: 'left'},
		{display: 'Due Amount', name : 'due_amt', width : 200, sortable : true, align: 'left'},
		{display: 'Date', name : 'last_update_date', width : 100, sortable : true, align: 'left'}
		],
	buttons : [
		
		
		],
	searchitems : [
		{display: 'Retailer', name : 'retailer_name', isdefault: true}		
		],
	sortname: "retailer_name",
	sortorder: "asc",
	usepager: true,
	title: 'Retailer Dues List',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 750,
	height: 'auto'
});  
}

/**************************** Retailer Dues 20 May 2014 ******************************************/


/***************************  Salesman Tags *******************************************/



function showSalesmanTags(){
j("#flex1").flexigrid({
	url: 'salesman_tag.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Tag Description', name : 'desc', width : 300, sortable : true, align: 'left'},
		//{display: 'Category Code', name : 'category_code', width : 200, sortable : true, align: 'left'},
		{display: 'Status', name : 'status', width : 100, sortable : true, align: 'left'}
		],
	buttons : [
		{name: 'Add', bclass: 'add', onpress : eventHandlerSalesmanTags},
		{separator: true},		
		{name: 'Edit', bclass: 'edit', onpress : eventHandlerSalesmanTags},
		{separator: true}		
		],
	searchitems : [
		{display: 'Tag Description', name : '`desc`', isdefault: true}
		//{display: 'Category Code', name : 'category_code'}
		
		],
	sortname: "`desc`",
	sortorder: "asc",
	usepager: true,
	title: 'Salesman Tags',
	useRp: true,
	rp: 40,
	showTableToggleBtn: true,
	width: 1000,
	height: 'auto'
});  
}
function eventHandlerSalesmanTags(com,grid)
{
	if (com=='Add')	{
		location.href="add_salesman_tags.php";		
	}else{
		if( j('.trSelected',grid).length<=0 ||  j('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=j('.trSelected',grid);		
		if (com=='Edit'){
			//alert("Hello");
		location.href="add_salesman_tags.php?id="+items[0].id.substr(3);				
		}
			
	}
}

	/***************************  Salesman Tags *******************************************/
	
	
	
	
	
	
	
	/**************************** Retailer Margin 23 May 2014 ******************************************/

function showRetailerMargin(){
j("#flex1").flexigrid({
	url: 'retailer_margin_list.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Retailer', name : 'retailer_name', width : 200, sortable : true, align: 'left'},
		{display: 'Category', name : 'category_name', width : 150, sortable : true, align: 'left'},
		{display: 'Actual Margin', name : 'margin_value', width : 80, sortable : true, align: 'left'},
		{display: 'Retailer Margin', name : 'ret_mar_val', width : 80, sortable : true, align: 'left'},
		{display: 'Last updated on', name : 'last_update_date', width : 170, sortable : true, align: 'left'},
		{display: 'Status', name : 'status', width : 50, sortable : true, align: 'left'}
		],
	buttons : [
		],
	searchitems : [
		{display: 'Retailer', name : 'retailer_name', isdefault: true},
		{display: 'Category', name : 'category_name', isdefault: true}	
		],
	sortname: "r.retailer_name",
	sortorder: "asc",
	usepager: true,
	title: 'Retailer Margin List',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 800,
	height: 'auto'
});  
}

/**************************** Retailer Margin 23 May 2014 ******************************************/





/**************************** Requested Retailer Margin List 23 May 2014 ******************************************/

function showRequestedRetailerMargin(){
j("#flex1").flexigrid({
	url: 'retailer_margin_request.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Retailer', name : 'retailer_name', width : 200, sortable : true, align: 'left'},
		{display: 'Request by', name : 'salesman_name', width : 120, sortable : true, align: 'left'},
		{display: 'Category', name : 'category_name', width : 150, sortable : true, align: 'left'},
		{display: 'Actual Margin', name : 'cur_mar_val', width : 80, sortable : true, align: 'left'},
		{display: 'Requested Margin', name : 'ret_mar_val', width : 100, sortable : true, align: 'left'},
		{display: 'Last Updated', name : 'created_on', width : 150, sortable : true, align: 'left'},
		{display: 'Approved', name : 'status', width : 50, sortable : true, align: 'left'}
		],
	buttons : [
		/*{name: 'Approve Margin', bclass: 'confirm', onpress : eventHandlerApproveMargin},
		{separator: true},
		{name: 'Disapprove Margin', bclass: 'confirm', onpress : eventHandlerApproveMargin},
		{separator: true}*/
		],
	searchitems : [
		{display: 'Retailer', name : 'retailer_name', isdefault: true},
		{display: 'Category', name : 'category_name', isdefault: true}	
		],
	sortname: "r.retailer_name",
	sortorder: "asc",
	usepager: true,
	title: 'Retailer Requested Margins',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 950,
	height: 'auto'
});  
}
function eventHandlerApproveMargin(com,grid)
{
	if (com=='Add')	{
		location.href="add_files.php";		
	}else{
		if( j('.trSelected',grid).length<=0 ||  j('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=j('.trSelected',grid);	
		if (com=='Approve Margin'){
			if(confirm("Are you sure, you want to confirm this margin?")){
					
				j.post("retailer_margin_request.php", {"approve":"yes","id":items[0].id.substr(3),"cat_id":items[0].sid},function(data)
				{	
				//alert(data);
				  if(data!="") {					 
					 j('#flex1').flexOptions().flexReload(); 
				  }else {
					  alert("Record doesn't exists.")
				  }
				  
				});		
			}			
		}	
		
		if (com=='Disapprove Margin'){
			if(confirm("Are you sure, you want to disapprove this margin?")){
					
				j.post("retailer_margin_request.php", {"disapprove":"no","id":items[0].id.substr(3),"order_id":items[0].sid},function(data)
				{	
				//alert(data);
				  if(data!="") {					 
					 j('#flex1').flexOptions().flexReload(); 
				  }else {
					  alert("Record doesn't exists.")
				  }
				  
				});		
			}			
		}	
	}
}


/**************************** Requested Retailer Margin List 23 May 2014 ******************************************/
	
	
	
function advisory(){
j("#flex1").flexigrid({
	url: 'advisory.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [
		//{display: 'Salesman', name : 'salesman_name', width : 100, sortable : true, align: 'left'},
		//{display: 'Retailer', name : 'retailer_id', width : 100, sortable : true, align: 'left'},
		{display: 'Retailer name', name : 'retailer_name', width : 100, sortable : true, align: 'left'},
		{display: 'Order Interval', name : 'order_interval', width : 100, sortable : true, align: 'left'},
		//{display: 'Order Value', name : 'order_id', width : 70, sortable : true, align: 'left'},
		{display: 'Order Frequency', name : 'order_frequency', width : 80, sortable : true, align: 'left'}	
		//{display: 'Map', name : 'lat', width : 80, sortable : true, align: 'left'},
		//{display: 'Date', name : 'transaction_date', width : 100, sortable : true, align: 'left'},
		//{display: 'Time', name : 'transaction_time', width : 100, sortable : true, align: 'left'}
		//{display: 'Status', name : 'order_status', width : 70, sortable : true, align: 'left'},
		//{display: 'Comments', name : 'comments', width : 100, sortable : true, align: 'left'}
		],
	buttons : [		
		//{name: 'View Details', bclass: 'viewdetails', onpress : eventHandlerTransactionList},
		//{separator: true}
		],
	searchitems : [
		//{display: 'Date of Order', name : 'Date of Order', isdefault: true},
		{display: 'Retailer', name : 'retailer_id', isdefault: true}	
		//{display: 'Distributor', name : 'distributor_name'},
		//{display: 'Retailer', name : 'retailer_name'}
		],
	sortname: "retailer_id",
	sortorder: "desc",
	usepager: true,
	title: 'Advisory',
	useRp: true,
	rp: 20,
	showTableToggleBtn: true,
	width: 'auto',
	height: 'auto'
});  
}

	
	
	
	
/**************************** DISTRIBUTOR STOCK DETAIL 24 June 2014 ******************************************/


function showDistribuotrStock(){
j("#flex1").flexigrid({
	url: 'distributor_stock.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Distributor Code', name : 'distributor_code', width : 80, sortable : true, align: 'left'},
		{display: 'Distributor Name', name : 'distributor_name', width : 200, sortable : true, align: 'left'},
		{display: 'Warehouse_Name', name : 'warehouse_name', width : 200, sortable : true, align: 'left'},
		{display: 'Category Name', name : 'category_name', width : 150, sortable : true, align: 'left'},
		//{display: 'Cases', name : 'case_size', width : 100, sortable : true, align: 'left'},
		{display: 'Item Code', name : 'item_code', width : 100, sortable : true, align: 'left'},
		{display: 'Item Name', name : 'item_name', width : 200, sortable : true, align: 'left'},
		{display: 'BSN', name : 'dis_stock_value', width : 100, sortable : true, align: 'left'},
		{display: 'Retailer Name', name : 'retailer_name', width : 100, sortable : true, align: 'left'},
		{display: 'Retailer Code', name : 'retailer_code', width : 100, sortable : true, align: 'left'},
		{display: 'Battery Ageing(in days)', name : 'batteryAge', width : 150, sortable : true, align: 'left'},
		{display: 'Last Update Date', name : 'last_updated_date', width : 100, sortable : true, align: 'left'},
		{display: 'Status', name : 'status', width : 100, sortable : true, align: 'left'}
		],
	buttons : [
		/*{name: 'Add', bclass: 'add', onpress : eventHandlerMasterCategory},
		{separator: true},		
		{name: 'Edit', bclass: 'edit', onpress : eventHandlerMasterCategory},
		{separator: true},
		{name: 'Set Margin', bclass: 'margin', onpress : eventHandlerMasterCategory},
		{separator: true}*/
		],
	searchitems : [
		{display: 'Category Name', name : 'category_name', isdefault: true},
		{display: 'Distributor Code', name : 'distributor_code'},
		{display: 'Distributor Name', name : 'distributor_name'},
		{display: 'Warehouse Name',   name : 'warehouse_name'},
		
		{display: 'Item Code', name : 'item_code'},
		{display: 'Item Name', name : 'item_name'}
		
		],
	sortname: "category_name",
	sortorder: "asc",
	usepager: true,
	title: 'Distributor Stock',
	useRp: true,
	rp: 40,
	showTableToggleBtn: true,
	width: 730,
	height: 'auto'
});  
}


/**************************** DISTRIBUTOR STOCK DETAIL 24 June 2014 ******************************************/

	
	

/*********************************** Report Emailer Schedule ****************************************/

function showReportSchedule(){
	
	sessionStorage.removeItem("rpt_id");
	j("#flex1").flexigrid({
		url: 'report_schedule_list.php?show=yes',
		dataType: 'json',
		method : "GET", 
		singleSelect:true, 
		colModel : [		
			{display: 'User Type', name : 'type', width : 120, sortable : true, align: 'left'},
			{display: 'User Name', name : '', width : 200, sortable : true, align: 'left'},
			{display: 'Start Date', name : 'from_date', width : 80, sortable : true, align: 'left'},
			{display: 'End Date', name : 'to_date', width : 80, sortable : true, align: 'left'},
			{display: 'Interval', name : 'sent_nature', width : 50, sortable : true, align: 'left'},
			{display: 'Schedule Day', name : 'weekday', width : 70, sortable : true, align: 'left'},
			{display: 'Additional emails', name : 'additional_emails', width : 120, sortable : true, align: 'left'},
			{display: 'Created', name : 'created', width :100, sortable : true, align: 'left'},
			{display: 'Status', name : 'status', width : 50, sortable : true, align: 'left'}
			],
		buttons : [
			{name: 'Add', bclass: 'add', onpress : eventHandlerReportSchedule},
			{separator: true},		
			{name: 'Edit', bclass: 'edit', onpress : eventHandlerReportSchedule},
			{separator: true},		
			{name: 'Send Mail', bclass: 'mail', onpress : eventHandlerReportSchedule}
			],
		searchitems : [
			{display: 'User Type', name : 'type', isdefault: true}
			//{display: 'Schedule Day', name : 'weekday'}
			],
		sortname: "",
		sortorder: "asc",
		usepager: true,
		title: 'Scheduled Reports List',
		useRp: true,
		rp: 15,
		showTableToggleBtn: true,
		width: "960",
		height: "auto"
	});  
}



function eventHandlerReportSchedule(com,grid) {

	if (com=='Add')	{
		location.href="reports_schedule.php";		
	}
	else if (com=='Send Mail'){ 
		location.href="weeklysendreportscript.php"; 
	}
	 else if( j('.trSelected',grid).length<=0 ||  j('.trSelected',grid).length>1) {
			alert("Please Select One Record.");
			return false;
	}
		
	var items=j('.trSelected',grid);		
		
	if (com=='Edit'){ 
		var rpt_id = items[0].id.substr(3); 
		sessionStorage.setItem("rpt_id", rpt_id); 
		//location.href="reports_schedule.php?rpt_id="+items[0].id.substr(3);
		location.href="reports_schedule.php"; 
	}
		
	if (com=='Delete'){
		if(confirm("Are you sure, you want to delete selected data?")){
			j.post("report_schedule_list.php", {"delete":"yes","id":items[0].id.substr(3)},function(data){		 
				  if(data!="") {					 
					 j('#flex1').flexOptions().flexReload(); 
				  } else {
					  alert("Record doesn't exists.")
				  }
				 });		
			}			
		}	
	}


/*********************************** Report Emailer Schedule ****************************************/



/******** Not Visited Dealer List 14 JULY 2014 ************************/

	
	
function showNotVisitedDealerReport(){
j("#flex1").flexigrid({
	url: 'notvisitedDealerReport.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Dealer Name', name : 'retailer_name', width : 180, sortable : true, align: 'left'},
		{display: 'Dealer Class', name : 'relationship_code', width : 80, sortable : true, align: 'left'},
		{display: 'City', name : 'city', width : 100, sortable : true, align: 'left'},
		{display: 'State', name : 'item_name', width : 100, sortable : true, align: 'left'},
		{display: 'Dealer Address', name : 'retailer_address', width : 200, sortable : true, align: 'left'},
		{display: 'Dealer Phone No', name : 'retailer_phone_no', width : 100, sortable : true, align: 'left'},
		{display: 'Market', name : 'opening_stock_quantity', width :180, sortable : true, align: 'left'}
		//{display: 'Date', name : 'date_of_order', width : 80, sortable : true, align: 'left'},
		//{display: 'Time', name : 'time_of_order', width :60, sortable : true, align: 'left'}					
		],
	searchitems : [
		{display: 'Dealer Name', name : 'retailer_name', isdefault: true},
		{display: 'City', name : 'city'}
		],
	sortname: "retailer_name",
	sortorder: "asc",
	usepager: true,
	title: 'Not Visited Dealers Report',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 980,
	height: 'auto'
});  
}	
	
	
/******** Not Visited Dealer List 14 JULY 2014 ************************/
	
	



/*******************************************************************
* desc : SKU based promotions data grid
* Created on : 05 January 2015
* Author : AV 
*/


function showPromotion(){
j("#flex1").flexigrid({
	url: 'promotion.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Chain(Group)', name : 'C.chain_name', width : 200, sortable : true, align: 'left'},
		{display: 'Item Code(SKU)', name : 'I.item_name', width : 220, sortable : true, align: 'left'},
		//{display: 'Item Code', name : 'I.item_code', width : 220, sortable : true, align: 'left'},
		{display: 'Item promotion', name : 'P.promo_dec', width : 200, sortable : true, align: 'left'},
		{display: 'Start Date', name : 'P.start_date', width : 65, sortable : true, align: 'left'},
		{display: 'End Date', name : 'P.end_date', width : 65, sortable : true, align: 'left'},
		{display: 'last updated', name : 'P.last_updated_on', width : 100, sortable : true, align: 'left'},
		{display: 'Status', name : 'P.status', width : 50, sortable : true, align: 'left'}
		],
	buttons : [
		{name: 'Add', bclass: 'add', onpress : eventHandlerPromotion},
		{separator: true},		
		{name: 'Edit', bclass: 'edit', onpress : eventHandlerPromotion},
		{separator: true},
		{name: 'Import', bclass: 'import', onpress : eventHandlerPromotion},
		{separator: true}
		
		],
	searchitems : [
		{display: 'Item Code', name : 'I.item_code', isdefault: true},
		{display: 'promotion', name : 'P.promo_desc'},
		{display: 'Chain Name', name : 'C.chain_name'}
		
		],
	sortname: "I.item_code",
	sortorder: "ASC",
	usepager: true,
	title: 'Promotion',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: "980",
	height: 'auto'
});  
}
function eventHandlerPromotion(com,grid)
{
	if (com=='Add')	{
		location.href="promotion.php?action=add";		
	} else if (com=='Import')	{
		location.href="promotion.php?action=import";		
	} else{
		if( j('.trSelected',grid).length<=0 ||  j('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=j('.trSelected',grid);		
		if (com=='Edit'){
			location.href="promotion.php?action=add&id="+items[0].id.substr(3);			
		}
		if (com=='Delete'){
			if(confirm("Are you sure, you want to delete selected Category?")){
					
				j.post("promotion.php", {"delete":"yes","id":items[0].id.substr(3)},function(data)
				{		 
				  if(data!="") {					 
					 j('#flex1').flexOptions().flexReload(); 
				  }else {
					  alert("Record doesn't exists.")
				  }
				  
				});		
			}			
		}	
	}
}




function showBrands(){
j("#flex1").flexigrid({
	url: 'brands.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Brands Name', name : 'brands_name', width : 280, sortable : true, align: 'left'},
		{display: 'Status', name : 'status', width : 200, sortable : true, align: 'left'}
		],
	buttons : [
		{name: 'Add', bclass: 'add', onpress : eventHandlerBrands},
		{separator: true},		
		{name: 'Edit', bclass: 'edit', onpress : eventHandlerBrands},
		{separator: true},			
		/*{name: 'Import', bclass: 'import', onpress : eventHandlerBrands},
		{separator: true}*/
		
		],
	searchitems : [
		{display: 'Brands name', name : 'brand_name', isdefault: true}
		
		],
	sortname: "brand_name",
	sortorder: "asc",
	usepager: true,
	title: 'Brands',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 850,
	height: 'auto'
});  
}
function eventHandlerBrands(com,grid)
{

	if (com=='Import')	{
		location.href="city_import.php";		
	}
	else if (com=='Export')	{
		location.href="#.php?export_route";		
	}
	else if (com=='Add')	{
		location.href="brands.php?action=add";		
	}else{
		if( j('.trSelected',grid).length<=0 ||  j('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=j('.trSelected',grid);	
		
		
		if (com=='Edit'){
			location.href="brands.php?action=edit&id="+items[0].id.substr(3);			
		}
		if (com=='Delete'){
			if(confirm("Are you sure, you want to delete selected city?")){
					
				j.post("city.php", {"delete":"yes","id":items[0].id.substr(3)},function(data)
				{		 
				  if(data!="") {					 
					 j('#flex1').flexOptions().flexReload(); 
					 alert('City deleted');
				  }else {
					  alert("Record doesn't exists.")
				  }
				  
				});		
			}			
		}
		
	}
}

/*---------------------------------------------
   Discription: show offer list in flexigrid
   date: 21 may
   by nizam
-----------------------------------------------*/

function showOffer(){
j("#flex1").flexigrid({
	url: 'offer.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Offer Name', name : 'offer_name', width : 280, sortable : true, align: 'left'},
		{display: 'Status', name : 'status', width : 200, sortable : true, align: 'left'}
		],
	buttons : [
		{name: 'Add', bclass: 'add', onpress : eventHandlerOffers},
		{separator: true},		
		{name: 'Edit', bclass: 'edit', onpress : eventHandlerOffers},
		{separator: true},			
		/*{name: 'Import', bclass: 'import', onpress : eventHandlerBrands},
		{separator: true}*/
		
		],
	searchitems : [
		{display: 'Offer name', name : 'offer_name', isdefault: true}
		
		],
	sortname: "offer_name",
	sortorder: "asc",
	usepager: true,
	title: 'Offers',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 850,
	height: 'auto'
});  
}
function eventHandlerOffers(com,grid)
{

	if (com=='Import')	{
		location.href="city_import.php";		
	}
	else if (com=='Export')	{
		location.href="#.php?export_route";		
	}
	else if (com=='Add')	{
		location.href="offer.php?action=add";		
	}else{
		if( j('.trSelected',grid).length<=0 ||  j('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=j('.trSelected',grid);	
		
		
		if (com=='Edit'){
			location.href="offer.php?action=edit&id="+items[0].id.substr(3);			
		}
		if (com=='Delete'){
			if(confirm("Are you sure, you want to delete selected city?")){
					
				j.post("city.php", {"delete":"yes","id":items[0].id.substr(3)},function(data)
				{		 
				  if(data!="") {					 
					 j('#flex1').flexOptions().flexReload(); 
					 alert('City deleted');
				  }else {
					  alert("Record doesn't exists.")
				  }
				  
				});		
			}			
		}
		
	}
}





/*---------------------------------------------
   Discription	: show cases list in flexigrid
   Date         : 21 may
   by           : nizam
-----------------------------------------------*/

function showCases(){
j("#flex1").flexigrid({
	url: 'cases.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Case Description', name : 'case_description', width : 280, sortable : true, align: 'left'},
		{display: 'Case Size', name : 'case_size', width : 280, sortable : true, align: 'left'},
		{display: 'Status', name : 'status', width : 200, sortable : true, align: 'left'}
		],
	buttons : [
		{name: 'Add', bclass: 'add', onpress : eventHandlerCases},
		{separator: true},		
		{name: 'Edit', bclass: 'edit', onpress : eventHandlerCases},
		{separator: true},			
		/*{name: 'Import', bclass: 'import', onpress : eventHandlerBrands},
		{separator: true}*/
		
		],
	searchitems : [
		{display: 'Case Description', name : 'case_description', isdefault: true}
		
		],
	sortname: "case_description",
	sortorder: "asc",
	usepager: true,
	title: 'Cases',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 850,
	height: 'auto'
});  
}
function eventHandlerCases(com,grid)
{

	if (com=='Add')	{
		location.href="cases.php?action=add";		
	}else{
		if( j('.trSelected',grid).length<=0 ||  j('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=j('.trSelected',grid);	
		
		
		if (com=='Edit'){
			location.href="cases.php?action=edit&id="+items[0].id.substr(3);			
		}
		if (com=='Delete'){
			if(confirm("Are you sure, you want to delete selected city?")){
					
				j.post("city.php", {"delete":"yes","id":items[0].id.substr(3)},function(data)
				{		 
				  if(data!="") {					 
					 j('#flex1').flexOptions().flexReload(); 
					 alert('City deleted');
				  }else {
					  alert("Record doesn't exists.")
				  }
				  
				});		
			}			
		}
		
	}
}





function showDistributorGrnStock(){
j("#flex1").flexigrid({
	url: 'distributor_grn.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Warehouse Code', name : 'warehouse_code', width : 100, sortable : true, align: 'left'},
		{display: 'Total BSN', name : 'TotalBsn', width : 50, sortable : true, align: 'left'},
		{display: 'Distributor Name', name : 'distributor_code', width : 200, sortable : true, align: 'left'},
		{display: 'Distributor Code', name : 'distributor_code', width : 120, sortable : true, align: 'left'},
		{display: 'Invoice Date', name : 'bill_date', width : 100, sortable : true, align: 'left'},
		{display: 'Invoice Number', name : 'bill_no', width : 100, sortable : true, align: 'left'},
		{display: 'Challan Date', name : 'delivery_challan_date', width : 100, sortable : true, align: 'left'},
		{display: 'Challan Number', name : 'delivery_challan_no', width : 90, sortable : true, align: 'left'},
		{display: 'Last Update Date', name : 'last_update_datetime', width : 100, sortable : true, align: 'left'}		
		],
	buttons : [
		/*{name: 'Add', bclass: 'add', onpress : eventDistributorGrnStock},
		{separator: true},		
		{name: 'Edit', bclass: 'edit', onpress : eventHandlerMasterCategory},
		{separator: true},*/
		{name: 'Accept GRN Transfer', bclass: 'transfer', onpress : eventDistributorGrnStock},
		{separator: true}
		],
	searchitems : [
		{display: 'Distributor Name', name : 'distributor_name', isdefault: true},
		{display: 'Distributor Code', name : 'distributor_code'},
		{display: 'Invoice Date', name : 'bill_date'},
		{display: 'Invoice Numuber', name : 'bill_no'}
		
		],
	sortname: "distributor_name",
	sortorder: "asc",
	usepager: true,
	title: 'Distributor GRN',
	useRp: true,
	rp: 40,
	showTableToggleBtn: true,
	width: 1000,
	height: 'auto'
});  
}

function eventDistributorGrnStock(com,grid)
{
  
	if (com=='Add')	{
		location.href="cases.php?action=add";		
	}else{
		if( j('.trSelected',grid).length<=0 ||  j('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=j('.trSelected',grid);	
		
		
		if (com=='Accept GRN Transfer'){
			location.href="stock_transfer.php?data="+items[0].id.substr(3);			
		}
		
		
	}
}

/*
   Description : Salesman return show in flexigrid.
   date        : 1 june
   by 		   : Nizam
*/


function showReturns(){

j("#flex1").flexigrid({
 url: 'return.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [
        {display: 'Distributor Name', name : 'distributor_name', width : 200, sortable : true, align: 'left'},
		{display: 'Distributor Code', name : 'distributor_code', width : 200, sortable : true, align: 'left'},
		{display: 'Return Date', name : 'date_of_order', width : 200, sortable : true, align: 'left'},
		{display: 'Return Time', name : 'time_of_order', width : 200, sortable : true, align: 'left'},
		{display: 'Last Update Date', name : 'last_update_date', width : 200, sortable : true, align: 'left'},
		
		],
	buttons : [
		{name: 'Add', bclass: 'add2', onpress : eventSalesReturn},
		{separator: true},		
		{name: 'View Primary Return', bclass: 'orderdeails', onpress : eventSalesReturn},
		{separator: true}
		],
	searchitems : [
		{display: 'Distributor Name', name : 'distributor_name',isdefault: true},
		{display: 'Distributor Code', name : 'distributor_code'}
		],
	sortname: "distributor_name",
	sortorder: "desc",
	usepager: true,
	title: 'Primary Return',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 1000,
	height: 'auto'
});  
}


function eventSalesReturn(com,grid)
{
	
	if (com=='Add')	{
		location.href="return.php?action=add";
	}
	else{
		if( j('.trSelected',grid).length<=0 ||  j('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=j('.trSelected',grid);		
		if (com=='View Primary Return'){
			var searchParam_1 = j('input[name=q]').val();
			var searchParam_2 = j('select[name=qtype]').val();
			var searchParam_3 = j(".pcontrol").find("input").val();
			location.href="return_list.php?id="+items[0].id.substr(3);	
		}	
	}
	
}


function showDistributorTarget(){

j("#flex1").flexigrid({
 url: 'distributortarget.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [
        {display: 'Distributor Name', name : 'distributor_name', width : 200, sortable : true, align: 'left'},
		{display: 'Distributor Code', name : 'distributor_code', width : 200, sortable : true, align: 'left'},
		{display: 'Target Type', name : 'target_type', width : 100, sortable : true, align: 'left'},
		{display: 'Month', name : 'target_month', width : 100, sortable : true, align: 'left'},
		{display: 'Year', name : 'target_year', width : 70, sortable : true, align: 'left'},		
		{display: 'Status', name : 'status', width : 70, sortable : true, align: 'left'},
		
		],
	buttons : [
		{name: 'Add', bclass: 'add2', onpress : eventDistributorTarget},
		{separator: true},		
		{name: 'Edit', bclass: 'orderdeails', onpress : eventDistributorTarget},
		{separator: true}
		],
	searchitems : [
		{display: 'Distributor Name', name : 'distributor_name',isdefault: true},
		{display: 'Distributor Code', name : 'distributor_code'},
		{display: 'Target Type', name : 'target_type'}
		],
	sortname: "distributor_name",
	sortorder: "desc",
	usepager: true,
	title: 'Distributor Target',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 750,
	height: 'auto'
});  
}


function eventDistributorTarget(com,grid)
{
	
	if (com=='Add')	{
		location.href="distributortarget.php?action=add";
	}
	else{
		if( j('.trSelected',grid).length<=0 ||  j('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=j('.trSelected',grid);		
		if (com=='Edit'){
			location.href="distributortarget.php?action=edit&id="+items[0].id.substr(3);
		}	
	}
	
}





	
/**************************** Dealer stock Details commeted by arvind 24-04-2017 ******************************************/


function showDealerStock(){
j("#flex1").flexigrid({
	url: 'dealer_stock.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Distributor Code', name : 'distributor_code', width : 80, sortable : true, align: 'left'},
		{display: 'Distributor Name', name : 'distributor_name', width : 200, sortable : true, align: 'left'},
		{display: 'Dealer Name', name : 'dealer_name', width : 200, sortable : true, align: 'left'},
		{display: 'Category Name', name : 'category_name', width : 150, sortable : true, align: 'left'},
		//{display: 'Cases', name : 'case_size', width : 100, sortable : true, align: 'left'},
		{display: 'Item Code', name : 'item_code', width : 100, sortable : true, align: 'left'},
		{display: 'Item Name', name : 'item_name', width : 200, sortable : true, align: 'left'},
		{display: 'BSN', name : 'dis_stock_value', width : 100, sortable : true, align: 'left'},
		{display: 'Last Update Date', name : 'last_updated_date', width : 100, sortable : true, align: 'left'},
		{display: 'Status', name : 'status', width : 100, sortable : true, align: 'left'}
		],
	buttons : [
		/*{name: 'Add', bclass: 'add', onpress : eventHandlerMasterCategory},
		{separator: true},		
		{name: 'Edit', bclass: 'edit', onpress : eventHandlerMasterCategory},
		{separator: true},
		{name: 'Set Margin', bclass: 'margin', onpress : eventHandlerMasterCategory},
		{separator: true}*/
		],
	searchitems : [
		{display: 'Category Name', name : 'category_name', isdefault: true},
		{display: 'Distributor Code', name : 'distributor_code'},
		{display: 'Distributor Name', name : 'distributor_name'},
		{display: 'Dealer Name', name : 'retailer_name'},
		{display: 'Item Code', name : 'item_code'},
		{display: 'Item Name', name : 'item_name'}
		
		],
	sortname: "category_name",
	sortorder: "asc",
	usepager: true,
	title: 'Dealer Stock',
	useRp: true,
	rp: 40,
	showTableToggleBtn: true,
	width: 730,
	height: 'auto'
});  
}


function showDealerStockForRetailer(){
j("#flex1").flexigrid({
	url: 'dealer_stock.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Distributor Code', name : 'distributor_code', width : 80, sortable : true, align: 'left'},
		{display: 'Distributor Name', name : 'distributor_name', width : 200, sortable : true, align: 'left'},
		//{display: 'Dealer Name', name : 'dealer_name', width : 200, sortable : true, align: 'left'},
		{display: 'Category Name', name : 'category_name', width : 150, sortable : true, align: 'left'},
		//{display: 'Cases', name : 'case_size', width : 100, sortable : true, align: 'left'},
		{display: 'Item Code', name : 'item_code', width : 100, sortable : true, align: 'left'},
		{display: 'Item Name', name : 'item_name', width : 200, sortable : true, align: 'left'},
		{display: 'BSN', name : 'dis_stock_value', width : 100, sortable : true, align: 'left'},
		{display: 'Last Update Date', name : 'last_updated_date', width : 100, sortable : true, align: 'left'},
		{display: 'Status', name : 'status', width : 100, sortable : true, align: 'left'}
		],
	buttons : [
		/*{name: 'Add', bclass: 'add', onpress : eventHandlerMasterCategory},
		{separator: true},		
		{name: 'Edit', bclass: 'edit', onpress : eventHandlerMasterCategory},
		{separator: true},
		{name: 'Set Margin', bclass: 'margin', onpress : eventHandlerMasterCategory},
		{separator: true}*/
		],
	searchitems : [
		{display: 'Category Name', name : 'category_name', isdefault: true},
		{display: 'Distributor Code', name : 'distributor_code'},
		{display: 'Distributor Name', name : 'distributor_name'},
		//{display: 'Dealer Name', name : 'retailer_name'},
		{display: 'Item Code', name : 'item_code'},
		{display: 'Item Name', name : 'item_name'}
		
		],
	sortname: "category_name",
	sortorder: "asc",
	usepager: true,
	title: 'Dealer Stock',
	useRp: true,
	rp: 40,
	showTableToggleBtn: true,
	width: 730,
	height: 'auto'
});  
}





/**************************** Dealer stock Details commeted by arvind 24-04-2017 ******************************************/




//Dealer GRN @ 21-04-2017 BY:Arvind


function showDealerGrnStock(){
j("#flex1").flexigrid({
	url: 'dealer_grn.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Distributor Code', name : 'Distributor_code', width : 100, sortable : true, align: 'left'},
		{display: 'Total BSN', name : 'TotalBsn', width : 50, sortable : true, align: 'left'},
		{display: 'Dealer Name', name : 'dealer_code', width : 200, sortable : true, align: 'left'},
		{display: 'Dealer Code', name : 'dealer_code', width : 120, sortable : true, align: 'left'},
		//{display: 'Invoice Date', name : 'bill_date', width : 100, sortable : true, align: 'left'},
		//{display: 'Invoice Number', name : 'bill_no', width : 100, sortable : true, align: 'left'},
		//{display: 'Challan Date', name : 'delivery_challan_date', width : 100, sortable : true, align: 'left'},
		//{display: 'Challan Number', name : 'delivery_challan_no', width : 90, sortable : true, align: 'left'},
		{display: 'Last Update Date', name : 'last_update_datetime', width : 100, sortable : true, align: 'left'}		
		],
	buttons : [
		/*{name: 'Add', bclass: 'add', onpress : eventDistributorGrnStock},
		{separator: true},		
		{name: 'Edit', bclass: 'edit', onpress : eventHandlerMasterCategory},
		{separator: true},*/
		{name: 'Accept GRN Transfer', bclass: 'transfer', onpress : eventDealerGrnStock},
		{separator: true}
		],
	searchitems : [
		{display: 'Dealer Name', name : 'retailer_name', isdefault: true},
		{display: 'Dealer Code', name : 'retailer_code'}
		//{display: 'Invoice Date', name : 'bill_date'},
		//{display: 'Invoice Numuber', name : 'bill_no'}
		
		],
	sortname: "retailer_name",
	sortorder: "asc",
	usepager: true,
	title: 'Dealer GRN',
	useRp: true,
	rp: 40,
	showTableToggleBtn: true,
	width: 1000,
	height: 'auto'
});  
}

function eventDealerGrnStock(com,grid)
{
  
	if (com=='Add')	{
		location.href="cases.php?action=add";		
	}else{
		if( j('.trSelected',grid).length<=0 ||  j('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=j('.trSelected',grid);	
		
		
		if (com=='Accept GRN Transfer'){
			location.href="dealer_stock_transfer.php?data="+items[0].id.substr(3);			
		}
		
		
	}
}











//End



























/*  
Description : Show Retailer Type List Master
date: 10 jun
BY Nizam 

*/


function showRetailerType(){
j("#flex1").flexigrid({
	url: 'retailer_type.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Retailer Type', name : 'type_name', width : 280, sortable : true, align: 'left'},
		{display: 'Status', name : 'status', width : 200, sortable : true, align: 'left'}
		],
	buttons : [
		{name: 'Add', bclass: 'add', onpress : eventHandlerRetailerType},
		{separator: true},		
		{name: 'Edit', bclass: 'edit', onpress : eventHandlerRetailerType},
		{separator: true}
		
		],
	searchitems : [
		{display: 'Retailer Type', name : 'type_name', isdefault: true}
		
		],
	sortname: "type_name",
	sortorder: "asc",
	usepager: true,
	title: 'Retailer Type Name',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 850,
	height: 'auto'
});  
}
function eventHandlerRetailerType(com,grid)
{

	if (com=='Add')	{
		location.href="retailer_type.php?action=add";		
	}else{
		if( j('.trSelected',grid).length<=0 ||  j('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=j('.trSelected',grid);	
		
		
		if (com=='Edit'){
			location.href="retailer_type.php?action=edit&id="+items[0].id.substr(3);			
		}
		
		
	}
}




/***********************************  complaint list of Distrubutors ****************************************/

function distributorcomplaint(){
	
	j("#flex1").flexigrid({
		url: 'distributor_complaint.php?show=yes',
		dataType: 'json',
		method : "GET", 
		singleSelect:true, 
		colModel : [		
			{display: 'BSN Number.', name : 'c.product_serial_no', width : 270, sortable : true, align: 'left'},
			
			{display: 'Sale Date', name : 'pdate', width : 120, sortable : true, align: 'left'},		
			{display: 'status', name : 'status', width : 100, sortable : true, align: 'left'},
                        {display: 'Image', name : 'images', width : 120, sortable : true, align: 'left'},
			{display: 'Complaint Date', name : 'Complaint', width : 100, sortable : true, align: 'left'},
			{display: 'Replaced BSN', name : 'RBSN', width : 100, sortable : true, align: 'left'},
                        {display: 'Comment ', name : 'comment', width : 200, sortable : true, align: 'left'}
			],
		buttons : [
			{name: 'Add', bclass: 'addhir', onpress : eventHandlercomplaint_t},
			{separator: true},
			{name: 'Edit', bclass: 'edithir', onpress : eventHandlercomplaint_t},
			{separator: true},		
			{name: 'View', bclass: 'viewhir', onpress : eventHandlercomplaint_t},
			{separator: true},
			{name: 'Delete', bclass: 'Deletehir', onpress : eventHandlercomplaint_t},
			{separator: true}		
			
			],
		searchitems : [
			{display: 'BSN ', name : 'c.product_serial_no', isdefault: true},
			{display: 'Complaint Date ', name : 'c.created_date', isdefault: true},
			{display: 'Complaint Status ', name : 'c.complain_status', isdefault: true}
			],

		sortname: "c.complaint_id",
		sortorder: "desc",
		usepager: true,
		title: 'Complaint',
		useRp: true,
		rp: 15,
		showTableToggleBtn: true,
		width: "900",
		height: "auto"
	});  
}



function eventHandlercomplaint_t(com,grid) {
	var items=j('.trSelected',grid);
	if (com=='Import')	{
		//location.href="import_item.php";	
		location.href="#";	
	}
	
		
	else if (com=='View')
	{
		location.href="view_complaint.php?id="+items[0].id.substr(3);			
	}	
	
	
	else if (com=='Export')	{
		location.href="export.inc.php?export_hierarchy";		
	}
	else if (com=='Add')	{
		location.href="raisecomplaint_distributor.php";		
	}else{
		if( j('.trSelected',grid).length<=0 ||  j('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
				
		if (com=='Edit')
		{
			
			location.href="raisecomplaint_distributor.php?id="+items[0].id.substr(3);	
			
		}
		
		
		
		
		
		if (com=='Delete'){
			if(confirm("Are you sure, you want to delete selected data?")){
					
				j.post("distributor_complaint.php", {"delete":"yes","id":items[0].id.substr(3)},function(data)
				{		 
				  if(data!="") {					 
					 j('#flex1').flexOptions().flexReload(); 
				  }else {
					  alert("Record doesn't exists.")
				  }
				  
				});		
			}			
		}	
	}
}
	
/*********************************** End of  complaint list of Distrubutors ****************************************/








	/***************************************************************************************
	* DESC : Order list of retailer taken by distributor Panel
	* Author : AJAY
 	* Created : 9th June 2015
 	* 
 	**/



function showRetailerOrderByDistributor(){
j("#flex1").flexigrid({
	url: 'order.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [
		{display: 'Distributor Name', name : 'distributor_name', width : 150, sortable : true, align: 'left'},
		{display: 'Salesperson Name', name : 'salesman_name', width : 150, sortable : true, align: 'left'},
		{display: 'Retailer Name', name : 'retailer_name', width : 200, sortable : true, align: 'left'},
		//{display: 'Retailer Market', name : 'retailer_location', width : 100, sortable : true, align: 'left'},
		//{display: 'Retailer Address', name : 'retailer_address', width : 150, sortable : true, align: 'left'},
		{display: 'Total Invoice Amount', name : 'acc_total_invoice_amount', width : 150, sortable : true, align: 'left'},
		//{display: 'Total no of items', name : 'total_item', width : 80, sortable : true, align: 'left'},
		{display: 'Last Update Order', name : 'date_of_order', width : 150, sortable : true, align: 'left'},
		{display: 'Order Status', name : 'order_status', width : 130, sortable : true, align: 'left'}
		],
	buttons : [		
		{name: 'Add', bclass: 'add', onpress : eventHandlershowRetailerOrderByDistributor},
		{separator: true},
		{name: 'Edit', bclass: 'edit', onpress : eventHandlershowRetailerOrderByDistributor},
		{separator: true},
		{name: 'View & Confirm Order', bclass: 'confirm', onpress : eventHandlershowRetailerOrderByDistributor},
		{separator: true}
		],
	searchitems : [
		{display: 'Distributor Name', name : 'distributor_name', isdefault: true},
		{display: 'Salesperson Name', name : 'salesman_name'},
		{display: 'Retailer Name', name : 'retailer_name'}
		],
	sortname: "date_of_order",
	sortorder: "desc",
	usepager: true,
	title: 'Last Saved Orders List',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 1000,
	height: 'auto'
});  
}


function eventHandlershowRetailerOrderByDistributor(com,grid)

{
	if (com=='Add')	{
		location.href="order.php?action=add";		
	} else if (com=='Import')	{
		location.href="";		
	} else{
		if( j('.trSelected',grid).length<=0 ||  j('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=j('.trSelected',grid);		
		if (com=='Edit'){
			location.href="order.php?action=edit&id="+items[0].id.substr(3);			
		}

		if (com=='View & Confirm Order'){
			location.href="order.php?action=confirm&id="+items[0].id.substr(3);	
		}


		if (com=='Delete'){
			if(confirm("Are you sure, you want to delete selected Category?")){
					
				j.post("order.php", {"delete":"yes","id":items[0].id.substr(3)},function(data)
				{		 
				  if(data!="") {					 
					 j('#flex1').flexOptions().flexReload(); 
				  }else {
					  alert("Record doesn't exists.")
				  }
				  
				});		
			}			
		}	
	}
}



/* Description: Show Item varient
   Date : 11 jun
   By : Nizam
   
*/

function showItemVariant(){
j("#flex1").flexigrid({
	url: 'variant.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Variant Name', name : 'variant_name', width : 280, sortable : true, align: 'left'},
		{display: 'Status', name : 'status', width : 200, sortable : true, align: 'left'}
		],
	buttons : [
		{name: 'Add', bclass: 'add', onpress : eventHandlerVariant},
		{separator: true},		
		{name: 'Edit', bclass: 'edit', onpress : eventHandlerVariant},
		{separator: true},			
		
		],
	searchitems : [
		{display: 'Variant name', name : 'variant_name', isdefault: true}
		
		],
	sortname: "variant_name",
	sortorder: "asc",
	usepager: true,
	title: 'Variant Name',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 850,
	height: 'auto'
});  
}


function eventHandlerVariant(com,grid)
{
    if(com=='Add'){
		location.href="variant.php?action=add";		
	}else{
		if( j('.trSelected',grid).length<=0 ||  j('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=j('.trSelected',grid);	
		
		
		if (com=='Edit'){
			location.href="variant.php?action=edit&id="+items[0].id.substr(3);			
		}
		/*if (com=='Delete'){
			if(confirm("Are you sure, you want to delete selected city?")){
					
				j.post("city.php", {"delete":"yes","id":items[0].id.substr(3)},function(data)
				{		 
				  if(data!="") {					 
					 j('#flex1').flexOptions().flexReload(); 
					 alert('City deleted');
				  }else {
					  alert("Record doesn't exists.")
				  }
				  
				});		
			}			
		}*/
		
	}
}



/* Description: Show Sku
   Date : 11 jun
   By : Nizam
   
*/

function showSku(){
j("#flex1").flexigrid({
	url: 'sku.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Sku Name', name : 'sku_name', width : 280, sortable : true, align: 'left'},
		{display: 'Status', name : 'status', width : 200, sortable : true, align: 'left'}
		],
	buttons : [
		{name: 'Add', bclass: 'add', onpress : eventHandlerSku},
		{separator: true},		
		{name: 'Edit', bclass: 'edit', onpress : eventHandlerSku},
		{separator: true},			
		],
	searchitems : [
		{display: 'Sku name', name : 'sku_name', isdefault: true}
		
		],
	sortname: "sku_name",
	sortorder: "asc",
	usepager: true,
	title: 'Sku Name',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 850,
	height: 'auto'
});  
}


function eventHandlerSku(com,grid)
{
    if(com=='Add'){
		location.href="sku.php?action=add";		
	}else{
		if( j('.trSelected',grid).length<=0 ||  j('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=j('.trSelected',grid);	
		
		
		if (com=='Edit'){
			location.href="sku.php?action=edit&id="+items[0].id.substr(3);			
		}
		/*if (com=='Delete'){
			if(confirm("Are you sure, you want to delete selected city?")){
					
				j.post("city.php", {"delete":"yes","id":items[0].id.substr(3)},function(data)
				{		 
				  if(data!="") {					 
					 j('#flex1').flexOptions().flexReload(); 
					 alert('City deleted');
				  }else {
					  alert("Record doesn't exists.")
				  }
				  
				});		
			}			
		}*/
		
	}
}



/*---------------------------------------------
   Discription: show region list in flexigrid
   date: 21 may
   by nizam
-----------------------------------------------*/

function showRegion(){
j("#flex1").flexigrid({
	url: 'region.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Region Name', name : 'region_name', width : 280, sortable : true, align: 'left'},
		{display: 'Status', name : 'status', width : 200, sortable : true, align: 'left'}
		],
	buttons : [
		{name: 'Add', bclass: 'add', onpress : eventHandlerRegions},
		{separator: true},		
		{name: 'Edit', bclass: 'edit', onpress : eventHandlerRegions},
		{separator: true},			
		/*{name: 'Import', bclass: 'import', onpress : eventHandlerBrands},
		{separator: true}*/
		
		],
	searchitems : [
		{display: 'Region name', name : 'region_name', isdefault: true}
		],
	sortname: "region_name",
	sortorder: "asc",
	usepager: true,
	title: 'Regions',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 850,
	height: 'auto'
});  
}
function eventHandlerRegions(com,grid)
{
    if(com=='Add'){
		location.href="region.php?action=add";		
	 }else{
		if( j('.trSelected',grid).length<=0 ||  j('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=j('.trSelected',grid);	
		if (com=='Edit'){
			location.href="region.php?action=edit&id="+items[0].id.substr(3);			
		}
		
		
	}
}


/*----------------------------- function added by Gaurav on 30 July 15 ----------------------*/

function showMarketIntelligence(){
j("#flex1").flexigrid({
	url: 'market_intelligence.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Salesman Name', name : 'salesman_name', width : 100, sortable : true, align: 'left'},
		//{display: 'Salesman Level', name : 'description', width : 180, sortable : true, align: 'left'},
		{display: 'Company Name', name : 'company_name', width : 100, sortable : true, align: 'left'},
		{display: 'Category', name : 'category', width : 100, sortable : true, align: 'left'},
		{display: 'Subcategory', name : 'subcategory', width : 100, sortable : true, align: 'left'},
		{display: 'Description', name : 'description', width : 300, sortable : true, align: 'left'},
		{display: 'Date', name : 'app_date', width : 80, sortable : true, align: 'left'},
		{display: 'Time', name : 'app_time', width : 80, sortable : true, align: 'left'}
		],
	buttons : [
		{name: 'View', bclass: 'viewmsg', onpress : eventHandlerMarketIntelligence},
		{separator: true}
		],
	searchitems : [
		{display: 'Salesman Name', name : 's.salesman_name', isdefault: true}
		],
	sortname: "market_intelligence_id",
	sortorder: "desc",
	usepager: true,
	title: 'Market Intelligence',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 950,
	height: 'auto'
});  
}
function eventHandlerMarketIntelligence(com,grid)
{
	if (com=='Send Message')	{
		//location.href="send_message.php";		
	}else{
		if( j('.trSelected',grid).length<=0 ||  j('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=j('.trSelected',grid);		
		if (com=='View'){
			window.open("view_market_intll.php?id="+items[0].id.substr(3), '_blank');
		}
	}
}

//


function showPlannedVsVisited(){
j("#flex1").flexigrid({
	url: 'plannedvsvisited_report.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel: [
	   	{display: 'Salesman', name : 'salesman_name', width : 80, sortable : true, align: 'left'},
	   	{display: 'State', name : 'state_name', width : 80, sortable : true, align: 'left'},
	   	{display: 'Reporting To', name : 'rpt_to', width : 90, sortable : true, align: 'left'},
		{display: 'Total Planned Route', name : 'plannedAccount', width :130, sortable : true, align: 'left'},
		{display: 'Total Visited Route(Planned)', name : 'plannedVisitedAccount', width :150, sortable : true, align: 'left'},
		//{display: 'Total Visited Route(Unplanned)', name : 'unplannedVisitedAccount', width :150, sortable : true, align: 'left'},
		{display: 'Working Hours', name : 'start_time', width :100, sortable : true, align: 'left'}
		//{display: 'Total Sale', name : 'unplannedVisitedAccount', width :100, sortable : true, align: 'left'}
		],
	buttons : [			
			{name: 'View Detail', bclass: 'view', onpress : eventHandlerPLVsVS},
			{separator: true}
		],
	searchitems : [
		{display: 'Salesman', name : 'salesman_name', isdefault: true}		
		],
	sortname: "last_updated_on",
	sortorder: "desc",
	usepager: true,
	title: 'Beat Adherence Report',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 1000,
	height: 'auto'
});  
}
	
function eventHandlerPLVsVS(com,grid){

	if( j('.trSelected',grid).length<=0 ||  j('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
	}
		var items=j('.trSelected',grid);		
		if (com=='View Detail'){
			window.open("plannedvisitedtown_detail.php?id="+items[0].id.substr(3), '_blank');		
		}
}


/*
  Description: show Customer list 
  Date: 1 oct 2015
  By : Nizam 
*/

function showCustomers(){
	j("#flex1").flexigrid({
	url: 'customer.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Name', name : 'customer_name', width : 80, sortable : true, align: 'left'},
		{display: 'Customer Code', name : 'customer_code', width : 80, sortable : true, align: 'left'},
		{display: 'Customer Type', name : 'type_name', width : 80, sortable : true, align: 'left'},
		{display: 'Customer Class', name : 'customer_class', width : 80, sortable : true, align: 'left'},
		{display: 'Added by(Salesman)', name : 'salesman_name', width : 80, sortable : true, align: 'left'},
		{display: 'Salesman Code', name : 'salesman_code', width : 80, sortable : true, align: 'left'},
		{display: 'Division', name : 'division_name', width : 80, sortable : true, align: 'left'},
		{display: 'Customer Region', name : 'region_name', width : 92, sortable : true, align: 'left'},
		{display: 'Interested', name : 'display_outlet', width : 50, sortable : true, align: 'left'},
		{display: 'State', name : 'state_name', width : 80, sortable : true, align: 'left'},
		{display: 'District', name : 'city_name', width : 80, sortable : true, align: 'left'},
		{display: 'Taluka', name : 'taluka_name', width : 80, sortable : true, align: 'left'},
		{display: 'City', name : 'market_name', width : 100, sortable : true, align: 'left'},
		{display: 'Pincode', name : 'pin_code', width : 100, sortable : true, align: 'left'},
		{display: 'Address 1', name : 'customer_address', width : 100, sortable : true, align: 'left'},
		{display: 'Address 2', name : 'customer_address2', width : 100, sortable : true, align: 'left'},
		{display: 'Map', name : 'lat', width : 80, sortable : true, align: 'left'},
		//{display: 'Send SMS Mobile No', name : 'sms_number', width : 100, sortable : true, align: 'left'},
		{display: 'Phone Number 1', name : 'customer_phone_no', width : 80, sortable : true, align: 'left'},
		{display: 'Phone Number 2', name : 'customer_phone_no2', width : 80, sortable : true, align: 'left'},
		{display: 'Phone Number 3', name : 'customer_phone_no3', width : 80, sortable : true, align: 'left'},
		{display: 'Landline Number', name : 'customer_leadline_no', width : 80, sortable : true, align: 'left'},
		{display: 'Contact Person 1', name : 'contact_person', width : 80, sortable : true, align: 'left'},
		{display: 'Contact Number 1', name : 'contact_number', width : 80, sortable : true, align: 'left'},
		{display: 'Contact Person 2', name : 'contact_person2', width : 80, sortable : true, align: 'left'},
		{display: 'Contact Number 2', name : 'contact_number2', width : 80, sortable : true, align: 'left'},
		{display: 'Contact Person 3', name : 'contact_person3', width : 80, sortable : true, align: 'left'},
		{display: 'Contact Number 3', name : 'contact_number3', width : 80, sortable : true, align: 'left'},
		{display: 'Username', name : 'username', width : 80, sortable : true, align: 'left'},
		{display: 'Email ID 1', name : 'customer_email', width : 100, sortable : true, align: 'left'},
		{display: 'Email ID 2', name : 'customer_email2', width : 100, sortable : true, align: 'left'},
		{display: 'Email ID 3', name : 'customer_email3', width : 100, sortable : true, align: 'left'},
		{display: 'Date of Birth', name :'customer_dob', width : 80, sortable : true, align: 'left'},
		{display: 'Start Date', name : 'start_date', width : 80, sortable : true, align: 'left'},
		{display: 'End Date', name : 'end_date', width : 80, sortable : true, align: 'left'},
		{display: 'Validation Status', name : 'validation_status', width : 80, sortable : true, align: 'center'},
		{display: 'Not Validation Reason', name : 'validation_reason', width : 80, sortable : true, align: 'center'},
		{display: 'Remark', name : 'remark', width : 80, sortable : true, align: 'left'}
		/*{display: 'Distributor Status', name : 'status', width : 80, sortable : true, align: 'center'},
		{display: 'Login Status', name : 'loginStatus', width : 80, sortable : true, align: 'center'}*/
			
		],
	buttons : [
		{name: 'Add', bclass: 'add', onpress : eventHandlerCustomerAdminUser},
		{separator: true},		
		{name: 'Edit', bclass: 'edit', onpress : eventHandlerCustomerAdminUser},
		{separator: true},
		{name: 'Delete', bclass: 'delete', onpress : eventHandlerCustomerAdminUser},
		{separator: true}
		],
	searchitems : [
		{display: 'Name', name : 'customer_name', isdefault: true},
		{display: 'Username', name : 'username'},
		{display: 'Added by', name : 'salesman_name'},
		{display: 'Division', name : 'division_name'},
		{display: 'Email ID', name : 'customer_email'},
		{display: 'Send SMS Mobile No', name : 'sms_number'},
		{display: 'Phone Number', name : 'customer_phone_no'},
		{display: 'State', name : 'state_name'},
		{display: 'District', name : 'city_name'},
		{display: 'Address', name : 'customer_address'},
		{display: 'City', name : 'customer_location'},
		{display: 'Contact Person', name : 'contact_person'},
		{display: 'Customer Type', name : 'type_name'},
		{display: 'Contact Number', name : 'contact_number'}
		],
	sortname: "customer_name",
	sortorder: "asc",
	usepager: true,
	title: 'Customers',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 700,
	height: 'auto'
});  
}
function eventHandlerCustomerAdminUser(com,grid)
{
	if (com=='Add')	{
		location.href="customer.php?add";	
	}else if (com=='Import')	{
		location.href="customer.php?import";	
	}else if (com=='Export')	{
		location.href="export.inc.php?export_distributors_list";	
	}
	else{
		if( j('.trSelected',grid).length<=0 ||  j('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=j('.trSelected',grid);		
		if (com=='Edit'){
			location.href="customer.php?id="+items[0].id.substr(3);			
		}else if (com=='Create Login')	{
		location.href="#.php?login&id="+items[0].id.substr(3);
	}

	else if (com=='Delete')	{
		
		if(confirm("Are you sure, you want to delete selected Customer?")){
					
				j.post("customer.php", {"delete":"yes","id":items[0].id.substr(3)},function(data)
				{

					j("#message-green").hide();
				  if(data!="") {

					 j('#flex1').flexOptions().flexReload(); 
				  }else {
					  alert("Record doesn't exists.")
				  }
				  
				});		
			}
	}
			
	}
}






/********************************    company User *******************/



function company_user(){
	
	j("#flex1").flexigrid({
		url: 'company_user.php?show=yes',
		dataType: 'json',
		method : "GET", 
		singleSelect:true, 
		colModel : [		
			{display: 'Name', name : 'a.user_label', width : 270, sortable : true, align: 'left'},
			{display: 'level', name : 'level', width : 120, sortable : true, align: 'left'},
			{display: 'Phone No', name : 'user_code', width : 120, sortable : true, align: 'left'},
			{display: 'User Name', name : 'created', width : 120, sortable : true, align: 'left'},
			{display: 'Start Date', name : 'start_date', width : 100, sortable : true, align: 'left'},
		    {display: 'End Date', name : 'end_date', width : 100, sortable : true, align: 'left'},		
			{display: 'Status', name : 'status', width : 50, sortable : true, align: 'left'}
			],
		buttons : [
			{name: 'Add', bclass: 'adduser', onpress : eventHandlerhierarchy1},
			{separator: true},		
			{name: 'Edit', bclass: 'edituser', onpress : eventHandlerhierarchy1},
			{separator: true}	
			
			],
		searchitems : [
			{display: 'Name', name : 'c.operator_name', isdefault: true}],
		sortname: "c.operator_name",
		sortorder: "asc",
		usepager: true,
		title: 'Company User',
		useRp: true,
		rp: 15,
		showTableToggleBtn: true,
		width: "900",
		height: "auto"
	});  
}



function eventHandlerhierarchy1(com,grid) {
	
	if (com=='Import')	{
		//location.href="import_item.php";	
		location.href="#";	
	}
	
		
	
	
	
	
	else if (com=='Add')	{
		location.href="add_company_user.php";		
	}else{
		if( j('.trSelected',grid).length<=0 ||  j('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=j('.trSelected',grid);		
		if (com=='Edit')
		{
			//alert('HEllo');
			//alert("addhierarchy.php?id="+items[0].id.substr(3));
			location.href="add_company_user.php?id="+items[0].id.substr(3);	
			
		}
		
		
		
		
		
		if (com=='Delete'){
			if(confirm("Are you sure, you want to delete selected data?")){
					
				j.post("company_hierarchy.php", {"delete":"yes","id":items[0].id.substr(3)},function(data)
				{		 
				  if(data!="") {					 
					 j('#flex1').flexOptions().flexReload(); 
				  }else {
					  alert("Record doesn't exists.")
				  }
				  
				});		
			}			
		}	
	}
}









/********************************    company User End *******************/






























function newDistributors(){
j("#flex1").flexigrid({
	url: 'new_distributors.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Name', name : 'distributor_name', width : 80, sortable : true, align: 'left'},
		{display: 'Division', name : 'division_name', width : 80, sortable : true, align: 'left'},
		{display: 'Distributor Code', name : 'distributor_code', width : 80, sortable : true, align: 'left'},
		{display: 'Distributor Class', name : 'relationship_code', width : 80, sortable : true, align: 'left'},
		// {display: 'Added by(Salesman)', name : 'salesman_name', width : 80, sortable : true, align: 'left'},
		// {display: 'Salesman Code', name : 'salesman_code', width : 80, sortable : true, align: 'left'},
		{display: 'Distributor Region', name : 'region_name', width : 92, sortable : true, align: 'left'},
		{display: 'State', name : 'state_name', width : 80, sortable : true, align: 'left'},
		{display: 'District', name : 'city_name', width : 80, sortable : true, align: 'left'},
		// {display: 'Taluka', name : 'taluka_name', width : 80, sortable : true, align: 'left'},
		{display: 'City', name : 'market_name', width : 100, sortable : true, align: 'left'},

		{display: 'Address 1', name : 'distributor_address', width : 100, sortable : true, align: 'left'},
		{display: 'Address 2', name : 'distributor_address2', width : 100, sortable : true, align: 'left'},
		{display: 'Send SMS Mobile No', name : 'sms_number', width : 100, sortable : true, align: 'left'},
		{display: 'Phone Number 1', name : 'distributor_phone_no', width : 80, sortable : true, align: 'left'},
		{display: 'Phone Number 2', name : 'distributor_phone_no2', width : 80, sortable : true, align: 'left'},
		{display: 'Phone Number 3', name : 'distributor_phone_no3', width : 80, sortable : true, align: 'left'},
		{display: 'Landline Number', name : 'distributor_leadline_no', width : 80, sortable : true, align: 'left'},
		{display: 'Contact Person 1', name : 'contact_person', width : 80, sortable : true, align: 'left'},
		{display: 'Contact Number 1', name : 'contact_number', width : 80, sortable : true, align: 'left'},
		{display: 'Contact Person 2', name : 'contact_person2', width : 80, sortable : true, align: 'left'},
		{display: 'Contact Number 2', name : 'contact_number2', width : 80, sortable : true, align: 'left'},
		{display: 'Contact Person 3', name : 'contact_person3', width : 80, sortable : true, align: 'left'},
		{display: 'Contact Number 3', name : 'contact_number3', width : 80, sortable : true, align: 'left'},
		{display: 'Username', name : 'username', width : 80, sortable : true, align: 'left'},
		{display: 'Email ID 1', name : 'distributor_email', width : 100, sortable : true, align: 'left'},
		{display: 'Email ID 2', name : 'distributor_email2', width : 100, sortable : true, align: 'left'},
		{display: 'Email ID 3', name : 'distributor_email3', width : 100, sortable : true, align: 'left'},
		{display: 'Date of Birth', name : 'distributor_dob', width : 80, sortable : true, align: 'left'},
		{display: 'Start Date', name : 'start_date', width : 80, sortable : true, align: 'left'},
		{display: 'End Date', name : 'end_date', width : 80, sortable : true, align: 'left'},
		{display: 'Distributor Status', name : 'status', width : 80, sortable : true, align: 'center'},
		{display: 'Login Status', name : 'loginStatus', width : 80, sortable : true, align: 'center'}
			
		],
	buttons : [
		/*{name: 'Add', bclass: 'add', onpress : eventHandlerDisAdminUser},
		{separator: true},*/		
		{name: 'Edit', bclass: 'edit', onpress : eventHandlerDisAdminUser},
		{separator: true}
		],
	searchitems : [
		{display: 'Name', name : 'distributor_name', isdefault: true},
		{display: 'Division', name : 'division_name'},
		{display: 'Username', name : 'username'},
		{display: 'Email ID', name : 'distributor_email'},
		{display: 'Send SMS Mobile No', name : 'sms_number'},
		{display: 'Phone Number', name : 'distributor_phone_no'},
		{display: 'State', name : 'state_name'},
		{display: 'District', name : 'city_name'},
		{display: 'Address', name : 'distributor_address'},
		{display: 'City', name : 'distributor_location'},
		{display: 'Contact Person', name : 'contact_person'},
		{display: 'Contact Number', name : 'contact_number'}
		],
	sortname: "distributor_name",
	sortorder: "asc",
	usepager: true,
	title: 'Distributors',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 1000,
	height: 'auto'
});  
}
function eventHandlerDisAdminUser(com,grid)
{
	if (com=='Add')	{
		location.href="distributors.php?add";	
	}else if (com=='Import')	{
		location.href="distributors.php?import";	
	}else if (com=='Export')	{
		location.href="export.inc.php?export_distributors_list";	
	}
	else{
		if( j('.trSelected',grid).length<=0 ||  j('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=j('.trSelected',grid);		
		if (com=='Edit'){
			location.href="distributors.php?id="+items[0].id.substr(3);			
		}else if (com=='Create Login')	{
		location.href="#.php?login&id="+items[0].id.substr(3);
	}
		else if (com=='Delete')	{
		if(confirm("Are you sure, you want to delete selected Distributor?")){
					
				j.post("distributors.php", {"delete":"yes","id":items[0].id.substr(3)},function(data)
				{
					j("#message-green").hide();
				  if(data!="") {
				  	//alert(data);
				  	if(data >0 )
				  	{				  		
				  		alert("This distributor cannot be deleted.");
				  	}
				  				 
					 j('#flex1').flexOptions().flexReload(); 
				  }else {
					  alert("Record doesn't exists.")
				  }
				  
				});		
			}
	}
			
	}
}


/******************************* NEW LEVEL OF APPROVAL in DISTRIBUTOR by CHIRAG 30 MAR 2017 ******************/

function newDistributorsComm(){
j("#flex1").flexigrid({
	url: 'new_distributors_comm.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Name', name : 'distributor_name', width : 80, sortable : true, align: 'left'},
		{display: 'Division', name : 'division_name', width : 80, sortable : true, align: 'left'},
		{display: 'Distributor Code', name : 'distributor_code', width : 80, sortable : true, align: 'left'},
		{display: 'Distributor Class', name : 'relationship_code', width : 80, sortable : true, align: 'left'},
		// {display: 'Added by(Salesman)', name : 'salesman_name', width : 80, sortable : true, align: 'left'},
		// {display: 'Salesman Code', name : 'salesman_code', width : 80, sortable : true, align: 'left'},
		{display: 'Distributor Region', name : 'region_name', width : 92, sortable : true, align: 'left'},
		{display: 'State', name : 'state_name', width : 80, sortable : true, align: 'left'},
		{display: 'District', name : 'city_name', width : 80, sortable : true, align: 'left'},
		// {display: 'Taluka', name : 'taluka_name', width : 80, sortable : true, align: 'left'},
		{display: 'City', name : 'market_name', width : 100, sortable : true, align: 'left'},

		{display: 'Address 1', name : 'distributor_address', width : 100, sortable : true, align: 'left'},
		{display: 'Address 2', name : 'distributor_address2', width : 100, sortable : true, align: 'left'},
		{display: 'Send SMS Mobile No', name : 'sms_number', width : 100, sortable : true, align: 'left'},
		{display: 'Phone Number 1', name : 'distributor_phone_no', width : 80, sortable : true, align: 'left'},
		{display: 'Phone Number 2', name : 'distributor_phone_no2', width : 80, sortable : true, align: 'left'},
		{display: 'Phone Number 3', name : 'distributor_phone_no3', width : 80, sortable : true, align: 'left'},


		{display: 'Party Strength', name : 'party_strength', width : 80, sortable : true, align: 'left'},
		{display: 'Tin Number', name : 'tin_number', width : 80, sortable : true, align: 'left'},
		{display: 'Party Turnover', name : 'party_turnover', width : 80, sortable : true, align: 'left'},
		{display: 'Old / New Party', name : 'old_new_party', width : 80, sortable : true, align: 'left'},
		{display: 'Party Industry', name : 'party_industry', width : 80, sortable : true, align: 'left'},
		{display: 'Party Relative', name : 'party_relative', width : 80, sortable : true, align: 'left'},
		// {display: 'Landline Number', name : 'distributor_leadline_no', width : 80, sortable : true, align: 'left'},
		{display: 'Contact Person 1', name : 'contact_person', width : 80, sortable : true, align: 'left'},
		{display: 'Contact Number 1', name : 'contact_number', width : 80, sortable : true, align: 'left'},
		{display: 'Contact Person 2', name : 'contact_person2', width : 80, sortable : true, align: 'left'},
		{display: 'Contact Number 2', name : 'contact_number2', width : 80, sortable : true, align: 'left'},
		{display: 'Contact Person 3', name : 'contact_person3', width : 80, sortable : true, align: 'left'},
		{display: 'Contact Number 3', name : 'contact_number3', width : 80, sortable : true, align: 'left'},
		{display: 'Username', name : 'username', width : 80, sortable : true, align: 'left'},
		{display: 'Email ID 1', name : 'distributor_email', width : 100, sortable : true, align: 'left'},
		{display: 'Email ID 2', name : 'distributor_email2', width : 100, sortable : true, align: 'left'},
		{display: 'Email ID 3', name : 'distributor_email3', width : 100, sortable : true, align: 'left'},
		{display: 'Date of Birth', name : 'distributor_dob', width : 80, sortable : true, align: 'left'},
		{display: 'Start Date', name : 'start_date', width : 80, sortable : true, align: 'left'},
		{display: 'End Date', name : 'end_date', width : 80, sortable : true, align: 'left'},
		{display: 'Distributor Status', name : 'status', width : 80, sortable : true, align: 'center'},
		{display: 'Login Status', name : 'loginStatus', width : 80, sortable : true, align: 'center'}
			
		],
	buttons : [
		/*{name: 'Add', bclass: 'add', onpress : eventHandlerDisCommUser},
		{separator: true},*/		
		{name: 'Edit', bclass: 'edit', onpress : eventHandlerDisCommUser},
		{separator: true}
		],
	searchitems : [
		{display: 'Name', name : 'distributor_name', isdefault: true},
		{display: 'Division', name : 'division_name'},
		{display: 'Username', name : 'username'},
		{display: 'Email ID', name : 'distributor_email'},
		{display: 'Send SMS Mobile No', name : 'sms_number'},
		{display: 'Phone Number', name : 'distributor_phone_no'},
		{display: 'State', name : 'state_name'},
		{display: 'District', name : 'city_name'},
		{display: 'Address', name : 'distributor_address'},
		{display: 'City', name : 'distributor_location'},
		{display: 'Contact Person', name : 'contact_person'},
		{display: 'Contact Number', name : 'contact_number'}
		],
	sortname: "distributor_name",
	sortorder: "asc",
	usepager: true,
	title: 'Distributors',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 1000,
	height: 'auto'
});  
}


function eventHandlerDisCommUser(com,grid)
{
	if (com=='Add')	{
		location.href="distributors.php?add";	
	}else if (com=='Import')	{
		location.href="distributors.php?import";	
	}else if (com=='Export')	{
		location.href="export.inc.php?export_distributors_list";	
	}
	else{
		if( j('.trSelected',grid).length<=0 ||  j('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=j('.trSelected',grid);		
		if (com=='Edit'){
			location.href="distributors.php?id="+items[0].id.substr(3);			
		}else if (com=='Create Login')	{
		location.href="#.php?login&id="+items[0].id.substr(3);
	}
		else if (com=='Delete')	{
		if(confirm("Are you sure, you want to delete selected Distributor?")){
					
				j.post("distributors.php", {"delete":"yes","id":items[0].id.substr(3)},function(data)
				{
					j("#message-green").hide();
				  if(data!="") {
				  	//alert(data);
				  	if(data >0 )
				  	{				  		
				  		alert("This distributor cannot be deleted.");
				  	}
				  				 
					 j('#flex1').flexOptions().flexReload(); 
				  }else {
					  alert("Record doesn't exists.")
				  }
				  
				});		
			}
	}
			
	}
}


/******************************* NEW LEVEL OF APPROVAL in DISTRIBUTOR by CHIRAG 30 MAR 2017 ******************/


// Added taluka and market data list


function showTaluka(){
j("#flex1").flexigrid({
	url: 'taluka.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Taluka', name : 'taluka_name', width : 100, sortable : true, align: 'left'},
		{display: 'Taluka Code', name : 'taluka_code', width : 100, sortable : true, align: 'left'},
		{display: 'District', name : 'city_name', width : 200, sortable : true, align: 'left'},
		{display: 'State Name', name : 'state_name', width : 100, sortable : true, align: 'left'},
		{display: 'Status', name : 'status', width : 100, sortable : true, align: 'left'},
		
		],
	buttons : [
		{name: 'Add', bclass: 'add', onpress : eventHandlerTaluka},
		{separator: true},		
		{name: 'Edit', bclass: 'edit', onpress : eventHandlerTaluka},
		{separator: true},
		{name: 'Delete', bclass: 'delete', onpress : eventHandlerTaluka},
		{separator: true},			
		{name: 'Import', bclass: 'import', onpress : eventHandlerTaluka},
		{separator: true},
		{name: 'Export', bclass: 'export', onpress : eventHandlerTaluka},
		{separator: true}
		
		],
	searchitems : [
		{display: 'Taluka', name : 'taluka_name', isdefault: true},
		{display: 'Taluka Code', name : 'taluka_code', isdefault: true},
		{display: 'District', name : 'city_name'},
		{display: 'State', name : 'state_name'}
		
		],
	sortname: "taluka_name",
	sortorder: "asc",
	usepager: true,
	title: 'Taluka',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 850,
	height: 'auto'
});  
}
function eventHandlerTaluka(com,grid)
{

	if (com=='Import')	{
		location.href="taluka_import.php";		
	}
	else if (com=='Export')	{
		location.href="export.inc.php?export_taluka_new";		
	}
	else if (com=='Add')	{
		location.href="add_taluka.php";		
	}
	else if (com=='delete')	{
		location.href="taluka.php";	}
		else{
		if( j('.trSelected',grid).length<=0 ||  j('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=j('.trSelected',grid);	
		
		
		if (com=='Edit'){
			location.href="add_taluka.php?id="+items[0].id.substr(3);			
		}
		if (com=='Delete'){
			if(confirm("Are you sure, you want to delete selected Taluka?")){
					
				j.post("taluka.php", {"delete":"yes","id":items[0].id.substr(3)},function(data)
				{		 
				   var res  = data.split("-?");
					if(res[0].trim()=="0") {					 
					 j('#flex1').flexOptions().flexReload(); 
					 alert('Taluka deleted');
				  }else {
					  alert("Taluka can not be deleted, it is mapped with "+res[0])
				  }
				  
				});		
			}			
		}
		
	}
}


function showMarkets(){
j("#flex1").flexigrid({
	url: 'market.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'City', name : 'market_name', width : 150, sortable : true, align: 'left'},
		{display: 'City Code', name : 'market_code', width : 150, sortable : true, align: 'left'},
		{display: 'Taluka', name : 'taluka_name', width : 150, sortable : true, align: 'left'},
		{display: 'District Name', name : 'city_name', width : 150, sortable : true, align: 'left'},
		{display: 'State', name : 'state_name', width : 150, sortable : true, align: 'left'},
		{display: 'Status', name : 'status', width : 150, sortable : true, align: 'left'},
		],
	buttons : [
		{name: 'Add', bclass: 'add', onpress : eventHandlerMarket},
		{separator: true},		
		{name: 'Edit', bclass: 'edit', onpress : eventHandlerMarket},
		{separator: true},
		{name: 'Delete', bclass: 'delete', onpress : eventHandlerMarket},
		{separator: true},			
		{name: 'Import', bclass: 'import', onpress : eventHandlerMarket},
		{separator: true},
		{name: 'Export', bclass: 'export', onpress : eventHandlerMarket},
		{separator: true}
		
		],
	searchitems : [
		{display: 'City', name : 'market_name', isdefault: true},
		{display: 'Taluka', name : 'taluka_name'},
		{display: 'District', name : 'city_name'},
		{display: 'State', name : 'state_name'}
		
		],
	sortname: "market_name",
	sortorder: "asc",
	usepager: true,
	title: 'City',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 850,
	height: 'auto'
});  
}
function eventHandlerMarket(com,grid)
{

	if (com=='Import')	{
		location.href="market_import.php";		
	}
	else if (com=='Export')	{
		location.href="export.inc.php?export_market_new";		
	}
	else if (com=='Add')	{
		location.href="add_market.php";		
	}
	else if (com=='delete')	{
		location.href="market.php";	}
		else{
		if( j('.trSelected',grid).length<=0 ||  j('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=j('.trSelected',grid);	
		
		
		if (com=='Edit'){
			location.href="add_market.php?id="+items[0].id.substr(3);			
		}
		if (com=='Delete'){
			if(confirm("Are you sure, you want to delete selected City?")){
					
				j.post("market.php", {"delete":"yes","id":items[0].id.substr(3)},function(data)
				{		 
				  var res  = data.split("-?");
					if(res[0].trim()=="0") {					 
					 j('#flex1').flexOptions().flexReload(); 
					 alert('City deleted');
				  }else {
					  alert("City can not be deleted, it is mapped with "+res[0])
				  }
				  
				});		
			}			
		}
		
	}
}



// Added taluka and market data list



/********************************** Division List 16 Oct 2015 Nizam *************************/

function showDivision(){
j("#flex1").flexigrid({
	url: 'division.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Division Name', name : 'division_name', width : 300, sortable : true, align: 'left'},
		{display: 'Status', name : 'status', width : 100, sortable : true, align: 'left'}
		],
	buttons : [
		{name: 'Add', bclass: 'add', onpress : eventHandlerDivision},
		{separator: true},		
		{name: 'Edit', bclass: 'edit', onpress : eventHandlerDivision},
		{separator: true}		
		],
	searchitems : [
		{display: 'Division Name', name : 'division_name', isdefault: true}
		
		],
	sortname: "division_name",
	sortorder: "asc",
	usepager: true,
	title: 'Division',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 700,
	height: 'auto'
});  
}
function eventHandlerDivision(com,grid)
{
	if (com=='Add')	{
		location.href="addDivision.php";		
	}else{
		if( j('.trSelected',grid).length<=0 ||  j('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=j('.trSelected',grid);		
		if (com=='Edit'){
			location.href="addDivision.php?id="+items[0].id.substr(3);			
		}
		if (com=='Delete'){
			if(confirm("Are you sure, you want to delete selected Branch?")){
					
				j.post("division.php", {"delete":"yes","id":items[0].id.substr(3)},function(data)
				{		 
				  if(data!="") {					 
					 j('#flex1').flexOptions().flexReload(); 
				  }else {
					  alert("Record doesn't exists.")
				  }
				  
				});		
			}			
		}	
	}
}


/********************************** Division List 16 Oct 2015 Nizam *************************/






/*********************************** retailer complaints show wearehouse ****************************************/


function retailerwearhousecomplaint(){
	
	j("#flex1").flexigrid({
		url: 'retailer_wearhouse_comp.php?show=yes',
		dataType: 'json',
		method : "GET", 
		singleSelect:true, 
		colModel : [		
		    {display: 'Retailer Name', name : 'retailer_name', width : 150, sortable : true, align: 'left'},
			{display: 'BSN', name : 'c.product_serial_no', width : 150, sortable : true, align: 'left'},
			
			{display: 'Purchase Date', name : 'pdate', width : 120, sortable : true, align: 'left'},	
			{display: 'complain status', name : 'cstatus', width : 120, sortable : true, align: 'left'},		
		    {display: 'servicepersion', name : 'servicepersion', width : 120, sortable : true, align: 'left'},	
			{display: 'Complaint Date', name : 'Complaint', width : 100, sortable : true, align: 'left'},
			{display: 'Assign Complaint', name : 'Acomplaint', width : 180, sortable : true, align: 'left'}
			],
		buttons : [
			// {name: 'Add', bclass: 'addhirh', onpress : eventHandlercomplaint},
			// {separator: true},		
			// {name: 'Edit', bclass: 'edithihr', onpress : eventHandlercomplaint},
			// {separator: true},
			{name: 'Delete', bclass: 'Deletehhir', onpress : eventHandlercomplaint},
			{separator: true}		
			
			],
		searchitems : [
		{display: 'BSN', name : 'c.product_serial_no', isdefault: true}],
		sortname: "c.product_serial_no",
		sortorder: "asc",
		usepager: true,
		title: 'Wearhouse Complaint',
		useRp: true,
		rp: 15,
		showTableToggleBtn: true,
		width: "900",
		height: "auto"
	});  
}



function eventHandlercomplaint(com,grid) {
	
	if (com=='Import')	{
		//location.href="import_item.php";	
		location.href="#";	
	}
	
		
	
	
	
	else if (com=='Export')	{
		location.href="export.inc.php?export_hierarchy";		
	}
	else if (com=='Add')	{
		location.href="raisecomplaint_deler.php";		
	}else{
		if( j('.trSelected',grid).length<=0 ||  j('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=j('.trSelected',grid);		
		if (com=='Edit')
		{
			//alert('HEllo');
			//alert("addhierarchy.php?id="+items[0].id.substr(3));
			//location.href="raisecomplaint_distributor.php?id="+items[0].id.substr(3);	
			
		}
		
		
		
		
		
		if (com=='Delete'){
			if(confirm("Are you sure, you want to delete selected data?")){
					
				j.post("distributor_wearhouse_comp.php", {"delete":"yes","id":items[0].id.substr(3)},function(data)
				{		 
				  if(data!="") {					 
					 j('#flex1').flexOptions().flexReload(); 
				  }else {
					  alert("Record doesn't exists.")
				  }
				  
				});		
			}			
		}	
	}
}
	


/*********************************** retailer complaints show wearehouse ****************************************/







/*********************************** wearhouse complaint show servicepersion  for retailer****************************************/



function wearhouse_servicepersion_retailer_complaint(){
	
	j("#flex1").flexigrid({
		url: 'service_retailer_complaint.php?show=yes',
		dataType: 'json',
		method : "GET", 
		singleSelect:true, 
		colModel : [		
		    
		    {display: 'wearhouse Name', name : 'wearhouse_name', width : 200, sortable : true, align: 'left'},
			{display: 'BSN', name : 'c.product_serial_no', width : 200, sortable : true, align: 'left'},
			
			{display: 'Purchase Date', name : 'pdate', width : 120, sortable : true, align: 'left'},	
			{display: 'complain status', name : 'cstatus', width : 120, sortable : true, align: 'left'},		
		
			{display: 'Complaint Date', name : 'Complaint', width : 100, sortable : true, align: 'left'},
		
			],
		buttons : [
			// {name: 'Add', bclass: 'addhirh', onpress : eventHandlercomplaint},
			// {separator: true},		
			// {name: 'Edit', bclass: 'edithihr', onpress : eventHandlercomplaint},
			// {separator: true},
			{name: 'Delete', bclass: 'Deletehhir', onpress : eventHandlercomplaint},
			{separator: true}		
			
			],
		searchitems : [
		{display: 'BSN', name : 'c.product_serial_no', isdefault: true}],
		sortname: "c.product_serial_no",
		sortorder: "asc",
		usepager: true,
		title: ' Retailer Complaint',
		useRp: true,
		rp: 15,
		showTableToggleBtn: true,
		width: "900",
		height: "auto"
	});  
}



function eventHandlercomplaint(com,grid) {
	
	if (com=='Import')	{
		//location.href="import_item.php";	
		location.href="#";	
	}
	
		
	
	
	
	else if (com=='Export')	{
		location.href="export.inc.php?export_hierarchy";		
	}
	else if (com=='Add')	{
	//	location.href="raisecomplaint_distributor.php";		
	}else{
		if( j('.trSelected',grid).length<=0 ||  j('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=j('.trSelected',grid);		
		if (com=='Edit')
		{
			//alert('HEllo');
			//alert("addhierarchy.php?id="+items[0].id.substr(3));
			//location.href="raisecomplaint_distributor.php?id="+items[0].id.substr(3);	
			
		}
		
		
		
		
		
		if (com=='Delete'){
			if(confirm("Are you sure, you want to delete selected data?")){
					
				j.post("service_retailer_complaint.php", {"delete":"yes","id":items[0].id.substr(3)},function(data)
				{		 
				  if(data!="") {					 
					 j('#flex1').flexOptions().flexReload(); 
				  }else {
					  alert("Record doesn't exists.")
				  }
				  
				});		
			}			
		}	
	}
}
	






/*********************************** wearhouse complaint show servicepersion ****************************************/














/********************************** Segment List 20 Jan 2017 Chirag *************************/

function showSegment(){
j("#flex1").flexigrid({
	url: 'segment.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Segment Name', name : 'segment_name', width : 300, sortable : true, align: 'left'},
		{display: 'Status', name : 'status', width : 100, sortable : true, align: 'left'}
		],
	buttons : [
		{name: 'Add', bclass: 'add', onpress : eventHandlerSegment},
		{separator: true},		
		{name: 'Edit', bclass: 'edit', onpress : eventHandlerSegment},
		{separator: true}
		],
	searchitems : [
		{display: 'Segment Name', name : 'segment_name', isdefault: true}
		],
	sortname: "segment_name",
	sortorder: "asc",
	usepager: true,
	title: 'Segment',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 700,
	height: 'auto'
});  
}
function eventHandlerSegment(com,grid)
{
	if (com=='Add')	{
		location.href="addSegment.php";		
	}else{
		if( j('.trSelected',grid).length<=0 ||  j('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=j('.trSelected',grid);		
		if (com=='Edit'){
			location.href="addSegment.php?id="+items[0].id.substr(3);			
		}
		if (com=='Delete'){
			if(confirm("Are you sure, you want to delete selected Segment?")){
					
				j.post("segment.php", {"delete":"yes","id":items[0].id.substr(3)},function(data)
				{		 
				  if(data!="") {					 
					 j('#flex1').flexOptions().flexReload(); 
				  }else {
					  alert("Record doesn't exists.")
				  }
				  
				});		
			}			
		}	
	}
}

function showJobcard(){
j("#flex1").flexigrid({
	url: 'jobcard.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Jobcard No', name : 'jobcard_no', width : 100, sortable : true, align: 'left'},
		{display: 'Jobcard Date', name : 'jobcard_date', width : 100, sortable : true, align: 'left'},
		{display: 'BCF No', name : 'bcf_no', width : 80, sortable : true, align: 'left'},
		{display: 'MRN No', name : 'mrn_no', width : 80, sortable : true, align: 'left'},
		{display: 'Status', name : 'status', width : 100, sortable : true, align: 'left'},
		{display: 'Job Card Filling Status', name : 'status', width : 200, sortable : true, align: 'left'},
		{display: 'Service Personnel', name : 'sp_name', width : 120, sortable : true, align: 'left'},
		
		{display: 'Service Distributor', name : 'distributor_name', width : 150, sortable : true, align: 'left'},
		{display: 'DAF Number', name : 'daf_no', width : 100, sortable : true, align: 'left'},
		{display: 'DAF Date', name : 'daf_date', width : 80, sortable : true, align: 'left'},
		{display: 'Physical Condition', name : 'failure_id', width : 120, sortable : true, align: 'left'},
		{display: 'OCV1', name : 'OCV1', width : 80, sortable : true, align: 'left'},
		{display: 'OCV2', name : 'OCV2', width : 80, sortable : true, align: 'left'},
		{display: 'Electric Level', name : 'level_code', width : 100, sortable : true, align: 'left'},
		{display: 'TOC', name : 'TOC', width : 100, sortable : true, align: 'left'},
		{display: 'Charging Start Date', name : 'charging_start_date', width : 80, sortable : true, align: 'left'},
		{display: 'Charging Removal Date', name : 'charging_removal_date', width : 80, sortable : true, align: 'left'},
		// {display: 'Charging Start Time', name : 'charging_start_time', width : 300, sortable : true, align: 'left'},
		// {display: 'Charging Removal Time', name : 'failure_id', width : 300, sortable : true, align: 'left'},
		{display: 'Total Charging Duration Days', name : 'total_charging_duration_days', width : 80, sortable : true, align: 'left'},
		{display: 'Total Charging Duration Hours', name : 'total_charging_duration_hours', width : 80, sortable : true, align: 'left'},
		{display: 'HRD Backup Type', name : 'hrd_backup_type', width : 100, sortable : true, align: 'left'},
		{display: 'Load Current', name : 'load_current', width : 80, sortable : true, align: 'left'},
		{display: 'Duration Backup', name : 'duration_backup', width : 80, sortable : true, align: 'left'},
		{display: 'End Voltage', name : 'end_voltage', width : 80, sortable : true, align: 'left'},
		{display: 'Pass / Fail', name : 'pass_fail', width : 80, sortable : true, align: 'left'},
		{display: 'Failure Remarks', name : 'failure_remarks', width : 80, sortable : true, align: 'left'},
		{display: 'Replacement Type', name : 'replacement_type', width : 80, sortable : true, align: 'left'},
		{display: 'Failure Description', name : 'failure_desc', width : 150, sortable : true, align: 'left'},
		{display: 'Test Report Date', name : 'test_report_date', width : 80, sortable : true, align: 'left'},
		{display: 'Test Report Action', name : 'test_report_action', width : 150, sortable : true, align: 'left'},
		],
	buttons : [
		{name: 'Add', bclass: 'add', onpress : eventHandlerJobcard},
		{separator: true},		
		{name: 'Edit', bclass: 'edit', onpress : eventHandlerJobcard},
		{separator: true}
		],
	searchitems : [
		{display: 'Jobcard Number', name : 'jobcard_no', isdefault: true},
		{display: 'Jobcard Status', name : 'under_process', isdefault: true}
		],
	sortname: "jobcard_no",
	sortorder: "asc",
	usepager: true,
	title: 'Jobcard',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 1000,
	height: 'auto'
});  
}

function eventHandlerJobcard(com,grid)
{
	if (com=='Add')	{
		location.href="addJobcard.php";		
	}else{
		if( j('.trSelected',grid).length<=0 ||  j('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=j('.trSelected',grid);		
		if (com=='Edit'){
			location.href="addJobcard.php?id="+items[0].id.substr(3);			
		}
		if (com=='Delete'){
			if(confirm("Are you sure, you want to delete selected Jobcard?")){
					
				j.post("jobcard.php", {"delete":"yes","id":items[0].id.substr(3)},function(data)
				{		 
				  if(data!="") {					 
					 j('#flex1').flexOptions().flexReload(); 
				  }else {
					  alert("Record doesn't exists.")
				  }
				  
				});		
			}			
		}	
	}
}
function showSalesmanPlannedRoute(){
j("#flex1").flexigrid({
	url: 'salesmanplanned_report.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel: [
	   	{display: 'Name of Employee', name : 'salesman_name', width : 80, sortable : true, align: 'left'},
	   	{display: 'Employee Code', name : 'salesman_code', width : 80, sortable : true, align: 'left'},
	   	{display: 'Designation', name : 'designation', width : 80, sortable : true, align: 'left'},
	   	{display: 'Year', name : 'year', width : 90, sortable : true, align: 'left'},
		{display: 'Month', name : 'month', width :130, sortable : true, align: 'left'},
		{display: 'Date', name : 'assign_day', width :150, sortable : true, align: 'left'},
		{display: 'Route Name', name :'route_name', width :100, sortable : true, align: 'left'},
		{display: 'Dealer Name', name : 'retailer_name', width :100, sortable : true, align: 'left'},
		{display: 'Dealer Code', name : 'retailer_code', width :100, sortable : true, align: 'left'},
		/*{display: 'Achived Yes/No', name : 'achived', width :100, sortable : true, align: 'left'},*/
		{display: 'Order Taken Yes/No', name : 'order_id', width :100, sortable : true, align: 'left'},
		{display: 'Dealer Name', name : 'distributor_name', width :100, sortable : true, align: 'left'},
		{display: 'Dealer Code', name : 'distributor_code', width :100, sortable : true, align: 'left'},
		/*{display: 'Achived Yes/No', name : 'yes', width :100, sortable : true, align: 'left'},*/
		{display: 'Order Taken Yes/No', name : 'k', width :100, sortable : true, align: 'left'}
		//{display: 'Total Sale', name : 'unplannedVisitedAccount', width :100, sortable : true, align: 'left'}
		],
	buttons : [			
			/*{name: 'View Detail', bclass: 'view', onpress : eventHandlerPLVsVS},*/
			//{separator: true}
		],
	searchitems : [
		{display: 'Salesman', name : 'salesman_name', isdefault: true}		
		],
	sortname: " RSD.assign_day ,RS.month",
	sortorder: "desc",
	usepager: true,
	title: 'Salesman Route Plan',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 1000,
	height: 'auto'
});  
}



	/***************************************************************************
	* DESC : get serial number scan by customer report
	* Author : Abhishek
	* Created : 2016-02-24
	*
	**/

function serialNoScan(){
j("#flex1").flexigrid({
	url: 'serial_no_scan_report.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'ID', name : 'id', width : 50, sortable : true, align: 'left'},
		{display: 'Shakti-Partner Name', name : 'customer_name', width : 150, sortable : true, align: 'left'},
		{display: 'Shakti-Partner Code', name : 'customer_code', width : 100, sortable : true, align: 'left'},
		{display: 'Employee Name', name : 'employee_name', width : 150, sortable : true, align: 'left'},
		{display: 'Employee Code', name : 'employee_code', width : 100, sortable : true, align: 'left'},
		{display: 'Customer Name', name : 'customer_name', width : 150, sortable : true, align: 'left'},
		{display: 'Customer Mobile Number', name : 'customer_mobile_number', width : 100, sortable : true, align: 'left'},
		{display: 'Serial Number', name : 'serial_no', width : 100, sortable : true, align: 'left'},
		{display: 'Date', name : 'date', width : 70, sortable : true, align: 'left'},
		{display: 'Time', name : 'time', width : 70, sortable : true, align: 'left'},
		{display: 'Status', name : 'status', width : 100, sortable : true, align: 'left'}
		],
	buttons : [
	    {name: 'Add', bclass: 'add', onpress : eventHandlerSerial},
		{separator: true},
	    {name: 'Update', bclass: 'update', onpress : eventHandlerSerial},
		{separator: true},
		{name: 'Import', bclass: 'import', onpress : eventHandlerSerial},
		{separator: true},
		{name: 'Export', bclass: 'export', onpress : eventHandlerSerial},
		{separator: true}
		
		],
	searchitems : [
		{display: 'Shakti-Partner Name', name : 'customer_name', isdefault: true},
		{display: 'Serial Number', name : 'serial_number'},
		{display: 'Shakti-Partner Code', name : 'customer_code'},
		{display: 'Status', name : 'serial_number_status'}
		
		],
	sortname: "serial_id",
	sortorder: "asc",
	usepager: true,
	title: 'Serial Number Scan Report',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 700,
	height: 'auto'
});  
}

function eventHandlerSerial(com,grid)
{
	if (com=='Import')	{
		location.href="scan_serial_import.php";
	}
	else if (com=='Export')	{
		location.href="export.inc.php?export_serial_no_scan";
	}
	else if (com=='Add')	{
		location.href="serial_number_scan.php?add";	
	}
	else{
		if( j('.trSelected',grid).length<=0 ||  j('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=j('.trSelected',grid);	
		if (com=='Update'){
			location.href="serial_number_scan.php?id="+items[0].id.substr(3);
	}
}
}
	
/*function eventHandlerPLVsVS(com,grid){

	if( j('.trSelected',grid).length<=0 ||  j('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
	}
		var items=j('.trSelected',grid);		
		if (com=='View Detail'){
			window.open("plannedvisitedtown_detail.php?id="+items[0].id.substr(3), '_blank');		
		}
}*/





/*********************************** Company Hierarchy Management ****************************************/

function companyshowhierarchy(){
	
	j("#flex1").flexigrid({
		url: 'company_hierarchy.php?show=yes',
		dataType: 'json',
		method : "GET", 
		singleSelect:true, 
		colModel : [		
			{display: 'Hierarchy', name : 'a.user_label', width : 270, sortable : true, align: 'left'},
			
			{display: 'Date', name : 'created', width : 120, sortable : true, align: 'left'},		
			{display: 'Status', name : 'status', width : 50, sortable : true, align: 'left'}
			],
		buttons : [
			{name: 'Add', bclass: 'addhir', onpress : eventHandlercompanyhierarchy},
			{separator: true},		
			{name: 'Edit', bclass: 'edithir', onpress : eventHandlercompanyhierarchy},
			{separator: true},
			{name: 'Delete', bclass: 'Deletehir', onpress : eventHandlercompanyhierarchy},
			{separator: true}		
			
			],
		searchitems : [
			{display: 'hierarchy', name : 'a.user_label', isdefault: true}],
		sortname: "a.user_label",
		sortorder: "asc",
		usepager: true,
		title: 'Sub Admin',
		useRp: true,
		rp: 15,
		showTableToggleBtn: true,
		width: "900",
		height: "auto"
	});  
}



function eventHandlercompanyhierarchy(com,grid) {
	
	if (com=='Import')	{
		//location.href="import_item.php";	
		location.href="#";	
	}
	
		
	
	
	
	else if (com=='Export')	{
		location.href="export.inc.php?export_hierarchy";		
	}
	else if (com=='Add')	{
		location.href="company_hierarchy_add.php";		
	}else{
		if( j('.trSelected',grid).length<=0 ||  j('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=j('.trSelected',grid);		
		if (com=='Edit')
		{
			//alert('HEllo');
			//alert("addhierarchy.php?id="+items[0].id.substr(3));
			location.href="company_hierarchy_add.php?id="+items[0].id.substr(3);	
			
		}
		
		
		
		
		
		if (com=='Delete'){
			if(confirm("Are you sure, you want to delete selected data?")){
					
				j.post("company_hierarchy.php", {"delete":"yes","id":items[0].id.substr(3)},function(data)
				{		 
				  if(data!="") {					 
					 j('#flex1').flexOptions().flexReload(); 
				  }else {
					  alert("Record doesn't exists.")
				  }
				  
				});		
			}			
		}	
	}
}
	
/*********************************** End of  company Hierarchy Management ****************************************/



/***********************************  complaint list of Retailer ****************************************/

function retailercomplaint(){
	
	j("#flex1").flexigrid({
		url: 'retailer_complaint.php?show=yes',
		dataType: 'json',
		method : "GET", 
		singleSelect:true, 
		colModel : [		
			{display: 'BSN Number.', name : 'c.product_serial_no', width : 270, sortable : true, align: 'left'},
			
			{display: 'Sale Date', name : 'pdate', width : 120, sortable : true, align: 'left'},		
			{display: 'status', name : 'status', width : 100, sortable : true, align: 'left'},
                        {display: 'Image', name : 'images', width : 120, sortable : true, align: 'left'},
			{display: 'Complaint Date', name : 'Complaint', width : 100, sortable : true, align: 'left'},
			{display: 'Replaced BSN', name : 'RBSN', width : 100, sortable : true, align: 'left'},
                        {display: 'Comment ', name : 'comment', width : 200, sortable : true, align: 'left'}
			],
		buttons : [
			{name: 'Add', bclass: 'addhir', onpress : eventHandlercomplaintretailer},
			{separator: true},		
			{name: 'Edit', bclass: 'edithir', onpress : eventHandlercomplaintretailer},
			{separator: true},
			{name: 'Delete', bclass: 'Deletehir', onpress : eventHandlercomplaintretailer},
			{separator: true}		
			
			],
		searchitems : [
			{display: 'BSN', name : 'c.product_serial_no', isdefault: true},
			{display: 'Complaint Date', name : 'c.created_date', isdefault: true},
			{display: 'Complaint Status', name : 'c.complain_status', isdefault: true}],
		sortname: "c.complaint_id",
		sortorder: "desc",
		usepager: true,
		title: 'Dealer Complaint',
		useRp: true,
		rp: 15,
		showTableToggleBtn: true,
		width: "900",
		height: "auto"
	});  
}



function eventHandlercomplaintretailer(com,grid) {
	
	if (com=='Import')	{
		//location.href="import_item.php";	
		location.href="#";	
	}
	
		
	
	
	
	else if (com=='Export')	{
		location.href="export.inc.php?export_hierarchy";		
	}
	else if (com=='Add')	{
		location.href="raisecomplaint_deler.php";		
	}else{
		if( j('.trSelected',grid).length<=0 ||  j('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=j('.trSelected',grid);		
		if (com=='Edit')
		{
			//alert('HEllo');
			//alert("addhierarchy.php?id="+items[0].id.substr(3));
			location.href="raisecomplaint_deler.php?id="+items[0].id.substr(3);	
			
		}
		
		
		
		
		
		if (com=='Delete'){
			if(confirm("Are you sure, you want to delete selected data?")){
					
				j.post("retailer_complaint.php", {"delete":"yes","id":items[0].id.substr(3)},function(data)
				{		 
				  if(data!="") {					 
					 j('#flex1').flexOptions().flexReload(); 
				  }else {
					  alert("Record doesn't exists.")
				  }
				  
				});		
			}			
		}	
	}
}
	
/*********************************** End of  complaint list of Retailer ****************************************/



/*********************************** Distrubutors complaints show wearehouse ****************************************/

function distributorwearhousecomplaint(){
	
	j("#flex1").flexigrid({
		url: 'distributor_wearhouse_comp.php?show=yes',
		dataType: 'json',
		method : "GET", 
		singleSelect:true, 
		colModel : [		
		    {display: 'Distributor Name', name : 'distributor_name', width : 150, sortable : true, align: 'left'},
		    {display: 'Complaint No', name : 'complaint_no', width : 150, sortable : true, align: 'left'},
		    {display: 'Complaint Date', name : 'created_date', width : 100, sortable : true, align: 'left'},
			{display: 'Battery Serial No.', name : 'c.product_serial_no', width : 150, sortable : true, align: 'left'},
			{display: 'Replaced BSN', name : 'Rproduct_serial_no', width : 150, sortable : true, align: 'left'},
			{display: 'Item Code', name : '', width : 120, sortable : true, align: 'left'},
			{display: 'Battery Used(In Months)', name : '', width : 120, sortable : true, align: 'left'},
			{display: 'ProRata Claim', name : '', width : 120, sortable : true, align: 'left'},
			{display: 'Warranty(In Months)', name : '', width : 120, sortable : true, align: 'left'},	
			{display: 'ProRata(In Months)', name : '', width : 120, sortable : true, align: 'left'},	
			{display: 'Date Of Sale', name : '', width : 120, sortable : true, align: 'left'},			
			//{display: 'Sale Date', name : 'pdate', width : 120, sortable : true, align: 'left'},	
			{display: 'Dealer Name', name : 'retailer_name', width : 150, sortable : true, align: 'left'},	
			// {display: 'BCF Status', name : '', width : 120, sortable : true, align: 'left'},
	        {display: 'Warehouse Name', name : 'warehouse_name', width : 120, sortable : true, align: 'left'},		
		    {display: 'Service Personnel', name : 'sp_name', width : 120, sortable : true, align: 'left'},
		    {display: 'Service Distributor', name : 'service_distributor_name', width : 120, sortable : true, align: 'left'},	
		    {display: 'Service Distributor SE', name : 'service_distributor_sp_name', width : 120, sortable : true, align: 'left'},
		    {display: 'Complaint Assigned Time', name : 'assigned_sp_time', width : 120, sortable : true, align: 'left'},
		    {display: 'Customer Name', name : 'customer_name', width : 120, sortable : true, align: 'left'},		
		    {display: 'Customer Phone No', name : 'customer_phone_no', width : 120, sortable : true, align: 'left'},		
		    {display: 'Customer City', name : 'SP_CITY', width : 120, sortable : true, align: 'left'},		
		    {display: 'Customer Address', name : 'CUST_ADDRESS', width : 120, sortable : true, align: 'left'},		
            {display: 'Image', name : '', width : 120, sortable : true, align: 'left'},			    
            {display: 'Comment ', name : '', width : 200, sortable : true, align: 'left'},
            {display: 'Auth Token ', name : 'auth_token', width : 200, sortable : true, align: 'left'},
            {display: 'Action Taken ', name : 'action_taken', width : 200, sortable : true, align: 'left'},
            {display: 'View BCF Form ', name : 'view_form', width : 200, sortable : true, align: 'left'},
			{display: 'Complaint Action', name : '', width : 180, sortable : true, align: 'left'},
			{display: 'Initial Reading Save Time ', name : 'initial_reading_save_time', width : 200, sortable : true, align: 'left'},
			{display: 'Call Close Datetime ', name : 'call_close_datetime', width : 200, sortable : true, align: 'left'},
			{display: 'Operator Name ', name : 'operator_name', width : 150, sortable : true, align: 'left'},
			{display: 'Lattitude ', name : 'lat', width : 100, sortable : true, align: 'left'},
			{display: 'Longitude ', name : 'lng', width : 100, sortable : true, align: 'left'},
			{display: 'Capture Mode ', name : 'lat_lng_capcure_by', width : 100, sortable : true, align: 'left'},
			{display: 'Capture Accuracy ', name : 'lat_lng_capcure_accuracy', width : 150, sortable : true, align: 'left'},
			{display: 'Complaint Status', name : '', width : 200, sortable : true, align: 'left'}
			],
		buttons : [
			// {name: 'Add', bclass: 'addhirh', onpress : eventHandlercomplaintbattery},
			// {separator: true},	
			// // {name: 'Edit', bclass: 'edithirh', onpress : eventHandlercomplaintbattery},
			// // {separator: true},		
			// {name: 'View', bclass: 'viewhihr', onpress : eventHandlercomplaintbattery},
			// {separator: true},
			// {name: 'Delete', bclass: 'Deletehhir', onpress : eventHandlercomplaintbattery},
			// {separator: true}		

			// AJAY@017-04-28

			{name: 'Add', bclass: 'add', onpress : eventHandlercomplaintbattery},
			{separator: true},	
			// {name: 'Edit', bclass: 'edithirh', onpress : eventHandlercomplaintbattery},
			// {separator: true},		
			{name: 'View', bclass: 'view', onpress : eventHandlercomplaintbattery},
			{separator: true},
			{name: 'Export', bclass: 'export', onpress : eventHandlercomplaintbattery},
			{separator: true},
			{name: 'Delete', bclass: 'delete', onpress : eventHandlercomplaintbattery},
			{separator: true}	



			// AJAY@017-04-28
			
			],
		searchitems : [
		{display: 'Battery Serial No.', name : 'c.product_serial_no', isdefault: true},
		{display: 'Complaint Date', name : 'c.created_date', isdefault: true},
		{display: 'Complaint Number', name : 'c.complaint_no', isdefault: true},
		// {display: 'Complaint Status', name : 'c.complain_status', isdefault: true},
		{display: 'Distributor Name', name : 'distributor_name', isdefault: true},
		{display: 'Dealer Name', name : 'retailer_name', isdefault: true}
		],
		sortname: "c.complaint_id",
		sortorder: "desc",
		usepager: true,
		title: 'Complaint List',
		useRp: true,
		rp: 20,
		showTableToggleBtn: true,
		width: "900",
		height: "auto"
	});  
}



function eventHandlercomplaintbattery(com,grid) {
	var items=j('.trSelected',grid);
	if (com=='Import')	{
		//location.href="import_item.php";	
		location.href="#";	
	}
	
	else if (com=='View')
	{
		location.href="view_complaint.php?id="+items[0].id.substr(3);			
	}	
	
	
	
	else if (com=='Export')	{
		location.href="export.inc.php?export_asd_complaints";		
	}
	else if (com=='Add')	{
		location.href="raisecomplaint_distributor.php";		
	}else{
		if( j('.trSelected',grid).length<=0 ||  j('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		// var items=j('.trSelected',grid);		
		if (com=='Edit')
		{
			location.href="raisecomplaint_distributor.php?id="+items[0].id.substr(3);	
			
		}
		
		
		
		
		
		if (com=='Delete'){
			if(confirm("Are you sure, you want to delete selected data?")){
					
				j.post("distributor_wearhouse_comp.php", {"delete":"yes","id":items[0].id.substr(3)},function(data)
				{		 
				  if(data!="") {					 
					 j('#flex1').flexOptions().flexReload(); 
				  }else {
					  alert("Record doesn't exists.")
				  }
				  
				});		
			}			
		}	
	}
}
	


/*********************************** Distrubutors complaints show wearehouse ****************************************/




/*********************************** wearhouse complaint show servicepersion ****************************************/



function wearhouse_servicepersion_complaint(){
	
	j("#flex1").flexigrid({
		url: 'servicepersion_complaint.php?show=yes',
		dataType: 'json',
		method : "GET", 
		singleSelect:true, 
		colModel : [		
		    
		    {display: 'wearhouse Name', name : 'wearhouse_name', width : 200, sortable : true, align: 'left'},
			{display: 'BSN', name : 'c.product_serial_no', width : 200, sortable : true, align: 'left'},
			
			{display: 'Purchase Date', name : 'pdate', width : 120, sortable : true, align: 'left'},	
			{display: 'complain status', name : 'cstatus', width : 120, sortable : true, align: 'left'},		
		
			{display: 'Complaint Date', name : 'Complaint', width : 100, sortable : true, align: 'left'},
			{display: 'status', name : 'status_change', width : 200, sortable : true, align: 'left'},
		
			],
		buttons : [
			// {name: 'Add', bclass: 'addhirh', onpress : eventHandlercomplaint},
			// {separator: true},		
			// {name: 'Edit', bclass: 'edithihr', onpress : eventHandlercomplaint},
			// {separator: true},
			{name: 'Delete', bclass: 'Deletehhir', onpress : eventHandlercomplaint},
			{separator: true}		
			
			],
		searchitems : [
		{display: 'BSN', name : 'c.product_serial_no', isdefault: true}],
		sortname: "c.product_serial_no",
		sortorder: "asc",
		usepager: true,
		title: 'Complaint',
		useRp: true,
		rp: 15,
		showTableToggleBtn: true,
		width: "900",
		height: "auto"
	});  
}



function eventHandlercomplaint(com,grid) {
	
	if (com=='Import')	{
		//location.href="import_item.php";	
		location.href="#";	
	}
	
		
	
	
	
	else if (com=='Export')	{
		location.href="export.inc.php?export_hierarchy";		
	}
	else if (com=='Add')	{
	//	location.href="raisecomplaint_distributor.php";		
	}else{
		if( j('.trSelected',grid).length<=0 ||  j('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=j('.trSelected',grid);		
		if (com=='Edit')
		{
			//alert('HEllo');
			//alert("addhierarchy.php?id="+items[0].id.substr(3));
			//location.href="raisecomplaint_distributor.php?id="+items[0].id.substr(3);	
			
		}
		
		
		
		
		
		if (com=='Delete'){
			if(confirm("Are you sure, you want to delete selected data?")){
					
				j.post("servicepersion_complaint.php", {"delete":"yes","id":items[0].id.substr(3)},function(data)
				{		 
				  if(data!="") {					 
					 j('#flex1').flexOptions().flexReload(); 
				  }else {
					  alert("Record doesn't exists.")
				  }
				  
				});		
			}			
		}	
	}
}
	






/*********************************** wearhouse complaint show servicepersion ****************************************/






	

/*******************************************************
* DESC: Show Warehose Stock
* Author: AJAY@2017-02-24
*
*
*
********************************************************/


function showWarehouseStock(){
j("#flex1").flexigrid({
	url: 'warehouse_stock.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Warehouse Name', name : 'warehouse_name', width : 150, sortable : true, align: 'left'},
		{display: 'Warehouse Code', name : 'warehouse_code', width : 100, sortable : true, align: 'left'},
		{display: 'Status', name : 'status', width : 80, sortable : true, align: 'left'},
		{display: 'Tally MasterId', name : '', width : 100, sortable : true, align: 'left'},
		 {display: 'warehouse recept ID', name : 'recept_id', width : 120, sortable : true, align: 'left'},
		{display: 'Recharged Date', name : 'stk_recharged_date', width : 80, sortable : true, align: 'left'},
		{display: 'BSN', name : 'bsn', width : 150, sortable : true, align: 'left'},
		{display: 'Distributor Name', name : 'distributor_name', width : 150, sortable : true, align: 'left'},
		{display: 'Distributor Code', name : 'distributor_code', width : 150, sortable : true, align: 'left'},
		{display: 'Item Code', name : 'item_code', width : 100, sortable : true, align: 'left'},
		{display: 'Item Name', name : 'item_name', width : 150, sortable : true, align: 'left'},
		{display: 'Category Name', name : 'category_name', width : 150, sortable : true, align: 'left'},
		{display: 'Manufacture Date', name : 'manufacture_date', width : 100, sortable : true, align: 'left'},
		{display: 'Warranty Period', name : 'warranty_period', width : 100, sortable : true, align: 'left'},
		{display: 'ProRata Period', name : 'warranty_prodata', width : 100, sortable : true, align: 'left'},
		{display: 'Grace Period', name : 'warranty_grace_period', width : 100, sortable : true, align: 'left'},
		{display: 'Last Update Date', name : 'last_updated_date', width : 100, sortable : true, align: 'left'}
                
		
		],
	buttons : [
		{name: 'Scan/Accept Factory BSN', bclass: 'Scan', onpress : eventHandlerWarehouseStock},
		 {separator: true},		
		 {name: 'Scan/Accept WH Transferred BSN', bclass: 'Scan', onpress : eventHandlerWarehouseStock},
		 {separator: true},			
		// {name: 'Edit', bclass: 'edit', onpress : eventHandlerMasterCategory},
		// {separator: true},
		// {name: 'Set Margin', bclass: 'margin', onpress : eventHandlerMasterCategory},
		// {separator: true}
		],
	searchitems : [
		{display: 'Category Name', name : 'category_name', isdefault: true},
		{display: 'Battery Serial No', name : 'bsn', isdefault: true},
		{display: 'Warehouse Code', name : 'warehouse_code'},
		{display: 'Warehouse Name', name : 'warehouse_name'},
		{display: 'Item Code', name : 'item_code'},
		{display: 'Item Name', name : 'item_name'}
		
		],
	sortname: "W.warehouse_name",
	sortorder: "asc",
	usepager: true,
	title: 'Warehouse Stock',
	useRp: true,
	rp: 40,
	showTableToggleBtn: true,
	width: '740',
	height: 'auto'
});  
}



function eventHandlerWarehouseStock(com,grid)
{
	//alert(com);
	if (com=='Scan/Accept Factory BSN')	{
		location.href="warehouse_scan_battery.php";		
	}else if (com=='Scan/Accept WH Transferred BSN')	{
		location.href="warehouse_scan_battery_transferred.php";		
	}else{
		if( j('.trSelected',grid).length<=0 ||  j('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=j('.trSelected',grid);	
		
		if (com=='Set Margin'){
			location.href="master_category_margin.php?id="+items[0].id.substr(3);			
		} else if (com=='Edit'){
			location.href="add_master_category.php?id="+items[0].id.substr(3);			
		} else if (com=='Delete'){
			if(confirm("Are you sure, you want to delete selected Category?")){
					
				j.post("master_category.php", {"delete":"yes","id":items[0].id.substr(3)},function(data)
				{		 
				  if(data!="") {					 
					 j('#flex1').flexOptions().flexReload(); 
				  }else {
					  alert("Record doesn't exists.")
				  }
				  
				});		
			}			
		}	
	}
}

function showBCSettlement(){
j("#flex1").flexigrid({
	url: 'BCS.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'BCS Number', name : 'bcs_number', width : 80, sortable : true, align: 'left'},
		{display: 'BCS Date', 	name : 'bcs_date', 	 width : 80, sortable : true, align: 'left'},
		{display: 'BCS Status', name : 'bcs_number', width : 80, sortable : true, align: 'left'},
		{display: 'JobCard No', name : 'jobcard_no', width : 80, sortable : true, align: 'left'},
		{display: 'BCF No', name : 'bcf_no', width : 80, sortable : true, align: 'left'},
		{display: 'MRN No', name : 'mrn_no', width : 80, sortable : true, align: 'left'},
                {display: 'TallyMasterId', name : 'masterid', width : 80, sortable : true, align: 'left'},
		{display: 'Complaint No.', name : 'complaint_id', width : 80, sortable : true, align: 'left'},
		{display: 'Defective BSN', name : 'defective_battery_bsn', width : 120, sortable : true, align: 'left'},
		{display: 'WH Replaced BSN', name : 'rpl_battery_bsn', width : 150, sortable : true, align: 'left'},
		{display: 'Replaced BSN by Del/Dis', name : 'rpl_battery_bsn', width : 150, sortable : true, align: 'left'},
		{display: 'Distributor Name', name : 'distributor_name', width : 150, sortable : true, align: 'left'},
		{display: 'Dealer Name', name : 'retailer_name', width : 150, sortable : true, align: 'left'},
		{display: 'Warehouse Name', name : 'retailer_name', width : 150, sortable : true, align: 'left'},
		{display: 'Good to Defective', name : '', width : 150, sortable : true, align: 'left'},
		{display: 'Defective BSN Delivery Date', name : 'defective_battery_delivery_date', width : 150, sortable : true, align: 'left'},
		{display: 'Replace Battery Action', name : 'rpl_battery_action', width : 110, sortable : true, align: 'left'},
		{display: 'Replacement Type', name : 'rpl_battery_action', width : 110, sortable : true, align: 'left'},
		{display: 'Brand', name : 'brand_name', width : 80, sortable : true, align: 'left'},
		{display: 'Category', name : 'category_name', width : 80, sortable : true, align: 'left'},
		{display: 'Color', name : 'color_code', width : 80, sortable : true, align: 'left'},
		{display: 'Transporter', name : 'delivery_chln_trans', width : 100, sortable : true, align: 'left'},
		{display: 'Transporter Mode', name : 'delivery_chln_trans_mode', width : 100, sortable : true, align: 'left'},
		{display: 'Invoice No', name : 'delivery_chln_eapl_invoice', width : 80, sortable : true, align: 'left'},
		{display: 'Invoice Date', name : 'delivery_chln_eapl_invoice_date', width : 80, sortable : true, align: 'left'},
		{display: 'Permit No', name : 'delivery_chln_road_permit_number', width : 80, sortable : true, align: 'left'},
		{display: 'Vehicle No', name : 'delivery_chln_vehicle_number', width : 80, sortable : true, align: 'left'},
		{display: 'Challan Date', name : 'delivery_chaln_date', width : 100, sortable : true, align: 'left'},
		{display: 'G.R. Number', name : 'delivery_chaln_gr_no', width : 80, sortable : true, align: 'left'},
		{display: 'Contact', name : 'delivery_chaln_contact', width : 80, sortable : true, align: 'left'},
		//{display: 'Status', name : 'status', width : 100, sortable : true, align: 'left'}
		],
	buttons : [
		{name: 'Add', bclass: 'add', onpress : eventHandlerBCS},
		{separator: true},		
		{name: 'Edit', bclass: 'edit', onpress : eventHandlerBCS},
		{separator: true}
		],
	searchitems : [
		{display: 'BCS Number', name : 'bcs_number', isdefault: true}
		],
	sortname: "bcs_number",
	sortorder: "asc",
	usepager: true,
	title: 'Battery Claim Settlement',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 1000,
	height: 'auto'
});  
}

function eventHandlerBCS(com,grid)
{
	if (com=='Add')	{
		location.href="addBCS.php";		
	}else{
		if( j('.trSelected',grid).length<=0 ||  j('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=j('.trSelected',grid);		
		if (com=='Edit'){
			location.href="addBCS.php?id="+items[0].id.substr(3);			
		}
		if (com=='Delete'){
			if(confirm("Are you sure, you want to delete selected BCS?")){
					
				j.post("BCS.php", {"delete":"yes","id":items[0].id.substr(3)},function(data)
				{		 
				  if(data!="") {					 
					 j('#flex1').flexOptions().flexReload(); 
				  }else {
					  alert("Record doesn't exists.")
				  }
				  
				});		
			}			
		}	
	}
}









/*******************************************************
* DESC: End of Show Warehose Stock
* Author: AJAY@2017-02-24
*
*
*
********************************************************/


/*******************************************************
* DESC: Show Warehose MRN
* Author: AJAY@2017-03-02
*
*
*
********************************************************/

function showWarehouseMRN(){
j("#flex1").flexigrid({
	url: 'warehouse_mrn.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Warehouse Name', name : 'warehouse_name', width : 150, sortable : true, align: 'left'},
		{display: 'Warehouse Code', name : 'warehouse_code', width : 100, sortable : true, align: 'left'},
		{display: 'MRN N0.', name : 'mrn_no', width : 80, sortable : true, align: 'left'},
		{display: 'MRN Date', name : 'mrn_date', width : 120, sortable : true, align: 'left'},
		{display: 'Service Distributor', name : 'service_distributor_id', width : 120, sortable : true, align: 'left'},
		{display: 'Recieving Mode', name : 'receiving_mode', width : 150, sortable : true, align: 'left'},
		{display: 'Challan No', name : 'challan_no', width : 100, sortable : true, align: 'left'},
		{display: 'Challan Date', name : 'challan_date', width : 150, sortable : true, align: 'left'},
		{display: 'Total BSN', name : 'last_updated_date', width : 100, sortable : true, align: 'left'},
                {display: 'TallyMasterId', name : 'masterid', width : 100, sortable : true, align: 'left'},
		
		],
	buttons : [
		{name: 'Add MRN', bclass: 'Add', onpress : eventHandlerWarehouseMRN},
		 {separator: true},		
		 {name: 'Edit', bclass: 'edit', onpress : eventHandlerWarehouseMRN},
		 {separator: true},
		 {name: 'View MRN', bclass: 'view', onpress : eventHandlerWarehouseMRN},
		 {separator: true}
		],
	searchitems : [
		{display: 'Category Name', name : 'category_name', isdefault: true},
		{display: 'Warehouse Code', name : 'warehouse_code'},
		{display: 'Warehouse Name', name : 'warehouse_name'},
		{display: 'Item Code', name : 'item_code'},
		{display: 'Item Name', name : 'item_name'}
		
		],
	sortname: "W.warehouse_name",
	sortorder: "asc",
	usepager: true,
	title: 'Warehouse MRN',
	useRp: true,
	rp: 40,
	showTableToggleBtn: true,
	width: '900',
	height: 'auto'
});  
}



function eventHandlerWarehouseMRN(com,grid)
{
	//alert(com);
	if (com=='Add MRN')	{
		location.href="warehouse_mrn.php?action=add";		
	}else{
		if( j('.trSelected',grid).length<=0 ||  j('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=j('.trSelected',grid);	
		
		if (com=='View MRN'){
			location.href="warehouse_mrn.php?action=view&id="+items[0].id.substr(3);			
		} else if (com=='Edit'){
			location.href="warehouse_mrn.php?action=edit&id="+items[0].id.substr(3);			
		} else if (com=='Delete'){
			if(confirm("Are you sure, you want to delete selected Category?")){
					
				j.post("master_category.php", {"delete":"yes","id":items[0].id.substr(3)},function(data)
				{		 
				  if(data!="") {					 
					 j('#flex1').flexOptions().flexReload(); 
				  }else {
					  alert("Record doesn't exists.")
				  }
				  
				});		
			}			
		}	
	}
}

/*********************************** Start IP Logging of Warehouses by Chirag Gupta 11 April, 2017 ******************************/

function showIpLogging(){
j("#flex1").flexigrid({
	url: 'ip_logging.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Warehouse Name', name : 'warehouse_name', width : 200, sortable : true, align: 'left'},
		{display: 'Warehouse City', name : 'city_name', width : 150, sortable : true, align: 'left'},
		{display: 'Warehouse State', name : 'state_name', width : 150, sortable : true, align: 'left'},
		{display: 'IP Address', name : 'ip_address', width : 120, sortable : true, align: 'left'},
		{display: 'Login Date', name : 'login_date', width : 110, sortable : true, align: 'left'},
		{display: 'Login Time', name : 'login_time', width : 110, sortable : true, align: 'left'}
		],
	buttons : [
		// {name: 'Add', bclass: 'add', onpress : eventHandlerSegment},
		// {separator: true},		
		// {name: 'Edit', bclass: 'edit', onpress : eventHandlerSegment},
		// {separator: true}
		],
	searchitems : [
			{display: 'Warehouse Name', name : 'warehouse_name', isdefault: true},
			{display: 'City', name : 'city_name'},
			{display: 'State', name : 'state_name'},
			{display: 'Ip Address', name : 'ip_address'},
			{display: 'Login Date', name : 'login_date'},
			{display: 'Login Time', name : 'login_time'}
			],
	sortname: "login_date",
	sortorder: "desc",
	usepager: true,
	title: 'IP Logging Details',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 900,
	height: 'auto'
});  
}


/*********************************** End IP Logging of Warehouses by Chirag Gupta 11 April, 2017 ******************************/




/*******************************************************
* DESC: End of Show Warehose MRN
* Author: AJAY@2017-03-02
*
*
*
********************************************************/

















/*******************************************************
* DESC: Show Warehose MRN BCF
* Author: AJAY@2017-03-03
*
*
*
********************************************************/


function showWarehouseBCF(){
j("#flex1").flexigrid({
	url: 'warehouse_bcf.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Warehouse Name', name : 'warehouse_name', width : 100, sortable : true, align: 'left'},
		{display: 'Warehouse Code', name : 'warehouse_code', width : 100, sortable : true, align: 'left'},
		{display: 'Service Personnel', name : 'sp_name', width : 100, sortable : true, align: 'left'},
		{display: 'MRN N0.', name : 'mrn_no', width : 80, sortable : true, align: 'left'},
		{display: 'MRN Date', name : 'mrn_date', width : 100, sortable : true, align: 'left'},
		{display: 'Jobcard No', name : 'bcf_no', width : 80, sortable : true, align: 'left'},
		{display: 'Jobcard Date', name : 'bcf_date', width : 100, sortable : true, align: 'left'},
		{display: 'BSN', name : 'bsn_no', width : 120, sortable : true, align: 'left'},
		{display: 'Jobcard Status', name : 'under_process', width : 180, sortable : true, align: 'left'},
		{display: 'Item Code', name : 'item_code', width : 120, sortable : true, align: 'left'},
		{display: 'Sale Distributor', name : 'sale_distributor_id', width : 120, sortable : true, align: 'left'},
		{display: 'Service Distributor', name : 'service_distributor_id', width : 120, sortable : true, align: 'left'},
		{display: 'Status', name : 'status', width : 100, sortable : true, align: 'left'},
		
		],
	buttons : [
		{name: 'Add', bclass: 'Add', onpress : eventHandlerWarehouseBCF},
		 {separator: true},		
		 // {name: 'Edit', bclass: 'edit', onpress : eventHandlerWarehouseBCF},
		 // {separator: true},
		 {name: 'Jobcard', bclass: 'jobcard', onpress : eventHandlerWarehouseBCF},
		 {separator: true}
		],
	searchitems : [
		//{display: 'Category Name', name : 'category_name', isdefault: true},
		{display: 'Warehouse Code', name : 'warehouse_code',  isdefault: true},
		{display: 'Warehouse Name', name : 'warehouse_name'},
		{display: 'Item Code', name : 'item_code'},
		{display: 'Item Name', name : 'item_name'}
		
		],
	sortname: "W.warehouse_name",
	sortorder: "asc",
	usepager: true,
	title: 'Jobcards',
	useRp: true,
	rp: 40,
	showTableToggleBtn: true,
	width: '900',
	height: 'auto'
});  
}



function eventHandlerWarehouseBCF(com,grid)
{
	//alert(com);
	if (com=='Add')	{
		location.href="addJobcard.php";		
	}else{
		if( j('.trSelected',grid).length<=0 ||  j('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=j('.trSelected',grid);	
		
		if (com=='View MRN'){
			location.href="warehouse_bcf.php?action=view&id="+items[0].id.substr(3);			
		} else if (com=='Edit'){
			location.href="add_bcf.php?id="+items[0].id.substr(3);			
		} else if (com=='jobcard'){
			location.href="addJobcard.php?job_card_id="+items[0].id.substr(3);			
		} else if (com=='Jobcard'){
			//console.log(items);
			//alert(items[0].sid);
			if(parseInt(items[0].sid)>0)
				location.href="addJobcard.php?bcf_id="+items[0].id.substr(3)+"&id="+items[0].sid;			
			else 
				location.href="addJobcard_mrn.php?bcf_id="+items[0].id.substr(3);
		} else if (com=='Delete'){
			if(confirm("Are you sure, you want to delete selected Category?")){
					
				j.post("master_category.php", {"delete":"yes","id":items[0].id.substr(3)},function(data)
				{		 
				  if(data!="") {					 
					 j('#flex1').flexOptions().flexReload(); 
				  }else {
					  alert("Record doesn't exists.")
				  }
				  
				});		
			}			
		}	
	}
}






/*******************************************************
* DESC: End of Show Warehose MRN
* Author: AJAY@2017-03-02
*
*
*
********************************************************/


// SalesEntryReport List

function showSalesReport(){

j("#flex1").flexigrid({
	url: 'sales_report.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Customer Name', name : 'customer_name', width : 100, sortable : true, align: 'left'},
		{display: 'Customer Address', name : 'customer_address', width : 100, sortable : true, align: 'left'},
		{display: 'Customer Phone No', name : 'customer_phone_no', width : 100, sortable : true, align: 'left'},
		{display: 'BSN', name : 'bsn', width : 100, sortable : true, align: 'left'},
		{display: 'Amount', name : 'price', width : 100, sortable : true, align: 'left'},
		{display: 'Date of Sale', name : 'sal_date', width : 100, sortable : true, align: 'left'},
		{display: 'Dealer Name', name : 'retailer_name', width : 100, sortable : true, align: 'left'},
		{display: 'Dealer Address', name : 'retailer_address', width : 100, sortable : true, align: 'left'},
		{display: 'Dealer Code', name : 'retailer_code', width : 80, sortable : true, align: 'left'},
		{display: 'Dealer Email', name : 'retailer_email', width : 100, sortable : true, align: 'left'},
		{display: 'Dealer Phone No', name : 'retailer_phone_no', width : 100, sortable : true, align: 'left'},
		{display: 'Distributor Name', name : 'distributor_name', width : 100, sortable : true, align: 'left'},
		{display: 'Distributor Address', name : 'distributor_address', width : 100, sortable : true, align: 'left'},
		{display: 'Sales Entry Date', name : 'Entry_date', width : 100, sortable : true, align: 'left'},
		{display: 'Distributor Code', name : 'distributor_code', width : 100, sortable : true, align: 'left'},
		{display: 'Distributor Email', name : 'distributor_email', width : 100, sortable : true, align: 'left'},
		{display: 'Distributor Phone No', name : 'distributor_phone_no', width : 100, sortable : true, align: 'left'}	
		],
	buttons : [
		// {name: 'View', bclass: 'viewmsg', onpress : eventHandlerMarketIntelligence},
		// {separator: true}
		],
	searchitems : [
				{display: 'Customer Name', name : 'customer_name', isdefault: true},
		{display: 'Date of Sale', name : 'date_of_order'},
		{display: 'BSN', name : 'bsn'}
		],
	sortname: "date_of_order",
	sortorder: "desc",
	usepager: true,
	title: 'Sales Report',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 1010,
	height: 'auto'
});  
}

// Show SurveyQuestions

function showSurveyQuestion(){
j("#flex1").flexigrid({
	url: 'survey_questions.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		
		{display: 'Question', name : 'question', width : 400, sortable : true, align: 'left'},
		{display: 'Action', name : '', width : 200, sortable : true, align: 'left'}
		
		],
	buttons : [
		
		
		],
	searchitems : [
		{display: 'Question', name : 'question', isdefault: true},
		],
	sortname: "sur_que_id",
	sortorder: "desc",
	usepager: true,
	title: 'Survey Question',
	useRp: true,
	rp: 15,
	showTableToggleBtn: true,
	width: '1000',
	height: 'auto'
});  
}

//Show Segment ChartApp

function showSegmentChartApp(){
j("#flex1").flexigrid({
	url: 'segment_chart_app.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Segment Name', name : 'segment_name', width : 300, sortable : true, align: 'left'},
		{display: 'Status', name : 'status', width : 100, sortable : true, align: 'left'}
		],
	buttons : [
		{name: 'Add', bclass: 'add', onpress : eventHandlerSegmentChartApp},
		{separator: true},		
		{name: 'Edit', bclass: 'edit', onpress : eventHandlerSegmentChartApp},
		{separator: true}
		],
	searchitems : [
		{display: 'Segment Name', name : 'segment_name', isdefault: true}
		],
	sortname: "segment_name",
	sortorder: "asc",
	usepager: true,
	title: 'Segment Chart App',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 700,
	height: 'auto'
});  
}

function eventHandlerSegmentChartApp(com,grid)
{
	if (com=='Add')	{
		location.href="addSegmentChartApp.php";		
	}else{
		if( j('.trSelected',grid).length<=0 ||  j('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=j('.trSelected',grid);		
		if (com=='Edit'){
			location.href="addSegmentChartApp.php?id="+items[0].id.substr(3);			
		}
		if (com=='Delete'){
			if(confirm("Are you sure, you want to delete selected Segment?")){
					
				j.post("segment_chart_app.php", {"delete":"yes","id":items[0].id.substr(3)},function(data)
				{		 
				  if(data!="") {					 
					 j('#flex1').flexOptions().flexReload(); 
				  }else {
					  alert("Record doesn't exists.")
				  }
				  
				});		
			}			
		}	
	}
}

// Show Maufacurer List

function showManufacturer(){
j("#flex1").flexigrid({
	url: 'manufacturer.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Manufacturer', name : 'manufacturer_name', width : 300, sortable : true, align: 'left'},
		{display: 'Segment', name : 'segment_name', width : 300, sortable : true, align: 'left'},
		{display: 'Status', name : 'status', width : 100, sortable : true, align: 'left'}
		],
	buttons : [
		{name: 'Add', bclass: 'add', onpress : eventHandlerManufacturer},
		{separator: true},		
		{name: 'Edit', bclass: 'edit', onpress : eventHandlerManufacturer},
		{separator: true}
		],
	searchitems : [
		{display: 'Manufacturer', name : 'manufacturer_name', isdefault: true},
		{display: 'Segment', name: 'segment_name', isdefault: true}
		],
	sortname: "manufacturer_name",
	sortorder: "asc",
	usepager: true,
	title: 'Manufacturer',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 1000,
	height: 'auto'
});  
}

function eventHandlerManufacturer(com,grid)
{
	if (com=='Add')	{
		location.href="add_manufacturer.php";		
	}else{
		if( j('.trSelected',grid).length<=0 ||  j('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=j('.trSelected',grid);		
		if (com=='Edit'){
			location.href="add_manufacturer.php?id="+items[0].id.substr(3);			
		}
		if (com=='Delete'){
			if(confirm("Are you sure, you want to delete selected Manufacturer?")){
					
				j.post("manufacturer.php", {"delete":"yes","id":items[0].id.substr(3)},function(data)
				{		 
				  if(data!="") {					 
					 j('#flex1').flexOptions().flexReload(); 
				  }else {
					  alert("Record doesn't exists.")
				  }
				  
				});		
			}			
		}	
	}
}

// Show Vehicle Model List

function showVehicleModel(){
j("#flex1").flexigrid({
	url: 'vehicle_model.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Model', name : 'model_name', width : 300, sortable : true, align: 'left'},
		{display: 'Manufacturer', name : 'manufacturer_name', width : 300, sortable : true, align: 'left'},
		{display: 'Segment', name : 'segment_name', width : 300, sortable : true, align: 'left'},
		{display: 'Status', name : 'status', width : 100, sortable : true, align: 'left'}
		],
	buttons : [
		{name: 'Add', bclass: 'add', onpress : eventHandlerModel},
		{separator: true},		
		{name: 'Edit', bclass: 'edit', onpress : eventHandlerModel},
		{separator: true}
		],
	searchitems : [
		{display: 'Model', name : 'model_name', isdefault: true},
		{display: 'Manufacturer', name : 'manufacturer_name', isdefault: true},
		{display: 'Segment', name : 'segment_name', isdefault: true}
		],
	sortname: "model_name",
	sortorder: "asc",
	usepager: true,
	title: 'Vehicle Model',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 1000,
	height: 'auto'
});  
}

function eventHandlerModel(com,grid)
{
	if (com=='Add')	{
		location.href="add_vehicle_model.php";		
	}else{
		if( j('.trSelected',grid).length<=0 ||  j('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=j('.trSelected',grid);		
		if (com=='Edit'){
			location.href="add_vehicle_model.php?id="+items[0].id.substr(3);			
		}
		if (com=='Delete'){
			if(confirm("Are you sure, you want to delete selected Model?")){
					
				j.post("vehicle_model.php", {"delete":"yes","id":items[0].id.substr(3)},function(data)
				{		 
				  if(data!="") {					 
					 j('#flex1').flexOptions().flexReload(); 
				  }else {
					  alert("Record doesn't exists.")
				  }
				  
				});		
			}			
		}	
	}
}

// Show Segment/Vehicle Type

function showSegmentType(){
j("#flex1").flexigrid({
	url: 'segment_type.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Segment Type', name : 'segment_type', width : 300, sortable : true, align: 'left'},
		{display: 'Segment', name : 'segment_name', width : 300, sortable : true, align: 'left'},
		{display: 'Status', name : 'status', width : 100, sortable : true, align: 'left'}
		],
	buttons : [
		{name: 'Add', bclass: 'add', onpress : eventHandlerSegmentType},
		{separator: true},		
		{name: 'Edit', bclass: 'edit', onpress : eventHandlerSegmentType},
		{separator: true}
		],
	searchitems : [
		{display: 'Segment Type', name : 'segment_type', isdefault: true},
		{display: 'Segment', name: 'segment_name', isdefault: true}
		],
	sortname: "segment_type",
	sortorder: "asc",
	usepager: true,
	title: 'Segment Type',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 1000,
	height: 'auto'
});  
}

function eventHandlerSegmentType(com,grid)
{
	if (com=='Add')	{
		location.href="add_segment_type.php";		
	}else{
		if( j('.trSelected',grid).length<=0 ||  j('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=j('.trSelected',grid);		
		if (com=='Edit'){
			location.href="add_segment_type.php?id="+items[0].id.substr(3);			
		}
		if (com=='Delete'){
			if(confirm("Are you sure, you want to delete selected Manufacturer?")){
					
				j.post("segment_type.php", {"delete":"yes","id":items[0].id.substr(3)},function(data)
				{		 
				  if(data!="") {					 
					 j('#flex1').flexOptions().flexReload(); 
				  }else {
					  alert("Record doesn't exists.")
				  }
				  
				});		
			}			
		}	
	}
}

// Show Fuel List
function showFuel(){
j("#flex1").flexigrid({
	url: 'fuel.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Fuel', name : 'fuel_name', width : 300, sortable : true, align: 'left'},
		//{display: 'Segment', name : 'segment_name', width : 300, sortable : true, align: 'left'},
		{display: 'Status', name : 'status', width : 100, sortable : true, align: 'left'}
		],
	buttons : [
		{name: 'Add', bclass: 'add', onpress : eventHandlerFuel},
		{separator: true},		
		{name: 'Edit', bclass: 'edit', onpress : eventHandlerFuel},
		{separator: true}
		],
	searchitems : [
		{display: 'Fuel', name : 'fuel_name', isdefault: true}
		//{display: 'Segment', name: 'segment_name', isdefault: true}
		],
	sortname: "fuel_name",
	sortorder: "asc",
	usepager: true,
	title: 'Fuel',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 1000,
	height: 'auto'
});  
}

function eventHandlerFuel(com,grid)
{
	if (com=='Add')	{
		location.href="add_fuel.php";		
	}else{
		if( j('.trSelected',grid).length<=0 ||  j('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=j('.trSelected',grid);		
		if (com=='Edit'){
			location.href="add_fuel.php?id="+items[0].id.substr(3);			
		}
		if (com=='Delete'){
			if(confirm("Are you sure, you want to delete selected fuel?")){
					
				j.post("fuel.php", {"delete":"yes","id":items[0].id.substr(3)},function(data)
				{		 
				  if(data!="") {					 
					 j('#flex1').flexOptions().flexReload(); 
				  }else {
					  alert("Record doesn't exists.")
				  }
				  
				});		
			}			
		}	
	}
}

// show Capacity List

function showCapacity(){
j("#flex1").flexigrid({
	url: 'capacity.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Capacity', name : 'capacity', width : 300, sortable : true, align: 'left'},
		{display: 'Segment', name : 'segment_name', width : 300, sortable : true, align: 'left'},
		{display: 'Status', name : 'status', width : 100, sortable : true, align: 'left'}
		],
	buttons : [
		{name: 'Add', bclass: 'add', onpress : eventHandlerCapacity},
		{separator: true},		
		{name: 'Edit', bclass: 'edit', onpress : eventHandlerCapacity},
		{separator: true}
		],
	searchitems : [
		{display: 'Capacity', name : 'capacity', isdefault: true},
		{display: 'Segment', name: 'segment_name', isdefault: true}
		],
	sortname: "capacity",
	sortorder: "asc",
	usepager: true,
	title: 'Capacity',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 1000,
	height: 'auto'
});  
}

function eventHandlerCapacity(com,grid)
{
	if (com=='Add')	{
		location.href="add_capacity.php";		
	}else{
		if( j('.trSelected',grid).length<=0 ||  j('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=j('.trSelected',grid);		
		if (com=='Edit'){
			location.href="add_capacity.php?id="+items[0].id.substr(3);			
		}
		if (com=='Delete'){
			if(confirm("Are you sure, you want to delete selected Capacity?")){
					
				j.post("capacity.php", {"delete":"yes","id":items[0].id.substr(3)},function(data)
				{		 
				  if(data!="") {					 
					 j('#flex1').flexOptions().flexReload(); 
				  }else {
					  alert("Record doesn't exists.")
				  }
				  
				});		
			}			
		}	
	}
}

// Show showChartAppBrands List
function showChartAppBrands(){
j("#flex1").flexigrid({
	url: 'chart_app_brands.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Brand Name', name : 'brand_name', width : 300, sortable : true, align: 'left'},
		//{display: 'Segment', name : 'segment_name', width : 300, sortable : true, align: 'left'},
		{display: 'Status', name : 'status', width : 100, sortable : true, align: 'left'}
		],
	buttons : [
		{name: 'Add', bclass: 'add', onpress : eventHandlerChartAppBrands},
		{separator: true},		
		{name: 'Edit', bclass: 'edit', onpress : eventHandlerChartAppBrands},
		{separator: true}
		],
	searchitems : [
		{display: 'Brand Name', name : 'brand_name', isdefault: true}
		//{display: 'Segment', name: 'segment_name', isdefault: true}
		],
	sortname: "brand_name",
	sortorder: "asc",
	usepager: true,
	title: 'Chart App Brands',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 1000,
	height: 'auto'
});  
}

function eventHandlerChartAppBrands(com,grid)
{
	if (com=='Add')	{
		location.href="add_chart_app_brands.php";		
	}else{
		if( j('.trSelected',grid).length<=0 ||  j('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=j('.trSelected',grid);		
		if (com=='Edit'){
			location.href="add_chart_app_brands.php?id="+items[0].id.substr(3);			
		}
		if (com=='Delete'){
			if(confirm("Are you sure, you want to delete selected brands?")){
					
				j.post("chart_app_brands.php", {"delete":"yes","id":items[0].id.substr(3)},function(data)
				{		 
				  if(data!="") {					 
					 j('#flex1').flexOptions().flexReload(); 
				  }else {
					  alert("Record doesn't exists.")
				  }
				  
				});		
			}			
		}	
	}
}

// show Watts List

function showWatts(){
j("#flex1").flexigrid({
	url: 'battery_power.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Watts', name : 'watts', width : 300, sortable : true, align: 'left'},
		{display: 'Segment', name : 'segment_name', width : 300, sortable : true, align: 'left'},
		{display: 'Status', name : 'status', width : 100, sortable : true, align: 'left'}
		],
	buttons : [
		{name: 'Add', bclass: 'add', onpress : eventHandlerWatts},
		{separator: true},		
		{name: 'Edit', bclass: 'edit', onpress : eventHandlerWatts},
		{separator: true}
		],
	searchitems : [
		{display: 'Watts', name : 'watts', isdefault: true},
		{display: 'Segment', name: 'segment_name', isdefault: true}
		],
	sortname: "watts",
	sortorder: "asc",
	usepager: true,
	title: 'Watts',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 1000,
	height: 'auto'
});  
}

function eventHandlerWatts(com,grid)
{
	if (com=='Add')	{
		location.href="add_battery_power.php";		
	}else{
		if( j('.trSelected',grid).length<=0 ||  j('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=j('.trSelected',grid);		
		if (com=='Edit'){
			location.href="add_battery_power.php?id="+items[0].id.substr(3);			
		}
		if (com=='Delete'){
			if(confirm("Are you sure, you want to delete selected watts?")){
					
				j.post("battery_power.php", {"delete":"yes","id":items[0].id.substr(3)},function(data)
				{		 
				  if(data!="") {					 
					 j('#flex1').flexOptions().flexReload(); 
				  }else {
					  alert("Record doesn't exists.")
				  }
				  
				});		
			}			
		}	
	}
}


function showMaxSpeed(){
j("#flex1").flexigrid({
	url: 'segment_max_speed.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Max Speed (Kms/hr)', name : 'segment_speed', width : 300, sortable : true, align: 'left'},
		{display: 'Segment', name : 'segment_name', width : 300, sortable : true, align: 'left'},
		{display: 'Status', name : 'status', width : 100, sortable : true, align: 'left'}
		],
	buttons : [
		{name: 'Add', bclass: 'add', onpress : eventHandlerMaxSpeed},
		{separator: true},		
		{name: 'Edit', bclass: 'edit', onpress : eventHandlerMaxSpeed},
		{separator: true}
		],
	searchitems : [
		{display: 'Max Speed', name : 'segment_speed', isdefault: true},
		{display: 'Segment', name: 'segment_name', isdefault: true}
		],
	sortname: "segment_speed",
	sortorder: "asc",
	usepager: true,
	title: 'Max Speed',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 1000,
	height: 'auto'
});  
}

function eventHandlerMaxSpeed(com,grid)
{
	if (com=='Add')	{
		location.href="add_segment_max_speed.php";		
	}else{
		if( j('.trSelected',grid).length<=0 ||  j('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=j('.trSelected',grid);		
		if (com=='Edit'){
			location.href="add_segment_max_speed.php?id="+items[0].id.substr(3);			
		}
		if (com=='Delete'){
			if(confirm("Are you sure, you want to delete selected max speed?")){
					
				j.post("segment_max_speed.php", {"delete":"yes","id":items[0].id.substr(3)},function(data)
				{		 
				  if(data!="") {					 
					 j('#flex1').flexOptions().flexReload(); 
				  }else {
					  alert("Record doesn't exists.")
				  }
				  
				});		
			}			
		}	
	}
}


function showDistCovrPerChrg(){
j("#flex1").flexigrid({
	url: 'distance_covered_per_charge.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Distance Covered Per Charge (Kms)', name : 'dist_cover_per_charge', width : 300, sortable : true, align: 'left'},
		{display: 'Segment', name : 'segment_name', width : 300, sortable : true, align: 'left'},
		{display: 'Status', name : 'status', width : 100, sortable : true, align: 'left'}
		],
	buttons : [
		{name: 'Add', bclass: 'add', onpress : eventHandlerDistCovrPerChrg},
		{separator: true},		
		{name: 'Edit', bclass: 'edit', onpress : eventHandlerDistCovrPerChrg},
		{separator: true}
		],
	searchitems : [
		{display: 'Distance Covered Per Charge', name : 'dist_cover_per_charge', isdefault: true},
		{display: 'Segment', name: 'segment_name', isdefault: true}
		],
	sortname: "dist_cover_per_charge",
	sortorder: "asc",
	usepager: true,
	title: 'Distance Covered Per Charge',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 1000,
	height: 'auto'
});  
}

function eventHandlerDistCovrPerChrg(com,grid)
{
	if (com=='Add')	{
		location.href="addDistanceCoveredPerCharge.php";		
	}else{
		if( j('.trSelected',grid).length<=0 ||  j('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=j('.trSelected',grid);		
		if (com=='Edit'){
			location.href="addDistanceCoveredPerCharge.php?id="+items[0].id.substr(3);			
		}
		if (com=='Delete'){
			if(confirm("Are you sure, you want to delete selected Charge?")){
					
				j.post("distance_covered_per_charge.php", {"delete":"yes","id":items[0].id.substr(3)},function(data)
				{		 
				  if(data!="") {					 
					 j('#flex1').flexOptions().flexReload(); 
				  }else {
					  alert("Record doesn't exists.")
				  }
				  
				});		
			}			
		}	
	}
}

function showLoadCarryingCapacity(){
j("#flex1").flexigrid({
	url: 'load_carrying_capacity.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Load Carrying Capacity(Kgs)', name : 'load_carry_capacity', width : 300, sortable : true, align: 'left'},
		{display: 'Segment', name : 'segment_name', width : 300, sortable : true, align: 'left'},
		{display: 'Status', name : 'status', width : 100, sortable : true, align: 'left'}
		],
	buttons : [
		{name: 'Add', bclass: 'add', onpress : eventHandlerLoadCarryingCapacity},
		{separator: true},		
		{name: 'Edit', bclass: 'edit', onpress : eventHandlerLoadCarryingCapacity},
		{separator: true}
		],
	searchitems : [
		{display: 'Load Carrying Capacity', name : 'load_carry_capacity', isdefault: true},
		{display: 'Segment', name: 'segment_name', isdefault: true}
		],
	sortname: "load_carry_capacity",
	sortorder: "asc",
	usepager: true,
	title: 'Load Carrying Capacity',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 1000,
	height: 'auto'
});  
}

function eventHandlerLoadCarryingCapacity(com,grid)
{
	if (com=='Add')	{
		location.href="add_load_carrying_capacity.php";		
	}else{
		if( j('.trSelected',grid).length<=0 ||  j('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=j('.trSelected',grid);		
		if (com=='Edit'){
			location.href="add_load_carrying_capacity.php?id="+items[0].id.substr(3);			
		}
		if (com=='Delete'){
			if(confirm("Are you sure, you want to delete selected load carrying capacity?")){
					
				j.post("load_carrying_capacity.php", {"delete":"yes","id":items[0].id.substr(3)},function(data)
				{		 
				  if(data!="") {					 
					 j('#flex1').flexOptions().flexReload(); 
				  }else {
					  alert("Record doesn't exists.")
				  }
				  
				});		
			}			
		}	
	}
}

// show Type Of Start

function showTypeOfStart(){
j("#flex1").flexigrid({
	url: 'type_of_start.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Type Of Start', name : 'type_of_start', width : 300, sortable : true, align: 'left'},
		{display: 'Segment', name : 'segment_name', width : 300, sortable : true, align: 'left'},
		{display: 'Status', name : 'status', width : 100, sortable : true, align: 'left'}
		],
	buttons : [
		{name: 'Add', bclass: 'add', onpress : eventHandlerTypeOfStart},
		{separator: true},		
		{name: 'Edit', bclass: 'edit', onpress : eventHandlerTypeOfStart},
		{separator: true}
		],
	searchitems : [
		{display: 'Type Of Start', name : 'type_of_start', isdefault: true},
		{display: 'Segment', name: 'segment_name', isdefault: true}
		],
	sortname: "type_of_start",
	sortorder: "asc",
	usepager: true,
	title: 'Type Of Start',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 1000,
	height: 'auto'
});  
}

function eventHandlerTypeOfStart(com,grid)
{
	if (com=='Add')	{
		location.href="add_type_of_start.php";		
	}else{
		if( j('.trSelected',grid).length<=0 ||  j('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=j('.trSelected',grid);		
		if (com=='Edit'){
			location.href="add_type_of_start.php?id="+items[0].id.substr(3);			
		}
		if (com=='Delete'){
			if(confirm("Are you sure, you want to delete selected Type Of Start?")){
					
				j.post("type_of_start.php", {"delete":"yes","id":items[0].id.substr(3)},function(data)
				{		 
				  if(data!="") {					 
					 j('#flex1').flexOptions().flexReload(); 
				  }else {
					  alert("Record doesn't exists.")
				  }
				  
				});		
			}			
		}	
	}
}

// Show System Voltage List

function showSystemVoltage(){
j("#flex1").flexigrid({
	url: 'system_voltage.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'System Voltage', name : 'system_voltage', width : 300, sortable : true, align: 'left'},
		{display: 'Segment', name : 'segment_name', width : 300, sortable : true, align: 'left'},
		{display: 'Status', name : 'status', width : 100, sortable : true, align: 'left'}
		],
	buttons : [
		{name: 'Add', bclass: 'add', onpress : eventHandlerSystemVoltage},
		{separator: true},		
		{name: 'Edit', bclass: 'edit', onpress : eventHandlerSystemVoltage},
		{separator: true}
		],
	searchitems : [
		{display: 'System Voltage', name : 'system_voltage', isdefault: true},
		{display: 'Segment', name: 'segment_name', isdefault: true}
		],
	sortname: "system_voltage",
	sortorder: "asc",
	usepager: true,
	title: 'System Voltage',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 1000,
	height: 'auto'
});  
}

function eventHandlerSystemVoltage(com,grid)
{
	if (com=='Add')	{
		location.href="add_system_voltage.php";		
	}else{
		if( j('.trSelected',grid).length<=0 ||  j('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=j('.trSelected',grid);		
		if (com=='Edit'){
			location.href="add_system_voltage.php?id="+items[0].id.substr(3);			
		}
		if (com=='Delete'){
			if(confirm("Are you sure, you want to delete selected Capacity?")){
					
				j.post("system_voltage.php", {"delete":"yes","id":items[0].id.substr(3)},function(data)
				{		 
				  if(data!="") {					 
					 j('#flex1').flexOptions().flexReload(); 
				  }else {
					  alert("Record doesn't exists.")
				  }
				  
				});		
			}			
		}	
	}
}
// Show Rating List

function showRating(){
j("#flex1").flexigrid({
	url: 'rating.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Rating', name : 'rating', width : 300, sortable : true, align: 'left'},
		{display: 'Segment', name : 'segment_name', width : 300, sortable : true, align: 'left'},
		{display: 'Status', name : 'status', width : 100, sortable : true, align: 'left'}
		],
	buttons : [
		{name: 'Add', bclass: 'add', onpress : eventHandlerRating},
		{separator: true},		
		{name: 'Edit', bclass: 'edit', onpress : eventHandlerRating},
		{separator: true}
		],
	searchitems : [
		{display: 'Rating', name : 'rating', isdefault: true},
		{display: 'Segment', name: 'segment_name', isdefault: true}
		],
	sortname: "rating",
	sortorder: "asc",
	usepager: true,
	title: 'Rating',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 1000,
	height: 'auto'
});  
}

function eventHandlerRating(com,grid)
{
	if (com=='Add')	{
		location.href="add_rating.php";		
	}else{
		if( j('.trSelected',grid).length<=0 ||  j('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=j('.trSelected',grid);		
		if (com=='Edit'){
			location.href="add_rating.php?id="+items[0].id.substr(3);			
		}
		if (com=='Delete'){
			if(confirm("Are you sure, you want to delete selected Capacity?")){
					
				j.post("rating.php", {"delete":"yes","id":items[0].id.substr(3)},function(data)
				{		 
				  if(data!="") {					 
					 j('#flex1').flexOptions().flexReload(); 
				  }else {
					  alert("Record doesn't exists.")
				  }
				  
				});		
			}			
		}	
	}
}

function showBatrySelectionChart(){

j("#flex1").flexigrid({
	url: 'battery_selection_chart.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Battery Name', name : 'battery_name', width : 100, sortable : true, align: 'left'},
		{display: 'Battery Code', name : 'battery_code', width : 100, sortable : true, align: 'left'},
		{display: 'Segment', name : 'segment_name', width : 100, sortable : true, align: 'left'},
		{display: 'Segment Type', name : 'segment_type', width : 100, sortable : true, align: 'left'},
		{display: 'Manufacturer', name : 'manufacturer_name', width : 200, sortable : true, align: 'left'},
		{display: 'Vehicle Model', name : 'model_name', width : 200, sortable : true, align: 'left'},
		{display: 'Fuel', name : 'fuel_name', width : 100, sortable : true, align: 'left'},
		{display: 'Capacity', name : 'capacity', width : 100, sortable : true, align: 'left'},
		{display: 'Electrical Load(Watts)', name : 'watts', width : 200, sortable : true, align: 'left'},
		{display: 'Max Speed (Kms/hr)', name : 'segment_speed', width : 200, sortable : true, align: 'left'},
		{display: 'Distance Covered / Charge( Kms)', name : 'dist_cover_per_charge', width : 200, sortable : true, align: 'left'},
		{display: 'Load Carrying Capacity(Kgs)', name : 'load_carry_capacity', width : 200, sortable : true, align: 'left'},
		{display: 'System Voltage (V)', name : 'system_voltage', width : 100, sortable : true, align: 'left'},
		{display: 'Type of Start', name : 'type_of_start', width : 100, sortable : true, align: 'left'},
		{display: 'Rating', name : 'rating', width : 100, sortable : true, align: 'left'},
		{display: 'BRAND', name : 'brand_name', width : 100, sortable : true, align: 'left'},
		{display: 'BATTERY Type', name : 'type_of_battery', width : 100, sortable : true, align: 'left'},
		{display: 'Warranty', name : 'warranty', width : 100, sortable : true, align: 'left'},
		{display: '5.0 Hrs.', name : 'five_hrs_model', width : 100, sortable : true, align: 'left'},
		{display: '4.0Hrs.', name : 'four_hrs_model', width : 100, sortable : true, align: 'left'},
		{display: '3.0Hrs.', name : 'three_hrs_model', width : 100, sortable : true, align: 'left'},
		{display: '2.0Hrs.', name : 'two_hrs_model', width : 100, sortable : true, align: 'left'},
		{display: 'Status', name : 'status', width : 100, sortable : true, align: 'left'}
		],
	buttons : [
		{name: 'Add', bclass: 'add', onpress : eventHandlerBattery},
		{separator: true},		
		{name: 'Edit', bclass: 'edit', onpress : eventHandlerBattery},
		{separator: true},
		{name: 'Delete', bclass: 'delete', onpress : eventHandlerBattery},
		{separator: true}
		],
	searchitems : [
		{display: 'Segment', name: 'segment_name', isdefault: true},
		{display: 'Segment Type', name : 'segment_type', isdefault: true},
		{display: 'Manufacturer', name : 'manufacturer_name', isdefault: true},
		{display: 'Vehicle Model', name : 'model_name', isdefault: true},
		{display: 'Fuel', name : 'fuel_name', isdefault: true},
		{display: 'Brands', name : 'brand_name', isdefault: true}
		],
	sortname: "battery_name",
	sortorder: "asc",
	usepager: true,
	title: 'Battery Selection Chart',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 1000,
	height: 'auto'
});  
}

function eventHandlerBattery(com,grid)
{
	if (com=='Add')	{
		location.href="add_battery_selection_chart.php";		
	}else{
		if( j('.trSelected',grid).length<=0 ||  j('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=j('.trSelected',grid);		
		if (com=='Edit'){
			location.href="add_battery_selection_chart.php?id="+items[0].id.substr(3);			
		}
		if (com=='Delete'){
			if(confirm("Are you sure, you want to delete selected record?")){
					
				j.post("battery_selection_chart.php", {"delete":"yes","id":items[0].id.substr(3)},function(data)
				{		 
				  if(data!="") {					 
					 j('#flex1').flexOptions().flexReload(); 
				  }else {
					  alert("Record doesn't exists.")
				  }
				  
				});		
			}			
		}	
	}
}







/*******************************************************
* DESC: Show BSN MASTER
* Author: AJAY@2017-04-11
*
*
*
********************************************************/


function showBSN(){
j("#flex1").flexigrid({
	url: 'bsn.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Item Name', name : 'item_name', width : 150, sortable : true, align: 'left'},
		{display: 'BSN', name : 'bsn', width : 150, sortable : true, align: 'left'},
		{display: 'Status', name : 'status', width : 80, sortable : true, align: 'left'},
		
		{display: 'Manufacture Date', name : 'manufacture_date', width : 100, sortable : true, align: 'left'},
		{display: 'Date of Sale', name : 'date_of_sale', width : 80, sortable : true, align: 'left'},
		{display: 'WH Dispatch Date', name : 'date_of_sale', width : 100, sortable : true, align: 'left'},
		{display: 'Warranty Upto', name : 'date_of_sale', width : 100, sortable : true, align: 'left'},
		{display: 'Recharged Date', name : 'stk_recharged_date', width : 80, sortable : true, align: 'left'},

		{display: 'Replaced BSN', name : 'replace_battery_bsn', width : 120, sortable : true, align: 'left'},
		{display: 'WH Issue New BSN', name : 'wh_issue_new_bsn', width : 120, sortable : true, align: 'left'},
		{display: 'Customer Name', name : 'date_of_sale', width : 80, sortable : true, align: 'left'},
		{display: 'Customer Phone', name : 'date_of_sale', width : 80, sortable : true, align: 'left'},
		//{display: 'Battery Ageing(In Days)', name : '', width : 120, sortable : true, align: 'left'},

		{display: 'Dispatched WH', name : 'warehouse_name', width : 120, sortable : true, align: 'left'},
		{display: 'Dispatched Distributor', name : 'distributor_name', width : 120, sortable : true, align: 'left'},
		{display: 'Dispatched Dealer', name : 'dealer_name', width : 120, sortable : true, align: 'left'},
		
		{display: 'Item Code', name : 'item_code', width : 100, sortable : true, align: 'left'},
		
		{display: 'Category Name', name : 'category_name', width : 150, sortable : true, align: 'left'},
		
		{display: 'Warranty Period', name : 'warranty_period', width : 100, sortable : true, align: 'left'},
		{display: 'ProRata Period', name : 'warranty_prodata', width : 100, sortable : true, align: 'left'},
		{display: 'Grace Period', name : 'warranty_grace_period', width : 100, sortable : true, align: 'left'},

		{display: 'Added By', name : 'user_type', width : 100, sortable : true, align: 'left'},
		{display: 'Adder Name', name : '', width : 100, sortable : true, align: 'left'},

		{display: 'Last Update Date', name : 'last_updated_date', width : 100, sortable : true, align: 'left'},
		
		],
	buttons : [
		{name: 'Add', bclass: 'Scan', onpress : eventHandlerBSN},
		{separator: true},		
		{name: 'Edit', bclass: 'edit', onpress : eventHandlerBSN},
		{separator: true},
		// {name: 'Set Margin', bclass: 'margin', onpress : eventHandlerMasterCategory},
		// {separator: true}
		],
	searchitems : [
		{display: 'BSN', name : 'bsn', isdefault: true},
		{display: 'Warehouse Code', name : 'warehouse_code'},
		{display: 'Warehouse Name', name : 'warehouse_name'},
		{display: 'Item Code', name : 'item_code'},
		{display: 'Item Name', name : 'item_name'},
		//{display: 'Status', name : 'BSN.status'}
		
		],
	sortname: "bsn",
	sortorder: "asc",
	usepager: true,
	title: 'BSN',
	useRp: true,
	rp: 20,
	showTableToggleBtn: true,
	width: '740',
	height: 'auto'
});  
}



function eventHandlerBSN(com,grid)
{
	//alert(com);
	if (com=='Add')	{
		location.href="addbsn.php";		
	}else{
		if( j('.trSelected',grid).length<=0 ||  j('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=j('.trSelected',grid);	
		
		if (com=='Edit'){
			location.href="addbsn.php?id="+items[0].id.substr(3);			
		} else if (com=='Delete'){
			if(confirm("Are you sure, you want to delete selected Category?")){
					
				j.post("addbsn.php", {"delete":"yes","id":items[0].id.substr(3)},function(data)
				{		 
				  if(data!="") {					 
					 j('#flex1').flexOptions().flexReload(); 
				  }else {
					  alert("Record doesn't exists.")
				  }
				  
				});		
			}			
		}	
	}
}

/************************** Start Service Personnel Routes Approval by Chirag Gupta 11 April, 2017 *********************/

function showServicePersonnelRoutes()
{
	j("#flex1").flexigrid({
	url: 'route_approval.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Service Personnel', name : 'sp_name', width : 150, sortable : true, align: 'left'},
		{display: 'Start Date', name : 'start_date', width : 100, sortable : true, align: 'left'},
		{display: 'End Date', name : 'end_date', width : 100, sortable : true, align: 'left'},
		{display: 'Week Date', name : 'week_day_date', width : 100, sortable : true, align: 'left'},
		{display: 'Distributor Count', name : 'distributor_count', width : 100, sortable : true, align: 'left'},
		{display: 'Dealer Count', name : 'dealer_count', width : 100, sortable : true, align: 'left'},
		{display: 'Status', name : 'status', width : 100, sortable : true, align: 'left'}
		],
	buttons : [
		// {name: 'Add', bclass: 'add', onpress : eventHandlerRoute},
		// {separator: true},		
		{name: 'Edit', bclass: 'edit', onpress : eventHandlerSPRoute},
		{separator: true}		
		// {name: 'Delete', bclass: 'delete', onpress : eventHandlerRoute},
		// {separator: true},
  		// {name: 'Sort Dealer', bclass: 'Sort', onpress : eventHandlerRoute},
  		// {separator: true}
		],
	searchitems : [
		{display: 'Service Personnel', name : 'sp_name', isdefault: true}
		
		],
	sortname: "sp_name",
	sortorder: "asc",
	usepager: true,
	title: 'Service Personnels Routes',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 800,
	height: 'auto'
}); 
}
function eventHandlerSPRoute(com,grid)
{	
	var items=j('.trSelected',grid);		
	if (com=='Edit')
	{
		location.href="approve_sp_route.php?id="+items[0].id.substr(3);			
	}		
}
/************************** End Service Personnel Routes Approval by Chirag Gupta 11 April, 2017 *********************/


/************************** Starts Goodwill Process by Chirag Gupta 12 April, 2017 *********************/
function showGoodwillRequest(){
	j("#flex1").flexigrid({
		url: 'goodwill.php?show=yes',
		dataType: 'json',
		method : "GET", 
		singleSelect:true, 
		colModel : [		
			{display: 'BSN', name : 'bsn', width : 300, sortable : true, align: 'left'},
			{display: 'Jobcard No', name : 'jobcard_no', width : 200, sortable : true, align: 'left'},
			{display: 'Jobcard Date', name : 'jobcard_date', width : 200, sortable : true, align: 'left'},
			{display: 'View Jobcard', name : 'jobcard_form', width : 200, sortable : true, align: 'left'},
			{display: 'Service Personnel', name : 'sp_name', width : 200, sortable : true, align: 'left'},
			{display: 'Date of Sale', name : 'date_of_sale', width : 200, sortable : true, align: 'left'},
			{display: 'Manufacture Date', name : 'manufacture_date', width : 200, sortable : true, align: 'left'}
			// {display: 'Status', name : 'status', width : 100, sortable : true, align: 'left'}
			],
		buttons : [
			// {name: 'Add', bclass: 'add', onpress : eventHandlerGoodwill},
			// {separator: true},		
			{name: 'Edit', bclass: 'edit', onpress : eventHandlerGoodwill},
			{separator: true}		
			],
		searchitems : [
			{display: 'Jobcard Number', name : 'jobcard_no', isdefault: true},
			{display: 'Jobcard Date', name : 'jobcard_date'},
			{display: 'Service Personnel', name : 'sp_name'},
			{display: 'Date Of Sale', name : 'date_of_sale'},
			{display: 'Manufacture Date', name : 'manufacture_date'}
			],
		sortname: "jobcard_no",
		sortorder: "asc",
		usepager: true,
		title: 'Goodwill Request',
		useRp: true,
		rp: 50,
		showTableToggleBtn: true,
		width: 700,
		height: 'auto'
	});  
	}
function eventHandlerGoodwill(com,grid)
{
	if (com=='Add')	{
		location.href="goodwill_edit.php";		
	}else{
		if( j('.trSelected',grid).length<=0 ||  j('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=j('.trSelected',grid);		
		if (com=='Edit'){
			location.href="goodwill_edit.php?id="+items[0].id.substr(3);			
		}	
	}
}

/************************** END Goodwill Process by Chirag Gupta 12 April, 2017 *********************/

// Show Secondary Scheme created by distributor
function showSecondaryScheme(){
j("#flex1").flexigrid({
	url: 'secondary_scheme.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Description', name : 'discount_desc', width : 150, sortable : true, align: 'left'},
		{display: 'Scheme Mode', name : 'mode', width : 100, sortable : true, align: 'left'},
	//	{display: 'Division', name : 'division_id', width : 100, sortable : true, align: 'left'},
		{display: 'Scheme', name : 'discount', width : 100, sortable : true, align: 'left'},
		{display: 'Secondary Scheme', name : 'is_dis_discount', width : 100, sortable : true, align: 'left'},
		{display: 'Item Type', name : 'item_type', width : 100, sortable : true, align: 'left'},
	//	{display: 'Party Type', name : 'price_detail', width : 100, sortable : true, align: 'left'},
		{display: 'Scheme Type', name : 'discount_type', width : 100, sortable : true, align: 'left'},
		//{display: 'Minimum Amount', name : 'minimum_amount', width : 100, sortable : true, align: 'left'},
		//{display: 'Minimum Quantity', name : 'minimum_quantity', width : 100, sortable : true, align: 'left'},
		{display: 'Start Date', name : 'start_date', width : 100, sortable : true, align: 'left'},
		{display: 'End Date', name : 'end_date', width : 100, sortable : true, align: 'left'},
		{display: 'Status', name : 'status', width : 50, sortable : true, align: 'left'}
		],
	buttons : [
		{name: 'Add', bclass: 'add', onpress : eventHandlerSecondaryScheme},
		{separator: true},		
		{name: 'Edit', bclass: 'edit', onpress : eventHandlerSecondaryScheme},
		{separator: true},
		{name: 'Delete', bclass: 'delete', onpress : eventHandlerSecondaryScheme},
		{separator: true}
		/*,
		{name: 'Sms Schedule', bclass: 'sms', onpress : eventHandlerDiscount},
		{separator: true}*/
		
		],
	searchitems : [
		{display: 'Description', name : 'discount_desc', isdefault: true}
		
		],
	sortname: "discount_id",
	sortorder: "Desc",
	usepager: true,
	title: 'Scheme',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 1000,
	height: 'auto'
});  
}
function eventHandlerSecondaryScheme(com,grid)
{
	if (com=='Import')	{
		//location.href="import_item.php";	
		location.href="#";	
	}
	else if (com=='Export')	{
		//location.href="export.inc.php?export_item";		
	}
	else if (com=='Add')	{
		location.href="add_secondary_scheme.php";		
	}else{
		if( j('.trSelected',grid).length<=0 ||  j('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=j('.trSelected',grid);	
		if (com=='Sms Schedule')	{
			var id = items[0].id.substr(3);
			//alert(id);
			var ids = btoa(id);
			//alert(atob(ids));
			window.open("sms_schedule.php?sid="+ids,'_newtab');
			//location.href="sms_schedule.php?sid="+ids;	
			} 
		if (com=='Edit'){
			
			location.href="edit_secondary_scheme.php?id="+items[0].id.substr(3);			
		}
		if (com=='Delete'){
			if(confirm("Are you sure, you want to delete selected Scheme?")){
					
				j.post("secondary_scheme.php", {"delete":"yes","id":items[0].id.substr(3)},function(data)
				{		 
				  if(data!="") {					 
					 j('#flex1').flexOptions().flexReload(); 
				  }else {
					  alert("Record doesn't exists.")
				  }
				  
				});		
			}			
		}	
	}
}

/***********************************  complaint list of Call Centers ****************************************/

function callCenterComplaint(){
	
	j("#flex1").flexigrid({
		url: 'call_center_complaint.php?show=yes',
		dataType: 'json',
		method : "GET", 
		singleSelect:true, 
		colModel : [		
			{display: 'Battery Serial No.', name : 'c.product_serial_no', width : 150, sortable : true, align: 'left'},
			{display: 'Battery Model', name : 'item_code', width : 150, sortable : true, align: 'left'},
			{display: 'Complaint Number.', name : 'c.complaint_no', width : 150, sortable : true, align: 'left'},
			{display: 'Operator Name.', name : 'operator_name', width : 150, sortable : true, align: 'left'},
			{display: 'Complaint Date', name : 'Complaint', width : 100, sortable : true, align: 'left'},
			{display: 'Edited By', name : 'editedBy', width : 100, sortable : true, align: 'left'},
			{display: 'Edited On', name : 'EditedDate', width : 100, sortable : true, align: 'left'},
			{display: 'Service Distributor', name : 'SD.service_distributor_name', width : 120, sortable : true, align: 'left'},
			{display: 'Customer Name', name : 'customer_name', width : 120, sortable : true, align: 'left'},
			{display: 'Customer Phone', name : 'customer_phone_no', width : 120, sortable : true, align: 'left'},
			{display: 'Customer Address', name : 'customer_address', width : 120, sortable : true, align: 'left'},
			{display: 'District', name : 'city_name', width : 120, sortable : true, align: 'left'},
			{display: 'Customer Email', name : 'customer_email', width : 120, sortable : true, align: 'left'},		
			{display: 'Sale Date', name : 'pdate', width : 120, sortable : true, align: 'left'},	
			{display: 'Service Distributor SE', name : 'service_distributor_sp_name', width : 120, sortable : true, align: 'left'},
			{display: 'Service Distributor SE Phone', name : 'service_distributor_sp_phone_no', width : 120, sortable : true, align: 'left'},	
			{display: 'Auth Token', name : 'auth_token', width : 120, sortable : true, align: 'left'},	
			{display: 'Action Taken', name : 'action_taken', width : 120, sortable : true, align: 'left'},	
			{display: 'Complaint Status', name : 'under_process', width : 120, sortable : true, align: 'left'},	
			{display: 'Allocation Date', name : 'allocation_datetime', width : 120, sortable : true, align: 'left'},	
			// {display: 'status', name : 'status', width : 100, sortable : true, align: 'left'},
            //{display: 'Image', name : 'images', width : 120, sortable : true, align: 'left'},
			{display: 'Pending For (in hours)', name : 'calculated_pending', width : 120, sortable : true, align: 'left'},
			{display: 'Complaint TAT1', name : 'cctat1', width : 100, sortable : true, align: 'left'},
			{display: 'Complaint TAT2', name : 'cctat2', width : 100, sortable : true, align: 'left'},
            {display: 'Distance From ASD(km)', name : 'asd_distance', width : 100, sortable : true, align: 'left'},
            {display: 'Amount', name : 'amount', width : 105, sortable : true, align: 'left'},
            {display: 'call login Date', name : 'logins_time', width : 105, sortable : true, align: 'left'},
            {display: 'call Close Date', name : 'closes_time', width : 105, sortable : true, align: 'left'},
            {display: 'Initial Reading Date', name : 'initial_reading', width : 105, sortable : true, align: 'left'},
            {display: 'Approver Comments', name : 'approver_comments', width : 105, sortable : true, align: 'left'},
			{display: 'Comment ', name : 'comment', width : 200, sortable : true, align: 'left'},
			{display: 'Engineer Remarks ', name : 'ENGINEER_REMARKS', width : 200, sortable : true, align: 'left'}
			],
		buttons : [
			{name: 'Add', bclass: 'addhir', onpress : eventHandlercomplaintcallcenter},
			{separator: true},		
			{name: 'Edit', bclass: 'edithir', onpress : eventHandlercomplaintcallcenter},
			{separator: true},	
			{name: 'Export', bclass: 'Deletehir', onpress : eventHandlercomplaintcallcenter},
			{separator: true},		
			// {name: 'Delete', bclass: 'Deletehir', onpress : eventHandlercomplaintcallcenter},
			// {separator: true}		
			
			],
		searchitems : [
			{display: 'Battery Serial No. ', name : 'c.product_serial_no', isdefault: true},
			{display: 'Complaint Number', name : 'complaint_no', isdefault: true},
			{display: 'Complaint Status', name : 'under_process', isdefault: true},
			{display: 'ASD Name', name : 'SD.service_distributor_name', isdefault: true},
			{display: 'Customer Phone', name : 'customer_phone_no', isdefault: true},			
			// {display: 'Complaint Status', name : 'complain_status', isdefault: true},
			{display: 'Complaint Date', name : 'created_date', isdefault: true}
			],
		sortname: "c.complaint_id",
		sortorder: "desc",
		usepager: true,
		title: 'Complaints List',
		useRp: true,
		rp: 50,
		showTableToggleBtn: true,
		width: "900",
		height: "auto"
	});  
}



function eventHandlercomplaintcallcenter(com,grid) {
	
	if (com=='Import')	{
		//location.href="import_item.php";	
		location.href="#";	
	}
	
		
	
	
	
	else if (com=='Export')	{
		location.href="export.inc.php?export_call_center_complaints";		
	}
	else if (com=='Add')	{
		location.href="raisecomplaint_call_center.php";		
	}else{
		if( j('.trSelected',grid).length<=0 ||  j('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=j('.trSelected',grid);		
		if (com=='Edit')
		{
			//alert('HEllo');
			//alert("addhierarchy.php?id="+items[0].id.substr(3));
			location.href="raisecomplaint_call_center.php?id="+items[0].id.substr(3);	
			
		}
		
		
		
		
		
		if (com=='Delete'){
			if(confirm("Are you sure, you want to delete selected data?")){
					
				j.post("call_center_complaint.php", {"delete":"yes","id":items[0].id.substr(3)},function(data)
				{		 
				  if(data!="") {					 
					 j('#flex1').flexOptions().flexReload(); 
				  }else {
					  alert("Record doesn't exists.")
				  }
				  
				});		
			}			
		}	
	}
}
	
/*********************************** End of  complaint list of Distrubutors ****************************************/



/*****************************************  Complaints History to ASD **********************************************/

function complaintHistoryASD(){
	
	j("#flex1").flexigrid({
		url: 'complaint_history.php?show=yes',
		dataType: 'json',
		method : "GET", 
		singleSelect:true, 
		colModel : [		
			{display: 'Battery Serial No.', name : 'c.product_serial_no', width : 150, sortable : true, align: 'left'},
			{display: 'Complaint Number.', name : 'c.complaint_no', width : 150, sortable : true, align: 'left'},
			{display: 'Operator Name.', name : 'operator_name', width : 150, sortable : true, align: 'left'},
			// {display: 'Service Distributor', name : 'SD.service_distributor_name', width : 120, sortable : true, align: 'left'},
			{display: 'Customer Name', name : 'customer_name', width : 120, sortable : true, align: 'left'},
			{display: 'Customer Phone', name : 'customer_phone_no', width : 120, sortable : true, align: 'left'},
			{display: 'Customer Address', name : 'customer_address', width : 120, sortable : true, align: 'left'},
			{display: 'Customer Email', name : 'customer_email', width : 120, sortable : true, align: 'left'},		
			{display: 'Sale Date', name : 'pdate', width : 120, sortable : true, align: 'left'},	
			{display: 'Service Distributor SP', name : 'service_distributor_sp_name', width : 120, sortable : true, align: 'left'},
			{display: 'Service Distributor SP Phone', name : 'service_distributor_sp_phone_no', width : 120, sortable : true, align: 'left'},	
			{display: 'Auth Token', name : 'auth_token', width : 100, sortable : true, align: 'left'},
			{display: 'Action Taken', name : 'action_taken', width : 100, sortable : true, align: 'left'},
			// {display: 'status', name : 'status', width : 100, sortable : true, align: 'left'},
			{display: 'Complaint Status', name : 'BCFST', width : 150, sortable : true, align: 'left'},
			{display: 'Call Close Datetime', name : 'call_close_datetime', width : 150, sortable : true, align: 'left'},
            //{display: 'Image', name : 'images', width : 120, sortable : true, align: 'left'},
			{display: 'Complaint Date', name : 'Complaint', width : 100, sortable : true, align: 'left'},
			{display: 'Complaint TAT1', name : 'cctat1', width : 100, sortable : true, align: 'left'},
			{display: 'Complaint TAT2', name : 'cctat2', width : 100, sortable : true, align: 'left'},
			{display: 'Comment ', name : 'comment', width : 200, sortable : true, align: 'left'}
			],
		buttons : [
			// {name: 'Add', bclass: 'addhir', onpress : eventHandlerComplaintHistoryASD},
			// {separator: true},		
			// {name: 'Edit', bclass: 'edithir', onpress : eventHandlerComplaintHistoryASD},
			// {separator: true},
			{name: 'Export', bclass: 'Deletehir', onpress : eventHandlerComplaintHistoryASD},
			{separator: true},	
			// {name: 'Delete', bclass: 'Deletehir', onpress : eventHandlercomplaintcallcenter},
			// {separator: true}		
			
			],
		searchitems : [
			{display: 'BSN ', name : 'c.product_serial_no', isdefault: true},
			{display: 'Complaint Status', name : 'under_process', isdefault: true},
			{display: 'ASD Name', name : 'SD.service_distributor_name', isdefault: true},
			{display: 'Customer Phone', name : 'customer_phone_no', isdefault: true},
			{display: 'Complaint Number', name : 'complaint_no', isdefault: true},
			{display: 'Complaint Date', name : 'created_date', isdefault: true}
			],
		sortname: "c.complaint_id",
		sortorder: "desc",
		usepager: true,
		title: 'Complaints List',
		useRp: true,
		rp: 15,
		showTableToggleBtn: true,
		width: "900",
		height: "auto"
	});  
}



function eventHandlerComplaintHistoryASD(com,grid) {
	
	if (com=='Import')	{
		//location.href="import_item.php";	
		location.href="#";	
	}
	
		
	
	
	
	else if (com=='Export')	{
		location.href="export.inc.php?export_complaint_history_asd";		
	}
	else if (com=='Add')	{
		location.href="raisecomplaint_call_center.php";		
	}else{
		if( j('.trSelected',grid).length<=0 ||  j('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=j('.trSelected',grid);		
		if (com=='Edit')
		{
			//alert('HEllo');
			//alert("addhierarchy.php?id="+items[0].id.substr(3));
			location.href="raisecomplaint_call_center.php?id="+items[0].id.substr(3);	
			
		}
		
		
		
		
		
		if (com=='Delete'){
			if(confirm("Are you sure, you want to delete selected data?")){
					
				j.post("call_center_complaint.php", {"delete":"yes","id":items[0].id.substr(3)},function(data)
				{		 
				  if(data!="") {					 
					 j('#flex1').flexOptions().flexReload(); 
				  }else {
					  alert("Record doesn't exists.")
				  }
				  
				});		
			}			
		}	
	}
}
	
/*************************************************  Complaints History to ASD *************************************************/



/***********************************************  Completed Complaints of SP *************************************************/

function completedComplaintsSP(){
	
	j("#flex1").flexigrid({
		url: 'completed_complaints.php?show=yes',
		dataType: 'json',
		method : "GET", 
		singleSelect:true, 
		colModel : [		
			{display: 'Battery Serial No.', name : 'c.product_serial_no', width : 150, sortable : true, align: 'left'},
			{display: 'Complaint Number.', name : 'c.complaint_no', width : 150, sortable : true, align: 'left'},
			{display: 'Operator Name.', name : 'operator_name', width : 150, sortable : true, align: 'left'},
			{display: 'Auth Token', name : 'auth_token', width : 150, sortable : true, align: 'left'},
			{display: 'Action Taken', name : 'action_taken', width : 150, sortable : true, align: 'left'},
			{display: 'Complaint Status', name : 'under_process', width : 150, sortable : true, align: 'left'},
			{display: 'Call Close Datetime', name : 'call_close_datetime', width : 150, sortable : true, align: 'left'},
			{display: 'Service Distributor', name : 'SD.service_distributor_name', width : 120, sortable : true, align: 'left'},
			{display: 'Customer Name', name : 'customer_name', width : 120, sortable : true, align: 'left'},
			{display: 'Customer Phone', name : 'customer_phone_no', width : 120, sortable : true, align: 'left'},
			{display: 'Customer Address', name : 'customer_address', width : 120, sortable : true, align: 'left'},
			{display: 'Customer Email', name : 'customer_email', width : 120, sortable : true, align: 'left'},		
			{display: 'Sale Date', name : 'pdate', width : 120, sortable : true, align: 'left'},	
			{display: 'Service Distributor SP', name : 'service_distributor_sp_name', width : 120, sortable : true, align: 'left'},
			{display: 'Service Distributor SP Phone', name : 'service_distributor_sp_phone_no', width : 120, sortable : true, align: 'left'},	
			// {display: 'status', name : 'status', width : 100, sortable : true, align: 'left'},
            //{display: 'Image', name : 'images', width : 120, sortable : true, align: 'left'},
			{display: 'Complaint Date', name : 'Complaint', width : 100, sortable : true, align: 'left'},
			{display: 'Complaint TAT1', name : 'cctat1', width : 100, sortable : true, align: 'left'},
			{display: 'Complaint TAT2', name : 'cctat2', width : 100, sortable : true, align: 'left'},
			{display: 'Approver Comments', name : 'approver_comments', width : 200, sortable : true, align: 'left'},
			{display: 'Comment ', name : 'comment', width : 200, sortable : true, align: 'left'}
			],
		buttons : [
			// {name: 'Add', bclass: 'addhir', onpress : eventHandlerComplaintHistoryASD},
			// {separator: true},		
			{name: 'Edit', bclass: 'edithir', onpress : eventHandlerCompletedComplaintsSP},
			{separator: true},
			{name: 'Export', bclass: 'Deletehir', onpress : eventHandlerCompletedComplaintsSP},
			{separator: true},	
			// {name: 'Delete', bclass: 'Deletehir', onpress : eventHandlercomplaintcallcenter},
			// {separator: true}		
			
			],
		searchitems : [
			{display: 'Battery Serial No. ', name : 'c.product_serial_no', isdefault: true},
			// {display: 'Complaint Status', name : 'complain_status', isdefault: true},
			{display: 'ASD Name', name : 'SD.service_distributor_name', isdefault: true},
			{display: 'Customer Phone', name : 'customer_phone_no', isdefault: true},
			{display: 'Complaint Number', name : 'complaint_no', isdefault: true},
			{display: 'Complaint Status', name : 'under_process', isdefault: true},
			{display: 'Complaint Date', name : 'created_date', isdefault: true}
			],
		sortname: "c.complaint_id",
		sortorder: "desc",
		usepager: true,
		title: 'Complaints List',
		useRp: true,
		rp: 15,
		showTableToggleBtn: true,
		width: "900",
		height: "auto"
	});  
}



function eventHandlerCompletedComplaintsSP(com,grid) {
	
	if (com=='Import')	{
		//location.href="import_item.php";	
		location.href="#";	
	}
	
		
	
	
	
	else if (com=='Export')	{
		location.href="export.inc.php?export_completed_complaintsSP";		
	}
	else if (com=='Add')	{
		location.href="raisecomplaint_call_center.php";		
	}else{
		if( j('.trSelected',grid).length<=0 ||  j('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=j('.trSelected',grid);		
		if (com=='Edit')
		{
			location.href="claim_approval_edit.php?id="+items[0].id.substr(3);			
		}
		
		
		
		
		
		if (com=='Delete'){
			if(confirm("Are you sure, you want to delete selected data?")){
					
				j.post("call_center_complaint.php", {"delete":"yes","id":items[0].id.substr(3)},function(data)
				{		 
				  if(data!="") {					 
					 j('#flex1').flexOptions().flexReload(); 
				  }else {
					  alert("Record doesn't exists.")
				  }
				  
				});		
			}			
		}	
	}
}
	
/*************************************************  Complaints History to ASD *************************************************/


/*********************************** Battery Needs to be Recharge Chirag 18 April,2017 ****************************************/

function batteryForRecharge(){
j("#flex1").flexigrid({
	url: 'battery_for_recharge.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Item Name', name : 'item_name', width : 150, sortable : true, align: 'left'},
		{display: 'BSN', name : 'bsn', width : 150, sortable : true, align: 'left'},
		{display: 'Status', name : 'status', width : 80, sortable : true, align: 'left'},
		
		{display: 'Manufacture Date', name : 'manufacture_date', width : 100, sortable : true, align: 'left'},
		// {display: 'Date of Sale', name : 'date_of_sale', width : 80, sortable : true, align: 'left'},
		{display: 'WH Dispatch Date', name : 'date_of_sale', width : 100, sortable : true, align: 'left'},
		{display: 'Warranty Upto', name : 'date_of_sale', width : 100, sortable : true, align: 'left'},
		{display: 'Recharged Date', name : 'stk_recharged_date', width : 80, sortable : true, align: 'left'},

		// {display: 'Replaced BSN', name : 'replace_battery_bsn', width : 120, sortable : true, align: 'left'},
		// {display: 'WH Issue New BSN', name : 'wh_issue_new_bsn', width : 120, sortable : true, align: 'left'},
		// {display: 'Customer Name', name : 'date_of_sale', width : 80, sortable : true, align: 'left'},
		// {display: 'Customer Phone', name : 'date_of_sale', width : 80, sortable : true, align: 'left'},
		//{display: 'Battery Ageing(In Days)', name : '', width : 120, sortable : true, align: 'left'},

		{display: 'Dispatched WH', name : 'warehouse_name', width : 120, sortable : true, align: 'left'},
		// {display: 'Dispatched Distributor', name : 'distributor_name', width : 120, sortable : true, align: 'left'},
		// {display: 'Dispatched Dealer', name : 'dealer_name', width : 120, sortable : true, align: 'left'},
		
		{display: 'Item Code', name : 'item_code', width : 100, sortable : true, align: 'left'},
		
		{display: 'Category Name', name : 'category_name', width : 150, sortable : true, align: 'left'},
		
		{display: 'Warranty Period', name : 'warranty_period', width : 100, sortable : true, align: 'left'},
		{display: 'ProRata Period', name : 'warranty_prodata', width : 100, sortable : true, align: 'left'},
		{display: 'Grace Period', name : 'warranty_grace_period', width : 100, sortable : true, align: 'left'},

		// {display: 'Added By', name : 'user_type', width : 100, sortable : true, align: 'left'},
		// {display: 'Adder Name', name : '', width : 100, sortable : true, align: 'left'},

		// {display: 'Last Update Date', name : 'last_updated_date', width : 100, sortable : true, align: 'left'},
		
		],
	buttons : [
		{name: 'Edit', bclass: 'Edit', onpress : eventHandlerBatteryRecharge},
		{separator: true},		
		],
	searchitems : [
		{display: 'BSN', name : 'bsn', isdefault: true},
		{display: 'Warehouse Code', name : 'warehouse_code'},
		{display: 'Warehouse Name', name : 'warehouse_name'},
		{display: 'Item Code', name : 'item_code'},
		{display: 'Item Name', name : 'item_name'},
		//{display: 'Status', name : 'BSN.status'}
		
		],
	sortname: "item_name",
	sortorder: "asc",
	usepager: true,
	title: 'Batteries Need to Recharge',
	useRp: true,
	rp: 20,
	showTableToggleBtn: true,
	width: '740',
	height: 'auto'
});  
}



function eventHandlerBatteryRecharge(com,grid)
{
	//alert(com);
	if (com=='Add')	{
		location.href="addbsn.php";		
	}else{
		if( j('.trSelected',grid).length<=0 ||  j('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=j('.trSelected',grid);	
		
		if (com=='Edit'){
			location.href="recharge_by_service_personnel.php?id="+items[0].id.substr(3);			
		} else if (com=='Delete'){
			if(confirm("Are you sure, you want to delete selected Category?")){
					
				j.post("addbsn.php", {"delete":"yes","id":items[0].id.substr(3)},function(data)
				{		 
				  if(data!="") {					 
					 j('#flex1').flexOptions().flexReload(); 
				  }else {
					  alert("Record doesn't exists.")
				  }
				  
				});		
			}			
		}	
	}
}

/*********************************** Battery Needs to be Recharge Chirag 18 April,2017 ****************************************/

/*********************************** Battery Needs to be Scrap Chirag 20 April,2017 ******************************************/

function batteryForScrap(){
j("#flex1").flexigrid({
	url: 'battery_for_scrap.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Item Name', name : 'item_name', width : 150, sortable : true, align: 'left'},
		{display: 'BSN', name : 'bsn', width : 150, sortable : true, align: 'left'},
		{display: 'Manufacture Date', name : 'manufacture_date', width : 100, sortable : true, align: 'left'},
		{display: 'Date of Sale', name : 'date_of_sale', width : 80, sortable : true, align: 'left'},
		{display: 'WH Dispatch Date', name : 'factory_dispatch_date', width : 100, sortable : true, align: 'left'},
		{display: 'Warranty Upto', name : 'date_of_sale', width : 100, sortable : true, align: 'left'},
		{display: 'Recharged Date', name : 'stk_recharged_date', width : 80, sortable : true, align: 'left'},		
		{display: 'Item Code', name : 'item_code', width : 100, sortable : true, align: 'left'},		
		{display: 'Category Name', name : 'category_name', width : 150, sortable : true, align: 'left'},		
		{display: 'Brand Name', name : 'brand_name', width : 150, sortable : true, align: 'left'},		
		{display: 'Warranty Period', name : 'warranty_period', width : 100, sortable : true, align: 'left'},
		{display: 'ProRata Period', name : 'warranty_prodata', width : 100, sortable : true, align: 'left'},
		{display: 'Grace Period', name : 'warranty_grace_period', width : 100, sortable : true, align: 'left'},
		{display: 'Battery Status', name : 'battery_status', width : 100, sortable : true, align: 'left'},
		// {display: 'Added By', name : 'user_type', width : 100, sortable : true, align: 'left'},
		// {display: 'Adder Name', name : '', width : 100, sortable : true, align: 'left'},

		// {display: 'Last Update Date', name : 'last_updated_date', width : 100, sortable : true, align: 'left'},
		
		],
	buttons : [
		{name: 'Edit', bclass: 'Edit', onpress : eventHandlerBatteryScrap},
		{separator: true},		
		],
	searchitems : [
		{display: 'BSN', name : 'bsn', isdefault: true},
		{display: 'Brand', name : 'brand_name'},
		{display: 'Category', name : 'category_name'},
		{display: 'Item Code', name : 'item_code'},
		{display: 'Item Name', name : 'item_name'},
		{display: 'Status', name : 'BSN.status'}
		
		],
	sortname: "BSN.last_updated_date",
	sortorder: "asc",
	usepager: true,
	title: 'Batteries Need to Recharge',
	useRp: true,
	rp: 20,
	showTableToggleBtn: true,
	width: '740',
	height: 'auto'
});  
}



function eventHandlerBatteryScrap(com,grid)
{
	//alert(com);
	if (com=='Add')	{
		location.href="addbsn.php";		
	}else{
		if( j('.trSelected',grid).length<=0 ||  j('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=j('.trSelected',grid);	
		
		if (com=='Edit'){
			location.href="scrapped_by_warehouse.php?id="+items[0].id.substr(3);			
		} else if (com=='Delete'){
			if(confirm("Are you sure, you want to delete selected Category?")){
					
				j.post("addbsn.php", {"delete":"yes","id":items[0].id.substr(3)},function(data)
				{		 
				  if(data!="") {					 
					 j('#flex1').flexOptions().flexReload(); 
				  }else {
					  alert("Record doesn't exists.")
				  }
				  
				});		
			}			
		}	
	}
}

/*********************************** Battery Needs to be Scrap Chirag 20 April,2017 ******************************************/



/*********************************************************************************
* DESC: Show warehouse stock transfer list
* Author: AJAY
* Created: 2017-04-26
*
*********************************************************************************/


function showWarehouseStockTransferList(){
j("#flex1").flexigrid({
	url: 'wh_stock_transfer_list.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'From Warehouse', name : 'from_warehouse_name', width : 180, sortable : true, align: 'left'},
		{display: 'To Warehouse', name : 'to_warehouse_name', width : 180, sortable : true, align: 'left'},
		{display: 'Transfer BSN', name : 'ttlbsntransfer', width : 100, sortable : true, align: 'left'},
		{display: 'Dispatched No.', name : 'bill_no', width : 100, sortable : true, align: 'left'},
		{display: 'Dispatched Date', name : 'bill_date', width : 100, sortable : true, align: 'left'},
		{display: 'Transfer Time', name : 'transfer_datetime', width : 170, sortable : true, align: 'left'},
                {display: 'TallyMasterId', name : 'masterid', width : 170, sortable : true, align: 'left'},
		],
	buttons : [
		{name: 'Transfer Stock', bclass: 'Transfer Stock', onpress : eventHandlerWarehouseStockTransfer},
		{separator: true},
		{name: 'Transfer Bulk Stock', bclass: 'Transfer Bulk Stock', onpress : eventHandlerWarehouseStockTransfer},
		{separator: true},
		{name: 'View', bclass: 'View', onpress : eventHandlerWarehouseStockTransfer},
		{separator: true},
		{name: 'Export', bclass: 'export', onpress : eventHandlerWarehouseStockTransfer},
		{separator: true},
		],
	searchitems : [
		{display: 'From Warehouse', name : 'W.warehouse_name', isdefault: true},
		{display: 'To Warehouse', name : 'W2.warehouse_name'}
		
		],
	sortname: "created_date",
	sortorder: "asc",
	usepager: true,
	title: 'Stock Tranfser List',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 900,
	height: 'auto'
});  
}
function eventHandlerWarehouseStockTransfer(com,grid)
{
	if (com=='Transfer Stock')	{
		location.href="wh_stock_transfer.php";		
	}
	else if(com=='Export')
	{
		location.href="export.inc.php?export_warehouse_transfer_list";
	}
	else if (com=='Transfer Bulk Stock')	{
		location.href="wh_stock_transfer_bulk.php";		
	}
	else{
		if( j('.trSelected',grid).length<=0 ||  j('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=j('.trSelected',grid);		
		if (com=='View'){
			location.href="wh_stock_transfer_detail_view.php?id="+items[0].id.substr(3);			
		}
		if (com=='Delete'){
			if(confirm("Are you sure, you want to delete selected Category?")){
					
				j.post("category.php", {"delete":"yes","id":items[0].id.substr(3)},function(data)
				{		 
				  if(data!="") {					 
					 j('#flex1').flexOptions().flexReload(); 
				  }else {
					  alert("Record doesn't exists.")
				  }
				  
				});		
			}			
		}	
	}
}





/*********************************************************************************
* DESC: Show warehouse stock transfer list
* Author: AJAY
* Created: 2017-04-26
*
*********************************************************************************/

//changes by arvind
/***************** RetailerSalesEntryReport List *************************/

function showRetailerSalesReport(valid_user){



if(valid_user == true){
	var colModel01 = [
	{name: 'Add Sales Entry', bclass: 'viewmsg', onpress : dealereventHandlerCapacity},
		 {separator: true}
		
		];
} else {
	var colModel01 = [
		
		];
	
}




j("#flex1").flexigrid({
	url: 'retailer_sales_report.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Customer Name', name : 'customer_name', width : 100, sortable : true, align: 'left'},
		{display: 'Customer Address', name : 'customer_address', width : 100, sortable : true, align: 'left'},
		{display: 'Customer Phone No', name : 'customer_phone_no', width : 100, sortable : true, align: 'left'},
		{display: 'BSN', name : 'bsn', width : 100, sortable : true, align: 'left'},
		{display: 'Amount', name : 'price', width : 100, sortable : true, align: 'left'},
		{display: 'Date of Sale', name : 'date_of_order', width : 100, sortable : true, align: 'left'}
		//{display: 'Status', name : 'status', width : 100, sortable : true, align: 'left'}
		//{display: 'Dealer Address', name : 'retailer_address', width : 100, sortable : true, align: 'left'},
		//{display: 'Dealer Code', name : 'retailer_code', width : 100, sortable : true, align: 'left'},
		//{display: 'Dealer Email', name : 'retailer_email', width : 100, sortable : true, align: 'left'},
		//{display: 'Dealer Phone No', name : 'retailer_phone_no', width : 100, sortable : true, align: 'left'}
		],
	buttons : colModel01,
	searchitems : [
				{display: 'Customer Name', name : 'customer_name', isdefault: true},
		{display: 'Date of Sale', name : 'date_of_order'},
		{display: 'BSN', name : 'bsn'}
		],
	sortname: "date_of_order",
	sortorder: "desc",
	usepager: true,
	title: 'Sales Report',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 1000,
	height: 'auto'
});  
}



function dealereventHandlerCapacity(com,grid)
{
	if (com=='Add Sales Entry')	{
		location.href="dealer_sales_entry.php";		
	}


	// if (com=='Add Counter Entry')
	// {
	// 	location.href="distributor_counter_sale.php";
	// }		
	// 	else{
	// 		alert("sdssd");
	// 	// if( j('.trSelected',grid).length<=0 ||  j('.trSelected',grid).length>1){
	// 	// 	alert("Please Select One Record.");
	// 		return false;
	// 	}
		//var items=j('.trSelected',grid);		
			
		
		// if (com=='Delete'){
		// 	if(confirm("Are you sure, you want to delete selected watts?")){
					
		// 		j.post("battery_power.php", {"delete":"yes","id":items[0].id.substr(3)},function(data)
		// 		{		 
		// 		  if(data!="") {					 
		// 			 j('#flex1').flexOptions().flexReload(); 
		// 		  }else {
		// 			  alert("Record doesn't exists.")
		// 		  }
				  
		// 		});		
		// 	}			
		// }	
	//}
}


/*********************End RetailerSalesEntryReport List*****************************/

/********** Start Show Secondary Scheme To Retailer That is Created By Distributor***********/
function showSecondarySchemeList(){
j("#flex1").flexigrid({
	url: 'secondary_scheme_list.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Description', name : 'discount_desc', width : 150, sortable : true, align: 'left'},
		{display: 'Scheme Mode', name : 'mode', width : 100, sortable : true, align: 'left'},
	//	{display: 'Division', name : 'division_id', width : 100, sortable : true, align: 'left'},
		{display: 'Scheme', name : 'discount', width : 100, sortable : true, align: 'left'},
		{display: 'Secondary Scheme', name : 'is_dis_discount', width : 100, sortable : true, align: 'left'},
		{display: 'Item Type', name : 'item_type', width : 100, sortable : true, align: 'left'},
	//	{display: 'Party Type', name : 'price_detail', width : 100, sortable : true, align: 'left'},
		{display: 'Scheme Type', name : 'discount_type', width : 100, sortable : true, align: 'left'},
		//{display: 'Minimum Amount', name : 'minimum_amount', width : 100, sortable : true, align: 'left'},
		//{display: 'Minimum Quantity', name : 'minimum_quantity', width : 100, sortable : true, align: 'left'},
		{display: 'Start Date', name : 'start_date', width : 100, sortable : true, align: 'left'},
		{display: 'End Date', name : 'end_date', width : 100, sortable : true, align: 'left'},
		{display: 'Distributor Name', name : 'distributor_name', width : 100, sortable : true, align: 'left'},
		{display: 'Distributor Address', name : 'distributor_address', width : 100, sortable : true, align: 'left'},
		{display: 'Distributor Code', name : 'distributor_code', width : 100, sortable : true, align: 'left'},
		{display: 'Distributor Email', name : 'distributor_email', width : 100, sortable : true, align: 'left'},
		{display: 'Distributor Phone No', name : 'distributor_phone_no', width : 100, sortable : true, align: 'left'}
	//	{display: 'Status', name : 'status', width : 50, sortable : true, align: 'left'}
		],
	buttons : [
		
		
		],
	searchitems : [
		{display: 'Description', name : 'discount_desc', isdefault: true}
		
		],
	sortname: "discount_id",
	sortorder: "Desc",
	usepager: true,
	title: 'Scheme',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 1000,
	height: 'auto'
});  
}
/********** End Show Secondary Scheme To Retailer That is Created By Distributor***********/







/*********************************************************************************
* Desc: Service Distribtuor list
* Author: AJAY
* Created: 2017-04-27
*
*********************************************************************************/



function showServiceDistributor(){
j("#flex1").flexigrid({
	url: 'service_distributor.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Service Distributor Name', name : 'service_distributor_name', width : 120, sortable : true, align: 'left'},
		{display: 'Service Distributor Code', name : 'service_distributor_code', width : 120, sortable : true, align: 'left'},
		{display: 'Country', name : 'country_name', width : 80, sortable : true, align: 'left'},
		{display: 'Region', name : 'region_name', width : 80, sortable : true, align: 'left'},
		{display: 'Zone', name : 'zone_name', width : 80, sortable : true, align: 'left'},
		{display: 'State', name : 'state_name', width : 80, sortable : true, align: 'left'},
		{display: 'City', name : 'city_name', width : 80, sortable : true, align: 'left'},
		{display: 'Warehouse', name : 'warehouse_name', width : 130, sortable : true, align: 'left'},
		{display: 'Address', name : 'service_distributor_address', width : 100, sortable : true, align: 'left'},
		{display: 'Phone Number', name : 'service_distributor_phone_no', width : 80, sortable : true, align: 'left'},
		{display: 'Pincode/Zipcode', name : 'zipcode', width : 100, sortable : true, align: 'left'},
		{display: 'Username', name : 'username', width : 80, sortable : true, align: 'left'},
		{display: 'Email ID', name : 'email_id', width : 100, sortable : true, align: 'left'},
		{display: 'Start Date', name : 'start_date', width : 80, sortable : true, align: 'left'},
		{display: 'End Date', name : 'end_date', width : 80, sortable : true, align: 'left'},
		{display: 'Status', name : 'status', width : 80, sortable : true, align: 'center'},
		{display: 'Login Status', name : 'loginStatus', width : 80, sortable : true, align: 'center'},
		{display: 'App Version', name : 'appStatus', width : 80, sortable : true, align: 'center'}
			
		],
	buttons : [
		{name: 'Add', bclass: 'add', onpress : eventHandlerServiceDistributor},
		{separator: true},		
		{name: 'Edit', bclass: 'edit', onpress : eventHandlerServiceDistributor},
		{separator: true},
		// {name: 'Delete', bclass: 'delete', onpress : eventHandlerWarAdminUser},
		//{separator: true}
		],
	searchitems : [
		{display: 'Name', name : 'service_distributor_name', isdefault: true},
		{display: 'Username', name : 'username'},
		{display: 'Email ID', name : 'email_id'},
		{display: 'Phone Number', name : 'service_distributor_phone_no'},
		{display: 'Country', name : 'country_name'},
		{display: 'Region', name : 'region_name'},
		{display: 'Zone', name : 'zone_name'},
		{display: 'State', name : 'state_name'},
		{display: 'City', name : 'city_name'},
		{display: 'Warehouse', name : 'warehouse_name'},
		{display: 'Address', name : 'service_distributor_address'}
		],
	sortname: "service_distributor_name",
	sortorder: "asc",
	usepager: true,
	title: 'Service Distributor',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 700,
	height: 'auto'
});  
}
function eventHandlerServiceDistributor(com,grid)
{
	if (com=='Add')	{
		location.href="service_distributor.php?add";	
	}else if (com=='Import')	{
		location.href="service_distributor.php?import";	
	}else if (com=='Export')	{
		location.href="export.inc.php?export_warehouses_list";	
	}
	else{
		if( j('.trSelected',grid).length<=0 ||  j('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=j('.trSelected',grid);		
		if (com=='Edit'){
			location.href="service_distributor.php?id="+items[0].id.substr(3);			
		}else if (com=='Create Login')	{
		location.href="#.php?login&id="+items[0].id.substr(3);
	}
		else if (com=='Delete')	{
		if(confirm("Are you sure, you want to delete selected Warehouse?")){
					
				j.post("service_distributor.php", {"delete":"yes","id":items[0].id.substr(3)},function(data)
				{
					j("#message-green").hide();
				  if(data!="") {
				  	//alert(data);
				  	if(data >0 )
				  	{				  		
				  		alert("This warehouse cannot be deleted.");
				  	}
				  				 
					 j('#flex1').flexOptions().flexReload(); 
				  }else {
					  alert("Record doesn't exists.")
				  }
				  
				});		
			}
	}
			
	}
}




/*********************************************************************************
* Desc: Service Distribtuor list
* Author: AJAY
* Created: 2017-04-27
*
*********************************************************************************/









/*********************************************************************************
* Desc: Service Distribtuor Service Personnel list
* Author: AJAY
* Created: 2017-04-27
*
*********************************************************************************/



function showServiceDistributorSP(){
j("#flex1").flexigrid({
	url: 'service_distributor_sp.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Service Distributor SP Name', name : 'service_distributor_sp_name', width : 150, sortable : true, align: 'left'},
		{display: 'Service Distributor SP Code', name : 'service_distributor_sp_code', width : 150, sortable : true, align: 'left'},
		{display: 'Service Distributor Name', name : 'service_distributor_name', width : 150, sortable : true, align: 'left'},
		{display: 'Service Distributor Code', name : 'service_distributor_code', width : 150, sortable : true, align: 'left'},
		{display: 'Country', name : 'country_name', width : 80, sortable : true, align: 'left'},
		{display: 'Region', name : 'region_name', width : 80, sortable : true, align: 'left'},
		{display: 'Zone', name : 'zone_name', width : 80, sortable : true, align: 'left'},
		{display: 'State', name : 'state_name', width : 80, sortable : true, align: 'left'},
		{display: 'City', name : 'city_name', width : 80, sortable : true, align: 'left'},
		{display: 'Address', name : 'service_distributor_sp_address', width : 100, sortable : true, align: 'left'},
		{display: 'Phone Number', name : 'service_distributor_sp_phone_no', width : 80, sortable : true, align: 'left'},
		{display: 'Pincode/Zipcode', name : 'zipcode', width : 100, sortable : true, align: 'left'},
		{display: 'Username', name : 'username', width : 80, sortable : true, align: 'left'},
		{display: 'Email ID', name : 'email_id', width : 100, sortable : true, align: 'left'},
		{display: 'Start Date', name : 'start_date', width : 80, sortable : true, align: 'left'},
		{display: 'End Date', name : 'end_date', width : 80, sortable : true, align: 'left'},
		{display: 'Status', name : 'status', width : 80, sortable : true, align: 'center'},
		{display: 'Login Status', name : 'loginStatus', width : 80, sortable : true, align: 'center'},
		{display: 'App Version', name : 'appStatus', width : 80, sortable : true, align: 'center'}
			
		],
	buttons : [
		{name: 'Add', bclass: 'add', onpress : eventHandlerServiceDistributorSP},
		{separator: true},		
		{name: 'Edit', bclass: 'edit', onpress : eventHandlerServiceDistributorSP},
		{separator: true},
		// {name: 'Delete', bclass: 'delete', onpress : eventHandlerWarAdminUser},
		//{separator: true}
		],
	searchitems : [
		{display: 'Name', name : 'service_distributor_sp_name', isdefault: true},
		{display: 'Username', name : 'username'},
		{display: 'Email ID', name : 'email_id'},
		{display: 'Phone Number', name : 'service_distributor_sp_phone_no'},
		{display: 'Country', name : 'country_name'},
		{display: 'Region', name : 'region_name'},
		{display: 'Zone', name : 'zone_name'},
		{display: 'State', name : 'state_name'},
		{display: 'City', name : 'city_name'},
		{display: 'Address', name : 'service_distributor_sp_address'}
		],
	sortname: "service_distributor_sp_name",
	sortorder: "asc",
	usepager: true,
	title: 'Service Distributor SP',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 700,
	height: 'auto'
});  
}
function eventHandlerServiceDistributorSP(com,grid)
{
	if (com=='Add')	{
		location.href="service_distributor_sp.php?add";	
	}else if (com=='Import')	{
		location.href="service_distributor_sp.php?import";	
	}else if (com=='Export')	{
		location.href="export.inc.php?export_warehouses_list";	
	}
	else{
		if( j('.trSelected',grid).length<=0 ||  j('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=j('.trSelected',grid);		
		if (com=='Edit'){
			location.href="service_distributor_sp.php?id="+items[0].id.substr(3);			
		}else if (com=='Create Login')	{
		location.href="#.php?login&id="+items[0].id.substr(3);
	}
		else if (com=='Delete')	{
		if(confirm("Are you sure, you want to delete selected Warehouse?")){
					
				j.post("service_distributor_sp.php", {"delete":"yes","id":items[0].id.substr(3)},function(data)
				{
					j("#message-green").hide();
				  if(data!="") {
				  	//alert(data);
				  	if(data >0 )
				  	{				  		
				  		alert("This warehouse cannot be deleted.");
				  	}
				  				 
					 j('#flex1').flexOptions().flexReload(); 
				  }else {
					  alert("Record doesn't exists.")
				  }
				  
				});		
			}
	}
			
	}
}




/*********************************************************************************
* Desc: Service Distribtuor Service Personnel list
* Author: AJAY
* Created: 2017-04-27
*
*********************************************************************************/



/****************************** Show BCFs from Service Distributor Chirag 27 April,2017 **********************************/

function showBcfSd(){
j("#flex1").flexigrid({
	url: 'bcf_from_service_distributors.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Complaint Number', name : 'complaint_no', width : 100, sortable : true, align: 'left'},
		{display: 'Operator Name', name : 'operator_name', width : 100, sortable : true, align: 'left'},
		{display: 'Complaint Date', name : 'created_date', width : 100, sortable : true, align: 'left'},
		{display: 'Battery Serial No.', name : 'bsn', width : 80, sortable : true, align: 'left'},
		{display: 'Auth Token', name : 'auth_token', width : 80, sortable : true, align: 'left'},
		{display: 'Action Taken', name : 'action_taken', width : 80, sortable : true, align: 'left'},
		{display: 'Complaint Status', name : 'BCFST', width : 150, sortable : true, align: 'left'},
		{display: 'Call Close Datetime', name : 'call_close_datetime', width : 150, sortable : true, align: 'left'},
		{display: 'Customer Name', name : 'customer_name', width : 80, sortable : true, align: 'left'},
		{display: 'Customer Address', name : 'customer_address', width : 100, sortable : true, align: 'left'},
		{display: 'Mobile Number', name : 'customer_phone_no', width : 120, sortable : true, align: 'left'},
		{display: 'Country', name : 'country_name', width : 150, sortable : true, align: 'left'},
		{display: 'State', name : 'state_name', width : 100, sortable : true, align: 'left'},
		{display: 'City', name : 'city_name', width : 80, sortable : true, align: 'left'},
		{display: 'View BCF', name : 'view_bcf', width : 80, sortable : true, align: 'left'},
		{display: 'Complaint Action', name : 'status', width : 120, sortable : true, align: 'left'},
		],
	buttons : [		
		{name: 'Edit', bclass: 'editqq', onpress : eventHandlerBcfSD},
		{separator: true},
		{name: 'Export', bclass: 'export', onpress : eventHandlerBcfSD},
		{separator: true}
		],
	searchitems : [
		{display: 'Complaint No.', name : 'complaint_no', isdefault: true},
		{display: 'Complaint Date', name : 'COMPLAINT.created_date', isdefault: true},
		{display: 'Battery Serial No.', name : 'bsn', isdefault: true},
		{display: 'Customer Name', name : 'customer_name', isdefault: true},
		{display: 'Customer Mobile', name : 'customer_phone_no', isdefault: true},
		{display: 'Country', name : 'country_name', isdefault: true},
		{display: 'State', name : 'state_name', isdefault: true},
		{display: 'City', name : 'city_name', isdefault: true},
		{display: 'Complaint Status', name : 'under_process', isdefault: true}
		],
	sortname: "COMPLAINT.complaint_no",
	sortorder: "desc",
	usepager: true,
	title: 'BCF List from Service Distributor',
	useRp: true,
	rp: 15,
	showTableToggleBtn: true,
	width: 1000,
	height: 'auto'
});  
}

function eventHandlerBcfSD(com,grid)
{
	if (com=='Add')	{
		location.href="claim_approval_call_center.php";		
	}
	else if (com=='Export')	{
			location.href="export.inc.php?export_final_process_complaints";		
		}
	else{
		if( j('.trSelected',grid).length<=0 ||  j('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		
		var items=j('.trSelected',grid);		
		if (com=='Edit'){
			location.href="claim_approval_call_center.php?id="+items[0].id.substr(3);			
		}
		if (com=='Delete'){
			if(confirm("Are you sure, you want to delete selected Jobcard?")){
					
				j.post("claim_approval_call_center.php", {"delete":"yes","id":items[0].id.substr(3)},function(data)
				{		 
				  if(data!="") {					 
					 j('#flex1').flexOptions().flexReload(); 
				  }else {
					  alert("Record doesn't exists.")
				  }
				  
				});		
			}			
		}	
	}
}

/****************************** Show BCFs from Service Distributor Chirag 27 April,2017 **********************************/



/********************************************************************************
*
* DESC: Show Distributor, Retailer Survey Reponse List
*  
* Created By: Pooja@2017-04-28
*
*********************************************************************************/

function showResponses(){
j("#flex1").flexigrid({
	url: 'survey_response_list.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [
		{display: 'Distributor Name', name : 'distributor_name', width : 300, sortable : true, align: 'left'},	
		{display: 'Dealer Name', name : 'retailer_name', width : 300, sortable : true, align: 'left'},
		{display: 'Request Date', name : '', width : 250, sortable : true, align: 'left'},			
		{display: 'Action', name : '', width : 250, sortable : true, align: 'left'}		
		],
	buttons : [
		
		
		],
	searchitems : [
		{display: 'Distributor Name', name : 'distributor_name', isdefault: true},
		{display: 'Dealer Name', name : 'retailer_name'},
		//{display: 'Bussiness Phone', name : 'business_phone'},
		//{display: 'Status', name : 'status'}
		
		],
	sortname: "sur_res_id",
	sortorder: "desc",
	usepager: true,
	title: 'Survey Response',
	useRp: true,
	rp: 15,
	showTableToggleBtn: true,
	width: '1000',
	height: 'auto'
});  
}


/********************************************************************************
*
* DESC: Show Distributor Sales Entry and Counter Sales Report
*  
*********************************************************************************/

function showDistributorSalesReport(valid_user){


if(valid_user == true){
	var colModel01 = [
	{name: 'Add Sales Entry', bclass: 'addsales', onpress : distributoreventHandlerCapacity},
	{separator: true},		
	{name: 'Add Counter Sales Entry', bclass: 'addcountersalesentry', onpress : distributoreventHandlerCapacity},
	{separator: true}
		];
} else {
	var colModel01 = [
		
		];
	
}



j("#flex1").flexigrid({
	url: 'distributor_sales_report.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Customer Name', name : 'customer_name', width : 100, sortable : true, align: 'left'},
		{display: 'Customer Address', name : 'customer_address', width : 100, sortable : true, align: 'left'},
		{display: 'Customer Phone No', name : 'customer_phone_no', width : 100, sortable : true, align: 'left'},
		{display: 'BSN', name : 'bsn', width : 100, sortable : true, align: 'left'},
		{display: 'Amount', name : 'price', width : 100, sortable : true, align: 'left'},
		{display: 'Date of Sale', name : 'sal_date', width : 100, sortable : true, align: 'left'},
		{display: 'Dealer Name', name : 'retailer_name', width : 100, sortable : true, align: 'left'},
		{display: 'Dealer Address', name : 'retailer_address', width : 100, sortable : true, align: 'left'},
		{display: 'Dealer Code', name : 'retailer_code', width : 100, sortable : true, align: 'left'},
		{display: 'Sales Entry Date', name : 'Entry_date', width : 100, sortable : true, align: 'left'},
		{display: 'Dealer Email', name : 'retailer_email', width : 100, sortable : true, align: 'left'},
		{display: 'Dealer Phone No', name : 'retailer_phone_no', width : 100, sortable : true, align: 'left'}
		// {display: 'Distributor Sales Entry', name : 'distributor_sales', width : 100, sortable : true, align: 'left'}
		//{display: 'Distributor Address', name : 'distributor_address', width : 100, sortable : true, align: 'left'},
		//{display: 'Distributor Code', name : 'distributor_code', width : 100, sortable : true, align: 'left'},
		//{display: 'Distributor Email', name : 'distributor_email', width : 100, sortable : true, align: 'left'},
		//{display: 'Distributor Phone No', name : 'distributor_phone_no', width : 100, sortable : true, align: 'left'}	
		],
	buttons :colModel01,
	searchitems : [
		{display: 'Customer Name', name : 'customer_name', isdefault: true},
		{display: 'Date of Sale', name : 'date_of_order'},
		{display: 'BSN', name : 'bsn'}
		],
	sortname: "date_of_order",
	sortorder: "desc",
	usepager: true,
	title: 'Sales Report',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 1000,
	height: 'auto'
});  
}




function distributoreventHandlerCapacity(com,grid)
{
	if (com=='Add Sales Entry')	{
		location.href="distributor_sales_entry.php";		
	}


	if (com=='Add Counter Sales Entry')
	{
		location.href="distributor_counter_sale.php";
	}		
		else{
			//alert("sdssd");
		// if( j('.trSelected',grid).length<=0 ||  j('.trSelected',grid).length>1){
		// 	alert("Please Select One Record.");
			//return false;
		}
		//var items=j('.trSelected',grid);		
			
		
		// if (com=='Delete'){
		// 	if(confirm("Are you sure, you want to delete selected watts?")){
					
		// 		j.post("battery_power.php", {"delete":"yes","id":items[0].id.substr(3)},function(data)
		// 		{		 
		// 		  if(data!="") {					 
		// 			 j('#flex1').flexOptions().flexReload(); 
		// 		  }else {
		// 			  alert("Record doesn't exists.")
		// 		  }
				  
		// 		});		
		// 	}			
		// }	
	//}
}



/*****************End Show Distributor Sales Entry and Counter Sales Report
*  
*********************************************************************************/



/********************************************************************************
*
* DESC: Show Dealer Survey Reponse List
*  
* Created By: Pooja@2017-05-03
*
*********************************************************************************/

function showDealerResponses(){
j("#flex1").flexigrid({
	url: 'dealer_survey_response_list.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [
		{display: 'Dealer Name', name : 'retailer_name', width : 300, sortable : true, align: 'left'},
		{display: 'Request Date', name : '', width : 250, sortable : true, align: 'left'},			
		{display: 'Action', name : '', width : 250, sortable : true, align: 'left'}		
		],
	buttons : [
		
		
		],
	searchitems : [
		{display: 'Dealer Name', name : 'retailer_name'},
		//{display: 'Phone', name : 'phone'},
		//{display: 'Status', name : 'status'}
		
		],
	sortname: "sur_res_id",
	sortorder: "desc",
	usepager: true,
	title: 'Dealer Survey Response',
	useRp: true,
	rp: 15,
	showTableToggleBtn: true,
	width: '1000',
	height: 'auto'
});  
}


/********************************************************************************
*
* DESC: Show Distributor Survey Reponse List
*  
* Created By: Pooja@2017-05-03
*
*********************************************************************************/

function showDistributorResponses(){
j("#flex1").flexigrid({
	url: 'distributor_survey_response_list.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [
		{display: 'Distributor Name', name : 'distributor_name', width : 300, sortable : true, align: 'left'},	
		{display: 'Request Date', name : '', width : 250, sortable : true, align: 'left'},			
		{display: 'Action', name : '', width : 250, sortable : true, align: 'left'}		
		],
	buttons : [
		
		
		],
	searchitems : [
		{display: 'Distributor Name', name : 'distributor_name', isdefault: true},
		//{display: 'Phone', name : 'phone'},
		//{display: 'Status', name : 'status'}
		
		],
	sortname: "sur_res_id",
	sortorder: "desc",
	usepager: true,
	title: 'Distributor Survey Response',
	useRp: true,
	rp: 15,
	showTableToggleBtn: true,
	width: '1000',
	height: 'auto'
});  
}



/*********************************************************************************
* 
* DESC: Show Distributor Sales Return Report 
* Created By: Pooja@2017-05-08
*
*********************************************************************************/


function showSalesRtrn(){

j("#flex1").flexigrid({
	url: 'sales_return.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Distributor Name', name : 'distributor_name', width : 200, sortable : true, align: 'left'},
		{display: 'Distributor Code', name : 'distributor_code', width : 100, sortable : true, align: 'left'},
		{display: 'Distributor Address', name : 'distributor_address', width : 200, sortable : true, align: 'left'},
		{display: 'Distributor Phone No', name : 'distributor_phone_no', width : 100, sortable : true, align: 'left'},
		{display: 'Distributor Email', name : 'distributor_email', width : 200, sortable : true, align: 'left'},
		{display: 'State', name : 'state', width : 100, sortable : true, align: 'left'},
		{display: 'Warehouse Name', name : 'warehouse_name', width : 200, sortable : true, align: 'left'},
		{display: 'BSN', name : 'bsn', width : 100, sortable : true, align: 'left'},
		{display: 'Item Name', name : 'item_name', width : 200, sortable : true, align: 'left'},
		{display: 'Item Code', name : 'item_code', width : 100, sortable : true, align: 'left'},
		{display: 'Date of Sale Return', name : 'date_of_order', width : 100, sortable : true, align: 'left'},
	//	{display: 'Time Of Sale Return', name : 'time_of_order', width : 100, sortable : true, align: 'left'},
		{display: 'Comments', name : 'comments', width : 100, sortable : true, align: 'left'},
		{display: 'Status', name : 'status', width : 100, sortable : true, align: 'left'}
		],
	buttons : [
			{name: 'SalesReturn', bclass: 'addhir', onpress : eventHandlerSalesReturn},
			{separator: true}
		//	{name: 'Edit', bclass: 'edithir', onpress : eventHandlerSalesReturn},
		//	{separator: true},
		//	{name: 'View', bclass: 'viewhir', onpress : eventHandlerSalesReturn},
		//	{separator: true},
		//	{name: 'Delete', bclass: 'Deletehir', onpress : eventHandlerSalesReturn},
		//	{separator: true}		
			],
	searchitems : [
		{display: 'Distributor Name', name : 'distributor_name', isdefault: true},
		{display: 'Warehouse Name', name : 'warehouse_name', isdefault: true},
		{display: 'Date of Sale', name : 'date_of_order'},
		{display: 'BSN', name : 'bsn'},
		{display: 'Status', name : 'ws.status'}
		],
	sortname: "date_of_order",
	sortorder: "desc",
	usepager: true,
	title: 'Sales Return Report',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 1000,
	height: 'auto'
});  
}

function eventHandlerSalesReturn(com,grid) {
	var items=j('.trSelected',grid);
	if (com=='Import')	{
		//location.href="import_item.php";	
		location.href="#";	
	}
	
		
	else if (com=='View')
	{
		location.href="#";
	}	
	
	
	else if (com=='Export')	{
		location.href="#";		
	}
	else if (com=='SalesReturn') {
		location.href="add_sales_return.php";		
	}else{
		if( j('.trSelected',grid).length<=0 ||  j('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
				
		if (com=='Edit')
		{
			
		//	location.href="add_sales_return.php?id="+items[0].id.substr(3);	
			
		}
		
		
		if (com=='Delete'){
			if(confirm("Are you sure, you want to delete selected data?")){
					
				j.post("sales_return.php", {"delete":"yes","id":items[0].id.substr(3)},function(data)
				{		 
				  if(data!="") {					 
					 j('#flex1').flexOptions().flexReload(); 
				  }else {
					  alert("Record doesn't exists.")
				  }
				  
				});		
			}			
		}	
	}
}

/*******************************************************
* 
*	DESC: Show Warehouse Pending Stock
* 
********************************************************/


function showWarehousePendingStck(){
j("#flex1").flexigrid({
	url: 'warehouse_pending_stock.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Distributor Name', name : 'distributor_name', width : 200, sortable : true, align: 'left'},
		{display: 'Distributor Code', name : 'distributor_code', width : 100, sortable : true, align: 'left'},
		{display: 'Distributor Address', name : 'distributor_address', width : 200, sortable : true, align: 'left'},
		{display: 'Distributor Phone No', name : 'distributor_phone_no', width : 100, sortable : true, align: 'left'},
		{display: 'Distributor Email', name : 'distributor_email', width : 200, sortable : true, align: 'left'},
		{display: 'State', name : 'state_name', width : 100, sortable : true, align: 'left'},
		{display: 'Warehouse Name', name : 'warehouse_name', width : 200, sortable : true, align: 'left'},
		{display: 'BSN', name : 'bsn', width : 100, sortable : true, align: 'left'},
		{display: 'Item Name', name : 'item_name', width : 200, sortable : true, align: 'left'},
		{display: 'Item Code', name : 'item_code', width : 100, sortable : true, align: 'left'},
		{display: 'Date of Sale Return', name : 'date_of_order', width : 100, sortable : true, align: 'left'},
		{display: 'Time Of Sale Return', name : 'time_of_order', width : 100, sortable : true, align: 'left'},
		{display: 'Comments', name : 'comments', width : 100, sortable : true, align: 'left'},
		{display: 'Status', name : 'status', width : 100, sortable : true, align: 'left'}
		],
	buttons : [
		{name: 'Accept/Reject BSN', bclass: 'Scan', onpress : eventHandlerWarehousePendingStock},
		{separator: true},		
		// {name: 'Edit', bclass: 'edit', onpress : eventHandlerMasterCategory},
		// {separator: true},
		],
	searchitems : [
		{display: 'Distributor Name', name : 'distributor_name', isdefault: true},
		{display: 'Warehouse Name', name : 'warehouse_name', isdefault: true},
		{display: 'Date of Sale', name : 'date_of_order'},
		{display: 'BSN', name : 'bsn'},
		{display: 'Status', name : 'ws.status'}
		
		],
	sortname: "date_of_order",
	sortorder: "desc",
	usepager: true,
	title: 'Warehouse Pending Stock',
	useRp: true,
	rp: 40,
	showTableToggleBtn: true,
	width: '740',
	height: 'auto'
});  
}



function eventHandlerWarehousePendingStock(com,grid)
{
	//alert(com);
	if (com=='Accept/Reject BSN')	{
		location.href="warehouse_scan_pending_btry.php";		
	}else{
		if( j('.trSelected',grid).length<=0 ||  j('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
			
	}
}

/******************************* Service Distributor SP Login Status By Chirag Gupta May 16, 2017 *************************/

function showLoginServiceDistributorLogin(){
j("#flex1").flexigrid({
	url: 'service_distributor_sp_list.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Service Distributor SP Name', name : 'service_distributor_sp_name', width : 120, sortable : true, align: 'left'},
		{display: 'Username', name : 'username', width : 120, sortable : true, align: 'left'},
		{display: 'Email ID', name : 'email_id', width : 150, sortable : true, align: 'left'},
		{display: 'Phone Number', name : 'service_distributor_sp_phone_no', width : 120, sortable : true, align: 'left'},
		{display: 'Login Status', name : '', width : 120, sortable : true, align: 'left'},
		{display: '', name : '', width : 120, sortable : true, align: 'left'}
			
		],
	buttons : [
		],
	searchitems : [
		{display: 'SP Name', name : 'service_distributor_sp_name', isdefault: true},
		{display: 'Username', name : 'username'},
		{display: 'Email ID', name : 'email_id'},
		{display: 'Phone Number', name : 'service_distributor_sp_phone_no'}
		],
	sortname: "service_distributor_sp_name",
	sortorder: "asc",
	usepager: true,
	title: 'Service Distributor SP List',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 850,
	height: 'auto'
});  
}


/******************************* Service Distributor SP Login Status By Chirag Gupta May 16, 2017 *************************/



/***********************************************  Completed Complaints of Approver **********************************************/

function completedComplaintsApprover(){
	
	j("#flex1").flexigrid({
		url: 'completed_complaints_by_approver.php?show=yes',
		dataType: 'json',
		method : "GET", 
		singleSelect:true, 
		colModel : [		
			{display: 'BSN Number.', name : 'c.product_serial_no', width : 150, sortable : true, align: 'left'},
			{display: 'Complaint Number.', name : 'c.complaint_no', width : 150, sortable : true, align: 'left'},
			{display: 'Service Distributor', name : 'SD.service_distributor_name', width : 120, sortable : true, align: 'left'},
			{display: 'Auth Token', name : 'auth_token', width : 120, sortable : true, align: 'left'},
			{display: 'Action Taken', name : 'action_taken', width : 150, sortable : true, align: 'left'},
			{display: 'Complaint Status', name : 'under_process', width : 200, sortable : true, align: 'left'},
			{display: 'Customer Name', name : 'customer_name', width : 120, sortable : true, align: 'left'},
			{display: 'Customer Phone', name : 'customer_phone_no', width : 120, sortable : true, align: 'left'},
			{display: 'Customer Address', name : 'customer_address', width : 120, sortable : true, align: 'left'},
			{display: 'Customer Email', name : 'customer_email', width : 120, sortable : true, align: 'left'},		
			{display: 'Sale Date', name : 'pdate', width : 120, sortable : true, align: 'left'},	
			{display: 'Service Distributor SP', name : 'service_distributor_sp_name', width : 120, sortable : true, align: 'left'},
			{display: 'Service Distributor SP Phone', name : 'service_distributor_sp_phone_no', width : 120, sortable : true, align: 'left'},	
			// {display: 'C Status', name : 'under_process', width : 100, sortable : true, align: 'left'},
			// {display: 'Complaint Status', name : 'status', width : 100, sortable : true, align: 'left'},
            //{display: 'Image', name : 'images', width : 120, sortable : true, align: 'left'},
			{display: 'Complaint Date', name : 'Complaint', width : 100, sortable : true, align: 'left'},
			{display: 'Complaint TAT1', name : 'cctat1', width : 100, sortable : true, align: 'left'},
			{display: 'Complaint TAT2', name : 'cctat2', width : 100, sortable : true, align: 'left'},
			{display: 'Comment ', name : 'comment', width : 200, sortable : true, align: 'left'}
			],
		buttons : [
			// {name: 'Add', bclass: 'addhir', onpress : eventHandlerComplaintHistoryASD},
			// {separator: true},		
			{name: 'Edit', bclass: 'edithir', onpress : eventHandlerCompletedComplaintsApprover},
			{separator: true},
			{name: 'Export', bclass: 'Deletehir', onpress : eventHandlerCompletedComplaintsApprover},
			{separator: true},	
			// {name: 'Delete', bclass: 'Deletehir', onpress : eventHandlercomplaintcallcenter},
			// {separator: true}		
			
			],
		searchitems : [
			{display: 'BSN ', name : 'c.product_serial_no', isdefault: true},
			{display: 'Complaint Status', name : 'complain_status', isdefault: true},
			{display: 'ASD Name', name : 'SD.service_distributor_name', isdefault: true},
			{display: 'Customer Phone', name : 'customer_phone_no', isdefault: true},
			{display: 'Complaint Number', name : 'complaint_no', isdefault: true},
			{display: 'Complaint Date', name : 'created_date', isdefault: true}
			],
		sortname: "c.complaint_id",
		sortorder: "desc",
		usepager: true,
		title: 'Complaints List',
		useRp: true,
		rp: 15,
		showTableToggleBtn: true,
		width: "900",
		height: "auto"
	});  
}



function eventHandlerCompletedComplaintsApprover(com,grid) {
	
	if (com=='Import')	{
		//location.href="import_item.php";	
		location.href="#";	
	}
	
		
	
	
	
	else if (com=='Export')	{
		location.href="export.inc.php?export_completed_complaintsApprover";		
	}
	else if (com=='Add')	{
		location.href="raisecomplaint_call_center.php";		
	}else{
		if( j('.trSelected',grid).length<=0 ||  j('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=j('.trSelected',grid);		
		if (com=='Edit')
		{
			location.href="change_status_of_approved_complaint.php?id="+items[0].id.substr(3);			
		}
		
		
		
		
		
		if (com=='Delete'){
			if(confirm("Are you sure, you want to delete selected data?")){
					
				j.post("call_center_complaint.php", {"delete":"yes","id":items[0].id.substr(3)},function(data)
				{		 
				  if(data!="") {					 
					 j('#flex1').flexOptions().flexReload(); 
				  }else {
					  alert("Record doesn't exists.")
				  }
				  
				});		
			}			
		}	
	}
}
	
/**********************************************  Completed Complaints of Approver ********************************************/


/*****new report, copy of order list sku wise maninder on June 13th 2017*******/

function showOrderListSKU(){
j("#flex1").flexigrid({
	url: 'admin_order_list_sku.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [
		{display: 'Salesperson Code', name : 'salesman_code', width : 100, sortable : true, align: 'left'},
		{display: 'Salesperson Name', name : 'salesman_name', width : 100, sortable : true, align: 'left'},
		{display: 'Reporting To', name : 'rpt_to', width : 100, sortable : true, align: 'left'},
		{display: 'Brand', name : 'rpt_to', width : 100, sortable : true, align: 'left'},
		{display: 'Dealer Name', name : 'retailer_name', width : 100, sortable : true, align: 'left'},
		{display: 'Dealer Code', name : 'retailer_code', width : 100, sortable : true, align: 'left'},
		{display: 'Dealer Address', name : 'retailer_address', width : 150, sortable : true, align: 'left'},				
		{display: 'City', name : 'retailer_city', width : 100, sortable : true, align: 'left'},
		{display: 'District', name : 'retailer_city', width : 100, sortable : true, align: 'left'},
		{display: 'Dealer Pincode', name : 'rpin_code', width : 100, sortable : true, align: 'left'},
		{display: 'State', name : 'retailer_state', width : 100, sortable : true, align: 'left'},
		{display: 'Distributor Code', name : 'distributor_code', width : 100, sortable : true, align: 'left'},
		{display: 'Distributor Name', name : 'distributor_name', width : 100, sortable : true, align: 'left'},		
		{display: 'City', name : 'd_city', width : 100, sortable : true, align: 'left'},
		{display: 'Order Status', name : 'order_status', width : 100, sortable : true, align: 'left'},
		{display: 'No Order Reason', name : 'noOrderReason', width : 200, sortable : true, align: 'left'},
		{display: 'Comments', name : 'orderComments', width : 150, sortable : true, align: 'left'},		
		{display: 'Order Number', name : 'order_id', width : 100, sortable : true, align: 'left'},
		{display: 'Date of Order', name : 'date_of_order', width : 100, sortable : true, align: 'left'},
		{display: 'Time of Order', name : 'time_of_order', width : 100, sortable : true, align: 'left'},
		{display: 'SKU Code', name : 'sku_code', width : 100, sortable : true, align: 'left'},
		{display: 'Quantity', name : 'quantity', width : 100, sortable : true, align: 'left'},
		{display: 'Updated Quantity', name : 'accepted_quantity', width : 100, sortable : true, align: 'left'}
		],
	buttons : [		
		{name: 'View Order Detail', bclass: 'orderdeails', onpress : eventHandlerOrderListSku},
		{separator: true},
		{name: 'Dealer Report (F2)', bclass: 'redit', onpress : eventHandlerOrderListSku},
		{separator: true},
		{name: 'Salesman Report (F4)', bclass: 'sedit', onpress : eventHandlerOrderListSku},
		{separator: true},
        //{name: 'Export to Tally', bclass: 'export', onpress : eventHandlerOrderListSku},
		//{separator: true},
		{name: 'Push To Tally', bclass: 'export_one', onpress : eventHandlerOrderListSku},
		{separator: true},
        {name: 'Pull From Tally', bclass: 'pull', onpress : eventHandlerOrderListSku},
		{separator: true}



		],
	searchitems : [
		{display: 'Order Number', name : 'o.order_id', isdefault: true},
		{display: 'Date of Order', name : 'o.date_of_order'},
		{display: 'Distributor Name', name : 'distributor_name'},
		{display: 'Salesperson Name', name : 'salesman_name'},
		{display: 'Dealer Name', name : 'retailer_name'},
		{display: 'Dealer Address', name : 'retailer_address'},
		{display: 'Order Taken', name : 'order_type'}
		],
	sortname: "date_of_order",
	sortorder: "desc",
	usepager: true,
	title: 'Order List',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 1000,
	height: 'auto'
});  
}
function eventHandlerOrderListSku(com,grid)
{
	
	if (com=='Add')	{
		location.href="#";	
	}

  //         if (com=='Push to Tally')	{
		// //location.href="http://localhost/voucher2.php";	
		// window.open("http://localhost/voucher2.php",'_blank');
		// }
         



	else{
		if( j('.trSelected',grid).length<=0 ||  j('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=j('.trSelected',grid);		
		if (com=='View Order Detail'){
			var searchParam_1 = j('input[name=q]').val();
			var searchParam_2 = j('select[name=qtype]').val();
			var searchParam_3 = j(".pcontrol").find("input").val();
			location.href="admin_order_list_sku.php?id="+items[0].id.substr(3)+"&searchParam_1="+searchParam_1+"&searchParam_2="+searchParam_2+"&searchParam_3="+searchParam_3;	
		}else if (com=='Dealer Report (F2)')	{
		window.open("retailer_his_report.php?ordid="+items[0].id.substr(3), '_blank');
		//location.href="retailer_items_report.php?id="+items[0].id.substr(3);
		}else if (com=='Salesman Report (F4)')	{
		location.href="admin_order_list_sku.php?salid="+items[0].id.substr(3);	
		//location.href="retailer_items_report.php?id="+items[0].id.substr(3);
		}	

                 else if (com=='Export to Tally')	{
			var searchParam_1 = j('input[name=q]').val();
			var searchParam_2 = j('select[name=qtype]').val();
			var searchParam_3 = j(".pcontrol").find("input").val();
			location.href="download_tally_list.php?id="+items[0].id.substr(3)+"&searchParam_1="+searchParam_1+"&searchParam_2="+searchParam_2+"&searchParam_3="+searchParam_3;	
		
		//location.href="retailer_items_report.php?id="+items[0].id.substr(3);
		}	




  //       else  if (com=='Push to Tally')	{
        	
		// 	var link = "download_tally_list.php?id="+items[0].id.substr(3);
		// 	window.open(link,'_blank');
		// }
         
        else  if (com=='Push To Tally')	{

        	j( "#dialog" ).html('<center><img src="images/ajax-loader.gif" /></center>');

        	j( function() { j( "#dialog" ).dialog(); } );

        	var link = "download_tally_list.php?id="+items[0].id.substr(3);
        	j.ajax({
			  method: "POST",
			  url: link,
			  data: {}
			})
			  .done(function( data ) {

			  var msg = JSON.parse(data);
			  //alert( "Data Saved: " + msg.status );

			   	if(msg.status == 'Success') {
			   		 j(".trSelected td:nth-child(2)").find('div').text(msg.id);
			   		j(".trSelected td:nth-child(3)").find('div').text(msg.TALLYORDERNO);
			   	}
			   	 

			   		
			      j( "#dialog" ).html(msg.data);
			  });
			        	
			
			//window.open(link,'_blank');
		}
         




         
        else  if (com=='Pull From Tally')	{
        	
			// var link = "export_tally.php?id="+items[0].id.substr(3);
			// window.open(link,'_blank');

        	j( "#dialog2" ).html('<center><img src="images/ajax-loader.gif" /></center>');

        	j( function() { 
        		j( "#dialog2" ).dialog({
        			maxWidth:800,
        			maxHeight: 700,
       				width: 800,
        			height: 700,
        			modal: true,
        			buttons: { 
			            "Print": function() { 
			                j("#dialog2").printArea(); 
			            },
			            "Close": function() { 
			               j(this).dialog("close");
			            }
			         }
        		}); 
        	} );

        	var link = "invoice_template.php?id="+items[0].id.substr(3);
        	j.ajax({
			  method: "POST",
			  url: link,
			  data: {}
			})
			  .done(function( data ) {

			  //var msg = JSON.parse(data);
			 // alert( "Data Saved: " + data );

			   	// if(msg.status == 'Success')
			   	//   j(".trSelected td:nth-child(2)").find('div').text(items[0].id.substr(3));
			     j( "#dialog2" ).html(data);


			  });
			        	
			
			//window.open(link,'_blank');



		}




	}
}


/********************************** Start Today Target List 23 June 2017 Chirag *************************/

function showTodayTarget(){
j("#flex1").flexigrid({
	url: 'salesman_today_target.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Salesman Name', name : 'salesman_name', width : 180, sortable : true, align: 'left'},
		{display: 'Salesman Code', name : 'salesman_code', width : 100, sortable : true, align: 'left'},
		{display: 'State', name : 'state', width : 100, sortable : true, align: 'left'},
		{display: 'city', name : 'city', width : 100, sortable : true, align: 'left'},
		{display: 'Today Target Quantity', name : 'target_quantity', width : 130, sortable : true, align: 'left'},
        {display: 'Month Target Quantity', name : '', width : 130, sortable : true, align: 'left'},
        {display: 'Achieved Target (IN Month)', name : '', width : 150, sortable : true, align: 'left'},
        {display: 'Remaining Target (IN Month)', name : '', width : 150, sortable : true, align: 'left'},
		{display: 'Target Date', name : 'target_date', width : 100, sortable : true, align: 'left'},
		{display: 'Status', name : 'status', width : 100, sortable : true, align: 'left'}
		],
	buttons : [
		{name: 'Add', bclass: 'add', onpress : eventHandlerTodayTarget},
		{separator: true},		
		{name: 'Edit', bclass: 'edit', onpress : eventHandlerTodayTarget},
		{separator: true}
		],
	searchitems : [
		{display: 'Salesman Name', name : 'salesman_name', isdefault: true}
		],
	sortname: "salesman_name",
	sortorder: "asc",
	usepager: true,
	title: 'Salesman Todays Target',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 700,
	height: 'auto'
});  
}
function eventHandlerTodayTarget(com,grid)
{
	if (com=='Add')	{
		location.href="addSegment.php";		
	}else{
		if( j('.trSelected',grid).length<=0 ||  j('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=j('.trSelected',grid);		
		if (com=='Edit'){
			location.href="addSegment.php?id="+items[0].id.substr(3);			
		}
		if (com=='Delete'){
			if(confirm("Are you sure, you want to delete selected Segment?")){
					
				j.post("segment.php", {"delete":"yes","id":items[0].id.substr(3)},function(data)
				{		 
				  if(data!="") {					 
					 j('#flex1').flexOptions().flexReload(); 
				  }else {
					  alert("Record doesn't exists.")
				  }
				  
				});		
			}			
		}	
	}
}
/********************************** Start Today Target List 23 June 2017 Chirag *************************/

/*************************** Start of All Complaints of ASD to warehouse 26 June 2017 Chirag *******************/
function allWarehouseASDComplaints(){
j("#flex1").flexigrid({
	url: 'all_warehouse_asd_complaints.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Complaint Number', name : 'complaint_no', width : 100, sortable : true, align: 'left'},
		{display: 'Complaint Date', name : 'created_date', width : 100, sortable : true, align: 'left'},
		{display: 'Service Distributor', name : 'service_distributor_name', width : 120, sortable : true, align: 'left'},
		{display: 'BSN', name : 'bsn', width : 80, sortable : true, align: 'left'},
		{display: 'Auth Token', name : 'auth_token', width : 80, sortable : true, align: 'left'},
		{display: 'Action Taken', name : 'action_taken', width : 80, sortable : true, align: 'left'},
		{display: 'BCF Status', name : 'BCFST', width : 150, sortable : true, align: 'left'},
		{display: 'Customer Name', name : 'customer_name', width : 80, sortable : true, align: 'left'},
		{display: 'Customer Address', name : 'customer_address', width : 100, sortable : true, align: 'left'},
		{display: 'Mobile Number', name : 'customer_phone_no', width : 120, sortable : true, align: 'left'},
		{display: 'Country', name : 'country_name', width : 150, sortable : true, align: 'left'},
		{display: 'State', name : 'state_name', width : 100, sortable : true, align: 'left'},
		{display: 'City', name : 'city_name', width : 80, sortable : true, align: 'left'},
		{display: 'View BCF', name : 'view_bcf', width : 80, sortable : true, align: 'left'},
		// {display: 'Status', name : 'status', width : 120, sortable : true, align: 'left'},
		],
	buttons : [		
		// {name: 'Edit', bclass: 'editqq', onpress : eventHandlercomplaintallASD},
		// {separator: true}
		],
	searchitems : [
		{display: 'Complaint No.', name : 'complaint_no', isdefault: true},
		{display: 'Service Distributor', name : 'service_distributor_name', isdefault: true},
		{display: 'Complaint Date', name : 'COMPLAINT.created_date', isdefault: true},
		{display: 'BSN', name : 'bsn', isdefault: true},
		{display: 'Customer Name', name : 'customer_name', isdefault: true},
		{display: 'Customer Mobile', name : 'customer_phone_no', isdefault: true},
		{display: 'Country', name : 'country_name', isdefault: true},
		{display: 'State', name : 'state_name', isdefault: true},
		{display: 'City', name : 'city_name', isdefault: true},
		{display: 'BCF Status', name : 'under_process', isdefault: true}
		],
	sortname: "COMPLAINT.complaint_no",
	sortorder: "desc",
	usepager: true,
	title: 'Complaints of ASD',
	useRp: true,
	rp: 15,
	showTableToggleBtn: true,
	width: 1000,
	height: 'auto'
});  
}



function eventHandlercomplaintallASD(com,grid) {
	
	if (com=='Import')	{
		//location.href="import_item.php";	
		location.href="#";	
	}
	
		
	
	
	
	else if (com=='Export')	{
		location.href="export.inc.php?export_call_center_complaints";		
	}
	else if (com=='Add')	{
		location.href="raisecomplaint_call_center.php";		
	}else{
		if( j('.trSelected',grid).length<=0 ||  j('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=j('.trSelected',grid);		
		if (com=='Edit')
		{
			//alert('HEllo');
			//alert("addhierarchy.php?id="+items[0].id.substr(3));
			location.href="raisecomplaint_call_center.php?id="+items[0].id.substr(3);	
			
		}
		
		
		
		
		
		if (com=='Delete'){
			if(confirm("Are you sure, you want to delete selected data?")){
					
				j.post("call_center_complaint.php", {"delete":"yes","id":items[0].id.substr(3)},function(data)
				{		 
				  if(data!="") {					 
					 j('#flex1').flexOptions().flexReload(); 
				  }else {
					  alert("Record doesn't exists.")
				  }
				  
				});		
			}			
		}	
	}
}
/*************************** End of All Complaints of ASD to warehouse 26 June 2017 Chirag *******************/


/********************* Pending Complaints of ASD Escalated to RSI @Chirag July 14, 2017 *************/

function showPendingAsdComplaints(){
j("#flex1").flexigrid({
	url: 'complaints_of_asd.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Complaint Number', name : 'complaint_no', width : 100, sortable : true, align: 'left'},
		{display: 'Operator Name', name : 'operator_name', width : 100, sortable : true, align: 'left'},
		{display: 'Complaint Date', name : 'created_date', width : 100, sortable : true, align: 'left'},
		{display: 'Battery Serial No.', name : 'bsn', width : 80, sortable : true, align: 'left'},
		// {display: 'Auth Token', name : 'auth_token', width : 80, sortable : true, align: 'left'},
		// {display: 'Action Taken', name : 'action_taken', width : 80, sortable : true, align: 'left'},
		{display: 'Complaint Status', name : 'BCFST', width : 150, sortable : true, align: 'left'},
		// {display: 'Call Close Datetime', name : 'call_close_datetime', width : 150, sortable : true, align: 'left'},
		{display: 'Customer Name', name : 'customer_name', width : 80, sortable : true, align: 'left'},
		{display: 'Customer Address', name : 'customer_address', width : 100, sortable : true, align: 'left'},
		{display: 'Mobile Number', name : 'customer_phone_no', width : 120, sortable : true, align: 'left'},
		{display: 'Country', name : 'country_name', width : 150, sortable : true, align: 'left'},
		{display: 'State', name : 'state_name', width : 100, sortable : true, align: 'left'},
		{display: 'City', name : 'city_name', width : 80, sortable : true, align: 'left'}
		// {display: 'View BCF', name : 'view_bcf', width : 80, sortable : true, align: 'left'},
		// {display: 'Complaint Action', name : 'status', width : 120, sortable : true, align: 'left'},
		],
	buttons : [		
		{name: 'Edit', bclass: 'editqq', onpress : eventHandlerPendingAsdComplaints},
		{separator: true},
		{name: 'Export', bclass: 'export', onpress : eventHandlerPendingAsdComplaints},
		{separator: true}
		],
	searchitems : [
		{display: 'Complaint No.', name : 'complaint_no', isdefault: true},
		{display: 'Complaint Date', name : 'COMPLAINT.created_date', isdefault: true},
		{display: 'Battery Serial No.', name : 'bsn', isdefault: true},
		{display: 'Customer Name', name : 'customer_name', isdefault: true},
		{display: 'Customer Mobile', name : 'customer_phone_no', isdefault: true},
		{display: 'Country', name : 'country_name', isdefault: true},
		{display: 'State', name : 'state_name', isdefault: true},
		{display: 'City', name : 'city_name', isdefault: true},
		{display: 'Complaint Status', name : 'under_process', isdefault: true}
		],
	sortname: "COMPLAINT.complaint_no",
	sortorder: "desc",
	usepager: true,
	title: 'Pending Complaints of ASD Escalated to RSI',
	useRp: true,
	rp: 15,
	showTableToggleBtn: true,
	width: 1000,
	height: 'auto'
});  
}

function eventHandlerPendingAsdComplaints(com,grid)
{
	if (com=='Add')	{
		location.href="claim_approval_call_center.php";		
	}
	else if (com=='Export')	{
			location.href="export.inc.php?export_final_process_complaints";		
		}
	else{
		if( j('.trSelected',grid).length<=0 ||  j('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		
		var items=j('.trSelected',grid);		
		if (com=='Edit'){
			location.href="claim_approval_call_center.php?id="+items[0].id.substr(3);			
		}
		if (com=='Delete'){
			if(confirm("Are you sure, you want to delete selected Jobcard?")){
					
				j.post("claim_approval_call_center.php", {"delete":"yes","id":items[0].id.substr(3)},function(data)
				{		 
				  if(data!="") {					 
					 j('#flex1').flexOptions().flexReload(); 
				  }else {
					  alert("Record doesn't exists.")
				  }
				  
				});		
			}			
		}	
	}
}

/********************* Pending Complaints of ASD Escalated to RSI @Chirag July 14, 2017 *************/
/********************************** ASD call type 27 Jul 2017 Chirag *************************/

function asdCallType(){
j("#flex1").flexigrid({
	url: 'asd_call_type.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Call Type', name : 'segment_name', width : 300, sortable : true, align: 'left'},
		{display: 'Status', name : 'status', width : 100, sortable : true, align: 'left'}
		],
	buttons : [
		{name: 'Add', bclass: 'add', onpress : eventHandlerCallType},
		{separator: true},		
		{name: 'Edit', bclass: 'edit', onpress : eventHandlerCallType},
		{separator: true}
		],
	searchitems : [
		{display: 'Call Type', name : 'call_type', isdefault: true}
		],
	sortname: "call_type",
	sortorder: "asc",
	usepager: true,
	title: 'Call Types',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 700,
	height: 'auto'
});  
}
function eventHandlerCallType(com,grid)
{
	if (com=='Add')	{
		location.href="addAsdCallType.php";		
	}else{
		if( j('.trSelected',grid).length<=0 ||  j('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=j('.trSelected',grid);		
		if (com=='Edit'){
			location.href="addAsdCallType.php?id="+items[0].id.substr(3);			
		}
		if (com=='Delete'){
			if(confirm("Are you sure, you want to delete selected Call Type?")){
					
				j.post("asd_call_type.php", {"delete":"yes","id":items[0].id.substr(3)},function(data)
				{		 
				  if(data!="") {					 
					 j('#flex1').flexOptions().flexReload(); 
				  }else {
					  alert("Record doesn't exists.")
				  }
				  
				});		
			}			
		}	
	}
}

/********************************** ASD call type 27 Jul 2017 Chirag *************************/

/******************************** ASD Payout Pricing Master Chirag Gupta @ July 27, 2017 ****************************/


function showAsdPayoutPricing(){
j("#flex1").flexigrid({
	url: 'asd_payout_pricing_master.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Call Type', name : 'call_type', width : 280, sortable : true, align: 'left'},
		{display: 'Segment', name : 'segment_name',width : 150,sortable : true, align: 'left'},
		{display: 'Applicable From Date', name : 'applicable_from_date',width : 150,sortable : true, align: 'left'},
		{display: 'Applicable To Date', name : 'applicable_to_date',width : 150,sortable : true, align: 'left'},
		/*{display: 'State Code', name : 'state_code',width : 200,sortable : true, align: 'left'},*/
		{display: 'Status', name : 'status',width : 100,sortable : true}
		
		],
	buttons : [
		{name: 'Add', bclass: 'add', onpress : eventHandlerAsdPayoutPricingMaster},
		{separator: true},		
		{name: 'Edit', bclass: 'edit', onpress : eventHandlerAsdPayoutPricingMaster},
		{separator: true}
		// {name: 'Delete', bclass: 'delete', onpress : eventHandlerAsdPayoutPricingMaster},
		// {separator: true},			
		// {name: 'Import', bclass: 'import', onpress : eventHandlerAsdPayoutPricingMaster},
		// {separator: true},
		// {name: 'Export', bclass: 'export', onpress : eventHandlerAsdPayoutPricingMaster},
		// {separator: true}
		],
	searchitems : [
		{display: 'State', name : 'state_name', isdefault: true},
		],
	sortname: "payout_pricing_id",
	sortorder: "asc",
	usepager: true,
	title: 'ASD Payout Pricing Master',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 850,
	height: 'auto'
});  
}


function eventHandlerAsdPayoutPricingMaster(com,grid)
{

	if (com=='Import')	{
		location.href="state_import.php";		
	}
	else if (com=='Export')	{
		location.href="export.inc.php?export_state_new";		
	}
	else if (com=='Add')	{
		location.href="addAsdPayoutPricingRecord.php";		
	}else{
		if( j('.trSelected',grid).length<=0 ||  j('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=j('.trSelected',grid);	
		
		
		if (com=='Edit'){
			location.href="addAsdPayoutPricingRecord.php?id="+items[0].id.substr(3);			
		}		
	}
}

/******************************** ASD Payout Pricing Master Chirag Gupta @ July 27, 2017 ****************************/

/**************************** Warehouse Available Stock for Commercial Panel 05 August 2017 @Chirag ********************/


function showWarehouseStockSel(){
j("#flex1").flexigrid({
	url: 'warehouse_available_stock.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Warehouse Code', name : 'warehouse_code', width : 80, sortable : true, align: 'left'},
		{display: 'Warehouse Name', name : 'warehouse_name', width : 200, sortable : true, align: 'left'},
		{display: 'Category Name', name : 'category_name', width : 150, sortable : true, align: 'left'},
		{display: 'Division Name', name : 'division_name', width : 150, sortable : true, align: 'left'},
		{display: 'Segment Name', name : 'segment_name', width : 150, sortable : true, align: 'left'},
		{display: 'Brand Name', name : 'brand_name', width : 150, sortable : true, align: 'left'},
		//{display: 'Cases', name : 'case_size', width : 100, sortable : true, align: 'left'},
		{display: 'Item Code', name : 'item_code', width : 100, sortable : true, align: 'left'},
		{display: 'Item Name', name : 'item_name', width : 200, sortable : true, align: 'left'},
		{display: 'BSN', name : 'bsn', width : 100, sortable : true, align: 'left'},
		{display: 'Last Update Date', name : 'last_updated_date', width : 100, sortable : true, align: 'left'},
		{display: 'Status', name : 'status', width : 100, sortable : true, align: 'left'}
		],
	buttons : [
		/*{name: 'Add', bclass: 'add', onpress : eventHandlerMasterCategory},
		{separator: true},		
		{name: 'Edit', bclass: 'edit', onpress : eventHandlerMasterCategory},
		{separator: true},
		{name: 'Set Margin', bclass: 'margin', onpress : eventHandlerMasterCategory},
		{separator: true}*/
		],
	searchitems : [
		{display: 'Category Name', name : 'category_name', isdefault: true},
		{display: 'Warehouse Code', name : 'warehouse_code'},
		{display: 'Warehouse Name', name : 'warehouse_name'},
		
		{display: 'Item Code', name : 'item_code'},
		{display: 'Item Name', name : 'item_name'}
		
		],
	sortname: "warehouse_stock_id",
	sortorder: "asc",
	usepager: true,
	title: 'Warehouse Stock',
	useRp: true,
	rp: 20,
	showTableToggleBtn: true,
	width: 730,
	height: 'auto'
});  
}


/**************************** Warehouse Available Stock for Commercial Panel 05 August 2017 @Chirag ********************/

/********************************************************************************
*
* DESC: Show Warehouse Stock Marked Damaged by Distributor
*  
* Created By: Chirag@2017-08-22
*
*********************************************************************************/


function showWarehouseDamagedStck(){
j("#flex1").flexigrid({
	url: 'warehouse_stock_not_accepted.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Distributor Name', name : 'distributor_name', width : 200, sortable : true, align: 'left'},
		{display: 'Distributor Code', name : 'distributor_code', width : 100, sortable : true, align: 'left'},
		{display: 'Distributor Address', name : 'distributor_address', width : 200, sortable : true, align: 'left'},
		{display: 'Distributor Phone No', name : 'distributor_phone_no', width : 100, sortable : true, align: 'left'},
		{display: 'Warehouse Name', name : 'warehouse_name', width : 200, sortable : true, align: 'left'},
		{display: 'Warehouse Code', name : 'warehouse_code', width : 200, sortable : true, align: 'left'},
		{display: 'BSN', name : 'bsn', width : 150, sortable : true, align: 'left'},
		{display: 'Item Name', name : 'item_name', width : 200, sortable : true, align: 'left'},
		{display: 'Item Code', name : 'item_code', width : 100, sortable : true, align: 'left'},
		{display: 'Status', name : 'status', width : 100, sortable : true, align: 'left'}
		],
	buttons : [
		// {name: 'Accept/Reject BSN', bclass: 'Scan', onpress : eventHandlerWarehousePendingStock},
		// {separator: true},		
		// {name: 'Edit', bclass: 'edit', onpress : eventHandlerMasterCategory},
		// {separator: true},
		],
	searchitems : [
		{display: 'Distributor Name', name : 'distributor_name', isdefault: true},
		{display: 'Warehouse Name', name : 'warehouse_name', isdefault: true},
		{display: 'Date of Sale', name : 'date_of_order'},
		{display: 'BSN', name : 'bsn'},
		{display: 'Status', name : 'ws.status'}
		
		],
	sortname: "date_of_order",
	sortorder: "desc",
	usepager: true,
	title: 'Warehouse Damaged Stock by Distributor',
	useRp: true,
	rp: 40,
	showTableToggleBtn: true,
	width: '740',
	height: 'auto'
});  
}



function eventHandlerWarehousePendingStock(com,grid)
{
	//alert(com);
	if (com=='Accept/Reject BSN')	{
		location.href="warehouse_scan_pending_btry.php";		
	}else{
		if( j('.trSelected',grid).length<=0 ||  j('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
			
	}
}

/********************************************************************************
*
* DESC: Show Warehouse Stock Marked Damaged by Distributor
*  
* Created By: Chirag@2017-08-22
*
*********************************************************************************/






/********************************** Delivery Challan List 13 September 2017 Chirag *************************/

function showDeliveryChallan(){
j("#flex1").flexigrid({
	url: 'delivery_challan.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Delivery Challan No', name : 'delivery_challan_number', width : 150, sortable : true, align: 'left'},
		{display: 'Delivery Challan Date', name : 'delivery_challan_date', width : 150, sortable : true, align: 'left'},
		{display: 'Tally Master ID', name : 'tally_master_id', width : 100, sortable : true, align: 'left'},
		{display: 'Warehouse Name', name : 'warehouse_name', width : 200, sortable : true, align: 'left'},
		{display: 'Transporter', name : 'delivery_challan_transporter', width : 100, sortable : true, align: 'left'},
		{display: 'Transporter Mode', name : 'delivery_challan_transport_mode', width : 150, sortable : true, align: 'left'},
		{display: 'Permit Number', name : 'delivery_challan_permit_number', width : 100, sortable : true, align: 'left'},
		{display: 'Vehicle Number', name : 'delivery_challan_vehicle_number', width : 100, sortable : true, align: 'left'},
		{display: 'G.R. No', name : 'delivery_challan_gr_number', width : 100, sortable : true, align: 'left'},
		{display: 'Contact No.', name : 'delivery_challan_contact', width : 100, sortable : true, align: 'left'},
		{display: 'Voucher Type.', name : 'voucher_type', width : 100, sortable : true, align: 'left'},
		{display: 'View', name : 'status', width : 100, sortable : true, align: 'left'}
		],
	buttons : [
		
		],
	searchitems : [
		{display: 'Delivery Challan Number', name : 'delivery_challan_number', isdefault: true},
		{display: 'Delivery Challan Date', name : 'delivery_challan_date', isdefault: true}
		],
	sortname: "delivery_challan_id",
	sortorder: "asc",
	usepager: true,
	title: 'Delivery Challan',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 700,
	height: 'auto'
});  
}
function eventHandlerDeliveryChallan(com,grid)
{
	if (com=='Add')	{
		location.href="addSegment.php";		
	}else{
		if( j('.trSelected',grid).length<=0 ||  j('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=j('.trSelected',grid);		
		if (com=='Edit'){
			location.href="addSegment.php?id="+items[0].id.substr(3);			
		}
		if (com=='Delete'){
			if(confirm("Are you sure, you want to delete selected Segment?")){
					
				j.post("segment.php", {"delete":"yes","id":items[0].id.substr(3)},function(data)
				{		 
				  if(data!="") {					 
					 j('#flex1').flexOptions().flexReload(); 
				  }else {
					  alert("Record doesn't exists.")
				  }
				  
				});		
			}			
		}	
	}
}


/*********************************************************************************
* DESC: Show warehouse stock transferred from Charging Site
* Author: Chirag
* Created: 2017-10-04
*
*********************************************************************************/


function showChargingSiteInvoices(){
j("#flex1").flexigrid({
	url: 'wh_stock_transfer_from_chg_site.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'From', name : 'from_warehouse_name', width : 180, sortable : true, align: 'left'},
		{display: 'To Warehouse', name : 'to_warehouse_name', width : 180, sortable : true, align: 'left'},
		{display: 'Transfer BSN', name : 'ttlbsntransfer', width : 100, sortable : true, align: 'left'},
		{display: 'Dispatched No.', name : 'bill_no', width : 100, sortable : true, align: 'left'},
		{display: 'Dispatched Date', name : 'bill_date', width : 100, sortable : true, align: 'left'},
		{display: 'Transfer Time', name : 'transfer_datetime', width : 170, sortable : true, align: 'left'},
		],
	buttons : [
		
		{name: 'View', bclass: 'View', onpress : eventHandlerWarehouseStockTransferChg},
		{separator: true},
		],
	searchitems : [
		{display: 'From Warehouse', name : 'W.warehouse_name', isdefault: true},
		{display: 'To Warehouse', name : 'W2.warehouse_name'}
		
		],
	sortname: "created_date",
	sortorder: "asc",
	usepager: true,
	title: 'Stock Tranfser List',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 900,
	height: 'auto'
});  
}
function eventHandlerWarehouseStockTransferChg(com,grid)
{
	if (com=='Transfer Stock')	{
		location.href="wh_stock_transfer.php";		
	}else if (com=='Transfer Bulk Stock')	{
		location.href="wh_stock_transfer_bulk.php";		
	}
	else{
		if( j('.trSelected',grid).length<=0 ||  j('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=j('.trSelected',grid);		
		if (com=='View'){
			location.href="wh_stock_transfer_chg_site_detail_view.php?id="+items[0].id.substr(3);			
		}
		if (com=='Delete'){
			if(confirm("Are you sure, you want to delete selected Category?")){
					
				j.post("category.php", {"delete":"yes","id":items[0].id.substr(3)},function(data)
				{		 
				  if(data!="") {					 
					 j('#flex1').flexOptions().flexReload(); 
				  }else {
					  alert("Record doesn't exists.")
				  }
				  
				});		
			}			
		}	
	}
}





/*********************************************************************************
* DESC: Show warehouse stock transferred from Charging Site
* Author: Chirag
* Created: 2017-10-04
*
*********************************************************************************/


/*********************************************************************************
* DESC: Show Distributors stock transfer list
* Author: Chirag
* Created: 2017-11-03
*
*********************************************************************************/


function showDistributorStockTransferList(){
j("#flex1").flexigrid({
	url: 'distributor_stock_transfer_list.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Warehouse Code', name : 'from_warehouse_name', width : 180, sortable : true, align: 'left'},
		{display: 'Warehouse Name', name : 'to_warehouse_name', width : 180, sortable : true, align: 'left'},
		{display: 'Total BSN', name : '', width : 100, sortable : true, align: 'left'},
		{display: 'Distributor Name', name : 'distributor_name', width : 100, sortable : true, align: 'left'},
		{display: 'Distributor Code', name : 'distributor_code', width : 100, sortable : true, align: 'left'},
		{display: 'Invoice Date', name : 'bill_date', width : 170, sortable : true, align: 'left'},
        {display: 'Invoice Number', name : 'bill_no', width : 170, sortable : true, align: 'left'},
        {display: 'Delivery Challan Date', name : 'delivery_challan_date', width : 170, sortable : true, align: 'left'},
        {display: 'LR Number', name : 'bill_no', width : 170, sortable : true, align: 'left'},
        {display: 'Details', name : '', width : 170, sortable : true, align: 'left'},
		],
	buttons : [
		// {name: 'Transfer Stock', bclass: 'Transfer Stock', onpress : eventHandlerDistributorStockTransfer},
		// {separator: true},
		// {name: 'Transfer Bulk Stock', bclass: 'Transfer Bulk Stock', onpress : eventHandlerDistributorStockTransfer},
		// {separator: true},
		// {name: 'View', bclass: 'View', onpress : eventHandlerDistributorStockTransfer},
		// {separator: true},
		],
	searchitems : [
		{display: 'From Warehouse', name : 'W.warehouse_name', isdefault: true},
		{display: 'To Warehouse', name : 'W2.warehouse_name'}
		
		],
	sortname: "last_update_datetime",
	sortorder: "asc",
	usepager: true,
	title: 'Transfer Stock to Distributor List',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 900,
	height: 'auto'
});  
}
function eventHandlerDistributorStockTransfer(com,grid)
{
	if (com=='Transfer Stock')	{
		location.href="wh_stock_transfer.php";		
	}else if (com=='Transfer Bulk Stock')	{
		location.href="wh_stock_transfer_bulk.php";		
	}
	else{
		if( j('.trSelected',grid).length<=0 ||  j('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=j('.trSelected',grid);		
		if (com=='View'){
			location.href="distributor_stock_transfer_detail_view.php?id="+items[0].id.substr(3);			
		}
		if (com=='Delete'){
			if(confirm("Are you sure, you want to delete selected Category?")){
					
				j.post("category.php", {"delete":"yes","id":items[0].id.substr(3)},function(data)
				{		 
				  if(data!="") {					 
					 j('#flex1').flexOptions().flexReload(); 
				  }else {
					  alert("Record doesn't exists.")
				  }
				  
				});		
			}			
		}	
	}
}





/*********************************************************************************
* DESC: Show warehouse stock transfer list
* Author: AJAY
* Created: 2017-04-26
*
*********************************************************************************/


/***************** Cleaner Login Status By Chirag Gupta Nov 27, 2017 *********************/

function showLoginCleaner(){
j("#flex1").flexigrid({
	url: 'cleaner_login_list.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Cleaner Name', name : 'cleaner_name', width : 120, sortable : true, align: 'left'},
		{display: 'Username', name : 'username', width : 120, sortable : true, align: 'left'},
		{display: 'Email ID', name : 'email_id', width : 150, sortable : true, align: 'left'},
		{display: 'Phone Number', name : 'cleaner_phone_no', width : 120, sortable : true, align: 'left'},
		{display: 'Login Status', name : '', width : 120, sortable : true, align: 'left'},
		{display: '', name : '', width : 120, sortable : true, align: 'left'}
			
		],
	buttons : [
		],
	searchitems : [
		{display: 'Cleaner Name', name : 'cleaner_name', isdefault: true},
		{display: 'Username', name : 'username'},
		{display: 'Email ID', name : 'email_id'},
		{display: 'Phone Number', name : 'cleaner_phone_no'}
		],
	sortname: "cleaner_name",
	sortorder: "asc",
	usepager: true,
	title: 'Cleaner List',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 850,
	height: 'auto'
});  
}


/******************************* Service Distributor SP Login Status By Chirag Gupta May 16, 2017 *************************/



function showUnitType(){
j("#flex1").flexigrid({
	url: 'unitType.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Unit Type Name', name : 'unit_name', width : 300, sortable : true, align: 'left'},
		{display: 'Status', name : 'status', width : 300, sortable : true, align: 'left'}
		],
	buttons : [
		{name: 'Add', bclass: 'add', onpress : eventHandlerUnitType},
		{separator: true},		
		{name: 'Edit', bclass: 'edit', onpress : eventHandlerUnitType},
		{separator: true}		
		],
	searchitems : [
		{display: 'Unit Type Name', name : 'unit_name', isdefault: true}
		
		],
	sortname: "unit_name",
	sortorder: "asc",
	usepager: true,
	title: 'Unit Type',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	//width: 700,
	height: 'auto'
});  
}

function eventHandlerUnitType(com,grid)
{
	if (com=='Add')	{
		location.href="addUnitType.php";		
	}else{
		if( j('.trSelected',grid).length<=0 ||  j('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=j('.trSelected',grid);		
		if (com=='Edit'){
			location.href="addUnitType.php?id="+items[0].id.substr(3);			
		}
		if (com=='Delete'){
			if(confirm("Are you sure, you want to delete selected Branch?")){
					
				j.post("unitType.php", {"delete":"yes","id":items[0].id.substr(3)},function(data)
				{		 
				  if(data!="") {					 
					 j('#flex1').flexOptions().flexReload(); 
				  }else {
					  alert("Record doesn't exists.")
				  }
				  
				});		
			}			
		}	
	}
}

function showBuildings(){
j("#flex1").flexigrid({
	url: 'buildings.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Building Name', name : 'building_name', width : 300, sortable : true, align: 'left'},
		{display: 'Unit Type', name : 'building_name', width : 300, sortable : true, align: 'left'},
		{display: 'Area', name : 'area_name', width : 300, sortable : true, align: 'left'},
		{display: 'Latitude', name : 'building_name', width : 300, sortable : true, align: 'left'},
		{display: 'Longitude', name : 'building_name', width : 300, sortable : true, align: 'left'},
		{display: 'Status', name : 'status', width : 300, sortable : true, align: 'left'}
		],
	buttons : [
		{name: 'Add', bclass: 'add', onpress : eventHandlerBuildings},
		{separator: true},		
		{name: 'Edit', bclass: 'edit', onpress : eventHandlerBuildings},
		{separator: true}		
		],
	searchitems : [
		{display: 'Unit Type Name', name : 'unit_name', isdefault: true}
		
		],
	sortname: "unit_name",
	sortorder: "asc",
	usepager: true,
	title: 'Building Master',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	//width: 700,
	height: 'auto'
});  
}

function eventHandlerBuildings(com,grid)
{
	if (com=='Add')	{
		location.href="addBuildings.php";		
	}else{
		if( j('.trSelected',grid).length<=0 ||  j('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=j('.trSelected',grid);		
		if (com=='Edit'){
			location.href="addBuildings.php?id="+items[0].id.substr(3);			
		}
		if (com=='Delete'){
			if(confirm("Are you sure, you want to delete selected Building?")){
					
				j.post("addBuildings.php", {"delete":"yes","id":items[0].id.substr(3)},function(data)
				{		 
				  if(data!="") {					 
					 j('#flex1').flexOptions().flexReload(); 
				  }else {
					  alert("Record doesn't exists.")
				  }
				  
				});		
			}			
		}	
	}
}


function showCleaningType(){
j("#flex1").flexigrid({
	url: 'cleaningType.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Cleaning Type', name : 'cleaning_type', width : 300, sortable : true, align: 'left'},
        {display: 'Unit Type', name : 'unit_name', width : 300, sortable : true, align: 'left'},
		{display: 'Status', name : 'status', width : 300, sortable : true, align: 'left'}
		],
	buttons : [
		{name: 'Add', bclass: 'add', onpress : eventHandlerCleaningTypes},
		{separator: true},		
		{name: 'Edit', bclass: 'edit', onpress : eventHandlerCleaningTypes},
		{separator: true}		
		],
	searchitems : [
		{display: 'Cleaning Type', name : 'cleaning_type', isdefault: true},
		{display: 'Unit Type', name : 'unit_name'}
		],
	sortname: "cleaning_type",
	sortorder: "asc",
	usepager: true,
	title: 'Cleaning Type',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	//width: 700,
	height: 'auto'
});  
}
function eventHandlerCleaningTypes(com,grid)
{
	if (com=='Add')	{
		location.href="addCleaningType.php";		
	}else{
		if( j('.trSelected',grid).length<=0 ||  j('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=j('.trSelected',grid);		
		if (com=='Edit'){
			location.href="addCleaningType.php?id="+items[0].id.substr(3);			
		}
		if (com=='Delete'){
			if(confirm("Are you sure, you want to delete selected Building?")){
					
				j.post("addCleaningType.php", {"delete":"yes","id":items[0].id.substr(3)},function(data)
				{		 
				  if(data!="") {					 
					 j('#flex1').flexOptions().flexReload(); 
				  }else {
					  alert("Record doesn't exists.")
				  }
				  
				});		
			}			
		}	
	}
}

/**************************** Customers List @Chirag Gupta November 29, 2017 *****************************/

/**************************** Customers List @Chirag Gupta November 29, 2017 *****************************/

function showCustomersList(){
j("#flex1").flexigrid({
	url: 'customers.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Customer Name', name : 'customer_name', width : 300, sortable : true, align: 'left'},
        {display: 'Customer Email', name : 'customer_email', width : 200, sortable : true},
        {display: 'Customer Number', name : 'customer_number', width : 200, sortable : true},
        //{display: 'Gender', name : 'gender', width : 100, sortable : true},
        {display: 'Building Name', name : 'building_name', width : 200, sortable : true},
        {display: 'Address', name : 'customer_address', width : 200, sortable : true},
        {display: 'Status', name : 'status', width : 300, sortable : true, align: 'left'}
		],
	buttons : [
		// {name: 'Add', bclass: 'add', onpress : eventHandlerCleaningTypes},
		// {separator: true},		
		// {name: 'Edit', bclass: 'edit', onpress : eventHandlerCleaningTypes},
		// {separator: true}		
		],
	searchitems : [
		{display: 'Customer Name', name : 'customer_name', isdefault: true},
		//{display: 'Customer Code', name : 'customer_code'},
		//{display: 'Gender', name : 'gender'},
		{display: 'Customer Number', name : 'customer_number'},
		{display: 'Customer Email', name : 'customer_email'}
		],
	sortname: "customer_name",
	sortorder: "asc",
	usepager: true,
	title: 'Customer List',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	//width: 700,
	height: 'auto'
});  
}

/**************************** Customers List @Chirag Gupta November 29, 2017 *****************************/

function showCleaner(){
j("#flex1").flexigrid({
	url: 'cleaner.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Cleaner Name', name : 'cleaner_name', width : 250, sortable : true, align: 'left'},
		{display: 'Cleaner Code', name : 'cleaner_code', width : 100, sortable : true, align: 'left'},
                {display: 'Supervisor Name', name : 'supervisor_name', width : 100, sortable : true, align: 'left'},
		{display: 'Address', name : 'cleaner_address', width : 100, sortable : true, align: 'left'},
		{display: 'Phone Number', name : 'cleaner_phone_no', width : 100, sortable : true, align: 'left'},
		{display: 'email', name : 'cleaner_email', width : 150, sortable : true, align: 'left'},
                {display: 'Status', name : 'status', width : 300, sortable : true, align: 'left'}
		],
	buttons : [
		{name: 'Add', bclass: 'add',onpress : eventHandlerCleanerAdminUser},
		{separator: true},		
		{name: 'Edit', bclass: 'edit',onpress : eventHandlerCleanerAdminUser},
		{separator: true},
		{name: 'Delete', bclass: 'delete',onpress : eventHandlerCleanerAdminUser},
		{separator: true}
		],
	searchitems : [
		{display: 'Name', name : 'cleaner_name', isdefault: true},
                {display: 'Supervisor Name', name : 'Supervisor', isdefault: true},
		{display: 'Email ID', name : 'cleaner_email'},
		{display: 'Phone Number', name : 'cleaner_phone_no'},
		{display: 'Address', name : 'cleaner_address'},
		],
	sortname: "cleaner_name",
	sortorder: "asc",
	usepager: true,
	title: 'Cleaner',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	//width: 700,
	height: 'auto'
});  
}

function eventHandlerCleanerAdminUser(com,grid)
{
	if (com=='Add')	{
		location.href="cleaner.php?add";	
	}else if (com=='Import')	{
		location.href="distributors.php?import";	
	}else if (com=='Export')	{
		location.href="export.inc.php?export_distributors_list";	
	}
	else{
		if( j('.trSelected',grid).length<=0 ||  j('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=j('.trSelected',grid);		
		if (com=='Edit'){
			location.href="cleaner.php?id="+items[0].id.substr(3);			
		}else if (com=='Create Login')	{
		location.href="#.php?login&id="+items[0].id.substr(3);
	}
		else if (com=='Delete')	{
		if(confirm("Are you sure, you want to delete selected Cleaner?")){
					
				j.post("cleaner.php", {"delete":"yes","id":items[0].id.substr(3)},function(data)
				{
					j("#message-green").hide();
				  if(data!="") {
				  	//alert(data);
				  	if(data >0 )
				  	{				  		
				  		alert("This Cleaner cannot be deleted.");
				  	}
				  				 
					 j('#flex1').flexOptions().flexReload(); 
				  }else {
					  alert("Record doesn't exists.")
				  }
				  
				});		
			}
	}
			
	}
}


function showSupervisor(){
j("#flex1").flexigrid({
	url: 'supervisor.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Supervisor Name', name : 'supervisor_name', width : 250, sortable : true, align: 'left'},
                {display: 'Building', name : 'building_name', width : 150, sortable : true, align: 'left'},
                {display: 'Phone Number', name : 'supervisor_phone_no', width : 100, sortable : true, align: 'left'},
                {display: 'email', name : 'supervisor_email', width : 150, sortable : true, align: 'left'},
		{display: 'Address', name : 'supervisor_address', width : 100, sortable : true, align: 'left'},
                {display: 'Status', name : 'status', width : 300, sortable : true, align: 'left'}
		],
	buttons : [
		{name: 'Add', bclass: 'add',onpress : eventHandlerSupervisorAdminUser},
		{separator: true},		
		{name: 'Edit', bclass: 'edit',onpress : eventHandlerSupervisorAdminUser},
		{separator: true},
		{name: 'Delete', bclass: 'delete',onpress : eventHandlerSupervisorAdminUser},
		{separator: true}
		],
	searchitems : [
		{display: 'Name', name : 'supervisor_name', isdefault: true},
		{display: 'Email ID', name : 'supervisor_email'},
		{display: 'Phone Number', name : 'supervisor_phone_no'},
		{display: 'Address', name : 'supervisor_address'},
                {display: 'Building', name : 'building_name'},
		],
	sortname: "supervisor_name",
	sortorder: "asc",
	usepager: true,
	title: 'Supervisor',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	//width: 700,
	height: 'auto'
});  
}

function eventHandlerSupervisorAdminUser(com,grid)
{
	if (com=='Add')	{
		location.href="supervisor.php?add";	
	}else{
		if( j('.trSelected',grid).length<=0 ||  j('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=j('.trSelected',grid);		
		if (com=='Edit'){
			location.href="supervisor.php?id="+items[0].id.substr(3);			
		}else if (com=='Create Login')	{
		location.href="#.php?login&id="+items[0].id.substr(3);
	}
		else if (com=='Delete')	{
		if(confirm("Are you sure, you want to delete selected Supervisor?")){
					
				j.post("supervisor.php", {"delete":"yes","id":items[0].id.substr(3)},function(data)
				{
					j("#message-green").hide();
				  if(data!="") {
				  	//alert(data);
				  	if(data >0 )
				  	{				  		
				  		alert("This Supervisor cannot be deleted.");
				  	}
				  				 
					 j('#flex1').flexOptions().flexReload(); 
				  }else {
					  alert("Record doesn't exists.")
				  }
				  
				});		
			}
	}
			
	}
}

function showCity(){
j("#flex1").flexigrid({
	url: 'city.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'City Name', name : 'city_name', width : 300, sortable : true, align: 'left'},
		{display: 'Status', name : 'status', width : 300, sortable : true, align: 'left'}
		],
	buttons : [
		{name: 'Add', bclass: 'add', onpress : eventHandlerCity},
		{separator: true},		
		{name: 'Edit', bclass: 'edit', onpress : eventHandlerCity},
		{separator: true},
		{name: 'Delete', bclass: 'delete', onpress : eventHandlerCity},
		{separator: true}
		],
	searchitems : [
		{display: 'City Name', name : 'city_name', isdefault: true}
		],
	sortname: "city_name",
	sortorder: "asc",
	usepager: true,
	title: 'City',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	//width: 700,
	height: 'auto'
});  
}

function eventHandlerCity(com,grid)
{
	if (com=='Add')	{
		location.href="add_city.php";		
	}else{
		if( j('.trSelected',grid).length<=0 ||  j('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=j('.trSelected',grid);		
		if (com=='Edit'){
			location.href="add_city.php?id="+items[0].id.substr(3);			
		}
		if (com=='Delete'){
			if(confirm("Are you sure, you want to delete selected City?")){
					
				j.post("city.php", {"delete":"yes","id":items[0].id.substr(3)},function(data)
				{		 
				  if(data!="") {					 
					 j('#flex1').flexOptions().flexReload(); 
				  }else {
					  alert("Record doesn't exists.")
				  }
				  
				});		
			}			
		}	
	}
}


function showArea(){
j("#flex1").flexigrid({
	url: 'area.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Area Name', name : 'area_name', width : 300, sortable : true, align: 'left'},
                {display: 'City Name', name : 'city_name', width : 300, sortable : true, align: 'left'},
		{display: 'Status', name : 'status', width : 300, sortable : true, align: 'left'}
		],
	buttons : [
		{name: 'Add', bclass: 'add', onpress : eventHandlerArea},
		{separator: true},		
		{name: 'Edit', bclass: 'edit', onpress : eventHandlerArea},
		{separator: true},
		{name: 'Delete', bclass: 'delete', onpress : eventHandlerArea},
		{separator: true}		
		],
	searchitems : [
		{display: 'Area Name', name : 'area_name', isdefault: true}
		],
	sortname: "area_name",
	sortorder: "asc",
	usepager: true,
	title: 'Area',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	//width: 700,
	height: 'auto'
});  
}

function eventHandlerArea(com,grid)
{
	if (com=='Add')	{
		location.href="add_area.php";		
	}else{
		if( j('.trSelected',grid).length<=0 ||  j('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=j('.trSelected',grid);		
		if (com=='Edit'){
			location.href="add_area.php?id="+items[0].id.substr(3);			
		}
		if (com=='Delete'){
			if(confirm("Are you sure, you want to delete selected Area?")){
					
				j.post("area.php", {"delete":"yes","id":items[0].id.substr(3)},function(data)
				{		 
				  if(data!="") {					 
					 j('#flex1').flexOptions().flexReload(); 
				  }else {
					  alert("Record doesn't exists.")
				  }
				  
				});		
			}			
		}	
	}
}

function showCategoryType(){
j("#flex1").flexigrid({
	url: 'room_category_type.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Room Category', name : 'room_category', width : 200, sortable : true, align: 'left'},
        {display: 'Unit Name', name : 'unit_name', width : 200, sortable : true, align: 'left'},
        {display: 'Minimum', name : 'minimum', width : 150, sortable : true, align: 'left'},
        {display: 'Maximum', name : 'maximum', width : 150, sortable : true, align: 'left'},
		{display: 'Status', name : 'status', width : 100, sortable : true, align: 'left'}
		],
	buttons : [
		{name: 'Add', bclass: 'add', onpress : eventHandlerCategoryTypes},
		{separator: true},		
		{name: 'Edit', bclass: 'edit', onpress : eventHandlerCategoryTypes},
		{separator: true}		
		],
	searchitems : [
		{display: 'Room Category', name : 'room_category', isdefault: true},
		{display: 'Unit Name', name : 'unit_name'}
		],
	sortname: "room_category",
	sortorder: "asc",
	usepager: true,
	title: 'Room Category Type',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	//width: 800,
	height: 'auto'
});  
}
function eventHandlerCategoryTypes(com,grid)
{
	if (com=='Add')	{
		location.href="addCategoryType.php";		
	}else{
		if( j('.trSelected',grid).length<=0 ||  j('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=j('.trSelected',grid);		
		if (com=='Edit'){
			location.href="addCategoryType.php?id="+items[0].id.substr(3);			
		}
		if (com=='Delete'){
			if(confirm("Are you sure, you want to delete selected Building?")){
					
				j.post("addCategoryType.php", {"delete":"yes","id":items[0].id.substr(3)},function(data)
				{		 
				  if(data!="") {					 
					 j('#flex1').flexOptions().flexReload(); 
				  }else {
					  alert("Record doesn't exists.")
				  }
				  
				});		
			}			
		}	
	}
}

function showRoomServices(){
j("#flex1").flexigrid({
	url: 'room_services.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Service Name', name : 'service_name', width : 200, sortable : true, align: 'left'},
        {display: 'No. of Services', name : 'number_of_services', width : 200, sortable : true, align: 'left'},
        {display: 'Discount(%age)', name : 'discount_percentage', width : 150, sortable : true, align: 'left'},
		{display: 'Status', name : 'status', width : 100, sortable : true, align: 'left'}
		],
	buttons : [
		{name: 'Add', bclass: 'add', onpress : eventHandlerRoomServices},
		{separator: true},		
		{name: 'Edit', bclass: 'edit', onpress : eventHandlerRoomServices},
		{separator: true}		
		],
	searchitems : [
		{display: 'Service Name', name : 'service_name', isdefault: true},
		{display: 'Discount', name : 'discount_percentage'}
		],
	sortname: "service_name",
	sortorder: "asc",
	usepager: true,
	title: 'Room Services',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	//width: 800,
	height: 'auto'
});  
}
function eventHandlerRoomServices(com,grid)
{
	if (com=='Add')	{
		location.href="addRoomService.php";		
	}else{
		if( j('.trSelected',grid).length<=0 ||  j('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=j('.trSelected',grid);		
		if (com=='Edit'){
			location.href="addRoomService.php?id="+items[0].id.substr(3);			
		}
		if (com=='Delete'){
			if(confirm("Are you sure, you want to delete selected Building?")){
					
				j.post("addRoomService.php", {"delete":"yes","id":items[0].id.substr(3)},function(data)
				{		 
				  if(data!="") {					 
					 j('#flex1').flexOptions().flexReload(); 
				  }else {
					  alert("Record doesn't exists.")
				  }
				  
				});		
			}			
		}	
	}
}

function showRoomServicePackages(){
j("#flex1").flexigrid({
	url: 'room_service_packages.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Service Name', name : 'service_name', width : 200, sortable : true, align: 'left'},
        {display: 'Room Category', name : 'room_category', width : 200, sortable : true, align: 'left'},
        {display: 'Cleaning Type', name : 'cleaning_type', width : 150, sortable : true, align: 'left'},
        {display: 'Package Rate', name : 'package_rate', width : 150, sortable : true, align: 'left'},
        {display: 'Counter', name : 'counter', width : 150, sortable : true, align: 'left'},
        {display: 'Linen Change', name : 'linen_change_per_room', width : 150, sortable : true, align: 'left'},
		{display: 'Status', name : 'status', width : 100, sortable : true, align: 'left'}
		],
	buttons : [
		{name: 'Add', bclass: 'add', onpress : eventHandlerRoomServicePackages},
		{separator: true},		
		{name: 'Edit', bclass: 'edit', onpress : eventHandlerRoomServicePackages},
		{separator: true}		
		],
	searchitems : [
		{display: 'Service Name', name : 'service_name', isdefault: true},
		{display: 'Room Category', name : 'room_category'},
		{display: 'Cleaning Type', name : 'cleaning_type'},
		],
	sortname: "room_package_id",
	sortorder: "desc",
	usepager: true,
	title: 'Room Service Packages',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	//width: 800,
	height: 'auto'
});  
}
function eventHandlerRoomServicePackages(com,grid)
{
	if (com=='Add')	{
		location.href="addRoomServicePackages.php";		
	}else{
		if( j('.trSelected',grid).length<=0 ||  j('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=j('.trSelected',grid);		
		if (com=='Edit'){
			location.href="addRoomServicePackages.php?id="+items[0].id.substr(3);			
		}
		if (com=='Delete'){
			if(confirm("Are you sure, you want to delete selected Building?")){
					
				j.post("addRoomServicePackages.php", {"delete":"yes","id":items[0].id.substr(3)},function(data)
				{		 
				  if(data!="") {					 
					 j('#flex1').flexOptions().flexReload(); 
				  }else {
					  alert("Record doesn't exists.")
				  }
				  
				});		
			}			
		}	
	}
}

function showBookings(){
j("#flex1").flexigrid({
	url: 'booking.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [
                {display: 'Booking No.', name : 'booking_id', width : 70, sortable : true, align: 'left'},
                
		{display: 'Customer Name', name : 'customer_name', width : 200, sortable : true, align: 'left'},
		{display: 'Cleaning Actual Cost', name : 'cleaning_actual_cost', width : 110, sortable : true, align: 'left'},
                {display: 'Total Cost Paid', name : 'total_cost_paid', width : 100, sortable : true, align: 'left'},
		{display: 'Customer Address', name : 'customer_address', width : 100, sortable : true, align: 'left'},
		{display: 'Building Name', name : 'building_name', width : 100, sortable : true, align: 'left'},
		{display: 'Cleaning Type', name : 'cleaning_type', width : 150, sortable : true, align: 'left'},
                {display: 'Unit Type', name : 'unit_name', width : 100, sortable : true, align: 'left'},
                {display: 'Cleaning Date', name : 'cleaning_date', width : 100, sortable : true, align: 'left'},
                {display: 'Slot Date Time', name : 'slot_start_time', width : 100, sortable : true, align: 'left'},
                {display: 'Slot End Time', name : 'slot_end_time', width : 100, sortable : true, align: 'left'},
                {display: 'Cleaner Name', name : 'cleaner_name', width : 200, sortable : true, align: 'left'},
                {display: 'Supervisor Name', name : 'supervisor_name', width : 200, sortable : true, align: 'left'},
                {display: 'Booking Status', name : 'booking_status', width : 100, sortable : true, align: 'left'},
                {display: 'Status', name : 'status', width : 50, sortable : true, align: 'left'},
                {display: 'Room Detail', name : 'room_detail', width : 300, sortable : true, align: 'left'}
                
		],
	buttons : [		
		{name: 'Edit', bclass: 'edit',onpress : eventHandlerBookings},
		{separator: true}
		],
	searchitems : [
		{display: 'Customer Name', name : 'customer_name', isdefault: true},
                {display: 'Building Name', name : 'building_name', isdefault: true},
		{display: 'Cleaner Name', name : 'cleaner_name'},
		{display: 'Supervisor Name', name : 'supervisor_name'},
		{display: 'Booking Status', name : 'booking_status'},
		],
	sortname: "booking_id",
	sortorder: "desc",
	usepager: true,
	title: 'Bookings',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	//width: 700,
	height: 'auto'
});
}

function eventHandlerBookings(com,grid)
{
	if (com=='Add')	{
		location.href="add_booking.php";		
	}else{
		if( j('.trSelected',grid).length<=0 ||  j('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=j('.trSelected',grid);		
		if (com=='Edit'){
			location.href="add_booking.php?id="+items[0].id.substr(3);			
		}
		if (com=='Delete'){
			if(confirm("Are you sure, you want to delete selected Branch?")){
					
				j.post("add_booking.php", {"delete":"yes","id":items[0].id.substr(3)},function(data)
				{		 
				  if(data!="") {					 
					 j('#flex1').flexOptions().flexReload(); 
				  }else {
					  alert("Record doesn't exists.")
				  }
				  
				});		
			}			
		}	
	}
}
function showCompletedBookings(){
j("#flex1").flexigrid({
	url: 'completed_bookings.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [
                {display: 'Booking No.', name : 'booking_id', width : 70, sortable : true, align: 'left'},
                
		{display: 'Customer Name', name : 'customer_name', width : 200, sortable : true, align: 'left'},
		{display: 'Cleaning Actual Cost', name : 'cleaning_actual_cost', width : 110, sortable : true, align: 'left'},
                {display: 'Total Cost Paid', name : 'total_cost_paid', width : 100, sortable : true, align: 'left'},
		{display: 'Customer Address', name : 'customer_address', width : 100, sortable : true, align: 'left'},
		{display: 'Building Name', name : 'building_name', width : 100, sortable : true, align: 'left'},
		{display: 'Cleaning Type', name : 'cleaning_type', width : 150, sortable : true, align: 'left'},
                {display: 'Unit Type', name : 'unit_name', width : 100, sortable : true, align: 'left'},
                {display: 'Cleaning Date', name : 'cleaning_date', width : 100, sortable : true, align: 'left'},
                {display: 'Slot Date Time', name : 'slot_start_time', width : 100, sortable : true, align: 'left'},
                {display: 'Slot End Time', name : 'slot_end_time', width : 100, sortable : true, align: 'left'},
                {display: 'Cleaner Name', name : 'cleaner_name', width : 200, sortable : true, align: 'left'},
                {display: 'Supervisor Name', name : 'supervisor_name', width : 200, sortable : true, align: 'left'},
                {display: 'Booking Status', name : 'booking_status', width : 100, sortable : true, align: 'left'},
                {display: 'Status', name : 'status', width : 50, sortable : true, align: 'left'},
                {display: 'Room Detail', name : 'room_detail', width : 300, sortable : true, align: 'left'}
                
		],
	buttons : [		
		
		],
	searchitems : [
		{display: 'Customer Name', name : 'customer_name', isdefault: true},
                {display: 'Building Name', name : 'building_name', isdefault: true},
		{display: 'Cleaner Name', name : 'cleaner_name'},
		{display: 'Supervisor Name', name : 'supervisor_name'}
		],
	sortname: "booking_id",
	sortorder: "desc",
	usepager: true,
	title: 'Bookings',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	//width: 700,
	height: 'auto'
});
}


function showCancelledBookings(){
j("#flex1").flexigrid({
	url: 'cancelled_bookings.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [
                {display: 'Booking No.', name : 'booking_id', width : 70, sortable : true, align: 'left'},
                
		{display: 'Customer Name', name : 'customer_name', width : 200, sortable : true, align: 'left'},
		{display: 'Cleaning Actual Cost', name : 'cleaning_actual_cost', width : 110, sortable : true, align: 'left'},
                {display: 'Total Cost Paid', name : 'total_cost_paid', width : 100, sortable : true, align: 'left'},
		{display: 'Customer Address', name : 'customer_address', width : 100, sortable : true, align: 'left'},
		{display: 'Building Name', name : 'building_name', width : 100, sortable : true, align: 'left'},
		{display: 'Cleaning Type', name : 'cleaning_type', width : 150, sortable : true, align: 'left'},
                {display: 'Unit Type', name : 'unit_name', width : 100, sortable : true, align: 'left'},
                {display: 'Cleaning Date', name : 'cleaning_date', width : 100, sortable : true, align: 'left'},
                {display: 'Slot Date Time', name : 'slot_start_time', width : 100, sortable : true, align: 'left'},
                {display: 'Slot End Time', name : 'slot_end_time', width : 100, sortable : true, align: 'left'},
                {display: 'Cleaner Name', name : 'cleaner_name', width : 200, sortable : true, align: 'left'},
                {display: 'Supervisor Name', name : 'supervisor_name', width : 200, sortable : true, align: 'left'},
                {display: 'Booking Status', name : 'booking_status', width : 100, sortable : true, align: 'left'},
                {display: 'Status', name : 'status', width : 50, sortable : true, align: 'left'},
                {display: 'Room Detail', name : 'room_detail', width : 300, sortable : true, align: 'left'}
                
		],
	buttons : [		
		
		],
	searchitems : [
		{display: 'Customer Name', name : 'customer_name', isdefault: true},
                {display: 'Building Name', name : 'building_name', isdefault: true},
		{display: 'Cleaner Name', name : 'cleaner_name'},
		{display: 'Supervisor Name', name : 'supervisor_name'}
		],
	sortname: "booking_id",
	sortorder: "desc",
	usepager: true,
	title: 'Bookings',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	//width: 700,
	height: 'auto'
});
}

/************************************  Query Type *****************************************************/


function showQueryType(){
j("#flex1").flexigrid({
	url: 'query_type.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Query Text', name : 'query_type_text', width : 280, sortable : true, align: 'left'},
		{display: 'Created Date', name : 'created_date', width : 150, sortable : true, align: 'left'},
		{display: 'status', name : 'status', width : 200, sortable : true, align: 'left'}
		],
	buttons : [
		{name: 'Add', bclass: 'add', onpress : eventHandlerquerytype},
		{separator: true},		
		{name: 'Edit', bclass: 'edit', onpress : eventHandlerquerytype},
		{separator: true},
		{name: 'Delete', bclass: 'delete', onpress : eventHandlerquerytype},
		{separator: true}		
		],
	searchitems : [
		{display: 'Query Text', name : 'query_type_text', isdefault: true},
		],
	sortname: "query_type_text",
	sortorder: "asc",
	usepager: true,
	title: 'Query Type',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	width: 850,
	height: 'auto'
});  
}
function eventHandlerquerytype(com,grid)
{

	if (com=='Import'){
		location.href="city_import.php";		
	}
	else if(com=='Export'){
		location.href="export.inc.php?export_distric_new";		
	}
	else if(com=='Add'){
		location.href="addQueryType.php";		
	}else{
		if( j('.trSelected',grid).length<=0 ||  j('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=j('.trSelected',grid);	
		
		
		if(com=='Edit'){
			location.href="addQueryType.php?id="+items[0].id.substr(3);			
		}
		if(com=='Delete'){
			if(confirm("Are you sure, you want to delete selected Query Type?")){
					
				j.post("query_type.php", {"delete":"yes","id":items[0].id.substr(3)},function(data){		 
				  
					
				  if(data!=''){					 
					 j('#flex1').flexOptions().flexReload(); 
					 alert('Query Type deleted successfully!.');
				  }else {
					  alert("Query Type can not be deleted, it is mapped with "+res[0])
				  }
				  
				});		
			}			
		}
		
	}
}
/***********************************End Query Type**************************/


function showCustomerFeedback(){
j("#flex1").flexigrid({
	url: 'customer_feedback.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Feedback', name : 'comments', width : 300, sortable : true, align: 'left'},
		{display: 'Booking No', name : 'booking_id', width : 300, sortable : true, align: 'left'},
		{display: 'Customer Name', name : 'customer_name', width : 150, sortable : true, align: 'left'},
		{display: 'Customer Number', name : 'customer_number', width : 150, sortable : true, align: 'left'},
		{display: 'Customer Email', name : 'customer_email', width : 150, sortable : true, align: 'left'},
		{display: 'Rating', name : 'star_rating', width : 150, sortable : true, align: 'left'},
		{display: 'Date', name : 'created_date', width : 150, sortable : true, align: 'left'}
		],
	buttons : [
		
		],
	searchitems : [
		//{display: 'Booking No',name :'booking_id', isdefault: true},
		{display: 'Customer Name', name : 'customer_name'},
		{display: 'Customer Email', name : 'customer_email'},
		{display: 'Customer Number', name : 'customer_number'}
		
	],
	sortname: "feedback_id",
	sortorder: "desc",
	usepager: true,
	title: 'Customer Feedback',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	//width: 700,
	height: 'auto'
});  
}


function showCleanerFeedback(){
j("#flex1").flexigrid({
	url: 'cleaner_feedback.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Feedback', name : 'comments', width : 300, sortable : true, align: 'left'},
		{display: 'Booking No', name : 'booking_id', width : 300, sortable : true, align: 'left'},
		{display: 'Cleaner Code', name : 'cleaner_code', width : 150, sortable : true, align: 'left'},
		{display: 'Cleaner Name', name : 'cleaner_name', width : 150, sortable : true, align: 'left'},
		{display: 'Cleaner Number', name : 'cleaner_phone_no', width : 150, sortable : true, align: 'left'},
		{display: 'Cleaner Email', name : 'cleaner_email', width : 150, sortable : true, align: 'left'},
		{display: 'Rating', name : 'star_rating', width : 150, sortable : true, align: 'left'},
		{display: 'Date', name : 'created_date', width : 150, sortable : true, align: 'left'}
		],
	buttons : [
		
		],
	searchitems : [
		//{display: 'Booking No',name :'booking_id', isdefault: true},
		{display: 'Cleaner Code', name : 'cleaner_code'},
		{display: 'Cleaner Name', name : 'cleaner_name'},
		{display: 'Cleaner Email', name : 'cleaner_email'},
		{display: 'Cleaner Number', name : 'cleaner_phone_no'}
		
	],
	sortname: "feedback_id",
	sortorder: "desc",
	usepager: true,
	title: 'Cleaner Feedback',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	//width: 700,
	height: 'auto'
});  
}

function showEnquiries(){
j("#flex1").flexigrid({
	url: 'enquiries.php?show=yes',
	dataType: 'json',
	method : "GET", 
	singleSelect:true, 
	colModel : [		
		{display: 'Message', name : 'message', width : 300, sortable : true, align: 'left'},
		{display: 'Query Type', name : 'query_type_text', width : 300, sortable : true, align: 'left'},
		{display: 'Customer Name', name : 'customer_name', width : 150, sortable : true, align: 'left'},
		{display: 'Phone Number', name : 'customer_number', width : 150, sortable : true, align: 'left'},
		{display: 'Email Address', name : 'customer_email', width : 200, sortable : true, align: 'left'},
		{display: 'Customer Address', name : 'customer_address', width : 300, sortable : true, align: 'left'},
		{display: 'Date', name : 'created_date', width : 100, sortable : true, align: 'left'},
		
		],
	buttons : [
		
		],
	searchitems : [
		{display: 'Customer Name', name : 'customer_name', isdefault: true},
		{display: 'Customer Phone', name : 'customer_number'},
		{display: 'Type', name : 'query_type_text'}
		],
	sortname: "contact_query_id",
	sortorder: "asc",
	usepager: true,
	title: 'Enquiries',
	useRp: true,
	rp: 50,
	showTableToggleBtn: true,
	//width: 700,
	height: 'auto'
});  
}

function eventHandlerEnquiry(com,grid)
{
	if (com=='Add')	{
		location.href="add_city.php";		
	}else{
		if( $('.trSelected',grid).length<=0 ||  $('.trSelected',grid).length>1){
			alert("Please Select One Record.");
			return false;
		}
		var items=$('.trSelected',grid);		
		if (com=='Edit'){
			location.href="add_city.php?id="+items[0].id.substr(3);			
		}
		if (com=='Delete'){
			if(confirm("Are you sure, you want to delete selected City?")){
					
				$.post("enquiries.php", {"delete":"yes","id":items[0].id.substr(3)},function(data)
				{		 
				  if(data!="") {					 
					 $('#flex1').flexOptions().flexReload(); 
				  }else {
					  alert("Record doesn't exists.")
				  }
				  
				});		
			}			
		}	
	}
}
