/* =======================================    From     ================================================*/
function validationFrom(d1)
{
	var dateTo = new Date($('#to').val());
	var dd = dateTo.getDate(); 	
	//var mm = dateTo.getMonth(); 
	var mm = ("0" + (dateTo.getMonth() + 1)).slice(-2);
	var yy = dateTo.getFullYear();
	
	var todt = (yy*10000)+(mm*100)+leadingZero(dd);
	var d1 =parseInt(d1,10);
	var d2 =parseInt(todt,10);

	if(d1 >= d2)
	{
		var dateFrom = new Date($('#from').val());
		var dd = dateFrom.getDate();
		var mm = dateFrom.getMonth(); 	
		var yy = dateFrom.getFullYear();
		
		
		var newDateFrom = new Date(yy,mm,dd);
		var dd = newDateFrom.getDate();	
		//var mm = newDateFrom.getMonth();	
		var mm = ("0" + (newDateFrom.getMonth() + 1)).slice(-2);	
		var yy = newDateFrom.getFullYear();

		var frmdt = yy+"-"+mm+"-"+leadingZero(dd);
		$('#to').val($.datepicker.formatDate( "d M yy", new Date(frmdt) ));
	}	
}
function dateFromNext()
{
	//alert('datefromNext');
	var dateFrom = new Date($('#from').val());
	//alert();
	var dd = dateFrom.getDate(); 	
	var mm = dateFrom.getMonth(); 	
	var yy = dateFrom.getFullYear();
	
	
	var newDateFrom = new Date(yy,mm,dd+1);
	var dd = newDateFrom.getDate();	

	//var mm = newDateFrom.getMonth();	
	var mm = ("0" + (newDateFrom.getMonth() + 1)).slice(-2);	
	var yy = newDateFrom.getFullYear();
	var frmdt = yy+"-"+mm+"-"+leadingZero(dd);
	$('#from').val($.datepicker.formatDate( "d M yy", new Date(frmdt) ));
	var d1 = (yy*10000)+(mm*100)+leadingZero(dd);
	 validationFrom(d1);
}

function dateFromPrev()
{
	var dateFrom = new Date($('#from').val());

	var dd = dateFrom.getDate(); 
	
	
	var mm = dateFrom.getMonth(); 	
	var yy = dateFrom.getFullYear();
	
	var newDateFrom = new Date(yy,mm,dd-1);
	var dd = newDateFrom.getDate();	
	//var mm = newDateFrom.getMonth();	
	var mm = ("0" + (newDateFrom.getMonth() + 1)).slice(-2);	
	var yy = newDateFrom.getFullYear();
	var frmdt = yy+"-"+mm+"-"+leadingZero(dd);
	$('#from').val($.datepicker.formatDate( "d M yy", new Date(frmdt) ));
}

/* =======================================    To     ================================================*/
 
function validationTo(d1)
{
	var dateFrom = new Date($('#from').val());
	var dd = dateFrom.getDate(); 	
	var mm = dateFrom.getMonth(); 	
	var yy = dateFrom.getFullYear();
		
	var frmdt = (yy*10000)+(mm*100)+leadingZero(dd);
	var d1 =parseInt(d1,10);
	var d2 =parseInt(frmdt,10);
	//alert(d1+' '+d2);
	if(d1 <= d2)
	{
		var dateTo = new Date($('#to').val());
		var dd = dateTo.getDate(); 	
		var mm = dateTo.getMonth(); 	
		var yy = dateTo.getFullYear();
	
		var newDateTo = new Date(yy,mm,dd);
		var dd = newDateTo.getDate();	
		//var mm = newDateTo.getMonth();	
		var mm = ("0" + (newDateTo.getMonth() + 1)).slice(-2);	
		var yy = newDateTo.getFullYear();
		var todt = yy+"-"+mm+"-"+leadingZero(dd);
		$('#from').val($.datepicker.formatDate( "d M yy", new Date(todt) ));
	}	
}
function dateToNext()
{
	var dateTo = new Date($('#to').val());
	
	var dd = dateTo.getDate(); 	
	var mm = dateTo.getMonth(); 	
	var yy = dateTo.getFullYear();
	
	var newDateTo = new Date(yy,mm,dd+1);
	var dd = newDateTo.getDate();	
	//var mm = newDateTo.getMonth();	
	var mm = ("0" + (newDateTo.getMonth() + 1)).slice(-2);	
	var yy = newDateTo.getFullYear();
	var todt = yy+"-"+mm+"-"+leadingZero(dd);
	$('#to').val($.datepicker.formatDate( "d M yy", new Date(todt) ));
	
	 
}
function dateToPrev()
{
	var dateTo = new Date($('#to').val());
	
	var dd = dateTo.getDate(); 	
	var mm = dateTo.getMonth(); 	
	var yy = dateTo.getFullYear();
	
	var newDateTo = new Date(yy,mm,dd-1);
	var dd = newDateTo.getDate();	
	//var mm = newDateTo.getMonth();	
	var mm = ("0" + (newDateTo.getMonth() + 1)).slice(-2);	
	var yy = newDateTo.getFullYear();
	var todt = yy+"-"+mm+"-"+leadingZero(dd);
	$('#to').val($.datepicker.formatDate( "d M yy", new Date(todt) ));
	var d1 = (yy*10000)+(mm*100)+leadingZero(dd);
	 validationTo(d1);
} 


function leadingZero(value){
   if(value < 10){
      return "0" + value.toString();
   }
   return value.toString();    
}


