function distype(value)
{

	var scheme_mode = $('input[name=slabCreateStatus]:checked').val();
	//alert(value);
	if(value==1)
	{

		$("#min_qty").removeAttr('disabled');
		$("#item_list").attr('disabled', 'disabled');
		$("#itemtypeshow").show();
		$(".SlabWiseSchemeEnteryPoint").show();

		if(scheme_mode == 1) {
			$(".withOutSlabWiseGroup").hide();
			$(".withSlabWiseGroup").show();
			$("#min_qty_1").removeAttr('disabled');
			$("#max_qty_1").removeAttr('disabled');
			$("#dis_per1").removeAttr('disabled');
			$("#dis_amt1").removeAttr('disabled');
			$("#trip_desc1").removeAttr('disabled');
			$("#minqtyshow").hide();

		} else {
			$(".withOutSlabWiseGroup").show();
			$("#min_qty_1").attr('disabled', 'disabled');
			$("#max_qty_1").attr('disabled', 'disabled');
			$("#dis_per1").attr('disabled', 'disabled');
			$("#dis_amt1").attr('disabled', 'disabled');
			$("#trip_desc1").attr('disabled', 'disabled');
			$(".withSlabWiseGroup").hide();
			$("#minqtyshow").show();
		}
		
		$("#showItem").hide();
	}
	else 
	{
		$("#min_qty").attr('disabled', 'disabled');
		$("#itemtypeshow").hide();
		$("#minqtyshow").hide();
	}



	if(value==2)
	{
		$("input[value=0]").prop('checked',true);
		$(".SlabWiseSchemeEnteryPoint").hide();
		$(".withOutSlabWiseGroup").show();
		$(".withSlabWiseGroup").hide();
		$("#min_qty_1").attr('disabled', 'disabled');
		$("#max_qty_1").attr('disabled', 'disabled');
		$("#dis_per1").attr('disabled', 'disabled');
		$("#dis_amt1").attr('disabled', 'disabled');
		$("#trip_desc1").attr('disabled', 'disabled');

		$("#min_amt").removeAttr('disabled');
		$("#minamtshow").show();
		$("#comboshow").show();
		$("#comboshow1").show();
		$("#comboshow2").show();
	}
	else 
	{
		$("#min_amt").attr('disabled', 'disabled');

		
		$("#minamtshow").hide();
		$("#comboshow").hide();
		$("#comboshow1").hide();
		$("#comboshow2").hide();
	}


	if(value=='')
	{
		$(".SlabWiseSchemeEnteryPoint").hide();
		$(".withOutSlabWiseGroup").hide();
		$(".withSlabWiseGroup").hide();
		//$("#Dis_type").hide();
		$("#dis_per").hide();
		$("#dis_amt").hide();
		$("#focshow").hide();
		$("#trip_desc").hide();
		
	}

}




function showSlabOptions (slabCreateStatus) {

	var scheme_mode = document.getElementById('dis_mode').value;
	//alert(slabCreateStatus);

	// Slab status 1 = true, 0 = false
	// Scheme Mode 1 = Quantity, 2 = Amount

	if(slabCreateStatus == 1 && scheme_mode == 1) {
		$(".withOutSlabWiseGroup").hide();
		$(".withSlabWiseGroup").show();
		$("#minqtyshow").hide();
		$("#minamtshow").hide();
		$("#min_qty").attr('disabled', 'disabled');
		$("#min_amt").attr('disabled', 'disabled');
		$("#trip_desc").attr('disabled', 'disabled');
		$("#dis_per").attr('disabled', 'disabled');

			$("#min_qty_1").removeAttr('disabled');
			$("#max_qty_1").removeAttr('disabled');
			$("#dis_per1").removeAttr('disabled');
			$("#dis_amt1").removeAttr('disabled');
			$("#trip_desc1").removeAttr('disabled');


	} else if( slabCreateStatus == 1 ) {
		
		$(".withOutSlabWiseGroup").show();
		$(".withSlabWiseGroup").hide();
		$("#minqtyshow").show();
		$("#minamtshow").hide();
		$("#min_qty").removeAttr('disabled');
		$("#dis_per").removeAttr('disabled');
		$("#min_amt").attr('disabled', 'disabled');
		$("#trip_desc").attr('disabled', 'disabled');


			$("#min_qty_1").removeAttr('disabled');
			$("#max_qty_1").removeAttr('disabled');
			$("#dis_per1").removeAttr('disabled');
			$("#dis_amt1").removeAttr('disabled');
			$("#trip_desc1").removeAttr('disabled');


	} else {
		
		$(".withOutSlabWiseGroup").show();
		$(".withSlabWiseGroup").hide();
		$("#minqtyshow").show();
		$("#minamtshow").hide();
		$("#min_amt").attr('disabled', 'disabled');
		$("#min_qty").removeAttr('disabled');



		$("#min_qty_1").attr('disabled', 'disabled');
		$("#max_qty_1").attr('disabled', 'disabled');
		$("#dis_per1").attr('disabled', 'disabled');
		$("#dis_amt1").attr('disabled', 'disabled');
		$("#trip_desc1").attr('disabled', 'disabled');

		
	}

}



function itemtype(value)
{



	if(value==2)
	{
		$("#cat").removeAttr('disabled');
		$("#catshow").show();
	}
	else 
	{
		$("#cat").attr('disabled', 'disabled');
		$("#catshow").hide();
	}
	if(value==3)
	{
		$("#item").removeAttr('disabled');
		$("#itmshow").show();
	}
	else 
	{
		$("#item").attr('disabled', 'disabled');
		$("#itmshow").hide();
	}


	if(value==4)
	{
		$("#brand").removeAttr('disabled');
		$("#brandshow").show();
	}
	else 
	{
		$("#brand").attr('disabled', 'disabled');
		$("#brandshow").hide();
	}







}
function partype(value)
{
	if(value==2)
	{
		$("#state_list").removeAttr('disabled');
		$("#satshow").show();
	}
	else 
	{
		$("#state_list").attr('disabled', 'disabled');
		$("#satshow").hide();
	}
	if(value==3)
	{
		$("#city_list").removeAttr('disabled');
		$("#cityshow").show();
	}
	else 
	{
		$("#city_list").attr('disabled', 'disabled');
		$("#cityshow").hide();
	}
	if(value==4)
	{
		$("#ret_list").removeAttr('disabled');
		$("#retshow").show();
	}
	else 
	{
		$("#ret_list").attr('disabled', 'disabled');
		$("#retshow").hide();
	}


	if(value==5)
	{
		$("#dis_list").removeAttr('disabled');
		$("#disshow").show();
	}
	else 
	{
		$("#dis_list").attr('disabled', 'disabled');
		$("#disshow").hide();
	}





}
function dtype(value)
{
	if(value==1)
	{
		$("#dis_per").removeAttr('disabled');
		$("#pershow").show();
	}
	else 
	{
		$("#dis_per").attr('disabled', 'disabled');
		$("#pershow").hide();
	}
	if(value==2)
	{
		$("#dis_amt").removeAttr('disabled');
		$("#amtshow").show();
	}
	else 
	{
		$("#dis_amt").attr('disabled', 'disabled');
		$("#amtshow").hide();
	}
	if(value==3)
	{
		$("#focshow").show();
	}
	else 
	{
		$("#focshow").hide();
	}

	if(value==4)
	{
		$("#trip_desc").removeAttr('disabled');
		$("#tripDescShow").show();
	}
	else 
	{
		$("#trip_desc").attr('disabled', 'disabled');
		$("#tripDescShow").hide();
	}



}











function dtypeslab(value, whichSlab)
{
	if(value==1)
	{
		$("#dis_per"+whichSlab).removeAttr('disabled');
		$("#pershow"+whichSlab).show();
	}
	else 
	{
		$("#dis_per"+whichSlab).attr('disabled', 'disabled');
		$("#pershow"+whichSlab).hide();
	}
	if(value==2)
	{
		$("#dis_amt"+whichSlab).removeAttr('disabled');
		$("#amtshow"+whichSlab).show();
	}
	else 
	{
		$("#dis_amt"+whichSlab).attr('disabled', 'disabled');
		$("#amtshow"+whichSlab).hide();
	}
	if(value==3)
	{
		$("#focshow"+whichSlab).show();
	}
	else 
	{
		$("#focshow"+whichSlab).hide();
	}

	if(value==4)
	{
		$("#trip_desc"+whichSlab).removeAttr('disabled');
		$("#tripDescShow"+whichSlab).show();
	}
	else 
	{
		$("#trip_desc"+whichSlab).attr('disabled', 'disabled');
		$("#tripDescShow"+whichSlab).hide();
	}



}



























// Changes For New Scheme
function hidePartyType(value)
{
	//alert(value);
	if(value==1)
	{
		//$("#disParty").hide();
		//$("#dispartyType").show();

		$(".disshow").show();
		$(".retshow").hide();
		$("#retshow").hide();
		$('#party_type  option[value="1').prop("selected", true);
		$("#ret_list").attr('disabled', 'disabled');
		//$("#disshow").show();
		
	}
	else 
	{
		$(".disshow").hide();
		$("#dis_list").attr('disabled', 'disabled');
		$("#ret_list").removeAttr('disabled');
		$(".retshow").show();
		$("#disshow").hide();
		//$("#party_type").attr('checked', false);
		//$("#party_type select").val("1");
		$('#party_type  option[value="1').prop("selected", true);
		//$("#disParty").show();
		//$("#dispartyType").hide();
	}
}
// End Changes For New Scheme

function combotype(value)
{
	if(value==2)
	{
		$("#item_list").removeAttr('disabled');
		$("#showItem").show();
	}
	else 
	{
		$("#item_list").attr('disabled', 'disabled');
		$("#showItem").hide();
	}
}



(function( $ ) {
		$.widget( "ui.catbox", {
			_create: function() {
				var self = this,
					select = this.element.hide(),
					selected = select.children( ":selected" ),
					value = selected.val() ? selected.text() : "";

				function removeIfInvalid(element) {
					var value = $( element ).val(),
						matcher = new RegExp( "^" + $.ui.autocomplete.escapeRegex( value ) + "$", "i" ),
						valid = false;
					select.children( "option" ).each(function() {
						if ( $( this ).text().match( matcher ) ) {
							this.selected = valid = true;
							return false;
						}
					});
					if ( !valid ) {
						// remove invalid value, as it didn't match anything
						$( element )
							.val( "" )
							.attr( "title", value + " didn't match any item" )
							.tooltip( "open" );
						select.val( "" );
						setTimeout(function() {
							input.tooltip( "close" ).attr( "title", "" );
						}, 2500 );
						input.data( "autocomplete" ).term = "";
						return false;
					}
				}

				var input = this.input = $( "<input>" )
					.insertAfter( select )
					.val( value )
					.attr( "title", "" )
					.autocomplete({
						delay: 0,
						minLength: 0,
						source: function( request, response ) {
							var matcher = new RegExp( $.ui.autocomplete.escapeRegex(request.term), "i" );
							response( select.children( "option" ).map(function() {
								var text = $( this ).text();
								if ( this.value && ( !request.term || matcher.test(text) ) )
									return {
										label: text.replace(
											new RegExp(
												"(?![^&;]+;)(?!<[^<>]*)(" +
												$.ui.autocomplete.escapeRegex(request.term) +
												")(?![^<>]*>)(?![^&;]+;)", "gi"
											), "<strong>$1</strong>" ),
										value: text,
										option: this
									};
							}) );
						},
						select: function( event, ui ) {
							ui.item.option.selected = true;
							self._trigger( "selected", event, {
								item: ui.item.option
							});
						},
						change: function( event, ui ) {
							if ( !ui.item )
								return removeIfInvalid( this );
						}
					})
					.addClass( "ui-widget ui-widget-content ui-corner-left" );

				input.data( "autocomplete" )._renderItem = function( ul, item ) {
					return $( "<li>" )
						.data( "item.autocomplete", item )
						.append( "<a>" + item.label + "</a>" )
						.appendTo( ul );
				};

				this.button = $( "<button type='button'>&nbsp;</button>" )
					.attr( "tabIndex", -1 )
					.attr( "title", "Show All Category" )
					.tooltip()
					.insertAfter( input )
					.button({
						icons: {
							primary: "ui-icon-triangle-1-s"
						},
						text: false
					})
					.removeClass( "ui-corner-all" )
					.addClass( "ui-corner-right ui-button-icon" )
					.click(function() {
						// close if already visible
						if ( input.autocomplete( "widget" ).is( ":visible" ) ) {
							input.autocomplete( "close" );
							removeIfInvalid( input );
							return;
						}

						// work around a bug (likely same cause as #5265)
						$( this ).blur();

						// pass empty string as value to search for, displaying all results
						input.autocomplete( "search", "" );
						input.focus();
					});

					input
						.tooltip({
							position: {
								of: this.button
							},
							tooltipClass: "ui-state-highlight"
						});
			},

			destroy: function() {
				this.input.remove();
				this.button.remove();
				this.element.show();
				$.Widget.prototype.destroy.call( this );
			}
		});
		$.widget( "ui.itembox", {
			_create: function() {
				var self = this,
					select = this.element.hide(),
					selected = select.children( ":selected" ),
					value = selected.val() ? selected.text() : "";

				function removeIfInvalid(element) {
					var value = $( element ).val(),
						matcher = new RegExp( "^" + $.ui.autocomplete.escapeRegex( value ) + "$", "i" ),
						valid = false;
					select.children( "option" ).each(function() {
						if ( $( this ).text().match( matcher ) ) {
							this.selected = valid = true;
							return false;
						}
					});
					if ( !valid ) {
						// remove invalid value, as it didn't match anything
						$( element )
							.val( "" )
							.attr( "title", value + " didn't match any item" )
							.tooltip( "open" );
						select.val( "" );
						setTimeout(function() {
							input.tooltip( "close" ).attr( "title", "" );
						}, 2500 );
						input.data( "autocomplete" ).term = "";
						return false;
					}
				}

				var input = this.input = $( "<input>" )
					.insertAfter( select )
					.val( value )
					.attr( "title", "" )
					.autocomplete({
						delay: 0,
						minLength: 0,
						source: function( request, response ) {
							var matcher = new RegExp( $.ui.autocomplete.escapeRegex(request.term), "i" );
							response( select.children( "option" ).map(function() {
								var text = $( this ).text();
								if ( this.value && ( !request.term || matcher.test(text) ) )
									return {
										label: text.replace(
											new RegExp(
												"(?![^&;]+;)(?!<[^<>]*)(" +
												$.ui.autocomplete.escapeRegex(request.term) +
												")(?![^<>]*>)(?![^&;]+;)", "gi"
											), "<strong>$1</strong>" ),
										value: text,
										option: this
									};
							}) );
						},
						select: function( event, ui ) {
							ui.item.option.selected = true;
							self._trigger( "selected", event, {
								item: ui.item.option
							});
						},
						change: function( event, ui ) {
							if ( !ui.item )
								return removeIfInvalid( this );
						}
					})
					.addClass( "ui-widget ui-widget-content ui-corner-left" );

				input.data( "autocomplete" )._renderItem = function( ul, item ) {
					return $( "<li>" )
						.data( "item.autocomplete", item )
						.append( "<a>" + item.label + "</a>" )
						.appendTo( ul );
				};

				this.button = $( "<button type='button'>&nbsp;</button>" )
					.attr( "tabIndex", -1 )
					.attr( "title", "Show All Items" )
					.tooltip()
					.insertAfter( input )
					.button({
						icons: {
							primary: "ui-icon-triangle-1-s"
						},
						text: false
					})
					.removeClass( "ui-corner-all" )
					.addClass( "ui-corner-right ui-button-icon" )
					.click(function() {
						// close if already visible
						if ( input.autocomplete( "widget" ).is( ":visible" ) ) {
							input.autocomplete( "close" );
							removeIfInvalid( input );
							return;
						}

						// work around a bug (likely same cause as #5265)
						$( this ).blur();

						// pass empty string as value to search for, displaying all results
						input.autocomplete( "search", "" );
						input.focus();
					});

					input
						.tooltip({
							position: {
								of: this.button
							},
							tooltipClass: "ui-state-highlight"
						});
			},

			destroy: function() {
				this.input.remove();
				this.button.remove();
				this.element.show();
				$.Widget.prototype.destroy.call( this );
			}
		});








		$.widget( "ui.freeitembox", {
			_create: function() {
				var self = this,
					select = this.element.hide(),
					selected = select.children( ":selected" ),
					value = selected.val() ? selected.text() : "";

				function removeIfInvalid(element) {
					var value = $( element ).val(),
						matcher = new RegExp( "^" + $.ui.autocomplete.escapeRegex( value ) + "$", "i" ),
						valid = false;
					select.children( "option" ).each(function() {
						if ( $( this ).text().match( matcher ) ) {
							this.selected = valid = true;
							return false;
						}
					});
					if ( !valid ) {
						// remove invalid value, as it didn't match anything
						$( element )
							.val( "" )
							.attr( "title", value + " didn't match any item" )
							.tooltip( "open" );
						select.val( "" );
						setTimeout(function() {
							input.tooltip( "close" ).attr( "title", "" );
						}, 2500 );
						input.data( "autocomplete" ).term = "";
						return false;
					}
				}

				var input = this.input = $( "<input>" )
					.insertAfter( select )
					.val( value )
					.attr( "title", "" )
					.autocomplete({
						delay: 0,
						minLength: 0,
						source: function( request, response ) {
							var matcher = new RegExp( $.ui.autocomplete.escapeRegex(request.term), "i" );
							response( select.children( "option" ).map(function() {
								var text = $( this ).text();
								if ( this.value && ( !request.term || matcher.test(text) ) )
									return {
										label: text.replace(
											new RegExp(
												"(?![^&;]+;)(?!<[^<>]*)(" +
												$.ui.autocomplete.escapeRegex(request.term) +
												")(?![^<>]*>)(?![^&;]+;)", "gi"
											), "<strong>$1</strong>" ),
										value: text,
										option: this
									};
							}) );
						},
						select: function( event, ui ) {
							ui.item.option.selected = true;
							self._trigger( "selected", event, {
								item: ui.item.option
							});
						},
						change: function( event, ui ) {
							if ( !ui.item )
								return removeIfInvalid( this );
						}
					})
					.addClass( "ui-widget ui-widget-content ui-corner-left" );

				input.data( "autocomplete" )._renderItem = function( ul, item ) {
					return $( "<li>" )
						.data( "item.autocomplete", item )
						.append( "<a>" + item.label + "</a>" )
						.appendTo( ul );
				};

				this.button = $( "<button type='button'>&nbsp;</button>" )
					.attr( "tabIndex", -1 )
					.attr( "title", "Show All Items" )
					.tooltip()
					.insertAfter( input )
					.button({
						icons: {
							primary: "ui-icon-triangle-1-s"
						},
						text: false
					})
					.removeClass( "ui-corner-all" )
					.addClass( "ui-corner-right ui-button-icon" )
					.click(function() {
						// close if already visible
						if ( input.autocomplete( "widget" ).is( ":visible" ) ) {
							input.autocomplete( "close" );
							removeIfInvalid( input );
							return;
						}

						// work around a bug (likely same cause as #5265)
						$( this ).blur();

						// pass empty string as value to search for, displaying all results
						input.autocomplete( "search", "" );
						input.focus();
					});

					input
						.tooltip({
							position: {
								of: this.button
							},
							tooltipClass: "ui-state-highlight"
						});
			},

			destroy: function() {
				this.input.remove();
				this.button.remove();
				this.element.show();
				$.Widget.prototype.destroy.call( this );
			}
		});
	})( jQuery );

	$(function() {
		$( "#cat" ).catbox();
		$( "#toggle" ).click(function() {
			$( "#cat" ).toggle();
		});
		$( "#item" ).itembox();
		$( "#toggle" ).click(function() {
			$( "#item" ).toggle();
		});

		$( "#brand" ).itembox();
		$( "#toggle" ).click(function() {
			$( "#brand" ).toggle();
		});



		$( ".free_item" ).freeitembox();
		$( "#toggle" ).click(function() {
			$( ".free_item" ).toggle();
		});
	});
	
