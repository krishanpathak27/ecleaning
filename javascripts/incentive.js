function inctype(value)
{
	if(value==1 || value== 2 || value== 4 || value== 6 || value== 7 || value==10 || value==12 || value==14)
		{		
			$("#showSecondaryValuerow").hide();
			$("#secondary_value").attr('disabled', 'disabled');
		}
		else{
			
			$("#showSecondaryValuerow").show();
			$("#secondary_value").removeAttr('disabled');
			}
			
	if(value=='')
		{		
			$("#showPrimaryVal").hide();
			$("#value").attr('disabled', 'disabled');
			$("#showSecondaryValuerow").hide();
			$("#secondary_value").attr('disabled', 'disabled');
			$("#addquali").hide();
		}
		else
		{
			
			$("#showPrimaryVal").show();
			$("#value").removeAttr('disabled');
			$("#addquali").show();
		}

	if(value==14)
	{
		$("#segment_list").removeAttr('disabled');
		$("#showsegment").show();
		$("#showValue").show();
		$("#showSegmentHeading").show();
		$("#showQtu").show();
	}
	else 
	{
		$("#segment_list").attr('disabled', 'disabled');
		$("#showsegment").hide();
		$("#showQtu").hide();
		$("#showSegmentHeading").hide();
	}

	if(value==1)
	{
		$("#category_list").removeAttr('disabled');
		$("#showcat").show();
		$("#showValue").show();
		$("#showCategoryNumber").show();
		$("#showQtu").show();
		
	}
	else 
	{
		$("#category_list").attr('disabled', 'disabled');
		$("#showcat").hide();
		$("#showQtu").hide();
		$("#showCategoryNumber").hide();
	}
	if(value==2)
	{
		$("#item_list").removeAttr('disabled');
		$("#showItem").show();
		$("#showQtu").show();
		$("#showValue").show();
		$("#showItemDet1").show();
	
	}
	else 
	{
		$("#item_list").attr('disabled', 'disabled');
		$("#showItem").hide();
		$("#showQtu").hide();
		$("#showItemDet1").hide();
		
	}
	if(value==3)
	{
		$("#showQtu").show();
		$("#showValue").show();
		$("#showItemNumber").show();
		$("#showSecondaryValueOrder").show();
		
	}
	else 
	{
		$("#showItemNumber").hide();
		$("#showQtu").hide();
		$("#showSecondaryValueOrder").hide();
		
	}
	if(value==4)
	{
		$("#showValue").show();
		$("#showOrderNumber").show();
		$("#showQtu").show();
		
	}
	else 
	{
		//$("#item_list").attr('disabled', 'disabled');
		$("#showOrderNumber").hide();
		$("#showQtu").hide();
	}
	if(value==5)
	{
		$("#showValue").show();
		$("#showOrderValue").show();
		$("#showQtu").show();
		$("#showSecondaryValueOrder").show();
		
	}
	else 
	{

		$("#showOrderValue").hide();
		$("#showQtu").hide();
		
	}
	if(value==6)
	{
		$("#showValue").show();
		$("#showSchemeNumber").show();
		$("#showQtu").show();
	}
	else 
	{

		$("#showSchemeNumber").hide();
		$("#showQtu").hide();
	}
	if(value==7)
	{
		$("#showValue").show();
		$("#showRetailerNumber").show();
		$("#showQtu").show();
	}
	else 
	{

		$("#showRetailerNumber").hide();
		$("#showQtu").hide();
	}
	
	if(value==10)
	{
		$("#showValue").show();
		$("#showRetailerAdded").show();
		$("#showQtu").show();
	}
	else 
	{

		$("#showRetailerAdded").hide();
		$("#showQtu").hide();
	}
	
	if(value==8)
	{
		$("#item_list1").removeAttr('disabled');
		$("#showItem1").show();
		$("#showQtu").show();
		$("#showValue").show();
		$("#showItemDet").show();
	}
	else 
	{
		$("#item_list1").attr('disabled', 'disabled');
		$("#showItem1").hide();
		$("#showQtu").hide();
		$("#showItemDet").hide();
	}
	
	if(value==9)
	{
		$("#focus_item_list").removeAttr('disabled');
		$("#retailer_placed").removeAttr('disabled');
		$("#showFocusItem").show();
		$("#showRetailerPlaced").show();
		$("#showValue").show();
		$("#showFocusRetailerItemNumber").show();
		$("#showQtu").show();
		$("#showSecondaryValueRetailer").show();
		
	}
	else 
	{
		$("#focus_item_list").attr('disabled', 'disabled');
		$("#retailer_placed").attr('disabled', 'disabled');
		$("#showFocusItem").hide();
		$("#showRetailerPlaced").hide();
		$("#showFocusRetailerItemNumber").hide();
		$("#showQtu").hide();
		$("#showSecondaryValueRetailer").hide();
		
	}
	if(value==11)
	{
		$("#showValue").show();
		$("#showTotalCall").show();
		$("#showQtu").show();
		$("#showSecondaryValueProductive").show();
	}
	else 
	{

		$("#showTotalCall").hide();
		$("#showQtu").hide();
		$("#showSecondaryValueProductive").hide();
	}
	if(value==12)
	{
		$("#showValue").show();
		$("#showTotalAmount").show();
	}
	else 
	{

		$("#showTotalAmount").hide();
		$("#showQtu").hide();
	}
	if(value==1){
	$("#showRewordType").show();
	} else {
	$("#showRewordType").hide();
	$("#showRewordSlab").hide();
	}
}

function partype(value)
{
	if(value==2)
	{
		$("#state_list").removeAttr('disabled');
		$("#satshow").show();
	}
	else 
	{
		$("#state_list").attr('disabled', 'disabled');
		$("#satshow").hide();
	}
	if(value==3)
	{
		$("#city_list").removeAttr('disabled');
		$("#cityshow").show();
	}
	else 
	{
		$("#city_list").attr('disabled', 'disabled');
		$("#cityshow").hide();
	}
}

function shownumber(value)
{
	var count_value1=$("#day_list").val();
	var count_value2=$("#week_list").val();
	var count_value4=$("#month_list").val();
	var count_value5=$("#quarterly_list").val();
	var count_value6=$("#halfyearly_list").val();
	var count_value7=$("#yearly_list").val();
	if(count_value1=='' || count_value2=='' || count_value4=='' || count_value5=='' || count_value6=='' || count_value7==''){  
	document.getElementById("end_date").value = '';
	document.getElementById("day_list").value = '';
	document.getElementById("week_list").value = '';
	document.getElementById("month_list").value = '';
	document.getElementById("quarterly_list").value = '';
	document.getElementById("halfyearly_list").value = '';
	document.getElementById("yearly_list").value = '';
	}
	if(value==1)
	{
		$("#day_list").removeAttr('disabled');
		$("#dayshow").show();
	}
	else 
	{
		$("#day_list").attr('disabled', 'disabled');
		$("#dayshow").hide();
	}
	if(value==2)
	{
		$("#week_list").removeAttr('disabled');
		$("#weekshow").show();
	}
	else 
	{
		$("#week_list").attr('disabled', 'disabled');
		$("#weekshow").hide();
	}
	if(value==4)
	{
		$("#month_list").removeAttr('disabled');
		$("#monthshow").show();
	}
	else 
	{
		$("#month_list").attr('disabled', 'disabled');
		$("#monthshow").hide();
	}
	if(value==5)
	{
		$("#quarterly_list").removeAttr('disabled');
		$("#quarterlyshow").show();
	}
	else 
	{
		$("#quarterly_list").attr('disabled', 'disabled');
		$("#quarterlyshow").hide();
	}
	if(value==6)
	{
		$("#halfyearly_list").removeAttr('disabled');
		$("#halfyearlyshow").show();
	}
	else 
	{
		$("#halfyearly_list").attr('disabled', 'disabled');
		$("#halfyearlyshow").hide();
	}
	if(value==7)
	{
		$("#yearly_list").removeAttr('disabled');
		$("#yearlyshow").show();
	}
	else 
	{
		$("#yearly_list").attr('disabled', 'disabled');
		$("#yearlyshow").hide();
	}
}

function removenumber(value)
{
	document.getElementById("end_date").value = '';
	document.getElementById("day_list").value = '';
	document.getElementById("week_list").value = '';
	document.getElementById("month_list").value = '';
	document.getElementById("quarterly_list").value = '';
	document.getElementById("halfyearly_list").value = '';
	document.getElementById("yearly_list").value = '';
}

	
