  /**********************************************************
  * DESC : Select all the checkbox
  * Author : AJAY
  * Created on : 28 May 2015
  */

  function select_all() {
  $('input[class=case]:checkbox').each(function(){ 
    if($('input[class=check_all]:checkbox:checked').length == 0){ 
      $(this).prop("checked", false); 
    } else {
      $(this).prop("checked", true); 
    } 
  });
}






  /**********************************************************
  * DESC : Add new row of item with select & search option
  * Author : AJAY
  * Created on : 28 May 2015
  */



  $(document).ready(function() {
    // body...


    var today = new Date();
    var dd = today.getDate();

    var mm = today.getMonth()+1; 
    var yyyy = today.getFullYear();
    if(dd<10) 
    {
        dd='0'+dd;
    } 

    if(mm<10) 
    {
        mm='0'+mm;
    } 
    var todaydate = dd+'-'+mm+'-'+yyyy;

    var i=$('table #order tr').length;
    //alert(i);
    
    

      $(".addmore").on('click',function(){

        
      count=$('table #order tr').length;
      //alert(count);
     getServicePersonnelList();
     // alert(variable);
        var data="<tr><td><input type='checkbox' class='case'/></td>";
          data+="<td><span id='snum"+i+"'>"+count+".</span></td>";
          data+="<td><input class='form-control autocomplete_txt item_list required' type='text' data-type='bsn' id='bsn_"+i+"' name='bsn["+i+"]' autocomplete='off' /><input class='form-control autocomplete_txt lastsaveditemids' type='hidden' data-type='bsn_id' id='bsn_id_"+i+"' name='bsn_id["+i+"]' autocomplete='off'></td>";
         // data+="<td><input class='form-control autocomplete_txt' type='text' readonly data-type='item_name' id='item_name_"+i+"' name='item_name[]' autocomplete='off'/></td>";
         // data+="<td><input class='form-control autocomplete_txt' type='text' data-type='item_code' id='item_code_"+i+"' name='item_code[]' autocomplete='off'/></td>";
         data+="<td><input class='form-control autocomplete_txt item_name required' type='text' data-type='item_name' id='item_name_"+i+"' name='item_name_"+i+"' autocomplete='off' /><input class='form-control autocomplete_txt lastsaveditemnameids' type='hidden' data-type='item_id' id='item_id_"+i+"' name='item_id_"+i+"' autocomplete='on'></td>";

         data+="<td><input class='form-control autocomplete_txt division_name required' type='text' data-type='division_name' id='division_name_"+i+"' name='division_name_"+i+"' autocomplete='off' /><input class='form-control autocomplete_txt lastsaveditemids' type='hidden' data-type='division_id' id='division_id_"+i+"' name='division_id_"+i+"' autocomplete='on'></td>";

         data+="<td><input class='form-control autocomplete_txt segment_name required' type='text' data-type='segment_name' id='segment_name_"+i+"' name='segment_name_"+i+"' autocomplete='off' /><input class='form-control autocomplete_txt lastsaveditemids' type='hidden' data-type='segment_id' id='segment_id_"+i+"' name='segment_id_"+i+"' autocomplete='on'></td>";

         data+="<td><input class='form-control complaint_id' type='text' data-type='complaint_id' id='complaint_id_"+i+"' name='complaint_id_"+i+"' autocomplete='off' />";

          data+="<td><input class='form-control autocomplete_txt calc required' readonly type='text' data-type='manufacture_date' id='manufacture_date_"+i+"' name='manufacture_date_"+i+"' autocomplete='off' /></td>";
         data+="<td><input class='form-control autocomplete_txt required' readonly type='text' data-type='warranty_end_date' id='warranty_end_date_"+i+"' name='warranty_end_date_"+i+"' /></td>";

         data+="<td><input class='form-control autocomplete_txt required' readonly type='text' data-type='date_of_sale' id='date_of_sale_"+i+"' name='date_of_sale_"+i+"' /></td>";

          // data+="<td><input class='form-control autocomplete_txt date_of_sale required' readonly type='text' data-type='date_of_sale' id='date_of_sale_"+i+"' name='date_of_sale_"+i+"' /></td>";

          data+="";

          data+="<td><div class='row' style='margin:0px;''><label>Yes <input class='form-control' type='radio' data-type='doc_received_sts' id='doc_received_sts_"+i+"' name='doc_received_sts_"+i+"' value='Y' checked /></label><label>No <input class='form-control' type='radio' data-type='doc_received_sts' id='doc_received_sts_"+i+"' name='doc_received_sts_"+i+"' value='N' /></label></div></td>";
          data+="<td><input class='form-control autocomplete_txt datepicker' type='text' data-type='doc_received_date' id='doc_received_date_"+i+"' name='doc_received_date_"+i+"' value="+todaydate+" /></td>";

          data+="<td><select name='service_personnel_id_"+i+"' id='service_personnel_id_"+i+"' data-type='service_personnel_id_"+i+"' class='getHtml' required></select></td>";

         // data+="<td><select class='styledselect_form_5' id='scheme_"+i+"' name='scheme[]'><option value=''>No Scheme</option></select></td>";
          //data+="<td><input class='form-control autocomplete_txt calc' type='text' readonly data-type='discount' id='discount_"+i+"' name='discount[]' autocomplete='off'/></td>";
      $('table #order').append(data);
      row = i ;
      i++;
    });

function getServicePersonnelList()
        {
          
            $.ajax({
                url : 'ajax_itemlist.php',
                // async:false,
                data: {
                 type: 'servicePersonnel',
              },
               success: function( data1 ) {
                 variable = data1;
                 $(".getHtml").append(variable);
              }
          });
        }




  /**********************************************************
  * DESC : Deleted the selcted rows of where class name deletes
  * Author : AJAY
  * Created on : 28 May 2015
  */
      
       $(".deletes").on('click', function() {
        $('.case:checkbox:checked').parents("table #order tr").remove();
        $('.check_all').prop("checked", false); 
        check();
      });

  });



  /**********************************************************
  * DESC : Re assign the row number after deleted any row or rows
  * Author : AJAY
  * Created on : 28 May 2015
  */


    function check(){
      obj=$('table #order tr td').find('span');
      $.each( obj, function( key, value ) {
        //alert(key);
        id=value.id;
        $('#'+id).html(key+1);
      });
    }


    //$('#list').on("keyup", function(e) {
   /* $(document).on("keyup", function(e) {
      //alert('jjj');
      alert($(this).attr('id'));

      $('#'+current_element_id+'-error').remove();

    });*/




  /**********************************************************
  * DESC : select and search of retailer list
  * Author : AJAY
  * Created on : 28 May 2015
  */


  $(document).on('focus','.datepicker', function(){
    $( ".datepicker" ).datepicker({
     // showOn: "button",
     // buttonImage: "images/calendar.gif",
      //buttonImageOnly: true,
      //buttonText: "Select date",
      dateFormat: "d M yy",
      defaultDate: "w",
      changeMonth: true,
      numberOfMonths: 1
    });
  });

  var $s = jQuery.noConflict();
  $s(document).on('focus','#distributor_name', function(){
       
      $s(this).autocomplete({

          source: function( request, response ) {
            console.log(request);
              $s.ajax({
                url : 'ajax_retailerlist.php',
                dataType: "json",
              data: {
                 name_startsWith: request.term,
                 type: 'distributors',
                 row_num : 1
              },
               success: function( data ) {
                //alert(data);
                //console.log(data);
                if(data == 0){

                  alert('No distributors found');
                  $s('#distributor_name').val('');

               } else {

                     response( $s.map( data, function( item ) {
                      var code = item.split("|");
                      return {
                        label: code[0],
                        value: code[0],
                        data : item
                      } 
                    }));
                }
              },
              error: function(xhr, error, message) {

                alert(error+message);

              }
              });
            },
            autoFocus: true,          
            minLength: 0,
            select: function( event, ui ) 
            {
             var names = ui.item.data.split("|");           
             id_arr = $(this).attr('id');
             //id = id_arr.split("_");
             //elementId = id[id.length-1]; 
            //alert(names[0]);       
            $s('#distributor_name').val(names[0]);
           // alert(names[1]);
            $s('#distributor_id').val(names[1]);
             // $('#distributor_name').prop('disabled', true);
             $s('#distributor_code').val(names[3]);
             // $('#customer_name').prop('disabled', true);
             // $('#customer_address').prop('disabled', true);
             // $('#customer_contact').prop('disabled', true);
          }           
        });

     });





  /**********************************************************
  * DESC : Autocomplete item list
  * Author : AJAY
  * Created on : 28 May 2015
  */

    var $b = jQuery.noConflict();
    $b(document).on('focus','.item_list',function(){

      type = $b(this).data('type');
      //console.log(type);

      //var data_to_send = $.serialize(info);

      
      if(type =='bsn' )autoTypeNo=0;
      if(type =='item_code' )autoTypeNo=1; 
      // if(type =='item_code' )autoTypeNo=2; 
      //if(type =='country_code' )autoTypeNo=3; 
      
       $b(this).autocomplete({
           minLength: 0,
           source: function( request, response ) {



            //$.each( request, function( key, value ) {
              //alert( key + ": " + value );
           //});

           var inputTypes = [];

            //$('input[name="item_id[]"]').each(function() {
            //    inputTypes.push($(this).val());
            //});

            $.each($b('input[name="bsn_id[]"]'),function(){
               //console.log($(this).val());
               //console.log('Hello');
               inputTypes.push($b(this).val());
            });

            //alert(request.term);
            $b.ajax({
                url : 'ajax_itemlist.php',
                dataType: "json",
              data: {
                 name_startsWith: request.term,
                 last_used_items: JSON.stringify(inputTypes),
                 type: 'bsn',
                 row_num : 1
              },
               success: function( data ) {
                //alert($('input[name="item_id[]"]').serialize());
                //alert(data);
               // console.log(data);

                var array = $b.map(data, function (item) {
                   var code = item.split("|");
                   return {
                       label: code[autoTypeNo],
                       value: code[autoTypeNo],
                       data : item
                   }
               });
               //call the filter here
               response($b.ui.autocomplete.filter(array, request.term));
                /* response( $.map( data, function( item ) {
                  var code = item.split("|");
                  return {
                    label: code[0],
                    value: code[0],
                    data : item
                  }
                }));*/
              },
              error: function(xhr, error, message) {
                //console.log(xhr);
                //alert(error+message);
                //alert();

              }
              });

               
               
           },
           focus: function() {
             // prevent value inserted on focus
             return false;
           },
           select: function( event, ui ) {
          var names = ui.item.data.split("|"); 


           console.log(names);

           id_arr = $b(this).attr('id');
           id = id_arr.split("_");
           elementId = id[id.length-1];

         // alert(elementId);
           if(names[6]<=0) {
            $b('#bsn_'+elementId).val(names[0]);
            $b('#bsn_'+elementId).prop('readonly', true);
            $b('#bsn_'+elementId).attr('title', names[0]);
            //$('#bsn_'+elementId).prop('disabled', true);

            $b('#bsn_id_'+elementId).val(names[1]);
            $b('#bsn_id_'+elementId).prop('readonly', names[1]);

            $b('#item_name_'+elementId).val(names[2]);
            $b('#item_name_'+elementId).prop('readonly', names[2]);

            $b('#item_id_'+elementId).val(names[5]);
            $b('#item_id_'+elementId).prop('readonly', names[5]);

            $b('#division_name_'+elementId).val(names[9]);
            $b('#division_name_'+elementId).prop('readonly', names[9]);

            $b('#segment_name_'+elementId).val(names[8]);
            $b('#segment_name_'+elementId).prop('readonly', names[8]);

            

           
            $b('#manufacture_date_'+elementId).val(names[3]);
           // $('#manufacture_date_'+elementId).prop('readonly', names[3]);
            
            $b('#warranty_end_date_'+elementId).val(names[4]);
           // $('#warranty_end_date_'+elementId).prop('readonly', names[4]);
            $b('#date_of_sale_'+elementId).val(names[7]);
            $b('#date_of_sale_'+elementId).prop('readonly', names[7]);
            
            // $('#_'+elementId).val(names[2]);
            // $('#item_price_'+elementId).val(names[3]);



            // enable after select an item
            // $('#qty_'+elementId).removeAttr('readonly');
            // $('#qty_'+elementId).focus();



            // enable after applied
            //$('#discount_'+elementId).removeAttr('readonly');


            $b.ajax({
            method: "POST",
            url : 'ajax_itemlist.php',
            data: { runtimeBSN: names[0], requestType: "checkBsnComplaint" },
            success:function( resultData ) {
              var data = $b.parseJSON(resultData);
              if(data.status == 'success') {
                console.log(data);
                console.log(data.id);

                 if(parseInt(data.id)>0) {
                  $b('#complaint_id_'+elementId).val(data.id);
                  $b('#complaint_id_'+elementId).prop('readonly', true);
                }
              }
              
            }
          });

          } else {
            $b('#bsn_'+elementId).val('');
            alert('Duplicate BSN MRN!');

            return false;
          }

        

           }
       });
       
       $b.ui.autocomplete.filter = function (array, term) {
            var matcher = new RegExp("^" + $b.ui.autocomplete.escapeRegex(term), "i");
            return $b.grep(array, function (value) {
                return matcher.test(value.label || value.value || value);
            });
       };
    });
  var $c = jQuery.noConflict();
  $c(document).on('blur','.item_list',function(){
    
       var bsn = $c(this).val();
       var elem = $c(this).attr('id');
       var loop = elem.split("_");
        // console.log("loop: "+loop);
       var loop_final = loop[1];
       // console.log("loop: "+loop_final);

       if(bsn.length >=10)
        {
            $c.ajax({
              url: "ajax_itemlist.php",
              type: "post",
              data: {bsn:bsn,type:'addBSN'},
              DataType: 'text',
              success: function(data){
                //alert($('input[name="item_id[]"]').serialize());
                //alert(data);
                // console.log(data);
// ["CL02412361|271|Bronco 150AHTubular (EB2400TT)|01\/02\/14|01\/01\/70|23||Others|Channel"]

                var tableData = $c('#order tbody td').map( function() {
                  return $c(this).is(':has(:input)') ? // check td contains input 
                    $c(':input', this).val() : // if contains return it's value
                    $c(this).text(); // else return text content
                }).get();
                // console.log(tableData);
                var data_arr = data.split("|");
                // console.log(data);

                // $('#bsn_'+loop_final).val(data_arr[0]);
                $c('#item_name_'+loop_final).val(data_arr[2]);
                $c('#bsn_'+loop_final).prop('readonly', 'readonly');
                $c('#division_name_'+loop_final).val(data_arr[8]);
                $c('#segment_name_'+loop_final).val(data_arr[7]);
                $c('#manufacture_date_'+loop_final).val(data_arr[3]);
                $c('#item_id_'+loop_final).val(data_arr[5]);
                $c('#bsn_id_'+loop_final).val(data_arr[1]);
                $c('#warranty_end_date_'+loop_final).removeClass("required");
                $c('#date_of_sale_'+loop_final).removeClass("required");
                var array = $c.map(data, function (item) {
                   var code = item.split("|");
                   return {
                       label: code[autoTypeNo],
                       value: code[autoTypeNo],
                       data : item
                   }
               });
               //call the filter here
              // response($.ui.autocomplete.filter(array, request.term));
                /* response( $.map( data, function( item ) {
                  var code = item.split("|");
                  return {
                    label: code[0],
                    value: code[0],
                    data : item
                  }
                }));*/
              }});
        }
        else
        {
          return false;
        }
  });















  /**********************************************************
  * DESC : Autocomplete item list
  * Author : AJAY
  * Created on : 28 May 2015
  */

    var $d = jQuery.noConflict();
    $d(document).on('focus','.item_name',function(){

      type = $d(this).data('type');
      console.log(type);

      //var data_to_send = $.serialize(info);

      
      if(type =='item_name' )autoTypeNo=0;
      if(type =='item_code' )autoTypeNo=1; 
      // if(type =='item_code' )autoTypeNo=2; 
      //if(type =='country_code' )autoTypeNo=3; 
      
       $d(this).autocomplete({
           minLength: 0,
           source: function( request, response ) {



            //$.each( request, function( key, value ) {
              //alert( key + ": " + value );
           //});

           var inputTypes = [];

            //$('input[name="item_id[]"]').each(function() {
            //    inputTypes.push($(this).val());
            //});

            $d.each($d('input[name="item_id[]"]'),function(){
               console.log($d(this).val());
              // console.log('Hello');
               inputTypes.push($d(this).val());
            });

            //alert(request.term);
            $d.ajax({
                url : 'ajax_itemlist.php',
                dataType: "json",
              data: {
                 name_startsWith: request.term,
                 last_used_items: JSON.stringify(inputTypes),
                 type: 'items',
                 row_num : 1
              },
               success: function( data ) {
                //alert($('input[name="item_id[]"]').serialize());
                //alert(data);
                //console.log(data);

                var array = $d.map(data, function (item) {
                   var code = item.split("|");
                   return {
                       label: code[autoTypeNo],
                       value: code[autoTypeNo],
                       data : item
                   }
               });
               //call the filter here
               response($d.ui.autocomplete.filter(array, request.term));
                /* response( $.map( data, function( item ) {
                  var code = item.split("|");
                  return {
                    label: code[0],
                    value: code[0],
                    data : item
                  }
                }));*/
              },
              error: function(xhr, error, message) {
                //console.log(xhr);
                //alert(error+message);
                //alert();

              }
              });

               
               
           },
           focus: function() {
             // prevent value inserted on focus
             return false;
           },
           select: function( event, ui ) {
               var names = ui.item.data.split("|");           
           id_arr = $d(this).attr('id');
           id = id_arr.split("_");
           elementId = id[id.length-1];
           // $('#bsn_'+elementId).val(names[0]);
           //  $('#bsn_'+elementId).prop('readonly', true);
           //  $('#bsn_'+elementId).attr('title', names[0]);
            //$('#bsn_'+elementId).prop('disabled', true);

            $d('#item_name_'+elementId).val(names[1]);
           // $('#item_name_'+elementId).prop('readonly', names[1]);
             $d('#item_name_'+elementId).attr('title', names[1]);

            $d('#item_id_'+elementId).val(names[2]);
            $d('#division_name_'+elementId).val(names[9]);
            $d('#segment_name_'+elementId).val(names[8]);
            //$('#item_id_'+elementId).prop('readonly', names[2]);



            // $('#item_name_'+elementId).val(names[2]);
            // $('#item_name_'+elementId).prop('readonly', names[2]);
           
            // $('#manufacture_date_'+elementId).val(names[3]);
            // $('#manufacture_date_'+elementId).prop('readonly', names[3]);
            
            // $('#warranty_end_date_'+elementId).val(names[4]);
            // $('#warranty_end_date_'+elementId).prop('readonly', names[4]);
            
            // $('#_'+elementId).val(names[2]);
            // $('#item_price_'+elementId).val(names[3]);



            // enable after select an item
            // $('#qty_'+elementId).removeAttr('readonly');
            // $('#qty_'+elementId).focus();



            // enable after applied
            //$('#discount_'+elementId).removeAttr('readonly');

           }
       });
       
       $d.ui.autocomplete.filter = function (array, term) {
            var matcher = new RegExp("^" + $d.ui.autocomplete.escapeRegex(term), "i");
            return $d.grep(array, function (value) {
                return matcher.test(value.label || value.value || value);
            });
       };
    });




















  /**********************************************************
  * DESC : Datepicker
  * Author : AJAY
  * Created on : 28 May 2015
  */





  $(function() {
    $( "#datepicker" ).datepicker({
     // showOn: "button",
     // buttonImage: "images/calendar.gif",
      //buttonImageOnly: true,
      //buttonText: "Select date",
      dateFormat: "d M yy",
      defaultDate: "w",
      changeMonth: true,
      numberOfMonths: 1
    });
  });



  $(function() {
    $( "#challan_date" ).datepicker({
     // showOn: "button",
     // buttonImage: "images/calendar.gif",
      //buttonImageOnly: true,
      //buttonText: "Select date",
      dateFormat: "d M yy",
      defaultDate: "w",
      changeMonth: true,
      numberOfMonths: 1
    });
  });

 $(function() {
    $( ".date_picker" ).datepicker({
     // showOn: "button",
     // buttonImage: "images/calendar.gif",
      //buttonImageOnly: true,
      //buttonText: "Select date",
      dateFormat: "d M yy",
      defaultDate: "w",
      changeMonth: true,
      numberOfMonths: 1
    });
  });


  $(function() {
    $( "#frieght_bill_date" ).datepicker({
     // showOn: "button",
     // buttonImage: "images/calendar.gif",
      //buttonImageOnly: true,
      //buttonText: "Select date",
      dateFormat: "d M yy",
      defaultDate: "w",
      changeMonth: true,
      numberOfMonths: 1
    });
  });





  /**********************************************************
  * DESC : on select item show item details, Calculate item total amount based on quantity input by user & check sechmes
  * Author : AJAY
  * Created on : 8th June 2015
  */



    $(function(){
      var focus = 0,
        blur = 0;
      $(document).on('keyup', ".calc", function() {
         //var current_element_id = $(this).attr('id');
          var qty = $(this).val();
          var type = $(this).data('type');

          id_arr = $(this).attr('id');
          id = id_arr.split("_");

          //alert(id[1]);
          var item_price = $('#item_price_'+id[1]).val();


          var item_id = $('#item_id_'+id[1]).val();

          var retailer_id = $('#retailer_id').val();


          //alert(item_price);
          //alert(id[1]);
          //var qty = $(this).val();
         //alert(item_price);
          var ttlamt = 0;

          switch (type) {

            case 'qty' :

                     if(typeof qty!='undefined' && Number.isInteger(parseInt(qty)) && qty>0 && Number.isInteger(parseInt(item_id)) > 0 && Number.isInteger(parseInt(retailer_id)) > 0 ) {
                        ttlamt = qty * item_price;
                        $('#ttlamt_'+id[1]).val(ttlamt.toFixed(2));
                        $(this).css('border', '1px solid #eee');


                        // Check Schemes on the basis of selected item quantity

                        $.ajax({

                       //url: "ajax_checkschemes.php",
                       url: "order.php?action=checkSchemes",
                        dataType: "text",
                        data: { 'item_id' : item_id, 'quantity' : qty, 'retailer_id' : retailer_id},
                        success: function(result) {
                          //alert(result);
                          //console.log(result);
                          $('#scheme_'+id[1]).html(result);

                        },
                        error: function(xhr, error, message) {

                            alert(error+message);

                          }

                        });


                         // Check Schemes on the basis of selected item quantity




                     } else {
                      alert('Please select a retailer or field value should be numeric');
                      $('#scheme_'+id[1]).html('<option value="0">No Scheme</option>');
                      $(this).val();
                      $('#ttlamt_'+id[1]).val('');
                      $('#discount_'+id[1]).val('');
                      //$(this).css('border', '1px solid #ccc');
                      //$(this).parent().after('<label id="cname-error" class="'+current_element_id+', error" for="cname">This field value should be numeric</label>');
                     // $(this).foucs();
                      //$('#ttlamt_'+id[1]).val(ttlamt.toFixed(2));
                     }
              break;


             case 'discount' : 


             break;
            }

          });

      });








 









    /***************************************************************************
  * DESC : This function used to calcualte selected schemes 
  * Author : AJAY
  * Created : 10th June 2015
  *
  *
  **/

   
    $(function(){

      $('.calc').each(function(key, value){
          
          var qty = $(this).val();
          var type = $(this).data('type');

          id_arr = $(this).attr('id');
          id = id_arr.split("_");

          //alert(id[1]);
          var item_price = $('#item_price_'+id[1]).val();
          var item_id = $('#item_id_'+id[1]).val();
          var retailer_id = $('#retailer_id').val();

          //var discount_id = 'scheme_'+id[1];

          //var selected_discount_id = $('input[name=scheme_'+id[1]+']').val();
          var selected_discount_id = $('#last_saved_scheme_'+id[1]).val();

          // /alert(selected_discount_id);

          var ttlamt = 0;

          switch (type) {

            case 'qty' :

              if(typeof qty!='undefined' && Number.isInteger(parseInt(qty)) && qty>0 && Number.isInteger(parseInt(item_id)) > 0 && Number.isInteger(parseInt(retailer_id)) > 0 ) {
                // Check Schemes on the basis of selected item quantity
                var scheme_var = null;
                var scheme_var = 'scheme_'+id[1];

                 $.ajax({
                  //url: "ajax_checkschemes.php",
                  url: "order.php?action=checkSchemes",
                  dataType: "text",
                  data: { 'item_id' : item_id, 'quantity' : qty, 'retailer_id' : retailer_id, 'selected_discount_id' : selected_discount_id },
                  success: function(result) {
                    //alert(scheme_var);
                    //alert(result);
                    //console.log(result);
                    //alert('scheme_'+id[1]);
                    $('#'+scheme_var).html(result);
                  },error: function(xhr, error, message) {  alert(error+message); }
                });
                 // Check Schemes on the basis of selected item quantity
               }
            break;

            default : 
            break;
            }
          });
      });