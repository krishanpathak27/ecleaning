  /**********************************************************
  * DESC : Select all the checkbox
  * Author : AJAY
  * Created on : 28 May 2015
  */

  function select_all() {
  $('input[class=case]:checkbox').each(function(){ 
    if($('input[class=check_all]:checkbox:checked').length == 0){ 
      $(this).prop("checked", false); 
    } else {
      $(this).prop("checked", true); 
    } 
  });
}






  /**********************************************************
  * DESC : Add new row of item with select & search option
  * Author : AJAY
  * Created on : 28 May 2015
  */



  $(document).ready(function() {
    // body...

    var i=$('table #order tr').length;
    //alert(i);

      $(".addmore").on('click',function(){
       
      count=$('table #order tr').length;
      //alert(count);
      
        var data="<tr><td><input type='checkbox' class='case'/></td>";
          data+="<td><span id='snum"+i+"'>"+count+".</span></td>";
          data+="<td><input class='form-control autocomplete_txt item_list' type='text' data-type='itemcode' id='itemcode_"+i+"' name='itemcode[]' autocomplete='off' /><input class='form-control autocomplete_txt lastsaveditemids' type='hidden' data-type='item_id' id='item_id_"+i+"' name='item_id[]' autocomplete='off'></td>";
          data+="<td><input class='form-control autocomplete_txt' type='text' readonly data-type='item_name' id='item_name_"+i+"' name='item_name[]' autocomplete='off'/></td>";
          data+="<td><input class='form-control autocomplete_txt' type='text' readonly data-type='item_price' id='item_price_"+i+"' name='item_price[]' autocomplete='off'/></td>";
          data+="<td><input class='form-control autocomplete_txt calc' type='text' readonly data-type='qty' id='qty_"+i+"' name='qty[]' autocomplete='off' /></td>";
          data+="<td><input class='form-control autocomplete_txt' type='text' readonly data-type='ttlamt' id='ttlamt_"+i+"' name='ttlamt[]' /></td>";
          data+="<td><select class='styledselect_form_5' id='scheme_"+i+"' name='scheme[]'><option value=''>No Scheme</option></select></td>";
          //data+="<td><input class='form-control autocomplete_txt calc' type='text' readonly data-type='discount' id='discount_"+i+"' name='discount[]' autocomplete='off'/></td>";
      $('table #order').append(data);
      row = i ;
      i++;
    });






  /**********************************************************
  * DESC : Deleted the selcted rows of where class name deletes
  * Author : AJAY
  * Created on : 28 May 2015
  */
      
       $(".deletes").on('click', function() {
        $('.case:checkbox:checked').parents("table #order tr").remove();
        $('.check_all').prop("checked", false); 
        check();
      });

  });



  /**********************************************************
  * DESC : Re assign the row number after deleted any row or rows
  * Author : AJAY
  * Created on : 28 May 2015
  */


    function check(){
      obj=$('table #order tr td').find('span');
      $.each( obj, function( key, value ) {
        //alert(key);
        id=value.id;
        $('#'+id).html(key+1);
      });
    }


    //$('#list').on("keyup", function(e) {
   /* $(document).on("keyup", function(e) {
      //alert('jjj');
      alert($(this).attr('id'));

      $('#'+current_element_id+'-error').remove();

    });*/




  /**********************************************************
  * DESC : select and search of retailer list
  * Author : AJAY
  * Created on : 28 May 2015
  */




  $(document).on('focus','#retailer_name', function(){
       
      $(this).autocomplete({

          source: function( request, response ) {
            //alert('k');
              $.ajax({
                url : 'ajax_retailerlist.php',
                dataType: "json",
              data: {
                 name_startsWith: request.term,
                 type: 'retailers',
                 row_num : 1
              },
               success: function( data ) {
                //alert(data);
                //console.log(data);
                if(data == 0){

                  alert('No retailer found');

               } else {

                     response( $.map( data, function( item ) {
                      var code = item.split("|");
                      return {
                        label: code[0],
                        value: code[0],
                        data : item
                      }
                    }));
                }
              },
              error: function(xhr, error, message) {

                alert(error+message);

              }
              });
            },
            autoFocus: true,          
            minLength: 0,
            select: function( event, ui ) {
             var names = ui.item.data.split("|");           
             id_arr = $(this).attr('id');
             //id = id_arr.split("_");
             //elementId = id[id.length-1]; 
            //alert(names[0]);       
            $('#retailer_name').val(names[0]);
            $('#retailer_id').val(names[1]);
             $('#retailer_name').prop('disabled', true);
          }           
        });

     });





  /**********************************************************
  * DESC : Autocomplete item list
  * Author : AJAY
  * Created on : 28 May 2015
  */


    $(document).on('focus','.item_list',function(){

      type = $(this).data('type');

      //var data_to_send = $.serialize(info);

      
      if(type =='itemcode' )autoTypeNo=0;
      if(type =='item_name' )autoTypeNo=1; 
      if(type =='item_id' )autoTypeNo=2; 
      //if(type =='country_code' )autoTypeNo=3; 
      
       $(this).autocomplete({
           minLength: 0,
           source: function( request, response ) {



            //$.each( request, function( key, value ) {
              //alert( key + ": " + value );
           //});

           var inputTypes = [];

            //$('input[name="item_id[]"]').each(function() {
            //    inputTypes.push($(this).val());
            //});

            $.each($('input[name="item_id[]"]'),function(){
               //alert($(this).val());
               inputTypes.push($(this).val());
            });

            //alert(request.term);
            $.ajax({
                url : 'ajax_itemlist.php',
                dataType: "json",
              data: {
                 name_startsWith: request.term,
                 last_used_items: JSON.stringify(inputTypes),
                 type: 'items',
                 row_num : 1
              },
               success: function( data ) {
                //alert($('input[name="item_id[]"]').serialize());
                //alert(data);
                //console.log(data);

                var array = $.map(data, function (item) {
                   var code = item.split("|");
                   return {
                       label: code[autoTypeNo],
                       value: code[autoTypeNo],
                       data : item
                   }
               });
               //call the filter here
               response($.ui.autocomplete.filter(array, request.term));
                /* response( $.map( data, function( item ) {
                  var code = item.split("|");
                  return {
                    label: code[0],
                    value: code[0],
                    data : item
                  }
                }));*/
              },
              error: function(xhr, error, message) {

                alert(error+message);

              }
              });

               
               
           },
           focus: function() {
             // prevent value inserted on focus
             return false;
           },
           select: function( event, ui ) {
               var names = ui.item.data.split("|");           
           id_arr = $(this).attr('id');
           id = id_arr.split("_");
           elementId = id[id.length-1];
           $('#itemcode_'+elementId).val(names[0]);
            $('#itemcode_'+elementId).prop('readonly', true);
            $('#itemcode_'+elementId).attr('title', names[0]);
            $('#itemcode_'+elementId).prop('disabled', true);
            $('#item_name_'+elementId).val(names[1]);
            $('#item_name_'+elementId).prop('disabled', names[1]);
            $('#item_name_'+elementId).attr('title', names[1]);
            $('#item_id_'+elementId).val(names[2]);
            $('#item_price_'+elementId).val(names[3]);



            // enable after select an item
            $('#qty_'+elementId).removeAttr('readonly');
            $('#qty_'+elementId).focus();



            // enable after applied
            $('#discount_'+elementId).removeAttr('readonly');

           }
       });
       
       $.ui.autocomplete.filter = function (array, term) {
            var matcher = new RegExp("^" + $.ui.autocomplete.escapeRegex(term), "i");
            return $.grep(array, function (value) {
                return matcher.test(value.label || value.value || value);
            });
       };
    });






  /**********************************************************
  * DESC : Datepicker
  * Author : AJAY
  * Created on : 28 May 2015
  */





  $(function() {
    $( "#datepicker" ).datepicker({
     // showOn: "button",
     // buttonImage: "images/calendar.gif",
      //buttonImageOnly: true,
      //buttonText: "Select date",
      dateFormat: "d M yy",
      defaultDate: "w",
      changeMonth: true,
      numberOfMonths: 1
    });
  });












  /**********************************************************
  * DESC : on select item show item details, Calculate item total amount based on quantity input by user & check sechmes
  * Author : AJAY
  * Created on : 8th June 2015
  */



    $(function(){
      var focus = 0,
        blur = 0;
      $(document).on('keyup', ".calc", function() {
         //var current_element_id = $(this).attr('id');
          var qty = $(this).val();
          var type = $(this).data('type');

          id_arr = $(this).attr('id');
          id = id_arr.split("_");

          //alert(id[1]);
          var item_price = $('#item_price_'+id[1]).val();


          var item_id = $('#item_id_'+id[1]).val();

          var retailer_id = $('#retailer_id').val();


          //alert(item_price);
          //alert(id[1]);
          //var qty = $(this).val();
         //alert(item_price);
          var ttlamt = 0;

          switch (type) {

            case 'qty' :

                     if(typeof qty!='undefined' && Number.isInteger(parseInt(qty)) && qty>0 && Number.isInteger(parseInt(item_id)) > 0 && Number.isInteger(parseInt(retailer_id)) > 0 ) {
                        ttlamt = qty * item_price;
                        $('#ttlamt_'+id[1]).val(ttlamt.toFixed(2));
                        $(this).css('border', '1px solid #eee');


                        // Check Schemes on the basis of selected item quantity

                        $.ajax({

                       //url: "ajax_checkschemes.php",
                       url: "order.php?action=checkSchemes",
                        dataType: "text",
                        data: { 'item_id' : item_id, 'quantity' : qty, 'retailer_id' : retailer_id},
                        success: function(result) {
                          //alert(result);
                          //console.log(result);
                          $('#scheme_'+id[1]).html(result);

                        },
                        error: function(xhr, error, message) {

                            alert(error+message);

                          }

                        });


                         // Check Schemes on the basis of selected item quantity




                     } else {
                      alert('Please select a retailer or field value should be numeric');
                      $('#scheme_'+id[1]).html('<option value="0">No Scheme</option>');
                      $(this).val();
                      $('#ttlamt_'+id[1]).val('');
                      $('#discount_'+id[1]).val('');
                      //$(this).css('border', '1px solid #ccc');
                      //$(this).parent().after('<label id="cname-error" class="'+current_element_id+', error" for="cname">This field value should be numeric</label>');
                     // $(this).foucs();
                      //$('#ttlamt_'+id[1]).val(ttlamt.toFixed(2));
                     }
              break;


             case 'discount' : 


             break;
            }

          });

      });








 









    /***************************************************************************
  * DESC : This function used to calcualte selected schemes 
  * Author : AJAY
  * Created : 10th June 2015
  *
  *
  **/

   
    $(function(){

      $('.calc').each(function(key, value){
          
          var qty = $(this).val();
          var type = $(this).data('type');

          id_arr = $(this).attr('id');
          id = id_arr.split("_");

          //alert(id[1]);
          var item_price = $('#item_price_'+id[1]).val();
          var item_id = $('#item_id_'+id[1]).val();
          var retailer_id = $('#retailer_id').val();

          //var discount_id = 'scheme_'+id[1];

          //var selected_discount_id = $('input[name=scheme_'+id[1]+']').val();
          var selected_discount_id = $('#last_saved_scheme_'+id[1]).val();

          // /alert(selected_discount_id);

          var ttlamt = 0;

          switch (type) {

            case 'qty' :

              if(typeof qty!='undefined' && Number.isInteger(parseInt(qty)) && qty>0 && Number.isInteger(parseInt(item_id)) > 0 && Number.isInteger(parseInt(retailer_id)) > 0 ) {
                // Check Schemes on the basis of selected item quantity
                var scheme_var = null;
                var scheme_var = 'scheme_'+id[1];

                 $.ajax({
                  //url: "ajax_checkschemes.php",
                  url: "order.php?action=checkSchemes",
                  dataType: "text",
                  data: { 'item_id' : item_id, 'quantity' : qty, 'retailer_id' : retailer_id, 'selected_discount_id' : selected_discount_id },
                  success: function(result) {
                    //alert(scheme_var);
                    //alert(result);
                    //console.log(result);
                    //alert('scheme_'+id[1]);
                    $('#'+scheme_var).html(result);
                  },error: function(xhr, error, message) {  alert(error+message); }
                });
                 // Check Schemes on the basis of selected item quantity
               }
            break;

            default : 
            break;
            }
          });
      });