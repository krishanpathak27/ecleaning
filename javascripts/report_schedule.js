// Report Schedule JavaScript Document

 
 
// Default load saved data
 function showDetails(rpt_id) {
	//alert(rpt_id);
	if($.isNumeric(rpt_id) == true)  {
	var rpt_schedule_id;
	var selected_user_type;
	var selected_user_id;
	var selected_report_id;
	var selected_from_date;
	var selected_to_date;
	var selected_sent_nature;
	var selected_weekday;
	var selected_status;
	
		$.post( "reports_schedule.php", { rpt_id:rpt_id }).done(function( data )  { arr = JSON.parse(data);
		//console.log(arr); //alert(data); console.log(arr);
		 $.each(arr, function( key, value ) { 
			rpt_schedule_id = arr['rpt_schedule_id'];
		 	selected_user_type = arr['user_type'];
			selected_user_id = arr['user_id']; 
			selected_report_id = arr['report_id'];
			selected_from_date = arr['from_date']; 
			selected_to_date = arr['to_date']; 
			selected_sent_nature = arr['sent_nature']; 
			selected_weekday = arr['weekday']; 
			additional_emails = arr['additional_emails'];
			selected_status = arr['status'];

			}); 
			
			
			// Load user type menu
			loadUserType(selected_user_type);  
			loadURL(selected_user_type, 'single', selected_user_id);
			//if($('#skipfunction').val()!='skip')
			loaddata('', selected_user_type,'checkall',selected_report_id);
			
			// Default load values
			$('#rpt_schedule_id').val(rpt_schedule_id);
			$('#selected_user_type').val(selected_user_type);
			$('#selected_user_id').val(selected_user_id);
			$('#status').val(selected_status);
			
			/*$('#selected_report_id').val(selected_report_id);*/
			$('#from').val(selected_from_date);
			$('#to').val(selected_to_date);
			$('#additional_emails').val(additional_emails);
			// Default load values
			
			 if($.isNumeric(selected_sent_nature) == true && selected_sent_nature==2) { 
			 	$('select[name="weekday"]').addClass('required'); 
				$('#weekly').show();
				// nth(0) = weekly AND nth(1) = daily
				$('input:radio[name=sent_nature]:nth(0)').attr('checked',true);
				$("input[name=weekday],option[value="+selected_weekday+"]").attr('selected','selected'); 
			} else { $('input:radio[name=sent_nature]:nth(1)').attr('checked',true); $('#weekly').hide(); }
			
		 });
		} else {
		// Load default user type menu
		loadUserType();
		$('input:radio[name=sent_nature]:nth(1)').attr('checked',true);
		$('select[name="weekday"]').find('option').removeAttr('selected'); 
		$('select[name="weekday"]').removeClass('required'); 
		$('#weekly').hide(); 
	}
	
 }

   
// Post request for user type
function loadUserType(type) {  
	//alert('Function');
	//alert(type);
	if(type!='' && typeof(type)!='undefined') { $.post( "reports_schedule.php", { getuserType:type }).done(function( data ) { $('#user_type').html(data) });  
	} else {
		$.post( "reports_schedule.php", { defaultValue:"default" }).done(function( data ) { $('#user_type').html(data) });  
	}
}



// Post Request while select users
function loadURL(type, defaultselected, selectedvalue) {  
	//alert(type);
	//alert(defaultselected);
	//alert(selectedvalue);

	if(type!='') { $.post( "reports_schedule.php", { userType:type, defaultselected:defaultselected, selectedvalue:selectedvalue}).done(function( data ) { //console.log(data); 
	$('#user_id').html(data) });  
	}

	
	if(selectedvalue == '') { 
		loaddata('', $('#user_type').val(),'checkall','');
		setDefaultValue();
	}
}

// Post Request while select reports
function loaddata(user_id, type,defaultselected, selectedvalue) { 
//alert(user_id);
//alert(type);
//alert(defaultselected);
//alert(selectedvalue);
	var rpt_id;
	if(type!='')  
	{ $.post( "reports_schedule.php", { user_id:user_id, type:type, defaultselected:defaultselected, selectedvalue:selectedvalue }).done(function( result )  { resultarr = JSON.parse(result); var rpt_id = resultarr[0]; skipfunction=resultarr[1];
	if($.isNumeric(rpt_id) == true){
	//alert(true); 
	showDetails(rpt_id); 
	} else { 
	//alert(false);
	$('#report_id').html(resultarr[2]); 
	}
	
});   
}
	
	var utype = $('#selected_user_type').val(); 
	var uid = $('#user_id').val();
	var suid = $('#selected_user_id').val();
	//alert(uid);
	//alert(suid);
	
	if(uid != suid) { setDefaultValue(); }
	
}


function setDefaultValue() {
		//alert('Hi');
		sessionStorage.removeItem("rpt_id");
		var today = new Date();
		var date = today.getFullYear() + '-' + ('0' + (today.getMonth()+1)).slice(-2) + '-' + ('0' + today.getDate()).slice(-2);
		$('#rpt_schedule_id').val('');
		$('#report_id').html();
		$('#from').val(date); 
		$('#to').val(date); 
		$('#additional_emails').val('');
		
		$('input:radio[name=sent_nature]:nth(1)').attr('checked',true);
		$('select[name="weekday"]').find('option').removeAttr('selected'); 
		$('select[name="weekday"]').removeClass('required'); 
		$('#weekly').hide(); 
		
}