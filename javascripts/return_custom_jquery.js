  /**********************************************************
  * DESC : Select all the checkbox
  * Author : AJAY
  * Created on : 28 May 2015
  */

  function select_all() {
  $('input[class=case]:checkbox').each(function(){ 
    if($('input[class=check_all]:checkbox:checked').length == 0){ 
      $(this).prop("checked", false); 
    } else {
      $(this).prop("checked", true); 
    } 
  });
}

/**********************************************************
  * DESC : form validation without item name filled we can not moved to next step
  * Author : NIZAM
  * Created on : 4 May 2015
  */






  /**********************************************************
  * DESC : Add new row of item with select & search option
  * Author : AJAY
  * Created on : 28 May 2015
  */



  $(document).ready(function() {
    // body...




    var i=$('table #order tr').length;
    //alert(i);
      

      $(".addmore").on('click',function(){
       
       count=$('table #order tr').length;
      //alert(count);
      
        var data="<tr><td><input type='checkbox' class='case'/></td>";
          data+="<td><span id='snum"+i+"'>"+count+".</span></td>";
          data+="<td><input class='form-control autocomplete_txt item_list' type='text' data-type='itemcode' id='itemcode_"+i+"' name='itemcode[]' autocomplete='off'/><input class='form-control autocomplete_txt lastsaveditemids' type='hidden' data-type='item_id' id='item_id_"+i+"' name='item_id[]'></td>";
          data+="<td><input class='form-control autocomplete_txt' type='text' readonly data-type='item_name' id='item_name_"+i+"' name='item_name[]' autocomplete='off'/></td>";
          data+="<td id='qty_"+i+"'></td>";
          
          var k= parseInt(i-1);
          var item_code=$('#itemcode_'+k).val();
          if(!item_code)
           {
            $('#itemcode_'+k).css('border-color', 'red');
            return false;
           }
          
      

      $('table #order').append(data);
      row = i ;
      i++;
    });






  /**********************************************************
  * DESC : Deleted the selcted rows of where class name deletes
  * Author : AJAY
  * Created on : 28 May 2015
  */
      
       $(".deletes").on('click', function() {
        $('.case:checkbox:checked').parents("table #order tr").remove();
        $('.check_all').prop("checked", false); 
        check();
      });

  });



  /**********************************************************
  * DESC : Re assign the row number after deleted any row or rows
  * Author : AJAY
  * Created on : 28 May 2015
  */


    function check(){
      obj=$('table #order tr td').find('span');
      $.each( obj, function( key, value ) {
        //alert(key);
        id=value.id;
        $('#'+id).html(key+1);
      });
    }





  /**********************************************************
  * DESC : select and search of retailer list
  * Author : AJAY
  * Created on : 28 May 2015
  */




  $(document).on('focus','#retailer_name', function(){



       
      $(this).autocomplete({

          source: function( request, response ) {
            //alert('k');
              $.ajax({
                url : 'ajax_retailerlist.php',
                dataType: "json",
              data: {
                 name_startsWith: request.term,
                 type: 'retailers',
                 row_num : 1
              },
               success: function( data ) {
                //alert(data);
                //console.log(data);
                 response( $.map( data, function( item ) {
                  var code = item.split("|");
                  return {
                    label: code[0],
                    value: code[0],
                    data : item
                  }
                }));
              }
              });
            },
            autoFocus: true,          
            minLength: 0,
            select: function( event, ui ) {
             var names = ui.item.data.split("|");           
             id_arr = $(this).attr('id');
             //id = id_arr.split("_");
             //elementId = id[id.length-1]; 
            //alert(names[0]);       
            $('#retailer_name').val(names[0]);
            $('#retailer_id').val(names[1]);
          }           
        });

     });





  /**********************************************************
  * DESC : Autocomplete item list
  * Author : AJAY
  * Created on : 28 May 2015
  */


    $(document).on('focus','.item_list',function(){



      type = $(this).data('type');

      //var data_to_send = $.serialize(info);

      
      if(type =='itemcode' )autoTypeNo=0;
      if(type =='item_name' )autoTypeNo=1; 
      if(type =='item_id' )autoTypeNo=2; 
      //if(type =='country_code' )autoTypeNo=3; 
      
       $(this).autocomplete({
           minLength: 0,
           source: function( request, response ) {



            //$.each( request, function( key, value ) {
              //alert( key + ": " + value );
           // });


            //alert(request.term);
            $.ajax({
                url : 'ajax_itemlist.php',
                dataType: "json",
              data: {
                 name_startsWith: request.term,
                 type: 'items',
                 row_num : 1
              },
               success: function( data ) {

                 //alert(data);
                //console.log(data);

                var array = $.map(data, function (item) {
                   var code = item.split("|");
                   return {
                       label: code[autoTypeNo],
                       value: code[autoTypeNo],
                       data : item
                   }
               });
               //call the filter here
               response($.ui.autocomplete.filter(array, request.term));
                /* response( $.map( data, function( item ) {
                  var code = item.split("|");
                  return {
                    label: code[0],
                    value: code[0],
                    data : item
                  }
                }));*/
              }
              });

               
               
           },
           focus: function() {
             // prevent value inserted on focus
             return false;
           },
           select: function( event, ui ) {
           
            var names       = ui.item.data.split("|");         
                id_arr      = $(this).attr('id');
                id          = id_arr.split("_");
                elementId   = id[id.length-1];
             //alert(elementId);
            $('#itemcode_'+elementId).val(names[0]);
            $('#item_name_'+elementId).val(names[1]);
            $('#item_id_'+elementId).val(names[2]);

            //alert(names[2]);
            


            /*alert(str);
      alert(id);*/
  $.ajax({
    'type': 'POST',
    'url': 'cases_sizes.php',
    'data': 's='+names[2],
    'success' : function(mystring) {
    //alert(mystring);
   // document.getElementById('qty_'+names[2]).innerHTML = mystring;
   $('#qty_'+elementId).html(mystring);
    },
    error: function (xhr, ajaxOptions, thrownError) {
        alert(xhr.status);
        alert(thrownError);
      }
    });


   }
       });
       
       $.ui.autocomplete.filter = function (array, term) {
            var matcher = new RegExp("^" + $.ui.autocomplete.escapeRegex(term), "i");
            return $.grep(array, function (value) {
                return matcher.test(value.label || value.value || value);
            });
       };
    });






  /**********************************************************
  * DESC : Datepicker
  * Author : AJAY
  * Created on : 28 May 2015
  */





  $(function() {
    $( "#datepicker" ).datepicker({
     // showOn: "button",
     // buttonImage: "images/calendar.gif",
      //buttonImageOnly: true,
      //buttonText: "Select date",
      dateFormat: "d M yy",
      defaultDate: "w",
      changeMonth: true,
      numberOfMonths: 1
    });
  });


