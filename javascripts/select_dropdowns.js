$(document).ready(function()
{
	$("#country_id").change(function()
	{
	    var selected_country = $("#country_id").val();
    	$.ajax({
	    	url: "add_state_city_ajax.php",
	    	type: "post",
	    	data: {selected_country:selected_country,type:'country'},
	    	DataType: 'text',
	     	success: function(result){
	     		$(".region_id").html(result);
	     		console.log(result);
	    }});
	});
	$(".region_id").change(function()
	{
	    var selected_region = $(".region_id").val();
	    $.ajax({
	    	url: "add_state_city_ajax.php",
	    	type: "post",
	    	data: {selected_region:selected_region,type:'region'},
	     	success: function(result){
	        	$(".zone_id").html(result);
	    }});
	});
	$(".zone_id").change(function()
	{
	    var selected_zone = $(".zone_id").val();
	    $.ajax({
	    	url: "add_state_city_ajax.php",
	    	type: "post",
	    	data: {selected_zone:selected_zone,type:'zone'},
	     	success: function(result){
	        	$(".state_id").html(result);
	    }});
	});
	$(".state_id").change(function()
	{
	    var selected_state = $(".state_id").val();
	    $.ajax({
	    	url: "add_state_city_ajax.php",
	    	type: "post",
	    	data: {selected_state:selected_state,type:'state'},
	     	success: function(result){
	        	$(".city_id").html(result);
	        	$("#city_id").html(result); // AJAY@2017-05-03
	    }});
	});
	$(".city_id").change(function(){
		var selected_city = $(".city_id").val();
		$.ajax({
			url: "add_state_city_ajax.php",
			type: "post",
			data: {selected_city:selected_city,type:'city'},
			success: function(result){
				$(".area_id").html(result);
			}
		});
	});

	// AJAY@2017-05-03 (Added cause of show all the areas/districts/location while selecting city from add new service distributor and mapped with sd)
	$(".district_city_location").change(function(){
		var selected_state = $("#state_id").val();
		$.ajax({
			url: "add_state_city_ajax.php",
			type: "post",
			data: {selected_state:selected_state,type:'getAllArea'},
			success: function(result){
				$(".area_id").html(result);
			}
		});
	});


	// AJAY@2017-05-03 (Added cause of show all the areas/districts/location while selecting city from add new service distributor and mapped with sd)
	$("#market_id").change(function(){
		var selected_city = $("#market_id").val();
		$.ajax({
			url: "add_state_city_ajax.php",
			type: "post",
			data: {selected_city:selected_city,type:'getServiceDistributor'},
			success: function(result){
				$("#service_distributor_id").html(result);
			}
		});
	});




$("#invoice_id").change(function(){
		var selected_invoice = $("#invoice_id").val();

		$.ajax({
			url: "add_state_city_ajax.php",
			type: "post",
			data: {selected_invoice:selected_invoice,type:'invoice_change'},
			success: function(result){
				$("#date_id").html(result);
			}
		});
	});







$("#product_serial_no").blur(function(){
		var selected_bsn = $("#product_serial_no").val();
		//alert(selected_bsn);

		if(selected_bsn!='') {

			$.ajax({
				url: "add_state_city_ajax.php",
				type: "post",
				data: {selected_bsn:selected_bsn,type:'bsn'},
				success: function(result){
				
				$('#datepicker_id').val(result);
				}
			});

		}

	});

// Get Complete Detail of BSN AJAY@2017-05-04
function call_me()
{
	alert("Called");
}

$("#bsn_no").blur(function(){
		var selected_bsn = $("#bsn_no").val();
		//alert(selected_bsn);

		if(selected_bsn!='') {
			$('#bsnStatus').html('');
			$.ajax({
				url: "add_state_city_ajax.php",
				type: "post",
				data: {selected_bsn:selected_bsn,type:'bsnDetail'},
				success: function(resultData){
				   var data = $.parseJSON(resultData);

		              if(data.status == 'success') {
						  console.log(data.data);
						  $("#bsn_no").prop("readonly","readonly");
		                 $.each( data.data, function( key, value ) {
						       //alert(value);
						        // console.log("key:"+key+"&Value:"+value);
						        var str = "";
						       //$('#'+key).val(value);

						       if(key == 'country_id' || key == 'region_id' || key == 'zone_id' || key == 'state_id' || key == 'city_id')
						       {
						       	var res = value.split("Name=");
						       	 str += "<option value='"+res[0]+"'>"+res[1]+"</option>";

						       	  $('#'+key).html(str);
						       	  $('#'+key).val(res[0]);
						       	  $('#'+key).prop("readonly","readonly");
						       }
						       else
						       {	
						       		if(value!=null)
						       		{
						       			$('#'+key).val(value);
						       			$('.'+key).val(value);
						       			$('#'+key).prop("readonly","readonly");	
						       		}
						       		
						       }


						       // if(key == 'purchased_from_type') {
						       // 	$('#purchased_from_type[value==]').
						       // }
						  });

		                 $('#bsnStatus').html(data.msg);
		              } else {
		              	  var data = $.parseJSON(resultData);
						$.each( data.data, function( key, value ) {
						       //alert(value);
						       $('#'+key).val(value);

						       // if(key == 'purchased_from_type') {
						       // 	$('#purchased_from_type[value==]').
						       // }
						  });



		              	 $('#bsnStatus').html(data.msg);
		              }
				}
			});

		}

	});


$("#stateid").change(function(){
		var wearhouse_name = $("#stateid").val();

		$.ajax({
			url: "add_state_city_ajax.php",
			type: "post",
			data: {wearhouse_name:wearhouse_name,type:'w_name'},
			success: function(result){
			//	$("#datepicker_id").html(result);
			$('#warehouse_id').html(result);
			}
		});
	});


	$("#segment_id").change(function(){
		
		var id = $("#segment_id").val();
		if(id > 0 )
		{

			$('.segment').hide();
			$(".segment_type_"+id).show();
			$(".manufacturer_"+id).show();
			$(".model_"+id).show();
			$(".capacity_"+id).show();
			$(".watts_"+id).show();
			$(".max_speed_"+id).show();
			$(".disCovrPerChrg_"+id).show();
			$(".load_carrying_capacity_"+id).show();
			$(".system_voltage_"+id).show();
			$(".type_of_start_"+id).show();
			$(".rating_"+id).show();
			$(".five_hrs_model_"+id).show();
			$(".four_hrs_model_"+id).show();
			$(".three_hrs_model_"+id).show();
			$(".two_hrs_model_"+id).show();

		}else{
				$('.segment').show();

			}
	});




	
$("#id_city").change(function(){
		var dealer_name = $("#id_city").val();

		$.ajax({
			url: "add_state_city_ajax.php",
			type: "post",
			data: {dealer_name:dealer_name,type:'dealer_name'},
			success: function(result){
			//	$("#datepicker_id").html(result);
			$('#dealer_id').html(result);
			}
		});
	});


$("#id_state").change(function()
	{
	    var selected_state = $("#id_state").val();
	    $.ajax({
	    	url: "add_state_city_ajax.php",
	    	type: "post",
	    	data: {selected_state:selected_state,type:'id_state'},
	     	success: function(result){
	        	$("#id_city").html(result);
	    }});
	});


	
});

$("#total_charging_duration_days").focus(function(){
		var start_date = $("#charging_start_date").val();
		var removal_date = $("#charging_removal_date").val();
		var start_time = $("#charging_start_time").val();
		var removal_time = $("#charging_removal_time").val();
		if(start_date !='' && removal_date !='' && start_time !='' && removal_time !='')
		{
			$.ajax({
			  method: "POST",
			  url: "add_state_city_ajax.php",
			  data: { start_date:start_date,removal_date:removal_date,start_time:start_time,removal_time:removal_time, requestType: "calculate_time" },
			  success:function(result) {
			  		var res = result.split("hours=");
			  		$("#total_charging_duration_days").val(res[0]);
			  		$("#total_charging_duration_hours").val(res[1]);
			  		$("#total_charging_duration_days").prop("readonly","readonly");
			  		$("#total_charging_duration_hours").prop("readonly","readonly");
			  	}
			});
		}
		else
		{
			alert("Please provide all details");
		}
		
	});
