$(document).ready(function()
{
	$("#country_id").change(function()
	{
	    var selected_country = $("#country_id").val();
    	$.ajax({
	    	url: "add_state_city_ajax_call_center.php",
	    	type: "post",
	    	data: {selected_country:selected_country,type:'country'},
	    	DataType: 'text',
	     	success: function(result){
	     		$(".region_id").html(result);
	    }});
	});
	$(".region_id").change(function()
	{
	    var selected_region = $(".region_id").val();
	    $.ajax({
	    	url: "add_state_city_ajax_call_center.php",
	    	type: "post",
	    	data: {selected_region:selected_region,type:'region'},
	     	success: function(result){
	        	$(".zone_id").html(result);
	    }});
	});
	$(".zone_id").change(function()
	{
	    var selected_zone = $(".zone_id").val();
	    $.ajax({
	    	url: "add_state_city_ajax_call_center.php",
	    	type: "post",
	    	data: {selected_zone:selected_zone,type:'zone'},
	     	success: function(result){
	        	$(".state_id").html(result);
	    }});
	});
	$(".state_id").change(function()
	{
	    var selected_state = $(".state_id").val();
	    $.ajax({
	    	url: "add_state_city_ajax_call_center.php",
	    	type: "post",
	    	data: {selected_state:selected_state,type:'state'},
	     	success: function(result){
	        	$(".city_id").html(result);
	        	$("#city_id").html(result); // AJAY@2017-05-03
	    }});
	});
	$(".city_id").change(function(){
		var selected_district = $(".city_id").val();
		$.ajax({
			url: "add_state_city_ajax_call_center.php",
			type: "post",
			data: {selected_district:selected_district,type:'getServiceDistributor'},
			success: function(result){
				$(".service_distributor_id").html(result);

			}
		});
	});



	




$("#invoice_id").change(function(){
		var selected_invoice = $("#invoice_id").val();

		$.ajax({
			url: "add_state_city_ajax_call_center.php",
			type: "post",
			data: {selected_invoice:selected_invoice,type:'invoice_change'},
			success: function(result){
				$("#date_id").html(result);
			}
		});
	});







$("#product_serial_no").blur(function(){
		var selected_bsn = $("#product_serial_no").val();
		//alert(selected_bsn);

		if(selected_bsn!='') {

			$.ajax({
				url: "add_state_city_ajax_call_center.php",
				type: "post",
				data: {selected_bsn:selected_bsn,type:'bsn'},
				success: function(result){
				
				$('#datepicker_id').val(result);
				}
			});

		}

	});

// Get Complete Detail of BSN AJAY@2017-05-04

$("#search_bsn").click(function(){
		var selected_bsn = $("#bsn_no").val();
		var purchase_date = $("#datepicker_id").val();
		var new_old = $('input:radio[name=choose_bsn]:checked').val();
		if(purchase_date == ' ')
		{
			alert("Please select Purchase Date First");
			return false;
		}
		//alert(selected_bsn);

		if(selected_bsn!='') {
			$('#bsnStatus').html('');
			$.ajax({
				url: "add_state_city_ajax_call_center.php",
				type: "post",
				data: {selected_bsn:selected_bsn,purchase_date:purchase_date,type:'bsnDetail',new_old:new_old},
				success: function(resultData){
				   var data = $.parseJSON(resultData);

		              if(data.status == 'success') {
						  console.log(data.data.segment_name);
						  $('#track_bsn').removeClass("hidden");
		                 $.each( data.data, function( key, value ) {
						       //alert(value);
						       $('#'+key).val(value);

						       // if(key == 'purchased_from_type') {
						       // 	$('#purchased_from_type[value==]').
						       // }
						  });

		                 $('#bsnStatus').html(data.msg);
		              } else {
		              	  var data = $.parseJSON(resultData);
						$.each( data.data, function( key, value ) {
						       //alert(value);
						       $('#'+key).val(value);

						       // if(key == 'purchased_from_type') {
						       // 	$('#purchased_from_type[value==]').
						       // }
						  });



		              	 $('#bsnStatus').html(data.msg);
		              }
				}
			});

		}

	});



$("#stateid").change(function(){
		var wearhouse_name = $("#stateid").val();

		$.ajax({
			url: "add_state_city_ajax_call_center.php",
			type: "post",
			data: {wearhouse_name:wearhouse_name,type:'w_name'},
			success: function(result){
			//	$("#datepicker_id").html(result);
			$('#warehouse_id').html(result);
			}
		});
	});


	$("#segment_id").change(function(){
		
		var id = $("#segment_id").val();
		if(id > 0 )
		{

			$('.segment').hide();
			$(".segment_type_"+id).show();
			$(".manufacturer_"+id).show();
			$(".model_"+id).show();
			$(".capacity_"+id).show();
			$(".watts_"+id).show();
			$(".max_speed_"+id).show();
			$(".disCovrPerChrg_"+id).show();
			$(".load_carrying_capacity_"+id).show();
			$(".system_voltage_"+id).show();
			$(".type_of_start_"+id).show();
			$(".rating_"+id).show();
			$(".five_hrs_model_"+id).show();
			$(".four_hrs_model_"+id).show();
			$(".three_hrs_model_"+id).show();
			$(".two_hrs_model_"+id).show();

		}else{
				$('.segment').show();

			}
	});

	$("#service_distributor_id").change(function(){
		var selected_distributor = $("#service_distributor_id").val();
		$.ajax({
			url: "add_state_city_ajax_call_center.php",
			type: "post",
			data: {selected_distributor:selected_distributor,type:'service_distributor'},
			success: function(result){
				var resultant = result.split('andname=');
				$("#asd_contact").val(resultant[1]);
				$("#asd_email").val(resultant[0]);
			}
		});
	});

	$("#datepicker_id").change(function(){
		var selected_date = $("#datepicker_id").val();
		var selected_bsn = $("#bsn_no").val();
		$.ajax({
	    	url: "add_state_city_ajax_call_center.php",
	    	type: "post",
	    	data: {selected_date:selected_date,selected_bsn:selected_bsn,type:'warranty_balance'},
	    	DataType: 'text',
	     	success: function(result)
	     	{
			   var resultant = result.split('-()-');
			   $("#balance_item_warranty").val(resultant[0]);
				$("#balance_item_prodata").val(resultant[1]);
	    	}});
		});
	
	});
