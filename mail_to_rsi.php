<?php 
	include("includes/config.inc.php");
	include("includes/function.php");
	$page_name="Segment";
	$_objAdmin = new Admin();
	$tat = "72";
	$hierarchy_id = 9;	//Hierarchy ID of RSI is 9
	$query = $_objAdmin->_getSelectList2('table_complaint as COMPLAINT left outer join table_service_distributor_sp_bcf as BCF on COMPLAINT.complaint_id = BCF.complaint_id','COMPLAINT.*','',' BCF.service_distributor_sp_bcf_id IS NULL and COMPLAINT.reminder = ""' );
	if(count($query)>0)
	{
		for($i=0;$i<count($query);$i++)
		{
			$date1 = date('Y-m-d H:i:s');
			$date2 = date('Y-m-d H:i:s',strtotime($query[$i]->created_date));
			$tat_in_hours = round((strtotime($date1) - strtotime($date2))/(60*60));
			if($tat_in_hours >= $tat)
			{
				$fetch_RSI = $_objAdmin->_getSelectList2('table_service_distributor as SD left join table_complaint as COMPLAINT on COMPLAINT.service_distributor_id = SD.service_distributor_id left join table_service_personnel_warehouse_mapping as TSPWM on TSPWM.warehouse_id = SD.warehouse_id left join table_service_personnel as SP on SP.service_personnel_id = TSPWM.service_personnel_id left join table_service_personnel_hierarchy_relationship as SPHR on SPHR.service_personnel_id = SP.service_personnel_id left join table_web_users as web on web.service_personnel_id = SPHR.service_personnel_id left join table_customer as CUST on CUST.customer_id = COMPLAINT.customer_id','SD.warehouse_id,SP.sp_name,web.email_id,COMPLAINT.complaint_no,CUST.customer_name,CUST.customer_phone_no,CUST.customer_address,COMPLAINT.product_serial_no,SD.service_distributor_name,SD.service_distributor_phone_no','',' COMPLAINT.complaint_id="'.$query[$i]->complaint_id.'" and SPHR.hierarchy_id="'.$hierarchy_id.'"');
				$to = $fetch_RSI[0]->email_id;
				$subject = "Complaint No. ".$fetch_RSI[0]->complaint_no." is pending.";
				$matter = "Complaint No <b>".$fetch_RSI[0]->complaint_no."</b> is pending to be served. The customer details for the following is:-";
				$salutation = "Dear ". $fetch_RSI[0]->sp_name.",";
				$message = "<html><body><p>".$salutation."</p><table><tr><p>".$matter."</p></tr><tr><td>Battery Serial No : </td><td><b>".$fetch_RSI[0]->product_serial_no."</b></td></tr><tr><td>Customer Name: </td><td><b>".$fetch_RSI[0]->customer_name."</b></td></tr><tr><td>Phone No: </td><td><b>".$fetch_RSI[0]->customer_phone_no."</b></td></tr><tr><td>Address : </td><td><b>".$fetch_RSI[0]->customer_address."</b></td></tr><tr><td>ASD Name : </td><td><b>".$fetch_RSI[0]->service_distributor_name."</b></td></tr><tr><td>ASD Phone No : </td><td><b>".$fetch_RSI[0]->service_distributor_phone_no."</b></td></tr></table></body></html>";
				// echo $message;
				$headers = "MIME-Version: 1.0" . "\r\n";
				$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
				$headers .= 'From: <info@eastman.com>' . "\r\n";
				//mail($to,$subject,$message,$headers);
			}
			else
			{
				
			}		
		}	
	}
?>