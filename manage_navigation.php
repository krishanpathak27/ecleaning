<?php
include("includes/config.inc.php");
include("includes/function.php");
$page_name="Manage Navigation";
$_objAdmin = new Admin();
$_objItem = new Item();
$objArrayList= new ArrayList();	
$pageAccess=1;

?>

<?php 
include("header.inc.php");
$check=$objArrayList->checkAccess($pageAccess, basename($_SERVER['PHP_SELF']));
if($check == false){ header('Location: ' . $_SERVER['HTTP_REFERER']); }
include("manage_navigation.inc.php") ;
?>
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <!-- BEGIN: Subheader -->
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">
                    Navigation
                </h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="#" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__separator">
                        -
                    </li>
                    <li class="m-nav__item">
                        <a href="#" class="m-nav__link">
                            <span class="m-nav__link-text">
                                User
                            </span>
                        </a>
                    </li>
                    <li class="m-nav__separator">
                        -
                    </li>
                    <li class="m-nav__item">
                        <a href="#" class="m-nav__link">
                            <span class="m-nav__link-text">
                                Navigation
                            </span>
                        </a>
                    </li>  
                </ul>
            </div> 
        </div>
    </div>
    <!-- END: Subheader -->
    <div class="m-content">
        <div class="row">
            <div class="col-lg-12">
                <!--begin::Portlet--> 
                <div class="m-portlet">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <span class="m-portlet__head-icon m--hide">
                                    <i class="la la-gear"></i>
                                </span>
                                <h3 class="m-portlet__head-text">
                                    Navigation Form
                                </h3>
                            </div>
                        </div>
                    </div>
                    <!--begin::Form-->
                    <form class="m-form m-form--fit m-form--label-align-right" id="frmPre"  method="post" action="<?php $_SERVER['PHP_SELF'] ?>" name="frmPre" enctype="multipart/form-data">

                        <div class="m-portlet__body">
                            <div class="m-form__content">
                                <div class="m-alert m-alert--icon alert alert-danger m--hide" role="alert" id="m_form_1_msg">
                                    <div class="m-alert__icon">
                                        <i class="la la-warning"></i>
                                    </div>
                                    <div class="m-alert__text">
                                        Oh snap! Change a few things up and try submitting again.
                                    </div>
                                    <div class="m-alert__close">
                                        <button type="button" class="close" data-close="alert" aria-label="Close"></button>
                                    </div>
                                </div>
                            </div>
                            <?php if (isset($user_name_err) && !empty($user_name_err)) { ?>
                                <div class="m-form__content">
                                    <div class="m-alert m-alert--icon alert alert-danger" role="alert" id="m_form_1_msg1">
                                        <div class="m-alert__icon">
                                            <i class="la la-warning"></i>
                                        </div>
                                        <div class="m-alert__text">
                                            <?php echo $user_name_err; ?>
                                        </div>
                                        <div class="m-alert__close">
                                            <button type="button" class="close" data-close="alert" aria-label="Close"></button>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                            <?php if(isset($man_add_sus) && !empty($man_add_sus)){ ?>
                            <div role="alert" style="background: #d7fbdc;" class="m-alert  m-alert--icon m-alert--air m-alert--square alert alert-dismissible  m--margin-bottom-30">
				                <div class="m-alert__icon" >
				                        <i class="flaticon-exclamation m--font-brand"></i>
				                </div>
				                <div class="m-alert__text">
				                       <?php echo  $man_add_sus; ?>
				                </div>
			            	</div>
			            	<?php } ?>

                            <div class="form-group m-form__group row">
                                <div class="col-lg-4">
                                    <label>
                                        User Type:
                                    </label>
                                    <div class="input-group m-input-group m-input-group--square"> 
									 <select id="user_type" name="user_type" class="styledselect_form_3 form-control required" style="padding:0px;" onchange="showParent(this.value)">
						<option value="">Please Select</option>
						<?php  $usertype = $_objItem->_getSelectList2('table_user_type',"id, type, parentType,hierarchy_id",''," account_id='".$_SESSION['accountId']."' OR account_id='' AND status='A'");
							$GetuserType = " nav_privilege_id='".$_GET['id']."'";		
							$pageGroup=$_objAdmin->_getselectList2('table_navigation_privilege','*',''," $GetuserType ");
							foreach($usertype as $value){
								if($value->parentType!=1){?>
									<option value="<?php echo $value->id;?>" title="<?php echo $value->parentType;?>" <?php if($value->parentType == $pageGroup[0]->user_type && $value->hierarchy_id == $pageGroup[0]->hierarchy_id){?> selected="selected"<?php }?>><?php echo $value->type;?></option>
								<?php } ?>
							<?php }?>
						</select>







                                    </div> 

                                </div>
                                <div class="col-lg-4">
                                    <label>
                                        Access Control:
                                    </label>
                                     <div class="m-input-icon m-input-icon--right">
                                      	<label class="checkbox-inline">
                                     		<input type="checkbox" id="checkall" class="checkboxFilter checkBoxClassStyle" name="checkall"/>Check All
                                     	</label>
                                     </div>
                                </div>
                            </div>
                             <!-- ALL CMS Navigation Page List -->
                             




  				 <?php 
					 if($_GET['id']!=''){
					 $userType = " account_id='".$_SESSION['accountId']."' and user_type=''";		
					 $pageGroup=$_objAdmin->_getselectList2('table_navigation_privilege','*',''," $userType ");
					  $pageGroupNew=$_objAdmin->_getselectList2('table_navigation_privilege','*',''," nav_privilege_id='".$_GET['id']."' ");
		 			 $pageID = $pageGroup[0]->page_privileges;
					 $user_type = $pageGroupNew[0]->user_type;
					 $UserID = $pageGroupNew[0]->user_id;
					// echo $UserID;
					 if($user_type!='' || $UserID!=''){
					 	 $pageGroup=$_objAdmin->_getselectList2('table_navigation_privilege','*',''," nav_privilege_id='".$_GET['id']."' ");
							 $CheckpageID = $pageGroup[0]->page_privileges;
						 	 $arrayIDs = explode(',',$CheckpageID);
							// echo $arrayIDs;
					 }
					 
					 //echo $pageID;
					 if($pageID!=''){
					 	$checkPageID="id IN ($pageID) AND";
					
					 }			 		  
		  			}?>
		  			<div class="form-group m-form__group row">
															
						<div class="col-lg-6">
						<?php $pages = $_objItem->_getSelectList2('table_cms','id, parentPage, page_alias_name',''," $checkPageID
						  			 (parentPage IS NULL OR parentPage = 0) AND status='A' ORDER BY ordID ASC"); 
							if(is_array($pages)){
								foreach($pages as $value){   $masterID = $value->id; ?>

								<div  class="alert alert-success"> 

									<label class="checkbox-inline">
									<input type="checkbox" id="master<?php echo $value->id;?>" onclick="selectMasterChild(this.value);"  class="checkBoxClass master<?php echo $value->id;?> checkBoxClassStyle" name="privilege_pages[]"  value="<?php echo $value->id;?>" <?php if(in_array($value->id,$arrayIDs)){?> checked="checked"<?php }?> /><strong><?php echo $value->page_alias_name;?></strong>
									</label>
									</div>
								




									 <?php $subpages=$_objItem->_getSelectList2('table_cms','id, page_name, parentPage, page_alias_name',''," $checkPageID	parentPage = '".$value->id."' AND status='A' ORDER BY ordID ASC"); 

									    	if(is_array($subpages)){
									    		foreach($subpages as $value){ $childID = $value->id; ?>
									    			<div  class="m-alert m-alert--outline alert alert-success alert-dismissible fade show"> 
														<div class="col col-md-offset-1">
															 <label class="checkbox-inline">
															 	<input type="checkbox" id="child<?php echo $value->id;?>" class="checkBoxClass child<?php echo $masterID;?>" onclick="selectMaster1(this.value, <?php echo $masterID;?>);  checkallval(<?php echo $masterID; ?>);" name="privilege_pages[]" value="<?php echo $value->id;?>" <?php if(in_array($value->id,$arrayIDs)){?> checked="checked"<?php }?> /><i><?php echo $value->page_alias_name;?></i>
																</label>
															</div>
															</div>
														





														<?php $subpages=$_objItem->_getSelectList2('table_cms','id, page_name, parentPage, page_alias_name',''," $checkPageID parentPage = '".$value->id."' AND status='A' ORDER BY ordID ASC"); 
														if(is_array($subpages)) {
															foreach($subpages as $value) {?>

														<div  class="m-alert m-alert--outline alert alert-success alert-dismissible fade show"> 
																<div class="col col-md-offset-2">
																	 <label class="checkbox-inline">
																	 	<input type="checkbox" onclick="CheckchildMaster(this.value, <?php echo $childID;?>, <?php echo $masterID;?>);" id="subchild<?php echo $value->id;?>" class="checkBoxClass subchild<?php echo $childID;?> child<?php echo $masterID;?>" name="privilege_pages[]"  value="<?php echo $value->id;?>"  <?php if(in_array($value->id,$arrayIDs)){?> checked="checked"<?php }?> /><?php echo $value->page_alias_name;?>
																		</label>
																	</div>
																</div>

															<?php }?>
													<?php } ?>
							 				<?php }?>

										<?php } ?>






								<?php } ?>
						<?php } ?>
																
					</div>
                            
                           
                        </div>
                        <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                            <div class="m-form__actions m-form__actions--solid">
                                <div class="row">
                                    <div class="col-lg-4"></div>
                                    <div class="col-lg-8">
                                        <button type="submit" class="btn btn-success" value="Save & Continue">
                                            Submit
                                        </button>
                                        <button type="reset" class="btn btn-secondary">
                                            Reset
                                        </button>
                                        <a href="supervisor.php">
                                        <button class="btn btn-secondary" onclick="location.href='user_navigation_list.php';">
                                            Back
                                        </button>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                       <input name="manage_user_add" type="hidden" value="yes" />
					<input name="man_nav_id" type="hidden" value="<?php echo $_GET['id']; ?>" />
					<input name="account_id" type="hidden" value="<?php echo $_SESSION['accountId']; ?>" />

                    </form>
                    <!--end::Form-->
                </div>
                <!--end::Portlet-->
            </div>
        </div>
    </div>
</div>
</div>
</div>
<!-- start footer -->         
<?php include("footer.php") ?>

<script type="text/javascript">
	function showParent(id) {
		//alert(id);
		var PID = jQuery('#user_type').find("option:selected").attr("title");
		//alert(PID);
		if(typeof PID != 'undefined') { 
		//alert(PID);
		jQuery.ajax({
			url: "gethierarchylistmenu.php",
			data: "pid="+PID+"&id="+id,
			success: function(data){
				//alert(data);
				jQuery('#parentType').html(data);
			}
		});
		} else {
			jQuery('#parentType').html('');
		}


		if(id == 2) {
			jQuery('#showAllCompanyUserList').show();
		} else {
			jQuery('#showAllCompanyUserList').hide();
			jQuery("#user_id").find('option').removeAttr("selected");
		}


	}
</script>

<script>
	jQuery(document).ready(function(){  
		//$('#showAllCompanyUserList').hide(); // hide company user
		checkedAll();	
				//var $j = jQuery.noConflict();
    			$('#checkall').change(function () { 
    			
				
					var value = $('#checkall').is(':checked');
					
					if(value == true){
						
						$(".checkBoxClass").prop('checked', $(this).prop('checked'));
					} else {
						$(".checkBoxClass").prop('checked', false);
					}	
				});
				
				$('.checkBoxClass').change(function() { 
					checkedAll();
				 });
				
			});
			
			
			function checkedAll() {
				//var $j = jQuery.noConflict();
					var numberOfCheckboxes = $('input:checkbox').length -1;
					var numberOfCheckd = $('.checkBoxClass:checkbox:checked').length;
					//alert(numberOfCheckboxes);
					//alert(numberOfCheckd);
					if(numberOfCheckboxes == numberOfCheckd){
						//alert('checked');
						$('.checkboxFilter').prop('checked', true); 
						 
					} else {
					
						$('.checkboxFilter').removeAttr('checked'); 
				
					}
			}
			
			
			
			
		</script>
	<script>
 

	function selectMasterChild(ID) {
		
		var isChecked = $('.master'+ID).prop('checked');
		//alert(isChecked);
		if(isChecked == true)
		$('.child'+ID).prop('checked', true);
		else 
		$('.child'+ID).prop('checked', false);
	
	}

	function selectMaster1(childVal1, masterID) {
		
		  var checkboxCount;
		// Total checkboxes
		checkboxCount =$('input.child'+masterID+'[type=checkbox]').length;
		//checked checkboxes
		var CountAllCheckBoxes = 0;
		CountAllCheckBoxes = $('input.child'+masterID+'[type=checkbox]:checked').length;
		//alert(CountAllCheckBoxes);
        
		
       var isChecked =$('#child'+childVal1).prop('checked');
	   if(isChecked == true)
			$('.subchild'+childVal1).prop('checked', true);
	    else
	       $('.subchild'+childVal1).prop('checked', false);
		   
	
	
	
	
		
   if(CountAllCheckBoxes <=0) {
		$('.master'+masterID).prop('checked', false);
		} else {
			
		$('.master'+masterID).prop('checked', true);
		}
		
        /*var checkboxCount=0;
        //alert(checkboxCount);
		// Total checkboxes
		checkboxCount =$('input.subchild'+masterID+'[type=checkbox]:checked').length;
		
		alert(checkboxCount);
*/
		
	}
function checkallval(masterID)
	  {
    
	CountAllCheckBoxes =jQuery('input.child'+masterID+'[type=checkbox]:checked').length;
	if(CountAllCheckBoxes <=0) {
		$('.master'+masterID).prop('checked', false);
		} else {
			
		$('.master'+masterID).prop('checked', true);
		} 	
	  }	
		
	
	
	function CheckchildMaster(subchildVal1, childMasterID, MasterID) {
		
		// checked checkboxes
		var CountAllCheckBoxes = 0;
		
		CountAllCheckBoxes = $('input.subchild'+childMasterID+'[type=checkbox]:checked').length;
		//alert(CountAllCheckBoxes);
		
	
		if(CountAllCheckBoxes <= 0) {
		
			$('#child'+childMasterID).prop('checked', false);
		} else {
		
			$('#child'+childMasterID).prop('checked', true);
		}
		
		var checkboxCount=0;
		// Total checkboxes
		checkboxCount =$('input.child'+MasterID+'[type=checkbox]:checked').length;
		//alert(checkboxCount);
		if(checkboxCount <= 0 ) {
		
			$('.master'+MasterID).prop('checked', false);
		} else {
		
			$('.master'+MasterID).prop('checked', true);
		}
		
	}
	
</script>


