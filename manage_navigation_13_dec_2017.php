<?php
include("includes/config.inc.php");
include("includes/function.php");
//include("check_page_access.php");//echo $check."hello";
$page_name="Manage Navigation";
$_objAdmin = new Admin();
$_objItem = new Item();
$objArrayList= new ArrayList();	
$pageAccess=1;


include("header.inc.php");
$check=$objArrayList->checkAccess($pageAccess, basename($_SERVER['PHP_SELF']));
if($check == false){
header('Location: ' . $_SERVER['HTTP_REFERER']);
}
include("manage_navigation.inc.php") ;
/*echo "<pre>";
print_r($_SESSION);*/
?>
<script type="text/javascript">
	function showParent(id) {
		//alert(id);
		var PID = $('#user_type').find("option:selected").attr("title");
		//alert(PID);
		if(typeof PID != 'undefined') { 
		//alert(PID);
		$.ajax({
			url: "gethierarchylistmenu.php",
			data: "pid="+PID+"&id="+id,
			success: function(data){
				//alert(data);
				$('#parentType').html(data);
			}
		});
		} else {
			$('#parentType').html('');
		}
	}
</script>

<script>
	$(document).ready(function(){  
		checkedAll();	
				var $j = jQuery.noConflict();
    			$j('#checkall').change(function () { 
				
					var value = $j('#checkall').attr( "checked" );
					
					if(value == true){
						$(".checkBoxClass").prop('checked', $(this).prop('checked'));
					} else {
						$(".checkBoxClass").prop('checked', false);
					}	
				});
				
				$('.checkBoxClass').change(function() { 
					checkedAll();
				 });
				
			});
			
			
			function checkedAll() {
				var $j = jQuery.noConflict();
					var numberOfCheckboxes = $('input:checkbox').length -1;
					var numberOfCheckd = $('.checkBoxClass:checkbox:checked').length;
					//alert(numberOfCheckboxes);
					//alert(numberOfCheckd);
					if(numberOfCheckboxes == numberOfCheckd){
						//alert('checked');
						$('.checkboxFilter').prop('checked', true); 
						 
					} else {
					
						$('.checkboxFilter').removeAttr('checked'); 
				
					}
			}
			
			
			
			
		</script>

<!--<input name="pagename" type="hidden"  id="pagename" value="editdiscount.php" />-->
<div id="content-outer">
<!-- start content -->
<div id="content">
<div id="page-heading"><h1><span style="color: #d74343; font-family: Tahoma; font-weight: bold;">Manage Navigation</span></h1></div>
<table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">

<tr>
	<!--<td id="tbl-border-left"></td>-->
	<td>
	<!--  start content-table-inner -->
	<div id="content-table-inner">
	<!--  start message-green -->
	<?php if($man_add_sus!=''){?>
	<div id="message-green">
		<table border="0" width="100%" cellpadding="0" cellspacing="0">
		<tr>
			<td class="green-left"><?php echo $man_add_sus; ?></td>
			<td class="green-right"><a class="close-green"><img src="images/icon_close_green.gif"   alt="" /></a></td>
		</tr>
		</table>
	</div>
	<?php } ?>
	<?php if($user_name_err!=''){?>
					<div id="message-red">
						<table border="0" width="100%" cellpadding="0" cellspacing="0">
						<tr>
							<td class="red-left"> <?php echo $user_name_err; ?></td>
							<td class="red-right"><a class="close-red"><img src="images/icon_close_red.gif"   alt="" /></a></td>
						</tr>
						</table>
					</div>
					<?php } ?>
	<!--  end message-green -->
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
	<tr valign="top">
	<td>
		<!-- start id-form -->
		<table  border="0" cellspacing="0" cellpadding="0"  align="center" width="100%">
			<tr>
				<td align="lift" valign="top"  >
				<table id="flex1" style="display:none"></table>
	<!--  end message-red -->
<!--  start step-holder -->
<div id="step-holder">
<div class="step-no">1</div>
<div class="step-dark-left">User Navigation</div>
<div class="step-dark-right">&nbsp;</div>
<div class="clear"></div>
</div>
	
	<!--  end step-holder -->
	<?php //echo "<pre>"; print_r($auRec);?>
	
	<form name="frmPre" id="frmPre" method="post" action="<?php $_SERVER['PHP_SELF'] ?>" enctype="multipart/form-data" >
		<table border="0" width="70%" cellpadding="0" cellspacing="0"  id="id-form">
		
		<tr>
			<th valign="top">User Type</th>
			<td>
				<select id="user_type" name="user_type" class="styledselect_form_3 required" style="padding:0px;" onchange="showParent(<?php if(isset($_GET['id'])){ echo $_GET['id']; } else { echo "0";}?>)">
					<option value="">Please Select</option>
		<?php   $usertype = $_objItem->_getSelectList2('table_user_type',"id, type, parentType,hierarchy_id",''," account_id='".$_SESSION['accountId']."' OR account_id='' AND status='A'");
				$GetuserType = " nav_privilege_id='".$_GET['id']."'";		
				$pageGroup=$_objAdmin->_getselectList2('table_navigation_privilege','*',''," $GetuserType ");
				foreach($usertype as $value):
				//echo $value->id." ".$value->parentType;;
				if($value->parentType!=1){
				?>
				
				<option value="<?php echo $value->id;?>" title="<?php echo $value->parentType;?>" 
					<?php if($value->parentType == $pageGroup[0]->user_type && $value->hierarchy_id == $pageGroup[0]->hierarchy_id){?> selected="selected"<? }?>><?php echo $value->type;?>
				</option>
				<?php }endforeach;?>
				</select>
			</td>
			<td></td>
		</tr>
		
		
		<!--<tr id="">
			<th valign="top">Page Access Control:</th>
			<td>
				<input type="checkbox" id="addPage" value="1" class="checkboxFilter" name="addPage" /><span style="font-size:16px; font-weight:bold; padding:3px;"> Add</span>
				<input type="checkbox" id="editPage" value="2" class="checkboxFilter" name="editPage" /><span style="font-size:16px; font-weight:bold; padding:3px;"> Edit</span>
				<input type="checkbox" id="viewPage" value="3" class="checkboxFilter" name="viewPage" /><span style="font-size:16px; font-weight:bold; padding:3px;"> View</span>
				<input type="checkbox" id="importPage" value="4" class="checkboxFilter" name="importPage" /><span style="font-size:16px; font-weight:bold; padding:3px;"> Import</span>
			</td>
			<td></td>
		</tr>-->


<script>
 

	function selectMasterChild(ID) {
		
		var isChecked = $('.master'+ID).prop('checked');
		//alert(isChecked);
		if(isChecked == true)
		$('.child'+ID).prop('checked', true);
		else 
		$('.child'+ID).prop('checked', false);
	
	}

	function selectMaster1(childVal1, masterID) {
		
		  var checkboxCount;
		// Total checkboxes
		checkboxCount =$('input.child'+masterID+'[type=checkbox]').length;
		//checked checkboxes
		var CountAllCheckBoxes = 0;
		CountAllCheckBoxes = $('input.child'+masterID+'[type=checkbox]:checked').length;
		//alert(CountAllCheckBoxes);
        
		
       var isChecked = $('#child'+childVal1).prop('checked');
	   if(isChecked == true)
			$('.subchild'+childVal1).prop('checked', true);
	    else
	       $('.subchild'+childVal1).prop('checked', false);
		   
	
	
	
	
		
   if(CountAllCheckBoxes <=0) {
		$('.master'+masterID).prop('checked', false);
		} else {
			
		$('.master'+masterID).prop('checked', true);
		}
		
        /*var checkboxCount=0;
        //alert(checkboxCount);
		// Total checkboxes
		checkboxCount =$('input.subchild'+masterID+'[type=checkbox]:checked').length;
		
		alert(checkboxCount);
*/
		
	}
function checkallval(masterID)
	  {
    
	CountAllCheckBoxes = $('input.child'+masterID+'[type=checkbox]:checked').length;
	if(CountAllCheckBoxes <=0) {
		$('.master'+masterID).prop('checked', false);
		} else {
			
		$('.master'+masterID).prop('checked', true);
		} 	
	  }	
		
	
	
	function CheckchildMaster(subchildVal1, childMasterID, MasterID) {
		
		// checked checkboxes
		var CountAllCheckBoxes = 0;
		
		CountAllCheckBoxes = $('input.subchild'+childMasterID+'[type=checkbox]:checked').length;
		//alert(CountAllCheckBoxes);
		
	
		if(CountAllCheckBoxes <= 0) {
		
			$('#child'+childMasterID).prop('checked', false);
		} else {
		
			$('#child'+childMasterID).prop('checked', true);
		}
		
		var checkboxCount=0;
		// Total checkboxes
		checkboxCount =$('input.child'+MasterID+'[type=checkbox]:checked').length;
		//alert(checkboxCount);
		if(checkboxCount <= 0 ) {
		
			$('.master'+MasterID).prop('checked', false);
		} else {
		
			$('.master'+MasterID).prop('checked', true);
		}
		
	}
	
</script>
		
		<tr>
			<th valign="top">Access Control:</th>
			<td><input type="checkbox" id="checkall" class="checkboxFilter" name="checkall" /> &nbsp;&nbsp;&nbsp;<strong>Check All</strong></td>
			<td>&nbsp;</td>
		</tr>
		
		<tr>
			<td>&nbsp;</td>
			<td>
				<div  style="overflow:scroll; ">
				<table width="100%" cellspacing="5" cellpadding="5">
				<?php //$arrayIDs = explode(',',$auRec[0]->privilege_pages);
					  //echo "<pre>";
					 // print_r($arrayIDs);
					 if($_GET['id']!=''){
					 $userType = " account_id='".$_SESSION['accountId']."' and user_type=''";		
					 $pageGroup=$_objAdmin->_getselectList2('table_navigation_privilege','*',''," $userType ");
					  $pageGroupNew=$_objAdmin->_getselectList2('table_navigation_privilege','*',''," nav_privilege_id='".$_GET['id']."' ");
		 			 $pageID = $pageGroup[0]->page_privileges;
					 $user_type = $pageGroupNew[0]->user_type;
					 $UserID = $pageGroupNew[0]->user_id;
					// echo $UserID;
					 if($user_type!='' || $UserID!=''){
					 	 $pageGroup=$_objAdmin->_getselectList2('table_navigation_privilege','*',''," nav_privilege_id='".$_GET['id']."' ");
							 $CheckpageID = $pageGroup[0]->page_privileges;
						 	 $arrayIDs = explode(',',$CheckpageID);
							// echo $arrayIDs;
					 }
					 
					 //echo $pageID;
					 if($pageID!=''){$checkPageID="id IN ($pageID) AND";
					
					 }			 		  
		  			}
					 
					  $pages = $_objItem->_getSelectList2('table_cms','id, parentPage, page_alias_name',''," $checkPageID
					  			 (parentPage IS NULL OR parentPage = 0) AND status='A' ORDER BY ordID ASC"); 

					if(is_array($pages))
					{
					 foreach($pages as $value){  
					 
					 $masterID = $value->id;
					 ?>
					<tr >
					<td colspan="2">
						<div style=" height:20px; background: #999999; padding:5px; border:1px solid #000; border-radius:3px;">
					<input type="checkbox" id="master<?php echo $value->id;?>" onclick="selectMasterChild(this.value);"  class="checkBoxClass master<?php echo $value->id;?>" name="privilege_pages[]" 
value="<?php echo $value->id;?>" 
										<?php if(in_array($value->id,$arrayIDs)){?> checked="checked"<? }?> />
									<strong><?php echo $value->page_alias_name;?></strong></div>
								</td>
								</tr>
							<?php $subpages=$_objItem->_getSelectList2('table_cms','id, page_name, parentPage, page_alias_name',''," $checkPageID	parentPage = '".$value->id."' AND status='A' ORDER BY ordID ASC"); 
						
	  						if(is_array($subpages))
							{
								foreach($subpages as $value)
								{   $childID = $value->id; ?>
								<tr>
									<td colspan="2">
									<div style=" background: #c3c3c3; margin-left:7px; padding:5px 0px 5px 15px; border:1px solid #000; border-radius:3px;">
									<input type="checkbox" id="child<?php echo $value->id;?>" class="checkBoxClass child<?php echo $masterID;?>" onclick="selectMaster1(this.value, <?php echo $masterID;?>);  checkallval(<?php echo $masterID; ?>);" name="privilege_pages[]" 
										value="<?php echo $value->id;?>" 
										<?php if(in_array($value->id,$arrayIDs)){?> checked="checked"<? }?> />
									<?php echo $value->page_alias_name;?>
								</div>
								</td>
								</tr>
								<?php $subpages=$_objItem->_getSelectList2('table_cms','id, page_name, parentPage, page_alias_name',''," $checkPageID parentPage = '".$value->id."' AND status='A' ORDER BY ordID ASC"); 
						
	  						if(is_array($subpages))
							{
								foreach($subpages as $value)
								{?>
								<tr>
									<td colspan="2">
									<div style=" padding:5px 0px 0px 25px;">
									<input type="checkbox" onclick="CheckchildMaster(this.value, <?php echo $childID;?>, <?php echo $masterID;?>);" id="subchild<?php echo $value->id;?>" class="checkBoxClass subchild<?php echo $childID;?> child<?php echo $masterID;?>" name="privilege_pages[]" 
										value="<?php echo $value->id;?>" 
										<?php if(in_array($value->id,$arrayIDs)){?> checked="checked"<? }?> />
									<?php echo $value->page_alias_name;?>
								</div>
								</td>
								</tr>
								<? }?>
					<?php } ?>
						 	<? }?>
					<?php } ?>
				<?php } ?>
			<?php } ?>
				
				
				
				
				<?php  /* $arrayIDs = explode(',',$auRec[0]->privilege_pages);
						//print_r($arrayIDs);
						$listofpages = $_objItem->_getSelectList('table_cms',"id, page_alias_name, parentPage",'',' 
							ORDER BY page_alias_name ASC');
							//echo "<pre>";
							//print_r($listofpages);
						foreach($listofpages as $value):*/?>
						<!--<tr>
						<td>
						<input type="checkbox" class="checkBoxClass" name="privilege_pages[]" 
							value="<?php echo $value->id;?>" 
							<?php if(in_array($value->id,$arrayIDs)){?> checked="checked"<? }?> />
						</td>
						<td><?php echo $value->page_alias_name;?></td>
						</tr>-->
					<?php //endforeach;?>
				</table>
				</div>
			</td>
			<td>&nbsp;</td>
		</tr>

		<tr>
			<th valign="top"></th>
			<td><input type="hidden" class="checkbox" id="checkbox" name="manage_report" <?php if($auRec[0]->view_activity_report=='Yes') echo "checked"; ?> value="Yes" /><!-- <b>View Activity Report</b>--></td>
			<td></td>
		</tr>
		<tr>
			<th valign="top"></th>
			<td><input type="hidden" class="checkbox" id="checkbox" name="manage_retailer_relationship" <?php if($auRec[0]->manage_retailer_relationship=='Yes') echo "checked"; ?> value="Yes" /> <!--<b>Manage Retailer Relationship</b>--></td>
			<td></td>
		</tr>
		<tr>
		<th>&nbsp;</th>
		<td valign="top">
			<input name="manage_user_add" type="hidden" value="yes" />
			<input name="man_nav_id" type="hidden" value="<?php echo $_GET['id']; ?>" />
			<input name="account_id" type="hidden" value="<?php echo $_SESSION['accountId']; ?>" />
			<!--<input name="start_date" type="hidden" value="<?php //echo $date; ?>" />-->
			<!--<input name="end_date" type="hidden" value="<?php //echo $_SESSION['EndDate']; ?>" />-->
			<input type="reset" value="Reset!" class="form-reset">
			<input type="button" value="Back" class="form-reset" onclick="location.href='user_navigation_list.php';" />
			<input name="submit" class="form-submit" type="submit" id="submit" value="Save & Continue" />
		</td>
		</tr>
		</table>
	</form>

</td>
			</tr>
		</table>
	<!-- end id-form  -->

	</td>
		<td>
		<!-- right bar-->
		<?php //include("rightbar/item_bar.php") ?>
		</td>
	</tr>
	</table>
	<div class="clear"></div>
	</div>
	<!--  end content-table-inner  -->
	</td>
	<td id="tbl-border-right"></td>
	</tr>
<!--<tr>
	<th class="sized bottomleft"></th>
	<td id="tbl-border-bottom">&nbsp;</td>
	<th class="sized bottomright"></th>
</tr>-->
</table>
<div class="clear">&nbsp;</div>
</div>
<!--  end content -->
<div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->
<div class="clear">&nbsp;</div> 
<!-- start footer -->         
<?php include("footer.php") ?>
<!-- end footer -->
 
</body>
</html>