<?php
include("includes/config.inc.php");
include("includes/function.php");
$_objAdmin = new Admin();
$_objItem = new Item();
if(isset($_REQUEST['show']) && $_REQUEST['show']=="yes"){
	$_objAdmin->showManageUser();
	die;
}
 
include("manage_user.inc.php");
include("import.inc.php");
include("header.inc.php");
 ?>
<!-- start content-outer -->
<input name="pagename" type="hidden"  id="pagename" value="manage_user.php" />
<div id="content-outer">
<!-- start content -->
<div id="content">
<div id="page-heading"><h1><span style="color: #d74343; font-family: Tahoma; font-weight: bold;">Company Users</span></h1></div>
<table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">
<!--<tr>
	<th rowspan="3" class="sized"><img src="images/side_shadowleft.jpg" width="20" height="300" alt="" /></th>
	<th class="topleft"></th>
	<td id="tbl-border-top">&nbsp;</td>
	<th class="topright"></th>
	<th rowspan="3" class="sized"><img src="images/side_shadowright.jpg" width="20" height="300" alt="" /></th>
</tr>
-->
<tr>
	<!--<td id="tbl-border-left"></td>-->
	<td>
	<!--  start content-table-inner -->
	<div id="content-table-inner">
	
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
	<tr valign="top">
	<td>
		<!-- start id-form -->
		<?php
		if(isset($_REQUEST['add']) || $man_name_err!='' || ($_POST) && $_POST['submit'] == 'Save & Add Next Manage User' || $_REQUEST['id']!='' || $_SESSION['man_err']!=''){
			include("manage_user/add_manage_user.php");
			}else
			if($man_add_sus!='' || $login_err!=''){
				include("manage_user/login_manage_user.php");
				}else {
					include("manage_user/view_manage_user.php");
					unset($_SESSION['man_err'], $_SESSION['manId'], $_SESSION['man_login'], $_SESSION['update'] );
					}
		?>
		<!-- end id-form  -->
	</td>
	<td>
	<!-- right bar-->
	<?php include("rightbar/manageuser_bar.php") ?>
	</td>
	</tr>
<tr>
<td><img src="images/blank.gif" width="695" height="1" alt="blank" /></td>
<td></td>
</tr>
</table>
<div class="clear"></div>
</div>
<!--  end content-table-inner  -->
</td>
<td id="tbl-border-right"></td>
</tr>
<!--<tr>
	<th class="sized bottomleft"></th>
	<td id="tbl-border-bottom">&nbsp;</td>
	<th class="sized bottomright"></th>
</tr>-->
</table>
<div class="clear">&nbsp;</div>
</div>
<!--  end content -->
<div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->
<div class="clear">&nbsp;</div> 
<!-- start footer -->         
<?php include("footer.php");?>
<!-- end footer -->
 
</body>
</html>