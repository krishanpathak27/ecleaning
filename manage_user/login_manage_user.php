<script>
$(document).ready(function(){
$('#username').keyup(username_check);
});
	
function username_check(){	
var username = $('#username').val();
if(username == "" || username.length < 1){
$('#username').css('border', 'solid 1px #dc0000');
$('#tick').hide();
}else{

jQuery.ajax({
   type: "POST",
   url: "check.php",
   data: 'username='+ username,
   cache: false,
   success: function(response){
if(response == 1){
	$('#username').css('border', 'solid 1px #dc0000');	
	$('#tick').hide();
	$('#cross').fadeIn();
	}else{
	$('#username').css('border', 'solid 1px #dc0000');
	$('#cross').hide();
	$('#tick').fadeIn();
	     }
}
});
}
}
</script>
<style>
#username{
	padding:3px;
	font-size:12px;
	border:solid 1px #dc0000;
}

#tick{display:none}
#cross{display:none}
	

</style>
<!--  start step-holder -->
<div id="step-holder">
<div class="step-no-off">1</div>
<div class="step-light-left">Company User Forms</div>
<div class="step-light-right">&nbsp;</div>
<div class="step-no">2</div>
<div class="step-dark-left">Login Form</div>
<div class="step-dark-round">&nbsp;</div>
<div class="clear"></div>
</div>
<!--  end step-holder -->
	<!--  start message-red -->
	<?php if($login_err!=''){?>
	<div id="message-red">
		<table border="0" width="100%" cellpadding="0" cellspacing="0">
		<tr>
			<td class="red-left">Error. <?php echo $login_err; ?></td>
			<td class="red-right"><a class="close-red"><img src="images/icon_close_red.gif"   alt="" /></a></td>
		</tr>
		</table>
	</div>
	<?php } ?>
	<!--  end message-red -->
	<!-- start id-form -->
	<form name="frmPre" id="frmPre" method="post" action="manage_user.php" enctype="multipart/form-data" >
		<table border="0" cellpadding="0" cellspacing="0"  id="id-form">
		<tr>
			<th valign="top">User Name:</th>
			<td><input type="text" name="username" id="username" class="required" value="<?php echo $auRec[0]->username; ?>"/>
			<div id="tick"><span style=" color:#088A08;"><b>User Name Available</b></span><img src="images/tick.png" width="16" height="16"/></div>
			<div id="cross"><span style=" color:#FF0000;"><b>User Name Not Available</b></span><img  src="images/cross.png" width="16" height="16"/></div>
			</td>
			<td></td>
		</tr>
		<tr>
			<th valign="top">Password:</th>
			<td><input type="password" name="password" id="password" <?php if($auRec[0]->web_user_id!=''){ ?>class="text"<?php } else { ?>class="required minlength" <?php } ?>  minlength="8" /></td>
			<td>
			</td>
		</tr>
		<tr>
			<th valign="top">Confirm-Password:</th>
			<td><input type="password" name="conf_pass" id="conf_pass" equalto="#password" <?php if($auRec[0]->web_user_id!=''){ ?>class="text"<?php } else { ?>class="required" <?php } ?>/></td>
			<td>
			</td>
		</tr>
		<tr>
			<th valign="top">Email ID:</th>
			<td><input type="text" name="email" id="email" class="required email" value="<?php echo $auRec[0]->email_id; ?>"/></td>
			<td>
			</td>
		</tr>
		<th>&nbsp;</th>
		<td valign="top">
			<input type="hidden" name="ope_id" value="<?php echo $_SESSION['manId']; ?>" />
			<input type="hidden" name="web_id" value="<?php echo $auRec[0]->web_user_id; ?>"/>
			<input type="hidden" name="user_type" value="2" />
			<input name="account_id" type="hidden" value="<?php echo $_SESSION['accountId']; ?>" />
			<input name="start_date" type="hidden" value="<?php echo $_SESSION['StartDate']; ?>" />
			<input name="end_date" type="hidden" value="<?php echo $_SESSION['EndDate']; ?>" />
			<input name="man_login" type="hidden" value="yes" />
			<input type="reset" value="Reset!" class="form-reset">
			<?php if($_SESSION['update']==''){ ?>
			<input name="submit" class="form-submit" type="submit" id="submit" value="Save" />
			<input name="submit" class="form-submit" type="submit" id="submit" value="Save & Add Next Manage User" />
			<?php } else { ?>
			<input name="submit" class="form-submit" type="submit" id="submit" value="Save Manage User" />
			<?php } ?>
		</td>
		</table>
	  
	</form>