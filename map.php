<?php
include("includes/config.inc.php");
include("includes/function.php");
$_objAdmin = new Admin();

if($_REQUEST['id']!=""){
$auMarker=$_objAdmin->_getSelectList2('table_retailer_location as l left join table_retailer as r on l.retailer_id=r.retailer_id left join table_salesman as s on s.salesman_id=l.salesman_id',"l.lat,l.lng,l.capcure_mode,r.retailer_name,s.salesman_name",''," l.location_id='".base64_decode($_REQUEST['id'])."' ");
}

if($_REQUEST['rid']!=""){
$auMarker=$_objAdmin->_getSelectList('table_survey as s left join table_retailer as r on s.retailer_id=r.retailer_id left join table_salesman as sal on sal.salesman_id=s.salesman_id',"s.lat,s.lng,s.network_mode,r.retailer_name,sal.salesman_name",''," s.survey_id='".base64_decode($_REQUEST['rid'])."' ");
}
?>
<html>
<head>
<meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=9" >
<meta http-equiv="X-UA-Compatible" content="IE=8" />
<meta http-equiv="X-UA-Compatible" content="IE=7" />
<meta http-equiv="X-UA-Compatible" content="IE=6" />
<style type="text/css">
  html { height: 100% }
  body { height: 100%; margin: 0; padding: 0 }
  #map_canvas { height: 100% }
</style>
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
<script type="text/javascript">
function initialize() 
{
    var myOptions = 
	{  center: new google.maps.LatLng(<?php echo $auMarker[0]->lat.",".$auMarker[0]->lng; ?>), zoom: 18, mapTypeId: google.maps.MapTypeId.ROADMAP };
	var map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);

				var myLatlng = new google.maps.LatLng(<?php echo $auMarker[0]->lat.",".$auMarker[0]->lng; ?>)
				var marker = new google.maps.Marker
				({ position: myLatlng,title:"<?php echo "Retailer Name: " .$auMarker[0]->retailer_name.", Salesman Name: ".$auMarker[0]->salesman_name.", Location mode: ".$auMarker[0]->network_mode.$auMarker[0]->capcure_mode ?>"});//, animation: google.maps.Animation.DROP});
				marker.setMap(map);
	
}
</script>
 </head>
 <body onLoad="initialize();">
    <div id="map_canvas" ></div>
  </body>
</html>