<?php
include("includes/config.inc.php");
include("includes/function.php");
$page_name="Master Category Margin";

$_objAdmin = new Admin();
$objArrayList= new ArrayList();

if(isset($_REQUEST['id']) && $_REQUEST['id']!="")
{
	$auRec=$_objAdmin->_getSelectList('table_category AS c LEFT JOIN table_category_margins AS m ON m.category_id = c.category_id',"m.margin_id, m.margin_value, c.category_name, c.category_code, c.category_id",''," c.cat_type = 'S' AND c.category_id=".$_REQUEST['id']);
	if(count($auRec)<=0) header("Location: master_category.php");
} else {
 header("Location: master_category.php");

}
//echo "<pre>";
//print_r($auRec);

if(isset($_POST['add']) && $_POST['add'] == 'yes')
{
//print_r($_REQUEST);
//exit;

	if($_REQUEST['id']!="") 
	{
		if(is_numeric($_POST['margin_value']) && isset($_POST['margin_value'])) 
		{
		
			$checkMargin = $_objAdmin->_getSelectList('table_category_margins',"margin_value",''," category_id=".$_REQUEST['id']);
			if(count($checkMargin)>0) 
			{
				if($checkMargin[0]->margin_value!=trim($_POST['margin_value'])) 
				{
					$cid=$_objAdmin->updateMasterCategoryMargin($_REQUEST['margin_id']);
					$_objAdmin->addMasterCategoryActivity($cid);
					$_SESSION['SESS_MSG'] ="Category Margin has been updated successfully.";
					//header('location: master_category_margin.php?id='.$_POST['id']);
				} else {
				
					$_SESSION['SESS_MSG'] ="Category Margin has been updated successfully.";
					//header('location: master_category_margin.php?id='.$_POST['id']);
				
				}
			} 
			else 
			{
				$cid=$_objAdmin->addMasterCategoryMargin();
				$_objAdmin->addMasterCategoryActivity($cid);
				$_SESSION['SESS_MSG'] = "Category Margin has been added successfully.";
				//header('location: master_category_margin.php?id='.$_POST['id']);
			}
		} else {
		
			$err ="Margin value should be numeric or greater than 0.";
		
		}
	}
}





?>
<?php include("header.inc.php");

$pageAccess=1;
$check=$objArrayList->checkAccess($pageAccess, basename($_SERVER['PHP_SELF']));

if($check == false){
header('Location: ' . $_SERVER['HTTP_REFERER']);
}
 ?>
<script type="text/javascript" src="javascripts/validate.js"></script>
<div class="clear"></div>
<!-- start content-outer -->
<div id="content-outer">
<!-- start content -->
<div id="content">
<div id="page-heading"><h1><span style="color: #d74343; font-family: Tahoma; font-weight: bold;">Set Margin</span></h1></div>
		<table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">
		<tr>
			<!--<td id="tbl-border-left"></td>-->
			<td>
			<!--  start content-table-inner -->
			<div id="content-table-inner">
			<table border="0" width="100%" cellpadding="0" cellspacing="0">
				<tr valign="top">
					<td>
					<!--  start message-red -->
					<?php if($err!=''){?>
					<div id="message-red">
						<table border="0" width="100%" cellpadding="0" cellspacing="0">
						<tr>
							<td class="red-left"> <?php echo $err; ?></td>
							<td class="red-right"><a class="close-red"><img src="images/icon_close_red.gif"   alt="" /></a></td>
						</tr>
						</table>
					</div>
					<?php } ?>
					<!--  end message-red -->
					<?php if($sus!=''){?>
					<!--  start message-green -->
					<div id="message-green">
						<table border="0" width="100%" cellpadding="0" cellspacing="0">
						<tr>
							<td class="green-left"><?php echo $sus; ?></td>
							<td class="green-right"><a class="close-green"><img src="images/icon_close_green.gif"   alt="" /></a></td>
						</tr>
						</table>
					</div>
					<?php } ?>
					<!--  end message-green -->
					
					
					<?php require('includes/error.msg.inc.php'); ?>
					
					
					<!-- start id-form -->
						<form name="frmPre" id="frmPre" method="post" enctype="multipart/form-data" >
							<table border="0" cellpadding="0" cellspacing="0"  id="id-form">
							<tr>
								<th valign="top">Category name:</th>
								<td><?php echo $auRec[0]->category_name; ?></td>
								<td></td>
							</tr>
							
							<tr>
								<th valign="top">Category Code:</th>
								<td><?php echo $auRec[0]->category_code; ?></td>
								<td></td>
							</tr>
							
							<tr>
								<th valign="top">Margin Value:</th>
								<td><input type="text" name="margin_value" id="margin_value" class="required" value="<?php echo $auRec[0]->margin_value; ?>" maxlength="50" /></td>
								<td></td>
							</tr>
							<!--<tr>
								<th valign="top">Status:</th>
								<td>
								<select name="status" id="status" class="styledselect_form_3">
								<option value="A" <?php if($auRec[0]->status=='A') echo "selected";?> >Active</option>
								<option value="I" <?php if($auRec[0]->status=='I') echo "selected";?> >Inactive</option>
								</select>
								</td>
								<td></td>
							</tr>-->
							<tr>
								<th>&nbsp;</th>
								<td valign="top">
									<input name="account_id" type="hidden" value="<?php echo $_SESSION['accountId']; ?>" />
									<input name="id" type="hidden" value="<?php echo $auRec[0]->category_id; ?>" />
									<input name="margin_id" type="hidden" value="<?php echo $auRec[0]->margin_id; ?>" />
									<input name="add" type="hidden" value="yes" />
									<!--<input type="reset" value="Reset!" class="form-reset">-->
									<input type="button" value="Back" class="form-reset" onclick="location.href='master_category.php';" />
									<input name="submit" class="form-submit" type="submit" id="submit" value="Save" />
									
								</td>
							</tr>
							</table>
						</form>
					<!-- end id-form  -->
					</td>
				</tr>
			</table>
		<div class="clear"></div>
		</div>
	<!--  end content-table-inner  -->
	</td>
	<td id="tbl-border-right"></td>
	</tr>
</table>
<div class="clear">&nbsp;</div>
</div>
<!--  end content -->
<div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->
<div class="clear">&nbsp;</div>
    
<!-- start footer --> 
<?php include("footer.php") ?>  
<!-- end footer -->
</body>
</html>