<?php include("includes/config.inc.php");
      include("includes/function.php");
	  include("includes/globalarraylist.php");

	if(isset($_POST['showOrderlist']) && $_POST['showOrderlist'] == 'yes') {	

		if($_POST['sal']!="")  {
			$_SESSION['monthSalesmanID']=$_POST['sal'];	
			} else {
			unset($_SESSION['monthSalesmanID']);
		}

		if($_POST['month']!="" && is_numeric($_POST['month'])) {
			$_SESSION['dismonth']=$_POST['month'];	
		}
		
		if($_POST['year']!="" && is_numeric($_POST['year'])) {
			$_SESSION['disCyear']=$_POST['year'];	
		}

		if($_POST['sal']=='All') {
		  unset($_SESSION['monthSalesmanID']);	
		}

		header("Location: mechanics_summary_report.php");
	}
		
		
		if($_SESSION['disCyear']==''){
			$_SESSION['disCyear']=date('Y');
		}

		if($_SESSION['dismonth']==''){
			$_SESSION['dismonth']=date('m');
		}

	if(isset($_REQUEST['reset']) && $_REQUEST['reset'] == 'yes'){
		 unset($_SESSION['monthSalesmanID']);	 	
		 $_SESSION['dismonth']= '';
		 $_SESSION['disCyear']= '';
		 header("Location: mechanics_summary_report.php");
	}


	// Get All the categories 

	$categotyList = $_objAdmin->_getSelectList('table_category','category_id, category_name','',""); 



?>

<?php include("header.inc.php") ?>
<script type="text/javascript">

	var tableToExcel = (function () {
        var uri = 'data:application/vnd.ms-excel;base64,'
        , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table><tr><td></td></tr></table><table>{table}</table></body></html>'
        , base64 = function (s) { return window.btoa(unescape(encodeURIComponent(s))) }
        , format = function (s, c) { return s.replace(/{(\w+)}/g, function (m, p) { return c[p]; }) }
        return function (table, name, filename) {
        	//alert(table.name);
		  
            if (!table.nodeType) table = document.getElementById(table)
            var ctx = { worksheet: name || 'Worksheet', table: table.innerHTML }

            document.getElementById("dlink").href = uri + base64(format(template, ctx));
            document.getElementById("dlink").download = filename;
            document.getElementById("dlink").click();

        }
    })()	

</script>
<?php //echo "<pre>"; print_r($_SESSION);?>
<!-- start content-outer -->
<input name="pagename" type="hidden"  id="pagename" value="mechanics_summary_report.php">
<div id="content-outer">
<!-- start content -->
<div id="content">
<div id="page-heading"><h1>
	<span style="color: #d74343; font-family: Tahoma; font-weight: bold;">Mechanics Summary</span></h1></div>
	<table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">
	<tr>
		<!--<td id="tbl-border-left"></td>-->
		<td>
		<!--  start content-table-inner -->
	<div id="content-table-inner">
	
	<div id="page-heading" align="left" >
	<?php //echo "<pre>";
		 // print_r($_SESSION);
	?>
	<form name="frmPre" id="frmPre" method="post" action="" enctype="multipart/form-data" >
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
	
	<tr>
	<td id="distributorMenu">
		<h3>Salesman: </h3>
		<h6><select name="sal" id="sal" class="menulist">
			<?php echo $salesmanOptionList = $_objArrayList->GetSalesmenMenu($salsList, $_SESSION['monthSalesmanID'],'root');?>
		</select></h6>
	</td>

	<td><?php echo $months = $_objArrayList->getMonthList($_SESSION['dismonth']);?></td>
	<td><?php echo $year = $_objArrayList->getYearList($_SESSION['disCyear']);?></td>

	<td><h3></h3>
	<input name="submit" class="result-submit" type="submit" id="submit" value="View Details" />
	<input type="button" value="Reset!" class="form-reset" onclick="location.href='mechanics_summary_report.php?reset=yes';" />
	</td></tr>

	<tr>
	<td colspan="5"><!-- <a id="dlink"  style="display:none;"></a><input name="export" class="result-submit" type="button" value="Export to Excel" onclick="tableToExcel('report_export', 'Salesman Summary', 'Salesman Summary.xls');"> -->

	<a id="dlink"  style="display:none;"></a>
	<input type="button" name="submit" value="Export to Excel" class="result-submit" onclick="tableToExcel('report_export', 'Mecahnics Summary', 'Mechanics Summary.xls');" >



	</td>


	<td></td>
	</tr>

	<tr>
	<td colspan="7"><input name="showOrderlist" type="hidden" value="yes" /></td>
	</tr>
	</table>


	</form>

	</div>
	
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
	<tr valign="top">
	<td>
		<!-- start id-form -->
	<div id="Report" style="width:980px; overflow:scroll;">
		<table border="1" width="100%" cellpadding="0" cellspacing="0"  id="report_export" name="report_export">
			<tr  bgcolor="#6E6E6E" style="color: #fff;font-weight: bold;" >
				<td style="white-space: nowrap; margin: 10px; padding: 10px; font-family: monospace;">MSR Name </td>
				<td style="white-space: nowrap; margin: 10px; padding: 10px; font-family: monospace;">Emp Code </td>
				<td style="white-space: nowrap; margin: 10px; padding: 10px; font-family: monospace;"> Date</td>
				<td style="white-space: nowrap; margin: 10px; padding: 10px; font-family: monospace;">Route Name</td>
				<td style=" margin: 10px; padding: 10px; font-family: monospace;">No. Shakti Partner To be Visited  </td>
				<td style=" margin: 10px; padding: 10px; font-family: monospace;">No. of Shakti Partner Visited </td>
				<td style=" margin: 10px; padding: 10px; font-family: monospace;">No. of Shakti Partner Placed Order</td>
				<td style=" margin: 10px; padding: 10px; font-family: monospace;">Beat Adherance </td>
				<td style=" margin: 10px; padding: 10px; font-family: monospace;">Beat Productivity </td>
				
			</tr>
			<?php


			//AND DATE_FORMAT(CUS.start_date, "%Y-%m-%d") < DATE_FORMAT(CONCAT(RS.year,"-",RS.month,"-",RSD.assign_day),"%Y-%m-%d")
			
			$routeCustomerOfSalesman =$_objAdmin->_getSelectList('`table_route_scheduled` AS RS
			LEFT JOIN table_route_schedule_details AS RSD ON RSD.`route_schedule_id` = RS.`route_schedule_id`
			LEFT JOIN table_route_retailer AS RR ON RR.route_id = RSD.route_id AND RR.status IN ("C", "P", "S")
			LEFT JOIN table_customer AS CUS ON CUS.customer_id = RR.retailer_id AND  CUS.display_outlet = "hot"			
			LEFT JOIN table_route AS R ON R.route_id = RSD.route_id
			LEFT JOIN table_salesman AS s ON s.salesman_id = RS.salesman_id',
			"RSD.route_id, R.route_name, RS.salesman_id, s.salesman_name,s.salesman_code, CONCAT(RS.year,'-',RS.month,'-',RSD.assign_day) AS route_date, RS.month, RS.year, RSD.assign_day,COUNT(RR.status) AS ttlRoutePersons, COUNT(DISTINCT(CUS.customer_id)) AS ttlRouteCustomers, RR.status",'',
			" RS.salesman_id = '".$_SESSION['monthSalesmanID']."' AND RS.month = '".$_SESSION['dismonth']."' AND RS.year = '".$_SESSION['disCyear']."' AND R.options ='I'  GROUP BY RSD.route_id,RS.salesman_id,RSD.assign_day,RR.status ORDER BY RSD.assign_day ASC");


			// echo "<pre>";
			// echo print_r($routeCustomerOfSalesman);
			// echo "</pre>";
			// exit;
			

	$routeCustomerOfSalesmanActivity = array();

	$routeCustomerOfSalesmanActivity = $_objAdmin->_getSelectList2("view_orderOrRetDisAddActivity AS A 

	LEFT JOIN table_route_scheduled AS RS ON RS.salesman_id = A.salesman_id AND DATE_FORMAT(A.activity_date, '%Y-%c') = CONCAT(RS.year,'-',RS.month) 
	LEFT JOIN table_route_schedule_details AS RSD ON RSD.route_schedule_id = RS.route_schedule_id AND RSD.assign_day = DATE_FORMAT(A.activity_date, '%e')


	LEFT JOIN table_order AS O ON O.order_id = A.ref_id AND O.order_type IN ('Yes','No')  AND O.customer_id >0  AND A.activity_type = 3 
	LEFT JOIN table_customer AS CUS ON CUS.customer_id = O.customer_id AND CUS.status = 'A' 
	AND CUS.display_outlet = 'hot'	
	LEFT JOIN table_route_retailer AS RR ON RR.route_id = RSD.route_id  AND RR.status IN('C','P','S') AND RR.retailer_id = CUS.customer_id  AND A.activity_type = 3 


	LEFT JOIN table_order AS O2 ON O2.order_id = A.ref_id AND O2.order_type IN ('Yes')  AND O2.customer_id >0  AND A.activity_type = 3 
	LEFT JOIN table_customer AS CUS2 ON CUS2.customer_id = O2.customer_id AND CUS2.status = 'A'
	AND CUS2.display_outlet = 'hot'
	LEFT JOIN table_route_retailer AS RR2 ON RR2.route_id = RSD.route_id  AND RR2.status IN('C','P','S') AND RR2.retailer_id = CUS2.customer_id  AND A.activity_type = 3 ",

	"RSD.route_id, COUNT(DISTINCT(RR.retailer_id)) AS plannedRouteCustomerTotalCalls,  COUNT(DISTINCT(RR2.retailer_id)) AS plannedRouteCustomerProductiveCalls,  A.salesman_id, A.activity_date", '',
	" A.salesman_id = '".$_SESSION['monthSalesmanID']."' AND DATE_FORMAT(A.activity_date,'%Y') = '".$_SESSION['disCyear']."' AND DATE_FORMAT(A.activity_date,'%m') = '".$_SESSION['dismonth']."' AND RSD.route_id IS NOT NULL GROUP BY A.activity_date, A.salesman_id, RSD.route_id");


			//print_r($routeCustomerOfSalesman);
			 // echo "<pre>";
			 // print_r($routeCustomerOfSalesmanActivity);
			 // exit;
			// echo "</pre>";

			// Distributor Activities
			foreach ($routeCustomerOfSalesmanActivity as $key => $cAnalytics) {
				# code...
				// echo "<pre>";
				// print_r($dAnalytics);
				$dataSetOfCustomerSalesmanActivity[$cAnalytics->salesman_id][$cAnalytics->activity_date][$cAnalytics->route_id] = $cAnalytics;

			}


			
			if(is_array($routeCustomerOfSalesman)){
			
			foreach ($routeCustomerOfSalesman as $key => $Mvalue) {
				# code...
				
				$tdStyleClass = "";
				$CustomerType = "";
				$NoofToBeVisitedCustomer = "0";
				$NoOfVisitedCustomer = "0";
				$NoOfCusPlacedOrder = "0";
				/*$NoOfNewOutletSurveyed = "0";
				$NoOfInterestedCustomer = "0";*/
				$BeatAdherence = "0";
				$BeatActivity = "0";

				$rtdate = date('Y-m-d',strtotime($Mvalue->route_date));
				$route_id = $Mvalue->route_id;


				// Order dataset
				// echo "<pre>";
				// print_r($Mvalue);
				// print_r($dataSetOfCustomerOrders[$Mvalue->salesman_id][$rtdate][$Mvalue->status]);

				if(isset($dataSetOfCustomerSalesmanActivity[$Mvalue->salesman_id][$rtdate][$Mvalue->route_id])) {
					$cdata = $dataSetOfCustomerSalesmanActivity[$Mvalue->salesman_id][$rtdate][$Mvalue->route_id];
				} else {
					$cdata = array();
				}


				//if($Mvalue->status == 'C')  {
					$tdStyleClass = "#ededed";
					$CustomerType = "Customer";
					$NoofToBeVisitedCustomer = $Mvalue->ttlRouteCustomers;
					if(sizeof($cdata)>0) {

						//$NoOfVisitedCustomer   =  $rdata[0]->ttlOrders + $rdata[0]->ttlSurvey + $rdata[0]->ttlAddedRet;
						//$NoofToBeVisitedCustomer = $Mvalue->ttlRouteCustomers;
						//$NoOfVisitedCustomer   =  $rdata[0]->ttlOrders;
						$NoOfVisitedCustomer   =  $cdata->plannedRouteCustomerTotalCalls;
						//$NoOfCusPlacedOrder    =  $rdata[0]->ttlProductiveCalls;
						$NoOfCusPlacedOrder  = 		$cdata->plannedRouteCustomerProductiveCalls;
						// $NoOfNewOutletSurveyed =  $cdata->ttlAddedCus;
						// $NoOfInterestedCustomer = $cdata->ttlInterestedCustomer;
						$BeatAdherence = round($NoOfVisitedCustomer/$Mvalue->ttlRouteCustomers,2);
						$BeatActivity  = round($NoOfCusPlacedOrder/$Mvalue->ttlRouteCustomers,2);
					}

				//}

				
				//  echo "<pre>";
				//  print_r($data);
				// // print_r($ddata);
				// echo "</pre>";
			?>

				<tr  style="border-bottom:2px solid #6E6E6E; background:<?php echo $tdStyleClass;?>">
					<td style="padding:10px;" width="10%"><?php echo $Mvalue->salesman_name;?></td>
					<td style="padding:10px;" width="10%"><?php echo $Mvalue->salesman_code;?></td>
					<td style="padding:10px;" width="10%"><?php echo $_objAdmin->_changeDate($Mvalue->route_date);?></td>
					<td style="padding:10px;" width="30%"><?php echo $Mvalue->route_name;?></td>
					<td style="padding:10px;" width="20%" align="center"><?php  echo $NoofToBeVisitedCustomer; ?></td>
					<td style="padding:10px;" width="20%" align="center"><?php  echo $NoOfVisitedCustomer; ?></td>				
					<td style="padding:10px;" width="20%" align="center"><?php  echo $NoOfCusPlacedOrder; ?></td>
					<td style="padding:10px;" width="20%" align="center"><?php  echo $BeatAdherence; ?></td>
					<td style="padding:10px;" width="20%" align="center"><?php  echo $BeatActivity; ?></td>
					
					


					

					
				</tr>
			<?php } } else { ?>
			<tr  style="border-bottom:2px solid #6E6E6E;text-align: center">
				<td style="padding:10px;" colspan="20">Report Not Available</td>
			</tr>
			<?php } ?>
		</table>
		</div>
	<!-- end id-form  -->
	</td>
</tr>
</table>
<div class="clear"></div>
</div>
<!--  end content-table-inner  -->
</td>
<td id="tbl-border-right"></td>
</tr>
</table>
<div class="clear">&nbsp;</div>
</div>
<!--  end content -->
<div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->
<div class="clear">&nbsp;</div> 
<!-- start footer -->         
<?php include("footer.php") ?>
<!-- end footer -->

</body>
</html>