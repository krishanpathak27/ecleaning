<?php include("includes/config.inc.php");
      include("includes/function.php");
	  include("includes/globalarraylist.php");
	  
	  $page_name="Complaint";

	if($_SESSION['SalComMsg']!='')
	{
		$salesman =" and s.salesman_id='".$_SESSION['SalComMsg']."'";
	}
		

	if(isset($_POST['showComMsg']) && $_POST['showComMsg'] == 'yes')
	{	
		if($_POST['sal']!="All") 
		{
			$_SESSION['SalComMsg']=$_POST['sal'];	
		}
	
		if($_POST['from']!="") 
		{
		$_SESSION['FromDateMSG']=$_objAdmin->_changeDate($_POST['from']);	
		}
		
		if($_POST['to']!="") 
		{
		$_SESSION['ToDateMSG']=$_objAdmin->_changeDate($_POST['to']);	
		}
	
		if($_POST['sal']=="All") 
		{
		  unset($_SESSION['SalComMsg']);	
		}
		
	header("Location: message_communication.php");
} 


if(isset($_REQUEST['reset']) && $_REQUEST['reset'] == 'yes')
{
	 unset($_SESSION['SalComMsg']);	
	$_SESSION['FromDateMSG']=$_objAdmin->_changeDate(date("Y-m-d",strtotime("-1 day")));
	$_SESSION['ToDateMSG']=$_objAdmin->_changeDate(date("Y-m-d",strtotime("-1 day")));	
	header("Location: message_communication.php");
}



if(isset($_POST['delete']) && $_POST['delete']=='yes' && $_POST['id']!=""){
	$id=$_objAdmin->_dbUpdate2(array("status"=>'I'),'table_communication', " comm_id='".$_POST['id']."'");
} 
	
if(isset($_REQUEST['show']) && $_REQUEST['show']=="yes")
{
	$_objAdmin->showMessageCom($salesman);
	die;
}


?>

<?php include("header.inc.php") ?>
<script src="javascripts/dateNextPrev.js" type="text/javascript"></script>
<input name="pagename" type="hidden"  id="pagename" value="" />
<!-- start content-outer -->
<div id="content-outer">
<!-- start content -->
<div id="content">
<div id="page-heading"><h1><span style="color: #d74343; font-family: Tahoma; font-weight: bold;">Complaint</span></h1></div>
<table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">
<tr>
	<!--<td id="tbl-border-left"></td>-->
	<td>
	<!--  start content-table-inner -->
	<div id="content-table-inner">
	
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
	<tr valign="top">
	<td>
	<?php if(isset($_REQUEST['sus']) && $_REQUEST['sus']=="sus"){?>
	<!--  start message-green -->
	<div id="message-green">
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
		<tr>
			<td class="green-left"><?php echo "Message has been sent successfully."; ?></td>
			<td class="green-right"><a class="close-green"><img src="images/icon_close_green.gif"   alt="" /></a></td>
		</tr>
	</table>
	</div>
	<?php } ?>
	
	
	<form name="frmPre" id="frmPre" method="post" action="message_communication.php" enctype="multipart/form-data" >
	<table border="0" width="90%" cellpadding="0" cellspacing="0">
	<tr>
	  <td><h3>&nbsp;&nbsp;Salesman:</h3><h6>
	  <select name="sal" id="sal" class="styledselect_form_5" style="" >
			<?php echo $rtndata = $_objArrayList->GetSalesmenMenu($salsList, $_SESSION['SalComMsg'], 'flex');?>
		</select></h6></td>

		
		<td><h3> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;From Date:</h3><h6> 
		<img src="css/images/prev.png" width="18" height="18" onclick="dateFromPrev();"> 
		<input type="text" id="from" name="from" class="date" style="width:150px" value="<?php if($_SESSION['FromDateMSG']!='') { echo $_SESSION['FromDateMSG']; } else { echo $_objAdmin->_changeDate(date("Y-m-d",strtotime("-1 day"))); }?>"  readonly />
		 <img src="css/images/next.png" width="18" height="18" onclick="dateFromNext();"> </h6></td>
		 
		<td><h3> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;To Date: </h3>
		<h6> <img src="css/images/prev.png" width="18" height="18" onclick="dateToPrev();">
		<input type="text" id="to" name="to" class="date" style="width:150px" value="<?php if($_SESSION['ToDateMSG']!='') { echo $_SESSION['ToDateMSG']; } else { echo $_objAdmin->_changeDate(date("Y-m-d",strtotime("-1 day"))); }?>"  readonly />
		<img src="css/images/next.png" width="18" height="18" onclick="dateToNext();"></h6>
		</td>
		
		
		<td>
		<input name="showComMsg" type="hidden" value="yes" />
		<input name="submit" class="result-submit" type="submit" id="submit" value="Show List" />
	<input type="button" value="Reset!" class="form-reset" onclick="location.href='message_communication.php?reset=yes';" /></td>
		</tr>
		<tr>
	</tr>
	</table>
	</form>
	
	
	
	
		<!-- start id-form -->
		<table  border="0" cellspacing="0" cellpadding="0"  align="center" width="100%">
				<tr>
				<td align="lift" valign="top"  >
				<table id="flex1" style="display:none"></table>
				<script type="text/javascript">showMessageCom();</script>
				</td>
				</tr>
				</table>
	<!-- end id-form  -->

	</td>
	<td>

	<!-- right bar-->
	<?php// include("rightbar/message_bar.php") ?>
</td>
</tr>
<tr>
<td><img src="images/blank.gif" width="695" height="1" alt="blank" /></td>
<td></td>
</tr>
</table>
<div class="clear"></div>
</div>
<!--  end content-table-inner  -->
</td>
<td id="tbl-border-right"></td>
</tr>
<!--<tr>
	<th class="sized bottomleft"></th>
	<td id="tbl-border-bottom">&nbsp;</td>
	<th class="sized bottomright"></th>
</tr>-->
</table>
<div class="clear">&nbsp;</div>
</div>
<!--  end content -->
<div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->
<div class="clear">&nbsp;</div> 
<!-- start footer -->         
<?php include("footer.php") ?>
<!-- end footer --> 
<script src="javascripts/jquery-1.8.2.js" type="text/javascript"></script>
<script src="javascripts/jquery-ui.js"></script>
<script type="text/javascript" src="javascripts/validate.js"></script>
 <script>
    $(function() {
        $( "#from" ).datepicker({
			dateFormat: "d M yy",
            defaultDate: "w",
            changeMonth: true,
            numberOfMonths: 1,
            onSelect: function( selectedDate ) {
                $( "#to" ).datepicker( "option", "minDate", selectedDate );
            }
        });
        $( "#to" ).datepicker({
			dateFormat: "d M yy",
            defaultDate: "-w",
            changeMonth: true,
            numberOfMonths: 1,
            onSelect: function( selectedDate ) {
                $( "#from" ).datepicker( "option", "maxDate", selectedDate );
            }
        });
    });
</script>
</body>
</html>