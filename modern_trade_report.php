<?php
include("includes/config.inc.php");
include("includes/function.php");
include("includes/globalarraylist.php");
$page_name="Modern Trade Report";


 if(isset($_POST['showModernTradeList']) && $_POST['showModernTradeList'] == 'yes') {	


	  
	  if(isset($_POST['from']) && $_POST['from']!=""){
	  	$_SESSION['mtfromdate'] = $_POST['from'];
		$_SESSION['mttodate'] = $_POST['to'];
	  } else {
	  	$_SESSION['mtfromdate'] = date('d M Y');
		$_SESSION['mttodate'] = date('d M Y');
	  }
	  
	  if(isset($_POST['salesman_id']) && $_POST['salesman_id']!="All")  
	   $_SESSION['mtsalesman'] = $_POST['salesman_id'];
	  else 
	   unset($_SESSION['mtsalesman']);
}


if(isset($_SESSION['mtsalesman']) && $_SESSION['mtsalesman']!="") { 
		$salesman = " AND s.salesman_id =".$_SESSION['mtsalesman']; }



if(!isset($_SESSION['mtfromdate']) && empty($_SESSION['mtfromdate'])) {  
	$_SESSION['mtfromdate'] = date('d M Y');  $_SESSION['mttodate'] = date('d M Y');}


	if(isset($_REQUEST['reset']) && $_REQUEST['reset'] == 'yes')
	{
		unset($_SESSION['mtfromdate']);
		unset($_SESSION['mttodate']);
		unset($_SESSION['mtsalesman']);
		header("Location: modern_trade_report.php");
	}
?>
<?php include("header.inc.php") ?>

<style type="text/css"> 
#page-heading { margin: 0 0 15px 2px; } 
.style1 { height:30px; background-color: #eaeaea; border:0px solid #666666; }
.salesman1 { height:30px; background-color: #eaeaea; border:1px solid #666666; }
.style2 { height:28px; border-top:1px solid #666666; }
.style3 { border-bottom:1px solid #666666; font-size:16px; font-weight:bold; }
.headerContainer { float:left; position:relative; width:100%;  }

.innerColumn1 { float:left; margin-left:25px; width:80px; height:auto; border:0px solid #000000;  }
.innerColumn { float:left; width:150px; height:auto; border:0px solid #000000;  }
.innerColumn5 { float:left; width:200px; border:0px solid #000000; padding: 5px 0px 5px 0px;}


.parentColumn1 { float:left; width:150px; height:auto; border:0px solid #000000; font-weight:bold;}
.parentColumn { float:left; width:150px; height:auto; border:0px solid #000000; }



.headerColumn1 { float:left; vertical-align: baseline; margin-left:30px; width:80px; border:0px solid #000000;  font-weight: bold; font-size:15px; }
.headerColumn { float:left; vertical-align: baseline; width:150px; border:0px solid #000000; font-weight: bold; font-size:15px;}
.headerColumn5 { float:left; vertical-align: baseline; width:200px; border:0px solid #000000; font-weight: bold; font-size:15px;}

.wrapper {  width: 100%; }
.ui-accordion .ui-accordion-content {
overflow: inherit;
}
.ui-accordion .ui-accordion-content {
padding: 0em;
}

h2 {
line-height: normal;
margin-bottom: auto; 
}
</style>

<!-- start content-outer -->
<div id="content-outer">
<!-- start content -->
<div id="content">
<div id="page-heading"><h1><span style="font-weight: bold; color:#d74343;">Modern Trade Report</span></h1></div>


<div id="content-table-inner">

<form name="form" method="post">
<table width="100%" border="0">

<tr><td width="9%"><h2>Salesman:</h2></td>
 <td width="10%"><select name="salesman_id" id="salesman_id" class="styledselect_form_5" style=""><?php echo $rtndata = $_objArrayList->GetSalesmenMenu($salsList, $_SESSION['mtsalesman']);?></select></td>

<td width="8%" align="right"><h2>From:</h2></td>
<td width="17%">
<input type="text" id="from" name="from" class="date" value="<?php if($_SESSION['mtfromdate']!='') { echo $_SESSION['mtfromdate']; } else { echo $_objAdmin->_changeDate(date('Y-m-d')); }?>"  readonly />
</td>


<td width="6%" align="right"><h2>To:</h2></td>
<td width="17%">
<input type="text" id="to" name="to" class="date" value="<?php if($_SESSION['mttodate']!='') { echo $_SESSION['mttodate']; } else { echo $_objAdmin->_changeDate(date('Y-m-d')); }?>"  readonly />
</td>

<td width="8%"><input type="button" value="Reset!" class="form-reset" onclick="location.href='modern_trade_report.php?reset=yes';" />
</td>
<td width="21%"><input name="showModernTradeList" type="hidden" value="yes" /><input name="submit" class="result-submit" type="submit" id="submit" value="Submit" /></td>
</tr>
</table>
</form>

<div class="wrapper"  style="width:99%; height:auto; padding:10px 0px 10px 0px;">
<div id="accordion">
<table width="100%" border="0" cellspacing="5" cellpadding="5">
  <tr class="salesman1">
    <td><div class="headerColumn1">Product</div></td>
    <td align="right"><div class="headerColumn">Salesman</div></td>
    <td align="right"><div class="headerColumn5">Store Name</div></td>
    <td align="right"><div class="headerColumn">Availability</div></td>
    <td align="right"><div class="headerColumn">Promo</div></td>
    <td align="right"><div class="headerColumn">Visibility</div></td>
  </tr>
  </table>

<?php 
	
	$aCategory = $_objArrayList->getItemList();
 	if(sizeof($aCategory)>0) {
 	foreach($aCategory as $catarr): ?>
 	<div class="accordion">
  	<h3><table width="100%" border="0">
	<tr>
	<td align="left" colspan="4"><!-- <div class="parentColumn1"> --><strong><?php echo $catarr->item_name;?></strong><!-- </div> --></td>
	<!-- <td align="right"><div class="parentColumn">Salesman</div></td>
	<td align="right"><div class="parentColumn">Store Name</td>
	<td align="right"><div class="parentColumn">AVAILABILITY</div></td>
	<td align="right"><div class="parentColumn">PROMO</div></td>
	<td align="right"><div class="parentColumn">Visibility</div></td> -->
	</tr>
	</table>
	</h3>
  	<div>
    <p>


    <?php 
	 $daterange = " AND MT.date_of_order >= '".date('Y-m-d', strtotime($_SESSION['mtfromdate']))."' AND MT.date_of_order <= '".date('Y-m-d', strtotime($_SESSION['mttodate']))."'";


    $mtdetails =	$_objAdmin->_getSelectList('table_modern_trade AS MT 
    	LEFT JOIN table_modern_trade_detail AS MTD ON MTD.trade_id = MT.trade_id
    	LEFT JOIN table_salesman AS s ON s.salesman_id = MT.salesman_id 
    	LEFT JOIN table_retailer AS R ON R.retailer_id = MT.retailer_id 
    	LEFT JOIN table_chain AS C ON C.chain_id = MT.chain_id',
    	"MTD.visibility, MTD.cur_stock, MTD.trade_detail_id, MT.date_of_order, s.salesman_name, R.retailer_name, MTD.item_id",'',"  MTD.item_id=".$catarr->item_id.$salesman.$daterange);

    	foreach ($mtdetails AS $mtValue) :
      $mtdetailsPhotos = array();
      $resultSetPhoto  = array ();

    	$mtdetailsPhotos = $_objAdmin->_getSelectList('table_image AS I',"I.image_url, I.tag_id, I.ref_id",''," I.image_type='22' AND I.ref_id=".$mtValue->trade_detail_id);
    	//echo "<pre>";
    	//print_r($mtdetailsPhotos);

    	$resultSetPhoto = $_objArrayList->changeArrayKeyName ($mtdetailsPhotos, tag_id);
		//print_r($resultSetPhoto);

      $stockPhoto = '';
      $visibilityPhoto = '';

    	if(isset($resultSetPhoto['17'])) {

    		$stockPhoto = '<a href="photo/'.$resultSetPhoto['17']->image_url.'" target="_blank">View Photo </a>';
    	}

    	if(isset($resultSetPhoto['16'])) {

    		$visibilityPhoto = '<a href="photo/'.$resultSetPhoto['16']->image_url.'" target="_blank">View Photo </a>';
    	}

    	?>

	<table width="100%" border="0">
	<tr style="border-bottom:1px solid #CCCCCC;">
	<td><div class="innerColumn1"><?php echo date('d M Y', strtotime($mtValue->date_of_order));?></div></td>
	<td align="right"><div class="innerColumn"><?php echo $mtValue->salesman_name;?></div></td>
	<td align="right"><div class="innerColumn5"><?php echo $mtValue->retailer_name;?></div></td>
	<td align="right"><div class="innerColumn"><?php echo $mtValue->cur_stock;?><br><?php echo $stockPhoto;?></div></td>
	<td align="right"><div class="innerColumn"><ul style="list-style:none;">
	<?php $mtProdetails = $_objAdmin->_getSelectList2('table_modern_trade_promo_detail AS MTPD LEFT JOIN table_promotion AS P ON P.promotion_id = MTPD.promotion_id',"P.promo_desc",''," MTPD.trade_detail_id=".$mtValue->trade_detail_id);
		
    	foreach ($mtProdetails AS $mtProValue) :?>
			<li style="padding:5px 0px 5px 0px;"><?php echo $mtProValue->promo_desc;?></li>
		<?php endforeach;?>
		</ul>
		</div></td>
	<td align="right"><div class="innerColumn"><?php echo $mtValue->visibility; ?><br><?php echo $visibilityPhoto;?></div></td>
	</tr>
	</table>


	<?php endforeach;?>
	</p>
  </div>
  </div>
<?php endforeach;?>
<? }?>

<table width="100%" border="0">
  <tr>
    <td colspan="5">&nbsp;</td>
  </tr>
  </table>
</div>
</div>
<div class="clear"></div>
</div>
<div class="clear">&nbsp;</div>
</div>
<!--  end content -->
<div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->
<div class="clear">&nbsp;</div> 
<!-- start footer -->         
<?php include("footer.php") ?>
<!-- end footer -->
<script src="javascripts/jquery-ui.js"></script>
 <script>
  $(function() {
  	var active = $( ".selector" ).accordion( "option", "active" );
    $( ".accordion" ).accordion({
	  header: "h3", 
	  active: -1,
      collapsible: true,
	  heightStyle: "content"
    });
  });
  </script>
<script>
    $(function() {
        $( "#from" ).datepicker({
			dateFormat: "d M yy",
            defaultDate: "w",
            changeMonth: true,
            numberOfMonths: 1,
            onSelect: function( selectedDate ) {
                $( "#to" ).datepicker( "option", "minDate", selectedDate );
            }
        });
        $( "#to" ).datepicker({
			dateFormat: "d M yy",
            defaultDate: "-w",
            changeMonth: true,
            numberOfMonths: 1,
            onSelect: function( selectedDate ) {
                $( "#from" ).datepicker( "option", "maxDate", selectedDate );
            }
        });
    });
</script>
</body>
</html>