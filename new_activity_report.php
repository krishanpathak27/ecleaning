<?php
include("includes/config.inc.php");
include("includes/function.php");
include("includes/globalarraylist.php");
$page_name="Activity Summary Report";
if($_SESSION['userLoginType'] == 5){
	$divisionIdString = implode(",", $divisionList);
	$condition1 = " division_id IN ($divisionIdString) ";
} else {
	$divisionIdString = implode(",", $divisionList);
	$condition1 = " ";
}
//echo $divisionIdString;
$divisionRec = $_objAdmin->_getSelectList2('table_division',"division_name as division,division_id",'',$condition1."ORDER BY division_name ASC");
if(isset($_POST['showReport']) && $_POST['showReport'] == 'yes'){
	if($_REQUEST['sal']!=""){
		$sal_list=$_REQUEST['sal'];
		$salesman=" AND o.salesman_id='".$sal_list."' ";
		$salesmanCond=" AND s.salesman_id='".$sal_list."' ";
	} else {
		$salesmanCond = " ";
	}

	if($_POST['from']!=""){
		$from_date=$_objAdmin->_changeDate($_POST['from']);	
	}
	if($_POST['division']!=""){
		$divisionIdString = $_POST['division'];
	}
} else {
	$from_date= $_objAdmin->_changeDate(date("Y-m-d"));
	unset($_SESSION['SalAct']);		
}
if(isset($_REQUEST['reset']) && $_REQUEST['reset'] == 'yes')
{
	/*unset($_SESSION['SalAttList']);	
	unset($_SESSION['FromAttList']);	
	unset($_SESSION['ToAttList']);
	unset($_SESSION['AttendanceStatus']);
	unset($_SESSION['DivisionAttList']);*/
	header("Location: new_activity_report.php");
}
if($_REQUEST['sal']!=''){
	$SalName=$_objAdmin->_getSelectList('table_salesman','salesman_name',''," salesman_id='".$_REQUEST['sal']."'"); 
	$sal_name=$SalName[0]->salesman_name;
} else {
	$sal_name="All Salesman";
}
$order_by_sal="asc";
$List= "ORDER BY s.salesman_name asc, a.activity_date";
if(isset($_SESSION['DivisionAttList']) && $_SESSION['DivisionAttList']!=""){
	$division=" s.division_id='".$_SESSION['DivisionAttList']."' and " ;
}
$date = date("Y-m-d",strtotime($from_date));
$year = date("Y",strtotime($from_date));
$month = date("n",strtotime($from_date));
$day = date("j",strtotime($from_date));
?>

<?php include("header.inc.php") ?>
<style type="text/css">
	body{
		overflow-x: hidden;
	}
</style>
<script type="text/javascript">
	function showloader(){
		$('#Report').hide();
		$('#loader').show();
	}
    function PrintElem(elem)
    {
        Popup($(elem).html());
    }

    function Popup(data) 
    {
		
        var mywindow = window.open('', 'Report');
		
        mywindow.document.write('<html><head><title>Activity Summary Report</title>');
		mywindow.document.write('<table><tr><td><b>From Date:</b> <?php echo $from_date; ?></td><td><b>To Date:</b> <?php echo $to_date; ?></td></tr></table>');
        mywindow.document.write('</head><body >');
        mywindow.document.write(data);
        mywindow.document.write('</body></html>');

        mywindow.print();
        mywindow.close();
        return true;
    }
	$(document).ready(function(){
		$('#loader').hide();
		$('#loader').html('<div id="loader" align="center"><img src="images/ajax-loader.gif" /><br/>Please Wait...</div>');
		$('#Report').show();
		
		<?php if($_POST['submit']=='Export to Excel'){ ?>
			tableToExcel('report_export', 'Activity Summary Report', 'Activity Summary Report.xls');	
		<?php } ?>
	});
</script>
<script src="javascripts/dateNextPrev.js" type="text/javascript"></script>
<!-- start content-outer -->
<input name="pagename" type="hidden"  id="pagename" value="new_activity_report.php" />
<div id="content-outer">
	<!-- start content -->
	<div id="content">
		<div id="loader" style="position:absolute; margin-left:40%; margin-top:10%;"></div>
		<div id="page-heading">
			<h1>
				<span style="color: #d74343; font-family: Tahoma; font-weight: bold;">
					Activity Summary Report
				</span>
			</h1>
		</div>
		<table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">
			<tr>
				<td>
					<!--  start content-table-inner -->
					<div id="content-table-inner" style="width:65%">
						<div id="page-heading" align="left" >
							<form name="frmPre" id="frmPre" method="post" action="" enctype="multipart/form-data" >
								<table border="0" width="100%" cellpadding="0" cellspacing="0">
									<tr>
										<td>
											<h3>
												Salesman Name:
											</h3>
											<h6> 
												<select name="sal" id="sal" class="styledselect_form_5" style="" >
													<?php echo $rtndata = $_objArrayList->GetSalesmenMenu($salsList, $_REQUEST['sal']);?>
												</select>
											</h6>
										</td>
										<td>
											<h3>
												Division :
											</h3>
											<h6> 
												<select name="division" id="division" class="styledselect_form_5" >
													<option value="">All</option>
													<?php 
														foreach ($divisionRec as $key => $value) {?>
															<option value="<?php echo $value->division_id?>" <?php echo ($_POST['division'] == $value->division_id)?'selected':''?>><?php echo $value->division ?></option>
														<?php }
													?>
												</select>
											</h6>
										</td>
										<td>
											<h3>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Date: </h3>
											<h6>
												<img src="css/images/prev.png" height="18" width="18" onclick="dateFromPrev();"> 
												<input type="text" id="from" name="from" class="date" style="width:150px" value="<?php  echo $from_date;?>"  readonly /> 
												<img src="css/images/next.png" height="18" width="18" onclick="dateFromNext();">
											</h6>
										</td>
										<td>
											<h3></h3>
											<input name="submit" class="result-submit" type="submit" id="submit" value="View Details" onclick="showloader()";/> 
											<input type="button" value="Reset!" class="form-reset" onclick="location.href='new_activity_report.php?reset=yes';" />
										</td>
										<td colspan="2"></td>
									</tr>
									<tr>
										<td colspan="6">
											<input name="showReport" type="hidden" value="yes" />		
											<input type="button" value="Print" class="result-submit" onclick="PrintElem('#Report')" />
											<a id="dlink"  style="display:none;"></a>
											<input type="submit" name="submit" value="Export to Excel" class="result-submit"  >
										</td>
									</tr>	
								</table>
							</form>
						</div>
						<table border="0" width="100%" cellpadding="0" cellspacing="0">
							<tr valign="top">
								<td>
									<div id="Report" style="width:64%; overflow-y:auto">
										<table  border="0" width="100%" cellpadding="0" cellspacing="0" id="report_export" name="report_export">
											<tr  bgcolor="#6E6E6E" style="color: #fff;font-weight: bold;" >
												<td style="padding:10px;"><div align="center">Salesman Name</div></td>
												<td style="padding:10px;"><div align="center">Salesman Code</div></td>
												<td style="padding:10px;"><div align="center">Salesman Division</div></td>
												<td style="padding:10px;"><div align="center">No Of Distributors + Dealers Assigned</div></td>
												<td style="padding:10px;"><div align="center">No of Distributor Assigned</div></td>
												<td style="padding:10px;"><div align="center">No of Dealers Assigned</div></td>
												<td style="padding:10px;"><div align="center">No of Total Dealer Visited</div></td>
												<td style="padding:10px;"><div align="center">No Of Total Distributor Visited</div></td>
												<td style="padding:10px;"><div align="center">No Of Total Distributor Ordered (Quantity/Amount)</div></td>
												<td style="padding:10px;"><div align="center">No of Total Dealers Ordered (Quantity/Amount)</div></td>
												<td style="padding:10px;"><div align="center">Total Distributors Not Ordered</div></td>
												<td style="padding:10px;"><div align="center">Total Dealers Not Ordered</div></td>
												<td style="padding:10px;"><div align="center">New Distributors Added</div></td>
												<td style="padding:10px;"><div align="center">New Dealers Added</div></td>
												
												<td style="padding:10px;"><div align="center">Total Quantity/Amount from Distributor and Retailers (Quantity/Amount)</div></td>
												<td style="padding:10px;"><div align="center">Attendance In Time</div></td>
												<td style="padding:10px;"><div align="center">Attendance Out Time</div></td>
												<td style="padding:10px;"><div align="center">Daily DSR Comments</div></td>
											</tr>
											<?php
											$salesman_list = $_objAdmin->_getSelectList2('table_salesman as s LEFT JOIN table_division as td ON s.division_id=td.division_id',"s.salesman_id,s.salesman_name,s.salesman_code,td.division_name",''," s.division_id IN (".$divisionIdString.") ".$salesmanCond." ORDER BY s.salesman_name ASC");
											//if(is_array($salesman_list)){
												//foreach ($salesman_list as $key => $value){

$actArr = array();
$routeRetDis = $_objAdmin->_getSelectList2("table_activity as ta
LEFT JOIN table_salesman as s ON ta.salesman_id=s.salesman_id 
LEFT JOIN table_division as tdiv ON s.division_id=tdiv.division_id 
LEFT JOIN table_route_scheduled as trs ON ta.salesman_id=trs.salesman_id AND trs.month='".$month."' AND trs.year='".$year."'
LEFT JOIN table_route_schedule_details as trsd ON trs.route_schedule_id=trsd.route_schedule_id AND trsd.assign_day='".$day."'
LEFT JOIN table_route_retailer as trr ON trr.route_id=trsd.route_id 
LEFT JOIN table_retailer as tret ON trr.retailer_id=tret.retailer_id AND trr.status='R'
LEFT JOIN table_distributors as tdist ON trr.retailer_id=tdist.distributor_id AND trr.status='D'
","s.salesman_id,s.salesman_name,s.salesman_code,tdiv.division_name,count(trr.status) AS totalrouteRetDis,count(tret.retailer_id) AS totalRouteRet,count(tdist.distributor_id) AS totalRouteDist
",''," ta.activity_type IN (2) AND trr.status!='' AND ta.activity_date='".$date."' AND s.division_id IN (".$divisionIdString.") ".$salesmanCond." GROUP BY ta.salesman_id ORDER BY s.salesman_name ");

foreach ($routeRetDis as $key => $value) {
	if(!isset($actArr[$value->salesman_id])){
		$actArr[$value->salesman_id] = array();
		$actArr[$value->salesman_id]['salesman_name'] = $value->salesman_name;
		$actArr[$value->salesman_id]['salesman_code'] = $value->salesman_code;
		$actArr[$value->salesman_id]['division_name'] = $value->division_name;
		$actArr[$value->salesman_id]['totalrouteRetDis'] = $value->totalrouteRetDis;
		$actArr[$value->salesman_id]['totalRouteRet'] = $value->totalRouteRet;
		$actArr[$value->salesman_id]['totalRouteDist'] = $value->totalRouteDist;
	} else {
		if($actArr[$value->salesman_id]['totalrouteRetDis']=="")
		$actArr[$value->salesman_id]['totalrouteRetDis'] = $value->totalrouteRetDis;
		if($actArr[$value->salesman_id]['totalRouteRet']=="")
		$actArr[$value->salesman_id]['totalRouteRet'] = $value->totalRouteRet;
		if($actArr[$value->salesman_id]['totalRouteDist']=="")
		$actArr[$value->salesman_id]['totalRouteDist'] = $value->totalRouteDist;
	}
	
}

$commentattendance=$_objAdmin->_getSelectList2("table_activity AS ta 
LEFT JOIN table_salesman AS s ON ta.salesman_id = s.salesman_id
LEFT JOIN table_division as tdiv ON s.division_id=tdiv.division_id 
LEFT JOIN table_customer_comments as tc ON ta.ref_id=tc.comment_id
LEFT JOIN dsr_comment as dsr ON ta.ref_type=dsr.dsr_id
LEFT JOIN table_retailer as tr ON ta.ref_id=tr.retailer_id AND ta.ref_type='5'
LEFT JOIN table_distributors as td ON ta.ref_id=td.distributor_id AND ta.ref_type='24'
LEFT JOIN table_customer as tcust ON ta.ref_id=tcust.customer_id AND ta.ref_type='25'
","s.salesman_id,s.salesman_name,s.salesman_code,tdiv.division_name,
CASE WHEN ta.activity_type='32' THEN tc.comment_desc END AS comment,
CASE WHEN ta.activity_type='11' THEN ta.start_time END AS intime,
CASE WHEN ta.activity_type='12' THEN ta.start_time END AS outtime,
CASE WHEN ta.activity_type='5' THEN count(tr.retailer_id) END AS totalRet,
CASE WHEN ta.activity_type='24' THEN count(td.distributor_id) END AS totalDis,
CASE WHEN ta.activity_type='25' THEN count(tcust.customer_id) END AS totalCust
",'',"ta.activity_type IN(5,24,25,11,12,32) AND ta.activity_date='".$date."' AND s.division_id IN (".$divisionIdString.") ".$salesmanCond." GROUP BY ta.activity_type,s.salesman_id ORDER BY s.salesman_name" );

foreach ($commentattendance as $key => $value) {
	if(!isset($actArr[$value->salesman_id])){
		$actArr[$value->salesman_id] = array();
		$actArr[$value->salesman_id]['salesman_name'] = $value->salesman_name;
		$actArr[$value->salesman_id]['salesman_code'] = $value->salesman_code;
		$actArr[$value->salesman_id]['division_name'] = $value->division_name;
		$actArr[$value->salesman_id]['dsr_comment'] = $value->comment;
		$actArr[$value->salesman_id]['intime'] = $value->intime;
		$actArr[$value->salesman_id]['outtime'] = $value->outtime;
		$actArr[$value->salesman_id]['totalRetailer'] = $value->totalRet;
		$actArr[$value->salesman_id]['totalDistirbutor'] = $value->totalDis;
		$actArr[$value->salesman_id]['totalCustomer'] = $value->totalCust;
	} else {
		if($actArr[$value->salesman_id]['dsr_comment']=="")
		$actArr[$value->salesman_id]['dsr_comment'] = $value->comment;
		if($actArr[$value->salesman_id]['intime']=="")
		$actArr[$value->salesman_id]['intime'] = $value->intime;
		if($actArr[$value->salesman_id]['outtime']=="")
		$actArr[$value->salesman_id]['outtime'] = $value->outtime;
		if($actArr[$value->salesman_id]['totalRetailer']=="")
		$actArr[$value->salesman_id]['totalRetailer'] = $value->totalRet;
		if($actArr[$value->salesman_id]['totalDistirbutor']=="")
		$actArr[$value->salesman_id]['totalDistirbutor'] = $value->totalDis;
		if($actArr[$value->salesman_id]['totalCustomer']=="")
		$actArr[$value->salesman_id]['totalCustomer'] = $value->totalCust;
	}
	
}

$routeRetailerOfSalesmanActivity = $_objAdmin->_getSelectList2("table_activity AS A 
LEFT JOIN table_salesman AS s ON A.salesman_id = s.salesman_id 
LEFT JOIN table_division as tdiv ON s.division_id=tdiv.division_id 
LEFT JOIN table_route_scheduled AS RS ON RS.salesman_id = A.salesman_id AND DATE_FORMAT(A.activity_date, '%Y-%c') = CONCAT(RS.year,'-',RS.month) 
LEFT JOIN table_route_schedule_details AS RSD ON RSD.route_schedule_id = RS.route_schedule_id AND RSD.assign_day = DATE_FORMAT(A.activity_date, '%e')
LEFT JOIN table_order AS O ON O.order_id = A.ref_id AND O.order_type IN ('Yes','No')  AND O.retailer_id >0  AND A.activity_type = 3 
LEFT JOIN table_retailer AS R3 ON R3.retailer_id = O.retailer_id AND R3.status = 'A' 
LEFT JOIN table_route_retailer AS RR ON RR.route_id = RSD.route_id AND RR.status = 'R' AND RR.retailer_id = R3.retailer_id AND A.activity_type = 3 
LEFT JOIN table_order AS O2 ON O2.order_id = A.ref_id AND O2.order_type IN ('Yes')  AND O2.retailer_id >0 AND A.activity_type = 3
LEFT JOIN table_order AS O3 ON O3.order_id = A.ref_id AND O3.order_type IN ('No')  AND O3.retailer_id >0 AND A.activity_type = 3

LEFT JOIN table_order_detail AS OD ON OD.order_id = O2.order_id

LEFT JOIN table_retailer AS R4 ON R4.retailer_id = O2.retailer_id AND R3.status = 'A' 
LEFT JOIN table_route_retailer AS RR2 ON RR2.route_id = RSD.route_id AND RR2.status = 'R' AND RR2.retailer_id = R4.retailer_id AND A.activity_type = 3 
LEFT JOIN table_retailer AS R ON R.retailer_id = A.ref_id AND A.activity_type = 5 
LEFT JOIN table_retailer AS R2 ON R2.retailer_id = A.ref_id AND A.activity_type =5 AND R2.display_outlet = 'hot'
 ",
	"OD.order_id,s.salesman_id,s.salesman_name,s.salesman_code,tdiv.division_name,RSD.route_id, SUM(OD.acc_quantity) AS ttlItemsQty, COUNT(DISTINCT(RR.retailer_id)) AS plannedRouteRetailerTotalCalls,  COUNT(DISTINCT(RR2.retailer_id)) AS plannedRouteRetailerProductiveCalls,  COUNT(DISTINCT(R3.retailer_id)) AS ttlOrders, COUNT(DISTINCT(R4.retailer_id)) AS ttlProductiveCalls, COUNT(R.retailer_id) AS ttlAddedRet,  COUNT(R2.retailer_id) AS ttlInterestedCustomer,count(O3.retailer_id) as retailer_no_order,sum(OD.acc_total) as total_amount,A.activity_date", ''," A.activity_date='".$date."' AND A.activity_type IN (3,5) AND RSD.route_id IS NOT NULL AND s.division_id IN (".$divisionIdString.") ".$salesmanCond." GROUP BY A.activity_date, A.salesman_id, OD.order_id, RSD.route_id");

foreach ($routeRetailerOfSalesmanActivity as $key => $value) {

	if(!isset($actArr[$value->salesman_id])){		
		$actArr[$value->salesman_id] = array();
		$actArr[$value->salesman_id]['visited_retailer'] = "0";
		$actArr[$value->salesman_id]['order_quantity_retailer'] = "0";
		$actArr[$value->salesman_id]['order_amount_retailer'] = "0";
		$actArr[$value->salesman_id]['no_order_retailer'] = "0";
		$actArr[$value->salesman_id]['salesman_name'] = $value->salesman_name;
		$actArr[$value->salesman_id]['salesman_code'] = $value->salesman_code;
		$actArr[$value->salesman_id]['division_name'] = $value->division_name;
		$actArr[$value->salesman_id]['visited_retailer'] = $value->plannedRouteRetailerTotalCalls;
		$actArr[$value->salesman_id]['order_quantity_retailer'] = $value->ttlItemsQty;
		$actArr[$value->salesman_id]['order_amount_retailer'] = $value->total_amount;
		$actArr[$value->salesman_id]['no_order_retailer'] = $value->retailer_no_order;
	} else {
		$actArr[$value->salesman_id]['visited_retailer'] = ($actArr[$value->salesman_id]['visited_retailer'] + $value->plannedRouteRetailerTotalCalls);
//		$actArr[$value->salesman_id]['order_quantity_retailer'] = ($actArr[$value->salesman_id]['order_quantity_retailer'] + $value->ttlItemsQty);
//		$actArr[$value->salesman_id]['order_amount_retailer'] = ($actArr[$value->salesman_id]['order_amount_retailer'] + $value->total_amount);
		if($value->order_id!=""){
			$actArr[$value->salesman_id]['order_quantity_retailer'][$value->order_id] = $value->ttlItemsQty;
			$actArr[$value->salesman_id]['order_amount_retailer'][$value->order_id] = $value->total_amount;
		}
		$actArr[$value->salesman_id]['no_order_retailer'] = ($actArr[$value->salesman_id]['no_order_retailer'] + $value->retailer_no_order);
	}
}

$routeDistributorOfSalesmanActivity = $_objAdmin->_getSelectList2("table_activity AS A 
LEFT JOIN table_salesman AS s ON A.salesman_id = s.salesman_id 
LEFT JOIN table_division as tdiv ON s.division_id=tdiv.division_id 
LEFT JOIN table_route_scheduled AS RS ON RS.salesman_id = A.salesman_id AND DATE_FORMAT(A.activity_date, '%Y-%c') = CONCAT(RS.year,'-',RS.month) 
LEFT JOIN table_route_schedule_details AS RSD ON RSD.route_schedule_id = RS.route_schedule_id AND RSD.assign_day = DATE_FORMAT(A.activity_date, '%e') 
LEFT JOIN table_order AS O ON O.order_id = A.ref_id AND O.order_type IN ('Yes','No')  AND O.retailer_id = 0  AND A.activity_type = 3 
LEFT JOIN table_distributors AS D3 ON D3.distributor_id = O.distributor_id  AND D3.status = 'A'	
LEFT JOIN table_route_retailer AS RR ON RR.route_id = RSD.route_id AND RR.status = 'D' AND RR.retailer_id = D3.distributor_id AND A.activity_type = 3 
LEFT JOIN table_order AS O2 ON O2.order_id = A.ref_id AND O2.order_type IN ('Yes')  AND O2.retailer_id = 0 AND A.activity_type = 3 
LEFT JOIN table_order AS O3 ON O3.order_id = A.ref_id AND O3.order_type IN ('No')  AND O3.retailer_id = 0 AND A.activity_type = 3 
LEFT JOIN table_order_detail AS OD ON OD.order_id = O2.order_id
LEFT JOIN table_distributors AS D4 ON D4.distributor_id = O2.distributor_id AND D4.status = 'A' 
LEFT JOIN table_route_retailer AS RR2 ON RR2.route_id = RSD.route_id AND RR2.status = 'D' AND RR2.retailer_id = D4.distributor_id AND A.activity_type = 3 
LEFT JOIN table_distributors AS D ON D.distributor_id = A.ref_id AND A.activity_type = 24 
LEFT JOIN table_distributors AS D2 ON D2.distributor_id = A.ref_id AND A.activity_type =24 AND D2.display_outlet = 'hot' ",
"s.salesman_id,s.salesman_name,s.salesman_code,tdiv.division_name,RSD.route_id,SUM(OD.acc_quantity) AS ttlItemsQty, COUNT(DISTINCT(RR.retailer_id)) AS plannedRouteDistributorTotalCalls,  COUNT(DISTINCT(RR2.retailer_id)) AS plannedRouteDistributorProductiveCalls, COUNT(DISTINCT(D3.distributor_id)) AS ttlOrders, COUNT(DISTINCT(D4.distributor_id)) AS ttlProductiveCalls, COUNT(D.distributor_id) AS ttlAddedDis, COUNT( D2.distributor_id ) AS ttlInterestedCustomer,count(O3.retailer_id) as distributor_no_order,O2.order_id,sum(OD.acc_total) as total_amount, A.activity_date", '',
	" A.activity_date = '".$date."' AND A.activity_type IN (3,5) AND RSD.route_id IS NOT NULL AND s.division_id IN (".$divisionIdString.") ".$salesmanCond." GROUP BY A.activity_date, A.salesman_id,OD.order_id, RSD.route_id");

foreach ($routeDistributorOfSalesmanActivity as $key => $value) {
	if(!isset($actArr[$value->salesman_id])){
		$actArr[$value->salesman_id] = array();
		$actArr[$value->salesman_id]['visited_distributor'] = "0";
		$actArr[$value->salesman_id]['order_quantity_distributor'] = "0";
		$actArr[$value->salesman_id]['order_amount_distributor'] = "0";
		$actArr[$value->salesman_id]['no_order_distributor'] = "0";
		$actArr[$value->salesman_id]['salesman_name'] = $value->salesman_name;
		$actArr[$value->salesman_id]['salesman_code'] = $value->salesman_code;
		$actArr[$value->salesman_id]['division_name'] = $value->division_name;
		$actArr[$value->salesman_id]['visited_distributor'] = $value->plannedRouteDistributorTotalCalls;
		$actArr[$value->salesman_id]['order_quantity_distributor'] = $value->ttlItemsQty;
		$actArr[$value->salesman_id]['order_amount_distributor'] = $value->total_amount;
		$actArr[$value->salesman_id]['no_order_distributor'] = $value->distributor_no_order;
	} else {
		$actArr[$value->salesman_id]['visited_distributor'] = ($actArr[$value->salesman_id]['visited_distributor'] + $value->plannedRouteDistributorTotalCalls);
		if($value->order_id!=""){
			$actArr[$value->salesman_id]['order_quantity_distributor'][$value->order_id] = $value->ttlItemsQty;
			$actArr[$value->salesman_id]['order_amount_distributor'][$value->order_id] = $value->total_amount;
		}
		//$actArr[$value->salesman_id]['order_quantity_distributor'] = ($actArr[$value->salesman_id]['order_quantity_distributor'] + $value->ttlItemsQty);
		//$actArr[$value->salesman_id]['order_amount_distributor'] = ($actArr[$value->salesman_id]['order_amount_distributor'] + $value->total_amount);
		$actArr[$value->salesman_id]['no_order_distributor'] = ($actArr[$value->salesman_id]['no_order_distributor'] + $value->distributor_no_order);
	}
}


												if(is_array($actArr)){
													foreach ($actArr as $key => $value) {
														$quantityDist	= 0;
														$amountDist		= 0;
														$quantityRet	= 0;
														$amountRet		= 0;
														if(is_array($value['order_quantity_distributor']) && is_array($value['order_amount_distributor'])){
															$quantityDist = array_sum($value['order_quantity_distributor']);
															$amountDist = array_sum($value['order_amount_distributor']);
														}
														if(is_array($value['order_quantity_retailer']) && is_array($value['order_amount_retailer'])){
															$quantityRet = array_sum($value['order_quantity_retailer']);
															$amountRet = array_sum($value['order_amount_retailer']);
														}
											?>			
											<tr  style="border-bottom:2px solid #6E6E6E;" >
												<td style="padding:10px;"><div align="center"><?php echo ($value['salesman_name']!='')?$value['salesman_name']:'-';?></div></td>
												<td style="padding:10px;"><div align="center"><?php echo ($value['salesman_code']!='')?$value['salesman_code']:'-';?></div></td>
												<td style="padding:10px;"><div align="center"><?php echo ($value['division_name']!='')?$value['division_name']:'-';?></div></td>
												<td style="padding:10px;"><div align="center"><?php echo ($value['totalrouteRetDis']!='')?$value['totalrouteRetDis']:'-';?></div></td>
												<td style="padding:10px;"><div align="center"><?php echo ($value['totalRouteDist']!='')?$value['totalRouteDist']:'-';?></div></td>
												<td style="padding:10px;"><div align="center"><?php echo ($value['totalRouteRet']!='')?$value['totalRouteRet']:'-';?></div></td>
												<td style="padding:10px;"><div align="center"><?php echo ($value['visited_retailer']!='')?$value['visited_retailer']:'0';?></div></td>
												<td style="padding:10px;"><div align="center"><?php echo ($value['visited_distributor']!='')?$value['visited_distributor']:'0';?></div></td>
												<td style="padding:10px;"><div align="center">
													<?php echo (($quantityDist!="")?$quantityDist:'0')." / ".(($amountDist!="")?number_format($amountDist,2):'0.00');?>
												</div></td>
												<td style="padding:10px;"><div align="center">
												<?php echo (($quantityRet!="")?$quantityRet:'0')." / ".(($amountRet!="")?number_format($amountRet,2):'0.00');?></div></td>
												<td style="padding:10px;"><div align="center"><?php echo ($value['no_order_distributor']!='')?$value['no_order_distributor']:'0'?></div></td>
												<td style="padding:10px;"><div align="center"><?php echo ($value['no_order_retailer']!='')?$value['no_order_retailer']:'0'?></div></td>
												<td style="padding:10px;"><div align="center"><?php echo ($value['totalDistirbutor']!='')?$value['totalDistirbutor']:'-';?></div></td>
												<td style="padding:10px;"><div align="center"><?php echo ($value['totalRetailer']!='')?$value['totalRetailer']:'-';?></div></td>
												
												<td style="padding:10px;"><div align="center">
													<?php 
														echo ($quantityDist + $quantityRet)." / ".number_format(($amountDist + $amountRet),2);
													?>
												</div></td>
												<td style="padding:10px;"><div align="center"><?php echo ($value['intime']!='')?$value['intime']:'-';?></div></td>
												<td style="padding:10px;"><div align="center"><?php echo ($value['outtime']!='')?$value['outtime']:'-';?></div></td>
												<td style="padding:10px;"><div align="center"><?php echo ($value['dsr_comment']!='')?$value['dsr_comment']:'-';?></div></td>
											</tr>
											<?php 
												}
											} else { ?>
												<tr  style="border-bottom:2px solid #6E6E6E;" align="center" >
													<td style="padding:10px;" colspan="19">Report Not Available</td>
												</tr>
											<?php } 
										?>
										</table>
									</div>
								</td>
							</tr>
							<tr>
								<td><img src="images/blank.gif" width="695" height="1" alt="blank" /></td>
								<td></td>
							</tr>
						</table>
						<div class="clear"></div>
					</div>
					<!--  end content-table-inner  -->
				</td>
				<td id="tbl-border-right"></td>
			</tr>
		</table>
		<div class="clear">&nbsp;</div>
	</div>
	<div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->
<div class="clear">&nbsp;</div> 
<!-- start footer -->         
<?php include("footer.php") ?>
<?php unset($_SESSION['SalAttList']); ?>
<!-- end footer -->
<script src="javascripts/jquery-1.8.2.js" type="text/javascript"></script>
<script src="javascripts/jquery-ui.js"></script>
<script type="text/javascript" src="javascripts/validate.js"></script>
<script type="text/javascript">
// Popup window code
function newPopup(url) {
	popupWindow = window.open(
		url,'popUpWindow','height=500,width=600,left=200,top=100,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no,status=yes')
}
</script>
<script>
    $(function() {
        $( "#from" ).datepicker({
			dateFormat: "d M yy",
            defaultDate: "w",
            changeMonth: true,
            numberOfMonths: 1,
            onSelect: function( selectedDate ) {
                $( "#to" ).datepicker( "option", "minDate", selectedDate );
            }
        });
        $( "#to" ).datepicker({
			dateFormat: "d M yy",
            defaultDate: "-w",
            changeMonth: true,
            numberOfMonths: 1,
            onSelect: function( selectedDate ) {
                $( "#from" ).datepicker( "option", "maxDate", selectedDate );
            }
        });
    });
</script>
<script type='text/javascript'>//<![CDATA[ 
var tableToExcel = (function () {
        var uri = 'data:application/vnd.ms-excel;base64,'
        , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table><tr><td><b>From Date:</b> <?php echo $from_date; ?></td><td><b>To Date:</b> <?php echo $to_date; ?></td></tr></table><table>{table}</table></body></html>'
        , base64 = function (s) { return window.btoa(unescape(encodeURIComponent(s))) }
        , format = function (s, c) { return s.replace(/{(\w+)}/g, function (m, p) { return c[p]; }) }
        return function (table, name, filename) {
            if (!table.nodeType) table = document.getElementById(table)
            var ctx = { worksheet: name || 'Worksheet', table: table.innerHTML }

            document.getElementById("dlink").href = uri + base64(format(template, ctx));
            document.getElementById("dlink").download = filename;
            document.getElementById("dlink").click();

        }
    })()

//]]>  
</script>
</body>
</html>