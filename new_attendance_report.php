<?php
include("includes/config.inc.php");
include("includes/function.php");
include("includes/globalarraylist.php");

$_objAdmin = new Admin();
$page_name="Attendance Report";

if(isset($_POST['showReport']) && $_POST['showReport'] == 'yes')
{	
	if($_POST['sal_id']!="" && $_POST['sal_id']!="All") 
	{
		$_SESSION['SalAttList']=$_POST['sal_id'];	
	} else {
		unset($_SESSION['SalAttList']);	
	}
	

	/*****************Create Session for month********************************/
	if($_POST['month']!="" && is_numeric($_POST['month']))
	{
		$_SESSION['dismonth']= $_POST['month'];
	}else
	{
		unset($_SESSION['dismonth']);
	}
	/****************Create Session for year********************************/
	if($_POST['year']!="" && is_numeric($_POST['year']))
	{
		$_SESSION['disCyear']= $_POST['year'];
	}else
	{
		unset($_SESSION['disCyear']);
	}

} else {
	// $_SESSION['FromAttList']= $_objAdmin->_changeDate(date("Y-m-d"));
	// $_SESSION['ToAttList']= $_objAdmin->_changeDate(date("Y-m-d"));
	$_SESSION['dismonth']= date("m");
	$_SESSION['disCyear']= date("Y");	
}

if(isset($_REQUEST['reset']) && $_REQUEST['reset'] == 'yes')
{
	unset($_SESSION['SalAttList']);	
	// unset($_SESSION['FromAttList']);	
	// unset($_SESSION['ToAttList']);
	unset($_SESSION['disCyear']);	
	unset($_SESSION['dismonth']);
	/*unset($_SESSION['BranchAttList']);
	unset($_SESSION['DivisionAttList']);	*/
	header("Location: new_attendance_report.php");
}

/*if($_SESSION['BranchAttList']!='')
{ 
	$branchCondition = " AND s.branch_id=".$_SESSION['BranchAttList']; 
}
if($_SESSION['DivisionAttList']!='')
{ 
	$divisionCondition = " AND s.division_id=".$_SESSION['DivisionAttList']; 
}*/
if($_SESSION['SalAttList']!=''){
	$salesman = " AND s.salesman_id = '".$_SESSION['SalAttList']."'";
}

if(isset($_SESSION['dismonth']) && isset($_SESSION['disCyear']) && $_SESSION['dismonth'] !="" && $_SESSION['disCyear'] != "")
{
	$days = date('t', strtotime($_SESSION['disCyear'].'-'.$_SESSION['dismonth']));
	$fromDate = date('Y-m-d', strtotime($_SESSION['disCyear'].'-'.$_SESSION['dismonth']));
	$toDate = date('Y-m-t', strtotime($_SESSION['disCyear'].'-'.$_SESSION['dismonth']));
}

$SalArray = $_objAdmin->_getSelectList('table_salesman as s
	LEFT JOIN state as st on st.state_id = s.state
	LEFT JOIN city as c on c.city_id = s.city
	LEFT JOIN table_web_users as w on w.salesman_id = s.salesman_id','s.salesman_id, s.salesman_name,s.salesman_code,w.username, st.state_name, c.city_name',''," AND s.status='A' AND s.salesman_id>0 $salesman ORDER BY s.salesman_name");
// echo "<pre>";
// print_r($SalArray);
// exit();



/*if(isset($_POST['export']) && $_POST['export'] == 'Export to Excel'){
	header("Location:export.inc.php?export_new_attendance_report&sal=".$_POST['sal_id']);
}*/


function timeDiff($datet1,$datet2){
	$datetime1 = new DateTime($datet1);
	$datetime2 = new DateTime($datet2);
	$interval = $datetime1->diff($datetime2);

	$min = $interval->format('%i');
	if($min < 10){
		$min = "0".$min;
	}else{
		$min = $min;
	}
	$timediff = $interval->format('%h').".".$min;

	return $timediff;
}

?>
<?php include("header.inc.php") ?>
<script src="javascripts/dateNextPrev.js" type="text/javascript"></script>
<script type="text/javascript">

function PrintElem(elem)
{
	Popup($(elem).html());
}
function Popup(data) 
{
	var mywindow = window.open('', 'Report');
	mywindow.document.write('<html><head><title>Attendance Report</title>');
	mywindow.document.write('<table><tr><td><b>From Date:</b> <?php echo $fromDate; ?></td><td><b>To Date:</b> <?php echo $toDate; ?></td></tr></table>');
	/*optional stylesheet*/ //mywindow.document.write('<link rel="stylesheet" href="main.css" type="text/css" />');
	mywindow.document.write('</head><body >');
	mywindow.document.write(data);
	mywindow.document.write('</body></html>');
	mywindow.print();
	mywindow.close();
	return true;
}
$(document).ready(function(){
	$('#export').click(function(){		
		tableToExcel('report_export', 'Attendance Report', 'Salesman Monthly-Attendance Report.xls');

		//window.location.href="export.inc.php?export_new_attendance_report";
	})
});
</script>
<script type="text/javascript">
// Popup window code
function photoPopup(url) {
	popupWindow = window.open(
		url,'popUpWindow','height=500,width=800,left=100,top=100,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no,status=yes')
}
</script>

<style type="text/css">
	td{
		font-weight: bold;
	}
</style>

<!-- start content-outer -->
<input name="pagename" type="hidden"  id="pagename" value="admin_order_list.php" />
<div id="content-outer">
	<!-- start content -->
	<div id="content">
		<div id="page-heading"><h1><span style="color: #d74343; font-family: Tahoma; font-weight: bold;">Attendance Report</span></h1></div>
		<table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">
			<tr>
				<td>
					<!--  start content-table-inner -->
					<div id="content-table-inner">
						<div id="page-heading" align="left" >
							<form name="frmPre" id="frmPre" method="post" action="" enctype="multipart/form-data" >
								<table border="0" width="100%" cellpadding="0" cellspacing="0">
									<tr>
										
										<td id="cityLevel">
											<h3>Salesman:</h3>
											<h6>
												<select name="sal_id" id="sal_id" class="menulist" >
													<?php echo $rtndata = $_objArrayList->GetSalesmenMenu($salsList, $_SESSION['SalAttList'], 'flex');?>
												</select>
											</h6>
										</td>
										<td><h3>Select Month: </h3>
											<h6>
												<select id="month" name="month" class="menulist">
													<?php echo $mList = $_objArrayList->getMonthList3($_SESSION['dismonth']) ?>
												</select>
											</h6>
										</td>
										<td><h3>Select Year: </h3>
											<h6>
												<select id="year" name="year" class="menulist">
													<?php echo $YList = $_objArrayList->getYearList2($_SESSION['disCyear']) ?>
												</select>
											</h6>
										</td>										
									</tr>
<tr>
	<td colspan="5"><h3></h3><input name="submit" class="result-submit" type="submit" id="submit" value="View Details" />
		<input type="button" value="Reset!" class="form-reset" onclick="location.href='new_attendance_report.php?reset=yes';" />
		<input name="showReport" type="hidden" value="yes" />
		<input type="button" value="Print" class="result-submit" onclick="PrintElem('#Report')" />
		<a id="dlink"  style="display:none;"></a>
		<input name="export" class="result-submit" type="submit" id="export" value="Export to Excel">
	</td>
	<td></td>
</tr>
</table>
</form>
</div>
<div id="Report" style="width: 1000px; height: 600px; overflow: auto;">
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
		<tr valign="top">
			<td>
				<table  border="0" width="100%" cellpadding="0" cellspacing="0" id="report_export" name="report_export">
					<div id="Report">
						<tr  bgcolor="#6E6E6E" style="color: #fff;font-weight: bold;" >
							<td style="padding:10px;min-width: 100px;max-width: 100px">Salesman Name</td>
							<td style="padding:10px;min-width: 80px;max-width: 80px">Salesman Code</td>
							<!-- <td style="padding:10px;min-width: 100px;max-width: 100px">State</td>
							<td style="padding:10px;min-width: 100px;max-width: 100px">District</td> -->
							<?php 
							for ($i=1; $i <= $days ; $i++) { 
								$date = $_objAdmin->_changeDate($i.'-'.$_SESSION['dismonth'].'-'.$_SESSION['disCyear']);
								?>								
								<td style="padding:10px;min-width: 70px;max-width: 70px;text-align: center;"><?php echo $date; ?></td>
								<?php	} ?>
								<td style="padding:10px;min-width: 70px;max-width: 70px;text-align: center;">Total</td>
							</tr>

							<?php 

							$salArr = $_objAdmin->_getSelectList2("table_activity_salesman_attendance AS A",' A.salesman_id, A.activity_date, A.activity_type,A.start_time,A.end_time',''," A.activity_type IN(11,12) AND A.activity_date BETWEEN '".$fromDate."' AND '".$toDate."' GROUP BY A.salesman_id, A.activity_date,A.activity_type ORDER BY A.salesman_id,A.activity_date");

							// echo "<pre>";
							// print_r($salArr);exit;
							$dataSet = array();
							foreach ($salArr as $key => $value) {
								$dataSet[$value->salesman_id];
								if($value->activity_date != ""){
									$dataSet[$value->salesman_id][$value->activity_date][] = $value;
								}

							}
							// echo '<pre>';
							// print_r($dataSet);//exit;
							$time = '';
							$time2 = '';
							$time3 = '';
							$marked = '';
							foreach ($SalArray as $valueS) {

								?>

								<tr style="border-bottom: 2px solid #000; ">							
									<td rowspan="4" style="padding:10px;min-width: 100px;max-width: 100px">
										<?php echo $valueS->salesman_name;?>									
									</td>
									<td rowspan="4" style="padding:10px;min-width: 80px;max-width: 80px">
										<?php echo $valueS->salesman_code;?>									
									</td>
									<!-- <td rowspan="4" style="padding:10px;min-width: 100px;max-width: 100px">
										<?php echo $valueS->state_name;?>									
									</td>
									<td rowspan="4" style="padding:10px;min-width: 100px;max-width: 100px">
										<?php echo $valueS->city_name;?>									
									</td> -->

									<?php 
									$j = 0;
									for ($i=1; $i <= $days ; $i++) 
									{ 
										$attDate = date('Y-m-d',strtotime($_SESSION['disCyear'].'-'.$_SESSION['dismonth'].'-'.$i));

										if(isset($dataSet[$valueS->salesman_id][$attDate])){

											if($dataSet[$valueS->salesman_id][$attDate][0]->activity_type == 11){
												$time = $dataSet[$valueS->salesman_id][$attDate][0]->start_time;
											}else{
											$time = '-';
											}
											$j++;
										}else{
											$time = '-';
										}
										?>								
										<td style="padding:10px;min-width: 70px;max-width: 70px;text-align: center;"><?php echo $time; ?></td>							
										<?php	
									} 
									?>
									<td style="padding:10px;min-width: 70px;max-width: 70px;text-align: center;"><?php echo $j; ?></td>
									
								</tr>

								<!-- ***********************************
								*********************************** -->
								<tr style="border-bottom: 2px solid #000; ">
									<?php 
									$j = 0;
									for ($i=1; $i <= $days ; $i++) 
									{ 
										$attDate = date('Y-m-d',strtotime($_SESSION['disCyear'].'-'.$_SESSION['dismonth'].'-'.$i));

										if(isset($dataSet[$valueS->salesman_id][$attDate])){

											if($dataSet[$valueS->salesman_id][$attDate][1]->activity_type == 12){
												$time2 = $dataSet[$valueS->salesman_id][$attDate][1]->start_time;
											}else{
											$time2 = '-';
										}
											$j++;
										}else{
											$time2 = '-';
										}
										?>								
										<td style="padding:10px;min-width: 70px70px70px;max-width: 70px70px70px;text-align: center;"><?php echo $time2; ?></td>							
										<?php	
									} 
									?>
									<td style="padding:10px;min-width: 70px70px70px;max-width: 70px70px70px;text-align: center;"><?php echo $j; ?></td>
									
								</tr>
								<tr style="border-bottom: 2px solid #000; ">
									<?php 
									$j = 0;
									for ($i=1; $i <= $days ; $i++) 
									{ 
										$attDate = date('Y-m-d',strtotime($_SESSION['disCyear'].'-'.$_SESSION['dismonth'].'-'.$i));

										if(isset($dataSet[$valueS->salesman_id][$attDate])){

											if($dataSet[$valueS->salesman_id][$attDate][0]->activity_type == 11 && $dataSet[$valueS->salesman_id][$attDate][1]->activity_type == 12){

												$datet1 = $attDate." ".$dataSet[$valueS->salesman_id][$attDate][0]->start_time;
												$datet2 = $attDate." ".$dataSet[$valueS->salesman_id][$attDate][1]->start_time;

												$time3 = timeDiff($datet1,$datet2);

											}else{
											$time3 = '-';
										}
											$j++;
										}else{
											$time3 = '-';
										}
										?>								
										<td style="padding:10px;min-width: 70px70px;max-width: 70px70px;text-align: center;"><?php echo $time3; ?></td>							
										<?php	
									} 
									?>
									<td style="padding:10px;min-width: 70px70px;max-width: 70px70px;text-align: center;"><?php echo $j; ?></td>
									
								</tr>
								<tr style="border-bottom: 2px solid #000; ">
									<?php 
									$j = 0;
									for ($i=1; $i <= $days ; $i++) 
									{ 
										$attDate = date('Y-m-d',strtotime($_SESSION['disCyear'].'-'.$_SESSION['dismonth'].'-'.$i));

										$weekDay = date("l",strtotime($attDate));

										if(isset($dataSet[$valueS->salesman_id][$attDate])){

											if($dataSet[$valueS->salesman_id][$attDate][0]->activity_type == 11 && $dataSet[$valueS->salesman_id][$attDate][1]->activity_type == 12){

												$datet1 = $attDate." ".$dataSet[$valueS->salesman_id][$attDate][0]->start_time;
												$datet2 = $attDate." ".$dataSet[$valueS->salesman_id][$attDate][1]->start_time;

												$time3 = timeDiff($datet1,$datet2);

												if($time3 > 8.30){
													$marked = 'P';
												}else{
													$marked = 'A';
												}
											}else{
											$marked = 'A';
											}
											$j++;
										}elseif($weekDay == "Sunday" || $weekDay == "Saturday"){
											$marked = 'W';
										}else{
											$marked = 'A';
										}
										?>								
										<td style="padding:10px;min-width: 70px;max-width: 70px;text-align: center;"><?php echo $marked; ?></td>							
										<?php	
									} 
									?>
									<td style="padding:10px;min-width: 70px;max-width: 70px;text-align: center;"><?php echo $j; ?></td>
									
								</tr>

								<!-- ************************************************
								************************************************** -->
							<?php 	}
								?>
							</div>
						</table>
					</td>
				</tr>
				<tr><td><img src="images/blank.gif" width="695" height="1" alt="blank" /></td><td></td></tr>
			</table>
		</div>
		<div class="clear"></div>
	</div>
	<!--  end content-table-inner  -->
</td>
<td id="tbl-border-right"></td>
</tr>
<!--<tr>
<th class="sized bottomleft"></th>
<td id="tbl-border-bottom">&nbsp;</td>
<th class="sized bottomright"></th>
</tr>-->
</table>
<div class="clear">&nbsp;</div>
</div>
<!--  end content -->
<div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->
<div class="clear">&nbsp;</div>
<div style="clear: both;">&nbsp;</div> 
<div style="clear: both;">&nbsp;</div>
<!-- start footer -->         
<?php include("footer.php") ?>
<?php unset($_SESSION['SalAttList']);	?>
<!-- end footer -->
<script src="javascripts/jquery-1.8.2.js" type="text/javascript"></script>
<script src="javascripts/jquery-ui.js"></script>
<script type="text/javascript" src="javascripts/validate.js"></script>
<script type="text/javascript">
// Popup window code
function newPopup(url) {
	popupWindow = window.open(
		url,'popUpWindow','height=500,width=600,left=200,top=100,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no,status=yes')
}
</script>
<!-- <script>
$(function() {
$( "#from" ).datepicker({
dateFormat: "d M yy",
defaultDate: "w",
changeMonth: true,
numberOfMonths: 1,
onSelect: function( selectedDate ) {
$( "#to" ).datepicker( "option", "minDate", selectedDate );
}
});
$( "#to" ).datepicker({
dateFormat: "d M yy",
defaultDate: "-w",
changeMonth: true,
numberOfMonths: 1,
onSelect: function( selectedDate ) {
$( "#from" ).datepicker( "option", "maxDate", selectedDate );
}
});
});
</script> -->
<script type='text/javascript'>//<![CDATA[ 
	var tableToExcel = (function () {
		var uri = 'data:application/vnd.ms-excel;base64,'
		, template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table><tr><td><b>From Date:</b> <?php echo $fromDate; ?></td><td><b>To Date:</b> <?php echo $toDate; ?></td></tr></table><table>{table}</table></body></html>'
		, base64 = function (s) { return window.btoa(unescape(encodeURIComponent(s))) }
		, format = function (s, c) { return s.replace(/{(\w+)}/g, function (m, p) { return c[p]; }) }
		return function (table, name, filename) {

			if (!table.nodeType) table = document.getElementById(table);

			table1 = table.cloneNode(true);

			table1.innerHTML = table1.innerHTML.replace(/<td style="padding:10px;" class="action_day" [^~]+?<\/td>\n/g, '');
			var ctx = { worksheet: name || 'Worksheet', table: table1.innerHTML }

			document.getElementById("dlink").href = uri + base64(format(template, ctx));
			document.getElementById("dlink").download = filename;
			document.getElementById("dlink").click();

		}
	})()

//]]>  
</script>
</body>
</html>