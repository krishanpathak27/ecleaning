<?php 
/* Modify By : Abhishek Jaiswal
* Date : 11 feb 2016
* Desc : filter issue
*/
include("includes/config.inc.php");
      include("includes/function.php");
	  include("includes/globalarraylist.php");
	$page_name="New Channel Partner";



	if(isset($_POST['showReport']) && $_POST['showReport'] == 'yes')
	{
		if(!isset($_POST['sal'])){
			$salesmanCond="";

			#this condition added by Maninder for view report salesman hierarchy wise
			#on 2016-11-11
			if($_SESSION['userLoginType'] == 5){
				$objArrayList = new ArrayList();
				$salesmanList = $objArrayList->SalesmanArrayList();

				if(sizeof($salesmanList)>0) {  
				$salList = implode(",", $salesmanList);
				//$salesmanCondition = ' AND O.salesman_id IN ('.$salList.')'; 
				    $salesmanCond = " s.salesman_id IN (".$salList.") AND ";
				} 
			}
		} else if($_POST['sal'] !="" && $_POST['sal'] !="All"){
			$salesmanCond=" s.salesman_id='".$_POST['sal']."' AND ";
			$sals_id = $_POST['sal'];
		}else if($_POST['sal'] !="" && $_POST['sal'] =="All"){
			$salesmanCond="";

			if($_SESSION['userLoginType'] == 5){

				$objArrayList = new ArrayList();
				$salesmanList = $objArrayList->SalesmanArrayList();

				if(sizeof($salesmanList)>0) {  
				$salList = implode(",", $salesmanList);
				//$salesmanCondition = ' AND O.salesman_id IN ('.$salList.')'; 
				    $salesmanCond = " s.salesman_id IN (".$salList.") AND ";
				} 
			}
		}

		if(isset($_POST['customer_class'])){			
			$_SESSION['customerClass'] = $_POST['customer_class'];
		}

		if(!isset($_POST['cust_type'])){
			$cust_typeCond = "";
			$cust_classCond = "";
		} else if($_POST['cust_type'] !="" && $_POST['cust_type'] !="All"){
			if($_POST['cust_type'] =="1"){
				$cust_typeCond=" ta.activity_type='5' AND ";
				if($_POST['customer_class'] !="" && $_POST['customer_class'] !="all"){
					$cust_classCond = " tr.relationship_id='".$_POST['customer_class']."' AND";
				}else{
					$cust_classCond = "";
				}
			}
			else if($_POST['cust_type'] =="2"){
				$cust_typeCond=" ta.activity_type='24' AND ";
				if($_POST['customer_class'] !="" && $_POST['customer_class'] !="all"){					
					$cust_classCond = " td.relationship_id='".$_POST['customer_class']."' AND";
				}else{
					$cust_classCond = "";
				}
			}
			else if($_POST['cust_type'] =="3"){
				$cust_typeCond=" ta.activity_type='25' AND ";
				if($_POST['customer_class'] !="" && $_POST['customer_class'] !="all"){
					$cust_classCond = "";
				}else{
					$cust_classCond = "";
				}
			}
		} else if($_POST['cust_type'] !="" && $_POST['cust_type'] =="All"){
			$cust_typeCond = "";
			$cust_classCond = "";
		}
		if($_POST['division_id']!="") 
	{
		$_SESSION['DivisionAttList']=$_POST['division_id'];	
	}

		/*if (sizeof($divisionList)>0 && $_SESSION['userLoginType'] == 5 && in_array('4', $divisionList)) {
		 	$divisionCond = " tdiv.division_id = 4 AND ";
		 }

		elseif(sizeof($divisionList)>0 && $_SESSION['userLoginType'] == 5 ) {
			$divisionIdString = implode(",", $divisionList);
			$divisionCond = " tdiv.division_id IN (".$divisionIdString.") AND ";
		} else {
			$divisionCond = "";
		}*/
      
		if($_POST['from']!="") 
		{
			$from_date=$_objAdmin->_changeDate($_POST['from']);	
		}
		if($_POST['to']!="") 
		{
			$to_date=$_objAdmin->_changeDate($_POST['to']);	
		}
		
		if($_POST['city']!="")
		{
			$cityr=" AND tr.city='".$_POST['city']."'";
			$cityd=" AND td.city='".$_POST['city']."'";
			$cityc=" AND tc.city='".$_POST['city']."'";
		} else {
			$cityr="";
			$cityd="";
			$cityc="";
		}
	} else {
		$from_date= $_objAdmin->_changeDate(date("Y-m-d"));
		$to_date= $_objAdmin->_changeDate(date("Y-m-d"));
		$salesmanCond="";
		if($_SESSION['userLoginType'] == 5){
			$objArrayList = new ArrayList();
			$salesmanList = $objArrayList->SalesmanArrayList();

			if(sizeof($salesmanList)>0) {  
			$salList = implode(",", $salesmanList);
			//$salesmanCondition = ' AND O.salesman_id IN ('.$salList.')'; 
			    $salesmanCond = " s.salesman_id IN (".$salList.") AND ";
			} 
		}
	}




	// AJAY@2016-06-24

		if(sizeof($divisionList)>0 && $_SESSION['userLoginType'] == 5 ) {

			$divisionIdString = implode(",", $divisionList);
			$divisionCond = " tdiv.division_id IN (".$divisionIdString.") AND ";

			// $sid=$_SESSION['salesmanId'];
		 //    $SalDiv=$_objAdmin->_getSelectList('table_salesman','division_id',''," salesman_id='".$sid."'"); 
			// $div_id=$SalDiv[0]->division_id;
			// $divisionCond = " s.division_id ='".$div_id."'  AND ";

		} else {
			$divisionCond = "";
		}



		// AJAY@2016-06-24

		if($_SESSION['userLoginType'] == 3 ) {
			$did=' TD1.distributor_id="'.$_SESSION['distributorId'].'" and';
		}




	if(isset($_REQUEST['reset']) && $_REQUEST['reset'] == 'yes')
	{
		/* unset($_SESSION['SalAttList']);	
		unset($_SESSION['FromAttList']);	
		unset($_SESSION['ToAttList']); */
		unset($_SESSION['customerClass']);
		unset($_SESSION['DivisionAttList']);
		header("Location: new_channel_patner_added.php");
	}
	if($_SESSION['DivisionAttList']!='' && $_SESSION['DivisionAttList']!='all'){
    $divisionCondition="  s.division_id='".$_SESSION['DivisionAttList']."' and";
} 
	if($sal_id!=''){
	$SalName=$_objAdmin->_getSelectList('table_salesman','salesman_name',''," salesman_id='".$sals_id."'"); 
	$sal_name=$SalName[0]->salesman_name;
	} 
	if($_POST['city']!=''){
	$CityName=$_objAdmin->_getSelectList2('city','city_name',''," city_id='".$_POST['city']."'"); 
	$city_name=$CityName[0]->city_name;
	} else {
	$city_name="All City";
	}?>

<?php include("header.inc.php") ?>
<script type="text/javascript">

    function PrintElem(elem)
    {
        Popup($(elem).html());
    }

    function Popup(data) 
    {
		
        var mywindow = window.open('', 'Report');
		
        mywindow.document.write('<html><head><title>New Added Customer</title>');
		mywindow.document.write('<table><tr><td><b>Salesman Name:</b> <?php echo $sal_name; ?></td><td><b>City Name:</b> <?php echo $city_name; ?></td><td><b>From Date:</b> <?php echo $from_date; ?></td><td><b>To Date:</b> <?php echo $to_date; ?></td></tr></table>');
        /*optional stylesheet*/ //mywindow.document.write('<link rel="stylesheet" href="main.css" type="text/css" />');
        mywindow.document.write('</head><body >');
        mywindow.document.write(data);
        mywindow.document.write('</body></html>');

        mywindow.print();
        mywindow.close();
        return true;
    }

$(document).ready(function()
{
<?php if($_POST['submit']=='Export to Excel'){ ?>
tableToExcel('report_export', 'New Added Customer', 'New Added Customer.xls');
<?php } ?>

	//Desc: get options in Customer class on change of Customer Type
	//By: Maninder On 05-04-2016
	if($("#cust_type").val()!="All"){
		customerClass($("#cust_type").val());
		}
	$("#cust_type").change(function(){
	var customer = $(this).val();		
	customerClass(customer);
   });
	function customerClass(customer){
	   if(customer=="All"){
			type='all';
		}else if(customer==1){
			type='retailer';
		}else if(customer==2){
			type='distributor';
		}else if(customer==3){
			type='customer';
		}
		if(type!='all'){
			$.ajax({
				method:"GET",
				url: "ajax_customer_geo_tag.php",
				data: { customer: type },
				success: function(result){
					$("#customer_class").html(result);
				}
			});
		} else {
			$("#customer_class").html("<option value='all'>All</option>");
		}
	}
	//By: Maninder On 05-04-2016 ends here.......
});

</script>
<script src="javascripts/dateNextPrev.js" type="text/javascript"></script>
<!-- start content-outer -->
<input name="pagename" type="hidden"  id="pagename" value="admin_order_list.php" />
<div id="content-outer">
<!-- start content -->
<div id="content">
<div id="page-heading"><h1><span style="color: #d74343; font-family: Tahoma; font-weight: bold;">New Added Customer</span></h1></div>
<table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">
<tr>
	<td>
	<!--  start content-table-inner -->
	<div id="content-table-inner">
	
	<div id="page-heading" align="left" >
	<form name="frmPre" id="frmPre" method="post" action="" enctype="multipart/form-data" >
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
	<tr>
		<td>
			<h3>Salesman: </h3>
			<h6>
				<select name="sal" id="sal" class="menulist">
					<?php echo $salesmanOptionList = $_objArrayList->GetSalesmenMenu($salsList, $sals_id,'flex');?>
				</select>
			</h6>
		</td>
		<td>
			<?php echo $cityList = $_objArrayList->getCityList($_POST['city']);?>
		</td>
		<!-- here code starts -->
		<td>
			<h3>Customer Type: </h3>
			<h6>
				<select name="cust_type" id="cust_type" class="menulist">
					<?php
					if(isset($_POST['cust_type']))
						echo $customerType = $_objArrayList->getCustomerType($_POST['cust_type']);
					else
						echo $customerType = $_objArrayList->getCustomerType();
					?>
				</select>
			</h6>
		</td>
		
		<td><h3>Division: </h3><h6>
		<select name="division_id" id="division_id" class="styledselect_form_3" style="width:150px;">
			<option value="all">All</option>
			<?php $divisionList=$_objAdmin->_getSelectList('table_division',"division_id,division_name",''," order by division_name asc ");
			foreach($divisionList as $divisionVal){
				
				//if($_SESSION['DivisionAttList'] == $divisionVal->division_id){ $select="selected"; }else {$select="";}
				if($_POST['division_id'] == $divisionVal->division_id){ $select="selected"; }else {$select="";}
			?>
			<option value="<?php echo $divisionVal->division_id; ?>" <?php echo $select; ?>><?php echo $divisionVal->division_name; ?></option>
			<?php	
			}
			?>
		</select>
		<!-- here code ends -->
		
		
		<td>
		<!-- here code ends -->
		
		
		<td><h3>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;From Date: </h3><h6><img src="css/images/prev.png" height="18" width="18" onclick="dateFromPrev();"> <input type="text" id="from" name="from" class="date" style="width:100px" value="<?php  echo $from_date;?>"  readonly /> <img src="css/images/next.png" height="18" width="18" onclick="dateFromNext();"></h6></td>
		
		<td><h3>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;To Date: </h3><h6><img src="css/images/prev.png" height="18" width="18" onclick="dateToPrev();"> <input type="text" id="to" name="to" class="date" style="width:100px" value="<?php echo $to_date; ?>"  readonly /> <img src="css/images/next.png" height="18" width="18" onclick="dateToNext();"></h6></td>
		<!-- <td>
		
		<h3></h3>
		
		</td>
		<td colspan="2"></td> -->
		</tr>
		<tr>
		<td colspan="6"><input name="showReport" type="hidden" value="yes" />
		<a id="dlink"  style="display:none;"></a>
		<input  type="submit" value="Export to Excel" name="submit" class="result-submit"  >
		<input type="button" value="Print" class="result-submit" onclick="PrintElem('#Report')" />
		<!-- <a href='new_retailer_report_year_graph.php?y=<?php echo checkFromdate($from_date)."&salID=".$_POST['sal']."&city=".$_POST['city']; ?>' target="_blank"><input type="button" value=" Show Graph" class="result-submit" /></a> -->
		<input name="submit" class="result-submit" type="submit" id="submit" value="View Details" />
	    <input type="button" value="Reset!" class="form-reset" onclick="location.href='new_channel_patner_added.php?reset=yes';" />
		</td>
	</tr>
	</table>
	</form>
	</div>
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
	<?php
	if($sal_id!=""){ 
	?>
	<tr valign="top">
		<td>
		<table border="0" width="100%" cellpadding="0" cellspacing="0" id="id-form">
			<tr bgcolor="#A52A2A" style="color: #fff;font-weight: bold;">
				<td align="center" style="padding:10px;">Select Salesman</td>
			</tr>
		</table>
		</td>
		<?php } else {  ?>
		<td>
		<div id="Report" style="width:1100px; overflow:auto;min-height:145px;max-height:400px;">
		<table  border="0" width="100%" cellpadding="0" cellspacing="0" id="report_export" name="report_export" style="text-align:center;">
			<tr  bgcolor="#6E6E6E" style="color: #fff;font-weight: bold;" >
				<td style="padding:10px;" width="10%">Salesman Name</td>
				<td style="padding:10px;" width="10%">Salesman Code</td>
				<td style="padding:10px;" width="10%">Salesman Division</td>
				<td style="padding:10px;" width="10%">Dealer Code</td>
				<td style="padding:10px;" width="10%">Dealer Name</td>
				<!-- <td style="padding:10px;" width="10%">Dealer Class</td> -->
				<td style="padding:10px;" width="10%">Dealer Pincode</td>
				<!-- <td style="padding:10px;" width="10%">Retailer Mapped Distributor</td> -->
				<td style="padding:10px;" width="10%">Distributor Code</td>
				<td style="padding:10px;" width="10%">Distributor Name</td>
				<!-- <td style="padding:10px;" width="10%">Distributor Class</td> -->
				<td style="padding:10px;" width="10%">Distributor Pincode</td>
				
				<td style="padding:10px;" width="10%">Date</td>
				<td style="padding:10px;" width="10%">Route</td>
				<td style="padding:10px;" width="10%">Latitude</td>
				<td style="padding:10px;" width="10%">Longitude</td>
				<td style="padding:10px;" width="10%">Customer Type</td>
				
				<td style="padding:10px;" width="10%">State</td>
				<td style="padding:10px;" width="10%">State Code</td>
				
				<td style="padding:10px;" width="10%">District</td>
				<td style="padding:10px;" width="10%">District Code</td>
				
				<td style="padding:10px;" width="10%">Address</td>
				<td style="padding:10px;" width="10%">Phone Number</td>
				<td style="padding:10px;" width="10%">Contact Person</td>
				<td style="padding:10px;" width="10%">Contact Number</td>
			</tr>
		<?php
		if($from_date!="" && $to_date!=""){
			$custData = $_objAdmin->_getSelectList2("table_activity as ta

				LEFT JOIN table_salesman as s ON ta.salesman_id=s.salesman_id

				LEFT JOIN table_division as tdiv ON s.division_id=tdiv.division_id

				LEFT JOIN table_retailer as tr ON ta.ref_id=tr.retailer_id AND ta.ref_type='5' $cityr

				LEFT JOIN table_distributors as td ON ta.ref_id=td.distributor_id AND ta.ref_type='24' $cityd

			LEFT JOIN table_relationship AS REL ON REL.relationship_id = tr.relationship_id

            LEFT JOIN table_relationship AS RELD ON RELD.relationship_id = td.relationship_id

				LEFT JOIN table_customer as tc ON ta.ref_id=tc.customer_id AND ta.ref_type='25' $cityc


				LEFT JOIN table_distributors AS TD1 ON TD1.distributor_id = tr.distributor_id

				LEFT JOIN table_relationship AS RELD1 ON RELD1.relationship_id = TD1.relationship_id

				",'s.salesman_code,s.salesman_name,ta.activity_type,tdiv.division_name,ta.activity_date,
				CASE WHEN ta.activity_type="5" THEN tr.retailer_id WHEN ta.activity_type="24" THEN td.distributor_id WHEN ta.activity_type="25" THEN tc.customer_id END AS customer_id,
				CASE WHEN ta.activity_type="5" THEN tr.retailer_name WHEN ta.activity_type="24" THEN td.distributor_name WHEN ta.activity_type="25" THEN tc.customer_name END AS customer_name,
				CASE WHEN ta.activity_type="5" THEN tr.retailer_code WHEN ta.activity_type="24" THEN td.distributor_code WHEN ta.activity_type="25" THEN tc.customer_code END AS customer_code,
				CASE WHEN ta.activity_type="5" THEN "Retailer" WHEN ta.activity_type="24" THEN "Distributor" WHEN ta.activity_type="25" THEN "Shakti-Partner" END AS customer_type,
				CASE WHEN ta.activity_type="5" THEN tr.display_outlet WHEN ta.activity_type="24" THEN td.display_outlet WHEN ta.activity_type="25" THEN tc.display_outlet END AS outlet,
				CASE WHEN ta.activity_type="5" THEN tr.state WHEN ta.activity_type="24" THEN td.state WHEN ta.activity_type="25" THEN tc.state END AS customer_state,
				CASE WHEN ta.activity_type="5" THEN tr.city WHEN ta.activity_type="24" THEN td.city WHEN ta.activity_type="25" THEN tc.city END AS customer_district,
				CASE WHEN ta.activity_type="5" THEN tr.taluka_id WHEN ta.activity_type="24" THEN td.taluka_id WHEN ta.activity_type="25" THEN tc.taluka_id END AS customer_taluka,
				CASE WHEN ta.activity_type="5" THEN tr.market_id WHEN ta.activity_type="24" THEN td.market_id WHEN ta.activity_type="25" THEN tc.market_id END AS customer_market,
				CASE WHEN ta.activity_type="5" THEN tr.contact_person WHEN ta.activity_type="24" THEN td.contact_person WHEN ta.activity_type="25" THEN tc.contact_person END AS contact_person,
				CASE WHEN ta.activity_type="5" THEN tr.contact_number WHEN ta.activity_type="24" THEN td.contact_number WHEN ta.activity_type="25" THEN tc.contact_number END AS contact_number,
				CASE WHEN ta.activity_type="5" THEN tr.retailer_address WHEN ta.activity_type="24" THEN td.distributor_address WHEN ta.activity_type="25" THEN tc.customer_address END AS address,
				CASE WHEN ta.activity_type="5" THEN tr.retailer_phone_no WHEN ta.activity_type="24" THEN td.distributor_phone_no WHEN ta.activity_type="25" THEN tc.customer_phone_no END AS contact,
				CASE WHEN ta.activity_type="5" THEN tr.lat WHEN ta.activity_type="24" THEN td.lat WHEN ta.activity_type="25" THEN tc.lat END AS latitude,
				CASE WHEN ta.activity_type="5" THEN tr.lng WHEN ta.activity_type="24" THEN td.lng WHEN ta.activity_type="25" THEN tc.lng END AS longitude,
				REL.relationship_desc AS retailer_class,tr.zipcode AS retailer_pincode, RELD.relationship_desc AS distributor_class,TD1.distributor_name as ret_distributor_name,TD1.distributor_code as ret_distributor_code, RELD1.relationship_desc AS ret_distributor_class,td.zipcode AS distributor_pincode','',"$salesmanCond $cust_typeCond  $cust_classCond  $divisionCond $divisionCondition $did ta.activity_type IN (5,24,25) AND ta.activity_date BETWEEN '".date('Y-m-d', strtotime($from_date))."' AND '".date('Y-m-d', strtotime($to_date))."' ORDER BY ta.activity_date ASC");
				$no_ret = 0;


				// echo '<pre>';
				// print_r($custData);
				
			foreach ($custData as $key => $value) {
				if($value->customer_district!=""){
				$dateCode=explode('-', $value->activity_date);
				if($value->activity_type=="5"){
					$custTypeStatus = 'R';
				} else if($value->activity_type=="24"){
					$custTypeStatus = 'D';
				} else if($value->activity_type=="25"){
					$custTypeStatus = 'C';
				} else {
					$custTypeStatus = '';
				}
				$auRoute=$_objAdmin->_getSelectList('table_route_scheduled as rs left join table_route_schedule_details sd  on rs.route_schedule_id=sd.route_schedule_id left join table_route as r on r.route_id=sd.route_id  left join table_route_retailer as rr on rr.route_id=r.route_id',"r.route_id,r.route_name,sd.assign_day,rs.month,rs.year,rr.retailer_id",''," rs.month='".$dateCode[1]."' and rs.year='".$dateCode[0]."' and sd.assign_day='".$dateCode[2]."' and rr.retailer_id='".$value->customer_id."' AND rr.status='".$custTypeStatus."'");
				$state_city = $_objAdmin->_getSelectList2('state as ts
					LEFT JOIN city as tc ON ts.state_id=tc.state_id AND tc.status="A" AND tc.city_id="'.$value->customer_district.'" 
					LEFT JOIN table_taluka as tt ON tc.city_id=tt.city_id AND tt.status="A" AND tt.taluka_id="'.$value->customer_taluka.'" 
					LEFT JOIN table_markets as tm ON tt.taluka_id=tm.taluka_id AND tm.status="A" AND tm.market_id="'.$value->customer_market.'"','ts.state_name,ts.state_code,tc.city_name,tc.city_code,tt.taluka_name,tt.taluka_code,tm.market_name,tm.market_code','',' ts.status="A" AND ts.state_id="'.$value->customer_state.'"');
				?>
				<tr  style="border-bottom:2px solid #6E6E6E;" >
				<td style="padding:10px;" width="10%"><?php echo $value->salesman_name;?> </td>
				<td style="padding:10px;" width="10%"><?php echo ($value->salesman_code!='')?$value->salesman_code:'-';?> </td>
				<td style="padding:10px;" width="10%"><?php echo ($value->division_name != '')?$value->division_name:'-';?> </td>
				<td style="padding:10px;" width="10%"><?php echo ($value->activity_type=="5")?($value->customer_code!='')?$value->customer_code:'-':'-'?></td>
				<td style="padding:10px;" width="10%"><?php echo ($value->activity_type=="5")?$value->customer_name:'-'?></td>

  
             <!-- <td style="padding:10px;" width="10%"><?php echo ($value->retailer_class!="")?$value->retailer_class:'-'?></td> -->

              <td style="padding:10px;" width="10%"><?php echo ($value->retailer_pincode!="")?$value->retailer_pincode:'-'?></td>

              <!-- <td style="padding:10px;" width="10%"><?php //echo ($value->ret_distributor_name!="")?$value->ret_distributor_name:'-'?></td> -->


			<!-- 	<td style="padding:10px;" width="10%"><?php //echo ($value->activity_type=="24")?($value->customer_code!='')?$value->customer_code:'-':'-'?></td> -->

				<td style="padding:10px;" width="10%">
				<?php 
					if($value->activity_type=="24"){
						if($value->customer_code!=''){
							echo $value->customer_code;
						}else{
							echo '-';
						}

					}else{

						if($value->ret_distributor_code!=''){
							echo $value->ret_distributor_code;
						}else{
							echo '-';
						}

					}

				?>
					
				</td>
				<!-- <td style="padding:10px;" width="10%"><?php //echo ($value->activity_type=="24")?$value->customer_name:'-'?></td> -->



				<td style="padding:10px;" width="10%">
				<?php 
					if($value->activity_type=="24"){
						if($value->customer_name!=''){
							echo $value->customer_name;
						}else{
							echo '-';
						}

					}else{

						if($value->ret_distributor_name!=''){
							echo $value->ret_distributor_name;
						}else{
							echo '-';
						}

					}

				?>
					
				</td>


				<!-- <td style="padding:10px;" width="10%"><?php //echo ($value->distributor_class!="")?$value->distributor_class:'-'?></td> -->

				<!--  -->

				<td style="padding:10px;" width="10%"><?php echo ($value->distributor_pincode!="")?$value->distributor_pincode:'-'?></td>




				
				<td style="padding:10px;" width="10%"><?php echo $_objAdmin->_changeDate($value->activity_date);?> </td>
				<td style="padding:10px;" width="10%"><?php echo ($auRoute[0]->route_name !='')?$auRoute[0]->route_name:'-'; ?></td>

				<td style="padding:10px;" width="10%"><?php echo ($value->latitude !='')?$value->latitude:'-'; ?></td>
				<td style="padding:10px;" width="10%"><?php echo ($value->longitude !='')?$value->longitude:'-'; ?></td>
				
				<td style="padding:10px;" width="10%"><?php echo $value->customer_type?> </td>
			    				
				<td style="padding:10px;" width="10%"><?php echo ($state_city[0]->state_name!="")?$state_city[0]->state_name:'-';?></td>
				<td style="padding:10px;" width="10%"><?php echo ($state_city[0]->state_code!="")?$state_city[0]->state_code:'-';?></td>
				<td style="padding:10px;" width="10%"><?php echo ($state_city[0]->city_name!="")?$state_city[0]->city_name:'-';?></td>
				<td style="padding:10px;" width="10%"><?php echo ($state_city[0]->city_code!="")?$state_city[0]->city_code:'-';?></td>
				
				
				<td style="padding:10px;" width="10%"><?php echo ($value->address!="")?$value->address:'-';?> </td>
				<td style="padding:10px;" width="10%"><?php echo $value->contact;?> </td>
				<td style="padding:10px;" width="10%"><?php echo $value->contact_person;?> </td>
				<td style="padding:10px;" width="10%"><?php echo $value->contact_number;?> </td>
				</tr>
				<?php 
				$no_ret++;
				}else{}
			} ?>
				<tr>
				<td style="padding:10px;text-align:left;" colspan="9"><b>Total Number:</b> <?php echo $no_ret; ?></td>
				</tr>
			<?php
			} else {
			?>
			<tr  style="border-bottom:2px solid #6E6E6E;" align="center" >
				<td style="padding:10px;" colspan="17">Report Not Available</td>
			</tr>
			<?php
			}
			?>
		</table>
		</div>
		</td>
	<?php } ?>
	</tr>
	<tr>
		<td><img src="images/blank.gif" width="695" height="1" alt="blank" /></td>
		<td></td>
	</tr>
	</table>
<div class="clear"></div>
</div>
<!--  end content-table-inner  -->
</td>
<td id="tbl-border-right"></td>
</tr>
<!--<tr>
	<th class="sized bottomleft"></th>
	<td id="tbl-border-bottom">&nbsp;</td>
	<th class="sized bottomright"></th>
</tr>-->
</table>
<div class="clear">&nbsp;</div>
</div>
<!--  end content -->
<div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->
<div class="clear">&nbsp;</div> 
<!-- start footer -->         
<?php include("footer.php") ?>
<?php unset($_SESSION['SalAttList']); ?>
<!-- end footer -->
<script src="javascripts/jquery-1.8.2.js" type="text/javascript"></script>
<script src="javascripts/jquery-ui.js"></script>
<script type="text/javascript" src="javascripts/validate.js"></script>
<script type="text/javascript">
// Popup window code
function newPopup(url) {
	popupWindow = window.open(
		url,'popUpWindow','height=500,width=600,left=200,top=100,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no,status=yes')
}
</script>
<script>
    $(function() {
        $( "#from" ).datepicker({
			dateFormat: "d M yy",
            defaultDate: "w",
            changeMonth: true,
            numberOfMonths: 1,
            onSelect: function( selectedDate ) {
                $( "#to" ).datepicker( "option", "minDate", selectedDate );
            }
        });
        $( "#to" ).datepicker({
			dateFormat: "d M yy",
            defaultDate: "-w",
            changeMonth: true,
            numberOfMonths: 1,
            onSelect: function( selectedDate ) {
                $( "#from" ).datepicker( "option", "maxDate", selectedDate );
            }
        });
    });
</script>
<script type='text/javascript'>//<![CDATA[ 
var tableToExcel = (function () {
        var uri = 'data:application/vnd.ms-excel;base64,'
        , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table><tr><td><b>Salesman Name:</b> <?php echo $sal_name; ?></td><td><b>From Date:</b> <?php echo $_objAdmin->_changeDate($from_date); ?></td><td><b>To Date:</b> <?php echo $_objAdmin->_changeDate($to_date); ?></td></tr></table><table>{table}</table></body></html>'
        , base64 = function (s) { return window.btoa(unescape(encodeURIComponent(s))) }
        , format = function (s, c) { return s.replace(/{(\w+)}/g, function (m, p) { return c[p]; }) }
        return function (table, name, filename) {
            if (!table.nodeType) table = document.getElementById(table)
            var ctx = { worksheet: name || 'Worksheet', table: table.innerHTML }

            document.getElementById("dlink").href = uri + base64(format(template, ctx));
            document.getElementById("dlink").download = filename;
            document.getElementById("dlink").click();

        }
    })()

//]]>  
</script>
</body>
</html>
