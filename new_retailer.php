<?php
include("includes/config.inc.php");
include("includes/function.php");
include("includes/globalarraylist.php");

$page_name="New Dealer";

//$_objAdmin = new Admin();
unset($_SESSION['retId']);
unset($_SESSION['new_ret']);
unset($_SESSION['ret_err']);
if(isset($_REQUEST['show']) && $_REQUEST['show']=="yes"){
	$_objAdmin->showNewRetailer($salesman);
	die;
}
if(isset($_POST['delete']) && $_POST['delete']=='yes' && $_POST['id']!=""){
		$id=$_objAdmin->_dbUpdate(array("last_update_date"=>date('Y-m-d'), "last_update_status"=>'Update', "status"=>'D'),'table_retailer', " retailer_id='".$_POST['id']."'");	
	}

include("header.inc.php");
 ?>
 <script type="text/javascript">
function show(value) {
	
	showUser(value);
	//showstage_city(value);
   
}
</script>
<!-- start content-outer -->
<input name="pagename" type="hidden"  id="pagename" value="retailer.php" />
<div id="content-outer">
<!-- start content -->
<div id="content">
<div id="page-heading"><h1><span style="color: #d74343; font-family: Tahoma; font-weight: bold;">New Dealer</span></h1></div>
<table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">
<!--<tr>
	<th rowspan="3" class="sized"><img src="images/side_shadowleft.jpg" width="20" height="300" alt="" /></th>
	<th class="topleft"></th>
	<td id="tbl-border-top">&nbsp;</td>
	<th class="topright"></th>
	<th rowspan="3" class="sized"><img src="images/side_shadowright.jpg" width="20" height="300" alt="" /></th>
</tr>
-->
<tr>
	<!--<td id="tbl-border-left"></td>-->
	<td>
	<!--  start content-table-inner -->
	<div id="content-table-inner">
	
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
	<tr valign="top">
	<a href="export.inc.php?export_new_retailers_list"><input type="button" name="submit" value="Export to Excel" class="result-submit" style="float:right;"  ></a>
	<td>
		<!-- start id-form -->
		<?php if($err!=''){?>
		<div id="message-red">
			<table border="0" width="100%" cellpadding="0" cellspacing="0">
			<tr>
				<td class="red-left">Error. <?php echo $err; ?></td>
				<td class="red-right"><a class="close-red"><img src="images/icon_close_red.gif"   alt="" /></a></td>
			</tr>
			</table>
		</div>
		<?php } ?>
		<!--  end message-red -->
		<?php if(isset($_REQUEST['sus']) && $_REQUEST['sus']=="sus"){?>
		<!--  start message-green -->
		<div id="message-green">
			<table border="0" width="100%" cellpadding="0" cellspacing="0">
			<tr>
				<td class="green-left"><?php echo "New Dealer has been added successfully.";?></td>
				<td class="green-right"><a class="close-green"><img src="images/icon_close_green.gif"   alt="" /></a></td>
			</tr>
			</table>
		</div>
		<?php } ?>
		<!--  end message-green -->
		<table  border="0" cellspacing="0" cellpadding="0"  align="center" width="100%">
		<tr>
			<td align="lift" valign="top"  >
			<table id="flex1" style="display:none"></table>
			<script type="text/javascript">showNewRetailer();</script>
			</td>
		</tr>
		</table>
		<!-- end id-form  -->
	</td>
	<td>
	<!-- right bar-->
	<?php //include("rightbar/retailer_bar.php") ?>
	</td>
	</tr>
<tr>
<td><img src="images/blank.gif" width="695" height="1" alt="blank" /></td>
<td></td>
</tr>
</table>
<div class="clear"></div>
</div>
<!--  end content-table-inner  -->
</td>
<td id="tbl-border-right"></td>
</tr>
<!--<tr>
	<th class="sized bottomleft"></th>
	<td id="tbl-border-bottom">&nbsp;</td>
	<th class="sized bottomright"></th>
</tr>-->
</table>
<div class="clear">&nbsp;</div>
</div>
<!--  end content -->
<div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->
<div class="clear">&nbsp;</div> 
<!-- start footer -->         
<?php include("footer.php");?>
<!-- end footer -->
 
</body>
</html>