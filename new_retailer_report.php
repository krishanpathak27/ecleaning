<?php 
	  include("includes/config.inc.php");
      include("includes/function.php");
	  include("includes/globalarraylist.php");

	  
	$page_name="New Added Retailer Report";
	if(isset($_POST['showReport']) && $_POST['showReport'] == 'yes')
	{

		if($_POST['sal']!="" && $_POST['sal']!='All') 
		{
			$sal_id=$_POST['sal'];
			$salesman_id=" AND sal.salesman_id='".$sal_id."'";	

			$_SESSION['salesman_id']=$_POST['sal'];

		}else{
			unset($_SESSION['salesman_id']);
		}


		if($_POST['distributorID']!="" && $_POST['distributorID']!="all") 
		{

			$distributor_id = $_POST['distributorID'];

			$distributor = " AND d.distributor_id='".$distributor_id."'";

			$_SESSION['distributor_id'] = $_POST['distributorID'];

		}else{
			unset($_SESSION['distributor_id']);
		}

		// if($_POST['distributorID']=="all"){
		// 	unset($_SESSION['distID']);
		// }

		if($_POST['from']!="") 
		{
			$from_date=$_objAdmin->_changeDate($_POST['from']);	
			$_SESSION['FromDate']=$_objAdmin->_changeDate($_POST['from']);
		}
		if($_POST['to']!="") 
		{
			$to_date=$_objAdmin->_changeDate($_POST['to']);	
			$_SESSION['ToDate']=$_objAdmin->_changeDate($_POST['to']);
		}
		if($_POST['state']!="") 
		{
		//$state=$_POST['state'];	
			$state="and r.state='".$_POST['state']."'";
			
			$_SESSION['state'] = $_POST['state'];

		} else {
			$state="";
			unset($_SESSION['state']);
		}

	} else {
		$from_date= $_objAdmin->_changeDate(date("Y-m-d"));
		$to_date= $_objAdmin->_changeDate(date("Y-m-d"));	

		$_SESSION['FromDate']= $_objAdmin->_changeDate(date("Y-m-d"));
		$_SESSION['ToDate']= $_objAdmin->_changeDate(date("Y-m-d"));

	}
	if(isset($_REQUEST['reset']) && $_REQUEST['reset'] == 'yes')
	{
		unset($_SESSION['salesman_id']);	
		unset($_SESSION['distributor_id']);
		unset($_SESSION['FromDate']);	
		unset($_SESSION['ToDate']); 
		unset($_SESSION['state']);
		header("Location: new_retailer_report.php");
	}

	if($sal_id!=''){
		$SalName=$_objAdmin->_getSelectList('table_salesman','salesman_name',''," salesman_id='".$sal_id."'"); 
		$sal_name=$SalName[0]->salesman_name;
	} 
	if($_POST['state']!=''){
		$StateName=$_objAdmin->_getSelectList2('state','state_name',''," state_id='".$_POST['state']."'"); 
		$state_name=$StateName[0]->state_name;
	} else {
		$state_name="All State";
	}?>

<?php include("header.inc.php") ?>
<script type="text/javascript">

    function PrintElem(elem)
    {
        Popup($(elem).html());
    }

    function Popup(data) 
    {
		
        var mywindow = window.open('', 'Report');
		
        mywindow.document.write('<html><head><title>New Added Dealer Report</title>');
		mywindow.document.write('<table><tr><td><b>Salesman Name:</b> <?php echo $sal_name; ?></td><td><b>State Name:</b> <?php echo $state_name; ?></td><td><b>From Date:</b> <?php echo $from_date; ?></td><td><b>To Date:</b> <?php echo $to_date; ?></td></tr></table>');
        /*optional stylesheet*/ //mywindow.document.write('<link rel="stylesheet" href="main.css" type="text/css" />');
        mywindow.document.write('</head><body >');
        mywindow.document.write(data);
        mywindow.document.write('</body></html>');

        mywindow.print();
        mywindow.close();
        return true;
    }


</script>
<script src="javascripts/dateNextPrev.js" type="text/javascript"></script>
<!-- start content-outer -->
<input name="pagename" type="hidden"  id="pagename" value="admin_order_list.php" />
<div id="content-outer">
<!-- start content -->
<div id="content">
<div id="page-heading"><h1><span style="color: #d74343; font-family: Tahoma; font-weight: bold;">New Dealers Report</span></h1></div>
<table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">
<tr>
	<td>
	<!--  start content-table-inner -->
	<div id="content-table-inner">
	
	<div id="page-heading" align="left" >
	<form name="frmPre" id="frmPre" method="post" action="" enctype="multipart/form-data" >
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
	<tr>
		<td><h3>Salesman: </h3><h6>
		<select name="sal" id="sal" class="menulist">
			<?php echo $salesmanOptionList = $_objArrayList->GetSalesmenMenu($salsList, $sal_id,'flex');?>
		</select>
		</h6></td>

		<td><h3>&nbsp;&nbsp;&nbsp;&nbsp;Distributor:</h3>
			<h6>
				<select name="distributorID" id="distributorID" class="styledselect_form_5" style="width:120px;" >
					<option value="all" >All</option>
					<?php $aDist=$_objAdmin->_getSelectList('table_distributors','*',''," account_id='".$_SESSION['accountId']."' ORDER BY distributor_name"); 
					if(is_array($aDist)){
						for($i=0;$i<count($aDist);$i++){?>
						<option value="<?php echo $aDist[$i]->distributor_id;?>" <?php if ($aDist[$i]->distributor_id==$distributor_id){ ?> selected <?php } ?>><?php echo $aDist[$i]->distributor_name;?></option>
						<?php } }?>
				</select>
			</h6>
		</td>
		
		<td><h3>State: </h3>
			<h6>
				<select name="state" id="state" class="menulist">
					<?php echo $stateList = $_objArrayList->getStateList($_POST['state']);?>
				</select>
			</h6>
		</td>
		
		<td><h3>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;From Date: </h3><h6><img src="css/images/prev.png" height="18" width="18" onclick="dateFromPrev();"> <input type="text" id="from" name="from" class="date" style="width:100px" value="<?php  echo $from_date;?>"  readonly /> <img src="css/images/next.png" height="18" width="18" onclick="dateFromNext();"></h6></td>
		
		<td><h3>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;To Date: </h3><h6><img src="css/images/prev.png" height="18" width="18" onclick="dateToPrev();"> <input type="text" id="to" name="to" class="date" style="width:100px" value="<?php echo $to_date; ?>"  readonly /> <img src="css/images/next.png" height="18" width="18" onclick="dateToNext();"></h6></td>

		
		<td>
		<h3></h3>
		<input name="submit" class="result-submit" type="submit" id="submit" value="View Details" />
		<input type="button" value="Reset!" class="form-reset" onclick="location.href='new_retailer_report.php?reset=yes';" />
		</td>
		<td colspan="2"></td>
		</tr>
		<tr>
		<td colspan="6"><input name="showReport" type="hidden" value="yes" />
		<a id="dlink"  style="display:none;"></a>
		<input  type="submit" value="Export to Excel" name="submit" class="result-submit"  >
		<input type="button" value="Print" class="result-submit" onclick="PrintElem('#Report')" />
		<!-- <a href='new_retailer_report_year_graph.php?y=<?php echo checkFromdate($from_date)."&salID=".$_POST['sal']."&city=".$_POST['city']; ?>' target="_blank"><input type="button" value=" Show Graph" class="result-submit" /></a> -->

		<?php

			$no_ret = array();

			$salDiv = $_objAdmin->_getSelectList('table_salesman'," division_id",''," AND salesman_id='".$_SESSION['salesmanId']."'");
		    if(sizeof($salDiv) > 0)
		   	 $divisionFilters = " AND r.division_id =".$salDiv[0]->division_id;


			$where = " r.new='' $divisionFilters and r.status!='D'";


			$auRet=$_objAdmin->_getSelectList('table_retailer as r 
		left join table_division AS dV ON dV.division_id = r.division_id
		left join table_brands AS tb ON tb.brand_id = r.brand_id  
		left join table_retailer_app_user AS AP ON AP.retailer_id = r.retailer_id  
		LEFT JOIN table_relationship as rr on rr.relationship_id=r.relationship_id 
		left join table_account as a on a.account_id=r.account_id 
		left join table_web_users as w on w.retailer_id=r.retailer_id 
		left join state as s on s.state_id=r.state 
		left join city as c on c.city_id=r.city 
		left join table_retailer_channel_master as cm on r.channel_id=cm.channel_id 
		left join table_retailer_type_master as tm on tm.type_id=r.type_id 
		left JOIN table_salesman AS sal ON sal.salesman_id = r.salesman_id
		left join table_distributors as d on d.distributor_id=r.distributor_id 
		left join table_markets as m on m.market_id=r.market_id 
		left join table_taluka as t on t.taluka_id=r.taluka_id
		left join country as cn on r.country = cn.country_id
		left join table_region as tr on r.region = tr.region_id
		left join table_zone as tz on r.zone = tz.zone_id',
				"r.*,w.username,cm.channel_name,d.distributor_name,w.email_id,w.web_user_id,rr.relationship_code,tm.type_name, w.status as loginStatus,sal.salesman_name,sal.salesman_code,s.state_name,c.city_name,dV.division_name, t.taluka_name,m.market_name,AP.app_version,tb.brand_name,cn.country_name,tr.region_name,tz.zone_name",''," $where  ORDER BY r.retailer_name asc");

			if(is_array($auRet)){
				$ar = 0;
				$ir = 0;
				for($i=0;$i<count($auRet);$i++)
				{
					$no_ret[] = $auRet[$i]->retailer_id;
					if($auRet[$i]->status=='A')
					 	$ar++;
					else
					 	$ir++;
				}
			}	 

		?>

			<b>Total Dealer  : </b><?php  echo count($no_ret); ?>
			<b>Total Active Dealer  : </b><?php echo $ar; ?>
			<b>Total Inactive Dealer : </b><?php echo $ir; ?>

		</td>
	</tr>
	</table>
	</form>
	</div>
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
	
	<tr valign="top">
		
		<td>
		<div id="content-table-inner" style="overflow-x:scroll; width:1100px;margin-bottom: 40px; height:400px">
		<div id="Report">
		<table  border="0" width="100%" cellpadding="0" cellspacing="0" id="report_export" name="report_export" >
			    <tr bgcolor="#6E6E6E" style="color: #fff;font-weight: bold;" >
				<td style="padding:10px;" width="10%">Dealer Name</td>
				<td style="padding:10px;" width="10%">Dealer Code</td>
				<td style="padding:10px;" width="10%">Salesman</td>
				<td style="padding:10px;" width="10%">Division</td>
				<td style="padding:10px;" width="10%">Added Date</td>
				<td style="padding:10px;" width="10%">Country</td>
				<td style="padding:10px;" width="10%">Region</td>
				<td style="padding:10px;" width="10%">Zone</td>
				<td style="padding:10px;" width="10%">State</td>
				<td style="padding:10px;" width="10%">District</td>
				<td style="padding:10px;" width="20%">Address</td>
				<td style="padding:10px;" width="10%">Phone Number</td>
				<td style="padding:10px;" width="10%">Contact Person</td>
				<td style="padding:10px;" width="10%">Contact Number</td>
				
			</tr>
			<?php
			$no_ret = array();
			$auRec=$_objAdmin->_getSelectList('table_retailer as r 
		left join table_division AS dV ON dV.division_id = r.division_id
		left join table_brands AS tb ON tb.brand_id = r.brand_id  
		left join table_retailer_app_user AS AP ON AP.retailer_id = r.retailer_id  
		LEFT JOIN table_relationship as rr on rr.relationship_id=r.relationship_id 
		left join table_account as a on a.account_id=r.account_id 
		left join table_web_users as w on w.retailer_id=r.retailer_id 
		left join state as s on s.state_id=r.state 
		left join city as c on c.city_id=r.city 
		left join table_retailer_channel_master as cm on r.channel_id=cm.channel_id 
		left join table_retailer_type_master as tm on tm.type_id=r.type_id 
		left JOIN table_salesman AS sal ON sal.salesman_id = r.salesman_id
		left join table_distributors as d on d.distributor_id=r.distributor_id 
		left join table_markets as m on m.market_id=r.market_id 
		left join country as cn on r.country = cn.country_id
		left join table_region as tr on r.region = tr.region_id
		left join table_zone as tz on r.zone = tz.zone_id',
				"r.*,w.username,cm.channel_name,d.distributor_name,w.email_id,w.web_user_id,rr.relationship_code,tm.type_name, w.status as loginStatus,sal.salesman_name,sal.salesman_code,s.state_name,c.city_name,dV.division_name,m.market_name,AP.app_version,tb.brand_name,cn.country_name,tr.region_name,tz.zone_name",''," $where $salesman_id $distributor $state and (r.start_date BETWEEN '".date('Y-m-d', strtotime($from_date))."' AND '".date('Y-m-d', strtotime($to_date))."')  ORDER BY r.start_date desc, c.city_name asc");
			if(is_array($auRec)){
				$ar = 0;
				$ir = 0;
				for($i=0;$i<count($auRec);$i++)
				{
					
				$no_ret[] = $auRec[$i]->retailer_id;
				 

				?>
				<tr  style="border-bottom:2px solid #6E6E6E;" >
				<td style="padding:10px;" width="10%"><?php echo $auRec[$i]->retailer_name;?> </td>
				<td style="padding:10px;" width="10%"><?php echo $auRec[$i]->retailer_code;?> </td>
				<td style="padding:10px;" width="10%"><?php echo $auRec[$i]->salesman_name;?> </td>
				<td style="padding:10px;" width="10%"><?php echo $auRec[$i]->division_name;?> </td>
				<td style="padding:10px;" width="10%"><?php echo $_objAdmin->_changeDate($auRec[$i]->start_date);?> </td>
				<td style="padding:10px;" width="10%"><?php echo $auRec[$i]->country_name;?> </td>
				<td style="padding:10px;" width="10%"><?php echo $auRec[$i]->region_name;?> </td>
				<td style="padding:10px;" width="10%"><?php echo $auRec[$i]->zone_name;?> </td>
				<td style="padding:10px;" width="10%"><?php echo $auRec[$i]->state_name;?> </td>
				<td style="padding:10px;" width="10%"><?php echo $auRec[$i]->city_name;?> </td>
				<td style="padding:10px;" width="10%"><?php echo $auRec[$i]->retailer_address;?> </td>
				<td style="padding:10px;" width="10%"><?php echo $auRec[$i]->retailer_phone_no;?> </td>
				<td style="padding:10px;" width="10%"><?php echo $auRec[$i]->contact_person;?> </td>
				<td style="padding:10px;" width="10%"><?php echo $auRec[$i]->contact_number;?> </td>
				
				</tr>
				<?php
				}
				?>
				<!-- <tr>
				<td style="padding:10px;" colspan="9"><b>Total Number of Dealer:</b> <?php echo count($no_ret) ?></td>
				</tr> -->
				
			<?php
			} else {
			?>
			<tr  style="border-bottom:2px solid #6E6E6E;" align="center" >
				<td style="padding:10px;" colspan="11">Report Not Available</td>
			</tr>
			<?php
			}
			?>
		</table>
		</div>
		</div>
		</td>



	</tr>
	<tr>
		<td><img src="images/blank.gif" width="695" height="1" alt="blank" /></td>
		<td></td>
	</tr>
	</table>
<div class="clear"></div>
</div>
<!--  end content-table-inner  -->
</td>
<td id="tbl-border-right"></td>
</tr>
<!--<tr>
	<th class="sized bottomleft"></th>
	<td id="tbl-border-bottom">&nbsp;</td>
	<th class="sized bottomright"></th>
</tr>-->
</table>
<div class="clear">&nbsp;</div>
</div>
<!--  end content -->
<div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->
<div class="clear">&nbsp;</div> 
<!-- start footer -->         
<?php include("footer.php") ?>
<?php unset($_SESSION['SalAttList']); ?>
<!-- end footer -->
<script src="javascripts/jquery-1.8.2.js" type="text/javascript"></script>
<script src="javascripts/jquery-ui.js"></script>
<script type="text/javascript" src="javascripts/validate.js"></script>
<script type="text/javascript">
// Popup window code
function newPopup(url) {
	popupWindow = window.open(
		url,'popUpWindow','height=500,width=600,left=200,top=100,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no,status=yes')
}
</script>
<script>
    $(function() {
        $( "#from" ).datepicker({
			dateFormat: "d M yy",
            defaultDate: "w",
            changeMonth: true,
            numberOfMonths: 1,
            onSelect: function( selectedDate ) {
                $( "#to" ).datepicker( "option", "minDate", selectedDate );
            }
        });
        $( "#to" ).datepicker({
			dateFormat: "d M yy",
            defaultDate: "-w",
            changeMonth: true,
            numberOfMonths: 1,
            onSelect: function( selectedDate ) {
                $( "#from" ).datepicker( "option", "maxDate", selectedDate );
            }
        });
    });
</script>
<script type="text/javascript">
$(document).ready(function()
{

	<?php if($_POST['submit']=='Export to Excel'){ ?>
	window.location.assign("export.inc.php?export_new_dealers_report");
	<?php } ?>

});
</script>


<script type='text/javascript'>//<![CDATA[ 
var tableToExcel = (function () {
        var uri = 'data:application/vnd.ms-excel;base64,'
        , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table><tr><td><b>Salesman Name:</b> <?php echo $sal_name; ?></td><td><b>From Date:</b> <?php echo $_objAdmin->_changeDate($from_date); ?></td><td><b>To Date:</b> <?php echo $_objAdmin->_changeDate($to_date); ?></td></tr></table><table>{table}</table></body></html>'
        , base64 = function (s) { return window.btoa(unescape(encodeURIComponent(s))) }
        , format = function (s, c) { return s.replace(/{(\w+)}/g, function (m, p) { return c[p]; }) }
        return function (table, name, filename) {
            if (!table.nodeType) table = document.getElementById(table)
            var ctx = { worksheet: name || 'Worksheet', table: table.innerHTML }

            document.getElementById("dlink").href = uri + base64(format(template, ctx));
            document.getElementById("dlink").download = filename;
            document.getElementById("dlink").click();

        }
    })()

//]]>  
</script>
</body>
</html>
