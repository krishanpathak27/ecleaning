<?php include("includes/config.inc.php");
      include("includes/function.php");
	  include("includes/globalarraylist.php");
	  
	  
	$page_name="New Added Retailer Report";
	if(isset($_POST['showReport']) && $_POST['showReport'] == 'yes')
	{	
		if($_POST['sal']!="") 
		{
		$sal_id=$_POST['sal'];	
		}
		if($_POST['from']!="") 
		{
		$from_date=$_objAdmin->_changeDate($_POST['from']);	
		}
		if($_POST['to']!="") 
		{
		$to_date=$_objAdmin->_changeDate($_POST['to']);	
		}
		if($_POST['city']!="") 
		{
		//$city=$_POST['city'];	
		$city="and r.city='".$_POST['city']."'";
		} else {
		$city="";
		}
	} else {
	$from_date= $_objAdmin->_changeDate(date("Y-m-d"));
	$to_date= $_objAdmin->_changeDate(date("Y-m-d"));	
	}
	if(isset($_REQUEST['reset']) && $_REQUEST['reset'] == 'yes')
	{
		/* unset($_SESSION['SalAttList']);	
		unset($_SESSION['FromAttList']);	
		unset($_SESSION['ToAttList']); */
		header("Location: new_retailer_report.php");
	}
	if($sal_id!=''){
	$SalName=$_objAdmin->_getSelectList('table_salesman','salesman_name',''," salesman_id='".$sal_id."'"); 
	$sal_name=$SalName[0]->salesman_name;
	} 
	if($_POST['city']!=''){
	$CityName=$_objAdmin->_getSelectList2('city','city_name',''," city_id='".$_POST['city']."'"); 
	$city_name=$CityName[0]->city_name;
	} else {
	$city_name="All City";
	}?>

<?php include("header.inc.php") ?>
<script type="text/javascript">

    function PrintElem(elem)
    {
        Popup($(elem).html());
    }

    function Popup(data) 
    {
		
        var mywindow = window.open('', 'Report');
		
        mywindow.document.write('<html><head><title>New Added Retailer Report</title>');
		mywindow.document.write('<table><tr><td><b>Salesman Name:</b> <?php echo $sal_name; ?></td><td><b>City Name:</b> <?php echo $city_name; ?></td><td><b>From Date:</b> <?php echo $from_date; ?></td><td><b>To Date:</b> <?php echo $to_date; ?></td></tr></table>');
        /*optional stylesheet*/ //mywindow.document.write('<link rel="stylesheet" href="main.css" type="text/css" />');
        mywindow.document.write('</head><body >');
        mywindow.document.write(data);
        mywindow.document.write('</body></html>');

        mywindow.print();
        mywindow.close();
        return true;
    }

</script>
<script src="javascripts/dateNextPrev.js" type="text/javascript"></script>
<!-- start content-outer -->
<input name="pagename" type="hidden"  id="pagename" value="admin_order_list.php" />
<div id="content-outer">
<!-- start content -->
<div id="content">
<div id="page-heading"><h1><span style="color: #d74343; font-family: Tahoma; font-weight: bold;">New Added Retailer Report</span></h1></div>
<table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">
<tr>
	<td>
	<!--  start content-table-inner -->
	<div id="content-table-inner">
	
	<div id="page-heading" align="left" >
	<form name="frmPre" id="frmPre" method="post" action="" enctype="multipart/form-data" >
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
	<tr>
		<td><h3>Salesman: </h3><h6>
		<select name="sal" id="sal" class="menulist">
			<?php echo $salesmanOptionList = $_objArrayList->GetSalesmenMenu($salsList, $sal_id,'root');?>
		</select>
		</h6></td>
		
		
		<td><?php echo $cityList = $_objArrayList->getCityList($_POST['city']);?></td>
		
		
		<td><h3>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;From Date: </h3><h6><img src="css/images/prev.png" height="18" width="18" onclick="dateFromPrev();"> <input type="text" id="from" name="from" class="date" style="width:100px" value="<?php  echo $from_date;?>"  readonly /> <img src="css/images/next.png" height="18" width="18" onclick="dateFromNext();"></h6></td>
		
		<td><h3>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;To Date: </h3><h6><img src="css/images/prev.png" height="18" width="18" onclick="dateToPrev();"> <input type="text" id="to" name="to" class="date" style="width:100px" value="<?php echo $to_date; ?>"  readonly /> <img src="css/images/next.png" height="18" width="18" onclick="dateToNext();"></h6></td>
		<td>
		
		<h3></h3>
		<input name="submit" class="result-submit" type="submit" id="submit" value="View Details" />
	<input type="button" value="Reset!" class="form-reset" onclick="location.href='new_retailer_report.php?reset=yes';" />
		</td>
		<td colspan="2"></td>
		</tr>
		<tr>
		<td colspan="6"><input name="showReport" type="hidden" value="yes" />
		<input type="button" value="Print" class="result-submit" onclick="PrintElem('#Report')" />
		<a href='new_retailer_report_year_graph.php?y=<?php echo checkFromdate($from_date)."&salID=".$_POST['sal']."&city=".$_POST['city']; ?>' target="_blank"><input type="button" value=" Show Graph" class="result-submit" /></a>
		</td>
	</tr>
	</table>
	</form>
	</div>
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
	<?php
	if($sal_id==""){
	?>
	<tr valign="top">
		<td>
		<table border="0" width="100%" cellpadding="0" cellspacing="0" id="id-form">
			<tr bgcolor="#A52A2A" style="color: #fff;font-weight: bold;">
				<td align="center" style="padding:10px;">Select Salesman</td>
			</tr>
		</table>
		</td>
		<?php } else { ?>
		<td>
		<div id="Report">
		<table  border="0" width="100%" cellpadding="0" cellspacing="0" >
			<tr  bgcolor="#6E6E6E" style="color: #fff;font-weight: bold;" >
				<td style="padding:10px;" width="10%">Retailer Name</td>
				<td style="padding:10px;" width="10%">Added Date</td>
				<td style="padding:10px;" width="10%">State</td>
				<td style="padding:10px;" width="10%">City</td>
				<td style="padding:10px;" width="10%">Market</td>
				<td style="padding:10px;" width="20%">Address</td>
				<td style="padding:10px;" width="10%">Phone Number</td>
				<td style="padding:10px;" width="10%">Contact Person</td>
				<td style="padding:10px;" width="10%">Contact Number</td>
			</tr>
			<?php
			$no_ret = array();
			$auRet=$_objAdmin->_getSelectList('table_retailer as r left join state as s on s.state_id=r.state left join city as c on c.city_id=r.city',"r.retailer_id,r.retailer_name,r.start_date,r.retailer_location,r.retailer_address,r.retailer_phone_no,r.contact_person,r.contact_number,s.state_name,c.city_name",''," r.salesman_id='".$sal_id."' $city and (r.start_date BETWEEN '".date('Y-m-d', strtotime($from_date))."' AND '".date('Y-m-d', strtotime($to_date))."') ORDER BY r.start_date desc, c.city_name asc");
			if(is_array($auRet)){
				for($i=0;$i<count($auRet);$i++)
				{
				$no_ret[] = $auRet[$i]->retailer_id;
				?>
				<tr  style="border-bottom:2px solid #6E6E6E;" >
				<td style="padding:10px;" width="10%"><?php echo $auRet[$i]->retailer_name;?> </td>
				<td style="padding:10px;" width="10%"><?php echo $_objAdmin->_changeDate($auRet[$i]->start_date);?> </td>
				<td style="padding:10px;" width="10%"><?php echo $auRet[$i]->state_name;?> </td>
				<td style="padding:10px;" width="10%"><?php echo $auRet[$i]->city_name;?> </td>
				<td style="padding:10px;" width="10%"><?php echo $auRet[$i]->retailer_location;?> </td>
				<td style="padding:10px;" width="10%"><?php echo $auRet[$i]->retailer_address;?> </td>
				<td style="padding:10px;" width="10%"><?php echo $auRet[$i]->retailer_phone_no;?> </td>
				<td style="padding:10px;" width="10%"><?php echo $auRet[$i]->contact_person;?> </td>
				<td style="padding:10px;" width="10%"><?php echo $auRet[$i]->contact_number;?> </td>
				</tr>
				<?php
				}
				?>
				<tr  >
				<td style="padding:10px;" colspan="9"><b>Total Number of Retailer:</b> <?php echo count($no_ret) ?></td>
				</tr>
			<?php
			} else {
			?>
			<tr  style="border-bottom:2px solid #6E6E6E;" align="center" >
				<td style="padding:10px;" colspan="9">Report Not Available</td>
			</tr>
			<?php
			}
			?>
		</table>
		</div>
		</td>
	<?php } ?>
	</tr>
	<tr>
		<td><img src="images/blank.gif" width="695" height="1" alt="blank" /></td>
		<td></td>
	</tr>
	</table>
<div class="clear"></div>
</div>
<!--  end content-table-inner  -->
</td>
<td id="tbl-border-right"></td>
</tr>
<!--<tr>
	<th class="sized bottomleft"></th>
	<td id="tbl-border-bottom">&nbsp;</td>
	<th class="sized bottomright"></th>
</tr>-->
</table>
<div class="clear">&nbsp;</div>
</div>
<!--  end content -->
<div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->
<div class="clear">&nbsp;</div> 
<!-- start footer -->         
<?php include("footer.php") ?>
<?php unset($_SESSION['SalAttList']); ?>
<!-- end footer -->
<script src="javascripts/jquery-1.8.2.js" type="text/javascript"></script>
<script src="javascripts/jquery-ui.js"></script>
<script type="text/javascript" src="javascripts/validate.js"></script>
<script type="text/javascript">
// Popup window code
function newPopup(url) {
	popupWindow = window.open(
		url,'popUpWindow','height=500,width=600,left=200,top=100,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no,status=yes')
}
</script>
<script>
    $(function() {
        $( "#from" ).datepicker({
			dateFormat: "d M yy",
            defaultDate: "w",
            changeMonth: true,
            numberOfMonths: 1,
            onSelect: function( selectedDate ) {
                $( "#to" ).datepicker( "option", "minDate", selectedDate );
            }
        });
        $( "#to" ).datepicker({
			dateFormat: "d M yy",
            defaultDate: "-w",
            changeMonth: true,
            numberOfMonths: 1,
            onSelect: function( selectedDate ) {
                $( "#from" ).datepicker( "option", "maxDate", selectedDate );
            }
        });
    });
</script>
</body>
</html>