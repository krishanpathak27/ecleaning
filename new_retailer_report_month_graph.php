<?php include("includes/config.inc.php");
      include("includes/function.php");
	  include("includes/globalarraylist.php");


if($_REQUEST['city']!="") 
{
	$city="and r.city='".$_REQUEST['city']."'";
} 
else 
{
	$city="";
	$cname = 'All City';
}


if($_REQUEST['salID']!='') {
	
	$salesmanName=$_objAdmin->_getSelectList('table_salesman',"salesman_name ",''," salesman_id='".$_REQUEST['salID']."'");
	$sname = $salesmanName[0]->salesman_name;
	$salesman = "r.salesman_id='".$_REQUEST['salID']."'";

}




$auRet=$_objAdmin->_getSelectList('table_retailer as r
left join table_salesman as s on s.salesman_id = r.salesman_id 
left join state as st on st.state_id = r.state
left join city as c on c.city_id=r.city
',"day(r.start_date) as day ",''," $salesman $city and monthname(r.start_date) = '$_REQUEST[m]' and EXTRACT(YEAR FROM r.start_date)='$_REQUEST[y]'ORDER BY r.start_date desc, c.city_name asc");
//print_r($auRet);
//echo count($graph);
$d1=0; $d2=0; $d3=0; $d4=0; $d5=0; $d6=0; $d7=0; $d8=0; $d9=0; $d10=0;
$d11=0; $d12=0; $d13=0; $d14=0; $d15=0; $d16=0; $d17=0; $d18=0; $d19=0; $d20=0;
$d21=0; $d22=0; $d23=0; $d24=0; $d25=0; $d26=0; $d27=0; $d28=0; $d29=0; $d30=0; $d31=0;
for($i=0; $i < count($auRet); $i++)
{
	if($auRet[$i]->day=='1') { $d1 = $d1+1; }
	if($auRet[$i]->day=='2') { $d2 = $d2+1; }
	if($auRet[$i]->day=='3') { $d3 = $d3+1; }
	if($auRet[$i]->day=='4') { $d4 = $d4+1; }
	if($auRet[$i]->day=='5') { $d5 = $d5+1; }
	if($auRet[$i]->day=='6') { $d6 = $d6+1; }
	if($auRet[$i]->day=='7') { $d7 = $d7+1; }
	if($auRet[$i]->day=='8') { $d8 = $d8+1; }
	if($auRet[$i]->day=='9') { $d9 = $d9+1; }
	if($auRet[$i]->day=='10') { $d10 = $d10+1; }
	if($auRet[$i]->day=='11') { $d11 = $d11+1; }
	if($auRet[$i]->day=='12') { $d12 = $d12+1; }
	if($auRet[$i]->day=='13') { $d13 = $d13+1; }
	if($auRet[$i]->day=='14') { $d14 = $d14+1; }
	if($auRet[$i]->day=='15') { $d15 = $d15+1; }
	if($auRet[$i]->day=='16') { $d16 = $d16+1; }
	if($auRet[$i]->day=='17') { $d17 = $d17+1; }
	if($auRet[$i]->day=='18') { $d18 = $d18+1; }
	if($auRet[$i]->day=='19') { $d19 = $d19+1; }
	if($auRet[$i]->day=='20') { $d20 = $d20+1; }
	if($auRet[$i]->day=='21') { $d21 = $d21+1; }
	if($auRet[$i]->day=='22') { $d22 = $d22+1; }
	if($auRet[$i]->day=='23') { $d23 = $d23+1; }
	if($auRet[$i]->day=='24') { $d24 = $d24+1; }
	if($auRet[$i]->day=='25') { $d25 = $d25+1; }
	if($auRet[$i]->day=='26') { $d26 = $d26+1; }
	if($auRet[$i]->day=='27') { $d27 = $d27+1; }
	if($auRet[$i]->day=='28') { $d28 = $d28+1; }
	if($auRet[$i]->day=='29') { $d29 = $d29+1; }
	if($auRet[$i]->day=='30') { $d30 = $d30+1; }
	if($auRet[$i]->day=='31') { $d31 = $d31+1; }
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta charset="utf-8">
<title>Quytech PepUpSales - Salesmen Newly Added Retailers Monthly Report Graph</title>
<?php include_once('graph/header-files.php');?>


    <script type="text/javascript">
      google.load('visualization', '1', {packages: ['corechart']});
	  function newMonth(val)
	  {
		window.location = 'new_retailer_report_month_graph.php?m='+val+'&y=<?php echo $_REQUEST['y']; ?>&sal=<?php echo $_REQUEST['salID']; ?>&city=<?php echo $_REQUEST['city']?>';
		
	  }
    </script>
    <script type="text/javascript">
      function drawVisualization() {
        // Some raw data (not necessarily accurate)
        var data = google.visualization.arrayToDataTable([
          ['day', 'No. of new retailer added'],
          ['1',  <?php echo $d1; ?> ],
          ['2',  <?php echo $d2; ?> ],
          ['3',  <?php echo $d3; ?> ],
          ['4',  <?php echo $d4; ?> ],
		  ['5',  <?php echo $d5; ?> ],
		  ['6',  <?php echo $d6; ?> ],
		  ['7',  <?php echo $d7; ?> ],
		  ['8',  <?php echo $d8; ?> ],
		  ['9',  <?php echo $d9; ?> ],
		  ['10',  <?php echo $d10; ?> ],
		  ['11',  <?php echo $d11; ?> ],
		  ['12',  <?php echo $d12; ?> ],
		  ['13',  <?php echo $d13; ?> ],
		  ['14',  <?php echo $d14; ?> ],
		  ['15',  <?php echo $d15; ?> ],
		  ['16',  <?php echo $d16; ?> ],
		  ['17',  <?php echo $d17; ?> ],
		  ['18',  <?php echo $d18; ?> ],
		  ['19',  <?php echo $d19; ?> ],
          ['20',  <?php echo $d20; ?> ],
		  ['21',  <?php echo $d21; ?> ],
		  ['22',  <?php echo $d22; ?> ],
		  ['23',  <?php echo $d23; ?> ],
		  ['24',  <?php echo $d24; ?> ],
		  ['25',  <?php echo $d25; ?> ],
		  ['26',  <?php echo $d26; ?> ],
		  ['27',  <?php echo $d27; ?> ],
		  ['28',  <?php echo $d28; ?> ],
		  ['29',  <?php echo $d29; ?> ],
          ['30',  <?php echo $d30; ?> ],
          ['31',  <?php echo $d31; ?> ]
        ]);

        var options = {
          title : 'New Added Retailer Graph',
          vAxis: {title: "No of retailer" , gridlines: { count: 8  }, minValue:0},
          hAxis: {title: "Day"},
          seriesType: "bars",
          series: {1: {type: "line"}}
        };

        var chart = new google.visualization.ComboChart(document.getElementById('chart_div'));
        chart.draw(data, options);
      }
      google.setOnLoadCallback(drawVisualization);
    </script>
	
	<style type="text/css">
		.styledselect_form_3 {
			border: solid 1px #BDBDBD;
			color: #393939;
			cursor: pointer;
			display: block;
			font-family: Arial;
			font-size: 12px;
			height: 30px;
			margin: 0px 0px 0px 0px;
			text-align: left;
			width: 100px;
			border-radius: 5px;
			-moz-border-radius: 5px;
			-o-border-radius: 5px;
			-ms-border-radius: 5px;
			-webkit-border-radius: 5px;
		}
	</style>
	</head>
<body> 
<!-- Start: page-top-outer -->

<?php include_once('graph/header.php');?>
<!-- End: page-top-outer -->
	
<div id="content-outer">
<div id="content">
<div id="page-heading"><h1>Salesmen Newly Added Retailers Monthly Report Graph</h1></div>
<div id="container">

<form name="frmPre" id="frmPre" method="post" action="<?php echo $_SERVER['PHP_SELF'];?>" enctype="multipart/form-data" >
<table width="100%" border="0">
  <tr>
  
	<td width="12%" valign="bottom" align="right"><h3>Salesman :</h3></td>
    <td width="12%" align="left">
	<select name="salID" id="salID" class="styledselect_form_5" >
		<?php echo $rtndata = $_objArrayList->GetSalesmenMenu($salsList, $_REQUEST['salID']);?>
	</select>
	</td>
	
	
	<td width="5%" valign="bottom" align="right"><h3>City :</h3></td>
		<td width="6%" align="left">
		<select name="city" id="city" class="styledselect_form_3" >
			<?php echo $city = $_objArrayList->getCityListOptions($_REQUEST['city']);?>
		</select>
	</td>
	
	
	
	<td width="9%" valign="bottom" align="right"><h3>Month :</h3></td>
    <td width="13%" align="left">
	<select name="m" id="m" onchange="newMonth(this.value)" class="styledselect_form_3" >
		<?php echo $months = $_objArrayList->getMonthList2($_REQUEST['m']);?>
		</select>
	</td>
  
  
  	<td width="6%" valign="bottom" align="right"><h3>Year :</h3></td>
    <td width="14%" align="left">
	<select name="y" id="y"  class="styledselect_form_3" >
		<?php echo $year = $_objArrayList->getYearList2($_REQUEST['y']);?>
	</select>
	</td>
	
	<td width="23%" valign="bottom"><input name="submit" class="result-submit" type="submit" id="submit" value="Show graph" /></td>
  </tr>
  
  
  
  
  <tr>
    <td colspan="9"><div id="chart_div" style="width: 100%; height: 450px;"></div></td>
  </tr>
</table>
</form>
<div class="clear">&nbsp;</div>
	
</div>
</div>
</div>
	<!-- Graph code ends here -->
	<?php include("footer.php") ?>
	<!-- Graph code ends here -->
	<script type="text/javascript">
		function OpenInNewTab(url )
		{
		  var win=window.open(url, '_blank');
		  win.focus();
		}
		
	</script>
	  </body>
</html>
	

	