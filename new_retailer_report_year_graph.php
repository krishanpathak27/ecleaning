<?php include("includes/config.inc.php");
      include("includes/function.php");
	  include("includes/globalarraylist.php");

if($_REQUEST['y']!='')
{
	$from_date="'".$_REQUEST['y']."-1-1"."'";
	$to_date="'".$_REQUEST['y']."-12-31"."'";
} else {

	$from_date="1970-1-1";
	$to_date="1970-12-31";

}
if($_REQUEST['city']!="") 
{
	$city="and r.city='".$_REQUEST['city']."'";
} 
else 
{
	$city="";
	$cname = 'All City';
}

if($_REQUEST['salID']!='') {
	
	$salesman = "r.salesman_id='".$_REQUEST['salID']."'";

}


$auRet=$_objAdmin->_getSelectList('table_retailer as r 
left join table_salesman as s on s.salesman_id = r.salesman_id 
left join state as st on st.state_id = r.state 
left join city as c on c.city_id=r.city
',"monthname(r.start_date) as month ",''," $salesman $city and (r.start_date BETWEEN ".$from_date." AND ".$to_date.") ORDER BY r.start_date desc, c.city_name asc");


$jan = 0; $feb = 0; $march = 0; $april = 0; $may = 0; $june = 0; $july = 0; $aug = 0; $sep = 0; $oct = 0; $nov = 0; $dec = 0;
for($i=0; $i < count($auRet); $i++)
{
	if($auRet[$i]->month=='January') { $jan = $jan+1; }
	if($auRet[$i]->month=='February') { $feb = $feb+1; }
	if($auRet[$i]->month=='March') { $march = $march+1; }
	if($auRet[$i]->month=='April') { $april = $april+1; }
	if($auRet[$i]->month=='May') { $may = $may+1; }
	if($auRet[$i]->month=='June') { $june = $june+1; }
	if($auRet[$i]->month=='July') { $july = $july+1; }
	if($auRet[$i]->month=='August') { $aug = $aug+1; }
	if($auRet[$i]->month=='September') { $sep = $sep+1; }
	if($auRet[$i]->month=='October') { $oct = $oct+1; }
	if($auRet[$i]->month=='November') { $nov = $nov+1; }
	if($auRet[$i]->month=='December') { $dec = $dec+1; }
}
?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta charset="utf-8">
<title>Quytech PepUpSales - Salesmen Newly Added Retailers Year Report Graph</title>
<?php include_once('graph/header-files.php');?>


    <script type="text/javascript">
      google.load('visualization', '1', {packages: ['corechart']});
	 
    </script>
    <script type="text/javascript">
      function drawVisualization() {
        // Some raw data (not necessarily accurate)
        var data = google.visualization.arrayToDataTable([
          ['Month', 'No. of new retailer added'],
          ['January',  <?php echo $jan; ?>],
          ['February',  <?php echo $feb; ?>],
          ['March',  <?php echo $march; ?>],
          ['April',  <?php echo $april; ?>],
		  ['May',  <?php echo $may; ?>],
          ['June',  <?php echo $june; ?>],
          ['July',  <?php echo $july; ?>],
          ['August',  <?php echo $aug; ?>],
		  ['September',  <?php echo $sep; ?>],
		  ['October',  <?php echo $oct; ?>],
		  ['November',  <?php echo $nov; ?>],
		  ['December',  <?php echo $dec; ?>]
        ]);

        var options = {
          title : 'New Added Retailer Graph',
          vAxis: {title: "No of retailer" , gridlines: { count: 8  }, minValue:0},
          hAxis: {title: "Month"},
          seriesType: "bars",
          series: {1: {type: "line"}}
        };

        var chart = new google.visualization.ComboChart(document.getElementById('chart_div'));
		/* click handler ends */
		 function selectHandler() {
          var selectedItem = chart.getSelection()[0];
          if (selectedItem) {
            var topping = data.getValue(selectedItem.row, 0);
            var url = 'new_retailer_report_month_graph.php?m='+topping+'&y=<?php echo $_REQUEST['y']; ?>&salID=<?php echo $_REQUEST['salID']; ?>&city=<?php echo $_REQUEST['city']?>';
			OpenInNewTab(url);
          }
        }

        google.visualization.events.addListener(chart, 'click', selectHandler);   
		
		/* click handler ends */
        chart.draw(data, options);
      }
      google.setOnLoadCallback(drawVisualization);
    </script>
</head>
<body> 
<!-- Start: page-top-outer -->

<?php include_once('graph/header.php');?>
<!-- End: page-top-outer -->
	
<div id="content-outer">
<div id="content">
<div id="page-heading"><h1>Salesmen Newly Added Retailers Year Report Graph</h1></div>
<div id="container">

<form name="frmPre" id="frmPre" method="post" action="<?php echo $_SERVER['PHP_SELF'];?>" enctype="multipart/form-data" >
<table width="100%" border="0">
  <tr>
  
	<td width="11%" valign="bottom" align="right"><h3>Salesman :</h3></td>
    <td width="6%" align="left">
	<select name="salID" id="salID" class="styledselect_form_5" >
		<?php echo $rtndata = $_objArrayList->GetSalesmenMenu($salsList, $_REQUEST['salID']);?>
	</select>
	</td>
	
	
	<td width="8%" valign="bottom" align="right"><h3>City :</h3></td>
		<td width="9%" align="left">
		<select name="city" id="city" class="styledselect_form_3" >
			<?php echo $city = $_objArrayList->getCityListOptions($_REQUEST['city']);?>
		</select>
	</td>
	
  
  
  	<td width="8%" valign="bottom" align="right"><h3>Year :</h3></td>
    <td width="13%" align="left">
	<select name="y" id="y"  class="styledselect_form_3" >
		<?php echo $year = $_objArrayList->getYearList2($_REQUEST['y']);?>
	</select>
	</td>
	
	<td width="45%" valign="bottom"><input name="submit" class="result-submit" type="submit" id="submit" value="Show graph" /></td>
  </tr>
  
  
  
  
  <tr>
    <td colspan="7"><div id="chart_div" style="width: 100%; height: 450px;"></div></td>
  </tr>
</table>
</form>
<div class="clear">&nbsp;</div>
	
</div>
</div>
</div>
	<!-- Graph code ends here -->
	<?php include("footer.php") ?>
	<!-- Graph code ends here -->
	<script type="text/javascript">
		function OpenInNewTab(url )
		{
		  var win=window.open(url, '_blank');
		  win.focus();
		}
		function newYear(val)
		{
			window.location = 'new_retailer_report_year_graph.php?y='+val+'&sal=<?php echo $_REQUEST['salID']; ?>&city=<?php echo $_REQUEST['city']; ?>';
		}
	</script>
 </body>
</html>