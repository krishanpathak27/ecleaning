<?php
include("includes/config.inc.php");
include("includes/function.php");
$_objAdmin = new Admin();
$day= date("D");
if(isset($_POST['no_order_add']) && $_POST['no_order_add']=='yes'){	
$_objAdmin->addNoOrder();
header("Location: today_retailer_list.php");
}


if(isset($_REQUEST['retailerId']) && $_REQUEST['retailerId']!="" ){
$auRet=$_objAdmin->_getSelectList('table_retailer',"*",''," retailer_id=".base64_decode($_REQUEST['retailerId'])." and account_id=".$_SESSION['accountId']);
	if(!is_array($auRet)){
	header("Location: today_retailer_list.php");
	}
}

$auDis=$_objAdmin->_getSelectList('table_salesman',"*",''," salesman_id=".$_SESSION['salesmanId']." and account_id=".$_SESSION['accountId']);
if(!is_array($auDis)){
header("Location: today_retailer_list.php");
}
?>
<?php include("header.inc.php") ?>
<!-- start content-outer -->
<div id="content-outer">
<!-- start content -->
<div id="content">
<div id="page-heading"><h1><span style="color: #d74343; font-family: Tahoma; font-weight: bold;">No Order</span></h1></div>
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" id="content-table">
<!--<tr>
	<th rowspan="3" class="sized"><img src="images/side_shadowleft.jpg" width="20" height="300" alt="" /></th>
	<th class="topleft"></th>
	<td id="tbl-border-top">&nbsp;</td>
	<th class="topright"></th>
	<th rowspan="3" class="sized"><img src="images/side_shadowright.jpg" width="20" height="300" alt="" /></th>
</tr>
-->
<tr>
	<!--<td id="tbl-border-left"></td>-->
	<td align="center" valign="middle">
	<!--  start content-table-inner -->
	<div id="content-table-inner">
	
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
	<tr valign="top">
	<td>
		<!-- start id-form -->
		
		<table border="0" width="100%" cellpadding="0" cellspacing="0" id="id-form">
			<tr>
				<td><b>Retailer Name:</b> <?php echo $auRet[0]->retailer_name;?></td>
				<td align="right" style="padding-right: 50px;"><b>Date:</b> <?php echo date("d-M-Y", strtotime($date)); ?></td>
			</tr>
			<tr>
				<td><b>Retailer Location:</b> <?php echo $auRet[0]->retailer_location;?></td>
				<td align="right" style="padding-right: 50px;"><b>Time:</b> <?php echo date("h:i A."); ?></td>
			</tr>
			<tr>
				<td><b>Retailer Address:</b> <?php echo $auRet[0]->retailer_address;?></td>
				<td></td>
			</tr>
		
		</table>
	<tr valign="top">
	<td>
		<form name="frmPre" id="frmPre" method="post" action="no_order_form.php" enctype="multipart/form-data" >
		<table border="0" width="100%" cellpadding="0" cellspacing="0" id="id-form">
			<tr bgcolor="#6E6E6E" style="color: #fff;font-weight: bold;">
				<td style="padding:10px;">Comments</td>
			</tr>
			<tr style="border-bottom:2px solid #6E6E6E;" >
				<td style="padding:10px;"><textarea rows="" cols="100" name="comments" class="textarea1" ></textarea></td>
			</tr>
			<tr style="border-bottom:2px solid #6E6E6E;" >
				<td style="padding:10px;">
				<input name="no_order_add" type="hidden" value="yes" />
				<input name="account_id" type="hidden" value="<?php echo $_SESSION['accountId']; ?>" />
				<input name="sal_id" type="hidden" value="<?php echo $_SESSION['salesmanId']; ?>" />
				<input name="ret_id" type="hidden" value="<?php echo $auRet[0]->retailer_id;?>" />
				<input name="dis_id" type="hidden" value="<?php echo $auDis[0]->distributor_id;?>" />
				<input name="date" type="hidden" value="<?php echo $date; ?>" />
				<input name="time" type="hidden" value="<?php echo date("h:i A."); ?>" />
				<input type="button" value="Back" class="form-reset" onclick="location.href='today_retailer_list.php';" />
				<input name="submit" class="form-submit" type="submit" id="submit" value="Save" />
				
				</td>
			</tr>
		</table>
		</form>
		<div id="fade" class="black_overlay"></div>
	</td>	
	<!-- end id-form  -->
	</tr>
	</td>

</tr>
<tr> 
 <td></td>
</tr>

<tr>
<td><img src="images/shared/blank.gif" width="695" height="1" alt="blank" /></td>
<td></td>
</tr>
</table>
<div class="clear"></div>
</div>
<!--  end content-table-inner  -->
</td>
<td id="tbl-border-right"></td>
</tr>
<!--<tr>
	<th class="sized bottomleft"></th>
	<td id="tbl-border-bottom">&nbsp;</td>
	<th class="sized bottomright"></th>
</tr>-->
</table>
<div class="clear">&nbsp;</div>
</div>
<!--  end content -->
<div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->
<div class="clear">&nbsp;</div> 
<!-- start footer -->         
<?php include("footer.php") ?>
<!-- end footer -->
 
</body>
</html>