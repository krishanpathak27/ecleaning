<?php include("includes/config.inc.php");
      include("includes/function.php");
	  include("includes/globalarraylist.php");
	  
	  
		$page_name="No Order Report";
	if($_SESSION['userLoginType'] == 5){
		$divisionIdString = implode(",", $divisionList);
		$condition1 = " division_id IN ($divisionIdString) ";
	} else {
		$divisionIdString = implode(",", $divisionList);
		$condition1 = " ";
	}
		
		$retailer_name = '-'; 
		$percentage = array();

	if(isset($_POST['showReport']) && $_POST['showReport'] == 'yes')
	{
		if($_POST['market']!="") 
		{
			$_SESSION['MarketList']=$_POST['market'];
			
		} else {
			
			unset($_SESSION['MarketList']);
		}
	
		
			if($_POST['filterby']!=""){
		
			$_SESSION['retfilterBY']=$_POST['filterby'];	
		
		}
	
		if($_POST['sname']!="") 
		{
			$_SESSION['SnameList']=$_POST['sname'];	
			
		} else {
		
			unset($_SESSION['SnameList']);
		}
	
	
		if($_POST['from']!="") 
		{
			$_SESSION['FromRetList']=$_objAdmin->_changeDate($_POST['from']);	
		}
		
		if($_POST['to']!="") 
		{
			$_SESSION['ToRetList']=$_objAdmin->_changeDate($_POST['to']);	
		}
		
		if($_POST['city']!="") 
		{
			$_SESSION['CityList']=$_POST['city'];
		}
	
		
		
		$_SESSION['order_by']="desc";
	
	
	} 
	else 
	{
	
		$_SESSION['FromRetList']= $_objAdmin->_changeDate(date("Y-m-d"));
		$_SESSION['ToRetList']= $_objAdmin->_changeDate(date("Y-m-d"));
		$_SESSION['order_by']="desc";
	
	}
		if($_SESSION['retfilterBY']==''){
		
			$_SESSION['retfilterBY']=1;	
		
		}
	
	
	if(isset($_REQUEST['reset']) && $_REQUEST['reset'] == 'yes')
	{
		unset($_SESSION['CityList']);	
		unset($_SESSION['MarketList']);	
		unset($_SESSION['SnameList']);
		header("Location: no_order_report.php");
	}

	if($_SESSION['CityList']!='')
	{
		$CityName=$_objAdmin->_getSelectList2('city','city_name',''," city_id='".$_SESSION['CityList']."'"); 
		$city=$CityName[0]->city_name;
	}

	if($_SESSION['SnameList']!='')
	{
		$salesman="AND s.salesman_id='".$_SESSION['SnameList']."'";
		$salArrList=$_SESSION['SnameList'];	
		$filterby=$_SESSION['retfilterBY'];
		$salesman = $_objArrayList->getSalesmanConsolidatedList($salArrList,$filterby);
	}


	if($_SESSION['MarketList']!=''){
	$list="r.retailer_location='".$_SESSION['MarketList']."'";
	} else {
	$list="r.city='".$_SESSION['CityList']."'";
	}
	
	if($_REQUEST['order_by']!='')
	{
		if($_REQUEST['order_by']=='desc')
		{
			$_SESSION['order_by']="asc";
		} else {
			$_SESSION['order_by']="desc";
		}
	
		$_SESSION['FromRetList']=$_REQUEST['from'];
		$_SESSION['ToRetList']=$_REQUEST['to'];
	
	} else {
		
		if($_POST['city']=="")
		{
			unset($_SESSION['CityList']);
		}
		
		if($_POST['sname']=="")
		{
			unset($_SESSION['SnameList']);
		}
	}
	

if( $_SESSION['userLoginType']==3){
	$disLogCond="o.distributor_id='".$_SESSION['distributorId']."' and ";
}
?>

<?php include("header.inc.php") ?>

<script type="text/javascript">
function showMarket(str,id)
{
	$.ajax({
		'type': 'POST',
		'url': 'ret_dropdown_market.php',
		'data': 'c='+str,
		'success' : function(mystring) {
			//alert(mystring);
		document.getElementById("outputMarket").innerHTML = mystring;
		$('#marketLevel').show();
		}
	});
}
</script>
<script type="text/javascript">

    function PrintElem(elem)
    {
        Popup($(elem).html());
    }

    function Popup(data) 
    {
		
        var mywindow = window.open('', 'Report');
		
        mywindow.document.write('<html><head><title>Retailer Wise Report</title>');
		mywindow.document.write('<table><tr><td><b>City Name:</b> <?php echo $city; ?></td><td><b>Market Name:</b> <?php echo $_SESSION["MarketList"]; ?></td><td><b>From Date:</b> <?php echo $_SESSION["FromRetList"]; ?></td><td><b>To Date:</b> <?php echo $_SESSION['ToRetList']; ?></td></tr></table>');
        /*optional stylesheet*/ //mywindow.document.write('<link rel="stylesheet" href="main.css" type="text/css" />');
        mywindow.document.write('</head><body >');
        mywindow.document.write(data);
        mywindow.document.write('</body></html>');

        mywindow.print();
        mywindow.close();
        return true;
    }
$(document).ready(function()
{
<?php if($_POST['submit']=='Export to Excel'){ ?>
tableToExcel('report_export', 'No order report', 'No Order Report.xls');
<?php } ?>
});
	
</script>

<script src="javascripts/dateNextPrev.js" type="text/javascript"></script>
<!-- start content-outer -->
<div id="content-outer">
<!-- start content -->
<div id="content">
<div id="page-heading"><h1><span style="color: #d74343; font-family: Tahoma; font-weight: bold;">No Order Report</span></h1></div>
<table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">

<tr>
	<!--<td id="tbl-border-left"></td>-->
	<td>
	<!--  start content-table-inner -->
	
	<div id="content-table-inner">
	
	<div id="page-heading" align="left" >
	<form name="frmPre" id="frmPre" method="post" action="" enctype="multipart/form-data" >
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
	<tr>
		
		
		<td ><h3>District: </h3><h6>
		<select name="city" id="city" class="menulist" style="width:120px;" >
			<?php echo $cityList = $_objArrayList->getCityListOptions($_SESSION['CityList'],'city');?>
		</select></h6></td>
		
		<td id="marketLevel" style="display:none;"><h3>City:</h3><h6><div id="outputMarket"></div></h6>
		
		</td>
		
		<td><h3>Salesman: </h3><h6>
		<select name="sname" id="sname" class="menulist" >
			<?php echo $salesmanOptionList = $_objArrayList->GetSalesmenMenu($salsList, $_SESSION['SnameList']);?>
		</select></h6></td>
		
		
		
		<td><h3>From Date:</h3><h6><img src="css/images/prev.png" height="18" width="18" onclick="dateFromPrev();">  <input type="text" id="from" name="from" class="date" style="width:100px" value="<?php  echo $_SESSION['FromRetList'];?>"  readonly /> <img src="css/images/next.png" height="18" width="18" onclick="dateFromNext();"></h6></td>
		<td><h3>To Date: </h3> <h6> <img src="css/images/prev.png" height="18" width="18" onclick="dateToPrev();"> <input type="text" id="to" name="to" class="date" style="width:100px" value="<?php echo $_SESSION['ToRetList']; ?>"  readonly /> <img src="css/images/next.png" height="18" width="18" onclick="dateToNext();"></h6></td>
		<td><h3></h3>
		<input name="submit" class="result-submit" type="submit" id="submit" value="View Details" />
		<input type="button" value="Reset!" class="form-reset" onclick="location.href='no_order_report.php?reset=yes';" />	
		</td>
		</tr>
		<!-- <tr class="consolidatedReport">
		<td colspan="3" ><h2>View Report For:&nbsp;&nbsp;
		<input type="radio" name="filterby" value="1" <?php if($_SESSION['retfilterBY']==1){ ?> checked="checked" <?php } ?>   />&nbsp;Individual
		<input type="radio" name="filterby" value="2" <?php if($_SESSION['retfilterBY']==2){ ?> checked="checked" <?php } ?>  />&nbsp;Hierarchy</h2>
		</td>
		</tr> -->
		<tr>
			<td colspan="6"><input name="showReport" type="hidden" value="yes" />
				<a id="dlink"  style="display:none;"></a>
				<input  type="submit" value="Export to Excel" name="submit" class="result-submit"  >
				<input type="button" value="Print" class="result-submit" onclick="PrintElem('#Report')" />
			</td>
		</tr>
	</table>
	</form>
	</div>
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
	<?php
	if($_SESSION['CityList']==""){
	?>
	<tr valign="top">
		<td>
		<table border="0" width="100%" cellpadding="0" cellspacing="0" id="id-form">
			<tr bgcolor="#A52A2A" style="color: #fff;font-weight: bold;">
				<td align="center" style="padding:10px;">Select City</td>
			</tr>
		</table>
		</td>
		<?php } else { ?>
		
		<td>
		<!-- <div style="padding:10px; font-size:15px; font-weight:bold;" >Press F2 to Check Retailer's Item Wise Report</div> -->
		<div style="width:1000px;overflow:auto; height:auto;overflow:auto;" >
		<div id="Report">
		
			
			<table  border="1"  cellpadding="0" cellspacing="0" id="report_export" name="report_export">
			<tr  bgcolor="#6E6E6E" style="color: #fff;font-weight: bold;">
				<td style="padding:10px;" width="20%"><div style="width:10%;">Salesman Name</div></td>
				<td style="padding:10px;" width="20%"><div style="width:10%;">Salesman Code</div></td>
				<td style="padding:10px;" width="10%"><div style="width:10%;">Retailer Name</div></td>
				<td style="padding:10px;" width="10%"><div style="width:10%;">Retailer Code</div></td>
				<td style="padding:10px;" width="10%"><div style="width:10%;">Distributor</div></td>
				<td style="padding:10px;" width="10%"><div style="width:10%;">Distributor Code</div></td>
				<td style="padding:10px;" width="10%"><div style="width:10%;">No of Visits</div></td>
				<td style="padding:10px;" width="10%"><div style="width:10%;">No of No orders</div></td>
				<td style="padding:10px;" width="10%"><div style="width:10%;">State</div></td>
				<td style="padding:10px;" width="10%"><div style="width:10%;">District</div></td>
				<td style="padding:10px;" width="10%"><div style="width:10%;">Taluka</div></td>
				<td style="padding:10px;" width="10%"><div style="width:10%;">Market</div></td>
			</tr>
			
			<?php
$retailer_no_order = $_objAdmin->_getSelectList2("table_order AS O 
LEFT JOIN table_salesman AS s ON s.salesman_id = O.salesman_id 
LEFT JOIN table_division as tdiv ON s.division_id=tdiv.division_id 
LEFT JOIN table_retailer AS R ON R.retailer_id = O.retailer_id AND R.status = 'A' AND O.order_type IN ('yes','no')
LEFT JOIN state AS state ON state.state_id = R.state AND state.status='A'
LEFT JOIN city AS city ON city.city_id = R.city AND city.status='A' AND city.state_id=state.state_id
LEFT JOIN table_taluka AS tt ON tt.taluka_id = R.taluka_id AND tt.status='A' AND tt.city_id=city.city_id
LEFT JOIN table_markets AS tm ON tm.market_id = R.market_id AND tm.status='A' AND tm.taluka_id=tt.taluka_id
LEFT JOIN table_distributors AS D ON D.distributor_id = O.distributor_id AND D.status = 'A'
LEFT JOIN table_retailer AS R1 ON R1.retailer_id = O.retailer_id AND R1.status = 'A' AND O.order_type IN ('no')",
	"s.salesman_name,s.salesman_code,tdiv.division_name,R.retailer_name,R.retailer_code,O.order_type,D.distributor_name,D.distributor_code,state.state_name,city.city_name,tt.taluka_name,tm.market_name,count(R.retailer_id) AS totalRetailerVisit,count(R1.retailer_id) AS totalRetailerNoOrder", '',"  O.date_of_order BETWEEN '".date('Y-m-d', strtotime($_SESSION['FromRetList']))."' AND '".date('Y-m-d', strtotime($_SESSION['ToRetList']))."' $salesman AND R.retailer_id > 0 AND city.city_id='".$_SESSION['CityList']."' AND s.division_id IN (".$divisionIdString.") GROUP BY R.retailer_id");

	if(is_array($retailer_no_order)){
		foreach ($retailer_no_order as $key => $value) {
			?>
			<tr>
				<td style="padding:10px;" width="20%"><div style="width:10%;"><?php echo ($value->salesman_name!="")?$value->salesman_name:"-";?></div></td>
				<td style="padding:10px;" width="20%"><div style="width:10%;"><?php echo ($value->salesman_code!="")?$value->salesman_code:"-";?></div></td>
				<td style="padding:10px;" width="10%"><div style="width:10%;"><?php echo ($value->retailer_name!="")?$value->retailer_name:"-";?></div></td>
				<td style="padding:10px;" width="10%"><div style="width:10%;"><?php echo ($value->retailer_code!="")?$value->retailer_code:"-";?></div></td>
				<td style="padding:10px;" width="10%"><div style="width:10%;"><?php echo ($value->distributor_name!="")?$value->distributor_name:"-";?></div></td>
				<td style="padding:10px;" width="10%"><div style="width:10%;"><?php echo ($value->distributor_code!="")?$value->distributor_code:"-";?></div></td>
				<td style="padding:10px;" width="10%"><div style="width:10%;"><?php echo ($value->totalRetailerVisit!="")?$value->totalRetailerVisit:"-";?></div></td>
				<td style="padding:10px;" width="10%"><div style="width:10%;"><?php echo ($value->totalRetailerNoOrder!="")?$value->totalRetailerNoOrder:"-";?></div></td>
				<td style="padding:10px;" width="10%"><div style="width:10%;"><?php echo ($value->state_name!="")?$value->state_name:"-";?></div></td>
				<td style="padding:10px;" width="10%"><div style="width:10%;"><?php echo ($value->city_name!="")?$value->city_name:"-";?></div></td>
				<td style="padding:10px;" width="10%"><div style="width:10%;"><?php echo ($value->taluka_name!="")?$value->taluka_name:"-";?></div></td>
				<td style="padding:10px;" width="10%"><div style="width:10%;"><?php echo ($value->market_name!="")?$value->market_name:"-";?></div></td>
			</tr>
		<?php }
	} 





$distributor_no_order = $_objAdmin->_getSelectList2("table_order AS O 
LEFT JOIN table_salesman AS s ON s.salesman_id = O.salesman_id 
LEFT JOIN table_division as tdiv ON s.division_id=tdiv.division_id 
LEFT JOIN table_distributors AS D ON D.distributor_id = O.distributor_id AND D.status = 'A' AND O.order_type IN ('yes','no')
LEFT JOIN state AS state ON state.state_id = D.state AND state.status='A'
LEFT JOIN city AS city ON city.city_id = D.city AND city.status='A' AND city.state_id=state.state_id
LEFT JOIN table_taluka AS tt ON tt.taluka_id = D.taluka_id AND tt.status='A' AND tt.city_id=city.city_id
LEFT JOIN table_markets AS tm ON tm.market_id = D.market_id AND tm.status='A' AND tm.taluka_id=tt.taluka_id
LEFT JOIN table_distributors AS D1 ON D1.distributor_id = O.distributor_id AND D1.status = 'A' AND O.order_type IN ('no')",
	"s.salesman_name,s.salesman_code,tdiv.division_name,O.order_type,D.distributor_name,D.distributor_code,D.distributor_id,state.state_name,city.city_name,tt.taluka_name,tm.market_name,count(D.distributor_id) AS totalRetailerVisit,count(D1.distributor_id) AS totalRetailerNoOrder", '',"  O.date_of_order BETWEEN '".date('Y-m-d', strtotime($_SESSION['FromRetList']))."' AND '".date('Y-m-d', strtotime($_SESSION['ToRetList']))."' $salesman AND O.retailer_id = 0 AND city.city_id='".$_SESSION['CityList']."' AND s.division_id IN (".$divisionIdString.") GROUP BY D.distributor_id");

		if(is_array($distributor_no_order)){
			foreach ($distributor_no_order as $key => $value) {
			?>
			<tr>
				<td style="padding:10px;" width="20%"><div style="width:10%;"><?php echo ($value->salesman_name!="")?$value->salesman_name:"-";?></div></td>
				<td style="padding:10px;" width="20%"><div style="width:10%;"><?php echo ($value->salesman_code!="")?$value->salesman_code:"-";?></div></td>
				<td style="padding:10px;" width="10%"><div style="width:10%;"><?php echo ($value->retailer_name!="")?$value->retailer_name:"-";?></div></td>
				<td style="padding:10px;" width="10%"><div style="width:10%;"><?php echo ($value->retailer_code!="")?$value->retailer_code:"-";?></div></td>
				<td style="padding:10px;" width="10%"><div style="width:10%;"><?php echo ($value->distributor_name!="")?$value->distributor_name:"-";?></div></td>
				<td style="padding:10px;" width="10%"><div style="width:10%;"><?php echo ($value->distributor_code!="")?$value->distributor_code:"-";?></div></td>
				<td style="padding:10px;" width="10%"><div style="width:10%;"><?php echo ($value->totalRetailerVisit!="")?$value->totalRetailerVisit:"-";?></div></td>
				<td style="padding:10px;" width="10%"><div style="width:10%;"><?php echo ($value->totalRetailerNoOrder!="")?$value->totalRetailerNoOrder:"-";?></div></td>
				<td style="padding:10px;" width="10%"><div style="width:10%;"><?php echo ($value->state_name!="")?$value->state_name:"-";?></div></td>
				<td style="padding:10px;" width="10%"><div style="width:10%;"><?php echo ($value->city_name!="")?$value->city_name:"-";?></div></td>
				<td style="padding:10px;" width="10%"><div style="width:10%;"><?php echo ($value->taluka_name!="")?$value->taluka_name:"-";?></div></td>
				<td style="padding:10px;" width="10%"><div style="width:10%;"><?php echo ($value->market_name!="")?$value->market_name:"-";?></div></td>
			</tr>
			<?php }
			} ?>
			</table>

		<!-- <table  border="1" width="100%" cellpadding="0" cellspacing="0" id="lists">
		<tr  style="border-bottom:2px solid #6E6E6E;" align="center" >
		 <td style="padding:10px;" width="100%">Report Not Available</td>
		</tr>
		</table> -->
		</div>
		</div>
		</td>
	<?php } ?>
	</tr>
	<tr>
		<td><img src="images/blank.gif" width="695" height="1" alt="blank" /></td>
		<td></td>
	</tr>
	</table>
<div class="clear"></div>
</div>

<!--  end content-table-inner  -->
</td>
<td id="tbl-border-right"></td>
</tr>
<!--<tr>
	<th class="sized bottomleft"></th>
	<td id="tbl-border-bottom">&nbsp;</td>
	<th class="sized bottomright"></th>
</tr>-->
</table>
<div class="clear">&nbsp;</div>
</div>
<!--  end content -->
<div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->
<div class="clear">&nbsp;</div> 
<!-- start footer -->        
<?php include("footer.php") ?>

<!-- end footer -->
<script src="javascripts/jquery-1.8.2.js" type="text/javascript"></script>
<script src="javascripts/jquery-ui.js"></script>
<script type="text/javascript" src="javascripts/validate.js"></script>
<script type="text/javascript">
// Popup window code
function newPopup(url) {
	popupWindow = window.open(
		url,'popUpWindow','height=500,width=600,left=200,top=100,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no,status=yes')
}
</script>
<script>
    $(function() {
        $( "#from" ).datepicker({
			dateFormat: "d M yy",
            defaultDate: "w",
            changeMonth: true,
            numberOfMonths: 1,
            onSelect: function( selectedDate ) {
                $( "#to" ).datepicker( "option", "minDate", selectedDate );
            }
        });
        $( "#to" ).datepicker({
			dateFormat: "d M yy",
            defaultDate: "-w",
            changeMonth: true,
            numberOfMonths: 1,
            onSelect: function( selectedDate ) {
                $( "#from" ).datepicker( "option", "maxDate", selectedDate );
            }
        });
    });
</script>
<script type='text/javascript'>//<![CDATA[ 
var tableToExcel = (function () {
        var uri = 'data:application/vnd.ms-excel;base64,'
        , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table><tr><td><b>Salesman Name:</b> <?php echo $sal_name; ?></td><td><b>From Date:</b> <?php echo $_objAdmin->_changeDate($from_date); ?></td><td><b>To Date:</b> <?php echo $_objAdmin->_changeDate($to_date); ?></td></tr></table><table>{table}</table></body></html>'
        , base64 = function (s) { return window.btoa(unescape(encodeURIComponent(s))) }
        , format = function (s, c) { return s.replace(/{(\w+)}/g, function (m, p) { return c[p]; }) }
        return function (table, name, filename) {
            if (!table.nodeType) table = document.getElementById(table)
            var ctx = { worksheet: name || 'Worksheet', table: table.innerHTML }

            document.getElementById("dlink").href = uri + base64(format(template, ctx));
            document.getElementById("dlink").download = filename;
            document.getElementById("dlink").click();

        }
    })()

//]]>  
</script>
</body>
</html>
