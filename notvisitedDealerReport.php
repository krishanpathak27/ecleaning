<?php
include("includes/config.inc.php");
include("includes/function.php");
$_objAdmin = new Admin();

if(isset($_REQUEST['show']) && $_REQUEST['show']=="yes")
{
	$_objAdmin->showNotVisitedDealerReport();
	die;
}
if(isset($_POST['DispatchOS']) && $_POST['DispatchOS'] == 'yes')
{	
	
	if($_POST['from']!="") 
	{
	$_SESSION['FromVisited']=$_objAdmin->_changeDate($_POST['from']);	
	}
	if($_POST['to']!="") 
	{
	$_SESSION['ToVisited']=$_objAdmin->_changeDate($_POST['to']);	
	}
	
	
	
   if(isset($_POST['export']) && $_POST['export'])
   {
   header("Location:export.inc.php?export_retailer_not_visited");
   exit;
   }	
	
	header("Location:notvisitedDealerReport.php");
}
if(isset($_REQUEST['reset']) && $_REQUEST['reset'] == 'yes')
{
	unset($_SESSION['RetailerID']);	
	$_SESSION['FromVisited']= $_objAdmin->_changeDate(date('Y-m-d'));
	$_SESSION['ToVisited']= $_objAdmin->_changeDate(date('Y-m-d'));
	header("Location: notvisitedDealerReport.php");
}
?>

<?php include("header.inc.php") ?>
<script src="javascripts/dateNextPrev.js" type="text/javascript"></script>

<!-- start content-outer -->
<input name="pagename" type="hidden"  id="pagename" value="notvisitedDealerReport.php" />
<div id="content-outer">
<!-- start content -->
<div id="content">
<div id="page-heading"><h1><span style="color: #d74343; font-family: Tahoma; font-weight: bold;">Not Visited Dealer</span></h1></div>
<table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">
<!--<tr>
	<th rowspan="3" class="sized"><img src="images/side_shadowleft.jpg" width="20" height="300" alt="" /></th>
	<th class="topleft"></th>
	<td id="tbl-border-top">&nbsp;</td>
	<th class="topright"></th>
	<th rowspan="3" class="sized"><img src="images/side_shadowright.jpg" width="20" height="300" alt="" /></th>
</tr>
-->
<tr>
	<!--<td id="tbl-border-left"></td>-->
	<td>
	<!--  start content-table-inner -->
	<div id="content-table-inner">
		
	<div id="page-heading" align="left" >
	<form name="frmPre" id="frmPre" method="post" action="" enctype="multipart/form-data" >
	<table border="0" width="85%" cellpadding="0" cellspacing="0">
	
	<tr>
	
	<?php //echo "<pre>"; print_r($_SESSION);?>
	
	
	
		<td><h3>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;From Date:</h3><h6><img src="css/images/prev.png" height="18" width="18" onclick="dateFromPrev();"> <input type="text" id="from" name="from" class="date" style="width:150px" value="<?php if($_SESSION['FromVisited']!='') { echo $_SESSION['FromVisited']; } else { echo $_objAdmin->_changeDate(date('Y-m-d')); }?>"  readonly /> <img src="css/images/next.png" height="18" width="18" onclick="dateFromNext();"></h6></td>
		<td><h3>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;To Date: </h3><h6><img src="css/images/prev.png" height="18" width="18" onclick="dateToPrev();"> <input type="text" id="to" name="to" class="date" style="width:150px" value="<?php if($_SESSION['ToVisited']!='') { echo $_SESSION['ToVisited']; } else { echo $_objAdmin->_changeDate(date('Y-m-d')); }?>"  readonly /><img src="css/images/next.png" height="18" width="18" onclick="dateToNext();"></h6></td>
		<td><h3></h3><input name="DispatchOS" type="hidden" value="yes" />
		
		<input name="submit" class="result-submit" type="submit" id="submit" value="View Details" />
		<input type="button" value="Reset!" class="form-reset" onclick="location.href='notvisitedDealerReport.php?reset=yes';" />
		<input name="export" class="result-submit" type="submit" id="export" value="Export to Excel" ></td>
		</tr>
		
	 </table>
	</form>
	
	</div>
	
	
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
	<tr valign="top">
	<td>
		<!-- start id-form -->
		<table  border="0" cellspacing="0" cellpadding="0"  align="center" width="100%">
				<tr>
				<td align="lift" valign="top"  >
				<table id="flex1" style="display:none"></table>
				<script type="text/javascript">showNotVisitedDealerReport();</script>
				</td>
				</tr>
				</table>
	<!-- end id-form  -->

	</td>
<td>
 <!-- right bar-->
 <?php //include("rightbar/dispatch_bar.php") ?>
 </td>
</tr>
<tr>
<td><img src="images/blank.gif" width="695" height="1" alt="blank" /></td>
<td></td>
</tr>
</table>
<div class="clear"></div>
</div>
<!--  end content-table-inner  -->
</td>
<td id="tbl-border-right"></td>
</tr>
<!--<tr>
	<th class="sized bottomleft"></th>
	<td id="tbl-border-bottom">&nbsp;</td>
	<th class="sized bottomright"></th>
</tr>-->
</table>
<div class="clear">&nbsp;</div>
</div>
<!--  end content -->
<div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->
<div class="clear">&nbsp;</div> 
<!-- start footer -->         
<?php include("footer.php") ?>
<!-- end footer -->
<script src="javascripts/jquery-1.8.2.js" type="text/javascript"></script>
<script src="javascripts/jquery-ui.js"></script>
<script type="text/javascript" src="javascripts/validate.js"></script>
<script type="text/javascript">
// Popup window code
function newPopup(url) {
	popupWindow = window.open(
		url,'popUpWindow','height=500,width=600,left=200,top=100,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no,status=yes')
}
</script>
<script>
    $(function() {
        $( "#from" ).datepicker({
			dateFormat: "d M yy",
            defaultDate: "w",
            changeMonth: true,
            numberOfMonths: 1,
            onSelect: function( selectedDate ) {
                $( "#to" ).datepicker( "option", "minDate", selectedDate );
            }
        });
        $( "#to" ).datepicker({
			dateFormat: "d M yy",
            defaultDate: "-w",
            changeMonth: true,
            numberOfMonths: 1,
            onSelect: function( selectedDate ) {
                $( "#from" ).datepicker( "option", "maxDate", selectedDate );
            }
        });
    });
</script>
</body>
</html>