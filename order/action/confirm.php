<?php
//include("includes/config.inc.php");
//include("includes/function.php");
$_objAdmin = new Admin();
$day= date("D");


if(isset($_REQUEST['action']) && $_REQUEST['action']=='confirm' && isset($_REQUEST['id']) && $_REQUEST['id']!="" && is_int((int) $_REQUEST[id])) {


  $auOrd=$_objAdmin->_getSelectList('table_order_tmp as o left join table_retailer as r on o.retailer_id=r.retailer_id left join table_distributors as d on d.distributor_id=o.distributor_id left join table_salesman as s on s.salesman_id=o.salesman_id',"o.*,d.distributor_name, r.retailer_name,r.retailer_address,r.retailer_location,s.salesman_name",'',"o.order_tmp_id=".$_GET['id']." ");
      if(sizeof($auOrd)<=0) {
        header("location: order.php?err=error");
        exit;
      }

  //print_r($auOrd);

} else {

    header("Location: order.php?err=err");
    exit;
}

?>
    <!-- Bootstrap core CSS -->
    <link href="bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Bootstrap theme -->
    <link href="bootstrap/dist/css/bootstrap-theme.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <!--<link href="bootstrap/css/theme.css" rel="stylesheet">-->

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <!--<script src="bootstrap/assets/js/ie-emulation-modes-warning.js"></script>-->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- start content-outer -->
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
   <script src="javascripts/jquery-1.11.2.min.js"></script>
   <!--<script src="bootstrap/dist/js/bootstrap.min.js"></script>-->
     <!--<script src="bootstrap/assets/js/docs.min.js"></script>-->
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
   <!--<script src="bootstrap/assets/js/ie10-viewport-bug-workaround.js"></script>-->
<style>
.frmSearch {border: 1px solid #F0F0F0;background-color:#C8EEFD;margin: 2px 0px;padding:40px;}
#retailer-list{float:left;list-style:none;margin:0;padding:0;width:100%; height: 200px; overflow-y: scroll;}
#retailer-list li{padding: 10px; background:#FAFAFA;border-bottom:#F0F0F0 1px solid;}
#retailer-list li:hover{background:#F0F0F0;}
#search-box{padding: 10px;border: #F0F0F0 1px solid;}
</style>
<!--<script type="text/javascript" src="javascripts/jquery-2.1.1.min.js"></script>
<script type="text/javascript" src="javascripts/jquery-ui-v1.11.2.min.js"></script>-->
<script src="javascripts/jquery-ui-1.11.4.js"></script>
<script src="javascripts/order_custom_jquery.js"></script>
<!-- start content -->
<!-- start content -->
<div id="content">
<div id="page-heading"><h1><span style="color: #d74343; font-family: Tahoma; font-weight: bold;">Order Cart</span></h1></div>
<table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">
<tr>
  <td>
    <div id="content-table-inner">
      <table border="0" width="100%" cellpadding="0" cellspacing="0">
     <tr valign="top">
  <td>
    
    <!-- start id-form -->
    
    <table border="0" width="100%" cellpadding="0" cellspacing="0" id="id-form">
    <tr>
        <td><b>Distributor Name : </b> <?php echo $auOrd[0]->distributor_name;?></td>
      </tr>
      <tr>
        <td><b>Retailer Name : </b> <?php echo $auOrd[0]->retailer_name;?></td>
        <td align="right"><b>Last update date : </b> <?php echo date("d M Y", strtotime($auOrd[0]->date_of_order)); ?></td>
      </tr>
      <tr>
        <td><b>Retailer Mobile-No : </b> <?php echo $auOrd[0]->retailer_phone_no;?></td>
        <td align="right"><b>Last updated time : </b> <?php echo date("h:i:s A", strtotime($auOrd[0]->time_of_order));?></td>
      </tr>
      <tr>
        <td><b>Retailer Market : </b> <?php echo $auOrd[0]->retailer_location;?></td>
        <td align="right"><?php if($auOrd[0]->bill_no !=''){ ?><b>Bill Date:</b> <?php echo $auOrd[0]->bill_date; }?></td>
        
      </tr>
      <tr>
        <td><b>Retailer Address : </b> <?php echo $auOrd[0]->retailer_address;?></td>
        <td align="right"><?php if($auOrd[0]->bill_no !=''){ ?><b>Bill No:</b> <?php echo $auOrd[0]->bill_no; }?></td>
      </tr>
      <!--<tr>
        <td colspan="2" align="center"><img src="images/accept.jpg" width="15" height="15" alt="" /> <b>Accepted</b> &nbsp;&nbsp;&nbsp;&nbsp;<img src="images/reject.jpg" width="15" height="15" alt="" /> <b>Rejected</b></td>
      </tr> -->
    
    </table>





    <form name="frmPre" id="frmPre" method="post" action="" enctype="multipart/form-data" >


    <table  border="1" width="100%" cellpadding="0" cellspacing="0" id="id-form">
    <tr bgcolor="#6E6E6E" style="color: #fff;font-weight: bold;">
        <td style="padding:10px;" width="10%">Status</td>
        <td style="padding:10px;" width="30%">Particulars</td>
        <!--<td style="padding:10px;" width="10%">Item Code</td>-->
        <td style="padding:10px;" width="10%">Item Color</td>
        <td style="padding:10px;" width="10%">Order Quantity</td>
        <!--<td style="padding:10px;" width="10%">Accept Quantity</td>-->
        <td style="padding:10px;" width="5%">Price</td>
        <td style="padding:10px;" width="5%" align="right">Total</td>
      </tr>







    <?php
    $total=array();
    $condi=" where t.order_tmp_id=".$auOrd[0]->order_tmp_id." and order_detail_status!=5";
    $Rec = mysql_query("SELECT t.*,i.item_name,i.item_code,c.color_code,g.tag_description FROM table_order_tmp_detail as t left join table_item as i on t.item_id=i.item_id left join table_color as c on t.color_id=c.color_id left join table_tags as g on g.tag_id=t.tag_id".$condi);
    while ($auRec = mysql_fetch_array($Rec)){ ?>



    <tr <?php if($auRec['order_detail_status']==2){ ?> style="background-color:#82FA58;border-bottom:2px solid #6E6E6E;" <?php } ?> <?php if($auRec['order_detail_status']==3){ ?> style="background-color:#d74343;color:#ffffff;border-bottom:2px solid #6E6E6E;" <?php } ?>style="border-bottom:2px solid #6E6E6E;" >

      <?php if($auRec['type']==2){ ?>


      <?php if($auRec['discount_type']==1) {?>


      <td style="padding:10px;">Scheme</td>

      <td style="padding:10px;" colspan="4">

        <font color="red"><?php echo $auRec['discount_desc'];?></font>

      </td>

      <td style="padding:10px;" width="5%" align="right"><font color="red">-<?php echo $auRec['acc_total'];?></font></td>


      <?php } ?>



      <?php if($auRec['discount_type']==2) {?>

      <td style="padding:10px;" >Scheme</td>

      <td style="padding:10px;" colspan="4">

      <font color="red"><?php echo $auRec['discount_desc'];?></font>

      </td>

      <td style="padding:10px" width="5%" align="right"><font color="red">-<?php echo $auRec['acc_total'];?></font></td>
      <?php } ?>


      <?php if($auRec['discount_type']==3) {?>

      <td style="padding:10px;" >Scheme</td>

      <td style="padding:10px;" colspan="2">

      <font color="red"><?php echo $auRec['discount_desc'];?></font>

      </td>

      <td style="padding:10px;" ><font color="red"><?php echo $auRec['total_free_quantity'];?></font></td>

      <!--<td style="padding:10px;" ><font color="red"><?php echo $auRec['acc_free_item_qty'];?></font></td>-->

      <td style="padding:10px;" ></td>

      <td style="padding:10px;" width="5%" align="right"><font color="red"><?php echo $auRec['acc_total'];?></font></td>

      <?php } ?>




      <?php } else { 

      if($auRec['tag_id']!=0){?>
        <td style="padding:10px;" width="10%"><?php echo $auRec['tag_description'] ?></td>
      <?php } else { ?>
       <td style="padding:10px;" width="10%"></td>
      <?php } ?>



      <td style="padding:10px;" width="25%"><?php echo $auRec['item_name'];?></td>
      <td style="padding:10px;" width="8%"><?php echo $auRec['item_code'];?></td>



      <?php if($auRec['color_type']==3) { ?>
      <!--<td style="padding:10px;" ><?php echo $auRec['color_code'];?></td>-->
      <?php } ?>
      <?php if($auRec['color_type']==2) { ?>
      <!--<td style="padding:10px;" >Assorted</td>-->
      <?php } ?>
      <?php if($auRec['color_type']==1 || $auRec['color_type']=='') { ?>
      <!--<td style="padding:10px;" ></td>-->
      <?php } ?>




      <td style="padding:10px;" width="10%"><?php echo $auRec['quantity'];?></td>

      <!--<td style="padding:10px;" width="15%">
      <?php echo $auRec['acc_quantity'];?>
      </td>-->



      <td style="padding:10px;" width="5%"><?php echo $auRec['price'];?></td>
      <td style="padding:10px;" width="5%" align="right"><?php echo $auRec['acc_total']; $total[]=$auRec['acc_total']; ?></td>


      <?php } ?>


    </tr>


    <?php } // close order detail ?>







    <?php
    //combo discount list
    $comboRec = mysql_query("SELECT dc.order_combo_detail_id,dc.acc_discount_desc,dc.acc_discount_amount,dc.discount_type,dc.acc_free_item_qty,dc.free_item_qty,i.item_code FROM table_order_tmp_combo_detail as dc left join table_item as i on i.item_id=dc.acc_free_item_id where dc.order_tmp_id=".$auOrd[0]->order_tmp_id." and dc.status!='5'");
    while ($comboListRec = mysql_fetch_array($comboRec)){
    ?>
    <tr  style="border-bottom:2px solid #6E6E6E;">
      <?php if($comboListRec['discount_type']==1) {?>
      <td style="padding:10px;">Scheme</td>
      <td style="padding:10px;" <?php if($auOrd[0]->order_status=="A"){ ?> colspan="5" <?php } else { ?>  colspan="6" <?php } ?>>
      <font color="red"><?php echo $comboListRec['acc_discount_desc'];?></font>
      </td>
      <td style="padding:10px" width="5%" align="right"><font color="red">-<?php echo $comboListRec['acc_discount_amount'];?></font></td>
      <?php } ?>
      <?php if($comboListRec['discount_type']==2) {?>
      <td style="padding:10px;" >Scheme</td>
      <td style="padding:10px;" <?php if($auOrd[0]->order_status=="A"){ ?> colspan="5" <?php } else { ?>  colspan="6" <?php } ?>>
      <font color="red"><?php echo $comboListRec['acc_discount_desc'];?></font>
      </td>
      <td style="padding:10px;" width="5%" align="right"><font color="red">-<?php echo $comboListRec['acc_discount_amount'];?></font></td>
      <?php } ?>
      <?php if($comboListRec['discount_type']==3) {?>
      <td style="padding:10px;" >Scheme</td>
      <td style="padding:10px;" colspan="1" >
      <font color="red"><?php echo $comboListRec['acc_discount_desc'];?></font>
      </td>
      <td style="padding:10px;" ><font color="red"><?php echo $comboListRec['item_code'];?></font></td>
      <td style="padding:10px;" ></td>
      <td style="padding:10px;" ><font color="red"><?php echo $comboListRec['free_item_qty'];?></font></td>
      <td style="padding:10px;" ><font color="red"><?php echo $comboListRec['acc_free_item_qty'];?></font></td>
      <td style="padding:10px;" ></td>
      <td style="padding:10px;" width="5%" align="right"><font color="red"><?php echo $comboListRec['acc_discount_amount'];?></font></td>
      <?php } ?>
    </tr>
    <?php } ?>



    <?php
    $net = mysql_query("SELECT o.*,t.tag_description FROM table_order_tmp as o left join table_tags as t on t.tag_id=o.tag_id where o.order_tmp_id=".$auOrd[0]->order_tmp_id." " );
    $netTotal = mysql_fetch_array($net);
    if($netTotal['acc_discount_id']!=0){ ?>


    <tr style="border-bottom:2px solid #6E6E6E; border-top:2px solid #6E6E6E;" >
      <td style="padding:10px" align="right" colspan="7">
      <b>Total</b></td>
      <td align="right" style="padding:10px"><b><?php echo number_format(array_sum($total), 2, '.', ',');?></b></td>
    </tr>
    <tr  >
      <td style="padding:10px;" colspan="">
      <font color="red"><b>Scheme(<?php echo $netTotal['discount_desc'];?>)</b></font></td>
      <td style="padding:10px;"><font color="red"><b><?php echo $netTotal['acc_free_item_qty'];?>
      <td style="padding:10px;"><font color="red"><b><?php //echo $netTotal['discount_amount'];?>
      <td style="padding:10px;" align="right"><font color="red"><b><?php if($netTotal['acc_discount_amount']!=0.00){ echo "-".$netTotal['acc_discount_amount'] ; }?></b></font></td>
    </tr>


    <?php } ?>





    <tr style="border-bottom:2px solid #6E6E6E; border-top:2px solid #6E6E6E;" >
      <td style="padding:10px;" align="right" colspan="5">
      <b>Net Total</b></td>
      <td style="padding:10px;" align="right"><b><?php echo $netTotal['acc_total_invoice_amount'];?></b></td>
    </tr>




    <?php if($auOrd[0]->acc_total_invoice_amount!=0.0){?>

     <!-- <tr>
        <td colspan="6">
          <table>
            <tr>
            <td style="padding:10px;">
              <div class="input-group">
                <span class="input-group-addon" id="basic-addon1">Invoice No:</span>
                <input type="text" name="bill_no" id="bill_no" class="form-control required" placeholder="Invoice Number" value="<?php echo $auOrd[0]->bill_no; ?>" aria-describedby="basic-addon1">
              </div>
            </td>


           <td style="padding:10px;">
            <div class="input-group">
                  <span class="input-group-addon" id="basic-addon1">Invoice No:</span>
                  <input type="text" class="form-control" name="invoice_date" id="datepicker" value="<?php echo $_objAdmin->_changeDate($auOrd[0]->bill_date); ?>">
                </div>
             
            </td>
          </tr>
        </table>
      </td>
    </tr>-->

    <tr>
        <td colspan="6">
          <table>
            <tr>
              <td style="padding:10px;">
                <input type="button" value="Back" class="btn btn-default" onclick="location.href='order.php'" />
              </td>

              <td style="padding:10px;">
                <input name="acc_order_id" type="hidden" value="<?php echo $auOrd[0]->order_tmp_id; ?>" />
                <input name="add" type="hidden" value="yes" />
                <!--<input name="submit" class="btn btn-success" type="submit" id="submit" value="Dispatch" disabled="" />-->
              </td>
          </tr>
        </table>
      </td>
    </tr>

    <?php } else { ?>

      <!--<tr align="center">
        <td style="padding:10px;">
        <input type="button" value="Back" class="btn btn-default" onclick="location.href='order.php?action=edit&id='".$_REQUEST['id'] /></td>
      </tr> -->

    <?php } ?>



      
  </table>

  </form>

  </td>

  </tr>

  <tr valign="top">
  <td>
    <div id="div1"></div>
  </td>
  </tr>
  </div>
  </td>
</tr>
</table>

<div class="clear"></div>
</div>
<!--  end content-table-inner  -->
</td>
</tr>
</table>
<table>
 <tr align="center"><td style="padding:10px;"></td></tr>
</table>
<div class="clear">&nbsp;</div>
</div>

<!--<script src="javascripts/jquery-1.8.2.js" type="text/javascript"></script>
<script src="javascripts/jquery-ui.js"></script>
<script type="text/javascript" src="javascripts/validate.js"></script>-->