<?php include("includes/config.inc.php");
      include("includes/function.php");
	  include("includes/globalarraylist.php");

	$page_name="Order Collection Report";
	$stateRec=$_objAdmin->_getSelectList2('state',"state_name,state_id",'','status="A"');



	if(isset($_POST['showReport']) && $_POST['showReport'] == 'yes')
	{
		

  
	    if($_POST['division'] !=''){
	        $_SESSION['findDivision'] = $_POST['division'];
	    }
	 
	    if($_POST['customer']!=''){
	        $_SESSION['findCust'] = $_POST['customer'];
	    }

	    if($_POST['customerclass'] !=''){
	        $_SESSION['customerClassRec'] = $_POST['customerclass'];
	    }

	    
	   
	    if($_POST['state'] !=''){
	        $_SESSION['stateCust'] = $_POST['state'];
	    }
	    if($_POST['district'] !=''){
	        $_SESSION['districtCust'] = $_POST['district'];
	    }
	    if($_POST['tehsil'] !=''){
	        $_SESSION['tehsilCust'] = $_POST['tehsil'];
	    }
    

		if($_POST['sal']!="" && $_POST['sal']!='All') 
		{
		 $salesman=" and s.salesman_id='".$_POST['sal']."' ";
		 $sals_id=$_POST['sal'];
		  $_SESSION['employeeCust'] = $_POST['sal'];
		}
		elseif($_POST['sal']=='All')
		{
			 $salesman;
			 $_SESSION['employeeCust'] = $_POST['sal'];
		}

		if($_POST['from']!="") 
		{
		$from_date=$_objAdmin->_changeDate($_POST['from']);	
		}
		if($_POST['to']!="") 
		{
		$to_date=$_objAdmin->_changeDate($_POST['to']);	
		}
	} else {
		$from_date= $_objAdmin->_changeDate(date("Y-m-d"));
		$to_date= $_objAdmin->_changeDate(date("Y-m-d"));
		$salesman;	
	}

	if($sal_id!=''){
		$SalName=$_objAdmin->_getSelectList('table_salesman','salesman_name',''," salesman_id='".$sals_id."'"); 
		$sal_name=$SalName[0]->salesman_name;
	}
/*	if($_SESSION['userLoginType'] == 5){
		$divisionIdString = implode(",", $divisionList);
		$division = " s.division_id IN ($divisionIdString) ";
		$division1 = " division_id IN ($divisionIdString) ";
	} else {
		$division = " ";
		$division1 = " ";
	}
	if(isset($_POST['division'])){
		if($_POST['division'] != 'all'){
			$division = " s.division_id IN (".$_POST['division'].") ";
		} else {
			$division = "";
		}
	}*/
	$divisionRec=$_objAdmin->_getSelectList2('table_division',"division_name as division,division_id",'',$division1);



$classCondition = "";
$divisionCondition = "";
$stateCondition = "";
$talukaCondition = "";




// if(isset($_SESSION['customerClassRec']) && $_SESSION['customerClassRec']!='all'){
//     $classCondition =" AND rr.relationship_id='".$_SESSION['customerClassRec']."'";
// } 


if($_SESSION['userLoginType'] == 5 && !isset($_SESSION['findDivision'])){
    $divisionIdString = implode(",", $divisionList);
    $divisionCondition = " AND ( R.division_id IN ($divisionIdString) OR  D.division_id IN($divisionIdString) OR C.division_id IN($divisionIdString) )";
} 

if(isset($_SESSION['findDivision']) && $_SESSION['findDivision']!="all"){
   $divisionCondition = " AND ( R.division_id IN(".$_SESSION['findDivision'].") OR  D.division_id IN(".$_SESSION['findDivision'].") OR  C.division_id IN(".$_SESSION['findDivision'].") )";
}

if($_SESSION['userLoginType'] == 5) {
	$employeeCondition = $salesman;
}

if(isset($_SESSION['employeeCust']) && $_SESSION['employeeCust']!="All"){
    $employeeCondition = " AND s.salesman_id =".$_SESSION['employeeCust']."";
} 
//else if(isset($_SESSION['employeeCust']) && $_SESSION['employeeCust']=="All"){
	//$salesmanGroupByCondition = ", O.salesman_id";
//}


$customerTypeCondition = "";

if(isset($_SESSION['findCust']) && $_SESSION['findCust']=="R"){
    $customerTypeCondition = " AND O.retailer_id > 0 ";
} else if(isset($_SESSION['findCust']) && $_SESSION['findCust']=="D"){
    $customerTypeCondition = " AND O.distributor_id > 0  AND O.retailer_id = 0";
} else if(isset($_SESSION['findCust'])  && $_SESSION['findCust']!="all"){
    $customerTypeCondition = " AND O.customer_id > 0 AND C.customer_type = '".$_SESSION['findCust']."' ";
}

if(isset($_SESSION['stateCust']) && $_SESSION['stateCust']!="all"){
    $stateCondition = " AND (R.state IN(".$_SESSION['stateCust'].") OR D.state IN(".$_SESSION['stateCust'].")  OR C.state IN(".$_SESSION['stateCust'].") )";
}

// if(isset($_SESSION['districtCust']) && $_SESSION['districtCust']!="all"){
//     $cityCondition = " AND CT.city_id IN(".$_SESSION['districtCust'].")";
// }

if(isset($_SESSION['districtCust']) && $_SESSION['districtCust']!="all"){
    $cityCondition = " AND (R.city IN(".$_SESSION['districtCust'].") OR D.city IN(".$_SESSION['districtCust'].")  OR C.city IN(".$_SESSION['districtCust'].") )";
}


// if(isset($_SESSION['tehsilCust']) && $_SESSION['tehsilCust']!="all"){
//     $talukaCondition = " AND TL.taluka_id IN(".$_SESSION['tehsilCust'].")";
// }

if(isset($_SESSION['tehsilCust']) && $_SESSION['tehsilCust']!="all"){
    $talukaCondition = " AND ( R.taluka_id IN(".$_SESSION['tehsilCust'].") OR D.taluka_id IN(".$_SESSION['tehsilCust'].") OR C.taluka_id IN(".$_SESSION['tehsilCust'].") )";
}


if(isset($_SESSION['customerClassRec']) && $_SESSION['customerClassRec']!='all'){
    $classCondition =" AND ( R.relationship_id IN(".$_SESSION['customerClassRec'].") OR D.relationship_id IN(".$_SESSION['customerClassRec'].") OR C.relationship_id IN(".$_SESSION['customerClassRec'].") )";
} 




if(isset($_REQUEST['reset']) && $_REQUEST['reset'] == 'yes')
	{
		    unset($_SESSION['findCust']); 		   
		    unset($_SESSION['findDivision']); 
		    unset($_SESSION['customerClassRec']);
		    unset($_SESSION['employeeCust']);		   
		    unset($_SESSION['stateCust']); 
		    unset($_SESSION['districtCust']); 
		    unset($_SESSION['tehsilCust']);
		    
		header("Location: order_collection_report.php");
	}


	$custData = array();


	//echo $_SESSION['findCust'];
	// $tdr1=$_objAdmin->_getSelectList('table_order as O left join table_retailer as R on O.retailer_id=R.retailer_id left join table_salesman as S on O.salesman_id=S.salesman_id left join table_distributors as d on o.distributor_id=d.distributor_id','count(*) as total_visit','',"o.date_of_order >= '".date('Y-m-d', strtotime(mysql_escape_string($duration_start_date)))."' and o.date_of_order <= '".date('Y-m-d', strtotime(mysql_escape_string($duration_start_date)))."' and r.new='' $salesman $orderby"); 



	// $custData = $_objAdmin->_getSelectList("table_order AS O
	// LEFT JOIN table_retailer as R on O.retailer_id=R.retailer_id AND R.new='' 
	// LEFT JOIN table_distributors as D on O.distributor_id=D.distributor_id AND D.new=''
	// LEFT JOIN table_customer as C on O.customer_id=C.customer_id AND C.new=''
	// LEFT JOIN table_customer_type AS CT ON CT.type_code = C.customer_type
 //    LEFT JOIN table_salesman AS s ON s.salesman_id = O.salesman_id",
 //    ' CASE WHEN (O.distributor_id>0 AND O.retailer_id = 0 AND D.new="") THEN COUNT(O.distributor_id)  
 //    WHEN (O.retailer_id>0 AND R.new="") THEN COUNT(O.retailer_id) 
 //    WHEN (O.customer_id>0 AND C.new="") THEN COUNT(O.customer_id) ELSE 0 END AS totol_visit,  
 //    CASE WHEN (O.order_type ="Yes" AND O.retailer_id = 0 AND O.distributor_id > 0 AND D.new="") THEN COUNT(O.distributor_id)
 //    WHEN (O.order_type ="Yes" AND O.retailer_id>0 AND R.new="") THEN COUNT(O.retailer_id) 
 //    WHEN (O.order_type ="Yes" AND O.customer_id>0 AND C.new="") THEN COUNT(O.customer_id) ELSE 0 END AS total_order, R.retailer_name, R.retailer_id, D.distributor_name, D.distributor_id, C.customer_name, C.customer_id, CT.type_name',''," $classCondition  $divisionCondition $employeeCondition  $stateCondition $cityCondition $talukaCondition AND O.date_of_order >= '".date('Y-m-d', strtotime(mysql_escape_string($from_date)))."' and O.date_of_order <= '".date('Y-m-d', strtotime(mysql_escape_string($to_date)))."' group by O.salesman_id , O.retailer_id, O.distributor_id, O.customer_id"  );

	$custData = $_objAdmin->_getSelectList("table_order AS O
	LEFT JOIN table_retailer as R on O.retailer_id=R.retailer_id AND R.new='' 
	LEFT JOIN table_distributors as D on O.distributor_id=D.distributor_id AND D.new=''
	LEFT JOIN table_customer as C on O.customer_id=C.customer_id AND C.new=''
	LEFT JOIN table_customer_type AS CT ON CT.type_code = C.customer_type
    LEFT JOIN table_salesman AS s ON s.salesman_id = O.salesman_id",
    'CASE WHEN (O.distributor_id>0 AND O.retailer_id = 0) THEN COUNT(O.distributor_id)  WHEN (O.retailer_id>0) THEN COUNT(O.retailer_id) WHEN (O.customer_id>0) THEN COUNT(O.customer_id) ELSE 0 END AS totol_visit, SUM(CASE WHEN O.order_type ="Yes" THEN 1 ELSE 0 END) AS total_order, R.retailer_name, R.retailer_id, R.relationship_id AS retailer_relationship_id, R.division_id AS retailer_division_id, R.retailer_address, D.distributor_name, D.distributor_id,D.relationship_id AS distributor_relationship_id, D.division_id AS distributor_division_id, D.distributor_address, C.customer_name, C.customer_id, C.relationship_id AS customer_relationship_id, C.division_id AS customer_division_id, C.customer_address, CT.type_name','', " $customerTypeCondition $classCondition  $divisionCondition $employeeCondition  $stateCondition $cityCondition $talukaCondition AND O.date_of_order >= '".date('Y-m-d', strtotime(mysql_escape_string($from_date)))."' and O.date_of_order <= '".date('Y-m-d', strtotime(mysql_escape_string($to_date)))."'  GROUP BY CASE WHEN (O.distributor_id>0 AND O.retailer_id = 0) THEN O.distributor_id WHEN (O.retailer_id>0) THEN O.retailer_id WHEN (O.customer_id>0) THEN O.customer_id END ORDER BY totol_visit DESC" );



		// Get the division master array primary key as index key AJAY@2016-07-20
		$divisionIDArr = array();
		foreach ($divisionRec as $key => $value) {
		 	# code...
		 	$divisionIDArr[$value->division_id]['division_name'] = $value->division;
		 } 
		 //print_r($divisionIDArr);


		 // Get the class master array primary key as index key AJAY@2016-07-20
		 $classificationIDArr = array();
		 $classification = $_objAdmin->_getSelectList('table_relationship',"relationship_id, relationship_code",'','');
		 foreach ($classification as $key => $value) {
		 	$classificationIDArr[$value->relationship_id]['relationship_code'] = $value->relationship_code;
		 	# code...
		 }
		// print_r($classificationIDArr);












	// echo "<pre>";
 //    print_r($custData);
    

// } else if( !isset($_SESSION['findCust']) || $_SESSION['findCust'] == 'all' || $_SESSION['findCust'] == 'R' ) {

// 	//echo $_SESSION['findCust'];
// 	// $tdr1=$_objAdmin->_getSelectList('table_order as O left join table_retailer as R on O.retailer_id=R.retailer_id left join table_salesman as S on O.salesman_id=S.salesman_id left join table_distributors as d on o.distributor_id=d.distributor_id','count(*) as total_visit','',"o.date_of_order >= '".date('Y-m-d', strtotime(mysql_escape_string($duration_start_date)))."' and o.date_of_order <= '".date('Y-m-d', strtotime(mysql_escape_string($duration_start_date)))."' and r.new='' $salesman $orderby"); 

// 	$retailerData = $_objAdmin->_getSelectList("table_order AS O
// 	LEFT JOIN table_retailer as R on O.retailer_id=R.retailer_id
//     LEFT JOIN table_salesman AS s ON s.salesman_id = O.salesman_id       
//     LEFT JOIN table_relationship as rr on rr.relationship_id=R.relationship_id
//     LEFT JOIN table_division AS dV ON dV.division_id = R.division_id       
//     LEFT JOIN state as ST on ST.state_id=R.state 
//     LEFT JOIN city as CT on CT.city_id=R.city 
//     LEFT JOIN table_taluka as TL on TL.taluka_id=R.taluka_id",
//     'count(O.retailer_id)  as total_visit,SUM(CASE WHEN O.order_type="Yes"  THEN 1 ELSE 0 END) AS total_order,R.retailer_name as customer_name,dV.division_name as division_name',''," R.new=''  $classCondition  $divisionCondition $employeeCondition  $stateCondition $cityCondition $talukaCondition AND O.date_of_order >= '".date('Y-m-d', strtotime(mysql_escape_string($from_date)))."' and O.date_of_order <= '".date('Y-m-d', strtotime(mysql_escape_string($to_date)))."'   group by O.salesman_id order by total_visit DESC,total_order DESC  " );
//     //print_r($retailerData);
// 	//echo $retailerData->status;
    

// }
// 	elseif( !isset($_SESSION['findCust']) || $_SESSION['findCust'] == 'all' || $_SESSION['findCust'] == 'D' ) 
// 	{
// 		//echo $_SESSION['findCust'];
// 		$distributorData = $_objAdmin->_getSelectList("table_order AS O
//     LEFT JOIN table_distributors as D on O.distributor_id=D.distributor_id 
//     LEFT JOIN table_salesman AS s ON s.salesman_id = O.salesman_id 
//     LEFT JOIN table_relationship as rr on rr.relationship_id=D.relationship_id
//     LEFT JOIN table_division AS dV ON dV.division_id = D.division_id 
//     LEFT JOIN state as ST on ST.state_id=D.state 
//     LEFT JOIN city as CT on CT.city_id=D.city 
//     LEFT JOIN table_taluka as TL on TL.taluka_id=D.taluka_id",
//     'COUNT(O.distributor_id) as total_visit,SUM(CASE WHEN O.order_type="Yes"  THEN 1 ELSE 0 END) AS total_order,D.distributor_name as customer_name,dV.division_name as division_name',''," D.new=''  $classCondition  $divisionCondition $employeeCondition  $stateCondition $cityCondition $talukaCondition  AND O.date_of_order >= '".date('Y-m-d', strtotime(mysql_escape_string($from_date)))."' and O.date_of_order <= '".date('Y-m-d', strtotime(mysql_escape_string($to_date)))."'  group by O.salesman_id order by total_visit DESC,total_order DESC" );
//         // print_r($cusData);
// 		//echo $distributorData->status;
// 	}
// elseif( !isset($_SESSION['findCust']) || $_SESSION['findCust'] == 'all' || !in_array($_SESSION['findCust'], array('R','D')) ) {
// $typeCondition = "";
// //echo $_SESSION['findCust'];
// if( isset($_SESSION['findCust']) AND !in_array($_SESSION['findCust'], array('R','D', 'all'))) {
//   $typeCondition = " AND C.customer_type='".$_SESSION['findCust']."'";  
// }
// 	$customerData = $_objAdmin->_getSelectList("table_order AS O
// 	LEFT JOIN table_customer as C on O.customer_id=C.customer_id 
//     LEFT JOIN table_salesman AS s ON O.salesman_id = s.salesman_id 
//     LEFT JOIN table_division AS dV ON dV.division_id = C.division_id 
//     LEFT JOIN state as ST on ST.state_id= C.state 
//     LEFT JOIN city as CT on CT.city_id=C.city 
//     LEFT JOIN table_taluka as TL on TL.taluka_id=C.taluka_id",
//     'COUNT(C.customer_id) as total_visit,SUM(CASE WHEN O.order_type="Yes"  THEN 1 ELSE 0 END) AS total_order,C.customer_name as customer_name,dV.division_name as division_name',''," C.new=''  $classCondition  $divisionCondition $employeeCondition  $stateCondition $cityCondition $talukaCondition AND O.date_of_order >= '".date('Y-m-d', strtotime(mysql_escape_string($from_date)))."' and O.date_of_order <= '".date('Y-m-d', strtotime(mysql_escape_string($to_date)))."'  group by O.salesman_id order by total_visit DESC,total_order DESC " );
//    //print_r($customerData);
// }
// else{ $customerData  = array();}


// if(sizeof($retailerData)>0 && sizeof($distributorData)>0 && sizeof($customerData)>0) {
//     $custData = array_merge($retailerData, $distributorData, $customerData);
// } else if(sizeof($retailerData)> 0 && sizeof($distributorData)>0 && sizeof($customerData)==0) {
//     $custData = array_merge($retailerData, $distributorData);
// } else if(sizeof($retailerData)> 0 && sizeof($distributorData)==0 && sizeof($customerData)>0) {
//     $custData = array_merge($retailerData, $customerData);
// } else if(sizeof($retailerData)== 0 && sizeof($distributorData)>0 && sizeof($customerData)>0) {
//     $custData = array_merge($distributorData, $customerData);
// } else if(sizeof($retailerData)> 0 && sizeof($distributorData)==0 && sizeof($customerData)==0) {
//     $custData = $retailerData;
// } else if(sizeof($retailerData)== 0 && sizeof($distributorData)>0 && sizeof($customerData)==0) {
//     $custData = $distributorData;
// } else if(sizeof($retailerData)== 0 && sizeof($distributorData)==0 && sizeof($customerData)>0) {
//     $custData = $customerData;
// }

?>

<?php include("header.inc.php") ?>
<script type="text/javascript">

   /* function PrintElem(elem)
    {
        Popup($(elem).html());
    }
*/
  /*  function Popup(data) 
    {
		
        var mywindow = window.open('', 'Report');
		
        mywindow.document.write('<html><head><title>Salesman Survey Report</title>');
		mywindow.document.write('<table><tr><td><b>Salesman Name:</b> <?php echo $sal_name; ?></td><td><b>City Name:</b> <?php echo $city_name; ?></td><td><b>From Date:</b> <?php echo $from_date; ?></td><td><b>To Date:</b> <?php echo $to_date; ?></td></tr></table>');*/
        /*optional stylesheet*/ //mywindow.document.write('<link rel="stylesheet" href="main.css" type="text/css" />');
       /* mywindow.document.write('</head><body >');
        mywindow.document.write(data);
        mywindow.document.write('</body></html>');

        mywindow.print();
        mywindow.close();
        return true;
    }
*/
/*$(document).ready(function()
{
<?php if($_POST['submit']=='Export to Excel'){ ?>
tableToExcel('report_export', 'order collection report', 'order collection report.xls');
<?php } ?>
});
*/
</script>
<script src="javascripts/dateNextPrev.js" type="text/javascript"></script>
<!-- start content-outer -->
<input name="pagename" type="hidden"  id="pagename" value="order collection report.php" />
<div id="content-outer">
<!-- start content -->
<div id="content">
<div id="page-heading"><h1><span style="color: #d74343; font-family: Tahoma; font-weight: bold;">Order Collection Report</span></h1></div>
<table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">
<tr>
	<td>
	<!--  start content-table-inner -->
	<div id="content-table-inner">
	
	<div id="page-heading" align="left" >
	<form name="frmPre" id="frmPre" method="post" action="" enctype="multipart/form-data" >
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
	<tr>
		<td>
			  <h3>Division: </h3>
                        <h6>
                            <select name="division" id="division" class="menulist">
                                <option value="all"<?php echo ($_SESSION['findDivision'] =="all")?'selected':''?>>All</option>
                                <?php 
                                    foreach ($divisionRec as $key => $value) {?>
                                        <option value="<?php echo $value->division_id?>" <?php echo ($_SESSION['findDivision'] ==$value->division_id)?'selected':''?>><?php echo $value->division ?></option>
                                    <?php }
                                ?>
                            </select>
                        </h6>
		</td>
		<td>
			<h3>Customer Type: </h3>
			<h6>
				 <select name="customer" id="customer" class="menulist">
                        <option value="all" <?php echo ($_SESSION['findCust'] =="all")?'selected':''?>>All</option>
                        <?php 
                        $customer_type = $_objAdmin->_getSelectList('`table_customer_type`','`cus_type_id`, `type_code`, `type_name`',''," `status` = 'A'"); 
                        if(is_array($customer_type)){
                        for($i=0;$i<count($customer_type);$i++){?>
                        <option value="<?php echo $customer_type[$i]->type_code;?>" <?php if($_SESSION['findCust']==$customer_type[$i]->type_code){ echo 'selected';} ?> ><?php echo $customer_type[$i]->type_name;?></option>
                        <?php }} ?>
                        </select>
			</h6>
		</td>
		<td>
			<h3>Customer Class: </h3>
			<h6>
				<h6>
                            <select name="customerclass" id="customerclass" class="menulist">
                             <option value="all">All</option>
                               
                            </select>
                        </h6>
			</h6>
		</td>
		<!-- <td>
			<h3>Employee Name: </h3>
			<h6>
				<select name="sal" id="sal" class="menulist">
					<?php //echo $salesmanOptionList = $_objArrayList->GetSalesmenMenu($salsList, $sals_id,'flex');?>
				</select>
			</h6>
		</td> -->
		
		
		<td><h3>&nbsp;&emsp;&nbsp;From Date: </h3><h6><img src="css/images/prev.png" height="18" width="18" onclick="dateFromPrev();"> <input type="text" id="from" name="from" class="date" style="width:100px" value="<?php  echo $from_date;?>"  readonly /> <img src="css/images/next.png" height="18" width="18" onclick="dateFromNext();"></h6></td>
		
		<td><h3>&nbsp;&emsp;&nbsp;To Date: </h3><h6><img src="css/images/prev.png" height="18" width="18" onclick="dateToPrev();"> <input type="text" id="to" name="to" class="date" style="width:100px" value="<?php echo $to_date; ?>"  readonly /> <img src="css/images/next.png" height="18" width="18" onclick="dateToNext();"></h6></td>
		
		<td colspan="2"></td>
		</tr>
		<tr>
		
		
		<td>
			<h3>State: </h3>
			<h6>
				<select name="state" id="state" class="menulist">
                                <option value="all" <?php echo ($_SESSION['stateCust'] =="all")?'selected':''?>>All</option>
                                <?php foreach ($stateRec as $key => $value) {?>
                                    <option value="<?php echo $value->state_id?>" <?php echo ($_SESSION['stateCust'] ==$value->state_id)?'selected':''?>><?php echo $value->state_name?></option>
                                <?php }?>
                            </select>
			</h6>
		</td>
		<td>
			<h3>District: </h3>
			<h6>
				 <select name="district" id="district" class="menulist">
                                <option value="all">All</option>
                            </select>
			</h6>
		</td>
		<td>
			<h3>Taluka: </h3>
			<h6>
				<select name="tehsil" id="tehsil" class="menulist">
                                <option value="all">All</option>
                            </select>
			</h6>
		</td>
		<td>
		
		<h3></h3>
		<input name="submit" class="result-submit" type="submit" id="submit" value="View Details" />
	    <input type="button" value="Reset!" class="form-reset" onclick="location.href='order_collection_report.php?reset=yes';" />
		</td>
		<td colspan="6"><input name="showReport" type="hidden" value="yes" />
		<a id="dlink"  style="display:none;"></a>
	<input type="button" name="submit" value="Export to Excel" class="result-submit" onclick="tableToExcel('report_export', 'Order Collection Report', 'order_collection_report.xls');" >


		<!--<input  type="submit" value="Export to Excel" name="submit" class="result-submit"  >
		<input type="button" value="Print" class="result-submit" onclick="PrintElem('#Report')" />
		 <a href='new_retailer_report_year_graph.php?y=<?php echo checkFromdate($from_date)."&salID=".$_POST['sal']."&city=".$_POST['city']; ?>' target="_blank"><input type="button" value=" Show Graph" class="result-submit" /></a> -->
		</td>
	</tr>
	</table>
	</form>
	</div>
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
	<?php
	if($sal_id!=""){ 
	?>
	<tr valign="top">
		<td>
		<table border="0" width="100%" cellpadding="0" cellspacing="0" id="id-form">
			<tr bgcolor="#A52A2A" style="color: #fff;font-weight: bold;">
				<td align="center" style="padding:10px;">Select Salesman</td>
			</tr>
		</table>
		</td>
		<?php } else {  ?>
		<td>
		<div id="Report" style="width:1100px; overflow:scroll">
		<table  border="0" width="100%" cellpadding="0" cellspacing="0" id="report_export" name="report_export" >
			<tr bgcolor="#6E6E6E" style="color: #fff;font-weight: bold;" >
				
				<td style="padding:10px;" width="25%">Customer Name</td>	
				<td style="padding:10px;" width="25%">Customer Address</td>			
				<td style="padding:10px;" width="10%">Customer Type</td>
				<td style="padding:10px;" width="15%">Customer Division</td>
				<td style="padding:10px;" width="15%">Customer Class</td>
				<td style="padding:10px;" width="10%">Visit Count</td>
				<!-- <td style="padding:10px;" width="15%">No. of Order Taken</td> -->
				
			</tr>
			<?php
			//$no_ret = array();
			//print_r($cusData);
			//$cusData = $_objAdmin->_getSelectList('table_survey as ts left join table_salesman as s on ts.salesman_id=s.salesman_id left join table_retailer as tr on ts.retailer_id=tr.retailer_id left join table_distributors as td on ts.distributor_id=td.distributor_id', 'ts.*,s.salesman_name,s.salesman_code,tr.retailer_name,tr.retailer_code,td.distributor_name,td.distributor_code',''," $division $salesman and (ts.survey_date BETWEEN '".date('Y-m-d', strtotime($from_date))."' AND '".date('Y-m-d', strtotime($to_date))."') ORDER BY ts.survey_date ASC");
			if($from_date!="" && $to_date!="" && $custData!=""){

				foreach ($custData as $key => $value) {
					# code...
				
					
						//$dateCode=explode('-', $cusData[$i]->start_date);
						//$no_ret[] = $cusData[$i]->retailer_id;

						// $value->totol_visit;
						// echo "<br>";
						 $name = "-";
						 $type = "-";
						 $division_id = 0;
						 $relationship_id = 0;
						 $division_name = "-";
						 $relationship_code = "-";
						 $address = "-";

						 if($value->retailer_name!="") {
						 	$name = $value->retailer_name;
						 	$division_id = $value->retailer_division_id;
						 	$relationship_id = $value->retailer_relationship_id;
						 	$address = $value->retailer_address;
						 	$type = "Retailer";
						 } else if($value->distributor_name!="") {
						 	$name = $value->distributor_name;
						 	$division_id = $value->distributor_division_id;
						 	$relationship_id = $value->distributor_relationship_id;
						 	$address = $value->distributor_address;
						 	$type = "Distributor";
						 } else {
						 	$name = $value->customer_name;
						 	$division_id = $value->customer_division_id;
						 	$relationship_id = $value->customer_relationship_id;
						 	$address = $value->customer_address;
						 	$type = "Customer";
						 	$type = $value->type_name;
						 }

						 if(isset($divisionIDArr[$division_id]['division_name'])) {
						 	$division_name = $divisionIDArr[$division_id]['division_name'];
						 }

						 if(isset($classificationIDArr[$relationship_id]['relationship_code'])) {
						 	$relationship_code = $classificationIDArr[$relationship_id]['relationship_code'];
						 }						 

						 if($name!="" && $value->total_order==0) {?>
							<tr  style="border-bottom:2px solid #6E6E6E;" >
							<td style="padding:10px;" width="25%"><?php echo $name;?> </td>
							<td style="padding:10px;" width="25%"><?php echo $address;?> </td>
							<td style="padding:10px;" width="10%"><?php echo $type;?> </td>
							<td style="padding:10px;" width="15%"><?php echo $division_name;?> </td>
							<td style="padding:10px;" width="15%"><?php echo $relationship_code;?> </td>
							<td style="padding:10px;" width="10%"><?php echo $value->totol_visit;?> </td>			
							<!-- <td style="padding:10px;" width="20%"><?php echo $value->total_order;?> </td> -->	
							</tr>
						<?php }?>
					<?php } ?>
				
			<?php
			} else {
			?>
			<tr  style="border-bottom:2px solid #6E6E6E;" align="center" >
				<td style="padding:10px; width:20%"  colspan="5">Report Not Available</td>
			</tr>
			<?php
			}
			?>
		</table>
		</div>
		</td>
	<?php } ?>
	</tr>
	<tr>
		<td><img src="images/blank.gif" width="695" height="1" alt="blank" /></td>
		<td></td>
	</tr>
	</table>
<div class="clear"></div>
</div>
<!--  end content-table-inner  -->
</td>
<td id="tbl-border-right"></td>
</tr>
<!--<tr>
	<th class="sized bottomleft"></th>
	<td id="tbl-border-bottom">&nbsp;</td>
	<th class="sized bottomright"></th>
</tr>-->
</table>
<div class="clear">&nbsp;</div>
</div>
<!--  end content -->
<div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->
<div class="clear">&nbsp;</div> 
<!-- start footer -->         
<?php include("footer.php") ?>
<?php unset($_SESSION['SalAttList']); ?>
<!-- end footer -->
<script src="javascripts/jquery-1.8.2.js" type="text/javascript"></script>
<script src="javascripts/jquery-ui.js"></script>
<script type="text/javascript" src="javascripts/validate.js"></script>
<script type="text/javascript">
// Popup window code
function newPopup(url) {
	popupWindow = window.open(
		url,'popUpWindow','height=500,width=600,left=200,top=100,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no,status=yes')
}
</script>
<script>
    $(function() {
        $( "#from" ).datepicker({
			dateFormat: "d M yy",
            defaultDate: "w",
            changeMonth: true,
            numberOfMonths: 1,
            onSelect: function( selectedDate ) {
                $( "#to" ).datepicker( "option", "minDate", selectedDate );
            }
        });
        $( "#to" ).datepicker({
			dateFormat: "d M yy",
            defaultDate: "-w",
            changeMonth: true,
            numberOfMonths: 1,
            onSelect: function( selectedDate ) {
                $( "#from" ).datepicker( "option", "maxDate", selectedDate );
            }
        });
    });
</script>


<script type="text/javascript">
$(function(){
    if($("#state").val()!="all"){
        state($("#state").val());
        var dist="<?php echo $_SESSION['districtCust'];?>";
        if(dist!="all"){
            district(dist);
        }
    }
    if($("#customer").val()!="all"){
        customerClass($("#customer").val());
    }
    $("#customer").change(function(){
        var customer = $(this).val();
        customerClass(customer);
    });
    $("#state").change(function(){
        var stateID = $(this).val();
        state(stateID);
    });
    $("#district").change(function(){
        var cityID = $(this).val();
        district(cityID);
    });
});

function state(stateID){
    if(stateID!='all'){
        $.ajax({
            method:"GET",
            url: "ajax_customer_geo_tag.php",
            data: { stateID: stateID },
            success: function(result){
                $("#district").html(result);
            }
        });
    }else{
        $("#district").html("<option value='all'>All</option>");
    }
}
function district(cityID){
    if(cityID!='all'){
        $.ajax({
            method:"GET",
            url: "ajax_customer_geo_tag.php",
            data: { cityID: cityID },
            success: function(result){
                $("#tehsil").html(result);
            }
        });
    } else {
        $("#tehsil").html("<option value='all'>All</option>");
    }
}

function customerClass(customer){
    if(customer!='all'){
        $.ajax({
            method:"GET",
            url: "ajax_customer_geo_tag.php",
            data: { customer: customer },
            success: function(result){
                $("#customerclass").html(result);
            }
        });
    } else {
        $("#customerclass").html("<option value='all'>All</option>");
    }
}
</script>



<script type='text/javascript'>//<![CDATA[ 
var tableToExcel = (function () {
        var uri = 'data:application/vnd.ms-excel;base64,'
        , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table><tr><td><b>Salesman Name:</b> <?php echo $sal_name; ?></td><td><b>From Date:</b> <?php echo $_objAdmin->_changeDate($from_date); ?></td><td><b>To Date:</b> <?php echo $_objAdmin->_changeDate($to_date); ?></td></tr></table><table>{table}</table></body></html>'
        , base64 = function (s) { return window.btoa(unescape(encodeURIComponent(s))) }
        , format = function (s, c) { return s.replace(/{(\w+)}/g, function (m, p) { return c[p]; }) }
        return function (table, name, filename) {
            if (!table.nodeType) table = document.getElementById(table)
            var ctx = { worksheet: name || 'Worksheet', table: table.innerHTML }

            document.getElementById("dlink").href = uri + base64(format(template, ctx));
            document.getElementById("dlink").download = filename;
            document.getElementById("dlink").click();

        }
    })()

//]]>  
</script>
</body>
</html>
