<?php
include("includes/config.inc.php");
include("includes/function.php");
$_objAdmin = new Admin();
$day= date("D");
if(isset($_POST['temporder_add']) && $_POST['temporder_add']=='yes'){	
$_objAdmin->addTempOrder();
}
if(isset($_POST['order_add']) && $_POST['order_add']=='yes'){	
$ordID=$_objAdmin->addOrder();
	if($ordID!=''){
	$_objAdmin->addOrderDetail($ordID);
	$_objAdmin->mysql_query("delete from table_temporder where salesman_id=".$_SESSION['salesmanId']." and guid='".$_SESSION['Guid']."' and account_id=".$_SESSION['accountId']);
	header("Location: today_retailer_list.php");
	}
}

if(isset($_REQUEST['deleteid']) && $_REQUEST['deleteid']!="" ){
$_objAdmin->mysql_query("delete from table_temporder where temp_id=".base64_decode($_REQUEST['deleteid']));
//$auTempEdit=$_objAdmin->_getSelectList('table_temporder',"*",''," temp_id=".$_REQUEST['editid']);
}
$auRet=$_objAdmin->_getSelectList('table_retailer',"*",''," retailer_id=".$_SESSION['retailerList']." and account_id=".$_SESSION['accountId']);
if(!is_array($auRet)){
header("Location: today_retailer_list.php");
}
$auDis=$_objAdmin->_getSelectList('table_salesman',"*",''," salesman_id=".$_SESSION['salesmanId']." and account_id=".$_SESSION['accountId']);
if(!is_array($auDis)){
header("Location: today_retailer_list.php");
}
?>
<?php include("header.inc.php") ?>
<style>
		.black_overlay{
			display: none;
			position: absolute;
			top: 0%;
			left: 0%;
			width: 100%;
			height: 100%;
			
			z-index:1001;
			-moz-opacity: 0.8;
			opacity:.80;
			filter: alpha(opacity=80);
		}
		.white_content {
			display: none;
			position: absolute;
			top: 25%;
			left: 25%;
			width: 35%;
			height: 35%;
			padding: 16px;
			border: 16px solid #6E6E6E;
			background-color: white;
			z-index:1002;
			overflow: auto;
		}
</style>
<!-- start content-outer -->
<div id="content-outer">
<!-- start content -->
<div id="content">
<div id="page-heading"><h1><span style="color: #d74343; font-family: Tahoma; font-weight: bold;">Order Form</span></h1></div>
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" id="content-table">
<!--<tr>
	<th rowspan="3" class="sized"><img src="images/side_shadowleft.jpg" width="20" height="300" alt="" /></th>
	<th class="topleft"></th>
	<td id="tbl-border-top">&nbsp;</td>
	<th class="topright"></th>
	<th rowspan="3" class="sized"><img src="images/side_shadowright.jpg" width="20" height="300" alt="" /></th>
</tr>
-->
<tr>
	<!--<td id="tbl-border-left"></td>-->
	<td align="center" valign="middle">
	<!--  start content-table-inner -->
	<div id="content-table-inner">
	
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
	<tr valign="top">
	<td>
		<!-- start id-form -->
		
		<table border="0" width="100%" cellpadding="0" cellspacing="0" id="id-form">
			<tr>
				<td><b>Retailer Name:</b> <?php echo $auRet[0]->retailer_name;?></td>
				<td align="right" style="padding-right: 50px;"><b>Date:</b> <?php echo date("d-M-Y", strtotime($date)); ?></td>
			</tr>
			<tr>
				<td><b>Retailer Location:</b> <?php echo $auRet[0]->retailer_location;?></td>
				<td align="right" style="padding-right: 50px;"><b>Time:</b> <?php echo $time; ?></td>
			</tr>
			<tr>
				<td><b>Retailer Address:</b> <?php echo $auRet[0]->retailer_address;?></td>
				<td></td>
			</tr>
		
		</table>
	<tr valign="top">
	<td>
		<form name="frmPre" id="frmPre" method="post" action="order_form.php" enctype="multipart/form-data" >
		<table border="0" width="100%" cellpadding="0" cellspacing="0" id="id-form">
			<tr bgcolor="#6E6E6E" style="color: #fff;font-weight: bold;">
				<td style="padding:10px;">S.No</td>
				<td style="padding:10px;">Action</td>
				<td style="padding:10px;">Particulars</td>
				<td style="padding:10px;">Quantity</td>
				<td align="right" style="padding-right: 50px;">Price</td>
				<td align="right" style="padding-right: 50px;">Total</td>
			</tr>
			<?php
			$auTemp=$_objAdmin->_getSelectList('table_temporder as t left join table_item as i on t.item_id=i.item_id left join table_price as p on t.price_id=p.price_id',"t.*,i.item_name,p.item_price",''," t.salesman_id=".$_SESSION['salesmanId']." and t.guid='".$_SESSION['Guid']."' and t.account_id=".$_SESSION['accountId']);
			if(is_array($auTemp)){
				for($i=0;$i<count($auTemp);$i++){
			?>
			<tr style="border-bottom:2px solid #6E6E6E;" >
				<td style="padding:10px;"><?php echo $i+1; ?></td>
				<td style="padding:10px;"><a href="order_form.php?deleteid=<?php echo base64_encode($auTemp[$i]->temp_id);?>">Delete<a></td>
				<td style="padding:10px;"><?php echo $auTemp[$i]->item_name;?><input name="item_id[]" type="hidden" value="<?php echo $auTemp[$i]->item_id; ?>" /></td>
				<td style="padding:10px;"><?php echo $auTemp[$i]->quantity;?><input name="quantity[]" type="hidden" value="<?php echo $auTemp[$i]->quantity; ?>" /></td>
				<td align="right" style="padding-right: 50px;padding-top: 10px;"><?php echo number_format($auTemp[$i]->item_price);?><input name="price_id[]" type="hidden" value="<?php echo $auTemp[$i]->price_id; ?>" /></td>
				<td align="right" style="padding-right: 50px;padding-top: 10px;"><?php echo number_format($auTemp[$i]->total);?><input name="total[]" type="hidden" value="<?php echo $auTemp[$i]->total;?>" /></td>
			</tr>
			<?php } } else {?>
			<tr style="border-bottom:2px solid #6E6E6E;" >
				<td style="padding:10px;">1</td>
				<td style="padding:10px;">-</td>
				<td style="padding:10px;">-</td>
				<td style="padding:10px;">-</td>
				<td style="padding-right: 50px;padding-top: 10px;">-</td>
				<td style="padding-right: 50px;padding-top: 10px;">-</td>
			</tr>
			<?php } ?>
			<tr style="border-bottom:2px solid #6E6E6E;" >
				<td style="padding:10px;"></td>
				<td style="padding:10px;"></td>
				<td style="padding:10px;"></td>
				<td style="padding:10px;"></td>
				<td align="right" style="padding-right: 50px;padding-top: 10px;">
				<b>Net Total</b></td>
				<?php
				$auTotal=$_objAdmin->_getSelectList('table_temporder ',"SUM(total) as total",''," salesman_id=".$_SESSION['salesmanId']." and guid='".$_SESSION['Guid']."' and account_id=".$_SESSION['accountId']);
				if(is_array($auTotal)){
				?>
				<td align="right" style="padding-right: 50px;padding-top: 10px;"><b><?php echo number_format($auTotal[0]->total); ?></b></td>
				<?php } else { ?>
				<td align="right" style="padding-right: 50px;"><b>-</b></td>
				<?php } ?>
			</tr>
			<tr >
				<td style="padding:10px;"><input type="button" value="Back" class="form-reset" onclick="location.href='today_retailer_list.php';" /></td>
				<td style="padding:10px;"></td>
				<td style="padding:10px;"><a href = "javascript:void(0)" onclick = "document.getElementById('light').style.display='block';document.getElementById('fade').style.display='block'">
				<div id="order-act-top">
				Add Order Items</td>
				<td style="padding:10px;"></td>
				<td style="padding:10px;"></td>
				<td align="right" style="padding-right: 0px;padding-top: 10px;"><input name="submit" class="form-submit" type="submit" id="submit" value="Save Order" /></td>
				<input name="order_add" type="hidden" value="yes" />
			<input name="account_id" type="hidden" value="<?php echo $_SESSION['accountId']; ?>" />
			<input name="sal_id" type="hidden" value="<?php echo $_SESSION['salesmanId']; ?>" />
			<input name="ret_id" type="hidden" value="<?php echo $auRet[0]->retailer_id;?>" />
			<input name="dis_id" type="hidden" value="<?php echo $auDis[0]->distributor_id;?>" />
			<input name="date" type="hidden" value="<?php echo $date; ?>" />
			<input name="time" type="hidden" value="<?php echo date("h:i A."); ?>" />
			<input name="guid" type="hidden" value="<?php echo $_SESSION['Guid']; ?>" />
			</tr>
			
		</table>
		</form>
		<div id="light" class="white_content">
		<form name="frmPre1" id="frmPre1" method="post" action="order_form.php" enctype="multipart/form-data" >
		<table border="0" cellpadding="0" cellspacing="0"  id="id-form">
		<tr>
			<th valign="top">Select Category:</th>
			<td>
			<select name="category" id="category" class="styledselect_form_3" onClick="showstage_items(this.value)" >
			<?php $auCat=$_objAdmin->_getSelectList('table_category',"*",''," status='A' and account_id=".$_SESSION['accountId']);
			for($i=0;$i<count($auCat);$i++){?>
			<option value="<?php echo $auCat[$i]->category_id;?>" ><?php echo $auCat[$i]->category_name;?></option>
			<?php }?>
			</select>
			</td>
		</tr>
		<tr>
			<th valign="top">Select Item:</th>
			<td>
			<select name="item" id="item" class="styledselect_form_3 required" onclick="showstage_price(this.value)">
				<option value="" selected="selected" >- - Select - -</option>
				</select>
			</td>
		</tr>
		<tr>
			<th valign="top">Price:</th>
			<td><select name="price" id="price" class="styledselect_form_3 required">
				<option value="" selected="selected" ></option>
				</select>
			<!--<input type="text" name="price" id="price" class="text"   value=""/> --></td>
			<td></td>
		</tr>
		<tr>
			<th valign="top">Quantity:</th>
			<td><input type="text" name="quantity" id="quantity" class="required number" maxlength="3"  value="<?php echo $auTempEdit[0]->quantity;?>"/></td>
			<td></td>
		</tr>
		<tr>
		<th>&nbsp;</th>
		<td valign="top">
			<input name="temporder_add" type="hidden" value="yes" />
			<input name="sal_id" type="hidden" value="<?php echo $_SESSION['salesmanId']; ?>" />
			<input name="account_id" type="hidden" value="<?php echo $_SESSION['accountId']; ?>" />
			<input name="guid" type="hidden" value="<?php echo $_SESSION['Guid']; ?>" />
			<input type="reset" value="Reset!" class="form-reset">
			<input type="button" value="Back" class="form-reset" onclick="document.getElementById('light').style.display='none';document.getElementById('fade').style.display='none'" />
			<input name="submit" class="form-submit" type="submit" id="submit" value="Add" />
		</td>
		</tr>
		</table>
	</form>
		
		
		</div>
		<div id="fade" class="black_overlay"></div>
	</td>	
	<!-- end id-form  -->
	</tr>
	</td>

</tr>
<tr> 
 <td></td>
</tr>

<tr>
<td><img src="images/shared/blank.gif" width="695" height="1" alt="blank" /></td>
<td></td>
</tr>
</table>
<div class="clear"></div>
</div>
<!--  end content-table-inner  -->
</td>
<td id="tbl-border-right"></td>
</tr>
<!--<tr>
	<th class="sized bottomleft"></th>
	<td id="tbl-border-bottom">&nbsp;</td>
	<th class="sized bottomright"></th>
</tr>-->
</table>
<div class="clear">&nbsp;</div>
</div>
<!--  end content -->
<div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->
<div class="clear">&nbsp;</div> 
<!-- start footer -->         
<?php include("footer.php") ?>
<!-- end footer -->
 
</body>
</html>