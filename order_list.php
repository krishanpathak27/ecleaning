<?php
include("includes/config.inc.php");
include("includes/function.php");
$_objAdmin = new Admin();
$_objModuleClass = new ModuleClass();

$feature_array = $_objModuleClass->getFeatureModulel();
if(in_array(21,$feature_array))
{
	$flag=0;
}
else
{
	$flag=1;

}
$page_name="Order Details List";
$day= date("D");
if($_REQUEST['act_id']!='' && ($_REQUEST['act_type']==28) || ($_REQUEST['act_type']==29)){

	$auOrd=$_objAdmin->_getSelectList2('table_activity as a left join table_retailer as r on a.ref_id=r.retailer_id left join table_distributors as d on d.distributor_id=r.distributor_id left join table_salesman as s on s.salesman_id=a.salesman_id left join table_markets as m on m.market_id=r.market_id',"a.activity_date as date_of_order,a.start_time as time_of_order,a.lat,a.lng,a.accuracy_level,a.network_mode as location_provider,r.retailer_id,r.retailer_name,r.retailer_address,r.retailer_location,r.retailer_phone_no,d.distributor_name,s.salesman_name,m.market_name",''," a.ref_id=".base64_decode($_REQUEST['act_id'])." and a.activity_date ='".date('Y-m-d', strtotime($_SESSION['SalReportDate']))."' and a.activity_type=".$_REQUEST['act_type']);


	/*SELECT *
FROM table_activity
WHERE salesman_id =66
AND activity_date = '2016-10-24'
AND activity_type 
IN ( 28,29 )
ORDER BY start_time ASC */



	}

else if($_REQUEST['act_id']!=''){
$auOrd=$_objAdmin->_getSelectList('table_order as o left join table_retailer as r on o.retailer_id=r.retailer_id left join table_distributors as d on d.distributor_id=o.distributor_id left join table_salesman as s on s.salesman_id=o.salesman_id left join table_tags as t on t.tag_id=o.tag_id  left join table_markets as m on m.market_id=r.market_id',"o.*,r.retailer_name,r.retailer_address,r.retailer_location,r.retailer_phone_no,d.distributor_id,d.distributor_name,t.tag_description,s.salesman_name,m.market_name",''," o.order_id=".base64_decode($_REQUEST['act_id'])." and o.account_id=".$_SESSION['accountId']);

} else {
$auOrd=$_objAdmin->_getSelectList('table_order as o left join table_retailer as r on o.retailer_id=r.retailer_id left join table_distributors as d on d.distributor_id=o.distributor_id left join table_salesman as s on s.salesman_id=o.salesman_id left join table_tags as t on t.tag_id=o.tag_id',"o.*,r.retailer_name,r.retailer_address,r.retailer_location,r.retailer_phone_no,d.distributor_name,t.tag_description,s.salesman_name",''," o.order_id=".$_SESSION['orderdetails']." and o.account_id=".$_SESSION['accountId']);
}



$sal_name=$auOrd[0]->salesman_name;
$sal_lat=$auOrd[0]->lat;
$sal_lng=$auOrd[0]->lng;
$sal_accuracy_level=$auOrd[0]->accuracy_level;
$sal_network_mode=$auOrd[0]->location_provider;


// $auMarker=$_objAdmin->_getSelectList2('table_survey ',"lat as rlat,lng as rlng,accuracy_level,network_mode",''," retailer_id='".$auOrd[0]->retailer_id."' order by survey_date desc,survey_time desc limit 0,1");

$uType = "";

if($_REQUEST['act_id']!='' && ($_REQUEST['act_type']==28) || ($_REQUEST['act_type']==29)){

if($auOrd[0]->retailer_id == 0 || $auOrd[0]->retailer_id== "") {
	$uType = "Distributor ";
	$ret_name=$auOrd[0]->distributor_name;
	$auMarker=$_objAdmin->_getSelectList2('table_distributors'," lat as rlat,lng as rlng",''," distributor_id='".$auOrd[0]->distributor_id."' order by start_date");

} else {
	$uType = "Dealer ";
	$ret_name=$auOrd[0]->retailer_name;
	$auMarker=$_objAdmin->_getSelectList2('table_retailer ',"lat as rlat,lng as rlng",''," retailer_id='".$auOrd[0]->retailer_id."' order by start_date");
}

}
else
{
	if($auOrd[0]->retailer_id == 0 || $auOrd[0]->retailer_id== "") {
	$uType = "Distributor ";
	$ret_name=$auOrd[0]->distributor_name;
	$auMarker=$_objAdmin->_getSelectList2('table_survey'," lat as rlat,lng as rlng,accuracy_level,network_mode",''," distributor_id='".$auOrd[0]->distributor_id."' order by survey_date desc,survey_time desc limit 0,1");

} else {
	$uType = "Dealer ";
	$ret_name=$auOrd[0]->retailer_name;
	$auMarker=$_objAdmin->_getSelectList2('table_survey ',"lat as rlat,lng as rlng,accuracy_level,network_mode",''," retailer_id='".$auOrd[0]->retailer_id."' order by survey_date desc,survey_time desc limit 0,1");
}

}

$ret_lat ="";
$ret_lng ="";
$accuracy_level = "";
$network_mode ="";


if($auMarker[0]->rlat>0){
$ret_lat=$auMarker[0]->rlat;
$ret_lng=$auMarker[0]->rlng;
$accuracy_level=$auMarker[0]->accuracy_level;
$network_mode=$auMarker[0]->network_mode;
}

?>
<?php include("header.inc.php") ?>
<style type="text/css">
  html { height: 100% }
  body { height: 100%; margin: 0; padding: 0 }
  #map_canvas { height: 100% }
</style>
<?php if($sal_lat!=''){ ?>
<script type="text/javascript"
src="http://maps.googleapis.com/maps/api/js?sensor=false">
</script>
<script type="text/javascript">
      function initialize() {
        var myOptions = 
		{
		<?php if($ret_lat!='' && $ret_lat>0){ ?>
          center: new google.maps.LatLng(<?php echo $ret_lat; ?>, <?php echo $ret_lng; ?>), zoom: 17, mapTypeId: google.maps.MapTypeId.ROADMAP
		  <?php } else { ?>
		  center: new google.maps.LatLng(<?php echo $sal_lat; ?>, <?php echo $sal_lng; ?>), zoom: 17, mapTypeId: google.maps.MapTypeId.ROADMAP
		  <?php } ?>
        };
        var map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
		<?php if($ret_lat!='' && $ret_lat>0){ ?>
		var myLatlng = new google.maps.LatLng(<?php echo $ret_lat; ?>, <?php echo $ret_lng; ?>)
		var marker = new google.maps.Marker
		({ position: myLatlng,title:"<?php echo $uType;?> Name: <?php echo $ret_name; ?>, Location Type: <?php echo $network_mode; ?>, Accuracy: <?php echo $accuracy_level; ?>", animation: google.maps.Animation.DROP,});
		marker.setMap(map);
		<?php } ?>
		var myLatlng = new google.maps.LatLng(<?php echo $sal_lat; ?>, <?php echo $sal_lng; ?>)
		var marker = new google.maps.Marker
		({ position: myLatlng,title:"Salesman Name: <?php echo $sal_name; ?>, Location Type: <?php echo $sal_network_mode; ?>, Accuracy: <?php echo $sal_accuracy_level; ?>",icon: 'images/sal_marker.png', animation: google.maps.Animation.DROP,});
		marker.setMap(map);
		<?php if($ret_lat!='' && $ret_lat>0){ ?>
		var flightPlanCoordinates = [
			new google.maps.LatLng(<?php echo $ret_lat; ?>, <?php echo $ret_lng; ?>),
			new google.maps.LatLng(<?php echo $sal_lat; ?>, <?php echo $sal_lng; ?>)
		];
		var flightPath = new google.maps.Polyline({
			path: flightPlanCoordinates,
			strokeColor: "#FF0000",
			strokeOpacity: 1.0,
			strokeWeight: 2
		});
		var circleOptions1 = {
			center: new google.maps.LatLng(<?php echo $ret_lat; ?>, <?php echo $ret_lng; ?>),
			radius: 50,
			strokeWeight: 0,
			fillColor: "#BDBDBD",
			fillOpacity: 0.50,
			map: map,
			editable: false
			};
		var myLatlng1 = new google.maps.LatLng(<?php echo $ret_lat; ?>, <?php echo $ret_lng; ?>);	
		var circle = new google.maps.Circle(circleOptions1);
		flightPath.setMap(map);
		<?php } ?>
     }
</script>
<?php } ?>
<script type="text/javascript">
 $(document).keypress(function(e) {
    if(e.which == 96) {
       location.href='admin_order_list.php';
    }
});
</script>
<style>
div.img
  {
  margin:2px;
  border:1px solid  #43A643;
  height:auto;
  width:100%;
  float:left;
  text-align:center;
  }
div.img img
  {
  display:inline;
  margin:3px;
  border:1px solid #fbfcfc;
  }
div.img a:hover img
  {
  border:1px solid #fbfcfc;
  }
div.desc
  {
  text-align:center;
  font-weight:normal;
  width:120px;
  margin:2px;
  }
</style>
<!-- start content-outer -->
<body onLoad="initialize()">
<div id="content-outer">
<!-- start content -->
<div id="content">
<div id="page-heading"><h1><span style="color: #d74343; font-family: Tahoma; font-weight: bold;">Order Details List</span></h1></div>
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" id="content-table">
<!--<tr>
	<th rowspan="3" class="sized"><img src="images/side_shadowleft.jpg" width="20" height="300" alt="" /></th>
	<th class="topleft"></th>
	<td id="tbl-border-top">&nbsp;</td>
	<th class="topright"></th>
	<th rowspan="3" class="sized"><img src="images/side_shadowright.jpg" width="20" height="300" alt="" /></th>
</tr>
-->
<tr>
	<!--<td id="tbl-border-left"></td>-->
	<td align="center" valign="middle">
	<!--  start content-table-inner -->
	<div id="content-table-inner">
	
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
	<tr valign="top">
	<td>
		<!-- start id-form -->
		
		<table border="0" width="100%" cellpadding="0" cellspacing="0" id="id-form">
			<tr>
				<td><b>Salesman Name:</b> <?php echo $auOrd[0]->salesman_name;?></td>
				<!--<td align="right" style="padding-right: 50px;"><b>Order Number:</b> <?php //echo $auOrd[0]->order_id;?></td>-->
			</tr>
			<tr>
				<td><b>Distributor Name:</b> <?php echo $auOrd[0]->distributor_name;?></td>
				<td align="right" style="padding-right: 50px;"><b>Order Number:</b> <?php echo $auOrd[0]->order_id;?></td>
			</tr>
			<tr>
				<td><b>Dealer Name:</b> <?php echo $auOrd[0]->retailer_name;?></td>
				<td align="right" style="padding-right: 50px;"><b>Date:</b> <?php echo date("d-M-Y", strtotime($auOrd[0]->date_of_order)); ?></td>
			</tr>
			<tr>
				<td><b>Dealer Mobile-No:</b> <?php echo $auOrd[0]->retailer_phone_no;?></td>
				<td align="right" style="padding-right: 50px;"><b>Time:</b> <?php echo $auOrd[0]->time_of_order;?></td>
			</tr>
			
			<tr>
			
				<td><?php if($flag==1){ ?><b>Dealer Photo:</b> <a href="retailer_survey.php?retId=<?php echo $auOrd[0]->retailer_id;?>" target="_blank">View Photo</a><?php } ?></td>
				
				<td align="right" style="padding-right: 50px;"><?php if($auOrd[0]->bill_no !=''){ ?><b>Bill Date:</b> <?php echo $auOrd[0]->bill_date; }?></td>
			</tr>
			
			<!-- <tr>
				<td><b>Dealer Market:</b> <?php echo $auOrd[0]->market_name;?></td>
				<td align="right" style="padding-right: 50px;"><?php if($auOrd[0]->bill_no !=''){ ?><b>Bill No:</b> <?php echo $auOrd[0]->bill_no; }?></td>
			</tr> -->
			<tr>
				<td><b>Dealer Address:</b> <?php echo $auOrd[0]->retailer_address;?></td>
				<td align="center" style="padding-right: 500px;">
				<?php if($auOrd[0]->order_type=='Yes' || $auOrd[0]->order_type=='Adhoc'){ ?>
				<img src="images/accept.jpg" width="15" height="15" alt="" /> <b>Accepted</b> &nbsp;&nbsp;&nbsp;&nbsp;<img src="images/reject.jpg" width="15" height="15" alt="" /> <b>Rejected</b>
				<?php } else if($auOrd[0]->order_type=='No' ) { ?>
				<b>No Order</b>
				<?php } ?>
				</td>
			</tr>
		
		</table>
	</td>
	</tr>
	<tr valign="top">
	<td>
		<?php if($auOrd[0]->order_type=='No'){ ?>
		<table border="0" width="100%" cellpadding="0" cellspacing="0" id="id-form">
			<tr bgcolor="#6E6E6E" style="color: #fff;font-weight: bold;">
				<td style="padding:10px;" >Reason</td>
				<td style="padding:10px;" >Comments</td>
			</tr>
			<tr>
				<td style="padding:10px;" ><?php echo $auOrd[0]->tag_description;?></td>
				<td style="padding:10px;" ><?php echo $auOrd[0]->comments;?></td>
			</tr>
			<tr>
				<td colspan="6">
				<?php
				$auImg=$_objAdmin->_getSelectList('table_image as i',"i.*",''," image_type=2 and ref_id=".$auOrd[0]->order_id);
					if(is_array($auImg)){
						for($j=0;$j<count($auImg);$j++){
				?>
					
					<div class="img">
						<a href="photo/<?php echo $auImg[$j]->image_url;?>"  target="_blank"><img src="photo/<?php echo $auImg[$j]->image_url;?>" alt="" width="140" height="120"></a>
					</div>
					
				<?php } } else { ?>
				<div class="img">
						<div style="width: 100%;height:50px;"><span style="color: #A52A2A; font-family: Tahoma; font-weight: bold;">Image Not Available</span></div>
					</div>
				<?php } ?>
				</td>
			</tr>
			<?php if($ret_lat=='' || $ret_lat==0) { ?>
			<tr bgcolor="#A52A2A" style="color: #fff;font-weight: bold;" align="center">
			<!--<tr align="center" style="border-top:2px solid #6E6E6E;" >-->
				<td style="padding:10px;" colspan="10"> 
					<div style="width: 100%;height:15px;"><?php echo $uType;?> Location Not Available</div>
				</td>
			</td>
			<?php } ?>
			<tr align="center" >
				<td style="padding:10px;" colspan="10"> 
				<?php if($sal_lat!=''){ ?>
				<div id="map_canvas" style="width:100%; height:350px"></div>
				<?php } else { ?>
				<div bgcolor="#A52A2A" style="color: #fff;font-weight: bold;background-color:#A52A2A; height:15px; padding:10px;" align="center">Order Location Not Available</div>
				<?php } ?>
				</td>
			</tr>
			<tr align="center" >
				<?php if($_REQUEST['act_id']!=''){ ?>
				<td style="padding:10px;" colspan="8"><input type="button" value="Close" class="form-cen" onClick="javascript:window.close();" /></td>
				<?php } else { ?>
				<?php  if($_SESSION['userLoginType']==1 || $_SESSION['userLoginType']==2)  { ?>
				<td style="padding:10px;" colspan="8"><input type="button" value="Back" class="form-cen" onClick="location.href='admin_order_list.php?searchParam_1=<?php echo $_REQUEST['searchParam_1']; ?>&searchParam_2=<?php echo $_REQUEST['searchParam_2']; ?>&searchParam_3=<?php echo $_REQUEST['searchParam_3']; ?>';" /></td>
				<?php } ?>
				<?php  if($_SESSION['userLoginType']==4)  { ?>
				<td style="padding:10px;"><input type="button" value="Back" class="form-cen" onClick="location.href='retailer_order_list.php';" /></td>
				<?php } ?>
				<?php  if($_SESSION['userLoginType']==5)  { ?>
				<td style="padding:10px;"><input type="button" value="Back" class="form-cen" onClick="location.href='salesman_order_list.php';" /></td>
				<?php } ?>
				<?php } ?>
			</tr>
		</table>
		
		<?php } else { ?>
		<table border="0" width="100%" cellpadding="0" cellspacing="0" id="id-form">
			<tr bgcolor="#6E6E6E" style="color: #fff;font-weight: bold;">
				<td style="padding:10px;" width="10%">Status</td>
				<td style="padding:10px;" width="25%">Item Name</td>
				<td style="padding:10px;" width="10%">Item Code</td>
				<td style="padding:10px;" width="10%">Brand Name</td>
				<?php if($auOrd[0]->order_status=="A"){ ?>
				<td style="padding:10px;" width="10%">Quantity</td>
				<td style="padding:10px;" width="10%">Accepted Quantity</td>
				<?php } else { ?>
				<td style="padding:10px;" width="10%">Order Quantity</td>
				<td style="padding:10px;" width="10%">Accepted Quantity</td>
				<?php } ?>
				<td style="padding:10px;" width="10%">Price</td>
				<td align="right" style="padding-right: 50px;" width="5%">Total</td>
			</tr>
			<?php
		$condi=" Where t.order_id=".$auOrd[0]->order_id." and t.order_detail_status!=5 ";
		$Rec = mysql_query("SELECT t.*,i.item_name,i.item_code,b.brand_name,g.tag_description FROM table_order_detail as t left join table_item as i on t.item_id=i.item_id left join table_brands as b on b.brand_id=i.brand_id left join table_tags as g on g.tag_id=t.tag_id".$condi);
		while ($auRec = mysql_fetch_array($Rec)){
		?>
		<tr <?php if($auRec['order_detail_status']==2){ ?> bgcolor="#82FA58;" <?php } ?> <?php if($auRec['order_detail_status']==3){ ?> bgcolor="#B40404;" <?php } ?>style="border-bottom:2px solid #6E6E6E;" >
			</td>
			<?php if($auRec['type']==2){ ?>




			<?php if($auRec['discount_type']==1) {

				// Get the slab details

				$discountDetailCon=" WHERE DD.discount_detail_id = ".$auRec['discount_detail_id'];
				$discountDetailSql = mysql_query("SELECT DD.* FROM table_discount_detail AS DD".$discountDetailCon);
				$discountDetail = mysql_fetch_array($discountDetailSql);
				//print_r($discountDetail );
				$maxCondition = "";
				if($discountDetail['maximum_quantity']>0){
					$maxCondition = " - ".$discountDetail['maximum_quantity'];
				}

			?>
			<td style="padding:10px;">Percentage(%) Scheme</td>
			<td style="padding:10px;" <?php if($auOrd[0]->order_status=="A"){ ?> colspan="5" <?php } else { ?>  colspan="6" <?php } ?>>
			<font color="red"><?php echo $auRec['discount_desc'];?>  (Buy minimum off take <?php echo $discountDetail['minimum_quantity'].$maxCondition;?> nos. and get <?php echo $discountDetail['discount_percentage'];?>% discount.)</font>
			</td>
			<td align="right" style="padding-right: 50px;padding-top: 10px;" width="5%"><font color="red">-<?php echo $auRec['acc_total'];?></font></td>
			<?php } ?>



			<?php if($auRec['discount_type']==2) {

				// Get the slab details

				$discountDetailCon=" WHERE DD.discount_detail_id = ".$auRec['discount_detail_id'];
				$discountDetailSql = mysql_query("SELECT DD.* FROM table_discount_detail AS DD".$discountDetailCon);
				$discountDetail = mysql_fetch_array($discountDetailSql);

			?>
			<td style="padding:10px;" >Amount Scheme</td>
			<td style="padding:10px;" <?php if($auOrd[0]->order_status=="A"){ ?> colspan="5" <?php } else { ?>  colspan="6" <?php } ?>>
			<font color="red"><?php echo $auRec['discount_desc'];?> (BUY Miminum Amount <?php echo $discountDetail['minimum_amount'];?> and get <?php echo $discountDetail['discount_amount'];?> Rs. discount.)</font>
			</td>
			<td align="right" style="padding-right: 50px;padding-top: 10px;" width="5%"><font color="red">-<?php echo $auRec['acc_total'];?></font></td>
			<?php } ?>




			<?php if($auRec['discount_type']==3) {

			// Get the slab details

			$discountDetailCon=" WHERE DD.discount_detail_id = ".$auRec['discount_detail_id'];
			$discountDetailSql = mysql_query("SELECT DD.*, I.item_name, I.item_code FROM table_discount_detail AS DD LEFT JOIN table_foc_detail AS FOC ON FOC.foc_id = DD.foc_id LEFT JOIN table_item AS I ON I.item_id = FOC.free_item_id".$discountDetailCon);
			$discountDetail = mysql_fetch_array($discountDetailSql);

			?>
			<td style="padding:10px;" >FOC Scheme</td>
			<td style="padding:10px;" <?php if($auOrd[0]->order_status=="A"){ ?> colspan="3" <?php } else { ?>  colspan="4" <?php } ?>>
			<font color="red"><?php echo $auRec['discount_desc'];?> (Item Code- <?php echo $discountDetail['item_code'];?>)</font>
			</td>
			<td style="padding:10px;" ><font color="red"><?php echo $auRec['acc_free_item_qty'];?></font></td>
			<td style="padding:10px;" ></td>
			<td align="right" style="padding-right: 50px;padding-top: 10px;" width="5%"><font color="red"><?php echo $auRec['acc_total'];?></font></td>
			<?php } ?>



			<?php if($auRec['discount_type']==4) {?>
			<td style="padding:10px;" >Trip Scheme</td>
			<td style="padding:10px;" <?php if($auOrd[0]->order_status=="A"){ ?> colspan="3" <?php } else { ?>  colspan="4" <?php } ?>>
			<font color="red"><?php echo $auRec['discount_desc'].'-'.$auRec['trip_desc'];?></font>
			</td>
			<td style="padding:10px;" ><font color="red"><?php echo $auRec['acc_free_item_qty'];?></font></td>
			<td style="padding:10px;" ></td>
			<td align="right" style="padding-right: 50px;padding-top: 10px;" width="5%"><font color="red"><?php echo $auRec['acc_total'];?></font></td>
			<?php } ?>





			<?php } else { 
			if($auRec['tag_id']!=0){
			?>
			<td style="padding:10px;" ><?php echo $auRec['tag_description'] ?></td>
			<?php } else { ?>
			<td style="padding:10px;" ><?php if($auRec['order_detail_status']==2){ echo "Accepted"; } elseif($auRec['order_detail_status']==3){ echo "Rejected"; } else { echo " New Order";}?></td>
			<?php } ?>
			<td style="padding:10px;" ><?php echo $auRec['item_name'];?></td>
			<td style="padding:10px;" ><?php echo $auRec['item_code'];?></td>
			<td style="padding:10px;" ><?php echo $auRec['brand_name'];?></td>
			
			<?php /*if($auRec['color_type']==3) { ?>
			<td style="padding:10px;" ><?php echo $auRec['color_code'];?></td>
			<?php } ?>
			<?php if($auRec['color_type']==2) { ?>
			<td style="padding:10px;" >Assorted</td>
			<?php } ?>
			<?php if($auRec['color_type']==1 || $auRec['color_type']=='') { ?>
			<td style="padding:10px;" ></td>
			<?php }*/ ?>


			<?php if($auRec['type']==1) {?>
			<?php if($auOrd[0]->order_status=="A"){ ?>
			<td style="padding:10px;" ><?php echo $auRec['quantity'];?></td>
			<td style="padding:10px;" ><?php echo $auRec['acc_quantity'];?></td>
			<?php } else { ?>
			<td style="padding:10px;" ><?php echo $auRec['quantity'];?></td>
			<td style="padding:10px;" ><?php echo $auRec['acc_quantity'];?></td>
			<?php } ?>
			<?php } ?>
			<td style="padding:10px;" ><?php echo $auRec['price'];?></td>
			<td align="right" style="padding-right: 50px;padding-top: 10px;" ><?php echo $auRec['acc_total'];?></td>
			<?php } ?>
		</tr>
		<?php } ?>
		<?php
		//combo discount list
		$comboRec = mysql_query("SELECT dc.order_combo_detail_id,dc.acc_discount_desc,dc.acc_discount_amount,dc.discount_type,dc.acc_free_item_qty,i.item_code FROM table_order_combo_detail as dc left join table_item as i on i.item_id=dc.acc_free_item_id where dc.order_id=".$auOrd[0]->order_id." and dc.status!='5'");
		while ($comboListRec = mysql_fetch_array($comboRec)){
		?>
		<tr  style="border-bottom:2px solid #6E6E6E;">
			<?php if($comboListRec['discount_type']==1) {?>
			<td style="padding:10px;">Scheme</td>
			<td style="padding:10px;" <?php if($auOrd[0]->order_status=="A"){ ?> colspan="5" <?php } else { ?>  colspan="6" <?php } ?>>
			<font color="red"><?php echo $comboListRec['acc_discount_desc'];?></font>
			</td>
			<td align="right" style="padding-right: 50px;padding-top: 10px;" width="5%"><font color="red">-<?php echo $comboListRec['acc_discount_amount'];?></font></td>
			<?php } ?>
			<?php if($comboListRec['discount_type']==2) {?>
			<td style="padding:10px;" >Scheme</td>
			<td style="padding:10px;" <?php if($auOrd[0]->order_status=="A"){ ?> colspan="5" <?php } else { ?>  colspan="6" <?php } ?>>
			<font color="red"><?php echo $comboListRec['acc_discount_desc'];?></font>
			</td>
			<td align="right" style="padding-right: 50px;padding-top: 10px;" width="5%"><font color="red">-<?php echo $comboListRec['acc_discount_amount'];?></font></td>
			<?php } ?>
			<?php if($comboListRec['discount_type']==3) {?>
			<td style="padding:10px;" >Scheme</td>
			<td style="padding:10px;" <?php if($auOrd[0]->order_status=="A"){ ?> colspan="1" <?php } else { ?>  colspan="2" <?php } ?>>
			<font color="red"><?php echo $comboListRec['acc_discount_desc'];?></font>
			</td>
			<td style="padding:10px;" ><font color="red"><?php echo $comboListRec['item_code'];?></font></td>
			<td style="padding:10px;" ><font color="red"></font></td>
			<td style="padding:10px;" ><font color="red"><?php echo $comboListRec['acc_free_item_qty'];?></font></td>
			<td style="padding:10px;" ></td>
			<td align="right" style="padding-right: 50px;padding-top: 10px;" width="5%"><font color="red"><?php echo $comboListRec['acc_discount_amount'];?></font></td>
			<?php } ?>
		</tr>
		<?php } ?>
		<?php if($auOrd[0]->acc_discount_type!='') {?>
		<tr  >
			<td align="right" style="padding-right: 50px;padding-top: 10px;" <?php if($auOrd[0]->order_status=="A"){ ?> colspan="6" <?php } else { ?>  colspan="7" <?php } ?>>
			<b>Total</b></td>
			<td align="right" style="padding-right: 50px;padding-top: 10px;"><b><?php echo number_format($auOrd[0]->acc_total_invoice_amount + $auOrd[0]->acc_discount_amount, 2, '.', ',');?></b></td>
		</tr>
		<tr  >
			<td align="right" style="padding-right: 50px;padding-top: 10px;" <?php if($auOrd[0]->order_status=="A"){ ?> colspan="6" <?php } else { ?>  colspan="7" <?php } ?>>
			<font color="red"><b>Scheme(<?php echo $auOrd[0]->discount_desc;?>)</b></font></td>
			<td align="right" style="padding-right: 50px;padding-top: 10px;"><font color="red"><b>-<?php echo number_format($auOrd[0]->acc_discount_amount, 2, '.', ',');?></b></font></td>
		</tr>
		<tr  >
			<td align="right" style="padding-right: 50px;padding-top: 10px;" <?php if($auOrd[0]->order_status=="A"){ ?> colspan="6" <?php } else { ?>  colspan="7" <?php } ?>>
			<b>Net Total</b></td>
			<td align="right" style="padding-right: 50px;padding-top: 10px;"><b><?php echo number_format($auOrd[0]->acc_total_invoice_amount, 2, '.', ',');?></b></td>
		</tr>
		<?php } else { ?>
		<tr  >
			<td align="right" style="padding-right: 50px;padding-top: 10px;" <?php if($auOrd[0]->order_status=="A"){ ?> colspan="6" <?php } else { ?>  colspan="7" <?php } ?>>
			<b>Net Total</b></td>
			<td align="right" style="padding-right: 50px;padding-top: 10px;"><b><?php echo number_format($auOrd[0]->acc_total_invoice_amount, 2, '.', ',');?></b></td>
		</tr>
		<?php } ?>
		
		<?php if($ret_lat=='' || $ret_lat==0) { ?>
		<tr bgcolor="#A52A2A" style="color: #fff;font-weight: bold;" align="center">
		<!--<tr align="center" style="border-top:2px solid #6E6E6E;" >-->
			<td style="padding:10px;" colspan="10"> 
				<div style="width: 100%;height:15px;"><?php echo $uType;?>  Location Not Available</div>
			</td>
		</td>
		<?php } ?>
			
		<tr align="center" style="border-top:2px solid #6E6E6E;" >
			<td style="padding:10px;" colspan="10">
			<?php if($sal_lat!=''){ ?>
				<div id="map_canvas" style="width:100%; height:350px"></div>
				<?php } else { ?>
				<div bgcolor="#A52A2A" style="color: #fff;font-weight: bold;background-color:#A52A2A; height:15px; padding:10px;" align="center">Order Location Not Available</div>
			<?php } ?>
			</td>
		</tr>
		<tr align="center" >
			<?php if($_REQUEST['act_id']!=''){ ?>
			<td style="padding:10px;" colspan="8"><input type="button" value="Close" class="form-cen" onClick="javascript:window.close();" /></td>
			<?php } else { ?>
			<?php  if($_SESSION['userLoginType']==1 || $_SESSION['userLoginType']==2)  { ?>
			<td style="padding:10px;" colspan="8"><input type="button" value="Back" class="form-cen" onClick="location.href='admin_order_list.php?searchParam_1=<?php echo $_REQUEST['searchParam_1']; ?>&searchParam_2=<?php echo $_REQUEST['searchParam_2']; ?>&searchParam_3=<?php echo $_REQUEST['searchParam_3']; ?>';" /></td>
			<?php } ?>
			<?php  if($_SESSION['userLoginType']==4)  { ?>
			<td style="padding:10px;"><input type="button" value="Back" class="form-cen" onClick="location.href='retailer_order_list.php';" /></td>
			<?php } ?>
			<?php  if($_SESSION['userLoginType']==5)  { ?>
			<td style="padding:10px;"><input type="button" value="Back" class="form-cen" onClick="location.href='admin_order_list.php';" /></td>
			<?php } ?>
			<?php } ?>
		</tr>
		
		</table>
		<?php } ?>
	</td>
	</tr>
	</div>
	</td>
</tr>
</table>
<div class="clear"></div>
</div>
<!--  end content-table-inner  -->
</td>
<td id="tbl-border-right"></td>
</tr>
<!--<tr>
	<th class="sized bottomleft"></th>
	<td id="tbl-border-bottom">&nbsp;</td>
	<th class="sized bottomright"></th>
</tr>-->
</table>
<div class="clear">&nbsp;</div>
</div>
<!--  end content -->
<div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->
<div class="clear">&nbsp;</div> 
<!-- start footer -->         
<?php include("footer.php") ?>
<!-- end footer -->

</body>
</html>