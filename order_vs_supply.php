<?php
include("includes/config.inc.php");
include("includes/function.php");
include("includes/globalarraylist.php");



$page_name="Order Vs Supply";
//echo "<pre >";
if(isset($_POST['showOrderlist']) && $_POST['showOrderlist'] == 'yes')
{	
	//echo "Hello";
	//print_r($_POST);
	//exit;
	
	if($_POST['sal']!="") 
	{
	$_SESSION['SalDisList']=$_POST['sal'];	
	}
	if($_POST['ret']!="A") 
	{
	$_SESSION['retDisId']=$_POST['ret'];	
	}
	if($_POST['item']!="A") 
	{
	$_SESSION['itemDisId']=$_POST['item'];	
	}
	
	if($_POST['from']!="") 
	{
	$_SESSION['FromDisList']=$_POST['from'];
	}
	if($_POST['to']!="") 
	{
		$_SESSION['ToDisList']=$_POST['to'];	
	}
	
	if($_POST['sal']=="") 
	{
	  unset($_SESSION['SalDisList']);	
	}
	
	if($_POST['ret']=="A") 
	{
	  unset($_SESSION['retDisId']);	
	}
	if($_POST['item']=="A") 
	{
	  unset($_SESSION['itemDisId']);	
	}
	
	if($_POST['dis']!="A") 
	{
		$_SESSION['OrderDisId']=$_POST['dis'];	
	}
	else{	
		if($_SESSION['userLoginType']!='3'){
			$_SESSION['OrderDisId']="A";
		}
		
	}
	
	/*if(isset($_POST['export']) && $_POST['export'])
	{
		header("Location:export.inc.php?export_order_vs_supply");
		exit;
	}*/
	
	if(isset($_SESSION['SalDisList']) && $_SESSION['SalDisList']!=""){
			$sal=$_objAdmin->_getSelectList('table_salesman','salesman_name',''," salesman_id='".$_SESSION['SalDisList']."'"); 
			$salesman_name1=$sal[0]->salesman_name;
		}
	else
		{
			$salesman_name1="All";
		}
	//echo $salesman_name;
	//header("Location: distributors_order_list.php");
	
	
}


if(isset($_REQUEST['reset']) && $_REQUEST['reset'] == 'yes')
{
	$_SESSION['OrderDisId']="A";	
	unset($_SESSION['SalDisList']);	
	unset($_SESSION['retDisId']);	
	unset($_SESSION['itemDisId']);	
	//unset($_SESSION['OrderBy']);	
	$_SESSION['FromDisList']= date('Y-m-d');
	$_SESSION['ToDisList']= date('Y-m-d');
	header("Location: order_vs_supply.php");
}
include("header.inc.php");
//echo $salesman_name."-".$_SESSION['ToDisList']."-".$_SESSION['FromDisList'];
//exit;
?>
<script src="javascripts/dateNextPrev.js" type="text/javascript"></script>
<script src="http://code.jquery.com/jquery-1.7.2.js"></script>
<script type="text/javascript">

    function PrintElem(elem)
    {
        Popup($(elem).html());
    }

    function Popup(data) 
    {
		
        var mywindow = window.open('', 'Report');
		
        mywindow.document.write('<html><head><title>Order Vs Supply Report</title>');
		mywindow.document.write('<table><tr><td><b>Salesman Name:</b> <?php echo $salesman_name1; ?></td><td><b>From Date:</b><?php echo $_SESSION['FromDisList']; ?></td><td><b>To Date:</b> <?php echo $_SESSION['ToDisList']; ?></td></tr></table>');
        /*optional stylesheet*/ //mywindow.document.write('<link rel="stylesheet" href="main.css" type="text/css" />');
        mywindow.document.write('</head><body >');
        mywindow.document.write(data);
        mywindow.document.write('</body></html>');

        mywindow.print();
        mywindow.close();
        return true;
    }

$(document).ready(function(){
<?php if($_POST['submit']=='Export to Excel'){ ?>
tableToExcel('report_export', 'Order Vs Supply Report', 'Order Vs Supply Report.xls');
<?php } ?>
});

</script>
<script type="text/javascript">
// Popup window code
function photoPopup(url) {
	popupWindow = window.open(
		url,'popUpWindow','height=500,width=800,left=100,top=100,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no,status=yes')
}
</script>
<!-- start content-outer -->
<input name="pagename" type="hidden"  id="pagename" value="admin_order_list.php" />
<div id="content-outer">
<!-- start content -->
<div id="content">
<div id="page-heading"><h1><span style="color: #d74343; font-family: Tahoma; font-weight: bold;">Order Vs Supply</span></h1></div>
<table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">
<tr>
	<td>
	<!--  start content-table-inner -->
	<div id="content-table-inner">
	
	<div id="page-heading" align="left" >
	<form name="frmPre" id="frmPre" method="post" action="" enctype="multipart/form-data" >
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
		<tr>
			<td>
				<h3>&nbsp;&nbsp;Salesman:</h3>
				<h6>
					<select name="sal" id="sal" class="styledselect_form_5" style="" >
					<?php echo $rtndata = $_objArrayList->GetSalesmenMenu($salsList, $_SESSION['SalDisList']);?>
					</select>
				</h6>
			</td>
			
			 <td>
				 <h3>&nbsp;&nbsp;Distributors:</h3><h6>
					<select name="dis" id="dis" class="styledselect_form_5" style="">			  
					<?php if($_SESSION['userLoginType']==3)  {
						$aDis=$_objAdmin->_getSelectList('table_distributors','*',''," distributor_id='".$_SESSION['OrderDisId']."'"); 
					?>
						<option value="<?php echo $aDis[0]->distributor_id;?>" ><?php echo $aDis[0]->distributor_name;?></option>
					<?php } else { ?>			  
						<option value="A" >All Distributors</option>
					<?php 
						$aDis=$_objAdmin->_getSelectList('table_distributors','*',''," account_id='".$_SESSION['accountId']."'ORDER BY distributor_name"); 
							if(is_array($aDis))
							{
								for($i=0;$i<count($aDis);$i++)
								{
					?>
						<option value="<?php echo $aDis[$i]->distributor_id;?>" <?php  if ($aDis[$i]->distributor_id==$_SESSION['OrderDisId']){ ?> selected <?php } ?>><?php echo $aDis[$i]->distributor_name;?></option>
					<?php  
								}
							} 
						}
					?>
					</select>
					</h6>
				</td>				
				<td>
					<h3>&nbsp;&nbsp;Retailers:</h3>
						<h6>
							<select name="ret" id="ret" class="styledselect_form_5" style="" >
							<option value="A" >All Retailers</option>
					<?php 
							$retailerList=$_objAdmin->_getSelectList('table_retailer','*',''," account_id='".$_SESSION['accountId']."' ORDER BY retailer_name"); 
							if(is_array($retailerList))
							{
								for($i=0;$i<count($retailerList);$i++)
								{
					?>
							<option value="<?php echo $retailerList[$i]->retailer_id;?>" <?php  if ($retailerList[$i]->retailer_id==$_SESSION['retDisId']){ ?> selected <?php } ?>><?php echo $retailerList[$i]->retailer_name;?></option>
					<?php  		}
							}
					?>
							</select>
						</h6>
				</td>			
				<td>
					<h3>&nbsp;&nbsp;Item:</h3>
						<h6>
							<select name="item" id="item" class="styledselect_form_5" style="" >
								<option value="A" >All Item</option>
					<?php 
							$itemList=$_objAdmin->_getSelectList('table_item','*',''," account_id='".$_SESSION['accountId']."' ORDER BY item_name"); 
								if(is_array($itemList))
								{
									for($i=0;$i<count($itemList);$i++)
									{
								?>
								<option value="<?php echo $itemList[$i]->item_id;?>" <?php  if ($itemList[$i]->item_id==$_SESSION['itemDisId']){ ?> selected <?php } ?>><?php echo $itemList[$i]->item_name;?></option>
					<?php  			}
								} 
					?>
						</select>
					</h6>
				</td>
			
			</tr>
			<tr>			
			 
			  <td>
				  <h3>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;From Date:</h3>
					<h6>
						<img src="css/images/prev.png" height="18" width="18" onclick="dateFromPrev();"> 
							<input type="text" id="from" name="from" class="date" style="width:150px" value="<?php if($_SESSION['FromDisList']!='') { echo $_objAdmin->_changeDate($_SESSION['FromDisList']); } else { echo $_objAdmin->_changeDate(date('Y-m-d')); }?>"  readonly />
						<img src="css/images/next.png" height="18" width="18" onclick="dateFromNext();">
					</h6>
			</td>
			<td>
				<h3>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;To Date:</h3>
					<h6>
						<img src="css/images/prev.png" height="18" width="18" onclick="dateToPrev();"> 
							<input type="text" id="to" name="to" class="date" style="width:150px" value="<?php if($_SESSION['ToDisList']!='') { echo $_objAdmin->_changeDate($_SESSION['ToDisList']); } else { echo $_objAdmin->_changeDate(date('Y-m-d')); }?>" readonly /> 
						<img src="css/images/next.png" height="18" width="18" onclick="dateToNext();">
					</h6>
			</td>
			  
			<td colspan="3">
				<input name="submit" class="result-submit" type="submit" id="submit" value="View Details" />
				<input type="button" value="Print" class="result-submit" onclick="PrintElem('#Report')" />
				<a id="dlink"  style="display:none;"></a>
				<input type="submit" name="submit" value="Export to Excel" class="result-submit"  >
				<input type="button" value="Reset!" class="form-reset" onclick="location.href='order_vs_supply.php?reset=yes';" />
				<input name="showOrderlist" type="hidden" value="yes" />
			</td>
			<td colspan="2"></td>
		</tr>
	</table>
	</form>
	</div>
<div id="Report">
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
		<tr valign="top">
<?php
	
	if(isset($_SESSION['OrderDisId']) && $_SESSION['OrderDisId']!="A" ) 
		{
			$disList=" and o.distributor_id=".$_SESSION['OrderDisId']." ";
		}
		else{
		
		$disList="";
		
		}
		
	if(isset($_SESSION['retDisId']) && $_SESSION['retDisId']!="A") 
		{
			$retList=" and o.retailer_id=".$_SESSION['retDisId']."";
		}
		else{
			
			$retList="";
		
		}


	if(isset($_SESSION['SalDisList']) && $_SESSION['SalDisList']!="") 
		{
			$salList=" and o.salesman_id=".$_SESSION['SalDisList']."";
		}
		else{
		$salList="";
		
		}

	if(isset($_SESSION['itemDisId']) && $_SESSION['itemDisId']!="A") 
		{
			$itemList=" and od.item_id=".$_SESSION['itemDisId']."";
		}
		else{
			$itemList="";
		
		}

	$getSalesmanWiseOrderDetail=$_objAdmin->_getSelectList('table_order as o left join table_order_detail as od on od.order_id=o.order_id left join table_item as i on i.item_id=od.item_id left join table_salesman as sal on sal.salesman_id=o.salesman_id left join table_category as c on c.category_id=i.category_id ','i.item_name,c.category_name,SUM(od.quantity) AS oQuantity,SUM(od.acc_quantity) AS dQuantity,SUM(od.total) AS oTotal,SUM(od.acc_total) AS dTotal,i.item_id',''," $salList $disList $retList $itemList and sal.status!='D' and o.order_status='D' and o.date_of_order >= '".date('Y-m-d', strtotime(mysql_escape_string($_SESSION['FromDisList'])))."' and o.date_of_order <= '".date('Y-m-d', strtotime(mysql_escape_string($_SESSION['ToDisList'])))."' and o.order_status='D' GROUP BY od.item_id "); 

//print_r($getSalesmanWiseOrder);
?>
		<td>
		<table  border="0" width="100%" cellpadding="0" cellspacing="0" id="report_export" name="report_export">
				<div id="Report">
			<tr  bgcolor="#6E6E6E" style="color: #fff;font-weight: bold;" >			
				<td style="padding:10px;" width="20%">Item Name</td>
				<td style="padding:10px;" width="10%">Category Name</td>
				<td style="padding:10px;" width="10%"> Ordered Quantity</td>
				<td style="padding:10px;" width="10%"> Ordered Value</td>
				<td style="padding:10px;" width="10%"> Dispatced Quantity</td>
				<td style="padding:10px;" width="10%"> Dispatced Value</td>
				<td style="padding:10px;" width="10%">Details</td>
			</tr>
			<?php	
					if(sizeof($getSalesmanWiseOrderDetail)>0)
						{
							foreach($getSalesmanWiseOrderDetail as $orderDetailKey=>$orderDetailValue)
								{	
			 ?>
			 <tr  style="border-bottom:2px solid #6E6E6E;" >
				<td style="padding:10px;" width="20%"><?php echo $orderDetailValue->item_name; ?></td>
				<td style="padding:10px;" width="20%"><?php echo $orderDetailValue->category_name; ?></td>
				<td style="padding:10px;" width="10%"><?php echo $orderDetailValue->oQuantity; ?></td>
				<td style="padding:10px;" width="10%"><?php echo $orderDetailValue->oTotal; ?></td>
				<td style="padding:10px;" width="10%"><?php echo $orderDetailValue->dQuantity; ?></td>
				<td style="padding:10px;" width="10%"><?php echo $orderDetailValue->dTotal; ?></td>
				<td style="padding:10px;" width="10%">
					<a href="order_vs_supply_details.php?id=<?php echo $orderDetailValue->item_id; ?>">View Details</a>
				</td>
			</tr>
				<?php } ?>
			<?php } else { ?>
			<tr  style="border-bottom:2px solid #6E6E6E;" align="center">
				<td style="padding:10px;" colspan="7">Report Not Available</td>
			</tr>
			<?php } ?>
			</div>
		</table>
		</td>
	</tr>
	<tr>
		<td><img src="images/blank.gif" width="695" height="1" alt="blank" /></td>
		<td></td>
	</tr>
	</table>
	</div>
<div class="clear"></div>
</div>
<!--  end content-table-inner  -->
</td>
<td id="tbl-border-right"></td>
</tr>
<!--<tr>
	<th class="sized bottomleft"></th>
	<td id="tbl-border-bottom">&nbsp;</td>
	<th class="sized bottomright"></th>
</tr>-->
</table>
<div class="clear">&nbsp;</div>
</div>
<!--  end content -->
<div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->
<div class="clear">&nbsp;</div> 
<!-- start footer -->         
<?php include("footer.php") ?>
<?php //unset($_SESSION['SalAttList']);	?>
<!-- end footer -->
<script src="javascripts/jquery-1.8.2.js" type="text/javascript"></script>
<script src="javascripts/jquery-ui.js"></script>
<script type="text/javascript" src="javascripts/validate.js"></script>
<script type="text/javascript">
// Popup window code
function newPopup(url) {
	popupWindow = window.open(
		url,'popUpWindow','height=500,width=600,left=200,top=100,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no,status=yes')
}
</script>
<script>
    $(function() {
        $( "#from" ).datepicker({
			dateFormat: "d M yy",
            defaultDate: "w",
            changeMonth: true,
            numberOfMonths: 1,
            onSelect: function( selectedDate ) {
                $( "#to" ).datepicker( "option", "minDate", selectedDate );
            }
        });
        $( "#to" ).datepicker({
			dateFormat: "d M yy",
            defaultDate: "-w",
            changeMonth: true,
            numberOfMonths: 1,
            onSelect: function( selectedDate ) {
                $( "#from" ).datepicker( "option", "maxDate", selectedDate );
            }
        });
    });
</script>
<script type='text/javascript'>//<![CDATA[ 
var tableToExcel = (function () {
        var uri = 'data:application/vnd.ms-excel;base64,'
        , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table><tr><td><b>Salesman Name:</b> <?php echo $salesman_name1; ?></td><td><b>From Date:</b> <?php echo $_SESSION['FromDisList']; ?></td><td><b>To Date:</b> <?php echo $_SESSION['ToDisList']; ?></td></tr></table><table>{table}</table></body></html>'
        , base64 = function (s) { return window.btoa(unescape(encodeURIComponent(s))) }
        , format = function (s, c) { return s.replace(/{(\w+)}/g, function (m, p) { return c[p]; }) }
        return function (table, name, filename) {
            if (!table.nodeType) table = document.getElementById(table)
            var ctx = { worksheet: name || 'Worksheet', table: table.innerHTML }

            document.getElementById("dlink").href = uri + base64(format(template, ctx));
            document.getElementById("dlink").download = filename;
            document.getElementById("dlink").click();

        }
    })()

//]]>  
</script>


</body>
</html>
