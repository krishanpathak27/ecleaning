<?php
include("includes/config.inc.php");
include("includes/function.php");
include("includes/globalarraylist.php");



$page_name="Order Vs Supply Report Details";
if(isset($_SESSION['SalDisList']) && $_SESSION['SalDisList']!=""){
			$sal=$_objAdmin->_getSelectList('table_salesman','salesman_name',''," salesman_id='".$_SESSION['SalDisList']."'"); 
			$salesman_name1=$sal[0]->salesman_name;
		}
	else
		{
			$salesman_name1="All";
		}
		
if(isset($_REQUEST['id']) && $_REQUEST['id']!=""){
			$itemDetails=$_objAdmin->_getSelectList('table_item as i left join table_category as cat on cat.category_id=i.category_id','i.item_name,cat.category_name',''," i.item_id='".$_REQUEST['id']."'"); 
			$item_name=$itemDetails[0]->item_name;
			$category_name=$itemDetails[0]->category_name;
		}
	else
		{
			$salesman_name1="All";
		}
//echo "<pre >";

include("header.inc.php");

?>
<script src="javascripts/dateNextPrev.js" type="text/javascript"></script>
<script src="http://code.jquery.com/jquery-1.7.2.js"></script>
<script type="text/javascript">

    function PrintElem(elem)
    {
        Popup($(elem).html());
    }

    function Popup(data) 
    {
		
        var mywindow = window.open('', 'Report');
		
        mywindow.document.write('<html><head><title>Order Vs Supply Report Details</title>');
		mywindow.document.write('<table><tr><td><b>Salesman Name:</b> <?php echo $salesman_name1; ?></td><td><b>From Date:</b><?php echo $_SESSION['FromDisList']; ?></td><td><b>To Date:</b> <?php echo $_SESSION['ToDisList']; ?></td></tr></table>');
        /*optional stylesheet*/ //mywindow.document.write('<link rel="stylesheet" href="main.css" type="text/css" />');
        mywindow.document.write('</head><body >');
        mywindow.document.write(data);
        mywindow.document.write('</body></html>');

        mywindow.print();
        mywindow.close();
        return true;
    }

$(document).ready(function(){
<?php if($_POST['submit']=='Export to Excel'){ ?>
tableToExcel('report_export', 'Order Vs Supply Report Details', 'Order Vs Supply Report Details.xls');
<?php } ?>
});

</script>
<script type="text/javascript">
// Popup window code
function photoPopup(url) {
	popupWindow = window.open(
		url,'popUpWindow','height=500,width=800,left=100,top=100,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no,status=yes')
}
</script>
<!-- start content-outer -->
<input name="pagename" type="hidden"  id="pagename" value="admin_order_list.php" />
<div id="content-outer">
<!-- start content -->
<div id="content">
<div id="page-heading"><h1><span style="color: #d74343; font-family: Tahoma; font-weight: bold;">Order Vs Supply</span></h1></div>
<table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">
<tr>
	<td>
	<!--  start content-table-inner -->
	<div id="content-table-inner">
	
	<div id="page-heading" align="left" >
		<table border="0" width="100%" cellpadding="0" cellspacing="0" id="id-form">
			<tr>
				<td><b>Salesman Name:</b> <?php echo $salesman_name1;?></td>
				<!--<td align="right" style="padding-right: 50px;"><b>Order Number:</b> <?php //echo $auOrd[0]->order_id;?></td>-->
			</tr>
			<tr>
				<td><b>Item Name:</b> <?php echo $item_name;?></td>
				
			</tr>
			<tr>
				<td><b>Category:</b> <?php echo $category_name;?></td>
			</tr>
			<tr>
				<td><b>From:</b> <?php echo $_SESSION['FromDisList'];?><b>&nbsp;&nbsp;To:</b> <?php echo $_SESSION['ToDisList']; ?></td>
			</tr>
			
		</table>
		<form name="frmPre" id="frmPre" method="post" action="" enctype="multipart/form-data" >
				<table border="0" width="100%" cellpadding="0" cellspacing="0">
					<tr>
						<td colspan="10">
							<a href="order_vs_supply.php">
								<input class="result-submit" style="padding:5px; margin-top:10px; cursor: pointer;" type="button" name="back" value="Back">
							</a>
								<input type="button" value="Print" class="result-submit" onclick="PrintElem('#Report')" />
							<a id="dlink"  style="display:none;"></a>
								<input type="submit" name="submit" value="Export to Excel" class="result-submit"  >
						</td>
					</tr>
				</table>
			</form>
	</div>
<div id="Report">
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
		<tr valign="top">
<?php
	
	if(isset($_SESSION['OrderDisId']) && $_SESSION['OrderDisId']!="A" ) 
		{
			$disList=" and o.distributor_id=".$_SESSION['OrderDisId']." ";
		}
		else{
		
		$disList="";
		
		}
		
	if(isset($_SESSION['retDisId']) && $_SESSION['retDisId']!="A") 
		{
			$retList=" and o.retailer_id=".$_SESSION['retDisId']."";
		}
		else{
			
			$retList="";
		
		}


	if(isset($_SESSION['SalDisList']) && $_SESSION['SalDisList']!="") 
		{
			$salList=" and o.salesman_id=".$_SESSION['SalDisList']."";
		}
		else{
		$salList="";
		
		}

	if(isset($_SESSION['itemDisId']) && $_SESSION['itemDisId']!="A") 
		{
			$itemList=" and od.item_id=".$_SESSION['itemDisId']."";
		}
		else{
			$itemList="";
		
		}

	$getSalesmanWiseOrderDetail=$_objAdmin->_getSelectList('table_order as o left join table_order_detail as od on od.order_id=o.order_id left join table_item as i on i.item_id=od.item_id left join table_retailer as r on r.retailer_id=o.retailer_id left join table_distributors as d on d.distributor_id=o.distributor_id left join city as c on c.city_id=r.city left join state as st on st.state_id=r.state  ','i.item_name,SUM(od.quantity) AS oQuantity,SUM(od.acc_quantity) AS dQuantity,SUM(od.total) AS oTotal,SUM(od.acc_total) AS dTotal,r.retailer_name,d.distributor_name,i.item_id,od.order_id,st.state_name,c.city_name,o.date_of_order,o.last_update_date',''," $salList $disList $retList $itemList and o.order_status='D' and o.date_of_order >= '".date('Y-m-d', strtotime(mysql_escape_string($_SESSION['FromDisList'])))."' and o.date_of_order <= '".date('Y-m-d', strtotime(mysql_escape_string($_SESSION['ToDisList'])))."' and o.order_status='D' and od.item_id='".$_REQUEST['id']."' GROUP BY od.order_id "); 

//print_r($getSalesmanWiseOrder);
?>
		<td>
		<table  border="0" width="100%" cellpadding="0" cellspacing="0" id="report_export" name="report_export">
			<div id="Report">
			<tr  bgcolor="#6E6E6E" style="color: #fff;font-weight: bold;" >			
				<td style="padding:10px;" width="10%"> Order Number</td>
				<td style="padding:10px;" width="10%"> Distributor</td>
				<td style="padding:10px;" width="10%"> Retailer</td>
				<td style="padding:10px;" width="10%"> State</td>
				<td style="padding:10px;" width="10%"> City</td>
				<td style="padding:10px;" width="10%"> Order Date</td>
				<td style="padding:10px;" width="10%"> Ordered Quantity</td>
				<td style="padding:10px;" width="10%"> Ordered Value</td>
				<td style="padding:10px;" width="10%"> Dispatch Date</td>
				<td style="padding:10px;" width="10%"> Dispatced Quantity</td>
				<td style="padding:10px;" width="10%"> Dispatced Value</td>
			</tr>
			
			<?php	
					if(sizeof($getSalesmanWiseOrderDetail)>0)
						{
							foreach($getSalesmanWiseOrderDetail as $orderDetailKey=>$orderDetailValue)
								{	
									
						
			 ?>
			 <tr  style="border-bottom:2px solid #6E6E6E;" >
				<td style="padding:10px;" width="10%"><?php echo $orderDetailValue->order_id; ?></td>
				<td style="padding:10px;" width="10%"><?php echo $orderDetailValue->distributor_name; ?></td>
				<td style="padding:10px;" width="10%"><?php echo $orderDetailValue->retailer_name; ?></td>
				<td style="padding:10px;" width="10%"><?php echo $orderDetailValue->state_name; ?></td>
				<td style="padding:10px;" width="10%"><?php echo $orderDetailValue->city_name; ?></td>
				<td style="padding:10px;" width="10%"><?php echo $_objAdmin->_changeDate($orderDetailValue->date_of_order); ?></td>
				<td style="padding:10px;" width="10%"><?php echo $orderDetailValue->oQuantity; ?></td>
				<td style="padding:10px;" width="10%"><?php echo $orderDetailValue->oTotal; ?></td>
				<td style="padding:10px;" width="10%"><?php echo $_objAdmin->_changeDate($orderDetailValue->last_update_date); ?></td>
				<td style="padding:10px;" width="10%"><?php echo $orderDetailValue->dQuantity; ?></td>
				<td style="padding:10px;" width="10%"><?php echo $orderDetailValue->dTotal; ?></td>
			</tr>
				<?php } ?>
			
			<?php } else { ?>
			<tr  style="border-bottom:2px solid #6E6E6E;" align="center">
				<td style="padding:10px;" colspan="7">Report Not Available</td>
			</tr>
			<?php } ?>
			</div>
		</table>
		</td>
	</tr>
	<tr>
		<td><img src="images/blank.gif" width="695" height="1" alt="blank" /></td>
		<td></td>
	</tr>
	</table>
	</div>
<div class="clear"></div>
</div>
<!--  end content-table-inner  -->
</td>
<td id="tbl-border-right"></td>
</tr>
<!--<tr>
	<th class="sized bottomleft"></th>
	<td id="tbl-border-bottom">&nbsp;</td>
	<th class="sized bottomright"></th>
</tr>-->
</table>
<div class="clear">&nbsp;</div>
</div>
<!--  end content -->
<div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->
<div class="clear">&nbsp;</div> 
<!-- start footer -->         
<?php include("footer.php") ?>
<?php //unset($_SESSION['SalAttList']);	?>
<!-- end footer -->
<script src="javascripts/jquery-1.8.2.js" type="text/javascript"></script>
<script src="javascripts/jquery-ui.js"></script>
<script type="text/javascript" src="javascripts/validate.js"></script>
<script type="text/javascript">
// Popup window code
function newPopup(url) {
	popupWindow = window.open(
		url,'popUpWindow','height=500,width=600,left=200,top=100,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no,status=yes')
}
</script>
<script>
    $(function() {
        $( "#from" ).datepicker({
			dateFormat: "d M yy",
            defaultDate: "w",
            changeMonth: true,
            numberOfMonths: 1,
            onSelect: function( selectedDate ) {
                $( "#to" ).datepicker( "option", "minDate", selectedDate );
            }
        });
        $( "#to" ).datepicker({
			dateFormat: "d M yy",
            defaultDate: "-w",
            changeMonth: true,
            numberOfMonths: 1,
            onSelect: function( selectedDate ) {
                $( "#from" ).datepicker( "option", "maxDate", selectedDate );
            }
        });
    });
</script>
<script type='text/javascript'>//<![CDATA[ 
var tableToExcel = (function () {
        var uri = 'data:application/vnd.ms-excel;base64,'
        , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table><tr><td><b>Salesman Name:</b> <?php echo $salesman_name1; ?></td><td><b>From Date:</b> <?php echo $_SESSION['FromDisList']; ?></td><td><b>To Date:</b> <?php echo $_SESSION['ToDisList']; ?></td></tr></table><table>{table}</table></body></html>'
        , base64 = function (s) { return window.btoa(unescape(encodeURIComponent(s))) }
        , format = function (s, c) { return s.replace(/{(\w+)}/g, function (m, p) { return c[p]; }) }
        return function (table, name, filename) {
            if (!table.nodeType) table = document.getElementById(table)
            var ctx = { worksheet: name || 'Worksheet', table: table.innerHTML }

            document.getElementById("dlink").href = uri + base64(format(template, ctx));
            document.getElementById("dlink").download = filename;
            document.getElementById("dlink").click();

        }
    })()

//]]>  
</script>


</body>
</html>
