<?php
include("includes/config.inc.php");
include("includes/function.php");
$_objAdmin = new Admin();
if(isset($_POST['add']) && $_POST['add'] == 'yes'){
	unlink('logo/'.$_POST['image_name'].'');
	$_objAdmin->addAccountlogo($_POST['account_id']);
	}


$auRec=$_objAdmin->_getSelectList('table_account',"*",''," account_id=".$_SESSION['accountId']);
if($auRec[0]->logo!=''){
$ImgName=$auRec[0]->logo;
} else {
$ImgName="company_logo.jpg";
}
?>
<?php include("header.inc.php"); ?>
<script type="text/javascript" src="javascripts/validate.js"></script>
<!-- start content-outer -->
<div id="content-outer">
<!-- start content -->
	<div id="content">
	<div id="page-heading"><h1><span style="color: #d74343; font-family: Tahoma; font-weight: bold;">My Organizations</span></h1></div>
	<table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">

	<tr>
		<!--<td id="tbl-border-left"></td>-->
		<td>
		<!--  start content-table-inner -->
		<div id="content-table-inner">
		<table border="0" width="100%" cellpadding="0" cellspacing="0">
		<tr valign="top">
			<td>
				<form name="frmPre" id="frmPre" method="post" action="organizations.php" enctype="multipart/form-data" >
			<table border="0" cellpadding="0" cellspacing="0"  id="id-form">
			<tr>
				<th valign="top"><img src="logo/<?php echo $ImgName; ?>" alt="" /></th>
				<td>
				<h3><?php // echo $auRec[0]->company_name; ?></h3><br/>
				You are an
				<?php  if($_SESSION['userLoginType']==1) {  echo '<b>Admin</b>'; }?>
				<?php  if($_SESSION['userLoginType']==2) {  echo '<b>Manage User</b>'; }?>
				<?php  if($_SESSION['userLoginType']==3) {  echo '<b>Distributor</b>'; }?>
				<?php  if($_SESSION['userLoginType']==4) {  echo '<b>Retailer</b>'; }?>
				<?php  if($_SESSION['userLoginType']==5) {  echo '<b>Salesman</b>'; }?>
				in this organization<br/><br/>
				<i><font color="grey">Organization created on: <?php echo date('d M Y', strtotime($auRec[0]->start_date)); ?></font></i>
				</td>
			</tr>
			<?php  if($_SESSION['userLoginType']==1) { ?>
			<tr>
				<th><input type="button" value="Change Organization Logo" class="form-reset" onclick="location.href='organizations.php?logo=logo';" /></th>
				<?php
				if($_GET['logo']=='logo'){
				?>
				<td><input type="file" name="image_file" id="file_1" class="required" ></td>
				<td>
					<input name="add" type="hidden" value="yes" />
					<input name="account_id" type="hidden" value="<?php echo $auRec[0]->account_id; ?>" />
					<input name="image_name" type="hidden" value="<?php echo $auRec[0]->logo; ?>" />
					<input name="submit" class="form-submit" type="submit" id="submit" value="Save" />
					<input type="button" value="Cancel" class="form-reset" onclick="location.href='organizations.php';" />
				</td>
				<?php } ?>
			</tr>
			<?php } ?>
			</table>
			</form>
			</td>
		</tr>
		</table>	
		<div class="clear"></div>
		</div>
		</td>
	</tr>
	<!-- end related-act-bottom -->
	</table>
	<div class="clear"></div>
	</div>
	<!--  end content-table-inner  -->
<div class="clear">&nbsp;</div>
</div>
<!--  end content -->
<!--  end content-outer -->
<!-- start footer --> 
<?php include("footer.php") ?>  
<!-- end footer -->
</body>
</html>