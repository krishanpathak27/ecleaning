<?php
include("includes/config.inc.php");
include("includes/function.php");
$page_name="Planned Visited Town Detail";
$_objAdmin = new Admin();
$_arrList = new ArrayList();

	if(isset($_REQUEST['id'])){
 		 	$where.="sd.salesman_id =".$_REQUEST['id']." AND "; 
 			$visitedWhere.="a.salesman_id =".$_REQUEST['id']." AND ";
 		}
 		
 		if($_SESSION['PVsVMonth']!=''){
			$month=$_SESSION['PVsVMonth'];
		}
		if($_SESSION['PVsVYear']!=''){
		    $year=$_SESSION['PVsVYear'];
		}

		$visitedWhere.= 'a.activity_type = 3';
	    $visitedWhere.=" AND MONTH(a.activity_date) =".$month." AND YEAR(a.activity_date) =".$year;
		$visitedWhere.=" group by a.salesman_id,o.retailer_id,day(a.activity_date)";		

		if ($query) $where.=" AND $qtype LIKE '%$query%'";

		$visited = $_objAdmin->_getSelectList2('table_activity AS a 
			LEFT JOIN table_order AS o ON a.ref_id = o.order_id 
			LEFT JOIN table_route_retailer AS rr ON rr.retailer_id = o.retailer_id
			LEFT JOIN table_route AS ro ON ro.route_id = rr.route_id
			LEFT JOIN table_salesman AS s ON s.salesman_id = o.salesman_id',
			"DISTINCT(ro.route_id), ro.route_name,s.salesman_name,  day(a.activity_date) AS visited_date, a.start_time, a.end_time",'',$visitedWhere,'');

		

			foreach ($visited as $key=>$value) :
				$visitedTowns[$value->visited_date][] = $value;
			endforeach;
			//echo "<pre>";
		//print_r($visitedTowns);
		$where.=" sd.month =".$month." AND sd.year =".$year;
		$where.=" GROUP BY d.route_id, d.assign_day";		

		$Planned = $_objAdmin->_getSelectList2('table_route_scheduled as sd left join table_route_schedule_details as d on sd.route_schedule_id=d.route_schedule_id  left join table_route as r on r.route_id=d.route_id LEFT JOIN table_salesman AS s ON s.salesman_id = sd.salesman_id',"sd.salesman_id,r.route_id,r.route_name, s.salesman_name, sd.month, sd.year, d.assign_day AS planned_date",$rp,$where.$sort,'');

		$filteredPlanned = array();

		foreach ($Planned as $key=>$value) :
			$filteredPlanned[$value->planned_date][] = $value;
		endforeach;
	$no_of_days = cal_days_in_month(CAL_GREGORIAN, $_SESSION['PVsVMonth'], $_SESSION['PVsVYear']); 
 
    for($i=1; $i<=$no_of_days; $i++){
    	$daysArray[] = $i;
    }


    foreach ($daysArray as $key=>$value) :
			$finalData[$value][] = $filteredPlanned[$value];
		    $finalData[$value][] = $visitedTowns[$value];

	endforeach;
	//echo "<pre>";
	//print_r($finalData);
	
?>
<?php include("header.inc.php") ?>
<script type="text/javascript">
// Popup window code
function newPopup(url) {
	popupWindow = window.open(
		url,'popUpWindow','height=500,width=800,left=100,top=100,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no,status=yes')
}

    function PrintElem(elem)
    {
        Popup($(elem).html());
    }

    function Popup(data) 
    {
		
        var mywindow = window.open('', 'Report');
		
        mywindow.document.write('<html><head><title>Planned Vs Visited Town</title>');
		mywindow.document.write('<table><tr><td><b>Salesman Name:</b> <?php echo $sal_name; ?></td><td><b>From Date:</b> <?php echo $_SESSION['FromAttList']; ?></td><td><b>To Date:</b> <?php echo $_SESSION['ToAttList']; ?></td></tr></table>');
        /*optional stylesheet*/ //mywindow.document.write('<link rel="stylesheet" href="main.css" type="text/css" />');
        mywindow.document.write('</head><body >');
        mywindow.document.write(data);
        mywindow.document.write('</body></html>');

        mywindow.print();
        mywindow.close();
        return true;
    }
$(document).ready(function(){
<?php if($_POST['submit']=='Export to Excel'){ ?>
tableToExcel('report_export', 'Planned Vs Actual Report', 'Planned vs acc.xls');
<?php } ?>
});

</script>

<!-- start content-outer -->
<div id="content-outer">
<!-- start content -->
<div id="content">
<div id="page-heading"><h1><span style="color: #d74343; font-family: Tahoma; font-weight: bold;">Planned Vs Visited Route</span></h1></div>
<table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">

<tr>
	<td>
	<!--  start content-table-inner -->
	<div id="content-table-inner">
	
	<div id="page-heading" align="left" >


<style type="text/css">
tr.heading { font-weight: bold; background-color: #ededed; margin:10px;  }	
td.columns { padding:10px 0px 10px 0px; margin:10px; 
		font-family: monospace; text-align: center;}
</style>

<table border="1" width="100%" cellpadding="0" cellspacing="0"   >
<?php if(is_array($filteredPlanned) && sizeof($filteredPlanned)>0){?>
<tr>
	<td>

	<form name="frmPre" id="frmPre" method="post" action="" enctype="multipart/form-data" >
		<table border="1" width="100%" cellpadding="0" cellspacing="0">
		<tr>
		<td colspan="5"><input name="showReport" type="hidden" value="yes" />
		<a id="dlink"  style="display:none;"></a>
		<input type="submit" name="submit" value="Export to Excel" class="result-submit"  >
		</td>
		<td></td>
	</tr>
		<tr class="heading">
			<td class="columns">Salesman Name</td>
			<td class="columns">Month</td>
			<td class="columns">Year</td>
		</tr>
		<tr>
			<td class="columns"><?php echo $Planned[0]->salesman_name; ?></td>
			<td class="columns">
				<?php 
						foreach($_arrList->ARR_MONTHS as $key=>$value)
							{
								if($key == $_SESSION['PVsVMonth']) echo $selectedMonth = $value;
							}	
								
				?>
			</td>
			<td colspan="" class="columns"><?php echo $selectedYear = $_SESSION['PVsVYear'] ;?></td>
		</tr>
		</table>
		</form>
	</td>
</tr>


<tr>
	<td>
		<table border="1" width="100%" cellpadding="0" cellspacing="0" id="report_export" name="report_export">
		<tr class="heading">
			<td class="columns"> Date</td>
			<td class="columns" width="90"> Visited Route</td>
			<td class="columns"width="100" > Planned Route</td>
			<td class="columns"> In Time</td>
			<td class="columns" width="90"> In Address</td>
			<td class="columns"> Out Time</td>
			<td class="columns" width="90">Out Address</td>
			<td class="columns"> View On Map</td>
		</tr>

<?php if(isset($finalData) && sizeof($finalData)>0) {
	 foreach ($finalData as $key => $value) {
	 	unset($visitedtowns);
	 	foreach ($value[1] as $vkey => $vvalue) {  $visitedtowns[] = $vvalue->route_name; } 

			$visitedtwn = implode(', ', array_filter($visitedtowns));

			if(isset($visitedtwn) && !empty($visitedtwn))  $visited = $visitedtwn; else $visited = '-'; 

			unset($plannedtowns);

			foreach ($value[0] as $pkey => $pvalue) { $plannedtowns[] = $pvalue->route_name; }

			$plannedtwn = implode(', ', $plannedtowns);

			if(isset($plannedtwn) && !empty($plannedtwn))  $planned = $plannedtwn; else $planned = '-';


			/*  Fetch start attendance and ends attendance */
			if($key<10){ $day='0'.$key;} else{ $day=$key;}
			$workingday=$_SESSION['PVsVYear'].'-'.$_SESSION['PVsVMonth'].'-'.$day; 
			$condd=" activity_date='".$workingday."' ";

			$salesmanList = "and i.salesman_id='".$_REQUEST['id']."' ";
			$auAttSt=$_objAdmin->_getSelectList2('sal_id as i left join act_in as n on i.salesman_id=n.salesman_id and i.activity_date=n.activity_date left join act_out as o on i.salesman_id=o.salesman_id and i.activity_date=o.activity_date left join table_salesman as s on s.salesman_id=i.salesman_id',"s.salesman_name,s.salesman_id,n.attendance_start,o.attendance_end,i.activity_date,n.in_address,o.out_address,n.start_let,o.end_let,n.activity_id as in_activity,o.activity_id as out_activity",''," (i.activity_date BETWEEN '".$workingday."' AND '".$workingday."') ".$salesmanList."  order by s.salesman_name asc, i.activity_date asc");

            if(isset($auAttSt) && sizeof($auAttSt)>0) {

				 $Morning 	 =$auAttSt[0]->attendance_start;
				 $InAddress  =$auAttSt[0]->in_address;
				 $Evening    =$auAttSt[0]->attendance_end;
				 $OutAddress =$auAttSt[0]->out_address;
				 $MapViewdata=$auAttSt[0]->in_activity;

			 } else {

				 $Morning 	   ='-';
				 $InAddress    ='-';
				 $Evening      ='-';
				 $OutAddress   ='-';
				 $MapViewdata  ='-';

			 }
 ?>

<tr <?php 	if($planned !== '-' || $visited !== '-'){
	$str1 = explode(',', $planned);
	$str2 = explode(',', $visited);
	$diff1 = array_diff($str1, $str2); 
	$diff2 = array_diff($str2, $str1);
	$Diff = array_merge($diff1, $diff2);
	if(sizeof($Diff)>0) echo 'bgcolor="#CCEEFF"'; }?> >

	<td class="columns">&nbsp;<?php echo $key."-".$selectedMonth."-".$selectedYear; ?></td>
	<td class="columns">&nbsp;<?php echo $visited; ?></td>
	<td class="columns">&nbsp;<?php echo $planned; ?></td>
	<td class="columns">&nbsp;<?php echo $Morning; ?></td>
	<td class="columns">&nbsp;<?php echo $InAddress; ?></td>
	<td class="columns">&nbsp;<?php echo $Evening; ?></td>
	<td class="columns">&nbsp;<?php echo $OutAddress; ?></td>
	<td style="columns" align="center"><?php if($MapViewdata>0){?><a href="JavaScript:newPopup('activity_map.php?id=<?php echo base64_encode($MapViewdata);?>');"> View on Map</a><?php } else { echo $MapViewdata; }?></td>
	


	</tr>

	<?php } // End for each loop ?>
	<?php } // End if condition ?>
	</table>
	</td>
</tr>


<?php }  else { ?>
<tr><td class="columns">No data found</td></tr>
<?php } ?>



<tr><td><input type="button" name="back" value="Close" class="form-cen" onclick="javascript:window.close();"></td></tr>
</table>
		<div class="clear"></div>
	</div>
<!--  end content-table-inner  -->
</td>
<td id="tbl-border-right"></td>
</tr>
</table>
<div class="clear">&nbsp;</div>
</div>
<!--  end content -->
<div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->
<div class="clear">&nbsp;</div> 
<!-- start footer -->         
<?php include("footer.php") ?>

<script type='text/javascript'>//<![CDATA[ 
var tableToExcel = (function () {
        var uri = 'data:application/vnd.ms-excel;base64,'
        , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table><tr><td><b>Salesman Name:</b> <?php echo $Planned[0]->salesman_name; ?></td><td><b>Month:</b> <?php foreach($_arrList->ARR_MONTHS as $key=>$value){
  										if($key == $_SESSION['PVsVMonth']) echo $selectedMonth = $value;
									  }	 ?></td><td><b>year:</b> <?php echo $_SESSION['PVsVYear']; ?></td></tr></table><table>{table}</table></body></html>'
        , base64 = function (s) { return window.btoa(unescape(encodeURIComponent(s))) }
        , format = function (s, c) { return s.replace(/{(\w+)}/g, function (m, p) { return c[p]; }) }
        return function (table, name, filename) {
            if (!table.nodeType) table = document.getElementById(table)
            var ctx = { worksheet: name || 'Worksheet', table: table.innerHTML }

            document.getElementById("dlink").href = uri + base64(format(template, ctx));
            document.getElementById("dlink").download = filename;
            document.getElementById("dlink").click();

        }
    })()

//]]>  
</script>



<!-- end footer -->
</body>
</html>
