<?php

	include("includes/config.inc.php");
	include("includes/function.php");
	include("includes/globalarraylist.php");

	/******* Check salesman ID exists into session or not. ******************/

	if(isset($_SESSION['showPlannedVsVisited']) && $_SESSION['salesman']!='')
	{
	
			$salArrList=$_SESSION['salesman'];	
			$filterby=$_SESSION['orderfilterBY'];
			$salesman = $_objArrayList->getSalesmanConsolidatedList($salArrList,$filterby);
			
	} 

	$page_name="Salesman Planned Report";

	if(isset($_POST['showPlannedVsVisited']) && $_POST['showPlannedVsVisited'] == 'yes'){	
		if($_POST['sal']!="All"){
			 $_SESSION['salesman']=$_POST['sal'];	
		} 
	
		if($_POST['fromDate']!=""){
		 	$_SESSION['fromDate']=$_POST['fromDate'];	
		}
		if($_POST['toDate']!=""){
	        $_SESSION['toDate']=$_POST['toDate'];	
		}
		if($_POST['sal']=="All"){
	  	 	unset($_SESSION['salesman']);
	   }
	   if($_POST['state_id']!="" && $_POST['state_id']!='All')
	   	{
		$_SESSION['StateList']=$_POST['state_id'];
		 //$state= " and s.state=".$_SESSION['StateList'];	
		}
	   if($_POST['state_id']=='All')
	   {
	   	unset($_SESSION['StateList']);
	   }	
	    
	}
	//echo $_SESSION['StateList'];

	if(isset($_REQUEST['reset']) && $_REQUEST['reset'] == 'yes'){
		unset($_SESSION['salesman']);
		unset($_SESSION['StateList']);	
	 	
		$_SESSION['fromDate']  = date('Y-m-d');
		$_SESSION['toDate'] = date('Y-m-d');
		header("Location:productivity_report.php");
	}


	if($_SESSION['fromDate']==''){
		$_SESSION['fromDate'] = date('Y-m-d');
	}

	if($_SESSION['toDate']==''){
		$_SESSION['toDate']= date('Y-m-d');
	}
	
	if(isset($_REQUEST['show']) && $_REQUEST['show']=="yes"){
		$_objAdmin->showSalesmanProductivityReport($salesman);
		die;
	} 

	if(isset($_POST['export']) && $_POST['export']!="")
{

$date_new=$_SESSION['SalReportDate'];
//$val="sal=".$_REQUEST['sal']."&date=".$date_new;
$val="sal=".$_POST['sal']."&year=".$_POST['year']."&month=".$_POST['month'];
header('Location:export.inc.php?export_productivity_report&'.$val);
exit;
}	

?>
<?php include("header.inc.php") ?>
<script src="javascripts/dateNextPrev.js" type="text/javascript"></script>
<!-- start content-outer -->
<input name="pagename" type="hidden"  id="pagename" value="plannedvsvisited_report.php" />
<div id="content-outer">
<!-- start content -->
<div id="content">
<div id="page-heading"><h1><span style="color: #d74343; font-family: Tahoma; font-weight: bold;">Productivity Report</span></h1></div>
<table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">

<tr>
	<td>
	<!--  start content-table-inner -->
	<div id="content-table-inner">
	
	<div id="page-heading" align="left" >
	
	<form name="frmPre" id="frmPre" method="post" action="" enctype="multipart/form-data" >
	<table border="0" width="85%" cellpadding="0" cellspacing="0">
	<tr>	
	  	<td><h3>&nbsp;&nbsp;Salesman:</h3><h6>
	  		<select name="sal" id="sal" class="styledselect_form_5" style="" >
				<?php echo $rtndata = $_objArrayList->GetSalesmenMenu($salsList, $_SESSION['salesman'], 'flex');?>
			</select></h6>
		</td>
		 <!-- <td><h3>State: </h3> <h6> 
		  <select name="state_id" id="state_id" class="menulist">
		  <option value="All">All</option>
			<?php $stateList=$_objAdmin->_getSelectList2('state','state_id,state_name',''," state_name!='' ORDER BY state_name"); 
			if(is_array($stateList)){
			for($i=0;$i<count($stateList);$i++){
			?>
			<option value="<?php echo $stateList[$i]->state_id;?>" <?php if ($stateList[$i]->state_id==$_SESSION['StateList']){ ?> selected <?php } ?>><?php echo $stateList[$i]->state_name;?></option>
			<?php }}?>
			</select></h6></td>	 -->
	
		<td>
			<h3>From Date: </h3><h6>			
				<input type="text" id="fromDate" name="fromDate" class="date" style="width:130px" value="<?php if($_SESSION['fromDate']!='') { echo $_objAdmin->_changeDate($_SESSION['fromDate']); } else { echo $_objAdmin->_changeDate(date('Y-m-d')); }?>"  readonly /> 
			</h6>
		</td>
        <td>
		<h3>To Date: </h3><h6>
			<input type="text" id="toDate" name="toDate" class="date" style="width:130px" value="<?php if($_SESSION['toDate']!='') { echo $_objAdmin->_changeDate($_SESSION['toDate']); } else { echo $_objAdmin->_changeDate(date('Y-m-d')); }?>"  readonly /> 
		</h6>
        </td>
		<td><h3>&nbsp; </h3>
	  		<input name="showPlannedVsVisited" type="hidden" value="yes" />
	  		<input name="submit" class="result-submit" type="submit" id="submit" value="View Details" />
	  		<input name="export" class="result-submit" type="submit" id="export"  value="Export to Excel">
	  	</td>
	  <td><h3>&nbsp; </h3><input type="button" value="Reset!" class="form-reset" onclick="location.href='productivity_report.php
	  	.php?reset=yes';" />	</td>	
	  
	</tr>
	</table>
	</form>
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
		<tr valign="top">
			<td>
				<!-- start id-form -->
				<table  border="0" cellspacing="0" cellpadding="0"  align="center" width="100%">
					<tr>
						<td align="lift" valign="top"  >
							<table id="flex1" style="display:none"></table>
								<script type="text/javascript">showSalesmanProductivityReport();</script>
						</td>
					</tr>
				</table>
			<!-- end id-form  -->
			</td>
		</tr>
		<tr>
			<td><img src="images/blank.gif" width="695" height="1" alt="blank" /></td>
			<td></td>
		</tr>
	</table>
		<div class="clear"></div>
	</div>
<!--  end content-table-inner  -->
</td>
<td id="tbl-border-right"></td>
</tr>
</table>
<div class="clear">&nbsp;</div>
</div>
<!--  end content -->
<div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->
<div class="clear">&nbsp;</div> 
<!-- start footer -->         
<?php include("footer.php") ?>
<!-- end footer -->
<script src="javascripts/jquery-1.8.2.js" type="text/javascript"></script>
<script src="javascripts/jquery-ui.js"></script>
<script type="text/javascript" src="javascripts/validate.js"></script>
</body>
</html>
<style>
#content-table-inner
{

margin-left: -7%;
}
</style>
<script>
    $(function() {
        $( "#fromDate" ).datepicker({
			dateFormat: "d M yy",
            defaultDate: "w",
            changeMonth: true,
            numberOfMonths: 1,
            onSelect: function( selectedDate ) {
                $( "#fromDate" ).datepicker( "option", "minDate", selectedDate );
            }
        });
        $( "#toDate" ).datepicker({
			dateFormat: "d M yy",
            defaultDate: "-w",
            changeMonth: true,
            numberOfMonths: 1,
            onSelect: function( selectedDate ) {
                $( "#toDate" ).datepicker( "option", "maxDate", selectedDate );
            }
        });
    });
</script>