<?php
include("includes/config.inc.php");
include("includes/function.php");
include("includes/globalarraylist.php");


$jan = 0; $feb = 0; $march = 0; $april = 0; $may = 0; $june = 0; $july = 0; $aug = 0; $sep = 0; $oct = 0; $nov = 0; $dec = 0;
//echo $_POST['category']." ".$_POST['m']." ".$_POST['y'];
if($_REQUEST['y']!='') 
{	
	$fromdate="'".$_REQUEST['y']."-1-1"."'";	$todate="'".$_REQUEST['y']."-12-31"."'"; 
}

if($_REQUEST['catID']!='' && $_REQUEST['y'] != '')
{
	$graph=$_objAdmin->_getSelectList('table_order AS T LEFT JOIN table_order_detail AS o ON T.order_id = o.order_id LEFT JOIN table_item ON table_item.item_id = o.item_id LEFT JOIN table_category ON table_category.category_id = table_item.category_id ',' sum(o.quantity) as quantity, monthname(date_of_order) as month ',''," o.type=1 and table_category.category_id = ".$_REQUEST['catID']." AND date_of_order  >= $fromdate AND date_of_order <= $todate group by monthname(date_of_order)");
} 
//print_r($graph);
for($j=0; $j<count($graph); $j++)
{
	if($graph[$j]->month == 'January')	{ $jan += $graph[$j]->quantity;		}
	if($graph[$j]->month == 'February')	{ $feb += $graph[$j]->quantity;		}
	if($graph[$j]->month == 'March')	{ $march += $graph[$j]->quantity;		}
	if($graph[$j]->month == 'April')	{ $april += $graph[$j]->quantity;		}
	if($graph[$j]->month == 'May')		{ $may += $graph[$j]->quantity;		}
	if($graph[$j]->month == 'June')		{ $june += $graph[$j]->quantity;		}
	if($graph[$j]->month == 'July')		{ $july += $graph[$j]->quantity;		}
	if($graph[$j]->month == 'August')	{ $aug += $graph[$j]->quantity;		}
	if($graph[$j]->month == 'September'){ $sep += $graph[$j]->quantity;		}
	if($graph[$j]->month == 'October')	{ $oct += $graph[$j]->quantity;		}
	if($graph[$j]->month == 'November')	{ $nov += $graph[$j]->quantity;		}
	if($graph[$j]->month == 'December')	{ $dec += $graph[$j]->quantity;		}
}
//echo $aug;

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta charset="utf-8">
<title>Quytech PepUpSales - Sale of Category Items Report Graph</title>
<?php include_once('graph/header-files.php');?>
    <script type="text/javascript" src="javascripts/jsapi"></script>
    <script type="text/javascript">
      google.load('visualization', '1', {packages: ['corechart']});
	 
    </script>
    <script type="text/javascript">
      function drawVisualization() {
        // Some raw data (not necessarily accurate)
        var data = google.visualization.arrayToDataTable([
           ['Month', 'Quantity' ],
          ['January',  <?php echo $jan; ?>],
          ['February',  <?php echo $feb; ?>],
          ['March',  <?php echo $march; ?>],
          ['April',  <?php echo $april; ?>],
		  ['May',  <?php echo $may; ?>],
          ['June',  <?php echo $june; ?>],
          ['July',  <?php echo $july; ?>],
          ['August',  <?php echo $aug; ?>],
		  ['September',  <?php echo $sep; ?>],
		  ['October',  <?php echo $oct; ?>],
		  ['November',  <?php echo $nov; ?>],
		  ['December',  <?php echo $dec; ?>]
        ]);

        var options = {
          title : 'Total number of item sales in a category',
          vAxis: {title: "Total item sales" , gridlines: { count: 8  }, minValue:0},
          hAxis: {title: "<?php echo $_REQUEST['y']; ?>"},
          seriesType: "bars",
          series: {5: {type: "line"}}
        };

        var chart = new google.visualization.ComboChart(document.getElementById('chart_div'));
        chart.draw(data, options);
      }
      google.setOnLoadCallback(drawVisualization);
    </script>
</head>
<body> 
<!-- Start: page-top-outer -->

<?php include_once('graph/header.php');?>
<!-- End: page-top-outer -->
	
<div id="content-outer">
<div id="content">
<div id="page-heading"><h1>Sale of Category Items Report Graph</h1></div>
<div id="container">
<form name="form_graph" id="form_graph" method="post" action="productwise_report_graph.php" enctype="multipart/form-data" >
<table width="100%" border="0">
  <tr>
  
	<td width="11%" valign="bottom"><h3>Category :</h3></td>
    <td width="15%" align="left">
	<select name="catID" id="catID" class="styledselect_form_5" >
		<option value="">Please Select</option>
		<?php $aCategory=$_objAdmin->_getSelectList('table_category','category_id, category_name',''," ORDER BY category_name"); 
			if(is_array($aCategory)){
			for($i=0;$i<count($aCategory);$i++){?>
					<option value="<?php echo $aCategory[$i]->category_id;?>" <?php if($aCategory[$i]->category_id==$_REQUEST['catID']) echo "selected";?> ><?php echo $aCategory[$i]->category_name;?></option>
			<?php }}?>
	</select>
	</td>
  
  
  	<td width="6%" valign="bottom"><h3>Year :</h3></td>
    <td width="17%" align="left">
	<select name="y" id="y"  class="styledselect_form_5" >
		<?php echo $year = $_objArrayList->getYearList2($_REQUEST['y']);?>
	</select>
	</td>
	
	<td width="51%" valign="bottom"><input type='submit' name='frm-submit' id='frm-submit' value='Show Graph'  class="result-submit" ></td>
  </tr>
  
  
  
  
  <tr>
    <td colspan="5"><div id="chart_div" style="width: 100%; height: 450px;"></div></td>
  </tr>
</table>
</form>
  
	<div class="clear">&nbsp;</div>
	
</div>
</div>
</div>
	<!-- Graph code ends here -->
	<?php include("footer.php") ?>
	
  </body>
</html>