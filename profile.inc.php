<?php
$condi=	" account_id='".$_SESSION['accountId']."' and web_user_id='".$_SESSION['PepUpSalesUserId']."'";
$emailRec=$_objAdmin->_getSelectList('table_web_users',"email_id",'',$condi);
if(is_array($emailRec)){
$email_id=$emailRec[0]->email_id;
}
//admin condition
if($_SESSION['userLoginType']==1)  { 
	$condi=	" u.account_id='".$_SESSION['accountId']."' and u.web_user_id='".$_SESSION['PepUpSalesUserId']."'";
	$auRec=$_objAdmin->_getSelectList('table_account as a left join table_web_users as u on a.account_id=u.account_id left join state as s on a.state=s.state_id left join city as c on a.city=c.city_id left join table_area as ar on a.area=ar.area_id',"a.*,u.email_id,s.state_name,s.state_id,c.city_name,c.city_id,ar.area_name",'',$condi);
	if(is_array($auRec)){
		$id=$auRec[0]->account_id;
		$name=$auRec[0]->company_name;
		$email=$auRec[0]->email_id;
		$address=$auRec[0]->company_address;
		$phone_no=$auRec[0]->company_phone_no;
		$country=$auRec[0]->country;
		$state=$auRec[0]->state_name;
		$city=$auRec[0]->city_name;
		$state_id=$auRec[0]->state_id;
		$city_id=$auRec[0]->city_id;
		$zipcode=$auRec[0]->zipcode;
                $areaId=$auRec[0]->area;
                $area=$auRec[0]->area_name;
		}
	}

//manage user condition
if($_SESSION['userLoginType']==2)  { 
	$condi=	" a.account_id='".$_SESSION['accountId']."' and u.web_user_id='".$_SESSION['PepUpSalesUserId']."'";
	$auRec=$_objAdmin->_getSelectList('table_account_admin as a inner join table_web_users as u on a.operator_id=u.operator_id',"a.*,u.email_id",'',$condi);
	if(is_array($auRec)){
		$id=$auRec[0]->operator_id;
		$name=$auRec[0]->operator_name;
		$email=$auRec[0]->email_id;
		$phone_no=$auRec[0]->operator_phone_number;
		}
	}	

//distributor condition
if($_SESSION['userLoginType']==3 ) { 
	$condi=	" u.account_id='".$_SESSION['accountId']."' and u.web_user_id='".$_SESSION['PepUpSalesUserId']."'";
	$auRec=$_objAdmin->_getSelectList('table_distributors as d left join table_web_users as u on d.distributor_id=u.distributor_id left join state as s on d.state=s.state_id left join city as c on d.city=c.city_id',"d.*,u.email_id,s.state_name,s.state_id,c.city_name,c.city_id",'',$condi);
	if(is_array($auRec)){
		$id=$auRec[0]->distributor_id;
		$name=$auRec[0]->distributor_name;
		$email=$auRec[0]->email_id;
		$address=$auRec[0]->distributor_address;
		$phone_no=$auRec[0]->distributor_phone_no;
		$contact_person=$auRec[0]->contact_person;
		$contact_number=$auRec[0]->contact_number;
		$country=$auRec[0]->country;
		$state=$auRec[0]->state_name;
		$city=$auRec[0]->city_name;
		$state_id=$auRec[0]->state_id;
		$city_id=$auRec[0]->city_id;
		$zipcode=$auRec[0]->zipcode;
		}
	}
	
//retailer condition
if($_SESSION['userLoginType']==4 ) { 
	$condi=	" u.account_id='".$_SESSION['accountId']."' and u.web_user_id='".$_SESSION['PepUpSalesUserId']."'";
	$auRec=$_objAdmin->_getSelectList('table_retailer as r left join table_web_users as u on r.retailer_id=u.retailer_id left join state as s on r.state=s.state_id left join city as c on r.city=c.city_id',"r.*,u.email_id,s.state_name,s.state_id,c.city_name,c.city_id",'',$condi);
	if(is_array($auRec)){
		$id=$auRec[0]->retailer_id;
		$name=$auRec[0]->retailer_name;
		$email=$auRec[0]->email_id;
		$address=$auRec[0]->retailer_address;
		$phone_no=$auRec[0]->retailer_phone_no;
		$contact_person=$auRec[0]->contact_person;
		$contact_number=$auRec[0]->contact_number;
		$country=$auRec[0]->country;
		$state=$auRec[0]->state_name;
		$city=$auRec[0]->city_name;
		$state_id=$auRec[0]->state_id;
		$city_id=$auRec[0]->city_id;
		$zipcode=$auRec[0]->zipcode;
		}
	}
	
//salesman condition
if($_SESSION['userLoginType']==5 ) { 
	$condi=	" u.account_id='".$_SESSION['accountId']."' and u.web_user_id='".$_SESSION['PepUpSalesUserId']."'";
	$auRec=$_objAdmin->_getSelectList('table_supervisor as s left join table_web_users as u on s.supervisor_id=u.supervisor_id',"s.*,u.email_id",'',$condi);
	if(is_array($auRec)){
		$id=$auRec[0]->supervisor_id;
		$name=$auRec[0]->supervisor_name;
		$email=$auRec[0]->email_id;
		$address=$auRec[0]->supervisor_address;
		$phone_no=$auRec[0]->supervisor_phone_no;
		}
	}


/*************************************************
* DESC: Warehouse Profile Details
* Author: AJAY
* Created: 2017-02-23
* User Type: Warehouse - 7
*
**************************************************/



if($_SESSION['userLoginType']==8 ) { 
	$condi=	" u.account_id='".$_SESSION['accountId']."' and u.web_user_id='".$_SESSION['PepUpSalesUserId']."'";
	$auRec=$_objAdmin->_getSelectList('table_service_personnel as s left join table_web_users as u on s.service_personnel_id=u.service_personnel_id left join state as st on s.state_id=st.state_id left join city as c on s.city_id=c.city_id',"s.*,u.email_id,st.state_name,st.state_id,c.city_name,c.city_id",'',$condi);
	if(is_array($auRec)){
		$id=$auRec[0]->service_personnel_id;
		$name=$auRec[0]->sp_name;
		$email=$auRec[0]->email_id;
		$address=$auRec[0]->sp_address;
		$phone_no=$auRec[0]->sp_phone_no;
		$country=$auRec[0]->country;
		$state=$auRec[0]->state_name;
		$city=$auRec[0]->city_name;
		$state_id=$auRec[0]->state_id;
		$city_id=$auRec[0]->city_id;
		}
	}







if($_SESSION['userLoginType']==7 ) { 
	$condi=	" u.account_id='".$_SESSION['accountId']."' and u.web_user_id='".$_SESSION['PepUpSalesUserId']."'";
	$auRec=$_objAdmin->_getSelectList('table_warehouse as s left join table_web_users as u on s.warehouse_id=u.warehouse_id left join state as st on s.state_id=st.state_id left join city as c on s.city_id=c.city_id',"s.*,u.email_id,st.state_name,st.state_id,c.city_name,c.city_id",'',$condi);
	if(is_array($auRec)){
		$id=$auRec[0]->warehouse_id;
		$name=$auRec[0]->warehouse_name;
		$email=$auRec[0]->email_id;
		$address=$auRec[0]->warehouse_address;
		$phone_no=$auRec[0]->warehouse_phone_no;
		$country=$auRec[0]->country;
		$state=$auRec[0]->state_name;
		$city=$auRec[0]->city_name;
		$state_id=$auRec[0]->state_id;
		$city_id=$auRec[0]->city_id;
		}
	}










?>