  <?php
include("includes/config.inc.php");
include("includes/function.php");
$_objAdmin = new Admin();
?>

<?php include("header.inc.php"); ?>
<div class="m-grid__item m-grid__item--fluid m-wrapper">

<!-- BEGIN: Subheader -->
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">
                    Manage Profile
                </h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="#" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__separator">
                        -
                    </li>
                    <li class="m-nav__item">
                        <a href="#" class="m-nav__link">
                            <span class="m-nav__link-text">
                                Admin
                            </span>
                        </a>
                    </li>
                    <li class="m-nav__separator">
                        -
                    </li>
                    <li class="m-nav__item">
                        <a href="" class="m-nav__link">
                            <span class="m-nav__link-text">
                                MyProfile
                            </span>
                        </a>
                    </li>  
                </ul>
            </div> 
        </div>
    </div>
    <!-- END: Subheader -->

<div class="m-content">
        <div class="row">


    <div class="col-xl-12">
    <!--begin:: Widgets/Support Tickets -->
<div class="m-portlet m-portlet--full-height ">
  <div class="m-portlet__head">
    <div class="m-portlet__head-caption">
      <div class="m-portlet__head-title">
        <h3 class="m-portlet__head-text">
          Manage Profile
        </h3>
      </div>
    </div>
    
  </div>
  <div class="m-portlet__body">
    <div class="m-widget3">
      <div class="m-widget3__item">
        <div class="m-widget3__header">
          <div class="m-widget3__user-img">              
            <i class="m-nav__link-icon flaticon-profile-1 m--font-info" style="font-size: 25px"></i> 
          </div>
          <div class="m-widget3__info">
            <span class="m-widget3__username  m--font-info">
            <a href="profile_setting.php?profile=<?php echo base64_encode(view);?>">Personal Information</a>
            </span>   
          </div> 
        </div>
        <div class="m-widget3__body">
          <p class="m-widget3__text"> 
            Manage your personal details, contact information.
          </p>
        </div>
      </div>
      <div class="m-widget3__item">
        <div class="m-widget3__header">
          <div class="m-widget3__user-img">              
            <i class="m-nav__link-icon flaticon-chat-1 m--font-info" style="font-size: 25px"></i> 
          </div>
          <div class="m-widget3__info">
            <span class="m-widget3__username  m--font-info">
            <a href="profile_setting.php?profile=<?php echo base64_encode(email);?>">Email Address</a>   
            </span>   
          </div> 
        </div>
        <div class="m-widget3__body">
          <p class="m-widget3__text"> 
             Update your email preferences of primary/secondary addresses.  If you forget your password, we'll send reset instructions to your email.
          </p>
        </div>
      </div>
      <div class="m-widget3__item">
        <div class="m-widget3__header">
          <div class="m-widget3__user-img">              
            <i class="m-nav__link-icon flaticon-cogwheel-2 m--font-info" style="font-size: 25px"></i> 
          </div>
          <div class="m-widget3__info">
            <span class="m-widget3__username  m--font-info">
              <a href="profile_setting.php?profile=<?php echo base64_encode(password);?>">
                Change Password
              </a>   
            </span>   
          </div> 
        </div>
        <div class="m-widget3__body">
          <p class="m-widget3__text"> 
           Update your account password. Always set a strong password,  which helps to prevent unauthorized access to your account.
          </p>
        </div>
      </div>
      <?php if($_SESSION['userLoginType']==1){ ?>
      <div class="m-widget3__item">
        <div class="m-widget3__header">
          <div class="m-widget3__user-img">              
            <i class="m-nav__link-icon flaticon-profile-1 m--font-info" style="font-size: 25px"></i> 
          </div>
          <div class="m-widget3__info">
            <span class="m-widget3__username  m--font-info">
             Close Account  
            </span>   
          </div> 
        </div>
        <div class="m-widget3__body">
          <p class="m-widget3__text"> 
          
           Delete permanently all the services data and account information, which cannot be restored in future.
        </div>
          </p>
        </div>
      </div>
      <?php } ?>
    </div>
  </div>
</div>
<!--end:: Widgets/Support Tickets -->  </div>
       
    </div>
</div>
</div>
</div>
<!-- start footer -->         
<?php include("footer.php") ?>
