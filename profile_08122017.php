<?php
include("includes/config.inc.php");
include("includes/function.php");
$_objAdmin = new Admin();
?>

<?php include("header.inc.php"); ?>
<div class="clear"></div>
<!-- start content-outer -->
<div id="content-outer">
<!-- start content -->
<div id="content">
<div id="page-heading"><h1><span style="color: #d74343; font-family: Tahoma; font-weight: bold;">Manage Profile</span></h1></div>
<table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">
<!--<tr>
	<th rowspan="3" class="sized"><img src="images/side_shadowleft.jpg" width="20" height="300" alt="" /></th>
	<th class="topleft"></th>
	<td id="tbl-border-top">&nbsp;</td>
	<th class="topright"></th>
	<th rowspan="3" class="sized"><img src="images/side_shadowright.jpg" width="20" height="300" alt="" /></th>
</tr>
-->
<tr>
	<!--<td id="tbl-border-left"></td>-->
	<td>
	<!--  start content-table-inner -->
	<div id="content-table-inner">
	
	<table border="0" width="100%" cellpadding="0" cellspacing="0" >
	<tr valign="top">
	<td>
	
	<div id="related-act-body" >
				<div class="left"><a href=""><img src="images/profile.jpg" width="21" height="21" alt="" /></a></div>
				<div class="right">
					<a href="profile_setting.php?profile=<?php echo base64_encode(view);?>"><h5>Personal Information</h5></a>
					Manage your personal details, contact information.
				</div>
				
				<div class="clear"></div>
				<div class="lines-dotted-short"></div>
				
				<div class="left"><img src="images/email_change.jpg" width="21" height="21" alt="" /></a></div>
				<div class="right">
					<a href="profile_setting.php?profile=<?php echo base64_encode(email);?>"><h5>Email Address</h5></a>
					Update your email preferences of primary/secondary addresses. 
					If you forget your password, we'll send reset instructions to your email.
				</div>
				
				<div class="clear"></div>
				<div class="lines-dotted-short"></div>
				
				<div class="left"><img src="images/change_passowrd.jpg" width="21" height="21" alt="" /></a></div>
				<div class="right">
					<a href="profile_setting.php?profile=<?php echo base64_encode(password);?>"><h5>Change Password</h5></a>
					Update your account password. Always set a strong password, 
					which helps to prevent unauthorized access to your account.
				</div>
				<div class="clear"></div>
				<div class="lines-dotted-short"></div>
				
				<div class="left"><a href=""><img src="images/close_account.jpg" width="21" height="21" alt="" /></a></div>
				<div class="right">
					<h5>Close Account</h5>
					Delete permanently all the services data and account information, 
					which cannot be restored in future.
				</div>
				<div class="clear"></div>
				
			</div>
			<!-- end related-act-inner -->
			<div class="clear"></div>

	</td>
	<td>

	<!-- right bar-->
	<?php include("rightbar/profile_setting_bar.php") ?>
</td>
</tr>
<tr>
<td><img src="images/blank.gif" width="695" height="1" alt="blank" /></td>
<td></td>
</tr>
</table>
 
<div class="clear"></div>
 
</div>
<!--  end content-table-inner  -->
</td>
<td id="tbl-border-right"></td>
</tr>
<!--<tr>
	<th class="sized bottomleft"></th>
	<td id="tbl-border-bottom">&nbsp;</td>
	<th class="sized bottomright"></th>
</tr>-->
</table>
<div class="clear">&nbsp;</div>

</div>
<!--  end content -->
<div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->
<div class="clear">&nbsp;</div>
    
<!-- start footer -->         
<?php include("footer.php"); ?>
<!-- end footer -->
 
</body>
</html>