<!-- start content-outer -->
<div id="content-outer">
<!-- start content -->
<div id="content">
<div id="page-heading"><h1>Profile Setting</h1></div>
<table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">

<tr>
	<td>
	<table border="0" width="100%" cellpadding="0" cellspacing="0" >
	
	<tr valign="top">
	<td>
	<!--  start content-table-inner -->
	<div id="content-table-inner">
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
		<tr valign="top">
			<td>
		<?php if($pass_sus!=''){?>
		<!--  start message-green -->
		<div id="message-green">
		<table border="0" width="100%" cellpadding="0" cellspacing="0">
		<tr>
		<td class="green-left"><?php echo $pass_sus; ?></td>
		<td class="green-right"><a class="close-green"><img src="images/icon_close_green.gif"   alt="" /></a></td>
		</tr>
		</table>
		</div>
		<?php } ?>
		<!--  end message-green -->
		<!--  start message-red -->
		<?php if($pass_err!=''){?>
		<div id="message-red">
			<table border="0" width="100%" cellpadding="0" cellspacing="0">
			<tr>
				<td class="red-left">Error. <?php echo $pass_err; ?></td>
				<td class="red-right"><a class="close-red"><img src="images/icon_close_red.gif"   alt="" /></a></td>
			</tr>
			</table>
		</div>
		<?php } ?>
		<!--  end message-red -->
	<div id="related-act-body" >
	<div class="right">
		<h5>Password</h5>
		Update your account password.
	</div>
	
	<!-- start table -->
	<form name="frmPre" id="frmPre" method="post" action="profile_setting.php" enctype="multipart/form-data" >
		<table border="0" cellpadding="0" cellspacing="0"  id="id-form">
		<tr>
			<th valign="top">Current Password:</th>
			<td><input type="password" name="password" id="password" class="required minlength" minlength="8"/></td>
			<td>
			</td>
		</tr>
		<tr>
			<th valign="top">New Password:</th>
			<td><input type="password" name="new_password" id="new_password" class="required minlength" minlength="8"/></td>
			<td>
			</td>
		</tr>
		<tr>
			<th valign="top">Confirm-Password:</th>
			<td><input type="password" name="conf_pass" id="conf_pass" equalto="#new_password" class="required"/></td>
			<td>
			</td>
		</tr>
		<tr>
			<th>&nbsp;</th>
			<td valign="top">
			<input name="change_pass" type="hidden" value="yes" />
			<input name="id" type="hidden" value="<?php echo $_SESSION['PepUpSalesUserId']; ?>" />
			<input type="reset" value="Reset!" class="form-reset">
			<!--<input type="button" value="Back" class="form-reset" onclick="location.href='profile_setting.php?profile=<?php echo base64_encode(view);?>';" /> -->
			<input type="button" value="Back" class="form-reset" onclick="location.href='profile.php';" />
			<input name="submit" class="form-submit" type="submit" id="submit" value="Save" />
			</td>
			<td></td>
		</tr>
		</table>
	</form>
	<!-- end table  -->
	<div class="clear"></div>
</div>
<!-- end related-act-inner -->
	<div class="clear"></div>
	</td>
	<td>

	<!-- right bar-->
	<?php include("rightbar/profile_setting_bar.php") ?>
</td>
</tr>
<tr>
<td><img src="images/blank.gif" width="695" height="1" alt="blank" /></td>
<td></td>
</tr>
</table>
</td>
</tr>
</table>
 
<div class="clear"></div>
 
</div>
<!--  end content-table-inner  -->
</td>
<td id="tbl-border-right"></td>
</tr>
<!--<tr>
	<th class="sized bottomleft"></th>
	<td id="tbl-border-bottom">&nbsp;</td>
	<th class="sized bottomright"></th>
</tr>-->
</table>
<div class="clear">&nbsp;</div>

</div>
<!--  end content -->
<div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->
<div class="clear">&nbsp;</div>
    
<!-- start footer -->         
<?php include("footer.php"); ?>
<!-- end footer -->
 
</body>
</html>