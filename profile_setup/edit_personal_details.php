
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <!-- BEGIN: Subheader -->
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">
                    Personal Information
                </h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="#" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__separator">
                        -
                    </li>
                    <li class="m-nav__item">
                        <a href="#" class="m-nav__link">
                            <span class="m-nav__link-text">
                                Home
                            </span>
                        </a>
                    </li>
                    <li class="m-nav__separator">
                        -
                    </li>
                    <li class="m-nav__item">
                        <a href="" class="m-nav__link">
                            <span class="m-nav__link-text">
                                Admin
                            </span>
                        </a>
                    </li>  
                </ul>
            </div> 
        </div>
    </div>
    <!-- END: Subheader -->
    <div class="m-content">
        <div class="row">
            <div class="col-lg-12">
                <!--begin::Portlet--> 
                <div class="m-portlet">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <span class="m-portlet__head-icon m--hide">
                                    <i class="la la-gear"></i>
                                </span>
                                <h3 class="m-portlet__head-text">
                                    Personal Information
                                </h3>
                            </div>
                        </div>
                    </div>
                    <!--begin::Form-->
                    <form class="m-form m-form--fit m-form--label-align-right" id="m_form_1"  method="post" action="profile_setting.php" enctype="multipart/form-data">

                        <div class="m-portlet__body">
                            <div class="m-form__content">
                                <div class="m-alert m-alert--icon alert alert-danger m--hide" role="alert" id="m_form_1_msg">
                                    <div class="m-alert__icon">
                                        <i class="la la-warning"></i>
                                    </div>
                                    <div class="m-alert__text">
                                        Oh snap! Change a few things up and try submitting again.
                                    </div>
                                    <div class="m-alert__close">
                                        <button type="button" class="close" data-close="alert" aria-label="Close"></button>
                                    </div>
                                </div>
                            </div>
                            
                            <?php if(isset($profile_sus) && !empty($profile_sus)){ ?>
                            <div role="alert" style="background: #d7fbdc;" class="m-alert  m-alert--icon m-alert--air m-alert--square alert alert-dismissible  m--margin-bottom-30">
                                <div class="m-alert__icon" >
                                    <i class="flaticon-exclamation m--font-brand"></i>
                                </div>
                                <div class="m-alert__text">
                                    <?php echo  $profile_sus; ?>
                                </div>
                               
                            </div>
                            <?php } ?>

                            <div class="form-group m-form__group row">
                                <div class="col-lg-4">
                                    <label>
                                       Name:
                                    </label>

                                    <input type="text" name="name" id="name" class="form-control m-input" value="<?php echo $name; ?>"  placeholder="Enter Name">

                                </div>
                                <?php if($_SESSION['userLoginType']!=2){ ?>
                                <div class="col-lg-4">
                                  <label class="">
                                       Address:
                                  </label>
                                  <textarea name="address" id="address" class="form-control m-input" placeholder="Enter Address" ><?php echo $address; ?></textarea>
                                </div>
                                <?php } ?> 
                                <div class="col-lg-4">
                                    <label>
                                       Phone No:
                                    </label>

                                   <input type="text" name="phone_no" id="phone_no" class="form-control m-input" minlength="10" maxlength="15" value="<?php echo $phone_no; ?>"  placeholder="Enter Phone No">  
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                             <?php if($_SESSION['userLoginType']==1){ ?>
                                <div class="col-lg-4">
                                    <label class="">
                                        Select City:
                                    </label>
                                    <div class="input-group m-input-group m-input-group--square">
                                      <select name="city" id="<?php echo $city_id; ?>" class="form-control m-input m-input--square" onChange="showCityArea(this.value,id)" >
                                         <?php $auUsr=$_objAdmin->_getSelectList2('city','city_id,city_name','',"ORDER BY city_name");
                                          for($i=0;$i<count($auUsr);$i++){?>
                                            <option value="<?php echo $auUsr[$i]->city_id;?>"  <?php if ($auUsr[$i]->city_id==$city_id){ ?> selected <?php } ?>><?php echo $auUsr[$i]->city_name;?>
                                        
                                            </option>
                                          <?php } ?>
                                        </select> 
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <label class="">
                                        Select  Area:
                                    </label>
                                    <div class="m-input-icon m-input-icon--right">
                                        <div id="outputcity"></div> 
                                    </div> 
                                </div>
                                <?php } ?>
                                <?php if($_SESSION['userLoginType']!=1 && $_SESSION['userLoginType']!=5 ){ ?>
                                <div class="col-lg-4">
                                    <label class="">
                                        Contact Person:
                                    </label>
                                    <div class="m-input-icon m-input-icon--right">
                                      <input type="text" name="contact_person" id="contact_person" class="form-control m-input" value="<?php echo $auRec[0]->contact_person; ?>"/>
                                        
                                    </div> 
                                </div>
                                 <?php } ?>
                            </div>
                            <div class="form-group m-form__group row">
                             <?php if($_SESSION['userLoginType']!=1 && $_SESSION['userLoginType']!=5 )  { ?>
                                <div class="col-lg-4">
                                    <label class="">
                                       Contact Phone No:
                                    </label>
                                    <div class="m-input-icon m-input-icon--right">

                                    <input type="text" name="contact_number" id="contact_number" class="form-control m-input" maxlength="15" minlength="10" value="<?php echo $auRec[0]->contact_number; ?>"/>

                                       
                                    </div> 
                                </div>
                                 <?php } ?>
                            </div>
                            
                        </div>
                        <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                            <div class="m-form__actions m-form__actions--solid">
                                <div class="row">
                                    <div class="col-lg-4"></div>
                                    <div class="col-lg-8">
                                        <button type="submit" class="btn btn-success">
                                            Submit
                                        </button>
                                        <button type="reset" class="btn btn-secondary">
                                            Cancel
                                        </button>
                                        <button type="back" class="btn btn-secondary" onclick="location.href='profile.php';">
                                            Back
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <input name="edit_profile" type="hidden" value="yes" />
                        <input name="id" type="hidden" value="<?php echo $id; ?>" />
                        <input name="edit_details" type="hidden" value="yes" />
                       

                    </form>
                    <!--end::Form-->
                </div>
                <!--end::Portlet-->
            </div>
        </div>
    </div>
</div>
</div>
</div>
<!-- start footer -->         
<?php include("footer.php") ?>
<script type="text/javascript">
    var FormControls = function () {
        //== Private functions

        var demo1 = function () {
            $("#m_form_1").validate({
                // define validation rules
                rules: {
                    name: {
                        required: true
                    },
                    phone_no: {
                        required: true,
                        digits: true
                    },
                    address: {
                        required: true
                    },
                    city: {
                        required: true
                       
                    },
                    area: {
                        required: true
                    }
                },
                //display error alert on form submit  
                invalidHandler: function (event, validator) {
                    var alert = $('#m_form_1_msg');
                    alert.removeClass('m--hide').show();
                    mApp.scrollTo(alert, -200);
                },
                submitHandler: function (form) {
                    form.submit();
                }
            });
        }

        return {
            // public functions
            init: function () {
                demo1();
            }
        };
    }();

    jQuery(document).ready(function () {
        FormControls.init();
        
    });

jQuery(document).ready(function () {

    var str="<?php echo $city_id; ?>";
    var id="<?php echo $areaId; ?>";

    showCityArea(str,id);
    });


    function showCityArea(str,id){

        $.ajax({
            'type': 'POST',
            'url': 'dropdown_area.php',
            'data': 's='+str+'&c='+id,
            'success' : function(mystring){
                //alert(mystring);
                document.getElementById("outputcity").innerHTML = mystring;
        
        }
    });
    }  


</script>
