<script src="javascripts/jquery-1.8.2.js" type="text/javascript"></script>
<script src="javascripts/jquery-ui.js"></script>
<script type="text/javascript" src="javascripts/validate.js"></script>
<!-- start content-outer -->
<div id="content-outer">
<!-- start content -->
<div id="content">

<div id="page-heading"><h1>Profile Setting</h1></div>
<table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">
<tr>
	<td>
	<table border="0" width="100%" cellpadding="0" cellspacing="0" >
	
	<tr valign="top">
	<td>
	<!--  start content-table-inner -->
	<div id="content-table-inner">
		<table border="0" width="100%" cellpadding="0" cellspacing="0">
		<tr valign="top">
			<td>
		<?php if($profile_sus!=''){?>
		<!--  start message-green -->
			<div id="message-green">
		<table border="0" width="100%" cellpadding="0" cellspacing="0">
		<tr>
		<td class="green-left"><?php echo $profile_sus; ?></td>
		<td class="green-right"><a class="close-green"><img src="images/icon_close_green.gif"   alt="" /></a></td>
		</tr>
		</table>
		</div>
		<?php } ?>
		<!--  end message-green -->
	<div id="related-act-body" >
	<div class="right">
		<h5>Personal Information</h5>
	</div>
	
	<!-- start table -->
		<form name="frmPre" id="frmPre" method="post" action="profile_setting.php" enctype="multipart/form-data" >
		<table border="0" cellpadding="0" cellspacing="0"  id="id-form">
		<tr>
			<th valign="top">Name:</th>
			<td><input type="text" name="name" id="name" class="required" value="<?php echo $name; ?>" readonly /></td>
			<td></td>
		</tr>
		<?php if($_SESSION['userLoginType']!=2)  { ?>
		<tr>
			<th valign="top">Address:</th>
			<td><textarea rows="6" cols="40" name="address" class="textarea" ><?php echo $address; ?></textarea></td>
			<td>
			</td>
		</tr>
		<?php } ?>
		<tr>
			<th valign="top">Phone No:</th>
			<td><input type="text" name="phone_no" id="phone_no" class="required number minlength" maxlength="15" minlength="10" value="<?php echo $phone_no; ?>"/></td>
			<td>
			</td>
		</tr>
		<?php if($_SESSION['userLoginType']!=2)  { ?>
		<tr>
			<th valign="top">Country:</th>
			<td>
				<select name="country" id="country" class="styledselect_form_3">
				<option value="india">India</option>
				</select>
			</td>
		</tr>
		<tr>
			<th valign="top">Select State:</th>
			<td>
			<select name="state" id="<?php echo $city_id; ?>" class="styledselect_form_3" onChange="showStateCity(this.value,id)" >
			<?php $auUsr=$_objAdmin->_getSelectList2('state','state_id,state_name','',"ORDER BY state_name");
			for($i=0;$i<count($auUsr);$i++){?>
			<option value="<?php echo $auUsr[$i]->state_id;?>"  <?php if ($auUsr[$i]->state_id==$state_id){ ?> selected <?php } ?>><?php echo $auUsr[$i]->state_name;?></option>
			<?php } ?>
			</select>
			</td>
		</tr>
		<tr>
			<th valign="top">Select City:</th>
			<td><div id="outputcity"></div></td>
			<td></td>
		</tr>
		<tr>
			<th valign="top">Pincode/Zipcode:</th>
			<td><input type="text" name="zipcode" id="zipcode" class="text number minlength" maxlength="6" minlength="6" value="<?php echo $auRec[0]->zipcode; ?>"/></td>
			<td></td>
		</tr>
		<?php if($_SESSION['userLoginType']!=1 && $_SESSION['userLoginType']!=5 )  { ?>
		<tr>
			<th valign="top">Contact Person:</th>
			<td><input type="text" name="contact_person" id="contact_person" class="text" value="<?php echo $auRec[0]->contact_person; ?>"/></td>
			<td></td>
		</tr>
		<tr>
			<th valign="top">Contact Phone No:</th>
			<td><input type="text" name="contact_number" id="contact_number" class="text number minlength" maxlength="15" minlength="10" value="<?php echo $auRec[0]->contact_number; ?>"/></td>
			<td></td>
		</tr>
		<?php } } ?>
		<tr>
		<th>&nbsp;</th>
		<td valign="top">
			<input name="edit_profile" type="hidden" value="yes" />
			<input name="id" type="hidden" value="<?php echo $id; ?>" />
			<input type="reset" value="Reset!" class="form-reset">
			<!--<input type="button" value="Back" class="form-reset" onclick="location.href='profile_setting.php?profile=<?php echo base64_encode(view);?>';" /> -->
			<input type="button" value="Back" class="form-reset" onclick="location.href='profile.php';" />
			<input name="submit" class="form-submit" type="submit" id="submit" value="Save" />
		</td>
		<td></td>
	</tr>
	</table>
	  
	</form>
	<!-- end table  -->
	<div class="clear"></div>
</div>		
	</div>
<!-- end related-act-inner -->
	<div class="clear"></div>
	</td>
	<td>	<!-- right bar-->
	<?php include("rightbar/profile_setting_bar.php") ?>
	</td>
	</tr>
	</table>
	</td>
	</tr>
	
<tr>
<td><img src="images/blank.gif" width="695" height="1" alt="blank" /></td>
<td></td>
</tr>
</table>
</td>
</tr>
</table>
 
<div class="clear"></div>
</div>
<!--  end content -->
<div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->
<div class="clear">&nbsp;</div>
    
<!-- start footer -->         
<?php include("footer.php"); ?>
<!-- end footer -->
 
</body>
</html>