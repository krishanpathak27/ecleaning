
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <!-- BEGIN: Subheader -->
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">
                    Personal Information
                </h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="#" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__separator">
                        -
                    </li>
                    <li class="m-nav__item">
                        <a href="#" class="m-nav__link">
                            <span class="m-nav__link-text">
                                Home
                            </span>
                        </a>
                    </li>
                    <li class="m-nav__separator">
                        -
                    </li>
                    <li class="m-nav__item">
                        <a href="" class="m-nav__link">
                            <span class="m-nav__link-text">
                               Admin
                            </span>
                        </a>
                    </li>  
                </ul>
            </div> 
        </div>
    </div>
    <!-- END: Subheader -->
    <div class="m-content">
        <div class="row">
            <div class="col-lg-12">
                <!--begin::Portlet--> 
                <div class="m-portlet">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <span class="m-portlet__head-icon m--hide">
                                    <i class="la la-gear"></i>
                                </span>
                                <h3 class="m-portlet__head-text">
                                    Personal Information
                                </h3>
                            </div>
                        </div>
                    </div>
                    <!--begin::Form-->
                    <form class="m-form m-form--fit m-form--label-align-right" id="m_form_1"  method="post" action="profile_setting.php" enctype="multipart/form-data">

                        <div class="m-portlet__body">
                            <div class="m-form__content">
                                <div class="m-alert m-alert--icon alert alert-danger m--hide" role="alert" id="m_form_1_msg">
                                    <div class="m-alert__icon">
                                        <i class="la la-warning"></i>
                                    </div>
                                    <div class="m-alert__text">
                                        Oh snap! Change a few things up and try submitting again.
                                    </div>
                                    <div class="m-alert__close">
                                        <button type="button" class="close" data-close="alert" aria-label="Close"></button>
                                    </div>
                                </div>
                            </div>
                            <?php if (isset($err) && !empty($err)) { ?>
                              <div class="m-form__content">
                                <div class="m-alert m-alert--icon alert alert-danger" role="alert" id="m_form_1_msg1">
                                    <div class="m-alert__icon">
                                        <i class="la la-warning"></i>
                                    </div>
                                    <div class="m-alert__text">
                                        <?php echo $err; ?>
                                    </div>
                                    <div class="m-alert__close">
                                        <button type="button" class="close" data-close="alert" aria-label="Close"></button>
                                    </div>
                                </div>
                            </div>
                            <?php } ?>
                            <div class="form-group m-form__group row">
                                <div class="col-lg-4">
                                    <label>
                                        Name:
                                    </label>
                                    <div class="m-input-icon m-input-icon--right">
                                    <?php echo $name; ?>
                                    </div>

                                </div>
                                <div class="col-lg-4">
                                    <label class="">
                                       Email ID:
                                    </label>
                                    <div class="m-input-icon m-input-icon--right">
                                    <?php echo $email; ?>
                                    </div>
                                </div>
                              <?php if($_SESSION['userLoginType']!=2){ ?>
                                <div class="col-lg-4">
                                    <label>
                                        Building:
                                    </label>
                                    <div class="m-input-icon m-input-icon--right">
                                    <?php echo $address; ?>
                                    </div>
                                </div>
                              <?php } ?> 

                            </div>
                            <div class="form-group m-form__group row">
                                <div class="col-lg-4">
                                    <label class="">
                                       Phone No:
                                    </label>
                                    <div class="m-input-icon m-input-icon--right">
                                    <?php echo $phone_no; ?> 
                                    </div>
                                </div>
                                <?php if($_SESSION['userLoginType']==1){ ?>
                                <div class="col-lg-4">
                                    <label class="">
                                        City:
                                    </label>
                                    <div class="m-input-icon m-input-icon--right">
                                       <?php echo $city; ?>
                                    </div> 
                                </div>
                                <?php } ?>
                                <?php if($_SESSION['userLoginType']!=1 && $_SESSION['userLoginType']!=5 ){ ?>
                                <div class="col-lg-4">
                                    <label class="">
                                       Contact Person:
                                    </label>
                                    <div class="m-input-icon m-input-icon--right">
                                        <?php echo $contact_person; ?> 
                                    </div> 
                                </div>
                                <?php }?>
                            </div>
                            <div class="form-group m-form__group row">
                              <?php if($_SESSION['userLoginType']!=1 && $_SESSION['userLoginType']!=5){ ?>
                                <div class="col-lg-4">
                                    <label class="">
                                        Contact Phone No:
                                    </label>
                                    <div class="m-input-icon m-input-icon--right">
                                        <?php echo $contact_number; ?> 
                                    </div> 
                                </div>
                              <?php }?>
                            </div>
                            
                        </div>
                        <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                            <div class="m-form__actions m-form__actions--solid">
                                <div class="row">
                                    <div class="col-lg-4"></div>
                                    <div class="col-lg-8">
                                        <button type="submit" class="btn btn-success" >
                                            Edit Personal Information
                                        </button>
                                        <button type="reset" class="btn btn-secondary">
                                            Cancel
                                        </button>
                                        <a class="btn btn-secondary" href="profile.php">
                                            Back
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                       <input name="edit_details" type="hidden" value="yes" />
                        <input name="account_id" type="hidden" value="<?php echo $_SESSION['accountId']; ?>" />
                        <input type="hidden" name="add" value="yes">

                    </form>
                    <!--end::Form-->
                </div>
                <!--end::Portlet-->
            </div>
        </div>
    </div>
</div>
</div>
</div>
<!-- start footer -->         
<?php include("footer.php") ?>
<script type="text/javascript">
    var FormControls = function () {
        //== Private functions

        var demo1 = function () {
            $("#m_form_1").validate({
                // define validation rules
                rules: {
                    supervisor_name: {
                        required: true
                    },
                    supervisor_phone_no: {
                        required: true,
                        digits: true
                    },
                    building_id: {
                        required: true
                    },
                    supervisor_email: {
                        required: true,
                        email: true,
                        minlength: 10
                    },
                    username: {
                        required: true
                    },
                    password: {
                        required: true,
                        minlength: 8
                    },
                },
                //display error alert on form submit  
                invalidHandler: function (event, validator) {
                    var alert = $('#m_form_1_msg');
                    alert.removeClass('m--hide').show();
                    mApp.scrollTo(alert, -200);
                },
                submitHandler: function (form) {
                    form.submit();
                }
            });
        }

        return {
            // public functions
            init: function () {
                demo1();
            }
        };
    }();

    jQuery(document).ready(function () {
        FormControls.init();
    });

</script>
