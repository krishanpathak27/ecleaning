<!-- start content-outer -->
<div id="content-outer">
<!-- start content -->
<div id="content">
<div id="page-heading"><h1>Profile Setting</h1></div>
<table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">
<!--<tr>
	<th rowspan="3" class="sized"><img src="images/side_shadowleft.jpg" width="20" height="300" alt="" /></th>
	<th class="topleft"></th>
	<td id="tbl-border-top">&nbsp;</td>
	<th class="topright"></th>
	<th rowspan="3" class="sized"><img src="images/side_shadowright.jpg" width="20" height="300" alt="" /></th>
</tr>
-->
<tr>
	<!--<td id="tbl-border-left"></td>-->
	<td>
	<!--  start content-table-inner -->
	<div id="content-table-inner">
	
	<table border="0" width="100%" cellpadding="0" cellspacing="0" >
	<tr valign="top">
	<td>
<div id="related-act-body" >
	<div class="right">
		<h5>Personal Information</h5>
	</div>
	<!-- start table -->
	<form name="frmPre" id="frmPre" method="post" action="profile_setting.php" enctype="multipart/form-data" >
		<table border="0" cellpadding="0" cellspacing="0"  id="id-form">
		<tr>
			<th valign="top">Name:</th>
			<td><?php echo $name; ?></td>
			
		</tr>
		<tr>
			<th valign="top">Email ID:</th>
			<td><?php echo $email; ?></td>
			<td>
			<ul class="greyarrow">
			<li><a href="profile_setting.php?profile=<?php echo base64_encode(email);?>">Edit</a></li> 
			</ul>
			</td>
		</tr>
		<?php if($_SESSION['userLoginType']!=2)  { ?>
		<tr>
			<th valign="top">Address:</th>
			<td><?php echo $address; ?></td>
			
		</tr>
		<?php } ?>
		<tr>
			<th valign="top">Phone No:</th>
			<td><?php echo $phone_no; ?></td>
			
		</tr>
		<?php if($_SESSION['userLoginType']!=2)  { ?>
		<tr>
			<th valign="top">Country:</th>
			<td><?php echo $country; ?></td>
			
		</tr>
		<tr>
			<th valign="top">State:</th>
			<td><?php echo $state; ?></td>
		</tr>
		<tr>
			<th valign="top">City:</th>
			<td><?php echo $city; ?></td>
		</tr>
		<tr>
			<th valign="top">Pincode/Zipcode:</th>
			<td><?php echo $zipcode; ?></td>
		</tr>
		<?php if($_SESSION['userLoginType']!=1 && $_SESSION['userLoginType']!=5 )  { ?>
		<tr>
			<th valign="top">Contact Person:</th>
			<td><?php echo $contact_person; ?></td>
		</tr>
		<tr>
			<th valign="top">Contact Phone No:</th>
			<td><?php echo $contact_number; ?></td>
		</tr>
		<?php } } ?>
		<tr>
		<th valign="top"><input type="button" value="Back" class="form-reset" onclick="location.href='profile.php';" /></th>
		<td >
			<input name="edit_details" type="hidden" value="yes" />
			<input name="submit" class="form-submit" type="submit" id="submit" value="Edit Personal Information" />
		</td>
		<td></td>
	</tr>
		</table>
	</form>
	<!-- end table  -->
	<div class="clear"></div>
</div>
<!-- end related-act-inner -->
	<div class="clear"></div>
	</td>
	<td>

	<!-- right bar-->
	<?php include("rightbar/profile_setting_bar.php") ?>
</td>
</tr>
<tr>
<td><img src="images/blank.gif" width="695" height="1" alt="blank" /></td>
<td></td>
</tr>
</table>
 
<div class="clear"></div>
 
</div>
<!--  end content-table-inner  -->
</td>
<td id="tbl-border-right"></td>
</tr>
<!--<tr>
	<th class="sized bottomleft"></th>
	<td id="tbl-border-bottom">&nbsp;</td>
	<th class="sized bottomright"></th>
</tr>-->
</table>
<div class="clear">&nbsp;</div>

</div>
<!--  end content -->
<div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->
<div class="clear">&nbsp;</div>
    
<!-- start footer -->         
<?php include("footer.php"); ?>
<!-- end footer -->
 
</body>
</html>