<?php
include("includes/config.inc.php");
include("includes/function.php");
$page_name = "WhitSpot Promo Code Master";
$_objAdmin = new Admin();

if (isset($_REQUEST['stid']) && $_REQUEST['value'] != "") {
    if ($_REQUEST['value'] == 1) {
        $status = 'I';
    } else {
        $status = 'A';
    }
    $cid = $_objAdmin->_dbUpdate(array("status" => $status, "last_updated_date" => date('Y-m-d H:i:s')), 'table_promo_code ', " promo_code_id in(" . $_REQUEST['stid'] . ")");
    $_SESSION['order'] = 'updated';
    header("Location: promoCode.php?suc=Status Changes Successfully");
}
?>

<?php include("header.inc.php") ?>
<div class="m-grid__item m-grid__item--fluid m-wrapper" style="background-color:">
    <!-- BEGIN: Subheader -->
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="#" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__separator">
                        -
                    </li>
                    <li class="m-nav__item">
                        <a href="#" class="m-nav__link">
                            <span class="m-nav__link-text">
                                Master
                            </span>
                        </a>
                    </li>
                    <li class="m-nav__separator">
                        -
                    </li>
                    <li class="m-nav__item">
                        <a href="" class="m-nav__link">
                            <span class="m-nav__link-text">
                                Promo Code
                            </span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <!-- END: Subheader -->
    <div class="m-content">
        <?php if (isset($_REQUEST['suc']) && $_REQUEST['suc'] != "") { ?>
            <div role="alert" style="background: #d7fbdc;" class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-dismissible m--margin-bottom-30">
                <div class="m-alert__icon" >
                    <i class="flaticon-exclamation m--font-brand"></i>
                </div>
                <div class="m-alert__text">
    <?php echo $_REQUEST['suc']; ?> 
                </div>
            </div>
<?php } ?>
        <div class="m-portlet m-portlet--mobile">

            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            Promo Code List
                        </h3>
                    </div>
                </div>
            </div>
            <div class="m-portlet__body">
                <!--begin: Search Form -->
                <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
                    <div class="row align-items-center">
                        <div class="col-xl-8 order-2 order-xl-1">
                            <div class="form-group m-form__group row align-items-center">
                                <div class="col-md-4">
                                    <div class="m-input-icon m-input-icon--left">
                                        <input type="text" class="form-control m-input" placeholder="Search..." id="generalSearch">
                                        <span class="m-input-icon__icon m-input-icon__icon--left">
                                            <span>
                                                <i class="la la-search"></i>
                                            </span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-4 order-1 order-xl-2 m--align-right add">
                            <a href="add_promoCode.php" class="btn btn-warning m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                                <span>
                                    <i class="la la-cart-plus"></i>
                                    <span>
                                        Add Promo Code
                                    </span>
                                </span>
                            </a>
                            <div class="m-separator m-separator--dashed d-xl-none "></div>
                        </div>
                    </div>
                </div>
                <!--end: Search Form -->
                
                <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30 collapse" id="m_datatable_group_action_form">
                    <div class="row align-items-center">
                        <div class="col-xl-12">
                            <div class="m-form__group m-form__group--inline">
                                <div class="m-form__label m-form__label-no-wrap">
                                    <label class="m--font-bold m--font-danger-">
                                        Selected
                                        <span id="m_datatable_selected_number"></span>
                                        records:
                                    </label>
                                </div>
                                <div class="m-form__control">
                                    <div class="btn-toolbar">
                                        <div class="dropdown">
                                            <button type="button" class="btn btn-accent btn-sm dropdown-toggle" data-toggle="dropdown">
                                                Update status
                                            </button>
                                            <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                                                <a class="dropdown-item changeStatusBnt" href="#" id="activeBnt">
                                                    Active
                                                </a>
                                                <a class="dropdown-item changeStatusBnt" href="#" id="inactiveBnt">
                                                    Inactive
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--begin: Datatable -->
                <div class="promoCode" id="promoCode"></div>
                <!--end: Datatable -->
            </div>
        </div>
    </div>
</div>
</div>
<!-- start footer -->         
<?php include("footer.php") ?>
<script type="text/javascript" >
    var DefaultDatatableDemo = function () {
        //== Private functions

        // basic demo
         var options = {
                data: {
                    type: 'remote',
                    source: {
                        read: {
                            url: '<?php echo $ajaxUrl . '?pageType=promoCode' ?>'
                        }
                    },
                    pageSize: 20,
                    serverPaging: true,
                    serverFiltering: true,
                    serverSorting: true
                },
                layout: {
                    theme: 'default',
                    class: '',
                    scroll: true,
                    footer: false
                },
                sortable: true,
                filterable: false,
                pagination: true,
                search: {
                    input: $('#generalSearch')
                },
                columns: [{
                    field: "promo_code_id",
                    title: "#",
                    sortable: false,
                    width: 40,
                    selector: {class: 'm-checkbox--solid m-checkbox--brand'}
                },{
                        field: "promo_code",
                        title: "Promo Code",
                        sortable: 'asc',
                    }, {
                        field: "building_name",
                        title: "Building Name",
                        responsive: {visible: 'lg'}
                    }, {
                        field: "discription",
                        title: "Description",
                        responsive: {visible: 'lg'}
                    }, {
                        field: "max_limit_use",
                        title: "Max Use Limit",
                        responsive: {visible: 'lg'}
                    }, {
                        field: "promo_code_discount",
                        title: "Discount",
                        responsive: {visible: 'lg'}
                    }, {
                        field: "start_date",
                        title: "Start Date",
                        responsive: {visible: 'lg'}
                    }, {
                        field: "end_date",
                        title: "End Date",
                        responsive: {visible: 'lg'}
                    }, {
                        field: "Status",
                        title: "Status",
                        locked: {right: 'xl'},
                        template: function (row) {
                            var status = {
                                1: {'title': 'Active', 'class': 'm-badge--brand'},
                                2: {'title': 'Inactive', 'class': 'm-badge--warning'}
                            };
                            return '<span class="m-badge ' + status[row.Status].class + ' m-badge--wide">' + status[row.Status].title + '</span>';

                        },
                        responsive: {visible: 'lg'}
                    }, {
                        field: "Actions",
                        title: "Actions",
                        sortable: false,
                        locked: {right: 'xl'},
                        overflow: 'visible',
                        template: function (row, index, datatable) {
                            var dropup = (datatable.getPageSize() - index) <= 4 ? 'dropup' : '';
                            return '\
                                                <a href="promoCode.php?stid=' + row.promo_code_id + '&value=' + row.Status + '" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Change Status">\
                                                        <i class="la la-i-cursor"></i>\
                                                </a>\
                                        ';
                        }
                    }]
            }
        var localSelectorDemo = function () {

            options.search = {
                input: $('#generalSearch'),
            };

            var datatable = $('#promoCode').mDatatable(options);

            $('#m_form_status').on('change', function () {
                datatable.search($(this).val().toLowerCase(), 'Status');
            });

            $('#m_form_type').on('change', function () {
                datatable.search($(this).val().toLowerCase(), 'Type');
            });

            $('#m_form_status,#m_form_type').selectpicker();

            datatable.on('m-datatable--on-check m-datatable--on-uncheck m-datatable--on-layout-updated', function (e) {
                var checkedNodes = datatable.rows('.m-datatable__row--active').nodes();
                var count = checkedNodes.length;
                $('#m_datatable_selected_number').html(count / 2);
                if (count > 0) {
                    $('#m_datatable_group_action_form').collapse('show');
                } else {
                    $('#m_datatable_group_action_form').collapse('hide');
                }
            });

            $('.changeStatusBnt').on('click', function (e) {
                var id = '';
                var ids = datatable.rows('.m-datatable__row--active').
                        nodes().
                        find('.m-checkbox--single > [type="checkbox"]').
                        map(function (i, chk) {
                            return $(chk).val();
                        });
                for (var i = 0; i < ids.length; i++) {
                    if (i == 0) {
                        id += ids[i];
                    } else {
                        id += ',' + ids[i];
                    }

                }
                if (this.id == 'activeBnt') {
                    window.location.assign("promoCode.php?stid=" + id + "&value=2");
                } else if (this.id == 'inactiveBnt') {
                    window.location.assign("promoCode.php?stid=" + id + "&value=1");
                }

            })

        };
        return {
            // public functions
            init: function () {
                localSelectorDemo();
            }
        };
    }();

    jQuery(document).ready(function () {
        DefaultDatatableDemo.init();
    });



</script>


