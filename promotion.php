<?php
include("includes/config.inc.php");
include("includes/function.php");
include("includes/globalarraylist.php");

$page_name="Promotion";
$checkPromotionStatus = true;

if(isset($_REQUEST['show']) && $_REQUEST['show']=="yes"){
	$_objAdmin->showPromotion($checkPromotionStatus);
	die;
}

//Delete Salesman
if(isset($_REQUEST['stid']) && isset($_REQUEST['value']) && $_REQUEST['stid']!=""){
	
		if($_REQUEST['value'] == 'Active')
		$id=$_objAdmin->_dbUpdate(array("last_updated_on"=>date('Y-m-d H:i:s'),"status"=>'I'),'table_promotion', " promotion_id='".$_REQUEST['stid']."'");
		else 
		$id=$_objAdmin->_dbUpdate(array("last_updated_on"=>date('Y-m-d H:i:s'),"status"=>'A'),'table_promotion', " promotion_id='".$_REQUEST['stid']."'");

		$sus="Status updated successfully";
		header ("location: promotion.php");
}
	
include("header.inc.php");
?>

<!-- start content-outer -->
 
<input name="pagename" type="hidden"  id="pagename" value="promotion.php" />
<div id="content-outer">
<!-- start content -->
<div id="content">
<div id="page-heading"><h1><span style="color: #d74343; font-family: Tahoma; font-weight: bold;">Promotion</span></h1></div>
<?php if($_REQUEST['err']!=''){?>
	<div id="message-red">
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
	<tr>
		<td class="red-left">Error. <?php echo "You have exceeded the maximum limit of active users"; ?></td>
		<td class="red-right"><a class="close-red"><img src="images/icon_close_red.gif"   alt="" /></a></td>
	</tr>
	</table>
	</div>
<?php } ?>
<table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">
<tr>
	<td>
	<!--  start content-table-inner -->
	<div id="content-table-inner">
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
	<tr valign="top">
	<td>
		<!-- start id-form -->
		<?php
		if(basename($_SERVER['PHP_SELF']) == "promotion.php") { 
			if(isset($_REQUEST['action'])) {

				switch ($_REQUEST['action']) {

					case add : 
						include("promotion/add.php");
						break;

					case edit :
						include("promotion/index.php");
						break;

					case import :
						//echo "hello";
						include("promotion/import.php");
						break;

					case 'export':
						include("promotion/index.php");
						break;
					case 'delete':
						include("promotion/index.php");
						break;

					default :
						echo "hello";
						include("promotion/index.php");
					break;

				} 
			} else {

				include("promotion/index.php");
			}
			
		} ?>
		<!-- end id-form  -->
	</td>
	<td>
	<!-- right bar-->
	<?php //include("promotion/rightbar.php") ?>
	</td>
	</tr>
<tr>
</tr>
</table>
<div class="clear"></div>
</div>
<!--  end content-table-inner  -->
</td>
<td id="tbl-border-right"></td>
</tr>
</table>
<div class="clear">&nbsp;</div>
</div>
<!--  end content -->
<div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->
<div class="clear">&nbsp;</div> 
<!-- start footer -->         
<?php include("footer.php");?>
<!-- end footer -->
</body>
</html>