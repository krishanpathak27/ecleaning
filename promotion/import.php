<?php 
/*****************************************************
 * desc : download preformatted CSV Sheet for added new promo_desc
 * created on : 05 Jan 2015
 * Author : AJAY
 *
 ***/

 function promotion_import_csv_format () {
	ob_clean();
	$data = "Chain Name*,Item Code*,Offer*, Start Date, End Date \n";					
	header("Content-type: application/octet-stream");
	header("Content-Disposition: attachment; filename=\"item_promo_sheet.csv\"");		
	echo $data;	
	die;	

 }	


if(isset($_POST['promotion_import_csv_format']) && $_POST['promotion_import_csv_format']=='yes') {

	promotion_import_csv_format ();

} else if(isset($_POST['import_item_promotion']) && $_POST['import_item_promotion']=='yes') {


		if(isset($_FILES['fileToUpload']['tmp_name']) && $_FILES['fileToUpload']['tmp_name']!=""){	
		$_objItem = new Item();
		$ret = $_objItem->import_item_promotion();
		if($ret=='') {																	
			$msg= "File has been successfully imported.";								
			}elseif($ret=='no') {
			$error= "Empty file";
			}else {
			$error=  implode(",", $ret);
			//print($error);													
			}
			if($msg!=''){
			$cat_sus=$msg;
			}else{
			ob_clean();	
			$data="Row Number(Please Delete The Row When Import),Chain Name*,Item Code*,Offer*\n";	
			$data .="$error \n";					
			header("Content-type: application/octet-stream");
			header("Content-Disposition: attachment; filename=\"error.csv\"");		
			echo $data;	
			die;
			
			}		
		}

}?>

<div class="clear"></div>
<!-- start content-outer -->
<table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">
	<tr>
		<!--<td id="tbl-border-left"></td>-->
		<td>
		<!--  start content-table-inner -->
		<div id="content-table-inner" style="border:none">
		<table border="0" width="100%" cellpadding="0" cellspacing="0">
		<tr valign="top">
		<td>
			<!--  start message-red -->
			<?php if($cat_err!=''){?>
			<div id="message-red">
				<table border="0" width="100%" cellpadding="0" cellspacing="0">
				<tr>
					<td class="red-left">Error. <?php echo $cat_err; ?></td>
					<td class="red-right"><a class="close-red"><img src="images/icon_close_red.gif"   alt="" /></a></td>
				</tr>
				</table>
			</div>
			<?php } ?>
			<!--  end message-red -->
			<?php if($cat_sus!=''){?>
			<!--  start message-green -->
			<div id="message-green">
				<table border="0" width="100%" cellpadding="0" cellspacing="0">
				<tr>
					<td class="green-left"><?php echo $cat_sus; ?></td>
					<td class="green-right"><a class="close-green"><img src="images/icon_close_green.gif"   alt="" /></a></td>
				</tr>
				</table>
			</div>
			<?php } ?>
			<!--  end message-green -->
			<!-- start id-form -->
			<form name="frmPre" id="frmPre" method="post" enctype="multipart/form-data" >
			<table border="0" cellpadding="0" cellspacing="0"  id="id-form">
			<tr>
				<td valign="top">Pre-formated Spreadsheet to enter multiple Promotion</td>
			</tr>
			<tr>
			<td valign="top">
				<input name="account_id" type="hidden" value="<?php echo $_SESSION['accountId']; ?>" />
				<input name="promotion_import_csv_format" type="hidden" value="yes" />
				<input name="submit" class="form-submit" type="submit" id="submit" value="Click to Downlaod" />
			</td>
			</tr>
			</table>
			</form>
			
			
			
			<form name="form" id="form" method="post" action="promotion.php?action=import" enctype="multipart/form-data" >
			<table border="0" cellpadding="0" cellspacing="0"  id="id-form">
			<tr>
				<th valign="top">Import Promotion</th>
			</tr>
			<tr>
				<td valign="top">Select Spreadsheet File in which your entered SKU promotion</td>
			</tr>
			<tr>
				<td><input type="file" name="fileToUpload" id="file_1" class="required" ></td>
			</tr>
			<tr>
				<td valign="top">Click Import button Now </td>
			</tr>
			<tr>
			<td valign="top">
				<input name="account_id" type="hidden" value="<?php echo $_SESSION['accountId']; ?>" />
				<input name="import_item_promotion" type="hidden" value="yes" />
				<input type="button" value="Back" class="form-reset" onclick="location.href='promotion.php';" />
				<input name="submit" class="form-submit" type="submit" id="submit" value="Import" />
				<div id="divLoad1" style="display:none;"><img src="images/load.gif" /> Please wait... </div>
			</td>
			</tr>
			</table>
			</form>
			<!-- end id-form  -->
			</td>
			<td>
			<!-- right bar-->
			<div class="clear"></div>
		</div>
<!-- end related-act-bottom -->
</div>
<!-- end related-activities -->
</td>
</tr>
</table>
<div class="clear"></div>
</div>
<!--  end content-table-inner  -->
</td>
</tr>
</table>

