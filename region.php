<?php
include("includes/config.inc.php");
include("includes/function.php");

$_objAdmin = new Admin();

if(isset($_REQUEST['show']) && $_REQUEST['show']=="yes"){
	$_objAdmin->showRegion();
	die;
}

/*********************************************** 
 |  Discription: Add Region Master
 |  Date : 20 May 2015
 |	By   : Nizam 	  
************************************************/
if(isset($_POST['submit']) && $_POST['submit']=='Save')
 {  
 	
        /* Offer Already exits condtions */
 	if(isset($_POST['region_name']) && $_POST['region_name']!="")
 	 {  
 		$region_exits=$_objAdmin->RegionExits();
 		if($region_exits==1){ $sus="Region name Already exits in the system.";}
 		else
 		{   /* Update region  */
 			if(isset($_POST['region_id']) && $_POST['region_id']!="")
 			 { 
 			 	$update=$_objAdmin->updateRegion($_POST['region_id']);
 			 	if($update>0){ $sus="Region has been successfully updated.";}
 			 }
 			 else
 			 {
              $offer=$_objAdmin->addRegion();
     		if($offer>0){ $sus="Region has been successfully added .";}
 	         }	
 			
 		}	

 	}
     
 }

/*********************************************** 
 |  Discription: Edit retailer channel
 |  Date : 20 May 2015
 |	By   : Nizam 	  
************************************************/

 if(isset($_REQUEST['action']) && $_REQUEST['action']=='edit')
       { 
       	
         $regionEdit=$_objAdmin->RegionEdit($_REQUEST['id']);
       	 /*echo '<pre>';
       	 print_r($channelEdit);*/
       	 
       } 

	

if(isset($_REQUEST['statusId']) && $_REQUEST['value']!="")
{
	if($_REQUEST['value']=="Active"){$status='I'; } else { $status='A';}
	$cid=$_objAdmin->_dbUpdate(array("status"=>$status),'table_region', " region_id='".$_REQUEST['statusId']."'");
	header("Location: region.php");
}	

include("header.inc.php");



?>
<!-- start content-outer -->
<div id="content-outer">
<!-- start content -->
<div id="content">
<?php if(isset($sus)){?>
	<!--  start message-green -->
	<div id="message-green">
		<table border="0" width="100%" cellpadding="0" cellspacing="0">
		<tr>
			<td class="green-left"><?php echo $sus; ?></td>
			<td class="green-right"><a class="close-green"><img src="images/icon_close_green.gif"   alt="" /></a></td>
		</tr>
		</table>
	</div>
	<?php } ?>
<div id="page-heading"><h1><span style="color: #d74343; font-family: Tahoma; font-weight: bold;"><?php if($_REQUEST['action']=='add'){ echo 'Add Region ';} else if($_REQUEST['action']=='edit'){ echo 'Edit Region';}  else {?>Region List<?php } ?></span></h1></div>
<table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">
<tr>
<td>
	<!--  start content-table-inner -->
	<div id="content-table-inner">

	<table border="0" width="100%" cellpadding="0" cellspacing="0">
	
	<tr valign="top">
	<td>
		<!-- start id-form -->
		<?php  //echo $_REQUEST['action'];
		      switch ($_REQUEST['action']) {
			  case 'add':
				include('region/action/add.php');
				break;
			  case 'edit':
				include('region/action/add.php');
				break;
				
			 default:
			 include('region/action/index.php');
			 break;
				
		} ?>
		<!-- end id-form  -->
	</td>
	<td>
	<!-- right bar-->
	
	</td>
	</tr>
<tr>
<td><img src="images/blank.gif" width="695" height="1" alt="blank" /></td>
<td></td>
</tr>
</table>
<div class="clear"></div>
</div>
<!--  end content-table-inner  -->
</td>
<td id="tbl-border-right"></td>
</tr>
</table>
<div class="clear">&nbsp;</div>
</div>
<!--  end content -->
<div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->
<div class="clear">&nbsp;</div> 
<!-- start footer --> 
   
<?php include("footer.php");?>
<!-- end footer -->
</body>
</html>
