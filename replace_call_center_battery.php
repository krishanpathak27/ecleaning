<?php
include("includes/config.inc.php");
include("includes/function.php");
$_objAdmin = new Admin();
// print_r($_SESSION);

if(($_POST['search_auth_code']!='') && ($_POST['search_auth_code']=='yes'))
{
	if(($_POST['auth_code'] !='') && ($_POST['bsn'])!='')
	{
		$auRec=$_objAdmin->_getSelectList2('table_service_distributor_sp_bcf as BCF left join table_complaint as COMPLAINT on COMPLAINT.complaint_id = BCF.complaint_id left join table_customer as CUST on CUST.customer_id = COMPLAINT.customer_id left join table_account_admin as ADMIN on ADMIN.operator_id = BCF.operator_id','COMPLAINT.*,CUST.*,COMPLAINT.created_date as complaint_register_date,ADMIN.operator_name,BCF.last_updated_date as approved_date','','eapi_auth_code="'.$_POST['auth_code'].'"');
		if(count($auRec)<=0)
		{
			echo "<script>
			alert('Authorization Code not Found');
			window.location.href='".$_SERVER['HTTP_REFERER']."';
			</script>";
		}
	}
	else
	{
		
		echo "<script>
			alert('Please enter Auth Code and BSN');
			window.location.href='".$_SERVER['HTTP_REFERER']."';
			</script>";
	}
}


if(($_POST['replace']!='') && ($_POST['replace']=='yes'))
{
	if($_POST['final_status']=='R')
	{
		if($_SESSION['distributorId']>0)
		{
			header('LOCATION:raisecomplaint_distributor.php?id='.$_POST['complaints_id']);
		}
		else if($_SESSION['retailerId']>0)
		{
			header('LOCATION:raisecomplaint_deler.php?id='.$_POST['complaints_id']);
		}
		else
		{
			echo "<script>
			alert('You are not Authorized to replace battery');
			window.location.href='".$_SERVER['HTTP_REFERER']."';
			</script>";
		}
		
	}
	else
	{
		echo "<script>
			alert('Battery is not Replaced');
			window.location.href='".$_SERVER['HTTP_REFERER']."';
			</script>";
	}
}
?>

<?php include("header.inc.php") ?>
 
 <style>
	.custom
	{
		color: #423b3b;
    	font-size: 12px;
    	line-height: 12px;
    	font-weight: bold;
    	padding-left: 10px;
	}


.formStyle {
    border: 1px solid #ccc;
    color: #393939;
    height: 20px;
    padding: 6px 6px 0px;
    width: 187px;
    border-radius: 5px;
}

.alert-success {
    color: #3C763D;
    background-color: #DFF0D8;
    border-color: #D6E9C6;
}
.alert {
    padding: 15px;
    margin-bottom: 20px;
    border: 1px solid transparent;
    border-radius: 4px;
}

</style>
<script type="text/javascript" src="javascripts/validate.js"></script>
<script src="docsupport/chosen.jquery.js" type="text/javascript"></script>

<script type="text/javascript">
var config = {
  '.chosen-select'           : {},
  '.chosen-select-deselect'  : {allow_single_deselect:true},
  '.chosen-select-no-single' : {disable_search_threshold:10},
  '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
  '.chosen-select-width'     : {width:"100%"}
}
for (var selector in config) {
  $(selector).chosen(config[selector]);
}
</script>
<script src="javascripts/jquery-1.8.2.js" type="text/javascript"></script>
<script src="javascripts/jquery-ui.js"></script>
<script type="text/javascript" src="javascripts/validate.js"></script>

<style>
	.custom
	{
		color: #423b3b;
    	font-size: 12px;
    	line-height: 12px;
    	font-weight: bold;
    	padding-left: 10px;
	}


.formStyle {
    border: 1px solid #ccc;
    color: #393939;
    height: 20px;
    padding: 6px 6px 0px;
    width: 187px;
    border-radius: 5px;
}


</style>
<!-- start content-outer -->
<div id="content-outer">
<!-- start content -->
<div id="content">
<div id="page-heading"><h1><span style="color: #d74343; font-family: Tahoma; font-weight: bold;"> Approved Battery</span></h1></div>
<table border="0" width="100%" cellpadding="0" cellspacing="0">
<!--<tr>
	<th rowspan="3" class="sized"><img src="images/side_shadowleft.jpg" width="20" height="300" alt="" /></th>
	<th class="topleft"></th>
	<td id="tbl-border-top">&nbsp;</td>
	<th class="topright"></th>
	<th rowspan="3" class="sized"><img src="images/side_shadowright.jpg" width="20" height="300" alt="" /></th>
</tr>-->
<tr>
	<td id="tbl-border-left"></td>
	<td>
	<!--  start content-table-inner -->
	<div id="content-table-inner">
	
	<div id="page-heading" align="left" >
	<form name="myform" id="auth_code_form" method="post" action="replace_call_center_battery.php" enctype="multipart/form-data" >
		<div class="row" style="margin:0px;">
	        <div class="col-sm">
	          <div class="panel panel-default">
	            <div class="panel-heading">
	              <h3 class="panel-title">Authorization Code</h3>
	            </div>
	            <div class="panel-body">
	            	<div class="form-group">
			            <div class="row">
		            		<div class="col-sm-6" >
			                <div class="input-group">
			                   <span class="input-group-addon" id="basic-addon1">Complaint No.</span>
			                    <input type="text" class="form-control required"  id="complaint_id" name="complaint_id" placeholder="Complaint Number">
			                </div>
			              </div>
			              <div class="col-sm-6" >
			                <div class="input-group">
			                   <span class="input-group-addon" id="basic-addon1">Complaint Date</span>
			                    <input type="text" class="form-control required datepicker"  id="complaint_date" name="complaint_date" placeholder="Complaint Date" >
			                </div>
			              </div>	              
			            </div>
		            </div>
					<div class="form-group">
			            <div class="row">
		            		<div class="col-sm-6" >
			                <div class="input-group">
			                   <span class="input-group-addon" id="basic-addon1">BSN</span>
			                    <input type="text" class="form-control required"  id="bsn" name="bsn" placeholder="BSN No." value="<?php echo $auRec[0]->bsn; ?>">
			                </div>
			              </div>
			              <div class="col-sm-6" >
			                <div class="input-group">
			                   <span class="input-group-addon" id="basic-addon1">EAPL Authorization Code</span>
			                    <input type="text" class="form-control required"  id="auth_code" name="auth_code" placeholder="EAPI Auth Code" value="<?php echo $auRec[0]->eapi_auth_code; ?>">
			                </div>
			              </div>	              
			            </div>
		            </div>
		            <div class="form-group">
			            <div class="row">
			            	<div class="col-sm-8" >
			                <div class="input-group">
			                	&nbsp;
			                </div>
			                </div>
		            		<div class="col-sm-8" >
			                <div class="input-group">
			                	<input name="account_id" type="hidden" value="<?php echo $_SESSION['accountId']; ?>" />
								<input name="id" type="hidden" value="<?php echo $_REQUEST['id']; ?>" />
								<input name="search_auth_code" type="hidden" value="yes" />

								<input type="button" value="Back" class="form-reset btn btn-default" onclick="location.href='replace_call_center_battery.php'" style="padding-bottom: 25px; padding-left: 25px; padding-right: 25px;background: #d9534f;background-image: linear-gradient(to bottom, #d9534f 0px, #c12e2a 100%);color:#fff;"/>

								<input name="submit" class="form-submit btn btn-default" type="submit" id="submit" value="Search" style="padding-bottom: 25px; padding-left: 25px; padding-right: 25px;background-image: linear-gradient(to bottom, #5cb85c 0px, #419641 100%);background-color: #5cb85c;color:#fff;"/>

								<input type="hidden" name="search_code" value="yes">
			                </div>
			              </div>
			              
			             </div>
			            </div>
		        </div>
		     </div>
		    </div>
		 </div>
		 </form>
		 <form name="approval_form" action="replace_call_center_battery.php" method="post">
		 <div class="row" style="margin:0px;">
	        <div class="col-sm">
	          <div class="panel panel-default">
	            <div class="panel-heading">
	              <h3 class="panel-title">Customer Details</h3>
	            </div>
	            <div class="panel-body">
					<div class="form-group">
			            <div class="row">
		            		<div class="col-sm-3" >
			                <div class="input-group">
			                   <span class="input-group-addon" id="basic-addon1">Customer Name</span>
			                    <input type="text" class="form-control <?php echo $classRequire;?>"  id="customer_name" name="customer_name" readonly placeholder="Customer Name" value="<?php echo $auRec[0]->customer_name; ?>">
			                </div>
			              </div>
			              <div class="col-sm-3" >
			                <div class="input-group">
			                   <span class="input-group-addon" id="basic-addon1">Mobile No.</span>
			                    <input type="text" class="form-control <?php echo $classRequire;?>"  id="customer_phone_no" name="customer_phone_no" readonly placeholder="Mobile No." value="<?php echo $auRec[0]->customer_phone_no; ?>">
			                </div>
			              </div>
		              <div class="col-sm-3" >
		            	<div class="input-group">
		                   <span class="input-group-addon" id="basic-addon1">Alternate No.</span>
		                    <input type="text" class="form-control required"  id="customer_phone_no2" name="customer_phone_no2" readonly placeholder="Alternate No. " value="<?php echo $auRec[0]->customer_phone_no2; ?>" readonly>
		                </div>
		                </div>
		        		<div class="col-sm-3" >
			                <div class="input-group">
			                   <span class="input-group-addon" id="basic-addon1">Email Id</span>
			                    <input type="text" class="form-control <?php echo $classRequire;?>"  id="customer_email" name="customer_email" readonly placeholder="Email ID" value="<?php echo $auRec[0]->customer_email; ?>">
			                </div>
			              </div>
			              
			              </div>              
			            </div>
			            <div class="form-group">
			            <div class="row">
		            		<div class="col-sm-12" >
			                <div class="input-group">
			                   <span class="input-group-addon" id="basic-addon1">Customer Address</span>
			                    <input type="text" class="form-control <?php echo $classRequire;?>"  id="customer_address" name="customer_address" readonly placeholder="Customer Address" value="<?php echo $auRec[0]->customer_address; ?>">
			                </div>
			              </div>
			              </div>              
			            </div>
		            </div>
		        </div>
		     </div>
		    </div>
		    <div class="row" style="margin:0px;" id="cust_details">
	        <div class="col-sm">
	          <div class="panel panel-default">
	            <div class="panel-heading">
	              <h3 class="panel-title">Approval By Call Center</h3>
	            </div>
	            <div class="panel-body">
					<div class="form-group">
			            <div class="row">
		            		<div class="col-sm-6" >
			                <div class="input-group">
			                   <span class="input-group-addon" id="basic-addon1">Call Center Operator</span>
			                    <input type="text" class="form-control"  id="operator_id" name="operator_id" readonly placeholder="Customer Name" value="<?php echo $auRec[0]->operator_name; ?>">
			                </div>
			              </div>
			              <div class="col-sm-6" >
			                <div class="input-group">
			                   <span class="input-group-addon" id="basic-addon1">Approved Date</span>
			                    <input type="text" class="form-control"  id="approved_date" name="approved_date" readonly placeholder="Mobile No." value="<?php echo $auRec[0]->approved_date; ?>">
			                </div>
			              </div>
		              
		        		     
			              </div>              
			            </div>
			            
		            </div>
		        </div>
		     </div>
		    </div>
		    <div class="row" style="margin:0px;">
	        <div class="col-sm">
	          <div class="panel panel-default">
	            <div class="panel-heading">
	              <h3 class="panel-title">Replace / Not Replace</h3>
	            </div>
	            <div class="panel-body">
					<div class="form-group">
			            <div class="row">
		            		<div class="col-sm-12" >
			                <div class="input-group">
			                   <span class="input-group-addon" id="basic-addon1">Approve</span>
			                    <select class="form-control required"  id="final_status" name="final_status"  placeholder="Status">
										<option value="N">Not Replace</option>
										<option value="R">Replace</option>
								</select>

			                </div>
			              </div>
			              
			             </div>
			            </div>
			            <div class="form-group">
			            <div class="row">
			            	<div class="col-sm-8" >
			                <div class="input-group">
			                	&nbsp;
			                </div>
			                </div>
		            		<div class="col-sm-8" >
			                <div class="input-group">
			                	<input name="account_id" type="hidden" value="<?php echo $_SESSION['accountId']; ?>" />
								<input name="id" type="hidden" value="<?php echo $auRec[0]->service_distributor_sp_bcf_id; ?>" />
								<input name="complaints_id" type="hidden" value="<?php echo $auRec[0]->complaint_id;?>" />
								<input name="replace" type="hidden" value="yes" />
								<?php 
									if(($_POST['search_auth_code']!='') && ($_POST['search_auth_code']=='yes'))
									{

										echo '<input name="submit" class="form-submit btn btn-default" type="submit" id="submit" value="Save" style="padding-bottom: 25px; padding-left: 25px; padding-right: 25px;background-image: linear-gradient(to bottom, #5cb85c 0px, #419641 100%);background-color: #5cb85c;color:#fff;" />';
									}
								?>
			                </div>
			              </div>
			              
			             </div>
			            </div>		            
		            </div>
		        </div>
		     </div>
		    </div>
	</form>
	</div>

	<table border="0" width="100%" cellpadding="0" cellspacing="0">
	<tr valign="top">
	<td>
		<!-- start id-form -->
		<table  border="0" cellspacing="0" cellpadding="0"  align="center" width="100%">
				<tr>
				<td align="lift" valign="top"  >
				<table id="flex1" style="display:none"></table>
					<script type="text/javascript">distributorcomplaint();</script>
				</td>
				</tr>
				</table>
	<!-- end id-form  -->

	</td>
	<td>

	<!-- right bar-->
</td>
</tr>
<tr>
<td></td>
<td></td>
</tr>
</table>
<div class="clear"></div>
</div>
<!--  end content-table-inner  -->
</td>
<td id="tbl-border-right"></td>
</tr>
<tr>
	<th class="sized bottomleft"></th>
	<td id="tbl-border-bottom">&nbsp;</td>
	<th class="sized bottomright"></th>
</tr>
</table>
<div class="clear">&nbsp;</div>
</div>
<!--  end content -->
<div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->
<div class="clear">&nbsp;</div> 
<!-- start footer -->         
<?php include("footer.php") ?>
<!-- end footer -->
 
</body>
</html>
<link href="bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<link href="bootstrap/dist/css/bootstrap-theme.min.css" rel="stylesheet">
<style>
	.form-control
	{
		color:black;
	}
</style>
<script src="javascripts/jquery-1.12.4.js"></script>
<script src="javascripts/jquery-ui-1.12.1.js"></script>

<script type="text/javascript" src="javascripts/validate.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script type="text/javascript">
  

  $(function() {
    $( ".datepicker" ).datepicker({
     // showOn: "button",
     // buttonImage: "images/calendar.gif",
      //buttonImageOnly: true,
      //buttonText: "Select date",
      dateFormat: "d M yy",
      defaultDate: "w",
      changeMonth: true,
      numberOfMonths: 1
    });
  });


  
</script>
