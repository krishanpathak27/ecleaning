<?php
include("includes/config.inc.php");
include("includes/function.php");
include("includes/globalarraylist.php");

//print_r($_POST);

if(isset($_POST['schedule_add']) && $_POST['schedule_add']=='yes'){ 
	
	if(isset($_POST['rpt_schedule_id']) && $_POST['rpt_schedule_id']!='') {
	
		$_objAdmin->updateEmailSchedule($_POST['rpt_schedule_id']); 
		$_SESSION['SESS_MSG'] = "This user schedule has been updated successfully.";
		//header('location: reports_schedule.php');
		
	} else {
	
		$result = $_objAdmin->_getSelectList('table_report_schedule',"rpt_schedule_id",''," user_type = ".$_POST['user_type']." AND user_id = ".$_POST['user_id'].""); 
		
		if(count($result)==0) {
			//echo "<pre>"; print_r($_POST);exit;	
			$_objAdmin->addEmailSchedule(); 
			$_SESSION['SESS_MSG'] ="This user schedule has been added successfully.";
			//header('location: reports_schedule.php');
		}
		else {
			$_SESSION['SESS_MSG'] = "This user schedule already exists in the system.";
			//header('location: reports_schedule.php');
		}	
	}
}


// Post Request while select user type

if(isset($_POST['getuserType']) && $_POST['getuserType']!="" && is_numeric($_POST['getuserType'])) {
 	echo $_objRptGenClass->getUserType($_POST['getuserType']); exit;
} 


// Post Request load default user type menu
if(isset($_POST['defaultValue']) && $_POST['defaultValue']!="") {
	echo $_objRptGenClass->getUserType(); exit;
}
	

// Post Request while select user type

if(isset($_POST['userType']) && $_POST['userType']!="") {
 echo $optionData = $_objRptGenClass->getUserTypeList($_POST['userType'],$_POST['defaultselected'],$_POST['selectedvalue']); exit;
}

// Post Request while select user type

	
if(isset($_POST['type']) && $_POST['type']!="") {
	$result = array();
	$reportData = array();
	
	if($_POST['user_id']!='') { $result = $_objAdmin->_getSelectList('table_report_schedule',"rpt_schedule_id",''," user_type = ".$_POST['type']." AND user_id = ".$_POST['user_id'].""); } 
	
	
	$reportData = $_objRptGenClass->getReportsList($_POST['type'], $_POST['defaultselected'], $_POST['selectedvalue']);
	echo json_encode(array($result[0]->rpt_schedule_id, "skip", $reportData)); 
 	exit;
}

if(isset($_POST['rpt_id']) && is_numeric($_POST['rpt_id'])) { 

	$auRec = $_objAdmin->_getSelectList('table_report_schedule',"*",''," rpt_schedule_id = ".$_POST['rpt_id']."");
	//print_r($auRec);
	echo $jsondata = json_encode($auRec[0]);
	//echo '['.$jsondata.']';
	exit;
	/*$type = $auRec[0]->user_type;
	$selected = $auRec[0]->user_id;
	$selected_report_id = explode(',',$auRec[0]->report_id);
	$from_date = $auRec[0]->from_date;
	$to_date = $auRec[0]->to_date;
	$sent_nature = $auRec[0]->sent_nature;
	$weekday = $auRec[0]->weekday;*/
}
	
include("header.inc.php");?>
<script type="text/javascript" src="javascripts/report_schedule.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	var rpt_id = sessionStorage.getItem("rpt_id");
	//alert(rpt_id);
	//alert($.isNumeric(rpt_id));

 	showDetails(rpt_id);

	$('.interval').click(function(){
	var status = $(this).val();
	//alert(status);
	if(status == 2) { 
		$('select[name="weekday"]').addClass('required'); $('#weekly').show(); 
	}
	else { 
		$('select[name="weekday"]').find('option').removeAttr('selected'); 
		$('select[name="on_time"]').find('option').removeAttr('selected');
		$('select[name="weekday"]').removeClass('required'); 
		$('#weekly').hide(); 
	}		
	});
});
</script>

<style type="text/css">
.checkboxListStyle {
line-height:20px;
text-transform: uppercase;
font-size: 12px;
font-family: 'RobotoBlack';
color: #000000;
list-style:none;
}

.optlegend {
background: none repeat scroll 0 0 #6E6E6E;
border-radius: 0.2em 0.2em 0.2em 0.2em;
color: #FFFFFF;
font-family: arial;
font-size: 17px;
font-weight: bold;
padding: 3px;
}
</style>
<div id="content-outer">
<!-- start content -->
<div id="content">
<div id="savingButton" style="display:none;"><center>Do you want to send SMS to selected retailers?</center></div>
<div id="loadingAjax" style="display:none;"><center><img src="images/ajax-loader.gif"><br/> We are loading data</center></div>
<div id="savingDataAjax" style="display:none;"><center><img src="images/ajax-loader.gif"><br/> We are saving your requested data</center></div>
<div id="page-heading"><h1><span style="color: #d74343; font-family: Tahoma; font-weight: bold;"><?php if(isset($_GET['id'])){?> <?php } else {?> <?php }?> Report Emailer Schedule</span></h1></div>
<table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">
<tr>
	<td>
	<!--  start content-table-inner -->
	<div id="content-table-inner">
	<div id="error"></div>
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
	<tr valign="top">
	<td>
	<!-- start id-form -->
	<?php if($rou_name_err!=''){?>
	<div id="message-red">
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
	<tr>
		<td class="red-left">Error. <?php echo $rou_name_err; ?></td>
		<td class="red-right"><a class="close-red"><img src="images/icon_close_red.gif"   alt="" /></a></td>
	</tr>
	</table>
	</div>
	<?php } ?>
	<!--  end message-red -->
	<?php if($sus!=''){?>
	<!--  start message-green -->
	<div id="message-green">
		<table border="0" width="100%" cellpadding="0" cellspacing="0">
		<tr>
			<td class="green-left"><?php echo $sus; ?></td>
			<td class="green-right"><a class="close-green"><img src="images/icon_close_green.gif"   alt="" /></a></td>
		</tr>
		</table>
	</div>
	<?php } ?>
	
	<?php require('includes/error.msg.inc.php'); ?>
	
	
	<!--  end message-green -->
	<form name="frmPre" id="frmPre" method="post" enctype="multipart/form-data">
	<input type="hidden" name="selected_user_type" id="selected_user_type" />
	<input type="hidden" name="selected_user_id" id="selected_user_id"  />
	<!--<input type="text" name="selected_report_id" id="selected_report_id"  />-->
	<input type="hidden" name="rpt_schedule_id" id="rpt_schedule_id"  />
	
	<input name="account_id" type="hidden" value="<?php echo $_SESSION['accountId']; ?>" />
	<table cellpadding="0" cellspacing="0" border="0" id="id-form">
	<tr>
		<td colspan="4"> 
			<table width="100%" style="padding:0px; border-radius:0.5em;">
			<tr>
			<th width="28%">User Type: &nbsp;</th> 
			<td><select name="user_type" id="user_type" class="required styledselect_form_4" onchange="loadURL(this.value, 'single', '')" style="width:350px;"><?php //echo $_objRptGenClass->getUserType();?></select></td>
			</tr>
			</table>
		</td>
	</tr>
		
	
	<tr>
		<td colspan="4"> 
			<table width="100%" style="padding:0px; border-radius:0.5em;">
			<tr>
			<th width="28%">Select User: &nbsp;</th> 
			<td><select name="user_id" id="user_id" class="required styledselect_form_4" onchange="loaddata(this.value, user_type.value,'checkall','')" style="width:350px;"><option value="">Please Select</option></select></td>
			</tr>
			</table>
		</td>
	</tr>
	
	
	<tr>
		<td colspan="4"> 
			<table width="100%" style="padding:0px; border-radius:0.5em;">
			<tr>
			<th width="28%">Reports: &nbsp;</th> 
			<td><div id="report_id" style="height: 100px; width:350px; overflow: scroll;"></div></td>
			</tr>
			</table>
		</td>
	</tr>	
	
	<tr>
	<td colspan="4"> 
	<table width="100%" border="0" style="padding:0px; border-radius:0.5em;">
	<tr><th width="28%">Date Range from: &nbsp;</th> 
	<td width="69%" align="left" colspan="2"><input type="text" id="from" name="from" class="date" style="width:140px" value="<?php echo date('Y-m-d');?>"  readonly />To: &nbsp;<input type="text" id="to" name="to" class="date" style="width:140px" value="<?php echo date('Y-m-d');?>" readonly /></td>
	</tr>
	</table>
	</td>
	</tr>
	

	<tr>
		<td colspan="4"> 
			<table width="100%" border="0" style="padding:0px; border-radius:0.5em;">
			<tr>
			<th width="28%">Interval: &nbsp;</th> 
			<td width="69%" align="left"><input type="radio" name="sent_nature" class="interval" value="2" />Weekly &nbsp;
	<input type="radio" name="sent_nature" class="interval" value="1" />Daily</td>	
			</tr>
			</table>
		</td>
	</tr>	
	
	<tr>
	<td colspan="4" id="weekly">
	<table width="100%" border="0" style="padding:0px; border-radius:0.5em;">
		<tr><td width="22%"><strong>Select Day:</strong> &nbsp;</td>
	<td width="53%"><select name="weekday" class="styledselect_form_4"><option value="">Please Select</option><?php foreach($_objAdmin->weekArray as $key=>$val):?><option value="<?php echo $key;?>"  <?php if($weekday == $key){?> selected="selected"<?php }?> ><?php echo $val;?></option><?php endforeach;?></select></td></tr></table>
	</td>
	</tr>


	
	<tr>
	<th width="146">Multiple Emails:<br /> with (,) seperated</th>
	<td width="340"><textarea name="additional_emails" id="additional_emails" class="text" style="height:40px; width:325px;"></textarea></td>
	</tr>
	

	<tr>
		<th valign="top">Status:</th>
		<td> <select name="status" id="status" class="styledselect_form_3"><option value="A">Active</option><option value="I">Inactive</option></select>
			</td>
			<td width="4"></td>
		</tr>



	<tr>
	<td valign="top" colspan="2">
		<input type="hidden" name="schedule_add" value="yes"  />
		<input type="button" value="Back" class="form-reset" onclick="location.href='report_schedule_list.php';" />
		<!--<input name="submit" class="form-submit saveSend" type="button" id="emailer" onclick="ValidateSave()" value="Save" />-->		<input name="submit" class="form-submit saveSend" type="submit" value="Save" />
	</td>
	</tr>
</table>
</form>
	
<!-- end id-form  -->
</td>
</tr>
<tr>
</tr>
</table>
<div class="clear"></div>
</div>
<!--  end content-table-inner  -->
</td>
<td id="tbl-border-right"></td>
</tr>
</table>
<div class="clear">&nbsp;</div>
</div>
<!--  end content -->
<div class="clear">&nbsp;</div>
</div>
   
<?php include("footer.php");?>
<script>$.noConflict(); // Code that uses other library's $ can follow here.</script>
<script src="javascripts/jquery-1.8.2.js" type="text/javascript"></script>
<script src="javascripts/jquery-ui.js"></script>
<script type="text/javascript" src="javascripts/validate.js"></script>

<script type="text/javascript">
$(document).ready(function(){
		$( "#from" ).datepicker({
			dateFormat: "yy-mm-dd", 
			defaultDate: "w",
            changeMonth: true,
			changeYear: true, 
			onSelect: function( selectedDate ) {
                $( "#to" ).datepicker( "option", "minDate", selectedDate );
            }
			});
	 $( "#to" ).datepicker({
			dateFormat: "yy-mm-dd", 
			defaultDate: "-w",
            changeMonth: true,
			changeYear: true, 
			onSelect: function( selectedDate ) {
                $( "#from" ).datepicker( "option", "maxDate", selectedDate );
            }
			}
		);
	});
</script>
</body>
</html>