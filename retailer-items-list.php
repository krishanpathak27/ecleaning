<?php
include("includes/config.inc.php");
include("includes/function.php");
$_objAdmin = new Admin();
$orderDetails=$_objAdmin->_getSelectList2('table_order_detail AS D 
		LEFT JOIN table_order AS O ON D.order_id = O.order_id
		LEFT JOIN table_item AS I ON D.item_id = I.item_id
		LEFT JOIN table_price AS P ON D.item_id = P.item_id
		LEFT JOIN table_salesman AS S ON O.salesman_id = S.salesman_id
		LEFT JOIN table_retailer AS R ON O.retailer_id = R.retailer_id
		LEFT JOIN table_distributors AS Dis ON O.distributor_id = Dis.distributor_id',
	'D.price, D.quantity, D.total, I.item_name, I.item_code, P.item_mrp, O.order_id, S.salesman_name, Dis.distributor_name,R.retailer_name, O.date_of_order, O.time_of_order',''," 
	I.item_id=".$_GET['id']." AND D.type=1 AND O.account_id=".$_SESSION['accountId']." AND date_of_order = '".$_GET['date_of_order']."'");
/*	echo "<pre>";
	print_r($orderDetails);
	echo "</pre>";*/
?>


<?php //include("header.inc.php") ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta charset="utf-8">
<title>Order detail of items</title>
<link rel="stylesheet" href="css/stylesheet.css" type="text/css" />
<link rel="stylesheet" type="text/css" href="css/flexigrid.css">
<link rel="stylesheet" href="css/jquery-ui.css" />
<style>
	.list {
		float:left;
		
	}
	.list li {
		list-style:inside;
	}
</style>
</head>


<body>

<!-- start content-outer -->
<div id="content-outer">
<!-- start content -->
<div id="content">
<div id="page-heading">
<h1>
	<span style="color: #d74343; font-family: Tahoma; font-weight: bold;"><?php echo $orderDetails[0]->item_name;?></span>
</h1>
	<ul class="list">
		<li><strong>Item Name : </strong> <?php echo $orderDetails[0]->item_name;?></li>
		<li><strong>Item Code : </strong> <?php echo $orderDetails[0]->item_code;?></li>
		<li><strong>Item MRP  : </strong> <?php echo $orderDetails[0]->price;?></li>
		<li><strong>Date of order  : </strong> <?php echo $_objAdmin->_changeDate($orderDetails[0]->date_of_order);?></li>
		<!--<li><strong>Time of order  : </strong> <?php echo $orderDetails[0]->time_of_order;?></li>-->
	</ul>
</div>
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" id="content-table">
<!--<tr>
	<th rowspan="3" class="sized"><img src="images/side_shadowleft.jpg" width="20" height="300" alt="" /></th>
	<th class="topleft"></th>
	<td id="tbl-border-top">&nbsp;</td>
	<th class="topright"></th>
	<th rowspan="3" class="sized"><img src="images/side_shadowright.jpg" width="20" height="300" alt="" /></th>
</tr>
-->
<tr>
	<!--<td id="tbl-border-left"></td>-->
	<td align="center" valign="middle">
	<!--  start content-table-inner -->
	<div id="content-table-inner">

	
	
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
		
		<!--<tr>
			    <td>
					<table border="0" width="100%" cellpadding="0" cellspacing="0" id="id-form">
						<tr bgcolor="" style="color: #fff;font-weight: bold;">
							<td style="padding:10px;">Item Name</td>
							<td style="padding:10px;"><?php echo $orderDetails[0]->item_name;?></td>
						</tr>
						<tr bgcolor="" style="color: #fff;font-weight: bold;">
							<td style="padding:10px;">Item Code</td>
							<td style="padding:10px;"><?php echo $orderDetails[0]->item_code;?></td>
						</tr>
						<tr bgcolor="" style="color: #fff;font-weight: bold;">
							<td style="padding:10px;">Item Price</td>
							<td style="padding:10px;"><?php echo $orderDetails[0]->item_mrp;?></td>
						</tr>
					</table>

				</td>

			</tr>-->
		<tr valign="top">
		<td>
		<table border="1" width="100%" cellpadding="0" cellspacing="0" id="id-form">
			<tr bgcolor="#6E6E6E" style="color: #fff;font-weight: bold;">
			    <td style="padding:10px;">S.No</td>
				<td style="padding:10px;" align="left">Distributor</td>
				<td style="padding:10px;">Salesman</td>
				<td style="padding:10px;" align="left">Retailer</td>
				
				<td style="padding:10px;" align="right">Quantity</td>
				<td style="padding:10px;" align="right">Price</td>
				<td style="padding:10px;" align="right">Total</td>
				<td style="padding:10px;" align="center">View Order</td>
			</tr>

		<?php 
		$sum = 0;
		$quan = 0;
		foreach($orderDetails as $key=>$value): $sum = $sum + $orderDetails[$key]->total; $quan = $quan +  $orderDetails[$key]->quantity;
		  
		?>
		<tr>
			<td style="padding:10px;" ><?php echo $key+1;?></td>
			<td style="padding:10px;" align="left"><?php echo $orderDetails[$key]->distributor_name;?></td>
			<td style="padding:10px;" ><?php echo $orderDetails[$key]->salesman_name;?></td>
			<td style="padding:10px;" align="left"><?php echo $orderDetails[$key]->retailer_name;?></td>
			
			<td style="padding:10px;" align="right"><?php echo $orderDetails[$key]->quantity;?></td>
			<td style="padding:10px;" align="right"><?php echo $orderDetails[$key]->price;?></td>
			<td style="padding:10px;" align="right"><?php echo $orderDetails[$key]->total;?></td>
			<td style="padding:10px;" align="center"><a href="admin_order_list.php?id=<?php echo $orderDetails[$key]->order_id;  ?>" target="_blank">View</a></td>
		</tr>
		<?php endforeach;?>
		<tr bgcolor="#6E6E6E" style="color: #fff;font-weight: bold;">
			<td style="padding:10px;" ></td>

			<td style="padding:10px;" ></td>
			<td style="padding:10px;" align="left"></td>
			<td style="padding:10px;" align="left"></td>
			<td style="padding:10px;" align="right"><?php echo number_format($quan);?></td>
			<td style="padding:10px;" align="right"></td>
			<td style="padding:10px;" align="right"><?php echo number_format($sum,2);?></td>
			<td style="padding:10px;" align="center"></td>
		</tr>

		</table>
		
	</td>
	</tr>
	</table>
	</div>
	</td>
</tr>
</table>
<div class="clear"></div>
</div>
<!--  end content-table-inner  -->
</td>
<td id="tbl-border-right"></td>
</tr>
<!--<tr>
	<th class="sized bottomleft"></th>
	<td id="tbl-border-bottom">&nbsp;</td>
	<th class="sized bottomright"></th>
</tr>-->
</table>
<div class="clear">&nbsp;</div>
</div>
<!--  end content -->
<div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->
<div class="clear">&nbsp;</div> 
<!-- start footer -->         
<?php include("footer.php") ?>
<script type="text/javascript"> 
$('.example3demo').popupWindow({ 
centerScreen:1 
}); 
</script>
<!-- end footer -->

</body>
</html>