<?php

if (isset($_REQUEST['statusId']) && $_REQUEST['value'] != "") {
	if ($_REQUEST['value'] == "Active") {
		$status = 'I';
		$end_date = $date;
		$cid = $_objAdmin->_dbUpdate(array(
			"status" => $status,
			"end_date" => $end_date,
			"last_update_date" => $date,
			"last_update_status" => 'Update'
		) , 'table_retailer', " retailer_id='" . $_REQUEST['statusId'] . "'");
		$wid = $_objAdmin->_dbUpdate(array(
			"status" => $status
		) , 'table_web_users', " retailer_id='" . $_REQUEST['statusId'] . "'");
		header("Location: retailer.php");
	}
	else {
		$status = 'A';
		$end_date = $_SESSION['EndDate'];
		if ($account = $_objAdmin->getAccountRec() > 0) {
			$cid = $_objAdmin->_dbUpdate(array(
				"status" => $status,
				"end_date" => $end_date,
				"last_update_date" => $date,
				"last_update_status" => 'Update'
			) , 'table_retailer', " retailer_id='" . $_REQUEST['statusId'] . "'");
			$wid = $_objAdmin->_dbUpdate(array(
				"status" => $status
			) , 'table_web_users', " retailer_id='" . $_REQUEST['statusId'] . "'");
			header("Location: retailer.php");
		}
		else {
			header("Location: retailer.php?err=err");
		}
	}
}

if (isset($_REQUEST['statusLogId']) && $_REQUEST['value'] != "") {
	if ($_REQUEST['value'] == "Active") {
		$status = 'I';
		$cid = $_objAdmin->_dbUpdate(array(
			"status" => $status
		) , 'table_web_users', " retailer_id='" . $_REQUEST['statusLogId'] . "'");
		header("Location: retailer.php");
	}
	else {
		$status = 'A';
		if ($account = $_objAdmin->getAccountRec() > 0) {
			$cid = $_objAdmin->_dbUpdate(array(
				"status" => $status
			) , 'table_web_users', " retailer_id='" . $_REQUEST['statusLogId'] . "'");
			header("Location: retailer.php");
		}
		else {
			header("Location: retailer.php?err=err");
		}
	}
}

if (isset($_POST['retailer_add']) && $_POST['retailer_add'] == 'yes') {

	if ($_POST['id'] == "") {
		// echo 'aa'; exit;
		$condi = " (retailer_name='" . mysql_escape_string($_POST['retailer_name']) . "') and retailer_phone_no='" . mysql_escape_string($_POST['retailer_number']) . "'  and status!='D' and retailer_id<>'" . $_POST['ret_id'] . "'";
	} else {
		$condi = " (retailer_name='" . mysql_escape_string($_POST['retailer_name']) . "') and retailer_phone_no='" . mysql_escape_string($_POST['retailer_number']) . "' and and status!='D' account_id='" . $_POST['account_id'] . "') ";
	}

	$auRec = $_objAdmin->_getSelectList('table_retailer', "*", '', $condi);

	if (is_array($auRec)) {
		$ret_name_err = "Retailer already exists in the system.";
		$_SESSION['ret_err'] = $_POST['ret_id'];
		$auRec[0] = (object)$_POST;

	} else {
		
		if ($_POST['ret_id'] != "") {

			$scode = "retailer_code='" . mysql_escape_string($_POST['retailer_code']) . "' and retailer_id!='" . $_POST['ret_id'] . "'";
			$dsupdate = $_objAdmin->_getSelectList('table_retailer', "*", '', $scode);

			if (is_array($dsupdate)) {

				$ret_name_err = "Retailer code already exists in the System";
				$_SESSION['ret_err'] = $_POST['ret_id'];
				$auRec[0] = (object)$_POST;

			} else {

				if ($_POST['status'] == "A") {
					if ($_POST['save'] == "Save") {

						$_objAdmin->mysql_query("delete from table_route_retailer where retailer_id='". $_REQUEST['ret_id']."' AND status='R'");
						$_objAdmin->mysql_query("delete from table_retailer_to_distributer where retailer_id='".$_REQUEST['ret_id']."'");

						$_objAdmin->UpdateRetailer($_POST["ret_id"]);
						$cat_id = $_objAdmin->addRetCat($_POST["ret_id"]);

						$sus = "Dealer has been Updated successfully.";

					} else {

						if ($account = $_objAdmin->getAccountRec() > 0) {

							$wid=$_objAdmin->_dbUpdate(array("status" => 'A'),'table_web_users'," retailer_id='".$_POST["ret_id"]. "'");

							$_objAdmin->mysql_query("delete from table_route_retailer where retailer_id='".$_REQUEST['ret_id']."' AND status='R'");
							$_objAdmin->mysql_query("delete from table_retailer_to_distributer where retailer_id='".$_REQUEST['ret_id']. "'");

							$_objAdmin->UpdateRetailer($_POST["ret_id"]);
							$cat_id = $_objAdmin->addRetCat($_POST["ret_id"]);


							if ($_POST['submit'] == 'Approve & Exit' || $_POST['submit'] == 'Approve & Continue') {
								header("Location: new_retailer.php");
							}

							$ret_add_sus = "Dealer has been Updated successfully.";
							$_SESSION['retId'] = $_POST['ret_id'];
							$_SESSION['update'] = 'yes';
							unset($_SESSION['ret_err']);

						} else {

							
							$_objAdmin->mysql_query("delete from table_route_retailer where retailer_id='".$_REQUEST['ret_id']."' AND status='R'");
							$_objAdmin->mysql_query("delete from table_retailer_to_distributer where retailer_id='".$_REQUEST['ret_id']. "'");
							$_objAdmin->UpdateRetailer($_POST["ret_id"]);
							$cat_id = $_objAdmin->addRetCat($_POST["ret_id"]);


							if ($_POST['submit'] == 'Approve & Exit' || $_POST['submit'] == 'Approve & Continue') {
								header("Location: new_retailer.php");
							}

							$_SESSION['retId'] = $_POST['ret_id'];
							$_SESSION['update'] = 'yes';
							unset($_SESSION['ret_err']);

							// $ret_name_err = "Your record get updated successfully but you can't continue here because of maximum limit of active users has been exceeded. ";
							// newly added

							unset($_REQUEST['add']);
							unset($ret_name_err);
							$_GET['skip'] = "";
							$_SESSION['retId'] = $_POST['ret_id'];
							$login_sus = "Dealer has been Added successfully.";
							$_GET['skip'] = "";
						}
					}

				} else {

					if ($_POST['save'] == "Save") {

						$_objAdmin->mysql_query("delete from table_route_retailer where retailer_id='".$_REQUEST['ret_id']."' AND status='R'");
						$_objAdmin->mysql_query("delete from table_retailer_to_distributer where retailer_id='".$_REQUEST['ret_id']."'");

						$_objAdmin->UpdateRetailer($_POST["ret_id"]);
						$cat_id = $_objAdmin->addRetCat($_POST["ret_id"]);

						$sus = "Dealer has been Updated successfully.";

					} else {

						if ($account = $_objAdmin->getAccountRec() > 0) {

							$wid = $_objAdmin->_dbUpdate(array("status" => 'I'),'table_web_users'," retailer_id='".$_POST["ret_id"]."'");

							$_objAdmin->mysql_query("delete from table_route_retailer where retailer_id='" . $_REQUEST['ret_id'] . "'  AND status='R'");
							$_objAdmin->mysql_query("delete from table_retailer_to_distributer where retailer_id='" . $_REQUEST['ret_id'] . "'");

							$_objAdmin->UpdateRetailer($_POST["ret_id"]);
							$cat_id = $_objAdmin->addRetCat($_POST["ret_id"]);

							if ($_POST['submit'] == 'Approve & Exit' || $_POST['submit'] == 'Approve & Continue') {
								header("Location: new_retailer.php");
								exit;
							}

							$ret_add_sus = "Dealer has been Updated successfully.";
							$_SESSION['retId'] = $_POST['ret_id'];
							$_SESSION['update'] = 'yes';
							unset($_SESSION['ret_err']);

						} else {

							
							$_objAdmin->mysql_query("delete from table_route_retailer where retailer_id='".$_REQUEST['ret_id']."' AND status='R'");
							$_objAdmin->mysql_query("delete from table_retailer_to_distributer where retailer_id='".$_REQUEST['ret_id']. "'");

							$_objAdmin->UpdateRetailer($_POST["ret_id"]);
							$cat_id = $_objAdmin->addRetCat($_POST["ret_id"]);

							if ($_POST['submit'] == 'Approve & Exit' || $_POST['submit'] == 'Approve & Continue') {
								header("Location: new_retailer.php");
							}

							$_SESSION['retId'] = $_POST['ret_id'];
							$_SESSION['update'] = 'yes';
							unset($_SESSION['ret_err']);

							// $ret_name_err = "Your record get updated successfully but you can't continue here because of maximum limit of active users has been exceeded. ";
							// newly added

							unset($_REQUEST['add']);
							unset($ret_name_err);
							$_GET['skip'] = "";
							$_SESSION['retId'] = $_POST['ret_id'];
							$login_sus = "Dealer has been Added successfully.";
							$_GET['skip'] = "";
						}
					}
				}
			}
		}
		else {

			// if($account=$_objAdmin->getAccountRec()>0){

			$scode = "retailer_code='" . mysql_escape_string($_POST['retailer_code']) . "' and retailer_id!='" . $_POST['ret_id'] . "'";
			$dsupdate = $_objAdmin->_getSelectList('table_retailer', "*", '', $scode);
			if (is_array($dsupdate)) {
				$ret_name_err = "Retailer code already exists in the System";
				$_SESSION['ret_err'] = $_POST['ret_id'];
				$auRec[0] = (object)$_POST;
			}
			else {
				$ret_id = $_objAdmin->addRetailer();
				if ($ret_id != '') {
					$cat_id = $_objAdmin->addRetCat($ret_id);
					$ret_add_sus = "Dealer has been Added successfully.";
					$_SESSION['retId'] = $ret_id;
					unset($_SESSION['ret_err']);
				}
			}

			// }
			// else {
			// $ret_name_err="You have exceeded the maximum limit of active users";
			// }

		}
	}
}

if (isset($_POST['ret_login']) && $_POST['ret_login'] == 'yes') {

	// if($account=$_objAdmin->getAccountRec()>0){

	if ($_POST['web_id'] != "") {
		$condi = " username='" . $_POST['username'] . "' and web_user_id<>'" . $_POST['web_id'] . "'";
	}
	else {
		$condi = " username='" . $_POST['username'] . "'";
	}

	$auRec = $_objAdmin->_getSelectList('table_web_users', "*", '', $condi);
	if (is_array($auRec)) {
		$login_err = "User name already exists in the system.";
		$_SESSION['retId'] = $_POST['ret_id'];
		$auRec[0] = (object)$_POST;
	}
	else {
		if ($_POST['web_id'] != '') {
			$_objAdmin->UpdateLogin($_POST["web_id"]);
			$login_sus = "Dealer has been Added successfully.";
			$_SESSION['retId'] = $_POST['ret_id'];
		}
		else {
			$web_id = $_objAdmin->addLogin();
			if ($web_id != '') {
				$login_sus = "Dealer has been Added successfully.";
			}
		}
	}

	// } else {
	// $login_err="you have exceeded the maximum limit of active users.";
	// $_SESSION['retId']=$_POST['ret_id'];
	// }

}

if (isset($_POST['ret_map']) && $_POST['ret_map'] == 'yes') {
	$_objAdmin->updateRetailerMap($_POST["ret_id"]);
	if ($_SESSION['update'] == '') {
		$sus = "Dealer has been Added successfully.";
	}
	else {
		if ($_SESSION['new_ret'] != "") {
			header("Location: new_retailer.php?sus=sus");
		}
		else {
			$sus = "Dealer has been updated successfully.";
		}
	}
}



if ($_POST['submit'] == 'Save & Add Next Retailer') {
	unset($_SESSION['ret_err'], $_SESSION['retId'], $_SESSION['ret_login'], $_SESSION['update']);
}


if (isset($_REQUEST['id']) && $_REQUEST['id'] != "" || $_SESSION['ret_err'] != '') {
	if ($_SESSION['ret_err'] != '') {
		$auRec = $_objAdmin->_getSelectList('table_retailer as r left join state as s on r.state=s.state_id left join city as c on c.city_id=r.city left join table_web_users as w on w.retailer_id=r.retailer_id', "r.*,s.state_name,w.username,w.email_id,w.web_user_id,c.city_name", '', " r.retailer_id=" . $_SESSION['ret_err']);
		unset($_SESSION['ret_err']);
	}
	else {
		$auRec = $_objAdmin->_getSelectList('table_retailer as r left join state as s on r.state=s.state_id left join city as c on c.city_id=r.city left join table_web_users as w on w.retailer_id=r.retailer_id LEFT JOIN table_relationship AS tr on tr.relationship_id=r.relationship_id', "r.*,s.state_name,w.username,w.email_id,w.web_user_id,c.city_name,tr.relationship_code", '', " r.retailer_id=" . $_REQUEST['id']);
	}

	if (count($auRec) <= 0) header("Location: retailer.php");
}

If ($_SESSION['ret_login'] != '') {
	$auRec = $_objAdmin->_getSelectList('table_retailer as r left join state as s on r.state=s.state_id left join city as c on c.city_id=r.city left join table_web_users as w on w.retailer_id=r.retailer_id', "r.*,s.state_name,w.username,w.email_id,w.web_user_id,c.city_name", '', " r.retailer_id=" . $_SESSION['retId']);
}

if ($_SESSION['retId'] != "") {
	$auRec = $_objAdmin->_getSelectList('table_retailer as r left join state as s on r.state=s.state_id left join city as c on c.city_id=r.city left join table_web_users as w on w.retailer_id=r.retailer_id', "r.*,s.state_name,w.username,w.email_id,w.web_user_id,c.city_name", '', " r.retailer_id=" . $_SESSION['retId']);
}

?>