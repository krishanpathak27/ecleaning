<script src="javascripts/jquery-1.8.2.js" type="text/javascript"></script>
<script src="javascripts/jquery-ui.js"></script>
<script type="text/javascript" src="javascripts/validate.js"></script>
 <script>
  $(function() {
    $( "#datepicker" ).datepicker({dateFormat: 'd M yy',changeMonth: true, changeYear: true, yearRange: '1930:2038'});
	
  });

   <?php if($auRec[0]->taluka_id!=''){?>
	  $(document).ready(function(){
		  //alert("Heloo");
			showTalukaCity(<?php echo $auRec[0]->city.",". $auRec[0]->taluka_id; ?>);
			showTalukaMarket(<?php echo $auRec[0]->taluka_id.",". $auRec[0]->market_id; ?>);
			
			 
		  });
	 <?php }  ?>
  
  </script>

  
<!--  start message-blue -->
	<?php if($auRec[0]->retailer_id=="" && $ret_name_err==''){ ?>
	<div id="message-blue">
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
	<tr>
		<!--<td class="blue-left">Import Retailer. <a href="retailer.php?import">Your import file to ensure you have the file perfect for the import.</a> </td>-->
		<td class="blue-left">Import Retailer. <a href="#">Your import file to ensure you have the file perfect for the import.</a> </td>
		<td class="blue-right"><a class="close-blue"><img src="images/icon_close_blue.gif"   alt="" /></a></td>
	</tr>
	</table>
	</div>
	<?php } ?>
<!--  end message-blue -->
<!--  start message-red -->
	<?php if($ret_name_err!=''){?>
	<div id="message-red">
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
	<tr>
		<td class="red-left">Error. <?php echo $ret_name_err; ?></td>
		<td class="red-right"><a class="close-red"><img src="images/icon_close_red.gif"   alt="" /></a></td>
	</tr>
	</table>
	</div>
	<?php } ?>
	<!--  end message-red -->
<!--  start step-holder -->
<div id="step-holder">
<div class="step-no">1</div>
<div class="step-dark-left">Retailer Form</div>
<div class="step-dark-right">&nbsp;</div>
<div class="step-no-off">2</div>
<div class="step-light-left">Login Form</div>
<div class="step-light-right">&nbsp;</div>
<div class="step-no-off">3</div>
<div class="step-light-left">Map</div>
<div class="step-light-round">&nbsp;</div>
<div class="clear"></div>
</div>
	
	<!--  end step-holder -->
	<form name="frmPre" id="frmPre" method="post" action="retailer.php" enctype="multipart/form-data" >
		<table border="0" cellpadding="0" cellspacing="0"  id="id-form">
		<tr>
			<th valign="top">Retailer Name:</th>
			<td><input type="text" name="retailer_name" id="retailer_name" class="required" value="<?php echo $auRec[0]->retailer_name; ?>"/></td>
			<td></td>
		</tr>
		<tr>
			<th valign="top">Retailer Code:</th>
			<td><input type="text" name="retailer_code" id="retailer_code" class="required" value="<?php echo $auRec[0]->retailer_code; ?>" maxlength="255" /></td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<th valign="top">Retailer Channel:</th>
			<td>
			<select name="retailer_channel" id="retailer_channel" class="styledselect_form_5">
			<option value="" >Select</option>
			<?php 
			$condi=" and channel_name!=''";
			$aDis=$_objAdmin->_getSelectList('table_retailer_channel_master','*','',"$condi"); 
			if(is_array($aDis)){
			for($i=0;$i<count($aDis);$i++){?>
			<option value="<?php echo $aDis[$i]->channel_id;?>" <?php if($auRec[0]->channel_id==$aDis[$i]->channel_id){ echo 'selected';} ?> ><?php echo $aDis[$i]->channel_name;?></option>
			<?php }} ?>
			</select>
			</td>
		</tr>
		<tr>
			<th valign="top">Retailer Class:</th>
			<td>
			<select name="relationship_id" id="relationship_id" class="styledselect_form_3">
			<option value="" >Select</option>
			<?php 
			//$condi=" and relationship_code!=''";
			$condi=" and relationship_code!='' AND relationship_type IN (4) AND status='A'";
			$aDis=$_objAdmin->_getSelectList('table_relationship','*','',"$condi"); 
			if(is_array($aDis)){
			for($i=0;$i<count($aDis);$i++){?>
			<option value="<?php echo $aDis[$i]->relationship_id;?>" <?php if($auRec[0]->relationship_id==$aDis[$i]->relationship_id){ echo 'selected';} ?> ><?php echo $aDis[$i]->relationship_code;?></option>
			<?php }} ?>
			</select>
			</td>
		</tr>



		  <tr>
			<th valign="top">Division:</th>
			<td>
				<select name="division_id" id="division_id" class="styledselect_form_4 required">
					<option value="">Select Division</option>
					<?php 
					$condi = "";
					if(!empty($divisionIdString)) {
						$condi = " AND division_id IN (".$divisionIdString.") ";	
					}
					
					$branchList=$_objAdmin->_getSelectList('table_division',"division_id,division_name",''," status='A' $condi ");
						for($i=0;$i<count($branchList);$i++){
						
						if($branchList[$i]->division_id==$auRec[0]->division_id){$select="selected";} else {$select="";}
							
					 ?>
					 <option value="<?php echo $branchList[$i]->division_id; ?>" <?php echo $select ?>><?php echo $branchList[$i]->division_name; ?></option>
					 <?php } ?>
				</select>
			</td>
			<td></td>
		</tr>




		
		<tr>
			<th valign="top">Retailer Type:</th>
			<td>
			<select name="retailer_type" id="retailer_type" class="styledselect_form_5">
			<option value="" >Select</option>
			<?php 
			$condi=" and type_name!=''";
			$aDis=$_objAdmin->_getSelectList('table_retailer_type_master','*','',"$condi"); 
			if(is_array($aDis)){
			for($i=0;$i<count($aDis);$i++){?>
			<option value="<?php echo $aDis[$i]->type_id;?>" <?php if($auRec[0]->type_id==$aDis[$i]->type_id){ echo 'selected';} ?> ><?php echo $aDis[$i]->type_name;?></option>
			<?php }} ?>
			</select>
			</td>
		</tr>

		
		<tr>
	    <th valign="top">Interested:</th>
		<td colspan="2">
		<table width="20%">
		<tr><td><input type="radio" name="display_outlet" value="hot" <?php if($auRec[0]->display_outlet=='hot'){ ?> checked <?php } ?>>Hot</td>
		<td><input type="radio" name="display_outlet" value="cold" <?php if($auRec[0]->display_outlet=='cold'){ ?> checked <?php } ?>>Cold</td>
		</tr>
		</table>
		</td>
		</tr>


		<?php /*
		<tr>
			<th valign="top">Distributors:</th>
			<td>
			<select name="distributor_id" id="<?php echo $auRec[0]->distributor_id; ?>" class="styledselect_form_5 required">
			<option value="" >Select</option>
			<?php 
			$condi=" and distributor_name!=''";
			$aDis=$_objAdmin->_getSelectList('table_distributors','*','',"$condi"); 
			if(is_array($aDis)){
			for($i=0;$i<count($aDis);$i++){?>
			<option value="<?php echo $aDis[$i]->distributor_id;?>" <?php if($auRec[0]->distributor_id==$aDis[$i]->distributor_id){ echo 'selected';} ?> ><?php echo $aDis[$i]->distributor_name;?></option>
			<?php }} ?>
			</select>
			</td>
		</tr>

		*/
		?>



		<tr>
			<th valign="top">Distributors:</th>
			<td>
			<select name="distributor_id" id="distributor_id" class="chosen-select required" style="width:400px;">
			<option value="">Select</option>
			<?php 
			$condi=" AND D.distributor_name!=''";

			$cityList = $_objAdmin->_getSelectList2('city AS C LEFT JOIN state AS S ON S.state_id = C.state_id ','C.city_id,C.city_name,S.state_name',''," C.city_name!=''"); 

			foreach($cityList AS $key=>$cValue){

				$aDis=$_objAdmin->_getSelectList('table_distributors AS D LEFT JOIN city AS C ON C.city_id = D.city LEFT JOIN state AS S ON S.state_id = D.state ','D.distributor_id, D.distributor_name, C.city_name, S.state_name',''," D.city = '".$cValue->city_id."' $condi"); 
				if(is_array($aDis)){?>

				<optgroup class="routeList" label="<?php echo ucwords(strtolower(trim($cValue->city_name)))."(".$cValue->state_name.")";?>">

				<?php for($i=0;$i<count($aDis);$i++){?>
						<option value="<?php echo $aDis[$i]->distributor_id;?>"  <?php if($auRec[0]->distributor_id==$aDis[$i]->distributor_id){ echo 'selected';} ?> ><?php echo $aDis[$i]->distributor_name;?></option>
					<?php }?>
				<?php }?>
			<?php }?>
			</select>
			</td>
		</tr>







		<!-- <tr>
			<th valign="top">Distributor Code:</th>
			<td>
			<div id="outputdistributor_code"></div>
			</td>
		</tr> -->

		




	<?php /**********************************
			* desc : code for retailer Chain
			* created on : 02 Jan 2015 
			* Ajay 
		*/
	?>

	<?php /* <tr>
		<th valign="top">Retailer Chain:</th>
			<td>
			<select name="chain_id" id="chain_id" class="styledselect_form_3">
			<option>Please Select</option>
			<?php $aDis = $_objAdmin->_getSelectList('table_chain','*','',""); 

			if(is_array($aDis)){
			for($i=0;$i<count($aDis);$i++){?>
			<option value="<?php echo $aDis[$i]->chain_id;?>" <?php if($auRec[0]->chain_id == $aDis[$i]->chain_id){ echo 'selected';} ?> ><?php echo $aDis[$i]->chain_name;?></option>
			<?php }} ?>
			</select>
			</td>
		</tr>
	 */ ?>

		<?php /*
		<tr>
			<th valign="top">Retailer Add On Route:</th>
			<td>
				<div style="width:200px; height:100px;overflow:auto;" >
				<table border="0" cellpadding="0" cellspacing="0"  id="id-form">
				<tr>
					<th valign="top"></th>
				</tr>
				<?php
				$auRut=$_objAdmin->_getSelectList('table_route',"*",''," status='A'  order by route_name");
				for($i=0;$i<count($auRut);$i++){
				?>
				<tr>
					<td><input type="checkbox" name="add_route[]" <?php 
						if($auRec[0]->retailer_id!=''){
						$auchk=$_objAdmin->_getSelectList('table_route_retailer',"*",''," route_id=".$auRut[$i]->route_id." and retailer_id=".$auRec[0]->retailer_id);
							if(is_array($auchk)){
							echo "checked";
							}
						}
					?> value="<?php echo $auRut[$i]->route_id;?>" > <?php echo $auRut[$i]->route_name;?></td>
					<?php  } ?>
				</tr>
				</table>
				</div>
			</td>
			<td></td>
		</tr>
		*/?>


	<tr>
		<th valign="top">Retailer Add On Route:</th>
		<td colspan="2">
		<table border="0">
		<tr>
		<td>
		<select name="add_route[]" id="route_id" data-placeholder="Select Routes" style="width:450px;"  class="chosen-select" multiple>
		<?php $auRut=$_objAdmin->_getSelectList('table_route',"route_id, route_name",''," status='A'  order by route_name");
			  for($i=0;$i<count($auRut);$i++){?>
				  <option value="<?php echo $auRut[$i]->route_id;?>" 
				  <?php if($auRec[0]->retailer_id!=''){
				  	// AJAY@2016-10-28 Added status = R in query.
					$auchk=$_objAdmin->_getSelectList('table_route_retailer',"*",''," route_id=".$auRut[$i]->route_id." AND status = 'R' and retailer_id=".$auRec[0]->retailer_id);
					if(is_array($auchk)){?>selected<?php } }?> ><?php echo ucwords(strtolower(trim($auRut[$i]->route_name)));?></option>
			  <?php } ?>
		</select>
		</td>
		</tr>
		</table>
		</td>
	</tr>




		<tr>
			<th valign="top">Retailer Mobile No:</th>
			<td><input type="text" name="retailer_number" id="retailer_number" class="required number minlength" maxlength="15" minlength="10" value="<?php echo $auRec[0]->retailer_phone_no; ?>" placeholder="Phone Mobile"/></td>
		</tr>
		<tr>
			<th valign="top">Retailer Landline No:</th>
			<td><input type="text" name="retailer_number2" id="retailer_number2" class="text" value="<?php echo $auRec[0]->retailer_phone_no2; ?>" placeholder="Landline No 1"/></td>
			<td><input type="text" name="retailer_leadline_no" id="retailer_leadline_no" class="text" value="<?php echo $auRec[0]->retailer_leadline_no; ?>" placeholder="Landline No 2"/></td>
			<td></td>
		</tr>

		<tr>
			<th valign="top">Aadhaar No:</th>
			<td><input type="text" name="aadhar_no" id="aadhar_no"  maxlength="12" minlength="12" value="<?php echo $auRec[0]->aadhar_no; ?>"/></td>
			<td></td>
			<td></td>
		</tr>

		<tr>
			<th valign="top">Pan No:</th>
			<td><input type="text" name="pan_no" id="pan_no"  maxlength="10" minlength="10" value="<?php echo $auRec[0]->pan_no; ?>"/></td>
			<td></td>
			<td></td>
		</tr>
		
		<!-- <tr>
			<th valign="top">Retailer City:</th>
			<td><input type="text" name="address1" id="address1" class="required" value="<?php echo $auRec[0]->retailer_location; ?>"/></td>
			<td></td>
		</tr>-->
		<tr>
			<th valign="top">Preferred day of order:</th>
			<td>
			<select name="pre_day" id="per_day" class="styledselect_form_3">
				<option value="">Select</option>
				<option value="Monday" <?php if($auRec[0]->pre_day=='Monday'){ ?> selected <?php } ?>>Monday</option>
				<option value="Tuesday" <?php if($auRec[0]->pre_day=='Tuesday'){ ?> selected <?php } ?>>Tuesday</option>
				<option value="Wednesday" <?php if($auRec[0]->pre_day=='Wednesday'){ ?> selected <?php } ?>>Wednesday</option>
				<option value="Thursday" <?php if($auRec[0]->pre_day=='Thursday'){ ?> selected <?php } ?>>Thursday</option>
				<option value="Friday" <?php if($auRec[0]->pre_day=='Friday'){ ?> selected <?php } ?>>Friday</option>
				<option value="Saturday" <?php if($auRec[0]->pre_day=='Saturday'){ ?> selected <?php } ?>>Saturday</option>
				<option value="Sunday" <?php if($auRec[0]->pre_day=='Sunday'){ ?> selected <?php } ?>>Sunday</option>
			</select>
			</td>
		</tr>
		<tr>
			<th valign="top">Weekly day off:</th>
			<td>
			<select name="off_day" id="off_day" class="styledselect_form_3">
				<option value="">Select</option>
				<option value="Monday" <?php if($auRec[0]->off_day=='Monday'){ ?> selected <?php } ?>>Monday</option>
				<option value="Tuesday" <?php if($auRec[0]->off_day=='Tuesday'){ ?> selected <?php } ?>>Tuesday</option>
				<option value="Wednesday" <?php if($auRec[0]->off_day=='Wednesday'){ ?> selected <?php } ?>>Wednesday</option>
				<option value="Thursday" <?php if($auRec[0]->off_day=='Thursday'){ ?> selected <?php } ?>>Thursday</option>
				<option value="Friday" <?php if($auRec[0]->off_day=='Friday'){ ?> selected <?php } ?>>Friday</option>
				<option value="Saturday" <?php if($auRec[0]->off_day=='Saturday'){ ?> selected <?php } ?>>Saturday</option>
				<option value="Sunday" <?php if($auRec[0]->off_day=='Sunday'){ ?> selected <?php } ?>>Sunday</option>
			</select>
			</td>
		</tr>
		<!-- <tr>
			<th valign="top">Shop Description:</th>
			<td><input type="radio" name="shop_desc" value="1" <?php if($auRec[0]->shop_desc=='1'){ ?> checked <?php } ?> > Touch and field shop</td>
			<td><input type="radio" name="shop_desc" value="2" <?php if($auRec[0]->shop_desc=='2'){ ?> checked <?php } ?> > Step in shop</td>
		</tr>
		<tr>
			<th valign="top"></th>
			<td><input type="radio" name="shop_desc" value="3" <?php if($auRec[0]->shop_desc=='3'){ ?> checked <?php } ?> > Regular counter</td>
			<td><input type="radio" name="shop_desc" value="4" <?php if($auRec[0]->shop_desc=='4'){ ?> checked <?php } ?> > Supplier</td>
		</tr> -->
		<tr>
			<th valign="top">Retailer Email-ID:</th>
			<td><input type="text" name="retailer_email" id="retailer_email" class="text email" value="<?php echo $auRec[0]->retailer_email; ?>" placeholder="Email-ID 1"/></td>
			<td><input type="text" name="retailer_email2" id="retailer_email2" class="text email" value="<?php echo $auRec[0]->retailer_email2; ?>" placeholder="Email-ID 2"/></td>
		</tr>
		<tr>
			<th valign="top">Contact Person:</th>
			<td><input type="text" name="contact_person" id="contact_person" class="required" value="<?php echo $auRec[0]->contact_person; ?>" placeholder="Contact Person 1"/></td>
			<td><input type="text" name="contact_person2" id="contact_person2" class="text" value="<?php echo $auRec[0]->contact_person2; ?>" placeholder="Contact Person 2"/></td>
		</tr>
		<tr>
			<th valign="top">Contact Mobile No:</th>
			<td><input type="text" name="contact_number" id="contact_number" class="required number minlength" maxlength="10" minlength="10" value="<?php echo $auRec[0]->contact_number; ?>" placeholder="Contact Mobile 1"/></td>
			<td><input type="text" name="contact_number2" id="contact_number2" class="text number minlength" maxlength="10" minlength="10" value="<?php echo $auRec[0]->contact_number2; ?>" placeholder="Contact Mobile 2"/></td>
		</tr>
		<tr>
			<th valign="top">Retailer DOB:</th>
			<td><input type="text" id="datepicker" name="retailer_dob" class="date" value="<?php echo $_objAdmin->_changeDate($auRec[0]->retailer_dob); ?>"  readonly /></td>
			<td></td>
		</tr>
		<tr>
			<th valign="top">Retailer Address:</th>
			<td colspan="2">
			<input type="text" style="width:400px;" name="retailer_address" id="retailer_address" class="text" value="<?php echo $auRec[0]->retailer_address; ?>" placeholder="Address Line 1"/>
			</td>
		</tr>
		<tr>
			<th valign="top"></th>
			<td colspan="2">
			<input type="text" style="width:400px;" name="retailer_address2" id="retailer_address2" class="text" value="<?php echo $auRec[0]->retailer_address2; ?>" placeholder="Address Line 2"/>
			</td>
		</tr>
		<tr>
			<th valign="top">Select Country:</th>
			<td>
			<select name="country_id" id="country_id" class="required styledselect_form_3">
				<option value="">Please Select</option>
				<?php $country = $_objAdmin->_getSelectList2('country','country_id,country_name',''," status = 'A' ORDER BY country_name"); 
					if(is_array($country)){
					 foreach($country as $value):?>
					 	<option value="<?php echo $value->country_id;?>" <?php if($value->country_id==$auRec[0]->country_id) echo "selected";?> ><?php echo $value->country_name;?></option>
				<?php endforeach; }?>
			</select>
			</td>
			<td></td>
		</tr>
		<tr>
			<th valign="top">Select Region:</th>
			<td>
			<select name="region_id" id="region_id" class="required styledselect_form_3">
				<option value="">Please Select</option>
				
			</select>
			</td>
			<td></td>
		</tr>
		<tr>
			<th valign="top">Select Zone:</th>
			<td>
			<select name="zone_id" id="zone_id" class="required styledselect_form_3">
				<option value="">Please Select</option>
				
			</select>
			</td>
			<td></td>
		</tr>
		<tr>
			<th valign="top">Select State:</th>
			<td>
			<select name="state_id" id="state_id" class="required styledselect_form_3">
				<option value="">Please Select</option>
				
			</select>
			</td>
			<td></td>
		</tr>
		<tr>
			<th valign="top">Select City:</th>
			<td>
			<select name="city_id" id="city_id" class="required styledselect_form_3">
				<option value="">Please Select</option>
				
			</select>
			</td>
			<td></td>
		</tr>
		
		<tr>
			<th valign="top">Zipcode:</th>
			<td><input type="text" name="zipcode" id="zipcode" class="text number minlength" maxlength="6" minlength="6" value="<?php echo $auRec[0]->zipcode; ?>"/></td>
			<td></td>
		</tr>
		<tr>
			<td colspan="3"><div id="disList"><b></b></div></td>
		</tr>
		
		
		<tr>
			<th valign="top">Status:</th>
			<td>
			<select name="status" id="status" class="styledselect_form_3">
			<?php 			
			if($auRec[0]->new == ''){
			?>
			<option value="A" <?php if($auRec[0]->status=='A') echo "selected";?> >Active</option>
			<option value="I" <?php if($auRec[0]->status=='I') echo "selected";?> >Inactive</option>
			<?php } else { ?>
			<option value="A" <?php if($auRec[0]->status=='A') echo "selected";?> >Active</option>
			<option value="I" <?php if($auRec[0]->status=='I') echo "selected";?> >Inactive</option>
			<?php } ?>
			</select>	
			</td>
			<td></td>
		</tr>
			<tr>
	    <th valign="top">Validate Retailer:</th>
		<td colspan="2">
		<table width="20%">
		<tr><td><input type="radio" name="valstatus" id="valyesstatus"  onclick="ChangeDropdowns()" value="yes" <?php if($auRec[0]->validation_status=='yes'){ ?> checked <?php } ?>>Yes</td>
		<td><input type="radio" name="valstatus" id="valnostatus" onclick="ChangeDropdowns()" value="no" <?php if($auRec[0]->validation_status=='no'){ ?> checked <?php } ?>>No</td>
		</tr>
		</table>
		</td>
		</tr>

		<tr>
			<th valign="top">No Validation Reason:</th>
			<td>
			<select name="validation_reason" id="validation_reason" class="styledselect_form_3 required" <?php if($auRec[0]->validation_status=='yes'){ echo "disabled='disabled' class='styledselect_form_3'>";} else { echo "class='styledselect_form_3 required'";}?> >
			<option value="" >Please Select</option>					
			<option value="Call Back later" <?php if($auRec[0]->validation_reason=='Call Back later' ) echo "selected";?>>Call Back later</option>
			<option value="No Response/Engage" <?php if($auRec[0]->validation_reason=='No Response/Engage') echo "selected";?>>No Response/Engage</option>
			<option value="Others" <?php if($auRec[0]->validation_reason=='Others') echo "selected";?>>Others</option>
			<option value="Not Interested" <?php if($auRec[0]->validation_reason=='Not Interested') echo "selected";?>>Not Interested</option>
			<option value="Does Not Exist/Wrong Number" <?php if($auRec[0]->validation_reason=='Does Not Exist/Wrong Number') echo "selected";?>>Does Not Exist/Wrong Number</option>
			<option value="Language Problem" <?php if($auRec[0]->validation_reason=='Language Problem') echo "selected";?>>Language Problem</option>
			
			
			</select>	
			</td>
			<td></td>
		</tr>

		<tr>
			<th valign="top">Remark:</th>
			<td><textarea name="remark" id="remark" rows="10" cols="25"   minlength="8"><?php echo $auRec[0]->remark; ?></textarea> </td>
			<td></td>
			<td></td>
		</tr>
		<tr>
		<th>&nbsp;</th>
		<td valign="top">
			
			<input name="retailer_add" type="hidden" value="yes" />
			<input name="ret_id" type="hidden" value="<?php echo $auRec[0]->retailer_id; ?>" />
			<input name="account_id" type="hidden" value="<?php echo $_SESSION['accountId']; ?>" />
			<input name="start_date" type="hidden" value="<?php echo $date; ?>" />
			<input name="end_date" type="hidden" value="<?php echo $_SESSION['EndDate']; ?>" />
			<!--<input type="reset" value="Reset!" class="form-reset">-->
			<?php 
			if($auRec[0]->new!=''){
			$_SESSION['new_ret']="New";
			?>
			<input type="button" value="Back" class="form-reset" onclick="location.href='new_retailer.php?searchParam_1=<?php echo $_REQUEST['searchParam_1']; ?>&searchParam_2=<?php echo $_REQUEST['searchParam_2']; ?>&searchParam_3=<?php echo $_REQUEST['searchParam_3']; ?>'" />
			<input name="submit" class="form-submit" type="submit" id="submit" value="Approve & Continue" />
			<input name="submit" class="form-submit" type="submit" id="submit" value="Approve & Exit" />
			<?php } else { ?>
			<input type="button" value="Back" class="form-reset" onclick="location.href='retailer.php?searchParam_1=<?php echo $_REQUEST['searchParam_1']; ?>&searchParam_2=<?php echo $_REQUEST['searchParam_2']; ?>&searchParam_3=<?php echo $_REQUEST['searchParam_3']; ?>'" />
			<input name="save" class="form-submit" type="submit" id="save" value="Save" />
			<input name="submit" class="form-submit" type="submit" id="submit" value="Save & Continue" />
			<?php } ?>
		</td>
		</tr>
		</table>
	</form>


  <link rel="stylesheet" href="docsupport/prism.css">
  <link rel="stylesheet" href="docsupport/chosen.css">
  <style type="text/css" media="all">
    /* fix rtl for demo */
    .chosen-rtl .chosen-drop { left: -9000px; }
	
	#smsform label.error {
	margin-left: 10px;
	width: auto;
	display: inline;
	}
	.error {
		color: #FF0000;
		font:normal 12px tahoma;
	}

		input, textarea {
		width:auto;
		}

  </style>

		  <script src="docsupport/chosen.jquery.js" type="text/javascript"></script>

		  <script type="text/javascript">
			var config = {
			  '.chosen-select'           : {},
			  '.chosen-select-deselect'  : {allow_single_deselect:true},
			  '.chosen-select-no-single' : {disable_search_threshold:10},
			  '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
			  '.chosen-select-width'     : {width:"100%"}
			}
			for (var selector in config) {
			  $(selector).chosen(config[selector]);
			}

			function ChangeDropdowns(){
			if ($("#valnostatus").is(":checked")) {
			    $("#validation_reason").removeAttr("disabled");
			}
			if ($("#valyesstatus").is(":checked")) {
			    $("#validation_reason").attr("disabled","disabled");
			}

			}
			</script>
			<script>
				$("#country_id").change(function(){
				    var selected_country = $("#country_id").val();
				    $.ajax({
				    	url: "add_state_city_ajax.php",
				    	type: "post",
				    	data: {selected_country:selected_country,type:'country'},
				     	success: function(result){
				        	$("#region_id").html(result);
				    }});
				});
				$("#region_id").change(function(){
				    var selected_region = $("#region_id").val();
				    $.ajax({
				    	url: "add_state_city_ajax.php",
				    	type: "post",
				    	data: {selected_region:selected_region,type:'region'},
				     	success: function(result){
				        	$("#zone_id").html(result);
				    }});
				});
				$("#zone_id").change(function(){
				    var selected_zone = $("#zone_id").val();
				    $.ajax({
				    	url: "add_state_city_ajax.php",
				    	type: "post",
				    	data: {selected_zone:selected_zone,type:'zone'},
				     	success: function(result){
				        	$("#state_id").html(result);
				    }});
				});
				$("#state_id").change(function(){
				    var selected_state = $("#state_id").val();
				    $.ajax({
				    	url: "add_state_city_ajax.php",
				    	type: "post",
				    	data: {selected_state:selected_state,type:'state'},
				     	success: function(result){
				        	$("#city_id").html(result);
				    }});
				});
			</script>
