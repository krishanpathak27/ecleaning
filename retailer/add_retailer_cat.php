<!--  start message-blue -->
	<?php if($auRec[0]->retailer_id=="" && $ret_name_err==''){ ?>
	<div id="message-blue">
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
	<tr>
		<!--<td class="blue-left">Import Retailer. <a href="retailer.php?import">Your import file to ensure you have the file perfect for the import.</a> </td>-->
		<td class="blue-left">Import Retailer. <a href="#">Your import file to ensure you have the file perfect for the import.</a> </td>
		<td class="blue-right"><a class="close-blue"><img src="images/icon_close_blue.gif"   alt="" /></a></td>
	</tr>
	</table>
	</div>
	<?php } ?>
<!--  end message-blue -->
<!--  start message-red -->
	<?php if($ret_name_err!=''){?>
	<div id="message-red">
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
	<tr>
		<td class="red-left">Error. <?php echo $ret_name_err; ?></td>
		<td class="red-right"><a class="close-red"><img src="images/icon_close_red.gif"   alt="" /></a></td>
	</tr>
	</table>
	</div>
	<?php } ?>
	<!--  end message-red -->
<!--  start step-holder -->
<div id="step-holder">
<div class="step-no">1</div>
<div class="step-dark-left">Retailer Form</div>
<div class="step-dark-right">&nbsp;</div>
<div class="step-no-off">2</div>
<div class="step-light-left">Login Form</div>
<div class="step-light-right">&nbsp;</div>
<div class="step-no-off">3</div>
<div class="step-light-left">Map</div>
<div class="step-light-round">&nbsp;</div>
<div class="clear"></div>
</div>
	
	<!--  end step-holder -->
	<form name="frmPre" id="frmPre" method="post" action="retailer.php" enctype="multipart/form-data" >
		<table border="0" cellpadding="0" cellspacing="0"  id="id-form">
		<tr>
			<th valign="top">Select Distributor:</th>
			<td>
			<select name="distributor_id" id="distributor_id" class="styledselect_form_3">
			<?php
			if(isset($_REQUEST['id']) && $_REQUEST['id']!=""){ ?>
			<option value="<?php echo $auRec[0]->distributor_id;?>" ><?php echo $auRec[0]->distributor_name;?></option>
			<?php } ?>
			<?php $aDis=$_objAdmin->_getSelectList('table_distributors','*',''," account_id='".$_SESSION['accountId']."'ORDER BY distributor_name"); 
			if(is_array($aDis)){
			for($i=0;$i<count($aDis);$i++){?>
			<option value="<?php echo $aDis[$i]->distributor_id;?>" ><?php echo $aDis[$i]->distributor_name;?></option>
			<?php }} ?>
			</select>
			</td>
		</tr>
		<tr>
			<th valign="top">Retailer Name:</th>
			<td><input type="text" name="retailer_name" id="retailer_name" class="required" value="<?php echo $auRec[0]->retailer_name; ?>"/></td>
			<td></td>
		</tr>
		<tr>
			<th valign="top">Retailer Phone No:</th>
			<td><input type="text" name="retailer_number" id="retailer_number" class="required number minlength" maxlength="15" minlength="10" value="<?php echo $auRec[0]->retailer_phone_no; ?>"/></td>
			<td></td>
		</tr>
		<tr>
			<th valign="top">Retailer Address:</th>
			<td><textarea rows="" cols="" name="retailer_address" class="textarea" ><?php echo $auRec[0]->retailer_address; ?></textarea></td>
			<td>
			</td>
		</tr>
		<tr>
			<th valign="top">Retailer Location:</th>
			<td><input type="text" name="address1" id="address1" class="required" value="<?php echo $auRec[0]->retailer_location; ?>"/></td>
			<td></td>
		</tr>
		<tr>
			<th valign="top">Country:</th>
			<td>
			<select name="country" id="country" class="styledselect_form_3">
			<option value="india">India</option>
			</select>
			</td>
		</tr>
		<tr>
			<th valign="top">Select State:</th>
			<td>
			<select name="state" id="state" class="styledselect_form_3" onClick="showstage_city(this.value)" >
			<?php
			if(isset($_REQUEST['id']) && $_REQUEST['id']!=""){ ?>
			<option value="<?php echo $auRec[0]->state;?>" ><?php echo $auRec[0]->state_name;?></option>
			<?php } ?>
			<?php $auUsr=$_objAdmin->_getSelectList2('state','state_id,state_name','',"ORDER BY field(state_name, 'Delhi','Haryana','Uttar Pradesh','Andaman Nicobar','Andhra Pradesh','Arunanchal Pradesh','Assam','Bihar','Chandigarh','Chattisgarh','Dadar & Nagar Haveli','Daman & Diu','Goa','Gujarat','Himachal Pradesh','Jammu & Kashmir','Jharkhand','Karnataka','Kerala','Lakshwdeep','Madhya Pradesh','Maharashtra','Manipur','Meghalaya','Mizoram','Nagaland','Orissa','Pondicherry','Punjab','Rajasthan','Sikkim','Tamil Nadu','Tripura','Uttarakhand','West Bengal')"); if(is_array($auUsr)){
			for($i=0;$i<count($auUsr);$i++){?>
			<option value="<?php echo $auUsr[$i]->state_id;?>" ><?php echo $auUsr[$i]->state_name;?></option>
			<?php }}?>
			</select>
			</td>
		</tr>
		<tr>
			<th valign="top">Select City:</th>
			<td>
			<select name="address2" id="address2" class="styledselect_form_3">
			<?php
			if(isset($_REQUEST['id']) && $_REQUEST['id']!=""){ ?>
			<option value="<?php echo $auRec[0]->city;?>" selected="selected" ><?php echo $auRec[0]->city;?></option>
			<?php } else {
			$aucity=$_objAdmin->_getSelectList2('city','*',''," state_id=25"); if(is_array($aucity)){
			for($i=0;$i<count($aucity);$i++){?>
			<option value="<?php echo $aucity[$i]->city_name;?>" ><?php echo $aucity[$i]->city_name;?></option>
			<?php }} }?>
			</select>
			</td>
		</tr>
		<tr>
			<th valign="top">Zipcode:</th>
			<td><input type="text" name="zipcode" id="zipcode" class="text number minlength" maxlength="6" minlength="6" value="<?php echo $auRec[0]->zipcode; ?>"/></td>
			<td></td>
		</tr>
		<tr>
			<th valign="top">Contact Person:</th>
			<td><input type="text" name="contact_person" id="contact_person" class="text" value="<?php echo $auRec[0]->contact_person; ?>"/></td>
			<td></td>
		</tr>
		<tr>
			<th valign="top">Contact Phone No:</th>
			<td><input type="text" name="contact_number" id="contact_number" class="text number minlength" maxlength="15" minlength="10" value="<?php echo $auRec[0]->contact_number; ?>" /></td>
			<td></td>
		</tr>
		<tr>
		<th>&nbsp;</th>
		<td valign="top">
			<input name="retailer_add" type="hidden" value="yes" />
			<input name="ret_id" type="hidden" value="<?php echo $auRec[0]->retailer_id; ?>" />
			<input name="account_id" type="hidden" value="<?php echo $_SESSION['accountId']; ?>" />
			<input name="start_date" type="hidden" value="<?php echo $date; ?>" />
			<input name="end_date" type="hidden" value="<?php echo $_SESSION['EndDate']; ?>" />
			<input type="reset" value="Reset!" class="form-reset">
			<input type="button" value="Back" class="form-reset" onclick="location.href='retailer.php';" />
			<input name="submit" class="form-submit" type="submit" id="submit" value="Save & Continue" />
		</td>
		</tr>
		</table>
	</form>
