<script src="javascripts/jquery-1.8.2.js" type="text/javascript"></script>
<script src="javascripts/jquery-ui.js"></script>
 <script>
  $(function() {
    $( "#datepicker" ).datepicker({dateFormat: 'yy-mm-dd',changeMonth: true, changeYear: true, yearRange: '1930:2050'});
	
  });
  </script>
  <script type="text/javascript">
    function getval(sel) {
		var nom = sel;
         alert(nom);
		 $.post(('retailer/test.php?value='+sel), function(data) {
  $('.result').html(data);
  alert('Load was performed.');
});
    }
</script>
<!--  start message-blue -->
	<?php if($auRec[0]->retailer_id=="" && $ret_name_err==''){ ?>
	<div id="message-blue">
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
	<tr>
		<!--<td class="blue-left">Import Retailer. <a href="retailer.php?import">Your import file to ensure you have the file perfect for the import.</a> </td>-->
		<td class="blue-left">Import Retailer. <a href="#">Your import file to ensure you have the file perfect for the import.</a> </td>
		<td class="blue-right"><a class="close-blue"><img src="images/icon_close_blue.gif"   alt="" /></a></td>
	</tr>
	</table>
	</div>
	<?php } ?>
<!--  end message-blue -->
<!--  start message-red -->
	<?php if($ret_name_err!=''){?>
	<div id="message-red">
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
	<tr>
		<td class="red-left">Error. <?php echo $ret_name_err; ?></td>
		<td class="red-right"><a class="close-red"><img src="images/icon_close_red.gif"   alt="" /></a></td>
	</tr>
	</table>
	</div>
	<?php } ?>
	<!--  end message-red -->
<!--  start step-holder -->
<div id="step-holder">
<div class="step-no">1</div>
<div class="step-dark-left">Retailer Form</div>
<div class="step-dark-right">&nbsp;</div>
<div class="step-no-off">2</div>
<div class="step-light-left">Login Form</div>
<div class="step-light-right">&nbsp;</div>
<div class="step-no-off">3</div>
<div class="step-light-left">Map</div>
<div class="step-light-round">&nbsp;</div>
<div class="clear"></div>
</div>
	
	<!--  end step-holder -->
	<form name="frmPre" id="frmPre" method="post" action="retailer.php" enctype="multipart/form-data" >
		<table border="0" cellpadding="0" cellspacing="0"  id="id-form">
		<tr>
			<th valign="top">Retailer Name:</th>
			<td><input type="text" name="retailer_name" id="retailer_name" class="required" value="<?php echo $auRec[0]->retailer_name; ?>"/></td>
			<td></td>
		</tr>
		<tr>
			<th valign="top">Relationship Code:</th>
			<td>
			<select name="relationship_id" id="relationship_id" class="styledselect_form_3">
			<?php
			if(isset($_REQUEST['id']) && $_REQUEST['id']!=""){ ?>
			<option value="<?php echo $auRec[0]->relationship_id;?>" ><?php echo $auRec[0]->relationship_code;?></option>
			<?php } ?>
			<option value="" >Select</option>
			<?php $aDis=$_objAdmin->_getSelectList('table_relationship','*',''," 1=1"); 
			if(is_array($aDis)){
			for($i=0;$i<count($aDis);$i++){?>
			<option value="<?php echo $aDis[$i]->relationship_id;?>" ><?php echo $aDis[$i]->relationship_code;?></option>
			<?php }} ?>
			</select>
			</td>
		</tr>
		<tr>
			<th valign="top">Retailer Phone No:</th>
			<td><input type="text" name="retailer_number" id="retailer_number" class="required number minlength" maxlength="15" minlength="10" value="<?php echo $auRec[0]->retailer_phone_no; ?>" placeholder="Phone Number 1"/></td>
			<td><input type="text" name="retailer_number2" id="retailer_number2" class="text number minlength" maxlength="15" minlength="10" value="<?php echo $auRec[0]->retailer_phone_no2; ?>" placeholder="Phone Number 2"/></td>
		</tr>
		<tr>
			<th valign="top">Retailer Leadline No:</th>
			<td><input type="text" name="retailer_leadline_no" id="retailer_leadline_no" class="text number minlength" maxlength="15" minlength="6" value="<?php echo $auRec[0]->retailer_leadline_no; ?>"/></td>
			<td></td>
		</tr>
		<tr>
			<th valign="top">Retailer Market:</th>
			<td><input type="text" name="address1" id="address1" class="required" value="<?php echo $auRec[0]->retailer_location; ?>"/></td>
			<td></td>
		</tr>
		<tr>
			<th valign="top">Preferred day of order:</th>
			<td>
			<select name="pre_day" id="per_day" class="styledselect_form_3">
				<option value="">Select</option>
				<option value="Monday" <?php if($auRec[0]->pre_day=='Monday'){ ?> selected <?php } ?>>Monday</option>
				<option value="Tuesday" <?php if($auRec[0]->pre_day=='Tuesday'){ ?> selected <?php } ?>>Tuesday</option>
				<option value="Wednesday" <?php if($auRec[0]->pre_day=='Wednesday'){ ?> selected <?php } ?>>Wednesday</option>
				<option value="Thursday" <?php if($auRec[0]->pre_day=='Thursday'){ ?> selected <?php } ?>>Thursday</option>
				<option value="Friday" <?php if($auRec[0]->pre_day=='Friday'){ ?> selected <?php } ?>>Friday</option>
				<option value="Saturday" <?php if($auRec[0]->pre_day=='Saturday'){ ?> selected <?php } ?>>Saturday</option>
				<option value="Sunday" <?php if($auRec[0]->pre_day=='Sunday'){ ?> selected <?php } ?>>Sunday</option>
			</select>
			</td>
		</tr>
		<tr>
			<th valign="top">Weekly day off:</th>
			<td>
			<select name="off_day" id="off_day" class="styledselect_form_3">
				<option value="">Select</option>
				<option value="Monday" <?php if($auRec[0]->off_day=='Monday'){ ?> selected <?php } ?>>Monday</option>
				<option value="Tuesday" <?php if($auRec[0]->off_day=='Tuesday'){ ?> selected <?php } ?>>Tuesday</option>
				<option value="Wednesday" <?php if($auRec[0]->off_day=='Wednesday'){ ?> selected <?php } ?>>Wednesday</option>
				<option value="Thursday" <?php if($auRec[0]->off_day=='Thursday'){ ?> selected <?php } ?>>Thursday</option>
				<option value="Friday" <?php if($auRec[0]->off_day=='Friday'){ ?> selected <?php } ?>>Friday</option>
				<option value="Saturday" <?php if($auRec[0]->off_day=='Saturday'){ ?> selected <?php } ?>>Saturday</option>
				<option value="Sunday" <?php if($auRec[0]->off_day=='Sunday'){ ?> selected <?php } ?>>Sunday</option>
			</select>
			</td>
		</tr>
		<tr>
			<th valign="top">Shop Description:</th>
			<td><input type="radio" name="shop_desc" value="1" <?php if($auRec[0]->shop_desc=='1'){ ?> checked <?php } ?> > Touch and field shop</td>
			<td><input type="radio" name="shop_desc" value="2" <?php if($auRec[0]->shop_desc=='2'){ ?> checked <?php } ?> > Step in shop</td>
		</tr>
		<tr>
			<th valign="top"></th>
			<td><input type="radio" name="shop_desc" value="3" <?php if($auRec[0]->shop_desc=='3'){ ?> checked <?php } ?> > Regular counter</td>
			<td><input type="radio" name="shop_desc" value="4" <?php if($auRec[0]->shop_desc=='4'){ ?> checked <?php } ?> > Supplier</td>
		</tr>
		<tr>
			<th valign="top">Retailer Email-ID:</th>
			<td><input type="text" name="retailer_email" id="retailer_email" class="text email minlength" maxlength="15" minlength="10" value="<?php echo $auRec[0]->retailer_email; ?>" placeholder="Email-ID 1"/></td>
			<td><input type="text" name="retailer_email2" id="retailer_email2" class="text email minlength" maxlength="15" minlength="10" value="<?php echo $auRec[0]->retailer_email2; ?>" placeholder="Email-ID 2"/></td>
		</tr>
		<tr>
			<th valign="top">Contact Person:</th>
			<td><input type="text" name="contact_person" id="contact_person" class="required" value="<?php echo $auRec[0]->contact_person; ?>" placeholder="Contact Person 1"/></td>
			<td><input type="text" name="contact_person2" id="contact_person2" class="text" value="<?php echo $auRec[0]->contact_person2; ?>" placeholder="Contact Person 2"/></td>
		</tr>
		<tr>
			<th valign="top">Contact Phone No:</th>
			<td><input type="text" name="contact_number" id="contact_number" class="required number minlength" maxlength="15" minlength="10" value="<?php echo $auRec[0]->contact_number; ?>" placeholder="Contact Phone 1"/></td>
			<td><input type="text" name="contact_number2" id="contact_number2" class="text number minlength" maxlength="15" minlength="10" value="<?php echo $auRec[0]->contact_number2; ?>" placeholder="Contact Phone 2"/></td>
		</tr>
		<tr>
			<th valign="top">Retailer DOB:</th>
			<td><input type="text" id="datepicker" name="retailer_dob" class="date" value="<?php echo $auRec[0]->retailer_dob; ?>"  readonly /></td>
			<td></td>
		</tr>
		<tr>
			<th valign="top">Retailer Address:</th>
			<td colspan="2">
			<input type="text" style="width:400px;" name="retailer_address" id="retailer_address" class="text" value="<?php echo $auRec[0]->retailer_address; ?>" placeholder="Address Line 1"/>
			</td>
		</tr>
		<tr>
			<th valign="top"></th>
			<td colspan="2">
			<input type="text" style="width:400px;" name="retailer_address2" id="retailer_address2" class="text" value="<?php echo $auRec[0]->retailer_address2; ?>" placeholder="Address Line 2"/>
			</td>
		</tr>
		<tr>
			<th valign="top">Country:</th>
			<td>
			<select name="country" id="country" class="styledselect_form_3">
			<option value="india">India</option>
			</select>
			</td>
		</tr>
		<tr>
			<th valign="top">Select State:</th>
			<td>
			<select name="state" id="state" class="styledselect_form_3" onClick="showstage_city(this.value)" onChange="getval(this.value);">
			<?php
			if(isset($_REQUEST['id']) && $_REQUEST['id']!=""){ ?>
			<option value="<?php echo $auRec[0]->state;?>" ><?php echo $auRec[0]->state_name;?></option>
			<?php } ?>
			<?php $auUsr=$_objAdmin->_getSelectList2('state','state_id,state_name','',"ORDER BY state_name"); if(is_array($auUsr)){
			for($i=0;$i<count($auUsr);$i++){?>
			<option value="<?php echo $auUsr[$i]->state_id;?>" ><?php echo $auUsr[$i]->state_name;?></option>
			<?php }}?>
			</select>
			<!--<div name="stid" id="stid"></div>-->
			</td>
			
		</tr>
		<tr>
			<th valign="top">Select City:</th>
			<td>
			<select name="address2" id="address2" class="styledselect_form_3" >
			<?php
			if(isset($_REQUEST['id']) && $_REQUEST['id']!=""){ ?>
			<option value="<?php echo $auRec[0]->city;?>" selected="selected" ><?php echo $auRec[0]->city_name;?></option>
			<?php } ?>
			</select>
			</td>
		</tr>
		<tr>
			<th valign="top">Zipcode:</th>
			<td><input type="text" name="zipcode" id="zipcode" class="text number minlength" maxlength="6" minlength="6" value="<?php echo $auRec[0]->zipcode; ?>"/></td>
			<td></td>
		</tr>
		<?php include("retailer/test.php"); ?>
		
		<tr>
			<th valign="top">Status:</th>
			<td>
			<select name="status" id="status" class="styledselect_form_3">
			<option value="A" <?php if($auRec[0]->status=='A') echo "selected";?> >Active</option>
			<option value="I" <?php if($auRec[0]->status=='I') echo "selected";?> >Inactive</option>
			</td>
			<td></td>
		</tr>
		<tr>
		<th>&nbsp;</th>
		<td valign="top">
			
			<input name="retailer_add" type="hidden" value="yes" />
			<input name="ret_id" type="hidden" value="<?php echo $auRec[0]->retailer_id; ?>" />
			<input name="account_id" type="hidden" value="<?php echo $_SESSION['accountId']; ?>" />
			<input name="start_date" type="hidden" value="<?php echo $date; ?>" />
			<input name="end_date" type="hidden" value="<?php echo $_SESSION['EndDate']; ?>" />
			<!--<input type="reset" value="Reset!" class="form-reset">-->
			<input type="button" value="Back" class="form-reset" onclick="location.href='retailer.php';" />
			<input name="submit" class="form-submit" type="submit" id="submit" value="Save & Continue" />
		</td>
		</tr>
		</table>
	</form>
