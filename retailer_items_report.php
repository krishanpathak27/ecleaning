<?php
include("includes/config.inc.php");
include("includes/function.php");
$page_name="Dealer Item Wise Report";
$_objAdmin = new Admin();

if($_REQUEST['retid']!=''){
$Ret=$_objAdmin->_getSelectList('table_retailer as r left join city as c on r.city=c.city_id','r.retailer_name,r.retailer_location,c.city_name',''," r.retailer_id='".$_REQUEST['retid']."'"); 
$_SESSION['ItemRetID']=$_REQUEST['retid'];
$_SESSION['ItemRetName']=$Ret[0]->retailer_name;
$_SESSION['ItemRetMarket']=$Ret[0]->retailer_location;
$_SESSION['ItemRetCity']=$Ret[0]->city_name;
$_SESSION['List']= "amt_total desc";

}
if($_REQUEST['order']==''){
$_SESSION['order_by_amt']="desc";
$_SESSION['order_by_qty']="desc";
}
if(isset($_POST['order_qty']) && $_POST['order_qty'] == 'yes')
{
	if($_REQUEST['order_by_qty']=='desc'){
	$_SESSION['order_by_qty']="asc";
	$_SESSION['order_by_amt']="asc";
	$_SESSION['List']="total_qty ".$_SESSION['order_by_qty']."";
	} else {
	$_SESSION['order_by_qty']="desc";
	$_SESSION['order_by_amt']="desc";
	$_SESSION['List']="total_qty ".$_SESSION['order_by_qty']."";
	}
}
if(isset($_POST['order_amt']) && $_POST['order_amt'] == 'yes')
{
	if($_REQUEST['order_by_amt']=='desc'){
	$_SESSION['order_by_amt']="asc";
	$_SESSION['order_by_qty']="asc";
	$_SESSION['List']= "amt_total ".$_SESSION['order_by_amt']."";
	} else {
	$_SESSION['order_by_amt']="desc";
	$_SESSION['order_by_qty']="desc";
	$_SESSION['List']= "amt_total ".$_SESSION['order_by_amt']."";
	}	
}
if($_SESSION['SnameList']!=''){
$sNameId="AND o.salesman_id='".$_SESSION['SnameList']."'";
}

?>

<?php include("header.inc.php") ?>

<script type="text/javascript">

    function PrintElem(elem)
    {
        Popup($(elem).html());
    }

    function Popup(data) 
    {
		
        var mywindow = window.open('', 'Report');
		
        mywindow.document.write('<html><head><title>Dealer Item Wise Report</title>');
		//mywindow.document.write('<table><tr><td><b>City Name:</b> <?php echo $sal_name; ?></td><td><b>Market Name:</b> <?php echo $_SESSION['MarketList']; ?></td><td><b>From Date:</b> <?php echo $_SESSION['FromRetList']; ?></td><td><b>To Date:</b> <?php echo $_SESSION['ToRetList']; ?></td></tr></table>');
        /*optional stylesheet*/ //mywindow.document.write('<link rel="stylesheet" href="main.css" type="text/css" />');
        mywindow.document.write('</head><body >');
        mywindow.document.write(data);
        mywindow.document.write('</body></html>');

        mywindow.print();
        mywindow.close();
        return true;
    }
	
	$(document).ready(function(){
		$('.maintr').click(function() {
			//alert("Hello");
			//$(this).css('background', '#E3E3E3');
			$('#lists tr').removeClass('trbgcolor');
			
			$(this).addClass('trbgcolor');
		});
		//$('#lists').find('tr').keydown(function(e){
		//$('#lists tr').keydown(function(e) {
		
		
	});
</script>

<script src="javascripts/dateNextPrev.js" type="text/javascript"></script>
<!-- start content-outer -->
<input name="pagename" type="hidden"  id="pagename" value="admin_order_list.php" />
<div id="content-outer">
<!-- start content -->
<div id="content">
<div id="page-heading"><h1><span style="color: #d74343; font-family: Tahoma; font-weight: bold;">Dealer Item Wise Report</span></h1></div>
<table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">
<!--<tr>
	<th rowspan="3" class="sized"><img src="images/side_shadowleft.jpg" width="20" height="300" alt="" /></th>
	<th class="topleft"></th>
	<td id="tbl-border-top">&nbsp;</td>
	<th class="topright"></th>
	<th rowspan="3" class="sized"><img src="images/side_shadowright.jpg" width="20" height="300" alt="" /></th>
</tr>
-->
<tr>
	<!--<td id="tbl-border-left"></td>-->
	<td>
	<!--  start content-table-inner -->
	<div id="content-table-inner">
	<div style="width: 100%;" align="right">
				
		</div>
	<table border="0" width="100%" cellpadding="0" cellspacing="0">	
		<td>
		<div style="width:1100px;overflow:auto; height:auto;" >
		<div id="Report">
		<table border="0" cellpadding="0" cellspacing="0"  >
			<tr>
				<th valign="top" style="width: 100px;"  align="left">Dealer Name:</th>
				<td align="left" style="min-width: 150px;"><?php echo $_SESSION['ItemRetName']; ?></td>
			</tr>
			<tr>
				<th valign="top" style="width: 100px;line-height: 28px;"  align="left">Market:</th>
				<td align="left" style="min-width: 150px;"><?php echo $_SESSION['ItemRetMarket']; ?></td>
			</tr>
			<tr>
				<th valign="top" style="width: 100px;line-height: 28px;"  align="left">City:</th>
				<td align="left" style="min-width: 150px;"><?php echo $_SESSION['ItemRetCity']; ?></td>
			</tr>
			<tr>
				<th valign="top" style="width: 100px;line-height: 28px;"  align="left">From Date:</th>
				<td valign="center" style="min-width: 150px;"><?php echo $_SESSION['FromRetList']; ?></td>
				<th valign="top" style="width: 100px;line-height: 28px;"  align="left">To Date:</th>
				<td valign="center" style="min-width: 150px;"><?php echo $_SESSION['ToRetList']; ?></td>
			</tr>
			<tr>
				
			</tr>
		</table>
		<table  border="0" cellpadding="0" cellspacing="0" id="lists">
			<?php
			$auRet=$_objAdmin->_getSelectList('table_order as o left join table_salesman as s on o.salesman_id=s.salesman_id',"o.order_id,o.date_of_order,s.salesman_name",''," o.retailer_id='".$_SESSION['ItemRetID']."' and (o.date_of_order BETWEEN '".date('Y-m-d', strtotime($_SESSION['FromRetList']))."' AND '".date('Y-m-d', strtotime($_SESSION['ToRetList']))."') AND o.order_type!='No' $sNameId order by o.date_of_order asc");
			if(is_array($auRet)){
			?>
			<tr  bgcolor="#6E6E6E" style="color: #fff;font-weight: bold;" id="candidateNameTD">
				<td style="padding:10px; width:10px;">SNO.</td>
				<td style="padding:10px;"><div style="width: 65px;" >Items</div></td>
				<?php
				 for($a=0;$a<count($auRet);$a++){ ?>
				<!--<td style="padding:10px; " width="20%">Date<?php echo $a+1; ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>-->
				<td style="padding:10px;"><div style="width: 100px;" align="center">Visit <?php echo $a+1; ?></div>
				<div style="width: 100px;" >Date: <?php echo $_objAdmin->_changeDate($auRet[$a]->date_of_order); ?></div>
				<div style="width: 100px;" align="center"><?php echo $auRet[$a]->salesman_name; ?></div>
				</td>
				<?php } ?>
				<form name="submitQty" method="POST" action="">
				<input type="hidden" name="order_by_qty" value="<?php echo $_SESSION['order_by_qty']; ?>">
				<input type="hidden" name="order" value="<?php echo $_SESSION['order_by_amt']; ?>">
				<input type="hidden" name="order_qty" value="yes">
				<td style="padding:10px;"><div style="width: 110px;" align="center">Total Quantity &nbsp;&nbsp;<a href="javascript:document.submitQty.submit()">
				<?php if($_SESSION['order_by_qty']=='desc'){ ?>
				<img src="images/arrow-up.png" width="13" height="13" />
				<?php } else { ?>
				<img src="images/arrow-down.png" width="13" height="13" />
				<?php } ?>
				</a></div></td>
				</form>
				<form name="submitAmt" method="POST" action="">
				<input type="hidden" name="order_by_amt" value="<?php echo $_SESSION['order_by_amt']; ?>">
				<input type="hidden" name="order" value="<?php echo $_SESSION['order_by_qty']; ?>">
				<input type="hidden" name="order_amt" value="yes">
				<td style="padding:10px;"><div style="width: 110px;" align="center">Total Amount &nbsp;&nbsp;<a href="javascript:document.submitAmt.submit()">
				<?php if($_SESSION['order_by_amt']=='desc'){ ?>
				<img src="images/arrow-up.png" width="13" height="13" />
				<?php } else { ?>
				<img src="images/arrow-down.png" width="13" height="13" />
				<?php } ?>
				</a></div></td>
				</form>
				
				<td style="padding:10px;"><div style="width: 50px;" align="center">% Share</div></td>
			</tr>
			<?php
			$itemList=$_objAdmin->_getSelectList2('table_order_detail as d left join table_order as o on o.order_id=d.order_id left join table_item as i on i.item_id=d.item_id',"sum(acc_quantity) as total_qty,sum(d.acc_total) as amt_total,i.item_code,i.item_name,i.item_id",''," d.order_id in (SELECT order_id FROM table_order where o.retailer_id='".$_SESSION['ItemRetID']."' and (o.date_of_order BETWEEN '".date('Y-m-d', strtotime($_SESSION['FromRetList']))."' AND '".date('Y-m-d', strtotime($_SESSION['ToRetList']))."') ) $sNameId GROUP BY d.item_id ORDER BY ".$_SESSION['List']."");
			
			$net_total_item=$_objAdmin->_getSelectList2('table_order_detail as d left join table_order as o on o.order_id=d.order_id',"sum(d.acc_quantity) as totalitem, sum(d.acc_total) as amt_total",''," o.retailer_id='".$_SESSION['ItemRetID']."' and (o.date_of_order BETWEEN '".date('Y-m-d', strtotime($_SESSION['FromRetList']))."' AND '".date('Y-m-d', strtotime($_SESSION['ToRetList']))."') $sNameId");
				
			for($i=0;$i<count($itemList);$i++)
				{
			?>
			<tr class="maintr"  style="border-bottom:2px solid #6E6E6E;">
				<td style="padding:10px;"><?php echo $i+1 ?></td>
				<td style="padding:10px;"><?php echo $itemList[$i]->item_name; ?>&nbsp;&nbsp;(<?php echo $itemList[$i]->item_code; ?>)</td>
				<?php
				 for($a=0;$a<count($auRet);$a++){ ?>
				<!--<td style="padding:10px; " width="20%"></td>-->
				<?php $q=($a&1)?1:0; 
				echo $m= ($q==1)?"<td style='padding:10px;' bgcolor='#98FB98' align='center'>":"<td style='padding:10px;' align='center'>";
				?>
				<!--<td style="padding:10px;" align="center">-->
				<?php
				$item=$_objAdmin->_getSelectList2('table_order_detail',"sum(acc_quantity) as total_qty",''," item_id='".$itemList[$i]->item_id."' and order_id='".$auRet[$a]->order_id."'");
				if(is_array($item)){
				echo $item[0]->total_qty;
				} else {
				echo "0";
				}
				?>
				</td>
				<?php } ?>
				<td style="padding:10px;" ><div style="width: 110px;" align="center"><?php echo $itemList[$i]->total_qty;?></div></td>
				<td style="padding:10px;" ><div style="width: 110px;" align="center"><?php echo $itemList[$i]->amt_total; ?></div></td>
				<td style="padding:10px;" ><div style="width: 50px;" align="center"><?php echo round($itemList[$i]->amt_total/$net_total_item[0]->amt_total*100,2)."%" ?></div></td>
			</tr>
			<?php } ?>
			<tr  bgcolor="#6E6E6E" style="color: #fff;font-weight: bold;" id="candidateNameTD">
				<td style="padding:10px; width:10px;" colspan="<?php echo count($auRet)+2; ?>" align="right">Total</td>
				<td style="padding:10px;"  align="center"><?php echo $net_total_item[0]->totalitem; ?></td>
				<td style="padding:10px;"  align="center"><?php echo $net_total_item[0]->amt_total; ?></td>
				<td style="padding:10px;"  align="center"></td>
			</tr>
		</table>
		</div>
		<?php } else {?>
		<table border="0" width="100%" cellpadding="0" cellspacing="0">
			<tr  style="border-bottom:2px solid #6E6E6E;border-top:2px solid #6E6E6E;" align="center">
				<td style="padding:10px;" colspan="6">Report Not Available</td>
			</tr>
		</table>
		<?php } ?>
		</div>
		<table border="0" width="25%" cellpadding="0" cellspacing="0">
		<tr  >
			<td style="padding:10px;"><div style="width: 2px;" align="left"><input type="button" value="Close" class="form-cen" onclick="javascript:window.close();" /></div></td>
			<td ><div style="width: 20px;" align="center"><input type="button" value="Print" class="result-submit" onclick="PrintElem('#Report')" /></div></td>
			<td ><div style="width: 20px;" align="center"><input type="button" value="Export to Excel" class="result-submit" onclick="location.href='export.inc.php?export_ret_item_vise_report';" /></div></td>
		</tr>
		</table>
		
		</td>
	
	</tr>
	<tr>
		<td><img src="images/blank.gif" width="695" height="1" alt="blank" /></td>
		<td></td>
	</tr>
	</table>
<div class="clear"></div>
</div>
<!--  end content-table-inner  -->
</td>
<td id="tbl-border-right"></td>
</tr>
<!--<tr>
	<th class="sized bottomleft"></th>
	<td id="tbl-border-bottom">&nbsp;</td>
	<th class="sized bottomright"></th>
</tr>-->
</table>
<div class="clear">&nbsp;</div>
</div>
<!--  end content -->
<div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->
<div class="clear">&nbsp;</div> 
<!-- start footer -->         
<?php include("footer.php") ?>
<?php unset($_SESSION['SalAttList']); ?>
<!-- end footer -->
<script src="javascripts/jquery-1.8.2.js" type="text/javascript"></script>
<script src="javascripts/jquery-ui.js"></script>
<script type="text/javascript" src="javascripts/validate.js"></script>
<script type="text/javascript">
// Popup window code
function newPopup(url) {
	popupWindow = window.open(
		url,'popUpWindow','height=500,width=600,left=200,top=100,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no,status=yes')
}
</script>
<script>
    $(function() {
        $( "#from" ).datepicker({
			dateFormat: "d M yy",
            defaultDate: "w",
            changeMonth: true,
			changeYear: true,
			yearRange: '2010:2020',
            numberOfMonths: 1,
            onSelect: function( selectedDate ) {
                $( "#to" ).datepicker( "option", "minDate", selectedDate );
            }
        });
        $( "#to" ).datepicker({
			dateFormat: "d M yy",
            defaultDate: "-w",
            changeMonth: true,
			changeYear: true,
			yearRange: '2010:2020',
            numberOfMonths: 1,
            onSelect: function( selectedDate ) {
                $( "#from" ).datepicker( "option", "maxDate", selectedDate );
            }
        });
    });
</script>
</body>
</html>