<?php
include("includes/config.inc.php");
include("includes/function.php");
$page_name="Retailer Margin Request";
$_objAdmin = new Admin();

if(isset($_POST['approve']) && $_POST['approve'] == 'yes')
{
	$cid = $_objAdmin->_dbUpdate(array("status"=>'A'),'table_retailer_margin_activity', " id='".$_POST['id']."'");
	$auRec = $_objAdmin->_getSelectList('table_retailer_margin_activity',"`retailer_id`, `category_id`, req_mar_val",''," id=".$_POST['id']);
	
	$condi = "retailer_id=".$auRec[0]->retailer_id." AND category_id = ".$auRec[0]->category_id."";
	
	$chkRetMar = $_objAdmin->_getSelectList('table_retailer_margin',"`retailer_id`, `category_id`",'',$condi);
	if(sizeof($chkRetMar) > 0) {
		$id = $_objAdmin->_dbUpdate(array("ret_mar_val"=>$auRec[0]->req_mar_val),'table_retailer_margin',$condi);
		echo $id;
	} else {
	
		$data['account_id']= $_SESSION['accountId'];
		$data['retailer_id']= $auRec[0]->retailer_id;
		$data['category_id']= $auRec[0]->category_id;	
		$data['ret_mar_val']= $auRec[0]->req_mar_val;	
		$data['last_update_time']=date('Y-m-d H:i:s');
		$id = $_objAdmin->_dbInsert($data,'table_retailer_margin');	
		echo $id;
		
	}
	exit;
}	


if(isset($_POST['disapprove']) && $_POST['disapprove'] == 'no')
{
	$cid = $_objAdmin->_dbUpdate(array("status"=>'C'),'table_retailer_margin_activity', " id='".$_POST['id']."'");
	$auRec = $_objAdmin->_getSelectList('table_retailer_margin_activity',"`retailer_id`, `category_id`, `order_id`",''," id=".$_POST['id']);
	
	$id = $_objAdmin->_dbUpdate(array("order_status"=>'C'),'table_order',' order_id='.$auRec[0]->order_id);
	echo $id;
	//$condi = "retailer_id=".$auRec[0]->retailer_id." AND category_id = ".$auRec[0]->category_id."";
	
	/*$chkRetMar = $_objAdmin->_getSelectList('table_retailer_margin',"`retailer_id`, `category_id`",'',$condi);
	if(sizeof($chkRetMar) > 0) {
		$_objAdmin->_dbUpdate(array("ret_mar_val"=>'0'),'table_retailer_margin',$condi);
	}*/
	//header("Location:retailer_margin_request.php");
	exit;
}	



if(isset($_REQUEST['show']) && $_REQUEST['show']=="yes")
{
	$_objAdmin->showRequestedRetailerMargin();

	die;
}


?>

<?php include("header.inc.php");?>

<script type="text/javascript">

function loadingdialog(type, ID) {
	//alert(Status);
	//alert(Value);
		   $( "#loadingAjax" ).dialog({
			  title:'Margin confirmation',
			  content:'Are you sure to approve this margin',
			  resizable: false,
			  height:'220',
			  modal: true,
			  buttons: {
				"Accept": function() {
					$.post( 'retailer_margin_request.php', { id:ID, approve: "yes" } ).done(function( data ) {
						if($.isNumeric(data) || isNaN(data)) alert('Margin confirm successfully.');
						window.location.reload(); //Another possiblity
					  });
										
					//window.location.reload(); //Another possiblity
				  //history.go(0); //And another
				},
				'Reject': function() {
					$.post( 'retailer_margin_request.php', { id:ID, disapprove: "no" } ).done(function( data ) {
						if($.isNumeric(data) || isNaN(data)) alert('Margin has been canceled.');
						window.location.reload(); //Another possiblity
					  });
				},
				Cancel: function() {
				  $( this ).dialog( "close" );
				}
			  }
			});
						 //  });
}
	// Updated on 13 Nov 2013
</script>
<input name="pagename" type="hidden"  id="pagename" value="retailer_margin_request.php" />
<div id="loadingAjax" style="display:none;">Are you sure to approve this margin?</div>
<!-- start content-outer -->
<div id="content-outer">
<!-- start content -->
<div id="content">
<div id="page-heading"><h1><span style="color: #d74343; font-family: Tahoma; font-weight: bold;">Retailer Margin Request</span></h1></div>
<table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">
<!--<tr>
	<th rowspan="3" class="sized"><img src="images/side_shadowleft.jpg" width="20" height="300" alt="" /></th>
	<th class="topleft"></th>
	<td id="tbl-border-top">&nbsp;</td>
	<th class="topright"></th>
	<th rowspan="3" class="sized"><img src="images/side_shadowright.jpg" width="20" height="300" alt="" /></th>
</tr>
-->
<tr>
	<!--<td id="tbl-border-left"></td>-->
	<td>
	<!--  start content-table-inner -->
	<div id="content-table-inner">
	
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
	<tr valign="top">
	<td>
		<!-- start id-form -->
		<table  border="0" cellspacing="0" cellpadding="0"  align="center" width="100%">
				<tr>
				<td align="lift" valign="top"  >
				<table id="flex1" style="display:none"></table>
				<script type="text/javascript">showRequestedRetailerMargin();</script>
				</td>
				</tr>
				</table>
	<!-- end id-form  -->

	</td>
	<td>

	<!-- right bar-->
</td>
</tr>
<tr>
<td><img src="images/blank.gif" width="695" height="1" alt="blank" /></td>
<td></td>
</tr>
</table>
<div class="clear"></div>
</div>
<!--  end content-table-inner  -->
</td>
<td id="tbl-border-right"></td>
</tr>
<!--<tr>
	<th class="sized bottomleft"></th>
	<td id="tbl-border-bottom">&nbsp;</td>
	<th class="sized bottomright"></th>
</tr>-->
</table>
<div class="clear">&nbsp;</div>
</div>
<!--  end content -->
<div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->
<div class="clear">&nbsp;</div> 
<!-- start footer -->         
<?php include("footer.php") ?>
<!-- end footer -->
 <script src="javascripts/jquery-1.8.2.js" type="text/javascript"></script>
<script src="javascripts/jquery-ui.js"></script>
<script type="text/javascript" src="javascripts/validate.js"></script>
</body>
</html>