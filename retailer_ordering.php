<?php include("includes/config.inc.php"); 
	  include("includes/function.php");
	  $page_name="Retailer Ordering";
	  $_objAdmin = new Admin();
	  
	  if(isset($_GET['rid']) && is_numeric($_GET['rid']) && !empty($_GET['rid']))
	  {
	  		$retailerCount = '';
	  
	  /*$auRec = $_objAdmin->_getSelectList('table_route_retailer AS RR LEFT JOIN table_retailer AS R ON RR.retailer_id = R.retailer_id LEFT JOIN table_route AS RT ON RR.route_id = RT.route_id','RT.route_name, RR.retailer_id, R.retailer_name, R.retailer_location, R.lat, R.lng ','','  R.lng!="" AND R.lat!=""  AND RR.route_id = '.$_GET['rid'].' ORDER BY ISNULL(sort_order),  sort_order ASC');*/

	  $auRec=$_objAdmin->_getSelectList('table_route_retailer as RT 
						 		LEFT JOIN table_retailer AS R on RT.retailer_id = R.retailer_id AND RT.status="R" 
						 		LEFT JOIN table_distributors AS D on RT.retailer_id = D.distributor_id AND RT.status="D" 
						 		LEFT JOIN table_customer AS C on RT.retailer_id = C.customer_id AND RT.status="C" 
						 		LEFT JOIN table_route AS RU on RT.route_id = RU.route_id 
						 		',"RU.route_name,RT.retailer_id,RT.status,RT.sort_order,R.retailer_id as retailer,D.distributor_id,C.customer_id,
						 		CASE RT.status 
						 		WHEN 'R' THEN R.retailer_name 
						 		WHEN 'D' THEN D.distributor_name 
						 		WHEN 'C' THEN C.customer_name END AS retailer_name,	
						 		CASE RT.status 
						 		WHEN 'R' THEN R.lat 
						 		WHEN 'D' THEN D.lat 
						 		WHEN 'C' THEN C.lat END AS lat,
						 		CASE RT.status 
						 		WHEN 'R' THEN R.lng 
						 		WHEN 'D' THEN D.lng 
						 		WHEN 'C' THEN C.lng END AS lng",''," RT.route_id='".$_GET['rid']."' ORDER BY ISNULL(RT.sort_order),  RT.sort_order ASC");



//echo "<pre>";
//print_r($auRec);


	$retailerCount = sizeof($auRec);		
	// Get retailer ID from Today Route retailer Table
	 if($retailerCount>=0) {
	  if($auRec[0]->lat!='' && $auRec[0]->lng!='')
	 	
		{
			//$err="This Salesman didn't work during these period";
			$lat = $auRec[0]->lat;
			$lng = $auRec[0]->lng;
			$retailername = htmlspecialchars($auRec[0]->retailer_name, ENT_QUOTES);
		
		}else {
				$lat = '28.3794788';
				$lng = '77.31431410000005';
				$retailername = 'Default';
		}
	}
	  
	 } else {
	 	

	 	$lat = '28.3794788';
		$lng = '77.31431410000005';
		$retailername =  'Default';
	 
	 }
	 
	 
	 /*$auRec2 = $_objAdmin->_getSelectList('table_route_retailer AS RR LEFT JOIN table_retailer AS R ON RR.retailer_id = R.retailer_id LEFT JOIN table_route AS RT ON RR.route_id = RT.route_id','RT.route_name, RR.retailer_id, R.retailer_name','',' RR.route_id = '.$_GET['rid'].' ORDER BY ISNULL(sort_order),  sort_order ASC');*/
	 	$auRec2=$_objAdmin->_getSelectList('table_route_retailer as RT 
						 		LEFT JOIN table_retailer AS R on RT.retailer_id = R.retailer_id AND RT.status="R" 
						 		LEFT JOIN table_distributors AS D on RT.retailer_id = D.distributor_id AND RT.status="D" 
						 		LEFT JOIN table_customer AS C on RT.retailer_id = C.customer_id AND RT.status="C" 
						 		LEFT JOIN table_route AS RU on RT.route_id = RU.route_id 
						 		',"RU.route_name,RT.retailer_id,RT.status,RT.sort_order,R.retailer_id as retailer,D.distributor_id,C.customer_id,
						 		CASE RT.status 
						 		WHEN 'R' THEN R.retailer_name 
						 		WHEN 'D' THEN D.distributor_name 
						 		WHEN 'C' THEN C.customer_name END AS retailer_name,	
						 		CASE RT.status 
						 		WHEN 'R' THEN R.lat 
						 		WHEN 'D' THEN D.lat 
						 		WHEN 'C' THEN C.lat END AS lat,
						 		CASE RT.status 
						 		WHEN 'R' THEN R.lng 
						 		WHEN 'D' THEN D.lng 
						 		WHEN 'C' THEN C.lng END AS lng",''," RT.route_id='".$_GET['rid']."' ORDER BY ISNULL(RT.sort_order),  RT.sort_order ASC");

	$retailerCount2 = sizeof($auRec2);	
	 include("header.inc.php");
?>


<script src="javascripts/jquery.sortable.js"></script>
<script>
	$(function() { 
		//initialize();
		$('#message-box').hide(); 
		$('.sortable').sortable(); 
	});


	function saveListOrder() {


		//var count = $('.sortable').find('li').length;
		//alert(count);
		
		var retailerId = [];
		var retailerType = [];
		$('.sortable li').each(function() {
		 retailerId.push($(this).attr('title'));
		 retailerType.push($(this).attr('class')); 
		});
		
		/*$.each(retailerType, function(key, value){
			alert("Key " + key +"value = "+ value);
		});*/
		
		var rid = $('#routeID').val();
		//alert(rid);
		if(typeof retailerId!='undefined' && retailerId!='' && rid!='')
		 {

			//$('#dd-form').serialize();
			$.ajax({
				type: "POST",
				url: 'ajaxsavesortorder.php',
				data: 'ID='+retailerId+'&rid='+rid+'&Rtype='+retailerType,
				success: function(status){
					//alert(status);
					if(status)
					{
						$('#message-box').show();
						$('#message-box').text('Updating the sort order in the database.');
						setTimeout(function(){
						$('#message-box').hide('slow'); 
						window.location.href = 'retailer_ordering.php?rid='+<?php echo $_GET['rid'];?>;
						},2000);
						
					} else {
						alert('error');
					}
				}
				});	
		}
	}
 
 </script>
<!-- start content-outer -->
<?php //include('actualvsplanned.php');?>
<style>
.sortable		{ padding:0; }
.sortable li	{ padding:4px 8px; color:#000; cursor:move; list-style:none; width:350px; background:#ddd; margin:10px 0; border:1px solid #999; }
#message-box		{ background:#fffea1; border:2px solid #fc0; padding:4px 8px; margin:0 0 14px 0; width:350px; }
</style>
<div id="content-outer">
<!-- start content -->
<div id="content">
<div id="page-heading">
	<h1><span style="color: #d74343; font-family: Tahoma; font-weight: bold;">Retailer Ordering(<?php echo $auRec2[0]->route_name;?>)</span></h1>
</div>

<table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">
<tr>
	<th rowspan="3" class="sized"><img src="images/side_shadowleft.jpg" width="20" height="300" alt="" /></th>
	<th class="topleft"></th>
	<td id="tbl-border-top">&nbsp;</td>
	<th class="topright"></th>
	<th rowspan="3" class="sized"><img src="images/side_shadowright.jpg" width="20" height="300" alt="" /></th>
</tr>
<tr>
	<td id="tbl-border-left"></td>
	<td>
	<!--  start content-table-inner -->
	<div id="content-table-inner">
	<div id="message-box" style="display:none;"></div>
	<h2>List of Route Dealers(<?php echo $retailerCount2;?>)</h2>
	<form id="dd-form" action="#" method="post">
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
	<tr valign="top">
	<td>
		<!-- start id-form -->
		<table  border="0" cellspacing="0" cellpadding="0"  align="center" width="100%">
			<tr>
				<td width="48%" valign="top">
				<div style="height:600px; width:430px; overflow:scroll;">
				<?php 
					
				if(sizeof($auRec2)>=1) {?>					
					
					<input type="hidden" name="routeID" id="routeID" value="<?php echo $_GET['rid'];?>" />
					<!--<p><input type="checkbox" value="1" name="autoSubmit" id="autoSubmit" <-?php if($_POST['autoSubmit']) { echo 'checked="checked"'; } ?> /><label for="autoSubmit">Automatically submit on drop event</label></p>-->
					<ul class="sortable">
					<?php foreach($auRec2 as $key=>$value):?>
						  	<!--<li title="<-?php echo $value->retailer_id;?>"><-?php echo $key+1 ;?>.&nbsp;<-?php echo ucwords(strtolower($value->retailer_name));?></li>-->
								  	<?php
								  	$position = $key+1; 

						 			 if($value->status == 'R'){
						 			    $user_id = $value->retailer_id;
						 			    $user_name = $position.". ".$value->retailer_name."<strong style='color:red;'> Retailer</strong>"; 
						 			}elseif($value->status == 'D'){ 
						 				$user_id =  $value->distributor_id;
						 				$user_name = $position.". ".$value->retailer_name."<strong style='color:green;'> Distributor</strong>"; 
						 			}elseif($value->status =='C'){ 
						 				$user_id =  $value->customer_id;
						 				$user_name = $position.". ".$value->retailer_name."<strong style='color:blue;'> Customer</strong>"; 
						 			}  ?>
						 		 
						 			<!--<li title="<-?php echo $user_id; ?>" class="<-?php echo $value->status; ?>">						 			
						 			<label><-?php 
						 			echo $user_name; ?></label>						 			
						 			</li>-->
						 			<li title="<?php echo $user_id;?>" class="<?php echo $value->status; ?>"><?php echo ucwords(strtolower($user_name));?></li>
							<?php endforeach; ?>
					</ul>
				
					
				<?php } else { ?>
				
				<p>Sorry!  There are no dealers in the system.</p>
				
			<?php } ?>
			</div>
			</td>
			<td width="52%" id="">
				<div id="map" style="width:480px; height:500px;margin-top:-85px;"></div> 
			</td>
		</tr>
		</table>
		<!-- end id-form  -->
	</td>
	<td>
	<!-- right bar-->
	</td>
	</tr>
<tr>
<td><img src="images/blank.gif" width="695" height="1" alt="blank" /></td>
<td></td>
</tr>
</table>
	<input type="button" name="do_submit" value="Submit Sortation" onclick="saveListOrder();" class="button" />
	<input type="button" name="back" value="Back" onclick="location.href='route.php'" class="button" />
	</form>
	<div class="clear"></div>
</div>
<!--  end content-table-inner  -->
</td>
<td id="tbl-border-right"></td>
</tr>
<tr>
	<th class="sized bottomleft"></th>
	<td id="tbl-border-bottom">&nbsp;</td>
	<th class="sized bottomright"></th>
</tr>
</table>

<div class="clear">&nbsp;</div>
</div>
<!--  end content -->
<div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->
<div class="clear">&nbsp;</div> 
<!-- start footer -->         
<?php include("footer.php");?>
<!-- end footer -->
    <!-- <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script> -->
    <script src="https://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyAuAzZRDkMuaLSPt1I-MPNlAOlzbJjbLlU"></script>
     
     <script>
// This example creates a simple polygon representing the Bermuda Triangle.

function initialize() {
  var mapOptions = {
    zoom: 10,
    center: new google.maps.LatLng(<?php echo $lat.",".$lng;?>),
    mapTypeId: google.maps.MapTypeId.ROADMAP
  };

  var bermudaTriangle;

  var map = new google.maps.Map(document.getElementById('map'),
      mapOptions);


	// Create Start Point
	  var myLatLng = new google.maps.LatLng(<?php echo $lat;?>,<?php echo $lng;?>);
          var marker = new google.maps.Marker({
              position: myLatLng,
              map: map,
              //icon: 'https://chart.googleapis.com/chart?chst=d_bubble_text_small&chld=bb|Start|006600|FFFFFF',
			  icon: 'https://chart.googleapis.com/chart?chst=d_map_pin_letter&chld=S|FF776B|000000|FFFFFF',
			  //icon: 'images/icon_greenA.png',
              title: '<?php echo $retailername;?>'
          });


	// Create all mid points
	<?php 
	
	if($retailerCount>1){
	
	for($h = 1; $h < $retailerCount-1; $h++)  {?>

		
	  var myLatLng = new google.maps.LatLng(<?php echo $auRec[$h]->lat;?>,<?php echo $auRec[$h]->lng;?>);
      var marker = new google.maps.Marker({
              position: myLatLng,
              map: map,
              icon: 'https://chart.googleapis.com/chart?chst=d_map_pin_letter&chld='+<?php echo $h+1;?>+'|FF776B|000000',
              title: '<?php echo htmlspecialchars($auRec[$h]->retailer_name, ENT_QUOTES);?>',
              zIndex: <?php echo intval($auRec[$h]->sort_order);?>,
          });
		  
		  
		   var infowindow = new google.maps.InfoWindow({
				  content: 'Hello'
			  });



		  google.maps.event.addListener(marker, 'click', function() {
			infowindow.open(map,marker);
		  });
		
	<?php } ?>

	
	//Created last or end points
					  
	  <?php $lastIndexof = $retailerCount-1;?>
	  
	  
		 var myLatLng = new google.maps.LatLng(<?php echo $auRec[$lastIndexof]->lat;?>,<?php echo $auRec[$lastIndexof]->lng;?>);
         var marker = new google.maps.Marker({
              position: myLatLng,
              map: map,
              //icon: 'https://chart.googleapis.com/chart?chst=d_bubble_text_small&chld=bb|End|FF6633',
			  icon: 'https://chart.googleapis.com/chart?chst=d_map_pin_letter&chld=E|FF776B|000000',
			  //icon: 'images/icon_greenB.png',
              title: '<?php echo htmlspecialchars($auRec[$lastIndexof]->retailer_name, ENT_QUOTES);?>'
          });

			
				
		
		// Create polygon
		<?php }
		$k=0;
		for($j=0;$j<$retailerCount-1;$j++) { $k=$j+1; ?>
		var flightPlanCoordinates = [
		new google.maps.LatLng(<?php echo $auRec[$j]->lat.",".$auRec[$j]->lng; ?> ),
		new google.maps.LatLng(<?php echo $auRec[$k]->lat.",".$auRec[$k]->lng; ?> )
		];
		var lineSymbol = {
        path: google.maps.SymbolPath.FORWARD_CLOSED_ARROW
		};
		var arrow = {
			icon: lineSymbol,
			offset: '50%',
			strokeColor: "#980000 ",
			strokeOpacity :1,
		};
		var flightPath = new google.maps.Polyline({
						path: flightPlanCoordinates,
						strokeColor: "#980000 ",
						strokeOpacity: 1.0,
						geodesic: true,
						strokeWeight: 2,
						icons : [arrow],
						});
		flightPath.setMap(map);
		<?php }  ?>
	}

	google.maps.event.addDomListener(window, 'load', initialize);

    </script> 
</body>
</html>