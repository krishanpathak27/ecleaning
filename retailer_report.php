<?php include("includes/config.inc.php");
      include("includes/function.php");
	  include("includes/globalarraylist.php");
	  
	  
		$page_name="Dealer Wise Report";
		
		$retailer_name = '-'; 
		$percentage = array();

	if(isset($_POST['showReport']) && $_POST['showReport'] == 'yes')
	{	
		if($_POST['market']!="") 
		{
			$_SESSION['MarketList']=$_POST['market'];
			
		} else {
			
			unset($_SESSION['MarketList']);
		}
	
		
			if($_POST['filterby']!=""){
		
			$_SESSION['retfilterBY']=$_POST['filterby'];	
		
		}
	
		if($_POST['sname']!="") 
		{
			$_SESSION['SnameList']=$_POST['sname'];	
			
		} else {
		
			unset($_SESSION['SnameList']);
		}
	
	
		if($_POST['from']!="") 
		{
			$_SESSION['FromRetList']=$_objAdmin->_changeDate($_POST['from']);	
		}
		
		if($_POST['to']!="") 
		{
			$_SESSION['ToRetList']=$_objAdmin->_changeDate($_POST['to']);	
		}
		
		if($_POST['city']!="") 
		{
			$_SESSION['CityList']=$_POST['city'];
		}
	
		
		
		$_SESSION['order_by']="desc";
	
	
	} 
	else 
	{
	
		$_SESSION['FromRetList']= $_objAdmin->_changeDate(date("Y-m-d"));
		$_SESSION['ToRetList']= $_objAdmin->_changeDate(date("Y-m-d"));
		$_SESSION['order_by']="desc";
	
	}
		if($_SESSION['retfilterBY']==''){
		
			$_SESSION['retfilterBY']=1;	
		
		}
	
	
	if(isset($_REQUEST['reset']) && $_REQUEST['reset'] == 'yes')
	{
		unset($_SESSION['CityList']);	
		unset($_SESSION['MarketList']);	
		unset($_SESSION['SnameList']);
		header("Location: retailer_report.php");
	}

	if($_SESSION['CityList']!='')
	{
		$CityName=$_objAdmin->_getSelectList2('city','city_name',''," city_id='".$_SESSION['CityList']."'"); 
		$city=$CityName[0]->city_name;
	}

	if($_SESSION['SnameList']!='')
	{
		//$salesman="AND o.salesman_id='".$_SESSION['SnameList']."'";
		$salArrList=$_SESSION['SnameList'];	
		$filterby=$_SESSION['retfilterBY'];
		$salesman = $_objArrayList->getSalesmanConsolidatedList($salArrList,$filterby);
	}


	if($_SESSION['MarketList']!=''){
	$list="r.retailer_location='".$_SESSION['MarketList']."'";
	} else {
	$list="r.city='".$_SESSION['CityList']."'";
	}
	
	if($_REQUEST['order_by']!='')
	{
		if($_REQUEST['order_by']=='desc')
		{
			$_SESSION['order_by']="asc";
		} else {
			$_SESSION['order_by']="desc";
		}
	
		$_SESSION['FromRetList']=$_REQUEST['from'];
		$_SESSION['ToRetList']=$_REQUEST['to'];
	
	} else {
		
		if($_POST['city']=="")
		{
			unset($_SESSION['CityList']);
		}
		
		if($_POST['sname']=="")
		{
			unset($_SESSION['SnameList']);
		}
	}
	

if( $_SESSION['userLoginType']==3){
	$disLogCond="o.distributor_id='".$_SESSION['distributorId']."' and ";
}

$DistrictName=$_objAdmin->_getSelectList2('city','city_id,city_name','',"");

if( $_SESSION['userLoginType']==5){
	$DivisionId=$_objAdmin->_getSelectList2('table_salesman','division_id',''," salesman_id='".$_SESSION['salesmanId']."'");
	
if($DivisionId->division_id==6)
{
	$DistrictName=$_objAdmin->_getSelectList2('city','city_id,city_name','',"state_id >46 OR state_id NOT IN(79,333,334)");
	 //print_r($DistrictName); die;
}
else
{
	$DistrictName=$_objAdmin->_getSelectList2('city','city_id,city_name','',"state_id <=46 OR state_id IN(79,333,334)");

}


	//print_r($_SESSION['DistrictName']);
}
?>

<?php include("header.inc.php") ?>

<script type="text/javascript">
function showMarket(str,id)
{
//alert(str);
//alert(id);
	$.ajax({
		'type': 'POST',
		'url': 'ret_dropdown_market.php',
		'data': 'c='+str,
		'success' : function(mystring) {
			//alert(mystring);
		document.getElementById("outputMarket").innerHTML = mystring;
		$('#marketLevel').show();
		}
	});
}
</script>
<script type="text/javascript">

    function PrintElem(elem)
    {
        Popup($(elem).html());
    }

    function Popup(data) 
    {
		
        var mywindow = window.open('', 'Report');
		
        mywindow.document.write('<html><head><title>Dealer Wise Report</title>');
		mywindow.document.write('<table><tr><td><b>City Name:</b> <?php echo $city; ?></td><td><b>Market Name:</b> <?php echo $_SESSION['MarketList']; ?></td><td><b>From Date:</b> <?php echo $_SESSION['FromRetList']; ?></td><td><b>To Date:</b> <?php echo $_SESSION['ToRetList']; ?></td></tr></table>');
        /*optional stylesheet*/ //mywindow.document.write('<link rel="stylesheet" href="main.css" type="text/css" />');
        mywindow.document.write('</head><body >');
        mywindow.document.write(data);
        mywindow.document.write('</body></html>');

        mywindow.print();
        mywindow.close();
        return true;
    }

	$(document).ready(function(){
		$('.maintr').click(function() {
			$('#lists tr').removeClass('trbgcolor');
			
			$(this).addClass('trbgcolor');
		});
	});
	
$(document).ready(function() {
    
   <?php 
   if($_POST['submit']=='Export to Excel'){?>
   window.location.assign("export.inc.php?export_ret_vise_report");
   <?php } ?> 

	
});		
	
	
	$(document).keydown(function(e) {
		//alert(e);
			if(e.keyCode==113){
			
				var id = $('.trbgcolor input').val();
				if(typeof id!='undefined')
				//alert(id); 
				window.open("retailer_items_report.php?retid="+id, '_blank');
				else 
				alert('Please Select One Record');
			}
			
		});
		
	
	
	
</script>

<script src="javascripts/dateNextPrev.js" type="text/javascript"></script>
<!-- start content-outer -->
<div id="content-outer">
<!-- start content -->
<div id="content">
<div id="page-heading"><h1><span style="color: #d74343; font-family: Tahoma; font-weight: bold;">Dealer Wise Report</span></h1></div>
<table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">

<tr>
	<!--<td id="tbl-border-left"></td>-->
	<td>
	<!--  start content-table-inner -->
	
	<div id="content-table-inner">
	
	<div id="page-heading" align="left" >
	<form name="frmPre" id="frmPre" method="post" action="" enctype="multipart/form-data" >
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
	<tr>
		
		
		<td ><h3>District: </h3><h6>
		<select name="city" id="city" class="menulist" style="width:120px;" >
			<?php 
			if(is_array($DistrictName)){

		if($flexiLoad == 'flex')
				$data.= '<option value="All" >All</option>';		
			else if($flexiLoad == 'city')
				$data.= '<option value="">Please Select</option>';	
			else 
				$data.= '<option value="">All</option>';
		foreach($DistrictName as $value):
			if($value->city_id == $selectedCity){ $selected = "selected"; } else{ $selected = ""; }
			$data.="<option value=".$value->city_id." $selected>".$value->city_name."</option>";
		endforeach;
		} else {
		
			$data.= '<option value="" >No City Available</option>';
		
		}
		echo $data;


			//echo $cityList = $_objArrayList->getCityListOptions($_SESSION['CityList'],'city');?>
		</select></h6>

		</td>
		
		<td id="marketLevel" style="display:none;"><h3>City:</h3><h6><div id="outputMarket"></div></h6>
		
		</td>
		
		<td><h3>Salesman: </h3><h6>
		<select name="sname" id="sname" class="menulist" >
			<?php echo $salesmanOptionList = $_objArrayList->GetSalesmenMenu($salsList, $_SESSION['SnameList']);?>
		</select></h6></td>
		
		
		
		<td><h3>From Date:</h3><h6><img src="css/images/prev.png" height="18" width="18" onclick="dateFromPrev();">  <input type="text" id="from" name="from" class="date" style="width:100px" value="<?php  echo $_SESSION['FromRetList'];?>"  readonly /> <img src="css/images/next.png" height="18" width="18" onclick="dateFromNext();"></h6></td>
		<td><h3>To Date: </h3> <h6> <img src="css/images/prev.png" height="18" width="18" onclick="dateToPrev();"> <input type="text" id="to" name="to" class="date" style="width:100px" value="<?php echo $_SESSION['ToRetList']; ?>"  readonly /> <img src="css/images/next.png" height="18" width="18" onclick="dateToNext();"></h6></td>
		<td><h3></h3>
		<input name="submit" class="result-submit" type="submit" id="submit" value="View Details" />
		<input type="button" value="Reset!" class="form-reset" onclick="location.href='retailer_report.php?reset=yes';" />	
		</td>
		</tr>
		<tr class="consolidatedReport">
		<td colspan="3" ><h2>View Report For:&nbsp;&nbsp;
		<input type="radio" name="filterby" value="1" <?php if($_SESSION['retfilterBY']==1){ ?> checked="checked" <?php } ?>   />&nbsp;Individual
		<input type="radio" name="filterby" value="2" <?php if($_SESSION['retfilterBY']==2){ ?> checked="checked" <?php } ?>  />&nbsp;Hierarchy</h2>
		</td>
		</tr>
		<tr>
		<td colspan="8"><input name="showReport" type="hidden" value="yes" />
	<input type="button" value="Print" class="result-submit" onclick="PrintElem('#Report')" />
	<input type="submit" name="submit" value="Export to Excel" class="result-submit"  />
	<!--<a href="retailer_report.php#chart_div"><input name="graph" class="result-submit" type="button" id="graph" value="Show Graph" /></a>-->	
	</td>
	</tr>
	</table>
	</form>
	</div>
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
	<?php
	if($_SESSION['CityList']==""){
	?>
	<tr valign="top">
		<td>
		<table border="0" width="100%" cellpadding="0" cellspacing="0" id="id-form">
			<tr bgcolor="#A52A2A" style="color: #fff;font-weight: bold;">
				<td align="center" style="padding:10px;">Select City</td>
			</tr>
		</table>
		</td>
		<?php } else { ?>
		
		<td>
		<div style="padding:10px; font-size:15px; font-weight:bold;" >Press F2 to Check Dealer's Item Wise Report</div>
		<div style="width:1000px;overflow:auto; height:auto;overflow:auto;" >
		<div id="Report">
		
			<?php
			$auRet=$_objAdmin->_getSelectList('table_order as o left join table_retailer as r on o.retailer_id=r.retailer_id 
			LEFT JOIN table_salesman AS s ON s.salesman_id = o.salesman_id 
			',"COUNT(o.retailer_id) as total",''," $disLogCond $list and (o.date_of_order BETWEEN '".date('Y-m-d', strtotime($_SESSION['FromRetList']))."' AND '".date('Y-m-d', strtotime($_SESSION['ToRetList']))."') $salesman AND r.new='' GROUP BY o.retailer_id ORDER BY total desc LIMIT 1");
			if(is_array($auRet)){
			?>
			<table  border="1"  cellpadding="0" cellspacing="0" id="lists">
			<tr  bgcolor="#6E6E6E" style="color: #fff;font-weight: bold;" id="candidateNameTD">
				<td style="padding:10px;" width="20%"><div style="width: 100px;">Dealer Name</div></td>
				<td style="padding:10px;" width="20%"><div style="width: 100px;">Dealer Code</div></td>
				<td style="padding:10px;" width="10%"><div style="width: 100px;">State</div></td>
				<td style="padding:10px;" width="10%"><div style="width: 100px;">District</div></td>
				<td style="padding:10px;" width="10%"><div style="width: 100px;">Area</div></td>
				<td style="padding:10px;" width="10%"><div style="width: 100px;">Distributor</div></td>
				<td style="padding:10px;" width="10%"><div style="width: 100px;">Distributor Code</div></td>
				<form name="submitForm" method="POST" action="">
				<input type="hidden" name="order_by" value="<?php echo $_SESSION['order_by']; ?>">
				<input type="hidden" name="from" value="<?php echo $_SESSION['FromRetList']; ?>">
				<input type="hidden" name="to" value="<?php echo $_SESSION['ToRetList'];  ?>">
				<!--<td style="padding:10px;" width="10%"><div style="width: 65px;">Total &nbsp;&nbsp;&nbsp;&nbsp;<a href="retailer_report.php?order_by=<?php echo $_SESSION['order_by']; ?>&from=<?php echo $_SESSION['FromRetList']; ?>&to=<?php echo $_SESSION['ToRetList'];  ?>" ><img src="images/arrow-up.png" width="13" height="13" /></a></div></td>-->
				<td style="padding:10px;" width="10%"><div style="width: 65px;">Total &nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:document.submitForm.submit()">
				<?php if($_SESSION['order_by']=='desc'){ ?>
				<img src="images/arrow-up.png" width="13" height="13" />
				<?php } else { ?>
				<img src="images/arrow-down.png" width="13" height="13" />
				<?php } ?>
				</a></div></td>
				</form>
				<td style="padding:10px;" width="10%"><div style="width: 65px;" align="center">Pro / Tot</div></td>
				<td style="padding:10px;" width="10%"><div style="width: 65px;" align="center">% Share</div></td>
				<?php
				$row_total=$auRet[0]->total;
				 for($a=0;$a<$auRet[0]->total;$a++){ ?>
				<td style="padding:10px; " width="20%"><div style="width: 65px;" align="center">Date <?php echo $a+1; ?></div></td>
				<td style="padding:10px;" width="10%">Salesman</td>
				<td style="padding:10px;" width="10%">Designation</td>				
				<td style="padding:10px;" width="20%"><div style="width: 85px;">Order Value <?php echo $a+1; ?></div></td>
				
				<?php } ?>
				
			</tr>
			
			<?php
			if(isset($_POST['ord'])) { $ordby = $_POST['ord']; } else { $ordby = 'desc';} 
			$orderList=$_objAdmin->_getSelectList('table_order as o left join table_retailer as r on o.retailer_id=r.retailer_id LEFT JOIN table_salesman AS s ON s.salesman_id = o.salesman_id 
			LEFT JOIN table_markets as m on m.market_id=r.market_id
			LEFT JOIN table_distributors as d on d.distributor_id=r.distributor_id
			LEFT JOIN state as st on st.state_id=r.state
			LEFT JOIN city as c on c.city_id=r.city',"sum(o.acc_total_invoice_amount) as total_amt,o.order_id,o.retailer_id,r.retailer_name,r.retailer_code,r.retailer_location,m.market_name,st.state_name,c.city_name,d.distributor_name,d.distributor_code,r.display_outlet",''," $disLogCond $list and (o.date_of_order BETWEEN '".date('Y-m-d', strtotime($_SESSION['FromRetList']))."' AND '".date('Y-m-d', strtotime($_SESSION['ToRetList']))."') $salesman AND r.new='' GROUP BY o.retailer_id  Order by total_amt ".$_SESSION['order_by']."");
			if(is_array($orderList)){
				//echo count($orderList);
				for($i=0;$i<count($orderList);$i++)
				{
					if($orderList[$i]->display_outlet=='Y'){ $outlet='Yes';} elseif ($orderList[$i]->display_outlet=='N'){ $outlet='No';} else{  $outlet=''; }	
					
			?>
			<tr class="maintr"  style="border-bottom:2px solid #6E6E6E;">
			<td style="padding:10px;" width="20%"><input type="hidden" id="<?php echo $i+1; ?>" value="<?php echo $orderList[$i]->retailer_id;?>"> <?php echo $retailer_name = $orderList[$i]->retailer_name;?></td>
			<td style="padding:10px;" width="20%"><?php echo $orderList[$i]->retailer_code;?> </td>
			<td style="padding:10px;" width="20%"><?php echo $orderList[$i]->state_name;?> </td>
			<td style="padding:10px;" width="20%"><?php echo $orderList[$i]->city_name;?> </td>
			<td style="padding:10px;" width="20%"><?php echo $orderList[$i]->market_name;?> </td>
			<td style="padding:10px;" width="20%"><?php echo $orderList[$i]->distributor_name;?> </td>
			<td style="padding:10px;" width="20%"><?php echo $orderList[$i]->distributor_code;?> </td>
			<td style="padding:10px;" width="20%"><?php echo  $orderList[$i]->total_amt;?> </td>
			<?php
			$orderTotal=$_objAdmin->_getSelectList('table_order as o left join table_retailer as r on o.retailer_id=r.retailer_id LEFT JOIN table_salesman AS s ON s.salesman_id = o.salesman_id',"count(o.retailer_id) as total_cal",''," $disLogCond o.retailer_id='".$orderList[$i]->retailer_id."' and (o.date_of_order BETWEEN '".date('Y-m-d', strtotime($_SESSION['FromRetList']))."' AND '".date('Y-m-d', strtotime($_SESSION['ToRetList']))."') $salesman AND r.new='' ");
			?>
			<?php
			$orderTotalPro=$_objAdmin->_getSelectList('table_order as o left join table_retailer as r on o.retailer_id=r.retailer_id LEFT JOIN table_salesman AS s ON s.salesman_id = o.salesman_id',"count(o.retailer_id) as total_cal_pro",''," $disLogCond o.retailer_id='".$orderList[$i]->retailer_id."' and (o.date_of_order BETWEEN '".date('Y-m-d', strtotime($_SESSION['FromRetList']))."' AND '".date('Y-m-d', strtotime($_SESSION['ToRetList']))."') $salesman AND r.new='' AND order_type!='No' ");
			?>
			<td style="padding:10px;" width="20%" align="center"><?php echo $orderTotalPro[0]->total_cal_pro;?> / <?php echo $orderTotal[0]->total_cal;?></td>
			<?php
			$Netorder=$_objAdmin->_getSelectList('table_order as o left join table_retailer as r on o.retailer_id=r.retailer_id LEFT JOIN table_salesman AS s ON s.salesman_id = o.salesman_id',"sum(o.acc_total_invoice_amount) as net_amt",''," $disLogCond $list and (o.date_of_order BETWEEN '".date('Y-m-d', strtotime($_SESSION['FromRetList']))."' AND '".date('Y-m-d', strtotime($_SESSION['ToRetList']))."') $salesman AND r.new='' ");
			?>
			<td style="padding:10px;" width="20%" align="center"><?php echo round($orderList[$i]->total_amt/$Netorder[0]->net_amt*100,2) ."%";
			$percentage[$retailer_name] = round($orderList[$i]->total_amt/$Netorder[0]->net_amt*100,2); ?> </td>
			<?php
			$orderDet=$_objAdmin->_getSelectList('table_order as o left join table_retailer as r on o.retailer_id=r.retailer_id 
			LEFT JOIN table_salesman AS s ON s.salesman_id = o.salesman_id
			 LEFT JOIN table_salesman_hierarchy_relationship AS SH ON SH.salesman_id = s.salesman_id LEFT JOIN table_salesman_hierarchy AS H ON H.hierarchy_id = SH.hierarchy_id LEFT JOIN table_salesman_hierarchy AS H2 ON H2.hierarchy_id = SH.rpt_hierarchy_id',"o.order_id,o.date_of_order,o.acc_total_invoice_amount,o.order_type,s.salesman_name,H.description AS des1",''," $disLogCond o.retailer_id='".$orderList[$i]->retailer_id."' and (o.date_of_order BETWEEN '".date('Y-m-d', strtotime($_SESSION['FromRetList']))."' AND '".date('Y-m-d', strtotime($_SESSION['ToRetList']))."') $salesman AND r.new='' Order by o.date_of_order asc ");
				for($b=0;$b<count($orderDet);$b++)
				{
			?>
			<td style="padding:5px;" bgcolor="#98FB98" width="20%"><?php echo $_objAdmin->_changeDate($orderDet[$b]->date_of_order);?> </td>
			<td style="padding:10px;" width="20%"><?php echo $orderDet[$b]->salesman_name;?> </td>
			<td style="padding:10px;" width="20%"><?php echo $orderDet[$b]->des1;?> </td>
			<td style="padding:10px;" width="20%">
			<?php if($orderDet[$b]->order_type!='No'){
			echo "<a href=\"order_list.php?act_id=".base64_encode($orderDet[$b]->order_id)."\" target='_blank' ><b>".$orderDet[$b]->acc_total_invoice_amount."</b></a>";
			} else {
			echo $orderDet[$b]->acc_total_invoice_amount;
			}
			?> 
			</td>
			<?php } ?>
			<?php
				for($c=0;$c<$row_total-count($orderDet);$c++){
			?>
			<td style="padding:10px;" bgcolor="#98FB98" width="20%" align="center">-</td>
			<td style="padding:10px;" width="20%" align="center">-</td>
			<td style="padding:10px;" width="20%" align="center">-</td>
			<?php } ?>
			</tr>
			<?php } } ?>
			</table>
		<?php } else { ?>
		<table  border="1" width="100%" cellpadding="0" cellspacing="0" id="lists">
		<tr  style="border-bottom:2px solid #6E6E6E;" align="center" >
		 <td style="padding:10px;" width="100%">Report Not Available</td>
		</tr>
		</table>	
		<?php } ?>	
		
		</div>
		</div>
		</td>
	<?php } ?>
	</tr>
	<tr>
		<td><img src="images/blank.gif" width="695" height="1" alt="blank" /></td>
		<td></td>
	</tr>
	</table>
<div class="clear"></div>
<!-- Graph code starts here -->
<?php 
arsort($percentage);
$r_name = array(); 
$p_value = array();
$j=0; 
$p_other = 0;
if(count($percentage)>10)
{
	foreach($percentage as $x=>$x_value)
    {
		if($j<10)
		{
			$r_name[]= $x;
			$p_value[]= $x_value;
			$j=$j+1;
		}
		else
		{
			$r_other = 'Others';
			$p_other = $p_other+$x_value;
		}		
	}
}
else
{
	foreach($percentage as $x=>$x_value)
    {
		$r_name[]= $x;
		$p_value[]= $x_value;
	}
	for($i=count($percentage); $i<10; $i++)
	{
		$r_name[]= '';
		$p_value[]= 0;
	}
	$r_other = 'Others';
	$p_other = 0;
}
 ?>
 
 <script type="text/javascript" src="javascripts/jsapi"></script>
    <script type="text/javascript">
      google.load("visualization", "1", {packages:["corechart"]});
      google.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Retailer', 'Percentage'],
          ['<?php echo $r_name[0]; ?>',     <?php echo $p_value[0]; ?>],
          ['<?php echo $r_name[1]; ?>',     <?php echo $p_value[1]; ?>],
          ['<?php echo $r_name[2]; ?>',  	<?php echo $p_value[2]; ?>],
          ['<?php echo $r_name[3]; ?>', 	<?php echo $p_value[3]; ?>],
		  ['<?php echo $r_name[4]; ?>', 	<?php echo $p_value[4]; ?>],
		  ['<?php echo $r_name[5]; ?>', 	<?php echo $p_value[5]; ?>],
		  ['<?php echo $r_name[6]; ?>', 	<?php echo $p_value[6]; ?>],
		  ['<?php echo $r_name[7]; ?>', 	<?php echo $p_value[7]; ?>],
		  ['<?php echo $r_name[8]; ?>', 	<?php echo $p_value[8]; ?>],
		  ['<?php echo $r_name[9]; ?>', 	<?php echo $p_value[9]; ?>],
          ['<?php echo $r_other; ?>',    <?php echo $p_other; ?>]
        ]);
		
		var rName= "<?php echo $r_name[0]; ?>";
		if(rName==''){
		var dTitle= '';
		}
	else{var dTitle= 'Top 10 Retailers';}
        var options = {
          title: dTitle
        };

        var chart = new google.visualization.PieChart(document.getElementById('chart_div'));
        chart.draw(data, options);
		//alert(chart);
      }
    </script>
    <!--<div id="chart_div" style="width: 100%; height: 500px;"></div>-->
	 <!--Graph code ends here -->
</div>

<!--  end content-table-inner  -->
</td>
<td id="tbl-border-right"></td>
</tr>
<!--<tr>
	<th class="sized bottomleft"></th>
	<td id="tbl-border-bottom">&nbsp;</td>
	<th class="sized bottomright"></th>
</tr>-->
</table>
<div class="clear">&nbsp;</div>
</div>
<!--  end content -->
<div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->
<div class="clear">&nbsp;</div> 
<!-- start footer -->        
<?php include("footer.php") ?>

<!-- end footer -->
<script src="javascripts/jquery-1.8.2.js" type="text/javascript"></script>
<script src="javascripts/jquery-ui.js"></script>
<script type="text/javascript" src="javascripts/validate.js"></script>
<script type="text/javascript">
// Popup window code
function newPopup(url) {
	popupWindow = window.open(
		url,'popUpWindow','height=500,width=600,left=200,top=100,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no,status=yes')
}
</script>
<script>
    $(function() {
        $( "#from" ).datepicker({
			dateFormat: "d M yy",
            defaultDate: "w",
            changeMonth: true,
            numberOfMonths: 1,
            onSelect: function( selectedDate ) {
                $( "#to" ).datepicker( "option", "minDate", selectedDate );
            }
        });
        $( "#to" ).datepicker({
			dateFormat: "d M yy",
            defaultDate: "-w",
            changeMonth: true,
            numberOfMonths: 1,
            onSelect: function( selectedDate ) {
                $( "#from" ).datepicker( "option", "maxDate", selectedDate );
            }
        });
    });
</script>
</body>
</html>
