<?php
include("includes/config.inc.php");
include("includes/function.php");
$page_name="Dealer Sales Entry Report";
$_objAdmin = new Admin();
if($_SESSION['userLoginType']==1)
{
	$valid_user=false;
}else
{
	$valid_user=true;
}


// echo "<pre>";
// 		print_r($_SESSION); 
/*if( $_SESSION['userLoginType']==3){
	echo "ret";die;
	}*/
if(isset($_POST['DispatchOS']) && $_POST['DispatchOS'] == 'yes')
{	
	/*if($_POST['retl']!="" && $_POST['retl']!="all") 
	{
		$_SESSION['RetlID']=$_POST['retl'];	
	}*/
//	Default Value All
	/*if($_POST['retl']=="all") 
	{
	  unset($_SESSION['RetlID']);	
	}*/

	if($_POST['from']!=""){
		$_SESSION['FromOrderList']=$_POST['from'];
	}
	
	if($_POST['to']!=""){
		$_SESSION['ToOrderList']=$_POST['to'];
	}

	if($_POST['distributorID']!="" && $_POST['distributorID']!="all") 
	{

		 $_SESSION['distID']=$_POST['distributorID'];

	}
	if($_POST['distributorID']=="all"){
		unset($_SESSION['distID']);
	}
}

if(isset($_REQUEST['reset']) && $_REQUEST['reset'] == 'yes')
{	
	unset($_SESSION['RetlID']);
	unset($_SESSION['distID']);
	$_SESSION['FromOrderList']= date('Y-m-d');
	$_SESSION['ToOrderList']= date('Y-m-d');
	header("Location: retailer_sales_report.php");
} 	

/*if(isset($_REQUEST['stid']) && $_REQUEST['value']!="")
{
	if($_REQUEST['value']=="Active")
	{	
		$status='I';
	}
	else
	{
		$status='A';
	}
	$cid=$_objAdmin->_dbUpdate(array("status"=>$status, "last_update_date"=>$date, "last_update_status"=>'Update'),'table_sales_entry', " order_id = '".$_REQUEST['stid']."' ");
	header("Location: retailer_sales_report.php");
}*/

if(isset($_REQUEST['show']) && $_REQUEST['show']=="yes")
{
	$_objAdmin->showRetailerSalesReport();
	die;
}
?>

<?php include("header.inc.php") ?>
<script src="javascripts/dateNextPrev.js" type="text/javascript"></script>

<!-- start content-outer -->
<div id="content-outer">
<!-- start content -->
<div id="content">
<div id="page-heading"><h1><span style="color: #d74343; font-family: Tahoma; font-weight: bold;">Sales Report</span></h1></div>
<table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">
<!--<tr>
	<th rowspan="3" class="sized"><img src="images/side_shadowleft.jpg" width="20" height="300" alt="" /></th>
	<th class="topleft"></th>
	<td id="tbl-border-top">&nbsp;</td>
	<th class="topright"></th>
	<th rowspan="3" class="sized"><img src="images/side_shadowright.jpg" width="20" height="300" alt="" /></th>
</tr>
-->
<tr>
	<!--<td id="tbl-border-left"></td>-->
	<td>
	<!--  start content-table-inner -->
	<div id="content-table-inner">		
	<div id="page-heading" align="left" >
	<form name="myform" id="myform" method="post" action="" enctype="multipart/form-data" >
		<table border="0" width="85%" cellpadding="0" cellspacing="0">	
			<tr>
				<!-- <td><h3>&nbsp;&nbsp;Dealer:</h3>
					<h6>
				 		<select name="retl" id="retl" class="styledselect_form_5" style="width:120px;" >
					  		<option value="all">All Dealers</option>

							<?php $aRet=$_objAdmin->_getSelectList('table_retailer','*',''," account_id='".$_SESSION['accountId']."'ORDER BY retailer_name"); 
							if(is_array($aRet)){
							for($i=0;$i<count($aRet);$i++){?>
							<option value="<?php echo $aRet[$i]->retailer_id;?>" <?php if ($aRet[$i]->retailer_id==$_SESSION['RetlID']){ ?> selected <?php } ?>><?php echo $aRet[$i]->retailer_name;?></option>
							<?php } }?>
						</select>
					</h6>
				</td> -->

				<td>
					<h3>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;From Date:</h3>
					<h6>
						<img src="css/images/prev.png" height="18" width="18" onclick="dateFromPrev();"> 
						<input type="text" id="from" name="from" class="date" style="width:120px;" value="<?php if($_SESSION['FromOrderList']!='') { echo $_objAdmin->_changeDate($_SESSION['FromOrderList']); } else { echo $_objAdmin->_changeDate(date('Y-m-d')); }?>"  readonly /> 
						<img src="css/images/next.png" height="18" width="18" onclick="dateFromNext();">
					</h6>
				</td>
				<td>
					<h3>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;To Date:</h3>
					<h6>
						<img src="css/images/prev.png" height="18" width="18" onclick="dateToPrev();"> 
						<input type="text" id="to" name="to" class="date" style="width:120px;" value="<?php if($_SESSION['ToOrderList']!='') { echo $_objAdmin->_changeDate($_SESSION['ToOrderList']); } else { echo $_objAdmin->_changeDate(date('Y-m-d')); }?>"  readonly /> 
						<img src="css/images/next.png" height="18" width="18" onclick="dateToNext();">
					</h6>
				</td>

		 		<td>
		 			<h3></h3>
			 		<input name="DispatchOS" type="hidden" value="yes" />		
					<input name="submitforms" class="result-submit" type="submit" id="submitform" value="View Details" />
					<input type="button" value="Reset!" class="form-reset" onclick="location.href='retailer_sales_report.php?reset=yes';" />
				</td>
				<!--
				<input name="export" class="result-submit" type="submit" id="export" value="Export to Excel" >
				</td> -->
			</tr>			
		</table>
	</form>
	
	</div>
	
	
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
	<tr valign="top">
	<td>
		<!-- start id-form -->
		<table  border="0" cellspacing="0" cellpadding="0"  align="center" width="100%">
				<tr>
				<td align="lift" valign="top"  >
				<table id="flex1" style="display:none"></table>
				<script type="text/javascript">

                var time=<?php echo json_encode($valid_user); ?>;
				showRetailerSalesReport(time);</script>
				</td>
				</tr>
				</table>
	<!-- end id-form  -->

	</td>
<td>
 <!-- right bar-->
 <?php //include("rightbar/dispatch_bar.php") ?>
 </td>
</tr>
<tr>
<td><img src="images/blank.gif" width="695" height="1" alt="blank" /></td>
<td></td>
</tr>
</table>
<div class="clear"></div>
</div>
<!--  end content-table-inner  -->
</td>
<td id="tbl-border-right"></td>
</tr>
<!--<tr>
	<th class="sized bottomleft"></th>
	<td id="tbl-border-bottom">&nbsp;</td>
	<th class="sized bottomright"></th>
</tr>-->
</table>
<div class="clear">&nbsp;</div>
</div>
<!--  end content -->
<div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->
<div class="clear">&nbsp;</div> 
<!-- start footer -->         
<?php include("footer.php") ?>
<!-- end footer -->
<script src="javascripts/jquery-1.8.2.js" type="text/javascript"></script>
<script src="javascripts/jquery-ui.js"></script>
<script type="text/javascript" src="javascripts/validate.js"></script>
<script type="text/javascript">
// Popup window code
function newPopup(url) {
	popupWindow = window.open(
		url,'popUpWindow','height=500,width=600,left=200,top=100,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no,status=yes')
}
</script>
<script>
    $(function() {
        $( "#from" ).datepicker({
			dateFormat: "d M yy",
            defaultDate: "w",
            changeMonth: true,
            numberOfMonths: 1,
            onSelect: function( selectedDate ) {
                $( "#to" ).datepicker( "option", "minDate", selectedDate );
            }
        });
        $( "#to" ).datepicker({
			dateFormat: "d M yy",
            defaultDate: "-w",
            changeMonth: true,
            numberOfMonths: 1,
            onSelect: function( selectedDate ) {
                $( "#from" ).datepicker( "option", "maxDate", selectedDate );
            }
        });
    });
</script>
<!-- <script type="text/javascript">
	$(document).ready(function() {
		$('#retl').on('change', function(){
		var id = $(this).val();

		 $.ajax({url: "retailer_sales_report.php?id="+id, success: function(result){
        		//$("#div1").html(result);
    		}});
			
		})
	})

</script> -->

<script type="text/javascript">
	/*$(document).ready(function() {
	    $("#retl").change(function(){
		 	$('#myform').submit();
		});
});*/
</script>



</body>
</html>