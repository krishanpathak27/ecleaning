<?php
include("includes/config.inc.php");
include("includes/function.php");


$_objAdmin = new Admin();

if(isset($_REQUEST['show']) && $_REQUEST['show']=="yes"){
	$_objAdmin->showRetailerType();
	die;
}

/*********************************************** 
 |  Discription: Add retailer channel
 |  Date : 20 May 2015
 |	By   : Nizam 	  
************************************************/
if(isset($_POST['submit']) && $_POST['submit']=='Save')
 {
 	/* Chainel Already exits condtions */
 	if(isset($_POST['type_name']) && $_POST['type_name']!="")
 	 {
 		         $channel_exits=$_objAdmin->retailerTypeExits();
 		if($channel_exits==1){ $sus="Retailer Type name Already exits in the system.";}
 		else
 		{   /* Update Retailer Chainel */
 			if(isset($_POST['type_id']) && $_POST['type_id']!="")
 			 {
 			 	 $update=$_objAdmin->updateRetailerType($_POST['type_id']);
 			 	if($update>0){ $sus="Retailer type updated successfully.";}
 			 }
 			 else
 			 {
 			    $channel=$_objAdmin->addRetailerType();
     		if($channel>0){ $sus="Retailer Type successfully added";}
 	         }	
 			
 		}	

 	}
     
 }

/*********************************************** 
 |  Discription: Edit retailer channel
 |  Date : 20 May 2015
 |	By   : Nizam 	  
************************************************/
 if(isset($_REQUEST['action']) && $_REQUEST['action']=='edit')
       { 
         $channelEdit=$_objAdmin->retailerTypeEdit($_REQUEST['id']);
       	 /*echo '<pre>';
       	 print_r($channelEdit);*/
       	 
       } 



if(isset($_REQUEST['statusId']) && $_REQUEST['value']!="")
{
	if($_REQUEST['value']=="Active"){	
		$status='I';
	} else {
		$status='A';
	}
	$cid=$_objAdmin->_dbUpdate(array("status"=>$status),'table_retailer_type_master', " type_id='".$_REQUEST['statusId']."'");
	header("Location: retailer_type.php");
}	

include("header.inc.php");



?>
<!-- start content-outer -->
<div id="content-outer">
<!-- start content -->
<div id="content">
<?php if(isset($sus)){?>
	<!--  start message-green -->
	<div id="message-green">
		<table border="0" width="100%" cellpadding="0" cellspacing="0">
		<tr>
			<td class="green-left"><?php echo $sus; ?></td>
			<td class="green-right"><a class="close-green"><img src="images/icon_close_green.gif"   alt="" /></a></td>
		</tr>
		</table>
	</div>
	<?php } ?>
<div id="page-heading"><h1><span style="color: #d74343; font-family: Tahoma; font-weight: bold;"><?php if($_REQUEST['action']=='add'){ echo 'Add Retailer Type';} else if($_REQUEST['action']=='edit'){ echo 'Edit Retailer Type';}  else {?>Retailer Type List<?php } ?></span></h1></div>
<table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">
<tr>
<td>
	<!--  start content-table-inner -->
	<div id="content-table-inner">

	<table border="0" width="100%" cellpadding="0" cellspacing="0">
	
	<tr valign="top">
	<td>
		<!-- start id-form -->
		<?php  //echo $_REQUEST['action'];
		      switch ($_REQUEST['action']) {
			  case 'add':
				include('retailer_type/retailertype/action/add.php');
				break;
			  case 'edit':
				include('retailer_type/retailertype/action/add.php');
				break;
				case 'delete':
				include("salesman/edit_salesman.php");
				break;
			  case 'import':
				include("city/import.php");
				break;
			  case 'export':
				include("salesman/export_salesman.php");
				break;
			  /*---------------------------------------------
			     Login Form Case:
			   ----------------------------------------------*/
			 case 'index':
			 include("city/view.php");
			 break;		
			 		
			 default:
			 include('retailer_type/retailertype/action/index.php');
			 break;
				
		} ?>
		<!-- end id-form  -->
	</td>
	<td>
	<!-- right bar-->
	
	</td>
	</tr>
<tr>
<td><img src="images/blank.gif" width="695" height="1" alt="blank" /></td>
<td></td>
</tr>
</table>
<div class="clear"></div>
</div>
<!--  end content-table-inner  -->
</td>
<td id="tbl-border-right"></td>
</tr>
</table>
<div class="clear">&nbsp;</div>
</div>
<!--  end content -->
<div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->
<div class="clear">&nbsp;</div> 
<!-- start footer --> 
   
<?php include("footer.php");?>
<!-- end footer -->
</body>
</html>
