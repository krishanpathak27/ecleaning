<?php
include("includes/config.inc.php");
 include("includes/function.php");
$page_name="Retailer Wise Sales of Distributor";

$_objAdmin = new Admin();

$distributor=$_objAdmin->_getSelectList('table_distributors','distributor_name',''," distributor_id='".$_REQUEST['id']."'"); 
$distributorName=$distributor[0]->distributor_name;

if($_REQUEST['sal']!=''){
$distributor=$_objAdmin->_getSelectList('table_salesman','salesman_name',''," salesman_id='".$_REQUEST['sal']."'"); 
$salesmanName=$distributor[0]->salesman_name;
$SalId="AND o.salesman_id='".$_REQUEST['sal']."'";
} else {
$salesmanName="All Salesman";
$SalId="";

}

?>

<?php include("header.inc.php") ?>

<script type="text/javascript">

    function PrintElem(elem)
    {
        Popup($(elem).html());
    }

    function Popup(data) 
    {
		
        var mywindow = window.open('', 'Report');
		
        mywindow.document.write('<html><head><title>Dealer Wise Sales of Distributor Report</title>');
		//mywindow.document.write('<table><tr><td><b>City Name:</b> <?php echo $sal_name; ?></td><td><b>Market Name:</b> <?php echo $_SESSION['MarketList']; ?></td><td><b>From Date:</b> <?php echo $_SESSION['FromRetList']; ?></td><td><b>To Date:</b> <?php echo $_SESSION['ToRetList']; ?></td></tr></table>');
        /*optional stylesheet*/ //mywindow.document.write('<link rel="stylesheet" href="main.css" type="text/css" />');
        mywindow.document.write('</head><body >');
        mywindow.document.write(data);
        mywindow.document.write('</body></html>');

        mywindow.print();
        mywindow.close();
        return true;
    }
	
	
</script>

<script src="javascripts/dateNextPrev.js" type="text/javascript"></script>
<!-- start content-outer -->
<input name="pagename" type="hidden"  id="pagename" value="admin_order_list.php" />
<div id="content-outer">
<!-- start content -->
<div id="content">
<div id="page-heading"><h1><span style="color: #d74343; font-family: Tahoma; font-weight: bold;">Dealer Wise Sales of Distributor</span></h1></div>
<table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">

<tr>
	
	<td>
	<!--  start content-table-inner -->
	<div id="content-table-inner">
	<div style="width: 100%;" align="right">
				
		</div>
	<table border="0" width="100%" cellpadding="0" cellspacing="0" id="report_export" name="report_export">	
		<td>
		<div style="width:900px;overflow:auto; height:auto;" >
		<div id="Report">
		<table border="0" cellpadding="0" cellspacing="0"  >
			<tr >
				<th valign="top" style="padding:5px;"  align="left">Distributor Name:</th>
				<td align="left" style="padding:5px;" colspan="3"><?php echo $distributorName; ?></td>
			</tr>
			<tr>
				<th valign="top" style="padding:5px;"  align="left">Salesman Name:</th>
				<td align="left" style="padding:5px;" colspan="3"><?php echo $salesmanName; ?></td>
			</tr>
			<tr>
				<th valign="top" style="padding:5px;"  align="left">From Date:</th>
				<td valign="center" style="padding:5px;"><?php echo $_REQUEST['from']; ?></td>
				<th valign="top" style="padding:5px;"  align="left">To Date:</th>
				<td valign="center" style="padding:5px;"><?php echo $_REQUEST['to']; ?></td>
			</tr>
			<tr>
				
			</tr>
		</table>
		<table  border="0" cellpadding="0" cellspacing="0" id="lists">
			<?php
			$NetOrder=$_objAdmin->_getSelectList('table_order as o left join table_retailer as r on r.retailer_id=o.retailer_id',"sum(o.acc_total_invoice_amount) as net_amount, r.retailer_name, r.retailer_phone_no,r.retailer_location",'',"  o.distributor_id='".$_REQUEST['id']."' and (o.date_of_order BETWEEN '".date('Y-m-d', strtotime($_REQUEST['from']))."' AND '".date('Y-m-d', strtotime($_REQUEST['to']))."') AND o.order_type!='No' AND r.new='' AND o.acc_total_invoice_amount!='0.00' $SalId ");
			
			
			$orderList=$_objAdmin->_getSelectList('table_order as o left join table_retailer as r on r.retailer_id=o.retailer_id',"sum(o.acc_total_invoice_amount) as total_amount, r.retailer_name, r.retailer_phone_no,r.retailer_location",'',"  o.distributor_id='".$_REQUEST['id']."' and (o.date_of_order BETWEEN '".date('Y-m-d', strtotime($_REQUEST['from']))."' AND '".date('Y-m-d', strtotime($_REQUEST['to']))."') AND o.order_type!='No' AND r.new='' AND o.acc_total_invoice_amount!='0.00' $SalId GROUP BY o.retailer_id ORDER BY total_amount desc");
			if(is_array($orderList)){
			$net_amount=array();
			?>
			<tr  bgcolor="#6E6E6E" style="color: #fff;font-weight: bold;" id="candidateNameTD">
				<td style="padding:10px;">SNO.</td>
				<!--<td style="padding:10px;">Retailer Name</td>
				<td style="padding:10px;">Retailer Phone No.</td>
				<td style="padding:10px;">Retailer Market</td>-->
				<td style="padding:10px;">Total Amount</td>
				<td style="padding:10px;">% Share</td>
			</tr>
				<?php
				for($i=0;$i<count($orderList);$i++)
				{
				?>
				<tr class="maintr"  style="border-bottom:2px solid #6E6E6E;">
					<td style="padding:10px;"><?php echo $i+1 ?></td>
					<!--<td style="padding:10px;"><?php echo $orderList[$i]->retailer_name; ?></td>
					<td style="padding:10px;"><?php echo $orderList[$i]->retailer_phone_no; ?></td>
					<td style="padding:10px;"><?php echo $orderList[$i]->retailer_location; ?></td>-->
					<td style="padding:10px;" align="right"><?php echo $orderList[$i]->total_amount; $net_amount[]=$orderList[$i]->total_amount;?></td>
					<td style="padding:10px;" align="right"><?php echo  round($orderList[$i]->total_amount/$NetOrder[0]->net_amount*100,2) ."%";?></td>
				</tr>
				<?php } ?>
				<tr  bgcolor="#6E6E6E" style="color: #fff;font-weight: bold;" >
					<td style="padding:10px;" align="right" colspan="1">Total</td>
					<td style="padding:10px;" align="right" ><?php echo array_sum($net_amount);?></td>
					<td style="padding:10px;" align="right" ></td>
				</tr>
		</table>
		</div>
		<?php } else {?>
		<table border="0" width="100%" cellpadding="0" cellspacing="0">
			<tr  style="border-bottom:2px solid #6E6E6E;border-top:2px solid #6E6E6E;" align="center">
				<td style="padding:10px;" colspan="5">Report Not Available</td>
			</tr>
		</table>
		<?php } ?>
		</div>
		<table border="0" width="25%" cellpadding="0" cellspacing="0">
		<tr  >
			<td style="padding:10px;"><div style="width: 2px;" align="left"><input type="button" value="Close" class="form-cen" onclick="javascript:window.close();" /></div></td>
			<td ><div style="width: 20px;" align="center"><input type="button" value="Print" class="result-submit" onclick="PrintElem('#Report')" /></div></td>
			<td ><div style="width: 20px;" align="center"><a id="dlink"  style="display:none;"></a>
		<input input type="button" value="Export to Excel" class="result-submit"  onclick="tableToExcel('report_export', 'Retailer Wise Sales of Distributor Report', 'Retailer Wise Sales of Distributor Report.xls')"></div></td>
		</tr>
		</table>
		
		</td>
	
	</tr>
	<tr>
		<td><img src="images/blank.gif" width="695" height="1" alt="blank" /></td>
		<td></td>
	</tr>
	</table>
<div class="clear"></div>
</div>
<!--  end content-table-inner  -->
</td>
<td id="tbl-border-right"></td>
</tr>

</table>
<div class="clear">&nbsp;</div>
</div>
<!--  end content -->
<div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->
<div class="clear">&nbsp;</div> 
<!-- start footer -->         
<?php include("footer.php") ?>
<?php unset($_SESSION['SalAttList']); ?>
<!-- end footer -->
<script src="javascripts/jquery-1.8.2.js" type="text/javascript"></script>
<script src="javascripts/jquery-ui.js"></script>
<script type="text/javascript" src="javascripts/validate.js"></script>
<script type="text/javascript">
// Popup window code
function newPopup(url) {
	popupWindow = window.open(
		url,'popUpWindow','height=500,width=600,left=200,top=100,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no,status=yes')
}
</script>
<script>
    $(function() {
        $( "#from" ).datepicker({
			dateFormat: "d M yy",
            defaultDate: "w",
            changeMonth: true,
			changeYear: true,
			yearRange: '2010:2020',
            numberOfMonths: 1,
            onSelect: function( selectedDate ) {
                $( "#to" ).datepicker( "option", "minDate", selectedDate );
            }
        });
        $( "#to" ).datepicker({
			dateFormat: "d M yy",
            defaultDate: "-w",
            changeMonth: true,
			changeYear: true,
			yearRange: '2010:2020',
            numberOfMonths: 1,
            onSelect: function( selectedDate ) {
                $( "#from" ).datepicker( "option", "maxDate", selectedDate );
            }
        });
    });
</script>
<script type='text/javascript'>//<![CDATA[ 
var tableToExcel = (function () {
        var uri = 'data:application/vnd.ms-excel;base64,'
        , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>'
        , base64 = function (s) { return window.btoa(unescape(encodeURIComponent(s))) }
        , format = function (s, c) { return s.replace(/{(\w+)}/g, function (m, p) { return c[p]; }) }
        return function (table, name, filename) {
            if (!table.nodeType) table = document.getElementById(table)
            var ctx = { worksheet: name || 'Worksheet', table: table.innerHTML }

            document.getElementById("dlink").href = uri + base64(format(template, ctx));
            document.getElementById("dlink").download = filename;
            document.getElementById("dlink").click();

        }
    })()

//]]>  
</script>
</body>
</html>