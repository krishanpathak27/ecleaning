<?php
include("includes/config.inc.php");
include("includes/function.php");
$_objAdmin = new Admin();

if(isset($_POST['add']) && $_POST['add']=='yes'){
		//echo $_POST['location'];
		$_objAdmin->UpdateLocation();
		$sus="Retailer Location has been Update successfully.";

}

if($_SESSION['RetMap']!=""){
$auRec=$_objAdmin->_getSelectList('table_retailer',"*",''," retailer_id=".$_SESSION['RetMap']);
}

//echo $_SESSION['RetMap'];
?>

<?php include("header.inc.php") ?>
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
<script type="text/javascript">
function initialize() 
{
	$('#map_canvas').show();
    var myOptions = 
	{  center: new google.maps.LatLng(<?php echo $auRec[0]->lat.",".$auRec[0]->lng; ?>), zoom: 15, mapTypeId: google.maps.MapTypeId.ROADMAP };
	var map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);

				var myLatlng = new google.maps.LatLng(<?php echo $auRec[0]->lat.",".$auRec[0]->lng; ?>)
				var marker = new google.maps.Marker
				({ position: myLatlng,title:"<?php echo "Accuracy:" .$auRec[0]->lat_lng_capcure_accuracy ?>"});//, animation: google.maps.Animation.DROP});
				marker.setMap(map);
	
}
</script>
<script type="text/javascript">
// Popup window code
function newPopup(url) {
	popupWindow = window.open(
		url,'popUpWindow','height=500,width=670,left=200,top=100,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no,status=yes')
}
</script>
<body onLoad="initialize();">
<!-- start content-outer -->
<div id="content-outer">
<!-- start content -->

<div id="content">
<?php if($sus!=''){?>
	<!--  start message-green -->
	<div id="message-green">
		<table border="0" width="100%" cellpadding="0" cellspacing="0">
		<tr>
			<td class="green-left"><?php echo $sus; ?></td>
			<td class="green-right"><a class="close-green"><img src="images/icon_close_green.gif"   alt="" /></a></td>
		</tr>
		</table>
	</div>
	<?php } ?>
<div id="page-heading"><h1><span style="color: #d74343; font-family: Tahoma; font-weight: bold;">Dealer</span></h1></div>
<table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">
<!--<tr>
	<th rowspan="3" class="sized"><img src="images/side_shadowleft.jpg" width="20" height="300" alt="" /></th>
	<th class="topleft"></th>
	<td id="tbl-border-top">&nbsp;</td>
	<th class="topright"></th>
	<th rowspan="3" class="sized"><img src="images/side_shadowright.jpg" width="20" height="300" alt="" /></th>
</tr>
-->
<tr>
	<!--<td id="tbl-border-left"></td>-->
	<td>
	<!--  start content-table-inner -->
	<div id="content-table-inner">
	
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
	<tr valign="top">
	<td>
		<!-- start id-form -->
		<table border="0" cellpadding="0" cellspacing="0"  >
			<tr>
				<th valign="top" style="width: 150px;line-height: 28px;"  align="left">Dealer name:</th>
				<td align="left" style="min-width: 150px;"><?php echo $auRec[0]->retailer_name; ?></td>
				<td rowspan="5" align="right" valign="top"><div id="map_canvas"  style="display:none; border:1px solid; width:800px;  height: 300px;"></div></td>
			</tr>
			<tr>
				<th valign="top" style="width: 150px;line-height: 28px;"  align="left">Dealer Address:</th>
				<td align="left"><?php echo $auRec[0]->retailer_address; ?></td>
			</tr>
			<tr>
				<th valign="top" style="width: 150px;line-height: 28px;"  align="left">Dealer Market:</th>
				<td align="left"><?php echo $auRec[0]->retailer_location; ?></td>
			</tr>
			<tr>
				<th valign="top" style="width: 150px;line-height: 28px;"  align="left">Reading Accuracy(m):</th>
				<td align="left"><?php echo $auRec[0]->lat_lng_capcure_accuracy; ?></td>
			</tr>
			<tr>
				<th valign="top" style="width: 150px;line-height: 28px;"  align="left">Mode:</th>
				<td align="left" valign="top"><?php echo strtoupper($auRec[0]->capcure_mode); ?></td>
			</tr>
			<tr>
				<td colspan="2" >
					
				</td>
			</tr>
		</table>
		<form name="frmPre" id="frmPre" method="post" action="retailermap.php" enctype="multipart/form-data" >
		<table border="0" width="100%" cellpadding="0" cellspacing="0" id="id-form">
			<tr bgcolor="#6E6E6E" style="color: #fff;font-weight: bold;">
				<td style="padding:10px;">Taken By</td>
				<td style="padding:10px;">Accuracy(m)</td>
				<td style="padding:10px;">Date</td>
				<td style="padding:10px;">Mode</td>
				<td style="padding:10px;" align="center">Set as Master</td>
				<td style="padding:10px;">Map</td>
			</tr>
			<?php
			$auRet=$_objAdmin->_getSelectList2('table_retailer_location as r left join table_salesman as s on s.salesman_id=r.salesman_id',"r.*,s.salesman_name",''," retailer_id=".$_SESSION['RetMap']."");
			if(is_array($auRet)){
				for($i=0;$i<count($auRet);$i++){
			?>
			<tr style="border-bottom:2px solid #6E6E6E;" >
				<td style="padding:10px;"><?php echo $auRet[$i]->salesman_name;?></td>
				<td style="padding:10px;"><?php echo $auRet[$i]->lat_lng_capcure_accuracy;?></td>
				<td style="padding:10px;"><?php echo $_objAdmin->_changeDate($auRet[$i]->lat_lng_capcure_date);?></td>
				<td style="padding:10px;"><?php echo strtoupper($auRet[$i]->capcure_mode);?></td>
				<td style="padding:10px;" align="center"><input type="radio" name="location_id" class="required" value="<?php echo $auRet[$i]->location_id;?>"></td>
				<td style="padding:10px;"><a href="JavaScript:newPopup('map.php?id=<?php echo base64_encode($auRet[$i]->location_id);?>');"><img src="images/google_map.png" ></img></a>
				</td>
			</tr>
				<?php } ?>
			<tr>
				<td colspan="3" align="right" style="padding:10px;">
				<input type="button" value="Back" class="form-cen" onClick="javascript:window.close();" />
				</td>
				<td colspan="3" align="left" style="padding:10px;">
				<input name="add" type="hidden" value="yes" />
				<input name="submit" class="form-submit" type="submit" id="submit" value="Save" />
				</td>
			</tr>
			
			<?php } else { ?>
			<tr style="border-bottom:2px solid #6E6E6E;" >
				<td colspan="6" align="center" style="padding:10px;"><span style="color: #A52A2A; font-family: Tahoma; font-weight: bold;">No Data Available</span></td>
			</tr>
			<tr  >
				<td colspan="6" align="center">
				<input type="button" value="Back" class="form-cen" onClick="javascript:window.close();" />
				</td>
			</tr>
			<?php } ?>
		</form>
		</table>
	<!-- end id-form  -->

	</td>
	<td>

	<!-- right bar-->
	<?php //include("rightbar/category_bar.php") ?>
</td>
</tr>
<tr>
<td><img src="images/blank.gif" width="695" height="1" alt="blank" /></td>
<td></td>
</tr>
</table>
<div class="clear"></div>
</div>
<!--  end content-table-inner  -->
</td>
<td id="tbl-border-right"></td>
</tr>
<!--<tr>
	<th class="sized bottomleft"></th>
	<td id="tbl-border-bottom">&nbsp;</td>
	<th class="sized bottomright"></th>
</tr>-->
</table>
<div class="clear">&nbsp;</div>
</div>
<!--  end content -->
<div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->
<div class="clear">&nbsp;</div> 
<!-- start footer -->         
<?php include("footer.php") ?>
<!-- end footer -->
 
</body>
</html>