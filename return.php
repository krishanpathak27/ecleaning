<?php
include("includes/config.inc.php");
include("includes/function.php");
include("includes/globalarraylist.php");



function checkUserType($sessionValueUserType) {

	$type = "";

	if(isset($sessionValueUserType) && !empty($sessionValueUserType)){

		switch ($sessionValueUserType) {

			case 3 : 

				$type = "distributor/";

			break;

			case 4 :

				$type = "retailer/";

			break;

			case 5 :

				$type = "salesman/";

			break;

		}


	}

	return $type;

}



$page_name="Primary Returns";
$checkPromotionStatus = true;

if(isset($_REQUEST['show']) && $_REQUEST['show']=="yes"){

	$_objAdmin->showReturns();
	die;
}

//Delete Salesman
if(isset($_REQUEST['stid']) && isset($_REQUEST['value']) && $_REQUEST['stid']!=""){
	
		if($_REQUEST['value'] == 'Active')
		$id=$_objAdmin->_dbUpdate(array("last_updated_on"=>date('Y-m-d H:i:s'),"status"=>'I'),'table_promotion', " promotion_id='".$_REQUEST['stid']."'");
		else 
		$id=$_objAdmin->_dbUpdate(array("last_updated_on"=>date('Y-m-d H:i:s'),"status"=>'A'),'table_promotion', " promotion_id='".$_REQUEST['stid']."'");

		$sus="Status updated successfully";
		header ("location: return.php");
}


   /*  Description : On submit value sales return added. */

     if(isset($_POST['submit']) && $_POST['submit']=='Submit')
     {
     	$return=$_objAdmin->salesReturnAdd();
     	if(!empty($return)){$return ="Sales Returns successfully added"; }

     }
	
include("header.inc.php");
?>
    <!-- Bootstrap core CSS -->
    <link href="bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Bootstrap theme -->
    <link href="bootstrap/dist/css/bootstrap-theme.min.css" rel="stylesheet">

    


    <!-- Bootstrap core JavaScript
    ================================================== -->
   <?php if($_REQUEST['action']!='') {?> 
   <script src="javascripts/jquery-1.11.2.min.js"></script>
   <?php } ?>



 
<input name="pagename" type="hidden"  id="pagename" value="return.php" />
<div id="content-outer">
<!-- start content -->
<div id="content">
<div id="page-heading"><h1><span style="color: #d74343; font-family: Tahoma; font-weight: bold;"><?php if($_REQUEST['action']=='add'){ ?>Primary Return</span><?php } else {?>Primary Returns<?php } ?></h1></div>
<?php if($_REQUEST['err']!=''){?>
	<div id="message-red">
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
	<tr>
		<td class="red-left">Error. <?php echo "You have exceeded the maximum limit of active users"; ?></td>
		<td class="red-right"><a class="close-red"><img src="images/icon_close_red.gif"   alt="" /></a></td>
	</tr>
	</table>
	</div>
<?php } ?>
<?php if($return!=''){?>
	<div class="alert alert-success" role="alert">
        <strong><?php echo $return; ?></strong> 
      </div>
<?php } ?>
<table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">
<tr>
	<td>
	<!--  start content-table-inner -->
	<div id="content-table-inner">
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
	<tr valign="top">
	<td>
		<!-- start id-form -->
		<?php
		if(basename($_SERVER['PHP_SELF']) == "return.php") { 
			$userType = checkUserType($_SESSION['userLoginType']);
			if(isset($_REQUEST['action'])) {

				

				//echo $userType = checkUserType($_SESSION['userLoginType']);


				//echo "return/".$userType."action/add.php";
				//echo $_REQUEST['action'];

				switch ($_REQUEST['action']) {
					case add : 
						include("return/".$userType."action/add.php");
					break;

					case edit :
						include("return/".$userType."action/index.php");
					break;

					case grn :
						include("return/".$userType."action/grn.php");
					break;

					case 'export':
						include("return/".$userType."action/index.php");
					break;
					case 'delete':
						include("return/".$userType."action/index.php");
					break;

					default :
					
					include("return/".$userType."action/index.php");
					break;
				} 
			} else {
					  
				include("return/".$userType."action/index.php");
			}
			
		} ?>
		<!-- end id-form  -->
	</td>
	<td>
	<!-- right bar-->
	<?php //include("return/rightbar.php") ?>
	</td>
	</tr>
<tr>
</tr>
</table>
<div class="clear"></div>
</div>
<!--  end content-table-inner  -->
</td>
<td id="tbl-border-right"></td>
</tr>
</table>
<div class="clear">&nbsp;</div>
</div>
<!--  end content -->
<div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->
<div class="clear">&nbsp;</div> 
<!-- start footer -->         
<?php include("footer.php");?>
<!-- end footer -->
<!--<script src="javascripts/jquery-1.8.2.js" type="text/javascript"></script>
<script src="javascripts/jquery-ui.js"></script>
<script type="text/javascript" src="javascripts/validate.js"></script>-->
</body>
</html>