<?php
//echo date('H:m:s');
?>


<style>
.frmSearch {border: 1px solid #F0F0F0;background-color:#C8EEFD;margin: 2px 0px;padding:40px;}
#retailer-list{float:left;list-style:none;margin:0;padding:0;width:100%; height: 200px; overflow-y: scroll;}
#retailer-list li{padding: 10px; background:#FAFAFA;border-bottom:#F0F0F0 1px solid;}
#retailer-list li:hover{background:#F0F0F0;}
#search-box{padding: 10px;border: #F0F0F0 1px solid;}
</style>

<!--<script type="text/javascript" src="javascripts/jquery-2.1.1.min.js"></script>
<script type="text/javascript" src="javascripts/jquery-ui-v1.11.2.min.js"></script>-->
<script src="javascripts/jquery-ui-1.11.4.js"></script>

<script src="javascripts/return_custom_jquery.js"></script>

  <body role="document">
    <div class="container theme-showcase" role="main">

     <!-- <div class="page-header">
        <h1>Panels</h1>
      </div>-->
      <div class="row">
        <div class="col-sm">
          <div class="panel panel-default">
            <div class="panel-heading">
              <h3 class="panel-title">Distributor Return</h3>
            </div>

            

            <div class="panel-body">
             
              <form class="bs-example bs-example-form" method="post" id="orderForm" action="" enctype="multipart/form-data" data-example-id="simple-input-groups">
               
              <table id="order" class="table table-bordered ">
              <tbody>
              <tr>
                  <th><input class="check_all" type="checkbox" onclick="select_all()"></th>
                  <th>S. No</th>
                  <th>Item Code</th>
                  <th>Item Name</th>
                  <th>Cases Sizes</th>
              </tr>

               <tr>
                  <td><input type="checkbox" class="case"></td>
                  <td><span id="snum">1</span></td>
                  <td><input class="form-control autocomplete_txt ui-autocomplete-input item_list required" type="text" data-type="itemcode" id="itemcode_1" name="itemcode[]" autocomplete="off"><input class="form-control autocomplete_txt lastsaveditemids" type="hidden" data-type="item_id" id="item_id_1" name="item_id[]"></td>
                  <td><input class="form-control autocomplete_txt" type="text" data-type="item_name" readonly id="item_name_1" name="item_name[]"></td>
                  <td id="qty_1" width="33%"></td>
                </tr>

              </tbody>
              </table>

              <button type="button" class="btn btn-danger deletes">Delete</button>
            <button type="button" class="btn btn-success addmore">Add More</button>
             <input type="submit" name="submit" value="Submit" class="btn btn-default" id="button1">
           <input type="button" name="reset" value="Back" class="btn btn-default" onclick="location.href='return.php'">
          </div>
        </form>
        </div>
          </div>
          </div>
        </div><!-- /.col-sm-4 -->
        </div><!-- /.col-sm-4 -->
       
        </div><!-- /.col-sm-4 -->
      </div>

    </div> <!-- /container -->
