<!--  start related-activities -->
	<div id="related-activities">
		
		<!--  start related-act-top -->
		<div id="related-act-top">
		Related Actions
		</div>
		<!-- end related-act-top -->
		
		<!--  start related-act-bottom -->
		<div id="related-act-bottom">
		
			<!--  start related-act-inner -->
			<div id="related-act-inner">
			
				<div class="left"><a href=""><img src="images/icon_edit.gif" width="21" height="21" alt="" /></a></div>
				<div class="right">
					<h5>Import Item</h5>
					Your import file to ensure you have the file perfect for the import.
					<ul class="greyarrow">
						<li><a href="import_item.php">Click here to import</a></li>
						<!--<li><a href="#">Click here to import</a></li>--> 
					</ul>
				</div>
				
				<div class="clear"></div>
				<div class="lines-dotted-short"></div>
				
				<div class="left"><a href=""><img src="images/icon_edit.gif" width="21" height="21" alt="" /></a></div>
				<div class="right">
					<h5>Download Item</h5>
					Download item list
					<ul class="greyarrow">
						<!--<li><a href="export.inc.php?export_category">Click here to export</a></li>-->
						<li><a href="export.inc.php?export_item">Click here to download</a></li> 
					</ul>
				</div>
				
				<div class="clear"></div>
				<div class="lines-dotted-short"></div>
				
				<div class="left"><a href=""><img src="images/icon_edit.gif" width="21" height="21" alt="" /></a></div>
				<div class="right">
					<h5>Download Cases</h5>
					Download Cases Size List
					<ul class="greyarrow">
						<li><a href="export.inc.php?export_cases">Click here to download</a></li>
						<!--<li><a href="#">Click here to download</a></li>-->  
					</ul>
				</div>
				
				<div class="clear"></div>
				<div class="lines-dotted-short"></div>
				
				<div class="left"><a href=""><img src="images/icon_edit.gif" width="21" height="21" alt="" /></a></div>
				<div class="right">
					<h5>Download Category</h5>
					Download Category List
					<ul class="greyarrow">
						<li><a href="export.inc.php?export_category">Click here to download</a></li> 
					</ul>
				</div>
				
				<div class="clear"></div>
				<div class="lines-dotted-short"></div>
				
			
				<div class="clear"></div>
				
			</div>
			<!-- end related-act-inner -->
			<div class="clear"></div>
		
		</div>
		<!-- end related-act-bottom -->
	
	</div>
	<!-- end related-activities -->