<!--  start related-activities -->
	<div id="related-activities">
		
		<!--  start related-act-top -->
		<div id="related-act-top">
		Related Actions
		</div>
		<!-- end related-act-top -->
		
		<!--  start related-act-bottom -->
		<div id="related-act-bottom">
		
			<!--  start related-act-inner -->
			<div id="related-act-inner">
			
				<div class="left"><a href=""><img src="images/icon_edit.gif" width="21" height="21" alt="" /></a></div>
				<div class="right">
					<h5>Edit Personal Information</h5>
					Edit your personal details, contact information.
					<ul class="greyarrow">
						<li><a href="profile_setting.php?profile=<?php echo base64_encode(edit);?>">Click here to visit</a></li> 
					</ul>
				</div>
				
				<div class="clear"></div>
				<div class="lines-dotted-short"></div>
				
				<div class="left"><a href=""><img src="images/icon_edit.gif" width="21" height="21" alt="" /></a></div>
				<div class="right">
					<h5>Edit Email Address</h5>
					Edit your personal Email address.
					<ul class="greyarrow">
						<li><a href="profile_setting.php?profile=<?php echo base64_encode(email);?>">Click here to visit</a></li> 
					</ul>
				</div>
				
				<div class="clear"></div>
				<div class="lines-dotted-short"></div>
				
				<div class="left"><a href=""><img src="images/icon_edit.gif" width="21" height="21" alt="" /></a></div>
				<div class="right">
					<h5>Change Password</h5>
					Change your account password. 
					<ul class="greyarrow">
						<li><a href="profile_setting.php?profile=<?php echo base64_encode(password);?>">Click here to visit</a></li> 
					</ul>
				</div>
				<div class="clear"></div>
				
			</div>
			<!-- end related-act-inner -->
			<div class="clear"></div>
		
		</div>
		<!-- end related-act-bottom -->
	
	</div>
	<!-- end related-activities -->