<!--  start related-activities -->
	<div id="related-activities">
		
		<!--  start related-act-top -->
		<div id="related-act-top">
		Related Actions
		</div>
		<!-- end related-act-top -->
		
		<!--  start related-act-bottom -->
		<div id="related-act-bottom">
		
			<!--  start related-act-inner -->
			<div id="related-act-inner">
			
				<div class="left"><a href=""><img src="images/icon_edit.gif" width="21" height="21" alt="" /></a></div>
				<div class="right">
					<h5>Import Warehouse</h5>
					Your import file to ensure you have the file perfect for the import.
					<ul class="greyarrow">
						<li><a href="warehouses.php?import">Click here to visit</a></li>
					</ul>
				</div>
				
				<div class="clear"></div>
				<div class="lines-dotted-short"></div>
				
				<div class="left"><a href=""><img src="images/icon_edit.gif" width="21" height="21" alt="" /></a></div>
				<div class="right">
					<h5>Export Warehouse List</h5>
					Show list order by Warehouse.
					<ul class="greyarrow">
						<!--<li><a href="export.inc.php?export_distributors_list">Click here to visit</a></li>--> 
						<li><a href="export.inc.php?export_warehouse_list">Click here to visit</a></li> 
					</ul>
				</div>
				
				<div class="clear"></div>
				<!--<div class="lines-dotted-short"></div>
				
				<div class="left"><a href=""><img src="images/icon_edit.gif" width="21" height="21" alt="" /></a></div>
				<div class="right">
					<h5>Export State List</h5>
					Show list order by State.
					<ul class="greyarrow">
						<li><a href="export.inc.php?export_state">Click here to visit</a></li> 
					</ul>
				</div>-->
				
				<div class="clear"></div>
				<div class="lines-dotted-short"></div>
				
			
				<div class="clear"></div>
				
			</div>
			<!-- end related-act-inner -->
			<div class="clear"></div>
		
		</div>
		<!-- end related-act-bottom -->
	
	</div>
	<!-- end related-activities -->