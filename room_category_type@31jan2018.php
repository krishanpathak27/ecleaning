<?php
include("includes/config.inc.php");
include("includes/function.php");
$page_name = "WhiteSpot Room Category Type";
$_objAdmin = new Admin();

if (isset($_REQUEST['stid']) && $_REQUEST['value'] != "") {
    if ($_REQUEST['value'] == 1) {
        $status = 'I';
    } else {
        $status = 'A';
    }
    $cid = $_objAdmin->_dbUpdate(array("status" => $status, "last_update_date" => date('Y-m-d H:i:s')), 'table_category_type', "room_category_id in(" . $_REQUEST['stid'] . ")");
    $_SESSION['order'] = 'updated';
    header("Location: room_category_type.php?suc=Status Changes Successfully");
}

if (isset($_REQUEST['delete']) && $_REQUEST['delete'] == 'yes' && $_REQUEST['stid'] != "") {
    $id = $_objAdmin->_dbUpdate(array("last_update_date" => date('Y-m-d H:i:s'), "status" => 'D'), 'table_category_type', " room_category_id in(" . $_REQUEST['stid'] . ")");
    $_SESSION['order'] = 'updated';
    header("Location:room_category_type.php?suc=Deleted Successfully");
}
?>

<?php include("header.inc.php") ?>

<div class="m-grid__item m-grid__item--fluid m-wrapper" style="background-color:">
    <!-- BEGIN: Subheader -->
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="#" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__separator">
                        -
                    </li>
                    <li class="m-nav__item">
                        <a href="#" class="m-nav__link">
                            <span class="m-nav__link-text">
                                Master
                            </span>
                        </a>
                    </li>
                    <li class="m-nav__separator">
                        -
                    </li>
                    <li class="m-nav__item">
                        <a href="" class="m-nav__link">
                            <span class="m-nav__link-text">
                                Room Category Type
                            </span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <!-- END: Subheader -->
    <div class="m-content">
        <?php if (isset($_REQUEST['suc']) && $_REQUEST['suc'] != "") { ?>
            <div role="alert" style="background: #d7fbdc;" class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-dismissible m--margin-bottom-30">
                <div class="m-alert__icon" >
                    <i class="flaticon-exclamation m--font-brand"></i>
                </div>
                <div class="m-alert__text">
    <?php echo $_REQUEST['suc']; ?> 
                </div>
            </div>
<?php } ?>
        <div class="m-portlet m-portlet--mobile">

            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            Room Category Type List
                        </h3>
                    </div>
                </div>
            </div>
            <div class="m-portlet__body">
                <!--begin: Search Form -->
                <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
                    <div class="row align-items-center">
                        <div class="col-xl-8 order-2 order-xl-1">
                            <div class="form-group m-form__group row align-items-center">
                                <div class="col-md-4">
                                    <div class="m-input-icon m-input-icon--left">
                                        <input type="text" class="form-control m-input" placeholder="Search..." id="generalSearch">
                                        <span class="m-input-icon__icon m-input-icon__icon--left">
                                            <span>
                                                <i class="la la-search"></i>
                                            </span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-4 order-1 order-xl-2 m--align-right add">
                            <a href="addCategoryType.php" class="btn btn-warning m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                                <span>
                                    <i class="la la-plus"></i>
                                    <span>
                                        Add Room Category Type
                                    </span>
                                </span>
                            </a>
                            <div class="m-separator m-separator--dashed d-xl-none "></div>
                        </div>
                    </div>
                </div>
                <!--end: Search Form -->
                    <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30 collapse" id="m_datatable_group_action_form">
                    <div class="row align-items-center">
                        <div class="col-xl-12">
                            <div class="m-form__group m-form__group--inline">
                                <div class="m-form__label m-form__label-no-wrap">
                                    <label class="m--font-bold m--font-danger-">
                                        Selected
                                        <span id="m_datatable_selected_number"></span>
                                        records:
                                    </label>
                                </div>
                                <div class="m-form__control">
                                    <div class="btn-toolbar">
                                        <div class="dropdown">
                                            <button type="button" class="btn btn-accent btn-sm dropdown-toggle" data-toggle="dropdown">
                                                Update status
                                            </button>
                                            <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                                                <a class="dropdown-item changeStatusBnt" href="#" id="activeBnt">
                                                    Active
                                                </a>
                                                <a class="dropdown-item changeStatusBnt" href="#" id="inactiveBnt">
                                                    Inactive
                                                </a>
                                            </div>
                                        </div>
                                        &nbsp;&nbsp;&nbsp;
                                        <a  class="btn btn-sm btn-danger"  href="#"  id="m_datatable_delete_all">
                                            Delete All
                                        </a></button>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--begin: Datatable -->
                <div class="catagoryType" id="catagoryType"></div>
                <!--end: Datatable -->
            </div>
        </div>
    </div>
</div>
</div>
</div>
<!-- start footer -->         
<div class="modal fade" id="m_modal_2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">
                    Ready to Delete?
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">
                        &times;
                    </span>
                </button>
            </div>
            <div class="modal-body">
                <p>
                    Are you sure,You want to Delete.
                </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">
                    Close
                </button>
                <a id="deteleA" href="" class="btn btn-primary">
                    Delete
                </a>
            </div>
        </div>
    </div>
</div>
<?php include("footer.php") ?>
<script type="text/javascript" >
    var DefaultDatatableDemo = function () {
        //== Private functions

        // basic demo
        var options = {
                data: {
                    type: 'remote',
                    source: {
                        read: {
                            url: '<?php echo $ajaxUrl . '?pageType=catagoryType' ?>'
                        }
                    },
                    pageSize: 10,
                    serverPaging: true,
                    serverFiltering: true,
                    serverSorting: true
                },
                layout: {
                    theme: 'default',
                    class: '',
                    scroll: true,
                    footer: false
                },
                sortable: true,
                filterable: false,
                pagination: true,
                search: {
                    input: $('#generalSearch')
                },
                columns: [ {
                    field: "room_category_id",
                    title: "#",
                    sortable: false,
                    width: 40,
                    selector: {class: 'm-checkbox--solid m-checkbox--brand'}
                },{
                        field: "room_category",
                        title: "Room Category",
                        sortable: 'asc'
                    }, {
                        field: "unit_name",
                        title: "Unit Name",
                        responsive: {visible: 'lg'}
                    }, {
                        field: "cleaning_type",
                        title: "Cleaning Type",
                        responsive: {visible: 'lg'}
                    }, {
                        field: "minimum",
                        title: "Minimun",
                        responsive: {visible: 'lg'}
                    }, {
                        field: "maximum",
                        title: "Maximum",
                        responsive: {visible: 'lg'}
                    }, {
                        field: "Status",
                        title: "Status",
                        width: 100,
                        locked: {right: 'xl'},
                        template: function (row) {
                            var status = {
                                1: {'title': 'Active', 'class': 'm-badge--brand'},
                                2: {'title': 'Inactive', 'class': 'm-badge--warning'}
                            };
                            return '<span class="m-badge ' + status[row.Status].class + ' m-badge--wide">' + status[row.Status].title + '</span>';

                        }
                    }, {
                        field: "Actions",
                        width: 110,
                        title: "Actions",
                        sortable: false,
                        locked: {right: 'xl'},
                        overflow: 'visible',
                        template: function (row, index, datatable) {
                            var dropup = (datatable.getPageSize() - index) <= 4 ? 'dropup' : '';
                            var botton = '\
                                                <a href="room_category_type.php?stid=' + row.room_category_id + '&value=' + row.Status + '" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Change Status">\
                                                        <i class="la la-i-cursor"></i>\
                                        ';
                            var edit = '<?php echo $edit; ?>';
                            if (edit == 1) {
                                botton += '\<a href="addCategoryType.php?id=' + row.room_category_id + '" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only edit m-btn--pill" title="Edit Details">\
                                                        <i class="la la-edit"></i>\
                                                </a>\
                                        ';
                            }
                            var del = '<?php echo $del; ?>';
                            if (del == 1) {
                                botton += '\
                                               \<a onclick="deletedetail(' + row.room_category_id + ')" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill delete" title="Delete">\
                                                        <i class="la la-trash"></i>\
                                                </a>\
                                        '
                            }
                            return botton;
                        }
                    }]
            }
            
            var localSelectorDemo = function () {

            options.search = {
                input: $('#generalSearch'),
            };

            var datatable = $('#catagoryType').mDatatable(options);

            $('#m_form_status').on('change', function () {
                datatable.search($(this).val().toLowerCase(), 'Status');
            });

            $('#m_form_type').on('change', function () {
                datatable.search($(this).val().toLowerCase(), 'Type');
            });

            $('#m_form_status,#m_form_type').selectpicker();

            datatable.on('m-datatable--on-check m-datatable--on-uncheck m-datatable--on-layout-updated', function (e) {
                var checkedNodes = datatable.rows('.m-datatable__row--active').nodes();
                var count = checkedNodes.length;
                $('#m_datatable_selected_number').html(count / 2);
                if (count > 0) {
                    $('#m_datatable_group_action_form').collapse('show');
                } else {
                    $('#m_datatable_group_action_form').collapse('hide');
                }
            });

            $('.changeStatusBnt').on('click', function (e) {
                var id = '';
                var ids = datatable.rows('.m-datatable__row--active').
                        nodes().
                        find('.m-checkbox--single > [type="checkbox"]').
                        map(function (i, chk) {
                            return $(chk).val();
                        });
                for (var i = 0; i < ids.length; i++) {
                    if (i == 0) {
                        id += ids[i];
                    } else {
                        id += ',' + ids[i];
                    }

                }
                if (this.id == 'activeBnt') {
                    window.location.assign("room_category_type.php?stid=" + id + "&value=2");
                } else if (this.id == 'inactiveBnt') {
                    window.location.assign("room_category_type.php?stid=" + id + "&value=1");
                }

            })

            $('#m_datatable_delete_all').on('click', function (e) {
                var id = '';
                var ids = datatable.rows('.m-datatable__row--active').
                        nodes().
                        find('.m-checkbox--single > [type="checkbox"]').
                        map(function (i, chk) {
                            return $(chk).val();
                        });
                for (var i = 0; i < ids.length; i++) {
                    if (i == 0) {
                        id += ids[i];
                    } else {
                        id += ',' + ids[i];
                    }

                }
                jQuery('#m_modal_2').modal('show');
                jQuery('#deteleA').attr('href', "room_category_type.php?stid=" + id + "&delete=yes");

            })

        };
        return {
            // public functions
            init: function () {
                localSelectorDemo();
            }
        };
    }();

    jQuery(document).ready(function () {
        DefaultDatatableDemo.init();
    });
    
    function deletedetail(id) {
        jQuery('#m_modal_2').modal('show');
        jQuery('#deteleA').attr('href', "room_category_type.php?stid=" + id + "&delete=yes");
    };



</script>
