<?php
include("includes/config.inc.php");
include("includes/function.php");
$page_name="Room Category Type";
$_objAdmin = new Admin();

if(isset($_REQUEST['stid']) && $_REQUEST['value']!="")
{
	if($_REQUEST['value']=="Active")
	{	
		$status='I';
	}
	else
	{
		$status='A';
	}
	$cid=$_objAdmin->_dbUpdate(array("status"=>$status,"last_update_date"=>$date),'table_category_type', " 	room_category_id='".$_REQUEST['stid']."'");
	header("Location: room_category_type.php");
}	


if(isset($_REQUEST['show']) && $_REQUEST['show']=="yes")
{
	$_objAdmin->showCategoryType();
	die;
}


?>

<?php include("header.inc.php") ?>

<div class="content-wrapper">
    <div class="container-fluid">
		<div class="pg_header">
			<div class="container">Room Category Type</div>
		</div>
	  	<div class="row pdng-top-lg pdng-btm-lg">
	        <div class="col-lg-12 col-md-12 col-sm-12"> 
	          	<div id="content-table-inner" class="panel panel-default card_bg"> 					
					 <div class="table-responsive table table-bordred table-striped">
								<!-- start id-form -->
								<table  border="0" cellspacing="0" cellpadding="0"  align="center" width="100%">
									<tr>
										<td align="lift" valign="top"  >
											<table id="flex1" style="display:none"></table>
											<script type="text/javascript">showCategoryType();</script>
										</td>
									</tr>
								</table>
							<!-- end id-form  -->
 					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="clear"></div> 
	<!-- start footer -->         
	<?php include("footer.php") ?>
	<!-- end footer -->
