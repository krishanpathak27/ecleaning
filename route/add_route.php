
<style>
::-webkit-scrollbar {
    width: 12px;
}
 
::-webkit-scrollbar-track {
    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3); 
    border-radius: 10px;
}
 
::-webkit-scrollbar-thumb {
    border-radius: 10px;
    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.5); 
}
</style>
<!--  start message-red -->
	<?php if($rou_name_err!=''){?>
	<div id="message-red">
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
	<tr>
		<td class="red-left">Error. <?php echo $rou_name_err; ?></td>
		<td class="red-right"><a class="close-red"><img src="images/icon_close_red.gif"   alt="" /></a></td>
	</tr>
	</table>
	</div>
	<?php } ?>
<!--  end message-red -->
	<form name="frmPre" id="frmPre" method="post" action="route.php" enctype="multipart/form-data" >
		<table border="0" cellpadding="0" cellspacing="0"  id="id-form">
		<tr>
			<th valign="top">Route Name:</th>
			<td><input type="text" name="route_name" id="route_name" class="required" value="<?php echo $auRec[0]->route_name; ?>" /></td>
			<td></td>
		</tr>
		
		<tr>
			<th valign="top">Select Retailer on Route:</th>
			<td>
				<table border="0" cellpadding="0" cellspacing="0"  id="id-form">
				<tr bgcolor="#6E6E6E" style="color: #fff;font-weight: bold;">
					<td style="padding:5px;"><input type="checkbox" name="" > </td>
					<td  width="200px" style="padding:5px;">Retailer Name <input name="search" class="editbox_search" id="editbox_search"  value="" type="text" placeholder="Search"/></td>
					<td width="250px" style="padding:5px;">Retailer Address <input name="search" class="editbox_search" id="editbox_search"  value="" type="text" placeholder="Search"/></td>
					<td width="200px" style="padding:5px;">Retailer Market <input class="editbox_search" onkeyup="showMarketHint(this.value)" value="<?php echo $_SESSION['Market_List']; ?>" type="text" placeholder="Search"/></td>
					<td width="150px" style="padding:5px;">City <input class="editbox_search" type="text" onkeyup="showCityHint(this.value)" value="<?php echo $_SESSION['City_List']; ?>" placeholder="Search"/></td>
					<td width="150px" style="padding:5px;">State <input name="search" class="editbox_search" id="editbox_search"  value="" type="text" placeholder="Search"/></td>
				</tr>
				</table>
				<span id="cityHint">
				<span id="marketHint">
		</tr>
		<tr>
			<th valign="top"></th>
			<td >
				<div style="width:auot; height:300px;overflow:auto;" >
				<table border="1" cellpadding="0" cellspacing="0"  id="id-form">
				<?php
				if($_SESSION['City_List']!=''){
				$city_data=$_SESSION['City_List'];
				$city="and c.city_name LIKE '%$city_data%'";
				}
				if($_SESSION['Market_List']!=''){
				$market_data=$_SESSION['Market_List'];
				$market="and r.retailer_location LIKE '%$market_data%'";
				}
				$auRet=$_objAdmin->_getSelectList('table_retailer as r left join city as c on r.city=c.city_id left join state as s on s.state_id=r.state',"r.retailer_id,r.retailer_name,r.retailer_address,r.retailer_location,c.city_name,s.state_name",''," r.status='A' $city $market and r.account_id=".$_SESSION['accountId']." ORDER BY c.city_name");
				for($i=0;$i<count($auRet);$i++){
				?>
				<tr>
					<td style="padding:5px;"><input type="checkbox" name="retailer_id[]" <?php 
					if($auRec[0]->route_id!=''){
						$auchk=$_objAdmin->_getSelectList('table_route_retailer',"*",''," retailer_id=".$auRet[$i]->retailer_id." and route_id=".$auRec[0]->route_id);
						if(is_array($auchk)){
						echo "checked";
						}
					}
					?> value="<?php echo $auRet[$i]->retailer_id;?>" >
					</td>
					<td  width="200px" style="padding:5px;"><?php echo $auRet[$i]->retailer_name;?></td>
					<td width="250px" style="padding:5px;"><?php echo $auRet[$i]->retailer_address;?></td>
					<td width="200px" style="padding:5px;"><?php echo $auRet[$i]->retailer_location;?></td>
					<td width="150px" style="padding:5px;"><?php echo $auRet[$i]->city_name;?></td>
					<td width="150px" style="padding:5px;"><?php echo $auRet[$i]->state_name;?></td>
				</tr>
				<?php } ?>
				</table>
				</div>
			</td>
		</tr>
		<tr>
			<th>&nbsp;</th>
			<td valign="top" >
			<input name="route_add" type="hidden" value="yes" />
			<input name="rou_id" type="hidden" value="<?php echo $auRec[0]->route_id; ?>" />
			<input name="account_id" type="hidden" value="<?php echo $_SESSION['accountId']; ?>" />
			<input type="reset" value="Reset!" class="form-reset">
			<input type="button" value="Back" class="form-reset" onclick="location.href='route.php?back=back';" />
			<input name="submit" class="form-submit" type="submit" id="submit" value="Save" />
			<?php if($auRec[0]->route_id==''){ ?>
			<input name="submit" class="form-submit" type="submit" id="submit" value="Save & Add Next Route" />
			<?php } ?>
			</td>
		</tr>
		</table>
	</form>
