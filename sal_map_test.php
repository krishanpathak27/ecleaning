 <?php
 $report_day = date('D', strtotime($_SESSION['Mapdate']));
// echo  $report_day;
 
 $auRec=$_objAdmin->_getSelectList2('table_activity',"COUNT(*) AS ttl",''," salesman_id='".$_SESSION['Mapsal']."' and activity_type IN (1,2,3,5,6,11,12,13) and activity_date='".date('Y-m-d', strtotime($_SESSION['Mapdate']))."' and accuracy_level!=0 order by end_time asc");


$locTrackdata =$_objAdmin->_getSelectList2('table_tracking_activity',"COUNT(*) AS ttl",''," salesman_id='".$_SESSION['Mapsal']."' and app_date='".date('Y-m-d', strtotime($_SESSION['Mapdate']))."' and gps_accuracy!=0 and tracking_type='L' order by app_time asc");



$rowCount1 = $auRec[0]->ttl;
$rowCount2 = $locTrackdata[0]->ttl;

$row = $rowCount1+$rowCount2;
//echo $row;
//exit;
if($row <=0)
{
	$err="This Salesman has not worked during these period";
}



if($_SESSION['Mapdate']==$date){
?>
<script type="text/javascript">
setInterval('initialize()', 120000);
</script>
<?php
}
 ?>
 <style type="text/css">
     html { height: 100% }
      body { height: 100%; margin: 0; padding: 0 }
      #map_canvas { height: 100% }
	  .tooltip { position:absolute;
	  width: 150px;
	  height: 150px;
	  padding: 5px;
	  margin:350px,120px,0px,100px;
	  border: 1px solid gray;
	  font-size: 9pt;
	  font-family: Verdana;
	  color: #000;
     }

</style>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>
<script type="text/javascript" src="javascripts/custom_map_tooltip.js"></script>
<script src="javascripts/jquery.min.1.10.2.js">
</script>
<script type="text/javascript">

function initialize() {
	<?php 
	$auRec = array();
	$result = array();
	$locTrackdata = array();

	$result =$_objAdmin->_getSelectList2('table_activity',"lat,lng,end_time as app_time,accuracy_level",''," salesman_id='".$_SESSION['Mapsal']."' and activity_type IN (1,2,3,5,6,11,12,13) and activity_date='".date('Y-m-d', strtotime($_SESSION['Mapdate']))."' and accuracy_level!=0  order by end_time asc");

	$locTrackdata =$_objAdmin->_getSelectList2('table_tracking_activity',"lat,lng,app_time,	gps_accuracy as accuracy_level",''," salesman_id='".$_SESSION['Mapsal']."' and app_date='".date('Y-m-d', strtotime($_SESSION['Mapdate']))."' and gps_accuracy!=0 and tracking_type='L' order by app_time asc");
	//print_r($result);
	//print_r($locTrackdata);

	if(sizeof($result)<=0 && sizeof($locTrackdata)>0)
		$auRec = $locTrackdata;
	elseif(sizeof($result)>0 && sizeof($locTrackdata)<=0)
		$auRec = $result;
	else
		$auRec = array_merge($result, $locTrackdata);


	function myfunc($a,$b){
    return strnatcmp($a->app_time,$b->app_time);
    }
    usort($auRec,"myfunc");
    $count=count($auRec);
  
    $lastlat=$auRec[$count-1]->lat;
    $lastlng=$auRec[$count-1]->lng;
    $lasttime=$auRec[$count-1]->app_time;

    ?>

<?php /*$Rec=$_objAdmin->_getSelectList2('table_activity',"lat,lng,end_time",''," salesman_id='".$_SESSION['Mapsal']."' and activity_type='13' and activity_date='".date('Y-m-d', strtotime($_SESSION['Mapdate']))."' and accuracy_level!=0 order by end_time desc"); 
*/
?>
    var myOptions = {
        zoom: 13,
        center: new google.maps.LatLng(<?php echo $lastlat.",".$lastlng;?>),
        mapTypeId: google.maps.MapTypeId.ROADMAP
            }
    var map = new google.maps.Map(document.getElementById("map_canvas"),myOptions);
		<?php
		for($h = 0; $h < $row-1; $h++) 
		{ 
		$lat_array[]=$auRec[$h]->lat;
		$lng_array[]=$auRec[$h]->lng;
		$i=$i+1;
		?>
		var myLatLng = new google.maps.LatLng(<?php echo substr($auRec[$h]->lat, 0, 10).",".substr($auRec[$h]->lng, 0, 10);?>);
		var beachMarker = new google.maps.Marker({
		position: myLatLng,title:" Time: <?php echo $auRec[$h]->app_time ?> ,Lat: <?php echo substr($auRec[$h]->lat, 0, 7) ?>,Long: <?php echo substr($auRec[$h]->lng, 0, 7) ?> , Accuracy: <?php echo $auRec[$h]->accuracy_level ?> ",
		map: map
		});
		<?php } ?>
		<?php
		$k=0;
		for($j=0;$j<$i-1;$j++)
		{
		$k=$j+1;
		?>
		var flightPlanCoordinates = [
		new google.maps.LatLng(<?php echo substr($lat_array[$j], 0, 10).",".substr($lng_array[$j], 0, 10); ?> ),
		new google.maps.LatLng(<?php echo substr($lat_array[$k], 0, 10).",".substr($lng_array[$k], 0, 10); ?> )
		];
		var lineSymbol = {
        path: google.maps.SymbolPath.FORWARD_CLOSED_ARROW
		};
		var arrow = {
			icon: lineSymbol,
			offset: '50%',
			strokeColor: "#980000 ",
			strokeOpacity :1,
		};
		var flightPath = new google.maps.Polyline({
						path: flightPlanCoordinates,
						strokeColor: "#980000 ",
						strokeOpacity: 1.0,
						strokeWeight: 2,
						icons : [arrow],
						});
		flightPath.setMap(map);
		<?php } ?>
		<?php //$StartRec=$_objAdmin->_getSelectList2('table_activity',"lat,lng,end_time",''," salesman_id='".$_SESSION['Mapsal']."' and activity_type='13' and activity_date='".date('Y-m-d', strtotime($_SESSION['Mapdate']))."' and accuracy_level!=0  order by end_time asc"); ?>
		var myLatlng = new google.maps.LatLng(<?php echo substr($auRec[0]->lat, 0, 10).",".substr($auRec[0]->lng, 0, 10);?>)
		var marker = new google.maps.Marker
		({ position: myLatlng,title:"START,Time: <?php echo $auRec[0]->app_time;?>", icon: 'images/yellow_MarkerS.png'});
		marker.setMap(map);
		var myLatlng = new google.maps.LatLng(<?php echo substr($lastlat, 0, 10).",".substr($lastlng, 0, 10);?>)
		var marker = new google.maps.Marker({ position: myLatlng,title:"FINISH,Time: <?php echo $lasttime;?>", icon: 'images/blue_MarkerF.png'});
		marker.setMap(map);
		
		
		
}
</script>
<body onLoad="initialize()">
