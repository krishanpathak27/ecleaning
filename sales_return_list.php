<?php
include("includes/config.inc.php");
include("includes/function.php");
$_objAdmin = new Admin();
$_objModuleClass = new ModuleClass();

$feature_array = $_objModuleClass->getFeatureModulel();
//print_r($feature_array);
if(in_array(21,$feature_array))
{
$flag=1;
// for  show.
}
else
{
$flag=0;
// for not show.	
}
$day= date("D");
//echo $_SESSION['orderdetails'];
$auOrd=$_objAdmin->_getSelectList('table_sales_return AS o 
	Left Join table_retailer AS r ON o.retailer_id    = r.retailer_id 
	Left Join table_distributors AS d ON d.distributor_id = o.distributor_id 
	Left Join table_salesman AS s ON s.salesman_id    = o.salesman_id',
	"o.*,
	r.retailer_name,
	r.retailer_address,
	r.retailer_location,
	r.retailer_phone_no,
	d.distributor_address,
	d.distributor_location,
	d.distributor_phone_no,
	d.distributor_name,
	s.salesman_name",''," o.sales_order_id=".$_SESSION['orderdetails']." ");
	
/*echo "<pre>";
print_r($_SESSION);	*/
	
	
?>


<?php include("header.inc.php") ?>
<script type="text/javascript">
 $(document).keypress(function(e) {
    if(e.which == 96) {
       location.href='admin_order_list.php';
    }
});
</script>
<style>
div.img
  {
  margin:2px;
  border:1px solid  #43A643;
  height:auto;
  /*width:100%;*/
  float:left;
 /* text-align:center;*/
  }
div.img img
  {
  display:inline;
  margin:3px;
  border:1px solid #fbfcfc;
  }
div.img a:hover img
  {
  border:1px solid #fbfcfc;
  }
div.desc
  {
  text-align:center;
  font-weight:normal;
  width:120px;
  margin:2px;
  }
</style>
<!-- start content-outer -->
<div id="content-outer">
<!-- start content -->
<div id="content">
<div id="page-heading">
	<h1><span style="color: #d74343; font-family: Tahoma; font-weight: bold;">Sales Return Details</span></h1></div>
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" id="content-table">
<!--<tr>
	<th rowspan="3" class="sized"><img src="images/side_shadowleft.jpg" width="20" height="300" alt="" /></th>
	<th class="topleft"></th>
	<td id="tbl-border-top">&nbsp;</td>
	<th class="topright"></th>
	<th rowspan="3" class="sized"><img src="images/side_shadowright.jpg" width="20" height="300" alt="" /></th>
</tr>
-->
<tr>
	<!--<td id="tbl-border-left"></td>-->
	<td align="center" valign="middle">
	<!--  start content-table-inner -->
	<div id="content-table-inner">
	
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
		<tr valign="top">
			<td>
				<table border="0" width="100%" cellpadding="0" cellspacing="0" id="id-form">
				<tr>
					<td><b>Quality Auditor Name:</b> <?php echo $auOrd[0]->salesman_name;?></td>
				</tr>
				
				<tr>
					<td><b>Distributor Name:</b> <?php echo $auOrd[0]->distributor_name;?></td>
					<td align="right" style="padding-right: 50px;"><b>Report Number:</b> <?php echo $auOrd[0]->sales_order_id;?></td>
				</tr>
				
				<?php if(isset($auOrd[0]->retailer_name) && $auOrd[0]->retailer_name!=""){?>
				<tr>
					<td><b>Retailer Name:</b> <?php echo $auOrd[0]->retailer_name;?></td>
					<td align="right" style="padding-right: 50px;"><b>Date:</b> <?php echo date("d-M-Y", strtotime($auOrd[0]->date_of_order)); ?></td>
				</tr>
				<tr>
					<td><b>Retailer Mobile-No:</b> <?php echo $auOrd[0]->retailer_phone_no;?></td>
					<td align="right" style="padding-right: 50px;"><b>Time:</b> <?php echo $auOrd[0]->time_of_order;?></td>
				</tr>
			
				<tr>
					<td><b>Retailer Market:</b> <?php echo $auOrd[0]->retailer_location;?></td>
					<td align="right" style="padding-right: 50px;"><?php if($auOrd[0]->bill_no !=''){ ?><b>Bill No:</b> <?php echo $auOrd[0]->bill_no; }?></td>
				</tr>
				
				<?php } else {?>
				
				<tr>
					<td><b>Distributor Mobile-No:</b> <?php echo $auOrd[0]->distributor_phone_no;?></td>
					<td align="right" style="padding-right: 50px;"><b>Time:</b> <?php echo $auOrd[0]->time_of_order;?></td>
				</tr>
			
				<tr>
					<td><b>Distributor Market:</b> <?php echo $auOrd[0]->distributor_location;?></td>
					<td align="right" style="padding-right: 50px;"><?php if($auOrd[0]->bill_no !=''){ ?><b>Bill No:</b> <?php echo $auOrd[0]->bill_no; }?></td>
				</tr>
				
				<tr>
					<td><b>Distributor Address:</b> <?php echo $auOrd[0]->distributor_address;?></td>
				
				</tr>
				
				<? }?>
			
			</table>
			</td>	
		</tr>
		<?php //echo $auOrd[0]->order_type; ?>
		<tr valign="top">
		<td>
		<?php if($auOrd[0]->order_type=='No'){ ?>
		<table border="0" width="100%" cellpadding="0" cellspacing="0" id="id-form">
			<tr bgcolor="#6E6E6E" style="color: #fff;font-weight: bold;">
				<td style="padding:10px;" >Comments</td>
			</tr>
			<tr>
				<td style="padding:10px;" ><?php echo $auOrd[0]->comments;?></td>
			</tr>
			
			
			<tr>
				<td colspan="6">
					<?php $auImg=$_objAdmin->_getSelectList('table_image AS i',"i.*",''," image_type=3 AND ref_id=".$auOrd[0]->sales_order_id);
						if(is_array($auImg)){
							for($j=0;$j<count($auImg);$j++){?>
							<div class="img">
								<a href="photo/<?php echo $auImg[$j]->image_url;?>"  target="_blank">
									<img src="photo/<?php echo $auImg[$j]->image_url;?>" alt="" width="140" height="120">
								</a>
							</div>
						<?php } } else { ?>
						<div class="img">
							<div style="width: 100%;height:50px;">
								<span style="color: #A52A2A; font-family: Tahoma; font-weight: bold;">Image Not Available</span>
							</div>
						</div>
					   <?php } ?>
				</td>
			</tr>
			<tr align="center" >
				<?php  if($_SESSION['userLoginType']==1 || $_SESSION['userLoginType']==2)  { ?>
				<td style="padding:10px;" colspan="7"><input type="button" value="Back" class="form-cen" onclick="location.href='<?php echo $page;?>?searchParam_1=<?php echo $_REQUEST['searchParam_1']; ?>&searchParam_2=<?php echo $_REQUEST['searchParam_2']; ?>';" /></td>
				<?php } ?>
				<?php  if($_SESSION['userLoginType']==4)  { ?>
				<td style="padding:10px;"><input type="button" value="Back" class="form-cen" onclick="location.href='retailer_order_list.php';" /></td>
				<?php } ?>
				<?php  if($_SESSION['userLoginType']==5)  { ?>
				<td style="padding:10px;"><input type="button" value="Back" class="form-cen" onclick="location.href='salesman_order_list.php';" /></td>
				<?php } ?>
			</tr>
		</table>
		
		<?php } else { ?>
		<table border="0" width="100%" cellpadding="0" cellspacing="0" id="id-form">
			<tr bgcolor="#6E6E6E" style="color: #fff;font-weight: bold;">
			    <!--<td style="padding:10px;" width="5%">ODID</td>-->
				<td style="padding:10px;" width="30%">Item</td>
				<td style="padding:10px;" width="10%">Item Code</td>
				<!--<td style="padding:10px;" width="10%">Color Code</td>
				<td style="padding:10px;" width="10%">Batch No.</td>
				<td style="padding:10px;" width="10%">Date of Manufacture</td>-->
				<td style="padding:10px;" width="10%">Quantity</td>
				<td style="padding:10px;" width="10%">Accepted</td>
				<td style="padding:10px;" width="10%">Comment</td>
				<td style="padding:10px;" width="10%">Reason</td>
				<td style="padding:10px;" width="10%">Status</td>
				<td></td>
				
			</tr>
  <?php $condi=" where t.sales_order_id=".$_SESSION['orderdetails']." ";		
		
		$Rec = mysql_query("SELECT t.*,i.item_name,i.item_code, p.item_mrp, p.item_mrp * t.quantity AS Total, qad.batch_no,  qad.date_of_manfacture, qad.comment, qad.reasons FROM table_sales_return_detail as t Left Join table_item as i on t.item_id=i.item_id Left Join table_price as p on t.item_id=p.item_id LEFT JOIN table_qa_odrdetail_attrs AS qad ON t.sales_order_detail_id = qad.sales_order_detail_id".$condi);
		$sum = 0;
		while ($auRec = mysql_fetch_array($Rec)){ //echo "<pre>"; print_r($auRec);?>
		<tr>
			<!--<td style="padding:10px;" ><?php echo $auRec['sales_order_detail_id'];?></td>-->
			<td style="padding:10px;" ><?php echo $auRec['item_name'];?></td>
			<td style="padding:10px;" ><?php echo $auRec['item_code'];?></td>
			<!--<td style="padding:10px;" ><?php echo $auRec['color_code'];?></td>
			<td style="padding:10px;" ><?php echo $auRec['batch_no'];?></td>-->
			<!--<td style="padding:10px;" ><?php echo $_objAdmin->_changeDate($auRec['date_of_manfacture']);?></td>-->
			<td style="padding:10px;" ><?php echo $auRec['quantity'];?></td>
			<td style="padding:10px;" ><?php echo $auRec['acc_quantity'];?></td>
			<td style="padding:10px;" ><?php echo $auRec['comment'];?></td>
			<td style="padding:10px;" ><?php echo $auRec['reasons'];?></td>
			<td style="padding:10px;" ><?php if($auRec['order_detail_status'] == 1){ echo "New"; } elseif($auRec['order_detail_status'] == 2){ echo "Accepted"; } else { echo "Rejected"; };?></td>
		</tr>
		
		
		
		<?php $sum = $sum +  $auRec['Total']; } ?>
		<tr bgcolor="#6E6E6E" style="color: #fff;font-weight: bold;">	
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td valign="baseline"><br /><?php //echo $sum;?></td>
			<td>&nbsp;</td>
		</tr>
		<?php if($flag==1){ ?>
		
<tr>
				<td colspan="7">
					<?php $auImg=$_objAdmin->_getSelectList('table_image AS i',"i.*",''," ref_id=".$auOrd[0]->sales_order_id." AND image_type='8'");
						if(is_array($auImg)){
							for($j=0;$j<count($auImg);$j++){?>
							<div class="img">
								<a href="photo/<?php echo $auImg[$j]->image_url;?>"  target="_blank">
									<img src="photo/<?php echo $auImg[$j]->image_url;?>" alt="" width="140" height="120">
								</a>
							</div>
						<?php } } else { ?>
						<div class="img">
							<div style="width: 100%;height:50px;">
								<span style="color: #A52A2A; font-family: Tahoma; font-weight: bold;">Image Not Available</span>
							</div>
						</div>
					   <?php } ?>
				</td>
			</tr>
<?php } ?>
		<tr align="center" >
			
			<td style="padding:10px;"><input type="button" value="Back" class="form-cen" onclick="location.href='salesreturns.php';" /></td>
		</tr>
		</table>
		
		<?php } ?>
		
		
	</td>
	</tr>
	</table>
	</div>
	</td>
</tr>
</table>
<div class="clear"></div>
</div>
<!--  end content-table-inner  -->
</td>
<td id="tbl-border-right"></td>
</tr>
<!--<tr>
	<th class="sized bottomleft"></th>
	<td id="tbl-border-bottom">&nbsp;</td>
	<th class="sized bottomright"></th>
</tr>-->
</table>
<div class="clear">&nbsp;</div>
</div>
<!--  end content -->
<div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->
<div class="clear">&nbsp;</div> 
<!-- start footer -->         
<?php include("footer.php") ?>
<!-- end footer -->

</body>
</html>
