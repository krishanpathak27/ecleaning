<?php
include("includes/config.inc.php");
include("includes/function.php");
$_objAdmin = new Admin();

/* $auMarker=$_objAdmin->_getSelectList('table_order as o left join table_retailer as r on o.retailer_id=r.retailer_id left join table_salesman as s on s.salesman_id=o.salesman_id',"o.lat,o.lng,r.lat as rlat,r.lng as rlng,r.retailer_name,r.retailer_id,s.salesman_name",''," where o.order_id='".base64_decode($_REQUEST['ord'])."' ");
$ret_name=$auMarker[0]->retailer_name;
$ret_lat=$auMarker[0]->rlat;
$ret_lng=$auMarker[0]->rlng;
$sal_name=$auMarker[0]->salesman_name;
$sal_lat=$auMarker[0]->lat;
$sal_lng=$auMarker[0]->lng;
if($auMarker[0]->rlat==''){
$auMarker=$_objAdmin->_getSelectList('table_survey ',"lat as rlat,lng as rlng",''," where retailer_id='".$auMarker[0]->retailer_id."' order by survey_date desc ");
$ret_lat=$auMarker[0]->rlat;
$ret_lng=$auMarker[0]->rlng;
} */
// retailer map only survey table
$auOrder=$_objAdmin->_getSelectList('table_sales_return as o left join table_retailer as r on o.retailer_id=r.retailer_id left join table_salesman as s on s.salesman_id=o.salesman_id',"o.lat,o.lng,o.location_provider,o.accuracy_level,r.retailer_name,r.retailer_id,r.lat as rlat,r.lng as rlng,s.salesman_name",''," o.sales_order_id='".base64_decode($_REQUEST['ord'])."' ");
$ret_name=$auOrder[0]->retailer_name;
$sal_name=$auOrder[0]->salesman_name;
$sal_lat=$auOrder[0]->lat;
$sal_lng=$auOrder[0]->lng;
$sal_accuracy_level=$auOrder[0]->accuracy_level;
$sal_network_mode=$auOrder[0]->location_provider;
$ret_lat=$auOrder[0]->rlat;
$ret_lng=$auOrder[0]->rlng;
/*$auMarker=$_objAdmin->_getSelectList('table_survey ',"lat as rlat,lng as rlng,accuracy_level,network_mode",''," where retailer_id='".$auOrder[0]->retailer_id."' order by survey_date desc,survey_time desc limit 0,1");
if($auMarker[0]->rlat!=0.0){
$ret_lat=$auMarker[0]->rlat;
$ret_lng=$auMarker[0]->rlng;
$accuracy_level=$auMarker[0]->accuracy_level;
$network_mode=$auMarker[0]->network_mode;
} else {
$ret_lat="";
$ret_lng="";
}*/


?>
<html>
<head>
<meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=9" >
<meta http-equiv="X-UA-Compatible" content="IE=8" />
<meta http-equiv="X-UA-Compatible" content="IE=7" />
<meta http-equiv="X-UA-Compatible" content="IE=6" />

<style type="text/css">
  html { height: 100% }
  body { height: 100%; margin: 0; padding: 0 }
  #map_canvas { height: 100% }
</style>
<script type="text/javascript">
 $(document).keypress(function(e) {
    if(e.which == 13) {
       javascript:window.close();
    }
});
</script>

<script type="text/javascript"
src="http://maps.googleapis.com/maps/api/js?sensor=false">
</script>
<script type="text/javascript">
      function initialize() {
        var myOptions = 
		{
		<?php if($ret_lat!=''){ ?>
          center: new google.maps.LatLng(<?php echo $ret_lat; ?>, <?php echo $ret_lng; ?>), zoom: 18, mapTypeId: google.maps.MapTypeId.ROADMAP
		  <?php } else { ?>
		  center: new google.maps.LatLng(<?php echo $sal_lat; ?>, <?php echo $sal_lng; ?>), zoom: 18, mapTypeId: google.maps.MapTypeId.ROADMAP
		  <?php } ?>
        };
        var map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
		<?php if($ret_lat!=''){ ?>
		var myLatlng = new google.maps.LatLng(<?php echo $ret_lat; ?>, <?php echo $ret_lng; ?>)
		var marker = new google.maps.Marker
		({ position: myLatlng,title:"Retailer Name: <?php echo $ret_name; ?>", animation: google.maps.Animation.DROP,});
		marker.setMap(map);
		<?php } ?>
		var myLatlng = new google.maps.LatLng(<?php echo $sal_lat; ?>, <?php echo $sal_lng; ?>)
		var marker = new google.maps.Marker
		({ position: myLatlng,title:"Salesman Name: <?php echo $sal_name; ?>, Location Type: <?php echo $sal_network_mode; ?>, Accuracy: <?php echo $sal_accuracy_level; ?>",icon: 'images/sal_marker.png', animation: google.maps.Animation.DROP,});
		marker.setMap(map);
		<?php if($ret_lat!=''){ ?>
		var flightPlanCoordinates = [
			new google.maps.LatLng(<?php echo $ret_lat; ?>, <?php echo $ret_lng; ?>),
			new google.maps.LatLng(<?php echo $sal_lat; ?>, <?php echo $sal_lng; ?>)
		];
		var flightPath = new google.maps.Polyline({
			path: flightPlanCoordinates,
			strokeColor: "#FF0000",
			strokeOpacity: 1.0,
			strokeWeight: 2
		});
		var circleOptions1 = {
			center: new google.maps.LatLng(<?php echo $ret_lat; ?>, <?php echo $ret_lng; ?>),
			radius: 50,
			strokeWeight: 0,
			fillColor: "#BDBDBD",
			fillOpacity: 0.50,
			map: map,
			editable: false
			};
		var myLatlng1 = new google.maps.LatLng(<?php echo $ret_lat; ?>, <?php echo $ret_lng; ?>);	
		var circle = new google.maps.Circle(circleOptions1);
		flightPath.setMap(map);
		<?php } ?>
     }
</script>

  </head>
  <body onLoad="initialize()">
  
    <div id="map_canvas" style="width:100%; height:100%"></div>
  </body>
  <?php if($ret_lat==''){ ?>
<script language="Javascript">
alert("Retailer Location does not exists.");
</script>
  <?php } ?>
</html>