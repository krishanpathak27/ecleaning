<?php
include("includes/config.inc.php");
include("includes/function.php");
include("includes/globalarraylist.php");

include("header.inc.php");
/*if(isset($_POST['add']) && $_POST['add'] == 'yes')
{
$from_date=$_POST['from'];
$to_date=$_POST['to'];
} else {
$from_date=date("Y-m-d",strtotime("-0 day"));
$to_date=date("Y-m-d",strtotime("-0 day"));
}*/
if(isset($_REQUEST['activity_date']) && $_REQUEST['activity_date']!="" )
{
	//$_SESSION['Mapsal']=base64_decode($_REQUEST['sal']);
	$_SESSION['activity_date']=$report_date;
	//$_SESSION['Todate']=$to_date;
	header("Location: salesman_activity_distance_report.php");
}

//$report_day = date('D', strtotime($report_date));
include("sal_activity_distance_map.php");?>
 
<!-- <script>
    $(function() {
        $( "#from" ).datepicker({
			dateFormat: "d M yy",
            defaultDate: "1w",
            changeMonth: true,
            numberOfMonths: 1,
            onSelect: function( selectedDate ) {
                $( "#to" ).datepicker( "option", "minDate", selectedDate );
            }
        });
        $( "#to" ).datepicker({
			dateFormat: "d M yy",
            defaultDate: "-1w",
            changeMonth: true,
            numberOfMonths: 1,
            onSelect: function( selectedDate ) {
                $( "#from" ).datepicker( "option", "maxDate", selectedDate );
            }
        });
    });
	$(document).ready(function(){
	    $("#sal").change(function(){
		 document.report.submit();
		})
	});
</script> -->
<script src="javascripts/jquery-1.8.2.js" type="text/javascript"></script>
<script src="javascripts/jquery-ui.js"></script>
<script type="text/javascript" src="javascripts/validate.js"></script>


<!-- start content-outer -->
<div id="content-outer">
<!-- start content -->
<div id="content">
<div id="page-heading"><h1><span style="color: #d74343; font-weight: bold;">Total Distance Report</span></h1> 
	<form name="report" id="report" action="salesman_distance_report.php" method="post">
	<div id="page-heading" align="left" style="padding-left: 350px;"><h3><span style=" color:#000000;">Salesman: 
	
	<select name="sal" id="sal" class="menulist" style="width:150px" >
		<option value="" >Select</option>
		<?php $aSal=$_objAdmin->_getSelectList('table_salesman AS s','*',''," s.status = 'A' $salesman ORDER BY salesman_name"); 
		if(is_array($aSal)){
		for($i=0;$i<count($aSal);$i++){?>
		<option value="<?php echo base64_encode($aSal[$i]->salesman_id);?>" <?php if ($aSal[$i]->salesman_id==$_SESSION['Mapsal']){ ?> selected <?php } ?>><?php echo $aSal[$i]->salesman_name;?></option>
		<?php } }?>
	</select>
	</span>
	</h3>
	</div>
	</form>
	
</div>

<table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">
<tr>
	<!--<td id="tbl-border-left"></td>-->
	<td>
	<!--  start content-table-inner -->
	<div id="content-table-inner">
	
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
	
	
	
	

	<div id="Report">
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
	<tr valign="top">
		<td>
		
		<table  border="0" width="100%" cellpadding="0" cellspacing="0" id="report_export" name="report_export">
			<div id="Report">
			<tr bgcolor="#6E6E6E" style="color: #fff;font-weight: bold;" >
				
				<td style="padding:10px;" width="20%">Date</td>
				<td style="padding:10px;" width="20%">Customer Name</td>
				<td style="padding:10px;" width="20%">Type</td>
				<td style="padding:10px;" width="20%">Address</td>
				<td style="padding:10px;" width="20%">Distance Travelled</td>
				
				
				
			</tr>
			<?php $i=0; foreach ($auRec as $key => $value) {?>
			
				<tr style="border:1px solid #CAC5C5;">
				<td style="padding:10px;" width="30%"><?php $activity_date=date("j M Y", strtotime($value1->activity_date)); ?> <a href=""><?php echo $activity_date; ?></a></td>				
				<td style="padding:10px;" width="30%"><?php echo round($sum[$i],2)." km"; ?></td>
			<?php $i++; } ?>
			<tr><td style="padding:10px;font-weight:bold; font-size:16px;" width="30%">Total Distance Travelled</td>				
				<td style="padding:10px;font-weight:bold; font-size:16px;" width="30%"><?php echo round($totalsum,2)." km"; ?></td></tr>
			</div>
			</table>
			</td>
			</tr>
			</table>
			<!--<tr>	
				<div id="map_canvas"></div>
			</tr>-->
			
		</table>
		</div>
		<!-- end id-form  -->
	</td>
	
	<td>
	<!-- right bar-->
	<?php //include("rightbar/salesmanMapList_bar.php") ?>
	</td>
	</tr>
<tr>
<td><img src="images/blank.gif" width="695" height="1" alt="blank" /></td>
<td></td>
</tr>
</table>

<!--  end content-table-inner  -->
</td>
<td id="tbl-border-right"></td>
</tr>
<!--<tr>
	<th class="sized bottomleft"></th>
	<td id="tbl-border-bottom">&nbsp;</td>
	<th class="sized bottomright"></th>
</tr>-->
</table>
<div class="clear">&nbsp;</div>
</div>
<!--  end content -->
<div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->
<div class="clear">&nbsp;</div> 
<!-- start footer -->         
<?php include("footer.php");?>
<!-- end footer -->
 
</body>
</html>