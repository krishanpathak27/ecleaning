<?php
include("includes/config.inc.php");
include("includes/function.php");
$_objAdmin = new Admin();

$mstData = $_objAdmin->_getSelectList("table_salesman_allowance_details AS AD 
LEFT JOIN table_salesman AS S ON S.salesman_id = AD.salesman_id 
LEFT JOIN city AS C ON C.city_id = S.city 
LEFT JOIN city as c2 on c2.city_id=AD.city_id
LEFT JOIN state AS st ON st.state_id = S.state",
"AD.cmt, AD.salesman_id,c2.city_name AS allwn_city, AD.city_id, st.state_name, S.salesman_name, AD.app_date, AD.app_time, C.city_name",'',
" allws_id = '".$_REQUEST['id']."'");

//echo "<pre>";
//print_r($mstData);


$auOrd=$_objAdmin->_getSelectList("table_salesman_allowance_details AS AD 
	LEFT JOIN city AS C ON C.city_id = AD.city_id 
	LEFT JOIN table_salesman_allowance_mapping AS AM ON AM.allws_id = AD.allws_id 
	LEFT JOIN table_salesman_tags AS A ON A.`id` = AM.tada_id AND A.`account_id` = '".$_SESSION['accountId']."'
	LEFT OUTER JOIN table_salesman_allwns_slab_category_amt AS SA ON SA.`allwns_cat_id` = AM.`allwns_cat_id` AND SA.`id` = AM.`tada_id` AND SA.salesman_id = '".$mstData['0']->salesman_id."' AND SA.`account_id` = '".$_SESSION['accountId']."' 
	LEFT JOIN table_salesman_allwns_slab_category AS SC ON SC.allwns_cat_id = SA.`allwns_cat_id`",
	"AD.allws_id, SC.allwns_cat_name, AD.city_id, AD.salesman_id, AD.cmt, AD.app_date, A.desc, AM.*, SA.status AS flag, C.city_name",'',
	" A.status = 'A' AND AD.app_date ='".$mstData['0']->app_date."' AND AD.salesman_id = '".$mstData['0']->salesman_id."'");
?>
<?php include("header.inc.php") ?>

<style>
div.img
  {
  margin:2px;
  border:1px solid  #43A643;
  height:auto;
  /*width:100%;*/
  float:left;
 /* text-align:center;*/
  }
div.img img
  {
  display:inline;
  margin:3px;
  border:1px solid #fbfcfc;
  }
div.img a:hover img
  {
  border:1px solid #fbfcfc;
  }
div.desc
  {
  text-align:center;
  font-weight:normal;
  width:120px;
  margin:2px;
  }
</style>
<!-- start content-outer -->
<div id="content-outer">
<!-- start content -->
<div id="content">
<div id="page-heading">
	<h1><span style="color: #d74343; font-family: Tahoma; font-weight: bold;">Sales Allowances Details</span></h1></div>
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" id="content-table">
<tr>
	<!--<td id="tbl-border-left"></td>-->
	<td align="center" valign="middle">
	<!--  start content-table-inner -->
	<div id="content-table-inner">
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
		<tr valign="top">
			<td>
				<table border="0" width="100%" cellpadding="0" cellspacing="0" id="id-form">
				<tr>
					<td><b>Salesman:</b> <?php echo $mstData[0]->salesman_name;?></td>
				</tr>
				
				<tr>
					<td><b>Salesman State:</b> <?php echo $mstData[0]->state_name;?></td>
					<td align="right" style="padding-right: 50px;"><b>Date:</b> <?php echo $_objAdmin->_changeDate($mstData[0]->app_date);?></td>
				</tr>
				
				<tr>
					<td><b>Salesman City:</b> <?php echo $mstData[0]->city_name;?></td>
					<td align="right" style="padding-right: 50px;"><b>Time:</b> <?php echo date('h:i A', strtotime($mstData[0]->app_time));?></td>
				</tr>
				
				<tr>
					<td><b>Allowance City:</b> <?php echo $mstData[0]->allwn_city;?></td>
					<!--<td align="right" style="padding-right: 50px;"><b>Report Number:</b> <?php echo $mstData[0]->allws_id;?></td>-->
				</tr>
				
<tr>
					<td><b>Comment :</b> <?php echo $mstData[0]->cmt;?></td>
					<!--<td align="right" style="padding-right: 50px;"><b>Report Number:</b> <?php echo $mstData[0]->allws_id;?></td>-->
				</tr>
				
			
			</table>
			</td>	
		</tr>
		
		<tr valign="top">
		<td>
		<table border="0" width="100%" cellpadding="0" cellspacing="0" id="id-form">
			<tr bgcolor="#6E6E6E" style="color: #fff;font-weight: bold;">
			   <!-- <td style="padding:10px;" width="5%">ODID</td>-->
				<td style="padding:10px;" align="left">Allowances</td>
				<td style="padding:10px;">Remark</td>
				<td style="padding:10px;" width="20%" align="center">Allowance Slab</td>
				<td style="padding:10px;" width="15%"align="right">Allowances</td>
				<!--<td style="padding:10px;">flag</td>-->
				<td></td>
				<td></td>
			</tr>
  <?php 
  	
//echo "<pre>";
//print_r($auOrd);	
		$sum = 0;
		foreach($auOrd as $auRec){ //echo "<pre>"; print_r($auRec);?>
		<tr>
			<!--<td style="padding:10px;" ><?php echo $auRec->allws_id;?></td>-->
			<td style="padding:10px;" ><?php echo $auRec->desc;?></td>
						<td style="padding:10px;" ><?php echo $auRec->remark;?></td>
			<td style="padding:10px;" align="center"><?php if(isset($auRec->allwns_cat_name)) echo $auRec->allwns_cat_name; else echo "-";?></td>
			<td style="padding:10px;" align="right"><?php echo $auRec->allwns_amt;?></td>

			<!--<td style="padding:10px;" ><?php echo $auRec->flag;?></td>	-->		
		</tr>
		
		
		
		<?php $sum = $sum +  $auRec->allwns_amt; } ?>
		<tr bgcolor="#6E6E6E" style="color: #fff;font-weight: bold;">	
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td style="padding:10px;" align="right"><?php echo number_format($sum, 2);?></td>
			<!--<td>&nbsp;</td>-->
		</tr>
		<?php $flag = 1;  if($flag==1){ ?>
		
<tr>
				<td colspan="5">
					<?php if(isset($auOrd[0]->allws_id)) {
					 $auImg=$_objAdmin->_getSelectList('table_image AS i',"i.*",''," ref_id=".$auOrd[0]->allws_id." AND image_type='10'"); //print_r($auImg);
						if(is_array($auImg)){
							for($j=0;$j<count($auImg);$j++){?>
							<div class="img">
								<a href="photo/<?php echo $auImg[$j]->image_url;?>"  target="_blank">
									<img src="photo/<?php echo $auImg[$j]->image_url;?>" alt="" width="140" height="120">
								</a>
							</div>
						<?php } } else { ?>
						<div class="img">
							<div style="width: 100%;height:50px;">
								<span style="color: #A52A2A; font-family: Tahoma; font-weight: bold;">Image Not Available</span>
							</div>
						</div>
					   <?php } } ?>
				</td>
			</tr>
<?php } ?>
		<tr align="center" >
			<td style="padding:10px;" align="left"><input type="button" value="Back" class="form-cen" onclick="location.href='salesman_allownce.php';" /></td>
		</tr>
		</table>
	</td>
	</tr>
	</table>
	</div>
	</td>
</tr>
</table>
<div class="clear"></div>
</div>
<!--  end content-table-inner  -->
</td>
<td id="tbl-border-right"></td>
</tr>
</table>
<div class="clear">&nbsp;</div>
</div>
<!--  end content -->
<div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->
<div class="clear">&nbsp;</div> 
<!-- start footer -->         
<?php include("footer.php") ?>
<!-- end footer -->

</body>
</html>