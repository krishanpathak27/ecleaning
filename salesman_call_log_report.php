<?php 
/*
* Created By : Abhishek Jaiswal
* date : 19 feb 2016
*/
include("includes/config.inc.php");
include("includes/function.php");
include("includes/globalarraylist.php");
$page_name="Salesman Call Log Report";
if($_SESSION['userLoginType'] == 5){
	$divisionIdString = implode(",", $divisionList);
	$division = " s.division_id IN ($divisionIdString) ";
	$division1 = " division_id IN ($divisionIdString) ";
} else {
	$division = " ";
	$division1 = " ";
}
if(isset($_POST['showReport']) && $_POST['showReport'] == 'yes'){
	if(isset($_POST['division_id']) && $_POST['division_id'] != 'all' && $_POST['division_id'] != ''){
		$division = " AND tdiv.division_id IN (".$_POST['division_id'].") ";
	} else {
		$division = "";
	}

	if(isset($_POST['callType']) && $_POST['callType'] != 'all' && $_POST['callType'] != ''){
		$calltype = " AND ta.ref_type='".$_POST['callType']."' ";
	} else {
		$calltype = "";
	}
}

$divisionRec=$_objAdmin->_getSelectList2('table_division',"division_name,division_id",'',$division1." ORDER BY division_name ASC");

if(isset($_POST['showReport']) && $_POST['showReport'] == 'yes') {
	if($_POST['sal']!="" && $_POST['sal']!='All') {
		$sal_id=$_POST['sal'];
		$salesman=" AND s.salesman_id='".$sal_id."'";	
	}
	if($_POST['from']!="") {
		$from_date=$_objAdmin->_changeDate($_POST['from']);	
	}
	if($_POST['to']!="") {
		$to_date=$_objAdmin->_changeDate($_POST['to']);	
	}
} else {
	$from_date= $_objAdmin->_changeDate(date("Y-m-d"));
	$to_date= $_objAdmin->_changeDate(date("Y-m-d"));	
}
if(isset($_REQUEST['reset']) && $_REQUEST['reset'] == 'yes') {
	/* unset($_SESSION['SalAttList']);	
	unset($_SESSION['FromAttList']);	
	unset($_SESSION['ToAttList']); */
	header("Location: salesman_call_log_report.php");
}
if($sal_id!=''){
	$SalName=$_objAdmin->_getSelectList('table_salesman','salesman_name',''," salesman_id='".$sal_id."'"); 
	$sal_name=$SalName[0]->salesman_name;
}
?>
<?php include("header.inc.php") ?>
<script type="text/javascript">

    function PrintElem(elem)
    {
        Popup($(elem).html());
    }

    function Popup(data) 
    {
		
        var mywindow = window.open('', 'Report');
		
        mywindow.document.write('<html><head><title>Salesman Call Log Report</title>');
		mywindow.document.write('<table><tr><td><b>Salesman Name:</b> <?php echo $sal_name; ?></td><td><b>From Date:</b> <?php echo $from_date; ?></td><td><b>To Date:</b> <?php echo $to_date; ?></td></tr></table>');
        /*optional stylesheet*/ //mywindow.document.write('<link rel="stylesheet" href="main.css" type="text/css" />');
        mywindow.document.write('</head><body >');
        mywindow.document.write(data);
        mywindow.document.write('</body></html>');
        mywindow.print();
        mywindow.close();
        return true;
    }

$(document).ready(function()
{
<?php if($_POST['submit']=='Export to Excel'){ ?>
tableToExcel('report_export', 'Salesman Call Log', 'Salesman Call Log.xls');
<?php } ?>
});

</script>
<script src="javascripts/dateNextPrev.js" type="text/javascript"></script>
<!-- start content-outer -->
<input name="pagename" type="hidden"  id="pagename" value="salesman_call_log_report.php" />
<div id="content-outer">
<!-- start content -->
<div id="content">
<div id="page-heading"><h1><span style="color: #d74343; font-family: Tahoma; font-weight: bold;">Salesman Call Log Report</span></h1></div>
<table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">
<tr>
	<td>
	<!--  start content-table-inner -->
	<div id="content-table-inner">
	
	<div id="page-heading" align="left" >
		<form name="frmPre" id="frmPre" method="post" action="" enctype="multipart/form-data" >
			<table border="0" width="100%" cellpadding="0" cellspacing="0">
				<tr>
					<td>
						<h3>Salesman: </h3>
						<h6>
							<select name="sal" id="sal" class="menulist">
								<?php echo $salesmanOptionList = $_objArrayList->GetSalesmenMenu($salsList, $sal_id,'flex');?>
							</select>
						</h6>
					</td>
					<td>
						<h3>Division: </h3>
						<h6>
							<select name="division_id" id="division_id" class="menulist" style="width:150px;">
								<option value="all">All</option>
								<?php 
								foreach($divisionRec as $divisionVal){ ?>
									<option value="<?php echo $divisionVal->division_id; ?>" <?php echo ($_POST['division_id']==$divisionVal->division_id)?"selected":""; ?>><?php echo $divisionVal->division_name; ?></option>
								<?php } ?>
							</select>
						</h6>
					</td>
					<td>
						<h3>Call Type: </h3>
						<h6>
							<select name="callType" id="callType" class="menulist">
								<option value="all" <?php echo ($_POST['callType']=='all')?"selected":""; ?>>All</option>
								<option value="1" <?php echo ($_POST['callType']=='1')?"selected":""; ?>>Incoming</option>
								<option value="2" <?php echo ($_POST['callType']=='2')?"selected":""; ?>>Outgoing</option>
								<option value="3" <?php echo ($_POST['callType']=='3')?"selected":""; ?>>Missed</option>
							</select>
						</h6>
					</td>
					<td>
						<h3>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;From Date: </h3>
						<h6>
							<img src="css/images/prev.png" height="18" width="18" onclick="dateFromPrev();"> 
							<input type="text" id="from" name="from" class="date" style="width:100px" value="<?php  echo $from_date;?>"  readonly /> 
							<img src="css/images/next.png" height="18" width="18" onclick="dateFromNext();">
						</h6>
					</td>		
					<td>
						<h3>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;To Date: </h3>
						<h6>
							<img src="css/images/prev.png" height="18" width="18" onclick="dateToPrev();"> 
							<input type="text" id="to" name="to" class="date" style="width:100px" value="<?php echo $to_date; ?>"  readonly /> 
							<img src="css/images/next.png" height="18" width="18" onclick="dateToNext();">
						</h6>
					</td>
					<td>		
						<h3></h3>
						<input name="submit" class="result-submit" type="submit" id="submit" value="View Details" />
						<input type="button" value="Reset!" class="form-reset" onclick="location.href='salesman_call_log_report.php?reset=yes';" />
					</td>
					<td colspan="2"></td>
				</tr>
				<tr>
					<td colspan="6"><input name="showReport" type="hidden" value="yes" />
						<a id="dlink"  style="display:none;"></a>
						<input  type="submit" value="Export to Excel" name="submit" class="result-submit"  >
						<input type="button" value="Print" class="result-submit" onclick="PrintElem('#Report')" />
					</td>
				</tr>
			</table>
		</form>
	</div>
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
	<tr valign="top">
		<td>
			<div id="Report">
				<table  border="0" width="100%" cellpadding="0" cellspacing="0" id="report_export" name="report_export" >
			    	<tr bgcolor="#6E6E6E" style="color: #fff;font-weight: bold;" >
						<td style="padding:10px;">Saleman&nbsp;Name</td>
						<td style="padding:10px;">Salesman&nbsp;Code</td>
						<td style="padding:10px;">Division</td>
						<td style="padding:10px;">Call&nbsp;Type</td>
						<td style="padding:10px;">To&nbsp;Name</td>
						<td style="padding:10px;">To&nbsp;Number</td>
						<td style="padding:10px;">Date</td>
						<td style="padding:10px;">Start&nbsp;Time</td>
						<td style="padding:10px;">End&nbsp;Time</td>
						<td style="padding:10px;">Duration(In Seconds)</td>
					</tr>
			<?php
			$no_ret = array();
			$auRet=$_objAdmin->_getSelectList2('table_activity as ta 
				LEFT JOIN table_salesman as s ON ta.salesman_id=s.salesman_id 
				LEFT JOIN table_division as tdiv ON s.division_id=tdiv.division_id 
				LEFT JOIN table_call_log as tc ON ta.ref_id=tc.call_id',"tc.*,s.salesman_name,s.salesman_code,tdiv.division_name",''," ta.activity_type IN (31) $salesman $division $calltype AND (tc.app_date BETWEEN '".date('Y-m-d', strtotime($from_date))."' AND '".date('Y-m-d', strtotime($to_date))."') ORDER BY tc.app_date desc");
			if(is_array($auRet)){
				for($i=0;$i<count($auRet);$i++)
				{
					if($auRet[$i]->calltype=='1'){
						$calltype = "Incoming";
					}else if($auRet[$i]->calltype=='2'){
						$calltype = "Outgoing";
					}else{
						$calltype = "Missed";
					}
				$no_ret[] = $auRet[$i]->call_id;
				?>
				<tr  style="border-bottom:2px solid #6E6E6E;" >
					<td style="padding:10px;" width="10%"><?php echo $auRet[$i]->salesman_name;?> </td>
					<td style="padding:10px;" width="10%"><?php echo $auRet[$i]->salesman_code;?> </td>
					<td style="padding:10px;" width="10%"><?php echo $auRet[$i]->division_name;?> </td>
					<td style="padding:10px;" width="10%"><?php echo $calltype;?> </td>
					<td style="padding:10px;" width="10%"><?php echo ($auRet[$i]->name_to=='Unkown')?"Unknown":$auRet[$i]->name_to;?> </td>
					<td style="padding:10px;" width="10%"><?php echo $auRet[$i]->number_to;?> </td>
					<td style="padding:10px;" width="10%"><?php echo $_objAdmin->_changeDate($auRet[$i]->app_date);?> </td>
					<td style="padding:10px;" width="10%"><?php echo $auRet[$i]->app_call_start_time;?> </td>
					<td style="padding:10px;" width="10%"><?php echo $auRet[$i]->app_call_end_time;?> </td>
					<td style="padding:10px;" width="10%"><?php echo $auRet[$i]->duration;?> </td>
				</tr>
				<?php
				}
				?>
				<tr  >
					<td style="padding:10px;" colspan="9"><b>Total Number of Calls:</b> <?php echo count($no_ret) ?></td>
				</tr>
			<?php
			} else {
			?>
			<tr  style="border-bottom:2px solid #6E6E6E;" align="center" >
				<td style="padding:10px;" colspan="11">Report Not Available</td>
			</tr>
			<?php
			}
			?>
		</table>
		</div>
		</td>
	</tr>
	<tr>
		<td><img src="images/blank.gif" width="695" height="1" alt="blank" /></td>
		<td></td>
	</tr>
	</table>
<div class="clear"></div>
</div>
<!--  end content-table-inner  -->
</td>
<td id="tbl-border-right"></td>
</tr>
<!--<tr>
	<th class="sized bottomleft"></th>
	<td id="tbl-border-bottom">&nbsp;</td>
	<th class="sized bottomright"></th>
</tr>-->
</table>
<div class="clear">&nbsp;</div>
</div>
<!--  end content -->
<div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->
<div class="clear">&nbsp;</div> 
<!-- start footer -->         
<?php include("footer.php") ?>
<?php unset($_SESSION['SalAttList']); ?>
<!-- end footer -->
<script src="javascripts/jquery-1.8.2.js" type="text/javascript"></script>
<script src="javascripts/jquery-ui.js"></script>
<script type="text/javascript" src="javascripts/validate.js"></script>
<script type="text/javascript">
// Popup window code
function newPopup(url) {
	popupWindow = window.open(
		url,'popUpWindow','height=500,width=600,left=200,top=100,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no,status=yes')
}
</script>
<script>
    $(function() {
        $( "#from" ).datepicker({
			dateFormat: "d M yy",
            defaultDate: "w",
            changeMonth: true,
            numberOfMonths: 1,
            onSelect: function( selectedDate ) {
                $( "#to" ).datepicker( "option", "minDate", selectedDate );
            }
        });
        $( "#to" ).datepicker({
			dateFormat: "d M yy",
            defaultDate: "-w",
            changeMonth: true,
            numberOfMonths: 1,
            onSelect: function( selectedDate ) {
                $( "#from" ).datepicker( "option", "maxDate", selectedDate );
            }
        });
    });
</script>
<script type='text/javascript'>//<![CDATA[ 
var tableToExcel = (function () {
        var uri = 'data:application/vnd.ms-excel;base64,'
        , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table><tr><td><b>Salesman Name:</b> <?php echo $sal_name; ?></td><td><b>From Date:</b> <?php echo $_objAdmin->_changeDate($from_date); ?></td><td><b>To Date:</b> <?php echo $_objAdmin->_changeDate($to_date); ?></td></tr></table><table>{table}</table></body></html>'
        , base64 = function (s) { return window.btoa(unescape(encodeURIComponent(s))) }
        , format = function (s, c) { return s.replace(/{(\w+)}/g, function (m, p) { return c[p]; }) }
        return function (table, name, filename) {
            if (!table.nodeType) table = document.getElementById(table)
            var ctx = { worksheet: name || 'Worksheet', table: table.innerHTML }

            document.getElementById("dlink").href = uri + base64(format(template, ctx));
            document.getElementById("dlink").download = filename;
            document.getElementById("dlink").click();

        }
    })()

//]]>  
</script>
</body>
</html>
