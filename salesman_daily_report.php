<?php include("includes/config.inc.php");
      include("includes/function.php");
	  $_objAdmin = new Admin();
	  
	  
	  if(isset($_POST['showOrderlist']) && $_POST['showOrderlist'] == 'yes')
	  {
		
		if($_POST['sal']!="") 
		{
			$_SESSION['salesmenID']=$_POST['sal'];	
		}

		if($_POST['month']!="" && is_numeric($_POST['month'])) 
		{
			$_SESSION['months']=$_POST['month'];	
		}
		if($_POST['year']!="") 
		{
			$_SESSION['Curyear']=$_POST['year'];
			$_SESSION['lastyear'] = $_SESSION['Curyear'] - 1;	
		}
	

	
		if($_POST['sal']=='all') 
		{
		  unset($_SESSION['salesmenID']);	
		}
		

	
			header("Location: salesman_total_sale.php");
	} 
	
	
	
	if($_SESSION['Curyear']==''){
		$_SESSION['Curyear'] = date('Y');
		$_SESSION['lastyear'] = date("Y",strtotime("-1 year"));
	}
	
	if($_SESSION['lastyear']==''){
		$_SESSION['lastyear'] = $_SESSION['Curyear'] - 1;
	} 
	
	
	if($_SESSION['months']==''){
		$_SESSION['months'] = date('m');
	} 

	if(isset($_REQUEST['reset']) && $_REQUEST['reset'] == 'yes')
	{
		 unset($_SESSION['salesmenID']);	 	
		 $_SESSION['months']= '';
		 $_SESSION['Curyear']= '';
		 $_SESSION['lastyear'] = '';
		 header("Location: salesman_total_sale.php");
	}

	 //echo "<pre>";	
	 //print_r($_SESSION);		
			
?>


<?php include("header.inc.php") ?>
<style type="text/css">
.menulist {
	border: solid 1px #BDBDBD; 
	color: #393939;
	font-family: Arial;font-size: 12px;
	border-radius:5px ;
	-moz-border-radius:5px;
	-o-border-radius:5px;
	-ms-border-radius:5px;
	-webkit-border-radius:5px;
	width: 150px;
	height: 30px;
}

.flexigrid div.bDiv table {
	margin-bottom: 0px;
}
</style>
<?php //echo "<pre>"; print_r($_SESSION);?>
<!-- start content-outer -->
<input name="pagename" type="hidden"  id="pagename" value="salesman_total_sale.php" />
<div id="content-outer">
<!-- start content -->
<div id="content">
<div id="page-heading"><h1>
	<span style="color: #d74343; font-family: Tahoma; font-weight: bold;"><?php echo $title;?></span></h1></div>
	<table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">
	<tr>
		<th rowspan="3" class="sized"><img src="images/side_shadowleft.jpg" width="20" height="300" alt="" /></th>
		<th class="topleft"></th>
		<td id="tbl-border-top">&nbsp;</td>
		<th class="topright"></th>
		<th rowspan="3" class="sized"><img src="images/side_shadowright.jpg" width="20" height="300" alt="" /></th>
	</tr>
	
	<tr>
		<!--<td id="tbl-border-left"></td>-->
		<td>
		<!--  start content-table-inner -->
	<div id="content-table-inner">
	
	<div id="page-heading" align="left" >
	<?php //echo "<pre>";
		 // print_r($_SESSION);
	?>
	<form name="frmPre" id="frmPre" method="post" action="" enctype="multipart/form-data" >

		<table border="0" width="85%" cellpadding="0" cellspacing="0">
	
			<tr>
			
				
				<td id="salesmenMenu">
				<h3>Salesmen: 
					<select name="sal" id="sal" class="menulist">
						<option value="all">All</option>
			<?php  if(isset($_SESSION['userLoginType']) && in_array($_SESSION['userLoginType'],array(6,7,8,9))){
					$condwhere = $_objAdmin->getSalesMenID();}
				   $aSal=$_objAdmin->_getSelectList('table_salesman AS s','*',''," s.account_id='".$_SESSION['accountId']."' ".$condwhere." ORDER BY s.salesman_name"); 
								if(is_array($aSal))
								{
									foreach($aSal as $key=>$value):?>
									<option value="<?php echo $value->salesman_id;?>" <?php if($_SESSION['salesmenID'] == $value->salesman_id){ ?> selected="selected" <?php } ?>><?php echo $value->salesman_name;?></option>
									<?php endforeach;?>
						  <?php } ?>
						 		
					</select>
				</h3>
				</td>
	
	
				
					<td><h3>Month:
					<?php // Generate Options 
							$thisYear = date('Y');?>
					<select name="month" class="menulist">
						
						
					<option>Select Month</option>
					<?php foreach($ARR_MONTHS AS $key=>$value):?>
						<option value="<?php echo $key;?>" <?php if($key==$_SESSION['months']) echo "selected";?> >
						<?php echo $value?></option>
						<?php endforeach; ?>
					</select>
						</h3></td>
						
					<td><h3>Year:
						<?php // Generate Options 
						    //print_r(range(date('Y'), 2013));
							$thisYear = range(date('Y'), 2013);?>
						<select name="year" class="menulist">
							<option>Please Year</option>
								<?php foreach($thisYear AS $value):?>
									<option value="<?php echo $value;?>" <?php if($value==$_SESSION['Curyear']) echo "selected";?> >
									<?php echo $value?></option>
								<?php endforeach;?>
						</select>
						
						
						</h3></td>
			
				
				<td>
					<input name="showOrderlist" type="hidden" value="yes" />
					<input name="submit" class="result-submit" type="submit" id="submit" value="Show Order List" />
						
				</td>
				
				<td><input type="button" value="Reset!" class="form-reset" 
							onclick="location.href='salesman_total_sale.php?reset=yes';" />
							
				</td>
				<td>
					<a id="dlink"  style="display:none;"></a>
		<input input type="button" value="Export to Excel" class="form-reset"  onclick="tableToExcel('report_export', 'Boyswise Sale Report', 'boyswisesale.xls')">
				</td>
			</tr>
		</table>
	</form>
	</div>
	
	
	<table border="0" width="100%" cellpadding="0" cellspacing="0" id="report_export">
	<tr valign="top">
	<td>
		<!-- start id-form -->
		
		<?php 
			if($_SESSION['salesmenID']!=''){
			$salesman=" AND O.salesman_id ='".$_SESSION['salesmenID']."'";
			}
			
			if($_SESSION['months']!=''){
				$smonth=" AND DATE_FORMAT(O.date_of_order, '%m') = '".$_SESSION['months']."'";
			} 
			
			if($_SESSION['Curyear']!=''){
				$syear="  AND DATE_FORMAT(O.date_of_order, '%Y')<= '".$_SESSION['Curyear']."'";
			} 
			
			if($_SESSION['lastyear']!=''){
				$slastyear="  AND DATE_FORMAT(O.date_of_order, '%Y')>= '".$_SESSION['lastyear']."'";
			} 
			
		
			$where = " 1 $salesman";
			
			$auRec=$_objAdmin->_getSelectList('table_order AS O
			LEFT JOIN table_salesman S ON O.salesman_id = S.salesman_id 
			LEFT JOIN city C ON S.city = C.city_id
			',"DISTINCT(O.salesman_id), S.salesman_name, city_name",'',$where,'');
			/*echo "<pre>";
			print_r($auRec);*/
		?>
		<style>
			.tblclass td{ 
			font-family: Arial, Helvetica, sans-serif;
			font-size: 13px;
			position: relative;
			border: 0px solid #eee;
			overflow: hidden;
			color: #000;
			font-weight:normal;
			
			border-left: 1px solid #ededed;
			border-bottom: 1px solid #ededed;

		}
		</style>
		<div style="overflow:scroll; width:1250px;">
		<table width="100%" class="tblclass">
		 <tr style="background: url(css/images/wbg.gif) repeat-x 0 -1px;">
			<td style="padding:10px;" align="left"><strong>S.No</strong></th>
			<td style="padding:10px;" ><strong>Boys Name</strong></th>
			<td style="padding:10px;" ><strong>Area</strong></th>
			<td style="padding:10px;" ><strong>Day 1</strong></th>
			<td style="padding:10px;" ><strong>Day 2</strong></th>
			<td style="padding:10px;" ><strong>Day 3</strong></th>
			<td style="padding:10px;" ><strong>Day 4</strong></th>
			<td style="padding:10px;" ><strong>Day 5</strong></th>
			<td style="padding:10px;" ><strong>Day 6</strong></th>
			<td style="padding:10px;" ><strong>Day 7</strong></th>
			<td style="padding:10px;" ><strong>Day 8</strong></th>
			<td style="padding:10px;" ><strong>Day 9</strong></th>
			<td style="padding:10px;" ><strong>Day 10</strong></th>
			<td style="padding:10px;" ><strong>Day 11</strong></th>
			<td style="padding:10px;" ><strong>Day 12</strong></th>
			<td style="padding:10px;" ><strong>Day 13</strong></th>
			<td style="padding:10px;" ><strong>Day 14</strong></th>
			<td style="padding:10px;" ><strong>Day 15</strong></th>
			<td style="padding:10px;" ><strong>Day 16</strong></th>
			<td style="padding:10px;" ><strong>Day 17</strong></th>
			<td style="padding:10px;" ><strong>Day 18</strong></th>
			<td style="padding:10px;" ><strong>Day 19</strong></th>
			<td style="padding:10px;" ><strong>Day 20</strong></th>
			<td style="padding:10px;" ><strong>Day 21</strong></th>
			<td style="padding:10px;" ><strong>Day 22</strong></th>
			<td style="padding:10px;" ><strong>Day 23</strong></th>
			<td style="padding:10px;" ><strong>Day 24</strong></th>
			<td style="padding:10px;" ><strong>Day 25</strong></th>
			<td style="padding:10px;" ><strong>Day 26</strong></th>
			<td style="padding:10px;" ><strong>Day 27</strong></th>
			<td style="padding:10px;" ><strong>Day 28</strong></th>
			<td style="padding:10px;" ><strong>Day 29</strong></th>
			<td style="padding:10px;" ><strong>Day 30</strong></th>
			<td style="padding:10px;" ><strong>Day 31</strong></th>
			<td style="padding:10px;" ><strong>Month Total</strong></th>
		  </tr>
		  <tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>
				<table width="100%"  class="tblclass">
				  <tr>
					<td style="padding:10px;" ><strong><?php echo $_SESSION['lastyear'];?></strong></th>
					<td style="padding:10px;" ><strong><?php echo $_SESSION['Curyear'];?></strong></th>
				  </tr>
				</table>
			</td>
			<td>
				<table width="100%"  class="tblclass">
					   <tr>
					<td style="padding:10px;" ><strong><?php echo $_SESSION['lastyear'];?></strong></th>
					<td style="padding:10px;" ><strong><?php echo $_SESSION['Curyear'];?></strong></th>
				  </tr>
				</table>
			</td>
			<td>
				<table width="100%"  class="tblclass">
				   <tr>
					<td style="padding:10px;" ><strong><?php echo $_SESSION['lastyear'];?></strong></th>
					<td style="padding:10px;" ><strong><?php echo $_SESSION['Curyear'];?></strong></th>
				  </tr>
				</table>
			</td>
			<td>
				<table width="100%"  class="tblclass">
				   <tr>
					<td style="padding:10px;" ><strong><?php echo $_SESSION['lastyear'];?></strong></th>
					<td style="padding:10px;" ><strong><?php echo $_SESSION['Curyear'];?></strong></th>
				  </tr>
				</table>
			</td>
			<td>
				<table width="100%"  class="tblclass">
				  <tr>
					<td style="padding:10px;" ><strong><?php echo $_SESSION['lastyear'];?></strong></th>
					<td style="padding:10px;" ><strong><?php echo $_SESSION['Curyear'];?></strong></th>
				  </tr>
				</table>
			</td>
		  </tr>
		  
		  
		  
		  <?php 		
			if(sizeof($auRec)>0) // Check Count
			{
				foreach($auRec as $key=>$value):?>
				<tr>
					<td style="padding:10px;" ><?php echo $key+1;?></td>
					<td style="padding:10px;" ><?php echo $value->salesman_name;?></td>
					<td style="padding:10px;" ><?php echo $value->city_name;?></td>
					
					<?php 
						$startDay = array('01','02','03','04','05','06','07','08','09','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27','28','29','30','31');
						$endDay = array('01','02','03','04','05','06','07','08','09','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27','28','29','30','31');
						$yearTotalSale = 0;
						$lastyearTotalSale = 0;
						//$smonth = 0;
						//$syear = 0;
						//$slastyear = 0;
						
						for($i=0; $i<31; $i++){	
						
						
						
						$startDayFrom = ' AND DATE_FORMAT(O.date_of_order, "%d")>= '.$startDay[$i];
						$endDayFrom = ' AND DATE_FORMAT(O.date_of_order, "%d")<= '.$endDay[$i];
						
						$result = $_objAdmin->_getSelectList('table_order AS O 
						LEFT JOIN table_order_detail AS D ON O.order_id = D.order_id',
						'SUM(quantity) As total, DATE_FORMAT(O.date_of_order, "%Y") As year ','','  O.salesman_id = '.$value->salesman_id.$slastyear.$syear.$smonth.$startDayFrom.$endDayFrom.' GROUP BY DATE_FORMAT(O.date_of_order, "%Y") ORDER BY DATE_FORMAT(O.date_of_order, "%Y") ASC','');
						

						//echo "<pre>";
						//print_r($result);
						$ltotal = 0;
						$total = 0;
						if(count($result)==2){
							$ltotal = $result[0]->total;
							$total = $result[1]->total;
							$yearTotalSale = $yearTotalSale + $result[1]->total;
							$lastyearTotalSale = $lastyearTotalSale + $result[0]->total;
						
						} else if(count($result) == 1) {
						
							if($result[0]->year == $_SESSION['lastyear']) 
							{ 	
									$ltotal = $result[0]->total;
								    $lastyearTotalSale = $lastyearTotalSale + $result[0]->total;									
									
								 } else {
								    $total = $result[0]->total; 
									$yearTotalSale = $yearTotalSale + $result[0]->total;
								}
						} else {
							
							$ltotal = 0;
							$total = 0;
						
						}?>
						
						
			<td>
				<table width="100%"  class="tblclass">
					<tr>							
							
						<td style="padding:10px;"><?php if(isset($ltotal)){ echo $ltotal; } else { $ltotal = 0; };?></td>
						<td style="padding:10px;" align="right;"><?php if(isset($total)){ echo $total; } else { $total = 0; };?>
						</td>
						  </tr>
						</table>
					</td><?php } //echo "<pre>";
					//print_r($result); ?>
					
			<td>
				<table width="100%"  class="tblclass">
					<tr>
						<td style="padding:10px;"><?php echo $lastyearTotalSale;?></th>
						<td style="padding:10px;"><?php echo $yearTotalSale;?></th>
						  </tr>
						</table>
					</td>					
					
					
				  </tr>
				<?php 
					
						
				endforeach;
				
			} // Check Count Closed?>
			
		</table>
		</div>
		<div class="flexigrid" style="width: 950px;">
		<div class="pDiv">
    <div class="pDiv2">
      <div class="pGroup">
        <span class="pPageStat">
        </span>
      </div>
    </div>
    <div style="clear:both">
    </div>
  </div>
  </div>
	<!-- end id-form  -->
	</td>
</tr>

	<tr>
		<td><img src="images/blank.gif" width="695" height="1" alt="blank" /></td>
		<td></td>
	</tr>
</table>

<div class="clear"></div>
</div>
<!--  end content-table-inner  -->
</td>
<td id="tbl-border-right"></td>
</tr>
<!--<tr>
	<th class="sized bottomleft"></th>
	<td id="tbl-border-bottom">&nbsp;</td>
	<th class="sized bottomright"></th>
</tr>-->
</table>
<div class="clear">&nbsp;</div>
</div>
<!--  end content -->
<div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->
<div class="clear">&nbsp;</div> 
<!-- start footer -->         
<?php include("footer.php") ?>
<!-- end footer -->
<script src="javascripts/jquery-1.8.2.js" type="text/javascript"></script>
<script src="javascripts/jquery-ui.js"></script>
<script type="text/javascript" src="javascripts/validate.js"></script>
<script type='text/javascript'>//<![CDATA[ 
var tableToExcel = (function () {
        var uri = 'data:application/vnd.ms-excel;base64,'
        , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table><tr><td><b>From Date:</b> <?php echo $from_date; ?></td><td><b>To Date:</b> <?php echo $to_date; ?></td></tr></table><table>{table}</table></body></html>'
        , base64 = function (s) { return window.btoa(unescape(encodeURIComponent(s))) }
        , format = function (s, c) { return s.replace(/{(\w+)}/g, function (m, p) { return c[p]; }) }
        return function (table, name, filename) {
            if (!table.nodeType) table = document.getElementById(table)
            var ctx = { worksheet: name || 'Worksheet', table: table.innerHTML }

            document.getElementById("dlink").href = uri + base64(format(template, ctx));
            document.getElementById("dlink").download = filename;
            document.getElementById("dlink").click();

        }
    })()

//]]>  
</script>
</body>
</html>