<?php
include("includes/config.inc.php");
include("includes/function.php");
include("includes/globalarraylist.php");

include("header.inc.php");
if(isset($_POST['add']) && $_POST['add'] == 'yes')
{
$from_date=$_POST['from'];
$to_date=$_POST['to'];
} else {
$from_date=date("Y-m-d",strtotime("-0 day"));
$to_date=date("Y-m-d",strtotime("-0 day"));
}
if(isset($_REQUEST['sal']) && $_REQUEST['sal']!="" )
{
	$_SESSION['Mapsal']=base64_decode($_REQUEST['sal']);
	$_SESSION['Fromdate']=$from_date;
	$_SESSION['Todate']=$to_date;
	header("Location: salesman_distance_report.php");
}

$report_day = date('D', strtotime($report_date));
include("sal_distance_map.php");?>
 
<script>
    $(function() {
        $( "#from" ).datepicker({
			dateFormat: "d M yy",
            defaultDate: "1w",
            changeMonth: true,
            numberOfMonths: 1,
            onSelect: function( selectedDate ) {
                $( "#to" ).datepicker( "option", "minDate", selectedDate );
            }
        });
        $( "#to" ).datepicker({
			dateFormat: "d M yy",
            defaultDate: "-1w",
            changeMonth: true,
            numberOfMonths: 1,
            onSelect: function( selectedDate ) {
                $( "#from" ).datepicker( "option", "maxDate", selectedDate );
            }
        });
    });
	$(document).ready(function(){
	    $("#sal").change(function(){
		 document.report.submit();
		})
	});
</script>
<script src="javascripts/jquery-1.8.2.js" type="text/javascript"></script>
<script src="javascripts/jquery-ui.js"></script>
<script type="text/javascript" src="javascripts/validate.js"></script>


<!-- start content-outer -->
<div id="content-outer">
<!-- start content -->
<div id="content">
<div id="page-heading"><h1><span style="color: #d74343; font-weight: bold;">Total Distance Report</span></h1> 
	<form name="report" id="report" action="salesman_distance_report.php" method="post">
	<div id="page-heading" align="left" style="padding-left: 350px;"><h3><span style=" color:#000000;">Salesman: 
	
	<select name="sal" id="sal" class="menulist" style="width:150px" >
		<option value="" >Select</option>
		<?php $aSal=$_objAdmin->_getSelectList('table_salesman AS s','*',''," s.status = 'A' $salesman ORDER BY salesman_name"); 
		if(is_array($aSal)){
		for($i=0;$i<count($aSal);$i++){?>
		<option value="<?php echo base64_encode($aSal[$i]->salesman_id);?>" <?php if ($aSal[$i]->salesman_id==$_SESSION['Mapsal']){ ?> selected <?php } ?>><?php echo $aSal[$i]->salesman_name;?></option>
		<?php } }?>
	</select>
	</span>
	</h3>
	</div>
	</form>
	
</div>

<table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">
<tr>
	<!--<td id="tbl-border-left"></td>-->
	<td>
	<!--  start content-table-inner -->
	<div id="content-table-inner">
	
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
	
	<tr valign="top">
	<td>
	<?php
	$auSal=$_objAdmin->_getSelectList('table_salesman',"salesman_name,salesman_id",''," salesman_id='".$_SESSION['Mapsal']."' ");
	if(is_array($auSal)){
	?>
	<form name="frmPre" id="frmPre" method="post" action="salesman_distance_report.php?sal=<?php echo base64_encode($auSal[0]->salesman_id);?>" enctype="multipart/form-data" >
	<div id="page-heading" align="center" ><h3>Salesman: <span style=" color:#000000;"><?php echo $auSal[0]->salesman_name;?>,</span>From Date: <input type="text" id="from" name="from" class="date" value="<?php echo $_objAdmin->_changeDate($_SESSION['Fromdate']);?>"  readonly /> To Date: <input type="text" id="to" name="to" class="date" value="<?php echo $_objAdmin->_changeDate($_SESSION['Todate']);?>"  readonly /><input name="add" type="hidden" value="yes" /><input name="submit" class="result-submit" type="submit" id="submit" value="View Details" /></h3>
	
	<h4><?php/* $i=0; foreach ($result2 as $key1 => $value1) {?>Distance Travelled on <?php echo $value1->activity_date.":".round($sum[$i],2)." km"; echo "<br/>"; $i++; }*/?></h4></div>
	<?php } ?>
	</form>
	</td>
	<?php 
	if($_SESSION['Mapsal']==""){
	?>
	<tr valign="top">
	<td>
	<table border="0" width="100%" cellpadding="0" cellspacing="0" id="id-form">
			<tr bgcolor="#A52A2A" style="color: #fff;font-weight: bold;">
				<td align="center" style="padding:10px;">Select Sales Team</td>
			</tr>
		</table>
	</td>
	<?php } else { ?>
	<tr valign="top">
	<td>
		<!-- start id-form -->
		<div style="height:500px;overflow:auto;" >
		<table border="0" width="100%" cellpadding="0" cellspacing="0" id="id-form">
			<?php  
			if($err!=''){
			?>
			<tr bgcolor="#A52A2A" style="color: #fff;font-weight: bold;">
				<td align="center" style="padding:10px;"><?php echo $err; ?></td>
			</tr>
			<?php } else { ?>

				<div id="Report">
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
	<tr valign="top">
		<td>
		
		<table  border="0" width="100%" cellpadding="0" cellspacing="0" id="report_export" name="report_export">
			<div id="Report">
			<tr bgcolor="#6E6E6E" style="color: #fff;font-weight: bold;" >
				
				<td style="padding:10px;" width="30%">Date</td>
				<td style="padding:10px;" width="30%">Distance Travelled</td>
				
				
				
			</tr>
			<?php $i=0; foreach ($result2 as $key1 => $value1) {?>
			
				<tr style="border:1px solid #CAC5C5;">
				<td style="padding:10px;" width="30%"><a href="salesman_activity_distance_report.php?sal=<?php echo $_SESSION['Mapsal']; ?>&activity_date=<?php echo $value1->activity_date; ?>"><?php $activity_date=date("j M Y", strtotime($value1->activity_date)); echo $activity_date; ?></a></td>				
				<td style="padding:10px;" width="30%"><?php echo round($sum[$i],2)." km" ; ?></td>
			<?php $i++; } ?>
			<tr><td style="padding:10px;font-weight:bold; font-size:16px;" width="30%">Total Distance Travelled</td>				
				<td style="padding:10px;font-weight:bold; font-size:16px;" width="30%"><?php echo round($totalsum,2)." km" ; ?></td></tr>
			</div>
			</table>
			</td>
			</tr>
			</table>
			<!--<tr>	
				<div id="map_canvas"></div>
			</tr>-->
			<?php } ?>
		</table>
		</div>
		<!-- end id-form  -->
	</td>
	<?php } ?>
	<td>
	<!-- right bar-->
	<?php //include("rightbar/salesmanMapList_bar.php") ?>
	</td>
	</tr>
<tr>
<td><img src="images/blank.gif" width="695" height="1" alt="blank" /></td>
<td></td>
</tr>
</table>

<!--  end content-table-inner  -->
</td>
<td id="tbl-border-right"></td>
</tr>
<!--<tr>
	<th class="sized bottomleft"></th>
	<td id="tbl-border-bottom">&nbsp;</td>
	<th class="sized bottomright"></th>
</tr>-->
</table>
<div class="clear">&nbsp;</div>
</div>
<!--  end content -->
<div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->
<div class="clear">&nbsp;</div> 
<!-- start footer -->         
<?php include("footer.php");?>
<!-- end footer -->
 
</body>
</html>