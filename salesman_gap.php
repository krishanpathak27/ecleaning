<?php
include("includes/config.inc.php");
include("includes/function.php");
include("includes/globalarraylist.php");

$page_name="Salesman Gap Analysis";


if(isset($_POST['showReport']) && $_POST['showReport'] == 'yes')
{	
	if($_POST['sal']!="") 
	{
	$_SESSION['SalGapList']=$_POST['sal'];	
	}
	if($_POST['from']!="") 
	{
	$_SESSION['FromGapList']=$_objAdmin->_changeDate($_POST['from']);	
	}
	if($_POST['to']!="") 
	{
	$_SESSION['ToGapList']=$_objAdmin->_changeDate($_POST['to']);	
	}
	
	if($_POST['sal']=="") 
	{
	  unset($_SESSION['SalGapList']);	
	}
	if($_POST['gap']!="") 
	{
	$_SESSION['GapTime']=$_POST['gap'];	
	}
} else {
$_SESSION['FromGapList']= $_objAdmin->_changeDate(date("Y-m-d"));
$_SESSION['ToGapList']= $_objAdmin->_changeDate(date("Y-m-d"));	
$_SESSION['GapTime']="60";	
}
if(isset($_REQUEST['reset']) && $_REQUEST['reset'] == 'yes')
{
	unset($_SESSION['SalGapList']);
	unset($_SESSION['GapTime']);
	header("Location: salesman_gap.php");
}

if($_SESSION['SalGapList']!=''){
$SalName=$_objAdmin->_getSelectList('table_salesman','salesman_name',''," salesman_id='".$_SESSION['SalGapList']."'"); 
$sal_name=$SalName[0]->salesman_name;
} else {
$sal_name="All Salesman";
}

	
function convertToHoursMins($time, $format = '%d:%d') {
    settype($time, 'integer');
    if ($time < 1) {
        return;
    }
    $hours = floor($time/60);
    $minutes = $time%60;
    return sprintf($format, $hours, $minutes);
}

if($_SESSION['userLoginType']==2) {
$opRec=$_objAdmin->_getSelectList('table_account_admin',"*",''," operator_id=".$_SESSION['operatorId']);
$opRec[0]->manage_items;
}
?>

<?php include("header.inc.php") ?>
<script src="javascripts/dateNextPrev.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function(){
	    $("#sal").change(function(){
		 document.report.submit();
		})
	});
</script>
<script type="text/javascript">

    function PrintElem(elem)
    {
        Popup($(elem).html());
    }

    function Popup(data) 
    {
		
        var mywindow = window.open('', 'Report');
		
        mywindow.document.write('<html><head><title>Gap Analysis</title>');
		mywindow.document.write('<table><tr><td><b>Salesman Name:</b> <?php echo $sal_name; ?></td><td><b>From Date:</b> <?php echo $_SESSION['FromAttList']; ?></td><td><b>To Date:</b> <?php echo $_SESSION['ToAttList']; ?></td></tr></table>');
        /*optional stylesheet*/ //mywindow.document.write('<link rel="stylesheet" href="main.css" type="text/css" />');
        mywindow.document.write('</head><body >');
        mywindow.document.write(data);
        mywindow.document.write('</body></html>');

        mywindow.print();
        mywindow.close();
        return true;
    }
$(document).ready(function()
{
<?php if($_POST['submit']=='Export to Excel'){ ?>
tableToExcel('report_export', 'Salesman Gap Analysis Report', 'Salesman Gap Analysis Report.xls');
<?php } ?>
});

</script>

<!-- start content-outer -->

<div id="content-outer">

<!-- start content -->
<div id="content">
<div id="page-heading"><h1><span style="color: #d74343; font-family: Tahoma; font-weight: bold;">Salesman Gap Analysis</span></h1></div>
<table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">

<tr>
	<!--<td id="tbl-border-left"></td>-->
	<td>
	<!--  start content-table-inner -->
	<div id="content-table-inner">
	
	<div id="page-heading" align="left" >
	<form name="frmPre" id="frmPre" method="post" action="" enctype="multipart/form-data" >
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
	
	<tr>
		<td>
			<h3>Salesman Name: </h3><h6>
			<select name="sal" id="sal" class="styledselect_form_5">
				<?php echo $rtndata = $_objArrayList->GetSalesmenMenu($salsList, $_SESSION['SalGapList']);?>
			</select>
			</h6>
		</td>
		
		
		
		<td ><h3>Time Gap: </h3>
		<h6><select name="gap" id="gap" class="" style="border: solid 1px #BDBDBD; color: #393939;font-family: Arial;font-size: 12px;border-radius:5px ;-moz-border-radius:5px;-o-border-radius:5px;-ms-border-radius:5px;-webkit-border-radius:5px;width: 100px;height: 30px;" >
		<option value="60" <?php if($_SESSION['GapTime']==60) echo "selected"?>>1 hour</option>
		<option value="30" <?php if($_SESSION['GapTime']==30) echo "selected"?>>30 min</option>
		<option value="15" <?php if($_SESSION['GapTime']==15) echo "selected"?>>15 min</option>
		</select></h6></td>
		<td><h3>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;From Date:</h3><h6><img src="css/images/prev.png" height="18" width="18" onclick="dateFromPrev();"> <input type="text" id="from" name="from" class="date" style="width:100px" value="<?php  echo $_SESSION['FromGapList'];?>"  readonly /> <img src="css/images/next.png" height="18" width="18" onclick="dateFromNext();"> </h6></td>
		<td><h3>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;To Date: </h3> <h6> <img src="css/images/prev.png" height="18" width="18" onclick="dateToPrev();"> <input type="text" id="to" name="to" class="date" style="width:100px" value="<?php echo $_SESSION['ToGapList']; ?>"  readonly /> <img src="css/images/next.png" height="18" width="18" onclick="dateToNext();"></h6></td>
		<td><h3></h3><input name="submit" class="result-submit" type="submit" id="submit" value="View Details" />
		<input type="button" value="Reset!" class="form-reset" onclick="location.href='salesman_gap.php?reset=yes';" /></td>
		<td></td>
		</tr>
		<tr>
		<td colspan="4">
		<input name="showReport" type="hidden" value="yes" />
		<input type="button" value="Print" class="result-submit" onclick="PrintElem('#Report')" />
		<a style="margin-top:25px; display:none;" id="dlink"></a>
		<input input type="submit" name="submit" value="Export to Excel" class="result-submit" >
		<!-- <a href='salesman_gap_year_graph.php?y=<?php echo checkFromdate($_SESSION['FromGapList']); ?>&salID=<?php echo $_SESSION['SalGapList'];?>' target="_blank"><input type="button" value=" Show Graph" class="result-submit" /></a> -->
		</td>
	</tr>
	</table>
	</form>
	</div>
	<div id="Report">
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
	<?php
	if($_SESSION['SalGapList']==""){
		if($salsList!=''){
			?>
		<tr valign="top">
	<td>
		<!-- start id-form -->
		<table  border="0" cellspacing="0" cellpadding="0"  align="center" width="100%" id="report_export" name="report_export">
		<tr>
			<td>
			<div>
				<table  border="0" width="100%" cellpadding="0" cellspacing="0" >
				<tr  bgcolor="#6E6E6E" style="color: #fff;font-weight: bold;" >
					<td style="padding:10px;" width="20%">Salesman Name</td>
					<td style="padding:10px;" width="20%">Salesman Code</td>
					<td style="padding:10px;" width="10%">Gap Time</td>
					<?php if ($opRec[0]->view_activity_report=="Yes"){ ?>
					<td style="padding:10px;" width="10%">Timeline</td>
					<?php } ?>
					<td style="padding:10px;" width="10%">From Time</td>
					<td style="padding:10px;" width="10%">To Time</td>
					<td style="padding:10px;" width="10%">Date</td>
					<td style="padding:10px;" width="10%">Day</td>
					<td style="padding:10px;" width="20%">Market</td>
				</tr>
			<?php
			for($a=0;$a<count($salsList);$a++){
				$result=mysql_query("SELECT salesman_id,ref_type,ref_id,activity_date,end_time,activity_id,Sec_to_time(@diff) AS starttime,end_time,IF(@diff = 0, 0,Time_to_sec(end_time) - @diff)/60 AS diff,@diff := Time_to_sec(end_time) FROM table_activity,(SELECT @diff := 0) AS x WHERE salesman_id='".$salsList[$a]."' and activity_date>='".date('Y-m-d', strtotime($_SESSION['FromGapList']))."' and activity_date<='".date('Y-m-d', strtotime($_SESSION['ToGapList']))."' and activity_type in (3,4,5,10,11,12) ORDER  BY table_activity.activity_date asc, table_activity.end_time asc");

					while($row=mysql_fetch_array($result)){ 
						if($row['diff']>$_SESSION['GapTime'] ){ ;	
							?>
						<tr  style="border-bottom:2px solid #6E6E6E;" >
							<?php
							$chick="1";
							$aSalName=$_objAdmin->_getSelectList('table_salesman','salesman_name,salesman_code',''," salesman_id='".$row['salesman_id']."' ");
							?>
							<td style="padding:10px;" width="20%"><?php echo $aSalName[0]->salesman_name;?> </td>
							<td style="padding:10px;" width="20%"><?php echo $aSalName[0]->salesman_code;?> </td>
							<td style="padding:10px;" width="15%">
								<?php if($row['diff']>60){
									echo convertToHoursMins(floor($row['diff']), '%d hours %d minutes');
									//echo floor($row['diff'])." minutes";
								   }
								   else{
									echo floor($row['diff'])." minutes";
									}?></td>
							<?php if ($opRec[0]->view_activity_report=="Yes"){
							$aRec=$_objAdmin->_getSelectList2('table_activity','activity_id',''," end_time='".$row['starttime']."' and activity_type in (3,4,5,10,11,12) and activity_date='".$row['activity_date']."' "); 
							 ?>
							<td style="padding:10px;" width="10%"><a href="salesman_report.php?sal=<?php echo base64_encode($row['salesman_id']);?>&date=<?php echo base64_encode($row['activity_date']); ?>&act=<?php echo base64_encode($row['activity_id']) ?>&act1=<?php echo base64_encode($aRec[0]->activity_id) ?>#<?php echo base64_encode($aRec[0]->activity_id) ?>" target="_blank">View</a></td>
							<?php } ?>
							<td style="padding:10px;" width="10%"><?php echo $row['starttime'];?></td>
							<td style="padding:10px;" width="10%"><?php echo $row['end_time'];?></td>
							<td style="padding:10px;" width="10%"><?php echo $_objAdmin->_changeDate($row['activity_date']);?></td>
							<td style="padding:10px;" width="10%"><?php echo date('l',strtotime($row['activity_date']));?></td>
							<?php if($row['ref_type']==2)
							{ 
							$result1=mysql_query("SELECT r.retailer_location,m.market_name FROM table_retailer as r left join table_market as m on r.market_id=m.market_id WHERE r.retailer_id='".$row['ref_id']."'");
							$row1=mysql_fetch_array($result1);
							$mkt=$row1['market_name'];?>
							<td style="padding:10px;" width="20%"><?php echo $mkt?></td>
							<?php }
							elseif($row['ref_type']==1){

							$result1=mysql_query("SELECT retailer_id FROM table_order WHERE order_id='".$row['ref_id']."'");
							$row1=mysql_fetch_array($result1);
							$retailer_id=$row1['retailer_id'];
							$result2=mysql_query("SELECT r.retailer_location,m.market_name FROM table_retailer as r left join table_market as m on m.market_id=r.market_id WHERE r.retailer_id='".$retailer_id."'");
							$row2=mysql_fetch_array($result2);
							$mkt=$row2['market_name'];?>
							<td style="padding:10px;" width="20%"><?php echo $mkt?></td>
							<?php } else{?>
							<td style="padding:10px;" width="20%">Not Available</td>
							<?php }?>
						</tr>
						<?php } } } ?>
						<?php
						if($chick=="") {
						?>
						<tr  style="border-bottom:2px solid #6E6E6E;" align="center">
						<td style="padding:10px;" colspan="8">Report Not Available</td>
						</tr>
						<?php } ?>
				</table>
			</div>
			</td>
		</tr>
		</table>
	</td>
	</tr>
	<?php	}
	} else {
	
	$result=mysql_query("SELECT salesman_id,ref_type,ref_id,activity_date,activity_id,Sec_to_time(@diff) AS starttime,end_time,IF(@diff = 0, 0,Time_to_sec(end_time) - @diff)/60 AS diff,@diff := Time_to_sec(end_time) FROM table_activity,(SELECT @diff := 0) AS x WHERE salesman_id='".$_SESSION['SalGapList']."' and activity_date>='".date('Y-m-d', strtotime($_SESSION['FromGapList']))."' and activity_date<='".date('Y-m-d', strtotime($_SESSION['ToGapList']))."' and activity_type in (3,4,5,10,11,12) ORDER  BY table_activity.activity_date asc, table_activity.end_time asc");
	?>
	<tr valign="top">
	<td>
		<!-- start id-form -->
		<table  border="0" cellspacing="0" cellpadding="0"  align="center" width="100%" id="report_export" name="report_export">
		<tr>
			<td>
			<div>
				<table  border="0" width="100%" cellpadding="0" cellspacing="0" >
					<tr  bgcolor="#6E6E6E" style="color: #fff;font-weight: bold;" >
						<td style="padding:10px;" width="20%">Gap Time</td>
						<?php if ($opRec[0]->view_activity_report=="Yes"){ ?>
						<td style="padding:10px;" width="10%">Timeline</td>
						<?php } ?>
						<td style="padding:10px;" width="10%">Date</td>
						<td style="padding:10px;" width="10%">From Time</td>
						<td style="padding:10px;" width="10%">To Time</td>
						<td style="padding:10px;" width="10%">Day</td>
						<td style="padding:10px;" width="10%">Market</td>
					</tr>
					<?php
					while($row=mysql_fetch_array($result)){
						if($row['diff']>$_SESSION['GapTime'] ){ ;
						$chick1="1";
					?>
					<tr  style="border-bottom:2px solid #6E6E6E;" >
						<td style="padding:10px;" width="20%">
						<?php if($row['diff']>60){
								echo convertToHoursMins(floor($row['diff']), '%d hours %d minutes');
									//echo floor($row['diff'])." minutes";
							   } else {
									echo floor($row['diff'])." minutes";
							}?>
						</td>
						<?php if ($opRec[0]->view_activity_report=="Yes"){
						$aRec=$_objAdmin->_getSelectList2('table_activity','activity_id',''," end_time='".$row['starttime']."' and activity_type in (3,4,5,10,11,12) and activity_date='".$row['activity_date']."' "); 
						 ?>
						<td style="padding:10px;" width="10%"><a href="salesman_report.php?sal=<?php echo base64_encode($row['salesman_id']);?>&date=<?echo base64_encode($row['activity_date']); ?>&act=<?php echo base64_encode($row['activity_id']) ?>&act1=<?php echo base64_encode($aRec[0]->activity_id) ?>#<?php echo base64_encode($aRec[0]->activity_id) ?>" target="_blank">View</a></td>
						<?php } ?>
						<td style="padding:10px;" width="10%"><?php echo $_objAdmin->_changeDate($row['activity_date']);?></td>
						<td style="padding:10px;" width="10%"><?php echo $row['starttime'];?></td>
						<td style="padding:10px;" width="10%"><?php echo $row['end_time'];?></td>
						<td style="padding:10px;" width="10%"><?php echo date('l',strtotime($row['activity_date']));?></td>
						
						<?php if($row['ref_type']==2)
						{ 
						$result1=mysql_query("SELECT retailer_location FROM table_retailer as r left join table_market as m on r.market_id=m.market_id WHERE r.retailer_id='".$row['ref_id']."'");
						$row1=mysql_fetch_array($result1);
						$mkt=$row1['market_name'];?>
						<td style="padding:10px;" width="20%"><?php echo $mkt?></td>
						<?php }
						elseif($row['ref_type']==1){
						$result1=mysql_query("SELECT retailer_id FROM table_order WHERE order_id='".$row['ref_id']."'");
						$row1=mysql_fetch_array($result1);
						$retailer_id=$row1['retailer_id'];
						$result2=mysql_query("SELECT r.retailer_location,m.market_name FROM table_retailer as r left join table_market as m on r.market_id=m.market_id WHERE r.retailer_id='".$retailer_id."'");
						$row2=mysql_fetch_array($result2);
						$mkt=$row2['market_name'];?>
						<td style="padding:10px;" width="20%"><?php echo $mkt?></td>
						<?php } else{?>
						<td style="padding:10px;" width="20%">Not Available</td>
						<?php }?>
						</tr>
						<?php }}
						if($chick1=="") {
						?>
						<tr  style="border-bottom:2px solid #6E6E6E;" align="center">
						<td style="padding:10px;" colspan="6">Report Not Available</td>
					</tr>
						<?php } ?>
				</table>
				</div>
		</td>
		</tr>
		</table>
	<?php } ?>
	</td>

</tr>
<tr>
<td><img src="images/blank.gif" width="695" height="1" alt="blank" /></td>
<td></td>
</tr>
</table>	
</div>	
<div class="clear"></div>
</div>
<!--  end content-table-inner  -->
</td>
<td id="tbl-border-right"></td>
</tr>
<!--<tr>
	<th class="sized bottomleft"></th>
	<td id="tbl-border-bottom">&nbsp;</td>
	<th class="sized bottomright"></th>
</tr>-->
</table>
<div class="clear">&nbsp;</div>
</div>
<!--  end content -->
<div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->
<div class="clear">&nbsp;</div> 
<!-- start footer -->         
<?php include("footer.php") ?>
<!-- end footer -->
<script src="javascripts/jquery-1.8.2.js" type="text/javascript"></script>
<script src="javascripts/jquery-ui.js"></script>
<script type="text/javascript" src="javascripts/validate.js"></script>
<script type="text/javascript">
// Popup window code
function newPopup(url) {
	popupWindow = window.open(
		url,'popUpWindow','height=500,width=600,left=200,top=100,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no,status=yes')
}
</script>
<script>
    $(function() {
        $( "#from" ).datepicker({
			dateFormat: "d M yy",
            defaultDate: "w",
            changeMonth: true,
            numberOfMonths: 1,
            onSelect: function( selectedDate ) {
                $( "#to" ).datepicker( "option", "minDate", selectedDate );
            }
        });
        $( "#to" ).datepicker({
			dateFormat: "d M yy",
            defaultDate: "-w",
            changeMonth: true,
            numberOfMonths: 1,
            onSelect: function( selectedDate ) {
                $( "#from" ).datepicker( "option", "maxDate", selectedDate );
            }
        });
    });
</script>
<script type='text/javascript'>//<![CDATA[ 
var tableToExcel = (function () {
        var uri = 'data:application/vnd.ms-excel;base64,'
        , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table><tr><td><b>Salesman Name:</b> <?php echo $sal_name; ?></td><td><b>Time Gap:</b> <?php echo $_SESSION['GapTime']; ?></td><td><b>From Date:</b> <?php echo $_SESSION['FromGapList']; ?></td><td><b>To Date:</b> <?php echo $_SESSION['ToGapList']; ?></td></tr></table><table>{table}</table></body></html>'
        , base64 = function (s) { return window.btoa(unescape(encodeURIComponent(s))) }
        , format = function (s, c) { return s.replace(/{(\w+)}/g, function (m, p) { return c[p]; }) }
        return function (table, name, filename) {
            if (!table.nodeType) table = document.getElementById(table)
            var ctx = { worksheet: name || 'Worksheet', table: table.innerHTML }

            document.getElementById("dlink").href = uri + base64(format(template, ctx));
            document.getElementById("dlink").download = filename;
            document.getElementById("dlink").click();

        }
    })()

//]]>  
</script>
</body>
</html>