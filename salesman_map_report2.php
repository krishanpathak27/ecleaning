<?php
include("includes/config.inc.php");
include("includes/function.php");
include("includes/globalarraylist.php");

include("header.inc.php");
if(isset($_POST['add']) && $_POST['add'] == 'yes')
{
$report_date=$_POST['to'];
} else {
$report_date=date("Y-m-d",strtotime("-0 day"));
}
if(isset($_REQUEST['sal']) && $_REQUEST['sal']!="" )
{
	$_SESSION['Mapsal']=base64_decode($_REQUEST['sal']);
	$_SESSION['Mapdate']=$report_date;
	header("Location: salesman_map_report.php");
}

$report_day = date('D', strtotime($report_date));
// include("sal_map.php");
?>

<script src="javascripts/jquery-1.8.2.js" type="text/javascript"></script>
<script src="javascripts/jquery-ui.js"></script>
<script type="text/javascript" src="javascripts/validate.js"></script>


<script type="text/javascript" src="javascripts/custom_map_tooltip.js"></script>
<link rel="stylesheet" href="css/mapcss.css"></link>
<style>
.name_list:hover {background-color: #cfcbcb}

.name_list button {border: none; background: transparent; cursor: pointer}
</style>
<script type='text/javascript'>
    //search from the salesman list
        function mySearch() {
                // Declare variables
                var input, filter, ul, li, a, i;
                input = document.getElementById('searchSL');
                filter = input.value.toUpperCase();
                ul = document.getElementById("mySL");
                li = ul.getElementsByTagName('li');

                // Loop through all list items, and hide those who don't match the search query
                for (i = 0; i < li.length; i++) {
                    a = li[i].getElementsByTagName("button")[0];
                    if (a.innerHTML.toUpperCase().indexOf(filter) > -1) {
                        li[i].style.display = "";
                    } else {
                        li[i].style.display = "none";
                    }
                }
            }

    $(document).ready(function(){
        //load map 
        var markers="";
        var map ="";
        var marker = "";


         navigator.geolocation.getCurrentPosition(function(position){
                    
                    if(position.coords.latitude != "")
                    {
                     pos = {lat: position.coords.latitude, lng: position.coords.longitude};
                    }
                    else
                    {
                        pos = {lat: '28.497109', lng: '77.084651'};
                    }
                    map.panTo(pos);
                });

        var latlng = new google.maps.LatLng(28.497109, 77.084651);
        var myOptions = {
            zoom: 10,
            center: latlng,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            fullscreenControl: true
        };

         map = new google.maps.Map(document.getElementById('sales_map'), myOptions);

          setInterval(getsales_data, 50000);
          // setInterval(getsales_data, 5000);

        $('.map_head').hide();



         $.ajax({
                type: 'GET', 
                url: 'salesman_locations.php',  
                dataType:'json',
                success : function(data) {
                    markers= data;
                    initMap();
                }
             });
       

        

        $('#reset').click(function(e){
            e.preventDefault();
            $('.map_head').hide();
            // initMap();
            var latlng = new google.maps.LatLng(28.497109, 77.084651);
                    var myOptions = {
                        zoom: 10,
                        center: latlng,
                        mapTypeId: google.maps.MapTypeId.ROADMAP
                    };

                     map = new google.maps.Map(document.getElementById('sales_map'), myOptions);

                     input = document.getElementById('searchSL');
                     input.value = "";
                     ul = document.getElementById("mySL");
                     li = ul.getElementsByTagName('li');
                    for (i = 0; i < li.length; i++) {
                        li[i].style.display = "";
                    }
                    
        });


        $('.salesman_list_man').click(function(e){
            e.preventDefault();
            var s_id = $(this).attr('id');
            $.ajax({
                type: 'POST', 
                url: 'salesman_location.php',  
                dataType:'json',
                data: 'id='+s_id,
                success : function(data) {
                    if(data[0] != "")
                    {

                        var pos2 = {lat: parseFloat(data[0].lat), lng: parseFloat(data[0].lng)};
                        var id = data[0].id;
                        var new_name = data[0].name;
                        var report_time_new = data[0].report_time;
                        $('#salesman_name_label').text(new_name);
                        $('#salesman_time_label').text(report_time_new);
                        $('.map_head').show();
                        map.panTo(pos2);
                         map.setZoom(15);


                                marker = new google.maps.Marker({
                                    position: pos2,
                                    name:new_name,
                                    report_time:report_time_new,
                                    map: map
                                }); 
                                var infoWindow = new google.maps.InfoWindow(), marker, lat, lng;

                                 var infoWindow = new google.maps.InfoWindow({
                                     content: "<p style='font-weight:400'>"+new_name +"</p><p style='font-weight:400'>Report Time: "+report_time_new+"</p>"
                                 });
                                 google.maps.event.addListener( marker, 'click', function(e){
                                    var infocontent = "<p style='font-weight:400'>"+this.name +"</p><p style='font-weight:400'>Report Time: "+this.report_time+"</p>";
                                    infoWindow.setContent( infocontent );
                                    infoWindow.open( map, this );
                                }.bind( marker ) );
                                 infoWindow.open(map, marker);
                            
                         //google.maps.event.trigger(marker[id], 'click');
                    }
                    else
                    {
                        alert("Location not found!");
                    }
                    
                    } 
             });
        });
    	 // $(function() {
    	
	    function getsales_data(){

            $.ajax({
                type: 'GET', 
                url: 'salesman_locations.php',  
                dataType:'json',
                success : function(data) {
                    markers= data;
                    initMap();
                    } 
             });

        };
        
	
    function initMap() {
    	var pos = "";
    	 // navigator.geolocation.getCurrentPosition(function(position){
                    
      //               if(position.coords.latitude != "")
      //               {
      //                pos = {lat: position.coords.latitude, lng: position.coords.longitude};
      //               }
      //               else
      //               {
      //               	pos = {lat: '28.497109', lng: '77.084651'};
      //               }
      //               map.panTo(pos);
      //           });

      //   var latlng = new google.maps.LatLng(28.497109, 77.084651);
      //   var myOptions = {
      //       zoom: 10,
      //       center: latlng,
      //       mapTypeId: google.maps.MapTypeId.ROADMAP
      //   };

      //    map = new google.maps.Map(document.getElementById('sales_map'), myOptions);

        var infowindow = new google.maps.InfoWindow(), marker, lat, lng;
        var json=JSON.stringify( markers );
	        	for(var i=0;i<markers.length;i++){

	            lat = markers[i].lat;
	            lng=markers[i].lng;
	            name=markers[i].name;
                var id = markers[i].id;
                report_time=markers[i].report_time;

	            marker = new google.maps.Marker({
	                position: new google.maps.LatLng(lat,lng),
	                name:name,
                    report_time:report_time,
	                map: map
	            }); 

                 // infowindow = new google.maps.InfoWindow({
                 //                    content: "<p>"+name +"</p><br>Report Time: "+report_time
                 //                });
	            google.maps.event.addListener( marker, 'click', function(e){
                    var infocontent = "<p style='font-weight:400'>"+this.name +"</p><p style='font-weight:400'>Report Time: "+this.report_time+"</p>";
	                infowindow.setContent( infocontent );
	                infowindow.open( map, this );
	            }.bind( marker ) );
              // infowindow.open(map, marker);
	        }



    }


    
     });


    
    
    </script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAuAzZRDkMuaLSPt1I-MPNlAOlzbJjbLlU"></script>
<!-- start content-outer -->
<div id="content-outer">
<!-- start content -->
<div id="content">
<div id="page-heading"><h1><span style="color: #d74343; font-weight: bold;">Salesman Map Report</span></h1> 
<div id="page-heading" align="left" style="margin:18px 0 15px 6px">


<div class="mapcover">
<!--map left list portion started from here-->
	<div class="mapcover_left">
    	<div class="sales_head">
        Salesman List
        </div>
        
        <div class="row_list"><!--Search--> 
        	<input type="search" id="searchSL" onkeyup="mySearch()" placeholder="Search Here" class="search_here"/>
        </div>
        
        <div class="row_list border_bottom"> <!--Button--> 
        	<input type="button" id="reset" value="Reset" class="gray_butn" style="margin-left:25%;"/>
            <input type="button" id="search_salesman" value="Search" class="red_butn"/>
        </div>
        
        <!--List--> 
        <div class="clear"></div>
        <!-- <div id="mySL" style="height:305px; width: 100%; overflow: auto;"> -->
        <ul id="mySL" style="height:305px; width: 100%; overflow: auto;list-style-type: none;">
        <?php $aSal=$_objAdmin->_getSelectList('table_salesman AS s','*',''," s.status = 'A' $salesman ORDER BY salesman_name"); 
		if(is_array($aSal)){
		for($i=0;$i<count($aSal);$i++){?>
        
        <li >
		<div class="row_list border_bottom">
        	 <div class="name_list">
             <button class="salesman_list_man" id="<?php echo ($aSal[$i]->salesman_id);?>"><?php echo $aSal[$i]->salesman_name;?></button> 
             </div>
        </div>
        </li>
        
		<!-- <option value="<?php echo base64_encode($aSal[$i]->salesman_id);?>" <?php if ($aSal[$i]->salesman_id==$_SESSION['Mapsal']){ ?> selected <?php } ?>><?php echo $aSal[$i]->salesman_name;?></option> -->
		<?php } }?>
<!-- </div> -->
</ul>
     <div class="clear"></div>    
         
    </div>    
<!--map left list portion end here-->
 
<!--map right portion started from here-->   
    <div class="mapcover_right">
        <div class="map_head">
            Salesman : <label id="salesman_name_label"></label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Report Time : <label id="salesman_time_label"></label>
        </div>
        
        <div class="row_list">
        	<!-- <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d30683992.893261995!2d64.48051417608622!3d20.150536871844487!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30635ff06b92b791%3A0xd78c4fa1854213a6!2sIndia!5e0!3m2!1sen!2sin!4v1496128286651" width="600" height="425" frameborder="0" style="border:0" allowfullscreen></iframe> -->
        	<div id="sales_map" style="width:100%;height:430px;"></div>
        </div>
    </div>
 
<!--map right portion End from here-->  
</div>

</div>


	
</div>


<div class="clear">&nbsp;</div>
</div>
<!--  end content -->
<div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->
<div class="clear">&nbsp;</div> 
<!-- start footer -->         
<?php include("footer.php");?>
<!-- end footer -->
 
</body>
</html>