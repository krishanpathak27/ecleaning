<?php
include("includes/config.inc.php");
include("includes/function.php");
include("includes/globalarraylist.php");

include("header.inc.php");
if(isset($_POST['add']) && $_POST['add'] == 'yes')
{
$report_date=$_POST['to'];
} else {
$report_date=date("Y-m-d",strtotime("-0 day"));
}
if(isset($_REQUEST['sal']) && $_REQUEST['sal']!="" )
{
	$_SESSION['Mapsal']=base64_decode($_REQUEST['sal']);
	$_SESSION['Mapdate']=$report_date;
	header("Location: salesman_map_report.php");
}

$report_day = date('D', strtotime($report_date));
include("sal_map_test.php");?>
 
<script>
    $(function() {
        $( "#from" ).datepicker({
			dateFormat: "d M yy",
            defaultDate: "1w",
            changeMonth: true,
            numberOfMonths: 3,
            onSelect: function( selectedDate ) {
                $( "#to" ).datepicker( "option", "minDate", selectedDate );
            }
        });
        $( "#to" ).datepicker({
			dateFormat: "d M yy",
            defaultDate: "-1w",
            changeMonth: true,
            numberOfMonths: 1,
            onSelect: function( selectedDate ) {
                $( "#from" ).datepicker( "option", "maxDate", selectedDate );
            }
        });
    });
	$(document).ready(function(){
	    $("#sal").change(function(){
		 document.report.submit();
		})
	});
</script>
<script src="javascripts/jquery-1.8.2.js" type="text/javascript"></script>
<script src="javascripts/jquery-ui.js"></script>
<script type="text/javascript" src="javascripts/validate.js"></script>


.<!-- start content-outer -->
<div id="content-outer">
<!-- start content -->
<div id="content">
<div id="page-heading"><h1><span style="color: #d74343; font-weight: bold;">Salesman Map Report</span></h1> 
	<form name="report" id="report" action="salesman_map_report_test.php" method="post">
	<div id="page-heading" align="left" style="padding-left: 350px;"><h3><span style=" color:#000000;">Salesman: 
	
	<select name="sal" id="sal" class="menulist" style="width:150px" >
		<option value="" >Select</option>
		<?php $aSal=$_objAdmin->_getSelectList('table_salesman AS s','*',''," s.status = 'A' $salesman ORDER BY salesman_name"); 
		if(is_array($aSal)){
		for($i=0;$i<count($aSal);$i++){?>
		<option value="<?php echo base64_encode($aSal[$i]->salesman_id);?>" <?php if ($aSal[$i]->salesman_id==$_SESSION['Mapsal']){ ?> selected <?php } ?>><?php echo $aSal[$i]->salesman_name;?></option>
		<?php } }?>
	</select>
	</span>
	</h3>
	</div>
	</form>
	
</div>

<table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">
<tr>
	<!--<td id="tbl-border-left"></td>-->
	<td>
	<!--  start content-table-inner -->
	<div id="content-table-inner">
	
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
	
	<tr valign="top">
	<td>
	<?php
	$auSal=$_objAdmin->_getSelectList('table_salesman',"salesman_name,salesman_id",''," salesman_id='".$_SESSION['Mapsal']."' ");
	if(is_array($auSal)){
	?>
	<form name="frmPre" id="frmPre" method="post" action="salesman_map_report_test.php?sal=<?php echo base64_encode($auSal[0]->salesman_id);?>" enctype="multipart/form-data" >
	<div id="page-heading" align="center" ><h3>Salesman: <span style=" color:#000000;"><?php echo $auSal[0]->salesman_name;?>,</span> Report Date: <input type="text" id="to" name="to" class="date" value="<?php echo $_objAdmin->_changeDate($_SESSION['Mapdate']);?>"  readonly /><input name="add" type="hidden" value="yes" /><input name="submit" class="result-submit" type="submit" id="submit" value="View Details" /></h3></div>
	<?php } ?>
	</form>
	</td>
	<?php 
	if($_SESSION['Mapsal']==""){
	?>
	<tr valign="top">
	<td>
	<table border="0" width="100%" cellpadding="0" cellspacing="0" id="id-form">
			<tr bgcolor="#A52A2A" style="color: #fff;font-weight: bold;">
				<td align="center" style="padding:10px;">Select Sales Team</td>
			</tr>
		</table>
	</td>
	<?php } else { ?>
	<tr valign="top">
	<td>
		<!-- start id-form -->
		<div style="height:500px;overflow:auto;" >
		<table border="0" width="100%" cellpadding="0" cellspacing="0" id="id-form">
			<?php  
			if($err!=''){
			?>
			<tr bgcolor="#A52A2A" style="color: #fff;font-weight: bold;">
				<td align="center" style="padding:10px;"><?php echo $err; ?></td>
			</tr>
			<?php } else { ?>
			<tr>	
				<div id="map_canvas"></div>
			</tr>
			<?php } ?>
		</table>
		</div>
		<!-- end id-form  -->
	</td>
	<?php } ?>
	<td>
	<!-- right bar-->
	<?php //include("rightbar/salesmanMapList_bar.php") ?>
	</td>
	</tr>
<tr>
<td><img src="images/blank.gif" width="695" height="1" alt="blank" /></td>
<td></td>
</tr>
</table>

<!--  end content-table-inner  -->
</td>
<td id="tbl-border-right"></td>
</tr>
<!--<tr>
	<th class="sized bottomleft"></th>
	<td id="tbl-border-bottom">&nbsp;</td>
	<th class="sized bottomright"></th>
</tr>-->
</table>
<div class="clear">&nbsp;</div>
</div>
<!--  end content -->
<div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->
<div class="clear">&nbsp;</div> 
<!-- start footer -->         
<?php include("footer.php");?>
<!-- end footer -->
 
</body>
</html>
