<?php
include("includes/config.inc.php");
include("includes/function.php");
include("includes/globalarraylist.php");

$page_name="Timeline Report";

$user=$_objAdmin->_getSelectList('table_account_admin as a inner join table_web_users as u on a.operator_id=u.operator_id','a.operator_id',''," u.web_user_id='".$_SESSION['PepUpSalesUserId']."'"); 
/*echo $user[0]->operator_id;
if($user[0]->operator_id==''){
header("Location: index.php");
}*/
if($_REQUEST['sal']!=''){
$Sal_Name=$_objAdmin->_getSelectList('table_salesman','salesman_name',''," salesman_id='".base64_decode($_REQUEST['sal'])."' ");
$name=$Sal_Name[0]->salesman_name;
}
 
include("header.inc.php");
if(isset($_POST['add']) && $_POST['add'] == 'yes')
{
$_SESSION['SalReportDate']=$_objAdmin->_changeDate($_POST['to']);
} else {
$_SESSION['SalReportDate']=$_objAdmin->_changeDate(date("Y-m-d",strtotime("-0 day")));
}
$report_day = date('D', strtotime($_SESSION['SalReportDate']));

if($_REQUEST['date']!=''){
$_SESSION['SalReportDate']=$_objAdmin->_changeDate(base64_decode($_REQUEST['date']));
}


if(isset($_POST['export']) && $_POST['export']!="")
{

$date_new=$_SESSION['SalReportDate'];
$val="sal=".$_REQUEST['sal']."&date=".$date_new;
header('Location:export.inc.php?export_salesman_timeline&'.$val);
exit;
}	


 ?>
<script type="text/javascript">

    function PrintElem(elem)
    {
        Popup($(elem).html());
    }

    function Popup(data) 
    {
		
        var mywindow = window.open('', 'Report');
		
        mywindow.document.write('<html><head><title>Timeline Report</title>');
		mywindow.document.write('<table><tr><td><b>Salesman Name:</b> <?php echo $name; ?></td><b>Date:</b> <?php echo $_SESSION['SalReportDate']; ?></td></tr></table>');
        /*optional stylesheet*/ //mywindow.document.write('<link rel="stylesheet" href="main.css" type="text/css" />');
        mywindow.document.write('</head><body >');
        mywindow.document.write(data);
        mywindow.document.write('</body></html>');

        mywindow.print();
        mywindow.close();
        return true;
    }

</script>
<script src="javascripts/jquery-1.8.2.js" type="text/javascript"></script>
<script src="javascripts/jquery-ui.js"></script>
<script type="text/javascript" src="javascripts/validate.js"></script>
<script type="text/javascript">
// Popup window code
function newPopup(url) {
	popupWindow = window.open(
		url,'popUpWindow','height=500,width=800,left=100,top=100,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no,status=yes')
}
</script>
<script type="text/javascript">
// Popup window code
function photoPopup(url) {
	popupWindow = window.open(
		url,'popUpWindow','height=500,width=800,left=100,top=100,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no,status=yes')
}
</script>
<script>
    $(function() {
        $( "#from" ).datepicker({
			dateFormat: "d M yy",
            defaultDate: "1w",
            changeMonth: true,
            numberOfMonths: 3,
            onSelect: function( selectedDate ) {
                $( "#to" ).datepicker( "option", "minDate", selectedDate );
            }
        });
        $( "#to" ).datepicker({
			dateFormat: "d M yy",
            defaultDate: "-1w",
            changeMonth: true,
            numberOfMonths: 1,
            onSelect: function( selectedDate ) {
                $( "#from" ).datepicker( "option", "maxDate", selectedDate );
            }
        });
    });
	$(document).ready(function(){
	    $("#sal").change(function(){
		 document.report.submit();
		})
	});
</script>
<?php //echo $salesman;?>
<!-- start content-outer -->
<div id="content-outer">
<!-- start content -->
<div id="content">
<div id="page-heading"><h1><span style="color: #d74343; font-family: Tahoma; font-weight: bold;">Timeline Report</span></h1> 
	<form name="report" id="report" action="salesman_report.php" method="post">
	<div id="page-heading" align="left" style="padding-left: 250px;"><h3><span style=" color:#000000;">Salesman Name: 
	<select name="sal" id="sal" class="" style="width:150px" >
		<option value="" >Select</option>
		<?php $aSal=$_objAdmin->_getSelectList('table_salesman AS s','*',''," status='A' $salesman ORDER BY salesman_name"); 
		if(is_array($aSal)){
		for($i=0;$i<count($aSal);$i++){?>
		<option value="<?php echo base64_encode($aSal[$i]->salesman_id);?>" <?php if (base64_encode($aSal[$i]->salesman_id)==$_REQUEST['sal']){ ?> selected <?php } ?>><?php echo $aSal[$i]->salesman_name;?></option>
		<?php } }?>
	</select>
	<input name="add" type="hidden" value="yes" />
	<input name="to" type="hidden" value="<?php echo $_SESSION['SalReportDate'];?>" />
	</span></h3></div>
	<!--<input type="button" onclick="functionnew();" value="submit"/>-->
	</form>
	
</div>

<table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">
<!--<tr>
	<th rowspan="3" class="sized"><img src="images/side_shadowleft.jpg" width="20" height="300" alt="" /></th>
	<th class="topleft"></th>
	<td id="tbl-border-top">&nbsp;</td>
	<th class="topright"></th>
	<th rowspan="3" class="sized"><img src="images/side_shadowright.jpg" width="20" height="300" alt="" /></th>
</tr>
-->
<tr>
	<!--<td id="tbl-border-left"></td>-->
	<td>
	<!--  start content-table-inner -->
	<div id="content-table-inner">
	
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
	
	<tr valign="top">
	<td>
	<?php
	$auSal=$_objAdmin->_getSelectList('table_salesman',"salesman_name,salesman_id",''," salesman_id='".base64_decode($_REQUEST['sal'])."' ");
	if(is_array($auSal)){
	?>
	<form name="frmPre" id="frmPre" method="post" action="salesman_report.php?sal=<?php echo base64_encode($auSal[0]->salesman_id);?>" enctype="multipart/form-data" >
	<div id="page-heading" align="center" ><h3>Salesman Name: <span style=" color:#000000;"><?php echo $auSal[0]->salesman_name;?>,</span> Report Date: <input type="text" id="to" name="to" class="date" value="<?php echo $_SESSION['SalReportDate'];?>"  readonly /><input name="add" type="hidden" value="yes" /><input name="submit" class="result-submit" type="submit" id="submit" value="Show Result" />
	<input name="export" class="result-submit" type="submit" id="export" value="Export to Excel">
	<input type="button" value="Print" class="result-submit" onclick="PrintElem('#Report')" /></h3></div>
	<?php } ?>
	</form>
	</td>
	<?php 
	if($_REQUEST['sal']==""){
	?>
	<tr valign="top">
	<td>
	<table border="0" width="100%" cellpadding="0" cellspacing="0" id="id-form">
			<tr bgcolor="#A52A2A" style="color: #fff;font-weight: bold;">
				<td align="center" style="padding:10px;">Select Salesman</td>
			</tr>
		</table>
	</td>
	<?php } else { ?>
	<tr valign="top">
	<td>
		<!-- start id-form -->
		
		<div>
		<div id="Report">
		<table border="0" width="100%" cellpadding="0" cellspacing="0" id="id-form">
			<?php
			if(date("Y-m-d",strtotime("-0 day")) < date('Y-m-d', strtotime($_SESSION['SalReportDate']))){ ?>
			<tr style="border-bottom:2px solid #6E6E6E;" >
				<td colspan="5" align="center" style="padding:10px;"><span style="color: #A52A2A; font-family: Tahoma; font-weight: bold;">No Data Available</span></td>
			</tr>
			<?php } else { ?>
			<?php 
			$activity_id = array();
			$time_start = array();
			$time_end = array();
			$min = array();
			$per = array();
			$action = array();
			$lat = array();
			$lng = array();
			$ref_id = array();
			$ref_type = array();
			$retailer = array();
			$distributor = array();
			$Customer_A = array();
			$total = 0;
		
		$row=$_objAdmin->_getSelectList2('table_activity'," * ",''," salesman_id=".base64_decode($_REQUEST['sal'])."  AND  activity_date ='".date('Y-m-d', strtotime($_SESSION['SalReportDate']))."' and activity_type NOT IN(1,6) order by start_time asc ");


		for($i=0;$i<count($row);$i++) 
		{ 
			$activity_id[] = $row[$i]->activity_id;
			$retailer[]    = $row[$i]->retailer_name;
			$salesman_id[] = $row[$i]->salesman_id;
			$app_version[] = $row[$i]->app_version;
			$device_id[] 	= $row[$i]->device_id;
			$device_brand_model[] = $row[$i]->device_brand_model;
			$distributor[] = $row[$i]->distributor_name;
			$Customer_A[] = $row[$i]->customer_name;
			$customer      = $row[$i]->ref_type;
			$action[] 	   = $row[$i]->activity;
			$lat[] 		   = $row[$i]->lat;
			$lng[] 		   = $row[$i]->lng;
			$ref_id[] 	   = $row[$i]->ref_id;
			$ref_type[]    = $row[$i]->ref_type;
			$activity_type[]=$row[$i]->activity_type;
			$time_start[] = date('H:i',STRTOTIME($row[$i]->start_time));
			$time_end[] = date('H:i',STRTOTIME($row[$i]->end_time));
			
			$seconds = STRTOTIME($row[$i]->end_time) - STRTOTIME($row[$i]->start_time);
			$days    = floor($seconds / 86400);
			$hours   = floor(($seconds - ($days * 86400)) / 3600);
			$minutes = floor(($seconds - ($days * 86400) - ($hours * 3600))/60);
			$min[] = ($hours*60)+$minutes;
			
			$total = $total + (($hours*60)+$minutes);	// Sum total of minuts to get percentage
		}


		for($i = 0; $i < count($time_start); $i++)
		{
			$per[] = ($min[$i]/$total)*100; 			// convert minut in percentagr
		}
		if(count($per) > 0 ) {	?>
		<tr><td>
		<div >
		<center>
		<table style=" border:1px solid grey;  "><tr><td>
		<div style=" padding:25px; ">
		<table border="0" cellpadding="0"  cellspacing="0">
			<tr class='tableRowHeader' align="center">
				<!--<td colspan="3"><b>Graph according to time and action</b></td>-->
			</tr>
			<tr class='tableRowData'>
				<td align="center" ><b>Time</b></td>
				<td >&nbsp;</td><td >&nbsp;</td>
				<td style='padding-left:15px' align="left"><b>Action</b></td>
				<td style='padding-left:15px' align="left"><b>Division</b></td>
				<td style='padding-left:15px' align="left"><b>App Version</b></td>
				<td style='padding-left:15px' align="left"><b>Device ID</b></td>
				<td style='padding-left:15px' align="left"><b>Device Model</b></td>
				<td style='padding-left:15px' align="left"><b>Details</b></td>
				<td style='padding-left:15px' align="left"><b>Map</b></td>
			</tr>
			<?php 
			for($i = 0; $i < count($per); $i++)
			{
				if($activity_id[$i]==base64_decode($_REQUEST['act']) || $activity_id[$i]==base64_decode($_REQUEST['act1'])){
				echo "<tr class='tableRowData'  bgcolor='#FFFF00' id=".base64_encode($activity_id[$i]).">";
				} else {
				echo "<tr class='tableRowData'>";
				}
				if(ceil($per[$i]) <= 0 )
				{
					echo "<td align='center' style='padding-right:15px'>".$time_start[$i]."</td>";
					if($activity_type[$i]=='13'){
					echo "<td ><div style=' height:6px; background-color:#0101DF'></div></td><td ><b>&nbsp;T</b></td>";
					} else {
					echo "<td ><div style=' height:3px; background-color:#FF0000'></div></td><td >&nbsp;</td>";
					}
					
				}
				else
				{
					echo "<td align='center' style='padding-right:15px'>".$time_start[$i]." - ".$time_end[$i]."</td>";
					if($activity_type[$i]=='13'){
					echo "<td ><div style=' height:".(ceil($per[$i])*4)."px; :#0101DF'></div></td><td >&nbsp;</td>";
					} else {
					echo "<td ><div style=' height:".(ceil($per[$i])*4)."px; background-color:#FF0000'></div></td><td >&nbsp;</td>";
					}
				}
				

				if($retailer[$i]!='')
				{
					if($activity_type[$i] == '3'){
						$Ret_loc=$_objAdmin->_getSelectList('table_order as todr LEFT JOIN table_retailer as r ON r.retailer_id=todr.retailer_id left join table_markets as m on m.market_id=r.market_id','market_name,retailer_name',''," todr.order_id='".$ref_id[$i]."' ");
					} else {
						$Ret_loc=$_objAdmin->_getSelectList('table_retailer as r left join table_markets as m on m.market_id=r.market_id','market_name,retailer_name',''," retailer_id='".$ref_id[$i]."' ");
					}
					$market=$Ret_loc[0]->market_name;
					//echo "<td style='padding-left:15px'>".$action[$i]." (".$retailer[$i].", Market: ".$market.")</td>";
					echo "<td style='padding-left:15px'>".$action[$i]." (".$retailer[$i].")</td>";
				}
				else if($distributor[$i]!=""){
					if($activity_type[$i] == '3'){
						$Des_loc=$_objAdmin->_getSelectList('table_order as todr LEFT JOIN table_distributors as d ON d.distributor_id=todr.distributor_id left join table_markets as m on m.market_id=d.market_id','market_name,distributor_name',''," todr.order_id='".$ref_id[$i]."' ");
					} else {
						$Des_loc=$_objAdmin->_getSelectList('table_distributors as d left join table_markets as m on m.market_id=d.market_id','market_name,distributor_name',''," distributor_id='".$ref_id[$i]."' ");
					}
					$market=$Des_loc[0]->market_name;
					//echo "<td style='padding-left:15px'>".$action[$i]." (".$distributor[$i].", Market: ".$market.")</td>";
					echo "<td style='padding-left:15px'>".$action[$i]." (".$distributor[$i].")</td>";

				}else if($Customer_A[$i]!=""){
					if($activity_type[$i] == '3'){
						$Des_loc=$_objAdmin->_getSelectList('table_order as todr LEFT JOIN table_customer as d ON d.customer_id=todr.customer_id left join table_markets as m on m.market_id=d.market_id','market_name,customer_name',''," todr.order_id='".$ref_id[$i]."' ");
					} else {
						$Des_loc=$_objAdmin->_getSelectList('table_customer as d left join table_markets as m on m.market_id=d.market_id','market_name,customer_name',''," customer_id='".$ref_id[$i]."' ");
					}
					$market=$Des_loc[0]->market_name;
					//echo "<td style='padding-left:15px'>".$action[$i]." (".$distributor[$i].", Market: ".$market.")</td>";
					echo "<td style='padding-left:15px'>".$action[$i]." (".$Customer_A[$i].")</td>";

				} else if ($ref_type[$i]=="25") {
					$Cus_loc=$_objAdmin->_getSelectList('table_customer as cus left join table_markets as m on m.market_id=cus.market_id','m.market_name,cus.customer_name',''," customer_id='".mysql_escape_string($ref_id[$i])."' ");
					$market=$Cus_loc[0]->market_name;
					//echo "<td style='padding-left:15px'>".$action[$i]." (".$Cus_loc[0]->customer_name.", Market: ".$market.")</td>";
					echo "<td style='padding-left:15px'>".$action[$i]." (".$Cus_loc[0]->customer_name.")</td>";
				}

				else
				{
						echo "<td style='padding-left:15px'>".$action[$i]." </td>";
				}
				?>
				<?php if($salesman_id[$i]!=""){ 
					$SalAct=$_objAdmin->_getSelectList('table_salesman as s left join table_division as d on s.division_id=d.division_id',' d.division_name,d.division_id ',''," salesman_id='".mysql_escape_string($salesman_id[$i])."' ");

					?>
				<td style='padding-left:15px'><?php echo $SalAct[0]->division_name; ?></td>
				<?php } else{?>
				<td style='padding-left:15px'></td>
				 <?php } if($app_version[$i]!=""){ ?>
				<td style='padding-left:15px'><?php echo $app_version[$i]; ?></td>
				<?php } else{ ?>
				<td style='padding-left:15px'></td>
				<?php }

				 if($device_id[$i]!=""){ ?>
				<td style='padding-left:15px'><?php echo $device_id[$i]; ?></td>
				<?php } else{ ?>
				<td style='padding-left:15px'></td>
				<?php }

				if($device_brand_model[$i]!=""){ ?>
				<td style='padding-left:15px'><?php echo $device_brand_model[$i]; ?></td>
				<?php } else{ ?>
				<td style='padding-left:15px'></td>
				<?php }


				if($lat[$i]!='' && $ref_type[$i]!=1){
				?>
					<?php
					$aPho=$_objAdmin->_getSelectList('table_image','image_id',''," ref_id='".$activity_id[$i]."' and image_type=6 "); 
					if(is_array($aPho)){
					?>
					<td style="padding-left:15px"><a href="JavaScript:photoPopup('activity_photo.php?id=<?php echo base64_encode($aPho[0]->image_id) ;?>');"> View Image </a></td>
					<?php } else { ?>
					<td style="padding-left:15px"></td>
					<?php } ?>
					<td style="padding-left:15px"><a href="JavaScript:newPopup('activity_map.php?id=<?php echo base64_encode($activity_id[$i]);?>');"> View on Map </a></td>
				<?php
					}
				if($ref_id[$i]!='' && $ref_type[$i]==1 && $activity_type[$i]!=31){
				?>
					<td style="padding-left:15px"><a href="order_list.php?act_id=<?php echo base64_encode($ref_id[$i]);?>&sal=<?php echo $_REQUEST['sal'];?>&act_type=<?php echo $activity_type[$i]; ?>" target="_blank"> View Details</a></td>
					<?php if($lat[$i]!=''){ ?>
					<td style="padding-left:15px"><a href="JavaScript:newPopup('show_map.php?ord=<?php echo base64_encode($ref_id[$i]);?>&sal=<?php echo $_REQUEST['sal'];?>&act_type=<?php echo $activity_type[$i]; ?>');"> View on Map </a></td>
					<?php } else { ?>
					<td style="padding-left:15px"></td>
					<?php } ?>
				<?php
				}
				if($ref_id[$i]!='' && $ref_type[$i]==2 && $activity_type[$i]!=31){
				?>
					<td style="padding-left:15px"><a href="retailer_survey.php?retId=<?php echo $ref_id[$i];?>&sal=<?php echo $_REQUEST['sal'];?>" target="_blank"> View Details</a></td>
					<td style="padding-left:15px"></td>
				<?php
				}
				if($ref_id[$i]!='' && $ref_type[$i]==5){
				?>
					<td style="padding-left:15px"><a href="activity_retailer.php?retId=<?php echo base64_encode($ref_id[$i]);?>" target="_blank"> View Details</a></td>
					<td style="padding-left:15px"></td>
				<?php
				}
				if($ref_id[$i]!='' && $ref_type[$i]==24){
				?>
					<td style="padding-left:15px"><a href="activity_distributor.php?desId=<?php echo base64_encode($ref_id[$i]);?>" target="_blank"> View Details</a></td>
					<td style="padding-left:15px"></td>
				<?php
				}
				if($ref_id[$i]!='' && $ref_type[$i]==25){
				?>
					<td style="padding-left:15px"><a href="activity_customer.php?cusId=<?php echo base64_encode($ref_id[$i]);?>" target="_blank"> View Details</a></td>
					<td style="padding-left:15px"></td>
				<?php
				}
				if($ref_id[$i]!='' && $activity_type[$i]==31){
				?>
					<td style="padding-left:15px">&nbsp;</td>
					<td style="padding-left:15px">&nbsp;</td>
				<?php
				}
				echo "</tr>";
			}
			?>
			
			<!-- ===================== start bottom red line =================== -->
			<tr style="background-color:#FF0000">
				<td colspan="8"></td>
			</tr>
			<?php if($_REQUEST['date']!=''){ ?>
			<tr align="center">
				<td colspan="8"><input type="button" value="Close" class="form-cen" onclick="javascript:window.close();" /></td>
			</tr>
			<?php } ?>
			<!-- ===================== end bottom red line =================== -->
		</table>
		</div>
		<div>
		</td></tr></table>
		</center>
	</div>
		</td></tr>
		<?php }
		else { ?>
		<tr style="border-bottom:2px solid #6E6E6E;" >
				<td colspan="5" align="center" style="padding:10px;"><span style="color: #A52A2A; font-family: Tahoma; font-weight: bold;">No Data Available</span></td>
			</tr>
	<?php	} } ?>
		</table>
		</div>
		<!-- end id-form  -->
	</td>
	<?php } ?>
	<td>
	<!-- right bar-->
	<?php include("rightbar/salesmanList_bar.php") ?>
	</td>
	</tr>
<tr>
<td><img src="images/blank.gif" width="695" height="1" alt="blank" /></td>
<td></td>
</tr>
</table>

<!--  end content-table-inner  -->
</td>
<td id="tbl-border-right"></td>
</tr>
<!--<tr>
	<th class="sized bottomleft"></th>
	<td id="tbl-border-bottom">&nbsp;</td>
	<th class="sized bottomright"></th>
</tr>-->
</table>
<div class="clear">&nbsp;</div>
</div>
<!--  end content -->
<div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->
<div class="clear">&nbsp;</div> 
<!-- start footer -->         
<?php include("footer.php");?>
<!-- end footer -->
 
</body>
</html>
