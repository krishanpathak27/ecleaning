<?php
include("includes/config.inc.php");
include("includes/function.php");
$_objAdmin = new Admin();
include("header.inc.php");
if(isset($_POST['add']) && $_POST['add'] == 'yes')
{
$report_date=$_POST['to'];
} else {
$report_date=date("Y-m-d",strtotime("-0 day"));
}
$report_day = date('D', strtotime($report_date));
 ?>
 
<script src="javascripts/jquery-1.8.2.js" type="text/javascript"></script>
<script src="javascripts/jquery-ui.js"></script>
<script type="text/javascript" src="javascripts/validate.js"></script>
<script type="text/javascript">
// Popup window code
function newPopup(url) {
	popupWindow = window.open(
		url,'popUpWindow','height=500,width=600,left=200,top=100,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no,status=yes')
}
</script>
<script>
    $(function() {
        $( "#from" ).datepicker({
			dateFormat: "yy-mm-dd",
            defaultDate: "1w",
            changeMonth: true,
            numberOfMonths: 3,
            onSelect: function( selectedDate ) {
                $( "#to" ).datepicker( "option", "minDate", selectedDate );
            }
        });
        $( "#to" ).datepicker({
			dateFormat: "yy-mm-dd",
            defaultDate: "-1w",
            changeMonth: true,
            numberOfMonths: 1,
            onSelect: function( selectedDate ) {
                $( "#from" ).datepicker( "option", "maxDate", selectedDate );
            }
        });
    });
	$(document).ready(function(){
	    $("#sal").change(function(){
		 document.report.submit();
		})
	});
</script>
<!-- start content-outer -->
<div id="content-outer">
<!-- start content -->
<div id="content">
<div id="page-heading"><h1><span style="color: #d74343; font-family: Tahoma; font-weight: bold;">Salesman Report</span></h1> 
	<form name="report" id="report" action="salesman_report.php" method="post">
	<div id="page-heading" align="left" style="padding-left: 250px;"><h3><span style=" color:#000000;">Salesman Name: 
	<select name="sal" id="sal" class="" style="width:150px" >
		<option value="" >Select</option>
		<?php $aSal=$_objAdmin->_getSelectList('table_salesman','*','',"where account_id='".$_SESSION['accountId']."'ORDER BY salesman_name"); 
		if(is_array($aSal)){
		for($i=0;$i<count($aSal);$i++){?>
		<option value="<?php echo base64_encode($aSal[$i]->salesman_id);?>" <?php if (base64_encode($aSal[$i]->salesman_id)==$_REQUEST['sal']){ ?> selected <?php } ?>><?php echo $aSal[$i]->salesman_name;?></option>
		<?php } }?>
	</select></span></h3></div>
	<!--<input type="button" onclick="functionnew();" value="submit"/>-->
	</form>
	
</div>

<table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">
<!--<tr>
	<th rowspan="3" class="sized"><img src="images/side_shadowleft.jpg" width="20" height="300" alt="" /></th>
	<th class="topleft"></th>
	<td id="tbl-border-top">&nbsp;</td>
	<th class="topright"></th>
	<th rowspan="3" class="sized"><img src="images/side_shadowright.jpg" width="20" height="300" alt="" /></th>
</tr>
-->
<tr>
	<!--<td id="tbl-border-left"></td>-->
	<td>
	<!--  start content-table-inner -->
	<div id="content-table-inner">
	
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
	
	<tr valign="top">
	<td>
	<?php
	$auSal=$_objAdmin->_getSelectList('table_salesman',"salesman_name,salesman_id",''," where salesman_id='".base64_decode($_REQUEST['sal'])."' ");
	if(is_array($auSal)){
	?>
	<form name="frmPre" id="frmPre" method="post" action="salesman_report.php?sal=<?php echo base64_encode($auSal[0]->salesman_id);?>" enctype="multipart/form-data" >
	<div id="page-heading" align="center" ><h3>Salesman Name: <span style=" color:#000000;"><?php echo $auSal[0]->salesman_name;?>,</span> Report Date: <input type="text" id="to" name="to" class="date" value="<?php echo $report_date;?>"  readonly /><input name="add" type="hidden" value="yes" /><input name="submit" class="result-submit" type="submit" id="submit" value="Show Result" /></h3></div>
	<?php } ?>
	</form>
	</td>
	<?php 
	if($_REQUEST['sal']==""){
	?>
	<tr valign="top">
	<td>
	<table border="0" width="100%" cellpadding="0" cellspacing="0" id="id-form">
			<tr bgcolor="#A52A2A" style="color: #fff;font-weight: bold;">
				<td align="center" style="padding:10px;">Select Salesman</td>
			</tr>
		</table>
	</td>
	<?php } else { ?>
	<tr valign="top">
	<td>
		<!-- start id-form -->
		<table border="0" width="100%" cellpadding="0" cellspacing="0" id="id-form">
			<tr bgcolor="#6E6E6E" style="color: #fff;font-weight: bold;">
				<td style="padding:10px;" width="1px">S.No</td>
				<td style="padding:10px;" width="140px" align="left">Retailer Name</td>
				<td style="padding:10px;" width="100px" align="center">Route Name</td>
				<td style="padding:10px;" width="50px">Time of order</td>
				<td align="center" style="padding:10px;" width="50px">Status</td>
				<td align="right" style="padding-right: 50px;" width="40px">Show on Map</td>
			</tr>
		</table>
		<div style="height:500px;overflow:auto;" >
		<table border="0" width="100%" cellpadding="0" cellspacing="0" id="id-form">
			<?php
			if(date("Y-m-d",strtotime("-0 day")) < $report_date){ ?>
			<tr style="border-bottom:2px solid #6E6E6E;" >
				<td colspan="5" align="center" style="padding:10px;"><span style="color: #A52A2A; font-family: Tahoma; font-weight: bold;">No Data Available</span></td>
			</tr>
			<?php } else { ?>
			<?php
			$auRutDet=$_objAdmin->_getSelectList('table_route_schedule as s left join table_route_schedule_by_day as d on s.route_schedule_id=d.route_schedule_id left join table_route as r on d.'.$report_day.'=r.route_id left join table_route_retailer as ret on ret.route_id=r.route_id left join table_retailer as tret on tret.retailer_id=ret.retailer_id',"s.salesman_id,r.route_name,r.route_id,ret.retailer_id,tret.lng as retlng,tret.lat as retlat,tret.retailer_name",''," where s.status='A' and ".$report_day."!='' and s.salesman_id='".base64_decode($_REQUEST['sal'])."' and s.account_id='".$_SESSION['accountId']."'");
			if(is_array($auRutDet))
			{	$sno = 1;
				for($i=0;$i<count($auRutDet);$i++)
				{
					
					?>
					<tr  style="border-bottom:2px solid #6E6E6E;" >
						<td style="padding:10px;"><?php echo $sno; ?></td>
						<td style="padding:10px;"><?php echo $auRutDet[$i]->retailer_name;?></td>
						<td style="padding:10px;"><?php echo $auRutDet[$i]->route_name;?></td>
											
					
					<?php
					$flag=1;
					$auOrd=$_objAdmin->_getSelectList('table_order',"*",''," where salesman_id='".$auRutDet[$i]->salesman_id."' and retailer_id='".$auRutDet[$i]->retailer_id."' and date_of_order='".$report_date."'");
					
					if(is_array($auOrd)){
					for($j=0;$j<count($auOrd);$j++)
					{
						if($flag!=1)
						{
						$sno = $sno + 1;
						?>
						<tr  style="border-bottom:2px solid #6E6E6E;" >
						<td style="padding:10px;"><?php echo $sno; ?></td>
						<td style="padding:10px;"><?php echo $auRutDet[$i]->retailer_name;?></td>
						<td style="padding:10px;"><?php echo $auRutDet[$i]->route_name;?></td>
						
					<?php
						}
						$flag=0;
						?>
							<td style="padding:10px;"><?php echo $auOrd[$j]->time_of_order;?></td>
						<?php
						if($auOrd[$j]->lat !=''){
							$center_lat=$auRutDet[$i]->retlat;
							$center_lng=$auRutDet[$i]->retlng;
							$radius = 1;
							$query = sprintf("SELECT order_id, lat, lng, ( 6371 * acos( cos( radians('%s') ) * cos( radians( lat ) ) * cos( radians( lng ) - radians('%s') ) + sin( radians('%s') ) * sin( radians( lat ) ) ) ) AS distance FROM table_order where order_id='".$auOrd[$j]->order_id."' HAVING distance < '%s' ",
							mysql_real_escape_string($center_lat),
							mysql_real_escape_string($center_lng),
							mysql_real_escape_string($center_lat),
							mysql_real_escape_string($radius));
							$result = mysql_query($query);
							$row = @mysql_fetch_assoc($result);
							if($row!=""){
							?>
							<td style="padding:10px;">
								<div id="visited-act-top">Visited</div>
							</td>
							<td align="left" style="padding-left: 50px;padding-top: 10px;"><a href="JavaScript:newPopup('show_map.php?ord=<?php echo base64_encode($auOrd[$j]->order_id);?>');"><img src="images/google_map.png" ></img></a></td>
					<?php
							
							} else {
					?>
					<td style="padding:10px;">
					<div id="unverified-act-top">Out of Range</div>
					</td>
					<td align="left" style="padding-left: 50px;padding-top: 10px;"><a href="JavaScript:newPopup('show_map.php?ord=<?php echo base64_encode($auOrd[$j]->order_id);?>');"><img src="images/google_map.png" ></img></a></td>
					
					<?php } } else { ?>
					<td style="padding:10px;">
					<div id="unverified-act-top">Unverified</div>
					</td>
					<td align="left" style="padding-left: 50px;padding-top: 10px;">-</td>
					<?php }} } else { 
					if($flag==1)
						{ ?>
						<td style="padding:10px;">-</td>	
						<?php } ?>
					<td style="padding:10px;">
					<div id="notvisited-act-top">No Visit</div>
					</td>
					<td align="left" style="padding-left: 50px;padding-top: 10px;">-</td>
					<?php } ?>
					</tr>
					<?php 
					$sno = $sno + 1;
				} 
			} else {?>
					<tr style="border-bottom:2px solid #6E6E6E;" >
					<td colspan="5" align="center" style="padding:10px;"><span style="color: #A52A2A; font-family: Tahoma; font-weight: bold;">No Data Available</span></td>
					</tr>
			<?php } } ?>
		</table>
		</div>
		<!-- end id-form  -->
	</td>
	<?php } ?>
	<td>
	<!-- right bar-->
	<?php include("rightbar/salesmanList_bar.php") ?>
	</td>
	</tr>
<tr>
<td><img src="images/blank.gif" width="695" height="1" alt="blank" /></td>
<td></td>
</tr>
</table>

<!--  end content-table-inner  -->
</td>
<td id="tbl-border-right"></td>
</tr>
<!--<tr>
	<th class="sized bottomleft"></th>
	<td id="tbl-border-bottom">&nbsp;</td>
	<th class="sized bottomright"></th>
</tr>-->
</table>
<div class="clear">&nbsp;</div>
</div>
<!--  end content -->
<div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->
<div class="clear">&nbsp;</div> 
<!-- start footer -->         
<?php include("footer.php");?>
<!-- end footer -->
 
</body>
</html>