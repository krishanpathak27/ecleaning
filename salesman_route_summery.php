<?php
include("includes/config.inc.php");
include("includes/function.php");
include("includes/globalarraylist.php");

$page_name="Salesman Variance Report";


$divisionRec=$_objAdmin->_getSelectList2('table_division',"division_name as division,division_id",'','');

$designationRec=$_objAdmin->_getSelectList2('table_salesman_hierarchy',"description as designation,hierarchy_id",'');

if(isset($_POST['showReport']) && $_POST['showReport'] == 'yes')
{	
	
	if($_POST['sal']!="" && $_POST['sal']!='All') 
	{
	$salesman='and s.salesman_id='.$_POST['sal'].' ';
	$_SESSION['SalGapList']=$_POST['sal'];	
	}

	if($_POST['month']!="") 
	{
	$monthVal= " and rs.month='".$_POST['month']."' ";
	$_SESSION['PVsVMonth']=$_POST['month'];
	}
	if($_POST['year']!="") 
	{
	$yearVal=" and rs.year='".$_POST['year']."' ";
	$_SESSION['PVsVYear']=$_POST['year'];	
	}

	if($_POST['division'] !='')
	{
	$_SESSION['findDivision'] = $_POST['division'];
	}
	if($_POST['designation'] !='')
	{

	$_SESSION['findDesignation'] = $_POST['designation'];
	}	


	$divisionCondition = "";
	$designationCondition = "";

	/*if($_POST['to']!="") 
	{
	$to_date=$_objAdmin->_changeDate($_POST['to']);	
	}

	 else {

	$to_date= $_objAdmin->_changeDate(date("Y-m-d"));
	}
	if($_POST['from']!="") 
		{
		$from_date=$_objAdmin->_changeDate($_POST['from']);	
		}

	 else {

	$from_date= $_objAdmin->_changeDate(date("Y-m-d"));
	}*/


	if($_POST['month']!="") 
	{
	$monthVal= " and rs.month='".$_POST['month']."' ";
	$_SESSION['PVsVMonth']=$_POST['month'];
	}
	if($_POST['year']!="") 
	{
	$yearVal=" and rs.year='".$_POST['year']."' ";
	$_SESSION['PVsVYear']=$_POST['year'];	
	}
	/* Route Name */
	if($_POST['route_id']!="" && $_POST['route_id']!='0') 
	{
	$routeVal=" and rsd.route_id='".$_POST['route_id']."' ";
	 $_SESSION['routeVal']=$_POST['route_id'];
	}
	if($_POST['route_id']=='0')
	{
	unset($_SESSION['routeVal']);
	$routeVal="";
	
	}	
	if($_SESSION['userLoginType'] == 5 && !isset($_SESSION['findDivision'])){
    $divisionIdString = implode(",", $divisionList);
    $divisionCondition = " AND ( s.division_id IN ($divisionIdString) )";
   
} 

	if(isset($_SESSION['findDivision']) && $_SESSION['findDivision']!="all"){
	   $divisionCondition = " AND ( s.division_id IN(".$_SESSION['findDivision'].") )";

	}

	if(isset($_SESSION['findDesignation']) && $_SESSION['findDesignation']!="all"){
	 $designationCondition = " AND ( SR.hierarchy_id IN(".$_SESSION['findDesignation'].") )";

	}


	if($_POST['sal']=="" || $_POST['sal']=="All") 
	{
	  unset($_SESSION['SalGapList']);	
	}
	if($_POST['gap']!="") 
	{
	$_SESSION['GapTime']=$_POST['gap'];	
	}
} else {
$_SESSION['PVsVMonth']= date('n');
$monthVal= " and rs.month='".$_SESSION['PVsVMonth']."' ";
$_SESSION['PVsVYear']=  date('Y');
$yearVal=" and rs.year='".$_SESSION['PVsVYear']."' ";	
	
}
if(isset($_REQUEST['reset']) && $_REQUEST['reset'] == 'yes')
{
	unset($_SESSION['SalGapList']);
	unset($_SESSION['findDivision']);
	unset($_SESSION['findDesignation']);	
	unset($_SESSION['GapTime']);
	unset($_SESSION['PVsVMonth']);
	unset($_SESSION['PVsVYear']);
	unset($_SESSION['routeVal']);

	header("Location: salesman_route_summery.php");
}

if($_SESSION['SalGapList']!=''){
$SalName=$_objAdmin->_getSelectList('table_salesman','salesman_name,salesman_code',''," salesman_id='".$_SESSION['SalGapList']."'"); 
$sal_name=$SalName[0]->salesman_name;
} else {
$sal_name="All Salesman";
}

/* Export Records into excel sheet */


if(isset($_POST['export']) && $_POST['export']!="")
  {

     header('Location:export.inc.php?salesman_route_summery&sal='.$_POST['sal'].'&month='.$_POST['month'].'&year='.$_POST['year'].'&route_id='.$_POST['route_id'].''); 	
  }



 /*echo '<pre>';
 print_r($salesman);*/

 /*$dateCondition = "AND rs.month>= '".date('m', strtotime($from_date))."' AND rs.month>= '".date('m', strtotime($to_date))."' AND rsd.assign_day>= '".date('d', strtotime($from_date))."' AND rsd.assign_day<= '".date('d', strtotime($to_date))."'  AND rs.year= '".date('Y', strtotime($to_date))."' AND rs.year= '".date('Y', strtotime($from_date))."' ";*/ 

$SalName=$_objAdmin->_getSelectList('table_route_scheduled as rs left join table_salesman as s on rs.salesman_id=s.salesman_id left join table_route_schedule_details as rsd on rsd.route_schedule_id=rs.route_schedule_id
left join table_route as ro on ro.route_id=rsd.route_id left join table_division as td on td.division_id=s.division_id LEFT JOIN table_salesman_hierarchy_relationship as SR on SR.salesman_id=s.salesman_id 
    	LEFT JOIN table_salesman_hierarchy as SH on SH.hierarchy_id=SR.hierarchy_id','rs.month,rs.year,s.salesman_name,s.salesman_id,s.salesman_code,SH.description,td.division_name,rsd.assign_day,ro.route_name,ro.route_id',''," $salesman $divisionCondition $designationCondition  $routeVal  $monthVal $yearVal ORDER BY rsd.assign_day ASC"); 



	
function convertToHoursMins($time, $format = '%d:%d') {
    settype($time, 'integer');
    if ($time < 1) {
        return;
    }
    $hours = floor($time/60);
    $minutes = $time%60;
    return sprintf($format, $hours, $minutes);
}

?>

<?php include("header.inc.php") ?>
<script src="javascripts/dateNextPrev.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function(){
	    $("#sal").change(function(){
		 document.report.submit();
		})
	});
</script>
<script type="text/javascript">

    function PrintElem(elem)
    {
        Popup($(elem).html());
    }

    function Popup(data) 
    {
		
        var mywindow = window.open('', 'Report');
		
        mywindow.document.write('<html><head><title>Salesman Variance Report</title>');
		mywindow.document.write('<table><tr><td><b>Salesman Name:</b> <?php echo $sal_name; ?></td><td><b>From Date:</b> <?php echo $_SESSION['FromAttList']; ?></td><td><b>To Date:</b> <?php echo $_SESSION['ToAttList']; ?></td></tr></table>');
        /*optional stylesheet*/ //mywindow.document.write('<link rel="stylesheet" href="main.css" type="text/css" />');
        mywindow.document.write('</head><body >');
        mywindow.document.write(data);
        mywindow.document.write('</body></html>');

        mywindow.print();
        mywindow.close();
        return true;
    }
$(document).ready(function()
{
<?php /*if($_POST['submit']=='Export to Excel'){ ?>
tableToExcel('report_export', 'Salesman Route Summery Report', 'Salesman Route Summery Report.xls');
<?php }*/ ?>
});
</script>
<!-- start content-outer -->
<div id="content-outer">
<!-- start content -->
<div id="content">
<div id="page-heading"><h1><span style="color: #d74343; font-family: Tahoma; font-weight: bold;">Salesman Variance Report</span></h1></div>
<table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">
<tr>
	<!--<td id="tbl-border-left"></td>-->
	<td>
	<!--  start content-table-inner -->
	<div id="content-table-inner">
	
	<div id="page-heading" align="left" >
	<form name="frmPre" id="frmPre" method="post" action="" enctype="multipart/form-data" >
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
	
	<tr>
		<td>
			<h3>Salesman Name: </h3><h6>
			<select name="sal" id="sal" class="styledselect_form_5">
				<?php echo $rtndata = $_objArrayList->GetSalesmenMenu($salsList, $_SESSION['SalGapList'],'flex');?>
			</select>
			</h6>
		</td>
		
        
        <td>
			  <h3>Division: </h3>
                        <h6>
                            <select name="division" id="division" class="menulist">
                                <option value="all"<?php echo ($_SESSION['findDivision'] =="all")?'selected':''?>>All</option>
                                <?php 
                                    foreach ($divisionRec as $key => $value) {?>
                                        <option value="<?php echo $value->division_id?>" <?php echo ($_SESSION['findDivision'] ==$value->division_id)?'selected':''?>><?php echo $value->division ?></option>
                                    <?php }
                                ?>
                            </select>
                        </h6>
		</td>
		 <td>
			  <h3>Designation: </h3>
                        <h6>
                            <select name="designation" id="designation" class="menulist">
                                <option value="all"<?php echo ($_SESSION['findDesignation'] =="all")?'selected':''?>>All</option>
                                <?php 
                                    foreach ($designationRec as $key => $value) {?>
                                        <option value="<?php echo $value->hierarchy_id?>" <?php echo ($_SESSION['findDesignation'] ==$value->hierarchy_id)?'selected':''?>><?php echo $value->designation ?></option>
                                    <?php }
                                ?>
                            </select>
                        </h6>
		</td>
        <td>
			<h3>Route : </h3><h6>			
				<select name="route_id" class="styledselect_form_3">
				<option value="0">All</option>
		<?php $RouteName=$_objAdmin->_getSelectList('table_route','route_id,route_name',''," status='A' ");

				$routeArr = array();	
				if(is_array($RouteName)){
					foreach ($RouteName as $key => $value) {
						$routeArr[$value->route_id] = $value->route_name;

				?>
			<option value="<?php echo $value->route_id;?>" <?php if($value->route_id==$_SESSION['routeVal']){ echo 'selected';} ?> ><?php echo $value->route_name;?></option>
			<?php }} ?>
		    ?></select>
			</h6>
		</td>
		 </tr>
		 <tr>
		 <td>
			<h3>Year: </h3><h6>			
				<select name="year" class="styledselect_form_3">					
					<?php echo $_objArrayList->getYearList2($_SESSION['PVsVYear']);?>
				</select>
			</h6>
		</td>
        <td>
		<h3>Month: </h3><h6>
			<select name="month" class="menulist styledselect_form_3">
				<?php echo $_objArrayList->getMonthList3($_SESSION['PVsVMonth']);?>
			</select>
		</h6>
        </td> 


       <!--  <td>
		<h3></h3>
		<h3>From Date: </h3><h6>	
        <input type="text" id="from" name="from" class="date" value="<?php echo $from_date; ?>"  readonly />
        </h6>
		<td>			
		<h3>To Date: </h3>
		<h6>	
        <input type="text" id="to" name="to" class="date" value="<?php echo $to_date; ?>"  readonly />
        </h6>
		</td>
		 -->
		</tr>
		<tr>
		<td colspan="6">
		
		<input name="submit" class="result-submit" type="submit" id="submit" value="View Details" />
		<input type="button" value="Reset!" class="form-reset" onclick="location.href='salesman_route_summery.php?reset=yes';" />
		<input name="showReport" type="hidden" value="yes" />
		<input type="button" value="Print" class="result-submit" onclick="PrintElem('#Report')" />
		<a style="margin-top:25px; display:none;" id="dlink"></a>
		<input name="export" class="result-submit" type="submit" id="export" value="Export to Excel"   >
		<?php

		// echo '<pre>';
		// print_r($SalName);
		// exit();

		/*$aRec=$_objAdmin->_getSelectList('table_route_scheduled AS RS
			LEFT JOIN table_route_schedule_details AS RSD ON RSD.route_schedule_id = RS.route_schedule_id
			LEFT JOIN table_route_retailer AS RR ON RR.route_id = RSD.route_id AND RR.status IN ("R", "D")
			LEFT JOIN table_retailer AS RET ON RET.retailer_id = RR.retailer_id AND RR.status = "R" AND DATE_FORMAT(RET.start_date, "%Y-%m-%d") < DATE_FORMAT(CONCAT(RS.year,"-",RS.month,"-",RSD.assign_day),"%Y-%m-%d") AND RET.status = "A"
			LEFT JOIN table_distributors AS D ON D.distributor_id = RR.retailer_id AND RR.status = "D" AND DATE_FORMAT(D.start_date, "%Y-%m-%d") < DATE_FORMAT(CONCAT(RS.year,"-",RS.month,"-",RSD.assign_day),"%Y-%m-%d") AND D.status = "A"
			LEFT JOIN table_route AS R ON R.route_id = RSD.route_id',
			"COUNT(RR.status) AS ttlRoutePersons, COUNT(RET.retailer_id) AS ttlRouteRetailers, COUNT(D.distributor_id) AS ttlRouteDistributors",'',
			"RS.month ='".$_SESSION['PVsVMonth']."' AND RS.year = '".$_SESSION['PVsVYear']."' ORDER BY RSD.assign_day ASC");*/

  /*$aRec=$_objAdmin->_getSelectList('table_route_scheduled AS rs
  			LEFT JOIN table_salesman as s on rs.salesman_id=s.salesman_id
			LEFT JOIN table_route_schedule_details AS rsd ON rsd.route_schedule_id = rs.route_schedule_id
			LEFT JOIN table_division as td on td.division_id=s.division_id
			LEFT JOIN table_salesman_hierarchy_relationship as SR on SR.salesman_id=s.salesman_id 
    	    LEFT JOIN table_salesman_hierarchy as SH on SH.hierarchy_id=SR.hierarchy_id
			LEFT JOIN table_route_retailer AS RR ON RR.route_id = rsd.route_id AND RR.status IN ("R", "D")
			LEFT JOIN table_retailer AS RET ON RET.retailer_id = RR.retailer_id AND RR.status = "R" AND DATE_FORMAT(RET.start_date, "%Y-%m-%d") < DATE_FORMAT(CONCAT(rs.year,"-",rs.month,"-",rsd.assign_day),"%Y-%m-%d") AND RET.status = "A"
			LEFT JOIN table_distributors AS D ON D.distributor_id = RR.retailer_id AND RR.status = "D" AND DATE_FORMAT(D.start_date, "%Y-%m-%d") < DATE_FORMAT(CONCAT(rs.year,"-",rs.month,"-",rsd.assign_day),"%Y-%m-%d") AND D.status = "A"
			LEFT JOIN table_route AS R ON R.route_id = rsd.route_id',
			"COUNT(RET.retailer_id) AS ttlRouteRetailers, COUNT(D.distributor_id) AS ttlRouteDistributors",'',
			"$salesman $divisionCondition $designationCondition  $routeVal AND rs.month ='".$_SESSION['PVsVMonth']."' AND rs.year = '".$_SESSION['PVsVYear']."' ORDER BY rsd.assign_day ASC");*/

  // echo '<pre>';
  // print_r($aRec);


$Newdate = $_SESSION['PVsVYear'].'-'.$_SESSION['PVsVMonth'];
$startDate = date('Y-m-d', strtotime($Newdate));
$endDate = date('Y-m-t', strtotime($Newdate));


//$RResult = $_objAdmin->_getSelectList("table_order",'COUNT(DISTINCT retailer_id) as total_orders','',"retailer_id>0 AND date_of_order BETWEEN ('".date('Y-m-d', strtotime($startDate))."') AND ('".date('Y-m-d', strtotime($endDate))."') " );

//$DResult = $_objAdmin->_getSelectList("table_order",'COUNT(DISTINCT distributor_id) as total_orders','',"distributor_id >0 AND customer_id=0 AND retailer_id=0 AND date_of_order BETWEEN ('".date('Y-m-d', strtotime($startDate))."') AND ('".date('Y-m-d', strtotime($endDate))."') " );


$RResult1 = $_objAdmin->_getSelectList("table_order",'COUNT(DISTINCT retailer_id) as total_orders,salesman_id,date_of_order','',"retailer_id>0 AND date_of_order BETWEEN ('".date('Y-m-d', strtotime($startDate))."') AND ('".date('Y-m-d', strtotime($endDate))."') GROUP BY salesman_id,date_of_order " );


$DResult1 = $_objAdmin->_getSelectList("table_order",'COUNT(DISTINCT distributor_id) as total_orders,salesman_id,date_of_order','',"distributor_id >0 AND customer_id=0 AND retailer_id=0 AND date_of_order BETWEEN ('".date('Y-m-d', strtotime($startDate))."') AND ('".date('Y-m-d', strtotime($endDate))."')  GROUP BY salesman_id,date_of_order" );

$salData = array();
foreach ($RResult1 as $key => $value) {
	$salData[$value->salesman_id][$value->date_of_order] = $value;
}

$disData = array();
foreach ($DResult1 as $key => $value) {
	$disData[$value->salesman_id][$value->date_of_order] = $value;
}




/*$aRec1=$_objAdmin->_getSelectList('table_route_retailer','route_id,retailer_id, COUNT(*) AS TOTAL, COUNT(IF(status="R",1,null)) as retailer,
    COUNT(IF(status="D",1,null)) as distributor',''," status!='' GROUP BY route_id");


$routeArray = array();
foreach ($aRec1 as $key => $value) {
	$routeArray[$value->route_id] = $value;
}*/


	/*$condi=	" rs.status='A' and s.status='A'  ";
	
	$auRec=$_objAdmin->_getSelectList('table_route_scheduled as rs left join table_salesman as s on rs.salesman_id=s.salesman_id left join table_route_schedule_details as rsd on rsd.route_schedule_id=rs.route_schedule_id
left join table_route as ro on ro.route_id=rsd.route_id','rs.*,s.salesman_name,rsd.assign_day,ro.route_name,ro.route_id',''," $condi $salesman $monthVal $yearVal $routeVal ");*/ 


	if(is_array($SalName)){
		$retailer=0;
		$distributor=0;
		$visited_dealer=0;
		$visited_distributor=0;
		
		foreach ($SalName as $key => $value) {

			$days = date('Y-m-d', strtotime($value->year.'-'.$value->month.'-'.$value->assign_day));
			
			
			$aRec2=$_objAdmin->_getSelectList('table_route_retailer','route_id, COUNT(*) AS TOTAL, COUNT(IF(status="R",1,null)) as retailer,
    COUNT(IF(status="D",1,null)) as distributor',''," route_id='".$value->route_id."' and status!='' ");
		
			$retailer=$retailer+$aRec2[0]->retailer;
			$distributor=$distributor+$aRec2[0]->distributor;

			if( isset($salData[$value->salesman_id][$days])){

				 $visited_dealer = $visited_dealer+$salData[$value->salesman_id][$days]->total_orders; 

			}	

			if( isset($disData[$value->salesman_id][$days])){

				$visited_distributor = $visited_distributor+$disData[$value->salesman_id][$days]->total_orders;
			}
		}

							
	}



if(count($SalName)>0){
	foreach ($SalName as $key => $value) {
		$arrayName[$value->salesman_id] = $value->salesman_id; 
	}
}


//$retailer=$retailer+$aRec[0]->ttlRouteRetailers;				   		
//$tretailer=$tretailer+$RResult[0]->total_orders;
//$distributor=$distributor+$aRec[0]->ttlRouteDistributors;	
//$tdistributor=$tdistributor+$DResult[0]->total_orders;


$total_salesman=count($arrayName);
				 
?>
		 <b>Total Dealer  : </b><?php  echo $retailer; ?>
		 <b>Total Dealer visited : </b><?php echo $visited_dealer; ?>
		 <b>Total distributor  : </b><?php echo $distributor; ?>
		 <b>Total distributor visited : </b><?php echo $visited_distributor; ?>
		 <b>Total salesman: </b><?php echo $total_salesman; ?>
		
		</td>
	</tr>
	</table>
	</form>
	</div>
	<?php //exit();?>
	<div id="Report">
	<table border="0" width="100%" cellpadding="0" cellspacing="0">

	
	<tr valign="top">
	<td>
		<!-- start id-form -->
		<table  border="0" cellspacing="0" cellpadding="0"  align="center" width="100%" id="report_export" name="report_export">
		<tr>
			<td>
			<div>
				<table  border="0" width="100%" cellpadding="0" cellspacing="0" >
					<tr  bgcolor="#6E6E6E" style="color: #fff;font-weight: bold;" >
						<td style="padding:10px;" width="20%">Salesman Name</td>
						<td style="padding:10px;" width="20%">Salesman Code</td>
						<td style="padding:10px;" width="20%">Salesman Division</td>
						<td style="padding:10px;" width="20%">Salesman Designation</td>
						<td style="padding:10px;" width="20%">Date</td>
						<td style="padding:10px;" width="20%">Route Name</td>
						<td style="padding:10px;" width="20%">Number Of Dealer</td>
						<td style="padding:10px;" width="20%">Number Of Dealer Visited</td>
						<td style="padding:10px;" width="20%">Number Of Distributor</td>						
						<td style="padding:10px;" width="20%">Number Of Distributor Visited</td>
					
						
					</tr>
					<?php 
					 if(count($SalName)>0){
					 	$retailer=0;
					 	$distributor=0;
					 	$tretailer=0;
					 	$tdistributor=0;

				   foreach ($SalName as $key => $value) {

				   	if($value->route_name=="")
				   	{

				   	}
				   	else

				   	{
				   		 $days = date('Y-m-d', strtotime($value->year.'-'.$value->month.'-'.$value->assign_day));
				     	 $date=$_objAdmin->_changeDate($days);

				     	?>
						 
					<tr  style="border-bottom:2px solid #6E6E6E;" >
						<td style="padding:10px;" width="20%"><?php echo $value->salesman_name; ?></td>
						<td style="padding:10px;" width="20%"><?php echo $value->salesman_code; ?></td>
						<td style="padding:10px;" width="20%"><?php echo $value->division_name; ?></td>
						<td style="padding:10px;" width="20%"><?php echo $value->description; ?></td>
						<td style="padding:10px;" width="20%"><?php echo $date;?></td>					
						<td style="padding:10px;" width="0%"><?php echo $value->route_name; ?></td>

						<?php 
						$aRec=$_objAdmin->_getSelectList('table_route_retailer','route_id, COUNT(*) AS TOTAL, COUNT(IF(status="R",1,null)) as retailer,
    COUNT(IF(status="D",1,null)) as distributor',''," route_id='".$value->route_id."' and status!='' "); 

						 ?>


						<td style="padding:10px;" width="10%"><?php echo $aRec[0]->retailer;?></td>

						<td style="padding:10px;" width="10%">

							<?php 
							if( isset($salData[$value->salesman_id][$days])){
								
								echo $salData[$value->salesman_id][$days]->total_orders;
							}else{
								echo 0;
							}	
							?>
						</td>

						<td style="padding:10px;" width="10%"><?php echo $aRec[0]->distributor;?></td>					
						<td style="padding:10px;" width="10%">

							<?php
							if( isset($disData[$value->salesman_id][$days])){

								echo $disData[$value->salesman_id][$days]->total_orders;
							}else{
								echo 0;
							}	
							?>
							
						</td>
					
					
				
				   </tr>
				   <?php   }} ?>
				   <tr bgcolor="#6E6E6E" style="color: #fff;font-weight: bold;">
				   
				   
				   </tr>
				   <?php  } else{ ?>
						<tr  style="border-bottom:2px solid #6E6E6E;" align="center">
						<td style="padding:10px;" colspan="7">Report Not Available</td>
					</tr>
					<?php } ?>	
				</table>
				</div>
		</td>
		</tr>
		</table>
	<?php //} ?>
	</td>

</tr>
<tr>
<td><img src="images/blank.gif" width="695" height="1" alt="blank" /></td>
<td></td>
</tr>
</table>	
</div>	
<div class="clear"></div>
</div>
<!--  end content-table-inner  -->
</td>
<td id="tbl-border-right"></td>
</tr>
<!--<tr>
	<th class="sized bottomleft"></th>
	<td id="tbl-border-bottom">&nbsp;</td>
	<th class="sized bottomright"></th>
</tr>-->
</table>
<div class="clear">&nbsp;</div>
</div>
<!--  end content -->
<div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->
<div class="clear">&nbsp;</div> 
<!-- start footer -->         
<?php include("footer.php") ?>
<!-- end footer -->
<script src="javascripts/jquery-1.8.2.js" type="text/javascript"></script>
<script src="javascripts/jquery-ui.js"></script>
<script type="text/javascript" src="javascripts/validate.js"></script>
<script type="text/javascript">
// Popup window code
function newPopup(url) {
	popupWindow = window.open(
		url,'popUpWindow','height=500,width=600,left=200,top=100,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no,status=yes')
}
function exportToexcel () {
	//alert('Start');
	tableToExcel('report_export', 'Salesman Route Summary Report', 'Salesman Route Summary Report.xls');
	//alert('End');
}
</script>
<script>
    $(function() {
        $( "#from" ).datepicker({
			dateFormat: "d M yy",
            defaultDate: "w",
            changeMonth: true,
            numberOfMonths: 1,
            minDate:-180,
            onSelect: function( selectedDate ) {
                $( "#to" ).datepicker( "option", "minDate", selectedDate );
            }
        });
        $( "#to" ).datepicker({
			dateFormat: "d M yy",
            defaultDate: "-w",
            changeMonth: true,
            numberOfMonths: 1,           
            onSelect: function( selectedDate ) {
                $( "#from" ).datepicker( "option", "maxDate", selectedDate );
            }
        });
    });
</script>
<script type='text/javascript'>//<![CDATA[ 
var tableToExcel = (function () {
        var uri = 'data:application/vnd.ms-excel;base64,'
        , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table><tr><td></td></tr></table><table>{table}</table></body></html>'
        , base64 = function (s) { return window.btoa(unescape(encodeURIComponent(s))) }
        , format = function (s, c) { return s.replace(/{(\w+)}/g, function (m, p) { return c[p]; }) }
        return function (table, name, filename) {
            if (!table.nodeType) table = document.getElementById(table)
            var ctx = { worksheet: name || 'Worksheet', table: table.innerHTML }

            document.getElementById("dlink").href = uri + base64(format(template, ctx));
            document.getElementById("dlink").download = filename;
            document.getElementById("dlink").click();

        }
    })()

//]]>  
</script>
</body>
</html>