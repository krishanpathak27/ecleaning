<?php
include("includes/config.inc.php");
include("includes/function.php");
$page_name="Route Schedule";
$_objAdmin = new Admin();

if(isset($_SESSION['SalesmanSchedule'])){
$_objAdmin->mysql_query("delete from table_route_schedule_by_day where Sun='' and Mon='' and Tue='' and Wed='' and Thu='' and Fri='' and Sat='' and route_schedule_id=".$_SESSION['SalesmanSchedule']);
}

if($_REQUEST['Mon']!=""){
	$num='';
	$_objAdmin->_dbUpdate(array("Mon"=>''),'table_route_schedule_by_day', " Mon=".$_REQUEST['Mon']." and id='".$_REQUEST['id']."'");
}
if($_REQUEST['Tue']!=""){
	$num='';
	$_objAdmin->_dbUpdate(array("Tue"=>''),'table_route_schedule_by_day', " Tue=".$_REQUEST['Tue']." and id='".$_REQUEST['id']."'");
}
if($_REQUEST['Wed']!=""){
	$num='';
	$_objAdmin->_dbUpdate(array("Wed"=>''),'table_route_schedule_by_day', " Wed=".$_REQUEST['Wed']." and id='".$_REQUEST['id']."'");
}
if($_REQUEST['Thu']!=""){
	$num='';
	$_objAdmin->_dbUpdate(array("Thu"=>''),'table_route_schedule_by_day', " Thu=".$_REQUEST['Thu']." and id='".$_REQUEST['id']."'");
}
if($_REQUEST['Fri']!=""){
	$num='';
	$_objAdmin->_dbUpdate(array("Fri"=>''),'table_route_schedule_by_day', " Fri=".$_REQUEST['Fri']." and id='".$_REQUEST['id']."'");
}
if($_REQUEST['Sat']!=""){
	$num='';
	$_objAdmin->_dbUpdate(array("Sat"=>''),'table_route_schedule_by_day', " Sat=".$_REQUEST['Sat']." and id='".$_REQUEST['id']."'");
}
if($_REQUEST['Sun']!=""){
	$num='';
	$_objAdmin->_dbUpdate(array("Sun"=>''),'table_route_schedule_by_day', " Sun=".$_REQUEST['Sun']." and id='".$_REQUEST['id']."'");
}
if(isset($_POST['addMon']) && $_POST['addMon'] == 'yes')
{
	$sid=$_objAdmin->addMonRouteSchedule();
	$sus="Monday Route has been added successfully.";
}
if(isset($_POST['addTue']) && $_POST['addTue'] == 'yes')
{
	$sid=$_objAdmin->addTueRouteSchedule();
	$sus="Tuesday Route has been added successfully.";
}
if(isset($_POST['addWed']) && $_POST['addWed'] == 'yes')
{
	$sid=$_objAdmin->addWedRouteSchedule();
	$sus="Wednesday Route has been added successfully.";
}
if(isset($_POST['addThu']) && $_POST['addThu'] == 'yes')
{
	$sid=$_objAdmin->addThuRouteSchedule();
	$sus="Thursday Route has been added successfully.";
}
if(isset($_POST['addFri']) && $_POST['addFri'] == 'yes')
{
	$sid=$_objAdmin->addFriRouteSchedule();
	$sus="Friday Route has been added successfully.";
}
if(isset($_POST['addSat']) && $_POST['addSat'] == 'yes')
{
	$sid=$_objAdmin->addSatRouteSchedule();
	$sus="Saturday Route has been added successfully.";
}
if(isset($_POST['addSun']) && $_POST['addSun'] == 'yes')
{
	$sid=$_objAdmin->addSunRouteSchedule();
	$sus="Sunday Route has been added successfully.";
}

$auSal=$_objAdmin->_getSelectList('table_route_schedule as r left join table_salesman as s on r.salesman_id=s.salesman_id',"r.*,s.salesman_name",''," r.route_schedule_id=".$_SESSION['SalesmanSchedule']." and r.account_id=".$_SESSION['accountId']);

?>

<?php include("header.inc.php") ?>
<script language="javascript"> 
function toggle() {
	var ele = document.getElementById("toggleText");
	var text = document.getElementById("displayText");
	if(ele.style.display == "block") {
    		ele.style.display = "none";
		text.innerHTML = "(Add Route)";
  	}
	else {
		ele.style.display = "block";
		text.innerHTML = "(Hide Route)";
	}
	
} 
function toggle1() {
	var ele = document.getElementById("toggleText1");
	var text = document.getElementById("displayText1");
	if(ele.style.display == "block") {
    		ele.style.display = "none";
		text.innerHTML = "(Add Route)";
  	}
	else {
		ele.style.display = "block";
		text.innerHTML = "(Hide Route)";
	}
	
} 
function toggle2() {
	var ele = document.getElementById("toggleText2");
	var text = document.getElementById("displayText2");
	if(ele.style.display == "block") {
    		ele.style.display = "none";
		text.innerHTML = "(Add Route)";
  	}
	else {
		ele.style.display = "block";
		text.innerHTML = "(Hide Route)";
	}
	
} 
function toggle3() {
	var ele = document.getElementById("toggleText3");
	var text = document.getElementById("displayText3");
	if(ele.style.display == "block") {
    		ele.style.display = "none";
		text.innerHTML = "(Add Route)";
  	}
	else {
		ele.style.display = "block";
		text.innerHTML = "(Hide Route)";
	}
	
} 
function toggle4() {
	var ele = document.getElementById("toggleText4");
	var text = document.getElementById("displayText4");
	if(ele.style.display == "block") {
    		ele.style.display = "none";
		text.innerHTML = "(Add Route)";
  	}
	else {
		ele.style.display = "block";
		text.innerHTML = "(Hide Route)";
	}
	
} 
function toggle5() {
	var ele = document.getElementById("toggleText5");
	var text = document.getElementById("displayText5");
	if(ele.style.display == "block") {
    		ele.style.display = "none";
		text.innerHTML = "(Add Route)";
  	}
	else {
		ele.style.display = "block";
		text.innerHTML = "(Hide Route)";
	}
	
} 
function toggle6() {
	var ele = document.getElementById("toggleText6");
	var text = document.getElementById("displayText6");
	if(ele.style.display == "block") {
    		ele.style.display = "none";
		text.innerHTML = "(Add Route)";
  	}
	else {
		ele.style.display = "block";
		text.innerHTML = "(Hide Route)";
	}
	
} 

</script>
<div class="clear"></div>
<!-- start content-outer -->
<div id="content-outer">
<!-- start content -->
	<div id="content">
	<div id="page-heading"><h1><span style="color: #d74343; font-family: Tahoma; font-weight: bold;">Route Schedule</span></h1></div>
	<div id="page-heading" align="center" ><h3>Salesman Name: <span style=" color:#000000;"><?php echo $auSal[0]->salesman_name;?>,</span> From Date: <span style=" color:#000000;"><?php echo $_objAdmin->_changeDate($auSal[0]->from_date);?>,</span> To Date: <span style=" color:#000000;"><?php echo $_objAdmin->_changeDate($auSal[0]->to_date);?></span></h3></div>
		<table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">
		<tr>
			<th rowspan="3" class="sized"><img src="images/side_shadowleft.jpg" width="20" height="300" alt="" /></th>
			<th class="topleft"></th>
			<td id="tbl-border-top">&nbsp;</td>
			<th class="topright"></th>
			<th rowspan="3" class="sized"><img src="images/side_shadowright.jpg" width="20" height="300" alt="" /></th>
		</tr>
		<tr>
			<!--<td id="tbl-border-left"></td>-->
			<td>
			<!--  start content-table-inner -->
			<div id="content-table-inner">
				<table border="0" width="100%" cellpadding="0" cellspacing="0">
				<tr valign="top">
					<td>
					<!--  start message-red -->
					<?php if($err!=''){?>
					<div id="message-red">
						<table border="0" width="100%" cellpadding="0" cellspacing="0">
						<tr>
							<td class="red-left"> <?php echo $err; ?></td>
							<td class="red-right"><a class="close-red"><img src="images/icon_close_red.gif"   alt="" /></a></td>
						</tr>
						</table>
					</div>
					<?php } ?>
					<!--  end message-red -->
					<?php if($sus!=''){?>
					<!--  start message-green -->
					<div id="message-green">
						<table border="0" width="100%" cellpadding="0" cellspacing="0">
						<tr>
							<td class="green-left"><?php echo $sus; ?></td>
							<td class="green-right"><a class="close-green"><img src="images/icon_close_green.gif"   alt="" /></a></td>
						</tr>
						</table>
					</div>
					<?php } ?>
					<!--  end message-green -->
					
			<!-- start id-form -->
			<table border="0" cellpadding="0" cellspacing="0"  id="id-form" width="1000px" style="position:fixd;" >
				<td align="top " valign="top">
					<div id="related-act-top">
					Monday &thinsp;<a id="displayText" href="javascript:toggle();" class="add">(Add Route)</a>
					</div>
					<div style="width:235px; height:100px;overflow:auto;" >
					<table border="0" cellpadding="0" cellspacing="0"  id="id-form" style="border-left:2px solid #6E6E6E;border-right:2px solid #6E6E6E;" >
					<?php
					$auRut=$_objAdmin->_getSelectList('table_route_schedule_by_day as s left join table_route as r on s.Mon=r.route_id',"s.*,r.route_name",''," s.Mon!='' and r.status = 'A' and s.route_schedule_id=".$_SESSION['SalesmanSchedule']." and s.account_id=".$_SESSION['accountId']);
					if(is_array($auRut)){
					for($i=0;$i<count($auRut);$i++){
					?>
					<tr style="border-bottom:2px solid #6E6E6E;" >
						<td width="190px" style="padding:5px;" ><?php echo $auRut[$i]->route_name;?></td>
						<td style="padding:5px;"><a href="salesman_schedule.php?Mon=<?php echo $auRut[$i]->Mon;  ?>&id=<?php echo $auRut[$i]->id;  ?>" class="delete">Delete</a></td>
					</tr>
					<?php } } else {?>
					<tr style="border-bottom:2px solid #6E6E6E;" >
						<td width="215px" style="padding:5px;" >No route available in the day.</td>
						<td style="padding:5px;"></td>
					</tr>
					<?php } ?>
					
					</table>
					</div>
						<div>
							<div id="toggleText" style="display: none" >
								<form name="frmPre" id="frmPre" method="post" action="salesman_schedule.php" enctype="multipart/form-data" >
								<div><b>Route Name</b></div>
									<div style="width:200px; height:100px;overflow:auto;" >
									<table border="0" cellpadding="0" cellspacing="0"  id="id-form">
									<tr>
										<th valign="top"></th>
									</tr>
									<?php
									$auRut=$_objAdmin->_getSelectList('table_route AS r',"r.*",''," NOT EXISTS (SELECT * FROM table_route_schedule_by_day AS d WHERE r.route_id = d.Mon AND route_schedule_id=".$_SESSION['SalesmanSchedule'].") and r.status = 'A' and r.account_id=".$_SESSION['accountId']);
									if(is_array($auRut)){
									for($i=0;$i<count($auRut);$i++){
									?>
									<tr>
										<td><input type="checkbox" name="mon_route_id[]" value="<?php echo $auRut[$i]->route_id;?>" > <?php echo $auRut[$i]->route_name;?></td>
									<?php  } } else {?>
									</tr>
									<tr>
										<td><span style="color: #8B0000;"><b>All Route Added</b></td>
									</tr>
									<?php } ?>
									</tr>
									</table>
									</div>
									<?php if(is_array($auRut)){ ?>
									<div>
									<input name="account_id" type="hidden" value="<?php echo $_SESSION['accountId']; ?>" />
									<input name="route_schedule_id" type="hidden" value="<?php echo $_SESSION['SalesmanSchedule']; ?>" />
									<input name="addMon" type="hidden" value="yes" />
									<input name="submit" class="form-submit" type="submit" id="submit" value="Save" />
									</div>
									<?php } ?>
								</form>
							</div>
						</div>
				</td>
				<td align="top " valign="top">
					<div id="related-act-top">
					Tuesday &thinsp;<a id="displayText1" href="javascript:toggle1();" class="add">(Add Route)</a>
					</div>
					<div style="width:235px; height:100px;overflow:auto;" >
					<table border="0" cellpadding="0" cellspacing="0"  id="id-form" style="border-left:2px solid #6E6E6E;border-right:2px solid #6E6E6E;" >
					<?php
					$auRut=$_objAdmin->_getSelectList('table_route_schedule_by_day as s left join table_route as r on s.Tue=r.route_id',"s.*,r.route_name",''," s.Tue!='' and r.status = 'A' and s.route_schedule_id=".$_SESSION['SalesmanSchedule']." and s.account_id=".$_SESSION['accountId']);
					if(is_array($auRut)){
					for($i=0;$i<count($auRut);$i++){
					?>
					<tr style="border-bottom:2px solid #6E6E6E;" >
						<td width="190px" style="padding:5px;" ><?php echo $auRut[$i]->route_name;?></td>
						<td style="padding:5px;"><a href="salesman_schedule.php?Tue=<?php echo $auRut[$i]->Tue;  ?>&id=<?php echo $auRut[$i]->id;  ?>" class="delete">Delete</a></td>
					</tr>
					<?php } } else {?>
					<tr style="border-bottom:2px solid #6E6E6E;" >
						<td width="215px" style="padding:5px;" >No route available in the day.</td>
						<td style="padding:5px;"></td>
					</tr>
					<?php } ?>
					</table>
					</div>
						<div>
							<div id="toggleText1" style="display: none" >
								<form name="frmPre" id="frmPre" method="post" action="salesman_schedule.php" enctype="multipart/form-data" >
								<div><b>Route Name</b></div>
									<div style="width:200px; height:100px;overflow:auto;" >
									<table border="0" cellpadding="0" cellspacing="0"  id="id-form">
									<tr>
										<th valign="top"></th>
									</tr>
									<?php
									$auRut=$_objAdmin->_getSelectList('table_route AS r',"r.*",''," NOT EXISTS (SELECT * FROM table_route_schedule_by_day AS d WHERE r.route_id = d.Tue AND route_schedule_id=".$_SESSION['SalesmanSchedule'].") and r.status = 'A' and r.account_id=".$_SESSION['accountId']);
									if(is_array($auRut)){
									for($i=0;$i<count($auRut);$i++){
									?>
									<tr>
										<td><input type="checkbox" name="tue_route_id[]" value="<?php echo $auRut[$i]->route_id;?>" > <?php echo $auRut[$i]->route_name;?></td>
									<?php  } } else {?>
									</tr>
									<tr>
										<td><span style="color: #8B0000;"><b>All Route Added</b></td>
									</tr>
									<?php } ?>
									</tr>
									</table>
									</div>
									<?php if(is_array($auRut)){ ?>
									<div>
									<input name="account_id" type="hidden" value="<?php echo $_SESSION['accountId']; ?>" />
									<input name="route_schedule_id" type="hidden" value="<?php echo $_SESSION['SalesmanSchedule']; ?>" />
									<input name="addTue" type="hidden" value="yes" />
									<input name="submit" class="form-submit" type="submit" id="submit" value="Save" />
									</div>
									<?php } ?>
								</form>
							</div>
					</div>
				</td>
				
				<td align="top " valign="top">
					<div id="related-act-top">
					Wednesday &thinsp;<a id="displayText2" href="javascript:toggle2();" class="add">(Add Route)</a>
					</div>
					<div style="width:235px; height:100px;overflow:auto;" >
					<table border="0" cellpadding="0" cellspacing="0"  id="id-form" style="border-left:2px solid #6E6E6E;border-right:2px solid #6E6E6E;" >
					<?php
					$auRut=$_objAdmin->_getSelectList('table_route_schedule_by_day as s left join table_route as r on s.Wed=r.route_id',"s.*,r.route_name",''," s.Wed!='' and r.status = 'A' and s.route_schedule_id=".$_SESSION['SalesmanSchedule']." and s.account_id=".$_SESSION['accountId']);
					if(is_array($auRut)){
					for($i=0;$i<count($auRut);$i++){
					?>
					<tr style="border-bottom:2px solid #6E6E6E;" >
						<td width="190px" style="padding:5px;" ><?php echo $auRut[$i]->route_name;?></td>
						<td style="padding:5px;"><a href="salesman_schedule.php?Wed=<?php echo $auRut[$i]->Wed;  ?>&id=<?php echo $auRut[$i]->id;  ?>" class="delete">Delete</a></td>
					</tr>
					<?php } } else {?>
					<tr style="border-bottom:2px solid #6E6E6E;" >
						<td width="215px" style="padding:5px;" >No route available in the day.</td>
						<td style="padding:5px;"></td>
					</tr>
					<?php } ?>
					</table>
					</div>
					<div>
							<div id="toggleText2" style="display: none" >
								<form name="frmPre" id="frmPre" method="post" action="salesman_schedule.php" enctype="multipart/form-data" >
								<div><b>Route Name</b></div>
									<div style="width:200px; height:100px;overflow:auto;" >
									<table border="0" cellpadding="0" cellspacing="0"  id="id-form">
									<tr>
										<th valign="top"></th>
									</tr>
									<?php
									$auRut=$_objAdmin->_getSelectList('table_route AS r',"r.*",''," NOT EXISTS (SELECT * FROM table_route_schedule_by_day AS d WHERE r.route_id = d.Wed AND route_schedule_id=".$_SESSION['SalesmanSchedule'].") and r.status = 'A' and r.account_id=".$_SESSION['accountId']);
									if(is_array($auRut)){
									for($i=0;$i<count($auRut);$i++){
									?>
									<tr>
										<td><input type="checkbox" name="wed_route_id[]" value="<?php echo $auRut[$i]->route_id;?>" > <?php echo $auRut[$i]->route_name;?></td>
									<?php  } } else {?>
									</tr>
									<tr>
										<td><span style="color: #8B0000;"><b>All Route Added</b></td>
									</tr>
									<?php } ?>
									</tr>
									</table>
									</div>
									<?php if(is_array($auRut)){ ?>
									<div>
									<input name="account_id" type="hidden" value="<?php echo $_SESSION['accountId']; ?>" />
									<input name="route_schedule_id" type="hidden" value="<?php echo $_SESSION['SalesmanSchedule']; ?>" />
									<input name="addWed" type="hidden" value="yes" />
									<input name="submit" class="form-submit" type="submit" id="submit" value="Save" />
									</div>
									<?php } ?>
								</form>
							</div>
					</div>
				</td>
			</tr>
			<tr>
				<td align="top " valign="top">
					<div id="related-act-top">
					Thursday &thinsp;<a id="displayText3" href="javascript:toggle3();" class="add">(Add Route)</a>
					</div>
					<div style="width:235px; height:100px;overflow:auto;" >
					<table border="0" cellpadding="0" cellspacing="0"  id="id-form" style="border-left:2px solid #6E6E6E;border-right:2px solid #6E6E6E;" >
					<?php
					$auRut=$_objAdmin->_getSelectList('table_route_schedule_by_day as s left join table_route as r on s.Thu=r.route_id',"s.*,r.route_name",''," s.Thu!='' and r.status = 'A' and s.route_schedule_id=".$_SESSION['SalesmanSchedule']." and s.account_id=".$_SESSION['accountId']);
					if(is_array($auRut)){
					for($i=0;$i<count($auRut);$i++){
					?>
					<tr style="border-bottom:2px solid #6E6E6E;" >
						<td width="190px" style="padding:5px;" ><?php echo $auRut[$i]->route_name;?></td>
						<td style="padding:5px;"><a href="salesman_schedule.php?Thu=<?php echo $auRut[$i]->Thu;  ?>&id=<?php echo $auRut[$i]->id;?>" class="delete">Delete</a></td>
					</tr>
					<?php } } else {?>
					<tr style="border-bottom:2px solid #6E6E6E;" >
						<td width="215px" style="padding:5px;" >No route available in the day.</td>
						<td style="padding:5px;"></td>
					</tr>
					<?php } ?>
					</table>
					</div>
					<div>
						
							<div id="toggleText3" style="display: none" >
								<form name="frmPre" id="frmPre" method="post" action="salesman_schedule.php" enctype="multipart/form-data" >
								<div><b>Route Name</b></div>
									<div style="width:200px; height:100px;overflow:auto;" >
									<table border="0" cellpadding="0" cellspacing="0"  id="id-form">
									<tr>
										<th valign="top"></th>
									</tr>
									<?php
									$auRut=$_objAdmin->_getSelectList('table_route AS r',"r.*",''," NOT EXISTS (SELECT * FROM table_route_schedule_by_day AS d WHERE r.route_id = d.Thu AND route_schedule_id=".$_SESSION['SalesmanSchedule'].") and r.status = 'A' and r.account_id=".$_SESSION['accountId']);
									if(is_array($auRut)){
									for($i=0;$i<count($auRut);$i++){
									?>
									<tr>
										<td><input type="checkbox" name="thu_route_id[]" value="<?php echo $auRut[$i]->route_id;?>" > <?php echo $auRut[$i]->route_name;?></td>
									<?php  } } else {?>
									</tr>
									<tr>
										<td><span style="color: #8B0000;"><b>All Route Added</b></td>
									</tr>
									<?php } ?>
									</tr>
									</table>
									</div>
									<div>
									<?php if(is_array($auRut)){ ?>
									<input name="account_id" type="hidden" value="<?php echo $_SESSION['accountId']; ?>" />
									<input name="route_schedule_id" type="hidden" value="<?php echo $_SESSION['SalesmanSchedule']; ?>" />
									<input name="addThu" type="hidden" value="yes" />
									<input name="submit" class="form-submit" type="submit" id="submit" value="Save" />
									</div>
									<?php } ?>
								</form>
							</div>
					</div>
				</td>
				<td align="top " valign="top">
					<div id="related-act-top">
					Friday &thinsp;<a id="displayText4" href="javascript:toggle4();" class="add">(Add Route)</a>
					</div>
					<div style="width:235px; height:100px;overflow:auto;" >
					<table border="0" cellpadding="0" cellspacing="0"  id="id-form" style="border-left:2px solid #6E6E6E;border-right:2px solid #6E6E6E;" >
					<?php
					$auRut=$_objAdmin->_getSelectList('table_route_schedule_by_day as s left join table_route as r on s.Fri=r.route_id',"s.*,r.route_name",''," s.Fri!='' and r.status = 'A' and s.route_schedule_id=".$_SESSION['SalesmanSchedule']." and s.account_id=".$_SESSION['accountId']);
					if(is_array($auRut)){
					for($i=0;$i<count($auRut);$i++){
					?>
					<tr style="border-bottom:2px solid #6E6E6E;" >
						<td width="190px" style="padding:5px;" ><?php echo $auRut[$i]->route_name;?></td>
						<td style="padding:5px;"><a href="salesman_schedule.php?Fri=<?php echo $auRut[$i]->Fri;  ?>&id=<?php echo $auRut[$i]->id;?>" class="delete">Delete</a></td>
					</tr>
					<?php } } else {?>
					<tr style="border-bottom:2px solid #6E6E6E;" >
						<td width="215px" style="padding:5px;" >No route available in the day.</td>
						<td style="padding:5px;"></td>
					</tr>
					<?php } ?>
					</table>
					</div>
					<div>
						
							<div id="toggleText4" style="display: none" >
								<form name="frmPre" id="frmPre" method="post" action="salesman_schedule.php" enctype="multipart/form-data" >
								<div><b>Route Name</b></div>
									<div style="width:200px; height:100px;overflow:auto;" >
									<table border="0" cellpadding="0" cellspacing="0"  id="id-form">
									<tr>
										<th valign="top"></th>
									</tr>
									<?php
									$auRut=$_objAdmin->_getSelectList('table_route AS r',"r.*",''," NOT EXISTS (SELECT * FROM table_route_schedule_by_day AS d WHERE r.route_id = d.Fri AND route_schedule_id=".$_SESSION['SalesmanSchedule'].") and r.status = 'A' and r.account_id=".$_SESSION['accountId']);
									if(is_array($auRut)){
									for($i=0;$i<count($auRut);$i++){
									?>
									<tr>
										<td><input type="checkbox" name="fri_route_id[]" value="<?php echo $auRut[$i]->route_id;?>" > <?php echo $auRut[$i]->route_name;?></td>
									<?php  } } else {?>
									</tr>
									<tr>
										<td><span style="color: #8B0000;"><b>All Route Added</b></td>
									</tr>
									<?php } ?>
									</table>
									</div>
									<?php if(is_array($auRut)){ ?>
									<div>
									<input name="account_id" type="hidden" value="<?php echo $_SESSION['accountId']; ?>" />
									<input name="route_schedule_id" type="hidden" value="<?php echo $_SESSION['SalesmanSchedule']; ?>" />
									<input name="addFri" type="hidden" value="yes" />
									<input name="submit" class="form-submit" type="submit" id="submit" value="Save" />
									</div>
									<?php } ?>
								</form>
							</div>
					</div>
				</td>
				<td align="top " valign="top">
					<div id="related-act-top">
					Saturday &thinsp;<a id="displayText5" href="javascript:toggle5();" class="add">(Add Route)</a>
					</div>
					<div style="width:235px; height:100px;overflow:auto;" >
					<table border="0" cellpadding="0" cellspacing="0"  id="id-form" style="border-left:2px solid #6E6E6E;border-right:2px solid #6E6E6E;" >
					<?php
					$auRut=$_objAdmin->_getSelectList('table_route_schedule_by_day as s left join table_route as r on s.Sat=r.route_id',"s.*,r.route_name",''," s.Sat!='' and r.status = 'A' and s.route_schedule_id=".$_SESSION['SalesmanSchedule']." and s.account_id=".$_SESSION['accountId']);
					if(is_array($auRut)){
					for($i=0;$i<count($auRut);$i++){
					?>
					<tr style="border-bottom:2px solid #6E6E6E;" >
						<td width="190px" style="padding:5px;" ><?php echo $auRut[$i]->route_name;?></td>
						<td style="padding:5px;"><a href="salesman_schedule.php?Sat=<?php echo $auRut[$i]->Sat;  ?>&id=<?php echo $auRut[$i]->id;?>" class="delete">Delete</a></td>
					</tr>
					<?php } } else {?>
					<tr style="border-bottom:2px solid #6E6E6E;" >
						<td width="215px" style="padding:5px;" >No route available in the day.</td>
						<td style="padding:5px;"></td>
					</tr>
					<?php } ?>
					</table>
					</div>
					<div>
						
							<div id="toggleText5" style="display: none" >
								<form name="frmPre" id="frmPre" method="post" action="salesman_schedule.php" enctype="multipart/form-data" >
								<div><b>Route Name</b></div>
									<div style="width:200px; height:100px;overflow:auto;" >
									<table border="0" cellpadding="0" cellspacing="0"  id="id-form">
									<tr>
										<th valign="top"></th>
									</tr>
									<?php
									$auRut=$_objAdmin->_getSelectList('table_route AS r',"r.*",''," NOT EXISTS (SELECT * FROM table_route_schedule_by_day AS d WHERE r.route_id = d.Sat AND route_schedule_id=".$_SESSION['SalesmanSchedule'].") and r.status = 'A' and r.account_id=".$_SESSION['accountId']);
									if(is_array($auRut)){
									for($i=0;$i<count($auRut);$i++){
									?>
									<tr>
										<td><input type="checkbox" name="sat_route_id[]" value="<?php echo $auRut[$i]->route_id;?>" > <?php echo $auRut[$i]->route_name;?></td>
									<?php  } } else {?>
									</tr>
									<tr>
										<td><span style="color: #8B0000;"><b>All Route Added</b></td>
									</tr>
									<?php } ?>
									</tr>
									</table>
									</div>
									<?php if(is_array($auRut)){ ?>
									<div>
									<input name="account_id" type="hidden" value="<?php echo $_SESSION['accountId']; ?>" />
									<input name="route_schedule_id" type="hidden" value="<?php echo $_SESSION['SalesmanSchedule']; ?>" />
									<input name="addSat" type="hidden" value="yes" />
									<input name="submit" class="form-submit" type="submit" id="submit" value="Save" />
									</div>
									<?php } ?>
								</form>
							</div>
					</div>
				</td>
			</tr>
			<tr>
				<td align="top " valign="top">
					<div id="related-act-top">
					Sunday &thinsp;<a id="displayText6" href="javascript:toggle6();" class="add">(Add Route)</a>
					</div>
					<div style="width:235px; height:100px;overflow:auto;" >
					<table border="0" cellpadding="0" cellspacing="0"  id="id-form" style="border-left:2px solid #6E6E6E;border-right:2px solid #6E6E6E;" >
					<?php
					$auRut=$_objAdmin->_getSelectList('table_route_schedule_by_day as s left join table_route as r on s.Sun=r.route_id',"s.*,r.route_name",''," s.Sun!='' and r.status = 'A' and s.route_schedule_id=".$_SESSION['SalesmanSchedule']." and s.account_id=".$_SESSION['accountId']);
					if(is_array($auRut)){
					for($i=0;$i<count($auRut);$i++){
					?>
					<tr style="border-bottom:2px solid #6E6E6E;" >
						<td width="190px" style="padding:5px;" ><?php echo $auRut[$i]->route_name;?></td>
						<td style="padding:5px;"><a href="salesman_schedule.php?Sun=<?php echo $auRut[$i]->Sun;  ?>&id=<?php echo $auRut[$i]->id;?>" class="delete">Delete</a></td>
					</tr>
					<?php } } else {?>
					<tr style="border-bottom:2px solid #6E6E6E;" >
						<td width="215px" style="padding:5px;" >No route available in the day.</td>
						<td style="padding:5px;"></td>
					</tr>
					<?php } ?>
					</table>
					</div>
					<div>
							<div id="toggleText6" style="display: none" >
								<form name="frmPre" id="frmPre" method="post" action="salesman_schedule.php" enctype="multipart/form-data" >
								<div><b>Route Name</b></div>
									<div style="width:200px; height:100px;overflow:auto;" >
									<table border="0" cellpadding="0" cellspacing="0"  id="id-form">
									<tr>
										<th valign="top"></th>
									</tr>
									<?php
									$auRut=$_objAdmin->_getSelectList('table_route AS r',"r.*",''," NOT EXISTS (SELECT * FROM table_route_schedule_by_day AS d WHERE r.route_id = d.Sun AND route_schedule_id=".$_SESSION['SalesmanSchedule'].") and r.status = 'A' and r.account_id=".$_SESSION['accountId']);
									if(is_array($auRut)){
									for($i=0;$i<count($auRut);$i++){
									?>
									<tr>
										<td><input type="checkbox" name="sun_route_id[]" value="<?php echo $auRut[$i]->route_id;?>" > <?php echo $auRut[$i]->route_name;?></td>
									<?php  } } else {?>
									</tr>
									<tr>
										<td><span style="color: #8B0000;"><b>All Route Added</b></td>
									</tr>
									<?php } ?>
									</tr>
									</table>
									</div>
									<?php 	if(is_array($auRut)){ ?>
									<div>
									<input name="account_id" type="hidden" value="<?php echo $_SESSION['accountId']; ?>" />
									<input name="route_schedule_id" type="hidden" value="<?php echo $_SESSION['SalesmanSchedule']; ?>" />
									<input name="addSun" type="hidden" value="yes" />
									<input name="submit" class="form-submit" type="submit" id="submit" value="Save" />
									</div>
									<?php } ?>
								</form>
							</div>
					</div>
				</td>
			</tr>
			
			<tr>
				<th>&nbsp;</th>
				<td valign="top">
					<?php if ($_SESSION['Schedule']!=''){ ?>
					<input type="button" value="Back" class="form-submit" onclick="location.href='salesman.php';" />
					<?php } else { ?>
					<input type="button" value="Back" class="form-submit" onclick="location.href='route_schedule.php';" />
					<?php } ?>
				</td>
			</tr>
			</table>
			<!-- end id-form  -->
			</td>
			<td>
			<!-- right bar-->
			<div class="clear"></div>
		</div>
<!-- end related-act-bottom -->
</div>
<!-- end related-activities -->
</td>
</tr>
<tr>
<td><img src="images/blank.gif" width="695" height="1" alt="blank" /></td>
<td></td>
</tr>
</table>
<div class="clear"></div>
</div>
<!--  end content-table-inner  -->
</td>
<td id="tbl-border-right"></td>
</tr>
<!--<tr>
	<th class="sized bottomleft"></th>
	<td id="tbl-border-bottom">&nbsp;</td>
	<th class="sized bottomright"></th>
</tr>-->
</table>
<div class="clear">&nbsp;</div>

</div>
<!--  end content -->
<div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->
<div class="clear">&nbsp;</div>
    
<!-- start footer --> 
<?php include("footer.php") ?>  
<!-- end footer -->
</body>
</html>