<?php include("includes/config.inc.php");
      include("includes/function.php");
	  include("includes/globalarraylist.php");

	if(isset($_POST['showOrderlist']) && $_POST['showOrderlist'] == 'yes') {	

		if($_POST['sal']!="")  {
			$_SESSION['monthSalesmanID']=$_POST['sal'];	
			} else {
			unset($_SESSION['monthSalesmanID']);
		}

		if($_POST['month']!="" && is_numeric($_POST['month'])) {
			$_SESSION['dismonth']=$_POST['month'];	
		}
		
		if($_POST['year']!="" && is_numeric($_POST['year'])) {
			$_SESSION['disCyear']=$_POST['year'];	
		}

		if($_POST['sal']=='All') {
		  unset($_SESSION['monthSalesmanID']);	
		}

		header("Location: salesman_summary.php");
	}
		
		
		if($_SESSION['disCyear']==''){
			$_SESSION['disCyear']=date('Y');
		}

		if($_SESSION['dismonth']==''){
			$_SESSION['dismonth']=date('m');
		}

	if(isset($_REQUEST['reset']) && $_REQUEST['reset'] == 'yes'){
		 unset($_SESSION['monthSalesmanID']);	 	
		 $_SESSION['dismonth']= '';
		 $_SESSION['disCyear']= '';
		 header("Location: salesman_summary.php");
	}


	// Get All the categories 

	$categotyList = $_objAdmin->_getSelectList('table_category','category_id, category_name','',""); 



?>

<?php include("header.inc.php") ?>
<script type="text/javascript">

	var tableToExcel = (function () {
        var uri = 'data:application/vnd.ms-excel;base64,'
        , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table><tr><td></td></tr></table><table>{table}</table></body></html>'
        , base64 = function (s) { return window.btoa(unescape(encodeURIComponent(s))) }
        , format = function (s, c) { return s.replace(/{(\w+)}/g, function (m, p) { return c[p]; }) }
        return function (table, name, filename) {
        	//alert(table.name);
		  
            if (!table.nodeType) table = document.getElementById(table)
            var ctx = { worksheet: name || 'Worksheet', table: table.innerHTML }

            document.getElementById("dlink").href = uri + base64(format(template, ctx));
            document.getElementById("dlink").download = filename;
            document.getElementById("dlink").click();

        }
    })()	

</script>
<?php //echo "<pre>"; print_r($_SESSION);?>
<!-- start content-outer -->
<input name="pagename" type="hidden"  id="pagename" value="salesman_summary.php" />
<div id="content-outer">
<!-- start content -->
<div id="content">
<div id="page-heading"><h1>
	<span style="color: #d74343; font-family: Tahoma; font-weight: bold;">Salesman Summary</span></h1></div>
	<table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">
	<tr>
		<!--<td id="tbl-border-left"></td>-->
		<td>
		<!--  start content-table-inner -->
	<div id="content-table-inner">
	
	<div id="page-heading" align="left" >
	<?php //echo "<pre>";
		 // print_r($_SESSION);
	?>
	<form name="frmPre" id="frmPre" method="post" action="" enctype="multipart/form-data" >
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
	
	<tr>
	<td id="distributorMenu">
		<h3>Salesmen: </h3>
		<h6><select name="sal" id="sal" class="menulist">
			<?php echo $salesmanOptionList = $_objArrayList->GetSalesmenMenu($salsList, $_SESSION['monthSalesmanID'],'root');?>
		</select></h6>
	</td>

	<td><?php echo $months = $_objArrayList->getMonthList($_SESSION['dismonth']);?></td>
	<td><?php echo $year = $_objArrayList->getYearList($_SESSION['disCyear']);?></td>

	<td><h3></h3>
	<input name="submit" class="result-submit" type="submit" id="submit" value="View Details" />
	<input type="button" value="Reset!" class="form-reset" onclick="location.href='salesman_summary.php?reset=yes';" />
	</td></tr>

	<tr>
	<td colspan="5"><!-- <a id="dlink"  style="display:none;"></a><input name="export" class="result-submit" type="button" value="Export to Excel" onclick="tableToExcel('report_export', 'Salesman Summary', 'Salesman Summary.xls');"> -->

	<a id="dlink"  style="display:none;"></a>
	<input type="button" name="submit" value="Export to Excel" class="result-submit" onclick="tableToExcel('report_export', 'Salesman Summary', 'Salesman Summary.xls');" >



	</td>


	<td></td>
	</tr>

	<tr>
	<td colspan="7"><input name="showOrderlist" type="hidden" value="yes" /></td>
	</tr>
	</table>


	</form>

	</div>
	
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
	<tr valign="top">
	<td>
		<!-- start id-form -->
	<div id="Report" style="width:980px; overflow:scroll;">
		<table border="1" width="100%" cellpadding="0" cellspacing="0"  id="report_export" name="report_export">
			<tr  bgcolor="#6E6E6E" style="color: #fff;font-weight: bold;" >
				<td style="white-space: nowrap; margin: 10px; padding: 10px; font-family: monospace;">Salesman </td>
				<td style="white-space: nowrap; margin: 10px; padding: 10px; font-family: monospace;">Salesman Code </td>
				<td style="white-space: nowrap; margin: 10px; padding: 10px; font-family: monospace;"> Date</td>
				<td style="white-space: nowrap; margin: 10px; padding: 10px; font-family: monospace;">Route Name</td>
				<td style=" margin: 10px; padding: 10px; font-family: monospace;">No. Customer To be Visited  </td>
				<td style=" margin: 10px; padding: 10px; font-family: monospace;">No. of Customer Visited </td>
				<td style=" margin: 10px; padding: 10px; font-family: monospace;">Customer Type </td>
				<td style=" margin: 10px; padding: 10px; font-family: monospace;">No. of Customer Placed Order</td>
				<!-- <td style=" margin: 10px; padding: 10px; font-family: monospace;">No. of New Outlet Surveyed </td>
				<td style=" margin: 10px; padding: 10px; font-family: monospace;">No. of New Outlet Assigned </td>
				<td style=" margin: 10px; padding: 10px; font-family: monospace;">No. of Interested Outlets </td>
				<td style=" margin: 10px; padding: 10px; font-family: monospace;">Beat Adherance </td>
				<td style=" margin: 10px; padding: 10px; font-family: monospace;">Beat Productivity </td> -->
				<td style="margin: 10px; padding: 10px; font-family: monospace;"> No. of Customer Not Placed Order in Last 30 Days </td>
				<td style="padding:10px;" align="center"  width="100%">
					<table>
						<tr>
							<?php foreach ($categotyList as $key => $value) {?>
								 <td width="150px;"  style=" margin: 10px; padding: 10px; font-family: monospace;"><?php echo $value->category_name;?><br>(Qty/Value)</td>
							<?php }?>
						</tr>
					</table>
				</td>
				<td style="padding:10px;" align="center" width="15%">TLSD</td>
			</tr>
			<?php




			// $routeRetailerDistributorOfSalesman =$_objAdmin->_getSelectList('`table_route_scheduled` AS RS
			// LEFT JOIN table_route_schedule_details AS RSD ON RSD.`route_schedule_id` = RS.`route_schedule_id`
			// LEFT JOIN table_route_retailer AS RR ON RR.route_id = RSD.route_id
			// LEFT JOIN table_retailer AS RET ON RET.retailer_id = RR.retailer_id AND RR.status = "R" AND DATE_FORMAT(RET.start_date, "%e") < RSD.assign_day
			// LEFT JOIN table_distributors AS D ON D.distributor_id = RR.retailer_id AND RR.status = "D" AND DATE_FORMAT(D.start_date, "%e") < RSD.assign_day
			// LEFT JOIN table_route AS R ON R.route_id = RSD.route_id
			// LEFT JOIN table_salesman AS s ON s.salesman_id = RS.salesman_id',
			// "RSD.route_id, R.route_name, RS.salesman_id, s.salesman_name, CONCAT(RS.year,'-',RS.month,'-',RSD.assign_day) AS route_date, RS.month, RS.year, RSD.assign_day,COUNT(RR.status) AS ttlRoutePersons, COUNT(DISTINCT(RET.retailer_id)) AS ttlRouteRetailers, COUNT(DISTINCT(D.distributor_id)) AS ttlRouteDistributors, RR.status",'',
			// " RS.salesman_id = '".$_SESSION['monthSalesmanID']."' AND RS.month = '".$_SESSION['dismonth']."' AND RS.year = '".$_SESSION['disCyear']."' GROUP BY RSD.route_id,RS.salesman_id,RSD.assign_day,RR.status ORDER BY RSD.assign_day ASC");

			//AND DATE_FORMAT(RET.start_date, "%Y-%m-%d") < DATE_FORMAT(CONCAT(RS.year,"-",RS.month,"-",RSD.assign_day),"%Y-%m-%d")


			//AND DATE_FORMAT(D.start_date, "%Y-%m-%d") < DATE_FORMAT(CONCAT(RS.year,"-",RS.month,"-",RSD.assign_day),"%Y-%m-%d")

			$routeRetailerDistributorOfSalesman =$_objAdmin->_getSelectList('`table_route_scheduled` AS RS
			LEFT JOIN table_route_schedule_details AS RSD ON RSD.`route_schedule_id` = RS.`route_schedule_id`
			LEFT JOIN table_route_retailer AS RR ON RR.route_id = RSD.route_id AND RR.status IN ("R", "D")
			LEFT JOIN table_retailer AS RET ON RET.retailer_id = RR.retailer_id AND RR.status = "R" 

			LEFT JOIN table_distributors AS D ON D.distributor_id = RR.retailer_id AND RR.status = "D" 

			LEFT JOIN table_route AS R ON R.route_id = RSD.route_id
			LEFT JOIN table_salesman AS s ON s.salesman_id = RS.salesman_id',
			"RSD.route_id, R.route_name, RS.salesman_id, s.salesman_name,s.salesman_code, CONCAT(RS.year,'-',RS.month,'-',RSD.assign_day) AS route_date, RS.month, RS.year, RSD.assign_day,COUNT(RR.status) AS ttlRoutePersons, COUNT(DISTINCT(RET.retailer_id)) AS ttlRouteRetailers, COUNT(DISTINCT(D.distributor_id)) AS ttlRouteDistributors, RR.status",'',
			" RS.salesman_id = '".$_SESSION['monthSalesmanID']."' AND RS.month = '".$_SESSION['dismonth']."' AND RS.year = '".$_SESSION['disCyear']."' AND R.options ='I' GROUP BY RSD.route_id,RS.salesman_id,RSD.assign_day,RR.status ORDER BY RSD.assign_day ASC");


			/*echo "<pre>";
			echo print_r($routeRetailerDistributorOfSalesman);
			echo "</pre>";
			exit;*/

			$routeRetailerDistributorOfSalesmanOrderCategoryWise = $_objAdmin->_getSelectList('table_order AS O 
				LEFT JOIN table_order_detail AS OD ON OD.order_id = O.order_id 
				LEFT JOIN table_retailer AS RET ON RET.retailer_id = O.retailer_id AND RET.new = ""
				LEFT JOIN table_distributors AS D ON D.distributor_id = O.distributor_id AND D.new = ""
				LEFT JOIN table_item AS I ON I.item_id = OD.item_id',
				"O.salesman_id, O.date_of_order, I.category_id, SUM(OD.acc_quantity) AS ttlCategoryQty, SUM(OD.acc_total) AS ttlCategoryAmt,O.order_type, CASE WHEN (O.retailer_id > 0) THEN 'R' WHEN (O.retailer_id = 0)  THEN 'D' END AS type_status ",''," O.salesman_id = '".$_SESSION['monthSalesmanID']."' AND DATE_FORMAT(O.date_of_order,'%Y') = '".$_SESSION['disCyear']."' AND DATE_FORMAT(O.date_of_order,'%m') = '".$_SESSION['dismonth']."' AND order_type IN ('Yes') GROUP BY O.date_of_order, O.salesman_id,I.category_id, type_status");

			// echo "<pre>";
			// echo print_r($routeRetailerDistributorOfSalesmanOrderCategoryWise);
			// echo "</pre>";
			// exit;


// SELECT COUNT( O.order_id ) AS ttlOrders, COUNT( O2.order_id ) AS ttlProductiveCalls, COUNT( SUR.survey_id ) AS ttlSurvey, COUNT( R.retailer_id ) AS ttlAddedRet, COUNT( R2.retailer_id ) AS ttlInterestedCustomer, A.salesman_id, A.activity_date
// FROM table_activity AS A
// LEFT JOIN table_order AS O ON O.order_id = A.ref_id
// AND O.order_type
// IN (
// 'Yes', 'No'
// )
// AND O.retailer_id >0
// AND A.activity_type =3
// LEFT JOIN table_order AS O2 ON O2.order_id = A.ref_id
// AND O2.order_type
// IN (
// 'Yes'
// )
// AND O.retailer_id >0
// AND A.activity_type =3
// LEFT JOIN table_survey AS SUR ON SUR.survey_id = A.ref_id
// AND A.activity_type =4
// LEFT JOIN table_retailer AS R ON R.retailer_id = A.ref_id
// AND A.activity_type =5
// LEFT JOIN table_retailer AS R2 ON R2.retailer_id = A.ref_id
// AND A.activity_type =5
// AND R2.display_outlet = 'hot'
// WHERE A.salesman_id = '109'
// AND DATE_FORMAT( A.activity_date, '%Y' ) = '2015'
// AND DATE_FORMAT( A.activity_date, '%m' ) = '10'
// GROUP BY A.activity_date, A.salesman_id


			$routeRetailerOfSalesmanActivity = array();
			$routeDistributorOfSalesmanActivity = array();

			$dataSetOfRetailerSalesmanActivity = array();
			$dataSetOfDistributorSalesmanActivity = array();



	// Retailer Orders, Survey & Added new retailers


	$routeRetailerOfSalesmanActivity = $_objAdmin->_getSelectList2("view_orderOrRetDisAddActivity AS A 

LEFT JOIN table_route_scheduled AS RS ON RS.salesman_id = A.salesman_id AND DATE_FORMAT(A.activity_date, '%Y-%c') = CONCAT(RS.year,'-',RS.month) 
LEFT JOIN table_route_schedule_details AS RSD ON RSD.route_schedule_id = RS.route_schedule_id AND RSD.assign_day = DATE_FORMAT(A.activity_date, '%e')

	LEFT JOIN table_order AS O ON O.order_id = A.ref_id AND O.order_type IN ('Yes','No')  AND O.retailer_id >0  AND A.activity_type = 3 
	LEFT JOIN table_retailer AS R3 ON R3.retailer_id = O.retailer_id AND R3.start_date < O.date_of_order 

	LEFT JOIN table_route_retailer AS RR ON RR.route_id = RSD.route_id AND RR.status = 'R' AND RR.retailer_id = R3.retailer_id AND A.activity_type = 3

	LEFT JOIN table_order AS O2 ON O2.order_id = A.ref_id AND O2.order_type IN ('Yes')  AND O2.retailer_id >0 AND A.activity_type = 3 AND O2.retailer_id = RR.retailer_id
	LEFT JOIN table_retailer AS R4 ON R4.retailer_id = O2.retailer_id AND R4.start_date < O2.date_of_order 

		LEFT JOIN table_route_retailer AS RR2 ON RR2.route_id = RSD.route_id AND RR2.status = 'R' AND RR2.retailer_id = R4.retailer_id AND A.activity_type = 3 

	LEFT JOIN table_retailer AS R ON R.retailer_id = A.ref_id AND A.activity_type = 5
	LEFT JOIN table_route_retailer AS RR3 ON RR3.route_id = RSD.route_id AND RR3.status = 'R' AND RR3.retailer_id = R.retailer_id
	LEFT JOIN table_retailer AS R2 ON R2.retailer_id = A.ref_id AND A.activity_type =5",
	"RSD.route_id, COUNT(DISTINCT(RR.retailer_id)) AS plannedRouteRetailerTotalCalls,  COUNT(DISTINCT(RR2.retailer_id)) AS plannedRouteRetailerProductiveCalls,  COUNT(DISTINCT(R3.retailer_id)) AS ttlOrders, COUNT(DISTINCT(R4.retailer_id)) AS ttlProductiveCalls, COUNT(R.retailer_id) AS ttlAddedRet,COUNT(RR3.retailer_id) AS ttlAddedRetInthisRoute,  COUNT(R2.retailer_id) AS ttlInterestedCustomer, A.salesman_id, A.activity_date", ''," A.salesman_id = '".$_SESSION['monthSalesmanID']."' AND DATE_FORMAT(A.activity_date,'%Y') = '".$_SESSION['disCyear']."' AND DATE_FORMAT(A.activity_date,'%m') = '".$_SESSION['dismonth']."' AND A.activity_type IN (3,5) AND RSD.route_id IS NOT NULL  GROUP BY A.activity_date, A.salesman_id, RSD.route_id");


	
	// echo "<pre>";
	// print_r($routeRetailerOfSalesmanActivity);
	//exit;



	/*$summary = $_objAdmin->_getSelectList2("55 view_orderOrRetDisAddActivity AS A 
		LEFT JOIN table_route_scheduled AS RS ON RS.salesman_id = A.salesman_id AND DATE_FORMAT(A.activity_date, '%Y-%c') = CONCAT(RS.year,'-',RS.month) 
LEFT JOIN table_route_schedule_details AS RSD ON RSD.route_schedule_id = RS.route_schedule_id AND RSD.assign_day = DATE_FORMAT(A.activity_date, '%e')
	LEFT JOIN table_retailer AS R ON R.retailer_id = A.ref_id AND A.activity_type = 5
	LEFT JOIN table_retailer AS R2 ON R2.retailer_id = A.ref_id AND A.activity_type =5 AND R2.display_outlet = 'hot' ",
	"RSD.route_id,COUNT(R.retailer_id) AS ttlAddedRet,  COUNT(R2.retailer_id) AS ttlInterestedCustomer, A.salesman_id, A.activity_date", ''," A.salesman_id = '".$_SESSION['monthSalesmanID']."' AND DATE_FORMAT(A.activity_date,'%Y') = '".$_SESSION['disCyear']."' AND DATE_FORMAT(A.activity_date,'%m') = '".$_SESSION['dismonth']."' AND A.activity_type IN (3,5)   GROUP BY A.activity_date, A.salesman_id, RSD.route_id");
	echo "<pre>";
	print_r($summary);
	die;*/
	
	








	// $routeRetailerOfSalesmanActivity = $_objAdmin->_getSelectList2("table_activity AS A 
	// LEFT JOIN table_order AS O ON O.order_id = A.ref_id AND O.order_type IN ('Yes','No')  AND O.retailer_id >0  AND A.activity_type = 3 
	// LEFT JOIN table_retailer AS R3 ON R3.retailer_id = O.retailer_id AND R3.start_date < O.date_of_order

	// LEFT JOIN table_order AS O2 ON O2.order_id = A.ref_id AND O2.order_type IN ('Yes')  AND O2.retailer_id >0 AND A.activity_type = 3
	// LEFT JOIN table_retailer AS R4 ON R4.retailer_id = O2.retailer_id AND R4.start_date < O2.date_of_order

	// LEFT JOIN table_retailer AS R ON R.retailer_id = A.ref_id AND A.activity_type = 5
	// LEFT JOIN table_retailer AS R2 ON R2.retailer_id = A.ref_id AND A.activity_type =5 AND R2.display_outlet = 'hot' ",

	// "COUNT(DISTINCT(R3.retailer_id)) AS ttlOrders, COUNT(DISTINCT(R4.retailer_id)) AS ttlProductiveCalls, COUNT(R.retailer_id) AS ttlAddedRet,  COUNT(R2.retailer_id) AS ttlInterestedCustomer, A.salesman_id, A.activity_date", ''," A.salesman_id = '".$_SESSION['monthSalesmanID']."' AND DATE_FORMAT(A.activity_date,'%Y') = '".$_SESSION['disCyear']."' AND DATE_FORMAT(A.activity_date,'%m') = '".$_SESSION['dismonth']."'  GROUP BY A.activity_date, A.salesman_id");
	

	// echo "<pre>";
	// print_r($routeRetailerOfSalesmanActivity);


	$routeDistributorOfSalesmanActivity = $_objAdmin->_getSelectList2("view_orderOrRetDisAddActivity AS A 

	LEFT JOIN table_route_scheduled AS RS ON RS.salesman_id = A.salesman_id AND DATE_FORMAT(A.activity_date, '%Y-%c') = CONCAT(RS.year,'-',RS.month) 
	LEFT JOIN table_route_schedule_details AS RSD ON RSD.route_schedule_id = RS.route_schedule_id AND RSD.assign_day = DATE_FORMAT(A.activity_date, '%e')


	LEFT JOIN table_order AS O ON O.order_id = A.ref_id AND O.order_type IN ('Yes','No')  AND O.retailer_id = 0 AND O.distributor_id > 0  AND A.activity_type = 3
	LEFT JOIN table_distributors AS D3 ON D3.distributor_id = O.distributor_id AND D3.start_date < O.date_of_order

	LEFT JOIN table_route_retailer AS RR ON RR.route_id = RSD.route_id AND RR.status = 'D' AND RR.retailer_id = D3.distributor_id AND A.activity_type = 3



	LEFT JOIN table_order AS O2 ON O2.order_id = A.ref_id AND O2.order_type IN ('Yes')  AND O2.retailer_id = 0 AND O2.distributor_id > 0 AND A.activity_type = 3
	LEFT JOIN table_distributors AS D4 ON D4.distributor_id = O2.distributor_id AND D4.start_date < O2.date_of_order  

	LEFT JOIN table_route_retailer AS RR2 ON RR2.route_id = RSD.route_id AND RR2.status = 'D' AND RR2.retailer_id = D4.distributor_id AND A.activity_type = 3 



	LEFT JOIN table_distributors AS D ON D.distributor_id = A.ref_id AND A.activity_type = 24
	

	LEFT JOIN table_distributors AS D2 ON D2.distributor_id = A.ref_id AND A.activity_type =24 ",


	"RSD.route_id, COUNT(DISTINCT(RR.retailer_id)) AS plannedRouteDistributorTotalCalls,  COUNT(DISTINCT(RR2.retailer_id)) AS plannedRouteDistributorProductiveCalls, COUNT(DISTINCT(D3.distributor_id)) AS ttlOrders, COUNT(DISTINCT(D4.distributor_id)) AS ttlProductiveCalls, COUNT(D.distributor_id) AS ttlAddedDis, COUNT( D2.distributor_id ) AS ttlInterestedCustomer, A.salesman_id, A.activity_date", '',
	" A.salesman_id = '".$_SESSION['monthSalesmanID']."' AND DATE_FORMAT(A.activity_date,'%Y') = '".$_SESSION['disCyear']."' AND DATE_FORMAT(A.activity_date,'%m') = '".$_SESSION['dismonth']."' AND RSD.route_id IS NOT NULL GROUP BY A.activity_date, A.salesman_id, RSD.route_id");


			/*echo "<pre>";
	        print_r($routeRetailerOfSalesmanActivity);
	        echo "<pre>";
			print_r($routeDistributorOfSalesmanActivity);
			exit;*/
			//echo "<pre>";
			//print_r($routeRetailerDistributorOfSalesmanOrderCategoryWise);
			//echo "</pre>";

			// Order dataset
			foreach ($routeRetailerDistributorOfSalesmanOrderCategoryWise as $key => $orderValue) {
				# code...

				if($orderValue->type_status == 'R')  {
					$dataSetOfCustomerOrders[$orderValue->salesman_id][$orderValue->date_of_order][$orderValue->type_status][] = $orderValue;
				} else if($orderValue->type_status == 'D')  {
					$dataSetOfCustomerOrders[$orderValue->salesman_id][$orderValue->date_of_order][$orderValue->type_status][] = $orderValue;
				}
			}

			// echo "<pre>";
			// print_r($dataSetOfCustomerOrders);
			// echo "</pre>";

			// Retailer Activities
			foreach ($routeRetailerOfSalesmanActivity as $key => $rAnalytics) {
				# code...
				$dataSetOfRetailerSalesmanActivity[$rAnalytics->salesman_id][$rAnalytics->activity_date][$rAnalytics->route_id] = $rAnalytics;

			}


			// Distributor Activities
			foreach ($routeDistributorOfSalesmanActivity as $key => $dAnalytics) {
				# code...
				// echo "<pre>";
				// print_r($dAnalytics);
				$dataSetOfDistributorSalesmanActivity[$dAnalytics->salesman_id][$dAnalytics->activity_date][$dAnalytics->route_id] = $dAnalytics;

			}


			// echo "<pre>";
			// print_r($routeRetailerDistributorOfSalesman);
			// echo "</pre>";exit;

			
			if(is_array($routeRetailerDistributorOfSalesman)){
			
			foreach ($routeRetailerDistributorOfSalesman as $key => $Mvalue) {
				# code...
				
				$tdStyleClass = "";
				$CustomerType = "";
				$NoofToBeVisitedCustomer = "0";
				$NoOfVisitedCustomer = "0";
				$NoOfCusPlacedOrder = "0";
				$NoOfNewOutletSurveyed = "0";
				$NoOfNewOutletAssigned = "0";
				$NoOfInterestedCustomer = "0";
				$BeatAdherence = "0";
				$BeatActivity = "0";

				$rtdate = date('Y-m-d',strtotime($Mvalue->route_date));
				$route_id = $Mvalue->route_id;


				// Order dataset
				//echo "<pre>";
				//print_r($Mvalue);exit;
				//print_r($dataSetOfCustomerOrders[$Mvalue->salesman_id][$rtdate][$Mvalue->status]);

				if(isset($dataSetOfCustomerOrders[$Mvalue->salesman_id][$rtdate][$Mvalue->status])) {
					$data = $dataSetOfCustomerOrders[$Mvalue->salesman_id][$rtdate][$Mvalue->status];
				} else {
					$data = array();
				}


				// Retailer Activities
				if(isset($dataSetOfRetailerSalesmanActivity[$Mvalue->salesman_id][$rtdate][$route_id])) {
					$rdata = $dataSetOfRetailerSalesmanActivity[$Mvalue->salesman_id][$rtdate][$route_id];
				} else {
					$rdata = array();
				}

				// Distributor Activities
				if(isset($dataSetOfDistributorSalesmanActivity[$Mvalue->salesman_id][$rtdate][$route_id])) {
					$ddata = $dataSetOfDistributorSalesmanActivity[$Mvalue->salesman_id][$rtdate][$route_id];
				} else {
					$ddata = array();
				}

				// echo "<pre>";
				// print_r($Mvalue);//exit;
				// echo "<pre>";
				// print_r($rdata);
				// echo "<pre>";
				// print_r($ddata);exit;

				$NoofToBeVisitedCustomer = $Mvalue->ttlRouteRetailers+$Mvalue->ttlRouteDistributors;
				$NoOfVisitedCustomer   =  $ddata->plannedRouteDistributorTotalCalls+$rdata->plannedRouteRetailerTotalCalls;
				
				$NoOfCusPlacedOrder  	= $ddata->plannedRouteDistributorProductiveCalls+$rdata->plannedRouteRetailerProductiveCalls;

				$NoOfNewOutletSurveyed = $ddata->ttlAddedDis+$rdata->ttlAddedRet;
				
				$NoOfInterestedCustomer = $ddata->ttlInterestedCustomer+$rdata->ttlInterestedCustomer;
				$BeatAdherence = round($NoOfVisitedCustomer/$NoofToBeVisitedCustomer,2);
				$BeatActivity  = round($NoOfCusPlacedOrder/$NoofToBeVisitedCustomer,2);

				if($Mvalue->status == 'R')
				{
					$CustomerType = "Dealer";
				}
			    else if($Mvalue->status == 'D')  
			    {
			    	$CustomerType = "Distributor";
			    }
			    else
			    {
			    	$CustomerType = "";
			    }



				/*if($Mvalue->status == 'R')  {
					$tdStyleClass = "#ededed";
					$CustomerType = "Retailer";
					$NoofToBeVisitedCustomer = $Mvalue->ttlRouteRetailers;
					if(sizeof($rdata)>0) {

						//$NoOfVisitedCustomer   =  $rdata[0]->ttlOrders + $rdata[0]->ttlSurvey + $rdata[0]->ttlAddedRet;
						//$NoofToBeVisitedCustomer = $Mvalue->ttlRouteRetailers;
						//$NoOfVisitedCustomer   =  $rdata[0]->ttlOrders;
						$NoOfVisitedCustomer   =  $rdata->plannedRouteRetailerTotalCalls;
						//$NoOfCusPlacedOrder    =  $rdata[0]->ttlProductiveCalls;
						$NoOfCusPlacedOrder  = 		$rdata->plannedRouteRetailerProductiveCalls;
						$NoOfNewOutletSurveyed =  $rdata->ttlAddedRet;
						$NoOfNewOutletAssigned =  $rdata->ttlAddedRetInthisRoute;
						$NoOfInterestedCustomer = $rdata->ttlInterestedCustomer;
						$BeatAdherence = round($NoOfVisitedCustomer/$Mvalue->ttlRouteRetailers,2);
						$BeatActivity  = round($NoOfCusPlacedOrder/$Mvalue->ttlRouteRetailers,2);
					}

				} else if($Mvalue->status == 'D')  {
					$tdStyleClass = "#FFF";
					$CustomerType = "Distributor";
					$NoofToBeVisitedCustomer = $Mvalue->ttlRouteDistributors;
					if(sizeof($ddata)>0) {

						//$NoOfVisitedCustomer = $ddata[0]->ttlOrders + $ddata[0]->ttlSurvey + $ddata[0]->ttlAddedRet;
						//$NoofToBeVisitedCustomer = $Mvalue->ttlRouteDistributors;
						//$NoOfVisitedCustomer = $ddata[0]->ttlOrders;
						$NoOfVisitedCustomer   =  $ddata->plannedRouteDistributorTotalCalls;
						//$NoOfCusPlacedOrder  = $ddata[0]->ttlProductiveCalls;
						$NoOfCusPlacedOrder  	= $ddata->plannedRouteDistributorProductiveCalls;
						$NoOfNewOutletSurveyed = $ddata->ttlAddedDis;
						//$NoOfNewOutletAssigned =  $rdata->ttlAddedDisInthisRoute;
						$NoOfInterestedCustomer = $ddata->ttlInterestedCustomer;
						$BeatAdherence = round($NoOfVisitedCustomer/$Mvalue->ttlRouteDistributors,2);
						$BeatActivity  = round($NoOfCusPlacedOrder/$Mvalue->ttlRouteDistributors,2);
					} 
				}*/

				//  echo "<pre>";
				//  print_r($data);
				// // print_r($ddata);
				// echo "</pre>";
			?>

				<tr  style="border-bottom:2px solid #6E6E6E; background:<?php echo $tdStyleClass;?>">
					<td style="padding:10px;" width="10%"><?php echo $Mvalue->salesman_name;?></td>
					<td style="padding:10px;" width="10%"><?php echo $Mvalue->salesman_code;?></td>
					<td style="padding:10px;" width="10%"><?php echo $_objAdmin->_changeDate($Mvalue->route_date);?></td>
					<td style="padding:10px;" width="30%"><?php echo $Mvalue->route_name;?></td>
					<td style="padding:10px;" width="20%" align="center"><?php  echo $NoofToBeVisitedCustomer; ?></td> 
					<!-- <td style="padding:10px;" width="20%" align="center"><?php  echo $Mvalue->ttlRoutePersons; ?>, R=<?php  echo $Mvalue->ttlRouteRetailers; ?>, D=<?php  echo $Mvalue->ttlRouteDistributors; ?></td> -->
					<td style="padding:10px;" width="20%" align="center"><?php  echo $NoOfVisitedCustomer; ?></td>
					<td style="padding:10px;" width="20%"><?php echo $CustomerType;?></td>
					<td style="padding:10px;" width="20%" align="center"><?php  echo $NoOfCusPlacedOrder; ?></td>
					<!-- <td style="padding:10px;" width="20%" align="center"><?php  echo $NoOfNewOutletSurveyed; ?></td>
					<td style="padding:10px;" width="20%" align="center"><?php  echo $NoOfNewOutletAssigned; ?></td>
					<td style="padding:10px;" width="20%" align="center"><?php  echo $NoOfInterestedCustomer; ?></td>
					<td style="padding:10px;" width="20%" align="center"><?php  echo $BeatAdherence; ?></td>
					<td style="padding:10px;" width="20%" align="center"><?php  echo $BeatActivity; ?></td> -->
					<td style="padding:10px;" width="20%" align="center">-</td>
					<!-- Check order type YES/NO-->
					<?php 
						$sizeOfTLDS = "-";
						$catWiseOrderValue = array();
						foreach ($data as $key => $catDataSet) {
								if($catDataSet->order_type == 'Yes' && $catDataSet->category_id > 0) {
									$catWiseOrderValue[$catDataSet->category_id] = $catDataSet;
								}
						$sizeOfTLDS = sizeOf($catWiseOrderValue);
						}?>
					<!-- Check order type YES/NO-->


					<!-- Category wise calculation -->
					<td>
						<table>
							<tr>
								<?php foreach ($categotyList as $key => $catValue) {?>
									 <td width="150px;" align="center" style="margin: 10px; padding: 10px; font-family: monospace;">
										<?php if(isset($catWiseOrderValue[$catValue->category_id])){
											echo $catWiseOrderValue[$catValue->category_id]->ttlCategoryQty."/".$catWiseOrderValue[$catValue->category_id]->ttlCategoryAmt;
										 } else {
											echo "-";
										}?>
									</td>
								<?php }?>
							</tr>
						</table>
					</td>
					<!-- Category wise calculation -->

					<td style="padding:10px;" align="center" width="15%"><?php echo $sizeOfTLDS;?></td>
				</tr>
			<?php } } else { ?>
			<tr  style="border-bottom:2px solid #6E6E6E;">
				<td style="padding:10px;" colspan="20">Report Not Available</td>
			</tr>
			<?php } ?>
		</table>
		</div>
	<!-- end id-form  -->
	</td>
</tr>
</table>
<div class="clear"></div>
</div>
<!--  end content-table-inner  -->
</td>
<td id="tbl-border-right"></td>
</tr>
</table>
<div class="clear">&nbsp;</div>
</div>
<!--  end content -->
<div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->
<div class="clear">&nbsp;</div> 
<!-- start footer -->         
<?php include("footer.php") ?>
<!-- end footer -->

</body>
</html>