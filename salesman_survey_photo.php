<?php
include("includes/config.inc.php");
include("includes/function.php");
$_objAdmin = new Admin();
if($_REQUEST['survey_id']!=""){
	$surveyRec = $_objAdmin->_getSelectList('table_survey as ts left join table_image as ti on ts.survey_id = ti.ref_id left join table_salesman as s on ts.salesman_id=s.salesman_id left join table_retailer as tr on ts.retailer_id=tr.retailer_id left join table_distributors as td on ts.distributor_id=td.distributor_id', 'ts.*,ti.*,s.salesman_name,tr.retailer_name,tr.retailer_address,td.distributor_name,td.distributor_address',''," survey_id=".$_REQUEST['survey_id']);
}
?>

<?php include("header.inc.php") ?>
<script type="text/javascript">
// Popup window code
function newPopup(url) {
	popupWindow = window.open(
		url,'popUpWindow','height=500,width=700,left=200,top=100,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no,status=yes')
}

function check()
{
var r=confirm("Are you sure, you want to delete survey?!");
if (r==true)
  {
		return true;
  }
else
  {
     return false;
  }
}

function checkSvy()
{
var r=confirm("Are you sure, you want to change survey status not ok.");
if (r==true)
  {
		return true;
  }
else
  {
     return false;
  }
}
</script>
<style>
div.img
  {
  margin:2px;
  border:1px solid  #43A643;
  height:auto;
  width:auto;
  float:left;
  text-align:center;
  }
div.img img
  {
  display:inline;
  margin:3px;
  border:1px solid #fbfcfc;
  }
div.img a:hover img
  {
  border:1px solid #fbfcfc;
  }
div.desc
  {
  text-align:center;
  font-weight:normal;
  width:120px;
  margin:2px;
  }
</style>
<!-- start content-outer -->
<div id="content-outer">
<!-- start content -->

<div id="content">
<div id="page-heading"><h1><span style="color: #d74343; font-family: Tahoma; font-weight: bold;">Salesman Survey Photo</span></h1></div>
<table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">
<tr>
	<!--<td id="tbl-border-left"></td>-->
	<td>
		<!--  start content-table-inner -->
		<div id="content-table-inner">
			<?php if($sus!=''){?>
			<!--  start message-green -->
				<div id="message-green">
					<table border="0" width="100%" cellpadding="0" cellspacing="0">
						<tr>
							<td class="green-left"><?php echo $sus; ?></td>
							<td class="green-right"><a class="close-green"><img src="images/icon_close_green.gif"   alt="" /></a></td>
						</tr>
					</table>
				</div>
			<?php } ?>
			<table border="0" width="100%" cellpadding="0" cellspacing="0">
				<tr valign="top">
					<td>
						<!-- start id-form -->
						<table border="0" cellpadding="0" cellspacing="0"  >
							<tr valign="top">
								<td>
									<div id="page-heading"><h3>Customer Name: <span style=" color:#000000;"><?php echo ($surveyRec[0]->retailer_name!='')?$surveyRec[0]->retailer_name:$surveyRec[0]->distributor_name;?></span> <!-- Survey Status: <span style=" color:#000000;"><?php echo $survey_status;?></span> --> </h3>
									</div>
								</td>
							</tr>
						</table>
						<table border="0" cellpadding="0" cellspacing="0">
							<?php if(is_array($surveyRec)){?>
								<tr valign="top" align="left">
									<td><b>Taken By: </b><?php echo $surveyRec[0]->salesman_name;?> <b>Date: </b><?php echo $_objAdmin->_changeDate($surveyRec[0]->date_server); ?> <b>Time: </b><?php echo $surveyRec[0]->time_server;?> <b>Map: </b>
										<?php if($surveyRec[0]->lat!=0.0 || $surveyRec[0]->lat==''){?>
											<a href="JavaScript:newPopup('map.php?rid=<?php echo base64_encode($_REQUEST[survey_id])?>');">View on Map</a>
										<?php } else { ?>-<?php } ?>
										<b>Location Type: </b><?php echo $surveyRec[0]->network_mode;?>
									</td>
								</tr>
								<tr valign="top" >
									<td>
										<?php
										for($i=0;$i<count($surveyRec);$i++){
											if($surveyRec[$i]->image_url != ""){
												?>
												<div class="img">
													<a href="photo/<?php echo $surveyRec[$i]->image_url;?>"  target="_blank"><img src="photo/<?php echo $surveyRec[$i]->image_url;?>" alt="" width="140" height="120"></a>
												</div>
											<?php } else { ?>
												<div class="img">
													<div style="width: 500px;height:50px;"><span style="color: #A52A2A; font-family: Tahoma; font-weight: bold;">Image Not Available</span></div>
												</div>
											<?php }
										} ?>
									</td>
								</tr>
								<?php } else { ?>
									<tr align="center">
										<td align="center" style="width: 500px;line-height: 20px;"><div class="img"><div style="width: 500px;height:50px;"><span style="color: #A52A2A; font-family: Tahoma; font-weight: bold;">Survey Not Available</span></div></div></td>
									</tr>
								<?php } ?>
									<tr>
										<td align="center"></td>
									</tr>
								</table>
	<!-- end id-form  -->

	</td>
	<td>

	<!-- right bar-->
	<?php //include("rightbar/category_bar.php") ?>
</td>
</tr>
<tr>
<td><img src="images/blank.gif" width="695" height="1" alt="blank" /></td>
<td></td>
</tr>
</table>
<div class="clear"></div>
</div>
<!--  end content-table-inner  -->
</td>
<td id="tbl-border-right"></td>
</tr>
<!--<tr>
	<th class="sized bottomleft"></th>
	<td id="tbl-border-bottom">&nbsp;</td>
	<th class="sized bottomright"></th>
</tr>-->
</table>
<div class="clear">&nbsp;</div>
</div>
<!--  end content -->
<div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->
<div class="clear">&nbsp;</div> 
<!-- start footer -->         
<?php include("footer.php") ?>
<!-- end footer -->
 
</body>
</html>