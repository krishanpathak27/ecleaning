<?php include("includes/config.inc.php");
      include("includes/function.php");
	  include("includes/globalarraylist.php");

	$page_name="Salesman Survey Report";
	if(isset($_POST['showReport']) && $_POST['showReport'] == 'yes')
	{
		if($_POST['sal']!="" && $_POST['sal']!='All') 
		{
		 $salesman=" and s.salesman_id='".$_POST['sal']."' ";
		 $sals_id=$_POST['sal'];
		}
		elseif($_POST['sal']=='All')
		{
			$salesman;
		}

		if($_POST['from']!="") 
		{
		$from_date=$_objAdmin->_changeDate($_POST['from']);	
		}
		if($_POST['to']!="") 
		{
		$to_date=$_objAdmin->_changeDate($_POST['to']);	
		}
	} else {
		$from_date= $_objAdmin->_changeDate(date("Y-m-d"));
		$to_date= $_objAdmin->_changeDate(date("Y-m-d"));
		$salesman;	
	}
	if(isset($_REQUEST['reset']) && $_REQUEST['reset'] == 'yes')
	{
		header("Location: salesman_survey_report.php");
	}
	if($sal_id!=''){
		$SalName=$_objAdmin->_getSelectList('table_salesman','salesman_name',''," salesman_id='".$sals_id."'"); 
		$sal_name=$SalName[0]->salesman_name;
	}
	if($_SESSION['userLoginType'] == 5){
		$divisionIdString = implode(",", $divisionList);
		$division = " s.division_id IN ($divisionIdString) ";
		$division1 = " division_id IN ($divisionIdString) ";
	} else {
		$division = " ";
		$division1 = " ";
	}
	if(isset($_POST['division'])){
		if($_POST['division'] != 'all'){
			$division = " s.division_id IN (".$_POST['division'].") ";
		} else {
			$division = "";
		}
	}
	$divisionRec=$_objAdmin->_getSelectList2('table_division',"division_name as division,division_id",'',$division1);
?>

<?php include("header.inc.php") ?>
<script type="text/javascript">

    function PrintElem(elem)
    {
        Popup($(elem).html());
    }

    function Popup(data) 
    {
		
        var mywindow = window.open('', 'Report');
		
        mywindow.document.write('<html><head><title>Salesman Survey Report</title>');
		mywindow.document.write('<table><tr><td><b>Salesman Name:</b> <?php echo $sal_name; ?></td><td><b>City Name:</b> <?php echo $city_name; ?></td><td><b>From Date:</b> <?php echo $from_date; ?></td><td><b>To Date:</b> <?php echo $to_date; ?></td></tr></table>');
        /*optional stylesheet*/ //mywindow.document.write('<link rel="stylesheet" href="main.css" type="text/css" />');
        mywindow.document.write('</head><body >');
        mywindow.document.write(data);
        mywindow.document.write('</body></html>');

        mywindow.print();
        mywindow.close();
        return true;
    }

$(document).ready(function()
{
<?php if($_POST['submit']=='Export to Excel'){ ?>
tableToExcel('report_export', 'salesman survey report', 'salesman survey report.xls');
<?php } ?>
});

</script>
<script src="javascripts/dateNextPrev.js" type="text/javascript"></script>
<!-- start content-outer -->
<input name="pagename" type="hidden"  id="pagename" value="admin_order_list.php" />
<div id="content-outer">
<!-- start content -->
<div id="content">
<div id="page-heading"><h1><span style="color: #d74343; font-family: Tahoma; font-weight: bold;">Salesman Survey Report</span></h1></div>
<table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">
<tr>
	<td>
	<!--  start content-table-inner -->
	<div id="content-table-inner">
	
	<div id="page-heading" align="left" >
	<form name="frmPre" id="frmPre" method="post" action="" enctype="multipart/form-data" >
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
	<tr>
		<td>
			<h3>Salesman: </h3>
			<h6>
				<select name="sal" id="sal" class="menulist">
					<?php echo $salesmanOptionList = $_objArrayList->GetSalesmenMenu($salsList, $sals_id,'flex');?>
				</select>
			</h6>
		</td>
		<td>
			<h3>Customer Type: </h3>
			<h6>
				<select name="cust_type" id="cust_type" class="menulist">
					<?php
					if(isset($_POST['cust_type']))
						echo $customerType = $_objArrayList->getCustomerType($_POST['cust_type']);
					else
						echo $customerType = $_objArrayList->getCustomerType();
					?>
				</select>
			</h6>
		</td>
		<td>
			<h3>Division: </h3>
			<h6>
				<select name="division" id="division" class="menulist">
					<option value="all">All</option>
					<?php 
						foreach ($divisionRec as $key => $value) {?>
							<option value="<?php echo $value->division_id?>" <?php echo (isset($_POST['division']) && $_POST['division']==$value->division_id)?'selected':''?>><?php echo $value->division?></option>
						<?php }
					?>
				</select>
			</h6>
		</td>
		
		
		<td><h3>&nbsp;&emsp;&nbsp;From Date: </h3><h6><img src="css/images/prev.png" height="18" width="18" onclick="dateFromPrev();"> <input type="text" id="from" name="from" class="date" style="width:100px" value="<?php  echo $from_date;?>"  readonly /> <img src="css/images/next.png" height="18" width="18" onclick="dateFromNext();"></h6></td>
		
		<td><h3>&nbsp;&emsp;&nbsp;To Date: </h3><h6><img src="css/images/prev.png" height="18" width="18" onclick="dateToPrev();"> <input type="text" id="to" name="to" class="date" style="width:100px" value="<?php echo $to_date; ?>"  readonly /> <img src="css/images/next.png" height="18" width="18" onclick="dateToNext();"></h6></td>
		<td>
		
		<h3></h3>
		<input name="submit" class="result-submit" type="submit" id="submit" value="View Details" />
	    <input type="button" value="Reset!" class="form-reset" onclick="location.href='salesman_survey_report.php?reset=yes';" />
		</td>
		<td colspan="2"></td>
		</tr>
		<tr>
		<td colspan="6"><input name="showReport" type="hidden" value="yes" />
		<a id="dlink"  style="display:none;"></a>
		<input  type="submit" value="Export to Excel" name="submit" class="result-submit"  >
		<input type="button" value="Print" class="result-submit" onclick="PrintElem('#Report')" />
		<!-- <a href='new_retailer_report_year_graph.php?y=<?php echo checkFromdate($from_date)."&salID=".$_POST['sal']."&city=".$_POST['city']; ?>' target="_blank"><input type="button" value=" Show Graph" class="result-submit" /></a> -->
		</td>
	</tr>
	</table>
	</form>
	</div>
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
	<?php
	if($sal_id!=""){ 
	?>
	<tr valign="top">
		<td>
		<table border="0" width="100%" cellpadding="0" cellspacing="0" id="id-form">
			<tr bgcolor="#A52A2A" style="color: #fff;font-weight: bold;">
				<td align="center" style="padding:10px;">Select Salesman</td>
			</tr>
		</table>
		</td>
		<?php } else {  ?>
		<td>
		<div id="Report" style="width:1100px; overflow:scroll">
		<table  border="0" width="100%" cellpadding="0" cellspacing="0" id="report_export" name="report_export" >
			<tr bgcolor="#6E6E6E" style="color: #fff;font-weight: bold;" >
				<td style="padding:10px;" width="10%">Salesman Name</td>
				<td style="padding:10px;" width="10%">Salesman Code</td>
				<td style="padding:10px;" width="10%">Customer Name</td>
				<td style="padding:10px;" width="10%">Customer Code</td>
				<td style="padding:10px;" width="10%">Customer Type</td>
				<td style="padding:10px;" width="10%">Remark</td>
				<td style="padding:10px;" width="10%">Photo</td>
				<td style="padding:10px;" width="10%">Map</td>
				<td style="padding:10px;" width="10%">Date</td>
				<td style="padding:10px;" width="10%">Time</td>
			</tr>
			<?php
			$no_ret = array();
			$surveyRep = $_objAdmin->_getSelectList('table_survey as ts left join table_salesman as s on ts.salesman_id=s.salesman_id left join table_retailer as tr on ts.retailer_id=tr.retailer_id left join table_distributors as td on ts.distributor_id=td.distributor_id', 'ts.*,s.salesman_name,s.salesman_code,tr.retailer_name,tr.retailer_code,td.distributor_name,td.distributor_code',''," $division $salesman and (ts.survey_date BETWEEN '".date('Y-m-d', strtotime($from_date))."' AND '".date('Y-m-d', strtotime($to_date))."') ORDER BY ts.survey_date ASC");
			if($from_date!="" && $to_date!=""){
				if($_POST['cust_type']=="All"){
					if(is_array($surveyRep)){
						for($i=0;$i<count($surveyRep);$i++)
						{
						$dateCode=explode('-', $surveyRep[$i]->start_date);
						$no_ret[] = $surveyRep[$i]->retailer_id;
						?>
							<tr  style="border-bottom:2px solid #6E6E6E;" >
							<td style="padding:10px;" width="10%"><?php echo $surveyRep[$i]->salesman_name;?> </td>
							<td style="padding:10px;" width="10%"><?php echo $surveyRep[$i]->salesman_code;?> </td>
							<td style="padding:10px;" width="10%"><?php echo ($surveyRep[$i]->retailer_name!=""?$surveyRep[$i]->retailer_name:$surveyRep[$i]->distributor_name) ?></td>
							<td style="padding:10px;" width="10%"><?php echo ($surveyRep[$i]->retailer_name!=""?$surveyRep[$i]->retailer_code:$surveyRep[$i]->distributor_code) ?></td>
							<td style="padding:10px;" width="10%"><?php echo ($surveyRep[$i]->retailer_name!=""?"Retailer":"Distributor") ?> </td>
							<td style="padding:10px;" width="10%"><?php echo $surveyRep[$i]->survey_remark;?> </td>
							<td style="padding:10px;" width="10%">
								<a href="salesman_survey_photo.php?survey_id=<?php echo $surveyRep[$i]->survey_id;?>"  target='_blank'>View Photo</a>
							</td>
							<td style="padding:10px;" width="10%">
								<a href="salesman_survey_map.php?survey_id=<?php echo $surveyRep[$i]->survey_id;?>" target="_blank">View on Map</a>
							</td>
							<td style="padding:10px;" width="10%"><?php echo $surveyRep[$i]->survey_date;?></td>
							<td style="padding:10px;" width="10%"><?php echo $surveyRep[$i]->survey_time;?></td>
							</tr>
						<?php
						}
					}
				}
				if($_POST['cust_type']=="1"){
					$surveyRepRet = $_objAdmin->_getSelectList('table_survey as ts left join table_salesman as s on ts.salesman_id=s.salesman_id left join table_retailer as tr on ts.retailer_id=tr.retailer_id', 'ts.*,s.salesman_name,s.salesman_code,tr.retailer_name,tr.retailer_code',''," $division $salesman and (ts.survey_date BETWEEN '".date('Y-m-d', strtotime($from_date))."' AND '".date('Y-m-d', strtotime($to_date))."') AND ts.retailer_id > 0 ORDER BY ts.survey_date ASC");
					if(is_array($surveyRepRet)){
						for($i=0;$i<count($surveyRepRet);$i++)
						{
						$dateCode=explode('-', $surveyRepRet[$i]->start_date);
						$no_ret[] = $surveyRepRet[$i]->retailer_id;
						?>
							<tr  style="border-bottom:2px solid #6E6E6E;" >
							<td style="padding:10px;" width="10%"><?php echo $surveyRepRet[$i]->salesman_name;?> </td>
							<td style="padding:10px;" width="10%"><?php echo $surveyRepRet[$i]->salesman_code;?> </td>
							<td style="padding:10px;" width="10%"><?php echo $surveyRepRet[$i]->retailer_name;?> </td>
							<td style="padding:10px;" width="10%"><?php echo $surveyRepRet[$i]->retailer_code;?> </td>
							<td style="padding:10px;" width="10%">Retailer </td>
							<td style="padding:10px;" width="10%"><?php echo $surveyRepRet[$i]->survey_remark;?> </td>
							<td style="padding:10px;" width="10%">
								<a href="salesman_survey_photo.php?survey_id=<?php echo $surveyRepRet[$i]->survey_id;?>"  target='_blank'>View Photo</a>
							</td>
							<td style="padding:10px;" width="10%">
								<a href="salesman_survey_map.php?survey_id=<?php echo $surveyRepRet[$i]->survey_id;?>" target="_blank">View on Map</a>
							</td>
							<td style="padding:10px;" width="10%"><?php echo $surveyRepRet[$i]->survey_date;?></td>
							<td style="padding:10px;" width="10%"><?php echo $surveyRepRet[$i]->survey_time;?></td>
							</tr>
						<?php
						}
					}
				}

				if($_POST['cust_type']=="2"){
					$surveyRepDist = $_objAdmin->_getSelectList('table_survey as ts left join table_salesman as s on ts.salesman_id=s.salesman_id left join table_distributors as td on ts.distributor_id=td.distributor_id', 'ts.*,s.salesman_name,s.salesman_code,td.distributor_name,td.distributor_code',''," $division $salesman and (ts.survey_date BETWEEN '".date('Y-m-d', strtotime($from_date))."' AND '".date('Y-m-d', strtotime($to_date))."') AND ts.distributor_id > 0 ORDER BY ts.survey_date ASC");
					if(is_array($surveyRepDist)){
						for($i=0;$i<count($surveyRepDist);$i++)
						{
						$dateCode=explode('-', $surveyRepDist[$i]->start_date);
						$no_ret[] = $surveyRepDist[$i]->retailer_id;
						?>
							<tr style="border-bottom:2px solid #6E6E6E;" >
							<td style="padding:10px;" width="10%"><?php echo $surveyRepDist[$i]->salesman_name;?> </td>
							<td style="padding:10px;" width="10%"><?php echo $surveyRepDist[$i]->salesman_code;?> </td>
							<td style="padding:10px;" width="10%"><?php echo $surveyRepDist[$i]->distributor_name;?> </td>
							<td style="padding:10px;" width="10%"><?php echo $surveyRepDist[$i]->distributor_code;?> </td>
							<td style="padding:10px;" width="10%">Distributor </td>
							<td style="padding:10px;" width="10%"><?php echo $surveyRepDist[$i]->survey_remark;?> </td>
							<td style="padding:10px;" width="10%">
								<a href="salesman_survey_photo.php?survey_id=<?php echo $surveyRepDist[$i]->survey_id;?>"  target='_blank'>View Photo</a>
							</td>
							<td style="padding:10px;" width="10%">
								<a href="salesman_survey_map.php?survey_id=<?php echo $surveyRepDist[$i]->survey_id;?>" target="_blank">View on Map</a>
							</td>
							<td style="padding:10px;" width="10%"><?php echo $surveyRepDist[$i]->survey_date;?></td>
							<td style="padding:10px;" width="10%"><?php echo $surveyRepDist[$i]->survey_time;?></td>
							</tr>
						<?php
						}
					}
				}

				if($_POST['cust_type']=="3"){
					$surveyRepCust = $_objAdmin->_getSelectList('table_survey as ts left join table_salesman as s on ts.salesman_id=s.salesman_id left join table_customer as tc on ts.customer_id=tc.customer_id', 'ts.*,s.salesman_name,s.salesman_code,tc.customer_name,tc.customer_code',''," $division $salesman and (ts.survey_date BETWEEN '".date('Y-m-d', strtotime($from_date))."' AND '".date('Y-m-d', strtotime($to_date))."') AND ts.customer_id > 0 ORDER BY ts.survey_date ASC");
					if(is_array($surveyRepCust)){
						for($i=0;$i<count($surveyRepCust);$i++)
						{
						$dateCode=explode('-', $surveyRepCust[$i]->start_date);
						$no_ret[] = $surveyRepCust[$i]->retailer_id;
						?>
							<tr  style="border-bottom:2px solid #6E6E6E;" >
							<td style="padding:10px;" width="10%"><?php echo $surveyRepCust[$i]->salesman_name;?> </td>
							<td style="padding:10px;" width="10%"><?php echo $surveyRepCust[$i]->salesman_code;?> </td>
							<td style="padding:10px;" width="10%"><?php echo $surveyRepCust[$i]->customer_name;?> </td>
							<td style="padding:10px;" width="10%"><?php echo $surveyRepCust[$i]->customer_code;?> </td>
							<td style="padding:10px;" width="10%">Shakti-Partner </td>
							<td style="padding:10px;" width="10%"><?php echo $surveyRepCust[$i]->survey_remark;?> </td>
							<td style="padding:10px;" width="10%">
								<a href="salesman_survey_photo.php?survey_id=<?php echo $surveyRepCust[$i]->survey_id;?>"  target='_blank'>View Photo</a>
							</td>
							<td style="padding:10px;" width="10%">
								<a href="salesman_survey_map.php?survey_id=<?php echo $surveyRepCust[$i]->survey_id;?>" target="_blank">View on Map</a>
							</td>
							<td style="padding:10px;" width="10%"><?php echo $surveyRepCust[$i]->survey_date;?></td>
							<td style="padding:10px;" width="10%"><?php echo $surveyRepCust[$i]->survey_time;?></td>
							</tr>
						<?php
						}
					}
				}
				?>
				<tr>
				<td style="padding:10px;" colspan="9"><b>Total Number:</b> <?php echo count($no_ret) ?></td>
				</tr>
			<?php
			} else {
			?>
			<tr  style="border-bottom:2px solid #6E6E6E;" align="center" >
				<td style="padding:10px;" colspan="17">Report Not Available</td>
			</tr>
			<?php
			}
			?>
		</table>
		</div>
		</td>
	<?php } ?>
	</tr>
	<tr>
		<td><img src="images/blank.gif" width="695" height="1" alt="blank" /></td>
		<td></td>
	</tr>
	</table>
<div class="clear"></div>
</div>
<!--  end content-table-inner  -->
</td>
<td id="tbl-border-right"></td>
</tr>
<!--<tr>
	<th class="sized bottomleft"></th>
	<td id="tbl-border-bottom">&nbsp;</td>
	<th class="sized bottomright"></th>
</tr>-->
</table>
<div class="clear">&nbsp;</div>
</div>
<!--  end content -->
<div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->
<div class="clear">&nbsp;</div> 
<!-- start footer -->         
<?php include("footer.php") ?>
<?php unset($_SESSION['SalAttList']); ?>
<!-- end footer -->
<script src="javascripts/jquery-1.8.2.js" type="text/javascript"></script>
<script src="javascripts/jquery-ui.js"></script>
<script type="text/javascript" src="javascripts/validate.js"></script>
<script type="text/javascript">
// Popup window code
function newPopup(url) {
	popupWindow = window.open(
		url,'popUpWindow','height=500,width=600,left=200,top=100,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no,status=yes')
}
</script>
<script>
    $(function() {
        $( "#from" ).datepicker({
			dateFormat: "d M yy",
            defaultDate: "w",
            changeMonth: true,
            numberOfMonths: 1,
            onSelect: function( selectedDate ) {
                $( "#to" ).datepicker( "option", "minDate", selectedDate );
            }
        });
        $( "#to" ).datepicker({
			dateFormat: "d M yy",
            defaultDate: "-w",
            changeMonth: true,
            numberOfMonths: 1,
            onSelect: function( selectedDate ) {
                $( "#from" ).datepicker( "option", "maxDate", selectedDate );
            }
        });
    });
</script>
<script type='text/javascript'>//<![CDATA[ 
var tableToExcel = (function () {
        var uri = 'data:application/vnd.ms-excel;base64,'
        , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table><tr><td><b>Salesman Name:</b> <?php echo $sal_name; ?></td><td><b>From Date:</b> <?php echo $_objAdmin->_changeDate($from_date); ?></td><td><b>To Date:</b> <?php echo $_objAdmin->_changeDate($to_date); ?></td></tr></table><table>{table}</table></body></html>'
        , base64 = function (s) { return window.btoa(unescape(encodeURIComponent(s))) }
        , format = function (s, c) { return s.replace(/{(\w+)}/g, function (m, p) { return c[p]; }) }
        return function (table, name, filename) {
            if (!table.nodeType) table = document.getElementById(table)
            var ctx = { worksheet: name || 'Worksheet', table: table.innerHTML }

            document.getElementById("dlink").href = uri + base64(format(template, ctx));
            document.getElementById("dlink").download = filename;
            document.getElementById("dlink").click();

        }
    })()

//]]>  
</script>
</body>
</html>
