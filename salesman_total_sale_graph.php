<?php
include("includes/config.inc.php");
include("includes/function.php");
include("includes/globalarraylist.php");

	  $p_g_total = array();
	  $c_g_total = array();
	  

	  
	if($_REQUEST['y']=='')
	{
		$_REQUEST['y'] = date('Y');
		$_SESSION['lastyear'] = date("Y",strtotime("-1 year"));
	}
	
	if($_SESSION['lastyear']==''){
		$_SESSION['lastyear'] = $_REQUEST['y'] - 1;
	} 
		
	if($_REQUEST['m']==''){
		$_REQUEST['m'] = date('m');
	} 
?>


	
	
	
		<!-- start id-form -->
		
		<?php 
			if($_REQUEST['salID']!=''){
				$salesman=" AND O.salesman_id ='".$_REQUEST['salID']."'";
			}
			
			if($_REQUEST['m']!=''){
				$smonth=" AND DATE_FORMAT(O.date_of_order, '%m') = '".$_REQUEST['m']."'";
			} 
			
			if($_REQUEST['y']!=''){
				$syear="  AND DATE_FORMAT(O.date_of_order, '%Y')<= '".$_REQUEST['y']."'";
			} 
			
			if($_SESSION['lastyear']!=''){
				$slastyear="  AND DATE_FORMAT(O.date_of_order, '%Y')>= '".$_SESSION['lastyear']."'";
			} 
			
		
			$where = " $salesman";
			
			$auRec=$_objAdmin->_getSelectList('table_order AS O
			LEFT JOIN table_salesman s ON O.salesman_id = s.salesman_id 
			LEFT JOIN city C ON s.city = C.city_id
			',"DISTINCT(O.salesman_id), s.salesman_name, city_name",'',$where,'');
			/*echo "<pre>";
			print_r($auRec);*/
		?>
		
		
		
		  <?php 		
			if(sizeof($auRec)>0) // Check Count
			{
				foreach($auRec as $key=>$value):?>
				
					
					<?php 
						$startDay = array('01','08','16','22');
						$endDay = array('07','15','23','31');
						$yearTotalSale = 0;
						$lastyearTotalSale = 0;
						//$smonth = 0;
						//$syear = 0;
						//$slastyear = 0;
						
						for($i=0; $i<4; $i++){	
						
						
						
						$startDayFrom = ' AND DATE_FORMAT(O.date_of_order, "%d")>= '.$startDay[$i];
						$endDayFrom = ' AND DATE_FORMAT(O.date_of_order, "%d")<= '.$endDay[$i];
						
						$result = $_objAdmin->_getSelectList('table_order AS O 
						LEFT JOIN table_order_detail AS D ON O.order_id = D.order_id',
						'SUM(quantity) As total, DATE_FORMAT(O.date_of_order, "%Y") As year ','',' O.salesman_id = '.$value->salesman_id.$slastyear.$syear.$smonth.$startDayFrom.$endDayFrom.' GROUP BY DATE_FORMAT(O.date_of_order, "%Y") ORDER BY DATE_FORMAT(O.date_of_order, "%Y") ASC','');
						

						//echo "<pre>";
						//print_r($result);
						$ltotal = 0;
						$total = 0;
						if(count($result)==2){
							$ltotal = $result[0]->total;
							$total = $result[1]->total;
							$yearTotalSale = $yearTotalSale + $result[1]->total;
							$lastyearTotalSale = $lastyearTotalSale + $result[0]->total;
						
						} else if(count($result) == 1) {
						
							if($result[0]->year == $_SESSION['lastyear']) 
							{ 	
									$ltotal = $result[0]->total;
								    $lastyearTotalSale = $lastyearTotalSale + $result[0]->total;									
									
								 } else {
								    $total = $result[0]->total; 
									$yearTotalSale = $yearTotalSale + $result[0]->total;
								}
						} else {
							
							$ltotal = 0;
							$total = 0;
						
						}?>
						
						
								
							<?php $p_g_total[] = $ltotal; $c_g_total[] = $total; ?>
						<?php } //echo "<pre>";
					//print_r($result); ?>
					
							
					
				<?php 
					
						
				endforeach;
				
			} // Check Count Closed?>
			
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta charset="utf-8">
<title>Quytech PepUpSales - Boys Wise Monthly Work Report Graph</title>
<?php include_once('graph/header-files.php');?>


    <script type="text/javascript">
      google.load('visualization', '1', {packages: ['corechart']});
	  
    </script>
    <script type="text/javascript">
      function drawVisualization() {
        // Some raw data (not necessarily accurate)
        var data = google.visualization.arrayToDataTable([
          ['day', 'Year <?php echo $_SESSION['lastyear']; ?>', 'Year <?php echo $_REQUEST['y']; ?>' ],
          ['Day 1 to 7',  <?php echo $p_g_total[0].','.$c_g_total[0]; ?> ],
          ['Day 8 to 15',  <?php echo $p_g_total[1].','.$c_g_total[1]; ?> ],
          ['Day 16 to 23',  <?php echo $p_g_total[2].','.$c_g_total[2]; ?> ],
          ['Day 24 to 31',  <?php echo $p_g_total[3].','.$c_g_total[3]; ?> ]
        ]);

        var options = {
          title : 'Salesman total sales',
          vAxis: {title: "Total sales" , gridlines: { count: 8  }, minValue:0},
          hAxis: {title: "<?php echo $_SESSION['lastyear'].' and '.$_REQUEST['y']; ?>"},
          seriesType: "bars",
          series: {5: {type: "line"}}
        };

        var chart = new google.visualization.ComboChart(document.getElementById('chart_div'));
        chart.draw(data, options);
      }
      google.setOnLoadCallback(drawVisualization);
    </script>
	<!-- end id-form  -->
</head>
 <body>
<!-- Start: page-top-outer -->
<?php include_once('graph/header.php');?>
<!-- End: page-top-outer -->

<div id="content-outer">
<div id="content">
<div id="page-heading"><h1>Boys Wise montly work done</h1></div>
<div id="container">
<form name="frmPre" id="frmPre" method="post" action="<?php echo $_SERVER['PHP_SELF'];?>" enctype="multipart/form-data" >
<table width="100%">
	
  <tr>
  	<td valign="bottom" align="right"><h3>Salesman :</h3></td>
    <td align="left">
	<select name="salID" id="salID" class="styledselect_form_5" >
		<?php echo $rtndata = $_objArrayList->GetSalesmenMenu($salsList, $_REQUEST['salID']);?>
	</select>
	</td>
	
	<td valign="bottom" align="right"><h3>Month :</h3></td>
    <td align="left">
	<select name="m" id="m" class="styledselect_form_5" >
		<?php echo $months = $_objArrayList->getMonthList3($_REQUEST['m']);?>
		</select>
	</td>
	
	
	<td valign="bottom" align="right"><h3>Year</h3></td>
    <td align="left">
	<select name="y" id="y" class="styledselect_form_5" >
		<?php echo $year = $_objArrayList->getYearList2($_REQUEST['y']);?>
	</select>
	</td>
	
	<td>
		<input name="showOrderlist" type="hidden" value="yes" />
		<input name="submit" class="result-submit" type="submit" id="submit" value="Show graph" />
	</td>
	
  </tr>
 
  <tr>
    <td colspan="8"><div id="chart_div" style="width: 100%; height: 450px;"></div></td>
  </tr>
	</table>
 
  	</form>
</div>
	
<div class="clear">&nbsp;</div>
	
</div>
</div>
	<!-- Graph code ends here -->
	<?php include("footer.php") ?>
</body>
</html>