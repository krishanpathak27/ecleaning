<?php
include("includes/config.inc.php");
include("includes/function.php");
include("includes/globalarraylist.php");
include("header.inc.php");
/*echo "<pre>";
print_r($_POST);
echo "</pre>";*/
if(isset($_POST['add']) && $_POST['add'] == 'yes')
{
	$_SESSION['todate'] = $_POST['to'];
	$_SESSION['frmdate'] = $_POST['from'];
	$_SESSION['salesmanID'] = $_POST['sal1'];
} else {
	$_SESSION['todate'] = date("Y-m-d",strtotime("-0 day"));
	$_SESSION['frmdate'] = date("Y-m-d",strtotime("-0 day"));
}
if(isset($_REQUEST['sal']) && $_REQUEST['sal']!="" )
{
	$_SESSION['salesmanID']=base64_decode($_REQUEST['sal']);
	header("Location: salesman_visit_distance_report.php");
}

$report_day = date('D', strtotime($report_to));
/***** Add More Activities By Gaurav (27 Feb 2015) ****************/
 
$auRec=$_objAdmin->_getSelectList2('table_activity',"COUNT(*) AS ttl",''," salesman_id='".$_SESSION['salesmanID']."' and activity_type IN (3,4,5,24,26) and activity_date >= '".date('Y-m-d', strtotime($_SESSION['frmdate']))."' and activity_date <= '".date('Y-m-d', strtotime($_SESSION['todate']))."' and accuracy_level!=0 order by end_time asc");

$row = $auRec[0]->ttl;
if($row <=0)
{
	$err="This Salesman has not worked during these period";
}

?>

<script type="text/javascript">

    function PrintElem(elem)
    {
        Popup($(elem).html());
    }

    function Popup(data) 
    {
		
        var mywindow = window.open('', 'Report');
		
        mywindow.document.write('<html><head><title>New Added Customer</title>');
		mywindow.document.write('<table><tr><td><b>Salesman Name:</b> <?php echo $sal_name; ?></td><td><b>City Name:</b> <?php echo $city_name; ?></td><td><b>From Date:</b> <?php echo $from_date; ?></td><td><b>To Date:</b> <?php echo $to_date; ?></td></tr></table>');
        /*optional stylesheet*/ //mywindow.document.write('<link rel="stylesheet" href="main.css" type="text/css" />');
        mywindow.document.write('</head><body >');
        mywindow.document.write(data);
        mywindow.document.write('</body></html>');

        mywindow.print();
        mywindow.close();
        return true;
    }

$(document).ready(function()
{
<?php if($_POST['submit']=='Export to Excel'){ ?>
tableToExcel('report_export', 'salesman visit distance report', 'salesman visit distance report.xls');
<?php } ?>
});

</script>
<script>
    $(function() {
        $( "#from" ).datepicker({
			dateFormat: "d M yy",
            defaultDate: "1w",
            changeMonth: true,
            numberOfMonths: 1,
            onSelect: function( selectedDate ) {
                $( "#to" ).datepicker( "option", "minDate", selectedDate );
            }
        });
        $( "#to" ).datepicker({
			dateFormat: "d M yy",
            defaultDate: "-1w",
            changeMonth: true,
            numberOfMonths: 1,
            onSelect: function( selectedDate ) {
                $( "#from" ).datepicker( "option", "maxDate", selectedDate );
            }
        });
    });
	$(document).ready(function(){
	    $("#sal").change(function(){
		 document.report.submit();
		})
	});
</script>


<script src="javascripts/jquery-1.8.2.js" type="text/javascript"></script>
<script src="javascripts/jquery-ui.js"></script>
<script type="text/javascript" src="javascripts/validate.js"></script>


<!-- start content-outer -->
<div id="content-outer">
<!-- start content -->
<div id="content">
<div id="page-heading"><h1><span style="color: #d74343; font-weight: bold;">Salesman Visit Distance Report</span></h1> 
	<form name="report" id="report" action="salesman_visit_distance_report.php" method="post">
		<div id="page-heading" align="left" style="padding-left: 350px;"><h3><span style=" color:#000000;">Salesman: 
		
			<select name="sal" id="sal" class="menulist" style="width:150px" >
				<option value="" >Select</option>
				<?php $aSal=$_objAdmin->_getSelectList('table_salesman AS s','*',''," s.status = 'A' $salesman ORDER BY salesman_name"); 
				if(is_array($aSal)){
				for($i=0;$i<count($aSal);$i++){?>
				<option value="<?php echo base64_encode($aSal[$i]->salesman_id);?>" <?php if ($aSal[$i]->salesman_id==$_SESSION['salesmanID']){ ?> selected <?php } ?>><?php echo $aSal[$i]->salesman_name;?></option>
				<?php } }?>
			</select>
		</span>
		</h3>
		</div>
	</form>
	
</div>

<table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">
<tr>
	<!--<td id="tbl-border-left"></td>-->
	<td>
	<!--  start content-table-inner -->
	<div id="content-table-inner">
	
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
	
	<tr valign="top">
	<td>
	<?php
	$auSal=$_objAdmin->_getSelectList('table_salesman',"salesman_name,salesman_id",''," salesman_id='".$_SESSION['salesmanID']."' ");
	if(is_array($auSal)){
	?>
	
	<form name="frmPre" id="frmPre" method="post" action="salesman_visit_distance_report.php" enctype="multipart/form-data" >
		<table style="text-align:center;width:100%;">
			<tr>
				<td>
					<h3>Salesman: 
						<!-- Report Date: 
				  		<input type="text" id="to" name="to" class="date" value="<?php echo $_objAdmin->_changeDate($_SESSION['saldate']);?>"  readonly /> -->
					</h3>
					<h3>
						<span style=" color:#000000;">
							<?php echo $auSal[0]->salesman_name;?>
							<input type="hidden" name="sal1" value="<?php echo $auSal[0]->salesman_id;?>" />
						</span>
					</h3>
				</td>
				<td>
					<h3>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;From Date:</h3>
					<h6>
						<img src="css/images/prev.png" height="18" width="18" onclick="dateFromPrev();"> 
						<input type="text" id="from" name="from" class="date" style="width:150px" value="<?php if($_SESSION['frmdate']!='') { echo $_objAdmin->_changeDate($_SESSION['frmdate']); } else { echo $_objAdmin->_changeDate(date('Y-m-d')); }?>"  readonly /> 
						<img src="css/images/next.png" height="18" width="18" onclick="dateFromNext();">
					</h6>
				</td>
				<td>
					<h3>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;To Date:</h3>
					<h6>
						<img src="css/images/prev.png" height="18" width="18" onclick="dateToPrev();"> 
						<input type="text" id="to" name="to" class="date" style="width:150px" value="<?php if($_SESSION['todate']!='') { echo $_objAdmin->_changeDate($_SESSION['todate']); } else { echo $_objAdmin->_changeDate(date('Y-m-d')); }?>"  readonly /> 
						<img src="css/images/next.png" height="18" width="18" onclick="dateToNext();">
					</h6>
				</td>
				<td>
					<input name="add" type="hidden" value="yes" />
					<a id="dlink"  style="display:none;"></a>
					<input name="submit" class="result-submit" type="submit" id="submit" value="View Details" />
				</td>
				<td>
					<input name="submit" class="result-submit" type="submit" value="Export to Excel" />
				</td>
			</tr>
		</table>
	</form>
	<?php } ?>
	</td>
	<?php 
	if($_SESSION['salesmanID']==""){
	?>
	<tr valign="top">
	<td>
	<table border="0" width="100%" cellpadding="0" cellspacing="0" id="id-form">
			<tr bgcolor="#A52A2A" style="color: #fff;font-weight: bold;">
				<td align="center" style="padding:10px;">Select Sales Team</td>
			</tr>
		</table>
	</td>
	<?php } else { ?>
	<tr valign="top">
	<td>
		<!-- start id-form -->
		<div style="height:500px;overflow:auto;" >
		<table border="0" width="100%" cellpadding="0" cellspacing="0">
			<?php  
			if($err!=''){
			?>
			<tr bgcolor="#A52A2A" style="color: #fff;font-weight: bold;">
				<td align="center" style="padding:10px;"><?php echo $err; ?></td>
			</tr>
			<?php } else { ?>
			<tr>
				<table border="0" width="100%" cellpadding="0" cellspacing="0" id="report_export" name="report_export">
					<tr bgcolor="#6E6E6E" style="color: #fff;font-weight: bold;" >
						<!-- <td style="padding:10px;">Salesman&nbsp;Name</td>
						<td style="padding:10px;">Salesman&nbsp;Code</td> 
						<td style="padding:10px;">Division</td>-->
						<td style="padding:10px;">Customer&nbsp;Name</td>
						<td style="padding:10px;">Customer&nbsp;Code</td>
						<td style="padding:10px;">Customer&nbsp;Class</td>
						<td style="padding:10px;">Customer&nbsp;Type</td>
						<td style="padding:10px;">Activity</td>
						<td style="padding:10px;">Route</td>
						<td style="padding:10px;">Date</td>
						<td style="padding:10px;">Time</td>
						<td style="padding:10px;">Distance(KMS)</td>
						<td style="padding:10px;">Salesman Distance(KMS)</td>
					</tr>
					<?php 
						$classRec =$_objAdmin->_getSelectList2("table_relationship","relationship_id,relationship_desc",'',"status='A'");
						$classArr=array();
						foreach ($classRec as $key => $value) {
							$classArr[$value->relationship_id]=$value->relationship_desc;
						}
						$Rec=$_objAdmin->_getSelectList2("table_activity as ta 
LEFT JOIN table_order as todr1 ON todr1.order_id=ta.ref_id AND ta.activity_type='3'
LEFT JOIN table_retailer as R1 ON R1.retailer_id=todr1.retailer_id 
LEFT JOIN viewRetDisCusSurvey as SR1 ON SR1.retailer_id=R1.retailer_id 
LEFT JOIN table_route_retailer as TRR1 ON TRR1.retailer_id=R1.retailer_id AND TRR1.status='R'
LEFT JOIN table_route as TR1 ON TR1.route_id=TRR1.route_id
LEFT JOIN table_distributors as D1 ON D1.distributor_id=todr1.distributor_id 
LEFT JOIN viewRetDisCusSurvey as SD1 ON SD1.distributor_id=D1.distributor_id AND SD1.retailer_id='0'
LEFT JOIN table_route_retailer as TRR2 ON TRR2.retailer_id=D1.distributor_id AND TRR2.status='D'
LEFT JOIN table_route as TR2 ON TR2.route_id=TRR2.route_id
LEFT JOIN viewRetDisCusSurvey as SR2 ON SR2.retailer_id=ta.ref_id AND ta.activity_type='4'
LEFT JOIN table_retailer as R2 ON R2.retailer_id=SR2.retailer_id 
LEFT JOIN table_route_retailer as TRR3 ON TRR3.retailer_id=R2.retailer_id AND TRR3.status='R'
LEFT JOIN table_route as TR3 ON TR3.route_id=TRR3.route_id
LEFT JOIN viewRetDisCusSurvey as SD2 ON SD2.retailer_id=ta.ref_id AND ta.activity_type='26'
LEFT JOIN table_distributors as D2 ON D2.distributor_id=SD2.distributor_id 
LEFT JOIN table_route_retailer as TRR4 ON TRR4.retailer_id=D2.distributor_id AND TRR4.status='D'
LEFT JOIN table_route as TR4 ON TR4.route_id=TRR4.route_id
LEFT JOIN table_retailer as R3 ON R3.retailer_id=ta.ref_id AND ta.activity_type='5' 
LEFT JOIN table_route_retailer as TRR5 ON TRR5.retailer_id=R3.retailer_id AND TRR5.status='R'
LEFT JOIN table_route as TR5 ON TR5.route_id=TRR5.route_id
LEFT JOIN viewRetDisCusSurvey as SR3 ON SR3.retailer_id=R3.retailer_id
LEFT JOIN table_distributors as D3 ON D3.distributor_id=ta.ref_id AND ta.activity_type='24'
LEFT JOIN table_route_retailer as TRR6 ON TRR6.retailer_id=D3.distributor_id AND TRR6.status='D'
LEFT JOIN table_route as TR6 ON TR6.route_id=TRR6.route_id
LEFT JOIN viewRetDisCusSurvey as SD3 ON SD3.distributor_id=D3.distributor_id AND SD3.retailer_id='0' ",
"ta.activity_id,ta.activity_type, ta.activity_date, ta.end_time, todr1.order_type,todr1.lat as orderLat,todr1.lng as orderLng,
TR1.route_name as orderRetRoute,TR2.route_name as orderDisRoute,TR3.route_name as orderRetRoute2,TR4.route_name as orderDisRoute2,TR5.route_name as orderRetRoute3,TR6.route_name as orderDisRoute3,
R1.retailer_name AS orderRetName, R1.retailer_code  AS orderRetCode,R1.relationship_id AS orderRetClass, R1.lat AS orderRetLat,R1.lng AS orderRetLng,SR1.lat as surveyRetLat,SR1.lng as surveyRetLng,
D1.distributor_name  AS orderDisName, D1.distributor_code  AS orderDisCode,D1.relationship_id AS orderDisClass, D1.lat AS orderDisLat,D1.lng AS orderDisLng,SD1.lat as surveyDisLat,SD1.lng as surveyDisLng,
R2.retailer_name AS orderRetName2, R2.retailer_code AS orderRetCode2,R2.relationship_id AS orderRetClass2, R2.lat AS orderRetLat2,R2.lng AS orderRetLng2, SR2.lat as surveyRetLat2,SR2.lng as surveyRetLng2,
D2.distributor_name AS orderDisName2, D2.distributor_code  AS orderDisCode2,D2.relationship_id AS orderDisClass2, D2.lat AS orderDisLat2,D2.lng AS orderDisLng2,SD2.lat as surveyDisLat2,SD2.lng as surveyDisLng2,
R3.retailer_name AS orderRetName3, R3.retailer_code AS orderRetCode3,R3.relationship_id AS orderRetClass3, R3.lat AS orderRetLat3,R3.lng AS orderRetLng3,SR3.lat as surveyRetLat3,SR3.lng as surveyRetLng3,
D3.distributor_name AS orderDisName3, D3.distributor_code AS orderDisCode3,D3.relationship_id AS orderDisClass3, D3.lat AS orderDisLat3,D3.lng AS orderDisLng3,SD3.lat as surveyDisLat3,SD3.lng as surveyDisLng3",'',
" ta.salesman_id='".$_SESSION['salesmanID']."' and ta.activity_type IN (3,4,5,24,26) and ta.activity_date >= '".date('Y-m-d', strtotime($_SESSION['frmdate']))."' and ta.activity_date <= '".date('Y-m-d', strtotime($_SESSION['todate']))."' order by ta.activity_id asc");

/*echo "<pre>";
print_r($Rec);
echo "</pre>";*/
						$customerLat = "";
						$customerLng = "";
						$customerSurveyLat="";
						$customerSurveyLng="";
						$salesSum = 0;
						$surveySum = 0;
					foreach ($Rec as $key => $value) {
						$customerName = "";
						$customerCode = "";
						$customerClass = "";
						$customerType = "";
						$activity = "";
						$route = "";
						if($value->activity_type=='5'){
							$customerName = $value->orderRetName3;
							$customerCode = $value->orderRetCode3;
							$customerClass = $value->orderRetClass3;
							$customerLat1 = $customerLat;
							$customerLat = $value->orderRetLat3;
							$customerLng1 = $customerLng;
							$customerLng = $value->orderRetLng3;
							$customerSurveyLat1 = $customerSurveyLat;
							$customerSurveyLat = $value->surveyRetLat3;
							$customerSurveyLng1 = $customerSurveyLng;
							$customerSurveyLng = $value->surveyRetLng3;
							$route = $value->orderRetRoute3;
							$customerType = "Retailer";
							$activity = "Add Retailer";
						} else if($value->activity_type=='24'){
							$customerName = $value->orderDisName3;
							$customerCode = $value->orderDisCode3;
							$customerClass = $value->orderDisClass3;
							$customerLat1 = $customerLat;
							$customerLat = $value->orderDisLat3;
							$customerLng1 = $customerLng;
							$customerLng = $value->orderDisLng3;
							$customerSurveyLat1 = $customerSurveyLat;
							$customerSurveyLat = $value->surveyDisLat3;
							$customerSurveyLng1 = $customerSurveyLng;
							$customerSurveyLng = $value->surveyDisLng3;
							$route = $value->orderDisRoute3;
							$customerType = "Distributor";
							$activity = "Add Distributor";
						} else if($value->activity_type=='4'){
							$customerName = $value->orderRetName2;
							$customerCode = $value->orderRetCode2;
							$customerClass = $value->orderRetClass2;
							$customerLat1 = $customerLat;
							$customerLat = $value->surveyRetLat2;//$value->orderRetLat2;
							$customerLng1 = $customerLng;
							$customerLng = $value->surveyRetLng2;//$value->orderRetLng2;
							$customerSurveyLat1 = $customerSurveyLat;
							$customerSurveyLat = $value->surveyRetLat2;
							$customerSurveyLng1 = $customerSurveyLng;
							$customerSurveyLng = $value->surveyRetLng2;
							$route = $value->orderRetRoute2;
							$customerType = "Retailer";
							$activity = "Retailer Survey";
						} else if($value->activity_type=='26'){
							$customerName = $value->orderDisName2;
							$customerCode = $value->orderDisCode2;
							$customerClass = $value->orderDisClass2;
							$customerLat1 = $customerLat;
							$customerLat = $value->surveyDisLat2;//$value->orderDisLat2;
							$customerLng1 = $customerLng;
							$customerLng = $value->surveyDisLng2;//$value->orderDisLng2;
							$customerSurveyLat1 = $customerSurveyLat;
							$customerSurveyLat = $value->surveyDisLat2;
							$customerSurveyLng1 = $customerSurveyLng;
							$customerSurveyLng = $value->surveyDisLng2;
							$route = $value->orderDisRoute2;
							$customerType = "Distributor";
							$activity = "Distributor Survey";
						} else if($value->activity_type=='3'){
							if($value->orderRetName != ""){
								$customerName = $value->orderRetName;
								$customerCode = $value->orderRetCode;
								$customerClass = $value->orderRetClass;
								$customerLat1 = $customerLat;
								$customerLat = $value->orderLat;//$value->orderRetLat;
								$customerLng1 = $customerLng;
								$customerLng = $value->orderLng;//$value->orderRetLng;
								$customerSurveyLat1 = $customerSurveyLat;
								$customerSurveyLat = $value->surveyRetLat;
								$customerSurveyLng1 = $customerSurveyLng;
								$customerSurveyLng = $value->surveyRetLng;
								$route = $value->orderRetRoute;
								$customerType = "Retailer";
								if(strtolower($value->order_type)=="yes"){
									$activity = "Order";
								} else {
									$activity = "No Order";
								}
							} else {
								$customerName = $value->orderDisName;
								$customerCode = $value->orderDisCode;
								$customerClass = $value->orderDisClass;
								$customerLat1 = $customerLat;
								$customerLat = $value->orderLat;//$value->orderRetLat;
								$customerLng1 = $customerLng;
								$customerLng = $value->orderLng;//$value->orderRetLng;
								$customerSurveyLat1 = $customerSurveyLat;
								$customerSurveyLat = $value->surveyDisLat;
								$customerSurveyLng1 = $customerSurveyLng;
								$customerSurveyLng = $value->surveyDisLng;
								$route = $value->orderDisRoute;
								$customerType = "Distributor";
								if(strtolower($value->order_type)=="yes"){
									$activity = "Order";
								} else {
									$activity = "No Order";
								}
							}
						}
						if($customerLat1!="" && $customerLng1!="" && $customerLat!="" && $customerLng!="" && $customerLat1!="0.0" && $customerLng1!="0.0" && $customerLat!="0.0" && $customerLng!="0.0"){
							$salesdistance = distance($customerLat1, $customerLng1, $customerLat, $customerLng, "K");
							$salesSum = $salesSum + $salesdistance;

						} else {
							$salesdistance = "NA";
						}
						if($customerSurveyLat1!="" && $customerSurveyLng1!="" && $customerSurveyLat!="" && $customerSurveyLng!="" && $customerSurveyLat1!="0.0" && $customerSurveyLng1!="0.0" && $customerSurveyLat!="0.0" && $customerSurveyLng!="0.0"){
							$surveydistance = distance($customerSurveyLat1, $customerSurveyLng1, $customerSurveyLat, $customerSurveyLng, "K");
							$surveySum = $surveySum+$surveydistance;
						} else {
							$surveydistance = "NA";
						}
					?>

					<tr style="border:1px solid #CAC5C5;">
						<!-- <td style="padding:10px;"><?php //echo $val->salesman_name;?></td>
						<td style="padding:10px;"><?php //echo $val->salesman_code;?></td> 
						<td style="padding:10px;"><?php //echo $val->customer_name;?></td>-->
						<td style="padding:10px;"><?php echo $customerName;?></td>
						<td style="padding:10px;"><?php echo $customerCode;?></td>
						<td style="padding:10px;"><?php echo $classArr[$customerClass];?></td>
						<td style="padding:10px;"><?php echo $customerType;?></td>
						<td style="padding:10px;"><?php echo $activity;?></td>
						<td style="padding:10px;"><?php echo $route;?></td>
						<td style="padding:10px;"><?php echo $value->activity_date;?></td> 
						<td style="padding:10px;"><?php echo $value->end_time;?></td>
						<td style="padding:10px;"><?php echo round($surveydistance,2);?></td>
						<td style="padding:10px;"><?php echo round($salesdistance,2);?></td>
					</tr>
					<?php }?>
					<tr style="border:1px solid #CAC5C5;">
						<td colspan="8" style="padding:10px;"><b>Total</b></td>
						<td style="padding:10px;"><b><?php echo round($surveySum,2);?></b></td>
						<td style="padding:10px;"><b><?php echo round($salesSum,2);?></b></td>
					</tr>
				</table>
			</tr>
			<?php } ?>
		</table>
		</div>
		<!-- end id-form  -->
	</td>
	<?php } ?>
	<td>
	<!-- right bar-->
	<?php //include("rightbar/salesmanMapList_bar.php") ?>
	</td>
	</tr>
<tr>
<td><img src="images/blank.gif" width="695" height="1" alt="blank" /></td>
<td></td>
</tr>
</table>

<!--  end content-table-inner  -->
</td>
<td id="tbl-border-right"></td>
</tr>
<!--<tr>
	<th class="sized bottomleft"></th>
	<td id="tbl-border-bottom">&nbsp;</td>
	<th class="sized bottomright"></th>
</tr>-->
</table>
<div class="clear">&nbsp;</div>
</div>
<!--  end content -->
<div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->
<div class="clear">&nbsp;</div> 
<!-- start footer -->         
<?php 

function distance($lat1, $lon1, $lat2, $lon2, $unit) {

  $theta = $lon1 - $lon2;
  $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
  $dist = acos($dist);
  $dist = rad2deg($dist);
  $miles = $dist * 60 * 1.1515;
  $unit = strtoupper($unit);

  if ($unit == "K") {
    return ($miles * 1.609344);
  } else if ($unit == "N") {
      return ($miles * 0.8684);
  } else {
        return $miles;
  }
}
include("footer.php");?>
<!-- end footer -->
<script src="javascripts/jquery-1.8.2.js" type="text/javascript"></script>
<script src="javascripts/jquery-ui.js"></script>
<script type="text/javascript" src="javascripts/validate.js"></script>
<script src="javascripts/dateNextPrev.js" type="text/javascript"></script>
<script type='text/javascript'>//<![CDATA[ 
var tableToExcel = (function () {
        var uri = 'data:application/vnd.ms-excel;base64,'
        , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>'
        , base64 = function (s) { return window.btoa(unescape(encodeURIComponent(s))) }
        , format = function (s, c) { return s.replace(/{(\w+)}/g, function (m, p) { return c[p]; }) }
        return function (table, name, filename) {
            if (!table.nodeType) table = document.getElementById(table)
            var ctx = { worksheet: name || 'Worksheet', table: table.innerHTML }

            document.getElementById("dlink").href = uri + base64(format(template, ctx));
            document.getElementById("dlink").download = filename;
            document.getElementById("dlink").click();

        }
    })()
//]]>  
</script>

</body>
</html>