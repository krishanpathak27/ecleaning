<?php
/* Created : Abhishek Jaiswal
*  Date : 9 feb 2016
*/
include("includes/config.inc.php");
include("includes/function.php");
include("includes/globalarraylist.php");

//$_objAdmin = new Admin();
$page_name="Salesman Visited Report";

if(isset($_POST['showReport']) && $_POST['showReport'] == 'yes')
{
	if($_POST['sal']!="")  {
		$_SESSION['SalAttList']=$_POST['sal'];	
	} else {
		unset($_SESSION['SalAttList']);	
	}


	if($_POST['from']!="")  {
	
	 $_SESSION['FromAttList']=$_objAdmin->_changeDate($_POST['from']);
	 $fromDate=date('Y-m-d',strtotime($_SESSION['FromAttList']));

	}

	if($_POST['to']!="")  {
	     $_SESSION['ToAttList']=$_objAdmin->_changeDate($_POST['to']);
		 $toDate=date('Y-m-d',strtotime($_SESSION['ToAttList']));	
	}

	
} else {
	$_SESSION['FromAttList']= $_objAdmin->_changeDate(date("Y-m-d"));
	$_SESSION['ToAttList']= $_objAdmin->_changeDate(date("Y-m-d"));	
}



if(isset($_REQUEST['reset']) && $_REQUEST['reset'] == 'yes')
{
	unset($_SESSION['SalAttList']);	
	unset($_SESSION['FromAttList']);	
	unset($_SESSION['ToAttList']);
	
	header("Location: salesman_visited_distance_report.php");
}

if($_SESSION['SalAttList']!=''){
	$salesman = " AND s.salesman_id=".$_SESSION['SalAttList']; 
} else if($_SESSION['userLoginType']!=5){
	$salesman = "";
}

?>

<?php include("header.inc.php") ?>
<script src="javascripts/dateNextPrev.js" type="text/javascript"></script>
<script type="text/javascript">

    function PrintElem(elem)
    {
        Popup($(elem).html());
    }

    function Popup(data) 
    {
		
        var mywindow = window.open('', 'Report');
		
        mywindow.document.write('<html><head><title>Salesman Visited Distance Report</title>');
		mywindow.document.write('<table><tr><td><b>Salesman Name:</b> <?php echo $sal_name; ?></td><td><b>From Date:</b> <?php echo $_SESSION['FromAttList']; ?></td><td><b>To Date:</b> <?php echo $_SESSION['ToAttList']; ?></td></tr></table>');
        /*optional stylesheet*/ //mywindow.document.write('<link rel="stylesheet" href="main.css" type="text/css" />');
        mywindow.document.write('</head><body >');
        mywindow.document.write(data);
        mywindow.document.write('</body></html>');

        mywindow.print();
        mywindow.close();
        return true;
    }


</script>
<script type="text/javascript">
// Popup window code
function photoPopup(url) {
	popupWindow = window.open(
		url,'popUpWindow','height=500,width=800,left=100,top=100,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no,status=yes')
}
</script>

<style type="text/css">

.row0 {

	background-color: green;
	color: #FFF;
}

</style>

<!-- start content-outer -->
<input name="pagename" type="hidden"  id="pagename" value="admin_order_list.php" />
<div id="content-outer">
<!-- start content -->
<div id="content">
<div id="page-heading"><h1><span style="color: #d74343; font-family: Tahoma; font-weight: bold;">Salesman Visited Distance Report</span></h1></div>
<table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">
<tr>
	<td>
	<!--  start content-table-inner -->
	<div id="content-table-inner" style="overflow-x:scroll; width:1100px;margin-bottom: 40px;">
	
	<div id="page-heading" align="left" >
	<form name="frmPre" id="frmPre" method="post" action="" enctype="multipart/form-data" >
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
		<!-- <tr class="consolidatedReport">
			<td colspan="5" ><h2>View Report For:&nbsp;&nbsp;
			<input type="radio" name="type" value="1" <?php echo ($_SESSION['Type']==1)?"checked":"checked" ?>/>&nbsp;Check In/Out&emsp;
			<input type="radio" name="type" value="2" <?php echo ($_SESSION['Type']==2)?"checked":"" ?>/>&nbsp;Comment&emsp;
			<input type="radio" name="type" value="3" <?php echo ($_SESSION['Type']==3)?"checked":"" ?>/>&nbsp;DSR Comment</h2>
			</td>
		</tr> -->
	<tr>

		<td><h3>Salesman Name: </h3><h6>
		<select name="sal" id="sal" class="styledselect_form_3" style="width:150px;" >
			<?php echo $rtndata = $_objArrayList->GetSalesmenMenu($salsList, $_SESSION['SalAttList']);?>
		</select>
		</h6>
		</td>	
		
	
	
		<td>
			<h3>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;From Date: </h3>
			<h6>
				<img src="css/images/prev.png" height="18" width="18" onclick="dateFromPrev();"> 
				<input type="text" id="from" name="from" class="date" style="width:120px" value="<?php  echo $_SESSION['FromAttList'];?>"  readonly /> 
				<img src="css/images/next.png" height="18" width="18" onclick="dateFromNext();">
			</h6>
		</td>
		<td>
			<h3>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;To Date: </h3>
			<h6>
				<img src="css/images/prev.png" height="18" width="18" onclick="dateToPrev();"> 
				<input type="text" id="to" name="to" class="date" style="width:120px" value="<?php echo $_SESSION['ToAttList']; ?>"  readonly /> 
				<img src="css/images/next.png" height="18" width="18" onclick="dateToNext();">
			</h6>
		</td>        
		
	</tr>

	
	<tr>
		<td>
			<input name="submit" class="result-submit" type="submit" id="submit" value="View Details" />
			<input type="button" value="Reset!" class="form-reset" onclick="location.href='salesman_visited_report.php?reset=yes';" />
			<input name="showReport" type="hidden" value="yes" />
		</td>
		<td colspan="5">
			<input type="button" value="Print" class="result-submit" onclick="PrintElem('#Report')" />
			<a id="dlink"  style="display:none;"></a>
			<input input type="button" value="Export to Excel" class="result-submit"  onclick="tableToExcel('report_export', 'Salesman Visited Report', 'Salesman visited Distance Report.xls')">
		</td>
	</tr>
	</table>
	</form>
	</div>
	<div id="Report">
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
	<tr valign="top">
		<td>
		<table  border="0" width="100%" cellpadding="0" cellspacing="0" id="report_export" name="report_export">
			<div id="Report">
			<tr bgcolor="#6E6E6E" style="color: #fff;font-weight: bold;" >
				
				<td style="padding:10px;" width="20%">Salesman&nbsp;Name</td>
				<td style="padding:10px;" width="20%">Salesman&nbsp;Code</td>
				<td style="padding:10px;" width="15%">Date</td>
				<td style="padding:10px;" width="15%">Total&nbsp;Distance</td>
			</tr>
	<?php
		$auAttSt=$_objAdmin->_getSelectList2('table_activity AS ta 
			LEFT JOIN table_salesman AS s ON ta.salesman_id = s.salesman_id
			','ta.salesman_id,s.salesman_name,s.salesman_code,ta.activity_date,ta.lat,ta.lng','',"ta.activity_type IN(3,5,11,12,13) ".$salesman." AND (activity_date BETWEEN '".date('Y-m-d', strtotime($_SESSION['FromAttList']))."' AND '".date('Y-m-d', strtotime($_SESSION['ToAttList']))."')" );

		
			
		$report = array();
		$count=1;
		$flag=1;

		function distance($lat1, $lon1, $lat2, $lon2, $unit) {

  $theta = $lon1 - $lon2;
  $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
  $dist = acos($dist);
  $dist = rad2deg($dist);
  $miles = $dist * 60 * 1.1515;
  $unit = strtoupper($unit);

  if ($unit == "K") {
    return ($miles * 1.609344);
  } else if ($unit == "N") {
      return ($miles * 0.8684);
  } else {
        return $miles;
  }
}

		if(count($auAttSt)>0){

		foreach ($auAttSt as $key=>$value) :?>

			<?php  
			
			$salesSum = 0; 


		$auRec =$_objAdmin->_getSelectList2('table_activity',"lat,lng,end_time as app_time,accuracy_level",''," salesman_id='".$value->salesman_id."' and activity_type IN (3,5,11,12,13) and activity_date='".$value->activity_date."' and accuracy_level!=0  order by end_time asc");
	/*echo "<pre/>";
		print_r($auRec);*/
	
		foreach ($auRec as $key => $value1) { 

	  $salesLat1 = $auRec[$key]->lat;

	 if(isset($auRec[$key+1]->lat)) 
	 	$salesLat =  $auRec[$key+1]->lat;
	 else 
	 	$salesLat = $auRec[$key]->lat;

	  $salesLng1 = $auRec[$key]->lng;


	if(isset($auRec[$key+1]->lng)) 
	 	$salesLng =  $auRec[$key+1]->lng;
	 else 
	 	$salesLng = $auRec[$key]->lng;

      $salesdistance = distance($salesLat1, $salesLng1, $salesLat, $salesLng, "K");

	if(!is_nan($salesdistance)){

		  $salesSum = $salesSum + $salesdistance;
	}

						
}

 ?>


			<?php 				
				
				$report[$value->activity_date][$value->salesman_id]['salesman_name'] = $value->salesman_name;
				$report[$value->activity_date][$value->salesman_id]['salesman_code'] = $value->salesman_code;
				
				$report[$value->activity_date][$value->salesman_id]['activity_date'] = $value->activity_date;
				
				$report[$value->activity_date][$value->salesman_id]['distance'] = $salesSum;

		endforeach;

		foreach ($report as $key => $val1) {
			foreach ($val1 as $key => $val) {
				?>
				<tr style="border:1px solid #CAC5C5;">

					<td style="padding:10px;" width="20%"><?php echo $val['salesman_name']?></td>
					<td style="padding:10px;" width="20%"><?php echo $val['salesman_code']?></td>
								
					
					<td style="padding:10px;" width="15%"><?php echo $val['activity_date'];?></td>
					
					<td style="padding:10px;" width="15%"><?php echo round($val['distance'],2)." km" ;//echo $val['distance'];?></td>
				</tr>
			<?php
		}
			
			}?>

			<tr bgcolor="#6E6E6E" style="color: #fff;font-weight: bold;">
			
			</tr>
			<?php  } else{ ?>
			<tr  style="border-bottom:2px solid #6E6E6E;" align="center">
				<td style="padding:10px;" colspan="7">Report Not Available</td>
			</tr>
			<?php } ?>	
		</div>
	</table>	
</td>
</tr>
<tr><td><img src="images/blank.gif" width="695" height="1" alt="blank" /></td><td></td></tr>
</table>
</div>
<div class="clear"></div>
</div>
<!--  end content-table-inner  -->
</td>
<td id="tbl-border-right"></td>
</tr>
<!--<tr>
	<th class="sized bottomleft"></th>
	<td id="tbl-border-bottom">&nbsp;</td>
	<th class="sized bottomright"></th>
</tr>-->
</table>
<div class="clear">&nbsp;</div>
</div>
<!--  end content -->
<div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->
<div class="clear">&nbsp;</div> 
<!-- start footer -->         
<?php include("footer.php") ?>
<?php //unset($_SESSION['SalAttList']);	?>
<!-- end footer -->
<script src="javascripts/jquery-1.8.2.js" type="text/javascript"></script>
<script src="javascripts/jquery-ui.js"></script>
<script type="text/javascript" src="javascripts/validate.js"></script>
<script type="text/javascript">
// Popup window code
function newPopup(url) {
	popupWindow = window.open(
		url,'popUpWindow','height=500,width=600,left=200,top=100,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no,status=yes')
}
</script>
<script>
    $(function() {
        $( "#from" ).datepicker({
			dateFormat: "d M yy",
            defaultDate: "w",
            changeMonth: true,
            numberOfMonths: 1,
            onSelect: function( selectedDate ) {
                $( "#to" ).datepicker( "option", "minDate", selectedDate );
            }
        });
        $( "#to" ).datepicker({
			dateFormat: "d M yy",
            defaultDate: "-w",
            changeMonth: true,
            numberOfMonths: 1,
            onSelect: function( selectedDate ) {
                $( "#from" ).datepicker( "option", "maxDate", selectedDate );
            }
        });
    });
</script>
<script type='text/javascript'>//<![CDATA[ 
var tableToExcel = (function () {
        var uri = 'data:application/vnd.ms-excel;base64,'
        , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table><tr><td><b>Salesman Name:</b> <?php echo $sal_name; ?></td><td><b>From Date:</b> <?php echo $_SESSION['FromAttList']; ?></td><td><b>To Date:</b> <?php echo $_SESSION['ToAttList']; ?></td></tr></table><table>{table}</table></body></html>'
        , base64 = function (s) { return window.btoa(unescape(encodeURIComponent(s))) }
        , format = function (s, c) { return s.replace(/{(\w+)}/g, function (m, p) { return c[p]; }) }
        return function (table, name, filename) {
            if (!table.nodeType) table = document.getElementById(table)
            var ctx = { worksheet: name || 'Worksheet', table: table.innerHTML }

            document.getElementById("dlink").href = uri + base64(format(template, ctx));
            document.getElementById("dlink").download = filename;
            document.getElementById("dlink").click();

        }
    })()

//]]>  
</script>
</body>
</html>
