<?php
/* Created : Abhishek Jaiswal
*  Date : 9 feb 2016
*/
include("includes/config.inc.php");
include("includes/function.php");
include("includes/globalarraylist.php");

//$_objAdmin = new Admin();
$page_name="Salesman Visited Report";

if(isset($_POST['showReport']) && $_POST['showReport'] == 'yes')
{
	if($_POST['sal']!="")  {
		$_SESSION['SalAttList']=$_POST['sal'];	
	} else {
		unset($_SESSION['SalAttList']);	
	}


	if($_POST['from']!="")  {
	
	 $_SESSION['FromAttList']=$_objAdmin->_changeDate($_POST['from']);
	 $fromDate=date('Y-m-d',strtotime($_SESSION['FromAttList']));

	}

	if($_POST['to']!="")  {
	     $_SESSION['ToAttList']=$_objAdmin->_changeDate($_POST['to']);
		 $toDate=date('Y-m-d',strtotime($_SESSION['ToAttList']));	
	}

	if($_POST['division_id']!="") 
	{
		$_SESSION['DivisionAttList']=$_POST['division_id'];	
	}
	if($_POST['ctype']!=""){
		$_SESSION['CustomerType']=$_POST['ctype'];
	}
	if($_POST['type'] != ""){
		$_SESSION['Type']=$_POST['type'];

	}
	if($_POST['reason_id'] != ""){

		$_SESSION['CommentType']=$_POST['reason_id'];
	}

} else {
	$_SESSION['FromAttList']= $_objAdmin->_changeDate(date("Y-m-d"));
	$_SESSION['ToAttList']= $_objAdmin->_changeDate(date("Y-m-d"));	
}



if(isset($_REQUEST['reset']) && $_REQUEST['reset'] == 'yes')
{
	unset($_SESSION['SalAttList']);	
	unset($_SESSION['FromAttList']);	
	unset($_SESSION['ToAttList']);
	unset($_SESSION['DivisionAttList']);
	unset($_SESSION['CustomerType']);
	unset($_SESSION['Type']);
	unset($_SESSION['CommentType']);
	header("Location: salesman_visited_report.php");
}
  /* $hierarchy_id=$_objAdmin->_getSelectList2('table_salesman_hierarchy_relationship',"hierarchy_id",''," salesman_id='".$_SESSION['SalAttList']."' ");
         $hire_id=$hierarchy_id[0]->hierarchy_id;

        $hierarchy_id=$_objAdmin->_getSelectList2('table_salesman_hierarchy_relationship',"salesman_id",''," hierarchy_id <='".$hire_id."' ");

		$array=array();
		foreach ($hierarchy_id as $value) 
		{
		    $array[] = $value->salesman_id;
		}

        $salList = implode(",", $array); */

       /* $Ids=$_SESSION['SalAttList'];

		   $aSal=$_objAdmin->_getSelectList('table_salesman AS s  
		   LEFT JOIN table_salesman_hierarchy_relationship AS SH ON SH.salesman_id = s.salesman_id
		   LEFT JOIN table_salesman_hierarchy AS H ON H.hierarchy_id = SH.hierarchy_id
		   LEFT JOIN table_salesman_hierarchy AS H2 ON H2.hierarchy_id = SH.rpt_hierarchy_id AND H2.sort_order < H.sort_order 
		   LEFT JOIN table_salesman AS S2 ON S2.salesman_id = SH.rpt_user_id  AND H2.sort_order < H.sort_order ', 's.salesman_id, H.sort_order ',''," SH.rpt_user_id = '".$Ids."'  ORDER BY s.salesman_name");
		   
$array1=array();
foreach ($aSal as $value) 
{
	$array1[] = $value->salesman_id;
}*/



/*if(is_array($array1))
  {
   $salList = implode(",", $array1); 
}else
{$salList=0;
	
}
*/

/*if(sizeof($array1)>0)
{
   $salList = implode(",", $array1); 
}else
{
	$salList=0;
}*/


$IDarray=array();

function getSalesbottomhierarchy($salID, $sortOrder, $IDlist = array()) {
 		 $_objAdmin = new Admin();
		  /* echo "<pre>";
		   print_r($salID);
		   echo "<br>";
		   echo $sortOrder;
		   exit;*/

		  if($salID[0]!='') 
		  {
		  	
			 if(count($salID)-1 == 0)
			 {
			 	 $IDlist[] = $salID[0];
			 }
		   $Ids = implode(',', $salID);
		
		  //echo $Ids; exit;
		   //if(isset($sortOrder) && $sortOrder!=0 && $sortOrder!=''){  $sort = "AND H.sort_order >= ".$sortOrder; }
		   
		   $aSal=$_objAdmin->_getSelectList('table_salesman AS s  
		   LEFT JOIN table_salesman_hierarchy_relationship AS SH ON SH.salesman_id = s.salesman_id
		   LEFT JOIN table_salesman_hierarchy AS H ON H.hierarchy_id = SH.hierarchy_id
		   LEFT JOIN table_salesman_hierarchy AS H2 ON H2.hierarchy_id = SH.rpt_hierarchy_id AND H2.sort_order < H.sort_order 
		   LEFT JOIN table_salesman AS S2 ON S2.salesman_id = SH.rpt_user_id AND H2.sort_order < H.sort_order ', 's.salesman_id, H.sort_order ',''," SH.rpt_user_id IN ($Ids) $sort ORDER BY s.salesman_name");
		  
			 if(is_object($aSal) || is_array($aSal)) 
			 {
				
			 	
			 	foreach ($aSal as $key=>$val) :
				
			   if($val->sort_order > $sortOrder)  {
			  
			   $IDlist[] = $val->salesman_id;
			   // print_R($IDlist);
			  
			   // exit;
		
				 $res = getSalesbottomhierarchy($IDlist, $val->sort_order, $IDlist);				
			   }	

			  endforeach ;
		
			 } 
			 return $IDlist; 
		   } 
		  //return $IDlist;
    		   //echo"<pre>";
		   //print_R($IDarray);
  		  // $IDarray = array_merge($IDarray, $salID);
  		   //echo"<pre>";
  		   //print_R($IDarray);
  		//return $IDarray;
}

function getSortOrderofsalesTeam($SalID) {
	 	$_objAdmin = new Admin();
	
		$getSortOrder = $_objAdmin->_getSelectList('table_salesman AS s  
		LEFT JOIN table_salesman_hierarchy_relationship AS SH ON SH.salesman_id = s.salesman_id 
		LEFT JOIN table_salesman_hierarchy AS H ON H.hierarchy_id = SH.hierarchy_id ', 
		'H.sort_order',''," s.salesman_id = ".$SalID." ORDER BY s.salesman_name");
		
		if(is_array($getSortOrder) && isset($getSortOrder[0]->sort_order)) 
			return $getSortOrder[0]->sort_order; else return $sort_order = false;
		
							
}


//$res=array(63);
//$sort_order=4;
//print_R($res[0]);
		if($_SESSION['SalAttList'])
		$salesman_id[] = $_SESSION['SalAttList'];
		else
		$salesman_id[] = 0;

		$sort_order = getSortOrderofsalesTeam($salesman_id[0]);		

		$list = getSalesbottomhierarchy($salesman_id, $sort_order);
		/*echo"<pre>";
		print_R($list);
		exit;*/






if(sizeof($list)>0)
{
   $salList = implode(",", $list); 
}else
{
	$salList=0;
}

if($_SESSION['SalAttList']!=''){
	$salesman=" and s.salesman_id IN (".$salList.")";
	//$salesman = " AND s.salesman_id=".$_SESSION['SalAttList']; 
} else if($_SESSION['userLoginType']!=5){
	$salesman = "";
}


if($_SESSION['DivisionAttList']!='' && $_SESSION['DivisionAttList']!='all'){
    $divisionCond=" and s.division_id='".$_SESSION['DivisionAttList']."'";
} 
if($_SESSION['CustomerType']!='' && $_SESSION['CustomerType']!='all'){
    $customerTypeCond=" AND ta.ref_type='".$_SESSION['CustomerType']."'";
}
if($_SESSION['CustomerType']!='' && $_SESSION['CustomerType']!='all' && $_SESSION['Type']=='2'){
	if($_SESSION['CustomerType'] == 1){
		$customerTypeCond=" AND tc.customer_type='R'";
	} elseif($_SESSION['CustomerType'] == 2) {
		$customerTypeCond=" AND tc.customer_type='D'";
	}
	
    
}
if($_SESSION['CommentType']!='' && $_SESSION['CommentType']!='all'){
    $commentTypeCond=" AND tscr.reason_id='".$_SESSION['CommentType']."'";
}
else
{
	$commentTypeCond="all";	
}
?>

<?php include("header.inc.php") ?>
<script src="javascripts/dateNextPrev.js" type="text/javascript"></script>
<script type="text/javascript">

    function PrintElem(elem)
    {
        Popup($(elem).html());
    }

    function Popup(data) 
    {
		
        var mywindow = window.open('', 'Report');
		
        mywindow.document.write('<html><head><title>Salesman Visited Report</title>');
		mywindow.document.write('<table><tr><td><b>Salesman Name:</b> <?php echo $sal_name; ?></td><td><b>From Date:</b> <?php echo $_SESSION['FromAttList']; ?></td><td><b>To Date:</b> <?php echo $_SESSION['ToAttList']; ?></td></tr></table>');
        /*optional stylesheet*/ //mywindow.document.write('<link rel="stylesheet" href="main.css" type="text/css" />');
        mywindow.document.write('</head><body >');
        mywindow.document.write(data);
        mywindow.document.write('</body></html>');

        mywindow.print();
        mywindow.close();
        return true;
    }


</script>
<script type="text/javascript">
// Popup window code
function photoPopup(url) {
	popupWindow = window.open(
		url,'popUpWindow','height=500,width=800,left=100,top=100,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no,status=yes')
}
</script>

<style type="text/css">

.row0 {

	background-color: green;
	color: #FFF;
}

</style>

<!-- start content-outer -->
<input name="pagename" type="hidden"  id="pagename" value="admin_order_list.php" />
<div id="content-outer">
<!-- start content -->
<div id="content">
<div id="page-heading"><h1><span style="color: #d74343; font-family: Tahoma; font-weight: bold;">Salesman Visited Report</span></h1></div>
<table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">
<tr>
	<td>
	<!--  start content-table-inner -->
	<div id="content-table-inner" style="overflow-x:scroll; width:1100px;margin-bottom: 40px;">
	
	<div id="page-heading" align="left" >
	<form name="frmPre" id="frmPre" method="post" action="" enctype="multipart/form-data" >
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
		<tr class="consolidatedReport">
			<td colspan="5" ><h2>View Report For:&nbsp;&nbsp;
			<input type="radio" name="type" value="1" <?php echo ($_SESSION['Type']==1)?"checked":"checked" ?>/>&nbsp;Check In/Out&emsp;
			<!-- <input type="radio" name="type" value="2" <?php echo ($_SESSION['Type']==2)?"checked":"" ?>/>&nbsp;Comment&emsp; -->
			<input type="radio" name="type" value="3" <?php echo ($_SESSION['Type']==3)?"checked":"" ?>/>&nbsp;DSR Comment</h2>
			</td>
		</tr>
	<tr>

		<td><h3>Salesman Name: </h3><h6>
		<select name="sal" id="sal" class="styledselect_form_3" style="width:150px;" >
			<?php echo $rtndata = $_objArrayList->GetSalesmenMenu($salsList, $_SESSION['SalAttList']);?>
		</select>
		</h6>
		</td>
		<td><h3>Division: </h3><h6>
		<select name="division_id" id="division_id" class="styledselect_form_3" style="width:150px;">
			<option value="all">All</option>
			<?php $divisionList=$_objAdmin->_getSelectList('table_division',"division_id,division_name",''," order by division_name asc ");
			foreach($divisionList as $divisionVal){
				
				//if($_SESSION['DivisionAttList'] == $divisionVal->division_id){ $select="selected"; }else {$select="";}
				if($_POST['division_id'] == $divisionVal->division_id){ $select="selected"; }else {$select="";}
			?>
			<option value="<?php echo $divisionVal->division_id; ?>" <?php echo $select; ?>><?php echo $divisionVal->division_name; ?></option>
			<?php	
			}
			?>
		</select>
		</h6>
		</td>
		<td><h3>Customer Type: </h3><h6>
		<select name="ctype" id="ctype" class="styledselect_form_3" style="width:150px;" >
			<option value="all">All</option>
			<option value="1" <?php echo ($_SESSION['CustomerType']=='1')?'selected':''?>>Dealer</option>
			<option value="2" <?php echo ($_SESSION['CustomerType']=='2')?'selected':''?>>Distributor</option>
		</select>
		</h6>
		</td>
		<td><h3>Comment Reason: </h3><h6>
		<select name="reason_id" id="reason_id" class="styledselect_form_3" style="width:150px;">
			<option value="all">All</option>
			<?php $reasonList=$_objAdmin->_getSelectList2('table_salesman_comment_reasonList',"reason_id,reason_desc",''," order by reason_id asc ");
			foreach($reasonList as $reasonVal){
				
				
				if($_POST['reason_id'] == $reasonVal->reason_id){ $select="selected"; }else {$select="";}
			?>
			<option value="<?php echo $reasonVal->reason_id; ?>" <?php echo $select; ?>><?php echo $reasonVal->reason_desc; ?></option>
			<?php	
			}
			?>
		</select>
		</h6>
		</td>
		<?php /*if(!isset($_SESSION['Type']) || $_SESSION['Type'] == '2') { ?>
		<td><h3>Comment Reason: </h3><h6>
		<select name="reason_id" id="reason_id" class="styledselect_form_3" style="width:150px;">
			<option value="all">All</option>
			<?php $reasonList=$_objAdmin->_getSelectList2('table_salesman_comment_reasonList',"reason_id,reason_desc",''," order by reason_id asc ");
			foreach($reasonList as $reasonVal){
				
				
				if($_POST['reason_id'] == $reasonVal->reason_id){ $select="selected"; }else {$select="";}
			?>
			<option value="<?php echo $reasonVal->reason_id; ?>" <?php echo $select; ?>><?php echo $reasonVal->reason_desc; ?></option>
			<?php	
			}
			?>
		</select>
		</h6>
		</td>
		<?php }*/ ?>
	
	
		<td>
			<h3>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;From Date: </h3>
			<h6>
				<img src="css/images/prev.png" height="18" width="18" onclick="dateFromPrev();"> 
				<input type="text" id="from" name="from" class="date" style="width:120px" value="<?php  echo $_SESSION['FromAttList'];?>"  readonly /> 
				<img src="css/images/next.png" height="18" width="18" onclick="dateFromNext();">
			</h6>
		</td>
		<td>
			<h3>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;To Date: </h3>
			<h6>
				<img src="css/images/prev.png" height="18" width="18" onclick="dateToPrev();"> 
				<input type="text" id="to" name="to" class="date" style="width:120px" value="<?php echo $_SESSION['ToAttList']; ?>"  readonly /> 
				<img src="css/images/next.png" height="18" width="18" onclick="dateToNext();">
			</h6>
		</td>        
		
	</tr>

	
	<tr>
		<td>
			<input name="submit" class="result-submit" type="submit" id="submit" value="View Details" />
			<input type="button" value="Reset!" class="form-reset" onclick="location.href='salesman_visited_report.php?reset=yes';" />
			<input name="showReport" type="hidden" value="yes" />
		</td>
		<td colspan="5">
			<input type="button" value="Print" class="result-submit" onclick="PrintElem('#Report')" />
			<a id="dlink"  style="display:none;"></a>
			<input input type="button" value="Export to Excel" class="result-submit"  onclick="tableToExcel('report_export', 'Salesman Visited Report', 'Salesman visited Report.xls')">
		</td>
	</tr>
	</table>
	</form>
	</div>
	<div id="Report">
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
	<tr valign="top">
		<td>
		<?php if(!isset($_SESSION['Type']) || $_SESSION['Type'] == '1') {?>
		<table  border="0" width="100%" cellpadding="0" cellspacing="0" id="report_export" name="report_export">
			<div id="Report">
			<tr bgcolor="#6E6E6E" style="color: #fff;font-weight: bold;" >
				
				<?php // if($_SESSION['SalAttList']==''){ ?>

				<td style="padding:10px;" width="20%">Salesman&nbsp;Name</td>
				<td style="padding:10px;" width="20%">Salesman&nbsp;Code</td>

				<?php // } ?>

				<td style="padding:10px;" width="10%">Customer&nbsp;Name</td>
				<td style="padding:10px;" width="15%">Customer&nbsp;Code</td>
				<td style="padding:10px;" width="15%">Customer&nbsp;Type</td>
				<!-- <td style="padding:10px;" width="15%">Customer&nbsp;Interest</td> -->
				<td style="padding:10px;" width="10%">Division</td>
				<td style="padding:10px;" width="10%">State</td>
				<td style="padding:10px;" width="10%">City</td> 
				<!-- <td style="padding:10px;" width="15%">Taluka</td> -->
				<td style="padding:10px;" width="15%">Date</td>
				<td style="padding:10px;" width="20%">Comment</td>
				<td style="padding:10px;" width="20%">Comment Reason</td>
				<td style="padding:10px;" width="15%">Checkin&nbsp;Address</td>
				<td style="padding:10px;" width="15%">Checkout&nbsp;Address</td>
				<td style="padding:10px;" width="15%">Check&nbsp;In</td>
				<td style="padding:10px;" width="15%">Check&nbsp;Out</td>
				<!-- <td style="padding:10px;" width="15%">Total&nbsp;Distance</td> -->
			</tr>
	<?php
		$auAttSt=$_objAdmin->_getSelectList2('table_activity AS ta 
			LEFT JOIN table_salesman AS s ON ta.salesman_id = s.salesman_id
			LEFT JOIN table_division AS tdiv ON s.division_id = tdiv.division_id
			LEFT JOIN table_retailer as tr ON ta.ref_id=tr.retailer_id AND ta.ref_type=1 
			LEFT JOIN table_distributors as td ON ta.ref_id=td.distributor_id AND ta.ref_type=2
			LEFT JOIN table_customer as cus1 ON ta.ref_id=cus1.customer_id AND ta.ref_type=3
			LEFT JOIN table_customer as cus2 ON ta.ref_id=cus2.customer_id AND ta.ref_type=4
			LEFT JOIN table_customer as cus3 ON ta.ref_id=cus3.customer_id AND ta.ref_type=5','ta.salesman_id,s.salesman_name,s.salesman_code,ta.activity_date,tdiv.division_name,ta.activity_type,ta.activity_id,ta.lat,ta.lng,
			CASE WHEN ta.ref_type=1 THEN tr.retailer_id WHEN ta.ref_type=2 THEN td.distributor_id WHEN ta.ref_type=3 THEN cus1.customer_id WHEN ta.ref_type=4 THEN cus2.customer_id WHEN ta.ref_type=5 THEN cus3.customer_id END AS customer_id,
			CASE WHEN ta.ref_type=1 THEN tr.retailer_name WHEN ta.ref_type=2 THEN td.distributor_name WHEN ta.ref_type=3 THEN cus1.customer_name WHEN ta.ref_type=4 THEN cus2.customer_name WHEN ta.ref_type=5 THEN cus3.customer_name END AS customer_name,
			CASE WHEN ta.ref_type=1 THEN tr.retailer_code WHEN ta.ref_type=2 THEN td.distributor_code WHEN ta.ref_type=3 THEN cus1.customer_code WHEN ta.ref_type=4 THEN cus2.customer_code WHEN ta.ref_type=5 THEN cus3.customer_code END AS customer_code,
			CASE WHEN ta.ref_type=1 THEN "Dealer" WHEN ta.ref_type=2 THEN "Distributor" WHEN ta.ref_type=3 THEN "Shakti Partner" WHEN ta.ref_type=4 THEN "Pump Installer" WHEN ta.ref_type=5 THEN "Solar Pump Installer" END AS customer_type,
			CASE WHEN ta.ref_type=1 THEN tr.state WHEN ta.ref_type=2 THEN td.state WHEN ta.ref_type=3 THEN cus1.state WHEN ta.ref_type=4 THEN cus2.state WHEN ta.ref_type=5 THEN cus3.state END AS state,
			CASE WHEN ta.ref_type=1 THEN tr.city WHEN ta.ref_type=2 THEN td.city WHEN ta.ref_type=3 THEN cus1.city WHEN ta.ref_type=4 THEN cus2.city WHEN ta.ref_type=5 THEN cus3.city END AS city,
			CASE WHEN ta.ref_type=1 THEN tr.taluka_id WHEN ta.ref_type=2 THEN td.taluka_id WHEN ta.ref_type=3 THEN cus1.taluka_id WHEN ta.ref_type=4 THEN cus2.taluka_id WHEN ta.ref_type=5 THEN cus3.taluka_id END AS taluka,
			CASE WHEN ta.activity_type=28 THEN ta.acc_address END AS checkin_address,
			CASE WHEN ta.activity_type=29 THEN ta.acc_address END AS checkout_address,
			CASE WHEN ta.activity_type=28 THEN ta.start_time END AS checkintime,
			CASE WHEN ta.activity_type=29 THEN ta.start_time END AS checkouttime','',"ta.activity_type IN(28,29) ".$salesman." ".$divisionCond." ".$customerTypeCond." AND (activity_date BETWEEN '".date('Y-m-d', strtotime($_SESSION['FromAttList']))."' AND '".date('Y-m-d', strtotime($_SESSION['ToAttList']))."')" );

		
			
		$report = array();
		$count=1;
		$flag=1;

		function distance($lat1, $lon1, $lat2, $lon2, $unit) {

  $theta = $lon1 - $lon2;
  $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
  $dist = acos($dist);
  $dist = rad2deg($dist);
  $miles = $dist * 60 * 1.1515;
  $unit = strtoupper($unit);

  if ($unit == "K") {
    return ($miles * 1.609344);
  } else if ($unit == "N") {
      return ($miles * 0.8684);
  } else {
        return $miles;
  }
}

		if(count($auAttSt)>0){

		foreach ($auAttSt as $key=>$value) :?>

			<?php  
			
			$salesSum = 0; 


		$auRec =$_objAdmin->_getSelectList2('table_activity',"lat,lng,end_time as app_time,accuracy_level",''," salesman_id='".$value->salesman_id."' and activity_type IN (3,5,11,12,13) and activity_date='".$value->activity_date."' and accuracy_level!=0  order by end_time asc");
	/*echo "<pre/>";
		print_r($auRec);*/
	
		foreach ($auRec as $key => $value1) {
   
  

	  $salesLat1 = $auRec[$key]->lat;

	 if(isset($auRec[$key+1]->lat)) 
	 	$salesLat =  $auRec[$key+1]->lat;
	 else 
	 	$salesLat = $auRec[$key]->lat;

	  $salesLng1 = $auRec[$key]->lng;


	if(isset($auRec[$key+1]->lng)) 
	 	$salesLng =  $auRec[$key+1]->lng;
	 else 
	 	$salesLng = $auRec[$key]->lng;

      $salesdistance = distance($salesLat1, $salesLng1, $salesLat, $salesLng, "K");

	if(!is_nan($salesdistance)){

		  $salesSum = $salesSum + $salesdistance;
	}

						
}

 ?>


			<?php 
				if(is_array($report[$value->activity_date][$value->customer_id]) && $report[$value->activity_date][$value->customer_id]['checkin'] != '' && $report[$value->activity_date][$value->customer_id]['checkout'] != ''){
					$value->customer_id = $value->customer_id."_".$count;
					if($flag <= 1){
						$flag++;
					} else {
						$flag=1;
						$count++;
					}
				}

				$report[$value->activity_date][$value->customer_id]['customer_id'] = $value->customer_id;
				$report[$value->activity_date][$value->customer_id]['salesman_name'] = $value->salesman_name;
				$report[$value->activity_date][$value->customer_id]['salesman_code'] = $value->salesman_code;
				$report[$value->activity_date][$value->customer_id]['customer_name'] = $value->customer_name;
				$report[$value->activity_date][$value->customer_id]['customer_code'] = $value->customer_code;
				$report[$value->activity_date][$value->customer_id]['customer_type'] = $value->customer_type;
				
				$report[$value->activity_date][$value->customer_id]['division_name'] = $value->division_name;
				$report[$value->activity_date][$value->customer_id]['activity_date'] = $value->activity_date;
				$report[$value->activity_date][$value->customer_id]['city'] = $value->city;
				$report[$value->activity_date][$value->customer_id]['state'] = $value->state;
				
				$report[$value->activity_date][$value->customer_id]['activity_id'][] = $value->activity_id;
				$report[$value->activity_date][$value->customer_id]['lat'][] = $value->lat;
				$report[$value->activity_date][$value->customer_id]['lng'][] = $value->lng;
				if($value->activity_type === "28"){
					$report[$value->activity_date][$value->customer_id]['checkin_address'] = $value->checkin_address;
					$report[$value->activity_date][$value->customer_id]['checkin'] = $value->checkintime;
				} else if($value->activity_type === "29"){
					$report[$value->activity_date][$value->customer_id]['checkout_address'] = $value->checkout_address;
					$report[$value->activity_date][$value->customer_id]['checkout'] = $value->checkouttime;
				} else {}
				$report[$value->activity_date][$value->customer_id]['distance'] = $salesSum;

		endforeach;
		foreach ($report as $key => $rpt) {

			foreach ($rpt as $key => $val) { 

				$state=$_objAdmin->_getSelectList2('state','state_name','',"state_id='".$val['state']."'");
				$city=$_objAdmin->_getSelectList2('city','city_name','',"city_id='".$val['city']."'");
				$taluka=$_objAdmin->_getSelectList2('table_taluka','taluka_name','',"taluka_id='".$val['taluka']."'");

				$cusid=$val['customer_id'];
				$customer_id=explode('_', $cusid);

				$sql1=mysql_query("select reason_id,comment_desc from table_customer_comments where customer_id=".$customer_id[0]." AND app_date='".$val['activity_date']."'");

				$res1=mysql_fetch_array($sql1);

				if($res1['reason_id']==$commentTypeCond)
				{

				$sql2=mysql_query("select reason_desc from table_salesman_comment_reasonList where reason_id=".$res1['reason_id']."");
				$res2=mysql_fetch_array($sql2);

				?>
				<tr style="border:1px solid #CAC5C5;">
				<?php  if($_SESSION['SalAttList']==''){?>
					<td style="padding:10px;" width="20%"><?php echo $val['salesman_name']?></td>
					<td style="padding:10px;" width="20%"><?php echo $val['salesman_code']?></td>
				<?php  } ?>
					<td style="padding:10px;" width="10%"><?php echo $val['customer_name']?></td>
					<td style="padding:10px;" width="15%"><?php echo $val['customer_code']?></td>
					<td style="padding:10px;" width="15%"><?php echo $val['customer_type']?></td>
					
					<td style="padding:10px;" width="10%"><?php echo $val['division_name']?></td>
					<td style="padding:10px;" width="10%"><?php echo $state[0]->state_name?></td>
					<td style="padding:10px;" width="10%"><?php echo $city[0]->city_name?></td> 
					
					<td style="padding:10px;" width="15%"><?php echo $val['activity_date'];?></td>
					<td style="padding:10px;" width="15%"><?php echo $res1['comment_desc'];?></td>
					<td style="padding:10px;" width="15%"><?php echo $res2['reason_desc'];?></td>
					<td style="padding:10px;" width="15%"><?php echo  $val['checkin_address'];?></td>
					<td style="padding:10px;" width="15%"><?php echo $val['checkout_address'];?></td>
					<?php
						if(is_array($val['activity_id'])){
							$photoList = array();
							$salesmanVisitedActivityPhotos=$_objAdmin->_getSelectList('table_image AS s','image_id, ref_id, image_url',''," ref_id IN (".implode(',', $val['activity_id']).")"); 
							if(is_array($salesmanVisitedActivityPhotos)) {
								foreach($salesmanVisitedActivityPhotos as $pkey=>$pValue) :
									$photoList[$pValue->ref_id][] = $pValue;
								endforeach;
							}
						}
					?>
					<td style="padding:10px;text-align:center;" width="15%">
						<?php echo $val['checkin']?>
						<?php if(is_array($val['activity_id'])){
							if($val['activity_id'][0] !="") {?>
								<?php
								$id=$val['activity_id'][0];
								 if(isset($val['lat'][0]) && $val['lat'][0] != ""){ ?>
								<a href="JavaScript:newPopup('activity_map.php?id=<?php echo base64_encode($val['activity_id'][0]);?>');">&nbsp;&nbsp;&nbsp;Map</a>
								<?php } else { echo "&nbsp;&nbsp;&nbsp;";}
								?>
								<a href="JavaScript:photoPopup('activity_photo.php?id=<?php echo base64_encode($photoList[$id][0]->image_id) ;?>');" style="display: none;">&nbsp;&nbsp;&nbsp;Image
								</a>
						<?php }else{echo "-";}} ?>
					</td>
					<td style="padding:10px;text-align:center;" width="15%">
						<?php echo $val['checkout'];?>
						<?php if(is_array($val['activity_id'])){
							if($val['activity_id'][1] !="") {?>
						<?php 
							$id=$val['activity_id'][1];
							if(isset($val['lat'][1]) && $val['lat'][1] != ""){ ?>
							<a href="JavaScript:newPopup('activity_map.php?id=<?php echo base64_encode($val['activity_id'][1]);?>');">&nbsp;&nbsp;&nbsp;Map</a>
							<?php } else { echo "&nbsp;&nbsp;&nbsp;";}?>
							<a href="JavaScript:photoPopup('activity_photo.php?id=<?php echo base64_encode($photoList[$id][0]->image_id) ;?>');" style="display: none;">&nbsp;&nbsp;&nbsp;Image
							</a>
						<?php }else{echo "-";}}?>
					</td>
					<!-- <td style="padding:10px;" width="15%"><?php echo round($val['distance'],2)." km" ;//echo $val['distance'];?></td> -->
					
					
					<!-- <td style="padding:10px;" width="20%">
						
						<a href="JavaScript:photoPopup('activity_photo.php?id=<?php echo base64_encode($photoList[$end_activity_id][0]->image_id) ;?>');">&nbsp;&nbsp;&nbsp;Image
						</a>
					</td> 
					<td style="padding:10px;" width="20%"><?php echo $val['comment']?></td>-->
				</tr>
			<?php }
			else if ($commentTypeCond == "all") {
				
				$sql2=mysql_query("select reason_desc from table_salesman_comment_reasonList where reason_id=".$res1['reason_id']."");
				$res2=mysql_fetch_array($sql2);
				// echo "<pre>";
				// print_r($sql);
				// echo $sql['comment_desc'];
				// die;
				?>
				<tr style="border:1px solid #CAC5C5;">
				<?php // if($_SESSION['SalAttList']==''){?>
					<td style="padding:10px;" width="20%"><?php echo $val['salesman_name']?></td>
					<td style="padding:10px;" width="20%"><?php echo $val['salesman_code']?></td>
				<?php // } ?>
					<td style="padding:10px;" width="10%"><?php echo $val['customer_name']?></td>
					<td style="padding:10px;" width="15%"><?php echo $val['customer_code']?></td>
					<td style="padding:10px;" width="15%"><?php echo $val['customer_type']?></td>
					
					<td style="padding:10px;" width="10%"><?php echo $val['division_name']?></td>
					<td style="padding:10px;" width="10%"><?php echo $state[0]->state_name?></td>
					<td style="padding:10px;" width="10%"><?php echo $city[0]->city_name?></td> 
					
					<td style="padding:10px;" width="15%"><?php echo $val['activity_date'];?></td>
					<td style="padding:10px;" width="15%"><?php echo $res1['comment_desc'];?></td>
					<td style="padding:10px;" width="15%"><?php echo $res2['reason_desc'];?></td>
					<td style="padding:10px;" width="15%"><?php echo  $val['checkin_address'];?></td>
					<td style="padding:10px;" width="15%"><?php echo $val['checkout_address'];?></td>
					<?php
						if(is_array($val['activity_id'])){
							$photoList = array();
							$salesmanVisitedActivityPhotos=$_objAdmin->_getSelectList('table_image AS s','image_id, ref_id, image_url',''," ref_id IN (".implode(',', $val['activity_id']).")"); 
							if(is_array($salesmanVisitedActivityPhotos)) {
								foreach($salesmanVisitedActivityPhotos as $pkey=>$pValue) :
									$photoList[$pValue->ref_id][] = $pValue;
								endforeach;
							}
						}
					?>
					<td style="padding:10px;text-align:center;" width="15%">
						<?php echo $val['checkin']?>
						<?php if(is_array($val['activity_id'])){
							if($val['activity_id'][0] !="") {?>
								<?php
								$id=$val['activity_id'][0];
								 if(isset($val['lat'][0]) && $val['lat'][0] != ""){ ?>
								<a href="JavaScript:newPopup('activity_map.php?id=<?php echo base64_encode($val['activity_id'][0]);?>');">&nbsp;&nbsp;&nbsp;Map</a>
								<?php } else { echo "&nbsp;&nbsp;&nbsp;";}
								?>
								<a href="JavaScript:photoPopup('activity_photo.php?id=<?php echo base64_encode($photoList[$id][0]->image_id) ;?>');" style="display: none;">&nbsp;&nbsp;&nbsp;Image
								</a>
						<?php }else{echo "-";}} ?>
					</td>
					<td style="padding:10px;text-align:center;" width="15%">
						<?php echo $val['checkout'];?>
						<?php if(is_array($val['activity_id'])){
							if($val['activity_id'][1] !="") {?>
						<?php 
							$id=$val['activity_id'][1];
							if(isset($val['lat'][1]) && $val['lat'][1] != ""){ ?>
							<a href="JavaScript:newPopup('activity_map.php?id=<?php echo base64_encode($val['activity_id'][1]);?>');">&nbsp;&nbsp;&nbsp;Map</a>
							<?php } else { echo "&nbsp;&nbsp;&nbsp;";}?>
							<a href="JavaScript:photoPopup('activity_photo.php?id=<?php echo base64_encode($photoList[$id][0]->image_id) ;?>');" style="display: none;">&nbsp;&nbsp;&nbsp;Image
							</a>
						<?php }else{echo "-";}}?>
					</td>

					<!-- <td style="padding:10px;" width="15%"><?php echo round($val['distance'],2)." km"; //echo $val['distance'];?></td> -->
					
					<!-- <td style="padding:10px;" width="20%">
						
						<a href="JavaScript:photoPopup('activity_photo.php?id=<?php echo base64_encode($photoList[$end_activity_id][0]->image_id) ;?>');">&nbsp;&nbsp;&nbsp;Image
						</a>
					</td> 
					<td style="padding:10px;" width="20%"><?php echo $val['comment']?></td>-->
				</tr>
			<?php 
				
			}
			


		}
			
			}?>

			<tr bgcolor="#6E6E6E" style="color: #fff;font-weight: bold;">
			
			</tr>
			<?php  } else{ ?>
			<tr  style="border-bottom:2px solid #6E6E6E;" align="center">
				<td style="padding:10px;" colspan="7">Report Not Available</td>
			</tr>
			<?php } ?>	
		</div>
	</table>
	<?php } else if(!isset($_SESSION['Type']) || $_SESSION['Type'] == '2') { ?>
	<table  border="0" width="100%" cellpadding="0" cellspacing="0" id="report_export" name="report_export">
			<div id="Report">
			<tr bgcolor="#6E6E6E" style="color: #fff;font-weight: bold;" >
				<?php if($_SESSION['SalAttList']==''){ ?>
				<td style="padding:10px;" width="20%">Salesman Name</td>
				<td style="padding:10px;" width="20%">Salesman Code</td>
				<?php } ?>
				<td style="padding:10px;" width="10%">Customer Name</td>
				<td style="padding:10px;" width="15%">Customer Code</td>
				<td style="padding:10px;" width="15%">Customer Type</td>
				<td style="padding:10px;" width="10%">Division</td>
				<td style="padding:10px;" width="10%">State</td>
				<td style="padding:10px;" width="10%">City</td> 
				<!-- <td style="padding:10px;" width="15%">Taluka</td> -->
				<td style="padding:10px;" width="15%">Date</td>
				<td style="padding:10px;" width="20%">Comment</td>
				<td style="padding:10px;" width="20%">Comment Reason</td>
			</tr>
	<?php
		$auAttSt=$_objAdmin->_getSelectList2('table_activity AS ta 
			LEFT JOIN table_customer_comments as tc ON ta.ref_id=tc.comment_id
			LEFT JOIN table_salesman AS s ON tc.salesman_id = s.salesman_id
			LEFT JOIN table_division AS tdiv ON s.division_id = tdiv.division_id
			LEFT JOIN table_retailer as tr ON tc.customer_id=tr.retailer_id AND tc.customer_type="R" 
			LEFT JOIN table_salesman_comment_reasonList as tscr ON tc.reason_id=tscr.reason_id 
			LEFT JOIN table_distributors as td ON tc.customer_id=td.distributor_id AND tc.customer_type="D"','tc.comment_desc,tscr.reason_id,tscr.reason_desc,s.salesman_name,s.salesman_code,tc.app_date,tdiv.division_name,ta.activity_type,ta.activity_id,
			CASE WHEN tc.customer_type="R" THEN tr.retailer_name WHEN tc.customer_type="D" THEN td.distributor_name END AS customer_name,
			CASE WHEN tc.customer_type="R" THEN tr.retailer_code WHEN tc.customer_type="D" THEN td.distributor_code END AS customer_code,
			CASE WHEN tc.customer_type="R" THEN "Dealer" WHEN tc.customer_type="D" THEN "Distributor" END AS customer_type,
			CASE WHEN tc.customer_type="R" THEN tr.state WHEN tc.customer_type="D" THEN td.state END AS state,
			CASE WHEN tc.customer_type="R" THEN tr.city WHEN tc.customer_type="D" THEN td.city END AS city,
			CASE WHEN tc.customer_type="R" THEN tr.taluka_id WHEN tc.customer_type="D" THEN td.taluka_id END AS taluka','',"ta.activity_type IN(30) ".$salesman." ".$divisionCond." ".$customerTypeCond." ".$commentTypeCond." AND (activity_date BETWEEN '".date('Y-m-d', strtotime($_SESSION['FromAttList']))."' AND '".date('Y-m-d', strtotime($_SESSION['ToAttList']))."')" );

			if(count($auAttSt)>0){

				foreach ($auAttSt as $key => $val) {
				$state=$_objAdmin->_getSelectList2('state','state_name','',"state_id='".$val->state."'");
				$city=$_objAdmin->_getSelectList2('city','city_name','',"city_id='".$val->city."'");
				$taluka=$_objAdmin->_getSelectList2('table_taluka','taluka_name','',"taluka_id='".$val->taluka."'");
				?>
				<tr style="border:1px solid #CAC5C5;">
				<?php if($_SESSION['SalAttList']==''){?>
					<td style="padding:10px;" width="20%"><?php echo $val->salesman_name;?></td>
					<td style="padding:10px;" width="20%"><?php echo $val->salesman_code;?></td>
				<?php } ?>
					<td style="padding:10px;" width="10%"><?php echo $val->customer_name;?></td>
					<td style="padding:10px;" width="15%"><?php echo $val->customer_code;?></td>
					<td style="padding:10px;" width="15%"><?php echo $val->customer_type;?></td>
					<td style="padding:10px;" width="10%"><?php echo $val->division_name;?></td>
					<td style="padding:10px;" width="10%"><?php echo $state[0]->state_name?></td>
					<td style="padding:10px;" width="10%"><?php echo $city[0]->city_name?></td> 
					<!-- <td style="padding:10px;" width="15%"><?php echo $taluka[0]->taluka_name?></td> -->
					<td style="padding:10px;" width="15%"><?php echo $val->app_date;?></td>
					<td style="padding:10px;" width="20%"><?php echo $val->comment_desc;?></td>
					<td style="padding:10px;" width="20%"><?php echo $val->reason_desc;?></td>
				</tr>
			<?php } ?>

			<tr bgcolor="#6E6E6E" style="color: #fff;font-weight: bold;">


			</tr>
			<?php  } else{ ?>
			<tr  style="border-bottom:2px solid #6E6E6E;" align="center">
				<td style="padding:10px;" colspan="7">Report Not Available</td>
			</tr>
			<?php } ?>	
		</div>
	</table>
	<?php } else { ?>
		<table  border="0" width="100%" cellpadding="0" cellspacing="0" id="report_export" name="report_export">
			<div id="Report">
				<tr bgcolor="#6E6E6E" style="color: #fff;font-weight: bold;" >
					<td style="padding:10px;" >Salesman&nbsp;Name</td>
					<td style="padding:10px;" >Salesman&nbsp;Code</td>
					<td style="padding:10px;" >Division</td>
					<td style="padding:10px;" >Country</td> 
					<td style="padding:10px;" >State</td>
					<td style="padding:10px;" >City</td> 
					<td style="padding:10px;" >Date</td>
					<td style="padding:10px;" >Time</td>
					<td style="padding:10px;" >Comment Type</td>
					<td style="padding:10px;" >Comment</td>
				</tr>
			<?php
			$auAttSt=$_objAdmin->_getSelectList2('table_activity AS ta 
				LEFT JOIN table_customer_comments as tc ON ta.ref_id=tc.comment_id
				LEFT JOIN dsr_comment as dsr ON ta.ref_type=dsr.dsr_id
				LEFT JOIN table_salesman AS s ON tc.salesman_id = s.salesman_id
				LEFT JOIN table_division AS tdiv ON s.division_id = tdiv.division_id','dsr.dsr_comment,tc.comment_desc,s.salesman_name,s.salesman_code,s.state,s.city,s.country,tc.app_date,tc.app_time,ta.activity_type,ta.activity_id,tdiv.division_name','',"ta.activity_type IN(32) ".$salesman." ".$divisionCond." AND (activity_date BETWEEN '".date('Y-m-d', strtotime($_SESSION['FromAttList']))."' AND '".date('Y-m-d', strtotime($_SESSION['ToAttList']))."')" );


				if(count($auAttSt)>0){

					foreach ($auAttSt as $key => $val) {
					$state=$_objAdmin->_getSelectList2('state','state_name','',"state_id='".$val->state."'");
					$city=$_objAdmin->_getSelectList2('city','city_name','',"city_id='".$val->city."'");
					?>
					<tr style="border:1px solid #CAC5C5;">
						<td style="padding:10px;"><?php echo $val->salesman_name;?></td>
						<td style="padding:10px;"><?php echo $val->salesman_code;?></td>
						<td style="padding:10px;"><?php echo $val->division_name;?></td>
						<td style="padding:10px;"><?php echo $val->country;?></td>
						<td style="padding:10px;"><?php echo $state[0]->state_name?></td>
						<td style="padding:10px;"><?php echo $city[0]->city_name?></td> 
						<td style="padding:10px;"><?php echo $val->app_date;?></td>
						<td style="padding:10px;"><?php echo $val->app_time;?></td>
						<td style="padding:10px;"><?php echo $val->dsr_comment;?></td>
						<td style="padding:10px;"><?php echo $val->comment_desc;?></td>
					</tr>
				<?php } ?>

				<tr bgcolor="#6E6E6E" style="color: #fff;font-weight: bold;">
					
					
				</tr>
				<?php }else{ ?>
				<tr  style="border-bottom:2px solid #6E6E6E;" align="center">
					<td style="padding:10px;" colspan="7">Report Not Available</td>
				</tr>
				<?php } ?>
			</div>
		</table>
	<?php } ?>
</td>
</tr>
<tr><td><img src="images/blank.gif" width="695" height="1" alt="blank" /></td><td></td></tr>
</table>
</div>
<div class="clear"></div>
</div>
<!--  end content-table-inner  -->
</td>
<td id="tbl-border-right"></td>
</tr>
<!--<tr>
	<th class="sized bottomleft"></th>
	<td id="tbl-border-bottom">&nbsp;</td>
	<th class="sized bottomright"></th>
</tr>-->
</table>
<div class="clear">&nbsp;</div>
</div>
<!--  end content -->
<div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->
<div class="clear">&nbsp;</div> 
<!-- start footer -->         
<?php include("footer.php") ?>
<?php //unset($_SESSION['SalAttList']);	?>
<!-- end footer -->
<script src="javascripts/jquery-1.8.2.js" type="text/javascript"></script>
<script src="javascripts/jquery-ui.js"></script>
<script type="text/javascript" src="javascripts/validate.js"></script>
<script type="text/javascript">
// Popup window code
function newPopup(url) {
	popupWindow = window.open(
		url,'popUpWindow','height=500,width=600,left=200,top=100,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no,status=yes')
}
</script>
<script>
    $(function() {
        $( "#from" ).datepicker({
			dateFormat: "d M yy",
            defaultDate: "w",
            changeMonth: true,
            numberOfMonths: 1,
            onSelect: function( selectedDate ) {
                $( "#to" ).datepicker( "option", "minDate", selectedDate );
            }
        });
        $( "#to" ).datepicker({
			dateFormat: "d M yy",
            defaultDate: "-w",
            changeMonth: true,
            numberOfMonths: 1,
            onSelect: function( selectedDate ) {
                $( "#from" ).datepicker( "option", "maxDate", selectedDate );
            }
        });
    });
</script>
<script type='text/javascript'>//<![CDATA[ 
var tableToExcel = (function () {
        var uri = 'data:application/vnd.ms-excel;base64,'
        , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table><tr><td><b>Salesman Name:</b> <?php echo $sal_name; ?></td><td><b>From Date:</b> <?php echo $_SESSION['FromAttList']; ?></td><td><b>To Date:</b> <?php echo $_SESSION['ToAttList']; ?></td></tr></table><table>{table}</table></body></html>'
        , base64 = function (s) { return window.btoa(unescape(encodeURIComponent(s))) }
        , format = function (s, c) { return s.replace(/{(\w+)}/g, function (m, p) { return c[p]; }) }
        return function (table, name, filename) {
            if (!table.nodeType) table = document.getElementById(table)
            var ctx = { worksheet: name || 'Worksheet', table: table.innerHTML }

            document.getElementById("dlink").href = uri + base64(format(template, ctx));
            document.getElementById("dlink").download = filename;
            document.getElementById("dlink").click();

        }
    })()

//]]>  
</script>
</body>
</html>
