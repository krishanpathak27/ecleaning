<?php include("includes/config.inc.php");
      include("includes/function.php");
	  include("includes/globalarraylist.php");

	if(isset($_POST['showOrderlist']) && $_POST['showOrderlist'] == 'yes') {	

		if($_POST['sal']!="")  {
			$_SESSION['monthSalesmanID']=$_POST['sal'];	
			} else {
			unset($_SESSION['monthSalesmanID']);
		}

		if($_POST['month']!="" && is_numeric($_POST['month'])) {
			$_SESSION['dismonth']=$_POST['month'];	
		}
		
		if($_POST['year']!="" && is_numeric($_POST['year'])) {
			$_SESSION['disCyear']=$_POST['year'];	
		}

		if($_POST['sal']=='All') {
		  unset($_SESSION['monthSalesmanID']);	
		}

		header("Location: salesman_visits.php");
	}
		
		
		if($_SESSION['disCyear']==''){
			$_SESSION['disCyear']=date('Y');
		}

		if($_SESSION['dismonth']==''){
			$_SESSION['dismonth']=date('m');
		}

	if(isset($_REQUEST['reset']) && $_REQUEST['reset'] == 'yes'){
		 unset($_SESSION['monthSalesmanID']);	 	
		 $_SESSION['dismonth']= '';
		 $_SESSION['disCyear']= '';
		 header("Location: salesman_visits.php");
	}


	// Get All the categories 

	$categotyList = $_objAdmin->_getSelectList('table_category','category_id, category_name','',""); 



?>

<?php include("header.inc.php") ?>
<script type="text/javascript">

	var tableToExcel = (function () {
        var uri = 'data:application/vnd.ms-excel;base64,'
        , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table><tr><td></td></tr></table><table>{table}</table></body></html>'
        , base64 = function (s) { return window.btoa(unescape(encodeURIComponent(s))) }
        , format = function (s, c) { return s.replace(/{(\w+)}/g, function (m, p) { return c[p]; }) }
        return function (table, name, filename) {
        	//alert(table.name);
		  
            if (!table.nodeType) table = document.getElementById(table)
            var ctx = { worksheet: name || 'Worksheet', table: table.innerHTML }

            document.getElementById("dlink").href = uri + base64(format(template, ctx));
            document.getElementById("dlink").download = filename;
            document.getElementById("dlink").click();

        }
    })()	

</script>
<?php //echo "<pre>"; print_r($_SESSION);?>
<!-- start content-outer -->
<input name="pagename" type="hidden"  id="pagename" value="salesman_visits.php" />
<div id="content-outer">
<!-- start content -->
<div id="content">
<div id="page-heading"><h1>
	<span style="color: #d74343; font-family: Tahoma; font-weight: bold;">Master Channel Partner Visit</span></h1></div>
	<table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">
	<tr>
		<!--<td id="tbl-border-left"></td>-->
		<td>
		<!--  start content-table-inner -->
	<div id="content-table-inner">
	
	<div id="page-heading" align="left" >
	<?php //echo "<pre>";
		 // print_r($_SESSION);
	?>
	<form name="frmPre" id="frmPre" method="post" action="" enctype="multipart/form-data" >
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
	
	<tr>
	<td id="distributorMenu">
		<h3>Salesmen: </h3>
		<h6><select name="sal" id="sal" class="menulist">
			<?php echo $salesmanOptionList = $_objArrayList->GetSalesmenMenu($salsList, $_SESSION['monthSalesmanID'],'root');?>
		</select></h6>
	</td>

	<td><?php echo $months = $_objArrayList->getMonthList($_SESSION['dismonth']);?></td>
	<td><?php echo $year = $_objArrayList->getYearList($_SESSION['disCyear']);?></td>

	<td><h3></h3>
	<input name="submit" class="result-submit" type="submit" id="submit" value="View Details" />
	<input type="button" value="Reset!" class="form-reset" onclick="location.href='salesman_visits.php?reset=yes';" />
	</td></tr>

	<tr>
	<td colspan="5"><!-- <a id="dlink"  style="display:none;"></a><input name="export" class="result-submit" type="button" value="Export to Excel" onclick="tableToExcel('report_export', 'Master Channel Partner Visit', 'Master Channel Partner Visit.xls');"> -->

	<a id="dlink"  style="display:none;"></a>
	<input type="button" name="submit" value="Export to Excel" class="result-submit" onclick="tableToExcel('report_export', 'Master Channel Partner Visit', 'Master Channel Partner Visit.xls');" >



	</td>


	<td></td>
	</tr>

	<tr>
	<td colspan="7"><input name="showOrderlist" type="hidden" value="yes" /></td>
	</tr>
	</table>


	</form>

	</div>
	
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
	<tr valign="top">
	<td>
		<!-- start id-form -->
	<div id="Report" style="width:980px; overflow:scroll;">
		<table border="1" width="100%" cellpadding="0" cellspacing="0"  id="report_export" name="report_export">
			<tr  bgcolor="#6E6E6E" style="color: #fff;font-weight: bold;" >
				<td style="white-space: nowrap; margin: 10px; padding: 10px; font-family: monospace;">Salesman Name</td>
				<td style="white-space: nowrap; margin: 10px; padding: 10px; font-family: monospace;">Salesman Code</td>
				<td style="white-space: nowrap; margin: 10px; padding: 10px; font-family: monospace;">Date</td>
				<td style="margin: 10px; padding: 10px; font-family: monospace;">Route</td>
				<td style="white-space: nowrap; margin: 10px; padding: 10px; font-family: monospace;">Customer Name </td>
				<td style="white-space: nowrap; margin: 10px; padding: 10px; font-family: monospace;">Customer Type </td>
				<td style="white-space: nowrap; margin: 10px; padding: 10px; font-family: monospace;">Order Taken Yes / NO </td>
				<td style="padding:10px;" align="center"  width="100%">
					<table>
						<tr>
							<?php foreach ($categotyList as $key => $value) {?>
								 <td width="150px;"  style="white-space: nowrap; margin: 10px; padding: 10px; font-family: monospace;"><?php echo $value->category_name;?></td>
							<?php }?>
						</tr>
					</table>
				</td>
				<td style="padding:10px;" align="center" width="15%">TLSD</td>
			</tr>
			<?php

			$routeRetailerDistributorOfSalesman =$_objAdmin->_getSelectList('`table_route_scheduled` AS RS
			LEFT JOIN table_route_schedule_details AS RSD ON RSD.`route_schedule_id` = RS.`route_schedule_id`
			LEFT JOIN table_route_retailer AS RR ON RR.route_id = RSD.route_id
			LEFT JOIN table_retailer AS RET ON RET.retailer_id = RR.retailer_id AND RR.status = "R"
			LEFT JOIN table_distributors AS D ON D.distributor_id = RR.retailer_id AND RR.status = "D"
			LEFT JOIN table_route AS R ON R.route_id = RSD.route_id
			LEFT JOIN table_salesman AS s ON s.salesman_id = RS.salesman_id',
			"RSD.route_id, R.route_name, RS.salesman_id, s.salesman_name, s.salesman_code,CONCAT(RS.year,'-',RS.month,'-',RSD.assign_day) AS route_date, RS.month, RS.year, RSD.assign_day,RET.retailer_name, D.distributor_name, RR.retailer_id, RR.status",'',
			" RS.salesman_id = '".$_SESSION['monthSalesmanID']."' AND RS.month = '".$_SESSION['dismonth']."' AND RS.year = '".$_SESSION['disCyear']."' GROUP BY RSD.assign_day, RS.salesman_id, RR.retailer_id");

			// echo "<pre>";
			// echo print_r($routeRetailerDistributorOfSalesman);
			// echo "</pre>";


			$routeRetailerDistributorOfSalesmanOrderCategoryWise = $_objAdmin->_getSelectList('table_order AS O LEFT JOIN table_order_detail AS OD ON OD.order_id = O.order_id LEFT JOIN table_item AS I ON I.item_id = OD.item_id',"O.salesman_id, O.date_of_order, I.category_id, SUM(OD.acc_quantity) AS ttlCategoryQty, O.retailer_id, O.distributor_id, O.order_type, CASE WHEN (O.distributor_id > 0 AND O.retailer_id > 0) THEN 'R' WHEN (O.distributor_id = 0 AND O.retailer_id > 0)  THEN 'R' ELSE 'D' END AS type_status ",''," O.salesman_id = '".$_SESSION['monthSalesmanID']."' AND DATE_FORMAT(O.date_of_order,'%Y') = '".$_SESSION['disCyear']."' AND DATE_FORMAT(O.date_of_order,'%m') = '".$_SESSION['dismonth']."' AND order_type IN ('Yes', 'No') GROUP BY O.date_of_order, O.salesman_id, O.retailer_id, O.distributor_id, I.category_id");


			//print_r($routeRetailerDistributorOfSalesman);
			// echo "<pre>";
			// print_r($routeRetailerDistributorOfSalesmanOrderCategoryWise);
			// echo "</pre>";


			foreach ($routeRetailerDistributorOfSalesmanOrderCategoryWise as $key => $orderValue) {
				# code...

				if($orderValue->type_status == 'R')  {
					$dataSetOfCustomerOrders[$orderValue->salesman_id][$orderValue->date_of_order][$orderValue->type_status][$orderValue->retailer_id][$orderValue->order_type][] = $orderValue;
				} else if($orderValue->type_status == 'D')  {

					$dataSetOfCustomerOrders[$orderValue->salesman_id][$orderValue->date_of_order][$orderValue->type_status][$orderValue->distributor_id][$orderValue->order_type][] = $orderValue;
				}
			}


			// echo "<pre>";
			// print_r($dataSetOfCustomerOrders);
			// echo "</pre>";

			
			if(is_array($routeRetailerDistributorOfSalesman)){
			
			foreach ($routeRetailerDistributorOfSalesman as $key => $Mvalue) {
				# code...
				
				$rtdate = date('Y-m-d',strtotime($Mvalue->route_date));
				if(isset($dataSetOfCustomerOrders[$Mvalue->salesman_id][$rtdate][$Mvalue->status][$Mvalue->retailer_id])) {
					$data = $dataSetOfCustomerOrders[$Mvalue->salesman_id][$rtdate][$Mvalue->status][$Mvalue->retailer_id];
				} else {
					$data = array();
				}

				$tdStyleClass = "";
				$CustomerType = "";

				if($Mvalue->status == 'R')  {
					$tdStyleClass = "#ededed";
					$CustomerType = "Retailer";
				} else if($Mvalue->status == 'D')  {
					$tdStyleClass = "#FFF";
					$CustomerType = "Distributor";
				}



				// echo "<pre>";
				// print_r($data);
				// echo "</pre>";
			?>

				<tr  style="border-bottom:2px solid #6E6E6E; background:<?php echo $tdStyleClass;?>">
					<td style="padding:10px;" width="10%"><?php echo $Mvalue->salesman_name;?></td>
					<td style="padding:10px;" width="10%"><?php echo $Mvalue->salesman_code;?></td>
					<td style="padding:10px;" width="10%"><?php echo $_objAdmin->_changeDate($Mvalue->route_date);?></td>
					<td style="padding:10px;" width="30%"><?php echo $Mvalue->route_name;?></td>
					<td style="padding:10px;" width="20%"><?php if(!empty($Mvalue->retailer_name)) { echo $Mvalue->retailer_name;  }  else { echo $Mvalue->distributor_name; } ?></td>
					<td style="padding:10px;" width="20%"><?php echo $CustomerType;?></td>

					<!-- Check order type YES/NO-->
					<td>
						<table>
						<?php 
						$sizeOfTLDS = "-";
						$catWiseOrderValue = array();


						foreach ($data as $key => $Invalue) {


							foreach ($Invalue as $Catkey => $catDataSet) {
								if($catDataSet->order_type == 'Yes' && $catDataSet->category_id > 0) {
									$catWiseOrderValue[$catDataSet->category_id] = $catDataSet;
								}
							}


							$sizeOfTLDS = sizeOf($catWiseOrderValue);

						 ?>
							<tr><td style="padding:10px;" align="center" width="10%"><?php echo $key;?></td></tr>
						<?php }?>
						</table>
					</td>
					<!-- Check order type YES/NO-->


					<!-- Category wise calculation -->
					<td>
						<table>
							<tr>
								<?php foreach ($categotyList as $key => $catValue) {?>
									 <td width="150px;" align="center" style="margin: 10px; padding: 10px; font-family: monospace;">
										<?php if(isset($catWiseOrderValue[$catValue->category_id])){
											echo $catWiseOrderValue[$catValue->category_id]->ttlCategoryQty;
										 } else {
											echo "-";
										}?>
									</td>
								<?php }?>
							</tr>
						</table>
					</td>
					<!-- Category wise calculation -->

					<td style="padding:10px;" align="center" width="15%"><?php echo $sizeOfTLDS;?></td>
				</tr>
			<?php } } else { ?>
			<tr  style="border-bottom:2px solid #6E6E6E;">
				<td style="padding:10px;" colspan="20">Report Not Available</td>
			</tr>
			<?php } ?>
		</table>
		</div>
	<!-- end id-form  -->
	</td>
</tr>
</table>
<div class="clear"></div>
</div>
<!--  end content-table-inner  -->
</td>
<td id="tbl-border-right"></td>
</tr>
</table>
<div class="clear">&nbsp;</div>
</div>
<!--  end content -->
<div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->
<div class="clear">&nbsp;</div> 
<!-- start footer -->         
<?php include("footer.php") ?>
<!-- end footer -->

</body>
</html>