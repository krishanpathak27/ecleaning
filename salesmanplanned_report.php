<?php
/**********************************************************************************
** Salesman Planned Report with Retailer and Distributor
** Author: Nizam
** Date: 20th October 2015
***********************************************************************************/
	include("includes/config.inc.php");
	include("includes/function.php");
	include("includes/globalarraylist.php");

	/******* Check salesman ID exists into session or not. ******************/

	if(isset($_SESSION['showPlannedVsVisited']) && $_SESSION['salesman']!='')
	{
	
			$salArrList=$_SESSION['salesman'];	
			$filterby=$_SESSION['orderfilterBY'];
			$salesman = $_objArrayList->getSalesmanConsolidatedList($salArrList,$filterby);
			
	} 

	$page_name="Salesman Planned Report";

	if(isset($_POST['showPlannedVsVisited']) && $_POST['showPlannedVsVisited'] == 'yes'){	
		if($_POST['sal']!="All"){
			 $_SESSION['salesman']=$_POST['sal'];	
		} 
	
		if($_POST['year']!=""){
		 	$_SESSION['PVsVYear']=$_POST['year'];	
		}
		if($_POST['month']!=""){
	        $_SESSION['PVsVMonth']=$_POST['month'];	
		}
		if($_POST['sal']=="All"){
	  	 	unset($_SESSION['salesman']);
	   }
	   if($_POST['state_id']!="" && $_POST['state_id']!='All')
	   	{
		$_SESSION['StateList']=$_POST['state_id'];
		 //$state= " and s.state=".$_SESSION['StateList'];	
		}
	   if($_POST['state_id']=='All')
	   {
	   	unset($_SESSION['StateList']);
	   }	
	    
	}
	//echo $_SESSION['StateList'];

	if(isset($_REQUEST['reset']) && $_REQUEST['reset'] == 'yes'){
		unset($_SESSION['salesman']);
		unset($_SESSION['StateList']);	
	 	
		$_SESSION['PVsVYear']  = date('Y');
		$_SESSION['PVsVMonth'] = date('m');
		header("Location:salesmanplanned_report.php");
	}


	if($_SESSION['PVsVYear']==''){
		$_SESSION['PVsVYear'] = date('Y');
	}

	if($_SESSION['PVsVMonth']==''){
		$_SESSION['PVsVMonth']= date('m');
	}
	
	if(isset($_REQUEST['show']) && $_REQUEST['show']=="yes"){
		$_objAdmin->showSalesmanPlannedRoute($salesman);
		die;
	} 

?>
<?php include("header.inc.php") ?>
<script src="javascripts/dateNextPrev.js" type="text/javascript"></script>
<!-- start content-outer -->
<input name="pagename" type="hidden"  id="pagename" value="plannedvsvisited_report.php" />
<div id="content-outer">
<!-- start content -->
<div id="content">
<div id="page-heading"><h1><span style="color: #d74343; font-family: Tahoma; font-weight: bold;">Salesman Planned Report</span></h1></div>
<table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">

<tr>
	<td>
	<!--  start content-table-inner -->
	<div id="content-table-inner">
	
	<div id="page-heading" align="left" >
	
	<form name="frmPre" id="frmPre" method="post" action="" enctype="multipart/form-data" >
	<table border="0" width="85%" cellpadding="0" cellspacing="0">
	<tr>	
	  	<td><h3>&nbsp;&nbsp;Salesman:</h3><h6>
	  		<select name="sal" id="sal" class="styledselect_form_5" style="" >
				<?php echo $rtndata = $_objArrayList->GetSalesmenMenu($salsList, $_SESSION['salesman'], 'flex');?>
			</select></h6>
		</td>
		 <!-- <td><h3>State: </h3> <h6> 
		  <select name="state_id" id="state_id" class="menulist">
		  <option value="All">All</option>
			<?php $stateList=$_objAdmin->_getSelectList2('state','state_id,state_name',''," state_name!='' ORDER BY state_name"); 
			if(is_array($stateList)){
			for($i=0;$i<count($stateList);$i++){
			?>
			<option value="<?php echo $stateList[$i]->state_id;?>" <?php if ($stateList[$i]->state_id==$_SESSION['StateList']){ ?> selected <?php } ?>><?php echo $stateList[$i]->state_name;?></option>
			<?php }}?>
			</select></h6></td>	 -->
	
		<td>
			<h3>Year: </h3><h6>			
				<select name="year" class="styledselect_form_3">					
					<?php echo $_objArrayList->getYearList2($_SESSION['PVsVYear']);?>
				</select>
			</h6>
		</td>
        <td>
		<h3>Month: </h3><h6>
			<select name="month" class="menulist styledselect_form_3">
				<?php echo $_objArrayList->getMonthList3($_SESSION['PVsVMonth']);?>
			</select>
		</h6>
        </td>
		<td><h3>&nbsp; </h3>
	  		<input name="showPlannedVsVisited" type="hidden" value="yes" />
	  		<input name="submit" class="result-submit" type="submit" id="submit" value="View Details" />
	  		
	  	</td>
	  <td><h3>&nbsp; </h3><input type="button" value="Reset!" class="form-reset" onclick="location.href='salesmanplanned_report.php?reset=yes';" />	</td>	
	  
	</tr>
	</table>
	</form>
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
		<tr valign="top">
			<td>
				<!-- start id-form -->
				<table  border="0" cellspacing="0" cellpadding="0"  align="center" width="100%">
					<tr>
						<td align="lift" valign="top"  >
							<table id="flex1" style="display:none"></table>
								<script type="text/javascript">showSalesmanPlannedRoute();</script>
						</td>
					</tr>
				</table>
			<!-- end id-form  -->
			</td>
		</tr>
		<tr>
			<td><img src="images/blank.gif" width="695" height="1" alt="blank" /></td>
			<td></td>
		</tr>
	</table>
		<div class="clear"></div>
	</div>
<!--  end content-table-inner  -->
</td>
<td id="tbl-border-right"></td>
</tr>
</table>
<div class="clear">&nbsp;</div>
</div>
<!--  end content -->
<div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->
<div class="clear">&nbsp;</div> 
<!-- start footer -->         
<?php include("footer.php") ?>
<!-- end footer -->
<script src="javascripts/jquery-1.8.2.js" type="text/javascript"></script>
<script src="javascripts/jquery-ui.js"></script>
<script type="text/javascript" src="javascripts/validate.js"></script>
</body>
</html>
