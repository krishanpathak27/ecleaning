<?php
include("includes/config.inc.php");
include("includes/function.php");
$page_name="Battery to be Scraped";
$_objAdmin = new Admin();
$objArrayList= new ArrayList();


if($_REQUEST['id'] && $_REQUEST['id']!='')
{
	$auRec = $_objAdmin->_getSelectList2("table_item_bsn as BSN left join table_item as ITEM on BSN.item_id = ITEM.item_id left join table_brands as BRAND on BRAND.brand_id = ITEM.brand_id left join table_category as CATEGORY on CATEGORY.category_id = ITEM.category_id left join table_color as clr on clr.color_id = ITEM.item_color ","BSN.*, ITEM.item_name, BRAND.brand_name, CATEGORY.category_name,clr.color_desc",""," bsn_id='".$_REQUEST['id']."'");
	if($auRec[0]->warranty_end_date== '0000-00-00')
    {
        $objStock = new StockClass;
        $defectiveBatterySerialNo = trim($TransRet['barcode']);
        $warrantyEndDate = $objStock->getBSNWarrantyEndDate($auRec[0]->date_of_sale, $auRec[0]->warranty_period, $auRec[0]->warranty_prorata);

    }
    else
    {
        $warrantyEndDate = $auRec[0]->warranty_end_date;
    }
    $auRec[0]->warranty_end_date = $warrantyEndDate;
	// print_r($auRec);
}
else
{
	header('Location: battery_for_scrap.php');
}

if($_POST['battery_scrapped'])			// Function Called
{
	if(($_POST['scraped_date'] !='') && ($_POST['insurance_id'])!='')
	{
		$cid=$_objAdmin->updateBatteryScraped($_POST['id']);
		$sus="Battery Successfully Scrapped."; 	
	}
	else
	{
		
		echo "<script>
			alert('Please check Scrapped Date and Insurance ID');
			window.location.href='".$_SERVER['HTTP_REFERER']."';
			</script>";
	}
	
}


?>
<script type="text/javascript" src="javascripts/validate.js"></script>
<?php include("header.inc.php");
//$pageAccess=1;
//$check=$objArrayList->checkAccess($pageAccess, basename($_SERVER['PHP_SELF']));
//if($check == false){
//header('Location: ' . $_SERVER['HTTP_REFERER']);
//}

 ?>
 <link href="bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Bootstrap theme -->
    <link href="bootstrap/dist/css/bootstrap-theme.min.css" rel="stylesheet">

    <!-- <link rel="stylesheet" href="css/jquery-ui-1.12.1.css"> -->
    <!-- Custom styles for this template -->
    <!--<link href="bootstrap/css/theme.css" rel="stylesheet">-->

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <!--<script src="bootstrap/assets/js/ie-emulation-modes-warning.js"></script>-->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
<!-- start content-outer -->



    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
   <!--  <script src="javascripts/jquery-1.11.2.min.js"></script> -->
   <!--<script src="bootstrap/dist/js/bootstrap.min.js"></script>-->
     <!--<script src="bootstrap/assets/js/docs.min.js"></script>-->
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
   <!--<script src="bootstrap/assets/js/ie10-viewport-bug-workaround.js"></script>-->
   	
    
<div class="clear"></div>
<!-- start content-outer -->
<div id="content-outer">
<!-- start content -->
	<div id="content">
	<div id="page-heading"><h3><span style="color: #d74343; font-family: Tahoma; font-weight: bold;">Battery to be Scrapped</span></h3></div>
		<table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">
		<tr>
			<!--<td id="tbl-border-left"></td>-->
			<td>
			<!--  start content-table-inner -->
			<div id="content-table-inner">
				<table border="0" width="100%" cellpadding="0" cellspacing="0">
				<tr valign="top">
					<td>
					<!--  start message-red -->
					<?php if($err!=''){?>
					<div id="message-red">
						<table border="0" width="100%" cellpadding="0" cellspacing="0">
						<tr>
							<td class="red-left"> <?php echo $err; ?></td>
							<td class="red-right"><a class="close-red"><img src="images/icon_close_red.gif"   alt="" /></a></td>
						</tr>
						</table>
					</div>
					<?php } ?>
					<!--  end message-red -->
					<?php if($sus!=''){?>
					<!--  start message-green -->
					<div id="message-green">
						<table border="0" width="100%" cellpadding="0" cellspacing="0">
						<tr>
							<td class="green-left"><?php echo $sus; ?></td>
							<td class="green-right"><a class="close-green"><img src="images/icon_close_green.gif"   alt="" /></a></td>
						</tr>
						</table>
					</div>
					<?php } ?>
					<!--  end message-green -->
					
			<!-- start id-form -->
		<form name="frmPre" method="post" action="scrapped_by_warehouse.php" enctype="multipart/form-data" >
			<div class="table-responsive">
			<div class="form-group">
	            <div class="row">
	            	<div class="col-sm-4" >
		                <div class="input-group">
		                	<span class="input-group-addon" id="basic-addon1">BSN Number</span>
		                   <input type="text" class="form-control  col-sm-8"  id="bsn" name="bsn" placeholder="BSN Number" style="color:black;" value="<?php echo $auRec[0]->bsn; ?>" readonly>
		                </div>
		              </div>            
		            </div>
	            </div>
            <div class="form-group">
             <div class="row">
              <div class="col-sm-4" >
                <div class="input-group">
                   <span class="input-group-addon" id="basic-addon1">Date of Sale</span>
                    <input type="text" class="form-control "  id="date_of_sale" name="date_of_sale" readonly placeholder="Date of Sale" value="<?php echo $auRec[0]->date_of_sale; ?>" readonly>
                </div>
              </div>

              <div class="col-sm-4" >
                <div class="input-group">
                   <span class="input-group-addon" id="basic-addon1">Item Name</span>
                    <input type="text" class="form-control "  id="item_name" name="item_name" readonly placeholder="Item Name" value="<?php echo $auRec[0]->item_name; ?>" readonly>
                </div>
              </div>

              <div class="col-sm-4" >
                <div class="input-group">
                   <span class="input-group-addon" id="basic-addon1">Manufacture Date</span>
                    <input type="text" class="form-control "  id="manufacture_date" name="manufacture_date" readonly placeholder="Manufacture Date" value="<?php echo $auRec[0]->manufacture_date; ?>" readonly>
                </div>
              </div>
             </div>
            </div>
            <div class="form-group">
	            <div class="row">
	            	
		              <div class="col-sm-4" >
		                <div class="input-group">
		                   <span class="input-group-addon" id="basic-addon1">Warranty Upto</span>
		                    <input type="text" class="form-control "  id="warranty_end_date" name="warranty_end_date" readonly placeholder="Warranty Upto" value="<?php echo $auRec[0]->warranty_end_date; ?>" readonly>
		                </div>
		              </div>
	        		<div class="col-sm-4" >
		                <div class="input-group">
		                   <span class="input-group-addon" id="basic-addon1">Prorata (in months)</span>
		                    <input type="text" class="form-control "  id="warranty_prorata" name="warranty_prorata" readonly placeholder="Prorata" value="<?php echo $auRec[0]->warranty_prorata; ?>" readonly>
		                </div>
		              </div>
		              <div class="col-sm-4" >
		                <div class="input-group">
		                   <span class="input-group-addon" id="basic-addon1">Grace Period (in months)</span>
		                    <input type="text" class="form-control "  id="warranty_grace_period" name="warranty_grace_period" readonly placeholder="Warranty Grace Period(in months)" value="<?php echo $auRec[0]->warranty_grace_period; ?>" readonly>
		                </div>
		              </div>
		              
		              
		            </div>
	            </div>

	            <div class="form-group">
	            <div class="row">
	            		<div class="col-sm-4" >
		                <div class="input-group">
		                   <span class="input-group-addon" id="basic-addon1">Brand Name</span>
		                    <input type="text" class="form-control "  id="brand_name" name="brand_name" readonly placeholder="Brand Name" value="<?php echo $auRec[0]->brand_name; ?>" readonly>
		                </div>
		              </div>
		              <div class="col-sm-4" >
		                <div class="input-group">
		                   <span class="input-group-addon" id="basic-addon1">Category Name</span>
		                    <input type="text" class="form-control "  id="category_name" name="category_name" readonly placeholder="Category Name" value="<?php echo $auRec[0]->category_name; ?>" readonly>
		                </div>
		              </div>
	        		<div class="col-sm-4" >
		                <div class="input-group">
		                   <span class="input-group-addon" id="basic-addon1">Color</span>
		                    <input type="text" class="form-control"  id="color_desc" name="color_desc" readonly placeholder="Color" value="<?php echo $auRec[0]->color_desc; ?>" readonly>
		                </div>
		              </div>
		              
		              
		            </div>
	            </div>
	            <div class="form-group">
	            <div class="row">		              
	        		<div class="col-sm-4" >
		                <div class="input-group">
		                   <span class="input-group-addon" id="basic-addon1">Scraped Date</span>
		                    <input type="text" class="form-control required datepicker"  id="scraped_date" name="scraped_date" readonly placeholder="Scraped Date" required="required" value="<?php echo $_objAdmin->_changeDate($auRec[0]->last_updated_date); ?>">
		                </div>
		              </div>
		              <div class="col-sm-8" >
		                <div class="input-group">
		                   <span class="input-group-addon" id="basic-addon1">Insurance ID</span>
		                    <input type="text" class="form-control required"  id="insurance_id" name="insurance_id"  placeholder="Insurance ID" value="<?php echo $auRec[0]->insurance_id; ?>">
		                </div>
		              </div>
		              
		            </div>
	            </div>
	            <div class="form-group">
	            <div class="row">		              
	        		<div class="col-sm-6" >
		                <div class="input-group">
		                   <input type="button" value="Back" class="form-reset btn btn-default" onclick="location.href='battery_for_scrap.php'" style="padding-bottom: 25px; padding-left: 25px; padding-right: 25px;background: #d9534f;background-image: linear-gradient(to bottom, #d9534f 0px, #c12e2a 100%);color:#fff;"/>
		                   <input name="submit" class="form-submit btn btn-default" type="submit" id="submit" value="Battery Scraped" style="padding-bottom: 25px; padding-left: 25px; padding-right: 25px;background-image: linear-gradient(to bottom, #5cb85c 0px, #419641 100%);background-color: #5cb85c;color:#fff;"/>
		                </div>
		              </div>
		              <div class="col-sm-4" >
		                <div class="input-group">
		                   	
							<input name="id" type="hidden" value="<?php echo $_REQUEST['id'] ?>" />
							<input name="battery_scrapped" id="battery_scrapped" type="hidden" value="yes" />
							<input name="warehouse_id" id="warehouse_id" type="hidden" value="<?php echo $_SESSION['warehouseId']?>" />
		                </div>
		              </div>
		              
		            </div>
	            </div>
             </div>
            </div>
		</form>
			<!-- end id-form  -->
			</td>
			<td>
			<!-- right bar-->
			<?php include("rightbar/segment_bar.php") ?>
			<div class="clear"></div>
		</div>
<!-- end related-act-bottom -->
</div>
<!-- end related-activities -->
</td>
</tr>
<tr>
<td><img src="images/blank.gif" width="695" height="1" alt="blank" /></td>
<td></td>
</tr>
</table>
<div class="clear"></div>
</div>
<!--  end content-table-inner  -->
</td>
<td id="tbl-border-right"></td>
</tr>
</table>
<div class="clear">&nbsp;</div>

</div>
<!--  end content -->
<div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->
<div class="clear">&nbsp;</div>
    
<!-- start footer --> 
<?php include("footer.php") ?>  
<!-- end footer -->
</body>
</html>
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

<script type="text/javascript">
  

  $(function() {
    $( ".datepicker" ).datepicker({
     
      dateFormat: "d M yy",
      defaultDate: "w",
      changeMonth: true,
      numberOfMonths: 1
    });
  });


  
</script>
