<?php
include("includes/config.inc.php");
$_objAdmin = new Admin();
include("includes/function.php");
include("header.inc.php");
include("gcm.php");
//include("includes/function.php");
//include("includes/globalarraylist.php");
$pageAccess = 1;

$page_name = "Message Broadcast";
if (isset($_POST['add']) && $_POST['add'] == 'yes'){
   if(!empty($_POST['cleaner_id'])){
        if($_POST['cleaner_id'] == 'allcleaner'){
        $result= $_objAdmin->_getSelectList2('table_cleaner','*','','');
            foreach ($result as $res) {
            //echo "<pre>";
            //print_r($res);
            // echo "id : ". $res->cleaner_id;
            //echo "</pre>";
            
                if($res){                    
                $data['account_id'] = mysql_escape_string($_SESSION['accountId']);
                $data['message'] = preg_replace("/\r|\n/","",mysql_escape_string($_POST['message']));
                $data['subject'] = $_POST['subject'];
                //$data['customer_id'] = $_POST['supervisor_id'];
                //$data['cleaner_id'] = $_POST['cleaner_id'];
                $data['cleaner_id'] = $res->cleaner_id;
                $data['send_date'] = date('Y-m-d');
                $data['send_time'] = date('Y-m-d h:i:s');
                $data['status'] = 'A';
                $dis_id = $_objAdmin->_dbInsert($data,'table_messages');              
                }                 
            }            
        }else{
            $data['account_id'] = mysql_escape_string($_SESSION['accountId']);
            $data['message'] = preg_replace("/\r|\n/","",mysql_escape_string($_POST['message']));
            $data['subject'] = $_POST['subject'];
            $data['customer_id'] = $_POST['supervisor_id'];
            $data['cleaner_id'] = $_POST['cleaner_id'];
            $data['send_date'] = date('Y-m-d');
            $data['send_time'] = date('Y-m-d h:i:s');
            $data['status'] = 'A';
            $dis_id = $_objAdmin->_dbInsert($data, 'table_messages');
        }
   }
   if(!empty($_POST['supervisor_id'])){
        if($_POST['supervisor_id'] == 'allcustomer'){
            $result= $_objAdmin->_getSelectList2('table_customer','*','','');
            foreach ($result as $res) {
                     
                if($res){                    
                $data['account_id'] = mysql_escape_string($_SESSION['accountId']);
                $data['message'] = preg_replace("/\r|\n/","",mysql_escape_string($_POST['message']));
                $data['subject'] = $_POST['subject'];
                //$data['customer_id'] = $_POST['supervisor_id'];
                //$data['cleaner_id'] = $_POST['cleaner_id'];
                $data['customer_id'] = $res->customer_id;
                $data['send_date'] = date('Y-m-d');
                $data['send_time'] = date('Y-m-d h:i:s');
                $data['status'] = 'A';
                $dis_id = $_objAdmin->_dbInsert($data,'table_messages');              
                }                 
            }
            
        }else{
            $data['account_id'] = mysql_escape_string($_SESSION['accountId']);
            $data['message'] = preg_replace("/\r|\n/","",mysql_escape_string($_POST['message']));
            $data['subject'] = $_POST['subject'];
            $data['customer_id'] = $_POST['supervisor_id'];
            $data['cleaner_id'] = $_POST['cleaner_id'];
            $data['send_date'] = date('Y-m-d');
            $data['send_time'] = date('Y-m-d h:i:s');
            $data['status'] = 'A';
            $dis_id = $_objAdmin->_dbInsert($data, 'table_messages');
        }
   }
   $sus = "Message has been sent successfully!.";
}
// This bellow code is use for the one cleaner id / supervisor id 
/*
if (isset($_POST['add']) && $_POST['add'] == 'yes') {
    
    $data['account_id'] = mysql_escape_string($_SESSION['accountId']);
    $data['message'] = preg_replace("/\r|\n/","",mysql_escape_string($_POST['message']));
    $data['subject'] = $_POST['subject'];
    $data['customer_id'] = $_POST['supervisor_id'];
    $data['cleaner_id'] = $_POST['cleaner_id'];
    $data['send_date'] = date('Y-m-d');
    $data['send_time'] = date('Y-m-d h:i:s');
    $data['status'] = 'A';
    $dis_id = $_objAdmin->_dbInsert($data, 'table_messages');
    $sus = "Message has been sent successfully!.";
}
*/
?>

<script type="text/javascript">
    
</script>
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <!-- BEGIN: Subheader -->
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">
                    Messages
                </h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="#" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__separator">
                        -
                    </li>
                    <li class="m-nav__item">
                        <a href="#" class="m-nav__link">
                            <span class="m-nav__link-text">
                                Master
                            </span>
                        </a>
                    </li>
                    <li class="m-nav__separator">
                        -
                    </li>
                    <li class="m-nav__item">
                        <a href="" class="m-nav__link">
                            <span class="m-nav__link-text">
                                Send Messages
                            </span>
                        </a>
                    </li>  
                </ul>
            </div> 
        </div>
    </div>
    <!-- END: Subheader -->
    <div class="m-content">
        <div class="row">
            <div class="col-lg-12">
                <!--begin::Portlet--> 
                <div class="m-portlet">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <span class="m-portlet__head-icon m--hide">
                                    <i class="la la-gear"></i>
                                </span>
                                <h3 class="m-portlet__head-text">
                                    Send Messages Form
                                </h3>
                            </div>
                        </div>
                    </div>
                    <!--begin::Form-->
                    <form class="m-form m-form--fit m-form--label-align-right" id="m_form_1"  method="post" action="send_message.php" enctype="multipart/form-data">

                        <div class="m-portlet__body">
                            <div class="m-form__content">
                                <div class="m-alert m-alert--icon alert alert-danger m--hide" role="alert" id="m_form_1_msg">
                                    <div class="m-alert__icon">
                                        <i class="la la-warning"></i>
                                    </div>
                                    <div class="m-alert__text">
                                        Oh snap! Change a few things up and try submitting again.
                                    </div>
                                    <div class="m-alert__close">
                                        <button type="button" class="close" data-close="alert" aria-label="Close"></button>
                                    </div>
                                </div>
                            </div>
                            <?php if (isset($err) && !empty($err)) { ?>
                                <div class="m-form__content">
                                    <div class="m-alert m-alert--icon alert alert-danger" role="alert" id="m_form_1_msg1">
                                        <div class="m-alert__icon">
                                            <i class="la la-warning"></i>
                                        </div>
                                        <div class="m-alert__text">
                                            <?php echo $err; ?>
                                        </div>
                                        <div class="m-alert__close">
                                            <button type="button" class="close" data-close="alert" aria-label="Close"></button>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                            <?php if($sus != ''){?>
                                <!--  start message-green -->

                                <div role="alert" style="background: #d7fbdc;" class="m-alert  m-alert--icon m-alert--air m-alert--square alert alert-dismissible  m--margin-bottom-30">
                                    <div class="m-alert__icon" >
                                        <i class="flaticon-exclamation m--font-brand"></i>
                                    </div>
                                    <div class="m-alert__text">
                                           <?php echo  $sus; ?>
                                    </div>
                                </div>


                            <?php }?>
                            <div class="form-group m-form__group row">
                                <div class="col-lg-4">
                                    <label>
                                        Subject:
                                    </label>
                                    <input type="text" class="form-control m-input" placeholder="" id="subject" name="subject">

                                </div>
                                <div class="col-lg-4">
                                    <label>
                                        Message:
                                    </label>
                                    <textarea type="text" class="form-control m-input" id="message" name="message"></textarea>

                                </div>
                            </div>
                            <div class="form-group m-form__group row"> 
                                <div class="col-lg-4">
                                    <label class="">
                                        User:
                                    </label>
                                    <div class="m-radio-inline">
                                        <label class="m-radio m-radio--solid">
                                            <input class="user_type" type="radio" name="user_type"  value="cleaner" onclick="changeUserType()">
                                            Cleaner
                                            <span></span>
                                        </label>
                                        <label class="m-radio m-radio--solid">
                                            <input class="user_type" type="radio" name="user_type" value="customer" checked onclick="changeUserType()">
                                            Customer
                                            <span></span>
                                        </label>
                                       
                                    </div> 
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <div class="col-lg-4" id="supervisorShow">
                                    <label>
                                        Customer:
                                    </label>
                                    <div class="input-group m-input-group m-input-group--square"> 
                                        <select class="form-control m-input m-input--square" id="supervisor_id" name="supervisor_id">
                                            <option value="">Select Customer</option>
                                            <option value="allcustomer">All Customer</option>
                                            <?php
                                            $condi = "";


                                            $branchList = $_objAdmin->_getSelectList('table_customer', "customer_id,customer_name", '', " status='A'");
                                            for ($i = 0; $i < count($branchList); $i++) {

                                                if ($branchList[$i]->supervisor_id == $auRec[0]->customer_id) {
                                                    $select = "selected";
                                                } else {
                                                    $select = "";
                                                }
                                                ?>
                                                <option value="<?php echo $branchList[$i]->customer_id; ?>" <?php echo $select ?>><?php echo $branchList[$i]->customer_name; ?></option>

                                            <?php } ?>
                                        </select>
                                    </div> 
                                </div>
                            <div class="col-lg-4" id="cleanerShow">
                                    <label>
                                        Cleaner:
                                    </label>
                                    <div class="input-group m-input-group m-input-group--square"> 
                                        <select class="form-control m-input m-input--square" id="cleaner_id" name="cleaner_id">
                                            <option value="">Select Cleaner</option>
                                            <option value="allcleaner">All Cleaner</option>
                                            <?php
                                            $condi = "";


                                            $branchList = $_objAdmin->_getSelectList('table_cleaner', "cleaner_id,cleaner_name", '', " status='A'");

                                            for ($i = 0; $i < count($branchList); $i++) {

                                                if ($branchList[$i]->cleaner_id == $auRec[0]->cleaner_id) {
                                                    $select = "selected";
                                                } else {
                                                    $select = "";
                                                }
                                                ?>
                                                <option value="<?php echo $branchList[$i]->cleaner_id; ?>" <?php echo $select ?>><?php echo $branchList[$i]->cleaner_name; ?></option>

                                            <?php } ?>
                                        </select>
                                    </div> 
                                </div>
                            </div>
                        </div>
                        <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                            <div class="m-form__actions m-form__actions--solid">
                                <div class="row">
                                    <div class="col-lg-4"></div>
                                    <div class="col-lg-8">
                                        <button type="submit" class="btn btn-success">
                                            Send
                                        </button>
                                        <button type="reset" class="btn btn-secondary">
                                            Reset
                                        </button>
                                        <a href="index.php" class="btn btn-secondary">
                                            Back
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <input name="account_id" type="hidden" value="<?php echo $_SESSION['accountId']; ?>" />
                        <input type="hidden" name="add" value="yes">

                    </form>
                    <!--end::Form-->
                </div>
                <!--end::Portlet-->
            </div>
        </div>
    </div>
</div>
</div>
</div>
<!-- start footer -->         
<?php include("footer.php") ?>

<script type="text/javascript">
    var FormControls = function () {
        //== Private functions

        var demo1 = function () {
            $("#m_form_1").validate({
                // define validation rules
                rules: {
                    subject: {
                        required: true
                    },
                    message: {
                        required: true
                    },
                    supervisor_id: {
                        required: true
                    }
                },
                //display error alert on form submit  
                invalidHandler: function (event, validator) {
                    var alert = $('#m_form_1_msg');
                    alert.removeClass('m--hide').show();
                    mApp.scrollTo(alert, -200);
                },
                submitHandler: function (form) {
                    form.submit();
                }
            });
        }

        return {
            // public functions
            init: function () {
                demo1();
            }
        };
    }();

    jQuery(document).ready(function () {

        FormControls.init();

    });
    
    function changeUserType () {
            var user_type = $('input[name=user_type]:checked', '#m_form_1').val();
            if(user_type == 'cleaner') {
                $("#supervisorShow").hide();
                $('#cleanerShow').show();
                $('#supervisor_id').attr('disabled');
                $('#supervisor_id').removeClass('required');
                $('#cleaner_id').addClass('required');
                $('#cleaner_id').val('');
                $('#supervisor_id').val('');
                $('#supervisor_id').rules('add', {
            required: false
        });
        $('#cleaner_id').rules('add', {
            required: true
        });
            } else if (user_type == 'customer') {
                $("#supervisorShow").show();
                $('#cleanerShow').hide();
                $('#cleaner_id').attr('disabled');
                $('#cleaner_id').removeClass('required');
                $('#supervisor_id').addClass('required');
                $('#cleaner_id').val('');
                $('#supervisor_id').val('');
                $('#supervisor_id').rules('add', {
            required: true
        });
        $('#cleaner_id').rules('add', {
            required: false
        });
            }
        }

    $(document).ready(function () {
        $("#supervisorShow").show();
        $('#cleanerShow').hide();
        $('#cleaner_id').attr('disabled');
        $('#cleaner_id').removeClass('required');
        $('#supervisor_id').rules('add', {
            required: true
        });
        $('#cleaner_id').rules('add', {
            required: false
        });
        
    });

</script>

