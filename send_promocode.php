<?php
include("includes/config.inc.php");
include("includes/function.php");
include("header.inc.php");

$_objAdmin = new Admin();
//include("includes/function.php");
//include("includes/globalarraylist.php");

$pageAccess=1;


$page_name="Message Broadcast";


if(isset($_POST['add']) && $_POST['add'] == 'yes') {
    $_objAdmin->sendPromoCode();
    $sus="Promo Code has been sent successfully!.";
}


?>
<script type="text/javascript">
function showBuilding(id){
            $.ajax({
                'type': 'POST',
                'url': 'promoBuilding.php',
                'data': 'id=' + id,
                'success': function (mystring) {
                    var buildingId = $.trim(mystring);
                    if(mystring != 0) {
                        $('#building_id').val(buildingId);
                        $('#building_id').attr("disabled", true); 
                    } else {
                        $('#building_id').val("");
                        $('#building_id').attr("disabled", false); 
                    }
                }
            });
        }
</script>
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <!-- BEGIN: Subheader -->
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">
                    Send Promo Code
                </h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="#" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__separator">
                        -
                    </li>
                    <li class="m-nav__item">
                        <a href="#" class="m-nav__link">
                            <span class="m-nav__link-text">
                                Master
                            </span>
                        </a>
                    </li>
                    <li class="m-nav__separator">
                        -
                    </li>
                    <li class="m-nav__item">
                        <a href="" class="m-nav__link">
                            <span class="m-nav__link-text">
                                Promo Code
                            </span>
                        </a>
                    </li>  
                </ul>
            </div> 
        </div>
    </div>
    <!-- END: Subheader -->
    <div class="m-content">
        <div class="row">
            <div class="col-lg-12">
                <!--begin::Portlet--> 
                <div class="m-portlet">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <span class="m-portlet__head-icon m--hide">
                                    <i class="la la-gear"></i>
                                </span>
                                <h3 class="m-portlet__head-text">
                                    Send Promo Code Form
                                </h3>
                            </div>
                        </div>
                    </div>
                    <!--begin::Form-->
                    <form class="m-form m-form--fit m-form--label-align-right" id="m_form_1"  method="post" action="send_promocode.php" enctype="multipart/form-data">

                        <div class="m-portlet__body">
                            <div class="m-form__content">
                                <div class="m-alert m-alert--icon alert alert-danger m--hide" role="alert" id="m_form_1_msg">
                                    <div class="m-alert__icon">
                                        <i class="la la-warning"></i>
                                    </div>
                                    <div class="m-alert__text">
                                        Oh snap! Change a few things up and try submitting again.
                                    </div>
                                    <div class="m-alert__close">
                                        <button type="button" class="close" data-close="alert" aria-label="Close"></button>
                                    </div>
                                </div>
                            </div>
                            <?php if (isset($err) && !empty($err)) { ?>
                                <div class="m-form__content">
                                    <div class="m-alert m-alert--icon alert alert-danger" role="alert" id="m_form_1_msg1">
                                        <div class="m-alert__icon">
                                            <i class="la la-warning"></i>
                                        </div>
                                        <div class="m-alert__text">
                                            <?php echo $err; ?>
                                        </div>
                                        <div class="m-alert__close">
                                            <button type="button" class="close" data-close="alert" aria-label="Close"></button>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                            <?php if($sus!=''){?>
                                <!--  start message-green -->

                                <div role="alert" style="background: #d7fbdc;" class="m-alert  m-alert--icon m-alert--air m-alert--square alert alert-dismissible  m--margin-bottom-30">
                                    <div class="m-alert__icon" >
                                        <i class="flaticon-exclamation m--font-brand"></i>
                                    </div>
                                    <div class="m-alert__text">
                                           <?php echo  $sus; ?>
                                    </div>
                                </div>
                            <?php }?>
                            <div class="form-group m-form__group row">

                                <div class="col-lg-4">
                                    <label>
                                        Promo Code:
                                    </label>
                                    <div class="input-group m-input-group m-input-group--square"> 
                                        <select class="form-control m-input m-input--square" id="promo_code_id" name="promo_code_id" onchange="showBuilding(this.value)">
                                            <option value="">Select Promo Code</option>
									<?php 
									$condi = "";
									
									$todayDate = date('Y-m-d');
									$branchList=$_objAdmin->_getSelectList2('table_promo_code',"promo_code_id,promo_code,building_id",''," status='A' and 	end_date >= '".$todayDate."'");
				                                      
										for($i=0;$i<count($branchList);$i++){
										
										if($branchList[$i]->promo_code_id==$auRec[0]->promo_code_id){$select="selected";} else {$select="";}
											
									 ?>
                                                                        <option value="<?php echo $branchList[$i]->promo_code_id; ?>" <?php echo $select ?>><?php echo $branchList[$i]->promo_code; ?></option>
									 
									 <?php } ?>
								</select>
                                    </div> 
                                </div>
                                <div class="col-lg-4">
                                    <label>
                                        Building:
                                    </label>
                                    <div class="input-group m-input-group m-input-group--square"> 
                                        <select class="form-control m-input m-input--square" id="building_id" name="building_id">
                                            <option value="">Select Building</option>
									<?php 
									$condi = "";
									
									
									$branchList=$_objAdmin->_getSelectList2('table_buildings',"building_id,building_name",''," status='A'");
				                                      
										for($i=0;$i<count($branchList);$i++){
										
										if($branchList[$i]->building_id==$auRec[0]->building_id){$select="selected";} else {$select="";}
											
									 ?>
									 <option value="<?php echo $branchList[$i]->building_id; ?>" <?php echo $select ?>><?php echo $branchList[$i]->building_name; ?></option>
									 
									 <?php } ?>
								</select>

                                    </div> 
                                </div>

                            </div>
                            <div class="form-group m-form__group row">
                                <div class="col-lg-4">
                                    <label>
                                        Message:
                                    </label>
                                    <textarea type="text" class="form-control m-input" id="message" name="message"></textarea>

                                </div>
                            </div>
                        </div>
                        <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                            <div class="m-form__actions m-form__actions--solid">
                                <div class="row">
                                    <div class="col-lg-4"></div>
                                    <div class="col-lg-8">
                                        <button type="submit" class="btn btn-success">
                                            Submit
                                        </button>
                                        <button type="reset" class="btn btn-secondary">
                                            Reset
                                        </button>
                                        <a href="index.php" class="btn btn-secondary" >
                                                Back
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <input name="account_id" type="hidden" value="<?php echo $_SESSION['accountId']; ?>" />
                        <input type="hidden" name="add" value="yes">

                    </form>
                    <!--end::Form-->
                </div>
                <!--end::Portlet-->
            </div>
        </div>
    </div>
</div>
</div>
</div>
<!-- start footer -->         
<?php include("footer.php") ?>

<script type="text/javascript">
    var FormControls = function () {
        //== Private functions

        var demo1 = function () {
            $("#m_form_1").validate({
                // define validation rules
                rules: {
                    promo_code_id: {
                        required: true
                    }
                },
                //display error alert on form submit  
                invalidHandler: function (event, validator) {
                    var alert = $('#m_form_1_msg');
                    alert.removeClass('m--hide').show();
                    mApp.scrollTo(alert, -200);
                },
                submitHandler: function (form) {
                    form.submit();
                }
            });
        }

        return {
            // public functions
            init: function () {
                demo1();
            }
        };
    }();

    jQuery(document).ready(function () {
        
        FormControls.init();
    });

</script>

