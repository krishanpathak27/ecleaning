<?php
include("includes/config.inc.php");
include("includes/function.php");
include("includes/globalarraylist.php");


if(isset($_REQUEST['show']) && $_REQUEST['show']=="yes"){
	$_objAdmin->showServiceDistributorSP();
	die;
}

include("service_distributor_sp.inc.php");
include("import.inc.php");
include("header.inc.php");

if(isset($_REQUEST['add']) || $_REQUEST['add']=='yes')
{
	$_SESSION['service_distributor_sp_id'] = '';
}
 ?>
 
<!-- start content-outer -->
<input name="pagename" type="hidden"  id="pagename" value="service_distributor_sp.php" />
<div id="content-outer">
<!-- start content -->
<div id="content">
<div id="page-heading"><h1><span style="color: #d74343; font-family: Tahoma; font-weight: bold;">Service Distributor Service Personnel</span></h1></div>
<?php if($_REQUEST['err']!=''){?>
	<div id="message-red">
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
	<tr>
		<td class="red-left">Error. <?php echo "you have exceeded the maximum limit of active users"; ?></td>
		<td class="red-right"><a class="close-red"><img src="images/icon_close_red.gif"   alt="" /></a></td>
	</tr>
	</table>
	</div>
	<?php } ?>
<table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">

<tr>
	<!--<td id="tbl-border-left"></td>-->
	<td>
	<!--  start content-table-inner -->
	<div id="content-table-inner">
	
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
	<tr valign="top">
	<td>
		<!-- start id-form -->
		<?php
		if(isset($_REQUEST['add']) || $service_dis_name_err!='' || ($_POST) && $_POST['submit'] == 'Save & Add Next Warehouse' || $_REQUEST['id']!='' || $_SESSION['dis_err']!=''){
		
			$pageAccess=1;
			$check=$_objArrayList->checkAccess($pageAccess, 'add_service_distributor_sp.php');	//	Correct code for User Type Checking			
			if($check == false){ header('Location:'. basename($_SERVER['PHP_SELF']));}	//	Correct code for User Type Checking
			

			// $check=$_objArrayList->checkAccess($pageAccess, 'add_service_distributor_sp.php');				
			// if($check == true){ header('Location:'. basename($_SERVER['PHP_SELF']));}
			else{ include("service_distributor_sp/add_service_distributor_sp.php");}
			
			// include("warehouses/add_service_distributor_sp.php");
			}
			else
			if($dis_add_sus!='' || $login_err!=''){
				include("service_distributor_sp/login_service_distributor_sp.php");
				}else
				if(isset ($_GET['skip']) || $login_sus!=''){
				include("service_distributor_sp/map_service_distributor_sp.php");
				}else
					if(isset ($_GET['import']) || $dis_err!=''){
					include("service_distributor_sp/import_service_distributor_sp.php");
					} else {
						include("service_distributor_sp/view_service_distributor_sp.php");
						unset($_SESSION['dis_err'], $_SESSION['service_distributor_sp_id'], $_SESSION['service_distributor_login'], $_SESSION['update'] );
					}
		?>
		<!-- end id-form  -->
	</td>
	<td>
	<!-- right bar-->
	<?php include("rightbar/service_distributor_sp_bar.php") ?>
	</td>
	</tr>
<tr>
</tr>
</table>
<div class="clear"></div>
</div>
<!--  end content-table-inner  -->
</td>
<td id="tbl-border-right"></td>
</tr>

</table>
<div class="clear">&nbsp;</div>
</div>
<!--  end content -->
<div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->
<div class="clear">&nbsp;</div> 
<!-- start footer -->         
<?php include("footer.php");?>
<!-- end footer -->
 
</body>
</html>

