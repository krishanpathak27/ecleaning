<?php include("includes/config.inc.php"); 
	  include("includes/function.php");
	  $page_name="Hierarchy Ordering Service Persons";
	  $_objAdmin = new Admin();
	  $auRec=$_objAdmin->_getSelectList('table_service_personnel_hierarchy',"*",''," status = 'A' ORDER BY sort_order ASC");
	  include("header.inc.php");
	 ?>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="javascripts/jquery.sortable.js"></script>
<script>
	$(function() { 
		$('#message-green').hide(); 
		$('.sortable').sortable(); 
	});


	function saveListOrder() {


		//var count = $('.sortable').find('li').length;
		//alert(count);
		
		var hierarchyId = [];
		$('.sortable li').each(function() { hierarchyId.push($(this).attr('title')) });
		
		// $.each(hierarchyId, function(key, value){
		// 	alert("Key " + key +"value = "+ value);
		// });
		
			//$('#dd-form').serialize();
			$.ajax({
				type: "POST",
				url: 'hierarchy_ajax_sortlevel_service_personnel.php',
				data: 'ID='+hierarchyId,
				success: function(){

					//console.log(status);
					//if(status)
					//{
						$('#message-green').show();
						//$('#message-green').text('Updating the sort order in the database.');
						//console.log(status);
						setTimeout(function(){
						$('#message-green').hide('slow'); 
						window.location.href = 'service_hierarchy_ordering.php';
						},2000);
						
					// } else {
					// 	alert('ajax error');
					// }
				}
				});	
	}
 
 </script>
<!-- start content-outer -->
<?php //include('actualvsplanned.php');?>
<style>
.sortable		{ padding:0; }
.sortable li	{ padding:4px 8px; color:#000; cursor:move; list-style:none; width:350px; background:#ddd; margin:10px 0; border:1px solid #999; }
#message-box		{ background:#fffea1; border:2px solid #fc0; padding:4px 8px; margin:0 0 14px 0; width:350px; }
</style>
<div id="content-outer">
<!-- start content -->
<div id="content">
<div id="page-heading">
	<h1><span style="color: #d74343; font-family: Tahoma; font-weight: bold;">Hierarchy Ordering</span></h1>
</div>

			<table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">
			<tr>
				
				<td>
				<!--  start content-table-inner -->
				<div id="content-table-inner">
				<div id="message-green" style="display:none;">
					<table border="0" width="100%" cellpadding="0" cellspacing="0">					<tr>
						<td class="green-left">Updating the sort order in the database.</td>
						<td class="green-right"><a class="close-green"><img src="images/icon_close_green.gif"   alt="" /></a> 						</td>
					</tr>
					</table>
							
				</div>
					<!--<h2>Hierarchy Level</h2>-->
						<form id="dd-form" action="<?php $_SERVER['PHP_SELF'];?>" method="post">
				<table border="0" width="100%" cellpadding="0" cellspacing="0">
				<tr valign="top">
				<td>
					<!-- start id-form -->
					<table  border="0" cellspacing="0" cellpadding="0"  align="center" width="100%">
						<tr>
							<td width="48%" valign="top">
							<div>
							<?php if(sizeof($auRec)>=1) {?>
								<ul class="sortable">
								<?php foreach($auRec as $key=>$value):?>
										<li title="<?php echo $value->hierarchy_id;?>"><?php echo $key+1 ;?>.&nbsp;<?php echo ucwords(strtolower($value->description));?></li>
										<?php endforeach; ?>
								</ul>
							
								
							<?php } else { ?>
							
							<p>Sorry!  There are no level in the system.</p>
							
						<?php } ?>
						</div>
						</td>
						
					</tr>
					</table>
					<!-- end id-form  -->
				</td>
				<td>
				<!-- right bar-->
				</td>
				</tr>
			<tr>
			<td><img src="images/blank.gif" width="695" height="1" alt="blank" /></td>
			<td></td>
			</tr>
			</table>
							<input type="button" name="back" value="Back" onclick="location.href='hierarchy_service_person.php'" class="form-reset" />
							<input type="button" name="do_submit" value="Sort" onclick="saveListOrder();" class="form-submit" />
			
				</form>
					<div class="clear"></div>
			</div>
			<!--  end content-table-inner  -->
			</td>
			<td id="tbl-border-right"></td>
			</tr>
			</table>

<div class="clear">&nbsp;</div>
</div>
<!--  end content -->
<div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->
<div class="clear">&nbsp;</div> 
<!-- start footer -->         
<?php include("footer.php");?>
<!-- end footer -->
</body>
</html>