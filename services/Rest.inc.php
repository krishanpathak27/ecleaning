<?php
	class REST {
		
		public $_allow = array();
		public $_content_type = "application/json";
		public $_request = array();
		
		private $_method = "";		
		private $_code = 200;
		
		public function __construct(){
			$this->inputs();
			if(function_exists('date_default_timezone_set')) date_default_timezone_set("Asia/Calcutta");
		}
		
		public function get_referer(){
			return $_SERVER['HTTP_REFERER'];
		}

		public function utf8ize($d) {
		    if (is_array($d)) {
			foreach ($d as $k => $v) {
			    $d[$k] = $this->utf8ize($v);
			}
		    } else if (is_string ($d)) {
			return utf8_encode($d);
		    }
		    return $d;
		}
		
		public function response($data,$status){
			$this->_code = ($status)?$status:200;
			$this->set_headers();
			echo $data;
			exit;
		}

		// For a list of http codes checkout http://en.wikipedia.org/wiki/List_of_HTTP_status_codes
		private function get_status_message(){
			$status = array(
						200 => 'Success',
						201 => 'Request Blank',  
						202 => 'Database Not Connected',  
						203 => 'Invalid Username',
						207 => 'Invalid User ID OR Authentication Key',  
						209 => 'Data Does Not Exist',  
						210 => 'Json Data Is Blank', 
						212 => 'Detail Already Exist In The System', 
						214 => 'Fields Can Not Be Empty', 
						404 => 'Not Found',  
						406 => 'Not Acceptable');
						
			return ($status[$this->_code])?$status[$this->_code]:$status[500];
		}
		
		public function get_request_method(){
			return $_SERVER['REQUEST_METHOD'];
		}
		
		private function inputs(){
			switch($this->get_request_method()){
				case "POST":
					$this->_request = $this->cleanInputs($_POST);
					break;
				case "GET":
				case "DELETE":
					$this->_request = $this->cleanInputs($_GET);
					break;
				case "PUT":
					parse_str(file_get_contents("php://input"),$this->_request);
					$this->_request = $this->cleanInputs($this->_request);
					break;
				default:
					$this->response('',406);
					break;
			}
		}		
		
		protected function cleanInputs($data){
			$clean_input = array();
			if(is_array($data)){
				foreach($data as $k => $v){
					$clean_input[$k] = $this->cleanInputs($v);
				}
			}else{
				if(get_magic_quotes_gpc()){
					$data = trim(stripslashes($data));
				}
				$data = strip_tags($data);
				$clean_input = trim($data);
			}
			return $clean_input;
		}		
		
		private function set_headers(){
			header("HTTP/1.1 ".$this->_code." ".$this->get_status_message());
			header("Content-Type:".$this->_content_type);
			header("Access-Control-Allow-Origin: *");
		}
	}	
?>
