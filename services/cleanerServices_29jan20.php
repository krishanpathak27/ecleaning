<?php
 //ini_set('display_errors', 1);
 //ini_set('display_startup_errors', 1);
//error_reporting(E_ALL);

require_once("Rest.inc.php");
include("../classes/FCM.php");

header("Access-Control-Allow-Origin: *");
define("TEXTLOCAL_API_KEY", "gsZbshodVdA-6nNSs0bAdEPiWYn0jMaQRWVARe1EWu");

class cleanerServices extends REST {


	const HOST = "http://quytech.in/ecleaning/dev/";
    const DB_SERVER = "localhost";
    const DB_USER = "ecleaning_user";
    const DB_PASSWORD = "quytech1234";
    const DB = "ecleaning_dev_db";

	// const HOST = "localhost/ecleaning/";
	// const DB_SERVER = "localhost";
	// const DB_USER = "phpmyadmin";
	// const DB_PASSWORD = "root";
	// const DB = "ecleaning_db";

	private $db = NULL;
	private $mysqli = NULL;
	private $perPageItem;
	private static $instance = null;

	public function __construct() {

        parent::__construct();    // Init parent contructor
        $this->perPageItem = 10;
        $this->database = cleanerServices::getInstance(); // Initiate Database connection
    }

    /*
     *  Connect to Database
    */
    private static function getInstance() {
    	if (self::$instance == null) {
    		self::$instance = new mysqli(self::DB_SERVER, self::DB_USER, self::DB_PASSWORD, self::DB);
    	}
    	return self::$instance;
    }


    /*
     * Dynmically call the method based on the query string
     */

    public function processApi() {

    	if (isset($_REQUEST['action'])) {
    		$func = strtolower(trim(str_replace("/", "", $_REQUEST['action'])));
    		if ((int) method_exists($this, $func) > 0)
    			$this->$func();
    		else
    			$error = array('status' => "Failed", "msg" => "Request Blank");
            $this->response($this->json($error), 201); // If the request is blank.
        } else {
        	$error = array('status' => "Failed", "msg" => "Request Blank");
            $this->response($this->json($error), 201); // If the request is blank.
        }
    }


    /*
     * To Check Required Fields
     */

    private function required_field($post, $required_fields = array()) {
    	$response = array();
    	foreach ($required_fields as $key) {
    		if (!isset($post[$key])) {
    			$response['status'] = 0;
    			$response['error'][] = 'invalid post (' . $key . ' not set)';
    		} else {
    			if ($post[$key] === '') {
    				$response['status'] = 0;
    				$response['error'][] = 'invalid post (' . $key . ' empty)';
    			}
    		}
    	}
    	if (isset($response['error'])) {
    		$this->response($this->json($response), 214);
    		die();
    	}
    }

    private function authenticateUser($cleanerId, $sessionId) {

    	if (!empty($cleanerId) && !empty($sessionId)) {
    		$sql = "SELECT tc.cleaner_id FROM table_cleaner as tc  WHERE tc.cleaner_id='" . $cleanerId . "' AND tc.session_id='" . $sessionId . "' ";

    		$checkUser = $this->database->query($sql);
    		if ($checkUser->num_rows > 0) {
    			$user_auth_status = true;
    		} else {
    			$user_auth_status = false;
    		}
    	} else {
    		$user_auth_status = false;
    	}

    	return $user_auth_status;
    }

    /*
    *  CLEANER LOGIN 
    */

    private function login() {

    	$cleanrParams = json_decode(file_get_contents("php://input"), true);

    	$datetime = date('Y-m-d H:i:s');
    	$page = "Login Cleaner";
    	$created_date = date('Y-m-d');
    	$last_update_date = date('Y-m-d');

    	if (!isset($cleanrParams['data']) || empty($cleanrParams['data']) || !isset($cleanrParams['data']['userName']) || empty($cleanrParams['data']['userName']) || !isset($cleanrParams['data']['password']) && empty($cleanrParams['data']['password']) || !isset($cleanrParams['data']['deviceId']) || !isset($cleanrParams['data']['gcmId']) || !isset($cleanrParams['data']['loginType']) || !isset($cleanrParams['data']['languageCode'])) {

    		$error = array('status' => "0", "msg" => "Request Blank", 'code' => "201");

    		file_put_contents("Log/cleanerLogin.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
    		file_put_contents("Log/cleanerLogin.log", print_r($page, true) . "\r\n", FILE_APPEND);
    		file_put_contents("Log/cleanerLogin.log", print_r($cleanrParams, true) . "\r\n", FILE_APPEND);
    		file_put_contents("Log/cleanerLogin.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
    		file_put_contents("Log/cleanerLogin.log", print_r($error, true) . "\r\n", FILE_APPEND);
    		$this->response($this->json($error), 201);
    	}
        // Get user name and pasword
    	$username = $cleanrParams['data']['userName'];
    	$password = md5(trim($cleanrParams['data']['password']));

    	$query = " SELECT u.*,s.*,s.session_id,a.status as account_status FROM table_web_users  as u inner join table_cleaner as s on u.cleaner_id=s.cleaner_id left join table_account as a on u.account_id=a.account_id where u.username='".mysql_escape_string($username)."' and u.password='".mysql_escape_string($password)."' and s.status='A' AND u.status='A' ";

    	$cleanerdata = $this->database->query($query);



    	if ($cleanerdata->num_rows > 0) {

    		$response_data = array();
    		$auth_token = mt_rand(10000000, 99999999);
    		$cleaner_data = mysqli_fetch_assoc($cleanerdata);
    		$updateAuthKey = "UPDATE table_cleaner SET session_id = '" . $auth_token . "',last_update_date = '" . date('Y-m-d H:i:s') . "',gcm_regid='" . $cleanrParams['data']['gcmId'] . "',login_type='" . trim($cleanrParams['data']['loginType']). "' ,app_language = '".trim($cleanrParams['data']['languageCode'])."'WHERE cleaner_id='" . $cleaner_data['cleaner_id'] . "'";

    		$AuthKey = $this->database->query($updateAuthKey) or die($this->mysqli->error . __LINE__);

    		/****Check customer device id already exists****/
    		$checkDeviceQuery = "Select * from table_devices as td where td.cleaner_id=" . $cleaner_data['cleaner_id'];
    		$resultDevice = $this->database->query($checkDeviceQuery);
    		if ($resultDevice->num_rows > 0) {
    			$deleteQuery = "Delete from table_devices where cleaner_id=" . $cleaner_data['cleaner_id'];
    			$this->database->query($deleteQuery);
    		}
    		/******Insert device id into device table***/
    		if (!empty($cleanrParams['data']['deviceId'])) {
    			$deviceIdQuery = "INSERT INTO table_devices(account_id,cleaner_id,device_id,created_date,last_update_date) VALUES ('" . $account_id . "','" . $cleaner_data['cleaner_id'] . "', '" . $cleanrParams['data']['deviceId'] . "','" . $created_date . "','" . $last_update_date . "')";
    			$this->database->query($deviceIdQuery);
    		}

    		$response_data['session_id'] = (string)$auth_token;
            $response_data['cleaner_address'] = (string)$cleaner_data['cleaner_address'];
    		$response_data['cleaner_id'] = (string)$cleaner_data['cleaner_id'];
    		$response_data['cleaner_name'] = (string)$cleaner_data['cleaner_name'];
    		$response_data['cleaner_email'] = (string)$cleaner_data['cleaner_email'];
    		$response_data['mobile_number_country_code'] = (string)$cleaner_data['mobile_number_country_code'];
    		$response_data['cleaner_phone_no'] = (string)$cleaner_data['cleaner_phone_no'];
    		$response_data['country'] = (string)$cleaner_data['country'];

            // select language after updaing
    		$getCusLangQuery = "SELECT tc.app_language FROM table_cleaner as tc where tc.cleaner_id=".$cleaner_data['cleaner_id'];

    		$custLangData = $this->database->query($getCusLangQuery);
    		$cusdata1 = mysqli_fetch_assoc($custLangData);
    		$response_data['language_code'] = (string)$cusdata1['app_language'];

    		/*********************************************************************/
    		if (isset($cleaner_data['image']) && !empty($cleaner_data['image'])) {
    			$response_data['image'] = self::HOST . "images/cleaner/" . $cleaner_data['image'];
    		}else{
    			$response_data['image'] = '';
    		}
    		if (empty($cleaner_data['lat'])) {
    			$response_data['lat'] = '';
    		}
    		if (empty($cleaner_data['lng'])) {
    			$response_data['lng'] = '';
    		}
    		$response_data['otp_verified'] = 1;
            if (isset($cleaner_data['cleaner_id_document']) && !empty($cleaner_data['cleaner_id_document'])) {
                $response_data['id_document'] = self::HOST . "images/cleaner_id_document/" . $cleaner_data['cleaner_id_document'];
            }else{
                $response_data['id_document'] = '';
            }
    		$success = array('status' => "1", "msg" => "User Logged In Successfully.", 'code' => "200", "data" => $response_data);

    		file_put_contents("Log/cleanerLogin.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
    		file_put_contents("Log/cleanerLogin.log", print_r($page, true) . "\r\n", FILE_APPEND);
    		file_put_contents("Log/cleanerLogin.log", print_r($cleanrParams, true) . "\r\n", FILE_APPEND);
    		file_put_contents("Log/cleanerLogin.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
    		file_put_contents("Log/cleanerLogin.log", print_r($success, true) . "\r\n", FILE_APPEND);

    		$this->response($this->json($success), 200);

    	}else{       

    		$condi=" where u.username='".mysql_escape_string($username)."' and u.password='".mysql_escape_string($password)."' and s.status='I'";

    		$userRec = mysql_query("SELECT u.*,s.session_id,a.status as account_status FROM table_web_users  as u inner join table_cleaner as s on u.cleaner_id=s.cleaner_id left join table_account as a on u.account_id=a.account_id".$condi);

    		$cleanerdata = $this->database->query($userRec);

    		if ($cleanerdata->num_rows > 0) {
    			while ($row = mysqli_fetch_assoc($customerdata)) {
    				$data = $row;

    			}
    			$data['otp'] = 12345;
    			$cleaner_phone_no = $data['cleaner_phone_no'];
    			$data['otp_verified'] = 0;
    			$data['language_code'] = (string)$data['app_language'];

    			$auth_token = mt_rand(10000000, 99999999);
    			$data['session_id'] =  (string)$auth_token;
    			$updateAuthKey = "UPDATE table_cleaner SET session_id = '" . $auth_token . "',last_update_date = '" . date('Y-m-d H:i:s') . "',gcm_regid='" . $cleanrParams['data']['gcmId'] . "',login_type='" . trim($cleanrParams['data']['loginType']). "' ,app_language = '".trim($cleanrParams['data']['languageCode'])."'WHERE cleaner_id='" . $cleaner_data['cleaner_id'] . "'";

    			$AuthKey = $this->database->query($updateAuthKey) or die($this->mysqli->error . __LINE__);
    			// select language after updaing
    			$getCusLangQuery = "SELECT tc.app_language FROM table_cleaner as tc where tc.cleaner_id=".$cleaner_data['cleaner_id'];

    			$custLangData = $this->database->query($getCusLangQuery);
    			$cusdata1 = mysqli_fetch_assoc($custLangData);
    			$response_data['language_code'] = (string)$cusdata1['app_language'];
    			$smsRes = $this->sendSms(TEXTLOCAL_API_KEY,$cleaner_phone_no,$data['otp']);

    			$success = array('status' => "1", "msg" => "Cleaner exits and otp has been sent.", "code" => "200", "data" => $data);

    			file_put_contents("Log/cleanerLogin.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
    			file_put_contents("Log/cleanerLogin.log", print_r($page, true) . "\r\n", FILE_APPEND);
    			file_put_contents("Log/cleanerLogin.log", print_r($cleanrParams, true) . "\r\n", FILE_APPEND);
    			file_put_contents("Log/cleanerLogin.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
    			file_put_contents("Log/cleanerLogin.log", print_r($success, true) . "\r\n", FILE_APPEND);

    			$this->response($this->json($success), 200);


    		}else{

            // If Username is invalid
    			$error = array('status' => 0, "msg" => "Invalid Username Or Password", 'code' => "203");

    			file_put_contents("Log/cleanerLogin.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
    			file_put_contents("Log/cleanerLogin.log", print_r($page, true) . "\r\n", FILE_APPEND);
    			file_put_contents("Log/cleanerLogin.log", print_r($cleanrParams, true) . "\r\n", FILE_APPEND);
    			file_put_contents("Log/cleanerLogin.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
    			file_put_contents("Log/cleanerLogin.log", print_r($error, true) . "\r\n", FILE_APPEND);

    			$this->response($this->json($error), 203);

    		}
    	}
    }

    /*
    *  CLEANER LOGOUT
    */
    private function logout() {

    	$datetime = date('Y-m-d H:i:s');
    	$page = "Cleaner Logout.";
    	$inputData = json_decode(file_get_contents("php://input"), true);

    	if (isset($inputData['data']) && !empty($inputData['data']) && isset($inputData['data']['cleaner_id']) && !empty($inputData['data']['cleaner_id']) && isset($inputData['data']['sessionId']) && !empty($inputData['data']['sessionId'])) {

    		$cleaner_id = trim($inputData['data']['cleaner_id']);
    		$sessionId = trim($inputData['data']['sessionId']);
    		$response = $this->authenticateUser($cleaner_id, $sessionId);
    		if ($response) {
    			$condition = " Where cleaner_id=" . $cleaner_id;
    			$updateQuery = "UPDATE table_cleaner set session_id=''" . $condition;
    			$result = $this->database->query($updateQuery);

    			$success = array("status" => "1", "msg" => "Logout successfully.", "code" => "200");

    			file_put_contents("Log/cleanerlogout.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
    			file_put_contents("Log/cleanerlogout.log", print_r($page, true) . "\r\n", FILE_APPEND);
    			file_put_contents("Log/cleanerlogout.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
    			file_put_contents("Log/cleanerlogout.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
    			file_put_contents("Log/cleanerlogout.log", print_r($success, true) . "\r\n", FILE_APPEND);

    			$this->response($this->json($success), 200);
    		} else {
    			$error = array('status' => "0", "msg" => "No user found!", "code" => "203");

    			file_put_contents("Log/cleanerlogout.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
    			file_put_contents("Log/cleanerlogout.log", print_r($page, true) . "\r\n", FILE_APPEND);
    			file_put_contents("Log/cleanerlogout.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
    			file_put_contents("Log/cleanerlogout.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
    			file_put_contents("Log/cleanerlogout.log", print_r($error, true) . "\r\n", FILE_APPEND);

    			$this->response($this->json($error), 203);
    		}
    	} else {
    		$error = array('status' => "0", "msg" => "Request Blank!", "code" => "201");

    		file_put_contents("Log/cleanerlogout.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
    		file_put_contents("Log/cleanerlogout.log", print_r($page, true) . "\r\n", FILE_APPEND);
    		file_put_contents("Log/cleanerlogout.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
    		file_put_contents("Log/cleanerlogout.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
    		file_put_contents("Log/cleanerlogout.log", print_r($error, true) . "\r\n", FILE_APPEND);

    		$this->response($this->json($error), 201);
    	}
    }

    /*
    *  CLEANER ALLOTED BOOKING 
    */
    private function alloted_booking(){

    	$datetime = date('Y-m-d H:i:s');
    	$page = "CLEANER ALLOTED BOOKING.";
    	$inputData = json_decode(file_get_contents("php://input"), true);

    	if (isset($inputData['data']) && !empty($inputData['data']) && isset($inputData['data']['cleaner_id']) && !empty($inputData['data']['cleaner_id']) && isset($inputData['data']['sessionId']) && !empty($inputData['data']['sessionId']) && isset($inputData['data']['booking_date']) && !empty($inputData['data']['booking_date'])) {

    		$cleaner_id = trim($inputData['data']['cleaner_id']);
    		$sessionId = trim($inputData['data']['sessionId']);
    		$response = $this->authenticateUser($cleaner_id, $sessionId);
    		if ($response) {

    			$query = "SELECT DTL.booking_id FROM table_booking_details as DTL left join table_booking_register as REG on REG.booking_id = DTL.booking_id left join table_booking_allotment as ba on ba.booking_detail_id = DTL.booking_detail_id where ba.cleaner_id='".$cleaner_id."' and DTL.cleaning_date >= '".date('Y-m-d',strtotime($inputData['data']['booking_date']))."' and DTL.status = 'A'" ;

    			$cleanerdata = $this->database->query($query);

    			if($cleanerdata->num_rows > 0){

    				$sql_query = "SELECT br.booking_id,br.total_cost_paid,bd.booking_detail_id,br.customer_id,bd.cleaning_date,c.customer_name,b.building_name,ca.customer_address,cs.slot_start_time,cs.slot_end_time,ba.created_date as alloted_date ,TAR.area_name,CITY.city_name FROM table_booking_register as br 
    				left join table_customer as c on c.customer_id = br.customer_id
    				left join table_customer_address ca on br.customer_address_id = ca.customer_address_id
    				left join table_buildings as b on ca.building_id = b.building_id
    				left join table_booking_details as bd on bd.booking_id = br.booking_id
    				left join table_calender_slots as cs on cs.slot_id = bd.slot_id
    				left join table_booking_allotment as ba on ba.booking_detail_id = bd.booking_detail_id
                    LEFT join table_area AS TAR on TAR.area_id = ca.area_id 
                    LEFT join city AS CITY on CITY.city_id = TAR.city_id
    				where bd.cleaner_id='".$cleaner_id."' and cleaning_date >= '".date('Y-m-d',strtotime($inputData['data']['booking_date']))."' and bd.status = 'A' and ba.booking_status = 'AL'
    				";

    				$bookingData = $this->database->query($sql_query);

    				if($bookingData->num_rows > 0){

    					while($row = mysqli_fetch_assoc($bookingData)){

    						$result['booking_id'] = $row['booking_id'];
    						$result['customer_id'] = $row['customer_id'];
    						$result['customer_name'] = $row['customer_name'];
    						$result['building_name'] = $row['building_name'];
    						// $result['customer_address'] = $row['customer_address'];
                            $result['customer_address'] = $row['customer_address'] ." ".$row['area_name']." ".$row['city_name'];
    						$result['cleaning_date'] = date("d M Y",strtotime($row['cleaning_date']));
    						$result['slot_start_time'] = date("H:i A",strtotime($row['slot_start_time']));
    						$result['slot_end_time'] = date("H:i A",strtotime($row['slot_end_time']));
    						$result['booking_detail_id'] = $row['booking_detail_id'];
                            $result['alloted_date'] = date("d M Y",strtotime($row['alloted_date']));
                            $result['alloted_time'] = date("H:i A",strtotime($row['alloted_date']));
    						$data[] = $result ; 
    					}

    					$success = array('status' => "1", "msg" => "All Alloted Booking", "code" => "200", "data" => $data);

    					file_put_contents("Log/scheduleServices.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
    					file_put_contents("Log/scheduleServices.log", print_r($page, true) . "\r\n", FILE_APPEND);
    					file_put_contents("Log/scheduleServices.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
    					file_put_contents("Log/scheduleServices.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
    					file_put_contents("Log/scheduleServices.log", print_r($success, true) . "\r\n", FILE_APPEND);

    					$this->response($this->json($success), 200);

    				}else{

    					$error = array('status' => "0", "msg" => "Data not found!", "code" => "209");

    					file_put_contents("Log/CleanerAllotedBooking.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
    					file_put_contents("Log/CleanerAllotedBooking.log", print_r($page, true) . "\r\n", FILE_APPEND);
    					file_put_contents("Log/CleanerAllotedBooking.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
    					file_put_contents("Log/CleanerAllotedBooking.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
    					file_put_contents("Log/CleanerAllotedBooking.log", print_r($error, true) . "\r\n", FILE_APPEND);

    					$this->response($this->json($error), 209);

    				}


    			}else{

    				$error = array('status' => "0", "msg" => "Data not found!", "code" => "209");

    				file_put_contents("Log/CleanerAllotedBooking.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
    				file_put_contents("Log/CleanerAllotedBooking.log", print_r($page, true) . "\r\n", FILE_APPEND);
    				file_put_contents("Log/CleanerAllotedBooking.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
    				file_put_contents("Log/CleanerAllotedBooking.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
    				file_put_contents("Log/CleanerAllotedBooking.log", print_r($error, true) . "\r\n", FILE_APPEND);

    				$this->response($this->json($error), 209);


    			}

    		}else{

    			$error = array('status' => "0", "msg" => "No user found!", "code" => "203");

    			file_put_contents("Log/CleanerAllotedBooking.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
    			file_put_contents("Log/CleanerAllotedBooking.log", print_r($page, true) . "\r\n", FILE_APPEND);
    			file_put_contents("Log/CleanerAllotedBooking.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
    			file_put_contents("Log/CleanerAllotedBooking.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
    			file_put_contents("Log/CleanerAllotedBooking.log", print_r($error, true) . "\r\n", FILE_APPEND);

    			$this->response($this->json($error), 203);

    		}


    	}else{

    		$error = array('status' => "0", "msg" => "Request Blank!", "code" => "201");

    		file_put_contents("Log/CleanerAllotedBooking.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
    		file_put_contents("Log/CleanerAllotedBooking.log", print_r($page, true) . "\r\n", FILE_APPEND);
    		file_put_contents("Log/CleanerAllotedBooking.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
    		file_put_contents("Log/CleanerAllotedBooking.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
    		file_put_contents("Log/CleanerAllotedBooking.log", print_r($error, true) . "\r\n", FILE_APPEND);

    		$this->response($this->json($error), 201);

    	}


    }


   /*
    *  CLEANER CHANGE ONLINE STATUS 
    */
   private function change_status(){

   	$datetime = date('Y-m-d H:i:s');
   	$page = "CLEANER CHANGE STATUS.";
   	$inputData = json_decode(file_get_contents("php://input"), true);

   	if (isset($inputData['data']) && !empty($inputData['data']) && isset($inputData['data']['cleaner_id']) && !empty($inputData['data']['cleaner_id']) && isset($inputData['data']['sessionId']) && !empty($inputData['data']['sessionId']) && isset($inputData['data']['status']) && !empty($inputData['data']['status'])) {

   		$cleaner_id = trim($inputData['data']['cleaner_id']);
   		$sessionId = trim($inputData['data']['sessionId']);
   		$change_status = '';
   		if($inputData['data']['status'] == 'Online') {
   			$change_status = 1;
   		} else if($inputData['data']['status'] == 'Offline') {
   			$change_status = 0;
   		}
   		$response = $this->authenticateUser($cleaner_id, $sessionId);
   		if ($response) {

   			$status_query = "UPDATE table_cleaner SET online_status='".$change_status."',last_update_date=NOW() where cleaner_id='".mysql_escape_string($cleaner_id)."'";
   			$result = $this->database->query($status_query);
   			if ($result) {

   				$success = array("status" => "1", "msg" => "Status Change successfully .", "code" => "200");

   				file_put_contents("Log/CleanerChangeStatus.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
   				file_put_contents("Log/CleanerChangeStatus.log", print_r($page, true) . "\r\n", FILE_APPEND);
   				file_put_contents("Log/CleanerChangeStatus.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
   				file_put_contents("Log/CleanerChangeStatus.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
   				file_put_contents("Log/CleanerChangeStatus.log", print_r($success, true) . "\r\n", FILE_APPEND);

   				$this->response($this->json($success), 200);

   			}

   		}else{

   			$error = array('status' => "0", "msg" => "No user found!", "code" => "203");

   			file_put_contents("Log/CleanerChangeStatus.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
   			file_put_contents("Log/CleanerChangeStatus.log", print_r($page, true) . "\r\n", FILE_APPEND);
   			file_put_contents("Log/CleanerChangeStatus.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
   			file_put_contents("Log/CleanerChangeStatus.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
   			file_put_contents("Log/CleanerChangeStatus.log", print_r($error, true) . "\r\n", FILE_APPEND);

   			$this->response($this->json($error), 203);
   		}
   	}else{

   		$error = array('status' => "0", "msg" => "Request Blank!", "code" => "201");

   		file_put_contents("Log/CleanerChangeStatus.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
   		file_put_contents("Log/CleanerChangeStatus.log", print_r($page, true) . "\r\n", FILE_APPEND);
   		file_put_contents("Log/CleanerChangeStatus.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
   		file_put_contents("Log/CleanerChangeStatus.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
   		file_put_contents("Log/CleanerChangeStatus.log", print_r($error, true) . "\r\n", FILE_APPEND);

   		$this->response($this->json($error), 201);

   	}


   }

   /*
    *  CLEANER CHANGE BOOKING STATUS 
    */
   private function cleaner_booking_status(){


   	$datetime = date('Y-m-d H:i:s');
   	$page = "CLEANER BOOKING STATUS.";
   	$inputData = json_decode(file_get_contents("php://input"), true);

   	if (isset($inputData['data']) && !empty($inputData['data']) && isset($inputData['data']['cleaner_id']) && !empty($inputData['data']['cleaner_id']) && isset($inputData['data']['sessionId']) && !empty($inputData['data']['sessionId']) && isset($inputData['data']['booking_detail_id']) && !empty($inputData['data']['booking_detail_id']) && isset($inputData['data']['booking_status']) && !empty($inputData['data']['booking_status']) &&  isset($inputData['data']['customer_id']) && !empty($inputData['data']['customer_id']) && isset($inputData['data']['lat']) && !empty($inputData['data']['lat']) && isset($inputData['data']['lng']) && !empty($inputData['data']['lng']) && isset($inputData['data']['accuracy_level']) && !empty($inputData['data']['accuracy_level']) && isset($inputData['data']['app_version']) && !empty($inputData['data']['app_version']) && isset($inputData['data']['device_id']) && !empty($inputData['data']['device_id']) && isset($inputData['data']['device_brand_model']) && !empty($inputData['data']['device_brand_model']) ) {

   		$cleaner_id = trim($inputData['data']['cleaner_id']);
   		$sessionId = trim($inputData['data']['sessionId']);
   		$data['cleaner_id'] = $cleaner_id;
   		$data['booking_status'] = trim($inputData['data']['booking_status']);
   		$data['booking_detail_id'] = trim($inputData['data']['booking_detail_id']);
   		$data['customer_id'] = trim($inputData['data']['customer_id']);
   		$data['lat'] = trim($inputData['data']['lat']);
   		$data['lng'] = trim($inputData['data']['lng']);
   		$data['network_mode'] = trim($inputData['data']['network_mode']);
   		$data['accuracy_level'] = trim($inputData['data']['accuracy_level']);
   		$data['app_version'] = trim($inputData['data']['app_version']);
   		$data['device_id'] = trim($inputData['data']['device_id']);
   		$data['device_brand_model'] = trim($inputData['data']['device_brand_model']);

   		$response = $this->authenticateUser($cleaner_id, $sessionId);
   		if ($response) {
   			if($data['cleaner_id'] != ''){
    			//if booking Status is AC(Accepted)
   				if($data['booking_status'] == 'AC') {
	   					$message = 'Booking Accepted';
	       			 //update in booking register
	   					$sql = "UPDATE table_booking_details SET cleaner_id='".$data['cleaner_id']."' where booking_detail_id='".$data['booking_detail_id']."'";
	   					$this->database->query($sql) or die($this->mysqli->error . __LINE__);

	       			 //update allotment table
	   					$sql = "UPDATE table_booking_allotment SET booking_accepted_date=now(),booking_status='AC',booking_accepted='yes',last_update_date=now() where booking_detail_id='".$data['booking_detail_id']."'";
	   					$this->database->query($sql) or die($this->mysqli->error . __LINE__);

	       				 //insert in activity table
	   					$sql = "INSERT INTO table_cleaner_activity (cleaner_id,account_id,customer_id,activity,activity_type,start_time,end_time,activity_date,booking_detail_id,lat,lng,network_mode,accuracy_level,server_datetime,app_version,device_id,device_brand_model) VALUES "
	   					. "('".$data['cleaner_id']."','1','". $data['customer_id']."','Accepted','AC',now(),now(),now(),'".$data['booking_detail_id']."','".$data['lat']."
	   					','".$data['lng']."','".$data['network_mode']."','".$data['accuracy_level']."','".date('Y-m-d H:i:s')."','".$data['app_version']."','".$data['device_id']."','".$data['device_brand_model']."')";
	   					$this->database->query($sql) or die($this->mysqli->error . __LINE__);

	        			//update previous activity AL
	   					$sql = "UPDATE table_cleaner_activity SET end_time=now() where booking_detail_id='".$data['booking_detail_id']."' and activity_type = 'AL'";
	   					$this->database->query($sql) or die($this->mysqli->error . __LINE__);

				    }else if($data['booking_status'] == 'RD'){ //if booking Status is RD(Reached)
				    	$message = 'Cleaner Reached';
				        //update allotment table
				    	$sql = "UPDATE table_booking_allotment SET booking_status='RD',last_update_date=now() where booking_detail_id='".$data['booking_detail_id']."'";
				    	$this->database->query($sql) or die($this->mysqli->error . __LINE__);

				        //insert in activity table
				    	$sql = "INSERT INTO table_cleaner_activity (cleaner_id,account_id,customer_id,activity,activity_type,start_time,end_time,activity_date,booking_detail_id,lat,lng,network_mode,accuracy_level,server_datetime,app_version,device_id,device_brand_model) VALUES "
				    	. "('".$data['cleaner_id']."','1','". $data['customer_id']."','Reached','RD',now(),now(),now(),'".$data['booking_detail_id']."','".$data['lat']."','".$data['lng']."','".$data['network_mode']."','".$data['accuracy_level']."','".date('Y-m-d H:i:s')."','".$data['app_version']."','".$data['device_id']."','".$data['device_brand_model']."')";
				    	$this->database->query($sql) or die($this->mysqli->error . __LINE__);

				        //update previous activity AC
				    	$sql = "UPDATE table_cleaner_activity SET end_time=now() where booking_detail_id='".$data['booking_detail_id']."' and activity_type = 'AC'";
				    	$this->database->query($sql) or die($this->mysqli->error . __LINE__);
				    } else if($data['booking_status'] == 'SC'){ //if booking Status is SC(Start Cleaing)
				    	$message = 'Start Cleaning';
				        //update allotment table
				    	$sql = "UPDATE table_booking_allotment SET booking_status='SC',last_update_date=now() where booking_detail_id='".$data['booking_detail_id']."'";
				    	$this->database->query($sql) or die($this->mysqli->error . __LINE__);

				        //insert in activity table
				    	$sql = "INSERT INTO table_cleaner_activity (cleaner_id,account_id,customer_id,activity,activity_type,start_time,end_time,activity_date,booking_detail_id,lat,lng,network_mode,accuracy_level,server_datetime,app_version,device_id,device_brand_model) VALUES "
				    	. "('".$data['cleaner_id']."','1','". $data['customer_id']."','Start Cleaning','SC',now(),now(),now(),'".$data['booking_detail_id']."','".$data['lat']."','".$data['lng']."','".$data['network_mode']."','".$data['accuracy_level']."','".date('Y-m-d H:i:s')."','".$data['app_version']."','".$data['device_id']."','".$data['device_brand_model']."')";
				    	$this->database->query($sql) or die($this->mysqli->error . __LINE__);

				        //update previous activity RD
				    	$sql = "UPDATE table_cleaner_activity SET end_time=now() where booking_detail_id='".$data['booking_detail_id']."' and activity_type = 'RD'";
				    	$this->database->query($sql) or die($this->mysqli->error . __LINE__);
				    }else if($data['booking_status'] == 'CC'){ //if booking Status is CC(Complete Cleaing)
				    	$message = 'Complete Cleaning';
				        //update allotment table
				    	$sql = "UPDATE table_booking_allotment SET booking_status='CC',last_update_date=now() where booking_detail_id='".$data['booking_detail_id']."'";
				    	$this->database->query($sql) or die($this->mysqli->error . __LINE__);

				        //insert in activity table
				    	$sql = "INSERT INTO table_cleaner_activity (cleaner_id,account_id,customer_id,activity,activity_type,start_time,end_time,activity_date,booking_detail_id,lat,lng,network_mode,accuracy_level,server_datetime,app_version,device_id,device_brand_model) VALUES "
				    	. "('".$data['cleaner_id']."','1','". $data['customer_id']."','Complete Cleaning','CC',now(),now(),now(),'".$data['booking_detail_id']."','".$data['lat']."','".$data['lng']."','".$data['network_mode']."','".$data['accuracy_level']."','".date('Y-m-d H:i:s')."','".$data['app_version']."','".$data['device_id']."','".$data['device_brand_model']."')";
				    	$this->database->query($sql) or die($this->mysqli->error . __LINE__);

				        //update previous activity SC
				    	$sql = "UPDATE table_cleaner_activity SET end_time=now() where booking_detail_id='".$data['booking_detail_id']."' and activity_type = 'SC'";
				    	$this->database->query($sql) or die($this->mysqli->error . __LINE__);
				    } else if($data['booking_status'] == 'RP'){ //if booking Status is RP(Received Payment)
				    	$message = 'Received Payment';
				        //update allotment table
				    	$sql = "UPDATE table_booking_allotment SET booking_status='CB',last_update_date=now() where booking_detail_id='".$data['booking_detail_id']."'";
				    	$this->database->query($sql) or die($this->mysqli->error . __LINE__);

				        //insert in activity table
				    	$sql = "INSERT INTO table_cleaner_activity (cleaner_id,account_id,customer_id,activity,activity_type,start_time,end_time,activity_date,booking_detail_id,lat,lng,network_mode,accuracy_level,server_datetime,app_version,device_id,device_brand_model) VALUES "
				    	. "('".$data['cleaner_id']."','1','". $data['customer_id']."','Payment Received','RP',now(),now(),now(),'".$data['booking_detail_id']."','".$data['lat']."','".$data['lng']."','".$data['network_mode']."','".$data['accuracy_level']."','".date('Y-m-d H:i:s')."','".$data['app_version']."','".$data['device_id']."','".$data['device_brand_model']."')";
				    	$this->database->query($sql) or die($this->mysqli->error . __LINE__);

				        //update previous activity CC
				    	$sql = "UPDATE table_cleaner_activity SET end_time=now() where booking_detail_id='".$data['booking_detail_id']."' and activity_type = 'CC'";
				    	$this->database->query($sql) or die($this->mysqli->error . __LINE__);

				        //complete the booking
				        //insert in activity table
				    	$sql = "INSERT INTO table_cleaner_activity (cleaner_id,account_id,customer_id,activity,activity_type,end_time,start_time,activity_date,booking_detail_id,lat,lng,network_mode,accuracy_level,server_datetime,app_version,device_id,device_brand_model) VALUES "
				    	. "('".$data['cleaner_id']."','1','". $data['customer_id']."','Complete Booking','CB',now(),now(),now(),'".$data['booking_detail_id']."','".$data['lat']."','".$data['lng']."','".$data['network_mode']."','".$data['accuracy_level']."','".date('Y-m-d H:i:s')."','".$data['app_version']."','".$data['device_id']."','".$data['device_brand_model']."')";
				    	$this->database->query($sql) or die($this->mysqli->error . __LINE__);

				        //update previous activity RP
				    	$sql = "UPDATE table_cleaner_activity SET end_time=now() where booking_detail_id='".$data['booking_detail_id']."' and activity_type = 'RP'";
				    	$this->database->query($sql) or die($this->mysqli->error . __LINE__);
					    }else if($data['booking_status'] == 'RJ'){ //if booking Status is Rj(Rejected)
					    	$message = 'Booking Rejected';
					        //update allotment table
					    	$sql = "UPDATE table_booking_allotment SET booking_status='RJ',last_update_date=now(),reject_reason_id='".$data['reject_reason_id']."',other_rejection_reason='".$data['other_rejection_reason']."' where booking_detail_id='".$data['booking_detail_id']."'";
					    	$this->database->query($sql) or die($this->mysqli->error . __LINE__);

					        //insert in activity table
					    	$sql = "INSERT INTO table_cleaner_activity (cleaner_id,account_id,customer_id,activity,activity_type,start_time,end_time,activity_date,booking_detail_id,lat,lng,network_mode,accuracy_level,server_datetime,app_version,device_id,device_brand_model) VALUES "
					    	. "('".$data['cleaner_id']."','1','". $data['customer_id']."','Rejected','RJ',now(),now(),now(),'".$data['booking_detail_id']."','".$data['lat']."','".$data['lng']."','".$data['network_mode']."','".$data['accuracy_level']."','".date('Y-m-d H:i:s')."','".$data['app_version']."','".$data['device_id']."','".$data['device_brand_model']."')";
					    	$this->database->query($sql) or die($this->mysqli->error . __LINE__);

					        //update previous activity SC
					    	$sql = "UPDATE table_cleaner_activity SET end_time=now() where booking_detail_id='".$data['booking_detail_id']."' and activity_type = 'AL'";
					    	$this->database->query($sql) or die($this->mysqli->error . __LINE__);

					        //update in booking register
					    	$sql = "UPDATE table_booking_details SET cleaner_id='' where booking_detail_id='".$data['booking_detail_id']."'";
					    	$this->database->query($sql) or die($this->mysqli->error . __LINE__);
    					}

    					$success = array("status" => "1", "msg" => $message, "code" => "200");

    					file_put_contents("Log/CleanerBookingStatus.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
    					file_put_contents("Log/CleanerChangeStatus.log", print_r($page, true) . "\r\n", FILE_APPEND);
    					file_put_contents("Log/CleanerBookingStatus.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
    					file_put_contents("Log/CleanerBookingStatus.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
    					file_put_contents("Log/CleanerBookingStatus.log", print_r($success, true) . "\r\n", FILE_APPEND);

    					$this->response($this->json($success), 200);


				}// END INNER IF


   		}else{
   			$error = array('status' => "0", "msg" => "No user found!", "code" => "203");

   			file_put_contents("Log/CleanerBookingStatus.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
   			file_put_contents("Log/CleanerBookingStatus.log", print_r($page, true) . "\r\n", FILE_APPEND);
   			file_put_contents("Log/CleanerBookingStatus.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
   			file_put_contents("Log/CleanerBookingStatus.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
   			file_put_contents("Log/CleanerBookingStatus.log", print_r($error, true) . "\r\n", FILE_APPEND);

   			$this->response($this->json($error), 203);

   		}
   	}else{
   		$error = array('status' => "0", "msg" => "Request Blank!", "code" => "201");

   		file_put_contents("Log/CleanerBookingStatus.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
   		file_put_contents("Log/CleanerBookingStatus.log", print_r($page, true) . "\r\n", FILE_APPEND);
   		file_put_contents("Log/CleanerBookingStatus.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
   		file_put_contents("Log/CleanerBookingStatus.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
   		file_put_contents("Log/CleanerBookingStatus.log", print_r($error, true) . "\r\n", FILE_APPEND);

   		$this->response($this->json($error), 201);
   	}



   }

   /*
    *  GET CLEANER DETAILS
    */
   private function cleaner_detail(){

   	$datetime = date('Y-m-d H:i:s');
   	$page = "GET CLEANER DETAILS";
   	$inputData = json_decode(file_get_contents("php://input"), true);

   	if (isset($inputData['data']) && !empty($inputData['data']) && isset($inputData['data']['cleaner_id']) && !empty($inputData['data']['cleaner_id']) && isset($inputData['data']['sessionId']) && !empty($inputData['data']['sessionId']) ) {

   		$cleaner_id = trim($inputData['data']['cleaner_id']);
   		$sessionId = trim($inputData['data']['sessionId']);
   		$response = $this->authenticateUser($cleaner_id, $sessionId);
   		if ($response) {

   			$sql_query = "SELECT cleaner_id,cleaner_name,cleaner_email,cleaner_phone_no,mobile_number_country_code,cleaner_address,image,app_language,cleaner_id_document FROM table_cleaner where cleaner_id='".$cleaner_id."'";

   			$result = $this->database->query($sql_query) or die($this->mysqli->error . __LINE__);


	        if ($result->num_rows > 0) {

	        	$cleanerData = mysqli_fetch_assoc($result);

	        	$response_data['cleaner_id'] = (string)$cleanerData['cleaner_id'];
	        	$response_data['cleaner_name'] = (string)$cleanerData['cleaner_name'];
	        	$response_data['cleaner_email'] = (string)$cleanerData['cleaner_email'];
	        	$response_data['cleaner_phone_no'] = (string)$cleanerData['cleaner_phone_no'];
	        	$response_data['mobile_number_country_code'] = (string)$cleanerData['mobile_number_country_code'];
	        	$response_data['language_code'] = (string)$cleanerData['app_language'];
	        	$response_data['cleaner_address'] = (string)$cleanerData['cleaner_address'];

	        	if (isset($cleanerData['image']) && !empty($cleanerData['image'])) {
	        		$response_data['image'] = self::HOST . "images/cleaner/" . $cleanerData['image'];
	        	}else{
	        		$response_data['image'] = '';
	        	}
                if (isset($cleanerData['cleaner_id_document']) && !empty($cleanerData['cleaner_id_document'])) {
                    $response_data['id_document'] = self::HOST . "images/cleaner_id_document/" . $cleanerData['cleaner_id_document'];
                }else{
                    $response_data['id_document'] = '';
                }
	        	$success = array("status" => "1", "msg" => "Cleaner details is found!", "code" => "200", "data" => $response_data);
	        	file_put_contents("Log/CleanerDetail.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
	        	file_put_contents("Log/CleanerDetail.log", print_r($page, true) . "\r\n", FILE_APPEND);
	        	file_put_contents("Log/CleanerDetail.log", print_r($success, true) . "\r\n", FILE_APPEND);
	        	$this->response($this->json($success), 200);


	        }else{

	        	$error = array('status' => "0", "msg" => "Data not found!", "code" => "209");

	        	file_put_contents("Log/CleanerDetail.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
	        	file_put_contents("Log/CleanerDetail.log", print_r($page, true) . "\r\n", FILE_APPEND);
	        	file_put_contents("Log/CleanerDetail.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
	        	file_put_contents("Log/CleanerDetail.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
	        	file_put_contents("Log/CleanerDetail.log", print_r($error, true) . "\r\n", FILE_APPEND);

	        	$this->response($this->json($error), 209);
	        }
   		}else{

   			$error = array('status' => "0", "msg" => "No user found!", "code" => "203");

   			file_put_contents("Log/CleanerDetail.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
   			file_put_contents("Log/CleanerDetail.log", print_r($page, true) . "\r\n", FILE_APPEND);
   			file_put_contents("Log/CleanerDetail.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
   			file_put_contents("Log/CleanerDetail.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
   			file_put_contents("Log/CleanerDetail.log", print_r($error, true) . "\r\n", FILE_APPEND);

   			$this->response($this->json($error), 203);

   		}
   	}else{
   		$error = array('status' => "0", "msg" => "Request Blank!", "code" => "201");

   		file_put_contents("Log/CleanerDetail.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
   		file_put_contents("Log/CleanerDetail.log", print_r($page, true) . "\r\n", FILE_APPEND);
   		file_put_contents("Log/CleanerDetail.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
   		file_put_contents("Log/CleanerDetail.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
   		file_put_contents("Log/CleanerDetail.log", print_r($error, true) . "\r\n", FILE_APPEND);

   		$this->response($this->json($error), 201);
   	}

   }

   /*
    *  UPDATE CLEANER DETAILS
    */
  private function update_cleaner_details()
    {
        $datetime = date('Y-m-d H:i:s');
        $page = "UPDATE CLEANER DETAILS";
        //$inputData = json_decode(file_get_contents("php://input"), true);

        if(!isset($_POST)){

            $error = array('status' => "0", "msg" => "Request Blank!", "code" => "201");

            file_put_contents("Log/CleanerDetailUpdate.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/CleanerDetailUpdate.log", print_r($page, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/CleanerDetailUpdate.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/CleanerDetailUpdate.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/CleanerDetailUpdate.log", print_r($error, true) . "\r\n", FILE_APPEND);

            $this->response($this->json($error), 201);
        }
       
        $inputData['data'] = $_POST;
        if (isset($inputData['data']) && !empty($inputData['data']) && isset($inputData['data']['cleaner_id']) && !empty($inputData['data']['cleaner_id']) && isset($inputData['data']['sessionId']) && !empty($inputData['data']['sessionId']) && isset($inputData['data']['cleaner_address']) && !empty($inputData['data']['cleaner_address']) && isset($inputData['data']['cleaner_phone_no']) && !empty($inputData['data']['cleaner_phone_no']) && isset($inputData['data']['cleaner_name']) && !empty($inputData['data']['cleaner_name']) && isset($inputData['data']['mobile_number_country_code']) && !empty($inputData['data']['mobile_number_country_code'])) {

            $cleaner_id = trim($inputData['data']['cleaner_id']);
            $sessionId = trim($inputData['data']['sessionId']);
            $cleaneDetail = $inputData['data'];

            $response = $this->authenticateUser($cleaner_id, $sessionId);
            if ($response) {
                if(trim($cleaneDetail['cleaner_id'])!= '') {
                    $cleaner_id = trim($cleaneDetail['cleaner_id']);
                    $cleaner_address = trim($cleaneDetail['cleaner_address']);
                    $contact_number = trim($cleaneDetail['cleaner_phone_no']);
                    //$cleaner_email = trim($cleaneDetail['cleaner_email']);
                    $cleanerName = trim($cleaneDetail['cleaner_name']);
                    $mobile_number_country_code = trim($cleaneDetail['mobile_number_country_code']);
                    // $profilePic = '';
                    // if(isset($cleaneDetail['profile_pic']) && !empty($cleaneDetail['profile_pic']))
                    // {   
                    //     $num=$_objAdmin->randimg(10);
                    //     $binary=base64_decode($cleaneDetail['profile_pic']);
                    //     $image="img_".$num.".jpg";
                    //     $upload="../images/cleaner/".$image;
                    //     $file = fopen($upload, 'wb');
                    //     fwrite($file, $binary);  
                    //     fclose($file);
                    //     $profilePic = $image;
                    // }

                if(isset($_FILES['profile_pic']) && !empty($_FILES['profile_pic']))
                { 
                    if($_FILES['profile_pic']['name']!=''){
                        $file_name = $this->clean($_FILES['profile_pic']['name']);
                        $target_folder = '../images/cleaner/'; 
                        $newname=basename(str_replace(" ","", $file_name));
                        $upload_image = $target_folder.$newname;
                        if(move_uploaded_file($_FILES['profile_pic']['tmp_name'], $upload_image)) 
                        {               
                            $profilePic = $newname;
                        }else{
                            $error = array('status' => "0", "msg" => "Image is not uploaded!", "code" => "302");
                            $this->response($this->json($error), 302);
                        }
                    }
                }
                if(isset($_FILES['id_document']) && !empty($_FILES['id_document'])) {
                    if($_FILES['id_document']['name']!='') {
                        $id_document_file = $this->clean($_FILES['id_document']['name']);
                        $id_document_folder = '../images/cleaner_id_document/'; 
                        $date=date('dmY-His');
                        $id_doc_filename=rand(1,9).$date.basename( str_replace(" ","", $id_document_file));
                        $upload_id_image = $id_document_folder.$id_doc_filename;
                        //$types = array('image/jpeg', 'image/gif', 'image/png', 'image/jpg');
                        if(move_uploaded_file($_FILES['id_document']['tmp_name'], $upload_id_image)) {             
                            $idDocumentPic = $id_doc_filename;
                        } else {
                            $error = array('status' => "0", "msg" => "Id document Image is not uploaded!", "code" => "302");
                            $this->response($this->json($error), 302);
                        }
                    }
                }
                    $last_update_date = $datetime;
                    $last_update_status = "Update";
                    if(isset($profilePic) && !empty($profilePic) && !isset($idDocumentPic) && empty($idDocumentPic)) {
                        $sql = "UPDATE table_cleaner SET cleaner_address='$cleaner_address',cleaner_name='$cleanerName',image='$profilePic',cleaner_phone_no='$contact_number',last_update_status = 'Update',last_update_date = '$datetime' where cleaner_id='".$cleaner_id."'";
                    } else if(isset($idDocumentPic) && !empty($idDocumentPic) && !isset($profilePic) && empty($profilePic)) {
                        $sql = "UPDATE table_cleaner SET cleaner_address='$cleaner_address',cleaner_name='$cleanerName',cleaner_id_document='$idDocumentPic',cleaner_phone_no='$contact_number',last_update_status = 'Update',last_update_date = '$datetime' where cleaner_id='".$cleaner_id."'";
                    } else if(isset($profilePic) && !empty($profilePic) && isset($idDocumentPic) && !empty($idDocumentPic)) {
                        $sql = "UPDATE table_cleaner SET cleaner_address='$cleaner_address',cleaner_name='$cleanerName',image='$profilePic',cleaner_id_document='$idDocumentPic',cleaner_phone_no='$contact_number',last_update_status = 'Update',last_update_date = '$datetime' where cleaner_id='".$cleaner_id."'";
                    } else {
                        $sql = "UPDATE table_cleaner SET cleaner_address='$cleaner_address',cleaner_name='$cleanerName',cleaner_phone_no='$contact_number',last_update_status = 'Update',last_update_date = '$datetime' where cleaner_id='".$cleaner_id."'";
                    }
                    $result = $this->database->query($sql) or die($this->mysqli->error . __LINE__);
                    if ($result) {

                        $sql_query = "SELECT cleaner_id,cleaner_name,cleaner_email,cleaner_phone_no,mobile_number_country_code,cleaner_address,image,app_language,cleaner_id_document FROM table_cleaner where cleaner_id='".$cleaner_id."'";

                        $result = $this->database->query($sql_query) or die($this->mysqli->error . __LINE__);

                        if ($result->num_rows > 0) {

                            $cleanerData = mysqli_fetch_assoc($result);

                            $response_data['cleaner_id'] = (string)$cleanerData['cleaner_id'];
                            $response_data['cleaner_name'] = (string)$cleanerData['cleaner_name'];
                            $response_data['cleaner_email'] = (string)$cleanerData['cleaner_email'];
                            $response_data['cleaner_phone_no'] = (string)$cleanerData['cleaner_phone_no'];
                            $response_data['mobile_number_country_code'] = (string)$cleanerData['mobile_number_country_code'];
                            $response_data['language_code'] = (string)$cleanerData['app_language'];
                            $response_data['cleaner_address'] = (string)$cleanerData['cleaner_address'];

                            if (isset($cleanerData['image']) && !empty($cleanerData['image'])) {
                                $response_data['image'] = self::HOST . "images/cleaner/" . $cleanerData['image'];
                            }else{
                                $response_data['image'] = '';
                            }
                            if (isset($cleanerData['cleaner_id_document']) && !empty($cleanerData['cleaner_id_document'])) {
                                $response_data['id_document'] = self::HOST . "images/cleaner_id_document/" . $cleanerData['cleaner_id_document'];
                            } else{
                                $response_data['id_document'] = '';
                            }
                        }

                        $success = array("status" => "1", "msg" => "Cleaner detail updated successfully .", "code" => "200",'data'=>$response_data);

                        file_put_contents("Log/CleanerDetailUpdate.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
                        file_put_contents("Log/CleanerDetailUpdate.log", print_r($page, true) . "\r\n", FILE_APPEND);
                        file_put_contents("Log/CleanerDetailUpdate.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
                        file_put_contents("Log/CleanerDetailUpdate.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
                        file_put_contents("Log/CleanerDetailUpdate.log", print_r($success, true) . "\r\n", FILE_APPEND);

                        $this->response($this->json($success), 200);

                    }else{

                        $success = array("status" => "1", "msg" => "Something Went Worng!", "code" => "309");

                        file_put_contents("Log/CleanerDetailUpdate.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
                        file_put_contents("Log/CleanerDetailUpdate.log", print_r($page, true) . "\r\n", FILE_APPEND);
                        file_put_contents("Log/CleanerDetailUpdate.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
                        file_put_contents("Log/CleanerDetailUpdate.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
                        file_put_contents("Log/CleanerDetailUpdate.log", print_r($success, true) . "\r\n", FILE_APPEND);

                        $this->response($this->json($success), 200);
                    }
                }

            }else{
                $error = array('status' => "0", "msg" => "No user found!", "code" => "203");

                file_put_contents("Log/CleanerDetailUpdate.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/CleanerDetailUpdate.log", print_r($page, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/CleanerDetailUpdate.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/CleanerDetailUpdate.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/CleanerDetailUpdate.log", print_r($error, true) . "\r\n", FILE_APPEND);

                $this->response($this->json($error), 203);
            }
        }else{

            $error = array('status' => "0", "msg" => "Request Blank!", "code" => "201");

            file_put_contents("Log/CleanerDetailUpdate.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/CleanerDetailUpdate.log", print_r($page, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/CleanerDetailUpdate.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/CleanerDetailUpdate.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/CleanerDetailUpdate.log", print_r($error, true) . "\r\n", FILE_APPEND);

            $this->response($this->json($error), 201);
        }
    }

    /*
    *  UPDATE CLEANER FEEDBACK
    */
    private function update_feedback(){

    	$datetime = date('Y-m-d H:i:s');
    	$page = "ADD CLEANER FEEDBACK";
    	$inputData = json_decode(file_get_contents("php://input"), true);

    		if (isset($inputData['data']) && !empty($inputData['data']) && isset($inputData['data']['cleaner_id']) && !empty($inputData['data']['cleaner_id']) && isset($inputData['data']['sessionId']) && !empty($inputData['data']['sessionId']) && isset($inputData['data']['customer_id']) && !empty($inputData['data']['customer_id']) && isset($inputData['data']['booking_id']) && !empty($inputData['data']['booking_id']) && isset($inputData['data']['star_rating']) && !empty($inputData['data']['star_rating']) ) {

    		$cleaner_id = trim($inputData['data']['cleaner_id']);
    		$sessionId = trim($inputData['data']['sessionId']);
    		$data = $inputData['data'];
    		$response = $this->authenticateUser($cleaner_id, $sessionId);
    		if ($response) {
    			if(trim($data['cleaner_id'])!= ''){
    				$cleaner_id = trim($data['cleaner_id']);
    				$customer_id = trim($data['customer_id']);
    				$booking_id = trim($data['booking_id']);
    				$star_rating = trim($data['star_rating']);
    				if(isset($data['comments']) && !empty($data['comments'])) {
    					$comments = trim($data['comments']);
    				} else {
    					$comments = "";
    				}
    				$sql_query = "INSERT INTO table_service_feedback (star_rating,account_id,comments,booking_id,customer_id,cleaner_id,feedback_type,created_date,status) VALUES ('".$star_rating."','1','". $comments."','".$booking_id."','".$customer_id."','".$cleaner_id."','cleaner',now(),'A')";

    				$result = $this->database->query($sql_query) or die($this->mysqli->error . __LINE__);

    				if($result){
    					$success = array("status" => "1", "msg" => "Cleaner feedback updated successfully", "code" => "200");

    					file_put_contents("Log/CleanerFeedback.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
    					file_put_contents("Log/CleanerFeedback.log", print_r($page, true) . "\r\n", FILE_APPEND);
    					file_put_contents("Log/CleanerFeedback.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
    					file_put_contents("Log/CleanerFeedback.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
    					file_put_contents("Log/CleanerFeedback.log", print_r($success, true) . "\r\n", FILE_APPEND);

    					$this->response($this->json($success), 200);

    				}else{
    					$success = array("status" => "1", "msg" => "Something Went Worng!", "code" => "309");

    					file_put_contents("Log/CleanerFeedback.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
    					file_put_contents("Log/CleanerFeedback.log", print_r($page, true) . "\r\n", FILE_APPEND);
    					file_put_contents("Log/CleanerFeedback.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
    					file_put_contents("Log/CleanerFeedback.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
    					file_put_contents("Log/CleanerFeedback.log", print_r($success, true) . "\r\n", FILE_APPEND);

    					$this->response($this->json($success), 200);

    				}

    			}

    		}else{
    			$error = array('status' => "0", "msg" => "No user found!", "code" => "203");

    			file_put_contents("Log/CleanerFeedback.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
    			file_put_contents("Log/CleanerFeedback.log", print_r($page, true) . "\r\n", FILE_APPEND);
    			file_put_contents("Log/CleanerFeedback.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
    			file_put_contents("Log/CleanerFeedback.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
    			file_put_contents("Log/CleanerFeedback.log", print_r($error, true) . "\r\n", FILE_APPEND);

    			$this->response($this->json($error), 203);

    		}
    	}else{

    		$error = array('status' => "0", "msg" => "Request Blank!", "code" => "201");

    		file_put_contents("Log/CleanerFeedback.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
    		file_put_contents("Log/CleanerFeedback.log", print_r($page, true) . "\r\n", FILE_APPEND);
    		file_put_contents("Log/CleanerFeedback.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
    		file_put_contents("Log/CleanerFeedback.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
    		file_put_contents("Log/CleanerFeedback.log", print_r($error, true) . "\r\n", FILE_APPEND);

    		$this->response($this->json($error), 201);

    	}

    }
    /*
    *  CLEANER PAYMENT HISTORY
    */
    private function payment_history(){

    	$datetime = date('Y-m-d H:i:s');
    	$page = "CLEANER PAYMENT HISTORY";
    	$inputData = json_decode(file_get_contents("php://input"), true);

    		if (isset($inputData['data']) && !empty($inputData['data']) && isset($inputData['data']['cleaner_id']) && !empty($inputData['data']['cleaner_id']) && isset($inputData['data']['sessionId']) && !empty($inputData['data']['sessionId']) ) {

    		$cleaner_id = trim($inputData['data']['cleaner_id']);
    		$sessionId = trim($inputData['data']['sessionId']);
    		$response = $this->authenticateUser($cleaner_id, $sessionId);
    		if ($response) {
    			$sql_query = "SELECT br.booking_id FROM table_booking_register as br 
				        LEFT JOIN table_booking_details as bd on bd.booking_id = br.booking_id 
				        LEFT JOIN table_booking_allotment as ba on ba.booking_detail_id = bd.booking_detail_id where bd.cleaner_id='".$cleaner_id."' and ba.booking_status = 'CC' ";


				$result = $this->database->query($sql_query) or die($this->mysqli->error . __LINE__);
				if($result->num_rows > 0 ){

					$sql = "select br.customer_id,bd.cleaning_date,tc.customer_name,th.payment_mode,br.total_cost_paid,cs.slot_start_time,cs.slot_end_time,br.booking_id  from table_booking_register as br 
					LEFT JOIN table_payment_history as th on th.payment_id = br.payment_id 
					LEFT JOIN table_customer as tc on tc.customer_id = br.customer_id 
					LEFT JOIN table_booking_details as bd on bd.booking_id = br.booking_id 
					LEFT JOIN table_booking_allotment as ba on ba.booking_detail_id = bd.booking_detail_id 
					LEFT JOIN table_calender_slots as cs on cs.slot_id = bd.slot_id 
					where bd.cleaner_id='".$cleaner_id."' and ba.booking_status = 'CC'";

					$paymentSql = $this->database->query($sql) or die($this->mysqli->error . __LINE__);
					if($paymentSql->num_rows > 0 ){
						$data =array();
						while($row = mysqli_fetch_assoc($paymentSql)){

                            $response_data['booking_id'] = $row['booking_id'];
							$response_data['customer_name'] = $row['customer_name'];
							$response_data['payment_mode'] = $row['payment_mode'];
							$response_data['total_cost_paid'] = $row['total_cost_paid'];
							$response_data['slot_start_time'] = date("H:i A",strtotime($row['slot_start_time']));
							$response_data['slot_end_time'] = date("H:i A",strtotime($row['slot_end_time']));
							$response_data['cleaning_date'] = date("d M Y",strtotime($row['cleaning_date']));
							$data[] = $response_data;

						}
						$success = array('status' => "1", "msg" => "Cleaner payment history", 'code' => "200", "data" => $data);

						file_put_contents("Log/CleanerPaymentHistory.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
						file_put_contents("Log/CleanerPaymentHistory.log", print_r($page, true) . "\r\n", FILE_APPEND);
						file_put_contents("Log/CleanerPaymentHistory.log", print_r($cleanrParams, true) . "\r\n", FILE_APPEND);
						file_put_contents("Log/CleanerPaymentHistory.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
						file_put_contents("Log/CleanerPaymentHistory.log", print_r($success, true) . "\r\n", FILE_APPEND);

						$this->response($this->json($success), 200);

					}// END INNER IF


				}else{
					$error = array('status' => "0", "msg" => "Data not found!", "code" => "209");

					file_put_contents("Log/CleanerPaymentHistory.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
					file_put_contents("Log/CleanerPaymentHistory.log", print_r($page, true) . "\r\n", FILE_APPEND);
					file_put_contents("Log/CleanerPaymentHistory.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
					file_put_contents("Log/CleanerPaymentHistory.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
					file_put_contents("Log/CleanerPaymentHistory.log", print_r($error, true) . "\r\n", FILE_APPEND);

					$this->response($this->json($error), 209);

				}

    		}else{
    			$error = array('status' => "0", "msg" => "No user found!", "code" => "203");

    			file_put_contents("Log/CleanerPaymentHistory.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
    			file_put_contents("Log/CleanerPaymentHistory.log", print_r($page, true) . "\r\n", FILE_APPEND);
    			file_put_contents("Log/CleanerPaymentHistory.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
    			file_put_contents("Log/CleanerPaymentHistory.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
    			file_put_contents("Log/CleanerPaymentHistory.log", print_r($error, true) . "\r\n", FILE_APPEND);

    			$this->response($this->json($error), 203);

    		}
    	}else{

    		$error = array('status' => "0", "msg" => "Request Blank!", "code" => "201");

    		file_put_contents("Log/CleanerPaymentHistory.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
    		file_put_contents("Log/CleanerPaymentHistory.log", print_r($page, true) . "\r\n", FILE_APPEND);
    		file_put_contents("Log/CleanerPaymentHistory.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
    		file_put_contents("Log/CleanerPaymentHistory.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
    		file_put_contents("Log/CleanerPaymentHistory.log", print_r($error, true) . "\r\n", FILE_APPEND);

    		$this->response($this->json($error), 201);

    	}

    }

    /*
    *  CLEANER MESSAGE
    */
    private function messages(){

    	$datetime = date('Y-m-d H:i:s');
    	$page = "CLEANER PAYMENT HISTORY";
    	$inputData = json_decode(file_get_contents("php://input"), true);

    		if (isset($inputData['data']) && !empty($inputData['data']) && isset($inputData['data']['cleaner_id']) && !empty($inputData['data']['cleaner_id']) && isset($inputData['data']['sessionId']) && !empty($inputData['data']['sessionId']) ) {

    		$cleaner_id = trim($inputData['data']['cleaner_id']);
    		$sessionId = trim($inputData['data']['sessionId']);
    		$response = $this->authenticateUser($cleaner_id, $sessionId);
    		if ($response) {
    			$sql_query = "SELECT MSG.*,CLNR.cleaner_name,MSG.status as msg_sts FROM table_messages as MSG left join table_cleaner as CLNR on CLNR.cleaner_id = MSG.cleaner_id where MSG.cleaner_id='".$cleaner_id."' and MSG.status = 'A'";

    			$result = $this->database->query($sql_query) or die($this->mysqli->error . __LINE__);

				if($result->num_rows > 0 ){
					$data = array();
					   while($row = mysqli_fetch_assoc($result)){
					    	$response_data['subject'] = $row['subject'];
							$response_data['message'] = $row['message'];
							$response_data['send_date'] = date("d M Y",strtotime($row['send_date']));
							$response_data['send_time'] = date("H:i A",strtotime($row['send_time']));
							$response_data['cleaner_name'] = $row['cleaner_name'];
							$response_data['message_id'] = $row['message_id'];
							$response_data['status'] = $row['msg_sts'];
							$data[] = $response_data;
					   }
					   $success = array('status' => "1", "msg" => "Cleaner message", 'code' => "200", "data" => $data);

						file_put_contents("Log/CleanerMessage.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
						file_put_contents("Log/CleanerMessage.log", print_r($page, true) . "\r\n", FILE_APPEND);
						file_put_contents("Log/CleanerMessage.log", print_r($cleanrParams, true) . "\r\n", FILE_APPEND);
						file_put_contents("Log/CleanerMessage.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
						file_put_contents("Log/CleanerMessage.log", print_r($success, true) . "\r\n", FILE_APPEND);

						$this->response($this->json($success), 200);
				}else{
					$error = array('status' => "0", "msg" => "Data not found!", "code" => "209");

					file_put_contents("Log/CleanerMessage.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
					file_put_contents("Log/CleanerMessage.log", print_r($page, true) . "\r\n", FILE_APPEND);
					file_put_contents("Log/CleanerMessage.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
					file_put_contents("Log/CleanerMessage.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
					file_put_contents("Log/CleanerMessage.log", print_r($error, true) . "\r\n", FILE_APPEND);

					$this->response($this->json($error), 209);
				}


    		}else{
    			$error = array('status' => "0", "msg" => "No user found!", "code" => "203");

    			file_put_contents("Log/CleanerMessage.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
    			file_put_contents("Log/CleanerMessage.log", print_r($page, true) . "\r\n", FILE_APPEND);
    			file_put_contents("Log/CleanerMessage.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
    			file_put_contents("Log/CleanerMessage.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
    			file_put_contents("Log/CleanerPaymentHistory.log", print_r($error, true) . "\r\n", FILE_APPEND);

    			$this->response($this->json($error), 203);
    		}
    	}else{
    		$error = array('status' => "0", "msg" => "Request Blank!", "code" => "201");

    		file_put_contents("Log/CleanerMessage.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
    		file_put_contents("Log/CleanerMessage.log", print_r($page, true) . "\r\n", FILE_APPEND);
    		file_put_contents("Log/CleanerMessage.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
    		file_put_contents("Log/CleanerMessage.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
    		file_put_contents("Log/CleanerMessage.log", print_r($error, true) . "\r\n", FILE_APPEND);

    		$this->response($this->json($error), 201);
    	}
    }

    /*
    *  CLEANER REJECTION REASON
    */
    private function reject_reason(){

    	$datetime = date('Y-m-d H:i:s');
    	$page = "CLEANER REJECTION REASON";
    	$inputData = json_decode(file_get_contents("php://input"), true);

    		if (isset($inputData['data']) && !empty($inputData['data']) && isset($inputData['data']['cleaner_id']) && !empty($inputData['data']['cleaner_id']) && isset($inputData['data']['sessionId']) && !empty($inputData['data']['sessionId']) ) {

    		$cleaner_id = trim($inputData['data']['cleaner_id']);
    		$sessionId = trim($inputData['data']['sessionId']);
    		$response = $this->authenticateUser($cleaner_id, $sessionId);
    		if ($response) {

    			$sql_query = "SELECT reject_reason_id,reject_reason FROM table_reject_reason";
    			$result = $this->database->query($sql_query) or die($this->mysqli->error . __LINE__);

    			if($result->num_rows > 0){
    				$data = array();
    				while ($row = mysqli_fetch_assoc($result)) {
	    				$datas['reject_reason_id'] = $row['reject_reason_id'];
	    				$datas['reject_reason'] = $row['reject_reason'];
	    				$data[] = $datas;

    				}
    				$success = array('status' => "1", "msg" => "Cleaner Rejection Reason", 'code' => "200", "data" => $data);

    				file_put_contents("Log/CleanerRejectionReason.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
    				file_put_contents("Log/CleanerRejectionReason.log", print_r($page, true) . "\r\n", FILE_APPEND);
    				file_put_contents("Log/CleanerRejectionReason.log", print_r($cleanrParams, true) . "\r\n", FILE_APPEND);
    				file_put_contents("Log/CleanerRejectionReason.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
    				file_put_contents("Log/CleanerRejectionReason.log", print_r($success, true) . "\r\n", FILE_APPEND);

    				$this->response($this->json($success), 200);
    			}else{
    				$error = array('status' => "0", "msg" => "Data not found!", "code" => "209");

					file_put_contents("Log/CleanerRejectionReason.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
					file_put_contents("Log/CleanerRejectionReason.log", print_r($page, true) . "\r\n", FILE_APPEND);
					file_put_contents("Log/CleanerRejectionReason.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
					file_put_contents("Log/CleanerRejectionReason.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
					file_put_contents("Log/CleanerRejectionReason.log", print_r($error, true) . "\r\n", FILE_APPEND);

					$this->response($this->json($error), 209);
    			}
    		}else{
    			$error = array('status' => "0", "msg" => "No user found!", "code" => "203");

    			file_put_contents("Log/CleanerRejectionReason.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
    			file_put_contents("Log/CleanerRejectionReason.log", print_r($page, true) . "\r\n", FILE_APPEND);
    			file_put_contents("Log/CleanerRejectionReason.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
    			file_put_contents("Log/CleanerRejectionReason.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
    			file_put_contents("Log/CleanerRejectionReason.log", print_r($error, true) . "\r\n", FILE_APPEND);

    			$this->response($this->json($error), 203);

    		}
    	}else{
    		$error = array('status' => "0", "msg" => "Request Blank!", "code" => "201");

    		file_put_contents("Log/CleanerRejectionReason.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
    		file_put_contents("Log/CleanerRejectionReason.log", print_r($page, true) . "\r\n", FILE_APPEND);
    		file_put_contents("Log/CleanerRejectionReason.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
    		file_put_contents("Log/CleanerRejectionReason.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
    		file_put_contents("Log/CleanerRejectionReason.log", print_r($error, true) . "\r\n", FILE_APPEND);

    		$this->response($this->json($error), 201);
    	}
    }

    /*
    *  CLEANER ROUTE PLAN
    */
    private function route_plan(){
    	$datetime = date('Y-m-d H:i:s');
    	$page = "CLEANER ROUTE PLAN";
    	$inputData = json_decode(file_get_contents("php://input"), true);

    		if (isset($inputData['data']) && !empty($inputData['data']) && isset($inputData['data']['cleaner_id']) && !empty($inputData['data']['cleaner_id']) && isset($inputData['data']['sessionId']) && !empty($inputData['data']['sessionId']) && isset($inputData['data']['booking_date']) && !empty($inputData['data']['booking_date'])) {

    		$cleaner_id = trim($inputData['data']['cleaner_id']);
    		$sessionId = trim($inputData['data']['sessionId']);
    		$response = $this->authenticateUser($cleaner_id, $sessionId);
    		if ($response) {
    			$sql_query = "SELECT DTL.booking_id FROM table_booking_details as DTL left join table_booking_register as REG on REG.booking_id = DTL.booking_id where DTL.cleaner_id='".$cleaner_id."' and DTL.cleaning_date = '".date('Y-m-d',strtotime($inputData['data']['booking_date']))."' and DTL.status = 'A'";
    			$result = $this->database->query($sql_query) or die($this->mysqli->error . __LINE__);
    			if($result->num_rows > 0 ){

    				$route_sql = "SELECT br.booking_id,bd.booking_detail_id,br.customer_id,bd.cleaning_date,c.customer_name,b.building_name,ca.customer_address,cs.slot_start_time,cs.slot_end_time FROM table_booking_register as br 
    				left join table_customer as c on c.customer_id = br.customer_id 
    				left join table_customer_address ca on br.customer_address_id = ca.customer_address_id 
    				left join table_buildings as b on ca.building_id = b.building_id 
    				left join table_booking_details as bd on bd.booking_id = br.booking_id 
    				left join table_calender_slots as cs on cs.slot_id = bd.slot_id 
    				left join table_booking_allotment as ba on ba.booking_detail_id = bd.booking_detail_id
    				where bd.cleaner_id='".$cleaner_id."' and cleaning_date = '".date('Y-m-d',strtotime($inputData['data']['booking_date']))."' and bd.status = 'A' and (ba.booking_status = 'AC' or ba.booking_status = 'SC' )";

    				$routePlan = $this->database->query($route_sql)or die($this->mysqli->error . __LINE__);

    				if($routePlan->num_rows > 0 ){
    					$data = array();
    					while($row = mysqli_fetch_assoc($routePlan)){
    						$datas['booking_id'] = $row['booking_id'];
    						$datas['customer_id'] = $row['customer_id'];
    						$datas['customer_name'] = $row['customer_name'];
    						$datas['building_name'] = $row['building_name'];
    						$datas['customer_address'] = $row['customer_address'];
    						$datas['cleaning_date'] = date("d M Y",strtotime($row['cleaning_date']));
    						$datas['slot_start_time'] = date("H:i A",strtotime($row['slot_start_time']));
    						$datas['slot_end_time'] =  date("H:i A",strtotime($row['slot_end_time']));
    						$datas['booking_detail_id'] = $row['booking_detail_id'];
    						$data[] = $datas;
    					}
    					$success = array('status' => "1", "msg" => "Cleaner Route Plan", 'code' => "200", "data" => $data);

    					file_put_contents("Log/CleanerRoutePlan.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
    					file_put_contents("Log/CleanerRoutePlan.log", print_r($page, true) . "\r\n", FILE_APPEND);
    					file_put_contents("Log/CleanerRoutePlan.log", print_r($cleanrParams, true) . "\r\n", FILE_APPEND);
    					file_put_contents("Log/CleanerRoutePlan.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
    					file_put_contents("Log/CleanerRoutePlan.log", print_r($success, true) . "\r\n", FILE_APPEND);

    					$this->response($this->json($success), 200);
    				}else{
    					$error = array('status' => "0", "msg" => "Data not found!", "code" => "209");

    					file_put_contents("Log/CleanerRoutePlan.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
    					file_put_contents("Log/CleanerRoutePlan.log", print_r($page, true) . "\r\n", FILE_APPEND);
    					file_put_contents("Log/CleanerRoutePlan.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
    					file_put_contents("Log/CleanerRoutePlan.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
    					file_put_contents("Log/CleanerRoutePlan.log", print_r($error, true) . "\r\n", FILE_APPEND);

    					$this->response($this->json($error), 209);
    				}
    			}else{
    				$error = array('status' => "0", "msg" => "Data not found!", "code" => "209");

					file_put_contents("Log/CleanerRoutePlan.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
					file_put_contents("Log/CleanerRoutePlan.log", print_r($page, true) . "\r\n", FILE_APPEND);
					file_put_contents("Log/CleanerRoutePlan.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
					file_put_contents("Log/CleanerRoutePlan.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
					file_put_contents("Log/CleanerRoutePlan.log", print_r($error, true) . "\r\n", FILE_APPEND);

					$this->response($this->json($error), 209);
    			}
    		}else{
    			$error = array('status' => "0", "msg" => "No user found!", "code" => "203");

    			file_put_contents("Log/CleanerRoutePlan.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
    			file_put_contents("Log/CleanerRoutePlan.log", print_r($page, true) . "\r\n", FILE_APPEND);
    			file_put_contents("Log/CleanerRoutePlan.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
    			file_put_contents("Log/CleanerRoutePlan.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
    			file_put_contents("Log/CleanerRoutePlan.log", print_r($error, true) . "\r\n", FILE_APPEND);

    			$this->response($this->json($error), 203);
    		}
    	}else{
    		$error = array('status' => "0", "msg" => "Request Blank!", "code" => "201");

    		file_put_contents("Log/CleanerRoutePlan.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
    		file_put_contents("Log/CleanerRoutePlan.log", print_r($page, true) . "\r\n", FILE_APPEND);
    		file_put_contents("Log/CleanerRoutePlan.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
    		file_put_contents("Log/CleanerRoutePlan.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
    		file_put_contents("Log/CleanerRoutePlan.log", print_r($error, true) . "\r\n", FILE_APPEND);

    		$this->response($this->json($error), 201);
    	}

    }
    /*
    *  CLEANER ROUTE PLAN
    */
    private function route_plan_detail(){
    	$datetime = date('Y-m-d H:i:s');
    	$page = "CLEANER ROUTE PLAN";
    	$inputData = json_decode(file_get_contents("php://input"), true);

    		if (isset($inputData['data']) && !empty($inputData['data']) && isset($inputData['data']['cleaner_id']) && !empty($inputData['data']['cleaner_id']) && isset($inputData['data']['sessionId']) && !empty($inputData['data']['sessionId']) && isset($inputData['data']['booking_detail_id']) && !empty($inputData['data']['booking_detail_id'])) {

    		$cleaner_id = trim($inputData['data']['cleaner_id']);
    		$sessionId = trim($inputData['data']['sessionId']);
    		$response = $this->authenticateUser($cleaner_id, $sessionId);
    		if ($response) {
    			$sql_query = "SELECT DTL.booking_id FROM table_booking_details as DTL left join table_booking_register as REG on REG.booking_id = DTL.booking_id where DTL.booking_detail_id='".$inputData['data']['booking_detail_id']."' and REG.status = 'A'";
    			$result = $this->database->query($sql_query) or die($this->mysqli->error . __LINE__);
    			if($result->num_rows > 0 ){
    				$route_sql = "SELECT bd.booking_detail_id,br.booking_id,bd.cleaning_date,bd.area_sq_feets,br.customer_id,c.customer_name,c.customer_number,c.customer_email,c.image,br.cleaning_actual_cost,br.total_cost_paid,br.special_instruction,b.building_name,ca.customer_address,cs.slot_start_time,cs.slot_end_time,alt.booking_status,ut.unit_name,ut.unit_type_id,CITY.city_name,TAR.area_name,CLT.cleaning_type,br.user_area,br.linen_change,br.balcony
                    FROM table_booking_register as br 
    				LEFT join table_customer as c on c.customer_id = br.customer_id 
    				LEFT join table_customer_address ca on br.customer_address_id = ca.customer_address_id 
    				LEFT join table_buildings as b on ca.building_id = b.building_id 
    				LEFT join table_booking_details as bd on bd.booking_id = br.booking_id 
    				LEFT join table_calender_slots as cs on cs.slot_id = bd.slot_id 
    				LEFT JOIN table_booking_allotment as alt on alt.booking_detail_id = bd.booking_detail_id 
    				LEFT JOIN table_booking_room_type_mapping AS rtm ON rtm.booking_id = br.booking_id 
    				LEFT JOIN table_category_type AS ct ON ct.room_category_id = rtm.room_category_id 
    				LEFT JOIN table_unit_type AS ut ON ut.unit_type_id = br.unit_type_id 
                    LEFT JOIN table_cleaning_type AS CLT ON CLT.cleaning_type_id = br.cleaning_type_id 
                    LEFT join table_area AS TAR on TAR.area_id = ca.area_id 
                    LEFT join city AS CITY on CITY.city_id = TAR.city_id
    				where br.status = 'A' and bd.booking_detail_id = '".$inputData['data']['booking_detail_id']."' GROUP BY br.`booking_id`";

    				$routePlan = $this->database->query($route_sql)or die($this->mysqli->error . __LINE__);

    				// ROOM CATEGORY SQL
    				$roomCat_sql1 = "SELECT br.booking_id,rtm.counter,ct.unit_type_id,ut.unit_name,ALOTTMENT.booking_status,CAT.category_name as room_category
	    				FROM table_booking_register AS br 
	    				LEFT JOIN  table_booking_room_type_mapping AS rtm ON rtm.booking_id = br.booking_id 
	    				LEFT JOIN  table_category_type AS ct ON ct.room_category_id = rtm.room_category_id 
                        LEFT JOIN  table_category AS CAT ON CAT.category_id = ct.category_id 
	    				LEFT JOIN table_unit_type AS ut ON ct.unit_type_id = ut.unit_type_id 
	    				LEFT JOIN table_booking_details AS bd ON bd.booking_id = br.booking_id 
	    				LEFT JOIN table_booking_allotment as ALOTTMENT on ALOTTMENT.booking_detail_id = bd.booking_detail_id 
	    				where br.status = 'A' and bd.booking_detail_id = '".$inputData['data']['booking_detail_id']."' GROUP BY br.`booking_id`";

	    			$roomCat1 = $this->database->query($roomCat_sql1)or die($this->mysqli->error . __LINE__);

	    			$roomCatArray = array();
	    			while($row = mysqli_fetch_array($roomCat1)){
	    				$unitType = $row['unit_name'];
	    				$unitTypeId = $row['unit_type_id'];
	    				$string = $row['counter']." ".$row['room_category'];
	    				array_push($roomCatArray, $string);
	    			}
	    			// END ROOM CATEGORY

    				if($routePlan->num_rows > 0 ){
    					$data = array();
    					while($row = mysqli_fetch_assoc($routePlan)){
    						
    						$datas['booking_detail_id'] = $row['booking_detail_id'];
    						$datas['booking_id'] = $row['booking_id'];
    						$datas['customer_id'] = $row['customer_id'];
    						$datas['customer_name'] = $row['customer_name'];
    						$datas['building_name'] = $row['building_name'];
    						$datas['customer_address'] = $row['customer_address'] ." ".$row['area_name']." ".$row['city_name'];
    						$datas['cleaning_date'] = date("d M Y",strtotime($row['cleaning_date']));
    						$datas['slot_start_time'] = date("H:i A",strtotime($row['slot_start_time']));
    						$datas['slot_end_time'] =  date("H:i A",strtotime($row['slot_end_time']));
    						$datas['customer_number'] = $row['customer_number'];
    						$datas['customer_email'] = $row['customer_email'];
    						$datas['area_sq_feets'] = $row['user_area'];
    						$datas['booking_status'] = $row['booking_status'];
    						$datas['total_cost_paid'] = $row['total_cost_paid'];
    						$datas['cleaning_actual_cost'] = $row['cleaning_actual_cost'];
    						$datas['unit_type_id'] = $row['unit_type_id'];
    						$datas['unit_type'] = $row['unit_name'];
                            $datas['special_instructions'] = $row['special_instruction'];
    						if (isset($row['image']) && !empty($row['image'])) {
    							$datas['image'] = self::HOST . "services/customer_image/" . $row['image'];
    						}else{
    							$datas['image'] = '';
    						}
                            $datas['cleaning_type'] = $row['cleaning_type'];
                            $datas['linen_change'] = $row['linen_change'];
                            $datas['balcony'] = $row['balcony'];
                            foreach($roomCatArray as $roomCatArrayTemp){
                             $datas['room_category'] = $roomCatArrayTemp;
                            }
    						$data = $datas;
    					}
                        // echo "<pre>";
                        // print_r($data);
                        // exit;

    					$success = array('status' => "1", "msg" => "Cleaner Route Plan Detail", 'code' => "200", "data" => $data);

    					file_put_contents("Log/CleanerRoutePlan.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
    					file_put_contents("Log/CleanerRoutePlan.log", print_r($page, true) . "\r\n", FILE_APPEND);
    					file_put_contents("Log/CleanerRoutePlan.log", print_r($cleanrParams, true) . "\r\n", FILE_APPEND);
    					file_put_contents("Log/CleanerRoutePlan.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
    					file_put_contents("Log/CleanerRoutePlan.log", print_r($success, true) . "\r\n", FILE_APPEND);
    					$this->response($this->json($success), 200);
    				}else{
    					$error = array('status' => "0", "msg" => "Data not found!", "code" => "209");

    					file_put_contents("Log/CleanerRoutePlan.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
    					file_put_contents("Log/CleanerRoutePlan.log", print_r($page, true) . "\r\n", FILE_APPEND);
    					file_put_contents("Log/CleanerRoutePlan.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
    					file_put_contents("Log/CleanerRoutePlan.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
    					file_put_contents("Log/CleanerRoutePlan.log", print_r($error, true) . "\r\n", FILE_APPEND);

    					$this->response($this->json($error), 209);
    				}
    			}else{
    				$error = array('status' => "0", "msg" => "Data not found!", "code" => "209");

					file_put_contents("Log/CleanerRoutePlan.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
					file_put_contents("Log/CleanerRoutePlan.log", print_r($page, true) . "\r\n", FILE_APPEND);
					file_put_contents("Log/CleanerRoutePlan.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
					file_put_contents("Log/CleanerRoutePlan.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
					file_put_contents("Log/CleanerRoutePlan.log", print_r($error, true) . "\r\n", FILE_APPEND);

					$this->response($this->json($error), 209);
    			}

    		}else{
    			$error = array('status' => "0", "msg" => "No user found!", "code" => "203");

    			file_put_contents("Log/CleanerRoutePlan.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
    			file_put_contents("Log/CleanerRoutePlan.log", print_r($page, true) . "\r\n", FILE_APPEND);
    			file_put_contents("Log/CleanerRoutePlan.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
    			file_put_contents("Log/CleanerRoutePlan.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
    			file_put_contents("Log/CleanerRoutePlan.log", print_r($error, true) . "\r\n", FILE_APPEND);

    			$this->response($this->json($error), 203);
    		}
    	}else{
    		$error = array('status' => "0", "msg" => "Request Blank!", "code" => "201");

    		file_put_contents("Log/CleanerRoutePlan.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
    		file_put_contents("Log/CleanerRoutePlan.log", print_r($page, true) . "\r\n", FILE_APPEND);
    		file_put_contents("Log/CleanerRoutePlan.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
    		file_put_contents("Log/CleanerRoutePlan.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
    		file_put_contents("Log/CleanerRoutePlan.log", print_r($error, true) . "\r\n", FILE_APPEND);

    		$this->response($this->json($error), 201);
    	}
    }


    function upload_pro_pic($base64_string, $output_file, $type) {

        function randimg($cnt) {
            $img = str_shuffle('abcefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890_');
            return substr($img, 0, $cnt);
        }
        $num = randimg(10);
        $image_url = "img_" . $num . "." . $type;
        $upload = $output_file . $image_url;
        $ifp = fopen($upload, "wb");
        fwrite($ifp, base64_decode($base64_string));
        fclose($ifp);
        return $image_url;
    }

    /*
     *  TO CLEAN A STRING
    */


    function clean($string) {

       $string = str_replace(' ', '', $string);
       $string = str_replace('-', '', $string); // Replaces all spaces with hyphens.

       return preg_replace('/[^A-Za-z0-9.\-]/', '', $string); // Removes special chars.
    }

    /*
     *  Encode array into JSON
    */

    private function json($data) {
    	if (is_array($data)) {
    		return json_encode($data);
    	}
    }

    private function Apistatus () 
    {
        // 1 for APIs Active -- 0 for Inactive
    	$status = 1;
    	$success = array('status' => 1, "api_status" => $status);
    	$this->response($this->json($success), 200);
    }

    /*
     *  Upload images before and after assignment 
    */
    private function upload_assignment_images() {
        $datetime = date('Y-m-d H:i:s');
        $page = "UPLOAD ASSIGNMENT IMAGES";
        //$inputData = json_decode(file_get_contents("php://input"), true);
        if(!isset($_POST)){
            $error = array('status' => "0", "msg" => "Request Blank!", "code" => "201");
            file_put_contents("Log/CleanerAssignmentImage.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/CleanerAssignmentImage.log", print_r($page, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/CleanerAssignmentImage.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/CleanerAssignmentImage.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/CleanerAssignmentImage.log", print_r($error, true) . "\r\n", FILE_APPEND);

            $this->response($this->json($error), 201);
        }
        $inputData['data'] = $_POST;

        if (isset($inputData['data']) && !empty($inputData['data']) && isset($inputData['data']['cleaner_id']) && !empty($inputData['data']['cleaner_id']) && isset($inputData['data']['sessionId']) && !empty($inputData['data']['sessionId']) && isset($inputData['data']['booking_id']) && !empty($inputData['data']['booking_id']) && isset($_FILES['images']) && !empty($_FILES['images']) && isset($inputData['data']['type']) && !empty($inputData['data']['type']))
        {
            $cleaner_id = trim($inputData['data']['cleaner_id']);
            $sessionId = trim($inputData['data']['sessionId']);
            $response = $this->authenticateUser($cleaner_id, $sessionId);
            if ($response) {
                $sql_query = "SELECT booking_id FROM table_booking_register where booking_id='".$inputData['data']['booking_id']."'";
                $result = $this->database->query($sql_query) or die($this->mysqli->error . __LINE__);
                if ($result->num_rows > 0) {
                    for ($i = 0; $i < count($_FILES['images']['name']); $i++) {
                        $file_name = $this->clean($_FILES['images']['name'][$i]);
                        $date=date('dmY-His');
                        $target_folder = '../images/assignment/'; 
                        $assignment_image=rand(1,9).$date.basename( str_replace(" ","", $file_name));
                        $upload_image = $target_folder.$assignment_image;
                        $status = "A";
                        //$types = array('image/jpeg', 'image/gif', 'image/png', 'image/jpg');
                        if(move_uploaded_file($_FILES['images']['tmp_name'][$i], $upload_image)) 
                        {               
                            $insertQuery = "INSERT INTO table_assignment_images(booking_id,image_url,before_after,status) VALUES ('" . $inputData['data']['booking_id'] . "','" . $assignment_image . "', '" . $inputData['data']['type'] . "','" . $status . "')";
                            $this->database->query($insertQuery);
                        } else {
                            $error = array('status' => "0", "msg" => "Image is not uploaded!", "code" => "302");
                            $this->response($this->json($error), 302);
                        }
                    }
                    
                    if($inputData['data']['type']=="before")
                    {
                        $update_status = "BC";
                    }
                    else
                    {
                        $update_status = "CE";
                    }

                    $update_sql_query = "UPDATE table_booking_allotment 
                                         join table_booking_details on table_booking_details.booking_detail_id = table_booking_allotment.booking_detail_id
                                         SET booking_status='$update_status',last_update_date = '$datetime'
                                         where table_booking_details.booking_id='".$inputData['data']['booking_id']."'";
                    $this->database->query($update_sql_query) or die($this->mysqli->error . __LINE__);

                    $sql_query_new = "SELECT assignment_image_id,booking_id,image_url,before_after,status FROM table_assignment_images where booking_id='".$inputData['data']['booking_id']."' and before_after='".$inputData['data']['type']."'";
                    $result_new = $this->database->query($sql_query_new) or die($this->mysqli->error . __LINE__);

                    if ($result_new->num_rows > 0) {
                        $i=-1;
                        while($row = mysqli_fetch_assoc($result_new))
                        {
                            $i++;

                            $response_data[$i]['assignment_image_id']=(string)$row['assignment_image_id'];
                            $response_data[$i]['booking_id'] = (string)$row['booking_id'];
                            $response_data[$i]['image_url'] = self::HOST . "images/assignment/" . $row['image_url'];
                            $response_data[$i]['before_after'] = (string)$row['before_after'];
                            $response_data[$i]['status'] = (string)$row['status'];

                        }
                        $success = array("status" => "1", "msg" => "Assignment Images uploaded successfully.", "code" => "200",'data'=>$response_data);
                        file_put_contents("Log/CleanerAssignmentImage.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
                        file_put_contents("Log/CleanerAssignmentImage.log", print_r($page, true) . "\r\n", FILE_APPEND);
                        file_put_contents("Log/CleanerAssignmentImage.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
                        file_put_contents("Log/CleanerAssignmentImage.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
                        file_put_contents("Log/CleanerAssignmentImage.log", print_r($success, true) . "\r\n", FILE_APPEND);
                        $this->response($this->json($success), 200);
                    }
                } else {
                    $error = array('status' => "0", "msg" => "Booking does not exist!", "code" => "203");

                    file_put_contents("Log/CleanerAssignmentImage.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
                    file_put_contents("Log/CleanerAssignmentImage.log", print_r($page, true) . "\r\n", FILE_APPEND);
                    file_put_contents("Log/CleanerAssignmentImage.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
                    file_put_contents("Log/CleanerAssignmentImage.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
                    file_put_contents("Log/CleanerAssignmentImage.log", print_r($error, true) . "\r\n", FILE_APPEND);

                    $this->response($this->json($error), 201);
                }
            } else {
                $error = array('status' => "0", "msg" => "No user found!", "code" => "203");
                
                file_put_contents("Log/CleanerAssignmentImage.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/CleanerAssignmentImage.log", print_r($page, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/CleanerAssignmentImage.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/CleanerAssignmentImage.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/CleanerAssignmentImage.log", print_r($error, true) . "\r\n", FILE_APPEND);

                $this->response($this->json($error), 203);
            }
        } else {
            $error = array('status' => "0", "msg" => "Request Blank!", "code" => "201");

            file_put_contents("Log/CleanerAssignmentImage.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/CleanerAssignmentImage.log", print_r($page, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/CleanerAssignmentImage.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/CleanerAssignmentImage.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/CleanerAssignmentImage.log", print_r($error, true) . "\r\n", FILE_APPEND);

            $this->response($this->json($error), 201);
        }
    }

    /*
     *  Get images before and after assignment 
    */
    private function get_assignment_images() {
        $datetime = date('Y-m-d H:i:s');
        $page = "UPLOAD ASSIGNMENT IMAGES";
        $inputData = json_decode(file_get_contents("php://input"), true);
        if (isset($inputData['data']) && !empty($inputData['data']) && isset($inputData['data']['cleaner_id']) && !empty($inputData['data']['cleaner_id']) && isset($inputData['data']['sessionId']) && !empty($inputData['data']['sessionId']) && isset($inputData['data']['booking_id']) && !empty($inputData['data']['booking_id']) && isset($inputData['data']['type']) && !empty($inputData['data']['type'])) {
            $cleaner_id = trim($inputData['data']['cleaner_id']);
            $sessionId = trim($inputData['data']['sessionId']);
            $response = $this->authenticateUser($cleaner_id, $sessionId);
            if ($response) {
                $sql_query = "SELECT booking_id FROM table_booking_register where booking_id='".$inputData['data']['booking_id']."'";
                $result = $this->database->query($sql_query) or die($this->mysqli->error . __LINE__);
                if ($result->num_rows > 0) {
                    $sql_query_new = "SELECT assignment_image_id,booking_id,image_url,before_after,status FROM table_assignment_images where booking_id='".$inputData['data']['booking_id']."' and before_after='".$inputData['data']['type']."'";
                    $result_new = $this->database->query($sql_query_new) or die($this->mysqli->error . __LINE__);

                    if ($result_new->num_rows > 0) {
                        $i=-1;
                        while($row = mysqli_fetch_assoc($result_new))
                        {
                            $i++;

                            $response_data[$i]['assignment_image_id']=(string)$row['assignment_image_id'];
                            $response_data[$i]['booking_id'] = (string)$row['booking_id'];
                            $response_data[$i]['image_url'] = self::HOST . "images/assignment/" . $row['image_url'];
                            $response_data[$i]['before_after'] = (string)$row['before_after'];
                            $response_data[$i]['status'] = (string)$row['status'];

                        }
                        $success = array("status" => "1", "msg" => "Assignment's Images found successfully.", "code" => "200",'data'=>$response_data);
                        file_put_contents("Log/CleanerAssignmentImage.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
                        file_put_contents("Log/CleanerAssignmentImage.log", print_r($page, true) . "\r\n", FILE_APPEND);
                        file_put_contents("Log/CleanerAssignmentImage.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
                        file_put_contents("Log/CleanerAssignmentImage.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
                        file_put_contents("Log/CleanerAssignmentImage.log", print_r($success, true) . "\r\n", FILE_APPEND);
                        $this->response($this->json($success), 200);
                    } else {
                        $error = array('status' => "0", "msg" => "No assignment's images found!", "code" => "203");
                
                        file_put_contents("Log/CleanerAssignmentImage.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
                        file_put_contents("Log/CleanerAssignmentImage.log", print_r($page, true) . "\r\n", FILE_APPEND);
                        file_put_contents("Log/CleanerAssignmentImage.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
                        file_put_contents("Log/CleanerAssignmentImage.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
                        file_put_contents("Log/CleanerAssignmentImage.log", print_r($error, true) . "\r\n", FILE_APPEND);

                        $this->response($this->json($error), 203);
                    }
                } else {
                    $error = array('status' => "0", "msg" => "Booking does not exist!", "code" => "203");

                    file_put_contents("Log/CleanerAssignmentImage.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
                    file_put_contents("Log/CleanerAssignmentImage.log", print_r($page, true) . "\r\n", FILE_APPEND);
                    file_put_contents("Log/CleanerAssignmentImage.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
                    file_put_contents("Log/CleanerAssignmentImage.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
                    file_put_contents("Log/CleanerAssignmentImage.log", print_r($error, true) . "\r\n", FILE_APPEND);

                    $this->response($this->json($error), 201);
                }
            } else {
                $error = array('status' => "0", "msg" => "No user found!", "code" => "203");
                
                file_put_contents("Log/CleanerAssignmentImage.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/CleanerAssignmentImage.log", print_r($page, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/CleanerAssignmentImage.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/CleanerAssignmentImage.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/CleanerAssignmentImage.log", print_r($error, true) . "\r\n", FILE_APPEND);

                $this->response($this->json($error), 203);
            }
        } else {
            $error = array('status' => "0", "msg" => "Request Blank!", "code" => "201");

            file_put_contents("Log/CleanerAssignmentImage.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/CleanerAssignmentImage.log", print_r($page, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/CleanerAssignmentImage.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/CleanerAssignmentImage.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/CleanerAssignmentImage.log", print_r($error, true) . "\r\n", FILE_APPEND);

            $this->response($this->json($error), 201);
        }
    }


}// END STATUS




// Initiiate Library

$api = new cleanerServices;
$api->processApi();

?>