<?php

require_once("Rest.inc.php");
// require_once("../classes/FCM.php");
//require_once("gcm.php");
// error_reporting(E_ALL |E_ERROR | E_WARNING | E_PARSE | E_NOTICE);	
// error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING);
header("Access-Control-Allow-Origin: *");
define("TEXTLOCAL_API_KEY", "gsZbshodVdA-6nNSs0bAdEPiWYn0jMaQRWVARe1EWu");
//echo "fsd";die;	
class customerServices extends REST {

    const HOST = "http://quytech.in/ecleaning/";
    const DB_SERVER = "localhost";
    const DB_USER = "ecleaning_1";
    const DB_PASSWORD = "quytech1234";
    const DB = "ecleaning_db";

    private $db = NULL;
    private $mysqli = NULL;
    private $perPageItem;
    private static $instance = null;


    public function __construct() {

        parent::__construct();    // Init parent contructor
        //$this->dbConnect();					
        $this->perPageItem = 10;
        $this->database = customerServices::getInstance(); // Initiate Database connection
    }

    /*
     *  Connect to Database
     */

    private static function getInstance() {
        if (self::$instance == null) {
            self::$instance = new mysqli(self::DB_SERVER, self::DB_USER, self::DB_PASSWORD, self::DB);
        }
        return self::$instance;
    }

    // private function dbConnect() {
    // 		$this->dbconnect->mysqli = new mysqli(self::DB_SERVER, self::DB_USER, self::DB_PASSWORD, self::DB);
    // 	}

    /*
     * Dynmically call the method based on the query string
     */
    public function processApi() {
        //echo $_REQUEST['action'];exit;
        if (isset($_REQUEST['action'])) {
            $func = strtolower(trim(str_replace("/", "", $_REQUEST['action'])));
            //echo $func;exit;
            if ((int) method_exists($this, $func) > 0)
                $this->$func();
            else
                $error = array('status' => "Failed", "msg" => "Request Blank");
            $this->response($this->json($error), 201); // If the request is blank.
        } else {
            $error = array('status' => "Failed", "msg" => "Request Blank");
            $this->response($this->json($error), 201); // If the request is blank.
        }
    }

    /*     * ************************************************************************
     * DESC : To Check Required Fields
     * AUTHOR : Yogesh
     * Created : 2017-03-09
     *
     * */

    private function required_field($post, $required_fields = array()) {
        $response = array();
        foreach ($required_fields as $key) {
            if (!isset($post[$key])) {
                $response['status'] = 0;
                $response['error'][] = 'invalid post (' . $key . ' not set)';
            } else {
                if ($post[$key] === '') {
                    $response['status'] = 0;
                    $response['error'][] = 'invalid post (' . $key . ' empty)';
                }
            }
        }

        if (isset($response['error'])) {
            $this->response($this->json($response), 214);
            die();
        }
    }

    private function authenticateUser($customerId, $sessionId) {

        if (!empty($customerId) && !empty($sessionId)) {
            $sql = "SELECT tc.customer_id FROM table_customer as tc  WHERE tc.customer_id='" . $customerId . "' AND tc.session_id='" . $sessionId . "' ";
            $checkUser = $this->database->query($sql);
            if ($checkUser->num_rows > 0) {
                $user_auth_status = true;
            } else {
                $user_auth_status = false;
            }
        } else {
            $user_auth_status = false;
        }

        return $user_auth_status;
    }

    private function login() {
//echo "dd";die;
        $customerParams = json_decode(file_get_contents("php://input"), true);
        //	print_r($customerParams);exit;	
        $datetime = date('Y-m-d H:i:s');
        $page = "Login Customer";
        //$userDetails = json_decode('{"data":{"email_id":"arvindks986@gmail.com","password":12345,"login_with":"fb","GCMID":" ","ios_device_token":""}}',true);	
        //print_r($customerParams);exit;
        if (!isset($customerParams['data']) || empty($customerParams['data']) || !isset($customerParams['data']['userName']) || empty($customerParams['data']['userName']) || !isset($customerParams['data']['password']) && empty($customerParams['data']['password']) || !isset($customerParams['data']['deviceId']) || !isset($customerParams['data']['gcmId']) || !isset($customerParams['data']['loginType'])) {

            $error = array('status' => "0", "msg" => "Request Blank", 'code' => "201");

            file_put_contents("Log/customerLogin.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/customerLogin.log", print_r($page, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/customerLogin.log", print_r($customerParams, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/customerLogin.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/customerLogin.log", print_r($error, true) . "\r\n", FILE_APPEND);
            $this->response($this->json($error), 201);
        }


        $username = $customerParams['data']['userName'];
        $password = md5(trim($customerParams['data']['password']));
    
        // Check user exists or not

        $getcustomerDetails = "SELECT tc.*,tb.building_name,tb.lat,tb.lng FROM table_web_users as twu left join table_customer as tc on tc.customer_id = twu.customer_id LEFT JOIN table_buildings as tb on tb.building_id=tc.building_id WHERE twu.username ='" . $username . "' AND twu.password = '" . $password . "' AND twu.status='A' AND tc.status='A' AND twu.customer_id!='' ";
        //print_r($getcustomerDetails);die;
        $customerdata = $this->database->query($getcustomerDetails);
        //print_r($customerdata);die;
        //print_r($customerdata);exit;
        if ($customerdata->num_rows > 0) {
            $response_data = array();
            // Create Unique Authentication Key
            $auth_token = mt_rand(10000000, 99999999);

            $cusdata = mysqli_fetch_assoc($customerdata);
            //print_r($cusdata);exit;
            $updateAuthKey = "UPDATE table_customer SET session_id = '" . $auth_token . "',last_update_date = '" . date('Y-m-d H:i:s') . "',gcm_regid='" . $customerParams['data']['gcmId'] . "',login_type='" . $customerParams['data']['loginType'] . "' WHERE customer_id='" . $cusdata['customer_id'] . "'";
            $AuthKey = $this->database->query($updateAuthKey) or die($this->mysqli->error . __LINE__);

            /*             * **Check customer device id already exists*** */
            $checkDeviceQuery = "Select * from table_devices as td where td.customer_id=" . $cusdata['customer_id'];
            $resultDevice = $this->database->query($checkDeviceQuery);
            if ($resultDevice->num_rows > 0) {
                $deleteQuery = "Delete from table_devices where customer_id=" . $cusdata['customer_id'];

                $this->database->query($deleteQuery);
            }

            /*             * *End checking device id***** */

            /*             * ****Insert device id into device table** */
            if (!empty($customerParams['data']['deviceId'])) {

                $deviceIdQuery = "INSERT INTO table_devices(account_id,customer_id,device_id,created_date,last_update_date) VALUES ('" . $account_id . "','" . $cusdata['customer_id'] . "', '" . $customerParams['data']['deviceId'] . "','" . $created_date . "','" . $last_update_date . "')";


                $this->database->query($deviceIdQuery);
            }

            /*             * ************ */

            $response_data['sessionId'] = (string)$auth_token;
            $response_data['customerId'] = (string)$cusdata['customer_id'];
            if (empty($cusdata['location'])) {
                $response_data['location'] = '';
            }
            if (empty($cusdata['lat'])) {
                $response_data['lat'] = '';
            }
            if (empty($cusdata['lng'])) {
                $response_data['lng'] = '';
            }

             $response_data['otp_verified'] = 1;
            //print_r($response_data);die;
            $success = array('status' => "1", "msg" => "User Logged In Successfully.", 'code' => "200", "data" => $response_data);

            file_put_contents("Log/customerLogin.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/customerLogin.log", print_r($page, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/customerLogin.log", print_r($customerParams, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/customerLogin.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/customerLogin.log", print_r($success, true) . "\r\n", FILE_APPEND);

            $this->response($this->json($success), 200);
        } else {

            $getcustomerDetails = "SELECT tc.*,tb.building_name,tb.lat,tb.lng FROM table_web_users as twu left join table_customer as tc on tc.customer_id = twu.customer_id LEFT JOIN table_buildings as tb on tb.building_id=tc.building_id WHERE twu.username ='" . $username . "' AND twu.password = '" . $password . "' AND twu.status='I' AND tc.status='I' AND twu.customer_id!='' ";



            $customerdata = $this->database->query($getcustomerDetails);
            

            if($customerdata->num_rows > 0){

             while ($row = mysqli_fetch_assoc($customerdata)) {
                $data = $row;
            }
            $otp = 12345;
            $data['otp'] = $otp;
           
            $customer_number = $data['customer_number'];
            $data['otp_verified'] = 0;

            $customerTimeUpdate = " UPDATE table_customer SET last_update_date = '" . date('Y-m-d H:i:s') . "' where customer_id='".$data['customer_id']."'";
            $this->database->query($customerTimeUpdate);
            

            $smsRes = $this->sendSms(TEXTLOCAL_API_KEY,$customer_number, $otp);

            $success = array('status' => "1", "msg" => "User Exits and OTP has been sent.", "code" => "200", "data" => $data);


            file_put_contents("Log/customerLogin.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/customerLogin.log", print_r($page, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/customerLogin.log", print_r($customerParams, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/customerLogin.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/customerLogin.log", print_r($success, true) . "\r\n", FILE_APPEND);

            $this->response($this->json($success), 200);



        }else{

                // If Username is invalid
            $error = array('status' => 0, "msg" => "Invalid username Or password", 'code' => "203");

            file_put_contents("Log/customerLogin.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/customerLogin.log", print_r($page, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/customerLogin.log", print_r($customerParams, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/customerLogin.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/customerLogin.log", print_r($error, true) . "\r\n", FILE_APPEND);

            $this->response($this->json($error), 203);

        }


    }
    }

// End of function



    /* * ************************************************************************
     * DESC : Register Customer
     * AUTHOR : Ayush
     * Created : 2017-11-21
     *
     * */

    private function register() {

        $datetime = date('Y-m-d H:i:s');
        $page = "Register Customer";
        $customerParams = json_decode(file_get_contents("php://input"), true);

        if (!isset($customerParams['data']) || empty($customerParams['data']) || !isset($customerParams['data']['customerName']) || empty($customerParams['data']['customerName']) || !isset($customerParams['data']['customerEmail']) || empty($customerParams['data']['customerEmail']) || !isset($customerParams['data']['customerNumber']) || empty($customerParams['data']['customerNumber']) || empty($customerParams['data']['country_code'])  || !isset($customerParams['data']['password']) || empty($customerParams['data']['password']) || !isset($customerParams['data']['deviceId']) || !isset($customerParams['data']['gcmId']) || !isset($customerParams['data']['loginType'])) {

            $error = array('status' => "0", "msg" => "Request Blank", 'code' => "201");

            file_put_contents("Log/customerRegister.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/customerRegister.log", print_r($page, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/customerRegister.log", print_r($customerParams, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/customerRegister.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/customerRegister.log", print_r($error, true) . "\r\n", FILE_APPEND);
            $this->response($this->json($error), 201);
        }

        $customerData = $customerParams['data'];
        $customer_email = trim($customerData['customerEmail']);

        $sql = 'SELECT customer_id FROM table_customer WHERE customer_email ="' . $customer_email . '"';
        $checkUser = $this->database->query($sql);

        if ($checkUser->num_rows > 0) {

            $error = array('status' => "0", "msg" => "Email already exist.", 'code' => "212");


            file_put_contents("Log/customerRegister.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/customerRegister.log", print_r($page, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/customerRegister.log", print_r($customerParams, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/customerRegister.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/customerRegister.log", print_r($error, true) . "\r\n", FILE_APPEND);

            $this->response($this->json($error), 212);
        }

        $numbersql = 'SELECT customer_id FROM table_customer WHERE customer_number ="' . $customerData['customerNumber'] . '"';
        $checkPhone = $this->database->query($numbersql);

        if ($checkPhone->num_rows > 0) {

            $error = array('status' => "0", "msg" => "This phone number already registered.Please try another one!.", 'code' => "212");


            file_put_contents("Log/customerRegister.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/customerRegister.log", print_r($page, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/customerRegister.log", print_r($customerParams, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/customerRegister.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/customerRegister.log", print_r($error, true) . "\r\n", FILE_APPEND);

            $this->response($this->json($error), 212);
        }

        $username = trim($customerData['customerEmail']);
        $sql = 'SELECT web_user_id FROM table_web_users WHERE username ="' . $username . '"';
        $checkusername = $this->database->query($sql);

        if ($checkusername->num_rows > 0) {

            $error = array('status' => "0", "msg" => "Username already exist.", 'code' => "212");


            file_put_contents("Log/customerRegister.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/customerRegister.log", print_r($page, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/customerRegister.log", print_r($customerParams, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/customerRegister.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/customerRegister.log", print_r($error, true) . "\r\n", FILE_APPEND);

            $this->response($this->json($error), 212);
        }


        $type = "png";
        if (isset($customerData['image']) && !empty($customerData['image'])) {
            $image_name = array();


            $dir = 'customer_image/';
            if (!is_dir($dir) && mkdir($dir, 0777, true)) {
                chmod('./customer_image/', 0777);
                chmod($dir, 0777);
            }
            if (is_dir($dir)) {

                $image = $this->upload_pro_pic($customerData['image'], $dir, $type);
            }
        } else {
            $image = '';
        }

        $customer_name = trim($customerData['customerName']);
        $customer_email = trim($customerData['customerEmail']);
        $country_code = trim($customerData['country_code']);
        $password = md5(trim($customerData['password']));
        $username = trim($customerData['customerEmail']);
        $customer_number = trim($customerData['customerNumber']);
        $status = 'I';
        $account_id = 1;
        $last_update_date = date('Y-m-d H:i:s');
        $created_date = date('Y-m-d H:i:s');
        $end_date = date('Y-m-d H:i:s');
        $start_date = date('Y-m-d H:i:s');
        $gcmId = trim($customerParams['data']['gcmId']);
        $login_type = trim($customerParams['data']['loginType']);
        $auth_token = mt_rand(10000000, 99999999);
        $otp = mt_rand(10000, 99999);
        $otp = 12345;
        try {
            $addCustomerSql = "INSERT INTO table_customer (customer_name,customer_email,country_code,customer_number,created_date, last_update_date,status,account_id,session_id,image,gcm_regid,login_type,otp) VALUES ('" . $customer_name . "', '" . $customer_email . "','".$country_code."','" . $customer_number . "', '" . $created_date . "', '" . $last_update_date . "','" . $status . "','" . $account_id . "','" . $auth_token . "','" . $image . "','" . $gcmId . "','" . $login_type . "','".$otp."')";

            $this->database->query($addCustomerSql) or die($this->database->error . __LINE__);
            $customer_id = $this->database->insert_id;

            // echo $customer_id;exit;

            // Add customer details

            if ($customer_id > 0) {

                $customerwebuserSql = "INSERT INTO table_web_users (account_id,username,password,email_id, customer_id, user_type, start_date,end_date,status) VALUES ('" . $account_id . "','" . $username . "', '" . $password . "', '" . $customer_email . "', '" . $customer_id . "', 4, '" . $start_date . "','" . $end_date . "','" . $status . "')";


                $this->database->query($customerwebuserSql) or die($this->mysqli->error . __LINE__);

                /*                 * ****Insert device id into device table** */
                if (!empty($customerParams['data']['deviceId'])) {

                    $deviceIdQuery = "INSERT INTO table_devices(account_id,customer_id,device_id,created_date,last_update_date) VALUES ('" . $account_id . "','" . $customer_id . "', '" . $customerParams['data']['deviceId'] . "','" . $created_date . "','" . $last_update_date . "')";


                    $this->database->query($deviceIdQuery);
                }

                $smsRes = $this->sendSms(TEXTLOCAL_API_KEY,$customer_number, $otp);

            }
            // $this->database->commit();

            $userProfileSql = " SELECT tc.customer_id,tc.session_id,tc.otp FROM table_customer AS tc LEFT JOIN table_web_users AS twu ON twu.customer_id = tc.customer_id WHERE tc.status = 'I' AND tc.customer_id = '" . $customer_id . "'";

            $cusQuery = $this->database->query($userProfileSql);
            while ($row = mysqli_fetch_assoc($cusQuery)) {
                $data = $row;
            }
            $otp = 12345;
            // $data['otp'] = $otp;


            $success = array('status' => "1", "msg" => "User Information saved and OTP has been sent.", "code" => "200", "data" => $data);


            file_put_contents("Log/customerRegister.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/customerRegister.log", print_r($page, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/customerRegister.log", print_r($customerParams, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/customerRegister.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/customerRegister.log", print_r($success, true) . "\r\n", FILE_APPEND);

            $this->response($this->json($success), 200);
        } catch (Exception $e) {
            $this->database->rollback();
            $error = array('status' => "0", "msg" => $e->getMessage(), 'code' => "201");
            // If the request is blank.

            file_put_contents("Log/customerRegister.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/customerRegister.log", print_r($page, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/customerRegister.log", print_r($customerParams, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/customerRegister.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/customerRegister.log", print_r($error, true) . "\r\n", FILE_APPEND);

            $this->response($this->json($error), 201);
        }
    }


    private function registerViaSocialPlatform() 
    {

        $datetime = date('Y-m-d H:i:s');
        $page = "Register Via Social Platform";
        $customerParams = json_decode(file_get_contents("php://input"), true);




        if (isset($customerParams['data']['access_token']) && !empty($customerParams['data']['access_token']) && $customerParams['data']['login_type']=='Facebook') {
            
            $access_token = $customerParams['data']['access_token'];

            try {

                require_once __DIR__ . '/SDK/Facebook/autoload.php'; // change path as needed


                // $fb = new \Facebook\Facebook([
                //     'app_id' => '509693559441223', //'331660494288258', //'230895244124455',
                //     'app_secret' => 'b99087573d95ca56faad6753cb87a33c', //'ed5ca5b3b4f8a2eed84d603fe5910b5e', //'144488d7ec41cf2d8b24bc4e33c0d4b3',
                //     'default_graph_version' => 'v2.12',
                //         //'default_access_token' => '{access-token}', // optional
                // ]);
                $fb = new \Facebook\Facebook([
                    'app_id' => '1525141504287838', //'331660494288258', //'230895244124455',
                    'app_secret' => '2e2735e85e3f6ce088dc73abd821bd5e', //'ed5ca5b3b4f8a2eed84d603fe5910b5e', //'144488d7ec41cf2d8b24bc4e33c0d4b3',
                    'default_graph_version' => 'v2.12',
                        //'default_access_token' => '{access-token}', // optional
                ]);

                try {
// Get the \Facebook\GraphNodes\GraphUser object for the current user.
// If you provided a 'default_access_token', the '{access-token}' is optional.
                    $response = $fb->get('/me?fields=id,name,email,birthday,gender,first_name,last_name', $access_token);
                    
                } catch (\Facebook\Exceptions\FacebookResponseException $e) {
// When Graph returns an error
// echo 'Graph returned an error: ' . $e->getMessage();
// exit;
                    $error = array('status' => "0", "msg" => 'Graph returned an error: ' . $e->getMessage(), 'code' => "400");
                    $this->response($this->json($error), 400);
                } catch (\Facebook\Exceptions\FacebookSDKException $e) {
// When validation fails or other local issues
// echo 'Facebook SDK returned an error: ' . $e->getMessage();
// exit;
                    $error = array('status' => "0", "msg" => 'Facebook SDK returned an error: ' . $e->getMessage(), 'code' => "400");
                    $this->response($this->json($error), 400);
                }





                $me = $response->getGraphUser();
                $username = "";

                

                if (isset($me['name'])) {
                    $profileData['full_name'] = $me['name'];
                }

                if (isset($me['first_name'])) {
                    $profileData['first_name'] = $me['first_name'];
                }

                if (isset($me['last_name'])) {
                    $profileData['last_name'] = $me['last_name'];
                }

                if (isset($me['birthday'])) {
//return  $me['birthday'];   $date = explode(" ", json_decode($me['birthday'])->date)[0];
// die;
                    $profileData['birthday'] = $me->getBirthday()->format('Y-m-d');
                }

                if (isset($me['email'])) {
                    $username = trim($me['email']);
                    $profileData['email_id'] = $me['email'];
                }

                if (isset($me['gender'])) {
                    $profileData['gender'] = $me['gender'];
                }

// Check if user already exists
                if ($username != "") {
                    $username = $profileData['email_id'];

//$sql = 'SELECT `loco_user_id` FROM table_web_users WHERE username ="'.$username.'"';

                    // $cols = array("twu.loco_user_id as loco_user_id");
                    // $this->database->join('table_loco_users tlu', 'tlu.loco_user_id = twu.loco_user_id', "LEFT");
                    // $this->database->where('twu.username', $username);
                    // $this->database->orWhere('tlu.facebook_user_id', $me['id']);
                    // $checkUser = $this->database->get('table_web_users twu', null, $cols);


                    $checkUser = "SELECT * from table_customer as tc left join table_web_users as twu on tc.customer_id = twu.customer_id where twu.username= '".$username."' or tc.facebook_user_id='".$me['id']."'";
                   

                    $result = $this->database->query($checkUser) or die($this->mysqli->error . __LINE__);
                    
                    if($result->num_rows > 0) 
                    {
                        $userData = mysqli_fetch_assoc($result);
                    }
                    
                    if ($userData['customer_id'] > 0) {
                        // $fieldSession = array("session_id");
                        // $this->database->where("customer_id", $userData['loco_user_id']);
                        $selectSessionId = "SELECT * from table_customer where session_id = '' and customer_id = '".$userData['customer_id']."'";

                        $sess = $this->database->query($selectSessionId);
                        // $checkSession = $this->database->get("table_loco_users", null, $fieldSession);
                        $dataUser = mysqli_fetch_assoc($sess);
                        if ($dataUser['session_id'] == '') {

                            // $updateSession = array('session_id' => mt_rand(10000000, 99999999));
                            // $this->database->where('loco_user_id', $checkUser[0]['loco_user_id']);
                            // $this->database->update("table_loco_users", $updateSession);

                            $updateSession = "UPDATE table_customer set session_id = '".mt_rand(10000000, 99999999)."' where customer_id = '".$dataUser['customer_id']."'";

                            $updsess = $this->database->query($updateSession) or die($this->mysqli->error . __LINE__);
                        }
                        
                        // $fieldsRet = array('loco_user_first_name', 'loco_user_last_name',
                        //     'loco_user_contact_no', 'loco_user_app_language', 'loco_user_tagline',
                        //     'loco_user_picture_privacy', 'loco_user_country_id', 'loco_user_city_id',
                        //     'session_id', 'loco_user_id', 'profile_pic', 'loco_user_birthday',
                        //     'loco_user_email_id', 'loco_user_gender', 'gcm_regid', 'lat', 'lng', 'loco_user_status');
                        $getUserProfile = "Select * from table_customer where customer_id = '".$userData['customer_id']."'";

                        $UserProfile = $this->database->query($getUserProfile) or die($this->mysqli->error . __LINE__);
                        // $getRecord = array();
                        // $this->database->where('loco_user_id', $checkUser[0]['loco_user_id']);
                        // $getRecord = $this->database->get('table_loco_users', null, $fieldsRet);

                        $getRecord = mysqli_fetch_assoc($UserProfile);

                        $success = array('status' => "1", "msg" => "User Logged in Successfully.", "code" => "200", "data" => $getRecord);



                        $this->response($this->json($success), 200);

                        // $response_data['loco_user_id'] = $getRecord[0]['loco_user_id'];
                        // $response_data['sessionId'] = $getRecord[0]['session_id'];
                        // $response_data['loginType'] = 'facebook';
                        // $response_data['full_name'] = $getRecord[0]['loco_user_first_name'] . ' ' . $getRecord[0]['loco_user_last_name'];
                        // $response_data['first_name'] = $getRecord[0]['loco_user_first_name'];
                        // $response_data['last_name'] = $getRecord[0]['loco_user_last_name'];
                        // $response_data['country_id'] = $getRecord[0]['loco_user_country_id'];
                        // $response_data['city_id'] = $getRecord[0]['loco_user_city_id'];
                        // $response_data['email_id'] = $getRecord[0]['loco_user_email_id'];
                        // $response_data['birthday'] = date('Y-m-d', strtotime($getRecord[0]['loco_user_birthday']));
                        // $response_data['gender'] = $getRecord[0]['loco_user_gender'];
                        // $response_data['contact_no'] = $getRecord[0]['loco_user_contact_no'];
                        // $response_data['user_picture_privacy'] = $getRecord[0]['loco_user_picture_privacy'];
                        // $response_data['profile_pic'] = $getRecord[0]['profile_pic'];
                        // $response_data['tagline'] = $getRecord[0]['loco_user_tagline'];
                        // $response_data['loco_user_app_language'] = $getRecord[0]['loco_user_app_language'];
                        // $response_data['languages'] = $getRecord[0]['languages'];
                        // $response_data['gcmId'] = $getRecord[0]['gcm_regid'];
                        // $response_data['lat'] = $getRecord[0]['lat'];
                        // $response_data['lng'] = $getRecord[0]['lng'];
                        // $response_data['user_status'] = $getRecord[0]['loco_user_status'];
                        // $response_data['deviceId'] = '';

                        // $success = array('status' => "1", "msg" => "User Logged In Successfully.", 'code' => "200", "data" => $getRecord);

                        // $createLogInputs['response'] = $success;
                        // $this->createLog($createLogInputs);

                        // $this->response($this->json($this->utf8ize($success)), 200);
                    }
                    else if($userData['customer_id'] <= 0)
                    {
                        $customer_name = trim($me['name']);
                        $customer_email = trim($me['email']);
                        $facebook_user_id = trim($me[id]);
                        // $password = md5(12345678));
                        $username = trim($me['email']);
                        // $customer_number = trim($customerData['customerNumber']);
                        $status = 'A';
                        $account_id = 1;
                        $last_update_date = date('Y-m-d H:i:s');
                        $created_date = date('Y-m-d H:i:s');
                        $end_date = date('Y-m-d H:i:s');
                        $start_date = date('Y-m-d H:i:s');
                        // $gcmId = trim($customerParams['data']['gcmId']);
                        $login_type = "Facebook";
                        $auth_token = mt_rand(10000000, 99999999);
                        // $otp = mt_rand(10000, 99999);
                        // $otp = 12345;
                        try {
                            $addCustomerSql = "INSERT INTO table_customer (customer_name,customer_email,facebook_user_id,created_date, last_update_date,status,account_id,session_id,image,login_type) VALUES ('" . $customer_name . "', '" . $customer_email . "','".$facebook_user_id."', '" . $created_date . "', '" . $last_update_date . "','" . $status . "','" . $account_id . "','" . $auth_token . "','" . $image . "','" . $login_type . "')";

                            $this->database->query($addCustomerSql) or die($this->database->error . __LINE__);
                            $customer_id = $this->database->insert_id;

                            // echo $customer_id;exit;

                            // Add customer details

                            if ($customer_id > 0) {

                                $customerwebuserSql = "INSERT INTO table_web_users (account_id,username,password,email_id, customer_id, user_type, start_date,end_date,status) VALUES ('" . $account_id . "','" . $username . "', '" . $password . "', '" . $customer_email . "', '" . $customer_id . "', 4, '" . $start_date . "','" . $end_date . "','" . $status . "')";


                                $this->database->query($customerwebuserSql) or die($this->mysqli->error . __LINE__);

                                /*                 * ****Insert device id into device table** */
                                if (!empty($customerParams['data']['deviceId'])) {

                                    $deviceIdQuery = "INSERT INTO table_devices(account_id,customer_id,device_id,created_date,last_update_date) VALUES ('" . $account_id . "','" . $customer_id . "', '" . $customerParams['data']['deviceId'] . "','" . $created_date . "','" . $last_update_date . "')";


                                    $this->database->query($deviceIdQuery);
                                }

                                // $smsRes = $this->sendSms(TEXTLOCAL_API_KEY,$customer_number, $otp);

                            }
                            // $this->database->commit();

                            
                            // $data['otp'] = $otp;
                            $getUserProfile = "Select * from table_customer where customer_id = '".$customer_id."'";

                            $UserProfile = $this->database->query($getUserProfile) or die($this->mysqli->error . __LINE__);
                            // $getRecord = array();
                            // $this->database->where('loco_user_id', $checkUser[0]['loco_user_id']);
                            // $getRecord = $this->database->get('table_loco_users', null, $fieldsRet);

                            $getRecord = mysqli_fetch_assoc($UserProfile);

                            $success = array('status' => "1", "msg" => "User Logged in Successfully.", "code" => "200", "data" => $getRecord);



                            $this->response($this->json($success), 200);

                            // $success = array('status' => "1", "msg" => "User Information saved and OTP has been sent.", "code" => "200", "data" => $data);


                            file_put_contents("Log/customerRegister.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
                            file_put_contents("Log/customerRegister.log", print_r($page, true) . "\r\n", FILE_APPEND);
                            file_put_contents("Log/customerRegister.log", print_r($customerParams, true) . "\r\n", FILE_APPEND);
                            file_put_contents("Log/customerRegister.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
                            file_put_contents("Log/customerRegister.log", print_r($success, true) . "\r\n", FILE_APPEND);

                            $this->response($this->json($success), 200);
                        } catch (Exception $e) {
                            $this->database->rollback();
                            $error = array('status' => "0", "msg" => $e->getMessage(), 'code' => "201");
                            // If the request is blank.

                            file_put_contents("Log/customerRegister.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
                            file_put_contents("Log/customerRegister.log", print_r($page, true) . "\r\n", FILE_APPEND);
                            file_put_contents("Log/customerRegister.log", print_r($customerParams, true) . "\r\n", FILE_APPEND);
                            file_put_contents("Log/customerRegister.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
                            file_put_contents("Log/customerRegister.log", print_r($error, true) . "\r\n", FILE_APPEND);

                            $this->response($this->json($error), 201);
                        }
                    }

//                    if ($this->database->count > 0) {
//                        $requestParameters['username'] = $username;
//                        $requestParameters['gcmId'] = "";
//                        $requestParameters['fb'] = $profileData;
//                        $this->loginFormSocial($requestParameters);
//                    }
                } // END IF


                // $userProfilePic = $this->getFacebookProfilePicture($access_token, $me['id'], $fb);

                // $profileData['fb_user_id'] = (string) $me['id'];
                // if (isset($userProfilePic['url'])) {
                //     $profileData['profile_pic'] = $userProfilePic['url'];
                // }

                // return $profileData;
            } catch (Exception $e) {
                $error = array('status' => "0", "msg" => $e->getMessage(), 'code' => "400");
                $this->response($this->json($error), 400);
            }
        }
        else  if (isset($customerParams['data']['access_token']) && !empty($customerParams['data']['access_token']) && $customerParams['data']['login_type']=='Google') {
            
            $access_token = $customerParams['data']['access_token'];

            try {

                // require_once __DIR__ . '/SDK/Facebook/autoload.php'; // change path as needed


                require_once __DIR__ . '/SDK/Google/vendor/autoload.php'; // change path as needed

                $client_id = "884105308350-n3tutt9v0c73kj3e9regprrg1ocd95c2.apps.googleusercontent.com";
                $google_client = new Google_Client(['client_id' => "884105308350-n3tutt9v0c73kj3e9regprrg1ocd95c2.apps.googleusercontent.com"]);
                // $fb->verifyIdToken($access_token);
                // $fb->setApplicationName("Ecleaning");
                // $fb->setDeveloperKey("AIzaSyCU3-HGcuALtC8jGXmnhRaq-fjeI_MWJ_A");
                        //'default_access_token' => '{access-token}', // optional
                // $payload = $google_client->verifyIdToken($access_token);


                // $payload = 'https://oauth2.googleapis.com/tokeninfo?id_token=' . $access_token;
                $payload = 'https://www.googleapis.com/oauth2/v3/tokeninfo?id_token=' . $access_token;
                $json = file_get_contents($payload);
                $userInfoArray = json_decode($json,true);


                $username = "";

               

                if (isset($userInfoArray['name'])) {
                    $profileData['customer_name'] = $userInfoArray['name'];
                }



                if (isset($userInfoArray['email'])) {
                    $username = trim($userInfoArray['email']);
                    $profileData['email_id'] = $userInfoArray['email'];
                }

                
                if ($username != "") {
                    $username = $profileData['email_id'];

                    $checkUser = "SELECT * from table_customer as tc left join table_web_users as twu on tc.customer_id = twu.customer_id where twu.username= '".$username."' or tc.google_user_id='".$userInfoArray['sub']."'";
                   

                    $result = $this->database->query($checkUser) or die($this->mysqli->error . __LINE__);
                    
                    if($result->num_rows > 0) 
                    {
                        $userData = mysqli_fetch_assoc($result);
                    }
                    
                    if ($userData['customer_id'] > 0) {
                        // $fieldSession = array("session_id");
                        // $this->database->where("customer_id", $userData['loco_user_id']);
                        $selectSessionId = "SELECT * from table_customer where session_id = '' and customer_id = '".$userData['customer_id']."'";

                        $sess = $this->database->query($selectSessionId);
                        // $checkSession = $this->database->get("table_loco_users", null, $fieldSession);
                        $dataUser = mysqli_fetch_assoc($sess);
                        if ($dataUser['session_id'] == '') {

                            $updateSession = "UPDATE table_customer set session_id = '".mt_rand(10000000, 99999999)."' where customer_id = '".$dataUser['customer_id']."'";

                            $updsess = $this->database->query($updateSession) or die($this->mysqli->error . __LINE__);
                        }
                        $getUserProfile = "Select * from table_customer where customer_id = '".$userData['customer_id']."'";

                        $UserProfile = $this->database->query($getUserProfile) or die($this->mysqli->error . __LINE__);
                        

                        $getRecord = mysqli_fetch_assoc($UserProfile);

                        $success = array('status' => "1", "msg" => "User Logged in Successfully.", "code" => "200", "data" => $getRecord);



                        $this->response($this->json($success), 200);
                    }
                    else if($userData['customer_id'] <= 0)
                    {
                        $customer_name = trim($userInfoArray['name']);
                        $customer_email = trim($userInfoArray['email']);
                        $google_user_id = trim($userInfoArray['sub']);
                        // $password = md5(12345678));
                        $username = trim($userInfoArray['email']);
                        $status = 'A';
                        $account_id = 1;
                        $last_update_date = date('Y-m-d H:i:s');
                        $created_date = date('Y-m-d H:i:s');
                        $end_date = date('Y-m-d H:i:s');
                        $start_date = date('Y-m-d H:i:s');
                        // $gcmId = trim($customerParams['data']['gcmId']);
                        $login_type = "Google";
                        $auth_token = mt_rand(10000000, 99999999);
                        // $otp = mt_rand(10000, 99999);
                        // $otp = 12345;
                        try {
                            $addCustomerSql = "INSERT INTO table_customer (customer_name,customer_email,google_user_id,created_date, last_update_date,status,account_id,session_id,image,login_type) VALUES ('" . $customer_name . "', '" . $customer_email . "','".$google_user_id."', '" . $created_date . "', '" . $last_update_date . "','" . $status . "','" . $account_id . "','" . $auth_token . "','" . $image . "','" . $login_type . "')";

                            $this->database->query($addCustomerSql) or die($this->database->error . __LINE__);
                            $customer_id = $this->database->insert_id;

                            if ($customer_id > 0) {

                                $customerwebuserSql = "INSERT INTO table_web_users (account_id,username,password,email_id, customer_id, user_type, start_date,end_date,status) VALUES ('" . $account_id . "','" . $username . "', '" . $password . "', '" . $customer_email . "', '" . $customer_id . "', 4, '" . $start_date . "','" . $end_date . "','" . $status . "')";


                                $this->database->query($customerwebuserSql) or die($this->mysqli->error . __LINE__);

                                /*                 * ****Insert device id into device table** */
                                if (!empty($customerParams['data']['deviceId'])) {

                                    $deviceIdQuery = "INSERT INTO table_devices(account_id,customer_id,device_id,created_date,last_update_date) VALUES ('" . $account_id . "','" . $customer_id . "', '" . $customerParams['data']['deviceId'] . "','" . $created_date . "','" . $last_update_date . "')";


                                    $this->database->query($deviceIdQuery);
                                }

                                // $smsRes = $this->sendSms(TEXTLOCAL_API_KEY,$customer_number, $otp);

                            }
                            $getUserProfile = "Select * from table_customer where customer_id = '".$customer_id."'";

                            $UserProfile = $this->database->query($getUserProfile) or die($this->mysqli->error . __LINE__);
                            // $getRecord = array();
                            // $this->database->where('loco_user_id', $checkUser[0]['loco_user_id']);
                            // $getRecord = $this->database->get('table_loco_users', null, $fieldsRet);

                            $getRecord = mysqli_fetch_assoc($UserProfile);

                            $success = array('status' => "1", "msg" => "User Logged in Successfully.", "code" => "200", "data" => $getRecord);



                            $this->response($this->json($success), 200);

                            // $success = array('status' => "1", "msg" => "User Information saved and OTP has been sent.", "code" => "200", "data" => $data);


                            file_put_contents("Log/customerRegister.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
                            file_put_contents("Log/customerRegister.log", print_r($page, true) . "\r\n", FILE_APPEND);
                            file_put_contents("Log/customerRegister.log", print_r($customerParams, true) . "\r\n", FILE_APPEND);
                            file_put_contents("Log/customerRegister.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
                            file_put_contents("Log/customerRegister.log", print_r($success, true) . "\r\n", FILE_APPEND);

                            $this->response($this->json($success), 200);
                        } catch (Exception $e) {
                            $this->database->rollback();
                            $error = array('status' => "0", "msg" => $e->getMessage(), 'code' => "201");
                            // If the request is blank.

                            file_put_contents("Log/customerRegister.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
                            file_put_contents("Log/customerRegister.log", print_r($page, true) . "\r\n", FILE_APPEND);
                            file_put_contents("Log/customerRegister.log", print_r($customerParams, true) . "\r\n", FILE_APPEND);
                            file_put_contents("Log/customerRegister.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
                            file_put_contents("Log/customerRegister.log", print_r($error, true) . "\r\n", FILE_APPEND);

                            $this->response($this->json($error), 201);
                        }
                    }

                } // END IF

                // return $profileData;
            } catch (Exception $e) {
                $error = array('status' => "0", "msg" => $e->getMessage(), 'code' => "400");
                $this->response($this->json($error), 400);
            }
        }
    
       
    }


    function upload_pro_pic($base64_string, $output_file, $type) {

        function randimg($cnt) {
            $img = str_shuffle('abcefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890_');
            return substr($img, 0, $cnt);
        }

        $num = randimg(10);
        $image_url = "img_" . $num . "." . $type;
        //$image_url="img_".$num.".jpg";
        $upload = $output_file . $image_url;
        //echo $upload;EXIT;
        //$output_file = "image.jpg";
        $ifp = fopen($upload, "wb");

        //$data = explode(',', $base64_string);

        fwrite($ifp, base64_decode($base64_string));
        fclose($ifp);

        return $image_url;
    }

    /*     * ***********************Forgot password ************************************* */

    private function forgotPassword() {

        $datetime = date('Y-m-d H:i:s');
        $page = "Forgot Password";
        $inputData = json_decode(file_get_contents("php://input"), true);
        if (!isset($inputData['data']) || empty($inputData['data']) || !isset($inputData['data']['customer_email']) || empty($inputData['data']['customer_email'])) {

            $error = array('status' => "0", "msg" => "Request Blank", 'code' => "201");

            file_put_contents("Log/customerForgotPassword.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/customerForgotPassword.log", print_r($page, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/customerForgotPassword.log", print_r($inputData, true) . "\r\n", FILE_APPEND);

            file_put_contents("Log/customerForgotPassword.log", print_r($error, true) . "\r\n", FILE_APPEND);

            $this->response($this->json($error), 201);
        }

        if (isset($inputData['data']['customer_email']) && !empty($inputData['data']['customer_email']) && !isset($inputData['data']['otp'])) {

            $email = trim($inputData['data']['customer_email']);
            $sql = "SELECT twu.*,tc.* FROM table_customer as tc left join table_web_users as twu on twu.customer_id=tc.customer_id WHERE tc.customer_email = '" . $email . "' ";

            $checkSts = $this->database->query($sql);
            if ($checkSts->num_rows > 0) {

                $otp = mt_rand(10000, 99999);
                $customer = $checkSts->fetch_assoc();
                // print_r($customer);exit;
                $custID = $customer['customer_id'];
                $mobileNO = $customer['customer_number'];
                $otp = 12345;
                $data['otp'] = $otp;
                $data['country_code'] = $customer['country_code'];
                $data['customerNumber'] = $mobileNO;
                //$data['sessionId']=$auth_token;
                $where = "WHERE customer_id = '" . $custID . "' ";
                $otpSql = " UPDATE table_customer SET otp = '" . $otp . "'" . ",last_update_date='" . $datetime . "'" . $where;
                $this->database->query($otpSql);
                //SMS API Intergration
                $smsRes = $this->sendSms(TEXTLOCAL_API_KEY,$mobileNO, $otp);

                $success = array('status' => "1", "msg" => "Please go to next step.", "code" => "200", "data" => $data);

                file_put_contents("Log/customerForgotPassword.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/customerForgotPassword.log", print_r($page, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/customerForgotPassword.log", print_r($inputData, true) . "\r\n", FILE_APPEND);

                file_put_contents("Log/customerForgotPassword.log", print_r($success, true) . "\r\n", FILE_APPEND);

                $this->response($this->json($success), 200);
            } else {

                $error = array('status' => "0", "msg" => "Your Email ID is not registered.", 'code' => "203");
                file_put_contents("Log/customerForgotPassword.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/customerForgotPassword.log", print_r($page, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/customerForgotPassword.log", print_r($inputData, true) . "\r\n", FILE_APPEND);

                file_put_contents("Log/customerForgotPassword.log", print_r($error, true) . "\r\n", FILE_APPEND);

                $this->response($this->json($error), 203);
            }
        } else if (isset($inputData['data']['customer_email']) && !empty($inputData['data']['customer_email']) && isset($inputData['data']['otp']) && !empty($inputData['data']['otp'])) {

            $email = trim($inputData['data']['customer_email']);
            $otp = trim($inputData['data']['otp']);

            $now = date('Y-m-d H:i:s');

            $checkOtp = "SELECT twu.* FROM table_customer as tc left join table_web_users as twu on twu.customer_id=tc.customer_id WHERE tc.customer_email = '" . $email . "' AND tc.otp='" . $otp . "' AND TIMESTAMPDIFF(MINUTE,tc.last_update_date,'".$now."') < 10";
            // print_r($checkOtp);exit;
            $otpQuery = $this->database->query($checkOtp);
            if ($otpQuery->num_rows <= 0) {

                $error = array('status' => "0", "msg" => "Your Otp has been exipred or invalid.", 'code' => "203");
                file_put_contents("Log/customerForgotPassword.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/customerForgotPassword.log", print_r($page, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/customerForgotPassword.log", print_r($inputData, true) . "\r\n", FILE_APPEND);

                file_put_contents("Log/customerForgotPassword.log", print_r($error, true) . "\r\n", FILE_APPEND);

                $this->response($this->json($error), 203);
            } else {

                $auth_token = mt_rand(10000000, 99999999);
                $customer = $otpQuery->fetch_assoc();
                //print_r($customer);exit;
                $custID = $customer['customer_id'];
                $data['customerId'] = "".$custID;
                $data['sessionId'] = "".$auth_token;
                $where = "WHERE customer_id = '" . $custID . "' ";
                $sql1 = " UPDATE table_customer SET session_id = '" . $auth_token . "'" . $where;
                $this->database->query($sql1);

                $success = array('status' => "1", "msg" => "Please go to next step.", "code" => "200", "data" => $data);

                file_put_contents("Log/customerForgotPassword.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/customerForgotPassword.log", print_r($page, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/customerForgotPassword.log", print_r($inputData, true) . "\r\n", FILE_APPEND);

                file_put_contents("Log/customerForgotPassword.log", print_r($success, true) . "\r\n", FILE_APPEND);

                $this->response($this->json($success), 200);
            }
        } else {

            $error = array('status' => "0", "msg" => "Request Blank.", 'code' => "201");
            file_put_contents("Log/customerForgotPassword.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/customerForgotPassword.log", print_r($page, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/customerForgotPassword.log", print_r($inputData, true) . "\r\n", FILE_APPEND);

            file_put_contents("Log/customerForgotPassword.log", print_r($error, true) . "\r\n", FILE_APPEND);

            $this->response($this->json($error), 201);
        }
    }

    /*     * ************function for sending SMS**************** */

    public function sendSms1($mobileNo, $otp) {
        // return true;
        //Details

        $user = "whitespot"; //username
        $password = "Sunshine@123"; //password
        $mobilenumbers = "971" . $mobileNo; //Mobile numbers comma seperated

        // $numbers = array($mobileNo);
        // $user = "abhishek.shrivastav@quytech.com"; //username
        // $password = "Abhishek#001"; //password
        // $mobilenumbers = implode(',', $numbers);

        // print_r($mobilenumbers);die;

        $message = $otp; //Your Message
        $senderid = "whitespot"; //senderid
        $messagetype = "N"; //Type Of Your Message
        $DReports = "Y"; //Delivery Reports
        $message = urlencode($message);
        $url = "http://api.smscountry.com/SMSCwebservice_bulk.aspx";
        // $url = "https://api.textlocal.in/send/";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "User=$user&passwd=$password&mobilenumber=$mobilenumbers&message=$message&sid=$senderid&mtype=$messagetype&DR=$DReports");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $server_output = curl_exec ($ch);
        if (curl_errno($ch)) {
            return false;
        }
        // print_r($server_output);die;
        //$info = curl_getinfo($ch);
        curl_close($ch);
        if (strpos($server_output, 'OK') !== false) {
            return true;
        } else {
            return false;
        }
        //if($server_output == "OK") { return true;} else { return false; }
    }

    /*     * ************End SMS*************** */


 /**************NEW function for sending SMS*****************/

    public function sendSms($api,$mobileNo,$otp) {

            $apiKey = urlencode($api);

            $numbers = array($mobileNo);
            $sender = urlencode('TXTLCL');

            $message = $otp;
            $message = rawurlencode($message);

            $numbers = implode(',', $numbers);
            $data = array('apikey' => $apiKey, 'numbers' => $numbers, "sender" => $sender, "message" => $message);

    // Send the POST request with cURL
            $ch = curl_init('https://api.textlocal.in/send/');
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $response = curl_exec($ch);
            curl_close($ch);
            // echo $response;exit;
        }

/**************End SMS*******************************************************/




    /****************************************Reset Password by Ayush kumar on 24/nov/2017 */

    public function resetPassword() {

        $datetime = date('Y-m-d H:i:s');
        $page = "Customer Reset Password";
        $inputData = json_decode(file_get_contents("php://input"), true);
        //$Details = json_decode('{"customerId":"1","sessionId":"8nqisjeo3","newPassword":"arvind123456"}',true);

        if (isset($inputData['data']) && !empty($inputData['data']) && isset($inputData['data']['customerId']) && !empty($inputData['data']['customerId']) && isset($inputData['data']['sessionId']) && !empty($inputData['data']['sessionId']) && isset($inputData['data']['newPassword']) && !empty($inputData['data']['newPassword'])) {

            $customerId = trim($inputData['data']['customerId']);
            $sessionId = trim($inputData['data']['sessionId']);
            $newPassword = md5(trim($inputData['data']['newPassword']));
            $response = $this->authenticateUser($customerId, $sessionId);

            if ($response) {


                $sql1 = "UPDATE table_web_users SET password = '" . $newPassword . "' WHERE customer_id = '" . $customerId . "' ";
                $sql2 = "UPDATE table_customer SET session_id = '' WHERE customer_id = '" . $customerId . "' ";
                try {
                    $this->database->query($sql1);
                    $this->database->query($sql2);
                } catch (Exception $e) {
                    $error = array('status' => "0", "msg" => $e->getMessage(), "code" => "500");

                    file_put_contents("Log/customerresetPassword.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
                    file_put_contents("Log/customerresetPassword.log", print_r($page, true) . "\r\n", FILE_APPEND);
                    file_put_contents("Log/customerresetPassword.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
                    file_put_contents("Log/customerresetPassword.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
                    file_put_contents("Log/customerresetPassword.log", print_r($error, true) . "\r\n", FILE_APPEND);

                    $this->response($this->json($error), 500);
                }

                $success = array("status" => "1", "msg" => "Your password has been changed successfully!.", "code" => "200");
                //echo "hiii";exit;
                file_put_contents("Log/customerresetPassword.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/customerresetPassword.log", print_r($page, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/customerresetPassword.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/customerresetPassword.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/customerresetPassword.log", print_r($success, true) . "\r\n", FILE_APPEND);



                $this->response($this->json($success), 200);
            } else {
                $error = array('status' => "0", "msg" => "No user found!", "code" => "207");

                file_put_contents("Log/customerresetPassword.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/customerresetPassword.log", print_r($page, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/customerresetPassword.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/customerresetPassword.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/customerresetPassword.log", print_r($error, true) . "\r\n", FILE_APPEND);

                $this->response($this->json($error), 207);
            }
        } else {
            $error = array("status" => "0", "msg" => "Request Blank!", "code" => "201");

            file_put_contents("Log/customerresetPassword.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/customerresetPassword.log", print_r($page, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/customerresetPassword.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/customerresetPassword.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/customerresetPassword.log", print_r($error, true) . "\r\n", FILE_APPEND);

            $this->response($this->json($error), 201);
        }
    }

    /** **************************************************
      Description: Customer Profile
      updated by :Ayush kumar on 22/nov/2017

     * *************************************************** */

    private function getCustomerProfile() {

        $datetime = date('Y-m-d H:i:s');
        $page = "Customer Profile";
        $inputData = json_decode(file_get_contents("php://input"), true);
        //print_r($inputData);exit;
        if (isset($inputData['data']['customerId']) && !empty($inputData['data']['customerId']) && isset($inputData['data']['sessionId']) && !empty($inputData['data']['sessionId'])) {

            $customerId = trim($inputData['data']['customerId']);
            $sessionId = ($inputData['data']['sessionId']);
            //echo $sessionId ;
            // Check customer exists in the system or not
            $response = $this->authenticateUser($customerId, $sessionId);

            if ($response) {

                $userProfileSql = " SELECT tc.customer_id, tc.customer_name, tc.customer_number, tc.customer_email, tc.created_date,tc.image,twu.username,tc.country_code FROM table_customer AS tc LEFT JOIN table_web_users AS twu ON twu.customer_id = tc.customer_id LEFT JOIN table_buildings as tb on tb.building_id=tc.building_id  WHERE tc.status = 'A' AND tc.customer_id = '" . $customerId . "'";

                $result = $this->database->query($userProfileSql) or die($this->database->error . __LINE__);

                if ($result->num_rows > 0) {
                    $getDetails = array();

                    $cusdata = mysqli_fetch_assoc($result);

                    if (isset($cusdata['image']) && !empty($cusdata['image'])) {
                        $cusdata['image'] = self::HOST . "services/customer_image/" . $cusdata['image'];
                    }
                    /*                     * * Find latest address and cleaningPlan** */
                    $condition = " Where tbr.customer_id='" . $customerId . "'order by tbr.booking_id desc LIMIT 1";

                    $planQuery = "Select tbr.booking_id,tbr.cleaning_plan,tb.building_name,ta.area_name,c.city_name,tca.customer_address,tca.lat,tca.lng from table_booking_register as tbr left join table_customer_address as tca on tca.customer_address_id=tbr.customer_address_id left join table_buildings as tb on tb.building_id=tca.building_id left join table_area as ta on ta.area_id=tb.area_id left join city as c on c.city_id=ta.city_id" . $condition;

                    $planResult = $this->database->query($planQuery);
                    if ($planResult->num_rows > 0) {
                        $planData = mysqli_fetch_assoc($planResult);
                        if (empty($planData['area_name'])) {
                            $planData['area_name'] = '';
                        }

                        if (empty($planData['city_name'])) {
                            $planData['city_name'] = '';
                        }
                        if (strtolower($planData['cleaning_plan']) == 'o') {
                            $plan = "Onetime";
                        } else {
                            $plan = "Contract";
                        }

                        $cusdata['location'] = rtrim($planData['customer_address'] . ',' . $planData['building_name'] . ',' . $planData['area_name'] . ',' . $planData['city_name'], ",");

                        $cusdata['lat'] = $planData['lat'];
                        $cusdata['lng'] = $planData['lng'];
                        $cusdata['cleaning_plan'] = $plan;
                    } else {

                        $cusdata['location'] = "";
                        $cusdata['lat'] = "";
                        $cusdata['lng'] = "";
                        $cusdata['cleaning_plan'] = "";
                    }

                    /*                     * End* */

                    $success = array('status' => "1", "msg" => "Customer details.", "data" => $cusdata, "code" => "200");

                    file_put_contents("Log/customerProfile.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
                    file_put_contents("Log/customerProfile.log", print_r($page, true) . "\r\n", FILE_APPEND);
                    file_put_contents("Log/customerProfile.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
                    file_put_contents("Log/customerProfile.log", print_r($success, true) . "\r\n", FILE_APPEND);

                    $this->response($this->json($success), 200);
                } else {

                    $error = array('status' => "0", "msg" => "No User Profile Exists!", "code" => "209");

                    file_put_contents("Log/customerProfile.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
                    file_put_contents("Log/customerProfile.log", print_r($page, true) . "\r\n", FILE_APPEND);
                    file_put_contents("Log/customerProfile.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
                    file_put_contents("Log/customerProfile.log", print_r($error, true) . "\r\n", FILE_APPEND);
                    $this->response($this->json($error), 209);
                }
            } else {
                $error = array('status' => "Failed", "msg" => "Authentication failed!", "code" => "209");

                file_put_contents("Log/customerProfile.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/customerProfile.log", print_r($page, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/customerProfile.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/customerProfile.log", print_r($error, true) . "\r\n", FILE_APPEND);
                $this->response($this->json($error), 209);
            }
        } else {

            $error = array('status' => "0", "msg" => "Request Blank!", "code" => "201");

            file_put_contents("Log/customerProfile.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/customerProfile.log", print_r($page, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/customerProfile.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/customerProfile.log", print_r($error, true) . "\r\n", FILE_APPEND);

            $this->response($this->json($error), 201); // If the request is blank.
        }
    }

    /*     * ***  End Get Profile      * */

    /*     * ***update Customer profile *** */

    private function updateCustomerProfile() {
        //echo "ddd";exit;
        $datetime = date('Y-m-d H:i:s');
        $page = "Update Customer Profile";
        $inputData = json_decode(file_get_contents("php://input"), true);

        if (isset($inputData['data']) && !empty($inputData['data']) && isset($inputData['data']['customerId']) && !empty($inputData['data']['customerId']) && isset($inputData['data']['sessionId']) && !empty($inputData['data']['sessionId'])) {
            // print_r($inputData);exit;
            $customerId = trim($inputData['data']['customerId']);
            $sessionId = trim($inputData['data']['sessionId']);
            $response = $this->authenticateUser($customerId, $sessionId);
            if ($response) {

                $customerEmail = trim($inputData['data']['customerEmail']);
                $customerName = trim($inputData['data']['customerName']);
                $countryCode = trim($inputData['data']['country_code']);
                $customerNumber = trim($inputData['data']['customerNumber']);
                $last_update_date = date('Y-m-d H:i:s');
                $type = "png";
                if (isset($inputData['data']['image']) && !empty($inputData['data']['image'])) {
                    $dir = 'customer_image/';
                    if (!is_dir($dir) && mkdir($dir, 0777, true)) {
                        chmod('./customer_image/', 0777);
                        chmod($dir, 0777);
                    }
                    if (is_dir($dir)) {

                        $image = $this->upload_pro_pic($inputData['data']['image'], $dir, $type);
                    }
                } else {
                    $image = '';
                }
                $numbersql = 'SELECT customer_id FROM table_customer WHERE customer_email ="' . $customerEmail . '" AND customer_id!=' . $customerId;

                $checkPhone = $this->database->query($numbersql);

                if ($checkPhone->num_rows > 0) {

                    $error = array('status' => "0", "msg" => "This phone number already registered.Please try another one!.", 'code' => "212");


                    file_put_contents("Log/customerUpdateProfile.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
                    file_put_contents("Log/customerUpdateProfile.log", print_r($page, true) . "\r\n", FILE_APPEND);
                    file_put_contents("Log/customerUpdateProfile.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
                    file_put_contents("Log/customerUpdateProfile.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
                    file_put_contents("Log/customerRegister.log", print_r($error, true) . "\r\n", FILE_APPEND);

                    $this->response($this->json($error), 212);
                }
                if (!empty($image)) {
                    $updatecustomertable = "UPDATE table_customer SET customer_name='" . $customerName . "', customer_number='" . $customerNumber . "', country_code='" . $countryCode . "', customer_email='" . $customerEmail . "', last_update_date='" . $last_update_date . "',image='" . $image . "' WHERE customer_id = '" . $customerId . "'";
                } else {
                    $updatecustomertable = "UPDATE table_customer SET customer_name='" . $customerName . "', customer_number='" . $customerNumber . "', country_code='" . $countryCode . "', customer_email='" . $customerEmail . "', last_update_date='" . $last_update_date . "' WHERE customer_id = '" . $customerId . "'";
                }
                // print_r($updatecustomertable);die;

                $updateWebUsertable = "UPDATE table_web_users SET email_id='" . $customerEmail . "' WHERE customer_id = '" . $customerId . "' ";


                $updated_custab = $this->database->query($updatecustomertable) or die($this->mysqli->error . __LINE__);
                $updated_webusertab = $this->database->query($updateWebUsertable) or die($this->mysqli->error . __LINE__);


                $userProfileSql = " SELECT tc.customer_name,tc.customer_number,tc.customer_email,tc.country_code,tc.image From table_customer as tc WHERE tc.status = 'A' AND tc.customer_id = '" . $customerId . "'";

                $result = $this->database->query($userProfileSql) or die($this->database->error . __LINE__);

                if ($result->num_rows > 0) {
                    $getDetails = array();

                    $cusdata = mysqli_fetch_assoc($result);

                    if (isset($cusdata['image']) && !empty($cusdata['image'])) {
                        $cusdata['image'] = self::HOST . "services/customer_image/" . $cusdata['image'];
                    }
                }





                $success = array('status' => "1", "msg" => "Your profile has been updated successfully.", "code" => "200" ,'data'=>$cusdata);

                file_put_contents("Log/customerUpdateProfile.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/customerUpdateProfile.log", print_r($page, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/customerUpdateProfile.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/customerUpdateProfile.log", print_r($success, true) . "\r\n", FILE_APPEND);

                $this->response($this->json($success), 200);
            } else {
                $error = array('status' => "0", "msg" => "No user found!", "code" => "201");

                file_put_contents("Log/customerUpdateProfile.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/customerUpdateProfile.log", print_r($page, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/customerUpdateProfile.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/customerUpdateProfile.log", print_r($error, true) . "\r\n", FILE_APPEND);

                $this->response($this->json($error), 201);
            }
        } else {
            $error = array('status' => "0", "msg" => "Request Blank!", "code" => "201");

            file_put_contents("Log/customerUpdateProfile.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/customerUpdateProfile.log", print_r($page, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/customerUpdateProfile.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/customerUpdateProfile.log", print_r($error, true) . "\r\n", FILE_APPEND);

            $this->response($this->json($error), 201);
        }
    }

    /*     * *End UPdate customer profile** */

    function sendEmail($Email, $Message, $Subject) {

        require_once('../phpMailer/PHPMailerAutoload.php');


        $customerEmail = $Email;
        //$customerEmail="ayush.kumar@quytech.com";
        $password = mt_rand(10000000, 99999999);
        $mail = new PHPMailer;
        $mail->isSMTP(true);
        $mail->Host = 'ssl://smtp.gmail.com';
        $mail->SMTPAuth = true;
        $mail->Username = 'aabhishek04@gmail.com';
        $mail->Password = 'aabhiabhi@2a';
        $mail->SMTPSecure = 'ssl';
        $mail->Port = '465';
        $mail->setFrom('aabhishek04@gmail.com', 'WhiteSpot');
        $mail->addAddress($customerEmail);
        $mail->isHTML(true);


        $mail->Subject = $Subject;
        $mail->Body = $Message;
        // $mail->AltBody =	

        if (!$mail->send()) {
               return false;
        } else {
            return true;
        }
    }

    /*     * *************** Customer Unit types on 24/nov/2017 By Ayush kumar ****************************** */

    private function unitType() {
        $datetime = date('Y-m-d H:i:s');
        $page = "Customer Unit Types";
        if ($this->get_request_method() != 'GET') {

            $error = array("status" => "0", "msg" => "Invalid request method.", "code" => "203");

            file_put_contents("Log/customerUnitType.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/customerUnitType.log", print_r($page, true) . "\r\n", FILE_APPEND);

            file_put_contents("Log/customerUnitType.log", print_r($error, true) . "\r\n", FILE_APPEND);
            //echo "ddd";
            $this->response($this->json($error), 203);
        }


        $sql = "SELECT tut.unit_type_id,tut.unit_name,tut.unit_image,tut.unit_image2 FROM table_unit_type as tut";
        $result = $this->database->query($sql) or die($this->mysqli->error . __LINE__);

        if ($result->num_rows > 0) {
            $i = 0;
            while ($row = mysqli_fetch_assoc($result)) {
                if (isset($row['unit_image']) && !empty($row['unit_image'])) {
                    $row['unit_image'] = self::HOST . "/services/images/" . $row['unit_image'];
                }
                if (isset($row['unit_image2']) && !empty($row['unit_image2'])) {
                    $row['unit_image2'] = self::HOST . "/services/images/" . $row['unit_image2'];
                }
                $data[] = $row;
            }

            $success = array("status" => "1", "msg" => "All Unit Types!.", "code" => "200", "data" => $data);
            //echo "hiii";exit;
            file_put_contents("Log/customerUnitType.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/customerUnitType.log", print_r($page, true) . "\r\n", FILE_APPEND);

            file_put_contents("Log/customerUnitType.log", print_r($success, true) . "\r\n", FILE_APPEND);
            $this->response($this->json($success), 200);
        } else {
            // If Username is invalid
            $error = array('status' => "0", "msg" => "Your old password is invalid.", "code" => "209");

            file_put_contents("Log/customerUnitType.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/customerUnitType.log", print_r($page, true) . "\r\n", FILE_APPEND);

            file_put_contents("Log/customerUnitType.log", print_r($error, true) . "\r\n", FILE_APPEND);

            $this->response($this->json($error), 209);
        }
    }

    /*     * *************** Customer Cleaning types on 24/nov/2017 By Ayush kumar ****************************** */

    private function cleaningType() {
        $datetime = date('Y-m-d H:i:s');
        $page = "Customer Cleaning Types";

        if ($this->get_request_method() != 'POST') {

            $error = array("status" => "0", "msg" => "Invalid request method.", "code" => "203");

            file_put_contents("Log/customercleaningType.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/customercleaningType.log", print_r($page, true) . "\r\n", FILE_APPEND);

            file_put_contents("Log/customercleaningType.log", print_r($error, true) . "\r\n", FILE_APPEND);
            //echo "ddd";
            $this->response($this->json($error), 203);
        }

        $inputData = json_decode(file_get_contents("php://input"), true);
        if (!isset($inputData['data']['unitTypeId']) || empty($inputData['data']['unitTypeId'])) {
            $error = array("status" => "0", "msg" => "Request Blank.", "code" => "201");

            file_put_contents("Log/customercleaningType.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/customercleaningType.log", print_r($page, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/customercleaningType.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/customercleaningType.log", print_r($error, true) . "\r\n", FILE_APPEND);
            //echo "ddd";
            $this->response($this->json($error), 201);
        }

        $condition = " Where tut.unit_type_id=" . $inputData['data']['unitTypeId'] . " AND tut.status='A'";
        $unitTypeSql = "SELECT tut.unit_type_id FROM table_unit_type as tut" . $condition;

        $result = $this->database->query($unitTypeSql) or die($this->mysqli->error . __LINE__);

        if ($result->num_rows > 0) {

            $condtion2 = " Where tct.unit_type_id=" . $inputData['data']['unitTypeId'] . " AND tct.status='A'";
            $cleaningTypeSql = "SELECT tct.cleaning_type_id,tct.cleaning_type ,tct.unit_type_id FROM table_cleaning_type as tct" . $condtion2;
            $result2 = $this->database->query($cleaningTypeSql) or die($this->mysqli->error . __LINE__);

            if ($result2->num_rows <= 0) {
                $error = array('status' => "0", "msg" => "Data does not exits.", "code" => "209");

                file_put_contents("Log/customercleaningType.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/customercleaningType.log", print_r($page, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/customercleaningType.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/customercleaningType.log", print_r($error, true) . "\r\n", FILE_APPEND);

                $this->response($this->json($error), 209);
            }

            while ($row = mysqli_fetch_assoc($result2)) {
                $data[] = $row;
            }

            $success = array("status" => "1", "msg" => "All Cleaning Types!.", "code" => "200", "data" => $data);
            //echo "hiii";exit;
            file_put_contents("Log/customercleaningType.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/customercleaningType.log", print_r($page, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/customercleaningType.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/customercleaningType.log", print_r($success, true) . "\r\n", FILE_APPEND);
            $this->response($this->json($success), 200);
        } else {
            // If Username is invalid
            $error = array('status' => "0", "msg" => "Invalid UnitType.", "code" => "209");

            file_put_contents("Log/customercleaningType.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/customercleaningType.log", print_r($page, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/customercleaningType.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/customercleaningType.log", print_r($error, true) . "\r\n", FILE_APPEND);

            $this->response($this->json($error), 209);
        }
    }

    /*     * *************** Customer Cleaning Details on 24/nov/2017 By Ayush kumar ****************************** */

    private function detailsOfCleaning() {
        $datetime = date('Y-m-d H:i:s');
        $page = "Customer Details Of Cleaning";

        if ($this->get_request_method() != 'POST') {

            $error = array("status" => "0", "msg" => "Invalid request method.", "code" => "203");

            file_put_contents("Log/customerdetailsOfCleaning.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/customerdetailsOfCleaning.log", print_r($page, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/customerdetailsOfCleaning.log", print_r($error, true) . "\r\n", FILE_APPEND);
            //echo "ddd";
            $this->response($this->json($error), 203);
        }

        $inputData = json_decode(file_get_contents("php://input"), true);
        if (!isset($inputData['data']['cleaningTypeId']) || empty($inputData['data']['cleaningTypeId'])) {
            $error = array("status" => "0", "msg" => "Request Blank.", "code" => "201");

            file_put_contents("Log/customerdetailsOfCleaning.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/customerdetailsOfCleaning.log", print_r($page, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/customerdetailsOfCleaning.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/customercleaningType.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/customerdetailsOfCleaning.log", print_r($error, true) . "\r\n", FILE_APPEND);
            //echo "ddd";
            $this->response($this->json($error), 201);
        }

        $condition = " Where tct.unit_type_id=" . $inputData['data']['cleaningTypeId'] . " AND tct.status='A'";
        $cleaningTypeSql = "SELECT tct.cleaning_type_id FROM table_cleaning_type as tct" . $condition;

        $result = $this->database->query($cleaningTypeSql) or die($this->mysqli->error . __LINE__);

        if ($result->num_rows > 0) {

            $condtion2 = " Where tcm.cleaning_type_id=" . $inputData['data']['cleaningTypeId'] . " AND tcm.status='A'";
            $costmasterSql = "SELECT tcm.cost_master_id,tcm.type_of_service,tcm.cleaning_type_id,tcm.cost_of_cleaning ,tcm.sq_feet_area FROM table_cost_master as tcm" . $condtion2;

            $result2 = $this->database->query($costmasterSql) or die($this->mysqli->error . __LINE__);

            if ($result2->num_rows <= 0) {
                $error = array('status' => "0", "msg" => "Data does not exits.", "code" => "209");

                file_put_contents("Log/customercleaningType.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/customercleaningType.log", print_r($page, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/customercleaningType.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/customercleaningType.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/customercleaningType.log", print_r($error, true) . "\r\n", FILE_APPEND);

                $this->response($this->json($error), 209);
            }

            while ($row = mysqli_fetch_assoc($result2)) {
                $data[] = $row;
            }

            $success = array("status" => "1", "msg" => "All Details of Cleaning!.", "code" => "200", "data" => $data);
            //echo "hiii";exit;
            file_put_contents("Log/customercleaningType.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/customercleaningType.log", print_r($page, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/customercleaningType.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/customercleaningType.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/customercleaningType.log", print_r($success, true) . "\r\n", FILE_APPEND);
            $this->response($this->json($success), 200);
        } else {
            // If Username is invalid
            $error = array('status' => "0", "msg" => "Invalid Cleaning Type.", "code" => "209");

            file_put_contents("Log/customercleaningType.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/customercleaningType.log", print_r($page, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/customercleaningType.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/customercleaningType.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/customercleaningType.log", print_r($error, true) . "\r\n", FILE_APPEND);

            $this->response($this->json($error), 209);
        }
    }

    /*     * *************** Customer Unit types to cost master on 24/nov/2017 By Ayush kumar ****************************** */

    private function unitTypeCleaningType() {
        $datetime = date('Y-m-d H:i:s');
        $page = "Unit Type to Cleaning Type.";
        if ($this->get_request_method() != 'GET') {

            $error = array("status" => "0", "msg" => "Invalid request method.", "code" => "203");

            file_put_contents("Log/UnitCleaningType.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/UnitCleaningType.log", print_r($page, true) . "\r\n", FILE_APPEND);

            file_put_contents("Log/UnitCleaningType.log", print_r($error, true) . "\r\n", FILE_APPEND);
            //echo "ddd";
            $this->response($this->json($error), 203);
        }


        $sql = "SELECT tut.unit_type_id,tut.unit_name,tut.unit_name,tut.unit_image,tut.unit_image2 FROM table_unit_type as tut where tut.status='A'";
        $result = $this->database->query($sql) or die($this->mysqli->error . __LINE__);

        if ($result->num_rows > 0) {
            $i = 0;
            while ($row = mysqli_fetch_assoc($result)) {
                if (isset($row['unit_image']) && !empty($row['unit_image'])) {
                    $row['unit_image'] = self::HOST . "/images/unitType/" . $row['unit_image'];
                }
                if (isset($row['unit_image2']) && !empty($row['unit_image2'])) {
                    $row['unit_image2'] = self::HOST . "/images/unitType/" . $row['unit_image2'];
                }
                $data[$i] = $row;

                $condtion2 = " Where cum.unit_type_id=" . $row['unit_type_id'] . " AND tct.status='A'";
                $cleaningTypeSql = "SELECT tct.cleaning_type_id,tct.cleaning_type,tct.is_long_term FROM table_cleaning_type  as tct LEFT JOIN table_cleaning_unit_mapping as cum on cum.cleaning_type_id=tct.cleaning_type_id" . $condtion2;

                $result2 = $this->database->query($cleaningTypeSql) or die($this->mysqli->error . __LINE__);
                $j = 0;
                //print_r($result2);die;
                if ($result2->num_rows <= 0) {
                    $data[$i]['cleanType'] = array();
                    //$data[$i]['cleanType'][]['costType']=array();
                } else {
                    while ($row2 = mysqli_fetch_assoc($result2)) {

                        $data[$i]['cleanType'][$j] = $row2;
                        $j++;
                    }
                }
                $i++;
            }
            //$data=array_reverse($data);
            $success = array("status" => "1", "msg" => "UnitCleaningType!.", "code" => "200", "data" => $data);
            //echo "hiii";exit;
            file_put_contents("Log/UnitCleaningType.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/UnitCleaningType.log", print_r($page, true) . "\r\n", FILE_APPEND);

            file_put_contents("Log/UnitCleaningType.log", print_r($success, true) . "\r\n", FILE_APPEND);
            $this->response($this->json($success), 200);
        } else {
            // If Username is invalid
            $error = array('status' => "0", "msg" => "Data does not exists.", "code" => "209");

            file_put_contents("Log/UnitCleaningType.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/UnitCleaningType.log", print_r($page, true) . "\r\n", FILE_APPEND);

            file_put_contents("Log/UnitCleaningType.log", print_r($error, true) . "\r\n", FILE_APPEND);

            $this->response($this->json($error), 209);
        }
    }

    /*     * ****    All building and communities by UnitTYpe   *** */

    private function unitBuildCommu() {
        $datetime = date('Y-m-d H:i:s');
        $page = "Unit Type to Building and communities";

        if ($this->get_request_method() != 'POST') {

            $error = array("status" => "0", "msg" => "Invalid request method.", "code" => "203");

            file_put_contents("Log/unitBuildCommu.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/unitBuildCommu.log", print_r($page, true) . "\r\n", FILE_APPEND);

            file_put_contents("Log/unitBuildCommu.log", print_r($error, true) . "\r\n", FILE_APPEND);
            //echo "ddd";
            $this->response($this->json($error), 203);
        }

        $inputData = json_decode(file_get_contents("php://input"), true);
        if (!isset($inputData['data']['unitTypeId']) || empty($inputData['data']['unitTypeId'])) {
            $error = array("status" => "0", "msg" => "Request Blank.", "code" => "201");

            file_put_contents("Log/unitBuildCommu.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/unitBuildCommu.log", print_r($page, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/unitBuildCommu.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/unitBuildCommu.log", print_r($error, true) . "\r\n", FILE_APPEND);
            //echo "ddd";
            $this->response($this->json($error), 201);
        }

        $condition = " Where tb.unit_type_id=" . $inputData['data']['unitTypeId'] . " AND tb.is_approved=1 AND tb.status='A'";
        $unitTypeSql = "SELECT tb.building_id,tb.area_id,a.area_name,tb.building_name,tb.lat,tb.lng,tb.linen_change,tb.balcony FROM table_unit_type as tut left join table_buildings as tb on tb.unit_type_id=tut.unit_type_id left join table_area as a on a.area_id = tb.area_id" . $condition;
        //print_r($unitTypeSql);die;
        $result = $this->database->query($unitTypeSql);

        if ($result->num_rows > 0) {

            while ($row = mysqli_fetch_assoc($result)) {
                $data[] = $row;
            }

            $success = array("status" => "1", "msg" => "All Building or communities!.", "code" => "200", "data" => $data);
            //echo "hiii";exit;
            file_put_contents("Log/unitBuildCommu.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/unitBuildCommu.log", print_r($page, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/unitBuildCommu.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/unitBuildCommu.log", print_r($success, true) . "\r\n", FILE_APPEND);
            $this->response($this->json($success), 200);
        } else {
            // If Username is invalid
            $error = array('status' => "0", "msg" => "Data not Found!.", "code" => "209");

            file_put_contents("Log/unitBuildCommu.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/unitBuildCommu.log", print_r($page, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/unitBuildCommu.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/unitBuildCommu.log", print_r($error, true) . "\r\n", FILE_APPEND);

            $this->response($this->json($error), 209);
        }
    }

    /*     * ****    All categoreis by UnitTYpe  and cleaning Type  *** */

    private function unitCleaningCategories() {
        $datetime = date('Y-m-d H:i:s');
        $page = "Categories";
        if ($this->get_request_method() != 'POST') {

            $error = array("status" => "0", "msg" => "Invalid request method.", "code" => "203");

            file_put_contents("Log/unitCleaningCategories.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/unitCleaningCategories.log", print_r($page, true) . "\r\n", FILE_APPEND);

            file_put_contents("Log/unitCleaningCategories.log", print_r($error, true) . "\r\n", FILE_APPEND);
            //echo "ddd";
            $this->response($this->json($error), 203);
        }

        $inputData = json_decode(file_get_contents("php://input"), true);
        if (!isset($inputData['data']['unitTypeId']) || empty($inputData['data']['unitTypeId']) && !isset($inputData['data']['cleaningTypeId']) || empty($inputData['data']['cleaningTypeId'])) {
            $error = array("status" => "0", "msg" => "Request Blank.", "code" => "201");

            file_put_contents("Log/unitCleaningCategories.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/unitCleaningCategories.log", print_r($page, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/unitCleaningCategories.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/unitCleaningCategories.log", print_r($error, true) . "\r\n", FILE_APPEND);
            //echo "ddd";
            $this->response($this->json($error), 201);
        }
        $con = " Where tct.cleaning_type_id=" . $inputData['data']['cleaningTypeId'] . " AND tct.unit_type_id= " . $inputData['data']['unitTypeId'] . " AND tct.status='A' AND tc.status='A'";

        $query = "SELECT tc.category_id as room_category_id,tc.category_name as room_category,tct.minimum,tct.maximum FROM table_category_type as tct left join table_category as tc on tc.category_id=tct.category_id" . $con;

        $result = $this->database->query($query);
        if ($result->num_rows <= 0) {
            $error = array('status' => "0", "msg" => "Data not Found!.", "code" => "209");

            file_put_contents("Log/unitCleaningCategories.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/unitCleaningCategories.log", print_r($page, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/unitCleaningCategories.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/unitCleaningCategories.log", print_r($error, true) . "\r\n", FILE_APPEND);

            $this->response($this->json($error), 209);
        } else {
            $i = 0;
            while ($row = mysqli_fetch_assoc($result)) {
                $data[$i] = $row;
                $i++;
            }
        }

        $success = array("status" => "1", "msg" => "All Categories!.", "code" => "200", "data" => $data);
        file_put_contents("Log/unitCleaningCategories.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
        file_put_contents("Log/unitCleaningCategories.log", print_r($page, true) . "\r\n", FILE_APPEND);
        file_put_contents("Log/unitCleaningCategories.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
        file_put_contents("Log/unitCleaningCategories.log", print_r($success, true) . "\r\n", FILE_APPEND);
        $this->response($this->json($success), 200);
    }

    /*     * **************************************Locations by Ayush kumar on 29/nov/2017************* */

    public function locations() {
        $datetime = date('Y-m-d H:i:s');
        $page = "Locations.";
        if ($this->get_request_method() != 'GET') {

            $error = array("status" => "0", "msg" => "Invalid request method.", "code" => "203");

            file_put_contents("Log/locations.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/locations.log", print_r($page, true) . "\r\n", FILE_APPEND);

            file_put_contents("Log/locations.log", print_r($error, true) . "\r\n", FILE_APPEND);
            //echo "ddd";
            $this->response($this->json($error), 203);
        }

        $condition = " Where ta.status='A' AND tc.status='A'";
        $locationQuery = "Select ta.area_id,ta.area_name from city as tc left join table_area as ta on tc.city_id=ta.city_id" . $condition;
        $result = $this->database->query($locationQuery);
        //print_r($locationQuery);exit;		
        if ($result->num_rows > 0) {
            $data = array();
            $i = 0;
            while ($row = mysqli_fetch_assoc($result)) {
                $data[$i]['area_id'] = $row['area_id'];
                $data[$i]['area_name'] = trim($row['area_name']);
                $i++;
            }
            $success = array("status" => "1", "msg" => "All locations.", "code" => "200", "data" => $data);

            file_put_contents("Log/locations.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/locations.log", print_r($page, true) . "\r\n", FILE_APPEND);
            //file_put_contents("Log/locations.log",print_r($inputData,true)."\r\n", FILE_APPEND);
            file_put_contents("Log/locations.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/locations.log", print_r($success, true) . "\r\n", FILE_APPEND);

            $this->response($this->json($success), 200);
        } else {
            $error = array('status' => "0", "msg" => "Data Does Not Exist!.", "code" => "209");

            file_put_contents("Log/locations.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/locations.log", print_r($page, true) . "\r\n", FILE_APPEND);
            //file_put_contents("Log/locations.log",print_r($inputData,true)."\r\n", FILE_APPEND);
            file_put_contents("Log/locations.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/locations.log", print_r($error, true) . "\r\n", FILE_APPEND);

            $this->response($this->json($error), 209);
        }
    }

    /*     * ****************longTermServices by Ayush kumar on 29/nov/2017************* */

    public function longTermServices() {
        $datetime = date('Y-m-d H:i:s');
        $page = "Locations.";
        if ($this->get_request_method() != 'GET') {

            $error = array("status" => "0", "msg" => "Invalid request method.", "code" => "203");

            file_put_contents("Log/longTermServices.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/longTermServices.log", print_r($page, true) . "\r\n", FILE_APPEND);

            file_put_contents("Log/longTermServices.log", print_r($error, true) . "\r\n", FILE_APPEND);
            //echo "ddd";
            $this->response($this->json($error), 203);
        }

        $con = " Where trs.status='A'";
        $Query = "Select trs.service_id,trs.service_name,trs.number_of_services from table_room_services as trs" . $con;
        $result = $this->database->query($Query);
        //print_r($locationQuery);exit;		
        if ($result->num_rows > 0) {
            $data = array();
            $i = 0;
            while ($row = mysqli_fetch_assoc($result)) {
                $data[$i]['service_id'] = $row['service_id'];
                $data[$i]['service_name'] = trim($row['service_name']);
                $data[$i]['number_of_services'] = trim($row['number_of_services']);
                $i++;
            }
            $success = array("status" => "1", "msg" => "All services.", "code" => "200", "data" => $data);

            file_put_contents("Log/longTermServices.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/longTermServices.log", print_r($page, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/longTermServices.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/longTermServices.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/longTermServices.log", print_r($success, true) . "\r\n", FILE_APPEND);

            $this->response($this->json($success), 200);
        } else {
            $error = array('status' => "0", "msg" => "Data Does Not Exist!.", "code" => "209");

            file_put_contents("Log/longTermServices.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/longTermServices.log", print_r($page, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/longTermServices.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/longTermServices.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/longTermServices.log", print_r($error, true) . "\r\n", FILE_APPEND);

            $this->response($this->json($error), 209);
        }
    }

    /*     * ******************  Terms of services by Ayush Kumar on 27/nov/2017  ************ */

    private function termsOfServices() {

        $datetime = date('Y-m-d H:i:s');
        $page = "Terms Of Service.";
        $inputData = json_decode(file_get_contents("php://input"), true);
        if ($this->get_request_method() != 'GET') {

            $error = array("status" => "0", "msg" => "Invalid request method.", "code" => "203");

            file_put_contents("Log/customertermsOfServices.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/customertermsOfServices.log", print_r($page, true) . "\r\n", FILE_APPEND);

            file_put_contents("Log/customertermsOfServices.log", print_r($error, true) . "\r\n", FILE_APPEND);
            $this->response($this->json($error), 203);
        }

        $termsquery = "Select ttu.terms_id,ttu.terms_text from table_terms_of_use as ttu";


        $result = $this->database->query($termsquery);

        if ($result->num_rows > 0) {
            $data = array();
            while ($row = mysqli_fetch_assoc($result)) {

                $data[] = $row;
            }

            $success = array("status" => "1", "msg" => "Terms of Service.", "code" => "200", "data" => $data);

            file_put_contents("Log/customertermsOfServices.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/customertermsOfServices.log", print_r($page, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/customertermsOfServices.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/customertermsOfServices.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/customertermsOfServices.log", print_r($success, true) . "\r\n", FILE_APPEND);

            $this->response($this->json($success), 200);
        } else {
            $error = array('status' => "0", "msg" => "Data does not exist!.", "code" => "209");

            file_put_contents("Log/customertermsOfServices.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/customertermsOfServices.log", print_r($page, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/customertermsOfServices.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/customertermsOfServices.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/customerUpdateProfile.log", print_r($error, true) . "\r\n", FILE_APPEND);

            $this->response($this->json($error), 209);
        }
    }

// End of function




    /*     * ******************  customer Messages by Ayush Kumar on 27/nov/2017  ************ */

    private function customerMessages() {
        $datetime = date('Y-m-d H:i:s');
        $page = "Terms Of Service.";
        $inputData = json_decode(file_get_contents("php://input"), true);


        if (isset($inputData['data']) && !empty($inputData['data']) && isset($inputData['data']['customerId']) && !empty($inputData['data']['customerId']) && isset($inputData['data']['sessionId']) && !empty($inputData['data']['sessionId'])) {

            $customerId = trim($inputData['data']['customerId']);
            $sessionId = trim($inputData['data']['sessionId']);
            $response = $this->authenticateUser($customerId, $sessionId);
            if ($response) {


                $condition = " Where tm.customer_id=" . $customerId . " AND tm.status='A' order by send_time desc";
                $messagequery = "Select tm.subject,tm.message,tm.send_date,tm.customer_id from table_messages as tm left join table_customer as tc on tc.customer_id=tm.customer_id" . $condition;

                //print_r($messagequery);exit;		
                $result = $this->database->query($messagequery);

                if ($result->num_rows > 0) {
                    $data = array();
                    while ($row = mysqli_fetch_assoc($result)) {

                        $data[] = $row;
                    }

                    $success = array("status" => "1", "msg" => "Customer Messages.", "code" => "200", "data" => $data);

                    file_put_contents("Log/customerMessages.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
                    file_put_contents("Log/customerMessages.log", print_r($page, true) . "\r\n", FILE_APPEND);
                    file_put_contents("Log/customerMessages.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
                    file_put_contents("Log/customerMessages.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
                    file_put_contents("Log/customerMessages.log", print_r($success, true) . "\r\n", FILE_APPEND);

                    $this->response($this->json($success), 200);
                } else {
                    $error = array('status' => "0", "msg" => "Data does not exist!.", "code" => "209");

                    file_put_contents("Log/customerMessages.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
                    file_put_contents("Log/customerMessages.log", print_r($page, true) . "\r\n", FILE_APPEND);
                    file_put_contents("Log/customerMessages.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
                    file_put_contents("Log/customerMessages.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
                    file_put_contents("Log/customerMessages.log", print_r($error, true) . "\r\n", FILE_APPEND);

                    $this->response($this->json($error), 209);
                }
            } else {
                $error = array('status' => "0", "msg" => "No user found!", "code" => "201");

                file_put_contents("Log/customerMessages.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/customerMessages.log", print_r($page, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/customerMessages.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/customerMessages.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/customerMessages.log", print_r($error, true) . "\r\n", FILE_APPEND);

                $this->response($this->json($error), 201);
            }
        } else {
            $error = array('status' => "0", "msg" => "Request Blank!", "code" => "201");

            file_put_contents("Log/customerMessages.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/customerMessages.log", print_r($page, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/customerMessages.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/customerMessages.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/customerMessages.log", print_r($error, true) . "\r\n", FILE_APPEND);

            $this->response($this->json($error), 201);
        }
    }

// End of function




    /*     * ******************  Contact Us by Ayush Kumar on 27/nov/2017  ************ */

    private function contactUs() {

        $datetime = date('Y-m-d H:i:s');
        $page = "Contact Us.";
        $inputData = json_decode(file_get_contents("php://input"), true);


        if (isset($inputData['data']) && !empty($inputData['data']) && isset($inputData['data']['customerId']) && !empty($inputData['data']['customerId']) && isset($inputData['data']['sessionId']) && !empty($inputData['data']['sessionId']) && isset($inputData['data']['queryId']) && !empty($inputData['data']['queryId']) && isset($inputData['data']['message']) && !empty($inputData['data']['message'])) {

            $customerId = trim($inputData['data']['customerId']);
            $sessionId = trim($inputData['data']['sessionId']);
            $response = $this->authenticateUser($customerId, $sessionId);
            if ($response) {
                $queryId = trim($inputData['data']['queryId']);
                $message = trim($inputData['data']['message']);

                $condition = "Where ctm.query_id=" . $queryId . " AND ctm.status='A'";
                $contactQuery = "select ctm.* from table_contact_type_master as ctm " . $condition;
                $result1 = $this->database->query($contactQuery);
                if ($result1->num_rows <= 0) {

                    $error = array('status' => "0", "msg" => "Invalid Query Type.", "code" => "203");

                    file_put_contents("Log/contactUs.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
                    file_put_contents("Log/contactUs.log", print_r($page, true) . "\r\n", FILE_APPEND);
                    file_put_contents("Log/contactUs.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
                    file_put_contents("Log/contactUs.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
                    file_put_contents("Log/contactUs.log", print_r($error, true) . "\r\n", FILE_APPEND);

                    $this->response($this->json($error), 203);
                }

                $account_id = 1;
                $created_date = date('Y-m-d H:i:s');

                $contactInsert = "INSERT INTO table_contact_query(account_id,query_id,message,customer_id,created_date) VALUES ('" . $account_id . "','" . $queryId . "','" . $message . "','" . $customerId . "', '" . $created_date . "')";

                $result2 = $this->database->query($contactInsert);

                $cusQuery = "SELECT tc.* from table_customer as tc where tc.customer_id=" . $customerId . " AND tc.status='A'";
                $result3 = $this->database->query($cusQuery);
                $cusDetail = mysqli_fetch_assoc($result3);
                $customerEmail = $cusDetail['customer_email'];
                //$customerEmail="ayush.kumar@quytech.com";

                $message = '<html><body><table style="font-family: arial"; align="center" width="700" cellpadding="0" cellspacing="0">'.
	            '<tr><td colspan="2" style="border-bottom: 1px solid #fff"><img src="http://app.whitespot-cleaning.com/images/email/header.jpg"></td></tr>'.
	            '<tr><td colspan="2" align="center" bgcolor="#9cc3e6" style="padding: 10px 15px; color: #fff">'.
	            'Thank you and apologies for driving to complain!</td></tr>'.
	            '<tr><td bgcolor="#051922" colspan="2">'.
	            '<div style="font-size: 14px; color: #fff; line-height: 28px; padding: 5px 18px; width:345px;display: inline-block; vertical-align: middle; box-sizing: border-box">'.
			'Whitespot Cleaning Services L.L.C<br>'.
			'P.O. Box 4080, Dubai,UAE<br>'.
			'<a href="Tel:+97145539799" style="color: #fff; text-decoration: none">Tel: +97145539799</a><br>'.
			'<a href="info@whitespot-cleaning.com" style="color: #fff;text-decoration: none">info@whitespot-cleaning.com</a>'.
	            '</div>'.
	            '<div style="padding: 0 10px  0 15px; width:350px;display: inline-block; vertical-align: middle; box-sizing: border-box; text-align: right">'.
	            '<span style="display: inline-block; vertical-align: bottom"><a href="https://www.instagram.com/wscleaning/"><img src="http://app.whitespot-cleaning.com/images/email/ig-icon.jpg"></a><a href="https://www.facebook.com/whitespotcleaning/"><img src="http://app.whitespot-cleaning.com/images/email/fb-icon.jpg"></a></span>'.
	            '<div style="display: inline-block; vertical-align: bottom; margin-left: 25px">'.
	                '<span style="margin-bottom: 15px; display: block">'.
	                '<a href=""><img src="http://app.whitespot-cleaning.com/images/email/icon-2.png"></a>'.
	                '</span>'.
	                '<span><a href=""><img src="http://app.whitespot-cleaning.com/images/email/icon-1.png"></a></span>'.
			'</div></div></td></tr></table></body></html>';

	            $res = $this->sendEmail($customerEmail, $message, 'Complain | Whitespot Cleaning');
	            $res = $this->sendEmail('hisham@whitespot-cleaning.com', $message, 'Complain | Whitespot Cleaning');
				

                $success = array("status" => "1", "msg" => "Your message has been received. We will be contacting you shortly.", "code" => "200");

                file_put_contents("Log/contactUs.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/contactUs.log", print_r($page, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/contactUs.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/contactUs.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/contactUs.log", print_r($success, true) . "\r\n", FILE_APPEND);

                $this->response($this->json($success), 200);
            } else {
                $error = array('status' => "0", "msg" => "No user found!", "code" => "203");

                file_put_contents("Log/contactUs.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/contactUs.log", print_r($page, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/contactUs.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/contactUs.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/contactUs.log", print_r($error, true) . "\r\n", FILE_APPEND);

                $this->response($this->json($error), 203);
            }
        } else {
            $error = array('status' => "0", "msg" => "Request Blank!", "code" => "201");

            file_put_contents("Log/contactUs.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/contactUs.log", print_r($page, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/contactUs.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/contactUs.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/contactUs.log", print_r($error, true) . "\r\n", FILE_APPEND);

            $this->response($this->json($error), 201);
        }
    }

// End of function




    /*     * ******************  Contact Us by Ayush Kumar on 27/nov/2017  ************ */

    private function GetContactQueries() {

        $datetime = date('Y-m-d H:i:s');
        $page = "Contact Us Queries.";
        $inputData = json_decode(file_get_contents("php://input"), true);


        if (isset($inputData['data']) && !empty($inputData['data']) && isset($inputData['data']['customerId']) && !empty($inputData['data']['customerId']) && isset($inputData['data']['sessionId']) && !empty($inputData['data']['sessionId'])) {

            $customerId = trim($inputData['data']['customerId']);
            $sessionId = trim($inputData['data']['sessionId']);
            $response = $this->authenticateUser($customerId, $sessionId);
            if ($response) {


                $condition = "Where status='A'";
                $contactQuery = "select ctm.query_id,ctm.query_type_text from table_contact_type_master as ctm " . $condition;
                $result = $this->database->query($contactQuery);




                if ($result->num_rows > 0) {
                    $data = array();
                    while ($row = mysqli_fetch_assoc($result)) {
                        $data[] = $row;
                    }


                    $success = array("status" => "1", "msg" => "All queries of contact.", "code" => "200", "data" => $data);

                    file_put_contents("Log/GetContactQueries.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
                    file_put_contents("Log/GetContactQueries.log", print_r($page, true) . "\r\n", FILE_APPEND);
                    file_put_contents("Log/GetContactQueries.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
                    file_put_contents("Log/GetContactQueries.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
                    file_put_contents("Log/GetContactQueries.log", print_r($success, true) . "\r\n", FILE_APPEND);

                    $this->response($this->json($success), 200);
                } else {
                    $error = array('status' => "0", "msg" => "Data does not exist.", "code" => "209");

                    file_put_contents("Log/GetContactQueries.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
                    file_put_contents("Log/GetContactQueries.log", print_r($page, true) . "\r\n", FILE_APPEND);
                    file_put_contents("Log/GetContactQueries.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
                    file_put_contents("Log/GetContactQueries.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
                    file_put_contents("Log/GetContactQueries.log", print_r($error, true) . "\r\n", FILE_APPEND);

                    $this->response($this->json($error), 500);
                }
            } else {
                $error = array('status' => "0", "msg" => "No user found!", "code" => "203");

                file_put_contents("Log/GetContactQueries.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/GetContactQueries.log", print_r($page, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/GetContactQueries.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/GetContactQueries.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/GetContactQueries.log", print_r($error, true) . "\r\n", FILE_APPEND);

                $this->response($this->json($error), 203);
            }
        } else {
            $error = array('status' => "0", "msg" => "Request Blank!", "code" => "201");

            file_put_contents("Log/GetContactQueries.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/GetContactQueries.log", print_r($page, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/GetContactQueries.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/GetContactQueries.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/GetContactQueries.log", print_r($error, true) . "\r\n", FILE_APPEND);

            $this->response($this->json($error), 201);
        }
    }

// End of function





    /*     * ******************  Customer Feedback by Ayush Kumar on 27/nov/2017  ************ */

    private function getCusFeedback() {

        $datetime = date('Y-m-d H:i:s');
        $page = "Customer Feedback.";
        $inputData = json_decode(file_get_contents("php://input"), true);


        if (isset($inputData['data']) && !empty($inputData['data']) && isset($inputData['data']['customerId']) && !empty($inputData['data']['customerId']) && isset($inputData['data']['sessionId']) && !empty($inputData['data']['sessionId']) && isset($inputData['data']['rating']) && !empty($inputData['data']['rating']) && isset($inputData['data']['comment']) && isset($inputData['data']['bookingId']) && !empty($inputData['data']['bookingId'])) {

            $customerId = trim($inputData['data']['customerId']);
            $sessionId = trim($inputData['data']['sessionId']);

            $response = $this->authenticateUser($customerId, $sessionId);
            if ($response) {
                //Check Booking Id is valid or not
                $bookingId = trim($inputData['data']['bookingId']);
                $rating = ceil(trim($inputData['data']['rating']));
                $comment = trim($inputData['data']['comment']);
                $account_id = 1;
                $status = 'A';
                $feedback_type = 'customer';
                $created_date = date('Y-m-d H:i:s');

                $feedbackInsert = "INSERT INTO table_service_feedback(account_id,star_rating,comments,booking_id,customer_id,feedback_type,created_date,status) VALUES ('" . $account_id . "','" . $rating . "','" . $comment . "','" . $bookingId . "','" . $customerId . "','" . $feedback_type . "', '" . $created_date . "','" . $status . "')";

                $result = $this->database->query($feedbackInsert);


                $message = '<html><body><table style="font-family: arial"; align="center" width="700" cellpadding="0" cellspacing="0">'.
	            '<tr><td colspan="2" style="border-bottom: 1px solid #fff"><img src="http://app.whitespot-cleaning.com/images/email/header.jpg"></td></tr>'.
	            '<tr><td colspan="2" align="center" bgcolor="#9cc3e6" style="padding: 10px 15px; color: #fff">'.
	            'Thank you for your Feedback!</td></tr>'.
	            '<tr><td bgcolor="#051922" colspan="2">'.
	            '<div style="font-size: 14px; color: #fff; line-height: 28px; padding: 5px 18px; width:345px;display: inline-block; vertical-align: middle; box-sizing: border-box">'.
			'Whitespot Cleaning Services L.L.C<br>'.
			'P.O. Box 4080, Dubai,UAE<br>'.
			'<a href="Tel:+97145539799" style="color: #fff; text-decoration: none">Tel: +97145539799</a><br>'.
			'<a href="info@whitespot-cleaning.com" style="color: #fff;text-decoration: none">info@whitespot-cleaning.com</a>'.
	            '</div>'.
	            '<div style="padding: 0 10px  0 15px; width:350px;display: inline-block; vertical-align: middle; box-sizing: border-box; text-align: right">'.
	            '<span style="display: inline-block; vertical-align: bottom"><a href="https://www.instagram.com/wscleaning/"><img src="http://app.whitespot-cleaning.com/images/email/ig-icon.jpg"></a><a href="https://www.facebook.com/whitespotcleaning/"><img src="http://app.whitespot-cleaning.com/images/email/fb-icon.jpg"></a></span>'.
	            '<div style="display: inline-block; vertical-align: bottom; margin-left: 25px">'.
	                '<span style="margin-bottom: 15px; display: block">'.
	                '<a href=""><img src="http://app.whitespot-cleaning.com/images/email/icon-2.png"></a>'.
	                '</span>'.
	                '<span><a href=""><img src="http://app.whitespot-cleaning.com/images/email/icon-1.png"></a></span>'.
			'</div></div></td></tr></table></body></html>';


				$userProfileSql = " SELECT tc.customer_email FROM table_customer AS tc  WHERE  tc.customer_id = '" . $inputData['data']['customerId'] . "'";

	            $cusQuery = $this->database->query($userProfileSql);
	            $queryResult = mysqli_fetch_assoc($cusQuery);
	            $data = $queryResult;
	            // print_r($data);die;

	            $res = $this->sendEmail($data['customer_email'], $message, 'Feedback | Whitespot Cleaning');
	            $res = $this->sendEmail('hisham@whitespot-cleaning.com', $message, 'Feedback | Whitespot Cleaning');

                $success = array("status" => "1", "msg" => "Thank you for your Feedback!.", "code" => "200");

                file_put_contents("Log/getCusFeedback.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/getCusFeedback.log", print_r($page, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/getCusFeedback.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/getCusFeedback.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/getCusFeedback.log", print_r($success, true) . "\r\n", FILE_APPEND);

                $this->response($this->json($success), 200);
            } else {
                $error = array('status' => "0", "msg" => "No user found!", "code" => "203");

                file_put_contents("Log/getCusFeedback.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/getCusFeedback.log", print_r($page, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/getCusFeedback.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/getCusFeedback.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/getCusFeedback.log", print_r($error, true) . "\r\n", FILE_APPEND);

                $this->response($this->json($error), 203);
            }
        } else {
            $error = array('status' => "0", "msg" => "Request Blank!", "code" => "201");

            file_put_contents("Log/getCusFeedback.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/getCusFeedback.log", print_r($page, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/getCusFeedback.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/getCusFeedback.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/getCusFeedback.log", print_r($error, true) . "\r\n", FILE_APPEND);

            $this->response($this->json($error), 201);
        }
    }

// End of function



    /*     * ********************Customer logout By Ayush kumar on  29/nov/2017********************************* */

    public function logout() {

        $datetime = date('Y-m-d H:i:s');
        $page = "Customer Logout.";
        $inputData = json_decode(file_get_contents("php://input"), true);


        if (isset($inputData['data']) && !empty($inputData['data']) && isset($inputData['data']['customerId']) && !empty($inputData['data']['customerId']) && isset($inputData['data']['sessionId']) && !empty($inputData['data']['sessionId'])) {

            $customerId = trim($inputData['data']['customerId']);
            $sessionId = trim($inputData['data']['sessionId']);
            $response = $this->authenticateUser($customerId, $sessionId);
            if ($response) {


                $condition = " Where customer_id=" . $customerId;
                $updateQuery = "UPDATE table_customer set session_id=''" . $condition;
                //print_r($updateQuery);exit;
                $result = $this->database->query($updateQuery);


                $success = array("status" => "1", "msg" => "Logout successfully.", "code" => "200");

                file_put_contents("Log/customerlogout.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/customerlogout.log", print_r($page, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/customerlogout.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/customerlogout.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/customerlogout.log", print_r($success, true) . "\r\n", FILE_APPEND);

                $this->response($this->json($success), 200);
            } else {
                $error = array('status' => "0", "msg" => "No user found!", "code" => "203");

                file_put_contents("Log/customerlogout.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/customerlogout.log", print_r($page, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/customerlogout.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/customerlogout.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/customerlogout.log", print_r($error, true) . "\r\n", FILE_APPEND);

                $this->response($this->json($error), 203);
            }
        } else {
            $error = array('status' => "0", "msg" => "Request Blank!", "code" => "201");

            file_put_contents("Log/customerlogout.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/customerlogout.log", print_r($page, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/customerlogout.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/customerlogout.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/customerlogout.log", print_r($error, true) . "\r\n", FILE_APPEND);

            $this->response($this->json($error), 201);
        }
    }

    /*     * **************************************Buildings by Ayush kumar on 29/nov/2017************* */
    /* public function buildings(){
      $datetime = date('Y-m-d H:i:s');
      $page = "Customer Unit Type To Cost Master.";
      if($this->get_request_method()!='GET'){

      $error = array("status" =>"0", "msg" => "Invalid request method.","code"=>"203");

      file_put_contents("Log/buildings.log",print_r($datetime,true)."\r\n", FILE_APPEND);
      file_put_contents("Log/buildings.log",print_r($page,true)."\r\n", FILE_APPEND);

      file_put_contents("Log/buildings.log",print_r($error,true)."\r\n", FILE_APPEND);
      //echo "ddd";
      $this->response($this->json($error),203);

      }

      $condition=" Where tb.status='A'";
      $buildingQuery="Select tb.building_id, tb.building_name,tb.location,tb.lat,tb.lng from table_buildings as tb".$condition;
      $result = $this->database->query($buildingQuery);

      if($result->num_rows>0){
      $data=array();
      while($row=mysqli_fetch_assoc($result)){
      $data[]=$row;
      }
      $success = array("status"=>"1", "msg" => "All Buildings.","code"=>"200","data"=>$data);

      file_put_contents("Log/buildings.log",print_r($datetime,true)."\r\n", FILE_APPEND);
      file_put_contents("Log/buildings.log",print_r($page,true)."\r\n", FILE_APPEND);
      file_put_contents("Log/buildings.log",print_r($inputData,true)."\r\n", FILE_APPEND);
      file_put_contents("Log/buildings.log",print_r(file_get_contents("php://input"),true)."\r\n", FILE_APPEND);
      file_put_contents("Log/buildings.log",print_r($success,true)."\r\n", FILE_APPEND);

      $this->response($this->json($success),200);

      }else{
      $error = array('status' => "0", "msg" => "Data Does Not Exist!.","code"=>"209");

      file_put_contents("Log/buildings.log",print_r($datetime,true)."\r\n", FILE_APPEND);
      file_put_contents("Log/buildings.log",print_r($page,true)."\r\n", FILE_APPEND);
      file_put_contents("Log/buildings.log",print_r($inputData,true)."\r\n", FILE_APPEND);
      file_put_contents("Log/buildings.log",print_r(file_get_contents("php://input"),true)."\r\n", FILE_APPEND);
      file_put_contents("Log/buildings.log",print_r($error,true)."\r\n", FILE_APPEND);

      $this->response($this->json($error),209);
      }

      }
     */



    /*     * **************************************saveCard by Ayush kumar on 29/nov/2017************* */

    private function saveCard() {

        $datetime = date('Y-m-d H:i:s');
        $page = "Customer Save Card.";
        $inputData = json_decode(file_get_contents("php://input"), true);

        if ($this->get_request_method() != 'POST') {

            $error = array("status" => "0", "msg" => "Invalid request method.", "code" => "203");

            file_put_contents("Log/saveCard.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/saveCard.log", print_r($page, true) . "\r\n", FILE_APPEND);

            file_put_contents("Log/saveCard.log", print_r($error, true) . "\r\n", FILE_APPEND);
            //echo "ddd";
            $this->response($this->json($error), 203);
        }

        if (isset($inputData['data']) && !empty($inputData['data']) && isset($inputData['data']['customerId']) && !empty($inputData['data']['customerId']) && isset($inputData['data']['sessionId']) && !empty($inputData['data']['sessionId']) && isset($inputData['data']['cardHolderName']) && !empty($inputData['data']['cardHolderName']) && isset($inputData['data']['cardNumber']) && !empty($inputData['data']['cardNumber']) && isset($inputData['data']['expiryDate']) && !empty($inputData['data']['expiryDate']) && isset($inputData['data']['cvv_number']) && !empty($inputData['data']['cvv_number'])) {

            $customerId = trim($inputData['data']['customerId']);
            $sessionId = trim($inputData['data']['sessionId']);

            $response = $this->authenticateUser($customerId, $sessionId);
            if ($response) {
                $condition = " Where card_number=" . trim($inputData['data']['cardNumber']);
                $checkCardQuery = "SELECT * from table_saved_cards as tsc" . $condition;
                $result = $this->database->query($checkCardQuery);

                if ($result->num_rows > 0) {

                    $error = array('status' => "0", "msg" => "Card already saved!.", "code" => "212");

                    file_put_contents("Log/saveCard.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
                    file_put_contents("Log/saveCard.log", print_r($page, true) . "\r\n", FILE_APPEND);
                    file_put_contents("Log/saveCard.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
                    file_put_contents("Log/saveCard.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
                    file_put_contents("Log/saveCard.log", print_r($error, true) . "\r\n", FILE_APPEND);

                    $this->response($this->json($error), 212);
                }

                $condition = " Where customer_id=" . $customerId;
                $checkcusCardQuery = "SELECT * from table_saved_cards as tsc" . $condition;
                $result2 = $this->database->query($checkcusCardQuery);
                if ($result2->num_rows > 0) {

                    $condition = " Where customer_id=" . $customerId;
                    $checkCardQuery = "UPDATE table_saved_cards SET default_card=0" . $condition;
                    $result = $this->database->query($checkCardQuery);
                }

                //Check Booking Id is valid or not
                $cardHolderName = trim($inputData['data']['cardHolderName']);
                $cardNumber = trim($inputData['data']['cardNumber']);
                $cvv_number = trim($inputData['data']['cvv_number']);
                $expiryDate = explode("/", trim($inputData['data']['expiryDate']));
                $expiry_month = $expiryDate[0];
                $expiry_year = $expiryDate[1];
                $default_card = 1;
                $account_id = 1;
                $status = 'A';
                $created_date = date('Y-m-d H:i:s');
                $last_update_date = date('Y-m-d H:i:s');

                $cardInsert = "INSERT INTO table_saved_cards(account_id,card_holder_name,customer_id,card_number,expiry_month,expiry_year,cvv_number,created_date,last_update_date,status,default_card) VALUES ('" . $account_id . "','" . $cardHolderName . "','" . $customerId . "','" . $cardNumber . "','" . $expiry_month . "','" . $expiry_year . "','" . $cvv_number . "','" . $created_date . "','" . $last_update_date . "','" . $status . "','" . $default_card . "')";

                $result = $this->database->query($cardInsert);
                $success = array("status" => "1", "msg" => "Your card has been successfully saved!.", "code" => "200");

                file_put_contents("Log/saveCard.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/saveCard.log", print_r($page, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/saveCard.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/saveCard.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/saveCard.log", print_r($success, true) . "\r\n", FILE_APPEND);

                $this->response($this->json($success), 200);
            } else {
                $error = array('status' => "0", "msg" => "No user found!", "code" => "203");

                file_put_contents("Log/saveCard.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/saveCard.log", print_r($page, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/saveCard.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/saveCard.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/saveCard.log", print_r($error, true) . "\r\n", FILE_APPEND);

                $this->response($this->json($error), 203);
            }
        } else {
            $error = array('status' => "0", "msg" => "Request Blank!", "code" => "201");

            file_put_contents("Log/saveCard.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/saveCard.log", print_r($page, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/saveCard.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/saveCard.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/saveCard.log", print_r($error, true) . "\r\n", FILE_APPEND);

            $this->response($this->json($error), 201);
        }
    }

    /*     * **************************************Customer Cards by Ayush kumar on 30/nov/2017************* */

    private function getCustomerCards() {

        $datetime = date('Y-m-d H:i:s');
        $page = "Customer Get Card.";
        $inputData = json_decode(file_get_contents("php://input"), true);

        if ($this->get_request_method() != 'POST') {

            $error = array("status" => "0", "msg" => "Invalid request method.", "code" => "203");

            file_put_contents("Log/getCustomerCards.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/getCustomerCards.log", print_r($page, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/getCustomerCards.log", print_r($error, true) . "\r\n", FILE_APPEND);
            //echo "ddd";
            $this->response($this->json($error), 203);
        }

        if (isset($inputData['data']) && !empty($inputData['data']) && isset($inputData['data']['customerId']) && !empty($inputData['data']['customerId']) && isset($inputData['data']['sessionId']) && !empty($inputData['data']['sessionId'])) {

            $customerId = trim($inputData['data']['customerId']);
            $sessionId = trim($inputData['data']['sessionId']);

            $response = $this->authenticateUser($customerId, $sessionId);
            if ($response) {
                $data = array();
                $condition = " Where customer_id=" . $customerId . " AND status='A' AND save_card=1";
                $checkCardQuery = "SELECT * from table_saved_cards as tsc" . $condition;
                $result = $this->database->query($checkCardQuery);

                if ($result->num_rows <= 0) {

                    $error = array('status' => "0", "msg" => "No card available!.", "code" => "209");

                    file_put_contents("Log/getCustomerCards.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
                    file_put_contents("Log/getCustomerCards.log", print_r($page, true) . "\r\n", FILE_APPEND);
                    file_put_contents("Log/getCustomerCards.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
                    file_put_contents("Log/getCustomerCards.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
                    file_put_contents("Log/getCustomerCards.log", print_r($error, true) . "\r\n", FILE_APPEND);

                    $this->response($this->json($error), 209);
                }

                while ($row = mysqli_fetch_assoc($result)) {
                    $data[] = $row;
                }

                $success = array("status" => "1", "msg" => "Your all cards!.", "code" => "200", "data" => $data);

                file_put_contents("Log/getCustomerCards.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/getCustomerCards.log", print_r($page, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/getCustomerCards.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/getCustomerCards.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/getCustomerCards.log", print_r($success, true) . "\r\n", FILE_APPEND);

                $this->response($this->json($success), 200);
            } else {
                $error = array('status' => "0", "msg" => "No user found!", "code" => "203");

                file_put_contents("Log/getCustomerCards.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/getCustomerCards.log", print_r($page, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/getCustomerCards.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/getCustomerCards.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/getCustomerCards.log", print_r($error, true) . "\r\n", FILE_APPEND);

                $this->response($this->json($error), 203);
            }
        } else {
            $error = array('status' => "0", "msg" => "Request Blank!", "code" => "201");

            file_put_contents("Log/getCustomerCards.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/getCustomerCards.log", print_r($page, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/getCustomerCards.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/getCustomerCards.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/getCustomerCards.log", print_r($error, true) . "\r\n", FILE_APPEND);

            $this->response($this->json($error), 201);
        }
    }

    /*     * ************************Calendar Slots by Ayush kumar on 30/nov/2017*************************** */

    private function getDaySlots() {

        $datetime = date('Y-m-d H:i:s');
        $page = "getDaySlots.";
        $inputData = json_decode(file_get_contents("php://input"), true);

        if ($this->get_request_method() != 'GET') {

            $error = array("status" => "0", "msg" => "Invalid request method.", "code" => "203");

            file_put_contents("Log/getDaySlots.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/getDaySlots.log", print_r($page, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/getDaySlots.log", print_r($error, true) . "\r\n", FILE_APPEND);
            //echo "ddd";
            $this->response($this->json($error), 203);
        }
        $data = array();
        $day = strtolower(trim($inputData['data']['day']));
        $today_date = date('Y-m-d');
        $condition = " where status='A' order by tcs.slot_start_time";

        $checkSlotQuery = "SELECT tcs.slot_id,tcs.slot_start_time,tcs.slot_end_time from table_calender_slots as tcs" . $condition;
        $result = $this->database->query($checkSlotQuery);
        if ($result->num_rows <= 0) {

            $error = array('status' => "0", "msg" => "No Slot available!.", "code" => "209");

            file_put_contents("Log/getDaySlots.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/getDaySlots.log", print_r($page, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/getDaySlots.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/getDaySlots.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/getDaySlots.log", print_r($error, true) . "\r\n", FILE_APPEND);

            $this->response($this->json($error), 209);
        }

        while ($row = mysqli_fetch_assoc($result)) {
            $row['slot_start_time'] = date('h:i A', strtotime($row['slot_start_time']));
            $row['slot_end_time'] = date('h:i A', strtotime($row['slot_end_time']));
            $data[] = $row;
        }

        $success = array("status" => "1", "msg" => "All slots!.", "code" => "200", "data" => $data);

        file_put_contents("Log/getDaySlots.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
        file_put_contents("Log/getDaySlots.log", print_r($page, true) . "\r\n", FILE_APPEND);
        file_put_contents("Log/getDaySlots.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
        file_put_contents("Log/getDaySlots.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
        file_put_contents("Log/getDaySlots.log", print_r($success, true) . "\r\n", FILE_APPEND);

        $this->response($this->json($success), 200);
    }

    /*     * **********Cost Estimation by Ayush kumar on 30/nov/2017*************** */

    private function costEstimation() {
        $datetime = date('Y-m-d H:i:s');
        $page = "Cost Estimation.";
        $inputData = json_decode(file_get_contents("php://input"), true);

        if ($this->get_request_method() != 'POST') {

            $error = array("status" => "0", "msg" => "Invalid request method.", "code" => "203");

            file_put_contents("Log/cusCostEstimation.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/cusCostEstimation.log", print_r($page, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/cusCostEstimation.log", print_r($error, true) . "\r\n", FILE_APPEND);
            $this->response($this->json($error), 203);
        }

        if (isset($inputData['data']) && !empty($inputData['data']) && isset($inputData['data']['cleaningTypeId']) && !empty($inputData['data']['cleaningTypeId']) && isset($inputData['data']['unitTypeId']) && !empty($inputData['data']['unitTypeId']) && isset($inputData['data']['paymentOption']) && isset($inputData['data']['serviceType']) && !empty($inputData['data']['serviceType']) && isset($inputData['data']['serviceId']) && isset($inputData['data']['rooms']) && ($inputData['data']['rooms'] != '') && isset($inputData['data']['service']) && isset($inputData['data']['startDate']) && isset($inputData['data']['endDate']) && isset($inputData['data']['linen_change']) && isset($inputData['data']['balcony'])) {

            if ($inputData['data']['startDate'] != '' && $inputData['data']['endDate'] != '' && strtolower($inputData['data']['paymentOption']) == 'others') {
                $allSelectedCategory = $inputData['data']['rooms'];
                $amount = 0;
                $extraAmount = 0;
                $startDate = date('Y-m-d', strtotime($inputData['data']['startDate']));
                $endDate = date('Y-m-d', strtotime($inputData['data']['endDate']));
                $incr = 1;
                $endDate2 = date('Y-m-d', strtotime('+' . $incr . ' days', strtotime($endDate)));
                //echo $endDate2;die;


                foreach ($allSelectedCategory as $key => $category) {
                    if ($category['counter'] != 0) {

                        $condition = " Where trp.category_id=" . $category['roomCategoryId'] . " AND trp.counter=" . $category['counter'] . " AND trp.unit_type_id=" . $inputData['data']['unitTypeId'] . " AND trp.cleaning_type_id=" . $inputData['data']['cleaningTypeId'] . " AND trp.service_id=0 AND trp.status='A'";

                        $estQuery = "SELECT trp.* from table_room_packages as trp LEFT JOIN table_category as tc on trp.category_id=tc.category_id" . $condition;

                        $result = $this->database->query($estQuery);


                        if ($result->num_rows > 0) {

                            $arrResul = mysqli_fetch_assoc($result);
                            $amount+=$arrResul['package_rate'];
                            if ($inputData['data']['linen_change'] == 1) {

                                $extraAmount+=$arrResul['linen_change_per_room'];
                                $amount+=$arrResul['linen_change_per_room'];
                            }
                            if ($inputData['data']['balcony'] ==1 ) {
                                $extraAmount+=$arrResul['balcony_price'];
                                $amount+=$arrResul['balcony_price'];
                            }
                        }
                    }
                }

                // echo $amount ; exit;

                if ($amount <= 0) {
                    $error = array('status' => "0", "msg" => "This package not available!.", "code" => "209");

                    file_put_contents("Log/cusCostEstimation.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
                    file_put_contents("Log/cusCostEstimation.log", print_r($page, true) . "\r\n", FILE_APPEND);
                    file_put_contents("Log/cusCostEstimation.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
                    file_put_contents("Log/cusCostEstimation.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
                    file_put_contents("Log/cusCostEstimation.log", print_r($error, true) . "\r\n", FILE_APPEND);

                    $this->response($this->json($error), 209);
                }
                //Find all Services
                $slotArray = array();
                $dateAssArray = array("sunday" => 7, "monday" => 1, "tuesday" => 2, "wednesday" => 3, "thursday" => 4, "friday" => 5, "saturday" => 6);
                $start = new DateTime($startDate);
                $end = new DateTime($endDate2);

                $interval = DateInterval::createFromDateString('1 day');
                $period = new DatePeriod($start, $interval, $end);

                $serviceArray = $inputData['data']['service'];
                $i = 0;
                foreach ($serviceArray as $key => $slot) {

                    $dayValue = $dateAssArray[strtolower($slot['day'])];
                    $slotCon = " Where tcs.status='A' and tcs.slot_id = '" . $slot['slotId'] . "'";
                    $slotQuery = "SELECT tcs.slot_start_time,tcs.slot_end_time from table_calender_slots as tcs " . $slotCon;

                    $sotResult = $this->database->query($slotQuery);
                    $row = mysqli_fetch_assoc($sotResult);

                    foreach ($period as $dt) {
                        if ($dt->format("N") == $dayValue) {
                            $slotArray[$i]['cleaning_date'] = date('Y-m-d', strtotime($dt->format("l Y-m-d")));
                            $slotArray[$i]['slot_start_time'] = date('h:i A', strtotime($row['slot_start_time']));
                            $slotArray[$i]['slot_end_time'] = date('h:i A', strtotime($row['slot_end_time']));
                            $slotArray[$i]['slot_id'] = date('h:i A', strtotime($row['slot_id']));
                            $i++;
                        } else {
                            continue;
                        }
                        $i++;
                    }
                }
                $toatalServices = count($slotArray);
                asort($slotArray);
                $amount = $amount * $toatalServices;
                $extraAmount = $extraAmount * $toatalServices;
                /*                 * VAT Query */
                /* $vatQuery="Select vat from table_config where status='A'";
                  $vatQueryRes = $this->database->query($vatQuery);
                  $vatRes=mysqli_fetch_assoc($vatQueryRes);

                  if(!empty($vatRes)){
                  $amount=$amount+($amount*$vatRes['vat'])/100;
                  } */
                /*                 * End VAT Query** */
                $amount = round($amount, 2);
                $data['amount'] = "$amount";
                $data['extraAmount'] = $extraAmount;
                
                
                $slotArrrayTemp = array();
                foreach ($slotArray as $temp) {
                    $building_id = $inputData['data']['building_id'];
                    $availableSlots = $this->getServiceStatus($building_id, $temp['cleaning_date'], date("H:i:s", strtotime($temp['slot_start_time'])), date("H:i:s", strtotime($temp['slot_end_time'])));
                    $temp['services'] = array();
                    $otherAvailableTimeSlots = array();
                    if ($availableSlots == 0) {
                        $otherTimeSlot = $this->getOtherTimeSlot($building_id, $temp['cleaning_date']);
                        if(isset($otherTimeSlot) && !empty($otherTimeSlot)) {
                            $otherAvailableTimeSlots =  $otherTimeSlot;
                        } else {
                            //yesteday
                            $otherTimeSlot = $this->getOtherTimeSlot($building_id, date('Y-m-d',strtotime('-1 days',strtotime($temp['cleaning_date']))));
                            $otherAvailableTimeSlots = $otherTimeSlot;
                            //tomorrow 
                            $otherTimeSlot = $this->getOtherTimeSlot($building_id, date('Y-m-d',strtotime('+1 days',strtotime($temp['cleaning_date']))));
                            $otherAvailableTimeSlots = array_merge($otherAvailableTimeSlots,$otherTimeSlot);
                        }
                        $temp['services'] = $otherAvailableTimeSlots;
                    }

                    $paymentSql = "INSERT INTO table_booking_allotment_temp(cleaning_date,buidling_id,is_available,slot_id)VALUES('" . $temp['cleaning_date'] . "','" . $building_id . "','" . $availableSlots . "','" . $temp['slot_id'] . "')";

                    $result = $this->database->query($paymentSql);
                    $paymentId = $this->database->insert_id;
                    $temp['service_id'] = $paymentId;
                    $temp['is_available'] = $availableSlots;
                    $slotArrrayTemp[] = $temp;
                }
                
                $data['services'] = array_values($slotArrrayTemp);
                $success = array("status" => "1", "msg" => "Amount Estimation!.", "code" => "200", "data" => $data);

                file_put_contents("Log/cusCostEstimation.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/cusCostEstimation.log", print_r($page, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/cusCostEstimation.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/cusCostEstimation.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/cusCostEstimation.log", print_r($success, true) . "\r\n", FILE_APPEND);

                $this->response($this->json($success), 200);
            } else {

                $allSelectedCategory = $inputData['data']['rooms'];
                $amount = 0;
                $extraAmount = 0;

                //Find Total Services And End Date
                $startDate = date('Y-m-d', strtotime($inputData['data']['startDate']));
                $strdate = strtotime($startDate);
                if (($inputData['data']['serviceType'] == "l") && ($inputData['data']['paymentOption'] != "")) {
                    if (strtolower($inputData['data']['paymentOption']) == "monthly") {
                        $recCounter = 4;
                        $totalDays = 28;
                    } elseif (strtolower($inputData['data']['paymentOption']) == "yearly") {
                        $recCounter = 4 * 12;
                        $totalDays = 28 * 12;
                    } elseif (strtolower($inputData['data']['paymentOption']) == "quartly") {
                        $recCounter = 4 * 3;
                        $totalDays = 28 * 3;
                    }
                    $endDate = date('Y-m-d', strtotime('+' . $totalDays . ' days', $strdate));
                    /*                     * **find No of service**** */
                    $serCond = " Where service_id=" . $inputData['data']['serviceId'];
                    $serviceQuery = "SELECT trs.number_of_services,trs.service_id from table_room_services as trs " . $serCond;

                    $serviceResult = $this->database->query($serviceQuery);
                    $serviceData = mysqli_fetch_assoc($serviceResult);
                    $noofserv = $serviceData['number_of_services'];
                    //echo $noofserv;die;

                    /*                     * ***End** */
                } else {
                    $recCounter = 1;
                    $totalDays = 1;
                    $noofserv = 1;
                    $endDate = date('Y-m-d', strtotime('+' . $totalDays . ' days', $strdate));
                }
                //End Calculation of End Date






                foreach ($allSelectedCategory as $key => $category) {
                    if ($category['counter'] != 0) {

                        $condition = " Where trp.category_id=" . $category['roomCategoryId'] . " AND trp.counter=" . $category['counter'] . " AND trp.unit_type_id=" . $inputData['data']['unitTypeId'] . " AND trp.cleaning_type_id=" . $inputData['data']['cleaningTypeId'] . " AND trp.service_id=" . $inputData['data']['serviceId']." AND trp.status='A'";

                        $estQuery = "SELECT trp.* from table_room_packages as trp LEFT JOIN table_category as tc on trp.category_id=tc.category_id" . $condition;
                        //print_r($estQuery);die;
                        $result = $this->database->query($estQuery);

                        //print_r($result);die;
                        if ($result->num_rows > 0) {

                            $arrResul = mysqli_fetch_assoc($result);
                            $amount+=$arrResul['package_rate'];
                            $amount = $amount * $recCounter;
                        }

                        //echo $inputData['data']['linen_change'];die;
                        if ($inputData['data']['linen_change'] == 1) {
                            $extraAmount+=$arrResul['linen_change_per_room'] * $recCounter * $noofserv;
                            $amount+=$arrResul['linen_change_per_room'] * $recCounter * $noofserv;
                        }

                        if ($inputData['data']['balcony']== 1) {
                            $extraAmount+=$arrResul['balcony_price'] * $recCounter * $noofserv;
                            $amount+=$arrResul['balcony_price'] * $recCounter * $noofserv;
                        }
                    }
                }

                if ($amount <= 0) {
                    $error = array('status' => "0", "msg" => "This package not available!.", "code" => "209");

                    file_put_contents("Log/cusCostEstimation.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
                    file_put_contents("Log/cusCostEstimation.log", print_r($page, true) . "\r\n", FILE_APPEND);
                    file_put_contents("Log/cusCostEstimation.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
                    file_put_contents("Log/cusCostEstimation.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
                    file_put_contents("Log/cusCostEstimation.log", print_r($error, true) . "\r\n", FILE_APPEND);

                    $this->response($this->json($error), 209);
                }


                /*                 * VAT Query */
                /* $vatQuery="Select vat from table_config where status='A'";
                  $vatQueryRes = $this->database->query($vatQuery);
                  $vatRes=mysqli_fetch_assoc($vatQueryRes);

                  if(!empty($vatRes)){
                  $amount=$amount+($amount*$vatRes['vat'])/100;
                  } */
                /*                 * End VAT Query** */

                $amount = round($amount, 2);
                $data['amount'] = "$amount";
                $data['extraAmount'] = $extraAmount;
                //Find all Services
                $slotArray = array();
                if (!empty($inputData['data']['service'])) {
                    $dateAssArray = array("sunday" => 7, "monday" => 1, "tuesday" => 2, "wednesday" => 3, "thursday" => 4, "friday" => 5, "saturday" => 6);
                    $start = new DateTime($startDate);
                    $end = new DateTime($endDate);
                    $interval = DateInterval::createFromDateString('1 day');
                    $period = new DatePeriod($start, $interval, $end);
                    $serviceArray = $inputData['data']['service'];
                    $i = 0;
                    foreach ($serviceArray as $key => $slot) {

                        $dayValue = $dateAssArray[strtolower($slot['day'])];
                        $slotCon = " Where tcs.status='A' and tcs.slot_id = '" . $slot['slotId'] . "'";
                        $slotQuery = "SELECT tcs.slot_id,tcs.slot_start_time,tcs.slot_end_time from table_calender_slots as tcs " . $slotCon;
                        $sotResult = $this->database->query($slotQuery);
                        $row = mysqli_fetch_assoc($sotResult);

                        foreach ($period as $dt) {
                            if ($dt->format("N") == $dayValue) {
                                $slotArray[$i]['cleaning_date'] = date('Y-m-d', strtotime($dt->format("l Y-m-d")));
                                $slotArray[$i]['slot_start_time'] = date('h:i A', strtotime($row['slot_start_time']));
                                $slotArray[$i]['slot_end_time'] = date('h:i A', strtotime($row['slot_end_time']));
                                $slotArray[$i]['slot_id'] = $row['slot_id'];
                            } else {
                                continue;
                            }
                            $i++;
                        }
                    }
                }

                //End Services
                asort($slotArray);
                $slotArrrayTemp = array();
                $parent_id = 0;
                foreach ($slotArray as $temp) {
                    $temp['services'] = array();
                    $building_id = $inputData['data']['building_id'];
                    $availableSlots = $this->getServiceStatus($building_id, $temp['cleaning_date'], date("H:i:s", strtotime($temp['slot_start_time'])), date("H:i:s", strtotime($temp['slot_end_time'])));
                    $temp['services'] = array();
                    $otherAvailableTimeSlots = array();
                    if ($availableSlots == 0) {
                        $otherTimeSlot = $this->getOtherTimeSlot($building_id, $temp['cleaning_date']);
                        if(isset($otherTimeSlot) && !empty($otherTimeSlot)) {
                            $otherAvailableTimeSlots =  $otherTimeSlot;
                        } else {
                            //yesteday
                            $otherTimeSlot = $this->getOtherTimeSlot($building_id, date('Y-m-d',strtotime('-1 days',strtotime($temp['cleaning_date']))));
                            $otherAvailableTimeSlots = $otherTimeSlot;
                            //tomorrow 
                            $otherTimeSlot = $this->getOtherTimeSlot($building_id, date('Y-m-d',strtotime('+1 days',strtotime($temp['cleaning_date']))));
                            $otherAvailableTimeSlots = array_merge($otherAvailableTimeSlots,$otherTimeSlot);
                        }
                        $temp['services'] = $otherAvailableTimeSlots;
                    }

                    $paymentSql = "INSERT INTO table_booking_allotment_temp(parent_id,cleaning_date,buidling_id,is_available,slot_id)VALUES('".$parent_id."','" . $temp['cleaning_date'] . "','" . $building_id . "','" . $availableSlots . "','" . $temp['slot_id'] . "')";
                    
                    $result = $this->database->query($paymentSql);
                    $paymentId = $this->database->insert_id;
                    if($parent_id == 0) {
                        $parent_id = $paymentId;
                    }
                    $temp['service_id'] = $paymentId;
                    $temp['is_available'] = $availableSlots;
                    $slotArrrayTemp[] = $temp;
                }
                
                $data['services'] = array_values($slotArrrayTemp);
                $success = array("status" => "1", "msg" => "Amount Estimation!.", "code" => "200", "data" => $data);

                file_put_contents("Log/cusCostEstimation.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/cusCostEstimation.log", print_r($page, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/cusCostEstimation.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/cusCostEstimation.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/cusCostEstimation.log", print_r($success, true) . "\r\n", FILE_APPEND);

                $this->response($this->json($success), 200);
            }
        } else {
            $error = array('status' => "0", "msg" => "Request Blank!", "code" => "201");

            file_put_contents("Log/cusCostEstimation.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/cusCostEstimation.log", print_r($page, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/cusCostEstimation.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/cusCostEstimation.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/cusCostEstimation.log", print_r($error, true) . "\r\n", FILE_APPEND);

            $this->response($this->json($error), 201);
        }
    }

    public function getOtherTimeSlot($buiding_id, $date) {
        $condition = " where status='A' order by tcs.slot_start_time";

        $checkSlotQuery = "SELECT tcs.slot_id,tcs.slot_start_time,tcs.slot_end_time from table_calender_slots as tcs" . $condition;
        $result = $this->database->query($checkSlotQuery);
        while ($row = mysqli_fetch_assoc($result)) {
            $slotesList[] = $row;
        }


        $building_id = $buiding_id;
        $condition = " where c.status='A' and s.building_id = '" . $building_id . "'";
        $cleanersListsQuery = 'select c.cleaner_id,c.cleaner_name from table_cleaner as c left join table_supervisor as s on s.supervisor_id=c.supervisor_id ' . $condition;
        $cleanersListsTemp = $this->database->query($cleanersListsQuery);
        if ($cleanersListsTemp->num_rows > 0) {

            while ($row = mysqli_fetch_assoc($cleanersListsTemp)) {
                $cleanersLists[] = $row;
            }
            foreach ($cleanersLists as $temp) {
                $cleaner_leave_details = array();
                $cleaner_id = $temp['cleaner_id'];
                $query = 'select cleaner_name,duty_start_time,duty_end_time from table_cleaner WHERE cleaner_id="' . $cleaner_id . '"';
                $queryRes = $this->database->query($query);
                $cleaner_details = mysqli_fetch_assoc($queryRes);
                $duty_start_time = $cleaner_details['duty_start_time'];
                $duty_end_time = $cleaner_details['duty_end_time'];
                $cleaning_date = $date;
                $slotes = '';
                $query = 'select DATE(leave_from_date) as FromDate,'
                        . ' DATE(leave_to_date) as ToDate,leave_type_name from table_cleaner_leaves '
                        . 'as A LEFT JOIN table_leave_type as B ON A.leave_type = B.leave_type_id '
                        . 'WHERE cleaner_id="' . $cleaner_id . '"';
                $queryRes = $this->database->query($query);
                while ($row = mysqli_fetch_assoc($queryRes)) {
                    $cleaner_leave_details[] = $row;
                }
                foreach ($cleaner_leave_details as $cleaner_leave_detail) {
                    $leaves = array($cleaner_leave_detail['FromDate']);
                    while (end($leaves) < $cleaner_leave_detail['ToDate']) {
                        $leaves[] = date('Y-m-d', strtotime(end($leaves) . ' +1 day'));
                    }
                    if (in_array($cleaning_date, $leaves)) {
                        continue 2;
                    }
                }

                $time_booked = array();
                $query = 'select cleaning_time_start,cleaning_time_end from '
                        . '`table_booking_allotment` as A LEFT JOIN '
                        . '`table_booking_details` as B ON A.booking_detail_id = B.booking_detail_id '
                        . 'WHERE A.cleaner_id = "' . $cleaner_id . '" AND B.cleaning_date = "' . $cleaning_date . '"';
                $queryRes = $this->database->query($query);
                while ($row = mysqli_fetch_assoc($queryRes)) {
                    $time_booked[] = $row;
                }

                $booked_times = array();
                $rer = '';
                /* Creating time slotes based on the duty time of the selected cleaner */
                $starttime = date('H:i', strtotime('2016-10-30 ' . $duty_start_time));
                $endtime = date('H:i', strtotime('2016-10-30 ' . $duty_end_time));
                $duration = '120';  // split by 30 mins 
                $array_of_time = array();
                $start_time = strtotime($starttime);
                $end_time = strtotime($endtime);
                $add_mins = $duration * 60;
                while ($start_time <= $end_time) { // loop between time
                    $array_of_time[] = date("H:i", $start_time);
                    $start_time += $add_mins; // to check endtie=me  
                }

                $hidden = '';
                $slot_not_free = 0;
                foreach ($time_booked as $time_book) {
                    if ($time_book['cleaning_time_start'] == NULL || $time_book['cleaning_time_start'] == null)
                        continue;
                    $booked_slotes[] = (date("H:i", strtotime($time_book['cleaning_time_start'])) . '-' . date("H:i", strtotime($time_book['cleaning_time_end'])));
                }
                for ($i = 0; $i < count($array_of_time) - 1; $i++) {
                    $css_class = 0;
                    $style = 'green';
                    $title = '';
                    $slot_title = $array_of_time[$i] . '-' . $array_of_time[$i + 1];
                    $slot_title_disply = '';
                    foreach ($booked_slotes as $booked_slot) {

                        $booked_slot_parts = explode('-', $booked_slot);
                        $booked_start = new DateTime('2016-10-30 ' . $booked_slot_parts[0]);
                        $booked_end = new DateTime('2016-10-30 ' . $booked_slot_parts[1]);

                        $slot_start = new DateTime('2016-10-30 ' . $array_of_time[$i]);
                        $slot_end = new DateTime('2016-10-30 ' . $array_of_time[$i + 1]);
                        if ($booked_start > $slot_start)
                            continue;
                        if (($booked_start >= $slot_start && $slot_end >= $booked_end) || ($booked_start >= $slot_start && $booked_end >= $slot_end)) {

                            $css_class = 1;
                            $style = 'red';
                            $title = 'title="' . $booked_slot . '"';
                        }
                    }

                    $slotes .='<div style="background-color:' . $style . '"' . $title . ' class="' . $css_class . '">' . $slot_title . '</div>';

                    $slotesArray[] = array(
                        'slot' => $slot_title,
                        'start' => $array_of_time[$i],
                        'end' => $array_of_time[$i + 1],
                        'cleaner_id' => $temp['cleaner_id'],
                        'color' => $css_class
                    );
                }
            }
            $availableSlots = array();
            foreach ($slotesList as $tempSlots) {
                $tempSlots['status'] = 0;
                $tempSlots['cleaning_date'] = $date;
                foreach ($slotesArray as $temp) {
                    if (date('H:i:s', strtotime($tempSlots['slot_start_time'])) == date('H:i:s', strtotime($temp['start'])) && date('H:i:s', strtotime($tempSlots['slot_end_time'])) == date('H:i:s', strtotime($temp['end'])) && $temp['color'] == 0) {
                        $tempSlots['status'] = 1;
                        $availableSlots[$tempSlots['slot_id']] = $tempSlots;
                    }
                }
            }
            $availableSlots = array_values($availableSlots);
            $availableSlotsNew = array();
            foreach($availableSlots as $temp) {
                $temp['slot_start_time'] = date("H:i A",  strtotime($temp['slot_start_time'] ));
                $temp['slot_end_time'] = date("H:i A",  strtotime($temp['slot_end_time'] ));
                $availableSlotsNew[] = $temp;
            }
            return $availableSlotsNew;
            die;
        }
    }

    public function getServiceStatus($buiding_id, $date, $slot_start_time, $slot_end_time) {
        $building_id = $buiding_id;
        $condition = " where c.status='A' and s.building_id = '" . $building_id . "'";
        $cleanersListsQuery = 'select c.cleaner_id,c.cleaner_name from table_cleaner as c left join table_supervisor as s on s.supervisor_id=c.supervisor_id ' . $condition;
        $cleanersListsTemp = $this->database->query($cleanersListsQuery);
        if ($cleanersListsTemp->num_rows > 0) {

            while ($row = mysqli_fetch_assoc($cleanersListsTemp)) {
                $cleanersLists[] = $row;
            }
            foreach ($cleanersLists as $temp) {
                $cleaner_leave_details = array();
                $cleaner_id = $temp['cleaner_id'];
                $query = 'select cleaner_name,duty_start_time,duty_end_time from table_cleaner WHERE cleaner_id="' . $cleaner_id . '"';
                $queryRes = $this->database->query($query);
                $cleaner_details = mysqli_fetch_assoc($queryRes);
                $duty_start_time = $cleaner_details['duty_start_time'];
                $duty_end_time = $cleaner_details['duty_end_time'];
                $cleaning_date = $date;
                $slotes = '';
                $query = 'select DATE(leave_from_date) as FromDate,'
                        . ' DATE(leave_to_date) as ToDate,leave_type_name from table_cleaner_leaves '
                        . 'as A LEFT JOIN table_leave_type as B ON A.leave_type = B.leave_type_id '
                        . 'WHERE cleaner_id="' . $cleaner_id . '"';
                $queryRes = $this->database->query($query);
                while ($row = mysqli_fetch_assoc($queryRes)) {
                    $cleaner_leave_details[] = $row;
                }
                foreach ($cleaner_leave_details as $cleaner_leave_detail) {
                    $leaves = array($cleaner_leave_detail['FromDate']);
                    while (end($leaves) < $cleaner_leave_detail['ToDate']) {
                        $leaves[] = date('Y-m-d', strtotime(end($leaves) . ' +1 day'));
                    }
                    if (in_array($cleaning_date, $leaves)) {
                        continue 2;
                    }
                }

                $time_booked = array();
                $query = 'select cleaning_time_start,cleaning_time_end from '
                        . '`table_booking_allotment` as A LEFT JOIN '
                        . '`table_booking_details` as B ON A.booking_detail_id = B.booking_detail_id '
                        . 'WHERE A.cleaner_id = "' . $cleaner_id . '" AND B.cleaning_date = "' . $cleaning_date . '"';
                $queryRes = $this->database->query($query);
                while ($row = mysqli_fetch_assoc($queryRes)) {
                    $time_booked[] = $row;
                }

                $booked_times = array();
                $rer = '';
                /* Creating time slotes based on the duty time of the selected cleaner */
                $starttime = date('H:i', strtotime('2016-10-30 ' . $duty_start_time));
                $endtime = date('H:i', strtotime('2016-10-30 ' . $duty_end_time));
                $duration = '120';  // split by 30 mins 
                $array_of_time = array();
                $start_time = strtotime($starttime);
                $end_time = strtotime($endtime);
                $add_mins = $duration * 60;
                while ($start_time <= $end_time) { // loop between time
                    $array_of_time[] = date("H:i", $start_time);
                    $start_time += $add_mins; // to check endtie=me  
                }

                $hidden = '';
                $slot_not_free = 0;
                foreach ($time_booked as $time_book) {
                    if ($time_book['cleaning_time_start'] == NULL || $time_book['cleaning_time_start'] == null)
                        continue;
                    $booked_slotes[] = (date("H:i", strtotime($time_book['cleaning_time_start'])) . '-' . date("H:i", strtotime($time_book['cleaning_time_end'])));
                }
                for ($i = 0; $i < count($array_of_time) - 1; $i++) {
                    $css_class = 0;
                    $style = 'green';
                    $title = '';
                    $slot_title = $array_of_time[$i] . '-' . $array_of_time[$i + 1];
                    $slot_title_disply = '';
                    foreach ($booked_slotes as $booked_slot) {

                        $booked_slot_parts = explode('-', $booked_slot);
                        $booked_start = new DateTime('2016-10-30 ' . $booked_slot_parts[0]);
                        $booked_end = new DateTime('2016-10-30 ' . $booked_slot_parts[1]);

                        $slot_start = new DateTime('2016-10-30 ' . $array_of_time[$i]);
                        $slot_end = new DateTime('2016-10-30 ' . $array_of_time[$i + 1]);
                        if ($booked_start > $slot_start)
                            continue;
                        if (($booked_start >= $slot_start && $slot_end >= $booked_end) || ($booked_start >= $slot_start && $booked_end >= $slot_end)) {

                            $css_class = 1;
                            $style = 'red';
                            $title = 'title="' . $booked_slot . '"';
                        }
                    }

                    $slotes .='<div style="background-color:' . $style . '"' . $title . ' class="' . $css_class . '">' . $slot_title . '</div>';

                    $slotesArray[] = array(
                        'slot' => $slot_title,
                        'start' => $array_of_time[$i],
                        'end' => $array_of_time[$i + 1],
                        'cleaner_id' => $temp['cleaner_id'],
                        'color' => $css_class
                    );
                }
            }
            $isAvailable = 0;
            foreach ($slotesArray as $temp) {
                if (date('H:i:s', strtotime($slot_start_time)) == date('H:i:s', strtotime($temp['start'])) && date('H:i:s', strtotime($slot_end_time)) == date('H:i:s', strtotime($temp['end'])) && $temp['color'] == 0) {
                    $isAvailable = 1;
                }
            }
            return $isAvailable;
            die;
        }
    }

    /*     * *********Service History by Ayush kumar on 3/DEC/2017*************************** */

    private function serviceHistory() {

        $datetime = date('Y-m-d H:i:s');
        $page = "Service History.";
        $inputData = json_decode(file_get_contents("php://input"), true);

        if ($this->get_request_method() != 'POST') {

            $error = array("status" => "0", "msg" => "Invalid request method.", "code" => "203");

            file_put_contents("Log/serviceHistory.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/serviceHistory.log", print_r($page, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/serviceHistory.log", print_r($error, true) . "\r\n", FILE_APPEND);
            //echo "ddd";
            $this->response($this->json($error), 203);
        }

        if (isset($inputData['data']) && !empty($inputData['data']) && isset($inputData['data']['customerId']) && !empty($inputData['data']['customerId']) && isset($inputData['data']['sessionId']) && !empty($inputData['data']['sessionId'])) {
            $data = array();

            $customerId = trim($inputData['data']['customerId']);
            $sessionId = trim($inputData['data']['sessionId']);

            $response = $this->authenticateUser($customerId, $sessionId);
            if (!$response) {

                $error = array('status' => "0", "msg" => "You are already login in another device. Please logout!", "code" => "203");

                file_put_contents("Log/serviceHistory.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/serviceHistory.log", print_r($page, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/serviceHistory.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/serviceHistory.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/serviceHistory.log", print_r($error, true) . "\r\n", FILE_APPEND);

                $this->response($this->json($error), 203);
            }
            $endDate = date('Y-m-d');
            $starDate = date('Y-m-d', strtotime($endDate . '- 30 days'));


            $condition = " Where tbr.customer_id=" . $customerId . " AND tbd.cleaning_date >='" . $startDate . "'" . " AND tbd.cleaning_date <'" . $endDate . "'" . " AND tbd.status='A' order by transaction_date desc";
            $sql = "SELECT tbr.customer_id,tbd.booking_detail_id,tbd.amount,tbr.booking_id,tbd.cleaning_date,tcs.slot_start_time,tcs.slot_end_time,tcs.slot_id,tph.transaction_id,tph.payment_mode from table_booking_register as tbr left join table_booking_details as tbd on tbd.booking_id=tbr.booking_id left join table_calender_slots as tcs on tcs.slot_id=tbd.slot_id left join table_payment_history as tph on tph.payment_id=tbr.payment_id" . $condition . "order by tbd.cleaning_date";
            $result = $this->database->query($sql);
            if ($result->num_rows <= 0) {
                $error = array('status' => "0", "msg" => "No service history yet!", "code" => "209");

                file_put_contents("Log/serviceHistory.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/serviceHistory.log", print_r($page, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/serviceHistory.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/serviceHistory.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/serviceHistory.log", print_r($error, true) . "\r\n", FILE_APPEND);

                $this->response($this->json($error), 209);
            } else {
                while ($row = mysqli_fetch_assoc($result)) {
                    $row['slot_start_time'] = date('h:i A', strtotime($row['slot_start_time']));
                    $row['slot_end_time'] = date('h:i A', strtotime($row['slot_end_time']));
                    $row['amount'] = $row['amount'];
                    $row['transaction_id'] = $row['transaction_id'];
                    $row['payment_mode'] = $row['payment_mode'];
                    $data[] = $row;
                }
                $success = array('status' => "1", "msg" => "All service history", "code" => "200", "data" => $data);

                file_put_contents("Log/serviceHistory.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/serviceHistory.log", print_r($page, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/serviceHistory.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/serviceHistory.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/serviceHistory.log", print_r($success, true) . "\r\n", FILE_APPEND);

                $this->response($this->json($success), 200);
            }
        } else {
            $error = array('status' => "0", "msg" => "Request Blank!", "code" => "201");

            file_put_contents("Log/serviceHistory.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/serviceHistory.log", print_r($page, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/serviceHistory.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/serviceHistory.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/serviceHistory.log", print_r($error, true) . "\r\n", FILE_APPEND);

            $this->response($this->json($error), 201);
        }
    }

    /*     * ************************End Service History by Ayush kumar on 3/DEC/2017*************************** */



    /*     * ************************PromoCode by Ayush kumar on 29/DEC/2017*************************** */

    private function checkPromoCode() {

        $datetime = date('Y-m-d H:i:s');
        $page = "Check PromoCode.";
        $inputData = json_decode(file_get_contents("php://input"), true);

        if ($this->get_request_method() != 'POST') {

            $error = array("status" => "0", "msg" => "Invalid request method.", "code" => "203");

            file_put_contents("Log/checkPromoCode.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/checkPromoCode.log", print_r($page, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/checkPromoCode.log", print_r($error, true) . "\r\n", FILE_APPEND);
            //echo "ddd";
            $this->response($this->json($error), 203);
        }

        if (isset($inputData['data']) && !empty($inputData['data']) && isset($inputData['data']['promoCode']) && !empty($inputData['data']['promoCode']) && isset($inputData['data']['amount']) && isset($inputData['data']['customerId']) && !empty($inputData['data']['customerId']) && isset($inputData['data']['sessionId']) && !empty($inputData['data']['sessionId']) && isset($inputData['data']['buildingId']) && isset($inputData['data']['cleaningTypeId'])) {


            $promoCode = strtolower(trim($inputData['data']['promoCode']));
            $amount = strtolower(trim($inputData['data']['amount']));

            $customerId = trim($inputData['data']['customerId']);
            $sessionId = trim($inputData['data']['sessionId']);

            $response = $this->authenticateUser($customerId, $sessionId);
            if (!$response) {

                $error = array('status' => "0", "msg" => "You are already login in another device. Please logout!", "code" => "203");

                file_put_contents("Log/checkPromoCode.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/checkPromoCode.log", print_r($page, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/checkPromoCode.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/checkPromoCode.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/checkPromoCode.log", print_r($error, true) . "\r\n", FILE_APPEND);

                $this->response($this->json($error), 203);
            }

            if ($amount <= 0) {
                $error = array('status' => "0", "msg" => "Invalid amount.Please check you amount!.", "code" => "203");

                file_put_contents("Log/checkPromoCode.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/checkPromoCode.log", print_r($page, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/checkPromoCode.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/checkPromoCode.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/checkPromoCode.log", print_r($error, true) . "\r\n", FILE_APPEND);

                $this->response($this->json($error), 203);
            }

            //Check Promocode available or not
            $todayDate = DATE('Y-m-d');
            if (!empty($inputData['data']['buildingId'])) {
                //FIND_IN_SET(companyID, attachedCompanyIDs)
                //$condition=" Where tpc.promo_code='".$promoCode."' AND tpc.max_limit_use>0  AND (tpc.building_id=".$inputData['data']['buildingId']." OR tpc.cleaning_type_id=".$inputData['data']['cleaningTypeId'].") AND tpc.end_date >='".$todayDate."' AND tpc.status='A'";

                $condition = " Where tpc.promo_code='" . $promoCode . "' AND tpc.max_limit_use>0  AND ( FIND_IN_SET(" . $inputData['data']['buildingId'] . ",tpc.building_id)  OR (tpc.cleaning_type_id=" . $inputData['data']['cleaningTypeId'] . ")) AND tpc.end_date >='" . $todayDate . "' AND tpc.status='A'";
                //print_r($condition);die;
            } else {

                $condition = " Where tpc.promo_code='" . $promoCode . "' AND tpc.max_limit_use>0 AND tpc.cleaning_type_id=" . $inputData['data']['cleaningTypeId'] . " AND tpc.end_date >='" . $todayDate . "' AND tpc.status='A'";
            }

            $checkPromoCodeQuery = "SELECT tpc.* from table_promo_code as tpc " . $condition;
            //print_r($checkPromoCodeQuery);die;

            $result = $this->database->query($checkPromoCodeQuery);

            if ($result->num_rows <= 0) {

                $error = array('status' => "0", "msg" => "This promocode is invalid or expired!.", "code" => "209");

                file_put_contents("Log/checkPromoCode.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/checkPromoCode.log", print_r($page, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/checkPromoCode.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/checkPromoCode.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/checkPromoCode.log", print_r($error, true) . "\r\n", FILE_APPEND);

                $this->response($this->json($error), 209);
            }

            $resultArray = mysqli_fetch_assoc($result);
            //print_r($resultArray);die;


            $queryCheck = "Select tpcu.* from table_promo_code_usage as tpcu where tpcu.promo_code_id=" . $resultArray['promo_code_id'] . " AND tpcu.customer_id=" . $inputData['data']['customerId'];
            
            $result2 = $this->database->query($queryCheck);
            
            //Chekck promocode sent or not
            if($result2->num_rows<=0){
                $checkFirsttimeCus=true;
            }else{
                $checkFirsttimeCus=false;
            }

            if ($result2->num_rows > 0 || $checkFirsttimeCus) {

                $resultArray2 = mysqli_fetch_assoc($result2);
                //print_r($resultArray2);die;


                if(($resultArray2['promo_code_usage_times'] < $resultArray['max_limit_use']) ||$checkFirsttimeCus){

                    //$updagteQuery="update table_promo_code_usage set promo_code_usage_times=promo_code_usage_times+1 where promo_code_usage_id=".$resultArray2['promo_code_usage_id'];
                    //$updateResult = $this->database->query($updagteQuery);

                    $data = array();

                    //print_r($arrResult);exit;
                    $discount = $resultArray['promo_code_discount'];

                    $discountAmt = $amount * $discount;
                    $discountedPrice = $amount - ($discountAmt);
                    $data['discountAmount'] = $discountedPrice;
                    $data['promoCodeId'] = $resultArray['promo_code_id'];
                    $data['originalAmount'] = $amount;
                    $data['discount'] = $discountAmt;
                    //$vatPer=5;
                    //$vat=$discountedPrice*$vatPer/100;
                    //$data['discountAmount']= round($data['discountAmount']+$vat);

                    $success = array("status" => "1", "msg" => "Discounted Amount!.", "code" => "200", "data" => $data);

                    file_put_contents("Log/checkPromoCode.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
                    file_put_contents("Log/checkPromoCode.log", print_r($page, true) . "\r\n", FILE_APPEND);
                    file_put_contents("Log/checkPromoCode.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
                    file_put_contents("Log/checkPromoCode.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
                    file_put_contents("Log/checkPromoCode.log", print_r($success, true) . "\r\n", FILE_APPEND);

                    $this->response($this->json($success), 200);
                } else {
                    $error = array('status' => "0", "msg" => "Your maximum limit has been reached!.", "code" => "209");

                    file_put_contents("Log/checkPromoCode.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
                    file_put_contents("Log/checkPromoCode.log", print_r($page, true) . "\r\n", FILE_APPEND);
                    file_put_contents("Log/checkPromoCode.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
                    file_put_contents("Log/checkPromoCode.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
                    file_put_contents("Log/checkPromoCode.log", print_r($error, true) . "\r\n", FILE_APPEND);

                    $this->response($this->json($error), 209);
                }
            } else {

                $error = array('status' => "0", "msg" => "This promocode not available for you!.", "code" => "209");

                file_put_contents("Log/checkPromoCode.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/checkPromoCode.log", print_r($page, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/checkPromoCode.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/checkPromoCode.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/checkPromoCode.log", print_r($error, true) . "\r\n", FILE_APPEND);

                $this->response($this->json($error), 209);
            }
        } else {
            $error = array('status' => "0", "msg" => "Request Blank!", "code" => "201");

            file_put_contents("Log/checkPromoCode.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/checkPromoCode.log", print_r($page, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/checkPromoCode.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/checkPromoCode.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/checkPromoCode.log", print_r($error, true) . "\r\n", FILE_APPEND);

            $this->response($this->json($error), 201);
        }
    }

    /*     * ************************Payment History by Ayush kumar on 3/DEC/2017*************************** */

    private function paymentHistory() {

        $datetime = date('Y-m-d H:i:s');
        $page = "Payment History.";
        $inputData = json_decode(file_get_contents("php://input"), true);

        if ($this->get_request_method() != 'POST') {

            $error = array("status" => "0", "msg" => "Invalid request method.", "code" => "203");

            file_put_contents("Log/paymentHistory.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/paymentHistory.log", print_r($page, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/paymentHistory.log", print_r($error, true) . "\r\n", FILE_APPEND);
            //echo "ddd";
            $this->response($this->json($error), 203);
        }

        if (isset($inputData['data']) && !empty($inputData['data']) && isset($inputData['data']['customerId']) && !empty($inputData['data']['customerId']) && isset($inputData['data']['sessionId']) && !empty($inputData['data']['sessionId'])) {
            $data = array();

            $customerId = trim($inputData['data']['customerId']);
            $sessionId = trim($inputData['data']['sessionId']);

            $response = $this->authenticateUser($customerId, $sessionId);
            if (!$response) {

                $error = array('status' => "0", "msg" => "You are already login in another device. Please logout!", "code" => "203");

                file_put_contents("Log/paymentHistory.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/paymentHistory.log", print_r($page, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/paymentHistory.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/paymentHistory.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/paymentHistory.log", print_r($error, true) . "\r\n", FILE_APPEND);

                $this->response($this->json($error), 203);
            }
            $endDate = date('Y-m-d');
            $startDate = date('Y-m-d', strtotime($endDate . '- 30 days'));

            $condition = " Where tph.customer_id=" . $customerId . " AND tph.created_date >='" . $startDate . "' AND tph.created_date <='" . $endDate . "' AND tph.status='A' order by tph.created_date desc";
            $sql = "SELECT tph.payment_id,tph.customer_id,tph.created_date,tph.payment_mode,tph.transaction_status,tph.amount from table_payment_history as tph " . $condition;
            //print_r($sql);exit;
            $result = $this->database->query($sql);
            //print_r($result);die;
            if ($result->num_rows <= 0) {
                $error = array('status' => "0", "msg" => "No payment made yet!", "code" => "209");

                file_put_contents("Log/paymentHistory.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/paymentHistory.log", print_r($page, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/paymentHistory.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/paymentHistory.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/paymentHistory.log", print_r($error, true) . "\r\n", FILE_APPEND);

                $this->response($this->json($error), 209);
            } else {
                $i = 0;
                while ($row = mysqli_fetch_assoc($result)) {



                    if (strtolower($row['payment_mode']) == 'cash') {
                        $con = " where br.payment_id=" . $row['payment_id'] . " group by br.booking_id";
                        $payStsQuery = "Select sum(IF(tba.booking_status = 'CB',1,0)) AS totcb,count(tba.booking_allotment_id) AS cnt_auto_id from table_booking_register as br left join table_booking_details as tbd on tbd.booking_id=br.booking_id left join table_booking_allotment as tba on tba.booking_detail_id=tbd.booking_detail_id" . $con;


                        $payStsRes = $this->database->query($payStsQuery);
                        if ($payStsRes->num_rows <= 0) {
                            $paymentStatus = "Pending";
                        } else {
                            $row2 = mysqli_fetch_assoc($payStsRes);
                            if (($row2['totcb'] == $row2['cnt_auto_id']) && $row2['cnt_auto_id'] != 0) {
                                $paymentStatus = "Done";
                            } else {
                                $paymentStatus = "Pending";
                            }
                        }
                    } else {
                        $paymentStatus = "Done";
                    }
                    $data[$i]['payment_id'] = $row['payment_id'];
                    $data[$i]['customer_id'] = $row['customer_id'];
                    $data[$i]['payment_mode'] = $row['payment_mode'];
                    $data[$i]['created_date'] = $row['created_date'];
                    $data[$i]['payment_mode'] = $row['payment_mode'];
                    //$data[$i]['transaction_status']=$row['transaction_status'];
                    $data[$i]['amount'] = $row['amount'];
                    $data[$i]['payment_status'] = $paymentStatus;
                    $i++;
                }

                $success = array('status' => "0", "msg" => "All payments", "code" => "200", "data" => $data);

                file_put_contents("Log/paymentHistory.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/paymentHistory.log", print_r($page, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/paymentHistory.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/paymentHistory.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/paymentHistory.log", print_r($success, true) . "\r\n", FILE_APPEND);

                $this->response($this->json($success), 200);
            }
        } else {
            $error = array('status' => "0", "msg" => "Request Blank!", "code" => "201");

            file_put_contents("Log/paymentHistory.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/paymentHistory.log", print_r($page, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/paymentHistory.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/paymentHistory.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/paymentHistory.log", print_r($error, true) . "\r\n", FILE_APPEND);

            $this->response($this->json($error), 201);
        }
    }

    /*     * ************************PaymentBooking by Ayush kumar on 1/DEC/2017*************************** */




    /*     * ************************PaymentBooking by Ayush kumar on 1/DEC/2017*************************** */

    private function paymentBooking() {

        $datetime = date('Y-m-d H:i:s');
        $page = "Payment And Booking.";

        if ($this->get_request_method() != 'POST') {

            $error = array("status" => "0", "msg" => "Invalid request method.", "code" => "203");

            file_put_contents("Log/paymentBooking.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/paymentBooking.log", print_r($page, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/paymentBooking.log", print_r($error, true) . "\r\n", FILE_APPEND);
            //echo "ddd";
            $this->response($this->json($error), 203);
        }
        $inputData = json_decode(file_get_contents("php://input"), true);
        //print_r($inputData);exit;
        if (isset($inputData['data']['cleaningTypeId']) && !empty($inputData['data']['cleaningTypeId']) && isset($inputData['data']['unitTypeId']) && !empty($inputData['data']['unitTypeId']) && isset($inputData['data']['paymentOption']) && isset($inputData['data']['serviceType']) && !empty($inputData['data']['serviceType']) && isset($inputData['data']['serviceId']) && ($inputData['data']['serviceId'] != '') && isset($inputData['data']['transactionId']) && isset($inputData['data']['paymentType']) && !empty($inputData['data']['paymentType']) && isset($inputData['data']['startDate']) && !empty($inputData['data']['startDate']) && isset($inputData['data']['rooms']) && !empty($inputData['data']['rooms']) && isset($inputData['data']['actualCost']) && !empty($inputData['data']['actualCost']) && isset($inputData['data']['totalCostPaid']) && !empty($inputData['data']['totalCostPaid']) && isset($inputData['data']['promoCodeId']) && isset($inputData['data']['service']) && isset($inputData['data']['specialInstruction']) && isset($inputData['data']['userArea']) && isset($inputData['data']['customerAddressId']) && isset($inputData['data']['customerId']) && !empty($inputData['data']['customerId']) && isset($inputData['data']['sessionId']) && !empty($inputData['data']['sessionId']) && isset($inputData['data']['cardHolderName']) && isset($inputData['data']['cardNumber']) && isset($inputData['data']['endDate']) && isset($inputData['data']['expiryDate']) && isset($inputData['data']['tokenName']) && isset($inputData['data']['saveCard']) && isset($inputData['data']['linen_change']) && isset($inputData['data']['balcony'])) {

            $customerId = trim($inputData['data']['customerId']);
            $sessionId = trim($inputData['data']['sessionId']);

            $response = $this->authenticateUser($customerId, $sessionId);
            if (!$response) {

                $error = array('status' => "0", "msg" => "You are already login in another device. Please logout!", "code" => "203");

                file_put_contents("Log/paymentBooking.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/paymentBooking.log", print_r($page, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/paymentBooking.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/paymentBooking.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/paymentBooking.log", print_r($error, true) . "\r\n", FILE_APPEND);

                $this->response($this->json($error), 203);
            }


            if ($inputData['data']['paymentType'] == "cash") {

                $customer_id = $inputData['data']['customerId'];
                $account_id = 1;
                $transaction_date = date('Y-m-d H:i:s');
                $payment_mode = $inputData['data']['paymentType'];
                $transaction_status = "p";
                $amount = $inputData['data']['totalCostPaid'];
                $transaction_id = mt_rand(1000000000, 9999999999);
                $created_date = date('Y-m-d H:i:s');
                $last_update_date = date('Y-m-d H:i:s');
                $status = "A";


                $paymentSql = "INSERT INTO table_payment_history(account_id,customer_id,transaction_date,payment_mode,	transaction_status,amount,created_date,last_update_date,status,transaction_id)VALUES('" . $account_id . "','" . $customer_id . "','" . $transaction_date . "','" . $payment_mode . "','" . $transaction_status . "','" . $amount . "','" . $created_date . "','" . $last_update_date . "','" . $status . "','" . $transaction_id . "')";

                $result = $this->database->query($paymentSql);
                $paymentId = $this->database->insert_id;
                if ($paymentId > 0) {

                    $response = $this->Booking($inputData, $paymentId);

                    $success = array('status' => "1", "msg" => "Your booking has been confirmed.", "code" => "200", "bookingId" => $response);

                    file_put_contents("Log/paymentBooking.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
                    file_put_contents("Log/paymentBooking.log", print_r($page, true) . "\r\n", FILE_APPEND);
                    file_put_contents("Log/paymentBooking.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
                    file_put_contents("Log/paymentBooking.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
                    file_put_contents("Log/paymentBooking.log", print_r($success, true) . "\r\n", FILE_APPEND);

                    $this->response($this->json($success), 200);
                } else {
                    $error = array('status' => "0", "msg" => "Server Error", "code" => "500");

                    file_put_contents("Log/paymentBooking.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
                    file_put_contents("Log/paymentBooking.log", print_r($page, true) . "\r\n", FILE_APPEND);
                    file_put_contents("Log/paymentBooking.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
                    file_put_contents("Log/paymentBooking.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
                    file_put_contents("Log/paymentBooking.log", print_r($error, true) . "\r\n", FILE_APPEND);

                    $this->response($this->json($error), 500);
                }
            } else {


                //      		/*****Save Card**********/
                //      		$condition=" Where customer_id=".$customerId;
                // $checkcusCardQuery="SELECT * from table_saved_cards as tsc left join table_payment_history as tph on tph.saved_card_id= tsc.saved_card_id".$condition;
                // $result2 = $this->database->query($checkcusCardQuery);
                // if($result2->num_rows>0){
                // 	$condition=" Where customer_id=".$customerId;
                // 	$checkCardQuery="UPDATE table_saved_cards SET default_card=0".$condition;
                // 	$result = $this->database->query($checkCardQuery);
                // }
                //Check Booking Id is valid or not
                $cardHolderName = trim($inputData['data']['cardHolderName']);
                $cardNumber = trim($inputData['data']['cardNumber']);
                $expiryDate = explode("/", trim($inputData['data']['expiryDate']));
                $tokenName = trim($inputData['data']['tokenName']);
                $expiry_month = $expiryDate[0];
                $expiry_year = $expiryDate[1];
                $default_card = 1;
                $save_card = $inputData['data']['saveCard'];

                $account_id = 1;
                $status = 'A';
                $created_date = date('Y-m-d H:i:s');
                $last_update_date = date('Y-m-d H:i:s');

                $cardInsert = "INSERT INTO table_saved_cards(account_id,card_holder_name,customer_id,card_number,expiry_month,expiry_year,token_name,created_date,last_update_date,status,default_card,save_card) VALUES ('" . $account_id . "','" . $cardHolderName . "','" . $customerId . "','" . $cardNumber . "','" . $expiry_month . "','" . $expiry_year . "','" . $tokenName . "','" . $created_date . "','" . $last_update_date . "','" . $status . "','" . $default_card . "','" . $save_card . "')";

                $result = $this->database->query($cardInsert);
                $cardId = $this->database->insert_id;
//print_r($cardId);die;
                /*                 * ****End Save Card**** */
                /*                 * **Save payment* */
                $customer_id = $inputData['data']['customerId'];
                $account_id = 1;
                $transaction_date = date('Y-m-d H:i:s');
                $payment_mode = strtolower($inputData['data']['paymentType']);
                $transaction_status = "C";
                $amount = $inputData['data']['totalCostPaid'];
                $transaction_id = $inputData['data']['transactionId'];
                $created_date = date('Y-m-d H:i:s');
                $last_update_date = date('Y-m-d H:i:s');
                $saved_card_id = $cardId;
                $payment_gateway_invoice_number = $inputData['data']['paymentGatewayInvoiceNumber'];
                $status = "A";


                $paymentSql = "INSERT INTO table_payment_history(account_id,customer_id,transaction_date,payment_mode,transaction_status,amount,created_date,last_update_date,status,payment_gateway_invoice_number,transaction_id,saved_card_id)VALUES('" . $account_id . "','" . $customer_id . "','" . $transaction_date . "','" . $payment_mode . "','" . $transaction_status . "','" . $amount . "','" . $created_date . "','" . $last_update_date . "','" . $status . "','" . $payment_gateway_invoice_number . "','" . $transaction_id . "','" . $saved_card_id . "')";

                $result = $this->database->query($paymentSql);
                $paymentId = $this->database->insert_id;
                if ($paymentId > 0) {

                    $response = $this->Booking($inputData, $paymentId);

                    $success = array('status' => "1", "msg" => "Your booking has been confirmed.", "code" => "200", "bookingId" => $response);

                    file_put_contents("Log/paymentBooking.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
                    file_put_contents("Log/paymentBooking.log", print_r($page, true) . "\r\n", FILE_APPEND);
                    file_put_contents("Log/paymentBooking.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
                    file_put_contents("Log/paymentBooking.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
                    file_put_contents("Log/paymentBooking.log", print_r($success, true) . "\r\n", FILE_APPEND);

                    $this->response($this->json($success), 200);
                } else {
                    $error = array('status' => "0", "msg" => "Server Error", "code" => "500");

                    file_put_contents("Log/paymentBooking.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
                    file_put_contents("Log/paymentBooking.log", print_r($page, true) . "\r\n", FILE_APPEND);
                    file_put_contents("Log/paymentBooking.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
                    file_put_contents("Log/paymentBooking.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
                    file_put_contents("Log/paymentBooking.log", print_r($error, true) . "\r\n", FILE_APPEND);

                    $this->response($this->json($error), 500);
                }
            }
        } else {
            $error = array('status' => "0", "msg" => "Request Blank!", "code" => "201");

            file_put_contents("Log/paymentBooking.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/paymentBooking.log", print_r($page, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/paymentBooking.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/paymentBooking.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/paymentBooking.log", print_r($error, true) . "\r\n", FILE_APPEND);

            $this->response($this->json($error), 201);
        }
    }

    public function Booking($inputData, $paymentId) {
        if (strtolower($inputData['data']['paymentOption']) != "others") {
            $startDate = date('Y-m-d', strtotime($inputData['data']['startDate']));
            $strdate = strtotime($startDate);
            if (strtolower($inputData['data']['serviceType']) == "o") {
                $totalDays = 1;
                $endDate = date('Y-m-d', strtotime('+' . $totalDays . ' days', $strdate));
                $packg_endDate = $startDate;
            } else {
                if (strtolower($inputData['data']['paymentOption']) == "monthly") {
                    $totalDays = 28;
                } elseif (strtolower($inputData['data']['paymentOption']) == "quartly") {
                    $totalDays = 28 * 3;
                } elseif (strtolower($inputData['data']['paymentOption']) == "yearly") {
                    $totalDays = 28 * 12;
                }
                $endDate = date('Y-m-d', strtotime('+' . $totalDays . ' days', $strdate));
                $packg_endDate = $endDate;
            }
        } else {
            $startDate = date('Y-m-d', strtotime($inputData['data']['startDate']));
            $endDate = date('Y-m-d', strtotime($inputData['data']['endDate']));
            $incr = 1;
            $endDate2 = date('Y-m-d', strtotime('+' . $incr . ' days', strtotime($endDate)));
            $packg_endDate = $endDate;

            //count all services in case of Others
            $allServices = array();
            $dateAssArray = array("sunday" => 7, "monday" => 1, "tuesday" => 2, "wednesday" => 3, "thursday" => 4, "friday" => 5, "saturday" => 6);
            $start = new DateTime($startDate);
            $end = new DateTime($endDate2);
            $interval = DateInterval::createFromDateString('1 day');
            $period = new DatePeriod($start, $interval, $end);
            $serviceArray = $inputData['data']['service'];
            $i = 0;
            foreach ($serviceArray as $key => $slot) {

                $dayValue = $dateAssArray[strtolower($slot['day'])];
                $sotResult = $this->database->query($slotQuery);
                $row = mysqli_fetch_assoc($sotResult);

                foreach ($period as $dt) {

                    if ($dt->format("N") == $dayValue) {
                        $allServices[$i]['cleaning_date'] = date('Y-m-d', strtotime($dt->format("l Y-m-d")));
                        $i++;
                    } else {
                        continue;
                    }
                }
            }

            //print_r($allServices);die;
            //End counting all services
            $toatalServices = count($allServices);
        }


        $customerAddressId = $inputData['data']['customerAddressId'];


        //print_r($endDate);exit;
        $payment_id = $paymentId;
        $account_id = 1;
        $cleaning_type_id = $inputData['data']['cleaningTypeId'];
        $unit_type_id = $inputData['data']['unitTypeId'];
        $customer_id = $inputData['data']['customerId'];
        $cleaning_actual_cost = $inputData['data']['actualCost'];
        $promo_discount = $inputData['data']['discount'];
        $total_cost_paid = $inputData['data']['totalCostPaid'];
        $cleaning_plan = strtolower($inputData['data']['serviceType']);
        $subscriptionmode = strtolower($inputData['data']['paymentOption']);
        $specialInstruction = $inputData['data']['specialInstruction'];
        $userArea = $inputData['data']['userArea'];
        $promoCodeId = $inputData['data']['promoCodeId'];
        $serviceId = $inputData['data']['serviceId'];
        $linen_change = $inputData['data']['linen_change'];
        $balcony = $inputData['data']['balcony'];
        $packg_start_date = $startDate;
        $packg_end_date = $packg_endDate;
        $created_date = date('Y-m-d H:i:s');
        $last_update_date = date('Y-m-d H:i:s');
        $status = "A";

        $bookingSql = "INSERT INTO table_booking_register(payment_id,account_id,customer_id,promo_code_id,cleaning_actual_cost,promo_discount,total_cost_paid,packg_start_date,packg_end_date,created_date,last_update_date,status,customer_address_id,cleaning_plan,subscriptionmode,cleaning_type_id,unit_type_id,special_instruction,user_area,service_id,linen_change,balcony)VALUES('" . $payment_id . "','" . $account_id . "','" . $customer_id . "','" . $promoCodeId . "','" . $cleaning_actual_cost . "','" . $promo_discount . "','" . $total_cost_paid . "','" . $packg_start_date . "','" . $packg_end_date . "','" . $created_date . "','" . $last_update_date . "','" . $status . "','" . $customerAddressId . "','" . $cleaning_plan . "','" . $subscriptionmode . "','" . $cleaning_type_id . "','" . $unit_type_id . "','" . $specialInstruction . "','" . $userArea . "','" . $serviceId . "','" . $linen_change . "','" . $balcony . "')";

        $result = $this->database->query($bookingSql);
        $bookingId = $this->database->insert_id;
        /*         * ********Update in promocode usages******** */
        $cusPromoCode = "Select tpcu.* from table_promo_code_usage as tpcu where tpcu.promo_code_id=" . $inputData['data']['promoCodeId'] . " AND tpcu.customer_id=" . $customer_id;
        //print_r($queryCheck);die;
        $promoCusResult = $this->database->query($cusPromoCode);
        //print_r($promoCusResult);die;
        if ($promoCusResult->num_rows > 0) {

            $updagteQuery = "update table_promo_code_usage set promo_code_usage_times=promo_code_usage_times+1 where promo_code_id=" . $inputData['data']['promoCodeId'] . " AND customer_id=" . $inputData['data']['customerId'];
            //print_r($updagteQuery);die;
            $updateResult = $this->database->query($updagteQuery);
        } else {

            //New Customer Registered
            $promoUsageInsert = "INSERT INTO table_promo_code_usage(account_id,promo_code_id,customer_id,promo_code_usage_times)VALUES('" . $account_id . "'," . $inputData['data']['promoCodeId'] . "," . $inputData['data']['customerId'] . ",1)";

            $result = $this->database->query($promoUsageInsert);
        }
        /*         * ***************End in promocode usage updation**** */

        /*         * ******************Save Room Categoery into DB********* */
        $roomServiceArr = $inputData['data']['rooms'];
        foreach ($roomServiceArr as $key => $roomService) {
            if ($roomService['counter'] != 0) {

                $bookingRoomMapSql = "INSERT INTO table_booking_room_type_mapping(booking_id,counter,room_category_id)VALUES('" . $bookingId . "','" . $roomService['counter'] . "','" . $roomService['roomCategoryId'] . "')";

                $result2 = $this->database->query($bookingRoomMapSql);
            }
        }
        /*         * **********Save Room Category END******* */


        /*         * ***********************Save Slots in Booking Detail Table********************************** */
        $slotArray = array();
        $dateAssArray = array("sunday" => 7, "monday" => 1, "tuesday" => 2, "wednesday" => 3, "thursday" => 4, "friday" => 5, "saturday" => 6);
        $start = new DateTime($startDate);
        $end = new DateTime($endDate);
        $interval = DateInterval::createFromDateString('1 day');
        $period = new DatePeriod($start, $interval, $end);

        $serviceArray = $inputData['data']['service'];
        if ((strtolower($inputData['data']['serviceType']) == "l")) {
            if (strtolower($inputData['data']['paymentOption']) == "yearly") {
                $amount = $total_cost_paid / 48;
            } elseif (strtolower($inputData['data']['paymentOption']) == "quartly") {
                $amount = $total_cost_paid / 12;
            } elseif (strtolower($inputData['data']['paymentOption']) == "monthly") {
                $amount = $total_cost_paid / 4;
            } else {
                $amount = $total_cost_paid / $toatalServices;
            }
        } else {
            $amount = $total_cost_paid;
        }
        //print_r($serviceArray);die;
        $i = 0;
        $services = explode(',',$inputData['data']['service_ids']);
        foreach ($services as $temp) {
            $slotCon = " Where  id='" . $temp . "'";
            $slotQuery = "SELECT * from table_booking_allotment_temp " . $slotCon;
            $sotResult = $this->database->query($slotQuery);
            $row = mysqli_fetch_assoc($sotResult);
            $slotSql = "INSERT INTO table_booking_details(account_id,booking_id,slot_id,cleaning_date,status,amount)VALUES('" . $account_id . "','" . $bookingId . "','" . $row['slot_id'] . "','" . $row['cleaning_date'] . "','" . $status . "','" . $amount . "')";
            $result3 = $this->database->query($slotSql);

            $bookDetailId = $this->database->insert_id;

            /*                 * ************Save Booking Allotment**************************** */
            $booking_alloted = "no";
            $booking_status = "UA";
            $status = "A";

            $bookingAllotSql = "INSERT INTO table_booking_allotment(account_id,booking_detail_id,booking_alloted,booking_status,created_date,last_update_date,status)VALUES('" . $account_id . "','" . $bookDetailId . "','" . $booking_alloted . "','" . $booking_status . "','" . $created_date . "','" . $last_update_date . "','" . $status . "')";
            
            $bookalotresult = $this->database->query($bookingAllotSql);
            $slotArray[$i]['account_id'] = $account_id;
            $slotArray[$i]['booking_id'] = $bookingId;
            $slotArray[$i]['slot_id'] = $row['slot_id'];
            
            $deleteQuery = "Delete from table_booking_allotment_temp where id=" .$temp;

            $this->database->query($deleteQuery);

            /*                 * ************End Booking Allotment***************************** */
            $i++;
        }
//Send Mail and sms
        $cusquery = "Select c.*,b.building_name,u.unit_name,ca.customer_address from table_customer as c  "
                . "left join table_customer_address as ca on ca.customer_id = c.customer_id "
                . "left join table_buildings as b on b.building_id= ca.building_id "
                . "left join table_unit_type as u on u.unit_type_id = b.unit_type_id"
                . " where c.customer_id=" . $customer_id . " and ca.customer_address_id ='" . $customerAddressId . "'";
        //echo $cusquery;die;
        $cusResult = $this->database->query($cusquery);
        $custDetail = mysqli_fetch_assoc($cusResult);

        $findRoomCat = "Select GROUP_CONCAT( CONCAT(tbrtm.counter,' ',tc.category_name) SEPARATOR ',') as cat from table_booking_room_type_mapping as tbrtm left join table_category_type as tct on tct.room_category_id=tbrtm.room_category_id left join table_category as tc on tc.category_id = tct.category_id  where tbrtm.booking_id=" . $bookingId . " group by tbrtm.booking_id";
        
$queryresultCat = $this->database->query($findRoomCat);
        $resultArray = mysqli_fetch_assoc($queryresultCat);


$findRoomCat1 = "SELECT cs.slot_start_time FROM table_booking_details AS bd LEFT JOIN table_calender_slots AS cs ON cs.slot_id = bd.slot_id WHERE bd.booking_id =" . $bookingId . " GROUP BY bd.booking_id";
       
$queryresultCat1 = $this->database->query($findRoomCat1);
        $resultArray1 = mysqli_fetch_assoc($queryresultCat1);

        if ($inputData['data']['paymentType'] == "cash") {
            $paymentStatusMsg = "Cash";
        } else {
            $paymentStatusMsg = "Paid";
        }

        $message = '<!DOCTYPE html><html lang="en" > <head></head><body><table width="700px" align="center">'.
        '<tr><td><img src="http://pepupsales.net/whiteSpotINT/images/header.jpg"/></td></tr><tr><td><h2>Thank you for your booking at Whitespot cleaning</h2></td></tr><tr><td><table width="400px" align="center" border="1px" cellpadding="2" cellspacing="0"><tr style="background-color: #00b0f0;">'.
        '<th colspan="2" style="color: #ffffff;">Booking Summary</th></tr><tr height="30px"><td width="200px">'.
        'Reference No</td><td width="200px">'.$bookingId.'</td></tr><tr height="30px"><td>Guest Name</td><td>'.
    $custDetail['customer_name'].'</td></tr><tr height="30px"><td>Guest Number</td><td>'.
    $custDetail['customer_number'].'</td></tr><tr height="30px"><td>Property Details</td><td>'.$custDetail['customer_address'] . ' ' . $custDetail['building_name']. 
        '</td></tr><tr height="30px"><td>Requested service</td><td>'. $resultArray['cat'].'</td></tr><tr height="30px"><td>'.
    'Date & Time</td><td>'.$startDate . ' ' . $resultArray1 ['slot_start_time'] .'</td></tr><tr height="30px"><td>Amount</td>'.
    '<td>'.$total_cost_paid.'</td></tr><tr height="30px"><td>Mode of payment</td><td>'.$paymentStatusMsg.'</td></tr></table></td></tr><tr><td><h2>Thank you for your booking at Whitespot cleaning</h2></td></tr><tr>'.
    '<td><br><br>Do you still have quires? We are always at your service. You can call us at <a href="tel:971 4 553 9799" style="color:#00b0f0">+971 4 553 9799</a> or email us at <a href="mailto: info@whitespot-cleaning.com" style="color:#00b0f0">  info@whitespot-cleaning.com</a></td>'.
    '</tr></table></body></html>';

        $subject = "Booking.";
        $res = $this->sendEmail($custDetail['customer_email'], $message, $subject);
        //$res=$this->sendEmail('parul.nirwan@quytech.com',$message,$subject);
        //$res = $this->sendEmail('info@whitespot-cleaning.com', $message, $subject);
        //$res = $this->sendEmail('nikhil.paruthipilliyil@whitespot-cleaning.com', $message, $subject);
        //$res = $this->sendEmail('shahzad@whitespot-cleaning.com', $message, $subject);
        //$res = $this->sendEmail('uppercrest@whitespot-cleaning.com', $message, $subject);
        $mobileMsg = "Hello " . $custDetail['customer_name'] . " Your booking has been booked succesfully.!"
                . " Your booking is WSC_" . $bookingId . " and start from " . $startDate . " "
                . "Regards White-Spot";
        $smsRes = $this->sendSms($custDetail['customer_number'], $mobileMsg);
        $smsRes = $this->sendSms('0569635534', $mobileMsg);
        $smsRes = $this->sendSms('0525194828', $mobileMsg);
        $smsRes = $this->sendSms('0555058406', $mobileMsg);
        return $bookingId;
    }

    /*     * ************************End Payment BOOking by Ayush kumar on 3/DEC/2017*************************** */



    /*     * ************************Schedule Services by Ayush kumar on 3/DEC/2017*************************** */

    private function scheduleServices() {

        $datetime = date('Y-m-d H:i:s');
        $page = "Schedule Services.";
        $inputData = json_decode(file_get_contents("php://input"), true);

        if ($this->get_request_method() != 'POST') {

            $error = array("status" => "0", "msg" => "Invalid request method.", "code" => "203");

            file_put_contents("Log/scheduleServices.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/scheduleServices.log", print_r($page, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/scheduleServices.log", print_r($error, true) . "\r\n", FILE_APPEND);
            //echo "ddd";
            $this->response($this->json($error), 203);
        }

        if (isset($inputData['data']) && !empty($inputData['data']) && isset($inputData['data']['customerId']) && !empty($inputData['data']['customerId']) && isset($inputData['data']['sessionId']) && !empty($inputData['data']['sessionId'])) {
            $data = array();

            $customerId = trim($inputData['data']['customerId']);
            $sessionId = trim($inputData['data']['sessionId']);

            $response = $this->authenticateUser($customerId, $sessionId);
            if (!$response) {

                $error = array('status' => "0", "msg" => "You are already login in another device. Please logout!", "code" => "203");

                file_put_contents("Log/scheduleServices.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/scheduleServices.log", print_r($page, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/scheduleServices.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/scheduleServices.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/scheduleServices.log", print_r($error, true) . "\r\n", FILE_APPEND);

                $this->response($this->json($error), 203);
            }
            $startDate = date('Y-m-d');
            $endDate = date('Y-m-d', strtotime('+30 days', strtotime($startDate)));

            $condition = " Where tbr.customer_id=" . $customerId . " AND tbd.cleaning_date >='" . $startDate . "'" . " AND tbd.status='A'";
            $sql = "SELECT ca.building_id,tbr.customer_id,tbd.booking_detail_id,tbd.amount,tbd.remainder_set,tbr.booking_id,tbd.cleaning_date,tcs.slot_start_time,tcs.slot_end_time,tcs.slot_id,tph.transaction_id,tph.payment_mode from table_booking_register as tbr left join table_customer_address as ca on ca.customer_address_id = tbr.customer_address_id left join table_booking_details as tbd on tbd.booking_id=tbr.booking_id left join table_calender_slots as tcs on tcs.slot_id=tbd.slot_id left join table_payment_history as tph on tph.payment_id=tbr.payment_id" . $condition . "order by tbd.cleaning_date limit 30";
            $result = $this->database->query($sql);
            //print_r($result);exit;
            if ($result->num_rows <= 0) {
                $error = array('status' => "0", "msg" => "No service shedule yet!", "code" => "209");

                file_put_contents("Log/scheduleServices.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/scheduleServices.log", print_r($page, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/scheduleServices.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/scheduleServices.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/scheduleServices.log", print_r($error, true) . "\r\n", FILE_APPEND);

                $this->response($this->json($error), 209);
            } else {
                while ($row = mysqli_fetch_assoc($result)) {
                    $row['slot_start_time'] = date('h:i A', strtotime($row['slot_start_time']));
                    $row['slot_end_time'] = date('h:i A', strtotime($row['slot_end_time']));
                    $row['amount'] = $row['amount'];
                    $row['transaction_id'] = $row['transaction_id'];
                    $row['payment_mode'] = $row['payment_mode'];
                    $row['building_id'] = $row['building_id'];
                    $data[] = $row;
                }

                /*                 * **********Next Schedule ***** */
                $condition2 = " Where tc.status='A'" . " AND tbd.cleaning_date>'" . $startDate . "'" . " AND tc.customer_id=" . $customerId . "tbd.status='A'";

                $findNextSchedulesql = "SELECT ca.building_id,tc.customer_id,tc.customer_name,tc.created_date,tbd.booking_id,tbd.booking_detail_id,tbd.amount,tbd.remainder_set,tbd.cleaning_date,tcs.slot_start_time,tcs.slot_end_time,tbd.slot_id,tph.transaction_id,tph.payment_mode from table_customer as tc left join table_booking_register as tbr on tbr.customer_id=tc.customer_id left join table_booking_details as tbd on tbd.booking_id=tbr.booking_id left join table_customer_address as ca on ca.customer_address_id = tbr.customer_address_id left join table_calender_slots as tcs on tcs.slot_id=tbd.slot_id left join table_payment_history as tph on tph.payment_id=tbr.payment_id" . $condition . " order by tbd.cleaning_date,tcs.slot_start_time LIMIT 1";

                $result2 = $this->database->query($findNextSchedulesql);
                $resultArr = mysqli_fetch_assoc($result2);

                $nextSchedule['nextScheduleDate'] = $resultArr['cleaning_date'];
                $nextSchedule['nextScheduleStartTime'] = date('h:i A', strtotime($resultArr['slot_start_time']));
                $nextSchedule['nextScheduleEndTime'] = date('h:i A', strtotime($resultArr['slot_end_time']));
                $nextSchedule['booking_id'] = $resultArr['booking_id'];
                $nextSchedule['building_id'] = $resultArr['building_id'];
                $nextSchedule['bookingDetailId'] = $resultArr['booking_detail_id'];
                $nextSchedule['remainder_set'] = $resultArr['remainder_set'];
                $nextSchedule['slotId'] = $resultArr['slot_id'];
                $nextSchedule['amount'] = $resultArr['amount'];
                $nextSchedule['transaction_id'] = $resultArr['transaction_id'];
                $nextSchedule['payment_mode'] = $resultArr['payment_mode'];

                /*                 * ******End next Schedule*** */



                $success = array('status' => "1", "msg" => "All schedule service", "code" => "200", "data" => $data, "nextSchedule" => $nextSchedule);

                file_put_contents("Log/scheduleServices.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/scheduleServices.log", print_r($page, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/scheduleServices.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/scheduleServices.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/scheduleServices.log", print_r($success, true) . "\r\n", FILE_APPEND);

                $this->response($this->json($success), 200);
            }
        } else {
            $error = array('status' => "0", "msg" => "Request Blank!", "code" => "201");

            file_put_contents("Log/scheduleServices.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/scheduleServices.log", print_r($page, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/scheduleServices.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/scheduleServices.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/scheduleServices.log", print_r($error, true) . "\r\n", FILE_APPEND);

            $this->response($this->json($error), 201);
        }
    }

    /*     * ************************End Schedule Services by Ayush kumar on 3/DEC/2017*************************** */



    /*     * ************************Cancel schedule by Ayush kumar on 4/DEC/2017*************************** */

    private function cancelServiceSchedule() {

        $datetime = date('Y-m-d H:i:s');
        $page = "Cancel Services.";
        $inputData = json_decode(file_get_contents("php://input"), true);

        if ($this->get_request_method() != 'POST') {

            $error = array("status" => "0", "msg" => "Invalid request method.", "code" => "203");

            file_put_contents("Log/cancelService.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/cancelService.log", print_r($page, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/cancelService.log", print_r($error, true) . "\r\n", FILE_APPEND);
            //echo "ddd";
            $this->response($this->json($error), 203);
        }

        if (isset($inputData['data']) && !empty($inputData['data']) && isset($inputData['data']['customerId']) && !empty($inputData['data']['customerId']) && isset($inputData['data']['sessionId']) && !empty($inputData['data']['sessionId']) && isset($inputData['data']['bookingDetailId']) && !empty($inputData['data']['bookingDetailId']) && isset($inputData['data']['bookingId']) && !empty($inputData['data']['bookingId'])) {
            $data = array();

            $customerId = trim($inputData['data']['customerId']);
            $sessionId = trim($inputData['data']['sessionId']);

            $response = $this->authenticateUser($customerId, $sessionId);
            if (!$response) {

                $error = array('status' => "0", "msg" => "You are already login in another device. Please logout!", "code" => "203");

                file_put_contents("Log/cancelService.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/cancelService.log", print_r($page, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/cancelService.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/cancelService.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/cancelService.log", print_r($error, true) . "\r\n", FILE_APPEND);

                $this->response($this->json($error), 203);
            }
            //$startDate=strtotime($datetime);
            $startDate = date('Y-m-d');


            $condition = " Where tbr.customer_id=" . $customerId . " AND tbd.cleaning_date >='" . $startDate . "'" . " AND tbr.booking_id=" . $inputData['data']['bookingId'] . " AND tbd.booking_detail_id=" . $inputData['data']['bookingDetailId'];

            $sql = "SELECT ct.cleaning_type,tbr.customer_address_id ,tbr.customer_id,tbd.booking_detail_id,tbr.booking_id,tbd.cleaning_date,tcs.slot_start_time,tbr.total_cost_paid ,tcs.slot_end_time,tcs.slot_id from table_booking_register as tbr left join table_booking_details as tbd on tbd.booking_id=tbr.booking_id left join table_calender_slots as tcs on tcs.slot_id=tbd.slot_id left join table_cleaning_type as ct on ct.cleaning_type_id = tbr.cleaning_type_id" . $condition;

            //echo $sql;exit;
            $result = $this->database->query($sql);
            //print_r($result);exit;
            if ($result->num_rows <= 0) {
                $error = array('status' => "0", "msg" => "Invalid schedule!", "code" => "209");

                file_put_contents("Log/cancelService.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/cancelService.log", print_r($page, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/cancelService.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/cancelService.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/cancelService.log", print_r($error, true) . "\r\n", FILE_APPEND);

                $this->response($this->json($error), 209);
            } else {

            	$details = mysqli_fetch_assoc($result);

                $condi = " Where booking_detail_id=" . $inputData['data']['bookingDetailId'];
                $updateSQl = "UPDATE table_booking_details SET status='I'" . $condi;
                //print_r($updateSQl);die;
                $this->database->query($updateSQl);

                /** **********Cancel in table Allotment****** */


                $condi2 = " Where booking_detail_id=" . $inputData['data']['bookingDetailId'];
                $updateSQl2 = "UPDATE table_booking_allotment SET booking_status='CC'" . $condi2;
                $this->database->query($updateSQl2);


                /** ************END UPdate in table ALLOTMENT**************************** */

                //Send Mail and sms
		        $cusquery = "Select c.*,b.building_name,u.unit_name,ca.customer_address from table_customer as c  "
		                . "left join table_customer_address as ca on ca.customer_id = c.customer_id "
		                . "left join table_buildings as b on b.building_id= ca.building_id "
		                . "left join table_unit_type as u on u.unit_type_id = b.unit_type_id"
		                . " where c.customer_id=" . $details['customer_id'] . " and ca.customer_address_id ='" . $details['customer_address_id'] . "'";
		       	$cusResult = $this->database->query($cusquery);
		        $custDetail = mysqli_fetch_assoc($cusResult);


		        $bookingId = $details['booking_id'];

		        $findRoomCat = "Select GROUP_CONCAT( CONCAT(tbrtm.counter,' ',tc.category_name) SEPARATOR ',') as cat from table_booking_room_type_mapping as tbrtm left join table_category_type as tct on tct.room_category_id=tbrtm.room_category_id left join table_category as tc on tc.category_id = tct.category_id  where tbrtm.booking_id=" . $bookingId . " group by tbrtm.booking_id";
		        
				$queryresultCat = $this->database->query($findRoomCat);
				        $resultArray = mysqli_fetch_assoc($queryresultCat);


				$findRoomCat1 = "SELECT cs.slot_start_time,cs.slot_end_time FROM table_booking_details AS bd LEFT JOIN table_calender_slots AS cs ON cs.slot_id = bd.slot_id WHERE bd.booking_id =" . $bookingId . " GROUP BY bd.booking_id";
				

				$queryresultCat1 = $this->database->query($findRoomCat1);
		        $resultArray1 = mysqli_fetch_assoc($queryresultCat1);

		        if ($inputData['data']['paymentType'] == "cash") {
		            $paymentStatusMsg = "Cash";
		        } else {
		            $paymentStatusMsg = "Paid";
		        }
		        $message = '<html><body><table style="font-family: arial"; align="center" width="700" cellpadding="0" cellspacing="0">'.
            '<tr><td colspan="2" style="border-bottom: 1px solid #fff"><img src="http://app.whitespot-cleaning.com/images/email/header.jpg"></td></tr>'.
            '<tr><td bgcolor="#051922" align="center" style="color: #fff; padding: 10px; font-weight: bold" colspan="2">YOUR BOOKING IS CONFIRMED!</td></tr>'.
	    '<tr><td height="15px"></td></tr>'.
	    '<tr><td style="padding: 0 15px;">Reference No</td><td style="padding: 0 15px">'.$bookingId.'</td></tr>'.
            '<tr><td height="15px"></td></tr>'.
            '<tr><td style="padding: 0 15px">Guest Name</td><td style="padding: 0 15px">'.$custDetail['customer_name'].'</td></tr>'.
            '<tr><td height="15px"></td></tr>'.
	    '<tr><td style="padding: 0 15px">Phone Number</td><td style="padding: 0 15px">'.$custDetail['customer_number'].'</td></tr>'.
	    '<tr><td height="15px"></td></tr>'.

	    '<tr><td style="padding: 0 15px">Email</td><td style="padding: 0 15px">'.$custDetail['customer_email'].'</td></tr>'.
	    '<tr><td height="15px"></td></tr>'.
            '<tr><td style="padding: 0 15px">Property Details</td><td style="padding: 0 15px">'.$custDetail['customer_address'] . ' ' . $custDetail['building_name'].'</td></tr>'.
	    '<tr><td height="15px"></td></tr>'.
	    '<tr><td style="padding: 0 15px">Cleaning type</td><td style="padding: 0 15px">'.$details['cleaning_type'].'</td></tr>'.
            '<tr><td height="15px"></td></tr>'.
            '<tr><td style="padding: 0 15px">Requested service</td><td style="padding: 0 15px">'.$resultArray['cat'].'</td></tr>'.
            '<tr><td height="15px"></td></tr>'.
            '<tr><td style="padding: 0 15px">Cleaning Date & Time</td><td style="padding: 0 15px">'.$details['cleaning_date'] . ' ' . date('H:i A',strtotime($resultArray1 ['slot_start_time'])). '-' . date('H:i A',strtotime($resultArray1 ['slot_end_time'])).'</td></tr>'.
            '<tr><td height="15px"></td></tr>'.
            '<tr><td style="padding: 0 15px">Cancel Request date</td><td style="padding: 0 15px">'. date('Y-m-d').'</td></tr>'.
            '<tr><td height="15px"></td></tr>'.
            '<tr><td style="padding: 0 15px">Amount</td><td style="padding: 0 15px">'.$details['total_cost_paid'].'</td></tr>'.
            '<tr><td height="15px"></td></tr>'.
            '<tr><td style="padding: 0 15px">Payment method</td><td style="padding: 0 15px">'.$paymentStatusMsg.'</td></tr>'.
            '<tr><td height="15px"></td></tr>'.
            '<tr><td colspan="2" bgcolor="#e7e6e6"; style="padding: 15px 15px; font-size: 14px; line-height: 20px">'.
		'Do you still have any enquiry? Do not hesitate to call us at <a href="tel:+97145539799" style="color: #000; text-decoration: none">+971 4 553 9799</a> '.
                'or reach us at <a href="mailto:info@whitespot-cleaning.com" style="color: #000; text-decoration: none">info@whitespot-cleaning.com</a>'.
            '</td></tr>'.
            '<tr><td bgcolor="#051922" colspan="2">'.
            '<div style="font-size: 14px; color: #fff; line-height: 28px; padding: 5px 18px; width:345px;display: inline-block; vertical-align: middle; box-sizing: border-box">'.
		'Whitespot Cleaning Services L.L.C<br>'.
		'P.O. Box 4080, Dubai,UAE<br>'.
		'<a href="Tel:+97145539799" style="color: #fff; text-decoration: none">Tel: +97145539799</a><br>'.
		'<a href="info@whitespot-cleaning.com" style="color: #fff;text-decoration: none">info@whitespot-cleaning.com</a>'.
            '</div>'.
            '<div style="padding: 0 10px  0 15px; width:350px;display: inline-block; vertical-align: middle; box-sizing: border-box; text-align: right">'.
            '<span style="display: inline-block; vertical-align: bottom"><a href=""><img src="http://app.whitespot-cleaning.com/images/email/ig-icon.jpg"></a><a href=""><img src="images/email/fb-icon.jpg"></a></span>'.
            '<div style="display: inline-block; vertical-align: bottom; margin-left: 25px">'.
                '<span style="margin-bottom: 15px; display: block">'.
                '<a href=""><img src="http://app.whitespot-cleaning.com/images/email/icon-2.png"></a>'.
                '</span>'.
                '<span><a href=""><img src="http://app.whitespot-cleaning.com/images/email/icon-1.png"></a></span>'.
		'</div></div></td></tr></table></body></html>';

        $subject = "Booking Cancel.";
        $res = $this->sendEmail($custDetail['customer_email'], $message, $subject);



                $success = array('status' => "1", "msg" => "Your booking has been cancelled successfully. Please contact our customer support.", "code" => "200");

                file_put_contents("Log/cancelService.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/cancelService.log", print_r($page, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/cancelService.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/cancelService.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/cancelService.log", print_r($success, true) . "\r\n", FILE_APPEND);

                $this->response($this->json($success), 200);
            }
        } else {
            $error = array('status' => "0", "msg" => "Request Blank!", "code" => "201");

            file_put_contents("Log/scheduleServices.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/scheduleServices.log", print_r($page, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/scheduleServices.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/scheduleServices.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/scheduleServices.log", print_r($error, true) . "\r\n", FILE_APPEND);

            $this->response($this->json($error), 201);
        }
    }

    /*     * ************************End Cancel Schedule by Ayush kumar on 3/DEC/2017*************************** */






    /*     * ************************Modify schedule by Ayush kumar on 4/DEC/2017*************************** */

    private function modifyServiceSchedule() {

        $datetime = date('Y-m-d H:i:s');
        $page = "modify Service Schedule.";
        $inputData = json_decode(file_get_contents("php://input"), true);

        if ($this->get_request_method() != 'POST') {

            $error = array("status" => "0", "msg" => "Invalid request method.", "code" => "203");

            file_put_contents("Log/modifyServiceSchedule.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/modifyServiceSchedule.log", print_r($page, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/modifyServiceSchedule.log", print_r($error, true) . "\r\n", FILE_APPEND);
            //echo "ddd";
            $this->response($this->json($error), 203);
        }

        if (isset($inputData['data']) && !empty($inputData['data']) && isset($inputData['data']['customerId']) && !empty($inputData['data']['customerId']) && isset($inputData['data']['sessionId']) && !empty($inputData['data']['sessionId']) && isset($inputData['data']['bookingDetailId']) && !empty($inputData['data']['bookingDetailId']) && isset($inputData['data']['bookingId']) && !empty($inputData['data']['bookingId']) && isset($inputData['data']['slotId']) && !empty($inputData['data']['slotId']) && isset($inputData['data']['scheduleDate']) && !empty($inputData['data']['scheduleDate'])) {
            $data = array();

            $customerId = trim($inputData['data']['customerId']);
            $sessionId = trim($inputData['data']['sessionId']);

            $response = $this->authenticateUser($customerId, $sessionId);
            if (!$response) {

                $error = array('status' => "0", "msg" => "You are already login in another device. Please logout!", "code" => "203");

                file_put_contents("Log/modifyServiceSchedule.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/modifyServiceSchedule.log", print_r($page, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/modifyServiceSchedule.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/modifyServiceSchedule.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/modifyServiceSchedule.log", print_r($error, true) . "\r\n", FILE_APPEND);

                $this->response($this->json($error), 203);
            }
            //$startDate=strtotime($datetime);
            $startDate = date('Y-m-d');


            $condition = " Where tbr.customer_id=" . $customerId . " AND tbd.cleaning_date >='" . $startDate . "'" . " AND tbr.booking_id=" . $inputData['data']['bookingId'] . " AND tbd.booking_detail_id !=" . $inputData['data']['bookingDetailId'] . " AND tbd.slot_id=" . $inputData['data']['slotId'] . " AND tbd.cleaning_date ='" . $inputData['data']['scheduleDate'] . "'" . " AND tbd.status!='I'";

            $sql = "SELECT tbr.customer_id,tbd.booking_detail_id,tbr.booking_id,tbd.cleaning_date,tcs.slot_start_time,tcs.slot_end_time,tcs.slot_id from table_booking_register as tbr left join table_booking_details as tbd on tbd.booking_id=tbr.booking_id left join table_calender_slots as tcs on tcs.slot_id=tbd.slot_id" . $condition;

            //echo $sql;exit;
            $result = $this->database->query($sql);

            //print_r($result);exit;

            if ($result->num_rows > 0) {
                $error = array('status' => "0", "msg" => "This schedule already exists!.", "code" => "212");

                file_put_contents("Log/modifyServiceSchedule.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/modifyServiceSchedule.log", print_r($page, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/modifyServiceSchedule.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/modifyServiceSchedule.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/modifyServiceSchedule.log", print_r($error, true) . "\r\n", FILE_APPEND);

                $this->response($this->json($error), 212);
            } else {

            	$condition11 = " Where tbr.booking_id=" . $inputData['data']['bookingId'] . " AND tbd.booking_detail_id=" . $inputData['data']['bookingDetailId'];

            	$sql11 = "SELECT ct.cleaning_type,tbr.customer_address_id ,tbr.customer_id,tbd.booking_detail_id,tbr.booking_id,tbd.amount,tbd.cleaning_date,tcs.slot_start_time,tbr.total_cost_paid ,tcs.	slot_end_time,tcs.slot_id from table_booking_register as tbr left join table_booking_details as tbd on tbd.booking_id=tbr.booking_id left join table_calender_slots as tcs on tcs.slot_id=tbd.slot_id left join table_cleaning_type as ct on ct.cleaning_type_id = tbr.cleaning_type_id" . $condition11;
            	$result11 = $this->database->query($sql11);
            	$details = mysqli_fetch_assoc($result11);

	            //echo $sql;exit;
	            $result = $this->database->query($sql);

                $condi = " Where booking_detail_id=" . $inputData['data']['bookingDetailId'];
                $newDate = date('Y-m-d', strtotime($inputData['data']['scheduleDate']));
                $updateSQl = "UPDATE table_booking_details SET slot_id=" . $inputData['data']['slotId'] . ", cleaning_date='" . $newDate . "'" . $condi;
                //print_r($updateSQl);die;
                $this->database->query($updateSQl);

                
                //Send Mail and sms
		        $cusquery = "Select c.*,b.building_name,u.unit_name,ca.customer_address from table_customer as c  "
		                . "left join table_customer_address as ca on ca.customer_id = c.customer_id "
		                . "left join table_buildings as b on b.building_id= ca.building_id "
		                . "left join table_unit_type as u on u.unit_type_id = b.unit_type_id"
		                . " where c.customer_id=" . $details['customer_id'] . " and ca.customer_address_id ='" . $details['customer_address_id'] . "'";
		       	$cusResult = $this->database->query($cusquery);
		        $custDetail = mysqli_fetch_assoc($cusResult);


		        $bookingId = $details['booking_id'];

		        $findRoomCat = "Select GROUP_CONCAT( CONCAT(tbrtm.counter,' ',tc.category_name) SEPARATOR ',') as cat from table_booking_room_type_mapping as tbrtm left join table_category_type as tct on tct.room_category_id=tbrtm.room_category_id left join table_category as tc on tc.category_id = tct.category_id  where tbrtm.booking_id=" . $bookingId . " group by tbrtm.booking_id";
		        
				$queryresultCat = $this->database->query($findRoomCat);
				        $resultArray = mysqli_fetch_assoc($queryresultCat);


				$findRoomCat1 = "SELECT cs.slot_start_time,cs.slot_end_time FROM table_booking_details AS bd LEFT JOIN table_calender_slots AS cs ON cs.slot_id = bd.slot_id WHERE bd.booking_id =" . $bookingId . " GROUP BY bd.booking_id";

				

				$queryresultCat1 = $this->database->query($findRoomCat1);
		        $resultArray1 = mysqli_fetch_assoc($queryresultCat1);

		        if ($inputData['data']['paymentType'] == "cash") {
		            $paymentStatusMsg = "Cash";
		        } else {
		            $paymentStatusMsg = "Paid";
		        }


				$findSlot = "SELECT cs.slot_start_time,cs.slot_end_time From table_calender_slots AS cs where cs.slot_id =" . $inputData['data']['slotId'];
				$queryfindSlot = $this->database->query($findSlot);
		        $slots11 = mysqli_fetch_assoc($queryfindSlot);

		        $message = '<html><body><table style="font-family: arial"; align="center" width="700" cellpadding="0" cellspacing="0">'.
            '<tr><td colspan="2" style="border-bottom: 1px solid #fff"><img src="http://app.whitespot-cleaning.com/images/email/header.jpg"></td></tr>'.
            '<tr><td bgcolor="#051922" align="center" style="color: #fff; padding: 10px; font-weight: bold" colspan="2">YOUR BOOKING HAS BEEN MODIFIED</td></tr>'.
	    '<tr><td height="15px"></td></tr>'.
	    '<tr><td style="padding: 0 15px;">Reference No</td><td style="padding: 0 15px">'.$bookingId.'</td></tr>'.
            '<tr><td height="15px"></td></tr>'.
            '<tr><td style="padding: 0 15px">Guest Name</td><td style="padding: 0 15px">'.$custDetail['customer_name'].'</td></tr>'.
            '<tr><td height="15px"></td></tr>'.
	    '<tr><td style="padding: 0 15px">Phone Number</td><td style="padding: 0 15px">'.$custDetail['customer_number'].'</td></tr>'.
	    '<tr><td height="15px"></td></tr>'.

	    '<tr><td style="padding: 0 15px">Email</td><td style="padding: 0 15px">'.$custDetail['customer_email'].'</td></tr>'.
	    '<tr><td height="15px"></td></tr>'.
            '<tr><td style="padding: 0 15px">Property Details</td><td style="padding: 0 15px">'.$custDetail['customer_address'] . ' ' . $custDetail['building_name'].'</td></tr>'.
	    '<tr><td height="15px"></td></tr>'.
	    '<tr><td style="padding: 0 15px">Cleaning type</td><td style="padding: 0 15px">'.$details['cleaning_type'].'</td></tr>'.
            '<tr><td height="15px"></td></tr>'.
            '<tr><td style="padding: 0 15px">Requested service</td><td style="padding: 0 15px">'.$resultArray['cat'].'</td></tr>'.
            '<tr><td height="15px"></td></tr>'.
            '<tr><td style="padding: 0 15px">Previous Booking</td><td style="padding: 0 15px">'.$details['cleaning_date'] . ' ' . date('H:i A',strtotime($details ['slot_start_time'])). '-' . date('H:i A',strtotime($details ['slot_end_time'])).'</td></tr>'.
            '<tr><td height="15px"></td></tr>'.
            '<tr><td style="padding: 0 15px">New Booking</td><td style="padding: 0 15px">'. date('Y-m-d',strtotime($inputData['data']['scheduleDate'])). ' ' . date('H:i A',strtotime($slots11 ['slot_start_time'])). '-' . date('H:i A',strtotime($slots11 ['slot_end_time'])).'</td></tr>'.
            '<tr><td height="15px"></td></tr>'.
            '<tr><td style="padding: 0 15px">Amount</td><td style="padding: 0 15px">'.$details['amount'].'</td></tr>'.
            '<tr><td height="15px"></td></tr>'.
            '<tr><td style="padding: 0 15px">Payment method</td><td style="padding: 0 15px">'.$paymentStatusMsg.'</td></tr>'.
            '<tr><td height="15px"></td></tr>'.
            '<tr><td colspan="2" bgcolor="#e7e6e6"; style="padding: 15px 15px; font-size: 14px; line-height: 20px">'.
		'Do you still have any enquiry? Do not hesitate to call us at <a href="tel:+97145539799" style="color: #000; text-decoration: none">+971 4 553 9799</a> '.
                'or reach us at <a href="mailto:info@whitespot-cleaning.com" style="color: #000; text-decoration: none">info@whitespot-cleaning.com</a>'.
            '</td></tr>'.
            '<tr><td bgcolor="#051922" colspan="2">'.
            '<div style="font-size: 14px; color: #fff; line-height: 28px; padding: 5px 18px; width:345px;display: inline-block; vertical-align: middle; box-sizing: border-box">'.
		'Whitespot Cleaning Services L.L.C<br>'.
		'P.O. Box 4080, Dubai,UAE<br>'.
		'<a href="Tel:+97145539799" style="color: #fff; text-decoration: none">Tel: +97145539799</a><br>'.
		'<a href="info@whitespot-cleaning.com" style="color: #fff;text-decoration: none">info@whitespot-cleaning.com</a>'.
            '</div>'.
            '<div style="padding: 0 10px  0 15px; width:350px;display: inline-block; vertical-align: middle; box-sizing: border-box; text-align: right">'.
            '<span style="display: inline-block; vertical-align: bottom"><a href=""><img src="http://app.whitespot-cleaning.com/images/email/ig-icon.jpg"></a><a href=""><img src="images/email/fb-icon.jpg"></a></span>'.
            '<div style="display: inline-block; vertical-align: bottom; margin-left: 25px">'.
                '<span style="margin-bottom: 15px; display: block">'.
                '<a href=""><img src="http://app.whitespot-cleaning.com/images/email/icon-2.png"></a>'.
                '</span>'.
                '<span><a href=""><img src="http://app.whitespot-cleaning.com/images/email/icon-1.png"></a></span>'.
		'</div></div></td></tr></table></body></html>';
        $subject = "Booking cancelled.";
        $res = $this->sendEmail($details['customer_email'], $message, $subject);



                //$res=$this->sendEmail('parul.nirwan@quytech.com',$message,$subject);
                // $res = $this->sendEmail('info@whitespot-cleaning.com', $message, $subject);
                // $res = $this->sendEmail('shahzad@whitespot-cleaning.com', $message, $subject);
                // $res = $this->sendEmail('uppercrest@whitespot-cleaning.com', $message, $subject);


                $mobileMsg = "Your booking has been updated succesfully.";
                // $smsRes = $this->sendSms($custDetail['customer_number'], $mobileMsg);
                // $smsRes = $this->sendSms('0569635534', $mobileMsg);
                // $smsRes = $this->sendSms('0555058406', $mobileMsg);
                // $smsRes = $this->sendSms('0525194828', $mobileMsg);

                //End 
                $success = array('status' => "1", "msg" => "Your Schedule has been updated", "code" => "200");

                file_put_contents("Log/modifyServiceSchedule.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/modifyServiceSchedule.log", print_r($page, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/modifyServiceSchedule.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/modifyServiceSchedule.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/modifyServiceSchedule.log", print_r($success, true) . "\r\n", FILE_APPEND);

                $this->response($this->json($success), 200);
            }
        } else {
            $error = array('status' => "0", "msg" => "Request Blank!", "code" => "201");

            file_put_contents("Log/modifyServiceSchedule.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/modifyServiceSchedule.log", print_r($page, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/modifyServiceSchedule.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/modifyServiceSchedule.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/modifyServiceSchedule.log", print_r($error, true) . "\r\n", FILE_APPEND);

            $this->response($this->json($error), 201);
        }
    }

    /*     * ************************End Modify Schedule by Ayush kumar on 3/DEC/2017*************************** */




    /*     * ************************Current Schedule And profile by Ayush kumar on 4/DEC/2017*************************** */

    private function currentServiceAndProfile() {

        $datetime = date('Y-m-d H:i:s');
        $page = "Current Service And Profile Information.";
        $inputData = json_decode(file_get_contents("php://input"), true);

        if ($this->get_request_method() != 'POST') {

            $error = array("status" => "0", "msg" => "Invalid request method.", "code" => "203");

            file_put_contents("Log/currentServiceAndProfile.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/currentServiceAndProfile.log", print_r($page, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/currentServiceAndProfile.log", print_r($error, true) . "\r\n", FILE_APPEND);
            //echo "ddd";
            $this->response($this->json($error), 203);
        }

        if (isset($inputData['data']) && !empty($inputData['data']) && isset($inputData['data']['customerId']) && !empty($inputData['data']['customerId']) && isset($inputData['data']['sessionId']) && !empty($inputData['data']['sessionId'])) {
            $data = array();

            $customerId = trim($inputData['data']['customerId']);
            $sessionId = trim($inputData['data']['sessionId']);

            $response = $this->authenticateUser($customerId, $sessionId);
            if (!$response) {

                $error = array('status' => "0", "msg" => "You are already login in another device. Please logout!", "code" => "203");

                file_put_contents("Log/currentServiceAndProfile.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/currentServiceAndProfile.log", print_r($page, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/currentServiceAndProfile.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/currentServiceAndProfile.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/currentServiceAndProfile.log", print_r($error, true) . "\r\n", FILE_APPEND);

                $this->response($this->json($error), 203);
            }
            $startDate = date('Y-m-d');
            $condition = " Where tc.status='A'" . " AND tc.customer_id=" . $customerId;
            $sql = "SELECT tc.customer_number,tc.customer_id,tc.customer_name,tc.customer_email,tc.created_date,tc.image from table_customer as tc " . $condition . "";
            $result = $this->database->query($sql);
            if ($result->num_rows <= 0) {
                $error = array('status' => "0", "msg" => "No Details found!.", "code" => "209");

                file_put_contents("Log/currentServiceAndProfile.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/currentServiceAndProfile.log", print_r($page, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/currentServiceAndProfile.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/currentServiceAndProfile.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/currentServiceAndProfile.log", print_r($error, true) . "\r\n", FILE_APPEND);

                $this->response($this->json($error), 209);
            } else {
                $condition2 = " Where tc.status='A'" . " AND tbd.cleaning_date>='" . $startDate . "'" . " AND tc.customer_id=" . $customerId . " AND tbd.status='A'";

                $sql2 = "SELECT tc.customer_id,tc.customer_name,tc.created_date,tc.image,tbd.booking_id,tbd.booking_detail_id,tbd.cleaning_date,tcs.slot_start_time,tcs.slot_end_time from table_customer as tc left join table_booking_register as tbr on tbr.customer_id=tc.customer_id left join table_booking_details as tbd on tbd.booking_id=tbr.booking_id left join table_calender_slots as tcs on tcs.slot_id=tbd.slot_id " . $condition2 . " order by tbd.cleaning_date,tcs.slot_start_time LIMIT 5";
                $result2 = $this->database->query($sql2);
                $serviceArr = array();
                if ($result2->num_rows > 0) {
                    $j = 0;
                    while ($row2 = mysqli_fetch_assoc($result2)) {
                        $serviceArr[$j]['cleaning_date'] = $row2['cleaning_date'];
                        $serviceArr[$j]['slot_start_time'] = date('h:i A', strtotime($row2['slot_start_time']));
                        $serviceArr[$j]['slot_end_time'] = date('h:i A', strtotime($row2['slot_end_time']));
                        $j++;
                    }
                }
                $Arr = mysqli_fetch_assoc($result);
                $todayDate = date('Y-m-d');
                $date1 = new DateTime($Arr['created_date']);
                $date2 = new DateTime($todayDate);
                $diff = $date1->diff($date2);
                if (isset($Arr['image']) && !empty($Arr['image'])) {
                    $Arr['image'] = self::HOST . "/services/customer_image/" . $Arr['image'];
                }
                $str = "Member since " . $diff->y . " year and " . $diff->m . " month";
                $data['customerId'] = $Arr['customer_id'];
                $data['customerName'] = $Arr['customer_name'];
                $data['customerEmail'] = $Arr['customer_email'];
                if(isset($Arr['customer_number']) && !empty($Arr['customer_number'])) {
                    $data['customerNumber'] = $Arr['customer_number'];
                }
                $data['createdDate'] = $Arr['created_date'];
                $data['image'] = $Arr['image'];
                $data['service'] = $serviceArr;
                $data['memberSince'] = $str;
                $data['maintenace_text'] = "Just squeeze a lemon into a bowl containing 1/2 cup of water and microwave for 3 minutes.Let it stand for 5 minutes and then wipe the microwave clean! ";

                
                $condition = " Where tbr.customer_id='" . $customerId . "'order by tbr.booking_id desc LIMIT 1";

                $planQuery = "Select tbr.booking_id,tbr.cleaning_plan,tb.building_name,ta.area_name,c.city_name,tca.customer_address,tca.lat,tca.lng from table_booking_register as tbr left join table_customer_address as tca on tca.customer_address_id=tbr.customer_address_id left join table_buildings as tb on tb.building_id=tca.building_id left join table_area as ta on ta.area_id=tb.area_id left join city as c on c.city_id=ta.city_id" . $condition;
                //echo $planQuery;die;
                $planResult = $this->database->query($planQuery);
                if ($planResult->num_rows > 0) {
                    $planData = mysqli_fetch_assoc($planResult);
                    if (empty($planData['area_name'])) {
                        $planData['area_name'] = '';
                    }

                    if (empty($planData['city_name'])) {
                        $planData['city_name'] = '';
                    }
                    if (strtolower($planData['cleaning_plan']) == 'o') {
                        $plan = "One Time";
                    } else {
                        $plan = "Contract";
                    }

                    $data['location'] = rtrim($planData['customer_address'] . ',' . $planData['building_name'] . ',' . $planData['area_name'] . ',' . $planData['city_name'], ",");

                    $data['lat'] = $planData['lat'];
                    $data['lng'] = $planData['lng'];
                    $data['cleaning_plan'] = $plan;
                } else {

                    $data['location'] = "";
                    $data['lat'] = "";
                    $data['lng'] = "";
                    $data['cleaning_plan'] = "";
                }
                
                
                $success = array("status" => "1", "msg" => "Your current schedule", "code" => "200", "data" => $data);

                file_put_contents("Log/currentServiceAndProfile.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/currentServiceAndProfile.log", print_r($page, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/currentServiceAndProfile.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/currentServiceAndProfile.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/currentServiceAndProfile.log", print_r($success, true) . "\r\n", FILE_APPEND);

                $this->response($this->json($success), 200);
            }
        } else {
            $error = array('status' => "0", "msg" => "Request Blank!", "code" => "201");

            file_put_contents("Log/currentServiceAndProfile.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/currentServiceAndProfile.log", print_r($page, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/currentServiceAndProfile.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/currentServiceAndProfile.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/currentServiceAndProfile.log", print_r($error, true) . "\r\n", FILE_APPEND);

            $this->response($this->json($error), 201);
        }
    }

    /*     * ************************End Current Schedule And profile by Ayush kumar on 3/DEC/2017*************************** */



    /*     * **************************************city by Ayush kumar on 29/nov/2017************* */

    public function city() {
        $datetime = date('Y-m-d H:i:s');
        $page = "city.";
        if ($this->get_request_method() != 'GET') {

            $error = array("status" => "0", "msg" => "Invalid request method.", "code" => "203");

            file_put_contents("Log/city.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/city.log", print_r($page, true) . "\r\n", FILE_APPEND);

            file_put_contents("Log/city.log", print_r($error, true) . "\r\n", FILE_APPEND);
            //echo "ddd";
            $this->response($this->json($error), 203);
        }

        $condition = " Where tc.status='A'";
        $cityQuery = "Select city_id,city_name from city as tc" . $condition;
        $result = $this->database->query($cityQuery);
        //print_r($locationQuery);exit;		
        if ($result->num_rows > 0) {
            $data = array();
            while ($row = mysqli_fetch_assoc($result)) {
                $data[] = $row;
            }
            $success = array("status" => "1", "msg" => "All city.", "code" => "200", "data" => $data);

            file_put_contents("Log/city.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/city.log", print_r($page, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/city.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/city.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/city.log", print_r($success, true) . "\r\n", FILE_APPEND);

            $this->response($this->json($success), 200);
        } else {
            $error = array('status' => "0", "msg" => "Data Does Not Exist!.", "code" => "209");

            file_put_contents("Log/city.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/city.log", print_r($page, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/city.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/city.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/city.log", print_r($error, true) . "\r\n", FILE_APPEND);

            $this->response($this->json($error), 209);
        }
    }

    /*     * *********End city ************* */







    /*     * *********End Location ************* */



    /*     * **************************************Buildings by Ayush kumar on 29/nov/2017************* */

    public function buildings() {
        $datetime = date('Y-m-d H:i:s');
        $page = "Customer Buildings.";

        if ($this->get_request_method() != 'POST') {

            $error = array("status" => "0", "msg" => "Invalid request method.", "code" => "203");

            file_put_contents("Log/buildings.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/buildings.log", print_r($page, true) . "\r\n", FILE_APPEND);

            file_put_contents("Log/buildings.log", print_r($error, true) . "\r\n", FILE_APPEND);
            //echo "ddd";
            $this->response($this->json($error), 203);
        }
        $inputData = json_decode(file_get_contents("php://input"), true);

        if (isset($inputData['data']) && !empty($inputData['data']) && isset($inputData['data']['areaId']) && !empty($inputData['data']['areaId'])) {

            $condition = " Where tb.status='A' AND tb.area_id=" . $inputData['data']['areaId'];
            $buildingQuery = "Select tb.building_id, tb.building_name,tb.lat,tb.lng from table_buildings as tb" . $condition;
            $result = $this->database->query($buildingQuery);
            //print_r($result);exit;
            if ($result->num_rows > 0) {
                $data = array();
                while ($row = mysqli_fetch_assoc($result)) {
                    $data[] = $row;
                }
                $success = array("status" => "1", "msg" => "All Buildings.", "code" => "200", "data" => $data);

                file_put_contents("Log/buildings.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/buildings.log", print_r($page, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/buildings.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/buildings.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/buildings.log", print_r($success, true) . "\r\n", FILE_APPEND);

                $this->response($this->json($success), 200);
            } else {
                $error = array('status' => "0", "msg" => "Data Does Not Exist!.", "code" => "209");

                file_put_contents("Log/buildings.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/buildings.log", print_r($page, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/buildings.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/buildings.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/buildings.log", print_r($error, true) . "\r\n", FILE_APPEND);

                $this->response($this->json($error), 209);
            }
        } else {

            $error = array('status' => "0", "msg" => "Request Blank!.", "code" => "201");

            file_put_contents("Log/buildings.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/buildings.log", print_r($page, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/buildings.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/buildings.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/buildings.log", print_r($error, true) . "\r\n", FILE_APPEND);

            $this->response($this->json($error), 201);
        }
    }

    /*     * ********End Buildings******** */




    /*     * **************************************SAve Address by Ayush kumar on 29/nov/2017************* */

    public function saveAddress() {
        $datetime = date('Y-m-d H:i:s');
        $page = "Save Address.";

        if ($this->get_request_method() != 'POST') {

            $error = array("status" => "0", "msg" => "Invalid request method.", "code" => "203");

            file_put_contents("Log/saveAddress.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/saveAddress.log", print_r($page, true) . "\r\n", FILE_APPEND);

            file_put_contents("Log/saveAddress.log", print_r($error, true) . "\r\n", FILE_APPEND);
            //echo "ddd";
            $this->response($this->json($error), 203);
        }

        $inputData = json_decode(file_get_contents("php://input"), true);

        if (isset($inputData['data']) && !empty($inputData['data']) && isset($inputData['data']['areaId']) && !empty($inputData['data']['areaId']) && isset($inputData['data']['buildingId']) && !empty($inputData['data']['buildingId']) && isset($inputData['data']['otherBuilding']) && isset($inputData['data']['customerAddress']) && !empty($inputData['data']['customerAddress']) && isset($inputData['data']['customerId']) && !empty($inputData['data']['customerId']) && isset($inputData['data']['sessionId']) && !empty($inputData['data']['sessionId'])) {

            $customerId = trim($inputData['data']['customerId']);
            $sessionId = trim($inputData['data']['sessionId']);

            $response = $this->authenticateUser($customerId, $sessionId);

            if (!$response) {

                $error = array('status' => "0", "msg" => "You are already login in another device. Please logout!", "code" => "203");

                file_put_contents("Log/saveAddress.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/saveAddress.log", print_r($page, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/saveAddress.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/saveAddress.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/saveAddress.log", print_r($error, true) . "\r\n", FILE_APPEND);

                $this->response($this->json($error), 203);
            }
            $account_id = 1;
            $customer_id = $inputData['data']['customerId'];
            $building_id = $inputData['data']['buildingId'];
            $other_building = $inputData['data']['otherBuilding'];
            $customer_address = $inputData['data']['customerAddress'];
            $status = 'A';

            $addressSql = "INSERT INTO table_customer_address (account_id,customer_id,building_id,other_building, customer_address,status) VALUES ('" . $account_id . "', '" . $customer_id . "','" . $building_id . "', '" . $other_building . "', '" . $customer_address . "','" . $status . "')";

            $result = $this->database->query($addressSql);

            $success = array("status" => "1", "msg" => "Address Saved.", "code" => "200");

            file_put_contents("Log/saveAddress.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/saveAddress.log", print_r($page, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/saveAddress.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/saveAddress.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/saveAddress.log", print_r($success, true) . "\r\n", FILE_APPEND);

            $this->response($this->json($success), 200);
        } else {

            $error = array('status' => "0", "msg" => "Request Blank!.", "code" => "201");

            file_put_contents("Log/saveAddress.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/saveAddress.log", print_r($page, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/saveAddress.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/saveAddress.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/saveAddress.log", print_r($error, true) . "\r\n", FILE_APPEND);

            $this->response($this->json($error), 201);
        }
    }

    /*     * *****************************End Save Addresss*************************** */




    /*     * **************************************Save custom Address by Ayush kumar on 29/nov/2017************* */

    public function saveCustomAddress() {
        $datetime = date('Y-m-d H:i:s');
        $page = "Save Address.";

        if ($this->get_request_method() != 'POST') {

            $error = array("status" => "0", "msg" => "Invalid request method.", "code" => "203");

            file_put_contents("Log/saveCustomAddress.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/saveCustomAddress.log", print_r($page, true) . "\r\n", FILE_APPEND);

            file_put_contents("Log/saveCustomAddress.log", print_r($error, true) . "\r\n", FILE_APPEND);
            //echo "ddd";
            $this->response($this->json($error), 203);
        }
        $inputData = json_decode(file_get_contents("php://input"), true);

        if (isset($inputData['data']) && !empty($inputData['data']) && isset($inputData['data']['customerAddress']) && !empty($inputData['data']['customerAddress']) && isset($inputData['data']['customerId']) && !empty($inputData['data']['customerId']) && isset($inputData['data']['sessionId']) && !empty($inputData['data']['sessionId']) && isset($inputData['data']['building_id']) && !empty($inputData['data']['building_id'])) {

            $customerId = trim($inputData['data']['customerId']);
            $sessionId = trim($inputData['data']['sessionId']);

            $response = $this->authenticateUser($customerId, $sessionId);

            if (!$response) {

                $error = array('status' => "0", "msg" => "You are already login in another device. Please logout!", "code" => "203");

                file_put_contents("Log/saveCustomAddress.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/saveCustomAddress.log", print_r($page, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/saveCustomAddress.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/saveCustomAddress.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/saveCustomAddress.log", print_r($error, true) . "\r\n", FILE_APPEND);

                $this->response($this->json($error), 203);
            }
            $account_id = 1;
            $customer_id = $inputData['data']['customerId'];
            $customer_address = $inputData['data']['customerAddress'];
            $buidling = $inputData['data']['building_id'];
            $isSave = $inputData['data']['is_saved_address'];
            $lat = $inputData['data']['lat'];
            $lng = $inputData['data']['lng'];
            $area = $inputData['data']['area_id'];

            $status = 'A';


            $condition = " Where tc.customer_id=" . $customerId . " AND tc.status='A' AND tc.building_id = '" . $buidling . "' AND tc.customer_address like '" . $customer_address . "'";

            $addQuery = "SELECT * from table_customer_address as tc " . $condition;

            $result = $this->database->query($addQuery);

            if ($result->num_rows > 0) {
                while ($row = mysqli_fetch_assoc($result)) {
                    $addressId = $row['customer_address_id'];
                }
                $addressSql = "UPDATE table_customer_address as tc SET is_saved_address='" . $isSave . "' " . $condition;
                $result = $this->database->query($addressSql);
            } else {

                $addressSql = "INSERT INTO table_customer_address (account_id,customer_id,building_id,customer_address,status,is_saved_address,lat,lng,area_id) VALUES ('" . $account_id . "', '" . $customer_id . "', '" . $buidling . "','" . $customer_address . "', 'A', '" . $isSave . "', '" . $lat . "', '" . $lng . "','" . $area . "')";

                $result = $this->database->query($addressSql);
                $last_id = $this->database->insert_id;
                $addressId = $last_id;
            }




            $success = array("status" => "1", "msg" => "Address Saved.", "code" => "200", "data" => array('customer_address_id' => $addressId));

            file_put_contents("Log/saveCustomAddress.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/saveCustomAddress.log", print_r($page, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/saveCustomAddress.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/saveCustomAddress.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/saveCustomAddress.log", print_r($success, true) . "\r\n", FILE_APPEND);

            $this->response($this->json($success), 200);
        } else {

            $error = array('status' => "0", "msg" => "Request Blank!.", "code" => "201");

            file_put_contents("Log/saveCustomAddress.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/saveCustomAddress.log", print_r($page, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/saveCustomAddress.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/saveCustomAddress.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/saveCustomAddress.log", print_r($error, true) . "\r\n", FILE_APPEND);

            $this->response($this->json($error), 201);
        }
    }

    /*     * *****************************End Save custom Addresss*************************** */





    /*     * **************************************Get Customer Address by Ayush kumar on 29/nov/2017************* */

    public function getAddress() {
        $datetime = date('Y-m-d H:i:s');
        $page = "Get Address.";

        if ($this->get_request_method() != 'POST') {

            $error = array("status" => "0", "msg" => "Invalid request method.", "code" => "203");

            file_put_contents("Log/getAddress.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/getAddress.log", print_r($page, true) . "\r\n", FILE_APPEND);

            file_put_contents("Log/getAddress.log", print_r($error, true) . "\r\n", FILE_APPEND);
            //echo "ddd";
            $this->response($this->json($error), 203);
        }

        $inputData = json_decode(file_get_contents("php://input"), true);
        if (isset($inputData['data']) && !empty($inputData['data']) && isset($inputData['data']['customerId']) && !empty($inputData['data']['customerId']) && isset($inputData['data']['sessionId']) && !empty($inputData['data']['sessionId'])) {

            $customerId = trim($inputData['data']['customerId']);
            $sessionId = trim($inputData['data']['sessionId']);

            $response = $this->authenticateUser($customerId, $sessionId);
            if (!$response) {

                $error = array('status' => "0", "msg" => "You are already login in another device. Please logout!", "code" => "203");

                file_put_contents("Log/getAddress.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/getAddress.log", print_r($page, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/getAddress.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/getAddress.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/getAddress.log", print_r($error, true) . "\r\n", FILE_APPEND);

                $this->response($this->json($error), 203);
            }

            $condition = " Where tc.customer_id=" . $customerId . " AND tc.status='A' AND tca.is_saved_address = '1'";

            $addQuery = "SELECT tca.customer_address_id,tc.customer_name,tc.customer_id,tca.customer_address,tca.lat,tca.lng,tb.unit_type_id,tca.building_id,tb.area_id,tb.building_name,tb.linen_change,tb.balcony,aa.area_name from table_customer as tc left join table_customer_address as tca on tca.customer_id=tc.customer_id left join table_buildings as tb on tb.building_id=tca.building_id left join table_area as aa on aa.area_id = tca.area_id" . $condition;

            $result = $this->database->query($addQuery);

            if ($result->num_rows > 0) {
                while ($row = mysqli_fetch_assoc($result)) {
                    if (empty($row['building_name'])) {
                        $row['building_name'] = '';
                    }
                    if (empty($row['area_name'])) {
                        $row['area_name'] = '';
                    }
                    $data[] = $row;
                }
                $success = array("status" => "1", "msg" => "All address.", "code" => "200", "data" => $data);

                file_put_contents("Log/getAddress.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/getAddress.log", print_r($page, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/getAddress.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/getAddress.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/getAddress.log", print_r($success, true) . "\r\n", FILE_APPEND);

                $this->response($this->json($success), 200);
            } else {

                $error = array('status' => "0", "msg" => "Data does not exits!.", "code" => "209");

                file_put_contents("Log/getAddress.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/getAddress.log", print_r($page, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/getAddress.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/getAddress.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/getAddress.log", print_r($error, true) . "\r\n", FILE_APPEND);

                $this->response($this->json($error), 209);
            }
        } else {

            $error = array('status' => "0", "msg" => "Request Blank!.", "code" => "201");

            file_put_contents("Log/getAddress.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/getAddress.log", print_r($page, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/getAddress.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/getAddress.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/getAddress.log", print_r($error, true) . "\r\n", FILE_APPEND);

            $this->response($this->json($error), 201);
        }
    }

    /*     * *****************************End Get Customer Address*************************** */


    /*     * **************************************Get Customer lat,long by custom  Address by Ayush kumar on 18/Dec/2017************* */

    public function getLatLong() {
        $datetime = date('Y-m-d H:i:s');
        $page = "Get Lat Long.";

        if ($this->get_request_method() != 'POST') {

            $error = array("status" => "0", "msg" => "Invalid request method.", "code" => "203");

            file_put_contents("Log/getLatLong.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/getLatLong.log", print_r($page, true) . "\r\n", FILE_APPEND);

            file_put_contents("Log/getLatLong.log", print_r($error, true) . "\r\n", FILE_APPEND);
            //echo "ddd";
            $this->response($this->json($error), 203);
        }

        $inputData = json_decode(file_get_contents("php://input"), true);
        if (isset($inputData['data']) && !empty($inputData['data']) && isset($inputData['data']['customAddress']) && !empty($inputData['data']['customAddress'])
        ) {

            $data = array();
            $customAddress = trim($inputData['data']['customAddress']);


            $formattedAddr = str_replace(' ', '+', $customAddress);
            $geocodeFromAddr = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address=' . $formattedAddr . '&sensor=false&key=AIzaSyA23EqQp5gBdQnFrE586EOjhk-_O1i7d9k ');
            $output = json_decode($geocodeFromAddr);
            $data['lat'] = $output->results[0]->geometry->location->lat;
            $data['lng'] = $output->results[0]->geometry->location->lng;
            if (!empty($data) && ($data['lat'] != null) && ($data['lng'] != null)) {

                $success = array("status" => "1", "msg" => "Lat Long found.", "code" => "200", "data" => $data);

                file_put_contents("Log/getLatLong.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/getLatLong.log", print_r($page, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/getLatLong.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/getLatLong.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/getLatLong.log", print_r($success, true) . "\r\n", FILE_APPEND);

                $this->response($this->json($success), 200);
            } else {
                $error = array('status' => "0", "msg" => "Address is invalid!.", "code" => "203");

                file_put_contents("Log/getLatLong.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/getLatLong.log", print_r($page, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/getLatLong.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/getLatLong.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/getLatLong.log", print_r($error, true) . "\r\n", FILE_APPEND);

                $this->response($this->json($error), 201);
            }
        } else {

            $error = array('status' => "0", "msg" => "Request Blank!.", "code" => "201");

            file_put_contents("Log/getLatLong.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/getLatLong.log", print_r($page, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/getLatLong.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/getLatLong.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/getLatLong.log", print_r($error, true) . "\r\n", FILE_APPEND);

            $this->response($this->json($error), 201);
        }
    }

    /*     * *****************************End Get Custom Address Lat LOng*************************** */

    /*     * **************************************Get Payment Detail************* */

    public function getPaymentDetail() {
        $datetime = date('Y-m-d H:i:s');
        $page = "Get Payment Detail.";

        if ($this->get_request_method() != 'POST') {

            $error = array("status" => "0", "msg" => "Invalid request method.", "code" => "203");

            file_put_contents("Log/getPaymentDetail.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/getPaymentDetail.log", print_r($page, true) . "\r\n", FILE_APPEND);

            file_put_contents("Log/getPaymentDetail.log", print_r($error, true) . "\r\n", FILE_APPEND);
            //echo "ddd";
            $this->response($this->json($error), 203);
        }

        $inputData = json_decode(file_get_contents("php://input"), true);

        if (isset($inputData['data']) && !empty($inputData['data']) && isset($inputData['data']['customerId']) && !empty($inputData['data']['customerId']) && isset($inputData['data']['paymentId']) && isset($inputData['data']['paymentId']) && isset($inputData['data']['sessionId']) && !empty($inputData['data']['sessionId'])) {

            $customerId = trim($inputData['data']['customerId']);
            $sessionId = trim($inputData['data']['sessionId']);

            $response = $this->authenticateUser($customerId, $sessionId);

            if (!$response) {

                $error = array('status' => "0", "msg" => "You are already login in another device. Please logout!", "code" => "203");

                file_put_contents("Log/getPaymentDetail.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/getPaymentDetail.log", print_r($page, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/getPaymentDetail.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/getPaymentDetail.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/getPaymentDetail.log", print_r($error, true) . "\r\n", FILE_APPEND);

                $this->response($this->json($error), 203);
            }

            $payementId = $inputData['data']['paymentId'];
            $condition = " Where tph.customer_id=" . $customerId . " AND tph.payment_id=" . $payementId;

            $Query1 = "SELECT tph.*,tbr.*,ca.customer_address,bb.building_name,aa.	area_name,tut.unit_name,tut.unit_image2,tsc.card_holder_name,tsc.card_number,tsc.expiry_month,tsc.expiry_year from table_payment_history as tph left join table_booking_register as tbr on tbr.payment_id=tph.payment_id left join table_unit_type as tut on tut.unit_type_id=tbr.unit_type_id left join table_saved_cards as tsc on tph.saved_card_id=tsc.saved_card_id left join table_customer_address as ca on ca.customer_address_id = tbr.customer_address_id left join table_buildings as bb on bb.building_id = ca.building_id left join table_area as aa on aa.area_id = bb.area_id" . $condition;


            //print_r($Query1);die;
            $result1 = $this->database->query($Query1);

            if ($result1->num_rows > 0) {


                $category = array();
                $row = mysqli_fetch_assoc($result1);

                $data['payment_mode'] = $row['payment_mode'];
                $data['amount'] = $row['amount'];
                $data['date'] = date('Y-m-d', strtotime($row['created_date']));
                $data['transaction_id'] = $row['transaction_id'];
                $data['booking_id'] = $row['booking_id'];
                $data['customer_address'] = $row['customer_address'].', '.$row['building_name'].', '.$row['area_name'];
                //$data['unit_type']=$row['unit_name'];
                if (!empty($row['unit_name'])) {
                    $data['unit_type'] = $row['unit_name'];
                } else {
                    $data['unit_type'] = '';
                }
                if (!empty($row['unit_image2'])) {
                    $data['unit_image'] = self::HOST . "/images/unitType/" . $row['unit_image2'];
                } else {
                    $data['unit_image'] = '';
                }
                $condition2 = " Where tbrtm.booking_id=" . $row['booking_id'];

                $Query2 = "SELECT tbrtm.*,tct.category_name as room_category from table_booking_room_type_mapping as tbrtm left join table_category as tct on tct.category_id=tbrtm.room_category_id" . $condition2;

                $result2 = $this->database->query($Query2);
                if ($result2->num_rows > 0) {
                    $i = 0;
                    while ($row2 = mysqli_fetch_assoc($result2)) {
                        $category[$i]['counter'] = $row2['counter'];
                        $category[$i]['category'] = $row2['room_category'];
                        $i++;
                    }
                }
                if ($row['payment_mode'] != "cash") {
                    $card['card_number'] = $row['card_number'];
                    $card['card_holder_name'] = $row['card_holder_name'];
                    $card['expiry_month'] = $row['expiry_month'];
                    $card['expiry_year'] = $row['expiry_year'];
                } else {
                    $card['card_number'] = '';
                    $card['card_holder_name'] = '';
                    $card['expiry_month'] = '';
                    $card['expiry_year'] = '';
                }

                $data['category'] = $category;
                $data['category'] = $category;
                $data['card'] = $card;



                $success = array("status" => "1", "msg" => "All details.", "code" => "200", "data" => $data);

                file_put_contents("Log/getPaymentDetail.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/getPaymentDetail.log", print_r($page, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/getPaymentDetail.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/getPaymentDetail.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/getAddress.log", print_r($success, true) . "\r\n", FILE_APPEND);

                $this->response($this->json($success), 200);
            } else {

                $error = array('status' => "0", "msg" => "Data does not exits!.", "code" => "209");

                file_put_contents("Log/getPaymentDetail.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/getPaymentDetail.log", print_r($page, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/getPaymentDetail.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/getPaymentDetail.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/getAddress.log", print_r($error, true) . "\r\n", FILE_APPEND);

                $this->response($this->json($error), 209);
            }
        } else {

            $error = array('status' => "0", "msg" => "Request Blank!.", "code" => "201");

            file_put_contents("Log/getPaymentDetail.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/getPaymentDetail.log", print_r($page, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/getPaymentDetail.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/getPaymentDetail.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/getPaymentDetail.log", print_r($error, true) . "\r\n", FILE_APPEND);

            $this->response($this->json($error), 201);
        }
    }

    /*     * *****************************End Payment Detail************************** */

    /*     * **************************************Update Remainder of service Ayush kumar on 29/dec/2017************* */

    public function updateRemainder() {
        $datetime = date('Y-m-d H:i:s');
        $page = "updateRemainder.";
        if ($this->get_request_method() != 'POST') {

            $error = array("status" => "0", "msg" => "Invalid request method.", "code" => "203");
            file_put_contents("Log/updateRemainder.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/updateRemainder.log", print_r($page, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/updateRemainder.log", print_r($error, true) . "\r\n", FILE_APPEND);
            $this->response($this->json($error), 203);
        }
        $inputData = json_decode(file_get_contents("php://input"), true);
        if (isset($inputData['data']) && !empty($inputData['data']) && isset($inputData['data']['bookingId']) && !empty($inputData['data']['bookingId']) && isset($inputData['data']['bookingDetailId']) && !empty($inputData['data']['bookingDetailId']) && isset($inputData['data']['remainderSet']) && isset($inputData['data']['customerId']) && !empty($inputData['data']['customerId']) && isset($inputData['data']['sessionId']) && !empty($inputData['data']['sessionId'])) {

            $customerId = trim($inputData['data']['customerId']);
            $sessionId = trim($inputData['data']['sessionId']);

            $response = $this->authenticateUser($customerId, $sessionId);

            if (!$response) {

                $error = array('status' => "0", "msg" => "You are already login in another device. Please logout!", "code" => "203");

                file_put_contents("Log/updateRemainder.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/updateRemainder.log", print_r($page, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/updateRemainder.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/updateRemainder.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/updateRemainder.log", print_r($error, true) . "\r\n", FILE_APPEND);

                $this->response($this->json($error), 203);
            }
            $where = " Where tbd.booking_id=" . $inputData['data']['bookingId'] . " AND tbd.booking_detail_id=" . $inputData['data']['bookingDetailId'];

            $checkQuery = "Select tbr.booking_id from table_booking_register as tbr left join table_booking_details as tbd on tbd.booking_id=tbr.booking_id" . $where;
            //print_r($checkQuery);die;
            $checkResult = $this->database->query($checkQuery);

            if ($checkResult->num_rows <= 0) {

                $error = array('status' => "0", "msg" => "Invalid Booking!", "code" => "203");

                file_put_contents("Log/updateRemainder.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/updateRemainder.log", print_r($page, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/updateRemainder.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/updateRemainder.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/updateRemainder.log", print_r($error, true) . "\r\n", FILE_APPEND);

                $this->response($this->json($error), 203);
            }

            $updateBookingDetail = "UPDATE table_booking_details SET remainder_set = " . $inputData['data']['remainderSet'] . " WHERE booking_detail_id='" . $inputData['data']['bookingDetailId'] . "'";
//print_r($updateBookingDetail);die;
            $result = $this->database->query($updateBookingDetail);


            $success = array("status" => "1", "msg" => "Remainder has been updated successfully!.", "code" => "200");

            file_put_contents("Log/updateRemainder.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/updateRemainder.log", print_r($page, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/updateRemainder.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/updateRemainder.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/saveCustomAddress.log", print_r($success, true) . "\r\n", FILE_APPEND);

            $this->response($this->json($success), 200);
        } else {

            $error = array('status' => "0", "msg" => "Request Blank!.", "code" => "201");

            file_put_contents("Log/updateRemainder.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/updateRemainder.log", print_r($page, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/updateRemainder.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/updateRemainder.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/updateRemainder.log", print_r($error, true) . "\r\n", FILE_APPEND);

            $this->response($this->json($error), 201);
        }
    }

    /*     * *****************************End Update Remainder*************************** */

    public function saveBuildings() {
        $datetime = date('Y-m-d H:i:s');
        $page = "Save Buildings.";

        if ($this->get_request_method() != 'POST') {

            $error = array("status" => "0", "msg" => "Invalid request method.", "code" => "203");

            file_put_contents("Log/saveBuildings.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/saveBuildings.log", print_r($page, true) . "\r\n", FILE_APPEND);

            file_put_contents("Log/saveBuildings.log", print_r($error, true) . "\r\n", FILE_APPEND);
            //echo "ddd";
            $this->response($this->json($error), 203);
        }

        $inputData = json_decode(file_get_contents("php://input"), true);

        if (isset($inputData['data']) && !empty($inputData['data']) && isset($inputData['data']['building_name']) && isset($inputData['data']['unit_type_id']) && isset($inputData['data']['customerId']) && !empty($inputData['data']['customerId']) && isset($inputData['data']['sessionId']) && !empty($inputData['data']['sessionId'])) {

            $customerId = trim($inputData['data']['customerId']);
            $sessionId = trim($inputData['data']['sessionId']);

            $response = $this->authenticateUser($customerId, $sessionId);

            if (!$response) {

                $error = array('status' => "0", "msg" => "You are already login in another device. Please logout!", "code" => "203");

                file_put_contents("Log/updateRemainder.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/updateRemainder.log", print_r($page, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/updateRemainder.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/updateRemainder.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/updateRemainder.log", print_r($error, true) . "\r\n", FILE_APPEND);

                $this->response($this->json($error), 203);
            }

            $account_id = 1;
            $building = $inputData['data']['building_name'];
            $unitType = $inputData['data']['unit_type_id'];
            $lat = $inputData['data']['lat'];
            $lng = $inputData['data']['lng'];
            $addressSql = "INSERT INTO table_buildings(account_id,building_name,unit_type_id,is_approved,lat,lng,customer_id) VALUES ('" . $account_id . "', '" . $building . "','" . $unitType . "', '0', '" . $lat . "', '" . $lng . "','" . $customerId . "')";
            $result = $this->database->query($addressSql);

            /* find detail of customer* */
            $custCond = " Where tc.customer_id=" . $customerId;

            $cusQuery = "SELECT tc.* from table_customer as tc " . $custCond;
            //print_r($cusQuery);die;
            $queryRes = $this->database->query($cusQuery);

            $data = mysqli_fetch_assoc($queryRes);
            //print_r($data);die;
            $customerEmail = $data['customer_email'];
            $customerName = $data['customer_name'];
            $customermobileNo = $data['customer_number'];


            /*             * **End customer detail* */



            $success = array("status" => "1", "msg" => "Thank you for submitting your request to add a building. Our customer support team will contact you soon.", "code" => "200");

            $message = '<html><body><table style="font-family: arial"; align="center" width="700" cellpadding="0" cellspacing="0">'.
            '<tr><td colspan="2" style="border-bottom: 1px solid #fff"><img src="http://app.whitespot-cleaning.com/images/email/header.jpg"></td></tr>'.
            '<tr><td bgcolor="#051922" align="center" style="color: #fff; padding: 10px; font-weight: bold" colspan="2">NEW BUILDING REQUEST</td></tr>'.
	    '<tr><td height="15px"></td></tr>'.
            '<tr><td style="padding: 0 15px">Guest Name</td><td style="padding: 0 15px">'.$customerName.'</td></tr>'.
            '<tr><td height="15px"></td></tr>'.
            '<tr><td style="padding: 0 15px">Phone number</td><td style="padding: 0 15px">'.$customermobileNo.'</td></tr>'.
            '<tr><td height="15px"></td></tr>'.
            '<tr><td style="padding: 0 15px">Email</td><td style="padding: 0 15px">'.$customerEmail.'</td></tr>'.
            '<tr><td height="15px"></td></tr>'.
	    '<tr><td style="padding: 0 15px">Building Name</td><td style="padding: 0 15px">'.$inputData['data']['building_name'].'</td></tr>'.
	    '<tr><td height="15px"></td></tr>'.
            '<tr><td style="padding: 0 15px">Date of request</td><td style="padding: 0 15px">'.date('Y-m-d').'</td></tr>'.
            '<tr><td height="15px"></td></tr>'.
            '<tr><td colspan="2" bgcolor="#e7e6e6"; style="padding: 15px 15px; font-size: 14px; line-height: 20px">'.
		'Do you still have any enquiry? Do not hesitate to call us at <a href="tel:+97145539799" style="color: #000; text-decoration: none">+971 4 553 9799</a> '.
                'or reach us at <a href="mailto:info@whitespot-cleaning.com" style="color: #000; text-decoration: none">info@whitespot-cleaning.com</a>'.
            '</td></tr>'.
            '<tr><td bgcolor="#051922" colspan="2">'.
            '<div style="font-size: 14px; color: #fff; line-height: 28px; padding: 5px 18px; width:345px;display: inline-block; vertical-align: middle; box-sizing: border-box">'.
		'Whitespot Cleaning Services L.L.C<br>'.
		'P.O. Box 4080, Dubai,UAE<br>'.
		'<a href="Tel:+97145539799" style="color: #fff; text-decoration: none">Tel: +97145539799</a><br>'.
		'<a href="info@whitespot-cleaning.com" style="color: #fff;text-decoration: none">info@whitespot-cleaning.com</a>'.
            '</div>'.
            '<div style="padding: 0 10px  0 15px; width:350px;display: inline-block; vertical-align: middle; box-sizing: border-box; text-align: right">'.
            '<span style="display: inline-block; vertical-align: bottom"><a href=""><img src="http://app.whitespot-cleaning.com/images/email/ig-icon.jpg"></a><a href=""><img src="images/email/fb-icon.jpg"></a></span>'.
            '<div style="display: inline-block; vertical-align: bottom; margin-left: 25px">'.
                '<span style="margin-bottom: 15px; display: block">'.
                '<a href=""><img src="http://app.whitespot-cleaning.com/images/email/icon-2.png"></a>'.
                '</span>'.
                '<span><a href=""><img src="http://app.whitespot-cleaning.com/images/email/icon-1.png"></a></span>'.
		'</div></div></td></tr></table></body></html>';


            $subject = "New Building Request.";
            //$customer_email="ayush.kuamr@quytech.com";
            $toArray = array();
            $toArray[0] = "info@whitespot-cleaning.com";
            $toArray[1] = "hisham@whitespot-cleaning.com";
            $toArray[2] = "Saritha.Puli@damacgroup.com";
            $toArray[3]="parul@mailinator.com";


            /* $toArray[0]="ayush.kumar@quytech.com";
              $toArray[1]="ayush.kumar@quytech.com";
              $toArray[2]="ayush.kumar@quytech.com";
              $toArray[3]="ayush.kumar@quytech.com";
              $toArray[4]="ayush.kumar@quytech.com"; */

            for ($i = 0; $i < count($toArray); $i++) {
                $res = $this->sendEmail($toArray[$i], $message, $subject);
            }

            $mobileMsg = "New request for adding building by Name:" . $customerName . ", Email:" . $customerEmail . " ,Mobile No:" . $customermobileNo;
            //$smsRes=$this->sendSms('0569635534',$mobileMsg);
            // $smsRes=$this->sendSms('0525194828',$mobileMsg);
            // $smsRes=$this->sendSms('0555058406',$mobileMsg);

            file_put_contents("Log/saveBuildings.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/saveBuildings.log", print_r($page, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/saveBuildings.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/saveBuildings.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/saveBuildings.log", print_r($success, true) . "\r\n", FILE_APPEND);

            $this->response($this->json($success), 200);
        } else {

            $error = array('status' => "0", "msg" => "Request Blank!.", "code" => "201");

            file_put_contents("Log/saveBuildings.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/saveBuildings.log", print_r($page, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/saveBuildings.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/saveBuildings.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/saveBuildings.log", print_r($error, true) . "\r\n", FILE_APPEND);

            $this->response($this->json($error), 201);
        }
    }

    private function getAvailableDaySlots() {
        $datetime = date('Y-m-d H:i:s');
        $page = "Available Day Slots";

        if ($this->get_request_method() != 'POST') {

            $error = array("status" => "0", "msg" => "Invalid request method.", "code" => "203");

            file_put_contents("Log/unitCleaning.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/unitCleaning.log", print_r($page, true) . "\r\n", FILE_APPEND);

            file_put_contents("Log/unitCleaning.log", print_r($error, true) . "\r\n", FILE_APPEND);
            //echo "ddd";
            $this->response($this->json($error), 203);
        }

        $inputData = json_decode(file_get_contents("php://input"), true);
        if (!isset($inputData['data']['building_id']) || empty($inputData['data']['building_id']) || !isset($inputData['data']['service_type']) || empty($inputData['data']['service_type'])) {
            $error = array("status" => "0", "msg" => "Request Blank.", "code" => "201");

            file_put_contents("Log/unitCleaning.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/unitCleaning.log", print_r($page, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/unitCleaning.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/unitCleaning.log", print_r($error, true) . "\r\n", FILE_APPEND);
            //echo "ddd";
            $this->response($this->json($error), 201);
        }
        
        if (($inputData['data']['service_type'] == 'o') && (!isset($inputData['data']['date']) || empty($inputData['data']['date']))) {
            $error = array("status" => "0", "msg" => "Request Blank.", "code" => "201");

            file_put_contents("Log/unitCleaning.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/unitCleaning.log", print_r($page, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/unitCleaning.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/unitCleaning.log", print_r($error, true) . "\r\n", FILE_APPEND);
            //echo "ddd";
            $this->response($this->json($error), 201);
        }
        
        $condition = " where status='A' order by tcs.slot_start_time";

        $checkSlotQuery = "SELECT tcs.slot_id,tcs.slot_start_time,tcs.slot_end_time from table_calender_slots as tcs" . $condition;
        //print_r($cleaningTypeSql);die;
        $result = $this->database->query($checkSlotQuery);

        if ($result->num_rows > 0) {
            while ($row = mysqli_fetch_assoc($result)) {
                $slotesList[] = $row;
            }
            $building_id = $inputData['data']['building_id'];
            $condition = " where c.status='A' and s.building_id = '".$building_id."'";
            $cleanersListsQuery = 'select c.cleaner_id,c.cleaner_name from table_cleaner as c left join table_supervisor as s on s.supervisor_id=c.supervisor_id '.$condition;
            $cleanersListsTemp = $this->database->query($cleanersListsQuery);
            if ($cleanersListsTemp->num_rows > 0) {
                
                if($inputData['data']['service_type'] == 'l') {
                    foreach ($slotesList as $tempSlots) {
                        $tempSlots['slot_start_time'] = date("H:i A",  strtotime($tempSlots['slot_start_time'] ));
                        $tempSlots['slot_end_time'] = date("H:i A",  strtotime($tempSlots['slot_end_time'] ));
                        $tempSlots['status'] = 1;
                        $availableSlots[$tempSlots['slot_id']] = $tempSlots;
                    }
                    $availableSlots = array_values($availableSlots);
                    $success = array("status" => "1", "msg" => "All Cleaning!.", "code" => "200", "data" => $availableSlots);
                    file_put_contents("Log/availableSlots.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
                    file_put_contents("Log/availableSlots.log", print_r($page, true) . "\r\n", FILE_APPEND);
                    file_put_contents("Log/availableSlots.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
                    file_put_contents("Log/availableSlots.log", print_r($success, true) . "\r\n", FILE_APPEND);
                    $this->response($this->json($success), 200);
                } else if($inputData['data']['service_type'] == 'o') {
                
                    while ($row = mysqli_fetch_assoc($cleanersListsTemp)) {
                        $cleanersLists[] = $row;
                    }
                    foreach ($cleanersLists as $temp) {
                        $cleaner_leave_details = array();
                        $cleaner_id = $temp['cleaner_id'];
                        $query = 'select cleaner_name,duty_start_time,duty_end_time from table_cleaner WHERE cleaner_id="'.$cleaner_id.'"';

                        $queryRes = $this->database->query($query);
                        $cleaner_details = mysqli_fetch_assoc($queryRes);
                        $duty_start_time = $cleaner_details['duty_start_time'];
                        $duty_end_time = $cleaner_details['duty_end_time'];
                        $cleaning_date = $inputData['data']['date'];

                        $slotes = '';
                        $query = 'select DATE(leave_from_date) as FromDate,'
                                . ' DATE(leave_to_date) as ToDate,leave_type_name from table_cleaner_leaves '
                                . 'as A LEFT JOIN table_leave_type as B ON A.leave_type = B.leave_type_id '
                                . 'WHERE cleaner_id="'.$cleaner_id.'"';
                        $queryRes = $this->database->query($query);
                        while ($row = mysqli_fetch_assoc($queryRes)) {
                            $cleaner_leave_details[] = $row;
                        }
                        foreach ($cleaner_leave_details as $cleaner_leave_detail) {
                            $leaves = array($cleaner_leave_detail['FromDate']);
                            while (end($leaves) < $cleaner_leave_detail['ToDate']) {
                                $leaves[] = date('Y-m-d', strtotime(end($leaves) . ' +1 day'));
                            }
                            if (in_array($cleaning_date, $leaves)) {
                                continue 2;
                            }
                        }

                        $time_booked = array();
                        $query = 'select cleaning_time_start,cleaning_time_end from '
                                . '`table_booking_allotment` as A LEFT JOIN '
                                . '`table_booking_details` as B ON A.booking_detail_id = B.booking_detail_id '
                                . 'WHERE A.cleaner_id = "'.$cleaner_id.'" AND B.cleaning_date = "'.$cleaning_date.'"';
                        $queryRes = $this->database->query($query);
                        while ($row = mysqli_fetch_assoc($queryRes)) {
                            $time_booked[] = $row;
                        }

                        $booked_times = array();
                        $rer = '';
                        /* Creating time slotes based on the duty time of the selected cleaner */
                        $starttime = date('H:i',strtotime('2016-10-30 '.$duty_start_time));
                        $endtime = date('H:i',strtotime('2016-10-30 '.$duty_end_time));
                        $duration = '120';  // split by 30 mins 
                        $array_of_time = array();
                        $start_time = strtotime($starttime);
                        $end_time = strtotime($endtime);
                        $add_mins = $duration * 60;
                        while ($start_time <= $end_time) { // loop between time
                            $array_of_time[] = date("H:i", $start_time);
                            $start_time += $add_mins; // to check endtie=me  
                        }

                        $hidden = '';
                        $slot_not_free = 0;
                        foreach ($time_booked as $time_book) {
                            if ($time_book['cleaning_time_start'] == NULL || $time_book['cleaning_time_start'] == null)
                                continue;
                            $booked_slotes[] = (date("H:i", strtotime($time_book['cleaning_time_start'])) . '-' . date("H:i", strtotime($time_book['cleaning_time_end'])));
                        }
                        for ($i = 0; $i < count($array_of_time) - 1; $i++) {
                            $css_class = 0;
                            $style = 'green';
                            $title = '';
                            $slot_title = $array_of_time[$i] . '-' . $array_of_time[$i + 1];
                            $slot_title_disply = '';
                            foreach ($booked_slotes as $booked_slot) {

                                $booked_slot_parts = explode('-', $booked_slot);
                                $booked_start = new DateTime('2016-10-30 ' . $booked_slot_parts[0]);
                                $booked_end = new DateTime('2016-10-30 ' . $booked_slot_parts[1]);

                                $slot_start = new DateTime('2016-10-30 ' . $array_of_time[$i]);
                                $slot_end = new DateTime('2016-10-30 ' . $array_of_time[$i + 1]);
                                if ($booked_start > $slot_start)
                                    continue;
                                if (($booked_start >= $slot_start && $slot_end >= $booked_end) || ($booked_start >= $slot_start && $booked_end >= $slot_end)) {

                                    $css_class = 1;
                                    $style = 'red';
                                    $title = 'title="' . $booked_slot . '"';
                                }
                            }

                            $slotes .='<div style="background-color:' . $style . '"' . $title . ' class="' . $css_class . '">' . $slot_title . '</div>';

                            $slotesArray[] = array(
                                'slot' =>   $slot_title,
                                'start' => $array_of_time[$i],
                                'end' => $array_of_time[$i+1],
                                'cleaner_id' =>$temp['cleaner_id'],
                                'color' => $css_class
                            );
                        }

                    }
                    $availableSlots = array();
                    foreach ($slotesList as $tempSlots) {
                        $tempSlots['status'] = 0;
                        foreach ($slotesArray as $temp) {
                            if(date('H:i:s',  strtotime($tempSlots['slot_start_time'])) == date('H:i:s',  strtotime($temp['start'])) 
                                && date('H:i:s',  strtotime($tempSlots['slot_end_time'])) == date('H:i:s',  strtotime($temp['end']))
                                && $temp['color'] == 0){
                                $tempSlots['status'] = 1;
                                $availableSlots[$tempSlots['slot_id']] = $tempSlots;
                            } else {
                                $availableSlots[$tempSlots['slot_id']] = $tempSlots;
                            }
                        }
                    }
                    $availableSlots = array_values($availableSlots);
                    $availableSlotsNew = array();
                    foreach($availableSlots as $temp) {
                        $temp['slot_start_time'] = date("H:i A",  strtotime($temp['slot_start_time'] ));
                        $temp['slot_end_time'] = date("H:i A",  strtotime($temp['slot_end_time'] ));
                        $availableSlotsNew[] = $temp;
                    }
                    $success = array("status" => "1", "msg" => "All Cleaning!.", "code" => "200", "data" => $availableSlotsNew);
                    file_put_contents("Log/availableSlots.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
                    file_put_contents("Log/availableSlots.log", print_r($page, true) . "\r\n", FILE_APPEND);
                    file_put_contents("Log/availableSlots.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
                    file_put_contents("Log/availableSlots.log", print_r($success, true) . "\r\n", FILE_APPEND);
                    $this->response($this->json($success), 200);
                }
            } else {
                $error = array('status' => "0", "msg" => "Data not Found!.", "code" => "209");

                file_put_contents("Log/availableSlots.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/availableSlots.log", print_r($page, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/availableSlots.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/availableSlots.log", print_r($error, true) . "\r\n", FILE_APPEND);

                $this->response($this->json($error), 209);
            }
        } else {
            // If Username is invalid
            $error = array('status' => "0", "msg" => "Data not Found!.", "code" => "209");

            file_put_contents("Log/availableSlots.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/availableSlots.log", print_r($page, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/availableSlots.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/availableSlots.log", print_r($error, true) . "\r\n", FILE_APPEND);

            $this->response($this->json($error), 209);
        }
    }
    
    private function unitCleaning() {
        $datetime = date('Y-m-d H:i:s');
        $page = "Unit Type cleaning";

        if ($this->get_request_method() != 'POST') {

            $error = array("status" => "0", "msg" => "Invalid request method.", "code" => "203");

            file_put_contents("Log/unitCleaning.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/unitCleaning.log", print_r($page, true) . "\r\n", FILE_APPEND);

            file_put_contents("Log/unitCleaning.log", print_r($error, true) . "\r\n", FILE_APPEND);
            //echo "ddd";
            $this->response($this->json($error), 203);
        }

        $inputData = json_decode(file_get_contents("php://input"), true);
        if (!isset($inputData['data']['unitTypeId']) || empty($inputData['data']['unitTypeId'])) {
            $error = array("status" => "0", "msg" => "Request Blank.", "code" => "201");

            file_put_contents("Log/unitCleaning.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/unitCleaning.log", print_r($page, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/unitCleaning.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/unitCleaning.log", print_r($error, true) . "\r\n", FILE_APPEND);
            //echo "ddd";
            $this->response($this->json($error), 201);
        }

        $condtion = " Where cum.unit_type_id=" . $inputData['data']['unitTypeId'] . " AND tct.status='A'";
        $cleaningTypeSql = "SELECT u.unit_name,u.unit_image,u.unit_image2,u.name,tct.cleaning_type_id,tct.cleaning_type,tct.is_long_term FROM table_cleaning_type  as tct LEFT JOIN table_cleaning_unit_mapping as cum on cum.cleaning_type_id=tct.cleaning_type_id left join table_unit_type as u on u.unit_type_id = cum.unit_type_id" . $condtion;
        //print_r($cleaningTypeSql);die;
        $result = $this->database->query($cleaningTypeSql);

        if ($result->num_rows > 0) {
            $i = 0;
            while ($row = mysqli_fetch_assoc($result)) {

                $unitType = array(
                    'name' => $row['name'],
                    'unitType' => $row['unit_name'],
                    'unit_image' => self::HOST . "/images/unitType/" . $row['unit_image'],
                    'unit_image2' => self::HOST . "/images/unitType/" . $row['unit_image2'],
                );
                $data['unitType'] = $unitType;
                unset($row['name']);
                unset($row['unit_name']);
                unset($row['unit_image']);
                unset($row['unit_image2']);
                $data['cleanType'][$i] = $row;
                $i++;
            }

            $success = array("status" => "1", "msg" => "All Cleaning!.", "code" => "200", "data" => $data);
            //echo "hiii";exit;
            file_put_contents("Log/unitCleaning.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/unitCleaning.log", print_r($page, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/unitCleaning.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/unitCleaning.log", print_r($success, true) . "\r\n", FILE_APPEND);
            $this->response($this->json($success), 200);
        } else {
            // If Username is invalid
            $error = array('status' => "0", "msg" => "Data not Found!.", "code" => "209");

            file_put_contents("Log/unitCleaning.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/unitCleaning.log", print_r($page, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/unitCleaning.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/unitCleaning.log", print_r($error, true) . "\r\n", FILE_APPEND);

            $this->response($this->json($error), 209);
        }
    }

    private function resendOTP() {

        $datetime = date('Y-m-d H:i:s');
        $page = "Resend OTP";
        $inputData = json_decode(file_get_contents("php://input"), true);
        //print_r($inputData);exit;
        if (isset($inputData['data']['customerNumber']) && !empty($inputData['data']['customerNumber']) && !empty($inputData['data']['country_code'])) {


            $userProfileSql = " SELECT tc.customer_id,tc.otp, tc.customer_number FROM table_customer AS tc  WHERE  tc.customer_number = '" . $inputData['data']['customerNumber'] . "' and tc.country_code = '".$inputData['data']['country_code']."'";

            $result = $this->database->query($userProfileSql) or die($this->database->error . __LINE__);

            if ($result->num_rows > 0) {
                $getDetails = array();

                $cusdata = mysqli_fetch_assoc($result);
                //print_r($cusdata);die;
                if (isset($cusdata['otp']) && !empty($cusdata)) {
                    // $smsRes = $this->sendSms(TEXTLOCAL_API_KEY,$cusdata['customer_number'], $cusdata['otp']);
                    $data = array(
                        'otp' => $cusdata['otp']
                    );
                    $success = array('status' => "1", "msg" => "Please go to next step.", "code" => "200", "data" => $data);


                    file_put_contents("Log/customerProfile.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
                    file_put_contents("Log/customerProfile.log", print_r($page, true) . "\r\n", FILE_APPEND);
                    file_put_contents("Log/customerProfile.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
                    file_put_contents("Log/customerProfile.log", print_r($success, true) . "\r\n", FILE_APPEND);
                } else {
                    $error = array('status' => "0", "msg" => "No Data Exists!", "code" => "209");

                    file_put_contents("Log/customerProfile.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
                    file_put_contents("Log/customerProfile.log", print_r($page, true) . "\r\n", FILE_APPEND);
                    file_put_contents("Log/customerProfile.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
                    file_put_contents("Log/customerProfile.log", print_r($error, true) . "\r\n", FILE_APPEND);
                    $this->response($this->json($error), 209);
                }

                $this->response($this->json($success), 200);
            } else {

                $error = array('status' => "0", "msg" => "No Data Exists!", "code" => "209");

                file_put_contents("Log/customerProfile.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/customerProfile.log", print_r($page, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/customerProfile.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/customerProfile.log", print_r($error, true) . "\r\n", FILE_APPEND);
                $this->response($this->json($error), 209);
            }
        } else {

            $error = array('status' => "0", "msg" => "Request Blank!", "code" => "201");

            file_put_contents("Log/customerProfile.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/customerProfile.log", print_r($page, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/customerProfile.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/customerProfile.log", print_r($error, true) . "\r\n", FILE_APPEND);

            $this->response($this->json($error), 201); // If the request is blank.
        }
    }

    public function updateTimeSlot() {
        $datetime = date('Y-m-d H:i:s');
        $page = "Update Time Slot.";

        if ($this->get_request_method() != 'POST') {

            $error = array("status" => "0", "msg" => "Invalid request method.", "code" => "203");

            file_put_contents("Log/saveBuildings.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/saveBuildings.log", print_r($page, true) . "\r\n", FILE_APPEND);

            file_put_contents("Log/saveBuildings.log", print_r($error, true) . "\r\n", FILE_APPEND);
            //echo "ddd";
            $this->response($this->json($error), 203);
        }

        $inputData = json_decode(file_get_contents("php://input"), true);

        if (isset($inputData['data']) && !empty($inputData['data']) && isset($inputData['data']['services']) && !empty($inputData['data']['services']) && isset($inputData['data']['customerId']) && !empty($inputData['data']['customerId']) && isset($inputData['data']['sessionId']) && !empty($inputData['data']['sessionId'])) {

            $customerId = trim($inputData['data']['customerId']);
            $sessionId = trim($inputData['data']['sessionId']);

            $response = $this->authenticateUser($customerId, $sessionId);

            if (!$response) {

                $error = array('status' => "0", "msg" => "You are already login in another device. Please logout!", "code" => "203");

                file_put_contents("Log/updateRemainder.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/updateRemainder.log", print_r($page, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/updateRemainder.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/updateRemainder.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
                file_put_contents("Log/updateRemainder.log", print_r($error, true) . "\r\n", FILE_APPEND);

                $this->response($this->json($error), 203);
            }

            $sevices = $inputData['data']['services'];

            $service_id_response = 0;
            foreach ($sevices as $temp) {
                $cleaningDate = $temp['cleaning_date'];
                $slot_id = $temp['slot_id'];
                $service_id = $temp['service_id'];
                $service_id_response = $temp['service_id'];
                $updateBookingDetail = "UPDATE table_booking_allotment_temp SET cleaning_date = '" . $cleaningDate . "',is_available = '1', slot_id = '" . $slot_id . "' WHERE id='" . $service_id . "'";
                $result = $this->database->query($updateBookingDetail);
            }
            //print_r($service_id_response);die;
            $slotCon = " Where  id='" . $service_id_response . "'";
            $slotQuery = "SELECT * from table_booking_allotment_temp " . $slotCon;
            $sotResult = $this->database->query($slotQuery);
            $row = mysqli_fetch_assoc($sotResult);
            $data = array();
            if($row['parent_id'] == 0) {
                $slotCon = " Where  parent_id='" . $service_id_response . "' or id = '".$service_id_response."'";
                $slotQuery = "SELECT table_booking_allotment_temp.*,table_calender_slots.slot_start_time,table_calender_slots.slot_end_time from table_booking_allotment_temp left join table_calender_slots on table_calender_slots.slot_id=table_booking_allotment_temp.slot_id" . $slotCon;
                $sotResult = $this->database->query($slotQuery);
                while ($row = mysqli_fetch_assoc($sotResult)) {
                    $data[] = $row;
                }
            } else {
                $slotCon = " Where  parent_id='" . $row['parent_id'] . "' or id = '".$row['parent_id']."'";
                $slotQuery = "SELECT table_booking_allotment_temp.*,table_calender_slots.slot_start_time,table_calender_slots.slot_end_time from table_booking_allotment_temp left join table_calender_slots on table_calender_slots.slot_id=table_booking_allotment_temp.slot_id" . $slotCon;
                $sotResult = $this->database->query($slotQuery);
                while ($row = mysqli_fetch_assoc($sotResult)) {
                    $data[] = $row;
                }
            }
            $services = array();
            foreach ($data as $dataTemp) {
                $temp = array();
                $temp['cleaning_date'] = $dataTemp['cleaning_date'];
                $temp['slot_start_time'] = $dataTemp['slot_start_time'];
                $temp['slot_end_time'] = $dataTemp['slot_end_time'];
                $temp['slot_id'] = $dataTemp['slot_id'];
                $temp['service_id'] = $dataTemp['id'];
                $temp['is_available'] = $dataTemp['is_available'];
                $temp['services']  = array();
                if($dataTemp['is_available'] == 0) {
                    $otherTimeSlot = $this->getOtherTimeSlot($dataTemp['buidling_id'], $dataTemp['cleaning_date']);
                    if(isset($otherTimeSlot) && !empty($otherTimeSlot)) {
                        $otherAvailableTimeSlots =  $otherTimeSlot;
                    } else {
                        //yesteday
                        $otherTimeSlot = $this->getOtherTimeSlot($building_id, date('Y-m-d',strtotime('-1 days',strtotime($temp['cleaning_date']))));
                        $otherAvailableTimeSlots = $otherTimeSlot;
                        //tomorrow 
                        $otherTimeSlot = $this->getOtherTimeSlot($building_id, date('Y-m-d',strtotime('+1 days',strtotime($temp['cleaning_date']))));
                        $otherAvailableTimeSlots = array_merge($otherAvailableTimeSlots,$otherTimeSlot);
                    }
                    $temp['services'] = $otherAvailableTimeSlots;
                }
                $services[] = $temp;
            }
            $dataRetun['services'] = $services;

            $success = array("status" => "1", "msg" => "Sevice updated", "code" => "200", "data" => $dataRetun);


            file_put_contents("Log/saveBuildings.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/saveBuildings.log", print_r($page, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/saveBuildings.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/saveBuildings.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/saveBuildings.log", print_r($success, true) . "\r\n", FILE_APPEND);

            $this->response($this->json($success), 200);
        } else {

            $error = array('status' => "0", "msg" => "Request Blank!.", "code" => "201");

            file_put_contents("Log/saveBuildings.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/saveBuildings.log", print_r($page, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/saveBuildings.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/saveBuildings.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/saveBuildings.log", print_r($error, true) . "\r\n", FILE_APPEND);

            $this->response($this->json($error), 201);
        }
    }

private function verifySignupOTP(){


        $datetime = date('Y-m-d H:i:s');
        $page = "Customer Reset Password";
        $inputData = json_decode(file_get_contents("php://input"), true);
        
        if (isset($inputData['data']) && !empty($inputData['data']) && isset($inputData['data']['customerId']) && !empty($inputData['data']['customerId']) && isset($inputData['data']['sessionId']) && !empty($inputData['data']['sessionId']) && isset($inputData['data']['otp']) && !empty($inputData['data']['otp'])) 
        {

            $customerId = trim($inputData['data']['customerId']);
            $sessionId = trim($inputData['data']['sessionId']);
            $otp = $inputData['data']['otp'];
            $response = $this->authenticateUser($customerId, $sessionId);


            if ($response) {


                $now = date('Y-m-d H:i:s');

                $checkOtp = "SELECT tc.* FROM table_customer as tc WHERE tc.customer_id = '" . $customerId . "' AND tc.otp='" . $otp . "' AND TIMESTAMPDIFF(MINUTE,tc.last_update_date,'".$now."') < 5";

                $otpQuery = $this->database->query($checkOtp);

                if ($otpQuery->num_rows <= 0) {

                    $error = array('status' => "0", "msg" => "Your Otp has been exipred or invalid.", 'code' => "203");
                    file_put_contents("Log/customerForgotPassword.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
                    file_put_contents("Log/customerForgotPassword.log", print_r($page, true) . "\r\n", FILE_APPEND);
                    file_put_contents("Log/customerForgotPassword.log", print_r($inputData, true) . "\r\n", FILE_APPEND);

                    file_put_contents("Log/customerForgotPassword.log", print_r($error, true) . "\r\n", FILE_APPEND);

                    $this->response($this->json($error), 203);

                } else {


                    $statusQuery = " UPDATE table_customer SET status = 'A'  where customer_id = '".$customerId."'";
                    $this->database->query($statusQuery);

                     $statusQuery2 = " UPDATE table_web_users SET status = 'A'  where customer_id = '".$customerId."'";
                      $this->database->query($statusQuery2);


                    $userProfileSql = " SELECT tc.* FROM table_customer AS tc LEFT JOIN table_web_users AS twu ON twu.customer_id = tc.customer_id WHERE tc.status = 'A' AND tc.customer_id = '" . $customerId . "'";

                    $sotResult = $this->database->query($userProfileSql);
                    // $queryResult = mysqli_fetch_assoc($cusQuery);

                    while ($row = mysqli_fetch_assoc($sotResult)) {
                        $data = $row;
                    }

                    // if (isset($data['location']) || empty($data['location'])) {
                    //     $data['location'] = '';
                    // }
                    // if (empty($data['lat'])) {
                    //     $data['lat'] = '';
                    // }
                    // if (empty($data['lng'])) {
                    //     $data['lng'] = '';
                    // }
            //Send Mail
                   //  $message = '<html>
                   //  <body>
                   //     <p>Hello ' . $customer_name . '</p>' .
                   //     '<p></p>
                   //     <p>Your registration has been succesful.</p>
                   //     <p><b>Your UserName is</b>:.' . $username . '</p>' .
                   //     '<p><b>Your password is</b>:' . $customerData['password'] . '</p>' .
                   //     '<p>Regards</p>
                   //     <p>White-Spot</p>

                   // </body>
                   // </html>';
                   // $subject = "Registration.";



                   // $res = $this->sendEmail($customer_email, $message, $subject);
                   $this->database->query($sql1);

                   $success = array('status' => "1", "msg" => "OTP has been verified and user has been registerd successfully", "code" => "200", "data" => $data);

                   file_put_contents("Log/customerForgotPassword.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
                   file_put_contents("Log/customerForgotPassword.log", print_r($page, true) . "\r\n", FILE_APPEND);
                   file_put_contents("Log/customerForgotPassword.log", print_r($inputData, true) . "\r\n", FILE_APPEND);

                   file_put_contents("Log/customerForgotPassword.log", print_r($success, true) . "\r\n", FILE_APPEND);

                   $this->response($this->json($success), 200);

               }
         }else{

                 $error = array('status' => "0", "msg" => "No user found!", "code" => "207");

                 file_put_contents("Log/customerresetPassword.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
                 file_put_contents("Log/customerresetPassword.log", print_r($page, true) . "\r\n", FILE_APPEND);
                 file_put_contents("Log/customerresetPassword.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
                 file_put_contents("Log/customerresetPassword.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
                 file_put_contents("Log/customerresetPassword.log", print_r($error, true) . "\r\n", FILE_APPEND);

                 $this->response($this->json($error), 207);

             } 
         } else {
            $error = array("status" => "0", "msg" => "Request Blank!", "code" => "201");

            file_put_contents("Log/customerresetPassword.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/customerresetPassword.log", print_r($page, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/customerresetPassword.log", print_r($inputData, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/customerresetPassword.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/customerresetPassword.log", print_r($error, true) . "\r\n", FILE_APPEND);

            $this->response($this->json($error), 201);
        }
    

     }

    /*
     * 
     */


    private function getVatDetails(){

        $datetime = date('Y-m-d H:i:s');
        $page = "Get VAT.";

        if ($this->get_request_method() != 'GET') {

            $error = array("status" => "0", "msg" => "Invalid request method.", "code" => "203");

            file_put_contents("Log/GetVat.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/GetVat.log", print_r($page, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/GetVat.log", print_r($error, true) . "\r\n", FILE_APPEND);
            //echo "ddd";
            $this->response($this->json($error), 203);
        }

        $inputData = json_decode(file_get_contents("php://input"), true);

        $vatData = " SELECT tc.* FROM table_config AS tc  WHERE tc.status = 'A' ";

        $result = $this->database->query($vatData);

        while ($row = mysqli_fetch_assoc($result)) {
            $data['id'] = $row['config_id'];
            $data['vat'] = $row['vat'];
        }


        if(isset($data) && is_array($data) && count($data) > 0){


            $success = array("status" => "1", "msg" => "Vat data get successfully !", "code" => "200", "data" =>$data);

            file_put_contents("Log/GetVat.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/GetVat.log", print_r($page, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/GetVat.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/saveBuildings.log", print_r($success, true) . "\r\n", FILE_APPEND);

            $this->response($this->json($success), 200);
        } else {

            $error = array('status' => "0", "msg" => "Data Not Found!", "code" => "209");

            file_put_contents("Log/saveBuildings.log", print_r($datetime, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/saveBuildings.log", print_r($page, true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/saveBuildings.log", print_r(file_get_contents("php://input"), true) . "\r\n", FILE_APPEND);
            file_put_contents("Log/saveBuildings.log", print_r($error, true) . "\r\n", FILE_APPEND);

            $this->response($this->json($error), 201);
        }
    

    }

    /*
     * 	Encode array into JSON
     */

    private function json($data) {
        if (is_array($data)) {
            return json_encode($data);
        }
    }

    private function Apistatus () 
    {
        // 1 for APIs Active -- 0 for Inactive
        $status = 1;
        $success = array('status' => 1, "api_status" => $status);
        $this->response($this->json($success), 200);
    }

}

// Initiiate Library

$api = new customerServices;
$api->processApi();
?>
