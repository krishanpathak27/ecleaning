<?php
include("includes/config.inc.php");
include("includes/function.php");
$_objAdmin = new Admin();

/* $auMarker=$_objAdmin->_getSelectList('table_order as o left join table_retailer as r on o.retailer_id=r.retailer_id left join table_salesman as s on s.salesman_id=o.salesman_id',"o.lat,o.lng,r.lat as rlat,r.lng as rlng,r.retailer_name,r.retailer_id,s.salesman_name",''," o.order_id='".base64_decode($_REQUEST['ord'])."' ");
$ret_name=$auMarker[0]->retailer_name;
$ret_lat=$auMarker[0]->rlat;
$ret_lng=$auMarker[0]->rlng;
$sal_name=$auMarker[0]->salesman_name;
$sal_lat=$auMarker[0]->lat;
$sal_lng=$auMarker[0]->lng;
if($auMarker[0]->rlat==''){
$auMarker=$_objAdmin->_getSelectList('table_survey ',"lat as rlat,lng as rlng",''," retailer_id='".$auMarker[0]->retailer_id."' order by survey_date desc ");
$ret_lat=$auMarker[0]->rlat;
$ret_lng=$auMarker[0]->rlng;
} */
// retailer map only survey table


if($_REQUEST['ord']!='' && ($_REQUEST['act_type']==28) || ($_REQUEST['act_type']==29)){

	$auOrd=$_objAdmin->_getSelectList2('table_activity as a left join table_retailer as r on a.ref_id=r.retailer_id left join table_distributors as d on d.distributor_id=r.distributor_id left join table_salesman as s on s.salesman_id=a.salesman_id left join table_markets as m on m.market_id=r.market_id',"a.activity_date as date_of_order,a.start_time as time_of_order,a.lat,a.lng,a.accuracy_level,a.network_mode as location_provider,r.retailer_id,r.retailer_name,r.retailer_address,r.retailer_location,r.retailer_phone_no,d.distributor_name,s.salesman_name,m.market_name",''," a.ref_id=".base64_decode($_REQUEST['ord'])." and a.activity_date ='".date('Y-m-d', strtotime($_SESSION['SalReportDate']))."' and a.activity_type=".$_REQUEST['act_type']);


	}

	else
	{

		$auOrd=$_objAdmin->_getSelectList('table_order as o left join table_retailer as r on o.retailer_id=r.retailer_id  left join table_distributors as d on d.distributor_id=o.distributor_id left join table_salesman as s on s.salesman_id=o.salesman_id',"o.lat,o.lng,o.location_provider,o.accuracy_level,r.retailer_name,d.distributor_name, d.distributor_id, r.retailer_id,s.salesman_name",''," o.order_id='".base64_decode($_REQUEST['ord'])."' ");

	}


//$ret_name=$auOrder[0]->retailer_name;
$sal_name=$auOrd[0]->salesman_name;
$sal_lat=$auOrd[0]->lat;
$sal_lng=$auOrd[0]->lng;
$sal_accuracy_level=$auOrd[0]->accuracy_level;
$sal_network_mode=$auOrd[0]->location_provider;


//$auMarker=$_objAdmin->_getSelectList('table_survey ',"lat as rlat,lng as rlng,accuracy_level,network_mode",''," retailer_id='".$auOrder[0]->retailer_id."' order by survey_date desc,survey_time desc limit 0,1");

$uType = "";

if($_REQUEST['ord']!='' && ($_REQUEST['act_type']==28) || ($_REQUEST['act_type']==29)){

if($auOrd[0]->retailer_id == 0 || $auOrd[0]->retailer_id== "") {
	$uType = "Distributor ";
	$ret_name=$auOrd[0]->distributor_name;
	$auMarker=$_objAdmin->_getSelectList2('table_distributors'," lat as rlat,lng as rlng",''," distributor_id='".$auOrd[0]->distributor_id."' order by start_date");

} else {
	$uType = "Retailer ";
	$ret_name=$auOrd[0]->retailer_name;
	$auMarker=$_objAdmin->_getSelectList2('table_retailer ',"lat as rlat,lng as rlng",''," retailer_id='".$auOrd[0]->retailer_id."' order by start_date");
}

}
else
{

if($auOrd[0]->retailer_id == 0 || $auOrd[0]->retailer_id== "") {
	$uType = "Distributor ";
	$ret_name=$auOrd[0]->distributor_name;
	$auMarker=$_objAdmin->_getSelectList2('table_survey'," lat as rlat,lng as rlng,accuracy_level,network_mode",''," distributor_id='".$auOrd[0]->distributor_id."' order by survey_date desc,survey_time desc limit 0,1");

} else {
	$uType = "Retailer ";
	$ret_name=$auOrd[0]->retailer_name;
	$auMarker=$_objAdmin->_getSelectList2('table_survey ',"lat as rlat,lng as rlng,accuracy_level,network_mode",''," retailer_id='".$auOrd[0]->retailer_id."' order by survey_date desc,survey_time desc limit 0,1");
}

}


$ret_lat ="";
$ret_lng ="";
$accuracy_level = "";
$network_mode ="";


if($auMarker[0]->rlat>0){
$ret_lat=$auMarker[0]->rlat;
$ret_lng=$auMarker[0]->rlng;
$accuracy_level=$auMarker[0]->accuracy_level;
$network_mode=$auMarker[0]->network_mode;
}


?>
<html>
<head>
<meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=9" >
<meta http-equiv="X-UA-Compatible" content="IE=8" />
<meta http-equiv="X-UA-Compatible" content="IE=7" />
<meta http-equiv="X-UA-Compatible" content="IE=6" />

<style type="text/css">
  html { height: 100% }
  body { height: 100%; margin: 0; padding: 0 }
  #map_canvas { height: 100% }
</style>
<script type="text/javascript">
 $(document).keypress(function(e) {
    if(e.which == 13) {
       javascript:window.close();
    }
});
</script>

<script type="text/javascript"
src="http://maps.googleapis.com/maps/api/js?sensor=false">
</script>
<script type="text/javascript">
      function initialize() {
        var myOptions = 
		{
		<?php if($ret_lat!='' && $ret_lat>0){ ?>
          center: new google.maps.LatLng(<?php echo $ret_lat; ?>, <?php echo $ret_lng; ?>), zoom: 18, mapTypeId: google.maps.MapTypeId.ROADMAP
		  <?php } else { ?>
		  center: new google.maps.LatLng(<?php echo $sal_lat; ?>, <?php echo $sal_lng; ?>), zoom: 18, mapTypeId: google.maps.MapTypeId.ROADMAP
		  <?php } ?>
        };
        var map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
		<?php if($ret_lat!='' && $ret_lat>0){ ?>
		var myLatlng = new google.maps.LatLng(<?php echo $ret_lat; ?>, <?php echo $ret_lng; ?>)
		var marker = new google.maps.Marker
		({ position: myLatlng,title:"<?php echo $uType;?> Name: <?php echo $ret_name; ?>, Location Type: <?php echo $network_mode; ?>, Accuracy: <?php echo $accuracy_level; ?>", animation: google.maps.Animation.DROP,});
		marker.setMap(map);
		<?php } ?>
		var myLatlng = new google.maps.LatLng(<?php echo $sal_lat; ?>, <?php echo $sal_lng; ?>)
		var marker = new google.maps.Marker
		({ position: myLatlng,title:"Salesman Name: <?php echo $sal_name; ?>, Location Type: <?php echo $sal_network_mode; ?>, Accuracy: <?php echo $sal_accuracy_level; ?>",icon: 'images/sal_marker.png', animation: google.maps.Animation.DROP,});
		marker.setMap(map);
		<?php if($ret_lat!='' && $ret_lat>0){ ?>
		var flightPlanCoordinates = [
			new google.maps.LatLng(<?php echo $ret_lat; ?>, <?php echo $ret_lng; ?>),
			new google.maps.LatLng(<?php echo $sal_lat; ?>, <?php echo $sal_lng; ?>)
		];
		var flightPath = new google.maps.Polyline({
			path: flightPlanCoordinates,
			strokeColor: "#FF0000",
			strokeOpacity: 1.0,
			strokeWeight: 2
		});
		var circleOptions1 = {
			center: new google.maps.LatLng(<?php echo $ret_lat; ?>, <?php echo $ret_lng; ?>),
			radius: 50,
			strokeWeight: 0,
			fillColor: "#BDBDBD",
			fillOpacity: 0.50,
			map: map,
			editable: false
			};
		var myLatlng1 = new google.maps.LatLng(<?php echo $ret_lat; ?>, <?php echo $ret_lng; ?>);	
		var circle = new google.maps.Circle(circleOptions1);
		flightPath.setMap(map);
		<?php } ?>
     }
</script>

  </head>
  <body onload="initialize()">
  
    <div id="map_canvas" style="width:100%; height:100%"></div>
  </body>
  <?php if($ret_lat=='' && $uType == 'Retailer '){ ?>
		<script language="Javascript">
		alert("Dealer Location does not exists.");
		</script>
  <?php } else  if($ret_lat=='' && $uType == 'Distributor '){ ?>

		<script language="Javascript">
		alert("Distributor Location does not exists.");
		</script>
  	<?php } ?>
</html>