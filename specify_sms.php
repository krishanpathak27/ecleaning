<?php include("includes/config.inc.php");
      include("includes/function.php");
	  $_objAdmin = new Admin();
	  

	  if(isset($_POST['sms_schedule']) && $_POST['sms_schedule']=='yes'){
	  
	  	//echo "<pre>";
		//print_r($_POST);

		//$smslist = htmlentities($_POST['msg'], ENT_QUOTES, "UTF-8");
		//echo htmlspecialchars($_POST['msg'], ENT_QUOTES);

		
		$retailerID = $_POST['retailersID'];

		foreach($retailerID as $Rvalue):
		
		// Fetch all the retailers list with phone number or name.
		$data = $_objAdmin->_getSelectList('table_retailer AS R','contact_number','',' status = "A" AND retailer_id = '.$Rvalue.' AND contact_number REGEXP "^[1-9]+$" AND contact_number REGEXP "^.{10}$" ORDER BY retailer_name');
				
		//print_r($data);

		 if(!empty($data))
		 {
			//Retailer SMS
			$smslist = $_POST['msg'];
			$retailer_contact_number = $data[0]->contact_number;
						
			$sms = explode("||||",wordwrap($smslist,153,"||||"));
			$num = count($sms);
			$i = 1; 
						
			foreach ($sms as $value)
			 {
				if ($num > 1) 
				{
					$mobile=SMS_MOBILE;
					$pass=SMS_PASS;
					$senderid=SMS_SENDERID; 
					$to=$retailer_contact_number;
					$msg=urlencode("($i/$num) $value");
					//$sendsms = file("http://trans.smsndata4india.com/sendsms.aspx?mobile=$mobile&pass=$pass&senderid=$senderid&to=$to&msg=$msg");
				$i++;
				} 
				else 
				{
					$mobile=SMS_MOBILE;
					$pass=SMS_PASS;
					$senderid=SMS_SENDERID; 
					$to=$retailer_contact_number;
					$msg=urlencode($value);
					//$sendsms = file("http://trans.smsndata4india.com/sendsms.aspx?mobile=$mobile&pass=$pass&senderid=$senderid&to=$to&msg=$msg");
				}
			}
		}
		endforeach;
		//exit;
	  	$sus = 'Message sent to selected '.sizeof($retailerID).' retailers.';
		unset($_POST);
		//header('Location: specify_sms.php');
		
	  }

	include("header.inc.php");?>
  <!--<link rel="stylesheet" href="docsupport/style.css">-->
  <link rel="stylesheet" href="docsupport/prism.css">
  <link rel="stylesheet" href="docsupport/chosen.css">
  <style type="text/css" media="all">
    /* fix rtl for demo */
    .chosen-rtl .chosen-drop { left: -9000px; }
	
	#smsform label.error {
	margin-left: 10px;
	width: auto;
	display: inline;
	}
	.error {
		color: #FF0000;
		font:normal 12px tahoma;
	}

  </style>

<div id="content-outer">
<!-- start content -->
<div id="content">
<div id="savingButton" style="display:none;"><center>Do you want to send SMS to selected retailers?</center></div>
<div id="page-heading"><h1>Send SMS</h1></div>
<table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">
	<tr>
		<th rowspan="3" class="sized"><img src="images/side_shadowleft.jpg" width="20" height="300" alt="" /></th>
		<th class="topleft">&nbsp;</th>
		<td id="tbl-border-top">&nbsp;</td>
		<th class="topright">&nbsp;</th>
		<th rowspan="3" class="sized"><img src="images/side_shadowright.jpg" width="20" height="300" alt="" /></th>
	</tr>
	
	<tr>
		<!--<td id="tbl-border-left"></td>-->
	<td valign="top">
		<div id="content-table-inner">
			<table border="0" width="100%" cellpadding="0" cellspacing="0">
				<tr valign="top">
					<td>
					<?php if($rou_name_err!=''){?>
					<div id="message-red">
						<table border="0" width="100%" cellpadding="0" cellspacing="0">
							<tr>
								<td class="red-left">Error. <?php echo $rou_name_err; ?></td>
								<td class="red-right"><a class="close-red"><img src="images/icon_close_red.gif" alt="" /></a></td>
							</tr>
						</table>
					</div>
					<?php } ?>
					
					<?php if($sus!=''){?>
					<div id="message-green">
						<table border="0" width="100%" cellpadding="0" cellspacing="0">
						<tr>
							<td class="green-left"><?php echo $sus; ?></td>
							<td class="green-right"><a class="close-green"><img src="images/icon_close_green.gif" alt=""/></a></td>
						</tr>
						</table>
					</div>
					<?php } ?>




	<form  method="post">
	<div class="side-by-side clearfix">
	<table width="100%" border="0" cellspacing="5" cellpadding="5">
	
		<tr><th>&nbsp;</th><td class="error">&nbsp;</td></tr>
		<tr><th>&nbsp;</th><td>&nbsp;</td></tr>
	  <tr>
		<th scope="col" align="left"><em>Select City</em></th>
		<td scope="col" align="left">
		<select name="city" id="city" data-placeholder="Select City" style="width:350px;" onchange="getCityRetailers(this.value)" class="chosen-select" tabindex="5" >
			<option value=""></option>
			<?php  $states = $_objAdmin->_getSelectList2('state','state_id, state_name','','');
					foreach($states as $srows):
					$cities = $_objAdmin->_getSelectList2('city','city_id,city_name','',' state_id ='.$srows->state_id); ?>
						<optgroup label="<?php echo ucwords(strtolower(trim($srows->state_name)));?>"><?php
							foreach($cities as $rows):?>
							<option value="<?php echo $rows->city_id;?>" <?php if(is_numeric($_GET['cid']) && isset($_GET['cid']) &&  $_GET['cid'] == $rows->city_id){?> selected="selected"<?php }?>><?php echo ucwords(strtolower(trim($rows->city_name)));?></option>
							<?php endforeach;?>
						</optgroup>
					<?php endforeach; ?>
				</select>
		</td>
	  </tr>
	  
	  <tr><th>&nbsp;</th><th>&nbsp;</th></tr>
	  
	  <tr>
		<th scope="col" align="left" valign="top"><em>Multiple Select Retailers</em></th>
		<td scope="col" align="left">
		<select name="retailersID[]" id="retailersID" data-placeholder="Select Retailers" style="width:550px;" class="chosen-select" multiple tabindex="6">
			<option value=""></option>
			<?php if(isset($_GET['cid']) && $_GET['cid']!='' && is_numeric($_GET['cid'])){ 
			$markets = $_objAdmin->_getSelectList('table_retailer',"Distinct(retailer_location)",''," city=".$_GET['cid']);
			if(!empty($markets)){
			 foreach($markets as $key=>$Mvalue):
				$marketName = "'".$Mvalue->retailer_location."'";
				$data = $_objAdmin->_getSelectList('table_retailer','retailer_id, retailer_name','',
						' retailer_location = '.$marketName.' AND status = "A"');
				if(!empty($data)){?>
				<optgroup label="<?php echo ucwords(strtolower(trim($Mvalue->retailer_location)));?>">
				<?php foreach($data as $value): ?>
		<option value="<?php echo $value->retailer_id;?>"><?php echo ucwords(strtolower(trim($value->retailer_name)));?></option>
				<?php endforeach; ?>
				</optgroup>
				<?php }?>
				<?php endforeach; ?>
			<?php }?>
			<?php }?>				
				</select>
		</td>
	  </tr>
	  
	  <tr><th>&nbsp;</th><th>&nbsp;</th></tr>
	  
	  
	   <tr>	
	   		<th scope="col" align="left" valign="top"><em>Message</em></th>
	   		<td><textarea name="msg" class="default" id="msg"></textarea></td>
		</tr>
	  <tr><th>&nbsp;</th><th>&nbsp;</th></tr>
	  <tr>
			<th>&nbsp;</th>
			<td valign="top" >
			<input name="sms_schedule" type="hidden" value="yes" />
			<input type="button" value="Back" class="form-reset" onclick="location.href='specify_sms.php';" />
			<input name="submit" class="form-submit" type="submit" id="submit" value="Send Msg" />
			</td>
		</tr>
	</table>

	</div>	
	
		
		 <script src="docsupport/jquery1.6.4.min.js" type="text/javascript"></script>
		  <script src="docsupport/chosen.jquery.js" type="text/javascript"></script>
		  <!--<script src="docsupport/prism.js" type="text/javascript" charset="utf-8"></script>-->

		  <script type="text/javascript">
			var config = {
			  '.chosen-select'           : {},
			  '.chosen-select-deselect'  : {allow_single_deselect:true},
			  '.chosen-select-no-single' : {disable_search_threshold:10},
			  '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
			  '.chosen-select-width'     : {width:"95%"}
			}
			for (var selector in config) {
			  $(selector).chosen(config[selector]);
			}
			
			function getCityRetailers(cid){
			alert(cid);
				window.location.href = "specify_sms.php?cid="+cid;
				/*$.ajax({
					url: "ajaxGetlistofretailers.php",
					data: "cid="+cid,
					success: function(msg){
						alert(msg);
						$('.chosen-drop').text(msg);	
					}
				});*/
			}
			
			function NotifyUser() 
			{
				$( "#savingButton" ).dialog({
				  title:'Message',
				  content:'',
				  resizable: false,
				  height:'220',
				  modal: true,
				  buttons: {
					"Yes": function() {
						$("#frmPre").submit();
						$( this ).dialog( "close" );
					},
					Cancel: function() {
					  $( this ).dialog( "close" );
					}
				  }
				});
			}
			
			//function Validateform() {
			$().ready(function(){
			$( "#submit" ).click(function() {
				var city = $('#city').val();
				var rtr = $('#retailersID').val();
				var msg = $('#msg').val();

				if(typeof city == 'undefined' || city == '') {
					$('.error').text('Please select an city!');
					$( "#city" ).trigger( "click" );
					//$('.chosen-container').addClass('chosen-container-active chosen-with-drop');
					//$('.chosen-search').find('input').focus();
					return false;
				
				} else if(typeof rtr == 'undefined' || rtr == '' || rtr == null) { 
					$('.error').text('Please select one or few retailers!');
					$( "#retailersID" ).trigger( "click" );
					$('#rtr').focus();
					return false;
				
				} else if(typeof msg == 'undefined' || msg == '') {
					$('.error').text('Please enter your message here!');
					$('#msg').focus();
					return false;
				
				} else {
					$('.error').text('');
					NotifyUser();
					return false;
				}
		});
		});
		  </script>
		  <script src="javascripts/jquery-ui.js"></script>
			<script type="text/javascript">$.noConflict();</script>
			
		</form>
	
		</td>
	</tr>
	
	
	<tr>
		<td><img src="images/blank.gif" width="695" height="1" alt="blank" /></td>
		<td>&nbsp;</td>
	</tr>
	
	</table>
<div class="clear"></div>
</div>
<!--  end content-table-inner  -->
</td>
<td id="tbl-border-right"></td>
</tr>
<!--<tr>
	<th class="sized bottomleft"></th>
	<td id="tbl-border-bottom">&nbsp;</td>
	<th class="sized bottomright"></th>
</tr>-->
</table>
<div class="clear">&nbsp;</div>
</div>
<!--  end content -->
<div class="clear">&nbsp;</div>
</div>
<div  style="
position:fixed;
height: 40px;
bottom:0;
width:100%;
font:normal 12px tahoma;
font-weight:bold;
background: url(menu/images/main-bg.png) repeat-x;">
<div id="footer">
	<!--  start footer-left -->
<div id="footer-left">
	<div align="center"> &copy; 2011 <span style="color: #ce2700; font-family: Tahoma; font-weight: bold;">QUYTECH</span>  <a href="http://www.quytech.com/" target="_blank">www.quytech.com</a>. All rights reserved.</div></div>
	<div class="clear">&nbsp;</div>
</div>
</div>
</body>
</html>