<?php
include("includes/config.inc.php");
include("includes/function.php");
include("includes/globalarraylist.php");
$page_name="State Wise Order Report";


// Check salesman ID exists into session or not.
if(isset($_SESSION['SalOrderList']) && $_SESSION['SalOrderList']!='') { $salesman = " AND s.salesman_id='".$_SESSION['SalOrderList']."'"; } 

$stateRec=$_objAdmin->_getSelectList2('state',"state_name,state_id",'','status="A"');
$divisionRec=$_objAdmin->_getSelectList2('table_division',"division_name as division,division_id",'',$division1);
$designationRec=$_objAdmin->_getSelectList2('table_salesman_hierarchy',"description as designation,hierarchy_id",'');
$orderstatusRec=$_objAdmin->_getSelectList2('table_order',"DISTINCT(order_status)",'');


if(isset($_POST['showReport']) && $_POST['showReport'] == 'yes')
{	

	  if($_POST['state'] !=''){
	        $_SESSION['stateCust'] = $_POST['state'];
	    }

	if($_REQUEST['sal']!="") 
	{
		//$sal_list=$_REQUEST['sal'];
	    //$salesman=" AND O.salesman_id='".$sal_list."' ";
		$salArrList=$_REQUEST['sal'];	
		$filterby=$_REQUEST['filterby'];
		$salesman = $_objArrayList->getSalesmanConsolidatedList($salArrList,$filterby);
	}


	    if($_POST['customer']!=''){
	        $_SESSION['findCust'] = $_POST['customer'];
	    }

	    if($_POST['customerclass'] !=''){
	        $_SESSION['customerClassRec'] = $_POST['customerclass'];
	    }
	     if($_POST['division'] !=''){
	        $_SESSION['findDivision'] = $_POST['division'];
	    }
	    if($_POST['designation'] !='')
		{

		$_SESSION['findDesignation'] = $_POST['designation'];
		}
		 if($_POST['order_status'] !='')
		{

		$_SESSION['orderStatus'] = $_POST['order_status'];
		}

	    


		$customerTypeCondition = "";
		$classCondition = "";
		$divisionCondition = "";
		$designationCondition = "";
		$orderstatusCondition = "";
		$stateCondition="";



if(isset($_SESSION['findCust']) && $_SESSION['findCust']=="R"){
    $customerTypeCondition = " AND O.retailer_id > 0 ";
} else if(isset($_SESSION['findCust']) && $_SESSION['findCust']=="D"){
    $customerTypeCondition = " AND O.distributor_id > 0  AND O.retailer_id = 0";
} else if(isset($_SESSION['findCust'])  && $_SESSION['findCust']!="all"){
    $customerTypeCondition = " AND O.customer_id > 0 AND C.customer_type = '".$_SESSION['findCust']."' ";
}



if(isset($_SESSION['customerClassRec']) && $_SESSION['customerClassRec']!='all'){
    $classCondition =" AND ( R.relationship_id IN(".$_SESSION['customerClassRec'].") OR D.relationship_id IN(".$_SESSION['customerClassRec'].") OR C.relationship_id IN(".$_SESSION['customerClassRec'].") )";
} 

if($_SESSION['userLoginType'] == 5 && !isset($_SESSION['findDivision'])){
    $divisionIdString = implode(",", $divisionList);
    $divisionCondition = " AND ( R.division_id IN ($divisionIdString) OR  D.division_id IN($divisionIdString) OR C.division_id IN($divisionIdString) )";
    /*$statusCondition = "AND ( R.status='A' OR  D.status='A' OR C.status='A' )";
    $interestCondition = "AND ( R.status='A' OR  D.status='A' OR C.status='A' )";*/
} 

if(isset($_SESSION['findDivision']) && $_SESSION['findDivision']!="all"){
   $divisionCondition = " AND ( R.division_id IN(".$_SESSION['findDivision'].") OR  D.division_id IN(".$_SESSION['findDivision'].") OR  C.division_id IN(".$_SESSION['findDivision'].") )";

}

if(isset($_SESSION['findDesignation']) && $_SESSION['findDesignation']!="all"){
	 $designationCondition = " AND ( SR.hierarchy_id IN(".$_SESSION['findDesignation'].") )";

	}
	if(isset($_SESSION['orderStatus']) && $_SESSION['orderStatus']!="all"){
	 $orderstatusCondition = " AND ( O.order_status IN('".$_SESSION['orderStatus']."') )";

	}


if(isset($_SESSION['stateCust']) && $_SESSION['stateCust']!="all"){
     $stateCondition = " AND (R.state IN(".$_SESSION['stateCust'].") OR D.state IN(".$_SESSION['stateCust'].")  OR C.state IN(".$_SESSION['stateCust'].") )";
}
	
	
	
	
	if($_POST['from']!="") 
	{
	$from_date=$_objAdmin->_changeDate($_POST['from']);	
	}
	if($_POST['to']!="") 
	{
	$to_date=$_objAdmin->_changeDate($_POST['to']);	
	}

} else {
$from_date= $_objAdmin->_changeDate(date("Y-m-d"));
$to_date= $_objAdmin->_changeDate(date("Y-m-d"));
unset($_SESSION['SalAct']);		
}

if($_REQUEST['filterby']==''){
		
			$_REQUEST['filterby']=1;	
		
		}

if(isset($_REQUEST['reset']) && $_REQUEST['reset'] == 'yes')
{
	header("Location: statewise_report.php");
	unset($_SESSION['stateCust']);
	unset($_SESSION['findCust']);
	unset($_SESSION['findDivision']);
	unset($_SESSION['findDesignation']); 
	unset($_SESSION['orderStatus']);
    unset($_SESSION['customerClassRec']);
}

if($_REQUEST['sal']!=''){
$SalName=$_objAdmin->_getSelectList('table_salesman','salesman_name',''," salesman_id='".$_REQUEST['sal']."'"); 
$sal_name=$SalName[0]->salesman_name;
} else {
$sal_name="All Salesman";
}

$order_by_sal="asc";
$List= "ORDER BY s.salesman_name asc, O.date_of_order";
//$List= "ORDER BY O.date_of_order,s.salesman_name asc ";

if(isset($_POST['order_date']) && $_POST['order_date'] == 'yes')
{
	$List= "ORDER BY O.date_of_order asc,s.salesman_name asc ";
	$order_by_date="asc";
	$order_by_sal="desc";
	if($_REQUEST['sal']!="") 
	{
		if($_REQUEST['order_by_dt']=='asc')
		{
		$List= "ORDER BY O.date_of_order,s.salesman_name asc ";
		$order_by_date="desc";
		} else {
		$List= "ORDER BY O.date_of_order desc,s.salesman_name asc ";
		$order_by_date="asc";
		}
	}
}

if(isset($_POST['order_sal']) && $_POST['order_sal'] == 'yes')
{
	$List= "ORDER BY s.salesman_name asc, O.date_of_order";	
	$order_by_sal="asc";
	$order_by_date="desc";
}
?>

<?php include("header.inc.php") ?>

<script type="text/javascript">
	function showloader()
	{
		$('#Report').hide();
		$('#loader').show();
	}
</script>
<script>
$(document).ready(function(){
	$('#loader').hide();
	$('#loader').html('<div id="loader" align="center"><img src="images/ajax-loader.gif" /><br/>Please Wait...</div>');
	$('#Report').show();
});
</script>
<script type="text/javascript">

    function PrintElem(elem)
    {
        Popup($(elem).html());
    }

    function Popup(data) 
    {
		
        var mywindow = window.open('', 'Report');
		
        mywindow.document.write('<html><head><title>State Wise Order List</title>');
		mywindow.document.write('<table><tr><td><b>From Date:</b> <?php echo $from_date; ?></td><td><b>To Date:</b> <?php echo $to_date; ?></td></tr></table>');
        /*optional stylesheet*/ //mywindow.document.write('<link rel="stylesheet" href="main.css" type="text/css" />');
        mywindow.document.write('</head><body >');
        mywindow.document.write(data);
        mywindow.document.write('</body></html>');

        mywindow.print();
        mywindow.close();
        return true;
    }


function exportToexcel () {
	//alert('Start');
	tableToExcel('statewise_report', 'State wise Report', 'State_wise Report.xls');
	//alert('End');
}





$(document).ready(function()
{
	/*$('.orangeTotal').hide();
    $('.greenTotal').hide();*/
<?php //if($_POST['submit']=='Export to Excel'){ ?>
/*$('.orangeTotal').remove();
$('#thisElement').hide();
$('.greenTotal').remove();
$('#thisElement').show();*/

//$('.orangeTotal').hide();

//$('.greenTotal').hide();

			//document.getElementById("orangeTotal").style.display = "none";
			//document.getElementById("greenTotal").style.display = "none";
			
//alert('Ho Gya');
//tableToExcel('statewise_report', 'State wise Report', 'State_wise Report.xls');

//$('.orangeTotal').show();

//$('.greenTotal').show();
			//document.getElementById("orangeTotal").style.display = "block";
			//document.getElementById("greenTotal").style.display = "block";	
<?php //} ?>
});	
</script>

<script src="javascripts/dateNextPrev.js" type="text/javascript"></script>
<style>
.leftmarginwithoutborder td{ text-align: center;padding:10px; };

</style>
<!-- start content-outer -->
<input name="pagename" type="hidden"  id="pagename" value="statewise_report.php" />
<div id="content-outer">

<!-- start content -->
<div id="content">
<div id="loader" style="position:absolute; margin-left:40%; margin-top:10%;"></div>
<div id="page-heading"><h1><span style="color: #d74343; font-family: Tahoma; font-weight: bold;">State Wise Order Report</span></h1></div>
<table border="0" width="100%" cellpadding="0" cellspacing="0" id="statewise" class="statewise">

<tr>
	<td>
	<!--  start content-table-inner -->
	<div id="content-table-inner" style="padding-bottom:30px;">
	
	<div id="page-heading" align="left" >
	<form name="frmPre" id="frmPre" method="post" action="" enctype="multipart/form-data" >
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
	
	<tr>
	
		<td>
			<h3>State: </h3>
			<h6>
				<select name="state" id="state" class="menulist">
                                <option value="all" <?php echo ($_SESSION['stateCust'] =="all")?'selected':''?>>All</option>
                                <?php foreach ($stateRec as $key => $value) {?>
                                    <option value="<?php echo $value->state_id?>" <?php echo ($_SESSION['stateCust'] ==$value->state_id)?'selected':''?>><?php echo $value->state_name?></option>
                                <?php }?>
                            </select>
			</h6>
		</td>
	
		<td ><h3>Salesman:</h3><h6> 
		<select name="sal" id="sal" class="styledselect_form_5" style="" >
			<?php echo $rtndata = $_objArrayList->GetSalesmenMenu($salsList, $_REQUEST['sal']);?>
		</select>
		</h6></td>
		<td>
			<h3>Customer Type: </h3>
			<h6>
				 <select name="customer" id="customer" class="menulist">
                        <option value="all" <?php echo ($_SESSION['findCust'] =="all")?'selected':''?>>All</option>
                        <?php 
                        $customer_type = $_objAdmin->_getSelectList('`table_customer_type`','`cus_type_id`, `type_code`, `type_name`',''," `status` = 'A'"); 
                        if(is_array($customer_type)){
                        for($i=0;$i<count($customer_type);$i++){?>
                        <option value="<?php echo $customer_type[$i]->type_code;?>" <?php if($_SESSION['findCust']==$customer_type[$i]->type_code){ echo 'selected';} ?> ><?php echo $customer_type[$i]->type_name;?></option>
                        <?php }} ?>
                        </select>
			</h6>
		</td>
		<td>
			<h3>Customer Class: </h3>
			<h6>
				<h6>
                            <select name="customerclass" id="customerclass" class="menulist">
                             <option value="all">All</option>
                               
                            </select>
                        </h6>
			</h6>
		</td>
		<tr>
		<td>
			  <h3>Division: </h3>
                        <h6>
                            <select name="division" id="division" class="menulist">
                                <option value="all"<?php echo ($_SESSION['findDivision'] =="all")?'selected':''?>>All</option>
                                <?php 
                                    foreach ($divisionRec as $key => $value) {?>
                                        <option value="<?php echo $value->division_id?>" <?php echo ($_SESSION['findDivision'] ==$value->division_id)?'selected':''?>><?php echo $value->division ?></option>
                                    <?php }
                                ?>
                            </select>
                        </h6>
		</td>
		 <td>
			  <h3>Designation: </h3>
                        <h6>
                            <select name="designation" id="designation" class="menulist">
                                <option value="all"<?php echo ($_SESSION['findDesignation'] =="all")?'selected':''?>>All</option>
                                <?php 
                                    foreach ($designationRec as $key => $value) {?>
                                        <option value="<?php echo $value->hierarchy_id?>" <?php echo ($_SESSION['findDesignation'] ==$value->hierarchy_id)?'selected':''?>><?php echo $value->designation ?></option>
                                    <?php }
                                ?>
                            </select>
                        </h6>
		</td>
		 <td>
			  <h3>Order Status: </h3>
                        <h6>
                            <select name="order_status" id="order_status" class="menulist">
                                <option value="all"<?php echo ($_SESSION['orderStatus'] =="all")?'selected':''?>>All</option>
                                <?php 
                                    foreach ($orderstatusRec as $key => $value) {

                                    	if($value->order_status=='A'){
									    $status="New Order";
									    }
									    if($value->order_status=='I'){
									    $status="Processed";
									    }
									    if($value->order_status=='D'){
									    $status="Dispatched";
									    }

									    ?>
                                        <option value="<?php echo $value->order_status?>" <?php echo ($_SESSION['orderStatus'] ==$value->order_status)?'selected':''?>><?php echo $status; ?></option>
                                    <?php }
                                ?>
                            </select>
                        </h6>
		</td>
		<td><h3>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;From Date: </h3>
		<h6>
			<img src="css/images/prev.png" height="18" width="18" onclick="dateFromPrev();">  
			<input type="text" id="from" name="from" class="date" style="width:150px" value="<?php echo $from_date;?>" readonly />
		 	 <img src="css/images/next.png" height="18" width="18" onclick="dateFromNext();"> 
		</h6>
		</td>
		
		
		<td><h3>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;To Date: </h3> 
			<h6><img src="css/images/prev.png" height="18" width="18" onclick="dateToPrev();">  
			<input type="text" id="to" name="to" class="date" style="width:150px" value="<?php echo $to_date; ?>" readonly />
			 <img src="css/images/next.png" height="18" width="18" onclick="dateToNext();"> 
			</h6>
		</td>
	
		</tr>
		<tr>
				<td colspan="3" ><h2>View Report For:&nbsp;&nbsp;
					<input type="radio" name="filterby" value="1" <?php if($_REQUEST['filterby']==1){ ?> checked="checked" <?php } ?>   />&nbsp;Individual
					<input type="radio" name="filterby" value="2" <?php if($_REQUEST['filterby']==2){ ?> checked="checked" <?php } ?>  />&nbsp;Hierarchy</h2>
					</td></tr>
		<td colspan="2"></td>
		</tr>
		<tr class="consolidatedReport">
					
					</tr>
		<tr>
		<td colspan="6"><input name="showReport" type="hidden" value="yes" />
		
		<input name="submit" class="result-submit" type="submit" id="submit" value="View Details" onclick="showloader()";/>	
		<input type="button" value="Reset!" class="form-reset" onclick="location.href='statewise_report.php?reset=yes';" />
		
		
		<input type="button" value="Print" class="result-submit" onclick="PrintElem('#Report')" />
		<a id="dlink"  style="display:none;"></a>
		<input type="button" name="submit" value="Export to Excel" onclick="exportToexcel();" class="result-submit"  ></td>
	</tr>
	</table>
	</form>
	</div>
	

	<style type="text/css">
		.heading td {
			background: blue;
			color: white;
		}
	</style>
	
	<div style="overflow:scroll">
	<table border="0" cellpadding="0" cellspacing="0" id="statewise_report">
	<tr valign="top">
         <td style="border:none;">
         <div id="Report" style="width: 600%;">
		 <table border="0" id="report_export" name="report_export"  class="users">
			<tbody>	
			<tr class="heading">			
			<td width="30%">STATE</td>
			<td width="30%">Date</td>
			<td width="30%">Time</td>
			<td width="30%">Distributor Code</td>
			<td width="30%">Distributor Name</td>
			<td width="30%">Distributor Mobile No.</td>
			<td width="30%">Order ID</td>
			<td width="30%">Item Code</td>
			<td width="30%">Items</td>
			<td width="30%">Qty</td>
			<td width="30%">Material Price</td>
			<td width="30%">Total Price</td>
			<td width="30%">Retailer</td>
			<td width="30%">Retailer Mobile No.</td>
			<td width="30%">District</td>
			<td width="30%">Taluka</td>
			<td width="30%">Salesman</td>
			<td width="30%">Salesman Code</td>
			<td width="30%">Salesman Designation</td>
			<td width="30%">Status</td>
			</tr>
			</tbody>
		
<?php  $dateCond = "(O.date_of_order BETWEEN '".date('Y-m-d', strtotime($from_date))."' AND '".date('Y-m-d', strtotime($to_date))."')";
//if($_SESSION['StateWiseStateID']!="") $state_cond = " AND ST.state_id = ".$_SESSION['StateWiseStateID'];

 $auRet = $_objAdmin->_getSelectList('table_order AS O LEFT JOIN table_salesman AS s ON s.salesman_id = O.salesman_id   LEFT JOIN table_retailer AS R ON R.retailer_id = O.retailer_id LEFT JOIN table_distributors as D on O.distributor_id=D.distributor_id LEFT JOIN state AS ST ON ST.state_id = D.state LEFT JOIN table_customer as C on O.customer_id=C.customer_id LEFT JOIN table_salesman_hierarchy_relationship as SR on SR.salesman_id=s.salesman_id 
    	LEFT JOIN table_salesman_hierarchy as SH on SH.hierarchy_id=SR.hierarchy_id '," O.time_of_order, O.date_of_order,O.salesman_id,SH.description,O.order_status,s.salesman_name,s.salesman_code,ST.state_id, ST.state_name,R.relationship_id,D.relationship_id,C.relationship_id,R.division_id,D.division_id,C.division_id ",''," $dateCond  $stateCondition AND LOWER(O.order_type) = 'yes' $customerTypeCondition $classCondition $divisionCondition $designationCondition $orderstatusCondition GROUP BY O.salesman_id,O.date_of_order ORDER BY O.date_of_order DESC");

	//echo "<pre>";
	//print_r($auRet);
	$stateID = $_objAdmin->returnObjectArrayToIndexArray($auRet, 'state_id');
	$stateName = $_objAdmin->returnObjectArrayToIndexArray($auRet, 'state_name');
	$salesmenID = $_objAdmin->returnObjectArrayToIndexArray($auRet, 'salesman_id');
	$odrDateArr = $_objAdmin->returnObjectArrayToIndexArray($auRet, 'date_of_order');
	$filterstateName = array_values(array_unique($stateName));
	$filterstateId = array_values(array_unique($stateID));
	$filtersalesID = array_values(array_unique($salesmenID));
	$filterodrDateArr = array_values(array_unique($odrDateArr)); // Sort array into desendeing order
	//print_r(array_unique($stateID));
	//print_r(array_unique($salesmenID));
	//print_r($filterstateId);
	//print_r($filtersalesID);
	//print_r($filterstateName);
	//print_r($filterstateId);
	//print_r($filterodrDateArr);
	//echo sizeof($filterodrDateArr);
	
	//exit;
	if(is_array($filterstateId) && sizeof($filterstateId)>0) { // Check state
	
	
	foreach($filterstateId as $key=>$statevalue):?>
	
	
	
	<tr>
    <td width="30%" valign="top"><?php echo $filterstateName[$key];?></td>
	<td colspan="19">
	
	<?php if(is_array($filterodrDateArr) && sizeof($filterodrDateArr)>0) { // Check salesman
		foreach($filterodrDateArr as $datevalue):
		$step2 = $_objAdmin->_getSelectList('table_order AS O LEFT JOIN table_salesman AS s ON s.salesman_id = O.salesman_id  LEFT JOIN table_retailer AS R ON R.retailer_id = O.retailer_id LEFT JOIN table_distributors as D on O.distributor_id=D.distributor_id LEFT JOIN state AS ST ON ST.state_id = D.state LEFT JOIN table_customer as C on O.customer_id=C.customer_id LEFT JOIN table_order_detail AS OD ON OD.order_id = O.order_id LEFT JOIN table_item AS I ON I.item_id = OD.item_id LEFT JOIN table_salesman_hierarchy_relationship as SR on SR.salesman_id=s.salesman_id 
    	LEFT JOIN table_salesman_hierarchy as SH on SH.hierarchy_id=SR.hierarchy_id'," O.order_id, O.date_of_order,O.time_of_order,O.salesman_id,SH.description,O.order_status,R.retailer_code,s.salesman_name,R.retailer_code,D.distributor_name,D.distributor_phone_no,D.distributor_code,s.salesman_name,s.salesman_code,ST.state_name,R.relationship_id,D.relationship_id,C.relationship_id,R.division_id,D.division_id,C.division_id ",''," O.date_of_order = '".$datevalue."' AND ST.state_id =".$statevalue."  AND LOWER(O.order_type)='yes'  $customerTypeCondition $classCondition $divisionCondition $designationCondition $orderstatusCondition  GROUP BY O.salesman_id,O.date_of_order  ORDER BY O.date_of_order DESC");
		//echo count($auRet2);
		//echo "<pre>";
		//print_r($step2);
			
		if(is_array($step2) && sizeof($step2)>0) { // Check salesman 
	
		foreach($step2 as $orderData):?>
		
		<table border="0"  class="users">
		<tr>
		   <td valign="top" style="padding:5px 0px;" ><?php echo $orderData->date_of_order; ?></td>
		    
		     
			
    		
			 <td colspan="18" >
			<?php $step3 = $_objAdmin->_getSelectList('table_order AS O LEFT JOIN table_order_detail AS OD ON OD.order_id = O.order_id LEFT JOIN table_item AS I ON I.item_id = OD.item_id LEFT JOIN table_price AS P ON P.item_id=I.item_id  LEFT JOIN table_salesman AS s ON s.salesman_id = O.salesman_id  LEFT JOIN table_retailer AS R ON R.retailer_id = O.retailer_id LEFT JOIN table_distributors as D on O.distributor_id=D.distributor_id LEFT JOIN table_customer as C on O.customer_id=C.customer_id LEFT JOIN table_salesman_hierarchy_relationship as SR on SR.salesman_id=s.salesman_id 
    	LEFT JOIN table_salesman_hierarchy as SH on SH.hierarchy_id=SR.hierarchy_id ',"O.order_id,OD.quantity, I.item_name,I.item_code,P.item_mrp, O.time_of_order, O.date_of_order,O.salesman_id,SH.description,R.retailer_name,D.distributor_name,D.distributor_phone_no,D.distributor_code, R.retailer_location,R.retailer_code,O.time_of_order,O.order_status,R.relationship_id,D.relationship_id,C.relationship_id,R.division_id,D.division_id,C.division_id",''," O.date_of_order = '".$orderData->date_of_order."' AND LOWER(O.order_type)='yes' $stateCondition  $customerTypeCondition $classCondition $divisionCondition $designationCondition $orderstatusCondition  AND O.salesman_id =".$orderData->salesman_id." GROUP BY O.salesman_id,O.date_of_order,O.time_of_order ORDER BY O.date_of_order DESC ");
			//echo count($auRet2);
			//echo "<pre>";
			//print_r($step3);
			if(is_array($step3) && sizeof($step3)>0) { // Check salesman ?>
			<?php foreach($step3 as $orderData2):?>
				<table border="0" class="users">
				<tr>
				<td valign="top" style="padding:5px 0px;" ><?php echo $orderData2->time_of_order; ?></td>
				<td valign="top" style="padding:5px 20px;" ><?php echo $orderData2->distributor_code; ?></td>
				<td valign="top" style="padding:5px 0px;" ><?php echo $orderData2->distributor_name;?></td>
			    <td valign="top" ><?php echo $orderData2->distributor_phone_no;?></td>
					
					
					
					<td colspan="14">
					<?php $step4 = $_objAdmin->_getSelectList('table_order AS O  LEFT JOIN table_order_detail AS OD ON OD.order_id = O.order_id LEFT JOIN table_item AS I ON I.item_id = OD.item_id LEFT JOIN table_price AS P ON P.item_id=I.item_id LEFT JOIN table_retailer AS R ON R.retailer_id = O.retailer_id LEFT JOIN table_distributors as D on O.distributor_id=D.distributor_id LEFT JOIN table_customer as C on O.customer_id=C.customer_id LEFT JOIN city as CT on R.city=CT.city_id LEFT JOIN table_taluka as T on R.taluka_id=T.taluka_id LEFT JOIN table_salesman AS s ON s.salesman_id = O.salesman_id LEFT JOIN table_salesman_hierarchy_relationship as SR on SR.salesman_id=s.salesman_id 
    	LEFT JOIN table_salesman_hierarchy as SH on SH.hierarchy_id=SR.hierarchy_id',"O.order_status, OD.quantity, I.item_name,I.item_code,R.retailer_name,P.item_mrp,O.time_of_order,s.salesman_name,s.salesman_code,O.order_id,O.order_status, O.date_of_order,SH.description,R.relationship_id,D.relationship_id,C.relationship_id,R.division_id,D.division_id,C.division_id,R.retailer_phone_no,D.distributor_name,D.distributor_code,D.distributor_phone_no,CT.city_name,T.taluka_name ",''," O.order_id = '".$orderData2->order_id."' AND OD.price_type<>'' $stateCondition $customerTypeCondition $classCondition $divisionCondition $designationCondition $orderstatusCondition");
					
						
					
					//echo count($auRet2);
					//echo "<pre>";
					//print_r($step4);
					if(is_array($step4) && sizeof($step4)>0) { // Check salesman ?>
					<?php foreach($step4 as $orderDetailData):?>
					<?php if($orderDetailData->order_status=='A'){
					    $status="New Order";
					    }
					    if($orderDetailData->order_status=='I'){
					    $status="Processed";
					    }
					    if($orderDetailData->order_status=='D'){
					    $status="Dispatched";
					    }
					    ?>
					    <?php $totalQty=$orderDetailData->quantity*$orderDetailData->item_mrp; ?>
					
						<table border="0" class="users">
						<tr>
						<td width="30%" style="padding:5px 0px;"><?php if($orderDetailData->order_id!='' && $orderDetailData->order_id!='NULL'){   echo $orderDetailData->order_id; } else {  echo "-";} ?></td>
						
							<td width="30%"  ><?php if($orderDetailData->item_code!='' && $orderDetailData->item_code!='NULL'){  echo $orderDetailData->item_code; } else {  echo "-"; } ?></td>
						
							<td  width="30%" ><?php if($orderDetailData->item_name!='' && $orderDetailData->item_name!='NULL'){  echo $orderDetailData->item_name; } else { echo "NA"; } ?></td>
						
							<td width="30%"  >&nbsp;&nbsp;&nbsp;<?php if($orderDetailData->quantity!='' && $orderDetailData->quantity!='NULL'){  echo $orderDetailData->quantity; } else {  echo "-"; } ?></td>
						
							<td  width="30%" >&nbsp;&nbsp;&nbsp;<?php if($orderDetailData->item_mrp!='' && $orderDetailData->item_mrp!='NULL'){  echo $orderDetailData->item_mrp; } else {  echo "-"; } ?></td>

						    <td width="30%">&nbsp;&nbsp;&nbsp;<?php if($totalQty!='' && $totalQty!='NULL' ) { ?>
							<?php echo $totalQty; ?></td> <?php } else { ?><td><?php echo "-"; ?></td><?php }?>
						
							<td width="30%"  ><?php if($orderDetailData->retailer_name!='' && $orderDetailData->retailer_name!='NULL' ){  echo $orderDetailData->retailer_name; } else {  echo "-"; } ?></td>

							<td  width="30%" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php if($orderDetailData->retailer_phone_no!='' && $orderDetailData->retailer_phone_no!='NULL'){  echo $orderDetailData->retailer_phone_no;} else {  echo "-"; 
							} ?></td> 
						
							<td  width="30%" >&nbsp;&nbsp;&nbsp;<?php if($orderDetailData->city_name!='' && $orderDetailData->city_name!='NULL'){   echo $orderDetailData->city_name; } else {   echo "-";  }?> </td>
						
							<td  width="30%" >&nbsp;&nbsp;&nbsp;<?php if($orderDetailData->taluka_name!='' && $orderDetailData->taluka_name!='NULL' ){  echo $orderDetailData->taluka_name; } else {  echo "-"; }?></td>
						
							<td  width="30%" >&nbsp;&nbsp;&nbsp;<?php if($orderDetailData->salesman_name!='' && $orderDetailData->salesman_name!='NULL'  ){  echo $orderDetailData->salesman_name;} else {  echo "-"; } ?></td>

						
							<td  width="30%" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php if($orderDetailData->salesman_code!='' && $orderDetailData->salesman_code!='NULL' ){  echo $orderDetailData->salesman_code; } else { echo "-"; } ?></td>
						
							<td width="30%"  >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php if($orderDetailData->description!='' && $orderDetailData->description!='NULL' ){  echo $orderDetailData->description; } else {  echo "-"; } ?></td>

						
							<td  width="30%" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php if($orderDetailData->order_status!='' && $orderDetailData->order_status!='NULL' ){  echo $status; } else {  echo "-"; } ?></td>


						</tr>
						</table>	
					<?php /*$sum = $sum + $orderDetailData->quantity;*/  endforeach; ?>	
					<?php } else { ?>
					<?php }?>
					</td>
					</tr>
				</table>	
			<?php endforeach; ?>	
			<?php }?>
			</td>			
		</tr>

	
	<?php /* <tr  class="orangeTotal" style="color:#FFFFFF; font-weight:bold;">
		<td  id="totalitem" style="border:none;" colspan="14">Total Items</td>
		<th  style="border:none;"  align="right"><?php //$totalQty = $totalQty + $sum; echo $sum; unset($sum);?></th>
	</tr> */?>
	</table>
						
		<?php endforeach; ?>	
		<?php } else { ?>

		<?php }?>
		
		<?php endforeach; ?>
	<?php } else { ?>
			</td>
		<?php }?>
			<?php /*<tr class="greenTotal"  style="color:#FFFFFF; border-right:1px solid #000; font-weight:bold;"><td  colspan="15">&nbsp; <?php echo $filterstateName[$key];?> Total </td><td><?php echo $totalQty; unset($totalQty);?> </td></tr>*/ ?>
		<?php endforeach; ?>
	<?php } else { ?>
		<tr style="color:#000; border:1px solid #000; height:30px; font-weight:bold;"><td colspan="7" align="center">No Record</td></tr>
	<?php } ?>
	<tr><td  colspan="15" style="border:0px;">&nbsp;</td></tr>
	</table>	
	
	
	<div class="clear"></div>
	</div>
<!--  end content-table-inner  -->
</td>
<td id="tbl-border-right"></td>
</tr>

</table>
</div>



<div class="clear">&nbsp;</div>
</div>
</td></tr>
</table>
<!--  end content -->
<div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->
<div class="clear">&nbsp;</div> 
<!-- start footer -->         
<?php include("footer.php"); ?>
<!-- end footer -->
<script src="javascripts/jquery-1.8.2.js" type="text/javascript"></script>
<script src="javascripts/jquery-ui.js"></script>
<script type="text/javascript" src="javascripts/validate.js"></script>

<script>
    $(function() {
        $( "#from" ).datepicker({
			dateFormat: "d M yy",
            defaultDate: "w",
            changeMonth: true,
            numberOfMonths: 1,
            onSelect: function( selectedDate ) {
                $( "#to" ).datepicker( "option", "minDate", selectedDate );
            }
        });
        $( "#to" ).datepicker({
			dateFormat: "d M yy",
            defaultDate: "-w",
            changeMonth: true,
            numberOfMonths: 1,
            onSelect: function( selectedDate ) {
                $( "#from" ).datepicker( "option", "maxDate", selectedDate );
            }
        });
    });
	
	if($("#customer").val()!="all"){
        customerClass($("#customer").val());
    }
    $("#customer").change(function(){
        var customer = $(this).val();
        customerClass(customer);
    });

    function customerClass(customer){
    if(customer!='all'){
        $.ajax({
            method:"GET",
            url: "ajax_customer_geo_tag.php",
            data: { customer: customer },
            success: function(result){
                $("#customerclass").html(result);
            }
        });
    } else {
        $("#customerclass").html("<option value='all'>All</option>");
    }
}
	

var tableToExcel = (function () {
	
			//document.getElementsByClassName("greenTotal").remove();
			//document.getElementsByClassName("orangeToal").remove();
			


	/*$("#totalitem").html('');*/
        var uri = 'data:application/vnd.ms-excel;base64,'
        , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table><tr><td></td></tr></table><table>{table}</table></body></html>'
        , base64 = function (s) { return window.btoa(unescape(encodeURIComponent(s))) }
        , format = function (s, c) { return s.replace(/{(\w+)}/g, function (m, p) { return c[p]; }) }
        return function (table, name, filename) {
			
		/*	var cells = document.getElementById(table).getElementsByClassName("xyz");
var len = cells.length;
for(var i = 0; i < len; i++) {
    if(cells[i].className.toLowerCase() == "column") {
        cells[i].parentNode.removeChild(cells[i]);
    }
}*/


            if (!table.nodeType) table = document.getElementById(table);


            var ctx = { worksheet: name || 'Worksheet', table: table.innerHTML }
            
            document.getElementById("dlink").href = uri + base64(format(template, ctx));
            document.getElementById("dlink").download = filename;
            document.getElementById("dlink").click();
			//document.getElementById("orangeTotal").style.display = "block";
			//document.getElementById("greenTotal").style.display = "block";

        }
    })()
</script>

<style type="text/css">
.users {
    float: left;
    table-layout: fixed;
    white-space: nowrap;
    width: 100%;
}
#Report > div {
    background: none repeat scroll 0 0 blue;
    color: #fff;
    float: left;
    padding: 10px 0;
}

/* Column widths are based on these cells */
.row-ID {
  /*width: 20%;*/
}

.users td {
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
}
.users th {
  background: darkblue;
  color: white;
}
.users td,
.users th {
  text-align: left;
  padding: 5px 10px;
}
.users tr:nth-child(even) {
  /*background: lightblue;*/
}
</style>

</body>
</html>
