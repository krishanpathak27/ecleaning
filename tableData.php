<?php

include("includes/config.inc.php");
include("includes/function.php");

$_objAdmin = new Admin();
$page = $_REQUEST['datatable']['pagination']['page'];
$perPage = $_REQUEST['datatable']['pagination']['perpage'];
$sortname = $_REQUEST['datatable']['sort']['field'];
$sortorder = $_REQUEST['datatable']['sort']['sort'];
$search = $_REQUEST['datatable']['query']['generalSearch'];

if (!$sortname)
    $sortname = 'supervisor_name';
if (!$sortorder)
    $sortorder = 'asc';

$sort = " ORDER BY $sortname $sortorder";

if (!$page)
    $page = 1;
if (!$perPage)
    $perPage = 50;

$start = (($page - 1) * $perPage);

$limit = "$start, $perPage";
$query = $_REQUEST['query'];
$where = "(table_supervisor.status != 'D')";
$table = 'table_supervisor LEFT JOIN table_buildings on table_buildings.building_id=table_supervisor.building_id';
$clms = 'table_supervisor.supervisor_id,table_supervisor.supervisor_name,table_supervisor.building_id,table_supervisor.supervisor_address,'
        . 'table_supervisor.supervisor_phone_no,table_supervisor.status,table_supervisor.supervisor_email,table_supervisor.status,table_buildings.building_name';

if(isset($search) && !empty($search)) {
    $where .= " and (table_supervisor.supervisor_name like '%".$search."%' or table_supervisor.supervisor_address like '%".$search."%'"
            . "or table_supervisor.supervisor_email like '%".$search."%' or table_supervisor.supervisor_phone_no like '%".$search."%')";
}
$auRec = $_objAdmin->_getSelectList($table, $clms, $limit, $where.' '.$sort, '', '');
$auRecCount = $_objAdmin->_getSelectList('table_supervisor', 'count(*) as total', '', $where);
$total = $auRecCount[0]->total;

$dataReturn = array();
$dataReturn['meta'] = array(
    'page' => $page,
    'pages' => ceil($total/$perPage),
    'total' => $total,
    'perpage' => $perPage,
    'sort' => $sortorder,
    'field' => $sortname
);
foreach($auRec as $auRecTemp) {
    $temp['supervisor_id'] = $auRecTemp->supervisor_id;
    $temp['supervisor_name'] = $auRecTemp->supervisor_name;
    $temp['supervisor_address'] = $auRecTemp->supervisor_address;
    $temp['supervisor_phone_no'] = $auRecTemp->supervisor_phone_no;
    $temp['supervisor_email'] = $auRecTemp->supervisor_email;
    $temp['status'] = $auRecTemp->status;
    $temp['building_name'] = $auRecTemp->building_name;
    if($auRecTemp->status == 'A') {
        $temp['Status'] = 1;
    }else if($auRecTemp->status == 'I') {
        $temp['Status'] = 2;
    } else {
        $temp['Status'] = 2;
    }
    $dataReturn['data'][] = $temp;
}
echo json_encode($dataReturn, true);
die;


