<?php
include("includes/config.inc.php");
include("includes/function.php");
$_objAdmin = new Admin();
$day= date("D");
$_objAdmin->mysql_query("delete from table_temporder where salesman_id=".$_SESSION['salesmanId']." and guid='".$_SESSION['Guid']."' and account_id=".$_SESSION['accountId']);
// Generate Guid 
function NewGuid() { 
    $s = strtoupper(md5(uniqid(rand(),true))); 
    $guidText = 
        substr($s,0,8) .
        substr($s,8,4) . 
        substr($s,12,4). 
        substr($s,16,4). 
        substr($s,20); 
    return $guidText;
}

if(isset($_REQUEST['retailerId']) && $_REQUEST['retailerId']!="")
{
	$Guid = NewGuid();
	$_SESSION['Guid']=$Guid;	
	$_SESSION['retailerList']=$_REQUEST['retailerId'];	
	header("Location: order_form.php");
	die;
}
?>
<?php include("header.inc.php") ?>
<!-- start content-outer -->
<div id="content-outer">
<!-- start content -->
<div id="content">
<div id="page-heading"><h1><span style="color: #d74343; font-family: Tahoma; font-weight: bold;">Today's Retailer List</span></h1></div>
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" id="content-table">
<!--<tr>
	<th rowspan="3" class="sized"><img src="images/side_shadowleft.jpg" width="20" height="300" alt="" /></th>
	<th class="topleft"></th>
	<td id="tbl-border-top">&nbsp;</td>
	<th class="topright"></th>
	<th rowspan="3" class="sized"><img src="images/side_shadowright.jpg" width="20" height="300" alt="" /></th>
</tr>
-->
<tr>
	<!--<td id="tbl-border-left"></td>-->
	<td align="center" valign="middle">
	<!--  start content-table-inner -->
	<div id="content-table-inner">
	
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
	<tr valign="top">
	<td>
		<!-- start id-form -->
		
		<table border="0" width="100%" cellpadding="0" cellspacing="0" id="product-table">
			<tr>
				<th class="table-header-repeat line-left">Retailer Name</th>
				<th class="table-header-repeat line-left">Retailer Location</th>
				<th class="table-header-repeat line-left">Retailer Address</th>
				<th class="table-header-repeat line-left">Action</th>
			</tr>
			<?php
			$auRet=$_objAdmin->_getSelectList('table_route_retailer as r left join table_retailer as ret on r.retailer_id=ret.retailer_id',"ret.*",''," ret.status='A' and r.route_id=".$_SESSION['routeList']." and r.account_id=".$_SESSION['accountId']." order by ret.retailer_name	 ");
			for($i=0;$i<count($auRet);$i++){
			?>
			<tr>
				<td><?php echo $auRet[$i]->retailer_name;?></td>
				<td><?php echo $auRet[$i]->retailer_location;?></td>
				<td><?php echo $auRet[$i]->retailer_address;?></td>
				<td><a href="today_retailer_list.php?retailerId=<?php echo $auRet[$i]->retailer_id ?>">Order<a> / <a href="no_order_form.php?retailerId=<?php echo base64_encode($auRet[$i]->retailer_id) ?>">No Order<a></td>
			</tr>
			<?php } ?>
			
			
		</table>
		
	<!-- end id-form  -->

	</td>

</tr>
<tr valign="top">
		<td>
		<a href="today_route.php">
		<div id="order-act-top">
		Back
		</div><a>
		</td>
	</tr>
<tr>
<td><img src="images/shared/blank.gif" width="695" height="1" alt="blank" /></td>
<td></td>
</tr>
</table>
<div class="clear"></div>
</div>
<!--  end content-table-inner  -->
</td>
<td id="tbl-border-right"></td>
</tr>
<!--<tr>
	<th class="sized bottomleft"></th>
	<td id="tbl-border-bottom">&nbsp;</td>
	<th class="sized bottomright"></th>
</tr>-->
</table>
<div class="clear">&nbsp;</div>
</div>
<!--  end content -->
<div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->
<div class="clear">&nbsp;</div> 
<!-- start footer -->         
<?php include("footer.php") ?>
<?php unset($_SESSION['retailerList'], $_SESSION['Guid']); ?>
<!-- end footer -->
 
</body>
</html>