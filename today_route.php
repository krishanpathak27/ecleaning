<?php
include("includes/config.inc.php");
include("includes/function.php");
$_objAdmin = new Admin();


if(isset($_REQUEST['routeid']) && $_REQUEST['routeid']!="")
{
	$_SESSION['routeList']=$_REQUEST['routeid'];	
	header("Location: today_retailer_list.php");
	die;
}

?>
<?php include("header.inc.php") ?>
<!-- start content-outer -->
<div id="content-outer">
<!-- start content -->
<div id="content">
<div id="page-heading"><h1><span style="color: #d74343; font-family: Tahoma; font-weight: bold;">Today's Route</span></h1></div>
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" id="content-table">
<!--<tr>
	<th rowspan="3" class="sized"><img src="images/side_shadowleft.jpg" width="20" height="300" alt="" /></th>
	<th class="topleft"></th>
	<td id="tbl-border-top">&nbsp;</td>
	<th class="topright"></th>
	<th rowspan="3" class="sized"><img src="images/side_shadowright.jpg" width="20" height="300" alt="" /></th>
</tr>
-->
<tr>
	<!--<td id="tbl-border-left"></td>-->
	<td align="center" valign="middle">
	<!--  start content-table-inner -->
	<div id="content-table-inner">
	
	<table border="0" width="100%" cellpadding="0" cellspacing="0" id="product-table">
	<?php
	$auRut=$_objAdmin->_getSelectList('table_route_schedule as s left join table_route_schedule_by_day as d on s.route_schedule_id=d.route_schedule_id left join table_route as r on d.'.$day.'=r.route_id',"s.salesman_id,r.route_name,r.route_id",''," s.status='A' and ".$day."!='' and s.salesman_id=".$_SESSION['salesmanId']." and s.account_id=".$_SESSION['accountId']);
	if(is_array($auRut)){
	for($i=0;$i<count($auRut);$i++){
	?>
	<tr valign="top">
		<td>
		<div id="order-act-top">
		<a href="today_route.php?routeid=<?php echo$auRut[$i]->route_id; ?>"><?php echo $auRut[$i]->route_name;?><a>
		</div>
		</td>
	</tr>
	<?php } } else {
	?>
	<tr valign="top">
		<td>
		<div id="order-act-top">
		Today Route Not Available
		</div>
		</td>
	</tr>
	<?php } ?>

</table>
<div class="clear"></div>
</div>
<!--  end content-table-inner  -->
</td>
<td id="tbl-border-right"></td>
</tr>
<!--<tr>
	<th class="sized bottomleft"></th>
	<td id="tbl-border-bottom">&nbsp;</td>
	<th class="sized bottomright"></th>
</tr>-->
</table>
<div class="clear">&nbsp;</div>
</div>
<!--  end content -->
<div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->
<div class="clear">&nbsp;</div> 
<!-- start footer -->         
<?php include("footer.php") ?>
<?php unset($_SESSION['routeList']); ?>
<!-- end footer -->
 
</body>
</html>