<?php
include("includes/config.inc.php");
include("includes/function.php");
// division section starts here***********************
include("includes/globalarraylist.php");		  

		 $divisionArr ='';

		 $divisionArr = implode(",", $divisionList);

//********************************************************
$_objAdmin = new Admin();

	//From date  
	if(isset($_SESSION['FROM_DATE']) && $_SESSION['FROM_DATE'] != ''){  
		 $start_date = $_SESSION['FROM_DATE']; 

	}else
	{
		$start_date = date('Y-m-01');
	}

	//To date
	if(isset($_SESSION['TO_DATE']) && $_SESSION['TO_DATE'] != ''){
		$end_date = $_SESSION['TO_DATE'];			
	}else
	{
		$end_date = date('Y-m-d');
	}


	// Added this condition only when user logged in except admin
	if(isset($_SESSION['userLoginType']) && $_SESSION['userLoginType'] > 1) {

		$_objArrayList = new ArrayList();
		$salesmanList = $_objArrayList->SalesmanArrayList();

	}


	if(sizeof($salesmanList)>0) {  
			$salList = implode(",", $salesmanList); 
			$salesmanCondition = ' AND RET.salesman_id IN ('.$salList.')';


		} 

	 $yearFrom = date('Y',strtotime($start_date));
	 $yearTo = date('Y',strtotime($end_date));
	 $monthFrom = date('n',strtotime($start_date));
	 $monthTo = date('n',strtotime($end_date));
	 $dayFrom = date('j',strtotime($start_date));
	 $dayTo = date('j',strtotime($end_date));
	 $allMonths=0;
	 $allMonthsFromYear=0;
	 $allMonthsToYear=0;
	 if($yearFrom == $yearTo){
	 		 	
 		while($monthFrom <= $monthTo){
 			$monthsIn[]=$monthFrom;
 		  $monthFrom++; 		  	 		
 	     }
	 	$allMonths=implode(',', $monthsIn);
	 }else if($yearFrom < $yearTo){
	 		 	
 		while($monthFrom <= 12){
 			$monthsInFromYear[]=$monthFrom;
 		  $monthFrom++; 		  		 		
 	     }

 	     $mn=1;
 	     while($mn <= $monthTo){
 			$monthsInToYear[]=$mn;
 		  $mn++; 		  		 		
 	     }

	 	$allMonthsFromYear=implode(',', $monthsInFromYear);
	 	$allMonthsToYear=implode(',', $monthsInToYear);
	 }else{
	 	$allMonths=0;
	 	$allMonthsFromYear=0;
	 	$allMonthsToYear=0;
	 }
       $allMon = $allMonthsFromYear.','.$allMonthsToYear;


    $anaResult =$_objAdmin->_getSelectList('`table_route_scheduled` AS RS 
			LEFT JOIN table_route_schedule_details AS RSD ON RSD.`route_schedule_id` = RS.`route_schedule_id`

			LEFT JOIN table_route AS R ON R.route_id = RSD.route_id

			LEFT JOIN table_route_retailer AS RR ON RR.route_id = RSD.route_id AND RR.status IN ("R")
			
			INNER JOIN table_retailer AS RET ON RET.retailer_id = RR.retailer_id AND RR.status = "R" AND DATE_FORMAT(RET.start_date, "%Y-%m-%d") < DATE_FORMAT(CONCAT(RS.year,"-",RS.month,"-",RSD.assign_day),"%Y-%m-%d") AND RET.status = "A" 

			

			LEFT JOIN table_salesman as S on RS.salesman_id=S.salesman_id AND S.status = "A"
	    	LEFT JOIN table_salesman_hierarchy_relationship as SR on SR.salesman_id=S.salesman_id 
	    	LEFT JOIN table_salesman_hierarchy as SH on SH.hierarchy_id=SR.hierarchy_id		
			LEFT JOIN table_relationship AS RELD ON RELD.relationship_id = RET.relationship_id 
			LEFT JOIN table_division AS DI ON DI.division_id = RET.division_id 
			LEFT JOIN state AS ST ON ST.state_id = RET.state 
			LEFT JOIN city AS CT ON CT.city_id = RET.city 
			LEFT JOIN table_taluka AS TL ON TL.taluka_id = RET.taluka_id',


			"S.salesman_name,SH.description,RET.retailer_id,RET.retailer_name,RET.display_outlet,RET.retailer_address,RET.retailer_location as city_name,RELD.relationship_desc AS retailer_class,DI.division_name,ST.state_name,CT.city_name as district,TL.taluka_name,R.route_name,RS.created_date as route_date",'',
			"CASE WHEN '".$yearFrom."' = '".$yearTo."' THEN RS.month IN($allMonths) AND RS.year BETWEEN '".$yearFrom."' AND '".$yearTo."' AND RSD.assign_day BETWEEN '".$dayFrom."' AND '".$dayTo."' 
			ELSE 
				CASE 
			        WHEN '".$yearFrom."' < '".$yearTo."' THEN RS.month IN($allMon) AND RS.year BETWEEN '".$yearFrom."' AND '".$yearTo."' AND RSD.assign_day BETWEEN '".$dayFrom."' AND '".$dayTo."' 					
			    END 
			    
			END $salesmanCondition AND RET.division_id IN(".$divisionArr.") ORDER BY RSD.assign_day ASC"); //GROUP BY RS.created_date

    	// echo "<pre>";
    	// print_r($anaResult);exit;
        $err = '';

		if(count($anaResult) < 1){
			 $err='Data does not exist!';
		} 
    	

?>
<?php include("header.inc.php") ?>

<style type="text/css">
.wrapword{
white-space: -moz-pre-wrap !important;  /* Mozilla, since 1999 */
white-space: -webkit-pre-wrap; /*Chrome & Safari */ 
white-space: -pre-wrap;      /* Opera 4-6 */
white-space: -o-pre-wrap;    /* Opera 7 */
white-space: pre-wrap;       /* css-3 */
word-wrap: break-word;       /* Internet Explorer 5.5+ */
word-break: break-all;
white-space: normal;
}
</style>

<!-- start content-outer -->
<div id="content-outer">
<!-- start content -->
<div id="content">
<div id="page-heading"><h1><span style="color: #d74343; font-family: Tahoma; font-weight: bold;">Assigned Retailers to Salesman</span></h1>

<a href="export.inc.php?export_assigned_retailer"><input type="button" name="submit" value="Export to Excel" class="result-submit" style="float:right;"  ></a>

<!-- <input type="button" name="submit" value="Export to Excel" class="result-submit" style="float:right;" onclick="tableToExcel('id-form', 'Total Assigned Retailers', 'Total_Assigned_Retailers.xls');" > -->
<a id="dlink" href="#"></a>
</div>
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" id="content-table" style="margin-left:0%;">

<tr>
	<td id="tbl-border-left"></td>
	<td align="center" valign="middle">
	<!--  start content-table-inner -->
	<div id="content-table-inner">
	
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
	<tr valign="top">
	<td>		
		<!-- start id-form -->
		
		<table  border="0" width="100%" cellpadding="0" cellspacing="0" id="id-form">
		<?php if(count($anaResult) > 0){ ?>
		    <tr bgcolor="#6E6E6E" style="color: #fff;font-weight: bold;">
		    	<td style="padding:10px;max-width:70px;">Salesman Name</td>
				<td style="padding:10px;max-width:70px;">Salesman Designation</td>				
				<td style="padding:10px;max-width:90px;">Retailer Name</td>				
				<!--<td style="padding:10px;max-width:70px;">Division</td>-->
				<td style="padding:10px;max-width:70px;">Class</td>
				<td style="padding:10px;max-width:70px;">State</td>
				<td style="padding:10px;max-width:70px;">District</td>
				<td style="padding:10px;max-width:70px;">City</td>
				<td style="padding:10px;max-width:70px;">Taluka</td>
				<td style="padding:10px;max-width:100px;">Address</td>
				<!--<td style="padding:10px;max-width:70px;">Hot/Cold</td>-->
				<td style="padding:10px;max-width:70px;">Route Date</td>
				<td style="padding:10px;max-width:100px;">Route Name</td>				
			</tr>
		<?php
		for($i=0;$i<count($anaResult);$i++){
		?>  <tr  style="font-weight: bold;">
				<td style="padding:10px;max-width:70px;"><?php echo $anaResult[$i]->salesman_name; ?></td>
				<td style="padding:10px;max-width:70px;"><?php echo $anaResult[$i]->description; ?></td>
				<td style="padding:10px;max-width:90px;"><?php echo $anaResult[$i]->retailer_name; ?></td>
				<!--<td style="padding:10px;max-width:70px;"><-?php echo $anaResult[$i]->division_name; ?></td>-->
				<td style="padding:10px;max-width:70px;"><?php echo $anaResult[$i]->retailer_class; ?></td>
				<td style="padding:10px;max-width:70px;"><?php echo ucfirst($anaResult[$i]->state_name); ?></td>
				<td style="padding:10px;max-width:70px;"><?php echo ucfirst($anaResult[$i]->district); ?></td>
				<td style="padding:10px;max-width:70px;"><?php echo ucfirst($anaResult[$i]->city_name); ?></td>
				<td style="padding:10px;max-width:70px;"><?php echo ucfirst($anaResult[$i]->taluka_name); ?></td>
				<td style="padding:10px;max-width:100px;"><?php echo $anaResult[$i]->retailer_address; ?></td>
				<!--<td style="padding:10px;max-width:70px;"><-?php echo ucfirst($anaResult[$i]->display_outlet); ?></td>-->
				<td style="padding:10px;max-width:70px;"><?php echo $anaResult[$i]->route_date; ?></td>
				<td class="wrapword" style="padding:10px;max-width:100px;"><?php echo $anaResult[$i]->route_name; ?></td>			
			</tr>				
		
		<?php } ?>
		<?php } ?>
		<tr bgcolor="">
				<td colspan="11" style=""><span style="float: right;padding: 5px;padding-right: 10px;
 font-size:14px;"><strong>Total Sum:
				<?php echo count($anaResult); ?></strong></span></td>
			</tr>
	</table>
		<?php if($err!=''){?>
		<div id="message-red">
			<table border="0" width="100%" cellpadding="0" cellspacing="0">
			<tr>
				<td class="red-left" style="text-align:center;"> <?php echo $err; ?></td>
				<td class="red-right"><a class="close-red"><img src="images/icon_close_red.gif"   alt="" /></a></td>
			</tr>
			</table>
		</div>
		<?php } ?>
	</td>
	</tr>
	<tr valign="top">
	<td>
		<div id="div1"></div>
	</td>
	</tr>
	</div>
	</td>
</tr>
</table>
<div class="clear"></div>
</div>
<!--  end content-table-inner  -->
</td>
<td id="tbl-border-right"></td>
</tr>
<tr>
	<th class="sized bottomleft"></th>
	<td id="tbl-border-bottom">&nbsp;</td>
	<th class="sized bottomright"></th>
</tr>
</table>
<div class="clear">&nbsp;</div>
</div>
<!--  end content -->
<div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->
<div class="clear">&nbsp;</div> 
<!-- start footer -->         
<?php include("footer.php") ?>
<!-- end footer -->

<script type='text/javascript'>//<![CDATA[ 
var tableToExcel = (function () {
	
        var uri = 'data:application/vnd.ms-excel;base64,'
        , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table><tr><td></td></tr></table><table>{table}</table></body></html>'
        , base64 = function (s) { return window.btoa(unescape(encodeURIComponent(s))) }
        , format = function (s, c) { return s.replace(/{(\w+)}/g, function (m, p) { return c[p]; }) }
        return function (table, name, filename) {
			
		

            if (!table.nodeType) table = document.getElementById(table);


            var ctx = { worksheet: name || 'Worksheet', table: table.innerHTML }
            
            document.getElementById("dlink").href = uri + base64(format(template, ctx));
            document.getElementById("dlink").download = filename;
            document.getElementById("dlink").click();
			
        }
    })()

//]]>  
</script>

</body>
</html>