<?php
include("includes/config.inc.php");
include("includes/function.php");
$_objAdmin = new Admin();

	//From date  
	if(isset($_SESSION['FROM_DATE']) && $_SESSION['FROM_DATE'] != ''){  
		 $start_date = $_SESSION['FROM_DATE']; 

	}else
	{
		$start_date = date('Y-m-01');
	}

	//To date
	if(isset($_SESSION['TO_DATE']) && $_SESSION['TO_DATE'] != ''){
		$end_date = $_SESSION['TO_DATE'];			
	}else
	{
		$end_date = date('Y-m-d');
	}


	// Added this condition only when user logged in except admin
	if(isset($_SESSION['userLoginType']) && $_SESSION['userLoginType'] > 1) {

		$_objArrayList = new ArrayList();
		$salesmanList = $_objArrayList->SalesmanArrayList();

	}


	if(sizeof($salesmanList)>0) {  
			$salList = implode(",", $salesmanList); 
			$salesmanCondition = ' AND O.salesman_id IN ('.$salList.')';
		} 

    $anaResult = $_objAdmin->_getSelectList('table_order AS O 
    	LEFT JOIN table_salesman as S on O.salesman_id=S.salesman_id 
    	LEFT JOIN table_salesman_hierarchy_relationship as SR on SR.salesman_id=S.salesman_id 
    	LEFT JOIN table_salesman_hierarchy as SH on SH.hierarchy_id=SR.hierarchy_id
		LEFT JOIN table_retailer AS R ON R.retailer_id = O.retailer_id 
		LEFT JOIN table_relationship AS RELD ON RELD.relationship_id = R.relationship_id 
		LEFT JOIN table_division AS DI ON DI.division_id = R.division_id 
		LEFT JOIN state AS ST ON ST.state_id = R.state 
		LEFT JOIN city AS CT ON CT.city_id = R.city 
		LEFT JOIN table_taluka AS TL ON TL.taluka_id = R.taluka_id',
		'COUNT(O.salesman_id) as total_orders,S.salesman_name,SH.description,R.retailer_id,R.retailer_name,R.display_outlet,R.retailer_address,R.retailer_location as city_name,RELD.relationship_desc AS retailer_class,DI.division_name,ST.state_name,CT.city_name as district,TL.taluka_name',''," O.retailer_id!=0 AND O.date_of_order >= '".$start_date."' and O.date_of_order <= '".$end_date."' AND R.new='' $salesmanCondition GROUP BY O.retailer_id,O.salesman_id ");


        $err = '';

		if(count($anaResult) < 1){
			 $err='Data does not exist!';
		} 
    	

?>
<?php include("header.inc.php") ?>


<!-- start content-outer -->
<div id="content-outer">
<!-- start content -->
<div id="content">
<div id="page-heading"><h1><span style="color: #d74343; font-family: Tahoma; font-weight: bold;">Retailer Total Calls</span></h1>
<input type="button" name="submit" value="Export to Excel" class="result-submit" style="float:right;" onclick="tableToExcel('id-form', 'Total Retailer Call', 'Total_Retailer_Calls.xls');" >
<a id="dlink" href="#"></a>
</div>
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" id="content-table" style="margin-left:-7%;">

<tr>
	<td id="tbl-border-left"></td>
	<td align="center" valign="middle">
	<!--  start content-table-inner -->
	<div id="content-table-inner">
	
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
	<tr valign="top">
	<td>		
		<!-- start id-form -->
		
		<table  border="0" width="100%" cellpadding="0" cellspacing="0" id="id-form">
		<?php
$total_amt='';
		 if(count($anaResult) > 0){ ?>
		    <tr bgcolor="#6E6E6E" style="color: #fff;font-weight: bold;">				
				<td style="padding:10px;max-width:100px;">Retailer Name</td>
				<td style="padding:10px;max-width:100px;">Salesman Name</td>
				<td style="padding:10px;max-width:100px;">Salesman Designation</td>
				<td style="padding:10px;max-width:100px;">Division</td>
				<td style="padding:10px;max-width:100px;">Class</td>
				<td style="padding:10px;max-width:100px;">State</td>
				<td style="padding:10px;max-width:100px;">District</td>
				<td style="padding:10px;max-width:100px;">City</td>
				<td style="padding:10px;max-width:100px;">Taluka</td>
				<td style="padding:10px;max-width:100px;">Address</td>
				<td style="padding:10px;max-width:100px;">Hot/Cold</td>
				<td style="padding:10px;max-width:100px;">Total Calls</td>
			</tr>
		<?php
		for($i=0;$i<count($anaResult);$i++){
			$total_amt += $anaResult[$i]->total_orders;
		?>  <tr style="font-weight: bold;">				
				<td style="padding:10px;max-width:100px;"><?php echo $anaResult[$i]->retailer_name; ?></td>
				<td style="padding:10px;max-width:100px;"><?php echo $anaResult[$i]->salesman_name; ?></td>
				<td style="padding:10px;max-width:100px;"><?php echo $anaResult[$i]->description; ?></td>
				<td style="padding:10px;max-width:100px;"><?php echo $anaResult[$i]->division_name; ?></td>
				<td style="padding:10px;max-width:100px;"><?php echo $anaResult[$i]->retailer_class; ?></td>
				<td style="padding:10px;max-width:100px;"><?php echo ucfirst($anaResult[$i]->state_name); ?></td>
				<td style="padding:10px;max-width:100px;"><?php echo ucfirst($anaResult[$i]->district); ?></td>
				<td style="padding:10px;max-width:100px;"><?php echo ucfirst($anaResult[$i]->city_name); ?></td>
				<td style="padding:10px;max-width:100px;"><?php echo ucfirst($anaResult[$i]->taluka_name); ?></td>
				<td style="padding:10px;max-width:100px;"><?php echo $anaResult[$i]->retailer_address; ?></td>
				<td style="padding:10px;max-width:100px;"><?php echo ucfirst($anaResult[$i]->display_outlet); ?></td>
				<td style="padding:10px;max-width:100px;"><?php echo $anaResult[$i]->total_orders; ?></td>
			</tr>				
		
		<?php }?>
		<?php } ?>
		<tr bgcolor="">
				<td colspan="11" style=""><span style="float: right;padding: 5px;padding-right: 10px;
 font-size:14px;"><strong>Total Calls:
				<?php echo $total_amt; ?></strong></span></td>
			</tr>
	</table>
		<?php if($err!=''){?>
		<div id="message-red">
			<table border="0" width="100%" cellpadding="0" cellspacing="0">
			<tr>
				<td class="red-left"> <?php echo $err; ?></td>
				<td class="red-right"><a class="close-red"><img src="images/icon_close_red.gif"   alt="" /></a></td>
			</tr>
			</table>
		</div>
		<?php } ?>
	</td>
	</tr>
	<tr valign="top">
	<td>
		<div id="div1"></div>
	</td>
	</tr>
	</div>
	</td>
</tr>
</table>
<div class="clear"></div>
</div>
<!--  end content-table-inner  -->
</td>
<td id="tbl-border-right"></td>
</tr>
<tr>
	<th class="sized bottomleft"></th>
	<td id="tbl-border-bottom">&nbsp;</td>
	<th class="sized bottomright"></th>
</tr>
</table>
<div class="clear">&nbsp;</div>
</div>
<!--  end content -->
<div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->
<div class="clear">&nbsp;</div> 
<!-- start footer -->         
<?php include("footer.php") ?>
<!-- end footer -->

<script type='text/javascript'>//<![CDATA[ 
var tableToExcel = (function () {
	
        var uri = 'data:application/vnd.ms-excel;base64,'
        , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table><tr><td></td></tr></table><table>{table}</table></body></html>'
        , base64 = function (s) { return window.btoa(unescape(encodeURIComponent(s))) }
        , format = function (s, c) { return s.replace(/{(\w+)}/g, function (m, p) { return c[p]; }) }
        return function (table, name, filename) {
			
		

            if (!table.nodeType) table = document.getElementById(table);


            var ctx = { worksheet: name || 'Worksheet', table: table.innerHTML }
            
            document.getElementById("dlink").href = uri + base64(format(template, ctx));
            document.getElementById("dlink").download = filename;
            document.getElementById("dlink").click();
			
        }
    })()

//]]>  
</script>

</body>
</html>