<?php include("includes/config.inc.php");
      include("includes/function.php");
	  include("includes/globalarraylist.php");
	  
		if($_SESSION['transsalesmanID']!=''){
		
			//$salesman=" AND s.salesman_id = '".$_SESSION['transsalesmanID']."'";
			$salArrList=$_SESSION['transsalesmanID'];	
			$filterby=$_SESSION['transfilterBY'];
			$salesman = $_objArrayList->getSalesmanConsolidatedList($salArrList,$filterby);
		} 
	  
	   if(isset($_REQUEST['id']) && $_REQUEST['id']!="" )
	  {
		$_SESSION['TransID']=$_REQUEST['id'];	
		header("Location: transaction_details.php?searchParam_1=".$_REQUEST['searchParam_1']."&searchParam_2=".$_REQUEST['searchParam_2']."");
	  }
	  
	 if(isset($_POST['showTranslist']) && $_POST['showTranslist'] == 'yes')
	  {	
		if($_POST['retailer']!="") 
		{
			$_SESSION['retailerID']=$_POST['retailer'];	
		}
		
		
		if($_POST['salesman']!="") 
		{
			$_SESSION['transsalesmanID']=$_POST['salesman'];	
		} else {
		
			unset($_SESSION['transsalesmanID']);
		}
		
		if($_POST['from']!="") 
		{
			$_SESSION['FromTransList']=$_objAdmin->_changeDate($_POST['from']);	
		}
		if($_POST['to']!="") 
		{
			$_SESSION['ToTransList']=$_objAdmin->_changeDate($_POST['to']);	
		}
		
		
		if($_POST['filterby']!=""){			
				$_SESSION['transfilterBY']=$_POST['filterby'];			
		}

	
		if($_POST['retailer']=='all') 
		{
		  unset($_SESSION['retailerID']);
	  	
		}
		
		if($_POST['salesman']=='All') 
		{
		  unset($_SESSION['transsalesmanID']);
	  	
		}
		
	 if(isset($_POST['export']) && $_POST['export'])
	 {
	 header("Location:export.inc.php?export_transaction_list");
	 exit;
	 }			
		
		header("Location: transaction_list.php");
	}		

	if($_SESSION['transfilterBY']==''){		
			$_SESSION['transfilterBY']=1;			
		}
		

	if(isset($_REQUEST['reset']) && $_REQUEST['reset'] == 'yes')
	{
		 unset($_SESSION['retailerID']);	
		 unset($_SESSION['transsalesmanID']);		 
		 $_SESSION['FromTransList']=$_objAdmin->_changeDate(date('Y-m-d'));
		 $_SESSION['ToTransList']= $_objAdmin->_changeDate(date('Y-m-d'));
		 header("Location: transaction_list.php");
	}
		
	if(isset($_REQUEST['show']) && $_REQUEST['show']=="yes")
	{
		$_objAdmin->showTransaction($salesman);
		die;
	}		
?>	

<?php include("header.inc.php") ?>
<?php //echo "<pre>"; print_r($_SESSION);?>
<script src="javascripts/dateNextPrev.js" type="text/javascript"></script>

<!-- start content-outer -->
<input name="pagename" type="hidden"  id="pagename" value="" />
<div id="content-outer">
<!-- start content -->
<div id="content">
<div id="page-heading"><h1>
	<span style="color: #d74343; font-family: Tahoma; font-weight: bold;">Transaction List</span></h1></div>
	<table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">	
	<tr>
		<!--<td id="tbl-border-left"></td>-->
		<td>
		<!--  start content-table-inner -->
	<div id="content-table-inner">
	
	<div id="page-heading" align="left" >
	<form name="frmPre" id="frmPre" method="post" action="" enctype="multipart/form-data" >
		<table border="0" width="85%" cellpadding="0" cellspacing="0">
			
			<tr>
			<td id="salesmenMenu">
			<h3>Salesman: </h3><h6>
			<select name="salesman" id="salesman" class="menulist">
				<?php echo $salesmanOptionList = $_objArrayList->GetSalesmenMenu($salsList, $_SESSION['transsalesmanID'],'flex');?>
			</select>
			</h6>
			</td>
			
			<td id="salesmenMenu">
				<h3>Dealer: </h3><h6>
					<select name="retailer" id="retailer" class="menulist">
						<option value="all">All</option>
							<?php $aSal=$_objAdmin->_getSelectList('table_retailer','*',''," ORDER BY retailer_name"); 
								if(is_array($aSal))
								{
									foreach($aSal as $key=>$value):?>
									<option value="<?php echo $value->retailer_id;?>" <?php if($_SESSION['retailerID'] == $value->retailer_id){ ?> selected="selected" <?php } ?>><?php echo $value->retailer_name;?></option>
									<?php endforeach;?>
						  <?php } ?>
						 		
					</select>
				</h6>
				</td>
		
		
				<td><h3>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;From Date:</h3><h6><img src="css/images/prev.png" height="18" width="18" onclick="dateFromPrev();"> 			
				<input type="text" id="from" name="from" class="date" style="width:100px" value="<?php if($_SESSION['FromTransList']!='') { echo $_SESSION['FromTransList']; } else { echo $_objAdmin->_changeDate(date('Y-m-d')); }?>"  readonly /><img src="css/images/next.png" height="18" width="18" onclick="dateFromNext();"></h6>
				</td>
				
				<td><h3>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;To Date: </h3><h6><img src="css/images/prev.png" height="18" width="18" onclick="dateToPrev();"> 			
				<input type="text" id="to" name="to" class="date" style="width:100px" value="<?php if($_SESSION['ToTransList']!='') { echo $_SESSION['ToTransList']; } else { echo $_objAdmin->_changeDate(date('Y-m-d')); }?>"  readonly /><img src="css/images/next.png" height="18" width="18" onclick="dateToNext();"></h6>
				</td>				
				<td><h3></h3>
					<input name="showTranslist" type="hidden" value="yes" />
					<input name="submit" class="result-submit" type="submit" id="submit" value="View Details" />
			<input type="button" value="Reset!" class="form-reset" onclick="location.href='transaction_list.php?reset=yes';" />
				</td>	
				</tr>
				<tr class="consolidatedReport">
			<td colspan="3" ><h2>View Report For:&nbsp;&nbsp;
			<input type="radio" name="filterby" value="1" <?php if($_SESSION['transfilterBY']==1){ ?> checked="checked" <?php } ?>   />&nbsp;Individual
					<input type="radio" name="filterby" value="2" <?php if($_SESSION['transfilterBY']==2){ ?> checked="checked" <?php } ?>  />&nbsp;Hierarchy</h2>
			</td>
			</tr>
				<tr>
					<td colspan="5">
					<input name="export" class="result-submit" type="submit" id="export" value="Export To Excel" >
					<!-- <a href='transaction_list_year_graph.php?y=<?php echo checkFromdate($_SESSION['FromTransList'])."&salID=".$_SESSION['transsalesmanID']."&retID=".$_SESSION['RetailerID']; ?>' target="_blank"><input type="button" value=" Show Graph" class="result-submit" /></a> --></td>
				</tr>			
				
			</tr>
			<tr align="center" style="padding:10px; margin-top:10px;">
			<td colspan="8" >
			  <table border="0" width="100%" style="padding:10px; margin-top:10px;"><?php 
			    if($_SESSION['FromTransList']!='') { $frmNew = $_SESSION['FromTransList']; } else { $frmNew = date('Y-m-d'); }
		   		if($_SESSION['ToTransList']!='') { $toNew = $_SESSION['ToTransList']; } else { $toNew = date('Y-m-d'); }
				if($_SESSION['retailerID']!='') { $retailer = "  and retailer_id= ".$_SESSION['retailerID'].""; } 
			  
			  
			  
			  $td1=$_objAdmin->_getSelectList('table_transaction_details AS td LEFT JOIN table_salesman AS s ON s.salesman_id = td.salesman_id ','SUM(total_sale_amount) as total_cash',''," transaction_date >= '".date('Y-m-d', strtotime(mysql_escape_string($frmNew)))."' and transaction_date <= '".date('Y-m-d', strtotime(mysql_escape_string($toNew)))."' $salesman $retailer and transaction_type='1' "); 
			  
			   $td2=$_objAdmin->_getSelectList('table_transaction_details AS td LEFT JOIN table_salesman AS s ON s.salesman_id = td.salesman_id ','SUM(total_sale_amount) as total_cheque',''," transaction_date >= '".date('Y-m-d', strtotime(mysql_escape_string($frmNew)))."' and transaction_date <= '".date('Y-m-d', strtotime(mysql_escape_string($toNew)))."' $salesman $retailer and transaction_type='2' ");

			   $td3=$_objAdmin->_getSelectList('table_transaction_details AS td LEFT JOIN table_salesman AS s ON s.salesman_id = td.salesman_id ','SUM(total_sale_amount) as total_netbanking',''," transaction_date >= '".date('Y-m-d', strtotime(mysql_escape_string($frmNew)))."' and transaction_date <= '".date('Y-m-d', strtotime(mysql_escape_string($toNew)))."' $salesman $retailer and transaction_type='3' "); ?> 
			
			  
			  <tr>
			  	<td><span style="font-size:16px; margin-bottom:25px;"><b>Total Cash Amount:</b>&nbsp;<?php echo round($td1[0]->total_cash); ?></span></td>
				<td><span style="font-size:16px; margin-bottom:25px;"><b>Total Cheque Amount:</b>&nbsp;<?php echo round($td2[0]->total_cheque); ?></span></td>
				<td><span style="font-size:16px; margin-bottom:25px;"><b>Total Netbanking Amount:</b>&nbsp;<?php echo round($td3[0]->total_netbanking); ?></span></td>
				
				<td></td>
				<td></td>
			  </tr>
			  </table>
			  </td>
			  </tr>			
		</table>
	</form>
	</div>
	
	
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
	<tr valign="top">
	<td>
		<!-- start id-form -->
		<table  border="0" cellspacing="0" cellpadding="0"  align="center" width="100%">
			<tr>
				<td align="lift" valign="top"  >
					<table id="flex1" style="display:none"></table>
					<script type="text/javascript">showTransaction();</script>
				</td>
			</tr>
		</table>
	<!-- end id-form  -->
	</td>
</tr>


	<tr>
		<td><img src="images/blank.gif" width="695" height="1" alt="blank" /></td>
		<td></td>
	</tr>
</table>
<div class="clear"></div>
</div>
<!--  end content-table-inner  -->
</td>
<td id="tbl-border-right"></td>
</tr>
<!--<tr>
	<th class="sized bottomleft"></th>
	<td id="tbl-border-bottom">&nbsp;</td>
	<th class="sized bottomright"></th>
</tr>-->
</table>
<div class="clear">&nbsp;</div>
</div>
<!--  end content -->
<div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->
<div class="clear">&nbsp;</div> 
<!-- start footer -->         
<?php include("footer.php") ?>
<!-- end footer -->
<script src="javascripts/jquery-1.8.2.js" type="text/javascript"></script>
<script src="javascripts/jquery-ui.js"></script>
<script type="text/javascript" src="javascripts/validate.js"></script>
<script type="text/javascript">
// Popup window code
function newPopup(url) {
	popupWindow = window.open(
		url,'popUpWindow','height=500,width=600,left=200,top=100,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no,status=yes')
}
</script>
<script>
    $(function() {
        $( "#from" ).datepicker({
			dateFormat: "d M yy",
            defaultDate: "w",
            changeMonth: true,
            numberOfMonths: 1,
            onSelect: function( selectedDate ) {
                $( "#to" ).datepicker( "option", "minDate", selectedDate );
            }
        });
        $( "#to" ).datepicker({
			dateFormat: "d M yy",
            defaultDate: "-w",
            changeMonth: true,
            numberOfMonths: 1,
            onSelect: function( selectedDate ) {
                $( "#from" ).datepicker( "option", "maxDate", selectedDate );
            }
        });
    });
</script>
</body>
</html>