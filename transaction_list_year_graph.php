<?php
include("includes/config.inc.php");
include("includes/function.php");
include("includes/globalarraylist.php");
?>	
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta charset="utf-8">
<title>Quytech PepUpSales - Monthly Collection Graph</title>
<?php include_once('graph/header-files.php');?>
	</head>
<body> 
<!-- Start: page-top-outer -->

<?php include_once('graph/header.php');?>
<!-- End: page-top-outer -->
	
<div id="content-outer">
<div id="content">
<div id="page-heading"><h1>Monthly Collection Graph</h1></div>
<div id="container">


	<form name="frmPre" id="frmPre" method="post" action="<?php echo $_SERVER['PHP_SELF'];?>" enctype="multipart/form-data" >
		<table border="0" width="100%" cellpadding="0" cellspacing="0">
			
			<tr>
			<td><h3>Salesman: </h3></td>
			<td id="salesmenMenu">
			<select name="salID" id="salID" class="styledselect_form_5" >
			<?php echo $rtndata = $_objArrayList->GetSalesmenMenu($salsList, $_REQUEST['salID']);?>
			</select>
				</td>
			<td><h3>Retailer: </h3></td>
			<td id="salesmenMenu">
				
					<select name="retID" id="retID" class="styledselect_form_5" >
						<option value="all">All</option>
							<?php $aSal=$_objAdmin->_getSelectList('table_retailer','retailer_id, retailer_name',''," ORDER BY retailer_name"); 
								if(is_array($aSal))
								{
									foreach($aSal as $key=>$value):?>
									<option value="<?php echo $value->retailer_id;?>" <?php if($_REQUEST['retID'] == $value->retailer_id){ ?> selected="selected" <?php } ?>><?php echo $value->retailer_name;?></option>
									<?php endforeach;?>
						  <?php } ?>
						 		
					</select>
			</td>
		
				<td><h3>Year:</h3> </td>
				<td><select name="y" id="y"  class="styledselect_form_5" >
						<?php echo $year = $_objArrayList->getYearList2($_REQUEST['y']);?>
					</select>
				</td>
				
				<td>
					<input name="submit" class="result-submit" type="submit" id="submit" value="Show Graph" />
				</td>	
				
				
			</tr>
		</table>
	</form>
	
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
	<tr valign="top">
	<td>
		<!-- start id-form -->
		<table  border="0" cellspacing="0" cellpadding="0"  align="center" width="100%">
			<tr>
				<td align="lift" valign="top"  >
					<table id="flex1" style="display:none"></table>
					<script type="text/javascript">//showTransaction();</script>
				</td>
			</tr>
			<tr>
				<td align="lift" valign="top"  >
	<?php
	if($_REQUEST['retID']!='')
	{
		$retailer=" AND r.retailer_id = '".$_REQUEST['retID']."'";
	} 
	if($_REQUEST['salID']!='')
	{
		$salesman =" AND s.salesman_id = '".$_REQUEST['salID']."'";
	} 
	
	$fromdate=" and td.transaction_date >= '".$_REQUEST['y']."-1-1"."'";
	$todate=" and td.transaction_date <= '".$_REQUEST['y']."-12-31"."'";
	
	
	$where1 = " $salesman $retailer $fromdate $todate and td.transaction_type = 1 group by monthname(td.transaction_date)";
	
	$graph=$_objAdmin->_getSelectList('table_transaction_details as td left join table_retailer as r on td.retailer_id=r.retailer_id left join table_salesman as s on td.salesman_id=s.salesman_id',"td.transaction_type, sum(td.total_sale_amount) as amt, monthname(td.transaction_date) as month",'',$where1);
	
	
	$jan = 0; $feb = 0; $march = 0; $april = 0; $may = 0; $june = 0; $july = 0; $aug = 0; $sep = 0; $oct = 0; $nov = 0; $dec = 0;
	for($i=0; $i<count($graph); $i++)
	{
		if($graph[$i]->month=='January') 	{ $jan = $graph[$i]->amt; }
		if($graph[$i]->month=='February')	{ $feb = $graph[$i]->amt; }
		if($graph[$i]->month=='March') 		{ $march = $graph[$i]->amt; }
		if($graph[$i]->month=='April') 		{ $april = $graph[$i]->amt; }
		if($graph[$i]->month=='May') 		{ $may = $graph[$i]->amt; }
		if($graph[$i]->month=='June') 		{ $june = $graph[$i]->amt; }
		if($graph[$i]->month=='July') 		{ $july = $graph[$i]->amt; }
		if($graph[$i]->month=='August') 	{ $aug = $graph[$i]->amt; }
		if($graph[$i]->month=='September') 	{ $sep = $graph[$i]->amt; }
		if($graph[$i]->month=='October') 	{ $oct = $graph[$i]->amt; }
		if($graph[$i]->month=='November') 	{ $nov = $graph[$i]->amt; }
		if($graph[$i]->month=='December') 	{ $dec = $graph[$i]->amt; }
	}
	
	$where2 = " $salesman $retailer $fromdate $todate and td.transaction_type = 2 group by monthname(td.transaction_date)";
	$graph2=$_objAdmin->_getSelectList('table_transaction_details as td left join table_retailer as r on td.retailer_id=r.retailer_id left join table_salesman as s on td.salesman_id=s.salesman_id',"td.transaction_type, sum(td.total_sale_amount) as amt, monthname(td.transaction_date) as month",'',$where2);
		
	$jan2 = 0; $feb2 = 0; $march2 = 0; $april2 = 0; $may2 = 0; $june2 = 0; $july2 = 0; $aug2 = 0; $sep2 = 0; $oct2 = 0; $nov2 = 0; $dec2 = 0;
	for($i=0; $i<count($graph2); $i++)
	{
		if($graph2[$i]->month=='January') 	{ $jan2 = $graph2[$i]->amt; }
		if($graph2[$i]->month=='February')	{ $feb2 = $graph2[$i]->amt; }
		if($graph2[$i]->month=='March') 	{ $march2 = $graph2[$i]->amt; }
		if($graph2[$i]->month=='April') 	{ $april2 = $graph2[$i]->amt; }
		if($graph2[$i]->month=='May') 		{ $may2 = $graph2[$i]->amt; }
		if($graph2[$i]->month=='June') 		{ $june2 = $graph2[$i]->amt; }
		if($graph2[$i]->month=='July') 		{ $july2 = $graph2[$i]->amt; }
		if($graph2[$i]->month=='August') 	{ $aug2 = $graph2[$i]->amt; }
		if($graph2[$i]->month=='September') { $sep2 = $graph2[$i]->amt; }
		if($graph2[$i]->month=='October') 	{ $oct2 = $graph2[$i]->amt; }
		if($graph2[$i]->month=='November') 	{ $nov2 = $graph2[$i]->amt; }
		if($graph2[$i]->month=='December') 	{ $dec2 = $graph2[$i]->amt; }
	}
	?>

    <script type="text/javascript">
      google.load('visualization', '1', {packages: ['corechart']});
	 
    </script>
    <script type="text/javascript">
      function drawVisualization() {
        // Some raw data (not necessarily accurate)
        var data = google.visualization.arrayToDataTable([
          ['Month', 'Cash', 'Cheque'],
          ['January',  <?php echo $jan; ?>, <?php echo $jan2; ?>],
          ['February',  <?php echo $feb; ?>, <?php echo $feb2; ?>],
          ['March',  <?php echo $march; ?>, <?php echo $march2; ?>],
          ['April',  <?php echo $april; ?>, <?php echo $april2; ?>],
		  ['May',  <?php echo $may; ?>, <?php echo $may2; ?>],
          ['June',  <?php echo $june; ?>, <?php echo $june2; ?>],
          ['July',  <?php echo $july; ?>, <?php echo $july2; ?>],
          ['August',  <?php echo $aug; ?>, <?php echo $aug2; ?>],
		  ['September',  <?php echo $sep; ?>, <?php echo $sep2; ?>],
		  ['October',  <?php echo $oct; ?>, <?php echo $oct2; ?>],
		  ['November',  <?php echo $nov; ?>, <?php echo $nov2; ?>],
		  ['December',  <?php echo $dec; ?>, <?php echo $dec2; ?>]
        ]);

        var options = {
          title : 'Monthly Collection Graph',
          vAxis: {title: "Amount" , gridlines: { count: 8  }, minValue:0},
          hAxis: {title: "Month"},
          seriesType: "bars",
          series: {2: {type: "line"}}
        };

        var chart = new google.visualization.ComboChart(document.getElementById('chart_div'));
		/* click handler ends */
		 /* function selectHandler() {
          var selectedItem = chart.getSelection()[0];
          if (selectedItem) {
            var topping = data.getValue(selectedItem.row, 0);
            var url = 'admin_order_list_month_graph.php?m='+topping+'&y='+<?php echo $_REQUEST['y']; ?>;
			OpenInNewTab(url);
          }
        }

        google.visualization.events.addListener(chart, 'click', selectHandler);   
		 */
		/* click handler ends */
        chart.draw(data, options);
      }
      google.setOnLoadCallback(drawVisualization);
    </script>
	
	<div id="chart_div" style="width: 100%; height: 500px; border:0px solid;"></div>
	
	</td>
			</tr>
		</table>
	<!-- end id-form  -->
	</td>
</tr>


</table>

</div>
	
<div class="clear">&nbsp;</div>
	
</div>
</div>
	<!-- Graph code ends here -->
	<?php include("footer.php") ?>
</div>
</body>
</html>