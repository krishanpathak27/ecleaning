<?php 
include("includes/config.inc.php");
include("includes/function.php");
include("includes/globalarraylist.php");
$page_name="Unassigned Route Report";
if($_SESSION['userLoginType'] == 5){
	$divisionIdString = implode(",", $divisionList);
	$division = " AND s.division_id IN ($divisionIdString) ";
	$division1 = " division_id IN ($divisionIdString) ";
} else {
	$division = " ";
	$division1 = " ";
}
$divisionRec=$_objAdmin->_getSelectList2('table_division',"division_name as division,division_id",'',$division1);
if(isset($_POST['showReport']) && $_POST['showReport'] == 'yes'){
	if(!isset($_POST['sal'])){
		$salesmanCond="";
	} else if($_POST['sal'] !="" && $_POST['sal'] !="All"){
		$salesmanCond=" s.salesman_id='".$_POST['sal']."' ";
		$sals_id = $_POST['sal'];
	}else if($_POST['sal'] !="" && $_POST['sal'] =="All"){
		$salesmanCond="";
	}

	if(isset($_POST['division'])){
		if($_POST['division'] != 'all'){
			$division = " AND s.division_id ='".$_POST['division']."' ";
		} else {
			$division = "";
		}
	}

	/*if($_POST['from']!="") {
		$from_date=$_objAdmin->_changeDate($_POST['from']);	
	}
	if($_POST['to']!="") {
		$to_date=$_objAdmin->_changeDate($_POST['to']);	
	}*/
	if($_POST['month']!="" && is_numeric($_POST['month'])) {
		$month1=$_POST['month'];	
	}
	if($_POST['year']!="" && is_numeric($_POST['year'])) {
		$year1=$_POST['year'];	
	}
} else {
	/*$from_date= $_objAdmin->_changeDate(date("Y-m-d"));
	$to_date= $_objAdmin->_changeDate(date("Y-m-d"));*/
	$salesmanCond="";
}
if(isset($_REQUEST['reset']) && $_REQUEST['reset'] == 'yes'){
	header("Location: unassigned_route_report.php");
}

if($sal_id!=''){
	$SalName=$_objAdmin->_getSelectList('table_salesman','salesman_name',''," salesman_id='".$sals_id."'"); 
	$sal_name=$SalName[0]->salesman_name;
} 

if($_POST['city']!=''){
	$CityName=$_objAdmin->_getSelectList2('city','city_name',''," city_id='".$_POST['city']."'"); 
	$city_name=$CityName[0]->city_name;
} else {
	$city_name="All City";
}?>

<?php include("header.inc.php") ?>
<script type="text/javascript">

function PrintElem(elem) {
	Popup($(elem).html());
}

function Popup(data) {
	var mywindow = window.open('', 'Report');
	mywindow.document.write('<html><head><title>Unassigned Route Report</title>');
	mywindow.document.write('<table><tr><td><b>Salesman Name:</b> <?php echo $sal_name; ?></td><td><b>City Name:</b> <?php echo $city_name; ?></td><td><b>From Date:</b> <?php echo $from_date; ?></td><td><b>To Date:</b> <?php echo $to_date; ?></td></tr></table>');
	/*optional stylesheet*/ //mywindow.document.write('<link rel="stylesheet" href="main.css" type="text/css" />');
	mywindow.document.write('</head><body >');
	mywindow.document.write(data);
	mywindow.document.write('</body></html>');
	mywindow.print();
	mywindow.close();
	return true;
}

$(document).ready(function(){
	<?php if($_POST['submit']=='Export to Excel'){ ?>
		tableToExcel('report_export', 'Unassigned Route Report', 'Unassigned Route Report.xls');
	<?php } ?>
});
</script>

<script src="javascripts/dateNextPrev.js" type="text/javascript"></script>
<!-- start content-outer -->
	<input name="pagename" type="hidden"  id="pagename" value="admin_order_list.php" />
	<div id="content-outer">
	<!-- start content -->
		<div id="content">
			<div id="page-heading">
				<h1>
					<span style="color: #d74343; font-family: Tahoma; font-weight: bold;">Unassigned Route Report</span>
				</h1>
			</div>
			<table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">
				<tr>
					<td>
					<!--  start content-table-inner -->
						<div id="content-table-inner">
							<div id="page-heading" align="left" >
								<form name="frmPre" id="frmPre" method="post" action="" enctype="multipart/form-data" >
									<table border="0" width="100%" cellpadding="0" cellspacing="0">
										<tr>
											<td>
												<h3>Salesman: </h3>
												<h6>
													<select name="sal" id="sal" class="menulist">
														<?php echo $salesmanOptionList = $_objArrayList->GetSalesmenMenu($salsList, $sals_id,'flex');?>
													</select>
												</h6>
											</td>
											<!-- <td>
												<?php //echo $cityList = $_objArrayList->getCityList($_POST['city']);?>
											</td> -->
											<td>
												<h3>Division: </h3>
												<h6>
													<select name="division" id="division" class="menulist">
														<option value="all">All</option>
														<?php foreach ($divisionRec as $key => $value) {?>
															<option value="<?php echo $value->division_id?>" <?php echo (isset($_POST['division']) && $_POST['division']==$value->division_id)?'selected':''?>><?php echo $value->division?></option>
														<?php } ?>
													</select>
												</h6>
											</td>
											<td><?php echo $months = $_objArrayList->getMonthList($_POST['month']);?></td>
											<td><?php echo $year = $_objArrayList->getYearList($_POST['year']);?></td>
											<td>
												<h3></h3>
												<input name="submit" class="result-submit" type="submit" id="submit" value="View Details" />
												<input type="button" value="Reset!" class="form-reset" onclick="location.href='unassigned_route_report.php?reset=yes';" />
											</td>
											<td colspan="2"></td>
										</tr>
										<tr>
											<td colspan="6"><input name="showReport" type="hidden" value="yes" />
												<a id="dlink"  style="display:none;"></a>
												<input  type="submit" value="Export to Excel" name="submit" class="result-submit"  >
												<input type="button" value="Print" class="result-submit" onclick="PrintElem('#Report')" />
												<!-- <a href='new_retailer_report_year_graph.php?y=<?php echo checkFromdate($from_date)."&salID=".$_POST['sal']."&city=".$_POST['city']; ?>' target="_blank"><input type="button" value=" Show Graph" class="result-submit" /></a> -->
											</td>
										</tr>
									</table>
								</form>
							</div>
							<table border="0" width="100%" cellpadding="0" cellspacing="0">
								<tr valign="top">
									<td>
										<div id="Report" style="width:1100px; overflow:scroll">
											<table  border="0" width="100%" cellpadding="0" cellspacing="0" id="report_export" name="report_export" style="text-align:center;">
												<tr  bgcolor="#6E6E6E" style="color: #fff;font-weight: bold;" >
													<td style="padding:10px;" width="10%">Salesman Name</td>
													<td style="padding:10px;" width="10%">Salesman Code</td>
													<td style="padding:10px;" width="10%">Salesman Division</td>
													<td style="padding:10px;" width="10%">Date</td>
												</tr>
												<?php
												

													if($month1!="" && $year1!=""){
												$from_date = date('Y-n-d', strtotime($from_date));
												$to_date= date('Y-n-d', strtotime($to_date));
												
												$from_date2 = date('Y-n', strtotime($from_date));
												$to_date2 = date('Y-n', strtotime($to_date));									

/*$routeDate = $_objAdmin->_getSelectList("table_salesman as s 
LEFT JOIN table_division as tdiv ON tdiv.division_id = s.division_id 
LEFT JOIN table_route_scheduled as sd ON s.salesman_id=sd.salesman_id  AND CONCAT(sd.`year`,'-',sd.`month`)>='".$from_date2."' AND CONCAT(sd.`year`,'-',sd.`month`)<= '".$to_date2."'
LEFT JOIN table_route_schedule_details as d ON d.route_schedule_id=sd.route_schedule_id AND DATE_FORMAT(CONCAT(sd.`year`,'-',sd.`month`,'-',d.`assign_day`),'%Y-%m-%d') BETWEEN '".$from_date."' AND '".$to_date."'
LEFT JOIN table_route as r on r.route_id=d.route_id","d.route_id, s.salesman_id, s.salesman_code, s.salesman_name,tdiv.division_name, CONCAT(sd.year,'-',sd.month,'-',d.assign_day) AS date",'',"$salesmanCond $division AND d.route_id IS NULL
GROUP BY s.salesman_id,sd.year,sd.month,d.assign_day");*/

/*$auRut=$_objAdmin->_getSelectList('table_route_schedule as s left join table_route_schedule_by_day as d on s.route_schedule_id=d.route_schedule_id left join table_route as r on d.'.$day.'=r.route_id',"s.salesman_id,r.route_name,r.route_id",''," s.status='A' and ".$day."!='' and s.salesman_id=".$_SESSION['salesmanId']." and s.account_id=".$_SESSION['accountId']);*/


/*$routeDate = $_objAdmin->_getSelectList("table_salesman as s 
LEFT JOIN table_division as tdiv ON tdiv.division_id = s.division_id 
LEFT JOIN table_route_scheduled as sd ON s.salesman_id=sd.salesman_id 
LEFT JOIN table_route_schedule_details as d ON d.route_schedule_id=sd.route_schedule_id","s.salesman_id,s.salesman_name,tdiv.division_name,DATE_FORMAT(CONCAT(sd.year,'-',sd.month,'-',d.assign_day),'%Y-%m-%d') AS date",'',"$salesmanCond $division AND  CONCAT(sd.`year`,'-',sd.`month`,'-',d.assign_day)>='".$from_date."' AND CONCAT(sd.`year`,'-',sd.`month`,'-',d.assign_day)<= '".$to_date."' GROUP BY s.salesman_id,sd.year,sd.month,d.assign_day");*/

													$routeDate = $_objAdmin->_getSelectList("table_salesman as s 
													LEFT JOIN table_division as tdiv ON tdiv.division_id = s.division_id 
													LEFT JOIN table_route_scheduled as sd ON s.salesman_id=sd.salesman_id 
													LEFT JOIN table_route_schedule_details as d ON d.route_schedule_id=sd.route_schedule_id","s.salesman_id,DATE_FORMAT(CONCAT(sd.year,'-',sd.month,'-',d.assign_day),'%Y-%m-%d') AS date",''," AND sd.`year`='".$year1."' AND sd.`month`= '".$month1."' GROUP BY s.salesman_id,sd.year,sd.month,d.assign_day ORDER BY DATE_FORMAT(CONCAT(sd.year,'-',sd.month,'-',d.assign_day),'%Y-%m-%d') ASC");
													$dateArr = get_weeks($year1,$month1);
													
													$arr = array();
													foreach ($routeDate as $key => $value) {
														if(isset($arr[$value->date])){
															$arr[$value->date]['date'] = $value->date;
															$arr[$value->date]['id'][] = $value->salesman_id;
														} else {
															$arr[$value->date]['date'] = $value->date;
															$arr[$value->date]['id'][] = $value->salesman_id;
														}
													}
													$no_ret = 0;
													if(is_array($arr)){
														foreach ($dateArr as $key => $value) {
															if(isset($arr[$value])){

													$salesmanData = $_objAdmin->_getSelectList("table_salesman as s LEFT JOIN table_division as tdiv ON tdiv.division_id = s.division_id","s.salesman_code,s.salesman_name,tdiv.division_name",''," $salesmanCond $division AND s.salesman_id NOT IN (".implode(',', $arr[$value]['id']).") AND s.status='A' ORDER BY s.salesman_name ASC");

													foreach ($salesmanData as $key => $value1) {
														
												?>
												<tr style="border-bottom:2px solid #6E6E6E;">
													<td style="padding:10px;text-align:left;" width="10%"><?php echo $value1->salesman_name;?></td>
													<td style="padding:10px;" width="10%"><?php echo $value1->salesman_code;?></td>
													<td style="padding:10px;" width="10%"><?php echo $value1->division_name;?></td>
													<td style="padding:10px;" width="10%"><?php echo $arr[$value]['date'];?></td>
												</tr>
												<?php 
													$no_ret++;
													}
													} else {
													$salesmanData = $_objAdmin->_getSelectList("table_salesman as s LEFT JOIN table_division as tdiv ON tdiv.division_id = s.division_id","s.salesman_code,s.salesman_name,tdiv.division_name",''," $salesmanCond $division AND s.status='A' ORDER BY s.salesman_name ASC");

													foreach ($salesmanData as $key => $value1) {
														
												?>
												<tr style="border-bottom:2px solid #6E6E6E;">
													<td style="padding:10px;text-align:left;" width="10%"><?php echo $value1->salesman_name;?></td>
													<td style="padding:10px;" width="10%"><?php echo $value1->salesman_code;?></td>
													<td style="padding:10px;" width="10%"><?php echo $value1->division_name;?></td>
													<td style="padding:10px;" width="10%"><?php echo $value;?></td>
												</tr>
												<?php 
													$no_ret++;
													}	
													}
													}
													} ?>
												<tr>
													<td style="padding:10px;text-align:left;" colspan="9"><b>Total Number:</b> <?php echo $no_ret; ?></td>
												</tr>
												<?php } else { ?>
												<tr  style="border-bottom:2px solid #6E6E6E;" align="center" >
													<td style="padding:10px;" colspan="17">Report Not Available</td>
												</tr>
												<?php }?>
											</table>
										</div>
									</td>
								</tr>
								<tr>
									<td>
										<img src="images/blank.gif" width="695" height="1" alt="blank" />
									</td>
									<td></td>
								</tr>
							</table>
							<div class="clear"></div>
						</div>
						<!--  end content-table-inner  -->
					</td>
					<td id="tbl-border-right"></td>
				</tr>
			</table>
			<div class="clear">&nbsp;</div>
		</div>
		<!--  end content -->
		<div class="clear">&nbsp;</div>
	</div>
	<!--  end content-outer -->
	<div class="clear">&nbsp;</div> 
	<!-- start footer -->         
<?php 
function get_weeks($year, $month){

    $days_in_month = date("t", mktime(0, 0, 0, $month, 1, $year));
    $weeks_in_month = 1;
    $weeks = array();

    //loop through month
    for ($day=1; $day<=$days_in_month; $day++) {

       $date = date('Y-m-d',strtotime($year.'-'.$month.'-'.$day));

        $weeks[$weeks_in_month] = $date;

        if ($week_day == 0) {
            $weeks_in_month++;
        }

    }

    return $weeks;

}
include("footer.php") ?>
<?php unset($_SESSION['SalAttList']); ?>
<!-- end footer -->
<script src="javascripts/jquery-1.8.2.js" type="text/javascript"></script>
<script src="javascripts/jquery-ui.js"></script>
<script type="text/javascript" src="javascripts/validate.js"></script>
<script type="text/javascript">
// Popup window code
function newPopup(url) {
	popupWindow = window.open(
	url,'popUpWindow','height=500,width=600,left=200,top=100,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no,status=yes')
}
</script>
<script>
$(function() {
	$( "#from" ).datepicker({
		dateFormat: "d M yy",
		defaultDate: "w",
		changeMonth: true,
		numberOfMonths: 1,
		onSelect: function( selectedDate ) {
			$( "#to" ).datepicker( "option", "minDate", selectedDate );
		}
	});
	$( "#to" ).datepicker({
		dateFormat: "d M yy",
		defaultDate: "-w",
		changeMonth: true,
		numberOfMonths: 1,
		onSelect: function( selectedDate ) {
			$( "#from" ).datepicker( "option", "maxDate", selectedDate );
		}
	});
});
</script>
<script type='text/javascript'>//<![CDATA[ 
var tableToExcel = (function () {
var uri = 'data:application/vnd.ms-excel;base64,'
, template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table><tr><td><b>Salesman Name:</b> <?php echo $sal_name; ?></td><td><b>From Date:</b> <?php echo $_objAdmin->_changeDate($from_date); ?></td><td><b>To Date:</b> <?php echo $_objAdmin->_changeDate($to_date); ?></td></tr></table><table>{table}</table></body></html>'
, base64 = function (s) { return window.btoa(unescape(encodeURIComponent(s))) }
, format = function (s, c) { return s.replace(/{(\w+)}/g, function (m, p) { return c[p]; }) }
return function (table, name, filename) {
if (!table.nodeType) table = document.getElementById(table)
var ctx = { worksheet: name || 'Worksheet', table: table.innerHTML }

document.getElementById("dlink").href = uri + base64(format(template, ctx));
document.getElementById("dlink").download = filename;
document.getElementById("dlink").click();

}
})()

//]]>  
</script>
</body>
</html>
