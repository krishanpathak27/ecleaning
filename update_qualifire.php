<?php
include("includes/config.inc.php");
include("includes/function.php");
$_objAdmin = new Admin();
//echo $_SESSION['Guid'];
if(isset($_POST['add']) && $_POST['add'] == 'yes')
{
$temp_id=$_objAdmin->addQuaIncentive();
	if($_POST['type']==1){
		$cat_id=$_objAdmin->addQuaIncentiveCat($temp_id);
	}	
	if($_POST['type']==2 || $_POST['type']==9){
		$item_id=$_objAdmin->addQuaIncentiveItem($temp_id);
	}
	echo  "<script type='text/javascript'>";
	echo "javascript:window.close()";
	echo "</script>";
}
if(isset($_REQUEST['inc']) && $_REQUEST['inc']!="")
{
	$auRec=$_objAdmin->_getSelectList('table_target_incentive',"*",''," target_incentive_id=".$_REQUEST['inc']);
	if(count($auRec)<=0) header("Location: editincentive.php");
}
?>
<link rel="stylesheet" href="css/stylesheet.css" type="text/css" />
<link rel="stylesheet" href="./base/jquery.ui.all.css">
<script src="./Autocomplete/jquery-1.5.1.js"></script>
<script src="./Autocomplete/jquery.ui.core.js"></script>
<script src="./Autocomplete/jquery.ui.widget.js"></script>
<script src="./Autocomplete/jquery.ui.button.js"></script>
<script src="./Autocomplete/jquery.ui.position.js"></script>
<script src="./Autocomplete/jquery.ui.menu.js"></script>
<script src="./Autocomplete/jquery.ui.autocomplete.js"></script>
<script src="./Autocomplete/jquery.ui.tooltip.js"></script>
<script src="./Autocomplete/jquery.ui.datepicker.js"></script>
<script type="text/javascript" src="./javascripts/validate.js"></script>
<script src="./javascripts/incentive.js"></script>
<script type="text/javascript">
$(document).ready(function() {
	$("#category_list").attr('disabled', 'disabled');
	$("#item_list").attr('disabled', 'disabled');
	$("#item_list1").attr('disabled', 'disabled');
	$("#focus_item_list").attr('disabled', 'disabled');
	$("#state_list").attr('disabled', 'disabled');
	$("#city_list").attr('disabled', 'disabled');
	$("#day_list").attr('disabled', 'disabled');
	$("#week_list").attr('disabled', 'disabled');
	$("#month_list").attr('disabled', 'disabled');
	$("#quarterly_list").attr('disabled', 'disabled');
	$("#halfyearly_list").attr('disabled', 'disabled');
	$("#yearly_list").attr('disabled', 'disabled');
	$("#retailer_placed").attr('disabled', 'disabled');
});

</script>
<script type="text/javascript">	
$(document).ready(function(){
$('select').change(function(e) {
    e.preventDefault();
    $('option').css('background', '#E3E3E3');
	$('option').css('font-size', '20px');
    $('option:selected').css('backgroundColor', '#94B52C');
	
}).change();
});

</script>
<script type="text/javascript">
$(document).ready(function() {

	var v = $("#frmPre1").validate({
			submitHandler: function(form) {
				document.frmPre.submit();				
			}
		});
});

</script>
<?php include("incentive_list.php") ?>
<div class="clear"></div>
<!-- start content-outer -->
<div align="left" style="margin-left:25px; margin-top:25px;">
<!-- start content -->
	<div id="page-heading"><h1><span style="color: #d74343; font-family: Tahoma; font-weight: bold;">Add Qualifier</span></h1></div>
			<!-- start id-form -->
		<form name="frmPre" id="frmPre" method="post" action="<?php $_SERVER['PHP_SELF'] ?>" enctype="multipart/form-data" >
			<table border="0" cellpadding="0" cellspacing="0" id="id-form">
			<tr>
				<th valign="top">Incentive Type:</th>
				<td>
				<select name="type" id="type" class="required styledselect_form_4" onchange="inctype(this.value);">
					<?php 
					$auInc=$_objAdmin->_getSelectList2('table_type as t',"t.type_id,t.type_description",''," NOT EXISTS (SELECT i.type_id FROM table_target_incentive AS i WHERE i.type_id = t.type_id and i.applied_incentive_id='".$_REQUEST['inc']."' and i.status='A') and t.status='A' and t.type_id!='".$_REQUEST['id']."'ORDER BY t.type_id");
					if(is_array($auInc)){
					for($i=0;$i<count($auInc);$i++){
					?>
					<option value="<?php echo $auInc[$i]->type_id;?>"><?php echo $auInc[$i]->type_description;?></option>
						<?php } ?>
					<?php } else { ?>
					<option value="">All Incentive Type are Added</option>
				<?php } ?>
				</select>	
				</td>
				<td></td>
			</tr>
			<tr id="showcat" style="display:none;">
				<th valign="top">Category Code:</th>
				<td>
				<input id="category_list" style="width: 500px; height: 30px;" class="ui-autocomplete-input required" autocomplete="off" role="textbox" aria-autocomplete="list" aria-haspopup="true" name="category_list" placeholder="Search Category Code" />
				</td>
				<td></td>
			</tr>
			<tr id="showItem" style="display:none;">
				<th valign="top">Item Code:</th>
				<td>
				<input id="item_list" style="width: 500px; height: 30px;" class="ui-autocomplete-input required" autocomplete="off" role="textbox" aria-autocomplete="list" aria-haspopup="true" name="item_list" placeholder="Search Item Code" />
				</td>
				<td></td>
			</tr>
			<tr id="showItem1" style="display:none;">
				<th valign="top">Item Code:</th>
				<td>
				<input id="item_list1" style="width: 500px; height: 30px;" class="ui-autocomplete-input required" autocomplete="off" role="textbox" aria-autocomplete="list" aria-haspopup="true" name="item_list1" placeholder="Search Item Code" />
				</td>
				<td></td>
			</tr>
			<tr id="showFocusItem" style="display:none;">
				<th valign="top">Item Code:</th>
				<td>
				<input id="focus_item_list" style="width: 500px; height: 30px;" class="ui-autocomplete-input required" autocomplete="off" role="textbox" aria-autocomplete="list" aria-haspopup="true" name="focus_item_list" placeholder="Search Focus Item Code" />
				</td>
				<td></td>
			</tr>
			<tr id="showValue" style="display:none;">
				<th id="showCategoryNumber" style="display:none;" valign="top" >Minimum Quantity:</th>
				<th id="showItemDet1" style="display:none;" valign="top" >Minimum Quantity:</th>
				<th id="showItemDet2" style="display:none;" valign="top" >Number OF Item:</th>
				<th id="showItemNumber" style="display:none;" valign="top">Minimum Items:</th>
				<th valign="top" id="showOrderNumber" style="display:none;">Minimum Orders:</th>
				<th valign="top" id="showOrderValue" style="display:none;">Minimum Order Value:</th>
				<th id="showSchemeNumber" style="display:none;" valign="top">Minimum Schemes:</th>
				<th id="showRetailerNumber" style="display:none;"  valign="top">Minimum Retailers:</th>					
				<th id="showRetailerAdded" style="display:none;"  valign="top">Retailers To Be Added:</th>
				<th id="showFocusRetailerItemNumber" style="display:none;" valign="top">Minimum Quantity:</th>
				<th id="showTotalCall" style="display:none;" valign="top">Minimum Total Call:</th>
				<th id="showTotalAmount" style="display:none;" valign="top">Minimum Amount:</th>
				<td id="showPrimaryVal" style="display:none;"><input type="text"  style="height: 30px;" name="primary_values" id="primary_values"  value="" class="required numberDE"/></td>
				<td></td>
			</tr>
			<tr id="showSecondaryValuerow" style="display:none;"> 
				<th id="showSecondaryValueRetailer" valign="top" style="display:none;">Minimum Retailers:</th>
				<th id="showSecondaryValueOrder" valign="top" style="display:none;" >Minimum Order:</th>
				<th id="showSecondaryValueProductive" valign="top" style="display:none;" >% of Productive Calls:</th>
				<td>
				<input type="text" id="secondary_value" style="height: 30px;" class="required numberDE" name="secondary_value" />
				</td>
				<td></td>
			</tr>
			<br/>
			<tr>
				<th>&nbsp;</th>
				<td valign="top">
					<input type="hidden" name="qualifiers_type" value="2"  />
					<input type="hidden" name="applied_incentive_id" value="<?php echo $auRec[0]->target_incentive_id;?>"/>
					<input type="hidden" name="description" value="<?php echo $auRec[0]->description;?>"/>
					<input type="hidden" name="target_incentive_type" value="<?php echo $auRec[0]->target_incentive_type;?>"/>
					<input type="hidden" name="dur_id" value="<?php echo $auRec[0]->dur_id;?>"  />
					<input type="hidden" name="incentive_reward_amount" value="<?php echo $auRec[0]->incentive_reward_amount;?>"/>
					<input type="hidden" name="party_type_id" value="<?php echo $auRec[0]->party_type_id;?>"/>
					<input type="hidden" name="end_date" value="<?php echo $auRec[0]->end_date;?>"/>
					<input name="add" type="hidden" value="yes" />
					<input type="reset" value="Reset!" class="form-reset">
					<input type="button" value="Close" class="form-reset" onclick="javascript:window.close();" />
					<input name="submit" class="form-submit" type="submit" id="submit" value="Add"  />
				</td>
			</tr>
			</table>
		</form>
	</div>
</div>