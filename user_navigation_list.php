<?php
include("includes/config.inc.php");
include("includes/function.php");
//include("includes/globalarraylist.php");

$page_name = "WhiteSpot Cleaner";


$_objAdmin = new Admin();

if(isset($_REQUEST['show']) && $_REQUEST['show']=="yes"){
	$_objAdmin->showNavigationList();
	die;
}

?>
<?php include("header.inc.php");?>
<div class="m-grid__item m-grid__item--fluid m-wrapper" style="background-color:">
    <!-- BEGIN: Subheader -->
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="#" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__separator">
                        -
                    </li>
                    <li class="m-nav__item">
                        <a href="#" class="m-nav__link">
                            <span class="m-nav__link-text">
                                Master
                            </span>
                        </a>
                    </li>
                    <li class="m-nav__separator">
                        -
                    </li>
                    <li class="m-nav__item">
                        <a href="#" class="m-nav__link">
                            <span class="m-nav__link-text">
                                Navigation
                            </span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <!-- END: Subheader -->
    <div class="m-content">
       
        <div class="m-portlet m-portlet--mobile">
            
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            User Navigation
                        </h3>
                    </div>
                </div>
            </div>
            <div class="m-portlet__body">
                <!--begin: Search Form -->
                <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
                    <div class="row align-items-center">
                        <div class="col-xl-8 order-2 order-xl-1">
                            <div class="form-group m-form__group row align-items-center">
                                <div class="col-md-4">
                                    <div class="m-input-icon m-input-icon--left">
                                        <input type="text" class="form-control m-input" placeholder="Search..." id="generalSearch">
                                        <span class="m-input-icon__icon m-input-icon__icon--left">
                                            <span>
                                                <i class="la la-search"></i>
                                            </span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-4 order-1 order-xl-2 m--align-right add">
                         	<a href="manage_navigation.php" class="btn btn-warning m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                                <span>
                                    <i class="la la-cart-plus"></i>
                                    <span>
                                        Add Navigation
                                    </span>
                                </span>
                            </a>
                            <div class="m-separator m-separator--dashed d-xl-none "></div>
                        </div>
                    </div>
                </div>
                <!--end: Search Form -->
                <!--begin: Datatable -->
                <div class="m_datatable" id="user_navigation_list"></div>
                <!--end: Datatable -->
            </div>
        </div>
    </div>
</div>
</div>
</div>
<!-- start footer -->         
<?php include("footer.php") ?>
<script type="text/javascript" >
    var DefaultDatatableDemo = function () {
        //== Private functions

        // basic demo
        var demo = function () {
            var datatable = $('.m_datatable').mDatatable({
                data: {
                    type: 'remote',
                    source: {
                        read: {
                            url: '<?php echo $ajaxUrl . '?pageType=userNavigationList' ?>'
                        }
                    },
                    pageSize: 20,
                    serverPaging: true,
                    serverFiltering: true,
                    serverSorting: true
                },
                layout: {
                    theme: 'default',
                    class: '',
                    scroll: true,
                    footer: false
                },
                sortable: true,
                filterable: false,
                pagination: true,
                search: {
                    input: $('#generalSearch')
                },
                columns: [{
                        field: "nav_privilege_id",
                        title: "#",
                        width:40,
                        sortable: false,
                        selector: {class: 'm-checkbox--solid m-checkbox--brand'}
                    }, {
                        field: "company_name",
                        title: "Company Name",
                        sortable: 'asc'
                    }, {
                        field: "type",
                        title: "User Type",
                        responsive: {visible: 'lg'}
                    }, {
                        field: "username",
                        title: "User Name",
                        responsive: {visible: 'lg'}
                    }, {
                        field: "Previlages",
                        title: "page_privileges",
                        width:500,
                        responsive: {visible: 'lg'}
                    }, {
                        field: "Actions",
                        width: 110,
                        title: "Actions",
                        sortable: false,
                        locked: {right: 'xl'},
                        overflow: 'visible',
                        template: function (row, index, datatable) {
                            var dropup = (datatable.getPageSize() - index) <= 4 ? 'dropup' : '';
                            return '\<a href="manage_navigation.php?id='+row.nav_privilege_id+'" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only edit m-btn--pill" title="Edit Navigation"><i class="la la-edit"></i>\
                                                </a>';

                        }
                    }]
            });
        };

        return {
            // public functions
            init: function () {
                demo();
            }
        };
    }();

    jQuery(document).ready(function () {
        DefaultDatatableDemo.init();
    });



</script>


