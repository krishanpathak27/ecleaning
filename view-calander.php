
 <?php
include("includes/config.inc.php");
include("includes/function.php");
$page_name="Booking";
$_objAdmin = new Admin();
$objArrayList= new ArrayList();

if(isset($_POST['add']) && $_POST['add'] == 'yes'){
    if($_POST['booking_status'] == 'AL' && $_POST['cleaner_id'] == "") {
        $err =  "To allot a booking cleaner is require";
        $auRec[0]=(object)$_POST;
    } else {
        $cid=$_objAdmin->updateBooking($_POST['id']);
        $sus="Booking has been updated successfully."; 
    }
} else {
    if(isset($_REQUEST['cleaner_id']) && $_REQUEST['cleaner_id']!="")
    {
        $_objAdmin->showClenerServiceCount($_REQUEST['cleaner_id']);
        die;
    }
    if(isset($_REQUEST['id']) && $_REQUEST['id']!="")
    {
        $select = "table_booking_details AS bd "
                   ."LEFT JOIN table_booking_allotment AS ba ON bd.booking_detail_id = ba.booking_detail_id "
                   ."LEFT JOIN table_cleaner AS c ON c.cleaner_id = bd.cleaner_id ";
        $fields = " bd.booking_detail_id,c.cleaner_name,ba.booking_status,bd.cleaner_id";
        $auRec=$_objAdmin->_getSelectList($select,$fields,''," bd.booking_detail_id=".$_REQUEST['id']);
        if(count($auRec)<=0) header("Location: booking.php");
    } else {
        header("Location: booking.php");
    }
}
?>
<?php include("header.inc.php");?>
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <!-- BEGIN: Subheader -->
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">
                    Cleaner Allotment
                </h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="index.php" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__separator">
                      
                    </li>
                    <li class="m-nav__item">
                        <a href="" class="m-nav__link">
                            <span class="m-nav__link-text">
                                
                            </span>
                        </a>
                    </li>
                    <li class="m-nav__separator">
                       
                    </li>
                    <li class="m-nav__item">
                        <a href="" class="m-nav__link">
                            <span class="m-nav__link-text">
                                
                            </span>
                        </a>
                    </li>  
                </ul>
            </div> 
        </div>
    </div>
    <!-- END: Subheader -->
    <div class="m-content">
        <div class="row">
            <div class="col-lg-12">
                <!--begin::Portlet--> 
                <div class="m-portlet">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <span class="m-portlet__head-icon m--hide">
                                    <i class="la la-gear"></i>
                                </span>
                                <h3 class="m-portlet__head-text">
                                    Cleaner Allotment Form
                                </h3>
                            </div>
                        </div>
                    </div>
                    <!--begin::Form-->
                    <form class="m-form m-form--fit m-form--label-align-right" id="m_form_1"  method="post" action="add_booking.php" enctype="multipart/form-data">

                        <div class="m-portlet__body">
                            <div class="m-form__content">
                                <div class="m-alert m-alert--icon alert alert-danger m--hide" role="alert" id="m_form_1_msg">
                                    <div class="m-alert__icon">
                                        <i class="la la-warning"></i>
                                    </div>
                                    <div class="m-alert__text">
                                        Oh snap! Change a few things up and try submitting again.
                                    </div>
                                    <div class="m-alert__close">
                                        <button type="button" class="close" data-close="alert" aria-label="Close"></button>
                                    </div>
                                </div>
                            </div>
                             <?php if(isset($sus) && $sus != ""){ ?>
                                <div role="alert" style="background: #d7fbdc;" class="m-alert   m-alert--icon m-alert--air m-alert--square alert alert-dismissible m--margin-bottom-30">
                                  <div class="m-alert__icon" >
                                          <i class="flaticon-exclamation m--font-brand"></i>
                                  </div>
                                  <div class="m-alert__text">
                                         <?php echo $sus; ?> 
                                  </div>
                                </div>
                            <?php } ?>
                            <?php if(isset($err) && !empty($err)){ ?>
                                <div class="m-form__content">
                                    <div class="m-alert m-alert--icon alert alert-danger" role="alert" id="m_form_1_msg1">
                                        <div class="m-alert__icon">
                                            <i class="la la-warning"></i>
                                        </div>
                                        <div class="m-alert__text">
                                           Error. <?php echo $err; ?>
                                        </div>
                                        <div class="m-alert__close">
                                            <button type="button" class="close" data-close="alert" aria-label="Close"></button>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                            <div class="form-group m-form__group row">
                                <div class="col-lg-4">
                                    <label>
                                        Cleaner:
                                    </label>
                                    <div class="input-group m-input-group m-input-group--square"> 
                                        <select class="form-control m-input m-input--square" id="cleaner_id" name="cleaner_id">
                                           <option value="">Select Cleaner</option>
                                           <?php $condi = "";
                                                $branchList=$_objAdmin->_getSelectList('table_cleaner',"cleaner_id,cleaner_name",''," status='A'");
                                            
                                                for($i=0;$i<count($branchList);$i++){
                  
                                                    if($branchList[$i]->cleaner_id==$auRec[0]->cleaner_id){$select="selected";} else {$select="";}
                    
                                                ?>
                                                  <option value="<?php echo $branchList[$i]->cleaner_id; ?>" <?php echo $select ?>><?php echo $branchList[$i]->cleaner_name; ?></option>
                 
                                              <?php } ?>
                                          </select>
                                    </div>

                                </div>
                                <div class="col-lg-4">
                                    <label class="">
                                        Booking Status:
                                    </label>
                                    <div class="input-group m-input-group m-input-group--square"> 
                                        <select class="form-control m-input m-input--square" id="booking_status" name="booking_status">
                                          <option value="UA" <?php if($auRec[0]->booking_status=='UA') echo "selected";?> >Not Alloted</option>
                                          <option value="AL" <?php if($auRec[0]->booking_status=='AL') echo "selected";?> >Alloted</option>
                                          <option value="AC" <?php if($auRec[0]->booking_status=='AC') echo "selected";?> >Accepted</option>
                                          <option value="RJ" <?php if($auRec[0]->booking_status=='RJ') echo "selected";?> >Rejected</option>
                                          <option value="RD" <?php if($auRec[0]->booking_status=='RD') echo "selected";?> >Reached</option>
                                          <option value="SC" <?php if($auRec[0]->booking_status=='SC') echo "selected";?> >Start Cleaning</option>
                                          <option value="CC" <?php if($auRec[0]->booking_status=='CC') echo "selected";?> >Complete Cleaning</option>
                                          <option value="RP" <?php if($auRec[0]->booking_status=='RP') echo "selected";?> >Payment Received</option>
                                          <option value="CB" <?php if($auRec[0]->booking_status=='CB') echo "selected";?> >Complete Booking</option>
                                          <option value="CL" <?php if($auRec[0]->booking_status=='CL') echo "selected";?> >Cancel Booking</option>
                                        </select>
                                    </div>
                                </div>
                                
                            </div>
                            
                        </div>
                        <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                            <div class="m-form__actions m-form__actions--solid">
                                <div class="row">
                                    <div class="col-lg-4"></div>
                                    <div class="col-lg-8">
                                        <button type="submit" class="btn btn-success">
                                            Submit
                                        </button>
                                        <button type="reset" class="btn btn-secondary">
                                            Reset
                                        </button>
                                        <a href="supervisor.php">
                                        <!--<button class="btn btn-secondary" onclick="location.href='booking.php';">
                                            Back
                                        </button>-->
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <input name="account_id" type="hidden" value="<?php echo $_SESSION['accountId']; ?>" />
                       <input name="id" type="hidden" value="<?php echo $auRec[0]->booking_detail_id; ?>" />
                       <input name="add" type="hidden" value="yes" />
                    </form>
                    <!--end::Form-->
                </div>
                <!--end::Portlet-->
            </div>
        </div>
    </div>
</div>
</div>
</div>
<!-- start footer -->         
<?php include("footer.php") ?>

<script type="text/javascript">
    var FormControls = function () {
        //== Private functions

        var demo1 = function () {
            $("#m_form_1").validate({
                // define validation rules
                rules: {
                    cleaner_id: {
                        required: true
                    },
                    booking_status: {
                        required: true
                        
                    }
                },
                //display error alert on form submit  
                invalidHandler: function (event, validator) {
                    var alert = $('#m_form_1_msg');
                    alert.removeClass('m--hide').show();
                    mApp.scrollTo(alert, -200);
                },
                submitHandler: function (form) {
                    form.submit();
                }
            });
        }

        return {
            // public functions
            init: function () {
                demo1();
            }
        };
    }();

    jQuery(document).ready(function () {
        var id = '<?php echo $_REQUEST['id']; ?>';
        FormControls.init();
        if(id) {
            $('#password').rules('add', {
            required: false,
            minlength: 8
        });
        }
    });

</script>

