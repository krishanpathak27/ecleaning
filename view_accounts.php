<?php
include("includes/config.inc.php");
include("includes/function.php");
$page_name="Add Target";
$_objAdmin = new Admin();
$_objItem = new Item();


	$auRec=$_objAdmin->_getSelectList2('table_account',"*",'',"");	//echo $auRec[0]->status;
	if(count($auRec)<=0) header("Location: manage_account.php");
?>

<?php include("header.inc.php") ?>
<link rel="stylesheet" href="./base/jquery.ui.all.css">
<script src="./Autocomplete/jquery-1.5.1.js"></script>
<script src="./Autocomplete/jquery.ui.core.js"></script>
<script src="./Autocomplete/jquery.ui.widget.js"></script>
<script src="./Autocomplete/jquery.ui.button.js"></script>
<script src="./Autocomplete/jquery.ui.position.js"></script>
<script src="./Autocomplete/jquery.ui.menu.js"></script>
<script src="./Autocomplete/jquery.ui.autocomplete.js"></script>
<script src="./Autocomplete/jquery.ui.tooltip.js"></script>
<script src="./Autocomplete/jquery.ui.datepicker.js"></script>
<link rel="stylesheet" href="./base/demos.css">
<script type="text/javascript" src="./javascripts/validate.js"></script>


<script>
    $(function() {
        $( "#from" ).datepicker({
			dateFormat: "d M yy",
            defaultDate: "+1w",
            changeMonth: true,
            numberOfMonths: 3,
            onSelect: function( selectedDate ) {
                $( "#to" ).datepicker( "option", "minDate", selectedDate );
            }
        });
        $( "#to" ).datepicker({
			dateFormat: "d M yy",
            defaultDate: "+1w",
            changeMonth: true,
            numberOfMonths: 3,
            onSelect: function( selectedDate ) {
                $( "#from" ).datepicker( "option", "maxDate", selectedDate );
            }
        });
    });
</script>

	


<div class="clear"></div>
<!-- start content-outer -->
<div id="content-outer">
<!-- start content -->
	<div id="content">
	<div id="page-heading"><h1><span style="color: #d74343; font-weight: bold;">Add Company Account</span></h1></div>
		<table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">
		<tr>
			<td>
			<!--  start content-table-inner -->
			<div id="content-table-inner">
				<table border="0" width="100%" cellpadding="0" cellspacing="0">
				<tr valign="top">
					<td>
					
					<!--  end message-red -->
					
					<!--  end message-green -->
					
			<!-- start id-form -->
		<form name="frmPre" id="frmPre" method="post" action="<?php $_SERVER['PHP_SELF'] ?>" enctype="multipart/form-data" >
			<table border="0" cellpadding="0" cellspacing="0" id="id-form">
			
			<tr align="left">
				
				<td colspan="2"><?php 
					for($i=0; $i<count($auRec);$i++){ ?>
					
					<input style="margin:7px;" type="radio" name="check" id="check<?php echo $i; ?>" class="check" value="<?php echo $auRec[$i]->account_id; ?>" ><span style=" font-size:18px; font-weight:bold; padding:5px;"> <?php echo $auRec[$i]->company_name ?></span><br>
					
					<?php }?>
				
				</td>
			</tr>				
			<!--<tr>
				<th>&nbsp;</th>
				<td valign="top">
					<input type="hidden" name="target_incentive_type" value="1"  />
					<input type="hidden" name="qualifiers_type" value="1"  />
					<input name="add" type="hidden" value="yes" />
					<input type="reset" value="Reset!" class="form-reset">
					<input type="button" value="Back" class="form-reset" onClick="location.href='manage_account.php';" />
					<input name="submit" class="form-submit" type="submit" id="submit" value="Save"  />
				</td>
			</tr>-->
			</table>
		</form>
			<!-- end id-form  -->
			</td>
			<td>
			<!-- right bar-->
			<?php //include("rightbar/item_bar.php") ?>
			<div class="clear"></div>
		</div>
<!-- end related-act-bottom -->
</div>
<!-- end related-activities -->
</td>
</tr>
<tr>
<td><img src="images/blank.gif" width="695" height="1" alt="blank" /></td>
<td></td>
</tr>
</table>
<div class="clear"></div>
</div>
<!--  end content-table-inner  -->
</td>
<td id="tbl-border-right"></td>
</tr>
<!--<tr>
	<th class="sized bottomleft"></th>
	<td id="tbl-border-bottom">&nbsp;</td>
	<th class="sized bottomright"></th>
</tr>-->
</table>
<div class="clear">&nbsp;</div>

</div>
<!--  end content -->
<div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->
<div class="clear">&nbsp;</div>
    
<!-- start footer --> 
<?php include("footer.php") ?>  
<!-- end footer -->
</body>
</html>