<?php
include("includes/config.inc.php");
include("includes/function.php");
// print_r($_SESSION);
$page_name="Complaint Detail";
$_objAdmin = new Admin();
?>
<?php 
    if(isset($_REQUEST['id']))
    {
        $complaint_id = $_REQUEST['id'];
        $auRec=$_objAdmin->_getSelectList2('table_complaint as complaint left join table_distributors as distributor on distributor.distributor_id = complaint.distributor_id left join table_warehouse as warehouse on warehouse.warehouse_id=complaint.warehouse_id left join table_service_personnel as sp on warehouse.warehouse_id=sp.warehouse_id left join table_retailer as retailer on retailer.retailer_id = complaint.retailer_id left join table_item_bsn as itemBsn on itemBsn.bsn = complaint.product_serial_no left join table_item as item on item.item_id = itemBsn.item_id left join table_category as cat on cat.category_id = item.category_id left join table_brands as brnd on item.brand_id = brnd.brand_id left join table_color as clr on clr.color_id = item.item_color',"complaint.*,itemBsn.*,distributor.distributor_name,distributor.distributor_address,distributor.distributor_code,warehouse.warehouse_name,retailer.retailer_name,retailer.retailer_address,retailer.retailer_code,item.item_name,item.item_code,cat.category_name,brnd.brand_name,brnd.brand_id,clr.color_id,clr.color_desc, sp.sp_name",''," complaint_id=".$complaint_id);
        // print_r($auRec[0]);
    }

?>
<?php include("header.inc.php");
include("company.inc.php");?>
<link rel="stylesheet" type="text/css" href="css/bootstrap-table-component.css">
<link rel="stylesheet" type="text/css" href="css/stylesheet.css">

<!-- start content -->
<div id="content">
<div id="page-heading"><h1><span style="color: #d74343; font-family: Tahoma; font-weight: bold;">Complaint Detail</span></h1></div>

<table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">
<tr>
  <td>
    <div id="content-table-inner">
      <table border="0" width="100%" cellpadding="0" cellspacing="0">
     <tr valign="top">
  <td>
    
    <!-- start id-form -->
    
    <table border="0" width="100%" cellpadding="0" cellspacing="0" id="id-form">
    <?php if($auRec[0]->distributor_id!=0)
          {

    ?>
    <tr>
        <td><b>Distributor Name : </b> <?php echo $auRec[0]->distributor_name;?></td>
        <td align="right"><b>Distributor Address : </b> <?php echo $auRec[0]->distributor_address;?></td>
    </tr>
    <tr>
      <td><b>Distributor Code : </b> <?php echo $auRec[0]->distributor_code;?></td>
        <td align="right"><b>Warehouse Name : </b> <?php echo $auRec[0]->warehouse_name;?></td>
    </tr>
    <?php 
        }
        else
        {
    ?>
    <tr>
        <td><b>Dealer Name : </b> <?php echo $auRec[0]->retailer_name;?></td>
        <td align="right"><b>Dealer Address : </b> <?php echo $auRec[0]->retailer_address;?></td>
    </tr>
    <tr>
      <td><b>Dealer Code : </b> <?php echo $auRec[0]->retailer_code;?></td>
        <td align="right"><b>Warehouse Name : </b> <?php echo $auRec[0]->warehouse_name;?></td>
    </tr>
    <?php 
        }
    ?>
      <tr>
        <td><b>BSN : </b> <?php echo $auRec[0]->product_serial_no;?></td>
        <td align="right"><b>Complaint Date : </b> <?php echo date('Y-m-d',strtotime($auRec[0]->created_date));?></td>
      </tr>
      <tr>
        <td><b>Item Name : </b> <?php echo $auRec[0]->item_name;?></td>
        <td align="right"><b>Item Code : </b><?php echo $auRec[0]->item_code;?> </td>
      </tr>
      <tr>
        <td><b>Category Name : </b> <?php echo $auRec[0]->category_name;?></td>
        <td align="right"><b>Brand Name : </b> <?php echo $auRec[0]->brand_name;?></td>
        
      </tr>
      <tr>
        <td><b>Color : </b> <?php echo $auRec[0]->color_desc;?></td>
        <td align="right"><b>Battery Sale Date:</b><?php echo $auRec[0]->date_of_sale;?> </td>
      </tr>

       <tr>
        <td><b>Manufacture Date : </b> <?php echo $auRec[0]->manufacture_date;?></td>
        <td align="right"><b>Warranty End Date:</b> <?php echo $auRec[0]->warranty_end_date;?></td>
      </tr>
      <tr>
        <td><b>Warranty Period : </b> <?php echo $auRec[0]->warranty_period;?></td>
        <td align="right"><b>Warranty Prorata :</b> <?php echo $auRec[0]->warranty_prorata;?></td>
      </tr>
      <tr>
        <td colspan="2"><b>Complaint Subject :</b><?php echo $auRec[0]->reason_desc;?></td>
      </tr>
      <tr>
        <td colspan="2"><b>Complaint Remarks :</b><?php echo $auRec[0]->remark;?></td>
      </tr>
      <tr>
        <td><b>Service Personnel Name : </b> <?php echo $auRec[0]->sp_name;?></td>
      </tr>

    </table>
    <form name="frmPre" id="frmPre" method="post" action="" enctype="multipart/form-data" style="border-top:1px solid black;padding-top:10px;" >
    <?php 
      if($_SESSION['warehouseId']>0)
      {
    ?>
      <table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">
    <tr>
      <td id="tbl-border-left"></td>
      <td>
      <!--  start content-table-inner -->
      <div id="content-table-inner">
        <table border="0" width="100%" cellpadding="0" cellspacing="0" style="margin-left:10px;">
        <tr valign="top">
          <td>
          <!--  start message-red -->
          <?php if($err!=''){?>
          <div id="message-red">
            <table border="0" width="100%" cellpadding="0" cellspacing="0">
            <tr>
              <td class="red-left"> <?php echo $err; ?></td>
              <td class="red-right"><a class="close-red"><img src="images/icon_close_red.gif"   alt="" /></a></td>
            </tr>
            </table>
          </div>
          <?php }?>
          <!--  end message-red -->
          <?php if($sus!=''){?>
          <!--  start message-green -->
          <div id="message-green">
            <table border="0" width="100%" cellpadding="0" cellspacing="0">
            <tr>
              <td class="green-left"><?php echo $sus; ?></td>
              <td class="green-right"><a class="close-green"><img src="images/icon_close_green.gif"   alt="" /></a></td>
            </tr>
            </table>
          </div>
          <?php } ?>
          <!--  end message-green -->
          <div id="step-holder">



<div class="clear"></div>
</div>
      <!-- start id-form -->
      <form name="frmPre" id="frmPre" method="post" action="assign_servicepersion.php?id=<?php echo $_REQUEST['id'];?>" enctype="multipart/form-data" >
      
        <table border="0" cellpadding="0" cellspacing="0"  id="id-form">

<tr><th valign="top">BSN NUmber</th><td><?php echo $auRec[0]->product_serial_no;?></tr>
<tr><th valign="top">Sale Date</th><td><?php echo $auRec[0]->date_of_sale;?></tr>


    <tr>
    <th valign="top">Service Personnel:</th>
      <td>
      <select name="brand_id" id="brand_id" class="styledselect_form_4 required">
      <option>Please Select</option>
      <?php if($_SESSION['userLoginType']==7){
        $brands = $_objAdmin->_getSelectList('table_service_personnel','*','',"warehouse_id=".$_SESSION['warehouseId']); }
        else{
          //$brands = $_objAdmin->_getSelectList('table_service_personnel','*','',""); 
            $brands = $_objAdmin->_getSelectList('table_service_personnel','*','',''); 
        }

      if(is_array($brands)){
      for($i=0;$i<count($brands);$i++){?>
      <option value="<?php echo $brands[$i]->service_personnel_id;?>" brand_code="<?php echo $brands[$i]->service_personnel_id;?>"  ><?php echo ucwords(strtolower(trim($brands[$i]->sp_name)));?></option>
      <?php }} ?>
      </select>
      </td>
    </tr>



          <tr>
            <th>&nbsp;</th>
            <td valign="top">
                    <input name="service_persion" type="hidden" value="yes" />
                    <input name="comp_id" type="hidden" value="<?php echo $_REQUEST['id']; ?>" />
                    <input name="account_id" type="hidden" value="<?php echo $_SESSION['accountId']; ?>" />
                 
                   <input name="save" class="form-submit" type="submit" id="save" value="Save" />
                      <input type="button" value="Back" class="form-reset" onclick="location.href='distributor_wearhouse_comp.php';" />
                 
    </td>
    </tr>
          </tr>
          </table>
        </form>
      <!-- end id-form  -->
      </td>
      
      <td><?php /*function create_pyramid($limit) {
          for($row = 1; $row < $limit; $row ++) {
            $stars = str_repeat('*', ($row - 1) * 2 + 1);
            $space = str_repeat(' ', $limit - $row);
            echo $space . $stars . '<br/>';
          }
        }
          echo "<pre>" ;
        create_pyramid(10);*/?>
        
      <!--</div>-->
        
        
        </td>
      </tr>
      </table>
      <!-- right bar-->
      
      <div class="clear"></div>
      </div>
      </td>
    </tr>
  </table> 
    <?php 
      }
      else
      {
    ?>
      <table class="table">
        <tr>
          <td colspan="7"></td>
          <td><input type="button" value="Back" class="btn btn-default" onclick="location.href='distributor_wearhouse_comp.php';"></td>

        </tr>
      </table>
      <?php 
      }
      ?>
    </form>




    

  </td>

  </tr>

  </div>
  </td>
</tr>
</table>

<div class="clear"></div>
</div>
<!--  end content-table-inner  -->
</td>
</tr>
</table>

<div class="clear">&nbsp;</div>
</div>
