<?php
include("includes/config.inc.php");
include("includes/function.php");
$page_name="Message Broadcast";
$_objAdmin = new Admin();

$auRec=$_objAdmin->_getSelectList2('table_message as m left join table_salesman as s on s.salesman_id=m.salesman_id ',"m.*,s.salesman_name",''," m.message_id='".$_REQUEST['id']."' ");

?>

<?php include("header.inc.php") ?>
<style>
div.img
  {
  margin:2px;
  border:1px solid  #43A643;
  height:auto;
  width:auto;
  float:left;
  text-align:center;
  }
div.img img
  {
  display:inline;
  margin:3px;
  border:1px solid #fbfcfc;
  }
div.img a:hover img
  {
  border:1px solid #fbfcfc;
  }
div.desc
  {
  text-align:center;
  font-weight:normal;
  width:120px;
  margin:2px;
  }
</style>
<!-- start content-outer -->
<div id="content-outer">
<!-- start content -->

<div id="content">
<?php if($sus!=''){?>
	<!--  start message-green -->
	<div id="message-green">
		<table border="0" width="100%" cellpadding="0" cellspacing="0">
		<tr>
			<td class="green-left"><?php echo $sus; ?></td>
			<td class="green-right"><a class="close-green"><img src="images/icon_close_green.gif"   alt="" /></a></td>
		</tr>
		</table>
	</div>
	<?php } ?>
<div id="page-heading"><h1><span style="color: #d74343; font-family: Tahoma; font-weight: bold;">Message Broadcast</span></h1></div>
<table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">
<!--<tr>
	<th rowspan="3" class="sized"><img src="images/side_shadowleft.jpg" width="20" height="300" alt="" /></th>
	<th class="topleft"></th>
	<td id="tbl-border-top">&nbsp;</td>
	<th class="topright"></th>
	<th rowspan="3" class="sized"><img src="images/side_shadowright.jpg" width="20" height="300" alt="" /></th>
</tr>
-->
<tr>
	<!--<td id="tbl-border-left"></td>-->
	<td>
	<!--  start content-table-inner -->
	<div id="content-table-inner">
	
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
	<tr valign="top">
	<td>
		<!-- start id-form -->
		<table border="0" cellpadding="0" width="950px" cellspacing="0"  >
			<tr>
				<th valign="top" style="width: 150px;line-height: 28px;"  align="left">Salesman Name:</th>
				<td ><div style="width: 800px;" align="left"><?php echo $auRec[0]->salesman_name; ?></div></td>
			</tr>
			<tr>
				<th valign="top" style="width: 150px;line-height: 28px;"  align="left">Subject:</th>
				<td><div style="width: 800px;" align="left"><?php echo $auRec[0]->subject; ?></div></td>
			</tr>
			<tr>
				<th valign="top" style="width: 150px;line-height: 28px;"  align="left">Message:</th>
				<td><div style="width: 700px;" align="left"><?php echo $auRec[0]->message; ?></div></td>
			</tr>
			<tr>
				<th valign="top" style="width: 150px;line-height: 28px;"  align="left">Reply:</th>
				<td><div style="width: 800px;" align="left"><?php echo $auRec[0]->reply; ?></div></td>
			</tr>
			<tr>
				<th valign="top" style="width: 150px;line-height: 28px;"  align="left">Send Date:</th>
				<td><div style="width: 800px;" align="left"><?php echo $_objAdmin->_changeDate($auRec[0]->send_date); ?></div></td>
			</tr>
			<?php
			if($auRec[0]->image_required=='Yes'){
			?>
			<tr>
				<th valign="top" style="width: 150px;line-height: 28px;"  align="left">Photo:</th>
				<td align="left" style="min-width: 150px;">
				<?php
				$auImg=$_objAdmin->_getSelectList('table_image',"*",''," image_type=5 and ref_id=".$auRec[0]->message_id);
					if(is_array($auImg)){
						for($j=0;$j<count($auImg);$j++){
				?>
				<div class="img">
				<a href="photo/<?php echo $auImg[$j]->image_url;?>"  target="_blank"><img src="photo/<?php echo $auImg[$j]->image_url;?>" alt="" width="140" height="120"></a>
				</div>	
				<?php } } else { ?>
				<div class="img">
					<div style="width: 500px;height:50px;"><span style="color: #A52A2A; font-family: Tahoma; font-weight: bold;">Image No Available</span></div>
				</div>
				<?php } ?>
				</td>
			</tr>
			<?php } ?>
			
			<tr>
				<th valign="top" style="width: 150px;line-height: 28px;"  align="left"></th>
				<td align="left" style="min-width: 150px;"><input type="button" value="Close" class="form-cen" onclick="javascript:window.close();" /></td>
			</tr>
		</table>
	<!-- end id-form  -->

	</td>
	<td>

	<!-- right bar-->
	<?php //include("rightbar/category_bar.php") ?>
</td>
</tr>
<tr>
<td><img src="images/blank.gif" width="695" height="1" alt="blank" /></td>
<td></td>
</tr>
</table>
<div class="clear"></div>
</div>
<!--  end content-table-inner  -->
</td>
<td id="tbl-border-right"></td>
</tr>
<!--<tr>
	<th class="sized bottomleft"></th>
	<td id="tbl-border-bottom">&nbsp;</td>
	<th class="sized bottomright"></th>
</tr>-->
</table>
<div class="clear">&nbsp;</div>
</div>
<!--  end content -->
<div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->
<div class="clear">&nbsp;</div> 
<!-- start footer -->         
<?php include("footer.php") ?>
<!-- end footer -->
 
</body>
</html>