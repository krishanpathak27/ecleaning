<?php
include("includes/config.inc.php");
include("includes/function.php");
$page_name="Complaint";
$_objAdmin = new Admin();

$auRec=$_objAdmin->_getSelectList2('table_communication  AS TC
		left join table_salesman AS S ON S.salesman_id = TC.salesman_id  
		left join table_retailer AS R ON R.retailer_id = TC.retailer_id 
		LEFT JOIN table_salesman_hierarchy_relationship AS SH ON SH.salesman_id = S.salesman_id
		LEFT JOIN table_salesman AS S2 ON S2.salesman_id = SH.rpt_user_id 
		LEFT JOIN city AS C ON C.city_id= S.city
		LEFT JOIN state AS st ON st.state_id = S.state',
		" TC.*, S.salesman_name,R.retailer_name, C.city_name, S2.salesman_name AS rpt_to, st.state_name ",''," TC.comm_id='".$_REQUEST['id']."' ");
		
		if(sizeof($auRec)<=0) { header("location: message_communication.php"); }
?>

<?php include("header.inc.php") ?>
<style>
div.img
  {
  margin:2px;
  border:1px solid  #43A643;
  height:auto;
  width:auto;
  float:left;
  text-align:center;
  }
div.img img
  {
  display:inline;
  margin:3px;
  border:1px solid #fbfcfc;
  }
div.img a:hover img
  {
  border:1px solid #fbfcfc;
  }
div.desc
  {
  text-align:center;
  font-weight:normal;
  width:120px;
  margin:2px;
  }
</style>
<!-- start content-outer -->
<div id="content-outer">
<!-- start content -->

<div id="content">
<?php if($sus!=''){?>
	<!--  start message-green -->
	<div id="message-green">
		<table border="0" width="100%" cellpadding="0" cellspacing="0">
		<tr>
			<td class="green-left"><?php echo $sus; ?></td>
			<td class="green-right"><a class="close-green"><img src="images/icon_close_green.gif"   alt="" /></a></td>
		</tr>
		</table>
	</div>
	<?php } ?>
<div id="page-heading"><h1><span style="color: #d74343; font-family: Tahoma; font-weight: bold;">Complaint</span></h1></div>
<table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">
<tr>
	<!--<td id="tbl-border-left"></td>-->
	<td>
	<!--  start content-table-inner -->
	<div id="content-table-inner">
	
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
	<tr valign="top">
	<td>
		<!-- start id-form -->
		<table border="0" cellpadding="0" width="950px" cellspacing="0"  >
			<tr>
				<th valign="top" style="width: 150px;line-height: 28px;"  align="left">Salesman Name:</th>
				<td ><div style="width: 800px;" align="left"><?php echo $auRec[0]->salesman_name; ?></div></td>
			</tr>
			<tr>
				<th valign="top" style="width: 150px;line-height: 28px;"  align="left">Salesman State:</th>
				<td><div style="width: 800px;" align="left"><?php echo $auRec[0]->state_name; ?></div></td>
			</tr>
			<tr>
				<th valign="top" style="width: 150px;line-height: 28px;"  align="left">Salesman City:</th>
				<td><div style="width: 800px;" align="left"><?php echo $auRec[0]->city_name; ?></div></td>
			</tr>
			<tr>
				<th valign="top" style="width: 150px;line-height: 28px;"  align="left">Retailer Name:</th>
				<td><div style="width: 800px;" align="left"><?php echo $auRec[0]->retailer_name; ?></div></td>
			</tr>
			<!--<tr>
				<th valign="top" style="width: 150px;line-height: 28px;"  align="left">Reporting to:</th>
				<td><div style="width: 800px;" align="left"><?php echo $auRec[0]->rpt_to; ?></div></td>
			</tr>-->
			<tr>
				<th valign="top" style="width: 150px;line-height: 28px;"  align="left">Message:</th>
				<td><div style="width: 700px;" align="left"><?php echo $auRec[0]->comm_message; ?></div></td>
			</tr>
			<tr>
				<th valign="top" style="width: 150px;line-height: 28px;"  align="left">Reason:</th>
				<td><div style="width: 800px;" align="left"><?php echo $auRec[0]->reason; ?></div></td>
			</tr>
			<tr>
				<th valign="top" style="width: 150px;line-height: 28px;"  align="left">Date:</th>
				<td><div style="width: 800px;" align="left"><?php echo $_objAdmin->_changeDate($auRec[0]->comm_date); ?></div></td>
			</tr>
			<tr>
				<th valign="top" style="width: 150px;line-height: 28px;"  align="left">Time:</th>
				<td><div style="width: 800px;" align="left"><?php echo date('h:i A', strtotime($auRec[0]->comm_time)); ?></div></td>
			</tr>
			
			
			<tr><td colspan="2">
			<?php $flag = 1;  if($flag==1){ ?>
					<?php $auImg=$_objAdmin->_getSelectList('table_image AS i',"i.*",''," ref_id=".$auRec[0]->comm_id." AND image_type='12'"); //print_r($auImg);
						if(is_array($auImg)){
							for($j=0;$j<count($auImg);$j++){?>
							<div class="img">
								<a href="photo/<?php echo $auImg[$j]->image_url;?>"  target="_blank">
									<img src="photo/<?php echo $auImg[$j]->image_url;?>" alt="" width="140" height="120">
								</a>
							</div>
						<?php } } else { ?>
						<div class="img">
							<div style="width: 100%;height:50px;">
								<span style="color: #A52A2A; font-family: Tahoma; font-weight: bold;">Image Not Available</span>
							</div>
						</div>
					   <?php } ?>
<?php } ?>
	</td></tr>
		
			<tr>
				<th valign="top" style="width: 150px;line-height: 28px;"  align="left"><input type="button" value="Close" class="form-cen" onclick="javascript:window.close();" /></th>
				<td align="left" style="min-width: 150px;"></td>
			</tr>
		</table>
	<!-- end id-form  -->

	</td>
	<td><?php //include("rightbar/category_bar.php") ?></td>
</tr>
<tr>
<td><img src="images/blank.gif" width="695" height="1" alt="blank" /></td>
<td></td>
</tr>
</table>
<div class="clear"></div>
</div>
<!--  end content-table-inner  -->
</td>
<td id="tbl-border-right"></td>
</tr>
<!--<tr>
	<th class="sized bottomleft"></th>
	<td id="tbl-border-bottom">&nbsp;</td>
	<th class="sized bottomright"></th>
</tr>-->
</table>
<div class="clear">&nbsp;</div>
</div>
<!--  end content -->
<div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->
<div class="clear">&nbsp;</div> 
<!-- start footer -->         
<?php include("footer.php") ?>
<!-- end footer -->
 
</body>
</html>