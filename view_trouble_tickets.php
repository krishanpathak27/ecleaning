<?php
include("includes/config.inc.php");
include("includes/function.php");
$page_name="Trouble Tickets";
$_objAdmin = new Admin();


if(isset($_POST['add']) && $_POST['add'] == 'yes')
{ 
$update_date=date("Y-m-d",strtotime("-1 day"));
$id=$_objAdmin->_dbUpdate2(array("status"=>$_POST['status'],"last_update_date"=>$update_date),'table_trouble_tickets', " trouble_tickets_id='".$_POST['trouble_tickets_id']."'");
header("Location: trouble_tickets.php?sus=sus&searchParam_1=".$_REQUEST['searchParam_1']."&searchParam_2=".$_REQUEST['searchParam_2']."&searchParam_3=".$_REQUEST['searchParam_3']."");
//$sus="Trouble Tickets has been updated successfully.";

}
$auRec=$_objAdmin->_getSelectList2('table_trouble_tickets as t left join table_salesman as s on s.salesman_id=t.salesman_id ',"t.*,s.salesman_name",''," t.trouble_tickets_id='".$_REQUEST['id']."' ");

?>

<?php include("header.inc.php") ?>
<!-- start content-outer -->
<div id="content-outer">
<!-- start content -->

<div id="content">
<?php if($sus!=''){?>
	<!--  start message-green -->
	<div id="message-green">
		<table border="0" width="100%" cellpadding="0" cellspacing="0">
		<tr>
			<td class="green-left"><?php echo $sus; ?></td>
			<td class="green-right"><a class="close-green"><img src="images/icon_close_green.gif"   alt="" /></a></td>
		</tr>
		</table>
	</div>
	<?php } ?>
<div id="page-heading"><h1><span style="color: #d74343; font-family: Tahoma; font-weight: bold;">Trouble Tickets</span></h1></div>
<table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">
<!--<tr>
	<th rowspan="3" class="sized"><img src="images/side_shadowleft.jpg" width="20" height="300" alt="" /></th>
	<th class="topleft"></th>
	<td id="tbl-border-top">&nbsp;</td>
	<th class="topright"></th>
	<th rowspan="3" class="sized"><img src="images/side_shadowright.jpg" width="20" height="300" alt="" /></th>
</tr>
-->
<tr>
	<!--<td id="tbl-border-left"></td>-->
	<td>
	<!--  start content-table-inner -->
	<div id="content-table-inner">
	
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
	<tr valign="top">
	<td>
		<!-- start id-form -->
		<form name="frmPre" id="frmPre" method="post" action="" enctype="multipart/form-data" >
		<table border="0" cellpadding="0" width="950px" cellspacing="0"  >
			<tr>
				<th valign="top" style="width: 150px;line-height: 28px;"  align="left">Date:</th>
				<td ><div style="width: 800px;" align="left"><?php echo $_objAdmin->_changeDate($auRec[0]->trouble_tickets_date); ?></div></td>
			</tr>
			<tr>
				<th valign="top" style="width: 150px;line-height: 28px;"  align="left">Salesman Name:</th>
				<td >
					<div style="width: 800px;" align="left"><?php echo $auRec[0]->salesman_name; ?> 
					<?php if ($opRec[0]->view_activity_report=="Yes"){ ?>
					 (<a href="salesman_report.php?sal=<?php echo base64_encode($auRec[0]->salesman_id);?>&date=<?php echo base64_encode($auRec[0]->trouble_tickets_date); ?>" target="_blank">View Salesman Timeline</a>)
				<?php } ?>
					</div>
				</td>
			</tr>
			<tr>
				<th valign="top" style="width: 150px;line-height: 28px;"  align="left">Message:</th>
				<!--<td><div style="width: 400px; line-height: 15px;" align="left"></div></td>-->
				<td ><textarea style="border: none" rows="8" cols="100" readonly ><?php echo $auRec[0]->message; ?> </textarea></tr>
			</tr>
			<tr>
				<th valign="top" style="width: 150px;line-height: 28px;"  align="left">Reply:</th>
				<!--<td><div style="width: 800px;" align="left"></div></td>-->
				<td ><textarea style="border: none" rows="6" cols="100" readonly ><?php echo $auRec[0]->reply; ?> </textarea></tr>
			</tr>
			<tr>
				<th valign="top" style="width: 150px;line-height: 28px;"  align="left">Reply Date:</th>
				<td><div style="width: 800px;" align="left"><?php echo $_objAdmin->_changeDate($auRec[0]->reply_date); ?></div></td>
			</tr>
			<tr>
				<th valign="top" style="width: 150px;line-height: 28px;"  align="left">Status:</th>
				<td>
				<select name="status" id="status" class="styledselect_form_3">
					<?php 
					if($auRec[0]->status=='New'){
					?>
					<option value="New" <?php if($auRec[0]->status=='New'){ ?> selected <?php } ?>>New</option>
					<?php } ?>
					<?php 
					if($auRec[0]->status=='Replied'){
					?>
					<option value="Replied" <?php if($auRec[0]->status=='Replied'){ ?> selected <?php } ?>>Replied</option>
					<option value="Accepted" <?php if($auRec[0]->status=='Accepted'){ ?> selected <?php } ?>>Accepted</option>
					<option value="Rejected" <?php if($auRec[0]->status=='Rejected'){ ?> selected <?php } ?>>Rejected</option>
					<?php } ?>
					<?php 
					if($auRec[0]->status=='Accepted' || $auRec[0]->status=='Rejected'){
					?>
					<option value="Accepted" <?php if($auRec[0]->status=='Accepted'){ ?> selected <?php } ?>>Accepted</option>
					<option value="Rejected" <?php if($auRec[0]->status=='Rejected'){ ?> selected <?php } ?>>Rejected</option>
					<?php } ?>
				</select>
				</td>
			</tr>
			<tr>
				<th valign="top" style="width: 150px;line-height: 28px;"  align="left"> </th>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<th>&nbsp;</th>
				<td valign="top">
				<input name="add" type="hidden" value="yes" />
				<input name="trouble_tickets_id" type="hidden" value="<?php echo $auRec[0]->trouble_tickets_id; ?>" />
				<input name="submit" class="form-submit" type="submit" id="submit" value="Save" />
				<input type="button" value="Back" class="form-cen" onclick="location.href='trouble_tickets.php?searchParam_1=<?php echo $_REQUEST['searchParam_1']; ?>&searchParam_2=<?php echo $_REQUEST['searchParam_2']; ?>&searchParam_3=<?php echo $_REQUEST['searchParam_3']; ?>';" />
				</td>
			</tr>
		</table>
		</form>
	<!-- end id-form  -->

	</td>
	<td>

	<!-- right bar-->
	<?php //include("rightbar/category_bar.php") ?>
</td>
</tr>
<tr>
<td><img src="images/blank.gif" width="695" height="1" alt="blank" /></td>
<td></td>
</tr>
</table>
<div class="clear"></div>
</div>
<!--  end content-table-inner  -->
</td>
<td id="tbl-border-right"></td>
</tr>
<!--<tr>
	<th class="sized bottomleft"></th>
	<td id="tbl-border-bottom">&nbsp;</td>
	<th class="sized bottomright"></th>
</tr>-->
</table>
<div class="clear">&nbsp;</div>
</div>
<!--  end content -->
<div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->
<div class="clear">&nbsp;</div> 
<!-- start footer -->         
<?php include("footer.php") ?>
<!-- end footer -->
 
</body>
</html>